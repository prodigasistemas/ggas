<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<h1 class="tituloInterno">Pesquisar A��o de Cobran�a<a class="linkHelp" href="<help:help>/consultandoaodecobrana.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir um novo modelo clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form method="post" name="acaoCobrancaForm" id="acaoCobrancaForm" action="pesquisarAcaoCobranca">
	<script type='text/javascript' src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'> </script>
	<script type='text/javascript'>

		$(document).ready(function(){	

			$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
			
		});
	
		function limparFormulario() {		
			document.forms[0].descricao.value = "";
			document.forms[0].tipoDocumentoGerado.value = "-1";	
		}	

		function incluir() {
			location.href = '<c:url value="/exibirInclusaoAcaoCobranca"/>';			
		}

		function alterarAcaoCobranca(){
			
			var selecao = verificarSelecaoApenasUm();
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("acaoCobrancaForm", "exibirAlteracaoAcaoCobranca");
		    }
		}

		function detalharAcaoCobranca(chave) {
			document.forms[0].chavePrimaria.value = chave;
			submeter("acaoCobrancaForm", "exibirDetalhamentoAcaoCobranca");
		}

		function removerAcaoCobranca(){
			
			var selecao = verificarSelecao();
			if (selecao == true) {	
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('acaoCobrancaForm', 'excluirAcaoCobranca');
				}
		    }
		}
			
		
	</script>
	
	<token:token></token:token>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarAcaoCobranca">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="chavesPrimarias" type="hidden" id="chavesPrimarias" >
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	
	<fieldset id="pesquisarAcaoCobranca" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="descricao" >Descri��o da A��o:</label>
			<input class="campoTexto" id="descricao" type="text" name="descricao" maxlength="30" size="35" value="${acaoCobrancaVO.descricao}" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"><br />
		</fieldset>
		<fieldset class="colunaFinal">
			<label class="rotulo" for="tipoDocumentoGerado" >Tipo de Documento Gerado:</label>
			<select name="tipoDocumentoGerado" id="tipoDocumentoGerado" class="campoSelect">
		    	<option value="-1">Selecione</option>
		    	<c:forEach items="${listaTipoDocumento}" var="tipoDocumentoGerado">
					<option value="<c:out value="${tipoDocumentoGerado.chavePrimaria}"/>" <c:if test="${acaoCobrancaVO.tipoDocumentoGerado == tipoDocumentoGerado.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoDocumentoGerado.descricao}"/>
					</option>		
			    </c:forEach>					
		    </select>
		</fieldset>	
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="pesquisarAcaoCobranca">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit" >
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaAcaoCobranca ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaAcaoCobranca" sort="list" id="acaoCobranca"  decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarAcaoCobranca">
	        
	         <display:column style="text-align: center; width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
                 <input id="checkboxChavesPrimarias" type="checkbox" name="chavesPrimarias" value="${acaoCobranca.chavePrimaria}">
             </display:column>
             
	        <display:column sortable="true" sortProperty="descricao" title="Descri��o" headerClass="tituloTabelaEsq" maxLength="99" style="text-align: left">
	            <a href="javascript:detalharAcaoCobranca(<c:out value='${acaoCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acaoCobranca.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Dias de<br />Validade" sortProperty="numeroDiasValidade" style="width: 80px">
	            <a href="javascript:detalharAcaoCobranca(<c:out value='${acaoCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acaoCobranca.numeroDiasValidade}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Dias para o<br />Vencimento" sortProperty="numeroDiasVencimento" style="width: 80px">
	            <a href="javascript:detalharAcaoCobranca(<c:out value='${acaoCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acaoCobranca.numeroDiasVencimento}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" title="Tipo de Documento" sortProperty="documento.descricao" style="width: 130px">
	            <a href="javascript:detalharAcaoCobranca(<c:out value='${acaoCobranca.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${acaoCobranca.documento.descricao}"/>
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaAcaoCobranca}">
    		<vacess:vacess param="exibirAlteracaoAcaoCobranca">
   				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" id="botaoAlterar" onclick="alterarAcaoCobranca();" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="excluirAcaoCobranca">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerAcaoCobranca()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoAcaoCobranca">
			<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" value="Incluir" type="button" onclick="incluir();">
		</vacess:vacess>
	</fieldset>
	    
</form>



	