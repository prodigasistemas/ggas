<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<script>

	function limparFormulario(){
		document.forms[0].acaoPredecessora.value = "-1";
		document.forms[0].descricaoAcao.value = "";
		document.forms[0].numeroDiasValidadeAcao.value = "";
		document.forms[0].numeroDiasVencimento.value = "";
		document.forms[0].tipoDocumentoGerado.value = "-1";
		document.forms[0].gerarOrdemServico[1].checked = "false";
		document.forms[0].consideraDebitosVencer[1].checked = "false";
		document.forms[0].bloquearEmissaoDocumentosPagaveis[1].checked = "false";
			
	}	
	
	function cancelar() {
		submeter('0','pesquisarAcaoCobranca?unlock=true');
	}

</script>


<h1 class="tituloInterno">Alterar A��o de Cobran�a<a class="linkHelp" href="<help:help>/aodecobranainclusoalterao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Salvar</span> para finalizar.</p>

<form method="post" action="alterarAcaoCobranca" > 

<token:token></token:token>
<input type="hidden" name="acao" id="acao" value="alterarAcaoCobranca">
<input name="postBack" type="hidden" id="postBack" value="true">
<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${acaoCobrancaVO.chavePrimaria}"/>
<input name="versao" type="hidden" id="versao" value="${acaoCobrancaVO.versao}">

<fieldset id="dadosAcaoCobranca" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna">
		<label class="rotulo" for="acaoPredecessora">A��o predecessora:</label>
		<select name="acaoPredecessora" id="acaoPredecessora" class="campoSelect">
	    	<option value="-1">Selecione</option>
	    	<c:forEach items="${listaAcaoCobranca}" var="acaoPredecessora">
				<option title="<c:out value="${acaoPredecessora.descricao}"/>" value="<c:out value="${acaoPredecessora.chavePrimaria}"/>" <c:if test="${acaoCobrancaVO.acaoPredecessora == acaoPredecessora.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${acaoPredecessora.descricao}"/>
				</option>		
		    </c:forEach>				
	    </select><br />
	    
		<label class="rotulo campoObrigatorio" for="descricaoAcao"><span class="campoObrigatorioSimbolo">* </span>Descri��o da a��o:</label>
		<textarea id="descricao" class="campoTexto" name="descricao" 
		onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');"
		onkeypress="return formatarCampoTextoLivreComLimite(event,this,100);">${acaoCobrancaVO.descricao}</textarea>
		<label class="rotuloInformativo">M�ximo de 100 caracteres</label><br class="quebraLinha2" />
		
		<label class="rotulo rotulo2Linhas" for="numeroDiasValidadeAcao">N�mero de dias de validade da a��o:</label>
		<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numeroDiasValidadeAcao" name="numeroDiasValidadeAcao" maxlength="3" size="4"  onkeypress="return formatarCampoInteiro(event)" value="${acaoCobrancaVO.numeroDiasValidadeAcao}">
		<select name="tipoNumeroDiasValidade" class="campoSelect" id="tipoNumeroDiasValidade" <ggas:campoSelecionado campo="opcaoTipoNumeroDiasValidadeAcao"/>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoNumeroDias}" var="opcaoTipoNumeroDiasValidadeAcao">
				<option value="<c:out value="${opcaoTipoNumeroDiasValidadeAcao.chavePrimaria}"/>" <c:if test="${acaoCobrancaVO.tipoNumeroDiasValidade eq opcaoTipoNumeroDiasValidadeAcao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${opcaoTipoNumeroDiasValidadeAcao.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		<br class="quebraLinha2" />
		
		<label class="rotulo rotulo2Linhas" for="numeroDiasVencimento">N�mero de dias para o vencimento:</label>
		<input class="campoTexto campo2Linhas campoHorizontal" type="text" id="numeroDiasVencimento" name="numeroDiasVencimento" maxlength="3" size="4"  onkeypress="return formatarCampoInteiro(event)" value="${acaoCobrancaVO.numeroDiasVencimento}">
		<select name="tipoNumeroDiasVencimento" class="campoSelect" id="tipoNumeroDiasVencimento" <ggas:campoSelecionado campo="opcaoTipoNumeroDiasVencimentoAcao"/>>
			<option value="-1">Selecione</option>
			<c:forEach items="${listaTipoNumeroDias}" var="opcaoTipoNumeroDiasVencimentoAcao">
				<option value="<c:out value="${opcaoTipoNumeroDiasVencimentoAcao.chavePrimaria}"/>" <c:if test="${acaoCobrancaVO.tipoNumeroDiasVencimento eq opcaoTipoNumeroDiasVencimentoAcao.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${opcaoTipoNumeroDiasVencimentoAcao.descricao}"/>
				</option>		
			</c:forEach>
		</select>
		
	</fieldset>
	<fieldset class="colunaFinal">
		<label class="rotulo rotulo2Linhas campoObrigatorio" for="tipoDocumentoGerado"><span class="campoObrigatorioSimbolo">* </span>Tipo do documento<br />a ser gerado:</label>
		<select name="tipoDocumentoGerado" id="tipoDocumentoGerado" class="campoSelect campo2Linhas">
	    	<option value="-1">Selecione</option>
	    	<c:forEach items="${listaTipoDocumento}" var="tipoDocumentoGerado">
				<option value="<c:out value="${tipoDocumentoGerado.chavePrimaria}"/>" <c:if test="${acaoCobrancaVO.tipoDocumentoGerado == tipoDocumentoGerado.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${tipoDocumentoGerado.descricao}"/>
				</option>		
			 </c:forEach>				
	    </select><br />
	   
	    <label class="rotulo" for="gerarOrdemServico">Gerar ordem de servi�o:</label>
		<input class="campoRadio" type="radio" name="gerarOrdemServico" id="gerarOrdemServicoSim" value="true"<c:if test="${acaoCobrancaVO.gerarOrdemServico == 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="gerarOrdemServicoSim" >Sim</label>
		<input class="campoRadio" type="radio" name="gerarOrdemServico" id="gerarOrdemServicoNao" value="false" <c:if test="${acaoCobrancaVO.gerarOrdemServico == 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="gerarOrdemServicoNao">N�o</label><br class="quebraRadio" />
		
		<label class="rotulo" for="consideraDebitosVencer">Considera d�bitos a vencer:</label>
		<input class="campoRadio" type="radio" name="consideraDebitosVencer" id="consideraDebitosVencerSim" value="true" <c:if test="${acaoCobrancaVO.consideraDebitosVencer == 'true'}">checked</c:if>>
		<label class="rotuloRadio" for="consideraDebitosVencerSim">Sim</label>
		<input class="campoRadio" type="radio" name="consideraDebitosVencer" id="consideraDebitosVencerNao" value="false" <c:if test="${acaoCobrancaVO.consideraDebitosVencer == 'false'}">checked</c:if>>
		<label class="rotuloRadio" for="consideraDebitosVencerNao">N�o</label><br class="quebraRadio" />
		
		<label class="rotulo rotulo2Linhas" for="bloquearEmissaoDocumentosPagaveis">Bloquear emiss�o de documentos pag�veis:</label>
		<input class="campoRadio campoRadio2Linhas bloquearEmissaoDocumentosPagaveisRadio" type="radio" name="bloquearEmissaoDocumentosPagaveis" id="bloquearEmissaoDocumentosPagaveisSim" value="true" <c:if test="${acaoCobrancaVO.bloquearEmissaoDocumentosPagaveis == 'true'}">checked</c:if>>
		<label class="rotuloRadio rotuloRadio2Linhas bloquearEmissaoDocumentosPagaveisSimLabel" for="bloquearEmissaoDocumentosPagaveisSim">Sim</label>
		<input class="campoRadio campoRadio2Linhas bloquearEmissaoDocumentosPagaveisRadio" type="radio" name="bloquearEmissaoDocumentosPagaveis" id="bloquearEmissaoDocumentosPagaveisNao" value="false" <c:if test="${acaoCobrancaVO.bloquearEmissaoDocumentosPagaveis == 'false'}">checked</c:if>>
		<label class="rotuloRadio rotuloRadio2Linhas bloquearEmissaoDocumentosPagaveisSimLabel" for="bloquearEmissaoDocumentosPagaveisNao">N�o</label>
	</fieldset>
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de A��o de Cobran�a.</p>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="alterarAcaoCobranca">
    	<input id="buttonSalvar" name="Button" class="bottonRightCol2 botaoGrande1" value="Salvar"  type="submit" >
    </vacess:vacess>
</fieldset>

</form> 