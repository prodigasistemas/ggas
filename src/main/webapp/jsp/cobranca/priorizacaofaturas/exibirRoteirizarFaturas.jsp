<!--
Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

Este programa  um software livre; voc pode redistribu-lo e/ou
modific-lo sob os termos de Licena Pblica Geral GNU, conforme
publicada pela Free Software Foundation; verso 2 da Licena.

O GGAS  distribudo na expectativa de ser til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
Consulte a Licena Pblica Geral GNU para obter mais detalhes.

Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
junto com este programa; se no, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type="text/javascript">

	$(document).ready(function() {
			     
		iniciarDatatable('#table-aviso-corte');
				
	});
	
	function pesquisar() {
		submeter('priorizacaoForm', 'roteirizarCorte');
	}
	
	function emitirAutorizacaoServicoLote(){
		submeter('priorizacaoForm', 'emitirAutorizacaoServicoLote');
	}
	

</script>

<form:form action="pesquisarPriorizacaoCorte" id="priorizacaoForm"
	name="priorizacaoForm" method="post"
	modelAttribute="AvisoCorteNotificacaoVO">
	<input name="habilitar" type="hidden" id="habilitar" value="">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Controle de Roteiriza��o de Corte</h5>
		</div>
		<div class="card-body">
			<div class="alert alert-primary fade show" role="alert">
				<i class="fa fa-question-circle"></i>Para emitir as Autoriza��es de
				Servi�o basta clicar no bot�o <b>Emitir Autoriza��es de Servi�o
					em Lote</b>. Caso queira selecionar a quantidade de Autoriza��es de
				Servi�o a serem emitidas escreva o n�mero e clique no bot�o <b>Simular
					Novamente</b>
			</div>
			<hr>

			<div class="row">
			<div class="form-group form-inline" >
				<label>Quantidade de pontos para gera��o das Autoriza��es de Servi�o: </label>
					<div class="input-group input-group-sm">
						<input type="text" aria-label="numeroPontos" id="numeroPontos"
							placeholder="" aria-controls="table-aviso-corte"
							class="form-control form-control-sm"
							onkeypress="return formatarCampoInteiro(event,6);"
							name="numeroPontos" maxlength="7"
							value="${avisoCorteNotificacaoVO.numeroPontos}">


					<div class="col align-self-end ">
						<button class="btn btn-primary btn-sm" id="botaoPesquisar"
							type="button" onclick="pesquisar();">
							<i class="fa fa-search"></i> Simular novamente
						</button>
					</div>
				</div>
				
			</div>

			<c:if test="${listaAvisoCorte ne null}">
				<hr class="linhaSeparadoraPesquisa" />
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover"
						id="table-aviso-corte" width="100%" style="">
						<thead class="thead-ggas-bootstrap">
							<tr>
								<th scope="col" class="text-center">Cliente</th>
								<th scope="col" class="text-center">CPF/CNPJ do Cliente</th>
								<th scope="col" class="text-center">Data do Vencimento</th>
								<th scope="col" class="text-center">Ponto de Consumo</th>
								<th scope="col" class="text-center">N�mero de tentativas de
									corte</th>
								<th scope="col" class="text-center">Dist�ncia entre pontos
									(km)</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaAvisoCorte}"
								var="avisoCorte">
								<tr>
									<td class="text-center"><c:out
											value="${avisoCorte.cliente.nome}" /></td>
									<td class="text-center"><c:choose>
											<c:when
												test="${avisoCorte.cliente.cpf eq null || avisoCorte.cliente.cpf eq ''}">
												<c:out value='${avisoCorte.cliente.cnpjFormatado}' />
											</c:when>
											<c:otherwise>
												<c:out value='${avisoCorte.cliente.cpfFormatado}' />
											</c:otherwise>
										</c:choose></td>

									<td class="text-center"><c:if
											test="${avisoCorte.fatura.dataVencimento ne ''}">
											<fmt:formatDate value="${avisoCorte.fatura.dataVencimento}"
												pattern="dd/MM/yyyy" />
										</c:if></td>
									<td class="text-center"><c:out
											value="${avisoCorte.pontoConsumo.descricao}" /></td>
									<td class="text-center"><c:out
											value="${avisoCorte.numeroChecagens}" /></td>
									<td class="text-center"><fmt:formatNumber type="number"
											maxFractionDigits="2"
											value="${avisoCorte.distanciaEntrePontos}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</div>
				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<button class="btn btn-primary btn-sm" id="botaoEmitirAS"
							type="button" onclick="emitirAutorizacaoServicoLote();">Emitir
							Autoriza��es de Servi�o em Lote</button>
					</div>
				</div>
		</div>
		</c:if>


	</div>
	</div>
</form:form>