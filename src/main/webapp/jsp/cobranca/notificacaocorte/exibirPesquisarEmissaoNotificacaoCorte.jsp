<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Controle de Notifica��o de Corte<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para filtrar um registro espec�fico, informe os dados de cliente ou im�vel e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, 
ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir os pontos de consumo habilitados para notifica��o de corte.
Selecione os registros que ser�o desconsiderados na notifica��o de corte, e clique em <span class="destaqueOrientacaoInicial">Desabilitar</span>.</p>

<script type="text/javascript">
	$(document).ready(function() {
		if ($("#faturaTexto").val() == 0){
			$("#faturaTexto").val("");
		}
		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;
		
		if(idCliente != ''){
			selecionarCliente(idCliente);
		} {
			selecionarImovel(idImovel);
		}
		
	});
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function limpar(form) {
		limparFormularios(form);
		limparFormularioDadosCliente();
		
		document.getElementById("idCliente").value = "";
		document.getElementById("nomeCompletoCliente").value = "";
		document.getElementById("documentoFormatado").value = "";
		document.getElementById("enderecoFormatadoCliente").value = "";
		document.getElementById("emailCliente").value = "";
		
		document.getElementById("idImovel").value = "";
		document.getElementById("nomeFantasiaImovel").value = "";
		document.getElementById("matriculaImovel").value = "";
		document.getElementById("numeroImovel").value = "";
		document.getElementById("cidadeImovel").value = "";
		document.getElementById("enderecoImovel").value = "";
		
		form.habilitado[2].checked = true;
	}
	
	function pesquisar() {
		submeter('avisoCorteEmissaoForm', 'pesquisarEmissaoNotificacaoCorte');
	}
	
	function habilitarNotificacao() {
		document.getElementById("habilitar").value = "true";
		submeter('avisoCorteEmissaoForm', 'habilitarEmissaoNotificacaoCorte');
	}
	
	function desabilitarNotificacao() {
		document.getElementById("habilitar").value = "false";
		submeter('avisoCorteEmissaoForm', 'habilitarEmissaoNotificacaoCorte');
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}

</script>

<form:form method="post" action="exibirPesquisarEmissaoNotificacaoCorte" name="avisoCorteEmissaoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarEmissaoNotificacaoCorte">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idImovel" type="hidden" id="idImovel" value="${avisoCorteNotificacaoVO.idImovel}">
	<input name="idCliente" type="hidden" id="idCliente" value="${avisoCorteNotificacaoVO.idCliente}">
	<input name="habilitar" type="hidden" id="habilitar" value="">
		
	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${avisoCorteEmissaoForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${avisoCorteEmissaoForm.map.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${avisoCorteEmissaoForm.map.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${avisoCorteEmissaoForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${avisoCorteEmissaoForm.map.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${avisoCorteEmissaoForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${avisoCorteEmissaoForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${avisoCorteEmissaoForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${avisoCorteEmissaoForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${avisoCorteEmissaoForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${avisoCorteEmissaoForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${avisoCorteEmissaoForm.map.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Nome Fantasia:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${avisoCorteEmissaoForm.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.matriculaImovel}"><br />	
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${avisoCorteEmissaoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${avisoCorteEmissaoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco" id="conteinerPesquisarFaturaInferior">
			
			<br/>
			<label class="rotulo" id="rotuloFatura" for="faturaTexto">No. Fatura:</label>
			<input class="campoDesabilitado" onkeypress="return apenasNumeros()" type="text" id="faturaTexto" name="idFatura"  maxlength="18" size="18" value="${avisoCorteNotificacaoVO.idFatura}"><br />		
			
			<label class="rotulo" id="rotuloFatura" for="faturaTexto">Vencidos a partir de (dias):</label>
			<input class="campoDesabilitado" onkeypress="return apenasNumeros()" type="text" id="diasAtraso" name="diasAtraso"  maxlength="4" size="18" value="${avisoCorteNotificacaoVO.diasAtraso}"><br />
			
			<label class="rotulo" for="habilitado">Habilitado para Notifica��o de Corte: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${avisoCorteNotificacaoVO.habilitado eq true}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Sim</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${avisoCorteNotificacaoVO.habilitado eq false}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">N�o</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty avisoCorteNotificacaoVO.habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br/><br/>
			
			<label class="rotulo" for="idRubrica">Grupo:</label> 
			<select	name="idGrupoFaturamento" id="idGrupoFaturamento" class="campoSelect">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaGruposFaturamento}" var="grupo">
					<option value="<c:out value="${grupo.chavePrimaria}"/>"
						<c:if test="${avisoCorteNotificacaoVO.idGrupoFaturamento == grupo.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${grupo.descricao}" />
					</option>
				</c:forEach>
			</select><br class="quebraLinha" />

		</fieldset>
	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="button" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar();">
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limpar(this.form);">
		</fieldset>
		
		<c:if test="${listaFatura ne null}">
		
			<hr class="linhaSeparadora1" />
		
			<fieldset class="conteinerBloco">

				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="fatura"
					decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" 
					requestURI="pesquisarEmissaoNotificacaoCorte"> 
			        
			        <display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
		      			<input type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${fatura.chavePrimaria}">
					</display:column>
							
					<display:column title="Habilitado" style="width: 80px">
						<c:choose>
							<c:when test="${fatura.habilitadoCobranca == true}">
								<img alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
					</display:column>

					<display:column style="text-align: center;" title="Cliente" sortable="true" sortProperty="cliente.nome">
		            	<c:out value="${fatura.cliente.nome}"/>
		 			</display:column>

					<display:column style="text-align: center;" title="CPF/CNPJ do Cliente">
		        	   	<c:choose>
				    		<c:when test="${fatura.cliente.cpf eq null || fatura.cliente.cpf eq ''}">
						    	<c:out value='${fatura.cliente.cnpjFormatado}'/>
				    		</c:when>
				    		<c:otherwise>
				    			<c:out value='${fatura.cliente.cpfFormatado}'/>
				    		</c:otherwise>
				    	</c:choose>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Fatura" sortable="true" sortProperty="chavePrimaria">
		            	<c:out value="${fatura.chavePrimaria}"/>
		 			</display:column>

		 			<display:column style="text-align: center;" title="Data do Vencimento" sortable="true" sortProperty="dataVencimento">
		        		<c:if test="${fatura.dataVencimento ne ''}">
		        			<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
		        		</c:if>
		 			</display:column>
		 			
		 			<display:column style="text-align: center;" title="Dias em Atraso" sortable="true" sortProperty="diasAtraso">
		            	<c:out value="${fatura.diasAtraso}"/>
		 			</display:column>

					<display:column style="text-align: center;" title="Valor (R$)" sortable="true" sortProperty="valorTotal">
						<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2"/>
		 			</display:column>
		 			
					<display:column style="text-align: center;" title="Ponto de Consumo" sortable="true" sortProperty="pontoConsumo.codigoLegado">
		            	<c:out value="${fatura.pontoConsumo.descricao}"/>
		 			</display:column>		 			

			    </display:table>
			    
			</fieldset>
			
		</c:if>
	
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${listaFatura ne null}">
			<vacess:vacess param="habilitarEmissaoNotificacaoCorte">
				<input name="buttonAlterar" value="Desabilitar" class="bottonRightCol2" id="botaoAlterar" onclick="desabilitarNotificacao();" type="button">
			</vacess:vacess>
			<vacess:vacess param="habilitarEmissaoNotificacaoCorte">
				<input name="buttonIncluir" value="Habilitar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="habilitarNotificacao();" type="button">
			</vacess:vacess>
		</c:if>
	</fieldset>
	
</form:form>
