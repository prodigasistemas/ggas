<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Emiss�o da 2 Via de Aviso ou Notifica��o de Corte<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para filtrar um registro espec�fico, informe os dados de cliente ou im�vel e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, 
ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir os pontos de consumo habilitados para notifica��o de corte.
Selecione os registros que ser�o desconsiderados na notifica��o de corte, e clique em <span class="destaqueOrientacaoInicial">Desabilitar</span>.</p>

<script type="text/javascript">
	$(document).ready(function() {
		if ($("#faturaTexto").val() == 0){
			$("#faturaTexto").val("");
		}
		
		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;
		
		if(idCliente != ''){
			selecionarCliente(idCliente);
		} {
			selecionarImovel(idImovel);
		}
	});
	
	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function limpar(form) {
		limparFormularios(form);
		limparFormularioDadosCliente();
		
		document.getElementById("idCliente").value = "";
		document.getElementById("nomeCompletoCliente").value = "";
		document.getElementById("documentoFormatado").value = "";
		document.getElementById("enderecoFormatadoCliente").value = "";
		document.getElementById("emailCliente").value = "";
		
		document.getElementById("idImovel").value = "";
		document.getElementById("nomeFantasiaImovel").value = "";
		document.getElementById("matriculaImovel").value = "";
		document.getElementById("numeroImovel").value = "";
		document.getElementById("cidadeImovel").value = "";
		document.getElementById("enderecoImovel").value = "";
		
		form.habilitado[2].checked = true;
	}
	
	function pesquisar() {
		submeter('avisoCorteEmissaoForm', 'pesquisarEmissaoNotificacaoAvisoCorte');
	}
	
	function emitir2viaAvisoCorte(idAvisoCorte) {
		document.getElementById('chavePrimaria').value = idAvisoCorte;
		submeter("avisoCorteEmissaoForm", "emitir2viaAvisoCorte");
	}
	
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}

</script>

<form:form method="post" action="exibirPesquisarEmissaoNotificacaoCorte" name="avisoCorteEmissaoForm">
	<input name="acao" type="hidden" id="acao" value="pesquisarEmissaoNotificacaoCorte">
	<input name="fluxoPesquisa" type="hidden" id="fluxoPesquisa" value="true">
	<input name="idImovel" type="hidden" id="idImovel" value="${avisoCorteNotificacaoVO.idImovel}">
	<input name="idCliente" type="hidden" id="idCliente" value="${avisoCorteNotificacaoVO.idCliente}">
	<input name="habilitar" type="hidden" id="habilitar" value="">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
		
	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${avisoCorteEmissaoForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${avisoCorteEmissaoForm.map.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${avisoCorteEmissaoForm.map.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${avisoCorteEmissaoForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${avisoCorteEmissaoForm.map.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${avisoCorteEmissaoForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${avisoCorteEmissaoForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${avisoCorteEmissaoForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${avisoCorteEmissaoForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${avisoCorteEmissaoForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${avisoCorteEmissaoForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${avisoCorteEmissaoForm.map.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Nome Fantasia:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${avisoCorteEmissaoForm.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.matriculaImovel}"><br />	
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${avisoCorteEmissaoForm.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${avisoCorteEmissaoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${avisoCorteEmissaoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco" id="conteinerPesquisarFaturaInferior">
			
			<label class="rotulo" for="habilitado">Tipo: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="tipo" id="tipo" value="notificacaoCorte" checked="checked"<c:if test="${avisoCorteNotificacaoVO.tipo eq 'notificacaoCorte'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Notifica��o de Corte</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="tipo" id="tipo" value="avisoCorte" <c:if test="${avisoCorteNotificacaoVO.tipo eq 'avisoCorte'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Aviso de Corte</label>
		</fieldset>
	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="button" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar();">
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limpar(this.form);">
		</fieldset>
		
		<c:if test="${listaAvisoCorte ne null}">
			
				<legend class="conteinerBlocoTitulo">Notifica��o/Aviso de Corte</legend>
	
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaAvisoCorte" sort="list" id="documento"
				 decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEmissaoNotificacaoAvisoCorte"> 
			        
				<display:column style="text-align: center;" title="Data da Notifica��o">
					<fmt:formatDate value="${documento.dataNotificacao}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		
					<display:column style="text-align: center;" title="Valor Total em Atraso">
						<fmt:formatNumber value="${documento.fatura.valorTotal}" minFractionDigits="2" type="currency"/>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Data do Vencimento Fatura">
		            	<fmt:formatDate value="${documento.fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		 			
		 			<display:column style="text-align: center;" title="Dias em Atraso">
		 					<c:out value="${documento.diasAtraso}"/>
		 			</display:column>	
		 			
		 			<display:column style="width: 70px" title="Emitir 2 Via" >
		        	<a href='javascript:emitir2viaAvisoCorte(<c:out value='${documento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Emitir 2 Via" title="Emitir 2 Via" border="0">
			    	</a>
		        </display:column>
		 			
			    </display:table>
			    
		    </c:if>
	
	</fieldset>
	
<!-- 	<fieldset class="conteinerBotoes"> -->
<%-- 		<c:if test="${listaFatura ne null}"> --%>
<%-- 			<vacess:vacess param="habilitarEmissaoNotificacaoCorte.do"> --%>
<!-- 				<input name="buttonAlterar" value="Desabilitar" class="bottonRightCol2" id="botaoAlterar" onclick="desabilitarNotificacao();" type="button"> -->
<%-- 			</vacess:vacess> --%>
<%-- 			<vacess:vacess param="habilitarEmissaoNotificacaoCorte.do"> --%>
<!-- 				<input name="buttonIncluir" value="Habilitar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="habilitarNotificacao();" type="button"> -->
<%-- 			</vacess:vacess> --%>
<%-- 		</c:if> --%>
<!-- 	</fieldset> -->
	
</form:form>
