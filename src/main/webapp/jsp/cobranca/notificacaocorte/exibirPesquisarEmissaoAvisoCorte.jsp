<!--
Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

Este programa � um software livre; voc� pode redistribu�-lo e/ou
modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
publicada pela Free Software Foundation; vers�o 2 da Licen�a.

O GGAS � distribu�do na expectativa de ser �til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
junto com este programa; se n�o, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		iniciarDatatable('#table-aviso-corte', {
			"order" : [ [ 2, 'asc' ] ]
		});

		restaurarEstadoFiltros();

		var idCliente = document.getElementById("idCliente").value;
		var idImovel = document.getElementById("idImovel").value;

		if (idCliente != '') {
			selecionarCliente(idCliente);
		}
		{
			selecionarImovel(idImovel);
		}

		carregarComando();

	});

	function salvarEstadosFiltros() {
		try {
			sessionStorage.setItem('aba-chamado', $('#aba-chamado').attr(
					'class'));
			sessionStorage.setItem('caret-aba-chamado', $('#caret-aba-chamado')
					.attr('class'));

			sessionStorage.setItem('aba-pessoa-fisica', $('#aba-pessoa-fisica')
					.attr('class'));
			sessionStorage.setItem('caret-aba-pessoa-fisica', $(
					'#caret-aba-pessoa-fisica').attr('class'));

			sessionStorage.setItem('aba-pessoa-juridica', $(
					'#aba-pessoa-juridica').attr('class'));
			sessionStorage.setItem('caret-aba-pessoa-juridica', $(
					'#caret-aba-pessoa-juridica').attr('class'));

			sessionStorage.setItem('aba-localizacao-ponto-consumo', $(
					'#aba-localizacao-ponto-consumo').attr('class'));
			sessionStorage.setItem('caret-aba-localizacao-ponto-consumo', $(
					'#caret-aba-localizacao-ponto-consumo').attr('class'));

			sessionStorage
					.setItem('aba-imovel', $('#aba-imovel').attr('class'));
			sessionStorage.setItem('caret-aba-imovel', $('#caret-aba-imovel')
					.attr('class'));

			sessionStorage.setItem('aba-contrato', $('#aba-contrato').attr(
					'class'));
			sessionStorage.setItem('caret-aba-contrato', $(
					'#caret-aba-contrato').attr('class'));
		} catch (e) {
			console.error('browser nao suporte session storage');
		}
	}

	function restaurarEstadoFiltros() {
		try {

			if (sessionStorage.getItem('aba-pessoa-fisica') !== null) {
				$('#aba-pessoa-fisica').attr('class',
						sessionStorage.getItem('aba-pessoa-fisica'));
				$('#caret-aba-pessoa-fisica').attr('class',
						sessionStorage.getItem('caret-aba-pessoa-fisica'));
			}

			if (sessionStorage.getItem('aba-imovel') !== null) {
				$('#aba-imovel').attr('class',
						sessionStorage.getItem('aba-imovel'));
				$('#caret-aba-imovel').attr('class',
						sessionStorage.getItem('caret-aba-imovel'));
			}

		} catch (e) {
			console.error('browser nao suporte session storage');
		}
	}
	function toggleClass(id) {
		$(id).toggleClass('fa-caret-up');
		$(id).toggleClass('fa-caret-down');
	}

	function pesquisar() {
		salvarEstadosFiltros();
		submeter('avisoCorteForm', 'pesquisarEmissaoAvisoCorte');
	}

	function exibirPopupPesquisaImovel() {
		popup = window
				.open(
						'exibirPesquisaImovelCompletoPopup?postBack=true',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado) {
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		if (idSelecionado != '') {

			AjaxService
					.obterImovelPorChave(
							idSelecionado,
							{
								callback : function(imovel) {
									if (imovel != null) {

										idImovel.value = imovel["chavePrimaria"];
										matriculaImovel.value = imovel["chavePrimaria"];
										nomeFantasia.value = imovel["nomeFantasia"];
										numeroImovel.value = imovel["numeroImovel"];
										cidadeImovel.value = imovel["cidadeImovel"];
										indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
										enderecoImovel.value = imovel["enderecoImovel"];
									}
								},
								async : false
							}

					);
		} else {
			idImovel.value = "";
			matriculaImovel.value = "";
			nomeFantasia.value = "";
			numeroImovel.value = "";
			cidadeImovel.value = "";
			indicadorCondominio.value = "";
			enderecoImovel.value = "";
		}
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if (indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}

	function exibirPopupPesquisaCliente() {
		popup = window
				.open(
						'exibirPesquisaClientePopup',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
	}

	function selecionarCliente(idSelecionado) {
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document
				.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document
				.getElementById("enderecoFormatadoCliente");

		if (idSelecionado != '') {
			AjaxService.obterClientePorChave(idSelecionado, {
				callback : function(cliente) {
					if (cliente != null) {
						idCliente.value = cliente["chavePrimaria"];
						nomeCompletoCliente.value = cliente["nome"];
						if (cliente["cnpj"] != undefined) {
							documentoFormatado.value = cliente["cnpj"];
						} else {
							if (cliente["cpf"] != undefined) {
								documentoFormatado.value = cliente["cpf"];
							} else {
								documentoFormatado.value = "";
							}
						}
						emailCliente.value = cliente["email"];
						enderecoFormatado.value = cliente["enderecoFormatado"];
					}
				},
				async : false
			});
		} else {
			idCliente.value = "";
			nomeCompletoCliente.value = "";
			documentoFormatado.value = "";
			emailCliente.value = "";
			enderecoFormatado.value = "";

		}

		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}

	function limparFormulario() {
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";

		document.getElementById("idImovel").value = "";
		document.getElementById("nomeFantasiaImovel").value = "";
		document.getElementById("matriculaImovel").value = "";
		document.getElementById("numeroImovel").value = "";
		document.getElementById("cidadeImovel").value = "";
		document.getElementById("enderecoImovel").value = "";

		document.getElementById("nomeImovelTexto").value = "";
		document.getElementById("matriculaImovelTexto").value = "";
		document.getElementById("numeroImovelTexto").value = "";
		document.getElementById("cidadeImovelTexto").value = "";
		document.getElementById("indicadorCondominioImovelTexto1").checked = false;
		document.getElementById("indicadorCondominioImovelTexto2").checked = false;

		document.getElementById("numeroFatura").value = "";
		document.getElementById("diasAtraso").value = "";

		document.getElementById("habilitadoTodos").checked = true;

	}

function habilitarNotificacao() {
		document.getElementById("habilitar").value = "true";
		submeter('avisoCorteForm', 'habilitarOuDesabilitarAvisoCorteFatura');
	}
	
	function desabilitarNotificacao() {
		document.getElementById("habilitar").value = "false";
		submeter('avisoCorteForm', 'habilitarOuDesabilitarAvisoCorteFatura');
	}

	function carregarComando() {
		$("#comboComandos").load("carregarComandoAvisoCorte");
	}
</script>

<%@ page
	import="br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario"%>

<div class="bootstrap">
	
	<form:form action="pesquisarEmissaoAvisoCorte" id="avisoCorteForm"
		name="avisoCorteForm" method="post"
		modelAttribute="AvisoCorteNotificacaoVO">
		<input name="habilitar" type="hidden" id="habilitar" value="">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Controle de Aviso de Corte</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para pesquisar um registro
					espec�fico, informe os dados nos campos abaixo e clique em <b>Pesquisar</b>,
					ou clique apenas em <b>Pesquisar</b> para exibir todos os pontos de
					consumo habilitados para aviso de corte. Selecione os registros que
					ser�o desconsiderados no aviso de corte, e clique em <b>Desabilitar</b>.
				</div>
				<!--  INICIOOOO -->
				<div class="card">
					<div class="card-body bg-light">
						<div class="row mb-2">
							<div class="col-md-6">

								<div class="form-row mt-3">
									<div class="col-md-12">
										<h5>Filtro Cliente</h5>
									</div>
									<div class="col-md-12">
										<div class="accordion">
											<div class="card">
												<div class="card-header p-0">
													<h5 class="mb-0">
														<button class="btn btn-link btn-sm" type="button"
															onclick="$('#aba-pessoa-fisica').toggleClass('show'); toggleClass('#caret-aba-pessoa-fisica')"
															aria-expanded="true" aria-controls="collapseOne">
															Pessoa<i id="caret-aba-pessoa-fisica"
																class="fa fa-caret-down"></i>
														</button>
													</h5>
													<input name="idCliente" type="hidden" id="idCliente"
														value="${avisoCorteNotificacaoVO.idCliente}"> <input
														name="nomeCompletoCliente" type="hidden"
														id="nomeCompletoCliente"
														value="${avisoCorteForm.map.nomeCompletoCliente}">
													<input name="documentoFormatado" type="hidden"
														id="documentoFormatado"
														value="${avisoCorteForm.map.documentoFormatadoTexto}">
													<input name="enderecoFormatadoCliente" type="hidden"
														id="enderecoFormatadoCliente"
														value="${avisoCorteForm.map.enderecoFormatadoTexto}">
													<input name="emailCliente" type="hidden" id="emailCliente"
														value="${avisoCorteForm.map.emailClienteTexto}">
												</div>

												<div id="aba-pessoa-fisica" class="collapse"
													aria-labelledby="headingOne"
													data-parent="#aba-pessoa-fisica">
													</br>
													<div class="row justify-content-md-center">
														<h6>
															<i class="fa fa-question-circle"></i> Clique em <b>Pesquisar
																Pessoa</b> para selecionar a Pessoa.
														</h6>
													</div>
													<div class="card-body"
														style="border-bottom: 1px solid #dfdfdf;">
														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="nomeClienteTextoI" for="nomeClienteTexto">Pessoa:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="nomeClienteTexto" name="nomeClienteTexto"
																	maxlength="50" size="50" disabled="disabled"
																	value="${avisoCorteForm.map.nomeCompletoCliente}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="documentoFormatadoTextoI"
																for="documentoFormatadoTexto">CPF/CNPJ:</label>
															<div class="col-sm-6">
																<input class="form-control form-control-sm" type="text"
																	id="documentoFormatadoTexto"
																	name="documentoFormatadoTexto" maxlength="18" size="18"
																	disabled="disabled"
																	value="${avisoCorteForm.map.documentoFormatadoTexto}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="enderecoFormatadoTextoI"
																for="enderecoFormatadoTexto">Endere�o:</label>
															<div class="col-sm-8">
																<textarea class="form-control form-control-sm"
																	id="enderecoFormatadoTexto"
																	name="enderecoFormatadoTexto" rows="3" cols="37"
																	disabled="disabled">${avisoCorteForm.map.enderecoFormatadoTexto}</textarea>
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="rotuloPassaporte" for="emailClienteTexto">E-mail:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="emailClienteTexto" name="emailClienteTexto"
																	maxlength="80" size="40" disabled="disabled"
																	value="${avisoCorteForm.map.emailClienteTexto}">
															</div>
														</div>

														<div class="row justify-content-md-center">
															<input name="Button" id="botaoPesquisarImovel"
																class="btn btn-primary btn-sm" title="Pesquisar Imovel"
																value="Pesquisar Pessoa"
																onclick="exibirPopupPesquisaCliente();" type="button">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="idFatura">N�mero da Fatura:</label>
										<div class="input-group input-group-sm">

											<input type="text" aria-label="idFatura" id="numeroFatura"
												class="form-control form-control-sm"
												onkeypress="return formatarCampoInteiro(event,6);"
												name="idFatura" maxlength="7"
												value="${avisoCorteNotificacaoVO.idFatura}">

										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="diasAtraso">Vencidos a partir de (dias):</label>
										<div class="input-group input-group-sm">

											<input type="text" aria-label="diasAtraso" id="diasAtraso"
												class="form-control form-control-sm"
												onkeypress="return formatarCampoInteiro(event,6);"
												name="diasAtraso" maxlength="7"
												value="${avisoCorteNotificacaoVO.diasAtraso}">

										</div>
									</div>
								</div>

								<div class="form-row">
									<label for="habilitado" class="col-md-12">Habilitado
										para Aviso de Corte:</label>
									<div class="col-md-12">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoAtivo" name="habilitado"
												class="custom-control-input" value="true"
												<c:if test="${avisoCorteNotificacaoVO.habilitado eq 'true'}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoAtivo">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoInativo" name="habilitado"
												class="custom-control-input" value="false"
												<c:if test="${avisoCorteNotificacaoVO.habilitado eq 'false'}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoInativo">N�o</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoTodos" name="habilitado"
												class="custom-control-input" value=""
												<c:if test="${avisoCorteNotificacaoVO.habilitado eq null}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoTodos">Todos</label>
										</div>
									</div>
								</div>

							</div>


							<div class="col-md-6">

								<div class="form-row mt-3">
									<div class="col-md-12">
										<h5>Filtro Im�vel</h5>
									</div>
									<div class="col-md-12">
										<div class="accordion">
											<div class="card">
												<div class="card-header p-0">
													<h5 class="mb-0">
														<button class="btn btn-link btn-sm" type="button"
															onclick="$('#aba-imovel').toggleClass('show'); toggleClass('#caret-aba-imovel')"
															aria-expanded="true" aria-controls="collapseOne">
															Im�vel <i id="caret-aba-imovel" class="fa fa-caret-down"></i>
														</button>
													</h5>
												</div>

												<input name="idImovel" type="hidden" id="idImovel"
													value="${avisoCorteNotificacaoVO.idImovel}"> <input
													name="nomeFantasiaImovel" type="hidden"
													id="nomeFantasiaImovel"
													value="${avisoCorteForm.map.nomeFantasiaImovel}"> <input
													name="matriculaImovel" type="hidden" id="matriculaImovel"
													value="${avisoCorteForm.map.matriculaImovel}"> <input
													name="numeroImovel" type="hidden" id="numeroImovel"
													value="${avisoCorteForm.map.numeroImovel}"> <input
													name="cidadeImovel" type="hidden" id="cidadeImovel"
													value="${avisoCorteForm.map.cidadeImovel}"> <input
													name="condominio" type="hidden" id="condominio"
													value="${avisoCorteForm.map.condominio}"> <input
													name="enderecoImovel" type="hidden" id="enderecoImovel"
													value="${avisoCorteForm.map.enderecoImovel}">

												<div id="aba-imovel" class="collapse"
													aria-labelledby="headingOne" data-parent="#aba-imovel">
													</br>
													<div class="row justify-content-md-center">
														<h6>
															<i class="fa fa-question-circle"></i> Clique em <b>Pesquisar
																Im�vel</b> para selecionar o Im�vel.
														</h6>
													</div>
													<div class="card-body"
														style="border-bottom: 1px solid #dfdfdf;">
														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="rotuloNumeroImovel" for="numeroImovel">Nome
																Fantasia:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="nomeImovelTexto" name="nomeImovelTexto"
																	maxlength="50" size="37" disabled="disabled"
																	value="${avisoCorteForm.map.nomeFantasiaImovel}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																for="descricaoComplemento">Matricula:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="matriculaImovelTexto" name="matriculaImovelTexto"
																	maxlength="18" size="18" disabled="disabled"
																	value="${avisoCorteForm.map.matriculaImovel}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																id="rotuloComplemento" for="nomeImovel">N�mero:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="numeroImovelTexto" name="numeroImovelTexto"
																	maxlength="18" size="18" disabled="disabled"
																	value="${avisoCorteForm.map.numeroImovel}">
															</div>
														</div>

														<div class="form-row mb-1">
															<label class="col-sm-4 text-md-right"
																for="matriculaImovel">Cidade:</label>
															<div class="col-sm-8">
																<input class="form-control form-control-sm" type="text"
																	id="cidadeImovelTexto" name="cidadeImovelTexto"
																	maxlength="18" size="18" disabled="disabled"
																	value="${avisoCorteForm.map.cidadeImovel}">
															</div>
														</div>

														<div class="form-row">
															<div class="col-md-12">
																<label class="col-md-4 pr-2 text-md-right">Imovel
																	� Condom�nio:</label>
																<div
																	class="custom-control custom-radio custom-control-inline ml-1 mt-1">
																	<input class="custom-control-input" type="radio"
																		id="indicadorCondominioImovelTexto1"
																		name="indicadorCondominioImovelTexto" value="true"
																		disabled="disabled"
																		<c:if test="${avisoCorteEmissaoForm.map.condominio == 'true'}">checked="checked"</c:if>><label
																		class="custom-control-label"
																		for="indicadorImovelTexto1">Sim</label>
																</div>

																<div
																	class="custom-control custom-radio custom-control-inline ml-1">
																	<input class="custom-control-input" type="radio"
																		id="indicadorCondominioImovelTexto2"
																		name="indicadorCondominioImovelTexto" value="false"
																		disabled="disabled"
																		<c:if test="${avisoCorteEmissaoForm.map.condominio == 'false'}">checked="checked"</c:if>><label
																		class="custom-control-label"
																		for="indicadorImovelTexto2">N�o</label>
																</div>
															</div>
														</div>
														<br />
														<div class="row justify-content-md-center">
															<input name="Button" id="botaoPesquisarImovel"
																class="btn btn-primary btn-sm" title="Pesquisar Imovel"
																value="Pesquisar Imovel"
																onclick="exibirPopupPesquisaImovel();" type="button">
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>



								<div class="form-row">
									<div class="col-md-12">
										<label>Comando de
											A��o:</label>
										<div id="comboComandos">
											<jsp:include page="/jsp/batch/acaocomando/comboComandos.jsp"></jsp:include>
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="idGrupoFaturamento">Grupo de Faturamento:</label>
										<select name="idGrupoFaturamento" id="idGrupoFaturamento"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaGruposFaturamento}"
												var="grupoFaturamento">
												<option
													value="<c:out value="${grupoFaturamento.chavePrimaria}"/>"
													<c:if test="${avisoCorteNotificacaoVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${grupoFaturamento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

							</div>
																																		
						</div>
					</div>
				</div>
				<!-- FIMMMM -->
				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<button class="btn btn-primary btn-sm" id="botaoPesquisar"
							type="button" onclick="pesquisar();">
							<i class="fa fa-search"></i> Pesquisar
						</button>
						<button class="btn btn-secondary btn-sm" id="botaoLimpar"
							type="button" onclick="limparFormulario();">
							<i class="fa fa-times"></i> Limpar
						</button>
					</div>
				</div>


				<hr />


				<c:if test="${listaFatura ne null}">


					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="table-aviso-corte" width="100%" style="">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th scope="col" class="text-center">Habilitado</th>
									<th scope="col" class="text-center">Cliente</th>
									<th scope="col" class="text-center">CPF/CNPJ do Cliente</th>
									<th scope="col" class="text-center">Fatura</th>
									<th scope="col" class="text-center">Data do Vencimento</th>
									<th scope="col" class="text-center">Dias em Atraso</th>
									<th scope="col" class="text-center">Valor (R$)</th>
									<th scope="col" class="text-center">Ponto de Consumo</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaFatura}" var="fatura">
									<tr>
										<td>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${fatura.chavePrimaria}">
												<input id="chk${fatura.chavePrimaria}" type="checkbox"
													name="chavesPrimarias" class="custom-control-input"
													value="${fatura.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${fatura.chavePrimaria}"></label>
											</div>
										</td>
										<td class="text-center"><c:choose>
												<c:when test="${fatura.indicadorExibirAvisoCorte == true}">
													<img alt="Ativo" title="Ativo"
														src="<c:url value="/imagens/success_icon.png"/>"
														border="0">
												</c:when>
												<c:otherwise>
													<img alt="Inativo" title="Inativo"
														src="<c:url value="/imagens/cancel16.png"/>" border="0">
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><c:out
												value="${fatura.cliente.nome}" /></td>
										<td class="text-center"><c:choose>
												<c:when
													test="${fatura.cliente.cpf eq null || fatura.cliente.cpf eq ''}">
													<c:out value='${fatura.cliente.cnpjFormatado}' />
												</c:when>
												<c:otherwise>
													<c:out value='${fatura.cliente.cpfFormatado}' />
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><c:out
												value="${fatura.chavePrimaria}" /></td>

										<td class="text-center"><c:if
												test="${fatura.dataVencimento ne ''}">
												<fmt:formatDate value="${fatura.dataVencimento}"
													pattern="dd/MM/yyyy" />
											</c:if></td>
										<td class="text-center"><c:out
												value="${fatura.diasAtraso}" /></td>
										<td class="text-center"><fmt:formatNumber
												value="${fatura.valorTotal}" minFractionDigits="2" /></td>
										<td class="text-center"><c:out
												value="${fatura.pontoConsumo.descricao}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>

					<div class="row justify-content-end">
						<div class="col text-right mt-2">
							<vacess:vacess param="exibirInclusaoChamado">
								<button name="buttonIncluirChamadosEmLote"
									id="buttonIncluirChamadosEmLote" type="button"
									class="btn btn-primary btn-sm ml-1"
									onclick="desabilitarNotificacao();">Desabilitar</button>

								<button name="buttonIncluir" id="buttonIncluir" type="button"
									class="btn btn-primary btn-sm ml-1"
									onclick="habilitarNotificacao();">Habilitar</button>
							</vacess:vacess>
						</div>
					</div>

				</c:if>




			</div>
		</div>
	</form:form>
</div>


