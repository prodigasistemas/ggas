<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		iniciarDatatable('#agenteNegativador');

		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.png"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calendrio',
			dateFormat : 'dd/mm/yyyy'
		});

	});

	function incluir() {
		submeter("agenteNegativadorForm", "exibirIncluirAgenteNegativador");
	}

	function pesquisar() {
		submeter("agenteNegativadorForm", "pesquisarAgenteNegativador");
	}

	function alterarAgenteNegativador() {
		var selecao = verificarSelecaoApenasUm();
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		if (selecao == true) {
			submeter("agenteNegativadorForm", "exibirAlterarAgenteNegativador");
		}
	}

	function removerAgenteNegativador() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				submeter('agenteNegativadorForm', 'removerAgenteNegativador');
			}
		}
	}
	
	function limparFormulario(){
		limparFormularios(document.agenteNegativadorForm);
		document.getElementById("habilitadoTodos").checked = true;
	}
</script>

<div class="bootstrap">
	<form:form method="post" action="pesquisarAgenteNegativador"
		id="agenteNegativadorForm" name="agenteNegativadorForm"
		modelAttribute="AgenteNegativadorVO">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" />
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Agente Negativador</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para pesquisar um registro
					espec�fico, informe os dados nos campos abaixo e clique em <strong>Pesquisar</strong>,
					ou clique apenas em <strong>Pesquisar</strong> para exibir todos.
					Para incluir um novo registro clique em <strong>Incluir</strong>
				</div>

				<div class="card-body bg-light">
					<h5>Filtros Agente Negativador</h5>
					<div class="row mb-2">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12">
									<label>Nome:</label> <input
										class="form-control form-control-sm" type="text"
										id="nomeCliente" name="nomeCliente" maxlength="30" size="32"
										value="${agenteNegativadorVO.nomeCliente}">
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12">
									<label>N�mero Contrato:</label> <input
										class="form-control form-control-sm" type="text"
										id="numeroContrato" name="numeroContrato" maxlength="30"
										size="32" value="${agenteNegativadorVO.numeroContrato}"><br />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12">
									<label for="habilitado" class="col-md-15">Priorit�rio:</label>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="habilitadoAtivo"
											name="indicadorPrioritario" class="custom-control-input"
											value="true"
											<c:if test="${agenteNegativadorVO.indicadorPrioritario eq 'true'}">checked="checked"</c:if> />
										<label class="custom-control-label" for="habilitadoAtivo">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="habilitadoInativo"
											name="indicadorPrioritario" class="custom-control-input"
											value="false"
											<c:if test="${agenteNegativadorVO.indicadorPrioritario eq 'false'}">checked="checked"</c:if> />
										<label class="custom-control-label" for="habilitadoInativo">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="habilitadoTodos"
											name="indicadorPrioritario" class="custom-control-input"
											value=""
											<c:if test="${agenteNegativadorVO.indicadorPrioritario eq null}">checked="checked"</c:if> />
										<label class="custom-control-label" for="habilitadoTodos">Todos</label>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- fim da primeira row -->

				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<vacess:vacess param="pesquisarComandoNegativacao">
							<button class="btn btn-primary btn-sm" id="botaoPesquisar"
								type="button" onclick="pesquisar();">
								<i class="fa fa-search"></i> Pesquisar
							</button>
						</vacess:vacess>
						<button class="btn btn-secondary btn-sm" name="botaoLimpar"
							id="botaoLimpar" value="limparFormulario" type="button"
							onclick="limparFormulario();">
							<i class="far fa-trash-alt"></i> Limpar
						</button>
					</div>
				</div>
				</br>

				<c:if test="${listaAgenteNegativador ne null}">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="agenteNegativador" width="100%" style="opacity: 0;">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th scope="col" class="text-center">Nome Cliente</th>
									<th scope="col" class="text-center">N�mero do Contrato</th>
									<th scope="col" class="text-center">Data �nicio Contrato</th>
									<th scope="col" class="text-center">Data Fim Contrato</th>
									<th scope="col" class="text-center">Valor do Contrato (R$)</th>
									<th scope="col" class="text-center">Valor da tarifa de
										inclus�o (R$)</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaAgenteNegativador}"
									var="agenteNegativador">
									<tr>
										<td>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${agenteNegativador.chavePrimaria}">
												<input id="chk${agenteNegativador.chavePrimaria}"
													type="checkbox" name="chavesPrimarias"
													class="custom-control-input"
													value="${agenteNegativador.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${agenteNegativador.chavePrimaria}"></label>
											</div>
										</td>
										<td class="text-center"><c:out
												value="${agenteNegativador.cliente.nome}" /></td>
										<td class="text-center"><c:out
												value="${agenteNegativador.numeroContrato}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${agenteNegativador.dataInicioContrato}"
												pattern="dd/MM/yyyy" /></td>
										<td class="text-center"><fmt:formatDate
												value="${agenteNegativador.dataFimContrato}"
												pattern="dd/MM/yyyy" /></td>
										<td class="text-center"><fmt:formatNumber
												value="${agenteNegativador.valorContrato}" /></td>
										<td class="text-center"><fmt:formatNumber
												value="${agenteNegativador.valorTarifaInclusao}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>

			</div>
			<!--  fim do card-body -->

			<div class="card-footer">
				<c:if test="${not empty listaAgenteNegativador}">
					<button id="buttonAlterar" value="alterarAgenteNegativador"
						type="button" class="btn btn-primary btn-sm ml-1 mt-1 float-right"
						onclick="alterarAgenteNegativador();">
						<i class="fas fa-pencil-alt"></i> Alterar
					</button>
					<button id="buttonRemover" value="removerCliente" type="button"
						class="btn btn-danger btn-sm ml-1 mt-1 float-right"
						onclick="removerAgenteNegativador();">
						<i class="far fa-trash-alt"></i> Remover
					</button>
				</c:if>
				<button id="buttonIncluir" value="Incluir"
					class="btn btn-sm btn-primary float-right ml-1 mt-1"
					onclick="incluir();">
					<i class="fa fa-plus"></i> Incluir
				</button>
			</div>
		</div>
	</form:form>
</div>
