<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/util.js'></script>
<script type='text/javascript'
	src='<c:out value='${pageContext.request.contextPath}'/>/dwr/interface/AjaxService.js'></script>
<script type="text/javascript"
	src="<c:out value='${pageContext.request.contextPath}'/>/js/utils.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		iniciarDatatable('#comandoNegativacao');

		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.png"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calendrio',
			dateFormat : 'dd/mm/yyyy'
		});

	});

	function incluir() {
		nomeComando.value = "";
		submeter("comandoNegativacaoForm", "exibirInclusaoComandoNegativacao");
	}

	function pesquisar() {
		submeter("comandoNegativacaoForm", "pesquisarComandoNegativacao");
	}

	function executarComando(chaveComando) {

		$("#chaveComando").val(chaveComando);

		console.log($("#chaveComando").val());

		submeter("comandoNegativacaoForm", "executarComandoNegativacao");
	}


	function alterar() {
		var selecao = verificarSelecaoApenasUm();
		document.forms[0].chavePrimariaAtualizar.value = obterValorUnicoCheckboxSelecionado();
		if (selecao == true) {
			submeter('comandoNegativacaoForm',
					'exibirAlterarComandoNegativacao');
		}
	}

	function limparFormulario() {
		limparFormularios(document.comandoNegativacaoForm);
		document.getElementById("executadoTodos").checked = true;
	}

	function detalharComandoNegativacao(chave) {
		document.forms[0].chavePrimariaAtualizar.value = chave;
		submeter("comandoNegativacaoForm", "exibirDetalhamentoComandoNegativacao");
	}
	
	function removerComando() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if (retorno == true) {
				submeter('comandoNegativacaoForm', 'removerComandoNegativacao');
			}
		}
	}
</script>

<div class="bootstrap">
	<form:form method="post" action="pesquisarComandoNegativacao"
		id="comandoNegativacaoForm" name="comandoNegativacaoForm"
		modelAttribute="ComandoNegativacaoVO">
		<input type="hidden" id="chaveComando" name="chaveComando"
			value="${comandoNegativacaoVO.chaveComando}">
		<input name="chavePrimariaAtualizar" type="hidden"
			id="chavePrimariaAtualizar" />

		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Pesquisar Comando de Negativa��o</h5>
			</div>
			<div class="card-body">

				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para pesquisar um registro
					espec�fico, informe os dados nos campos abaixo e clique em <strong>Pesquisar</strong>,
					ou clique apenas em <strong>Pesquisar</strong> para exibir todos.
					Para incluir um novo registro clique em <strong>Incluir</strong>
				</div>

				<div class="card-body bg-light">
					<div class="row mb-2">
						<div class="col-md-6">
							<div class="form-row">
								<label for="nome" class="col-md-15">Nome do Comando</label>
								<div class="col-md-12">
									<input class="form-control form-control-sm" type="text"
										id="nomeComando" name="nomeComando"
										value="${comandoNegativacaoVO.nomeComando}" maxlength="50"
										size="30"
										onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
								</div>
							</div>

							</br>
							<div class="form-row">
								<div class="col-md-12">
									<label for="executado" class="col-md-15">Comando j�
										executado?</label>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="executadoAtivo" name="executado"
											class="custom-control-input" value="true"
											<c:if test="${comandoNegativacaoVO.executado eq 'true' or empty executado}">checked="checked"</c:if>>
										<label class="custom-control-label" for="executadoAtivo">Sim</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="executadoInativo" name="executado"
											class="custom-control-input" value="false"
											<c:if test="${comandoNegativacaoVO.executado eq 'false'}">checked="checked"</c:if>>
										<label class="custom-control-label" for="executadoInativo">N�o</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" id="executadoTodos" name="executado"
											class="custom-control-input" value=""
											<c:if test="${comandoNegativacaoVO.executado eq null}">checked="checked"</c:if>>
										<label class="custom-control-label" for="executadoTodos">Todos</label>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12">
									<label class="col-md-15">Per�odo de execu��o:</label>
									<div class="input-group input-group-sm">
										<input type="text"
											class="form-control form-control-sm campoData"
											id="dataInicioExecucao" name="dataInicioExecucao"
											value="${comandoNegativacaoVO.dataInicioExecucao}">
										<div class="input-group-prepend">
											<span class="input-group-text">at�</span>
										</div>
										<input type="text"
											class="form-control form-control-sm campoData"
											id="dataFimExecucao" name="dataFimExecucao"
											value="${comandoNegativacaoVO.dataFimExecucao}">
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- fim da primeira row -->

				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<vacess:vacess param="pesquisarComandoNegativacao">
							<button class="btn btn-primary btn-sm" id="botaoPesquisar"
								type="button" onclick="pesquisar();">
								<i class="fa fa-search"></i> Pesquisar
							</button>
						</vacess:vacess>
						<button class="btn btn-secondary btn-sm" name="botaoLimpar"
							id="botaoLimpar" value="limparFormulario" type="button"
							onclick="limparFormulario();">
							<i class="far fa-trash-alt"></i> Limpar
						</button>
					</div>
				</div>
				</br>

				<c:if test="${listaComandoNegativacao ne null}">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="comandoNegativacao" width="100%" style="opacity: 0;">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th scope="col" class="text-center">Nome comando</th>
									<th scope="col" class="text-center">Data Prevista de
										Execu��o</th>
									<th scope="col" class="text-center">Grupo de Faturamento</th>
									<th scope="col" class="text-center">Valor Inicial de
										D�bito</th>
									<th scope="col" class="text-center">Valor Final de D�bito</th>
									<th scope="col" class="text-center">Tipo de Filtro</th>
									<th scope="col" class="text-center">A��o</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaComandoNegativacao}" var="comando">
									<tr>
										<td>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${comando.chavePrimaria}">
												<input id="chk${comando.chavePrimaria}" type="checkbox"
													name="chavesPrimarias" class="custom-control-input"
													value="${comando.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${comando.chavePrimaria}"></label>
											</div>
										</td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<c:out value="${comando.nomeComando}" /></a></td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<fmt:formatDate value="${comando.dataPrevistaExecucao}"
													pattern="dd/MM/yyyy" /></a></td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<c:out value="${comando.grupoFaturamento.descricao}" /></a></td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<fmt:formatNumber value="${comando.valorInicialDebitos}" /></a></td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<fmt:formatNumber value="${comando.valorFinalDebitos}" /></a></td>
										<td class="text-center"><a
											href='javascript:detalharComandoNegativacao(<c:out value='${comando.chavePrimaria}'/>);'><span
												class="linkInvisivel"></span>
											<c:choose>
													<c:when test="${comando.indicadorComandoCliente == true}">
													Por Cliente
												</c:when>
													<c:otherwise>
													Por Crit�rio
												</c:otherwise>
												</c:choose></a></td>
										<td class="text-center"><a
											href="javascript:executarComando(<c:out value="${comando.chavePrimaria}"/>, this.form);">
												<i title="Executar Comando" class="fas fa-cog"></i>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>

			</div>
			<!--  fim do card-body -->

			<div class="card-footer">
				<vacess:vacess param="exibirInclusaoComandoNegativacao">
					<button id="buttonIncluir" value="Incluir"
						class="btn btn-sm btn-primary float-right ml-1 mt-1"
						onclick="incluir();">
						<i class="fa fa-plus"></i> Incluir
					</button>
				</vacess:vacess>
				<c:if test="${not empty listaComandoNegativacao}">
					<button id="buttonAlterar" value="alterarComando" type="button"
						class="btn btn-primary btn-sm ml-1 mt-1 float-right"
						onclick="alterar();">
						<i class="fas fa-pencil-alt"></i> Alterar
					</button>
					<button id="buttonRemover" value="removerCliente" type="button"
						class="btn btn-danger btn-sm ml-1 mt-1 float-right"
						onclick="removerComando();">
						<i class="far fa-trash-alt"></i> Remover
					</button>					
				</c:if>
			</div>
		</div>
	</form:form>
</div>
