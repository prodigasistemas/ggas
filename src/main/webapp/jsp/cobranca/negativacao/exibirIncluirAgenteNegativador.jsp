<!--
Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

Este programa  um software livre; voc pode redistribu-lo e/ou
modific-lo sob os termos de Licena Pblica Geral GNU, conforme
publicada pela Free Software Foundation; verso 2 da Licena.

O GGAS  distribudo na expectativa de ser til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
Consulte a Licena Pblica Geral GNU para obter mais detalhes.

Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
junto com este programa; se no, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type="text/javascript">
	$(function() {

		$("#nome").autocomplete(
				{
					source : function(request, response) {

						$.ajax({
							url : "carregaNomesClientes?nome=" + request.term,
							dataType : "json",
							data : {
								style : "full",
								maxRows : 12,
								name_startsWith : request.term
							},
							success : function(data) {

								response($.map(data, function(item) {
									//alert (item.nome+item.cpfCnpj);
									return {

										label : item.nome + '<br>'
												+ item.cpfCnpj + '<br>'
												+ item.endereco + '<br>'
												+ item.numeroContrato + '<br>'
												+ item.inicioVigenciaContrato
												+ '<br>'
												+ item.fimVigenciaContrato,
										value : item.nome,
										idCliente : item.chavePrimaria,
										idContrato : item.idContrato

									}
								}));
							}
						});
					},
					create : function() {
						$(this).data('ui-autocomplete')._renderItem = function(
								ul, item) {
							return $('<li>')
									.append('<a>' + item.label + '</a>')
									.appendTo(ul);
						};
					},
					minLength : 3,
					select : function(event, ui) {

						carregarCliente(ui.item.value, ui.item.idCliente,
								ui.item.idContrato);

					}
				});

	});

	$(document).ready(function() {
		$('input[name=nome]').change(function() {
			$('input[name=nome]').val($("#nomeCliente").val());
		});
		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.png"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calendrio',
			dateFormat : 'dd/mm/yyyy'
		});

	});

	function carregarCliente(nome, idCliente, idContrato) {

		var nomeCompletoCliente = document.getElementById("nome");
		var nomeCliente = document.getElementById("nomeCliente");
		var chaveCliente = document.getElementById("chaveCliente");
		var url = "carregarClienteAgenteNegativador?chavePrimaria=" + idCliente;
		if (url != null) {
			carregarFragmento('divClienteAgente', url);
		}
		if (idCliente != null) {
			AjaxService.obterClientePorChave(idCliente, {
				callback : function(cliente) {
					if (cliente != null) {
						nomeCompletoCliente.value = cliente["nome"];
						nomeCliente.value = cliente["nome"];
						chaveCliente.value = cliente["chavePrimaria"];
					}
				},
				async : false
			});
		} else {
			idCliente.value = "";
			nomeCompletoCliente.value = "";

		}

	}

	function incluir() {
		submeter('agenteNegativadorForm', 'incluirAgenteNegativador');
	}

	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaAgenteNegativador"/>';
	}
</script>

<style>
.botoes {
	width: 20%;
	height: 100%;
}

.textArea {
	width: 100%;
	height: 43%;
}

.container {
	justify-content: center;
	align-items: center
}
</style>

<div class="bootstrap">

	<form:form action="incluirAgenteNegativador" id="agenteNegativadorForm"
		name="agenteNegativadorForm" method="post"
		modelAttribute="AgenteNegativadorImpl">
		<input type="hidden" id="indexLista" name="indexLista" />
		<input type="hidden" id="chaveCliente" name="chaveCliente"
			value="${cliente.chavePrimaria}" />
		<input type="hidden" id="nomeCliente" name="nomeCliente"
			value="${cliente.nome}" />
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Inclus�o do Agente Negativador</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para
					incluir um novo agente negativador e clique em <b>Salvar</b> para
					inclu�-lo.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<h5>Incluir Agente Negativador</h5>
						<div class="row mb-2">
							<div id="gridDivClientesAgente" class="col-md-6">
								<div id="divClienteAgente" class="form-row">
									<jsp:include
										page="/jsp/cobranca/negativacao/gridDivClientesAgente.jsp"></jsp:include>
									<div class="col-md-12"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label>N�mero Contrato:<span class="text-danger">*</span></label><input
											class="form-control form-control-sm" type="text"
											id="numeroContrato" name="numeroContrato" maxlength="10"
											size="32" value="${agenteNegativador.numeroContrato}"><br />
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label>Data do Contrato:<span class="text-danger">*</span></label>
										<div class="input-group input-group-sm">
											<input type="text"
												class="form-control form-control-sm campoData"
												id="dataInicioContratoForm" name="dataInicioContratoForm"
												value="<fmt:formatDate
												value="${agenteNegativador.dataInicioContrato}"
												pattern="dd/MM/yyyy" />">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input type="text"
												class="form-control form-control-sm campoData"
												id="dataFimContratoForm" name="dataFimContratoForm"
												value="<fmt:formatDate
												value="${agenteNegativador.dataFimContrato}"
												pattern="dd/MM/yyyy" />">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="divPrazo">Prazo para negativa��o:</label> <input
											type="number" class="form-control form-control-sm"
											id="prazoNegativacao" name="prazoNegativacao" maxlength="9"
											size="9" value="${agenteNegativador.prazoNegativacao}">
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="divPrazo">Valor do Contrato (R$):</label>
										<div class="input-group input-group-sm">
											<input type="text" id="valorContratoForm" size="6"
												class="form-control form-control-sm"
												name="valorContratoForm"
												value="${agenteNegativador.valorContrato}"
												onblur="aplicarMascaraNumeroDecimal(this,2);"
												onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="divPrazo">Valor da tarifa de inclus�o
											(R$):</label>
										<div class="input-group input-group-sm">
											<input type="text" id="valorTarifaInclusaoForm" size="6"
												class="form-control form-control-sm"
												name="valorTarifaInclusaoForm"
												value="${agenteNegativador.valorTarifaInclusao}"
												onblur="aplicarMascaraNumeroDecimal(this,2);"
												onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="habilitado" class="col-md-15">Priorit�rio:</label>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoAtivo"
												name="indicadorPrioritario" class="custom-control-input"
												value="1"
												<c:if test="${agenteNegativador ne null and agenteNegativador.indicadorPrioritario eq true}">checked="checked"</c:if> />
											<label class="custom-control-label" for="habilitadoAtivo">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoInativo"
												name="indicadorPrioritario" class="custom-control-input"
												value="0"
												<c:if test="${agenteNegativador eq null or agenteNegativador.indicadorPrioritario eq false}">checked="checked"</c:if> />
											<label class="custom-control-label" for="habilitadoInativo">N�o</label>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>

				<div class="card-footer">
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
								type="button" onclick="cancelar();">
								<i class="fa fa-times"></i> Cancelar
							</button>
							<vacess:vacess param="incluirCliente">
								<button id="botaoIncluir" value="Incluir"
									class="btn btn-sm btn-primary float-right ml-1 mt-1"
									type="button" onclick="incluir();">
									<i class="fa fa-plus"></i> Incluir
								</button>
							</vacess:vacess>
						</div>
					</div>
				</div>

				<hr>
			</div>
	</form:form>
</div>