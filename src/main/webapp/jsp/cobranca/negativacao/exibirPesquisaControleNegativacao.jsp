<!--
Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

Este programa  um software livre; voc pode redistribu-lo e/ou
modific-lo sob os termos de Licena Pblica Geral GNU, conforme
publicada pela Free Software Foundation; verso 2 da Licena.

O GGAS  distribudo na expectativa de ser til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
Consulte a Licena Pblica Geral GNU para obter mais detalhes.

Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
junto com este programa; se no, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type="text/javascript">
	$(document).ready(function() {
		
		$(".campoData").datepicker({
			changeYear : true,
			showOn : 'button',
			buttonImage : '<c:url value="/imagens/calendario.png"/>',
			buttonImageOnly : true,
			buttonText : 'Exibir Calend�rio',
			dateFormat : 'dd/mm/yy'
		});

		iniciarDatatable('#table-aviso-corte');

	});

	function pesquisar() {
		submeter('faturaClienteForm', 'pesquisarClientesParaNegativacao');
	}

	function incluir() {
		$('#dataIncluirModal').modal('show');
	}

	function confirmar() {
		$('#dataConfirmarModal').modal('show');
	}

	function excluir() {
		$('#dataExclusaoModal').modal('show');
	}

	function cancelar() {
		$('#dataCancelamentoModal').modal('show');
	}
	
	function salvarDataInclusaoNegativacao() {
		var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_REPLICAR_DATA"/>');
		$("#replicar").val(retorno);
		submeter('faturaClienteForm', 'incluirClientesNegativacao');
	}

	function salvarDataConfirmacaoNegativacao() {
		submeter('faturaClienteForm', 'confirmarClientesNegativacao');
	}

	function salvarDataExclusaoNegativacao() {
		submeter('faturaClienteForm', 'excluirClientesNegativacao');
	}

	function salvarDataCancelamentoNegativacao() {
		submeter('faturaClienteForm', 'cancelarClientesNegativacao');
	}
</script>



<div class="bootstrap">
	<!-- INCIO DE BLOCO: Variveis JSP utilizads no arquivo index.js -->


	<!-- FIM DE BLOCO: Variveis JSP utilizads no arquivo index.js -->
	<form:form action="pesquisarClientesParaNegativacao"
		id="faturaClienteForm" name="faturaClienteForm" method="post"
		modelAttribute="ClienteNegativacaoVO">
		<input name="habilitar" type="hidden" id="habilitar" value="">
		<input name="replicar" type="hidden" id="replicar" value="">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Controle de negativa��o de clientes</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Para pesquisar as faturas
					selecionadas para negativa��o, clique em <b>Pesquisar</b>
					retornando a lista de clientes.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<h5>Filtrar Clientes</h5>

						<div class="form-row">
							<label for="nome" class="col-md-15">Nome Comando</label>
							<div class="col-md-12">
								<input class="form-control form-control-sm" type="text"
									id="nomeComando" name="nomeComando"
									value="${faturaClienteForm.nomeComando}" maxlength="50"
									size="30"
									onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
							</div>
						</div>

						<div class="form-row">
							<label for="habilitado" class="col-md-12">Situa��o da
								negativa��o:</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="aIncluir"
										name="habilitado" class="custom-control-input" value="5"
										<c:if test="${faturaClienteForm.situacaoNegativacao eq 5}">checked="checked"</c:if>>
									<label class="custom-control-label" for="aIncluir">A
										incluir</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="incluido"
										name="habilitado" class="custom-control-input" value="1"
										<c:if test="${faturaClienteForm.situacaoNegativacao eq 1}">checked="checked"</c:if>>
									<label class="custom-control-label" for="incluido">Inclu�do</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="confirmado"
										name="habilitado" class="custom-control-input" value="2"
										<c:if test="${faturaClienteForm.situacaoNegativacao eq 2}">checked="checked"</c:if>>
									<label class="custom-control-label" for="confirmado">Confirmado</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="excluido"
										name="habilitado" class="custom-control-input" value="3"
										<c:if test="${faturaClienteForm.situacaoNegativacao eq 3}">checked="checked"</c:if>>
									<label class="custom-control-label" for="excluido">Exclu�do</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="cancelado"
										name="habilitado" class="custom-control-input" value="4"
										<c:if test="${faturaClienteForm.situacaoNegativacao eq 4}">checked="checked"</c:if>>
									<label class="custom-control-label" for="cancelado">Cancelado</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoNegativacao" id="todos"
										name="habilitado" class="custom-control-input" value="0"
										<c:if test="${faturaClienteForm.situacaoNegativacao ne 1 
											and faturaClienteForm.situacaoNegativacao ne 2 
										    and faturaClienteForm.situacaoNegativacao ne 3
										    and faturaClienteForm.situacaoNegativacao ne 4
										    and faturaClienteForm.situacaoNegativacao ne 5}">checked="checked"</c:if>>
									<label class="custom-control-label" for="todos">Todos</label>
								</div>
							</div>
						</div>

						<div class="form-row">
							<label for="habilitado" class="col-md-12">Contas pagas?</label>
							<div class="col-md-12">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoFaturas" id="contasPagas"
										class="custom-control-input" value="1"
										<c:if test="${faturaClienteForm.situacaoFaturas eq 1}">checked="checked"</c:if>>
									<label class="custom-control-label" for="contasPagas">Pagas</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoFaturas" id="ContasNaoPagas"
										class="custom-control-input" value="2"
										<c:if test="${faturaClienteForm.situacaoFaturas eq 2}">checked="checked"</c:if>>
									<label class="custom-control-label" for="ContasNaoPagas">N�o
										Pagas</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoFaturas" id="contasPrecristas"
										class="custom-control-input" value="3"
										<c:if test="${faturaClienteForm.situacaoFaturas eq 3}">checked="checked"</c:if>>
									<label class="custom-control-label" for="contasPrecristas">Prescritas</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" name="situacaoFaturas" id="todasContas"
										class="custom-control-input" value="0"
										<c:if test="${faturaClienteForm.situacaoFaturas ne 1 && faturaClienteForm.situacaoFaturas ne 2 && faturaClienteForm.situacaoFaturas ne 3}">checked="checked"</c:if>>
									<label class="custom-control-label" for="todasContas">Todos</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<button class="btn btn-primary btn-sm" id="botaoPesquisar"
							type="button" onclick="pesquisar();">
							<i class="fa fa-search"></i> Pesquisar
						</button>
					</div>
				</div>

				<c:if test="${listaFaturasNegativacao ne null}">
					<hr class="linhaSeparadoraPesquisa" />
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="table-aviso-corte" width="100%" style="">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<th scope="col" class="text-center">Cliente</th>
									<th scope="col" class="text-center">Cpf/Cnpj</th>
									<th scope="col" class="text-center">Fatura</th>
									<th scope="col" class="text-center">Valor(R$)</th>
									<th scope="col" class="text-center">Data Vencimento</th>
									<th scope="col" class="text-center">Situa��o Pagamento</th>
									<th scope="col" class="text-center">Data notifica��o</th>
									<th scope="col" class="text-center">Data inclus�o</th>
									<th scope="col" class="text-center">Data confirma��o</th>
									<th scope="col" class="text-center">Data exclus�o</th>
									<th scope="col" class="text-center">Data cancelamento</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaFaturasNegativacao}"
									var="faturaNegativacao">
									<tr>
										<td>
											<div
												class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
												data-identificador-check="chk${faturaNegativacao.chavePrimaria}">
												<input id="chk${faturaNegativacao.chavePrimaria}"
													type="checkbox" name="chavesPrimarias"
													class="custom-control-input"
													value="${faturaNegativacao.chavePrimaria}"> <label
													class="custom-control-label p-0"
													for="chk${faturaNegativacao.chavePrimaria}"></label>
											</div>
										</td>
										<td class="text-center"><c:out
												value="${faturaNegativacao.clienteNegativacao.cliente.nome}" /></td>
										<td class="text-center"><c:choose>
												<c:when
													test="${faturaNegativacao.clienteNegativacao.cliente.cpf eq null || faturaNegativacao.clienteNegativacao.cliente.cpf eq ''}">
													<c:out
														value='${faturaNegativacao.clienteNegativacao.cliente.cnpjFormatado}' />
												</c:when>
												<c:otherwise>
													<c:out
														value='${faturaNegativacao.clienteNegativacao.cliente.cpfFormatado}' />
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><c:out
												value="${faturaNegativacao.fatura.chavePrimaria}" /></td>
										<td class="text-center"><fmt:formatNumber
												value="${faturaNegativacao.fatura.valorTotal}" /> </a></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.fatura.dataVencimento}" /></td>
										<td class="text-center"><c:out
												value="${faturaNegativacao.fatura.situacaoPagamento.descricao}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.clienteNegativacao.dataNotificacao}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.dataInclusaoNegativacao}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.dataConfirmacaoNegativacao}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.dataExclusaoNegativacao}" /></td>
										<td class="text-center"><fmt:formatDate
												value="${faturaNegativacao.dataCancelamentoNegativacao}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>

					<div class="row mt-3">
						<div class="col align-self-end text-right">
							<button class="btn btn-primary btn-sm" id="botaoIncluir"
								type="button" onclick="incluir();">Incluir na Negativa��o</button>
							<button class="btn btn-primary btn-sm" id="botaoConfirmar"
								type="button" onclick="confirmar();">Confirmar retorno
								Negativa��o</button>
							<button class="btn btn-primary btn-sm" id="botaoExcluir"
								type="button" onclick="excluir();">Excluir Negativa��o</button>
							<button class="btn btn-primary btn-sm" id="botaoExcluir"
								type="button" onclick="cancelar();">Cancelar
								Negativa��o</button>
						</div>
					</div>
			</div>
			</c:if>
			<hr>

		</div>
</div>


<!-- Modal -->
<div class="modal fade" id="dataIncluirModal" tabindex="-1" role="dialog"
	aria-labelledby="dataIncluirModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Data
					Inclus�o</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="input-group input-group-sm">
					<input type="text" class="form-control form-control-sm campoData"
						id="dataIncluir" name="dataIncluir" maxlength="10"
						value="${dataIncluir}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary"
					onclick="salvarDataInclusaoNegativacao();">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="dataConfirmarModal" tabindex="-1" role="dialog"
	aria-labelledby="dataConfirmarModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Data
					Confirma��o</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="input-group input-group-sm">
					<input type="text" class="form-control form-control-sm campoData"
						id="dataConfirmar" name="dataConfirmar" maxlength="10"
						value="${dataConfirmar}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary"
					onclick="salvarDataConfirmacaoNegativacao();">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="dataExclusaoModal" tabindex="-1" role="dialog"
	aria-labelledby="dataExclusaoModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Data
					de Exclus�o</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="input-group input-group-sm">
					<input type="text" class="form-control form-control-sm campoData"
						id="dataExclusao" name="dataExclusao" maxlength="10"
						value="${dataExclusao}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary"
					onclick="salvarDataExclusaoNegativacao();">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="dataCancelamentoModal" tabindex="-1" role="dialog"
	aria-labelledby="dataCancelamentoModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Data
					Cancelamento</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="input-group input-group-sm">
					<input type="text" class="form-control form-control-sm campoData"
						id="dataCancelamento" name="dataCancelamento" maxlength="10"
						value="${dataCancelamento}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary"
					onclick="salvarDataCancelamentoNegativacao();">Confirmar</button>
			</div>
		</div>
	</div>
</div>


</form:form>
</div>
