<!--
Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

Este programa  um software livre; voc pode redistribu-lo e/ou
modific-lo sob os termos de Licena Pblica Geral GNU, conforme
publicada pela Free Software Foundation; verso 2 da Licena.

O GGAS  distribudo na expectativa de ser til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
Consulte a Licena Pblica Geral GNU para obter mais detalhes.

Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
junto com este programa; se no, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type="text/javascript">
	$(document).ready(
			function() {

				iniciarDatatable('#table-cliente');
				iniciarDatatable('#table-faturas-simuladas');
				
				 if($("#atualizarSomenteNome").val() == "1"){
						$("#comandoNegativacaoForm :input").prop("disabled",
								true);
						$("#botaoIncluir").prop("disabled", false);
						$("#botaoCancelar").prop("disabled", false);
						$("#nomeComando").prop("disabled", false);
						$("#chavePrimariaAtualizar").prop("disabled", false);
						$("#grupoFaturamento").prop("disabled", false);
						$("#rotasSelecionadas").prop("disabled", false);
						$("#chavesRotasSelecionadas").prop("disabled", false);
				 }
				
				$("#referenciaInicioDebitos").inputmask("9999/99", {
					placeholder : "_"
				});
				$("#referenciaFimDebitos").inputmask("9999/99", {
					placeholder : "_"
				});

				if ($('input[name=tipoComando]:checked',
						'#comandoNegativacaoForm').val() == "criterio") {
					$("#filtroCliente").hide();
					$("#filtroCriterio").show();
				} else {
					$("#filtroCliente").show();
					$("#filtroCriterio").hide();
				}

				$(".campoData").datepicker({ 
					changeYear : true,
					showOn : 'button',
					buttonImage : '<c:url value="/imagens/calendario.png"/>',
					buttonImageOnly : true,
					buttonText : 'Exibir Calendrio',
					dateFormat : 'dd/mm/yy'
				});


				if ($('input[name=indicadorSimulacao]:checked',
				'#comandoNegativacaoForm').val() == "false") {
				$(botaoSimular).prop('disabled', true); //disable clicked button
				}
				
				carregarRotas(document.getElementById("grupoFaturamento"));

				 if ($("#dataVencimentoInicial").val().trim().length > 0 
						 && $("#dataVencimentoFinal").val().trim().length > 0) {
					var diasAtrasoInicial = document.getElementById("diasAtrasoInicial");
					 diasAtrasoInicial.disabled = true;
					 diasAtrasoFim.disabled = true;
					 
				 } else if ($("#diasAtrasoInicial").val().trim().length > 0){
					var dataVencimentoInicial = document.getElementById("dataVencimentoInicial");
					var dataVencimentoFinal = document.getElementById("dataVencimentoFinal");
					
					dataVencimentoInicial.disabled = true;
					dataVencimentoFinal.disabled = true;
				 }
				 
				 var dataInicial = document.getElementById("dataVencimentoInicial");
				 dataInicial.oninput = function () {
				     document.getElementById("diasAtrasoInicial").disabled = this.value != "";
				     document.getElementById("diasAtrasoFim").disabled = this.value != "";
				 };
				 
				 var diasAtrasoInicial = document.getElementById("diasAtrasoInicial");
				 diasAtrasoInicial.oninput = function () {
				     document.getElementById("dataVencimentoInicial").disabled = this.value != "";
				     document.getElementById("dataVencimentoFinal").disabled = this.value != "";
				 };
				            
				 $("#habilitadoAtivo").prop("disabled", true);
				 $("#habilitadoInativo").prop("disabled", true);
				 
				 
				});


	function pesquisar() {
		submeter('comandoNegativacaoForm', 'pesquisarPriorizacaoCorte');
	}

	function simulacaoSim() {
		if (${listaFaturaSimulada ne null}){
			$(botaoIncluir).prop('disabled', false); //disable clicked button
			$(botaoSimular).prop('disabled', false); //disable clicked button
			}else{
		$(botaoIncluir).prop('disabled', true); //disable clicked button
		$(botaoSimular).prop('disabled', false); //disable clicked button
			}
	}

	function simulacaoNao() {
		if (${listaFaturaSimulada ne null}){
			$(botaoIncluir).prop('disabled', false); //disable clicked button
			$(botaoSimular).prop('disabled', true); //disable clicked button
			}else{
		$(botaoIncluir).prop('disabled', false); //disable clicked button
		$(botaoSimular).prop('disabled', true); //disable clicked button
			}
	}

	function selecionarFiltro(elem) {
		if (elem.value == "criterio") {
			$("#filtroCliente").fadeOut(500);
			$("#filtroCriterio").fadeIn(500);
		} else {
			$("#filtroCliente").fadeIn(500)
			$("#filtroCriterio").fadeOut(500);
		}
	}

	function inserirClienteGrid() {
		if (!$("#nomeClienteTexto").val() == "") {
			carregarFragmentoPOST('gridClientes', "incluirClienteGrid",
					document.forms["comandoNegativacaoForm"], true);
			iniciarDatatable('#table-cliente');
		}

	}

	function removerCliente(indice, form) {
		document.forms["comandoNegativacaoForm"].indexLista.value = indice;
		carregarFragmentoPOST('gridClientes', "removerClientesGrid",
				document.forms["comandoNegativacaoForm"]);
		iniciarDatatable('#table-cliente');
	}

	function selecionarCliente(idSelecionado) {
		var idCliente = document.getElementById("idCliente");
		var nomeCompletoCliente = document
				.getElementById("nomeCompletoCliente");
		var documentoFormatado = document.getElementById("documentoFormatado");
		var emailCliente = document.getElementById("emailCliente");
		var enderecoFormatado = document
				.getElementById("enderecoFormatadoCliente");

		if (idSelecionado != '') {
			AjaxService.obterClientePorChave(idSelecionado, {
				callback : function(cliente) {
					if (cliente != null) {
						idCliente.value = cliente["chavePrimaria"];
						nomeCompletoCliente.value = cliente["nome"];
						if (cliente["cnpj"] != undefined) {
							documentoFormatado.value = cliente["cnpj"];
						} else {
							if (cliente["cpf"] != undefined) {
								documentoFormatado.value = cliente["cpf"];
							} else {
								documentoFormatado.value = "";
							}
						}
						emailCliente.value = cliente["email"];
						enderecoFormatado.value = cliente["enderecoFormatado"];
					}
				},
				async : false
			});
		} else {
			idCliente.value = "";
			nomeCompletoCliente.value = "";
			documentoFormatado.value = "";
			emailCliente.value = "";
			enderecoFormatado.value = "";

		}

		document.getElementById("nomeClienteTexto").value = nomeCompletoCliente.value;
		document.getElementById("documentoFormatadoTexto").value = documentoFormatado.value;
		document.getElementById("emailClienteTexto").value = emailCliente.value;
		document.getElementById("enderecoFormatadoTexto").value = enderecoFormatado.value;
	}

	function exibirPopupPesquisaCliente() {
		popup = window
				.open(
						'exibirPesquisaClientePopup',
						'popup',
						'height=750,width=700,toolbar=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes ,modal=yes');
	}

	function carregarRotas(elem) {
		var codGrupoFaturamento = elem.value;
		carregarRotasPorGrupo(codGrupoFaturamento);
	}

	function carregarRotasPorGrupo(codGrupoFaturamento){
		if (codGrupoFaturamento > 0) {
			var selectRotas = document.getElementById("rotasSelecionadas");
			var idRota = "${negativacaoVO.idRota}";

			selectRotas.length=0;
			var novaOpcao =  null;
			selectRotas.options[selectRotas.length] = novaOpcao;
			AjaxService.listarRotaPorGrupoFaturamento(codGrupoFaturamento,
					function(rotas) {
						for (key in rotas){
							var novaOpcao = new Option(rotas[key], key);
							if (key == idRota){
								novaOpcao.selected = true;
							}
							selectRotas.options[selectRotas.length] = novaOpcao;
						}
						ordernarSelect(selectRotas)
						 $.each($("#chavesRotasSelecionadas").val().split(","), function(i,e){
							    $("#rotasSelecionadas option[value='" + e + "']").prop("selected", true);
							});
						 if($("#atualizarSomenteNome").val() == "1"){
								$("#grupoFaturamento").prop("disabled", true);
								$("#rotasSelecionadas").prop("disabled", true);
						 }
					}
			);
		} else {
			var selectRotas = document.getElementById("rotasSelecionadas");
			selectRotas.length=0;
			 if($("#atualizarSomenteNome").val() == "1"){
					$("#grupoFaturamento").prop("disabled", true);
					$("#rotasSelecionadas").prop("disabled", true);
			 }
		}
	}

      function selecionarRotasGrupoFaturamento() {
		
		var rotasSelecionadas = $("#rotasSelecionadas option:selected").val();
		var idGrupoFaturamento = $("#idGrupoFaturamento").val();
			
				
			if( idGrupoFaturamento > 0 && typeof rotasSelecionadas == 'undefined' ) {
					
				$('#rotasSelecionadas').children().attr("selected", true);
					
			}
		}

      function nullOrEmpty (str) {
  	    var v = document.getElementById(str).value;
  	    return v == null || v == "" || v == "-1";  
  	}

	function alterar() {
		if($("#table-cliente td").hasClass("dataTables_empty") && $('input[name=tipoComando]:checked',
      '#comandoNegativacaoForm').val() == "cliente"){
			alert (" Necess�rio incluir pelo menos um cliente! ");
		} else if((nullOrEmpty("quantidadeMaximaInclusao")
		            && nullOrEmpty("valorInicialDebitos")
		            && nullOrEmpty("valorFinalDebitos")
		            && nullOrEmpty("referenciaInicioDebitos")
		            && nullOrEmpty("referenciaFimDebitos")
		            && nullOrEmpty("dataVencimentoInicial")
		            && nullOrEmpty("dataVencimentoFinal")
		            && nullOrEmpty ("situacaoConsumo")
		            && nullOrEmpty ("situacaoContrato")
		            && nullOrEmpty ("segmento")
		            && nullOrEmpty ("grupoFaturamento")
		            && nullOrEmpty ("rotuloRota")
		            && nullOrEmpty ("diasAtrasoInicial")
		            && nullOrEmpty ("diasAtrasoFim")) && $('input[name=tipoComando]:checked',
		            '#comandoNegativacaoForm').val() == "criterio"){
				alert (" Indique ao menos a quantidade m�xima de inclus�es ");
		} else {
			if($("#atualizarSomenteNome").val() != "1"){
				submeter('comandoNegativacaoForm', 'alterarComandoNegativacao');
			} else {
				submeter('comandoNegativacaoForm', 'alterarNomeComando');
			}
			
		}
	}

	function simular() {
		if((nullOrEmpty("quantidadeMaximaInclusao")
	            && nullOrEmpty("valorInicialDebitos")
	            && nullOrEmpty("valorFinalDebitos")
	            && nullOrEmpty("referenciaInicioDebitos")
	            && nullOrEmpty("referenciaFimDebitos")
	            && nullOrEmpty("dataVencimentoInicial")
	            && nullOrEmpty("dataVencimentoFinal")
	            && nullOrEmpty ("situacaoConsumo")
	            && nullOrEmpty ("situacaoContrato")
	            && nullOrEmpty ("segmento")
	            && nullOrEmpty ("grupoFaturamento")
	            && nullOrEmpty ("rotuloRota")
	            && nullOrEmpty ("diasAtrasoInicial")
	            && nullOrEmpty ("diasAtrasoFim")) && $('input[name=tipoComando]:checked',
	            '#comandoNegativacaoForm').val() == "criterio"){
			alert (" Indique ao menos a quantidade m�xima de inclus�es ");
	} else {
		submeter('comandoNegativacaoForm', 'simularComandoNegativacao');
		$(botaoIncluir).prop('disabled', false); //enable clicked button
	  }
	}
	
	function cancelar(){
		location.href = '<c:url value="/exibirPesquisaComandoNegativacao"/>';
	}
	
</script>

<style>
.botoes {
	width: 20%;
	height: 100%;
}

.textArea {
	width: 100%;
	height: 43%;
}

.container {
	justify-content: center;
	align-items: center
}
</style>

<div class="bootstrap">

	<form:form action="incluirComandoNegativacao"
		id="comandoNegativacaoForm" name="comandoNegativacaoForm"
		method="post" modelAttribute="ComandoNegativacaoImpl">
		<input name="atualizarSomenteNome" type="hidden" id="atualizarSomenteNome" value="${atualizarSomenteNome}">
		<input name="habilitar" type="hidden" id="habilitar" value="">
		<input name="telaAlterar" type="hidden" id="telaAlterar" value="1">
		<input name="comandoCliente" type="hidden" id="comandoCliente" value="${comandoCliente}">
		<input name="chavesRotasSelecionadas" type="hidden" id="chavesRotasSelecionadas" value="${chavesRotasSelecionadas}">
		<input type="hidden" id="indexLista" name="indexLista" />
		<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${comandoNegativacao.chavePrimaria}"/>
		<input name="chavePrimariaAtualizar" type="hidden" id="chavePrimariaAtualizar" value="${comandoNegativacao.chavePrimaria}"/>
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Altera��o do Comando de Negativa��o</h5>
			</div>
			<div class="card-body">
				<div class="alert alert-primary fade show" role="alert">
					<i class="fa fa-question-circle"></i> Preencha os campos para
					alterar um novo comando de negativa��o e clique em <b>Salvar</b>
					para alter�-lo.
				</div>

				<hr>

				<div class="card">
					<div class="card-body bg-light">
						<h5>Alterar Comando de Negativa��o</h5>
						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="habilitado" class="col-md-15">Tipo de
											Comando:</label>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoAtivo" name="tipoComando"
												class="custom-control-input" value="criterio"
												<c:if test="${comandoNegativacao.indicadorComandoCliente eq 'false'}">checked="checked"</c:if>
												 onclick="selecionarFiltro(this)" /> <label
												class="custom-control-label" for="habilitadoAtivo">Crit�rio</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoInativo" name="tipoComando"
												class="custom-control-input" value="cliente"
												<c:if test="${comandoNegativacao.indicadorComandoCliente eq 'true'}">checked="checked"</c:if>
												onclick="selecionarFiltro(this)" /> <label
												class="custom-control-label" for="habilitadoInativo">Cliente</label>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="agenteNegativador">Agente Negativador:</label> <select
											name="agenteNegativador" id="agenteNegativador"
											class="form-control form-control-sm">
											<c:forEach items="${listarAgenteNegativador}"
												var="agenteNegativador">
												<option
													value="<c:out value="${agenteNegativador.chavePrimaria}"/>"
													<c:if test="${comandoNegativacao.agenteNegativador.chavePrimaria == agenteNegativador.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${agenteNegativador.cliente.nome}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="nomeComando">Nome do comando: <span class="text-danger">*</span></label>
										<textarea class="textArea" id="nomeComando" name="nomeComando"
											placeholder="Nome do comando" rows="1"
											onblur="this.value=removerEspacoInicialFinal(this.value);">${comandoNegativacao.nomeComando}</textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="dataPrevistaExecucao">Previs�o de
											execu��o:</label>
										<div class="input-group input-group-sm">
											<input type="text"
												class="form-control form-control-sm campoData"
												id="dataPrevistaExecucao" name="dataPrevistaExecucao"
												value="${dataPrevistaExecucao}">
										</div>
									</div>
								</div>
							</div>
								
							</div>
										
						</div>

					<!-- INICIO FILTRO CRITERIO -->
					<div class="card-body bg-light" id="filtroCriterio">
						<h5>Dados Gerais</h5>
						<div class="form-row">
							<div class="col-md-12">
								<label for="indicadorSimulacao" class="col-md-15">Simular:</label>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="simularSim" name="indicadorSimulacao"
										class="custom-control-input" value="true"
										<c:if test="${comandoNegativacao.indicadorSimulacao ne null && comandoNegativacao.indicadorSimulacao eq 'true'}">checked="checked"</c:if> />
									<label class="custom-control-label" onclick="simulacaoSim();"
										for="simularSim">Sim</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="simularNao" name="indicadorSimulacao"
										class="custom-control-input" value="false"
										<c:if test="${comandoNegativacao.indicadorSimulacao ne
												 null && comandoNegativacao.indicadorSimulacao eq 'false'}">checked="checked"</c:if> />
									<label class="custom-control-label" onclick="simulacaoNao();"
										for="simularNao">N�o</label>
								</div>
							</div>
						</div>
						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label for="divPrazo">Quantidade M�xima de Inclus�es:</label> <input
											type="number" class="form-control form-control-sm"
											id="quantidadeMaximaInclusao" name="quantidadeMaximaInclusao"
											maxlength="9" size="9"
											value="${comandoNegativacao.quantidadeMaximaInclusao}">
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="divPrazo">Valor de D�bitos (R$):</label>
										<div class="input-group input-group-sm">
											<input type="text" id="valorInicialDebitos" size="6"
												class="form-control form-control-sm"
												name="valorInicialDebitos"
												value ="<fmt:formatNumber value="${comandoNegativacao.valorInicialDebitos}" />"
												onblur="aplicarMascaraNumeroDecimal(this,2);"
												onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input type="text" id="valorFinalDebitos" size="6"
												class="form-control form-control-sm"
												name="valorFinalDebitos"
												value ="<fmt:formatNumber value="${comandoNegativacao.valorFinalDebitos}" />"
												onblur="aplicarMascaraNumeroDecimal(this,2);"
												onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);">
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="habilitado" class="col-md-15">Considerar
											Contas em An�lise:</label>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="considerarSim"
												name="indicadorConsiderarContasEmAnalise"
												class="custom-control-input" value="true"
												<c:if test="${comandoNegativacao.indicadorConsiderarContasEmAnalise ne
												 null && comandoNegativacao.indicadorConsiderarContasEmAnalise eq 'true'}">checked="checked"</c:if> />
											<label class="custom-control-label" for="considerarSim">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="considerarNao"
												name="indicadorConsiderarContasEmAnalise"
												class="custom-control-input" value="false"
												<c:if test="${comandoNegativacao.indicadorConsiderarContasEmAnalise ne
												 null && comandoNegativacao.indicadorConsiderarContasEmAnalise eq 'false'}">checked="checked"</c:if> />
											<label class="custom-control-label" for="considerarNao">N�o</label>
										</div>
									</div>
								</div>

							</div>

							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label>Referencia de D�bito: </label>
										<div class="input-group input-group-sm">
											<input class="form-control form-control-sm" type="text"
												id="referenciaInicioDebitos" name="referenciaInicioDebitos"
												value="<c:out value="${comandoNegativacao.referenciaInicialDebitos}"/>">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input class="form-control form-control-sm" type="text"
												id="referenciaFimDebitos" name="referenciaFimDebitos"
												value="<c:out value="${comandoNegativacao.referenciaFinalDebitos}"/>">
										</div>
									</div>

									<div class="col-md-12">
										<label>Per�odo de Vencimento:</label>
										<div class="input-group input-group-sm">
											<input type="text"
												class="form-control form-control-sm campoData"
												id="dataVencimentoInicial" name="dataVencimentoInicial"
												value="${dataVencimentoInicial}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input type="text"
												class="form-control form-control-sm campoData"
												id="dataVencimentoFinal" name="dataVencimentoFinal"
												value="${dataVencimentoFinal}">
										</div>
									</div>
									
									<div class="col-md-12">
									<label>Vencidos a partir de (dias):</label>
									<div class="input-group input-group-sm">
			                          <input class="form-control form-control-sm" type="text"
			                          id="diasAtrasoInicial" name="diasAtrasoInicial"  maxlength="4" value="${comandoNegativacao.diasAtrasoInicial}">		                        
			                          <div class="input-group-prepend">
											<span class="input-group-text">at�</span>
								      </div>								  
			                          <input class="form-control form-control-sm" type="text"
			                          id="diasAtrasoFim" name="diasAtrasoFim"  maxlength="4" value="${comandoNegativacao.diasAtrasoFim}">
			                        </div>
			                     </div>
									
								</div>

							</div>
						</div>

						</br> </br>
						<h5>Pontos de Consumo</h5>
						<div class="row mb-2">
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="situacaoConsumo">Situa��o do Ponto de
											Consumo:</label> <select name="situacaoConsumo" id="situacaoConsumo"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoConsumo}"
												var="situacaoConsumo">
												<option
													value="<c:out value="${situacaoConsumo.chavePrimaria}"/>"
													<c:if test="${comandoNegativacao.situacaoConsumo.chavePrimaria == situacaoConsumo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacaoConsumo.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="habilitado" class="col-md-15">Tipo
											Cliente:</label>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="tipoClienteParticular"
												name="indicadorCliente" class="custom-control-input"
												value="1"
												<c:if test="${comandoNegativacao.indicadorCliente ne
												 null && comandoNegativacao.indicadorCliente eq '1'}">checked="checked"</c:if> />
											<label class="custom-control-label"
												for="tipoClienteParticular">Particular</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="tipoClientePublico"
												name="indicadorCliente" class="custom-control-input"
												value="2"
												<c:if test="${comandoNegativacao.indicadorCliente ne
												 null && comandoNegativacao.indicadorCliente eq '2'}">checked="checked"</c:if> />
											<label class="custom-control-label" for="tipoClientePublico">Publico</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="tipoClienteTodos"
												name="indicadorCliente" class="custom-control-input"
												value="0"
												<c:if test="${comandoNegativacao.indicadorCliente ne
												 null && comandoNegativacao.indicadorCliente eq '0'}">checked="checked"</c:if> />
											<label class="custom-control-label" for="tipoClienteTodos">Todos</label>
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="situacaoContrato">Situa��o do Contrato:</label> <select
											name="situacaoContrato" id="situacaoContrato"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSituacaoContrato}"
												var="situacaoContrato">
												<option
													value="<c:out value="${situacaoContrato.chavePrimaria}"/>"
													<c:if test="${comandoNegativacao.situacaoContrato.chavePrimaria == situacaoContrato.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${situacaoContrato.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-10">
										<label for="segmento">Segmento:</label> <select
											name="segmento" id="segmento"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaSegmento}" var="segmento">
												<option value="<c:out value="${segmento.chavePrimaria}"/>"
													<c:if test="${comandoNegativacao.segmento.chavePrimaria == segmento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${segmento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-10">
										<label for="grupoFaturamento">Grupo de Faturamento:</label> <select
											name="grupoFaturamento" id="grupoFaturamento"
											class="form-control form-control-sm" onchange="carregarRotas(this)">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaGruposFaturamento}"
												var="grupoFaturamento">
												<option
													value="<c:out value="${grupoFaturamento.chavePrimaria}"/>"
													<c:if test="${comandoNegativacao.grupoFaturamento.chavePrimaria == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${grupoFaturamento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-row">		
						<div class="col-md-10">
						<label id="rotuloRota" for="rotasSelecionadas">Rota:</label>
						<select name="rotasSelecionadas" id="rotasSelecionadas" class="form-control form-control-sm" multiple="multiple">
							<c:forEach items="${listaRotas}" var="rota">
								<option value="<c:out value="${rota.chavePrimaria}"/>"
										<c:if test="${negativacaoVO.idRota == rota.chavePrimaria}">selected="selected"</c:if>>
									<c:out value="${rota.numeroRota}"/>
								</option>
							</c:forEach>
						</select>
					</div>
					</div>
							</div>
						</div>

						</br> </br>
						<c:if test="${listaFaturaSimulada ne null}">
						<h5>Lista de Faturas</h5></br>
						<div class="card-body bg-light" id="filtroCriterio">
							<div class="form-row">
								<h6>Quantidade total de clientes: <c:out value="${qtdClientesSimulacao}" /></h6>
							</div>
								
							<div class="form-row">
								<h6>Valor total das faturas: R$ <fmt:formatNumber value="${valorTotalFaturasSimulacao}" /></h6>
							</div>
							
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover"
									id="table-faturas-simuladas" width="100%" style="opacity: 0;">
									<thead class="thead-ggas-bootstrap">
										<tr>
											<th scope="col" class="text-center">Ativo</th>
											<th scope="col" class="text-center">N�mero Fatura</th>
											<th scope="col" class="text-center">Valor Fatura (R$)</th>
											<th scope="col" class="text-center">Cliente</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaFaturaSimulada}" var="faturaSimulada">
											<tr>
												<td class="text-center"><a title="Habilitado"
													<c:choose>
												<c:when test="${faturaSimulada.habilitado == true}">
													<i class="fas fa-check-circle text-success" title="Ativo"></i>
												</c:when>
												<c:otherwise>
													<i class="fas fa-ban text-danger" title="Inativo"></i>
												</c:otherwise>
											</c:choose></td>
												<td><c:out value="${faturaSimulada.chavePrimaria}" />
												</td>
												<td><fmt:formatNumber value="${faturaSimulada.valorTotal}" /> </a></td>
												<td><c:out value="${faturaSimulada.cliente.nome}" /> </a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:if>


					</div>

				</div>

				<!-- FIM FILTRO CRITERIO -->

				<!-- INICIO FILTRO CLIENTE -->
				<div class="card-body bg-light" id="filtroCliente"
					style="display: none;">
					<div class="form-row mt-3">
						<div class="col-md-12">
							<h5>Filtro Cliente</h5>
						</div>
						<div class="col-md-6 container">
							<div class="accordion">
								<div class="card">
									<div class="card-header p-0">
										<input name="idCliente" type="hidden" id="idCliente"
											value="${comandoNegativacaoForm.map.idCliente}"> <input
											name="nomeCompletoCliente" type="hidden"
											id="nomeCompletoCliente"
											value="${comandoNegativacaoForm.map.nomeCompletoCliente}">
										<input name="documentoFormatado" type="hidden"
											id="documentoFormatado"
											value="${comandoNegativacaoForm.map.documentoFormatadoTexto}">
										<input name="enderecoFormatadoCliente" type="hidden"
											id="enderecoFormatadoCliente"
											value="${comandoNegativacaoForm.map.enderecoFormatadoTexto}">
										<input name="emailCliente" type="hidden" id="emailCliente"
											value="${comandoNegativacaoForm.map.emailClienteTexto}">
									</div>

									<div id="aba-pessoa-fisica" aria-labelledby="headingOne"
										data-parent="#aba-pessoa-fisica">
										</br>
										<div class="row justify-content-md-center">
											<h6>
												<i class="fa fa-question-circle"></i> Clique em <b>Pesquisar
													Pessoa</b> para selecionar a Pessoa.
											</h6>
										</div>
										<div class="card-body"
											style="border-bottom: 1px solid #dfdfdf;">
											<div class="form-row mb-1">
												<label class="col-sm-4 text-md-right" id="nomeClienteTextoI"
													for="nomeClienteTexto">Pessoa:</label>
												<div class="col-sm-8">
													<input class="form-control form-control-sm" type="text"
														id="nomeClienteTexto" name="nomeClienteTexto"
														maxlength="50" size="50" disabled="disabled"
														value="${comandoNegativacaoForm.map.nomeCompletoCliente}">
												</div>
											</div>

											<div class="form-row mb-1">
												<label class="col-sm-4 text-md-right"
													id="documentoFormatadoTextoI" for="documentoFormatadoTexto">CPF/CNPJ:</label>
												<div class="col-sm-6">
													<input class="form-control form-control-sm" type="text"
														id="documentoFormatadoTexto"
														name="documentoFormatadoTexto" maxlength="18" size="18"
														disabled="disabled"
														value="${comandoNegativacaoForm.map.documentoFormatadoTexto}">
												</div>
											</div>

											<div class="form-row mb-1">
												<label class="col-sm-4 text-md-right"
													id="enderecoFormatadoTextoI" for="enderecoFormatadoTexto">Endere�o:</label>
												<div class="col-sm-8">
													<textarea class="form-control form-control-sm"
														id="enderecoFormatadoTexto" name="enderecoFormatadoTexto"
														rows="1" cols="37" disabled="disabled">${comandoNegativacaoForm.map.enderecoFormatadoTexto}</textarea>
												</div>
											</div>

											<div class="form-row mb-1">
												<label class="col-sm-4 text-md-right" id="rotuloPassaporte"
													for="emailClienteTexto">E-mail:</label>
												<div class="col-sm-8">
													<input class="form-control form-control-sm" type="text"
														id="emailClienteTexto" name="emailClienteTexto"
														maxlength="80" size="40" disabled="disabled"
														value="${comandoNegativacaoForm.map.emailClienteTexto}">
												</div>
											</div>

											<div class="row justify-content-md-center">
												<input name="Button" id="botaoPesquisarCliente"
													class="btn btn-primary btn-sm botoes"
													title="Pesquisar Cliente" value="Pesquisar Cliente"
													onclick="exibirPopupPesquisaCliente();" type="button">
												<input name="Button" id="botaoAdicionarCliente"
													class="btn btn-primary btn-sm botoes"
													title="Adicionar Cliente" value="Adicionar Cliente"
													onclick="inserirClienteGrid();" type="button">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="gridClientes">
						<jsp:include
							page="/jsp/cobranca/negativacao/gridInserirClientes.jsp"></jsp:include>
					</div>

				</div>
				<!-- FIM FILTRO CLIENTE -->



			</div>
		</div>


		<div class="card-footer">
			<div class="row">
				<div class="col-sm-12">
					<button class="btn btn-danger btn-sm float-left ml-1 mt-1"
						id="botaoCancelar" type="button" onclick="cancelar();">
						<i class="fa fa-times"></i> Cancelar
					</button>
					<vacess:vacess param="incluirCliente">
						<button id="botaoIncluir" value="Incluir"
							class="btn btn-sm btn-primary float-right ml-1 mt-1"
							type="button" onclick="alterar();">
							<i class="fa fa-plus"></i> Alterar
						</button>
					</vacess:vacess>
					<vacess:vacess param="simularComandoNegativacao">
						<button id="botaoSimular" value="Simular"
							class="btn btn-sm btn-primary float-right ml-1 mt-1"
							type="button" onclick="simular();">
							<i class="fa fa-plus"></i> Simular
						</button>
					</vacess:vacess>
				</div>
			</div>
		</div>

		<hr>
</div>
</form:form>
</div>