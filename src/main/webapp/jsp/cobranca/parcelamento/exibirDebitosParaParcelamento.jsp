<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>

<fmt:setLocale value="pt-BR"/>

<h1 class="tituloInterno">Incluir Parcelamento de D�bitos - Negocia��o<a class="linkHelp" href="<help:help>/inclusodoparcelamentodedbitosnegociao.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

	<script type="text/javascript">

	$(document).ready(function(){

		// Datepicker
		$(".campoData").datepicker({
			changeYear: true, showOn: 'button', 
			buttonImage: '<c:url value="/imagens/calendario.gif"/>', 
			buttonImageOnly: true, 
			buttonText: 'Exibir Calend�rio', 
			dateFormat: 'dd/mm/yy', 
			minDate: '+0d'
			}
		);
		
		// Dialog			
		$("#pontoConsumoPopup").dialog({
			autoOpen: false,
			width: 370,
			modal: true,
			minHeight: 90,
			resizable: false
			}
		);

		$("input#cobrancaEmContaSim").click(function () {
			animatedcollapse.hide('divVencimentoNotaDebito');
			document.getElementById('vencimentoInicial').value = "";
			document.getElementById('periodicidadeParcelas').value = "";
		});

		$("input#cobrancaEmContaNao").click(function () { 
			animatedcollapse.show('divVencimentoNotaDebito');
		})

		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#fatura,#creditoDebito").chromatable({
			width: "891px"
		});

		if (stringVazia($("#percentualDesconto").val())) {		
			$("#percentualDesconto").addClass("campoDesabilitado");		
		}
		
		desablitarDataVencimento();
	});
	
	animatedcollapse.addDiv('divVencimentoNotaDebito', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('conteinerDocumentos', 'fade=0,speed=400,persist=0,hide=0');
	animatedcollapse.addDiv('conteinerDebitosCreditosRealizar', 'fade=0,speed=400,persist=0,hide=0');
	
	function desablitarDataVencimento(){
		
		var indicadorDataAtualSim = $('#indicadorDataAtualSim').prop('checked');
		var indicadorDataAtualNao = $('#indicadorDataAtualNao').prop('checked');
		if(indicadorDataAtualSim == true && indicadorDataAtualNao == false){
			
			$("#dataPrimeiroVencimento").removeAttr("disabled");
			$("#dataPrimeiroVencimentoMaisUmMes").val(null);
			$("#dataPrimeiroVencimentoMaisUmMes").attr("disabled","disabled");
		}else{
			
			$("#dataPrimeiroVencimento").val(null);
			$("#dataPrimeiroVencimento").attr("disabled","disabled");
			$("#dataPrimeiroVencimentoMaisUmMes").removeAttr("disabled");
		}
	}
	
	function gerarParcelas() {
		submeter('parcelamentoForm','gerarParcelas');
	}

	function cancelar() {
		manterCheckBoxDasTabelas(false);
		submeter('parcelamentoForm', 'cancelarDebitosParaParcelamento');
	}

	function salvar() {
		submeter('parcelamentoForm','salvarParcelamento');
	}

	function verificarSelecaoFaturas() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesFatura != undefined) {
			var total = form.chavesFatura.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesFatura[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesFatura.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}
	
	function verificarSelecaoCreditoDebitos() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesCreditoDebito != undefined) {
			var total = form.chavesCreditoDebito.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesCreditoDebito[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesCreditoDebito.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				alert ("Selecione um ou mais registros para realizar a opera��o!");
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}

	function exibirDadosPontoConsumo(idPontoConsumo) {
		var descricao = '';
		var endereco = '';
		var cep = '';
		var complemento = '';
		
		if(idPontoConsumo != ""){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, { 
	           	callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					complemento = pontoConsumo['complemento'];
	       	  }
	       	 , async: false}
	       	);

		document.getElementById('descricaoPopup').innerHTML = descricao;
		document.getElementById('enderecoPopup').innerHTML = endereco;
		document.getElementById('cepPopup').innerHTML = cep;
		document.getElementById('complementoPopup').innerHTML = complemento;

		}
		exibirJDialog("#pontoConsumoPopup");
	}

	function adicionarMascaraNumeroDecimal(campo,quantidadeDecimais){
		var numeroComMascara = '';
		var inteiros = obterParteInteira(campo);
		numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
		var decimais = obterParteDecimal(campo);
		if(campo != ''){	
			if(decimais.length > 0 && numeroComMascara.length == 0){
				numeroComMascara = 0;
			}
			numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
			return numeroComMascara;
		}		
	}

	function somarSaldoCorrigido() {
		var somatorioSaldoCorrigido = 0;
		
		var checkboxesSelecionados = $('input[name=chavesFatura]:checked');
		checkboxesSelecionados.each(function() {
			var codigoFatura =$(this).val();
			
			var saldoCorrigido = converterStringParaFloat(
					$('#fatura_saldo_corrigido_' + codigoFatura).text().trim());
			
			somatorioSaldoCorrigido += saldoCorrigido;
			
		});
		
		return somatorioSaldoCorrigido;
	}
	
	function somarSaldoCreditoDebitoRealizar() {
		var somatorioSaldoCreditoDebitoRealizar = 0;
		
		var checkboxesSelecionados = $('input[name=chavesCreditoDebito]:checked');
		checkboxesSelecionados.each(function() {
			var codigoFatura = $(this).val();
			
			var tipoLancamento = 
				$('#credito_debito_realizar_tipo_lancamento_' + codigoFatura).text().trim();
			var saldo = converterStringParaFloat(
				$('#credito_debito_realizar_saldo_' + codigoFatura).text().trim());
			
			if(tipoLancamento == "D�bito"){				
				somatorioSaldoCreditoDebitoRealizar += saldo;
			} else if(tipoLancamento == "Cr�dito"){
				somatorioSaldoCreditoDebitoRealizar -= saldo;
			}
			
		});
		
		return somatorioSaldoCreditoDebitoRealizar;
	}
	
	function atualizarValorTotal() {

		var somatorioSaldoCorrigido = somarSaldoCorrigido();
		var somatorioSaldoCreditoDebitoRealizar = somarSaldoCreditoDebitoRealizar();
		var somatorioDividasSelecionadas = 
			somatorioSaldoCorrigido + somatorioSaldoCreditoDebitoRealizar;

		var percentualJurosPerfilParcelamento = 0;
		var percentualDescontoPerfilParcelamento = 0;
		var idPerfilParcelamento = $('#idPerfilParcelamento').val();
		
		var percentualDesconto = document.getElementById("percentualDesconto");
		var percentualDescontoMaximo = document.getElementById("percentualDescontoMaximo");
		$("#percentualDesconto").attr("disabled","disabled").val(""); //limpar campo desconto
		
		if (idPerfilParcelamento != "-1") {
	      	AjaxService.obterPerfilParcelamento( idPerfilParcelamento, { 
	           	callback: function(perfil) {

	           		var indicadorJuros = perfil["indicadorJuros"];
					if(indicadorJuros == "true"){
						percentualJurosPerfilParcelamento = 
							converterStringParaFloat(perfil["percentualJuros"]);
					}

					var indicadorDesconto = perfil["indicadorDesconto"];
	      			if(indicadorDesconto == "true"){
	      				percentualDesconto.disabled = false;
	      				$("#percentualDesconto").removeClass("campoDesabilitado");
	      				var stringPercentualDescontoPerfilParcelamento = 
	      					perfil["percentualDesconto"];
		      			percentualDesconto.value = 
		      				stringPercentualDescontoPerfilParcelamento;
	      				percentualDescontoPerfilParcelamento = 
							converterStringParaFloat(stringPercentualDescontoPerfilParcelamento);
		      			percentualDescontoMaximo.value = percentualDesconto.value;
		}		
					
				}, async: false}
	       	);
		}
		
		var fatorJurosPerfilParcelamento = 
			(percentualJurosPerfilParcelamento / 100) + 1;
		var dividasSelecionadasJuros = 
			arredondar2Digitos(somatorioDividasSelecionadas * 
					fatorJurosPerfilParcelamento);
		
		var valorTotalDesconto = 
			arredondarParaBaixo2Digitos(dividasSelecionadasJuros * 
					(percentualDescontoPerfilParcelamento / 100));
		var valorTotalLiquidoJurosDesconto = 
			dividasSelecionadasJuros - valorTotalDesconto;
		
		definirValorHidden(
				'valorTotalDebito', 
				'valorTotalAux', 
				somatorioSaldoCorrigido);
// 		$('#valorTotalAux').text(
// 				"R$ " + aplicarLocalePtBrNumero(somatorioSaldoCorrigido));
		
		definirValorHidden(
				'valorDebitoCredito', 
				'valorDebitoCreditoSpan', 
				somatorioDividasSelecionadas);
// 		$('#valorDebitoCreditoSpan').text(
// 				"R$ " + aplicarLocalePtBrNumero(somatorioDividasSelecionadas));
		
		definirValorHidden(
				'valorCorrigidoDebitoCredito', 
				'valorCorrigidoDebitoCreditoSpan', 
				dividasSelecionadasJuros);
// 		$('#valorCorrigidoDebitoCreditoSpan').text(
// 				"R$ " + aplicarLocalePtBrNumero(dividasSelecionadasJuros));
		
		definirValorHidden(
				'valorDesconto', 
				'valorDescontoAux', 
				valorTotalDesconto);
// 		$('#valorDescontoAux').text(
// 				"R$ " + aplicarLocalePtBrNumero(valorTotalDesconto));

		definirValorHidden(
				'valorTotalLiquido', 
				'valorTotalLiquidoAux', 
				valorTotalLiquidoJurosDesconto);
// 		$('#valorTotalLiquidoAux').text(
// 				"R$ " + aplicarLocalePtBrNumero(valorTotalLiquidoJurosDesconto));
		
// 		atualizarBotaoGerarParcelas(somatorioDividasSelecionadas);
		
		}

	function definirValorHidden(id, idAux, valor){
		$('#' + id).val(valor);
		$('#' + idAux).text("R$ " + aplicarLocalePtBrNumero(valor));
	}

	function modificarDesconto(){
		
		var dividasSelecionadasJuros = $("#valorCorrigidoDebitoCredito").val();
		var percentualDescontoPerfilParcelamento = 
			converterStringParaFloat($("#percentualDesconto").val());
		var stringPercentualDescontoMaximoPerfilParcelamento = 
			$("#percentualDescontoMaximo").val();
		var percentualDescontoMaximoPerfilParcelamento = 
			converterStringParaFloat($("#percentualDescontoMaximo").val());
		
		var mostrarErroPercentualMaiorQueMaximo = false;

		if (percentualDescontoPerfilParcelamento > 
				percentualDescontoMaximoPerfilParcelamento) {
			mostrarErroPercentualMaiorQueMaximo = true;
			$("#percentualDesconto").val(stringPercentualDescontoMaximoPerfilParcelamento);
			percentualDescontoPerfilParcelamento =  
						percentualDescontoMaximoPerfilParcelamento;
			}
		
		var valorTotalDesconto = 
			arredondar2Digitos(dividasSelecionadasJuros * 
					(percentualDescontoPerfilParcelamento / 100));
		var valorTotalLiquidoJurosDesconto = 
			dividasSelecionadasJuros - valorTotalDesconto;
		
		definirValorHidden(
				'valorDesconto', 
				'valorDescontoAux', 
				valorTotalDesconto);
// 		$('#valorDescontoAux').text(
// 				"R$ " + aplicarLocalePtBrNumero(valorTotalDesconto));

		definirValorHidden(
				'valorTotalLiquido', 
				'valorTotalLiquidoAux', 
				valorTotalLiquidoJurosDesconto);
		$('#valorTotalLiquidoAux').text(
				"R$ " + aplicarLocalePtBrNumero(valorTotalLiquidoJurosDesconto));
		
		if (mostrarErroPercentualMaiorQueMaximo) {
			alert("O percentual n�o pode ser maior que " + 
					stringPercentualDescontoMaximoPerfilParcelamento + 
					"%, referente ao percentual m�ximo.");
				}
			}

	function atualizarBotaoGerarParcelas(valorTotal) {
		if (valorTotal <= 0) {
			document.getElementById('botaoGerarParcelas').disabled = true;
		} else {
			document.getElementById('botaoGerarParcelas').disabled = false;
		}
	}

	function carregarDescontoMaximo(elem){
		var idPerfilParcelamento = elem.value;
		carregarDescontoMaximoParcelamento(idPerfilParcelamento);
	}

	function prepararDecimalParaCalculo(eleParam){
		var valor = eleParam.replace(",",".");
		return parseFloat(valor);
	}

	function validarNumeroParcelas(elem){
		var numeroParcelasHidden = document.getElementById("numeroParcelasHidden").value;
		var numeroParcelas = document.getElementById("numeroParcelas");
		var valorNumeroParcelas = parseInt(elem.value);
		var numeroParcelasMaxima = parseInt(numeroParcelasHidden);
		if(valorNumeroParcelas > numeroParcelasMaxima){
			alert("O n�mero de parcelas n�o pode ser maior que "+ numeroParcelasMaxima);
			numeroParcelas.value = "";
			return false;
		}
		
	}

// 	function manterValorTotalLiquido(){
// 		var valorTotalLiquido = '<c:out value="${parcelamentoForm.map.valorTotalLiquido}"/>';
// 		if(valorTotalLiquido != undefined && valorTotalLiquido != ''){
// 			document.getElementById('valorTotalLiquidoAux').innerHTML = "R$ " + valorTotalLiquido.replace(".",",");
// 		}
// 	}

	function manterCheckBoxDasTabelas(valor){
		
		<c:forEach items="${parcelamentoVO.chavesFatura}" var="chavePrimariaFatura">
			if(document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>') != undefined){
				document.getElementById('chaveFatura'+'<c:out value="${chavePrimariaFatura}"/>').checked = valor;
			}	
		</c:forEach>
		<c:forEach items="${parcelamentoVO.chavesCreditoDebito}" var="chavePrimariaCreditoDebito">
			if(document.getElementById('chaveCreditoDebito'+'<c:out value="${chavePrimariaCreditoDebito}"/>') != undefined){
				document.getElementById('chaveCreditoDebito'+'<c:out value="${chavePrimariaCreditoDebito}"/>').checked = valor;
			}	
		</c:forEach>
	}
	
	function limparFormulario(){

		var cleanDocumentos = document.getElementsByName('chavesFatura');
		for(var i = 0 ; i < cleanDocumentos.length ; i++){
			if(cleanDocumentos[i] != null && cleanDocumentos[i].checked != false)
				cleanDocumentos[i].checked = false;
		}

		var cleanNotas = document.getElementsByName('chavesCreditoDebito');
		for(var i = 0 ; i < cleanNotas.length ; i++){
			if(cleanNotas[i] != null && cleanNotas[i].checked != false){
				cleanNotas[i].checked = false;
			}	
		}
		
		document.getElementById("idPerfilParcelamento").value = "-1";
		document.getElementById("percentualDesconto").value = "";
		document.getElementById("valorDesconto").value = "";
		document.getElementById("valorDescontoAux").value = "";
		document.getElementById('valorDescontoAux').innerHTML = "R$";
		document.getElementById("numeroParcelas").value = "";
		document.parcelamentoForm.cobrancaEmConta[0].checked = true;
		$(document).ready(function(){
			animatedcollapse.hide('divVencimentoNotaDebito');
		});
		
		document.parcelamentoForm.indicadorNotaPromissoria[1].checked = true;
		document.parcelamentoForm.indicadorConfissaoDivida[1].checked = true;
		document.getElementById('valorTotalAux').innerHTML = "R$ "; 
		document.getElementById('valorTotalLiquidoAux').innerHTML = "R$ ";

		//Limpar dados do Fiador
		document.getElementById('idCliente').value = "";
    	document.getElementById('nomeCompletoCliente').value = "";
    	document.getElementById('documentoFormatado').value = "";
    	document.getElementById('enderecoFormatadoCliente').value = "";
    	document.getElementById('emailCliente').value = "";
    	document.getElementById('nomeClienteTexto').value = "";
    	document.getElementById('documentoFormatadoTexto').value = "";
    	document.getElementById('emailClienteTexto').value = "";
    	document.getElementById('enderecoFormatadoTexto').value = "";
    	document.getElementsByName('chavesFatura').checked = false;

		//Limpa e reorganiza as informa��es dos valores.
		var inputValorTotal = document.getElementById('valorTotalDebito');
		var valorTotalLiquido = document.getElementById("valorTotalLiquido");
		
		var percentualDesconto = document.getElementById("percentualDesconto");
		var inputValorDebitos = document.getElementById('valorDebitos');
		var inputValorCreditos = document.getElementById("valorCreditos");
		var valorDesconto = document.getElementById("valorDesconto");
		
		inputValorTotal.value = "";
		valorTotalLiquido.value = "";
		percentualDesconto.value = "";
		inputValorDebitos.value = "";
		inputValorCreditos.value = "";
		valorDesconto.value = "";	

		document.getElementById('valorTotalLiquidoAux').innerHTML = "R$ ";
	}

	function init(){
		
		var tipoCobrancaEmFatura = '<c:out value="${parcelamentoVO.cobrancaEmConta}"/>';
		if(tipoCobrancaEmFatura == 'true'){
			$(document).ready(function(){
				animatedcollapse.hide('divVencimentoNotaDebito');
			});
		}else{
			$(document).ready(function(){
				animatedcollapse.show('divVencimentoNotaDebito');
			});
		}
		
// 		var percentualDesconto = '<c:out value="${parcelamentoForm.map.percentualDesconto}"/>';
// 		if(percentualDesconto != undefined && percentualDesconto == ''){			
// // 			modificarDescontoParcelamento(percentualDesconto);
// 			document.getElementById("percentualDesconto").disabled = true;
// 			document.getElementById("valorDescontoAux").innerHTML = "R$";
// 		} else {
// 			document.getElementById("percentualDesconto").disabled = false;
// 		}

		manterCheckBoxDasTabelas(true);
// 		manterValorTotalLiquido();
		gerarRelatorio();

// 		var inputValorTotal = '<c:out value="${parcelamentoForm.map.valorTotalDebito}"/>';
// 		if(inputValorTotal != undefined && inputValorTotal != ''){
// 			teste = document.getElementById('valorTotalAux');
// 			teste.innerHTML = "R$ " + inputValorTotal.replace(".",",");
// 		}
		
		//atualizarBotaoGerarParcelas(inputValorTotal.value)
	}

	function gerarRelatorio() {
		<c:if test="${not empty gerarRelatorioConfissaoDividaNotaPromissoria && gerarRelatorioConfissaoDividaNotaPromissoria == true}">
			location.href = '<c:url value="/gerarRelatorioConfissaoDividaNotaPromissoria"/>';
		</c:if>
	}

	function detalharCreditoDebito(idCreditoDebito) {
		document.getElementById('chavePrimaria').value = idCreditoDebito;
		submeter('parcelamentoForm','exibirDetalhamentoCreditoDebito');
	}

	function voltar() {
		submeter('parcelamentoForm','pesquisarPontosConsumoParcelamento');
	}		

	/*$('#checkAllFatura').click(function(){
		/*O seletor de filtro :enabled maraca apenas os checkboxes habilitados.
		Esta altera��o foi feita para atender � tela de Pesquisar Cr�ditos / d�bitos a Realizar.
		$("#fatura input[type='checkbox']:enabled").attr('checked', $('#checkAllFatura').is(':checked'));
	});*/
	
	function selecionarTodosDebitosCreditos(check){		
		$('input[name=chavesCreditoDebito]').attr("checked", check.checked);
		atualizarValorTotal();
		
	}

	function selecionarTodasFaturas(check){
		$('input[name=chavesFatura]').attr("checked", check.checked);
		atualizarValorTotal();
					
			}
						
	addLoadEvent(init);
	
	</script>	

<form method="post" action="exibirDebitosParaParcelamento" id="parcelamentoForm" name="parcelamentoForm">
	<token:token></token:token>
	<input name="acao" type="hidden" id="acao" value="gerarExtratoDebitos"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${parcelamentoVO.idPontoConsumo}">
	<input name="idCliente" type="hidden" id="idCliente" value="${parcelamentoVO.idCliente}">
	<input name="idImovel" type="hidden" id="idImovel" value="${parcelamentoVO.idImovel}">
	<input name="percentualDescontoMaximo" type="hidden" id="percentualDescontoMaximo">
	<input name="numeroParcelasHidden" type="hidden" id="numeroParcelasHidden">
	<input type="hidden" id="valorDebitos" name="valorDebitos" value="${parcelamentoVO.valorDebitos}">
	<input type="hidden" id="valorCreditos" name="valorCreditos" value="${parcelamentoVO.valorCreditos}">
	<input type="hidden" id="valorTaxaJuros" name="valorTaxaJuros">
	<input type="hidden" id="idIndiceFinanceiro" name="idIndiceFinanceiro" value="${parcelamentoVO.idIndiceFinanceiro}">

	<div id="pontoConsumoPopup" title="Ponto de Consumo">
		<label class="rotulo rotuloVertical">Descri��o:</label>
		<span  class="itemDetalhamento" id="descricaoPopup"></span><br />
		<label class="rotulo">Endere�o:</label>
		<span  class="itemDetalhamento" id="enderecoPopup"></span><br />
		<label class="rotulo">Cep:</label>
		<span  class="itemDetalhamento" id="cepPopup"></span><br />
		<label class="rotulo">Complemento:</label>
		<span  class="itemDetalhamento" id="complementoPopup"></span><br /><br />
	</div>
	
	<fieldset id="efetuarParcelamentoDebitos" class="conteinerPesquisarIncluirAlterar">
		<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
			<fieldset class="coluna">
				<label class="rotulo">Descricao:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.descricaoPontoConsumo}"/></span><br />
				<label class="rotulo">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.enderecoPontoConsumo}"/></span><br />
			</fieldset>
			<fieldset class="colunaFinal">
				<label class="rotulo">CEP:</label>
				<span class="itemDetalhamento"><c:out value="${parcelamentoVO.cepPontoConsumo}"/></span><br />
				<label class="rotulo">Complemento:</label>
				<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.complementoPontoConsumo}"/></span><br />
			</fieldset>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<c:if test="${empty (listaFatura)}"><span class="itemDetalhamento"> (N�o foram encontradas Faturas)</span></c:if>
		<c:if test="${not empty (listaFatura)}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[conteinerDocumentos]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Documentos <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="conteinerDocumentos" class="conteinerBloco">
			
			   	<display:table class="dataTableGGAS dataTableCabecalho2Linhas" sort="list" name="listaFatura" id="fatura" excludedParams="" requestURI="#">
		   			
		   			<display:column style="text-align: center;" sortable="false" title="<input type='checkbox' onclick='selecionarTodasFaturas(this);'/>">
				   		<input type="checkbox" name="chavesFatura" id="chaveFatura${fatura.chavePrimaria}" value="${fatura.chavePrimaria}" onclick="atualizarValorTotal()" />
				   	</display:column>
					
					<display:column sortable="true" property="chavePrimaria" title="N� do Documento"/>
			   		<display:column sortable="true" title="Data de<br />Emiss�o">
			   			<fmt:formatDate value="${fatura.dataEmissao}" pattern="dd/MM/yyyy" />
			   		</display:column>
			   		<display:column sortable="true" title="Vencimento">
			   			<fmt:formatDate value="${fatura.dataVencimento}" pattern="dd/MM/yyyy" />
			   		</display:column>
			   		<display:column sortable="false" title="Valor (R$)" style="width: 150px" >
			   			<fmt:formatNumber value="${fatura.valorOriginal}" minFractionDigits="2" maxFractionDigits="2"/>
			   		</display:column>
			   		<display:column sortable="false" title="Saldo (R$)" style="width: 150px" >
			   			<fmt:formatNumber value="${fatura.valorSaldo}" minFractionDigits="2" maxFractionDigits="2"/>
			   		</display:column>
			   		<display:column sortable="false" title="Saldo Corrigido (R$)" style="width: 150px" >
			   			<span id="fatura_saldo_corrigido_${fatura.chavePrimaria}">
			   			<fmt:formatNumber value="${fatura.valorTotal}" minFractionDigits="2" maxFractionDigits="2"/>
			   			</span>
			   		</display:column>
			   		<display:column property="tipoDocumento" sortable="true" title="Tipo de<br />Documento" style="width: 100px" />
			   		<display:column property="situacaoPagamento" sortable="true" title="Situa��o de<br />Pagamento"/>
			   		<c:if test="${parcelamentoVO.idCliente ne null && parcelamentoVO.idCliente > 0}">
				   		<display:column title="Ponto de<br />Consumo">
							<a href="javascript:exibirDadosPontoConsumo('<c:out value="${fatura.idPontoConsumo}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>						
						</display:column>	
					</c:if>   	
				</display:table>		
			</fieldset>
		</c:if>
		
		<hr class="linhaSeparadora1" />
		
		<c:if test="${empty (listaCreditoDebito)}"><span class="itemDetalhamento"> (N�o foram encontrados D�bitos e Cr�ditos a Realizar)</span></c:if>
		<c:if test="${not empty (listaCreditoDebito)}">
			<a class="linkExibirDetalhes" href="#" rel="toggle[conteinerDebitosCreditosRealizar]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">D�bitos e Cr�ditos a Realizar <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="conteinerDebitosCreditosRealizar" class="conteinerBloco">
			
		   		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" id="creditoDebito" excludedParams="" requestURI="#">
										
					<display:column style="width: 25px" sortable="false" title="<input type='checkbox' onclick='selecionarTodosDebitosCreditos(this);'/>"> 
			      		<input type="checkbox" name="chavesCreditoDebito" id="chaveCreditoDebito${creditoDebito.chavePrimaria}" value="${creditoDebito.chavePrimaria}" 
			      			onclick="javascript:atualizarValorTotal(this, '<c:out value="${creditoDebito.valorEntrada}"/>', '<c:out value="${creditoDebito.valorEntrada}"/>','<c:out value="${creditoDebito.creditoOrigem}"/>', '',2)"
			      			<c:if test="${creditoDebito.indicadorCancelamentoBloqueado}"> disabled="disabled" </c:if> />
			     	</display:column>
			     	
					<display:column title="Tipo de<br/>Lan�amento" style="width: 75px">
						<span id="credito_debito_realizar_tipo_lancamento_${creditoDebito.chavePrimaria}">
						<c:choose>
							<c:when test="${creditoDebito.creditoOrigem eq null}">D�bito</c:when>
							<c:otherwise>Cr�dito</c:otherwise>
						</c:choose>
						</span>
					</display:column>
					<display:column title="Rubrica" style="width: 200px">
						<c:out value="${creditoDebito.rubrica}"/>
					</display:column>    
					<display:column title="Cobrado/<br/>Total" style="width: 55px">
		            	<c:out value="${creditoDebito.numeroPrestacaoCobrada}"/>/<c:out value="${creditoDebito.quantidadeTotalPrestacoes}"/>
					</display:column>    
					<display:column title="Valor Total (R$)" style="width: 100px; text-align: right">
						<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2"/>
					</display:column>
					<!-- valorEntrada est� sendo usado como saldo devedor aqui na tela. -->
					<display:column title="Saldo (R$)" style="width: 100px; text-align: right">
						<span id="credito_debito_realizar_saldo_${creditoDebito.chavePrimaria}">
						<fmt:formatNumber value="${creditoDebito.valorEntrada}" minFractionDigits="2"/>
						</span>
					</display:column>
					<display:column title="Situa��o" style="width: 100px">
						<c:out value="${creditoDebito.situacao}"/>
					</display:column>
			   	</display:table>
			</fieldset>
		</c:if>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="dadosCondicoesNegociacao"  class="conteinerBloco">
			<legend class="conteinerBlocoTitulo">Condi��es da Negocia��o:</legend>			
			<fieldset class="coluna">
				<label class="rotulo campoObrigatorio" for="idPerfilParcelamento"><span class="campoObrigatorioSimbolo">* </span>Perfil de parcelamento:</label>
				<select name="idPerfilParcelamento" id="idPerfilParcelamento" class="campoSelect" onchange="atualizarValorTotal()">
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaPerfilParcelamento}" var="perfil">
						<option value="<c:out value="${perfil.chavePrimaria}"/>" <c:if test="${parcelamentoVO.idPerfilParcelamento == perfil.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${perfil.descricao}"/>
						</option>		
					</c:forEach>	
			    </select>
<!-- 			    <br class="quebraLinha2"/> -->
			    
				<label class="rotulo campoObrigatorio" for="valorTotalDebito" 
					style="margin-top: 0px;">Valor Total Negociado:</label>
				<input type="hidden" class="campoTexto" id="valorTotalDebito" 
					name="valorTotalDebito" value="${parcelamentoVO.valorTotalDebito}" >
				<span class="itemDetalhamento" id="valorTotalAux">
<%-- 					R$ <c:out value="${parcelamentoForm.map.valorTotalDebito}"/> --%>
					R$ <fmt:formatNumber value="${parcelamentoVO.valorTotalDebito}" 
						minFractionDigits="2" maxFractionDigits="2"/>
				</span>
<!-- 				<br class="quebraLinha" /> -->
				
				<label class="rotulo rotulo2Linhas campoObrigatorio">
					Valor Total com Cr�dito/D�bito:</label>
				<input type="hidden" class="campoTexto" id="valorDebitoCredito" 
					name="valorDebitoCredito" value="${parcelamentoVO.valorDebitoCredito}" >
				<span class="itemDetalhamento" id="valorDebitoCreditoSpan">
<%-- 					R$ <c:out value="${parcelamentoForm.map.valorDebitoCredito}"/> --%>
					R$ <fmt:formatNumber value="${parcelamentoVO.valorDebitoCredito}" 
						minFractionDigits="2" maxFractionDigits="2"/>
				</span>
<!-- 				<br class="quebraLinha" /> -->

				<label class="rotulo campoObrigatorio" 
					style="margin-top: 0px;">Valor Total com Juros:</label>
				<input type="hidden" class="campoTexto" id="valorCorrigidoDebitoCredito" 
					name="valorCorrigidoDebitoCredito" value="${parcelamentoVO.valorCorrigidoDebitoCredito}" >
				<span class="itemDetalhamento" id="valorCorrigidoDebitoCreditoSpan">
<%-- 					R$ <c:out value="${parcelamentoForm.map.valorCorrigidoDebitoCredito}"/> --%>
					R$ <fmt:formatNumber value="${parcelamentoVO.valorCorrigidoDebitoCredito}" 
						minFractionDigits="2" maxFractionDigits="2"/>
				</span>
<!-- 				<br class="quebraLinha" /> -->
				
				<label class="rotulo" for="percentualDesconto">Percentual M�ximo:</label>
				<input class="campoTexto campoHorizontal" type="text" id="percentualDesconto" name="percentualDesconto" maxlength="6" size="5" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimal(event,this,3,2);" onchange="modificarDesconto(this)" value="${parcelamentoVO.percentualDesconto}">
				<label class="rotuloHorizontal rotuloInformativo" for="percentualDesconto">%</label><br class="quebraLinha" />
				
				<label class="rotulo" for="valorDesconto">Desconto:</label>
				<input type="hidden" class="campoTexto" id="valorDesconto" 
					name="valorDesconto" value="${parcelamentoVO.valorDesconto}">
				<span class="itemDetalhamento" id="valorDescontoAux">
					R$ 
<%-- 					<c:out value="${parcelamentoForm.map.valorDesconto}"/> --%>
					<fmt:formatNumber value="${parcelamentoVO.valorDesconto}" 
						minFractionDigits="2" maxFractionDigits="2"/>
				</span>
<!-- 				<br class="quebraLinha" /> -->
				
				<label class="rotulo campoObrigatorio" 
					for="valorTotalLiquido">Valor Total L�quido:</label>
				<input type="hidden" class="campoTexto" id="valorTotalLiquido" 
					name="valorTotalLiquido" value="${parcelamentoVO.valorTotalLiquido}">
				<span class="itemDetalhamento" id="valorTotalLiquidoAux" 
					style="padding-top: 1px;">
					R$ 
<%-- 					<c:out value="${parcelamentoForm.map.valorTotalLiquido}"/> --%>
					<fmt:formatNumber value="${parcelamentoVO.valorTotalLiquido}" 
						minFractionDigits="2" maxFractionDigits="2"/>
				</span>
<!-- 				<br class="quebraLinha" /> -->
				
				<label class="rotulo campoObrigatorio" for="numeroParcelas"><span class="campoObrigatorioSimbolo">* </span>N�mero de Parcelas:</label>
				<input class="campoTexto" type="text" id="numeroParcelas" name="numeroParcelas" maxlength="3" size="2"  onkeypress="return formatarCampoInteiro(event,2);" onchange="validarNumeroParcelas(this);" value="${parcelamentoVO.numeroParcelas}"><br />
				
				<label class="rotulo" >Tipo de Cobran�a:</label>
				
				<c:choose>
					<c:when test="${parcelamentoVO.bloquearCobrancaFatura}">
						<input class="campoRadio" type="radio" name="cobrancaEmConta" id="cobrancaEmContaSim" value="true" disabled="disabled">
					</c:when>
					<c:otherwise>
						<input class="campoRadio" type="radio" name="cobrancaEmConta" id="cobrancaEmContaSim" value="true" <c:if test="${parcelamentoVO.cobrancaEmConta eq 'true'}">checked="checked"</c:if>>
					</c:otherwise>
				</c:choose>
				
				<label class="rotuloRadio" for="cobrancaEmContaSim">Cobran�a em D�bito a Cobrar</label><br class="quebraLinha2" />
				<input class="campoRadio" type="radio" name="cobrancaEmConta" id="cobrancaEmContaNao" value="false" <c:if test="${parcelamentoVO.cobrancaEmConta eq 'false'}">checked="checked"</c:if>>
				<label class="rotuloRadio" id="labelCobrancaEmContaNao" for="cobrancaEmContaNao">Cobran�a em Notas de D�bito</label><br class="quebraLinha2" />
				
				<div id="divVencimentoNotaDebito">
					<label class="rotulo rotulo2Linhas" id="labelDataAtual">Vencimento Nota de D�bito: </label>
		
					<input class="campoRadio" type="radio" name="vencimentoInicial" id="indicadorDataAtualSim" value="true" onclick="desablitarDataVencimento();" <c:if test="${parcelamentoVO.vencimentoInicial eq 'true' || parcelamentoVO.vencimentoInicial eq ''}">checked="checked"</c:if>>
					<label class="rotuloRadio" id="labelDataAtual">Data: </label>
					<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataPrimeiroVencimento" name="dataPrimeiroVencimento" maxlength="10" value="${parcelamentoVO.dataPrimeiroVencimento}">
					</br></br>
					<input class="campoRadio" type="radio" name="vencimentoInicial" id="indicadorDataAtualNao" value="false" onclick="desablitarDataVencimento();" <c:if test="${parcelamentoVO.vencimentoInicial eq 'false'}">checked="checked"</c:if>>
					<label class="rotuloRadio" id="labelDataAtualMes">Data: </label>
					<input class="campoData campo2Linhas campoHorizontal" type="text" id="dataPrimeiroVencimentoMaisUmMes" name="dataPrimeiroVencimento" maxlength="10" value="${parcelamentoVO.dataPrimeiroVencimento}">
					<label class="rotuloRadio" id="labelDataAtualMes">+ 1 M�s</label></br>
					
				</div>
				
				<label class="rotulo" id="labelEmitirNotaPromissoria">Emitir Nota Promiss�ria:</label>
				<input class="campoRadio" type="radio" name="indicadorNotaPromissoria" id="indicadorNotaPromissoriaSim" value="true" <c:if test="${parcelamentoVO.indicadorNotaPromissoria eq 'true'}">checked="checked"</c:if>>
				<label class="rotuloRadio" id="labelIndicadorNotaPromissoriaSim" for="indicadorNotaPromissoriaSim">Sim</label>
				<input class="campoRadio" type="radio" name="indicadorNotaPromissoria" id="indicadorNotaPromissoriaNao" value="false" <c:if test="${parcelamentoVO.indicadorNotaPromissoria eq 'false'}">checked="checked"</c:if>>
				<label class="rotuloRadio" id="labelIndicadorNotaPromissoriaNao" for="indicadorNotaPromissoriaNao">N�o</label><br class="quebraRadio" />
				<label class="rotulo rotulo2Linhas" >Emitir Termo de<br />Confiss�o de D�vida:</label>
				<input class="campoRadio campoRadio2Linhas" type="radio" name="indicadorConfissaoDivida" id="indicadorConfissaoDividaSim" value="true" <c:if test="${parcelamentoVO.indicadorConfissaoDivida eq 'true'}">checked="checked"</c:if>>
				<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorConfissaoDividaSim">Sim</label>
				<input class="campoRadio campoRadio2Linhas" type="radio" name="indicadorConfissaoDivida" id="indicadorConfissaoDividaNao" value="false" <c:if test="${parcelamentoVO.indicadorConfissaoDivida eq 'false'}">checked="checked"</c:if>>
				<label class="rotuloRadio rotuloRadio2Linhas" for="indicadorConfissaoDividaNao">N�o</label><br class="quebraRadio" />
			</fieldset>
			
			<fieldset id="pesquisarCliente" class="colunaDir">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${parcelamentoVO.idCliente}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
					<jsp:param name="nomeCliente" value="${parcelamentoVO.nomeCompletoCliente}"/>
					<jsp:param name="nomeFiador" value="${parcelamentoVO.nomeCompletoCliente}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
					<jsp:param name="documentoFormatadoCliente" value="${parcelamentoVO.documentoFormatado}"/>
					<jsp:param name="documentoFiador" value="${parcelamentoVO.documentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${parcelamentoVO.emailCliente}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${parcelamentoVO.enderecoFormatadoCliente}"/>
					<jsp:param name="funcionalidade" value=""/>
					<jsp:param name="possuiRadio" value="true"/>
					<jsp:param name="nomeComponente" value="Pesquisar Fiador"/>
				</jsp:include>
			</fieldset>
			
			<fieldset id="conteinerBotoesGerarParcelas" class="conteinerBotoesPesquisarDirFixo">
				<input class="bottonRightCol2" type="button" id="botaoGerarParcelas"  value="Gerar Parcelas" onclick="gerarParcelas();">
				<input class="bottonRightCol bottonRightColUltimo" type="button" value="Limpar" onclick="limparFormulario();">
			</fieldset>		
		</fieldset>		
	</fieldset>
	
	<c:if test="${dadosGerais ne null}">
		<br/>
   		<display:table class="dataTableGGAS" name="dadosGerais.listaDadosParcelas" id="parcela" excludedParams="" >
	   		<display:column title="Parcelas">
	            <c:out value="${parcela.numeroParcela}"/>        
	        </display:column>
	   		<display:column title="Saldo Inicial (R$)" >
	   			<fmt:formatNumber value="${parcela.saldoInicial}" minFractionDigits="2" maxFractionDigits="2"/>
	   		</display:column>
<%-- 	   		<display:column title="Juros (R$)" > --%>
<%-- 	   			<fmt:formatNumber value="${parcela.juros}" minFractionDigits="2" maxFractionDigits="2"/> --%>
<%-- 	   		</display:column> --%>
<%-- 	   		<display:column title="Amortiza��o (R$)"> --%>
<%-- 	   			<fmt:formatNumber value="${parcela.amortizacao}" minFractionDigits="2" maxFractionDigits="2"/> --%>
<%-- 	   		</display:column> --%>
	   		<display:column title="Valor da Parcela (R$)">
	   			<fmt:formatNumber value="${parcela.total}" minFractionDigits="2" maxFractionDigits="2"/>
	   		</display:column>
			
	   			<display:column title="Data de Vencimento">
		   			<fmt:formatDate value="${parcela.dataVencimento}" pattern="dd/MM/yyyy" />
		   		</display:column>	   			
	   		
	   	</display:table>

		<fieldset id="conteinerBotoesPesquisarDirPesquisarExtratoDebito" class="conteinerBotoes">
			<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
			<input class="bottonRightCol" type="button" value="Reiniciar Negocia��o" onclick="cancelar();">
			<input id="botaoSalvar" class="bottonRightCol2 botaoGrande1" type="button" value="Salvar" onclick="salvar();">
		</fieldset>
	</c:if>
	
	<c:if test="${dadosGerais eq null}">
		<fieldset class="conteinerBotoes"> 	
	    	<input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
		</fieldset>
	</c:if>
					
</form>