<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<h1 class="tituloInterno">Incluir Parcelamento de D�bitos - Selecionar Pontos de Consumo<a class="linkHelp" href="<help:help>/consultandoparcelamentodedbitos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para incluir o Parcelamento de D�bitos selecione o cliente e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir os Pontos de Consumo do Cliente.<br /></p>

<form method="post" action="pesquisarParcelamentoDebitos" name="parcelamentoForm" id="parcelamentoForm">

<script type="text/javascript">
	
	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});

		if (document.getElementById('idCliente').value != "" || document.getElementById('idImovel').value != "") {
			$("input#botaoPesquisar").removeAttr("disabled");
		} else {
			$("input#botaoPesquisar").attr("disabled","disabled");
		}

		//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
		//Estado Inicial desabilitado
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		//Dispara o evento no click do radiobutton.
		$("#indicadorPesquisaCliente").click(habilitaCliente);
		$("#indicadorPesquisaImovel").click(habilitaImovel);
	});

	//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
	function habilitaCliente(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado")
	};
	function habilitaImovel(){
		$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
		$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado")
	};

	function desabilitarPesquisaOposta(elem){
		var selecao = getCheckedValue(elem);
		if(selecao == "indicadorPesquisaImovel"){
			pesquisarImovel(true);
			pesquisarCliente(false);
			limparCamposPesquisa();
		}else{
			pesquisarImovel(false);
			pesquisarCliente(true);
			limparCamposPesquisa();
		}	
	}

	function pesquisarImovel(valor){
		document.getElementById('botaoPesquisarImovel').disabled = !valor;
	}
	function pesquisarCliente(valor){
		document.getElementById('botaoPesquisarCliente').disabled = !valor;
	}
	
	function limparCamposPesquisa(){
		document.getElementById('idCliente').value = "";
		document.getElementById('nomeCompletoCliente').value = "";
		document.getElementById('documentoFormatado').value = "";
		document.getElementById('enderecoFormatadoCliente').value = "";
		document.getElementById('emailCliente').value = "";
		
		document.getElementById('nomeClienteTexto').value = "";
		document.getElementById('documentoFormatadoTexto').value = "";
		document.getElementById('enderecoFormatadoTexto').value = "";
		document.getElementById('emailClienteTexto').value = "";
		
		document.getElementById('idImovel').value = "";
		document.getElementById('nomeFantasiaImovel').value = "";
		document.getElementById('matriculaImovel').value = "";
		document.getElementById('numeroImovel').value = "";
		document.getElementById('cidadeImovel').value = "";
		document.getElementById('condominio').value = "";
		
		document.getElementById('nomeImovelTexto').value = "";
		document.getElementById('matriculaImovelTexto').value = "";
		document.getElementById('numeroImovelTexto').value = "";
		document.getElementById('cidadeImovelTexto').value = "";
		document.getElementById('indicadorCondominioImovelTexto1').checked = false;
		document.getElementById('indicadorCondominioImovelTexto2').checked = false;

		$("input#botaoPesquisar").attr("disabled","disabled");
	}

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}

	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	$("input#botaoPesquisar").removeAttr("disabled");
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
			indicadorCondominio.value = "";
			$("input#botaoPesquisar").attr("disabled","disabled");
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}
	}

	function recarregaDadosCliente(idCliente) {
		$("input#botaoPesquisar").removeAttr("disabled");
	}	

	function pesquisar() {
		submeter('parcelamentoForm', 'pesquisarPontosConsumoParcelamento');
	}

	function exibirExtratoDebitosPontoConsumo(idPonto, idCliente) {
		document.getElementById('idPontoConsumo').value = idPonto;
		document.getElementById('idCliente').value = idCliente;
		submeter('parcelamentoForm', 'exibirDebitosParaParcelamento');
	}

	function init() {
		<c:choose>
			<c:when test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
				pesquisarImovel(false);
				habilitaCliente();
				if('${parcelamentoVO.idCliente}' != ''){
					selecionarCliente('${parcelamentoVO.idCliente}');
				}
			</c:when>
			<c:when test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
				pesquisarCliente(false);
				habilitaImovel();
				if('${parcelamentoVO.idImovel}' != ''){
					selecionarImovel('${parcelamentoVO.idImovel}');
				}
			</c:when>			
			<c:otherwise>
				pesquisarCliente(false);
				pesquisarImovel(false);
			</c:otherwise>				
		</c:choose>	
	}	
	
	addLoadEvent(init);	

	function voltar() {
		submeter("parcelamentoForm",  "exibirPesquisaParcelamentoDebitos");
	}		
	
	</script>	

	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value=""/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="efetuarParcelamentoDebitos" class="conteinerPesquisarIncluirAlterar">
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${parcelamentoVO.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${parcelamentoVO.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${parcelamentoVO.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${parcelamentoVO.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${parcelamentoVO.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="recarregaDadosCliente"/>
			</jsp:include>
		</fieldset>
	
		<fieldset id="pesquisarImovel" class="colunaDir">
			<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${parcelamentoVO.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${parcelamentoVO.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${parcelamentoVO.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${parcelamentoVO.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${parcelamentoVO.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${parcelamentoVO.condominio}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${parcelamentoVO.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${parcelamentoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${parcelamentoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input id="botaoPesquisar" class="bottonRightCol2" type="button" value="Pesquisar" onclick="pesquisar();" />
			<input class="bottonRightCol bottonRightColUltimo" value="Limpar" type="button" onclick="limparCamposPesquisa();">
		</fieldset>
	</fieldset>
	
	<c:if test="${listaContratoPontoConsumos ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS"
			name="listaContratoPontoConsumos" id="contratoPontoConsumo" decorator="br.com.ggas.web.cobranca.parcelamento.decorator.ContratoPontoConsumoResultadoPesquisaDecorator"  
			sort="list" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPontosConsumoParcelamento">
		     
		    <display:column property="contratoNumeroFormatadoComLock" sortable="true" sortProperty="contrato.numero" title="Contrato" style="width: 120px"/>
		    <display:column property="imovelPontoConsumoComLock" sortable="true" title="Im�vel - Ponto de Consumo" headerClass="tituloTabelaEsq" style="text-align: left"/>
		    <display:column property="pontoConsumoHabilitadoComLock" sortable="false" title="Situa��o" style="width: 100px"/>
		 	<display:column property="segmentoDescricaoComLock" sortable="false" title="Segmento" style="width: 100px"/>
	    	<display:column property="ramoAtividadeDescricaoComLock" sortable="false" title="Ramo de Atua��o" style="width: 150px"/>
	    	
		</display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes"> 	
	    <input name="Button" class="bottonRightCol" value="Cancelar" type="button" onclick="voltar();">
	</fieldset>	
</form> 