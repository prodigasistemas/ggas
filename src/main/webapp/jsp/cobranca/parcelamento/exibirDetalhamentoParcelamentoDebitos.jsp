
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Detalhar Parcelamento<a class="linkHelp" href="<help:help>/detalharparcelamentodosdbitos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<form method="post" action="exibirDetalhamentoParcelamentoDebitos" id="parcelamentoForm" name="parcelamentoForm"> 

<script>

	function voltar() {
		submeter("parcelamentoForm",  "exibirPesquisaParcelamentoDebitos");
	}

	function autorizar(chave){
		$("#chavePrimaria").val(chave);
		submeter("parcelamentoForm", "autorizarParcelamento");
	}

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');

</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${parcelamentoVO.chavePrimaria}"/>
<input name="versao" type="hidden" id="versao" value="${parcelamentoVO.versao}">
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset id="dadosParcelamento" class="detalhamento">
	
	<!-- Autoriza��o de registro pendente -->
	<c:if test="${parcelamentoVO.hasPermissaoAutorizar ne null && parcelamentoVO.hasPermissaoAutorizar}">
		<fieldset>
			<label class="rotulo" id="rotuloStatusCreditoDebito" for="status">O Parcelamento est� pendente de an�lise. Deseja autoriz�-lo?</label>
			<input class="campoRadio" type="radio" name="status" id="statusCreditoDebitoAutorizado" tabindex="30" value="${statusAutorizado}">
			<label class="rotuloRadio" for="statusCreditoDebitoAutorizado">Autorizar</label>
			<input class="campoRadio" type="radio" name="status" id="statusCreditoDebitoNaoAutorizado" tabindex="31" value="${statusNaoAutorizado}">
			<label class="rotuloRadio" for="statusCreditoDebitoNaoAutorizado">N�o Autorizar</label>			
		
			<input name="button" class="bottonRightCol2 botaoGrande1" value="Ok" type="button" onclick="autorizar(<c:out value='${parcelamentoVO.chavePrimaria}'/>);" <c:if test="${parcelamentoVO.hasPermissaoAutorizar ne true}">disabled</c:if> >
		</fieldset>
	</c:if>
	
	<c:choose>
		<c:when test="${parcelamentoVO.idCliente ne null && parcelamentoVO.idCliente > 0}">
			<input type="hidden" id="idCliente" name="idCliente" value="${parcelamentoVO.idCliente }" />
			<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosCliente" class="conteinerDadosDetalhe">
				<fieldset class="coluna">
					<label class="rotulo">Nome:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.nomeCompletoCliente}"/></span><br />
					<label class="rotulo">CPF/CNPJ:</label>
					<span class="itemDetalhamento"><c:out value="${parcelamentoVO.documentoFormatado}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.enderecoFormatadoCliente}"/></span><br />
					<label class="rotulo">E-mail:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.emailCliente}"/></span><br />
				</fieldset>
			</fieldset>
		</c:when>
		<c:otherwise>
			<input type="hidden" id="idImovel" name="idImovel" value="${parcelamentoVO.idImovel}" />
			<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
			<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
				<fieldset class="coluna">
					<label class="rotulo">Descricao:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.descricaoPontoConsumo}"/></span><br />
					<label class="rotulo">Endere�o:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.enderecoPontoConsumo}"/></span><br />
				</fieldset>
				<fieldset class="colunaFinal">
					<label class="rotulo">CEP:</label>
					<span class="itemDetalhamento"><c:out value="${parcelamentoVO.cepPontoConsumo}"/></span><br />
					<label class="rotulo">Complemento:</label>
					<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${parcelamentoVO.complementoPontoConsumo}"/></span><br />
				</fieldset>
			</fieldset>
		</c:otherwise>
	</c:choose>
	
	<hr class="linhaSeparadoraDetalhamento" />
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Documentos:</legend>
		<div class="conteinerTabela">
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaFatura" sort="list" id="fatura" pagesize="15" excludedParams="" requestURI="#" >
			<display:column style="width: 105px" property="chavePrimaria" sortable="true" title="N� do<br />Documento" />
	   		<display:column style="width: 65px" property="dataEmissaoFormatada" sortable="true" title="Data de<br />Emiss�o" />
	   		<display:column style="width: 65px" property="dataVencimentoFormatada" sortable="true" title="Vencimento" />
	   		<display:column style="width: 120px" property="valorTotalFormatado" sortable="true" title="Valor (R$)" />
	   		<display:column style="width: 130px" sortable="true" title="Saldo (R$)" >
	   			<fmt:formatNumber value="${fatura.valorConciliado}" minFractionDigits="2" type="currency"/>
	   		</display:column>
	   		<display:column style="width: 150px" property="tipoDocumento.descricao" sortable="true" title="Tipo de<br />Documento" />
	   		<display:column style="width: 70px" property="situacaoPagamento.descricao" sortable="true" title="Situa��o de/<br />Pagamento" />
	   	</display:table>
	   	</div>
	</fieldset>
	<hr class="linhaSeparadoraDetalhamento" />
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">D�bitos a Cobrar:</legend>
		<div class="conteinerTabela">
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" sort="list" id="creditoDebito" pagesize="15" excludedParams="" requestURI="#" >
			<display:column title="Tipo de<br/>Lan�amento" style="width: 75px">
				<c:choose>
					<c:when test="${creditoDebito.creditoOrigem eq null}">D�bito</c:when>
					<c:otherwise>Cr�dito</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Rubrica" style="width: 200px">
				<c:out value="${creditoDebito.rubrica}"/>
			</display:column>    
			<display:column title="Cobrado/<br/>Total" style="width: 55px">
            	<c:out value="${creditoDebito.numeroPrestacaoCobrada}"/>/<c:out value="${creditoDebito.quantidadeTotalPrestacoes}"/>
			</display:column>    
			<display:column title="Valor Total (R$)" style="width: 100px; text-align: right">
				<fmt:formatNumber value="${creditoDebito.valor}" minFractionDigits="2"/>
			</display:column>
			<!-- valorEntrada est� sendo usado como saldo devedor aqui na tela. -->
			<display:column title="Saldo (R$)" style="width: 100px; text-align: right">
				<fmt:formatNumber value="${creditoDebito.valorEntrada}" minFractionDigits="2"/>
			</display:column>
			<display:column title="Situa��o" style="width: 100px">
				<c:out value="${creditoDebito.situacao}"/>
			</display:column>	   		
	   	</display:table>
	   	</div>
	</fieldset>
	<hr class="linhaSeparadoraDetalhamento" />
	<fieldset id="dadosCondicoesNegociacao" class="conteinerBloco">
		<fieldset class="coluna detalhamentoColunaLarga">
			<legend class="conteinerBlocoTitulo">Condi��es da Negociacao</legend>
			<label class="rotulo" >Perfil de Parcelamento:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${parcelamentoVO.descricaoPerfilParcelamento}"/></span><br />
			<label class="rotulo" >Valor Total do D�bito:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (parcelamentoVO.valorTotalDebito)}">R$ </c:if><c:out value="${parcelamentoVO.valorTotalDebito}"/></span><br />
			<label class="rotulo">Desconto M�ximo:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (parcelamentoVO.valorDesconto)}">R$ </c:if><c:out value="${parcelamentoVO.valorDesconto}"/></span><br />
			<label class="rotulo">Valor Total L�quido:</label>
			<span class="itemDetalhamento"><c:if test="${not empty (parcelamentoVO.valorTotalLiquido)}">R$ </c:if><c:out value="${parcelamentoVO.valorTotalLiquido}"/></span><br />
			<label class="rotulo">N�mero de Parcelas:</label>
			<span class="itemDetalhamento"><c:out value="${parcelamentoVO.numeroParcelas}"/></span><br />
			<label class="rotulo">Tipo de Cobran�a:</label>
			<c:if test="${parcelamentoVO.cobrancaEmConta eq 'true'}">
				<span class="itemDetalhamento"><c:out value="Cobran�a em D�bito a Cobrar"/></span><br />
			</c:if>
			<c:if test="${parcelamentoVO.cobrancaEmConta eq 'false'}">
				<span class="itemDetalhamento"><c:out value="Cobran�a em Notas de D�bitos"/></span>
			<br class="quebraLinha2" />
			<div id="conteinerTipoCobranca" class="conteinerDados">
				<label class="rotulo">Vencimento Inicial:</label>
				<span class="itemDetalhamento"><c:out value="${parcelamentoVO.vencimentoInicial}"/></span><br />
				<label class="rotulo">Periodicidade das Parcelas:</label>
				<span class="itemDetalhamento"><c:out value="${parcelamentoVO.periodicidadeParcelas}"/></span><br />
			</div>
			</c:if>
			<label class="rotulo">Emitir Nota Promiss�ria:</label>
			<c:if test="${parcelamentoVO.indicadorNotaPromissoria eq 'true'}">
				<span class="itemDetalhamento"><c:out value="Sim"/></span>
			</c:if>
			<c:if test="${parcelamentoVO.indicadorNotaPromissoria eq 'false'}">
				<span class="itemDetalhamento"><c:out value="N�o"/></span>
			</c:if>
			<br />
			<label class="rotulo rotulo2Linhas">Emitir Termo de Confiss�o de D�vida:</label>
			<c:if test="${parcelamentoVO.indicadorConfissaoDivida eq 'true'}">
				<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="Sim"/></span>
			</c:if>
			<c:if test="${parcelamentoVO.indicadorConfissaoDivida eq 'false'}">
				<span class="itemDetalhamento itemDetalhamento2Linhas"><c:out value="N�o"/></span>
			</c:if>
		</fieldset>
		<fieldset id="pesquisarCliente" class="colunaFinal">
			<legend class="conteinerBlocoTitulo">Fiador</legend>
			<div class="conteinerDados">											
				<label class="rotulo" id="rotuloCliente">Cliente:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${parcelamentoVO.nomeFiador}"/></span><br />
				<label class="rotulo" id="rotuloCnpjTexto">CPF/CNPJ:</label>
				<span class="itemDetalhamento"><c:out value="${parcelamentoVO.documentoFiador}"/></span><br />
				<label class="rotulo" id="rotuloEnderecoTexto">Endere�o:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${parcelamentoVO.enderecoFiador}"/></span><br />
				<label class="rotulo" id="rotuloEnderecoTexto">E-mail:</label>
				<span class="itemDetalhamento itemDetalhamentoMedio"><c:out value="${parcelamentoVO.emailFiador}"/></span><br />
			</div>
		</fieldset>
	</fieldset>
	<hr class="linhaSeparadoraDetalhamento"/>
	<fieldset class="conteinerBloco">
		<legend class="conteinerBlocoTitulo">Parcelamento:</legend>
		<div class="conteinerTabela">
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaParcelamentoItem" sort="list" id="parcela" pagesize="15" excludedParams="" requestURI="#" >
	   		<display:column style="width: 70px" sortable="true" title="Parcela">
	   			<c:out value="${parcela.numeroParcela}"></c:out>
	   		</display:column>
	   		<display:column style="width: 150px" sortable="tue" title="Valor (R$)" >
	   			<c:out value="${parcela.valor}"></c:out>
	   		</display:column>
	   		
	   		<c:if test="${mostrarColunaDataVencimento eq true}">
		   		<display:column style="width: 150px" sortable="true" title="Data Vencimento">	   
					<c:out value="${parcela.dataVencimento}"></c:out>
				</display:column>
			</c:if>
						
	   	</display:table>
	   	</div>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">    
</fieldset>

</form> 