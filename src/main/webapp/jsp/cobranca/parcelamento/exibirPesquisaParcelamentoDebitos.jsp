<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Parcelamento de D�bitos<a class="linkHelp" href="<help:help>/consultandoparcelamentodedbitos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe as datas do <span class="destaqueOrientacaoInicial">Per�odo de Realiza��o do Parcelamento</span> e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>.
Para pesquisar parcelamentos de um Cliente ou Im�vel no per�odo informado, clique em <span class="destaqueOrientacaoInicial">Pesquisar por Cliente ou Im�vel</span>.
Para efetuar um parcelamento de d�bitos clique em <span class="destaqueOrientacaoInicial">Incluir.</span></p>

<form method="post" action="pesquisarParcelamentoDebitos" id="parcelamentoForm" name="parcelamentoForm">

	<script type="text/javascript">
		
		$(document).ready(function(){
		   	
			$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', onClose: function() { this.focus()} });	
	
			//-- Habilita ou Desabilita os r�tulos das pesquisas de Cliente e Im�vel --//
			//Estado Inicial desabilitado
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto,#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
			//Dispara o evento no click do radiobutton.
			$("#indicadorPesquisaCliente").click(habilitaCliente);
			$("#indicadorPesquisaImovel").click(habilitaImovel);
			//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
			

			//Habilita ou desabilita o bot�o Pesquisar se as datas do Per�odo de Realiza��o do Parcelamento tenham sido escolhidas
			$("input#dataParcelamentoInicial,input#dataParcelamentoFinal").blur(function() {
				var dataParcelamentoInicial = $("input#dataParcelamentoInicial").val();
				var dataParcelamentoFinal = $("input#dataParcelamentoFinal").val();
				if(dataParcelamentoInicial != "" && dataParcelamentoInicial != "__/__/____" && dataParcelamentoFinal != "" && dataParcelamentoFinal != "__/__/____"){
					$("input#botaoPesquisar").removeAttr("disabled");
				} else {
					$("input#botaoPesquisar").attr("disabled","disabled");
				}
			});

			$("input#dataParcelamentoFinal").focus(function() {
				var dataParcelamentoInicial = $("input#dataParcelamentoInicial").val();
				var dataParcelamentoFinal = $("input#dataParcelamentoFinal").val();
				if(dataParcelamentoInicial != "" && dataParcelamentoInicial != "__/__/____" && dataParcelamentoFinal != "" && dataParcelamentoFinal != "__/__/____"){
					$("input#botaoPesquisar").removeAttr("disabled");
				} else {
					$("input#botaoPesquisar").attr("disabled","disabled");
				}
			});

		});

		function habilitarBotaoPesquisar(){
			var dataParcelamentoInicial = $("input#dataParcelamentoInicial").val();
			var dataParcelamentoFinal = $("input#dataParcelamentoFinal").val();
			if(dataParcelamentoInicial != "" || dataParcelamentoFinal != ""){
				$("input#botaoPesquisar").removeAttr("disabled");
			} else {
				$("input#botaoPesquisar").attr("disabled","disabled");
			}			
		}

		animatedcollapse.addDiv('pesquisarParcelamentoDebitosConteinerInferior', 'fade=0,speed=400,persist=1,hide=1');
		
	
		function desabilitarPesquisaOposta(elem){
			var selecao = getCheckedValue(elem);
			if(selecao == "indicadorPesquisaImovel"){
				pesquisarImovel(true);
				pesquisarCliente(false);
				limparCamposPesquisa();
			} else{
				pesquisarImovel(false);
				pesquisarCliente(true);
				limparCamposPesquisa();
			}	
		}

		//Fun��es para adicionar ou remover classes que d�o aos r�tulos a apar�ncia de desabilitar/habilitar.
		function habilitaCliente(){
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").addClass("rotuloDesabilitado");
			$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").removeClass("rotuloDesabilitado");
		};
		function habilitaImovel(){
			$("#rotuloNomeFantasia,#rotuloMatriculaTexto,#rotuloNumeroTexto,#rotuloCidadeTexto,#rotuloindicadorCondominioAmbosTexto").removeClass("rotuloDesabilitado");
			$("#rotuloCliente,#rotuloCnpjTexto,#rotuloEnderecoTexto,#rotuloEmailClienteTexto").addClass("rotuloDesabilitado");
		};
	
		function pesquisarImovel(valor){
			document.getElementById('botaoPesquisarImovel').disabled = !valor;
		}
		function pesquisarCliente(valor){
			document.getElementById('botaoPesquisarCliente').disabled = !valor;
		}
		
		function limparCamposPesquisa(){
			document.getElementById('idCliente').value = "";
			document.getElementById('nomeCompletoCliente').value = "";
			document.getElementById('documentoFormatado').value = "";
			document.getElementById('enderecoFormatadoCliente').value = "";
			document.getElementById('emailCliente').value = "";
			
			document.getElementById('nomeClienteTexto').value = "";
			document.getElementById('documentoFormatadoTexto').value = "";
			document.getElementById('enderecoFormatadoTexto').value = "";
			document.getElementById('emailClienteTexto').value = "";
			
			document.getElementById('idImovel').value = "";
			document.getElementById('nomeFantasiaImovel').value = "";
			document.getElementById('matriculaImovel').value = "";
			document.getElementById('numeroImovel').value = "";
			document.getElementById('cidadeImovel').value = "";
			document.getElementById('condominio').value = "";
			
			document.getElementById('nomeImovelTexto').value = "";
			document.getElementById('matriculaImovelTexto').value = "";
			document.getElementById('numeroImovelTexto').value = "";
			document.getElementById('cidadeImovelTexto').value = "";
			document.getElementById('indicadorCondominioImovelTexto1').checked = false;
			document.getElementById('indicadorCondominioImovelTexto2').checked = false;
		}

		function limparFormulario() {
			document.getElementById('dataParcelamentoInicial').value = "";
			document.getElementById('dataParcelamentoFinal').value = "";
			$("input#botaoPesquisar").attr("disabled","disabled");
			limparCamposPesquisa();
		}

		function imprimirNotasPromissorias(idParcelamento) {
			document.forms[0].chavePrimaria.value = idParcelamento;
			submeter("0", "imprimirNotasPromissorias");
		}

		function imprimirConfissaoDividas(idParcelamento) {
			document.forms[0].chavePrimaria.value = idParcelamento;
			submeter("0", "imprimirConfissaoDividas");
		}
	
		function exibirPopupPesquisaImovel() {
			popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
		}
	
		function selecionarImovel(idSelecionado){
			var idImovel = document.getElementById("idImovel");
			var matriculaImovel = document.getElementById("matriculaImovel");
			var nomeFantasia = document.getElementById("nomeFantasiaImovel");
			var numeroImovel = document.getElementById("numeroImovel");
			var cidadeImovel = document.getElementById("cidadeImovel");
			var indicadorCondominio = document.getElementById("condominio");
			
			if(idSelecionado != '') {				
				AjaxService.obterImovelPorChave( idSelecionado, {
		           	callback: function(imovel) {	           		
		           		if(imovel != null){  	           			        		      		         		
			               	idImovel.value = imovel["chavePrimaria"];
			               	matriculaImovel.value = imovel["chavePrimaria"];		               	
			               	nomeFantasia.value = imovel["nomeFantasia"];
			               	numeroImovel.value = imovel["numeroImovel"];
			               	cidadeImovel.value = imovel["cidadeImovel"];
			               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	}
		        	}, async:false}
		        	
		        );	        
	        } else {
	       		idImovel.value = "";
	        	matriculaImovel.value = "";
	        	nomeFantasia.value = "";
				numeroImovel.value = "";
	              	cidadeImovel.value = "";
	              	indicadorCondominio.value = "";
	       	}
		
			document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
			document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
			document.getElementById("numeroImovelTexto").value = numeroImovel.value;
			document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
			if(indicadorCondominio.value == 'true') {
				document.getElementById("indicadorCondominioImovelTexto1").checked = true;
			} else {
				document.getElementById("indicadorCondominioImovelTexto2").checked = true;
			}
		}
	
		function pesquisar() {
			submeter('0', 'pesquisarParcelamentoDebitos');
		}
	
		function exibirInclusaoParcelamento(){
			submeter('0', 'exibirPesquisaEfetuarParcelamentoDebitos');
		}

		
	
		function init() {
			<c:choose>
				<c:when test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">
					pesquisarImovel(false);
					habilitaCliente();
				</c:when>
				<c:when test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">
					pesquisarCliente(false);
					habilitaImovel();
				</c:when>			
				<c:otherwise>
					pesquisarCliente(false);
					pesquisarImovel(false);
				</c:otherwise>				
			</c:choose>	

			gerarRelatorio();
			habilitarBotaoPesquisar();
			
		}	

		function detalharParcelamento(idParcelamento) {
			document.forms[0].chavePrimaria.value = idParcelamento;
			submeter("parcelamentoForm", "exibirDetalhamentoParcelamentoDebitos");
		}
		
		function gerarRelatorio() {
			<c:if test="${not empty gerarRelatorioConfissaoDividaNotaPromissoria && gerarRelatorioConfissaoDividaNotaPromissoria == true}">
				location.href = '<c:url value="/gerarRelatorioConfissaoDividaNotaPromissoria"/>';
			</c:if>
		}

		addLoadEvent(init);	

		
	</script>

	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" />
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="">
	<input name="postBack" type="hidden" id="postBack" value="true">
	
	<fieldset id="conteinerPesquisarParcelamentoDebitos" class="conteinerPesquisarIncluirAlterar">
		
		<fieldset class="conteinerBloco">
			<label class="rotulo campoObrigatorio" for="dataParcelamentoInicial"><span class="campoObrigatorioSimbolo">* </span>Per�odo de Realiza��o do Parcelamento:</label>
			<input class="campoData campoHorizontal" type="text" id="dataParcelamentoInicial" name="dataParcelamentoInicial" maxlength="10" value="${parcelamentoVO.dataParcelamentoInicial}">
			<label class="rotuloEntreCampos" for="dataParcelamentoFinal">at�:</label>
			<input class="campoData campoHorizontal" type="text" id="dataParcelamentoFinal" name="dataParcelamentoFinal" maxlength="10" value="${parcelamentoVO.dataParcelamentoFinal}">
		</fieldset>
		
		<fieldset>
			<label class="rotulo rotuloHorizontal" for="status">Al�ada:</label>
			<input class="campoRadio" type="radio" name="status" id="statusAutorizado" value="${statusAutorizado}" <c:if test="${parcelamentoVO.status eq statusAutorizado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitadoAtivo">Autorizado</label>
			<input class="campoRadio" type="radio" name="status" id="statusPendente" value="${statusPendente}" <c:if test="${parcelamentoVO.status eq statusPendente}">checked</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">Pendente</label>
			<input class="campoRadio" type="radio" name="status" id="statusNaoAutorizado" value="${statusNaoAutorizado}" <c:if test="${parcelamentoVO.status eq statusNaoAutorizado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">N�o Autorizado</label>
			<input class="campoRadio" type="radio" name="status" id="status" value="0" <c:if test="${parcelamentoVO.status eq null or parcelamentoVO.status eq 0}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br />
		</fieldset>	
		
		<a class="linkExibirDetalhesSeguinte" href="#" rel="toggle[pesquisarParcelamentoDebitosConteinerInferior]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Pesquisar por Cliente ou Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="pesquisarParcelamentoDebitosConteinerInferior" class="conteinerBloco">
			<fieldset id="pesquisarCliente" class="colunaEsq">
				<input class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaCliente" id="indicadorPesquisaCliente" <c:if test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaCliente'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
					<jsp:param name="idCampoIdCliente" value="idCliente"/>
					<jsp:param name="idCliente" value="${parcelamentoVO.idCliente}"/>
					<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
					<jsp:param name="nomeCliente" value="${parcelamentoVO.nomeCompletoCliente}"/>
					<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
					<jsp:param name="documentoFormatadoCliente" value="${parcelamentoVO.documentoFormatado}"/>
					<jsp:param name="idCampoEmail" value="emailCliente"/>
					<jsp:param name="emailCliente" value="${parcelamentoVO.emailCliente}"/>
					<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
					<jsp:param name="enderecoFormatadoCliente" value="${parcelamentoVO.enderecoFormatadoCliente}"/>
					<jsp:param name="funcionalidade" value="parcelamento"/>
					<jsp:param name="possuiRadio" value="true"/>
				</jsp:include>
			</fieldset>
		
			<fieldset id="pesquisarImovel" class="colunaDir">
				<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" name="indicadorPesquisa" value="indicadorPesquisaImovel" <c:if test="${parcelamentoVO.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
				<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
				<div class="pesquisarImovelFundo">
					<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
					<input name="idImovel" type="hidden" id="idImovel" value="${parcelamentoVO.idImovel}">
					<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${parcelamentoVO.nomeFantasiaImovel}">
					<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${parcelamentoVO.matriculaImovel}">
					<input name="numeroImovel" type="hidden" id="numeroImovel" value="${parcelamentoVO.numeroImovel}">
					<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${parcelamentoVO.cidadeImovel}">
					<input name="condominio" type="hidden" id="condominio" value="${parcelamentoVO.condominio}">
	
					<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
					<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
					<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${parcelamentoVO.nomeFantasiaImovel}"><br />
					<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
					<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.matriculaImovel}"><br />
					<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
					<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.numeroImovel}"><br />
					<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
					<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${parcelamentoVO.cidadeImovel}"><br />
					<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${parcelamentoVO.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
					<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${parcelamentoVO.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
				</div>
			</fieldset>
		</fieldset>
		
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<vacess:vacess param="pesquisarParcelamentoDebitos">
				<input id="botaoPesquisar" class="bottonRightCol2" type="button" value="Pesquisar" onclick="pesquisar();"/>
			</vacess:vacess>		
			<input type="button" class="bottonRightCol bottonRightColUltimo" value="Limpar"  onclick="limparFormulario();">
		</fieldset>
	</fieldset>
	
		<c:if test="${listaParcelamento ne null}">	
			<hr class="linhaSeparadoraPesquisa" />
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaParcelamento" sort="list" id="parcelamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="30" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarParcelamentoDebitos">   		
				<display:column sortable="true" sortProperty="cliente.nome" title="Cliente" headerClass="tituloTabelaEsq" style="text-align:left">
					<a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.cliente.nome}"/>
		            </a>
		        </display:column>
				<display:column sortable="true" sortProperty="pontoConsumo" title="Ponto de Consumo" headerClass="tituloTabelaEsq" class="quebraLinhaTexto conteudoTabelaEsq">
					<a style="width: 190px" href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.pontoConsumo.descricao}"/>
		            </a>
		        </display:column>
		        <display:column sortable="true" sortProperty="dataParcelamento" title="Data do<br />Parcelamento" style="width: 95px;">
					<a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<fmt:formatDate value="${parcelamento.dataParcelamento}" pattern="dd/MM/yyyy"/>
		            </a>
		        </display:column>
		    	<display:column sortable="true" sortProperty="numeroPrestacoes" title="N� de<br />Parcelas" style="width: 65px">
					<a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.numeroPrestacoes}"/>
		            </a>
		        </display:column>
		        <display:column sortable="false" title="Total<br />Parcelado (R$)" style="width: 90px; text-align: right">
					<a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.valorParcelado}"/>
		            </a>
		        </display:column>
		        <display:column sortable="false" title="Entrada (R$)" style="width: 80px; text-align: right">
					<a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.valorEntradaFormatado}"/>
		            </a>
		        </display:column>
		        <display:column sortable="false" title="Status" headerClass="tituloTabelaEsq" style="text-align: left">
		            <a href="javascript:detalharParcelamento(<c:out value='${parcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
		            	<c:out value="${parcelamento.status.descricao}"/> 
		            </a>
		        </display:column>
				<display:column style="width: 70px" title="Notas Promiss�rias" >
		        	<a href='javascript:imprimirNotasPromissorias(<c:out value='${parcelamento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			    	<c:if test="${parcelamento.emissaoNotasPromissorias}">
			    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir Notas Promiss�rias" title="Imprimir Notas Promiss�rias" border="0">
			    	</c:if>
			    	</a>
		        </display:column>
		       
		        <display:column style="width: 70px" title="Confiss�o de D�vida" >
		        	<a href='javascript:imprimirConfissaoDividas(<c:out value='${parcelamento.chavePrimaria}'/>);'><span class="linkInvisivel"></span>
			    	  <c:if test="${parcelamento.emissaoTermoParcelamento}">
			    		<img src="<c:url value="/imagens/icone_impressora.png" />" alt="Imprimir Confiss�o de D�vidas" title="Imprimir Confiss�o de D�vidas" border="0">
			    	 </c:if>
			    	</a>
		        </display:column>
			   
		   	</display:table>		   	
		</c:if>
	
	
		<c:if test="${true}">
			<fieldset class="conteinerBotoes">
				<vacess:vacess param="exibirPesquisaEfetuarParcelamentoDebitos">
					<input type="button" value="Incluir" id="botaoIncluir" class="bottonRightCol2 botaoGrande1" onclick="exibirInclusaoParcelamento();" >
				</vacess:vacess>
			</fieldset>
		</c:if>	
</form> 
