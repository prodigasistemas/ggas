<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Controle de Entrega de Documento em Lote<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para aletar os dados, informe os dados que deseja alterar e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.

<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	});
	
	function limpar(form) {
		limparFormularios(form);
		form.habilitado[0].checked = true;
	}
	
	function cancelar(form) {
		location.href = '<c:url value="/exibirPesquisarEntregaDocumento"/>';
	}
	
	function salvar() {
		submeter('entregaDocumentoForm', 'alterarEntregaDocumentoEmLote');
	}
	
</script>

<form:form method="post" action="exibirAlterarEntregaDocumentoEmLote" name="entregaDocumentoForm" id="entregaDocumentoForm">
	<input name="acao" type="hidden" id="acao" value="alterarEntregaDocumentoEmLote">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${entregaDocumento.chavePrimaria}">
		
	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisaControleDocumentoCol1" class="coluna">
			
			<label class="rotulo" id="rotuloSituacaoEntrega" for="situacaoEntrega"><span class="campoObrigatorioSimbolo">* </span>Situa��o da Entrega:</label> 
			<select name="situacaoEntrega" id="situacaoEntrega" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSituacaoEntrega}" var="situacaoEntrega">
				<option value="<c:out value="${situacaoEntrega.chavePrimaria}"/>"
					<c:if test="${entregaDocumento.situacaoEntrega.chavePrimaria == situacaoEntrega.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${situacaoEntrega.descricao}" />
				</option>
				</c:forEach>
			</select><br class="quebraLinha"/>	
			<label class="rotulo" id="rotuloDataTentativa" for="dataSituacao" ><span class="campoObrigatorioSimbolo">* </span>Data da Situa��o:</label>
			<input class="campoData campoHorizontal" type="text" id="dataSituacao" name="dataSituacao" maxlength="10" value="${entregaDocumento.dataSituacao}">
			<br class="quebraLinha"/><br class="quebraLinha"/>
			<label class="rotulo" id="rotuloMotivo" for="motivo">Motivo da N�o Entrega:</label> 
			<select name="motivoNaoEntrega" id="motivoNaoEntrega" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMotivoNaoEntrega}" var="motivoNaoEntrega">
				<option value="<c:out value="${motivoNaoEntrega.chavePrimaria}"/>"
					<c:if test="${entregaDocumento.motivoNaoEntrega.chavePrimaria == motivoNaoEntrega.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${motivoNaoEntrega.descricao}" />
				</option>
				</c:forEach>
			</select>									
				
		</fieldset>
		
		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios.</p>
		
		<c:if test="${listaEntregaDocumento ne null}">
		
			<hr class="linhaSeparadora1" />
		
			<fieldset class="conteinerBloco">

				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaEntregaDocumento" sort="list" id="documento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEntregaDocumento">
			        
			        <display:column media="html" sortable="false" class="selectedRowColumn" title="">
		      			<input type="hidden" name="chavesPrimarias" id="chavesPrimarias" value="${documento.chavePrimaria}">
					</display:column>
							
					<display:column title="Ativo" style="width: 30px">
						<c:choose>
							<c:when test="${documento.habilitado == true}">
								<img alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
					</display:column>

					<display:column style="text-align: center;" title="Cliente" sortable="true" sortProperty="cliente.nome">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${documento.cliente.nome}"/>
			            </a>
		 			</display:column>

					<display:column style="text-align: center;" title="CPF/CNPJ do Cliente">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			        	   	<c:choose>
					    		<c:when test="${documento.cliente.cpf eq null || documento.cliente.cpf eq ''}">
							    	<c:out value='${documento.cliente.cnpjFormatado}'/>
					    		</c:when>
					    		<c:otherwise>
					    			<c:out value='${documento.cliente.cpfFormatado}'/>
					    		</c:otherwise>
					    	</c:choose>
			            </a>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Tipo de Documento" sortable="true" sortProperty="tipoDocumento.descricao">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${documento.tipoDocumento.descricao}"/>
			            </a>
		 			</display:column>

		 			<display:column style="text-align: center;" title="Data do Vencimento" sortable="true" sortProperty="documentoCobranca.dataVencimento" >
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			        		<c:if test="${documento.documentoCobranca.chavePrimaria ne null && documento.documentoCobranca.dataVencimento ne ''}">
			        			<fmt:formatDate value="${documento.documentoCobranca.dataVencimento}" pattern="dd/MM/yyyy"/>
			        		</c:if>
			            </a>
		 			</display:column>
		 			
					<display:column style="text-align: center;" title="Situa��o da Entrega" sortable="true" sortProperty="situacaoEntrega.descricao">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${documento.situacaoEntrega.descricao}"/>
			            </a>
		 			</display:column>				
					
					<display:column style="text-align: center;" title="Data da Situa��o" sortable="true" sortProperty="dataSituacao" >
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<fmt:formatDate value="${documento.dataSituacao}" pattern="dd/MM/yyyy"/>
			            </a>
		 			</display:column>

			    </display:table>
			    
			</fieldset>
			
		</c:if>
			
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonAlterar" value="Cancelar" class="bottonRightCol2" id="botaoAlterar" onclick="cancelar();" type="button">
		<input name="buttonRemover" value="Limpar" class="bottonRightCol2 bottonLeftColUltimo" onclick="limpar(this.form);" type="button">
		<input name="buttonIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="salvar();" type="button">
	</fieldset>
	
</form:form>
