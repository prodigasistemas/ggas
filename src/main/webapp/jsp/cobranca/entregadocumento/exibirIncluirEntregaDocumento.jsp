<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Incluir Controle de Entrega de Documento<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.

<script type="text/javascript">

	$(document).ready(function(){
		var idCliente = $("#cliente").val();
		var idImovel = $("#imovel").val();
		if(idCliente!=""){
			selecionarCliente(idCliente);
		}else if (idImovel!=""){
			selecionarImovel(idImovel);
		}		
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		document.forms[0].habilitado[0].checked = true;
	});

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function limpar(form) {
		location.href = '<c:url value="/exibirIncluirEntregaDocumento"/>';
	}
	
	function cancelar(form) {
		location.href = '<c:url value="/exibirPesquisarEntregaDocumento"/>';
	}
	
	function salvar() {
		$("#cliente").val($("#idCliente").val());
		$("#imovel").val($("#idImovel").val());		
		submeter('entregaDocumentoForm', 'incluirEntregaDocumento');
	}
	
	function consultaDocumentoCobranca(){
		$("#imovel").val($("#idImovel").val());
		$("#cliente").val($("#idCliente").val());
		
		var idImovel = document.getElementById("idImovel").value;
		var idCliente = document.getElementById("idCliente").value;
		var tipoDocumento = document.getElementById("tipoDocumento").value;
		if ((idImovel!="" || idCliente!="") && tipoDocumento>0) {
			submeter('entregaDocumentoForm', 'pesquisarDocumentoCobranca');
		}
		selecionarImovel($("#imovel").val());
	}
	
	function selecionarImovel(idSelecionado){
		
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}		
	}
	
	function init() {
		consultaDocumentoCobranca();
	}
	
</script>

<form:form method="post" action="incluirEntregaDocumento" id="entregaDocumentoForm" name="entregaDocumentoForm" modelAttribute="EntregaDocumentoImpl">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${entregaDocumento.chavePrimaria}">
	<input name="cliente" type="hidden" id="cliente" value="<c:if test="${entregaDocumento.cliente ne null}"><c:out value="${entregaDocumento.cliente.chavePrimaria}"/></c:if>"/>
	<input name="imovel" type="hidden" id="imovel" value="${imovel}"/>

	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cobranca/entregadocumento/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${entregaDocumentoForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${entregaDocumentoForm.map.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${entregaDocumentoForm.map.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${entregaDocumentoForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${entregaDocumentoForm.map.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend class="legendIndicadorPesquisa"><span class="campoObrigatorioSimbolo2">* </span>Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${entregaDocumentoForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${entregaDocumentoForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${entregaDocumentoForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${entregaDocumentoForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${entregaDocumentoForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${entregaDocumentoForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${entregaDocumentoForm.map.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${entregaDocumentoForm.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${entregaDocumentoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${entregaDocumentoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<p class="legenda"><span class="campoObrigatorioSimbolo2">* </span>a identifica��o do Cliente � obrigat�ria, pelos dados do Cliente ou pelos dados do Im�vel.</p>
		
		<fieldset class="conteinerBloco" id="conteinerPesquisarFaturaInferior">

			<fieldset id="pesquisaControleDocumentoCol1" class="coluna" style="margin:20px 0 0 0\9;">
				<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento"><span class="campoObrigatorioSimbolo">* </span>Tipo de Documento:</label> 
				<select name="tipoDocumento" id="tipoDocumento" class="campoSelect campo2Linhas" onChange="consultaDocumentoCobranca()">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoDocumento}" var="tipoDocumento">
					<option value="<c:out value="${tipoDocumento.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.tipoDocumento.chavePrimaria eq tipoDocumento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoDocumento.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>
				<label class="rotulo" id="rotuloSituacaoEntrega" for="situacaoEntrega"><span class="campoObrigatorioSimbolo">* </span>Situa��o da Entrega:</label> 
				<select name="situacaoEntrega" id="situacaoEntrega" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoEntrega}" var="situacaoEntrega">
					<option value="<c:out value="${situacaoEntrega.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.situacaoEntrega.chavePrimaria eq situacaoEntrega.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${situacaoEntrega.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>			
			</fieldset>
			
			<fieldset id="pesquisaControleDocumentoCol2" class="colunaFinal" style="margin:20px 0 0 0\9;">
			<label class="rotulo" id="rotuloDataTentativa" for="dataSituacao" ><span class="campoObrigatorioSimbolo">* </span>Data da Situa��o:</label>
				<input class="campoData campoHorizontal" type="text" id="dataSituacao" name="dataSituacao" maxlength="10" value="<fmt:formatDate value="${entregaDocumento.dataSituacao}" pattern="dd/MM/yyyy" />" ><br class="quebraLinha"/>		
				<label class="rotulo" id="rotuloMotivo" for="motivo">Motivo da N�o Entrega:</label> 
				<select name="motivoNaoEntrega" id="motivoNaoEntrega" class="campoSelect campo2Linhas" style="margin:10px 0 0 0\9;">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMotivoNaoEntrega}" var="motivoNaoEntrega">
					<option value="<c:out value="${motivoNaoEntrega.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.motivoNaoEntrega.chavePrimaria eq motivoNaoEntrega.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${motivoNaoEntrega.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>
			</fieldset>
			
		</fieldset>

		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios.</p>
		
		<hr class="linhaSeparadora1" />
	
		<fieldset class="conteinerBloco">
		
			<c:if test="${listaDocumentoCobranca ne null}">
			
				<legend class="conteinerBlocoTitulo">Documento de Cobran�a</legend>
	
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaDocumentoCobranca" sort="list" id="documento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" requestURI="pesquisarDocumentoCobranca">
			        
			        <display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
		      			<input type="checkbox" name="documentos" id="documentos" value="${documento.documentoCobranca.chavePrimaria}" <c:if test="${entregaDocumento.documentoCobranca.chavePrimaria eq documento.chavePrimaria}">checked</c:if>>
					</display:column>
					
					<display:column style="text-align: center;" title="Ponto Consumo">
               		<c:out value="${documento.pontoConsumo.descricao}" /> 
		 			</display:column>	
		 			
					<display:column style="text-align: center;" title="Data da Emiss�o">
					<fmt:formatDate value="${documento.documentoCobranca.dataEmissao}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		
					<display:column style="text-align: center;" title="Valor Total">
						<fmt:formatNumber value="${documento.documentoCobranca.valorTotal}" minFractionDigits="2" type="currency"/>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Data do Vencimento">
		            	<fmt:formatDate value="${documento.documentoCobranca.dataVencimento}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		 			
		 			<display:column style="text-align: center;" title="Situa��o">
		 				<c:out value='${documento.documentoCobranca.cobrancaDebitoSituacao.descricao}'/>
		 			</display:column>
		 			
			    </display:table>
			    
		    </c:if>
		    
		    <c:if test="${listaAvisoCorte ne null}">
				
				<legend class="conteinerBlocoTitulo"><span class="campoObrigatorioSimbolo">* </span>Notifica��o/Aviso de Corte</legend>
	
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaAvisoCorte" sort="list" id="documento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" requestURI="pesquisarDocumentoCobranca">
			        
			        <display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
		      			<input type="checkbox" name="documentos" id="documentos" value="${documento.chavePrimaria}" <c:if test="${entregaDocumento.documentoCobranca.chavePrimaria eq documento.chavePrimaria}">checked</c:if>>
					</display:column>
							
					<display:column style="text-align: center;" title="Ponto Consumo">
               			<c:out value="${documento.pontoConsumo.descricao}" /> 
		 			</display:column>
		 					
					<display:column style="text-align: center;" title="Data da Emiss�o">
					<fmt:formatDate value="${documento.dataNotificacao}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		
					<display:column style="text-align: center;" title="Valor Total em Atraso">
						<fmt:formatNumber value="${documento.fatura.valorTotal}" minFractionDigits="2" type="currency"/>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Data do Vencimento">
		            	<fmt:formatDate value="${documento.fatura.dataVencimento}" pattern="dd/MM/yyyy"/>
		 			</display:column>

						<c:if test="${documento.numeroTitulo ne null}">
							<display:column style="text-align: center;"
								title="Numero do Titulo">
								<c:out value="${documento.numeroTitulo}"/>
							</display:column>
						</c:if>

					</display:table>
			    
		    </c:if>
		    
		</fieldset>
			
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonAlterar" value="Cancelar" class="bottonRightCol2" id="botaoAlterar" onclick="cancelar();" type="button">
		<input name="buttonRemover" value="Limpar" class="bottonRightCol2 bottonLeftColUltimo" onclick="limpar();" type="button">
		<input name="buttonIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="salvar();" type="button">
	</fieldset>
	
</form:form>
