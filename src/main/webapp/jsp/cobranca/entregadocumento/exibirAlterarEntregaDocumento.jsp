<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Alterar Controle de Entrega de Documento<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para aletar os dados, informe os dados que deseja alterar e clique em <span class="destaqueOrientacaoInicial">Salvar</span>.

<script type="text/javascript">

	$(document).ready(function(){
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		var idCliente = $("#cliente").val();
		var idImovel = $("#imovel").val();
		if(idCliente!=""){
			selecionarCliente(idCliente);
		}else{
			selecionarImovel(idImovel);
		}
	});
	
	function limpar(form) {
		limparFormularios(form);
		form.habilitado[0].checked = true;
	}
	
	function cancelar(form) {
		location.href = '<c:url value="/exibirPesquisarEntregaDocumento"/>';
	}

	function salvar() {
		submeter('entregaDocumentoForm', 'alterarEntregaDocumento');
	}
	
</script>

<form:form method="post" action="alterarEntregaDocumento" id="entregaDocumentoForm" name="entregaDocumentoForm" modelAttribute="EntregaDocumentoImpl">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${entregaDocumento.chavePrimaria}">
	<input name="cliente" type="hidden" id="cliente" value="<c:if test="${entregaDocumento.cliente ne null}"></c:if><c:out value="${entregaDocumento.cliente.chavePrimaria}"/>"/>
	<input name="avisoCorte" type="hidden" id="avisoCorte" value="<c:if test="${entregaDocumento.avisoCorte ne null}"></c:if><c:out value="${entregaDocumento.avisoCorte.chavePrimaria}"/>"/>
	<input name="dataEmissao" type="hidden" id="dataEmissao" value="<fmt:formatDate value="${entregaDocumento.dataEmissao}" pattern="dd/MM/yyyy"/>"/>
	
	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisaControleDocumentoClienteCol1" class="coluna">
		
			<legend class="conteinerBlocoTitulo">Cliente</legend>
		
			<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">Cliente:</label>
			<span class="itemDetalhamento">
			<c:out value="${entregaDocumento.cliente.nome}"/></span><br/>
			<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">CPF/CNPJ:</label>
				<span class="itemDetalhamento">
				<c:if test="${entregaDocumento.cliente.cpf eq null || entregaDocumento.cliente.cpf eq ''}">
		    	<c:out value='${entregaDocumento.cliente.cnpjFormatado}'/></c:if>
			<c:if test="${entregaDocumento.cliente.cnpj eq null || entregaDocumento.cliente.cnpj eq ''}">
		    	<c:out value='${entregaDocumento.cliente.cpfFormatado}'/></c:if>			    	
    		</span><br/>
			<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">Endere�o:</label>
			<span class="itemDetalhamento">
			<c:out value="${entregaDocumento.cliente.enderecoPrincipal.enderecoFormatado}"/></span><br/>
			<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">E-mail:</label>
			<span class="itemDetalhamento">
			<c:out value="${entregaDocumento.cliente.emailPrincipal}"/></span><br/>												
				
		</fieldset>
		
		<fieldset id="pesquisaControleDocumentoCol2" class="colunaFinal">
		
			<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">Tipo de Documento:</label>
			<input name="tipoDocumento" id="tipoDocumento" type="hidden" value="${entregaDocumento.tipoDocumento.chavePrimaria}">
			<c:if test="${entregaDocumento.tipoDocumento ne null}">
				<span class="itemDetalhamento">
				<c:out value="${entregaDocumento.tipoDocumento.descricao}"/></span><br class="quebraLinha"/>
			</c:if>
			<label class="rotulo" id="rotuloSituacaoEntrega" for="situacaoEntrega"><span class="campoObrigatorioSimbolo">* </span>Situa��o da Entrega:</label> 
			<select name="situacaoEntrega" id="situacaoEntrega" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaSituacaoEntrega}" var="situacaoEntrega">
				<option value="<c:out value="${situacaoEntrega.chavePrimaria}"/>"
					<c:if test="${entregaDocumento.situacaoEntrega ne null and situacaoEntrega.chavePrimaria eq entregaDocumento.situacaoEntrega.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${situacaoEntrega.descricao}" />
				</option>
				</c:forEach>
			</select><br class="quebraLinha"/>	
			<label class="rotulo" id="rotuloDataTentativa" for="dataSituacao" ><span class="campoObrigatorioSimbolo">* </span>Data da Situa��o:</label>
			<input class="campoData campoHorizontal" type="text" id="dataSituacao" name="dataSituacao" maxlength="10" value="<fmt:formatDate value="${entregaDocumento.dataSituacao}" pattern="dd/MM/yyyy"/>"><br />
			<br class="quebraLinha"/><br class="quebraLinha"/>
			<label class="rotulo" id="rotuloMotivo" for="motivo">Motivo da N�o Entrega:</label> 
			<select name="motivoNaoEntrega" id="motivoNaoEntrega" class="campoSelect campo2Linhas">
				<option value="-1">Selecione</option>
				<c:forEach items="${listaMotivoNaoEntrega}" var="motivoNaoEntrega">
				<option value="<c:out value="${motivoNaoEntrega.chavePrimaria}"/>"
					<c:if test="${entregaDocumento.motivoNaoEntrega ne null and entregaDocumento.motivoNaoEntrega.chavePrimaria eq motivoNaoEntrega.chavePrimaria}">selected="selected"</c:if>>
					<c:out value="${motivoNaoEntrega.descricao}" />
				</option>
				</c:forEach>
			</select><br class="quebraLinha"/>
			<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${entregaDocumento.habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Ativo</label>
			<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${entregaDocumento.habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="indicadorUso">Inativo</label>	
		</fieldset>

		<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios.</p>
		
		<c:if test="${listaDocumentoCobranca ne null}">
			
			<hr class="linhaSeparadora1" />
		
			<fieldset class="conteinerBloco">
			
				<legend class="conteinerBlocoTitulo">Documento de Cobran�a</legend>
	
				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaDocumentoCobranca" sort="list" id="documentoCobranca" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="exibirAlterarEntregaDocumento">
							
					<display:column style="text-align: center;" title="Data da Emiss�o">
					<fmt:formatDate value="${documentoCobranca.dataEmissao}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		
					<display:column style="text-align: center;" title="Valor Total">
						<fmt:formatNumber value="${documentoCobranca.valorTotal}" minFractionDigits="2" type="currency"/>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Data do Vencimento">
		            	<fmt:formatDate value="${documentoCobranca.dataVencimento}" pattern="dd/MM/yyyy"/>
		 			</display:column>
		 			
		 			<display:column style="text-align: center;" title="Situa��o">
		 				<c:out value='${documentoCobranca.cobrancaDebitoSituacao.descricao}'/>
		 			</display:column>
		 			
			    </display:table>
			    
			</fieldset>
			
		</c:if>
			
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<input name="buttonAlterar" value="Cancelar" class="bottonRightCol2" id="botaoAlterar" onclick="cancelar();" type="button">
		<input name="buttonRemover" value="Limpar" class="bottonRightCol2 bottonLeftColUltimo" onclick="limpar(this.form);" type="button">
		<input name="buttonIncluir" value="Salvar" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="salvar();" type="button">
	</fieldset>
	
</form:form>
