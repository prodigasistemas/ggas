<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>

<h1 class="tituloInterno">Pesquisar Controle de Entrega de Documento<a class="linkHelp" href="<help:help>/</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique <span class="destaqueOrientacaoInicial">Pesquisar</span>, 
ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos os registros. Para incluir um novo registro clique em <span class="destaqueOrientacaoInicial">Incluir</span>.</p>

<script type="text/javascript">

	$(document).ready(function(){
		if($("#indicador").val()==""){
			document.forms[0].habilitado[0].checked = true;
		}
		$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		var idCliente = $("#cliente").val();
		var idImovel = $("#imovel").val();
		if(idCliente!=""){
			selecionarCliente(idCliente);
		}else if (idImovel!=""){
			selecionarImovel(idImovel);
		}		
	});

	function exibirPopupPesquisaImovel() {
		popup = window.open('exibirPesquisaImovelCompletoPopup?postBack=true','popup','height=750,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
	}
	
	function limpar(form) {
		limparFormularios(form);
		limparFormularioDadosCliente();
		
		document.getElementById("idCliente").value = "";
		document.getElementById("nomeCompletoCliente").value = "";
		document.getElementById("documentoFormatado").value = "";
		document.getElementById("enderecoFormatadoCliente").value = "";
		document.getElementById("emailCliente").value = "";
		
		document.getElementById("idImovel").value = "";
		document.getElementById("nomeFantasiaImovel").value = "";
		document.getElementById("matriculaImovel").value = "";
		document.getElementById("numeroImovel").value = "";
		document.getElementById("cidadeImovel").value = "";
		document.getElementById("enderecoImovel").value = "";
		
		form.habilitado[0].checked = true;
	}
	
	function pesquisar() {
		$("#cliente").val($("#idCliente").val());
		$("#imovel").val($("#idImovel").val());
		$("#entregaDocumentoForm").submit();
	}
	
	
	function incluir() {
		location.href = '<c:url value="/exibirIncluirEntregaDocumento"/>';
	}
	
	function detalhar(chave) {
		document.forms[0].chavePrimaria.value = chave;
		submeter("entregaDocumentoForm", 'exibirDetalharEntregaDocumento');
	}
	
	function alterar(chave){
		var selecao = verificarSelecaoApenasUmSemMensagem();
		if (selecao) {	
			alteracao(chave);
	    } else {
	    	alert ("Selecione apenas um registro para realizar a opera��o!");
	    }
	}
	function alteracao(chave) {
		var selecionou = verificarSelecaoApenasUm();
		
		var selecao = verificarSelecaoApenasUmSemMensagem();
		if (selecao == true) {	
			document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('entregaDocumentoForm','exibirAlterarEntregaDocumento');
	    } else {
	    	document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
			submeter('entregaDocumentoForm','exibirAlterarEntregaDocumentoEmLote');
	    }
	}
	
	function remover(){
		var selecao = verificarSelecao();
		if (selecao) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
			if(retorno == true) {
				submeter('entregaDocumentoForm', 'removerEntregaDocumento');
			}
	    }
	}
	
	function selecionarImovel(idSelecionado){
		var idImovel = document.getElementById("idImovel");
		var matriculaImovel = document.getElementById("matriculaImovel");
		var nomeFantasia = document.getElementById("nomeFantasiaImovel");
		var numeroImovel = document.getElementById("numeroImovel");
		var cidadeImovel = document.getElementById("cidadeImovel");
		var indicadorCondominio = document.getElementById("condominio");
		var enderecoImovel = document.getElementById("enderecoImovel");
		
		if(idSelecionado != '') {				
			AjaxService.obterImovelPorChave( idSelecionado, {
	           	callback: function(imovel) {	           		
	           		if(imovel != null){  	           			        		      		         		
		               	idImovel.value = imovel["chavePrimaria"];
		               	matriculaImovel.value = imovel["chavePrimaria"];		               	
		               	nomeFantasia.value = imovel["nomeFantasia"];
		               	numeroImovel.value = imovel["numeroImovel"];
		               	cidadeImovel.value = imovel["cidadeImovel"];
		               	indicadorCondominio.value = imovel["indicadorCondominioAmbos"];
		               	enderecoImovel.value = imovel["enderecoImovel"];
	               	}
	        	}, async:false}
	        	
	        );	        
        } else {
       		idImovel.value = "";
        	matriculaImovel.value = "";
        	nomeFantasia.value = "";
			numeroImovel.value = "";
            cidadeImovel.value = "";
            indicadorCondominio.value = "";
            enderecoImovel.value = "";
       	}
	
		document.getElementById("nomeImovelTexto").value = nomeFantasia.value;
		document.getElementById("matriculaImovelTexto").value = matriculaImovel.value;
		document.getElementById("numeroImovelTexto").value = numeroImovel.value;
		document.getElementById("cidadeImovelTexto").value = cidadeImovel.value;
		if(indicadorCondominio.value == 'true') {
			document.getElementById("indicadorCondominioImovelTexto1").checked = true;
		} else {
			document.getElementById("indicadorCondominioImovelTexto2").checked = true;
		}

	}

</script>

<form:form method="post" action="pesquisarEntregaDocumento" id="entregaDocumentoForm" name="entregaDocumentoForm" modelAttribute="EntregaDocumentoImpl">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${entregaDocumento.chavePrimaria}">
	<input name="cliente" type="hidden" id="cliente" value="<c:if test="${entregaDocumento.cliente ne null}"></c:if><c:out value="${entregaDocumento.cliente.chavePrimaria}"/>"/>
	<input name="imovel" type="hidden" id="imovel" value="${imovel}"/>
	<input name="indicador" type="hidden" id="indicador" value="${habilitado}">
	
	
	<fieldset id="pesquisarEntregaDocumento" class="conteinerPesquisarIncluirAlterar">
	
		<fieldset id="pesquisarCliente" class="colunaEsq">
			<jsp:include page="/jsp/cadastro/cliente/dadosCliente.jsp">
				<jsp:param name="idCampoIdCliente" value="idCliente"/>
				<jsp:param name="idCliente" value="${entregaDocumentoForm.map.idCliente}"/>
				<jsp:param name="idCampoNomeCliente" value="nomeCompletoCliente"/>
				<jsp:param name="nomeCliente" value="${entregaDocumentoForm.map.nomeCompletoCliente}"/>
				<jsp:param name="idCampoDocumentoFormatado" value="documentoFormatado"/>
				<jsp:param name="documentoFormatadoCliente" value="${entregaDocumentoForm.map.documentoFormatado}"/>
				<jsp:param name="idCampoEmail" value="emailCliente"/>
				<jsp:param name="emailCliente" value="${entregaDocumentoForm.map.emailCliente}"/>
				<jsp:param name="idCampoEnderecoFormatado" value="enderecoFormatadoCliente"/>
				<jsp:param name="enderecoFormatadoCliente" value="${entregaDocumentoForm.map.enderecoFormatadoCliente}"/>
				<jsp:param name="possuiRadio" value="true"/>
				<jsp:param name="funcaoParametro" value="ativarBotoes"/>
			</jsp:include>
		</fieldset>
		
		<fieldset id="pesquisarImovel" class="colunaDir">
			<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
			<div class="pesquisarImovelFundo">
				<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
				<input name="idImovel" type="hidden" id="idImovel" value="${entregaDocumentoForm.map.idImovel}">
				<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${entregaDocumentoForm.map.nomeFantasiaImovel}">
				<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${entregaDocumentoForm.map.matriculaImovel}">
				<input name="numeroImovel" type="hidden" id="numeroImovel" value="${entregaDocumentoForm.map.numeroImovel}">
				<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${entregaDocumentoForm.map.cidadeImovel}">
				<input name="condominio" type="hidden" id="condominio" value="${entregaDocumentoForm.map.condominio}">
				<input name="enderecoImovel" type="hidden" id="enderecoImovel" value="${entregaDocumentoForm.map.enderecoImovel}">

				<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol2" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
				<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
				<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${entregaDocumentoForm.map.nomeFantasiaImovel}"><br />
				<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
				<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.matriculaImovel}"><br />
				<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
				<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.numeroImovel}"><br />
				<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
				<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${entregaDocumentoForm.map.cidadeImovel}"><br />
				<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${entregaDocumentoForm.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
				<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${entregaDocumentoForm.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
			</div>
		</fieldset>
		
		<fieldset class="conteinerBloco" id="conteinerPesquisarFaturaInferior">

			<fieldset id="pesquisaControleDocumentoCol1" class="coluna" style="margin:20px 0 0 0\9;">
				<label class="rotulo" id="rotuloTipoDocumento" for="tipoDocumento">Tipo de Documento:</label> 
				<select name="tipoDocumento" id="idTipoDocumento" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaTipoDocumento}" var="tipoDocumento">
					<option value="<c:out value="${tipoDocumento.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.tipoDocumento.chavePrimaria == tipoDocumento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${tipoDocumento.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>
				<label class="rotulo" id="rotuloSituacaoEntrega" for="situacaoEntrega">Situa��o da Entrega:</label> 
				<select name="situacaoEntrega" id="idSituacaoEntrega" class="campoSelect campo2Linhas">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaSituacaoEntrega}" var="situacaoEntrega">
					<option value="<c:out value="${situacaoEntrega.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.situacaoEntrega.chavePrimaria == situacaoEntrega.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${situacaoEntrega.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>			
				<label class="rotulo" id="rotuloDataTentativa" for="dataSituacao" >Data da Situa��o:</label>
				<input class="campoData campoHorizontal" type="text" id="dataSituacao" name="dataSituacao" maxlength="10" value="\<fmt:formatDate value="${entregaDocumento.dataSituacao}" pattern="dd/MM/yyyy"/>"\><br />	
			</fieldset>
			
			<fieldset id="pesquisaControleDocumentoCol2" class="colunaFinal" style="margin:20px 0 0 0\9;">
				<label class="rotulo" id="rotuloDataTentativa" for="dataVencimento" >Data de Vencimento do Documento:</label>
				<input class="campoData campoHorizontal" type="text" id="dataVencimento" name="dataVencimento" maxlength="10" 
				value="<c:if test="${entregaDocumento.documentoCobranca eq null}">
							<c:out value="${dataVencimento}"/>
					   </c:if>"
					   value="<c:if test="${entregaDocumento.documentoCobranca ne null}">
							<c:out value="${entregaDocumento.documentoCobranca.dataVencimento}"/>
					   </c:if>"
				/><br class="quebraLinha"/>		
				<label class="rotulo" id="rotuloMotivo" for="motivo">Motivo da N�o Entrega:</label> 
				<select name="motivoNaoEntrega" id="idMotivoNaoEntrega" class="campoSelect campo2Linhas" style="margin:10px 0 0 0\9;">
					<option value="-1">Selecione</option>
					<c:forEach items="${listaMotivoNaoEntrega}" var="motivoNaoEntrega">
					<option value="<c:out value="${motivoNaoEntrega.chavePrimaria}"/>"
						<c:if test="${entregaDocumento.motivoNaoEntrega.chavePrimaria == motivoNaoEntrega.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${motivoNaoEntrega.descricao}" />
					</option>
					</c:forEach>
				</select><br class="quebraLinha"/>
				<label class="rotulo" for="habilitado">Indicador de uso: &nbsp;</label>
				<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Ativo</label>
				<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
				<label class="rotuloRadio" for="indicadorUso">Inativo</label>
				<input class="campoRadio radioIndicadorControle" type="radio" name="habilitado" id="habilitado" value="null" <c:if test="${habilitado eq 'null'}">checked</c:if>>
				<label class="rotuloRadio" for="habilitado">Todos</label><br/>
			</fieldset>
		
			
		</fieldset>
	
		<fieldset class="conteinerBotoesPesquisarDirFixo">
			<input class="bottonRightCol2" type="button" id="botaoPesquisar" value="Pesquisar" onclick="pesquisar();">
			<input class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limpar(this.form);">
		</fieldset>
		
		<c:if test="${listaEntregaDocumento ne null}">
		
			<hr class="linhaSeparadora1" />
		
			<fieldset class="conteinerBloco">

				<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaEntregaDocumento" sort="list" id="documento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarEntregaDocumento">
			        
			        <display:column media="html" sortable="false" class="selectedRowColumn" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>">
		      			<input type="checkbox" name="chavesPrimarias" id="chavesPrimarias" value="${documento.chavePrimaria}">
					</display:column>
							
					<display:column title="Ativo" style="width: 30px">
						<c:choose>
							<c:when test="${documento.habilitado == true}">
								<img alt="Ativo" title="Ativo"
									src="<c:url value="/imagens/success_icon.png"/>" border="0">
							</c:when>
							<c:otherwise>
								<img alt="Inativo" title="Inativo"
									src="<c:url value="/imagens/cancel16.png"/>" border="0">
							</c:otherwise>
						</c:choose>
					</display:column>

					<display:column style="text-align: center;" title="Cliente/Imovel" sortable="true" sortProperty="cliente.nome">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
							<c:choose>
								<c:when test="${documento.cliente eq null }">
									<c:out value="${documento.imovel.nome}"/>
								</c:when>
								<c:otherwise>
									<c:out value="${documento.cliente.nome}"/>
								</c:otherwise>
							</c:choose> 
			            </a>
		 			</display:column>
					
					<display:column style="text-align: center;" title="N�mero T�tulo" sortable="true" sortProperty="aviso.fatura.chavePrimaria">
			            	<c:choose>
					    		<c:when test="${documento.avisoCorte eq null }">
							    	<c:out value=''/>
					    		</c:when>
					    		<c:otherwise>
					            	<c:out value="${documento.avisoCorte.numeroTitulo}"/>
					    		</c:otherwise>
					    	</c:choose>
		 			</display:column>
		 			
					<display:column style="text-align: center;" title="CPF/CNPJ do Cliente">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			        	   	<c:choose>
					    		<c:when test="${documento.cliente.cpf eq null || documento.cliente.cpf eq ''}">
							    	<c:out value='${documento.cliente.cnpjFormatado}'/>
					    		</c:when>
					    		<c:otherwise>
					    			<c:out value='${documento.cliente.cpfFormatado}'/>
					    		</c:otherwise>
					    	</c:choose>
			            </a>
		 			</display:column>	
		 				
					<display:column style="text-align: center;" title="Tipo de Documento" sortable="true" sortProperty="tipoDocumento.descricao">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${documento.tipoDocumento.descricao}"/>
			            </a>
		 			</display:column>

		 			<display:column style="text-align: center;" title="Data do Vencimento" sortable="true" sortProperty="documentoCobranca.dataVencimento" >
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			        		<c:if test="${documento.documentoCobranca ne null and documento.documentoCobranca.dataVencimento ne ''}">
			        			<fmt:formatDate value="${documento.documentoCobranca.dataVencimento}" pattern="dd/MM/yyyy"/>
			        		</c:if>
			            </a>
		 			</display:column>
		 			
					<display:column style="text-align: center;" title="Situa��o da Entrega" sortable="true" sortProperty="situacaoEntrega.descricao">
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<c:out value="${documento.situacaoEntrega.descricao}"/>
			            </a>
		 			</display:column>				
					
					<display:column style="text-align: center;" title="Data da Situa��o" sortable="true" sortProperty="dataSituacao" >
			        	<a href="javascript:detalhar(<c:out value='${documento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
			            	<fmt:formatDate value="${documento.dataSituacao}" pattern="dd/MM/yyyy"/>
			            </a>
		 			</display:column>

			    </display:table>
			    
			</fieldset>
			
		</c:if>
	
	</fieldset>
	
	<fieldset class="conteinerBotoes">
		<c:if test="${listaEntregaDocumento ne null}">
				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" id="botaoAlterar" onclick="alterar()" type="button">
			<%--<vacess:vacess param="exibirAlterarEntregaDocumento">--%>
			<%--</vacess:vacess>--%>
				<input name="buttonRemover" value="Excluir" class="bottonRightCol2 bottonLeftColUltimo" onclick="remover()" type="button">
			<%--<vacess:vacess param="removerEntregaDocumento">--%>
			<%--</vacess:vacess>--%>
		</c:if>
			<input name="buttonIncluir" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir();" type="button">
		<%--<vacess:vacess param="exibirIncluirEntregaDocumento">--%>
		<%--</vacess:vacess>--%>
	</fieldset>
	
</form:form>
