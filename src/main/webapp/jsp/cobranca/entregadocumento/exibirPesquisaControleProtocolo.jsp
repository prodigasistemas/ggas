<!--
Copyright (C) <2011> GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

Este arquivo  parte do GGAS, um sistema de gesto comercial de Servios de Distribuio de Gs

Este programa  um software livre; voc pode redistribu-lo e/ou
modific-lo sob os termos de Licena Pblica Geral GNU, conforme
publicada pela Free Software Foundation; verso 2 da Licena.

O GGAS  distribudo na expectativa de ser til,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implcita de
COMERCIALIZAO ou de ADEQUAO A QUALQUER PROPSITO EM PARTICULAR.
Consulte a Licena Pblica Geral GNU para obter mais detalhes.

Voc deve ter recebido uma cpia da Licena Pblica Geral GNU
junto com este programa; se no, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS ? Sistema de Gesto Comercial (Billing) de Servios de Distribuio de Gs

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
-->
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://ggas.procenge.com.br/jsp/ggas" prefix="ggas"%>

<script type="text/javascript">
	$(document).ready(function() {
		iniciarDatatable('#table-entrega-documento', {
			columnDefs: [
				{ orderable: false, targets: 0 }
			]
		});
		
		iniciarDatatable('#loteComunicacao');
		selecionarTipoVisualizacaoProtocolo();

		var datepicker = $.fn.datepicker.noConflict();
		$.fn.bootstrapDP = datepicker;  
		$('.bootstrapDP').bootstrapDP({
		    autoclose: true,
			format: 'dd/mm/yyyy',
			language: 'pt-BR',
			endDate: '+0d',
		});

		$('.bootstrapDP').inputmask("99/99/9999",{placeholder:"_"});

	});

	function pesquisar() {
		submeter('entregaDocumentoForm', 'pesquisarRetornoProtocolo');
	}

	function retornarProtococolo() {
		var selecao = verificarSelecao();
		if (selecao == true) {
			var chavesSelecionadas = [];
			$("input:checkbox[name=chavesPrimarias]:checked").each(function(){
				chavesSelecionadas.push($(this).val());
			}); 
			
	        AjaxService.consultarMesmaObservacaoProtocolo(chavesSelecionadas, {
	            callback: function (retorno) {
					var isMesmaMensagem;
		            if(retorno == "") {
		            	isMesmaMensagem = false;
			        } else {
				        isMesmaMensagem = true;
				    }

				    $("#isMesmaMensagem").val(isMesmaMensagem);
		            $("#descricaoObservacaoRetorno").val(retorno);
	            }, async: false
	        });
			
			$('#dataRetorno').modal('show');
		}
	}

	function salvarDataRetornoProtocolo() {
		if($("#dataRetornoPreenchida").val() == "" && $("#descricaoObservacaoRetorno").val() == "") {
			alert("Digite uma data ou a observa��o!");
		} else {
			submeter('entregaDocumentoForm', 'salvarDataRetornoProtocolos');
		}
	}

	function limparFormulario() {
		document.getElementById("dataInicioEmissao").value = "";
		document.getElementById("dataFimEmissao").value = "";
		document.getElementById("descricaoPontoConsumo").value = "";

		document.getElementById('idGrupoFaturamento').value = '-1';
		document.getElementById('servicoTipo').value = '-1';
		document.getElementById("dataInicioVencimento").value = "";
		document.getElementById("dataFimVencimento").value = "";
		
		document.getElementById("habilitadoTodos").checked = true;

	}

	function selecionarTipoVisualizacaoProtocolo() {
		if($("#tipoProtocolo").val() == "Comunica��o") {
			exibirServicoTipo();
		} else {
			exibirPeriodoVencimento();
		}
	}

    function exibirServicoTipo() {
    	$('#divServicoTipo').show();
        $('#divPeriodoVencimento').hide();
        $('#dataInicioVencimento').val('');
        $('#dataFimVencimento').val('');
    }

    function exibirPeriodoVencimento() {
    	$('#divPeriodoVencimento').show();
        $('#divServicoTipo').hide();
        $("#servicoTipo option[value=-1]").attr('selected','selected');
    }

    function exibirDetalhamentoLoteComunicacao(chaveLote) {
    	$("#idLoteComunicacao").val(chaveLote);

    	submeter("entregaDocumentoForm", 'exibirProtocolosLoteComunicacao');
    }

    function exibirMensagemRetornoProtocolo(chavePrimaria) {
        AjaxService.consultarInformacoesProtocolo(chavePrimaria, {
            callback: function (retorno) {
            $("#descricaoRetornoProtocolo").val(retorno[0]);
            $("#funcionario").val(retorno[1]);

            $('#observacaoRetornoPopup').modal('show');
            }, async: false
        });
    }

    function validardataAtual(objeto){

    	var valor = objeto.value;
    	var dia  = parseInt(valor.substring(0,2),10);
    	var mes  = parseInt(valor.substring(3,5),10);
    	var ano  = parseInt(valor.substring(6,10),10);

    	var dataAtual = new Date();
    	var dataInformada = new Date(ano + "-" + mes + "-" + dia);

    	if(dataInformada > dataAtual){
    		objeto.value = '';
    		return true;
    	}
		return false;
    }
    
    function cancelarProtococolo(){
    	if (verificarSelecaoApenasUm()) {
    		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
    		submeter("entregaDocumentoForm", "cancelarProtocolos");
    	}
    }

	
</script>



<div class="bootstrap">
	<!-- INCIO DE BLOCO: Variveis JSP utilizads no arquivo index.js -->


	<!-- FIM DE BLOCO: Variveis JSP utilizads no arquivo index.js -->
	<form:form action="pesquisarPriorizacaoCorte" id="entregaDocumentoForm"
		name="entregaDocumentoForm" method="post"
		modelAttribute="EntregaDocumentoVO">
		<input name="habilitar" type="hidden" id="habilitar" value="">
		<input name="idLoteComunicacao" type="hidden" id="idLoteComunicacao" value="${idLoteComunicacao}">
		<input name="isMesmaMensagem" type="hidden" id="isMesmaMensagem">
		<input name="chavePrimaria" type="hidden" id="chavePrimaria">
		
		<div class="card">
			<div class="card-header">
				<h5 class="card-title mb-0">Controle de Retorno do Protocolo</h5>
			</div>
			<div class="card-body">

				<hr>

				<!--  INICIOOOO -->
				<div class="card">
					<div class="card-body bg-light">
						<h5>Filtrar Protocolos</h5>
						<div class="row mb-2">
							<div class="col-md-6">
							
							
								<div class="form-row">
									<div class="col-md-12">
										<label for="descricaoPontoConsumo">Descri��o Ponto de Consumo:</label> 
										<input class="form-control form-control-sm" type="text"
										id="descricaoPontoConsumo" name="descricaoPontoConsumo"
										value="${entregaDocumentoVO.descricaoPontoConsumo}" maxlength="50"
										size="30"
										onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" />
									</div>
								</div>
							
							
								<div class="form-row">
									<div class="col-md-12">
										<label for="habilitado" class="col-md-12">Retornado:</label>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoAtivo"
												name="indicadorRetornado" class="custom-control-input"
												value="true"
												<c:if test="${entregaDocumentoVO.indicadorRetornado eq 'true'}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoAtivo">Sim</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoInativo"
												name="indicadorRetornado" class="custom-control-input"
												value="false"
												<c:if test="${entregaDocumentoVO.indicadorRetornado eq 'false'}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoInativo">N�o</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="habilitadoTodos"
												name="indicadorRetornado" class="custom-control-input"
												value=""
												<c:if test="${entregaDocumentoVO.indicadorRetornado eq null}">checked="checked"</c:if>>
											<label class="custom-control-label" for="habilitadoTodos">Todos</label>
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="tipoProtocolo">Tipo de Protocolo:</label>
										<select name="tipoProtocolo" id="tipoProtocolo"
											class="form-control form-control-sm" onchange="selecionarTipoVisualizacaoProtocolo();">
											<c:forEach items="${listaTipoProtocolo}"
												var="tipoProtocolo">
												<option
													value="<c:out value="${tipoProtocolo}"/>"
													<c:if test="${entregaDocumentoVO.tipoProtocolo eq tipoProtocolo}">selected="selected"</c:if>>
													<c:out value="${tipoProtocolo}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>


							</div>


							<div class="col-md-6">
								<div class="form-row">
									<div class="col-md-12">
										<label>Emiss�o do Protocolo:</label>
										<div class="input-group input-group-sm">
											<input type="text"
												class="form-control form-control-sm campoData bootstrapDP"
												id="dataInicioEmissao" name="dataInicioEmissao"
												value="${entregaDocumentoVO.dataInicioEmissao}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input type="text"
												class="form-control form-control-sm campoData bootstrapDP"
												id="dataFimEmissao" name="dataFimEmissao"
												value="${entregaDocumentoVO.dataFimEmissao}">
										</div>
									</div>
								</div>

								<div class="form-row">
									<div class="col-md-12">
										<label for="idGrupoFaturamento">Grupo de Faturamento:</label>
										<select name="idGrupoFaturamento" id="idGrupoFaturamento"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaGruposFaturamento}"
												var="grupoFaturamento">
												<option
													value="<c:out value="${grupoFaturamento.chavePrimaria}"/>"
													<c:if test="${entregaDocumentoVO.idGrupoFaturamento == grupoFaturamento.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${grupoFaturamento.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
								</div>
								
								<div id="divPeriodoVencimento" class="form-row">
									<div class="col-md-12">
										<label>Per�odo de Vencimento:</label>
										<div class="input-group input-group-sm">
											<input type="text"
												class="form-control form-control-sm campoData bootstrapDP"
												id="dataInicioVencimento" name="dataInicioVencimento"
												value="${entregaDocumentoVO.dataInicioVencimento}">
											<div class="input-group-prepend">
												<span class="input-group-text">at�</span>
											</div>
											<input type="text"
												class="form-control form-control-sm campoData bootstrapDP"
												id="dataFimVencimento" name="dataFimVencimento"
												value="${entregaDocumentoVO.dataFimVencimento}">
										</div>
									</div>
								</div>
								
								<div id="divServicoTipo" class="form-row">
									<div class="col-md-12">
										<label for="servicoTipo">Tipo de Servi�o:</label>
										<select name="servicoTipo" id="servicoTipo"
											class="form-control form-control-sm">
											<option value="-1">Selecione</option>
											<c:forEach items="${listaServicoTipo}"
												var="servicoTipo">
												<option
													value="<c:out value="${servicoTipo.chavePrimaria}"/>"
													<c:if test="${entregaDocumentoVO.servicoTipo.chavePrimaria == servicoTipo.chavePrimaria}">selected="selected"</c:if>>
													<c:out value="${servicoTipo.descricao}" />
												</option>
											</c:forEach>
										</select>
									</div>
									
									<div class="col-md-6">
										<label for="loteCancelado" class="col-md-12">Lotes Gerado para Cancelamento:</label>
										<div class="col-md-10">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="loteCartasCanceladoSim" name="lotesCartasCanceladas" class="custom-control-input" value="true"
													   <c:if test="${entregaDocumentoVO.lotesCartasCanceladas eq 'true'}">checked="checked"</c:if>>
												<label class="custom-control-label" for="loteCartasCanceladoSim">Sim</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="loteCartasCanceladoNao" name="lotesCartasCanceladas" class="custom-control-input" value="false"
													   <c:if test="${entregaDocumentoVO.lotesCartasCanceladas eq 'false' || entregaDocumentoVO.lotesCartasCanceladas eq null}">checked="checked"</c:if>>
												<label class="custom-control-label" for="loteCartasCanceladoNao">N�o</label>
											</div>
										</div>
									</div>									
									
								</div>
								
							</div>

						</div>
					</div>
				</div>
				<!-- FIMMMM -->

				<div class="row mt-3">
					<div class="col align-self-end text-right">
						<button class="btn btn-primary btn-sm" id="botaoPesquisar"
							type="button" onclick="pesquisar();">
							<i class="fa fa-search"></i> Pesquisar
						</button>
						<button class="btn btn-secondary btn-sm" id="botaoLimpar"
							type="button" onclick="limparFormulario();">
							<i class="fa fa-times"></i> Limpar
						</button>
					</div>
				</div>
				
				<c:if test="${listaLoteAcompanhamento ne null}">
					<hr class="linhaSeparadora1" />
					<fieldset class="conteinerBloco">
						<h5>Lotes de Comunica��o</h5>
						<div class="alert alert-primary" role="alert">
							<p class="orientacaoInicial">Para abrir os itens do lote basta clicar no lote. </p>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover" id="loteComunicacao" style="width:100%">
								<thead class="thead-ggas-bootstrap">
									<tr>
										<th scope="col" class="text-center">N�mero do Lote</th>
										<th scope="col" class="text-center">Tipo de Servi�o</th>
										<th scope="col" class="text-center">Data da Gera��o</th>
										<th scope="col" class="text-center">Todos Itens possuem Protocolo Retornado?</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaLoteAcompanhamento}" var="loteComunicacao">
										<tr>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
									        		<c:out value="${loteComunicacao.chavePrimaria}"/>
									        	</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
									        		<c:choose>
														<c:when test="${loteComunicacao.servicoTipo ne null}">
										        			<c:out value="${loteComunicacao.servicoTipo.descricao}"/>
										        		</c:when>
										        		<c:otherwise>
										        			Lote Gerado para Cancelamento
										        		</c:otherwise>
									        		</c:choose>
									        	</a>
											</td>
											<td class="text-center">
												<a href='javascript:exibirDetalhamentoLoteComunicacao(<c:out value='${loteComunicacao.chavePrimaria}'/>);'>
									        		<fmt:formatDate value="${loteComunicacao.dataGeracao}" pattern="dd/MM/yyyy"/>
									        	</a>
											</td>
											<td class="text-center">
									        	<c:choose>
									        		<c:when test="${loteComunicacao.getIsTodosProtocolosRetornados()}">
				                    					<img id="imagem" src="<c:url value="/imagens/circle_green.png"/>" border="0">
				               						</c:when>
				                					<c:otherwise>
					                    				<img id="imagem" src="<c:url value="/imagens/circle_red.png"/>" border="0">
				                					</c:otherwise>
				            					</c:choose>
											</td>									
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						
					</fieldset>
				</c:if>					
				

				<c:if test="${listaEntregaDocumento ne null}">
					<hr class="linhaSeparadoraPesquisa" />
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-hover"
							id="table-entrega-documento" width="100%" style="">
							<thead class="thead-ggas-bootstrap">
								<tr>
									<th>
										<div
											class="custom-control custom-checkbox custom-control-inline mr-0 ml-1">
											<input id="checkAllAuto" type="checkbox" name="checkAllAuto"
												class="custom-control-input"> <label
												class="custom-control-label p-0" for="checkAllAuto"></label>
										</div>
									</th>
									<c:if test="${isComunicacao}">
									<th scope="col" class="text-center">Sequencial da Carta</th>
									<th scope="col" class="text-center">Ponto de Consumo</th>
									</c:if>
									<th scope="col" class="text-center">Cliente</th>
									<th scope="col" class="text-center">Telefone Principal</th>
									<c:if test="${!isComunicacao}">
									<th scope="col" class="text-center">CPF/CNPJ do Cliente</th>
									<th scope="col" class="text-center">N�mero T�tulo</th>
									<th scope="col" class="text-center">Data do Vencimento</th>
									</c:if>
									<th scope="col" class="text-center">Data de Emiss�o</th>
									<th scope="col" class="text-center">Data do Retorno</th>
									<th scope="col" class="text-center">Retornado?</th>
									<th scope="col" class="text-center">Cancelado?</th>									
									<th scope="col" class="text-center">Informa��es Retorno</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listaEntregaDocumento}"
									var="entregaDocumento">
									<tr>
										<td>
												<div
													class="custom-control custom-checkbox custom-control-inline mr-0 ml-1"
													data-identificador-check="chk${entregaDocumento.chavePrimaria}">
													<input id="chk${entregaDocumento.chavePrimaria}"
														type="checkbox" name="chavesPrimarias"
														class="custom-control-input"
														value="${entregaDocumento.chavePrimaria}"> <label
														class="custom-control-label p-0"
														for="chk${entregaDocumento.chavePrimaria}"></label>
												</div>
										</td>
										<c:if test="${isComunicacao}">
										<td class="text-center">
									    	<c:out value="${entregaDocumento.itemLoteComunicacao.sequencialItem}"/>
										</td>										
										<td class="text-center">
									    	<c:out value="${entregaDocumento.itemLoteComunicacao.pontoConsumo.descricao}"/>
										</td>
										</c:if>
										<td class="text-center"><c:out
										value="${entregaDocumento.cliente.nome}" /></td>
										<td class="text-center"><c:out
										value="${entregaDocumento.cliente.retornarTelefonePrincipal()}" /></td>
										<c:if test="${!isComunicacao}">
										<td class="text-center"><c:choose>
												<c:when test="${entregaDocumento.numeroTitulo eq null }">
													<c:out value='' />
												</c:when>
												<c:otherwise>
													<c:out value="${entregaDocumento.numeroTitulo}" />
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><c:if
												test="${entregaDocumento.fatura ne ''}">
												<fmt:formatDate
													value="${entregaDocumento.fatura.dataVencimento}"
													pattern="dd/MM/yyyy" />
											</c:if></td>
										</c:if>
										<td class="text-center"><c:if
												test="${entregaDocumento.dataEmissao ne ''}">
												<fmt:formatDate value="${entregaDocumento.dataEmissao}"
													pattern="dd/MM/yyyy" />
											</c:if></td>											
										<td class="text-center"><c:if
												test="${entregaDocumento.indicadorRetornado eq true}">
												<fmt:formatDate value="${entregaDocumento.dataSituacao}"
													pattern="dd/MM/yyyy" />
											</c:if></td>

										<td class="text-center"><c:choose>
												<c:when
													test="${entregaDocumento.indicadorRetornado eq true}">
													<img alt="Ativo" title="Ativo"
														src="<c:url value="/imagens/success_icon.png"/>"
														border="0">
												</c:when>
												<c:otherwise>
													<img alt="Desativo" title="Desativo"
														src="<c:url value="/imagens/deletar_x.png"/>" border="0">
												</c:otherwise>
											</c:choose></td>
										<td class="text-center"><c:choose>
												<c:when
													test="${entregaDocumento.habilitado eq false}">
													<img alt="Ativo" title="Ativo"
														src="<c:url value="/imagens/success_icon.png"/>"
														border="0">
												</c:when>
												<c:otherwise>
													<img alt="Desativo" title="Desativo"
														src="<c:url value="/imagens/deletar_x.png"/>" border="0">
												</c:otherwise>
											</c:choose></td>											
											<td class="text-center">
												<c:if test="${entregaDocumento.indicadorRetornado or entregaDocumento.descricaoObservacaoRetorno ne null}">
													<a href='javascript:exibirMensagemRetornoProtocolo(<c:out value='"${entregaDocumento.chavePrimaria}"'/>);'>
															<img src="<c:url value="/imagens/icone_exibir_detalhes.png" />" alt="Exibir Observa��o Retorno" title="Exibir Observa��o Retorno" border="0" style="margin-top: 5px;" >
													</a>
												</c:if>
											</td>											

									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>

					<div class="row justify-content-end">
						<div class="col text-right mt-2">
							<button name="buttonIncluir" id="buttonIncluir" type="button"
								class="btn btn-primary btn-sm ml-1"
								onclick="retornarProtococolo();">Retornar Protocolo(s)</button>
							<c:if test="${idLoteComunicacao != null}">
								<button name="buttonCancelar" id="buttonCancelar" type="button"
									class="btn btn-primary btn-sm ml-1"
									onclick="cancelarProtococolo();">Cancelar Protocolo(s)</button>		
							</c:if>						
						</div>
					</div>
			</div>
			</c:if>
			<hr>

		</div>
		<!-- Modal -->
		<div class="modal fade" id=dataRetorno tabindex="-1" role="dialog"
			aria-labelledby="dataRetorno" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Formul�rio Retorno</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

				 		<div class="form-row">
			            	<div class="col-md-12">
								<label>Data Retorno:</label>
								<input type="text" class="form-control form-control-sm campoData bootstrapDP"
									id="dataRetornoPreenchida" name="dataRetorno" maxlength="10"
									value="${entregaDocumentoVO.dataRetorno}">
							</div>
						</div>
						
			            <div class="form-row">
			            	<div class="col-md-12">
			            			<label>Observa��o Retorno:</label>
			                    	<textarea
			                    		class="form-control form-control-sm" name="descricaoObservacaoRetorno"
			                        	id="descricaoObservacaoRetorno" cols="60"
			                            rows="6"
			                            maxlength="800"></textarea>
			                 </div>
			           	</div>							
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Cancelar</button>
						<button type="button" class="btn btn-primary"
							onclick="salvarDataRetornoProtocolo();">Confirmar</button>
					</div>
				</div>
			</div>
		</div>
		
		<!--  Modal Observa��o Retorno -->
		<div class="modal fade" id=observacaoRetornoPopup tabindex="-1" role="dialog"
			aria-labelledby="observacaoRetornoPopup" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Informa��es Retorno</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
					    <div class="form-row">
					    	<div class="col-md-12">
					    		<label>Observa��o Retorno:</label>
						    	<textarea
						    		class="form-control form-control-sm" name="descricaoRetornoProtocolo"
								id="descricaoRetornoProtocolo" cols="60"
							    rows="6"
							    maxlength="800" disabled></textarea>
						</div>
					</div>
					
				 		<div class="form-row">
			            	<div class="col-md-12">
								<label>Usu�rio Respons�vel:</label>
								<input class="form-control form-control-sm" type="text"
									id="funcionario" name="funcionario" maxlength="50"
									disabled>
							</div>
						</div>											
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>		
	</form:form>
</div>
