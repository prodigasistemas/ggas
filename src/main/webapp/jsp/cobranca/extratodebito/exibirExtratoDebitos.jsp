<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Extrato de D�bitos<a class="linkHelp" href="<help:help>/geraodoextratodedbitos.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>

<script type="text/javascript">
	
	$(document).ready(function(){

		// Dialog			
		$("#pontoConsumoPopupLista").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			minHeight: 120,
			resizable: false
		});
		
		//Tabela com cabe�alho fixo e rolagem do dados		
		$("#fatura,#creditoDebito").chromatable({
			width: "891px"
		});
		
		$('#todosFatura').click(function(){
			var checked = $('#todosFatura').is(':checked');
			var checkFatura = document.getElementsByName('chavesFatura');
			var arrayValorFatura = document.getElementsByName('valorFatura');
			var arrayTipoDocumentoFatura = document.getElementsByName('tipoDocumentoFatura');
			
			for (var i = 0; i < checkFatura.length; i++) {
				checkFatura[i].checked = checked;
				atualizarValorTotal(checkFatura[i], arrayValorFatura[i].value, arrayTipoDocumentoFatura[i].value);
				
			}
		});
		
		$('#todosDebito').click(function(){
			var checked = $('#todosDebito').is(':checked');
			var checkDebito = document.getElementsByName('chavesCreditoDebito');
			var arrayValorCreditoDebito = document.getElementsByName('valorCreditoDebito');
			var arrayTipoDocumentoCreditoDebito = document.getElementsByName('tipoDocumentoCreditoDebito');
			for (var i = 0; i < checkDebito.length; i++) {
				checkDebito[i].checked = checked;
				atualizarValorTotal(checkDebito[i], arrayValorCreditoDebito[i].value, arrayTipoDocumentoCreditoDebito[i].value);
			}
		});

		$('input[name="chavesFatura"]:checked').trigger('onclick');
		$('input[name="chavesCreditoDebito"]:checked').trigger('onclick');
		
		
		$(".campoData").datepicker({changeYear: true, yearRange: '<c:out value="${intervaloAnosData}"/>', showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy', minDate: '+0d'});
		
		
	 atualizarDataVencimento();
	 
		
	});
	
		
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaExtratoDebitos"/>';
	}
	
	function atualizarDataVencimento(){
		
		AjaxService.getQuantidadeDiasVencimentoDocumentoComplementar(
				function(valor){
					$("#dataVencimento").datepicker('option', 'maxDate', '+0d' + valor);
		});
		
		
	}

	function gerarExtratoDebitos() {
		var selecao = verificarSelecaoFaturas();
		if (selecao == false) {
			selecao = verificarSelecaoCreditoDebitos();
		}
		if (selecao == true) {
			var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_GERAR_RELATORIO"/>');
			if(retorno == true) {
				var testeArray = new Array();
				var chavesCreditoDebito = document.getElementsByName('chavesCreditoDebito');
				var parcelasCobradas = document.getElementById('parcelasCobradas');
				var parcelasCobradasAux = document.getElementsByName('parcelasCobradasAux');
				for(var i = 0; i < chavesCreditoDebito.length; i ++) {
					if (chavesCreditoDebito[i].checked) {
						testeArray[testeArray.length] = parcelasCobradasAux[i].value; 
					}
				}
				parcelasCobradas.value = testeArray;
				submeter('extratoDebitoForm', 'gerarExtratoDebitos');
			}
	    }
	}

	function verificarSelecaoFaturas() {
		var flag = 0;
		var form = document.forms[0];

		if (form != undefined && form.chavesFatura != undefined) {
			var total = form.chavesFatura.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesFatura[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesFatura.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}
	
	function verificarSelecaoCreditoDebitos() {
		var flag = 0;
		var form = document.getElementsByName('chavesCreditoDebito');
		if (form != undefined) {
			var total = form.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				alert ("Selecione um ou mais registros para realizar a opera��o!");
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
	}

	function closePopup(){
		$("#pontoConsumoPopupLista").dialog('close');
	}
	
	function exibirDadosPontoConsumoPorChave(idPontoConsumo) {
		
		var descricao = '';
		var endereco = '';
		var cep = '';
		var segmento = '';
		var matriculaImovel = '';
		$('#pontoConsumo tr:not(:first)').remove();
		
		if(idPontoConsumo != "" && parseInt(idPontoConsumo) > 0){
	       	AjaxService.obterPontoConsumoPorChave( idPontoConsumo, {callback: function(pontoConsumo) {
					descricao = pontoConsumo['descricao'];
					endereco = pontoConsumo['endereco'];
					cep = pontoConsumo['cep'];
					segmento = pontoConsumo['segmento'];
					matriculaImovel = pontoConsumo['matriculaImovel'];
		       		var inner = '';
		       		$('#corpoPontoConsumo').html('');		           		

		       		inner = inner + '<tr class='+"odd"+'><td>'+matriculaImovel+'</td>';
		       		inner = inner + '<td>'+descricao+'</td>';
		       		inner = inner + '<td>'+endereco+'</td>';
		       		inner = inner + '<td>'+segmento+'</td>';
		       		inner = inner + '<td>'+cep+'</td></tr>';

		           	$("#corpoPontoConsumo").append(inner);
	       	  }
	       	 , async: false}
	       	);
			$("#pontoConsumoPopupLista").dialog('open');
		} else {
			alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
		}
		
	}
	

	function exibirDadosPontoConsumoPorFatura(idFatura) {

		var descricao = '';
   		var endereco = '';
   		var cep = '';
   		var segmento = '';
   		var matriculaImovel = '';
   		var exibe = false;
   			
		if(idFatura != "" && parseInt(idFatura) > 0){
	       	AjaxService.obterListaPontoConsumoPorFatura( idFatura, { 
	           	callback: function(listaPontos) {
	       		answer = listaPontos;
	       		$('#pontoConsumo tr:not(:first)').remove();
	       		var param = '';
           		var div = 2;
           		var start = 2;
           		var cont = 0;
           		$('#corpoPontoConsumo').html('');

		       		for (key in listaPontos) {
		       			var pontoConsumo = listaPontos[cont];
		       			var inner = '';
		       			
		       			if(start % div == 0){
							param = "odd";
			            }else{
							param = "even";
		       			}
				        
		       			descricao = pontoConsumo['descricao'];
						endereco = pontoConsumo['endereco'];
						cep = pontoConsumo['cep'];
						segmento = pontoConsumo['segmento'];
						matriculaImovel = pontoConsumo['matriculaImovel'];
	
			       		inner = inner + '<tr class='+param+'><td>'+matriculaImovel+'</td>';
			       		inner = inner + '<td>'+descricao+'</td>';
			       		inner = inner + '<td>'+endereco+'</td>';
			       		inner = inner + '<td>'+segmento+'</td>';
			       		inner = inner + '<td>'+cep+'</td></tr>';
	
			           	$("#corpoPontoConsumo").append(inner);
			           	start = start + 1;
						cont++;
						exibe = true;
					}
				}
	       	 , async: false}
	       	);
			if (exibe){
				$("#pontoConsumoPopupLista").dialog('open');
			}else{
				alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
			}
			exibirJDialog("#pontoConsumoPopup");
		} else {
			alert("Cr�dito ou D�bito selecionado n�o possui ponto de consumo.");
		}
		
	}
	
	
	

	function atualizarValorTotal(obj, paramValor, paramTipo) {
		var inputValorDebitos = document.getElementById('valorDebitos');
		var inputValorCreditos = document.getElementById("valorCreditos");
		var tipoCredito = '<c:out value="${tipoCredito}"/>';
		var tipoNotaCredito = '<c:out value="${tipoNotaCredito}"/>';
		
		if (inputValorDebitos.value == "") {
			inputValorDebitos.value = 0;
		}
		if (inputValorCreditos.value == "") {
			inputValorCreditos.value = 0;
		}
		
		var valorFloat = converterStringParaFloat(paramValor);
		var valorDebitos = parseFloat(inputValorDebitos.value);
		var valorCreditos = parseFloat(inputValorCreditos.value);
		var valorTotal = 0;
		if (obj.checked) {
			if((tipoCredito == paramTipo) || (tipoNotaCredito == paramTipo)){
				// Se selecionou um cr�dito.
				valorCreditos = valorCreditos + valorFloat;
				inputValorCreditos.value = valorCreditos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
				atualizarBotaoGerar(valorTotal);
			} else {
				// Se selecionou um d�bito.
				valorDebitos = valorDebitos + valorFloat;
				inputValorDebitos.value = valorDebitos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
				atualizarBotaoGerar(valorTotal);
			}
		} else {
			if((tipoCredito == paramTipo) || (tipoNotaCredito == paramTipo)){
				// Se desmarcou um cr�dito.
				valorCreditos = valorCreditos - valorFloat;
				inputValorCreditos.value = valorCreditos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
				atualizarBotaoGerar(valorTotal);
			} else {
				// Se desmarcou um d�bito.
				valorDebitos = valorDebitos - valorFloat;
				inputValorDebitos.value = valorDebitos.toFixed(2);
				valorTotal = valorDebitos - valorCreditos;
				atualizarBotaoGerar(valorTotal);
			}
		}
	}
	
	function calcularValorNotasDebitos(){
		submeter('extratoDebitoForm', 'calcularValorNotasDebitos');
	}

	function atualizarBotaoGerar(valorTotal) {
		$('#valorDebitosSimulados').val(parseFloat(valorTotal).toLocaleString({minimumFractionDigits:2}));
		if (valorTotal <= 0) {
			document.getElementById('botaoGerar').disabled = true;
		} else {
			document.getElementById('botaoGerar').disabled = false;
		}
	}

	animatedcollapse.addDiv('dadosCliente', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosImovel', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('dadosPontoConsumo', 'fade=0,speed=400,persist=1,hide=0');
	animatedcollapse.addDiv('conteinerDocumentos', 'fade=0,speed=400,persist=0,hide=0');
	animatedcollapse.addDiv('conteinerDebitosCreditosRealizar', 'fade=0,speed=400,persist=0,hide=0');
	

</script>	

<form:form method="post" name="extratoDebitoForm" action="gerarExtratoDebitos">
	<input name="acao" type="hidden" id="acao" value="gerarExtratoDebitos"/>
	<input name="postBack" type="hidden" id="postBack" value="true">
	<input name="idPontoConsumo" type="hidden" id="idPontoConsumo" value="${extratoDebitoVO.idPontoConsumo}">
	<input name="idCliente" type="hidden" id="idCliente" value="${extratoDebitoVO.idCliente}">
	<input type="hidden" id="parcelasCobradas" name="parcelasCobradas" value="">
	<input name="idFatura" type="hidden" id="idFatura" value="${idFatura}">
	<input name="idImovel" type="hidden" id="idImovel" value="${extratoDebitoVO.idImovel}">
	
	<div id="pontoConsumoPopupLista" title="Ponto de Consumo" >
		<table class="dataTableGGAS dataTableDialog">
		
			<thead>
				<tr>
					<th>Matr�cula do Im�vel</th>
					<th style="width: 230px">Descricao</th>
					<th style="width: 230px">Endere�o</th>
					<th style="width: 140px">Segmento</th>
					<th style="width: 75px">CEP</th>
				</tr>
			</thead>
			<tbody id="corpoPontoConsumo"></tbody>
		</table>
		<hr class="linhaSeparadoraPopup" />
		<input name="Button" id="botaoConfirmar" class="bottonRightCol" value="Fechar" type="button" onclick="closePopup()">
	</div>
	
	<fieldset id="itensExtratoDebitosCliente" class="conteinerPesquisarIncluirAlterar">
		<c:choose>
			<c:when test="${extratoDebitoVO.idCliente ne null && extratoDebitoVO.idCliente > 0}">
				<a id="linkDadosCliente" class="linkExibirDetalhes" href="#" rel="toggle[dadosCliente]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Cliente <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosCliente" class="conteinerDados conteinerBloco">
					<label class="rotulo">Nome:</label>
					<span class="itemDetalhamento"><c:out value="${nomeCompletoCliente}"/></span><br />
				</fieldset>
			</c:when>
			<c:when test="${extratoDebitoVO.idImovel ne null && extratoDebitoVO.idImovel > 0}">
				<a id="linkDadosImovel" class="linkExibirDetalhes" href="#" rel="toggle[dadosImovel]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Im�vel <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosImovel" class="conteinerDadosDetalhe">
					<fieldset class="coluna">
						<label class="rotulo">Descri��o:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.nome}"/></span><br />
						<label class="rotulo">Matr�cula:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.chavePrimaria}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">N�mero:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.numeroImovel}"/></span><br />
						<label class="rotulo">Cidade:</label>
						<span class="itemDetalhamento"><c:out value="${imovel.quadraFace.endereco.cep.municipio.descricao}"/></span><br />
					</fieldset>
				</fieldset>
			</c:when>
			<c:otherwise>
				<a id="linkDadosPontoConsumo" class="linkExibirDetalhes" href="#" rel="toggle[dadosPontoConsumo]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Dados do Ponto de Consumo <img src="imagens/setaBaixo.png" border="0"></a>
				<fieldset id="dadosPontoConsumo" class="conteinerDadosDetalhe">
					<fieldset class="coluna">
						<label class="rotulo">Descricao:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.descricao}"/></span><br />
						<label class="rotulo">Endere�o:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.enderecoReferencia}"/></span><br />
					</fieldset>
					<fieldset class="colunaFinal">
						<label class="rotulo">CEP:</label>
						<span class="itemDetalhamento"><c:out value="${pontoConsumo.cep.cep}"/></span><br />
						<label class="rotulo">Complemento:</label>
						<span class="itemDetalhamento itemDetalhamentoLargo"><c:out value="${pontoConsumo.descricaoComplemento}"/></span><br />
					</fieldset>
				</fieldset>
			</c:otherwise>
		</c:choose>
		
		<hr class="linhaSeparadora1" />
		
		<a class="linkExibirDetalhes" href="#" rel="toggle[conteinerDocumentos]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">Documentos <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="conteinerDocumentos" class="conteinerBloco">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas"
				name="listaFatura" id="fatura" excludedParams="" requestURI="#" >

				<c:set var="selecionado" value="false" />
				<display:column headerClass="headerCheckbox"
					style="text-align: center; width: 25px" sortable="false"
					title="<input type='checkbox' name='todosFatura' id='todosFatura' />">
					<c:forEach items="${extratoDebitoVO.chavesFatura}"
						var="idChaveFatura" >
						<c:if test="${idChaveFatura eq fatura.idFatura }">
							<c:set var="selecionado" value="true" />
						</c:if>
					</c:forEach>
					<input type="hidden" value="${fatura.valor}" name="valorFatura" id="valorFatura">
					<input type="hidden" value="${fatura.idTipoDocumento}" name="tipoDocumentoFatura" id="tipoDocumentoFatura">
					<input type="checkbox" name="chavesFatura"
						value="${fatura.idFatura}"
						onclick="atualizarValorTotal(this, '<c:out value="${fatura.saldoCorrigido}"/>', '<c:out value="${fatura.idTipoDocumento}"/>');"  
						<c:if test="${selecionado eq true}">
				 			checked
						</c:if>/>
					
				</display:column>
				
				
				
				<display:column headerClass="headerNumeroDocumento"
					style="width: 105px" property="idFatura" sortable="false"
					title="N� do<br />Documento" />
				<display:column headerClass="headerCicloReferencia"
					style="width: 100px" property="cicloReferencia" sortable="false"
					title="M�s/Ano-Ciclo" />
				<display:column headerClass="headerValorVencimento"
					style="width: 120px" property="valor" sortable="false"
					title="Valor (R$)" />
				<display:column headerClass="headerValorVencimento"
					style="width: 120px" property="saldo" sortable="false"
					title="Saldo (R$)" />
				<display:column headerClass="headerValorVencimento"
					style="width: 120px" property="saldoCorrigido" sortable="false"
					title="Saldo Corrigido (R$)" />
				<display:column headerClass="headerDataEmissao" style="width: 65px"
					property="dataEmissao" sortable="false"
					title="Data de<br />Emiss�o" />
				<display:column headerClass="headerVencimento" style="width: 65px"
					property="dataVencimento" sortable="false" title="Vencimento" />
				<display:column headerClass="headerTipoDocumento"
					style="width: 135px" property="tipoDocumento" sortable="false"
					title="Tipo de<br />Documento" />
				<display:column headerClass="headerSituacaoPagamento"
					style="width: 135px" property="situacaoPagamento" sortable="false"
					title="Situa��o de<br /> Pagamento" />
				<c:if
					test="${extratoDebitoVO.idCliente ne null && extratoDebitoVO.idCliente > 0}">
					<display:column headerClass="headerPontoConsumo"
						style="width: 65px" title="Ponto de<br />Consumo">
						<c:if test="${fatura.indicadorPontoConsumo}">
							<a
								href="javascript:exibirDadosPontoConsumoPorFatura('<c:out value="${fatura.idFatura}"/>');"><img
								border="0"
								src="<c:url value="/imagens/icone_exibir_detalhes.png"/>" /></a>
						</c:if>
					</display:column>
				</c:if>
			</display:table>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<a class="linkExibirDetalhes" href="#" rel="toggle[conteinerDebitosCreditosRealizar]" data-openimage="imagens/setaCima.png" data-closedimage="imagens/setaBaixo.png">D�bitos e Cr�ditos a Realizar <img src="imagens/setaBaixo.png" border="0"></a>
		<fieldset id="conteinerDebitosCreditosRealizar" class="conteinerBloco">
			<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaCreditoDebito" id="creditoDebito" excludedParams="" requestURI="#" >


				<c:set var="selecionado" value="false" />
				<display:column style="text-align: center; width: 25px"
					sortable="false"
					title="<input type='checkbox' name='todosDebito' id='todosDebito' />">
					<c:forEach items="${extratoDebitoVO.chavesCreditoDebito}"
						var="idChaveCreditoDebito">
						<c:if
							test="${idChaveCreditoDebito eq creditoDebito.idCreditoDebito }">
							<c:set var="selecionado" value="true" />
						</c:if>
					</c:forEach>
					<input type="hidden" value="${creditoDebito.valorTotal}" name="valorCreditoDebito" id="valorCreditoDebito">
					<input type="hidden" value="${creditoDebito.idTipoDocumento}" name="tipoDocumentoCreditoDebito" id="tipoDocumentoCreditoDebito">
					<input type="checkbox" id="chavesCreditoDebito" name="chavesCreditoDebito"
						value="${creditoDebito.idCreditoDebito}"
						onclick="atualizarValorTotal(this, '<c:out value="${creditoDebito.valorTotal}"/>', '<c:out value="${creditoDebito.idTipoDocumento}"/>');"
						<c:if test="${selecionado eq true}">
						checked
						</c:if> />
				</display:column>


				<display:column property="tipoDocumento" sortable="false" title="Tipo" />
		   		<display:column property="descricaoRubrica" sortable="false" title="Rubrica" />	   		
		   		<display:column property="inicioCobranca" sortable="false" title="In�cio da<br />Cobran�a" />
		   		<display:column sortable="false" title="Parcelas" >
		   			<c:out value="${creditoDebito.parcelasCobradas}"></c:out>/<c:out value="${creditoDebito.totalParcelas}"></c:out>
		   			<input type="hidden" name="parcelasCobradasAux" value="${creditoDebito.parcelasCobradas}">
		   		</display:column>
		   		<display:column property="valorTotal" sortable="false" title="Valor no <br />Vencimento (R$)" />
		   		<display:column sortable="false" title="Antecipado">
		   			<c:choose>
		   				<c:when test="${creditoDebito.indicadorAntecipacao eq 'true'}">Sim</c:when>
		   				<c:otherwise>N�o</c:otherwise>
		   			</c:choose>
		   		</display:column>
		   		
		   		<c:if test="${extratoDebitoVO.idCliente ne null && extratoDebitoVO.idCliente > 0}">
		   			<display:column title="Ponto de<br />Consumo" style="width: 65px" >
		   				<c:if test="${creditoDebito.indicadorPontoConsumo}">
							<a href="javascript:exibirDadosPontoConsumoPorChave('<c:out value="${creditoDebito.idPontoConsumo}"/>');"><img border="0" src="<c:url value="/imagens/icone_exibir_detalhes.png"/>"/></a>
						</c:if>						
					</display:column>
		   		</c:if>
		   	</display:table>
		</fieldset>
		
		<hr class="linhaSeparadora1" />
		
		<fieldset id="conteinerDadosExtratoDebito" class="conteinerBloco">		
			<input type="hidden" id="valorDebitos" >
			<input type="hidden" id="valorCreditos" >		
			<label class="rotulo" for="dataVencimento">Data de Vencimento:</label>
			<input class="campoData campoHorizontal" type="text" id="dataVencimento" name="dataVencimento" maxlength="10" size="8" value="${extratoDebitoVO.dataVencimento}" ><br />
			<label class="rotulo" for="valorTotalCobranca">Total:</label> <input class="campoTexto" type="text" id="valorDebitosSimulados"
				name="valorDebitosSimulados" maxlength="5" size="10"
				value="${valorDebitosSimulados}"
				disabled="disabled" /> <input name="Button" class="bottonRightCol" value="Calcular"
				type="button" onClick="calcularValorNotasDebitos();" style="margin-left:20px;height:27px" >
		</fieldset>
	</fieldset>
	
	<fieldset id="conteinerBotoesPesquisarDirPesquisarExtratoDebito" class="conteinerBotoes">
		<input class="bottonRightCol" type="button" value="Cancelar" onclick="cancelar();">
		<input class="bottonRightCol2 botaoGrande1" id="botaoGerar" type="button" value="Gerar" onclick="gerarExtratoDebitos();" disabled="disabled">
	</fieldset>
</form:form>	