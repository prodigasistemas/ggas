<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<h1 class="tituloInterno">Detalhar Perfil de Parcelamento<a class="linkHelp" href="<help:help>/detalhamentodoperfildeparcelamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para modificar as informa��es deste registro clique em <span class="destaqueOrientacaoInicial">Alterar</span></p>

<form:form method="post" action="exibirDetalhamentoPerfilParcelamento" name="perfilParcelamentoForm"> 

<script>

	function alterar(){
		document.forms[0].postBack.value = false;
		submeter("perfilParcelamentoForm", "exibirAlteracaoPerfilParcelamento");
	}
	
	function voltar() {
		submeter("perfilParcelamentoForm",  "voltarPerfilParcelamento");
	}

</script>

<input name="chavePrimaria" type="hidden" id="chavePrimaria" value="${perfilParcelamento.chavePrimaria}"/>
<input name="versao" type="hidden" id="versao" value="${perfilParcelamento.versao}">
<input name="vigenciaInicialHidden" type="hidden" id="vigenciaInicialHidden" value="${vigenciaInicial}">
<input name="vigenciaFinalHidden" type="hidden" id="vigenciaFinalHidden" value="${vigenciaFinal}">
<input name="postBack" type="hidden" id="postBack" value="true">

<fieldset id="dadosPerfilParcelamento" class="detalhamento">
	<fieldset class="conteinerBloco">
		<fieldset class="coluna2 detalhamentoColunaLarga">
			<label class="rotulo" for="descricao">Descri��o:</label>
			<span class="itemDetalhamento itemDetalhamentoMedioPequeno"><c:out value="${perfilParcelamento.descricao}"/></span><br />
			<label class="rotulo" for="descricaoAbreviada">Descri��o Abreviada:</label>
			<span class="itemDetalhamento"><c:out value="${perfilParcelamento.descricaoAbreviada}"/></span><br />
			<label class="rotulo" for="maximoParcelas">N� M�ximo de Parcelas:</label>
			<span class="itemDetalhamento"><c:out value="${perfilParcelamento.maximoParcelas}"/></span><br />
			
			<label class="rotulo">Vig�ncia inicial:</label>
			<span class="itemDetalhamento"><fmt:formatDate pattern="dd/MM/yyyy" value="${perfilParcelamento.vigenciaInicial}" /></span><br />
			
			<label class="rotulo">Vig�ncia final:</label>
			<span class="itemDetalhamento"><fmt:formatDate pattern="dd/MM/yyyy" value="${perfilParcelamento.vigenciaFinal}" /></span>			
		</fieldset>
		
		<fieldset class="colunaFinal2">
			<label class="rotulo">Concede Desconto:</label>
			<span class="itemDetalhamento">
				<c:if test="${perfilParcelamento.indicadorDesconto eq 'true'}">Sim</c:if>
				<c:if test="${perfilParcelamento.indicadorDesconto eq 'false'}">N�o</c:if>
			</span><br />
			<c:if test="${perfilParcelamento.indicadorDesconto}">
				<label class="rotulo" for="valorDesconto">Desconto M�ximo:</label>
				<span class="itemDetalhamento"><c:out value="${perfilParcelamento.valorDesconto}"/>,00</span>
				<label class="rotuloHorizontal rotuloInformativo" for="taxaJuros">%</label><br class="quebraLinha" />
			</c:if>
				<label class="rotulo" >Aplica Juros:</label>
				<span class="itemDetalhamento">
				<c:if test="${perfilParcelamento.indicadorJuros eq 'true'}">Sim</c:if> 			
				<c:if test="${perfilParcelamento.indicadorJuros eq 'false'}">N�o</c:if>
				</span><br />
			<c:if test="${perfilParcelamento.indicadorJuros}">
				<label class="rotulo" for="taxaJuros">Taxa de Juros:</label>
				<c:if test="${perfilParcelamento.taxaJuros ne null}">
					<span class="itemDetalhamento"><c:out value="${perfilParcelamento.taxaJuros}"/>,00</span>
					<label class="rotuloHorizontal rotuloInformativo" for="taxaJuros">%</label><br class="quebraLinha" />
				</c:if>
				
				<label class="rotulo rotulo2Linhas">Aplica Corre��o Monet�ria:</label>
				<c:if test="${perfilParcelamento.indicadorCorrecaoMonetaria eq 'true'}"><label>Sim</label></c:if> 			
				<c:if test="${perfilParcelamento.indicadorCorrecaoMonetaria eq 'false'}"><label>N�o</label></c:if>
				
				<label class="rotulo">�ndice Financeiro:</label>
				<span class="itemDetalhamento"><c:out value="${descricaoIndiceFinanceiro}"/></span><br />
				
				<label class="rotulo rotulo2Linhas" for="idAmortizacao">Sistema de Amortiza��o:</label>
				<span class="itemDetalhamento"><c:out value="${descricaoAmortizacao}"/></span><br />		
			 </c:if>
			<label class="rotulo">Ativo:</label>
			<c:if test="${perfilParcelamento.habilitado eq 'true'}"><label  for="habilitadoSim">Sim</label></c:if> 			
			<c:if test="${perfilParcelamento.habilitado eq 'false'}"><label  for="habilitadoNao">N�o</label></c:if>
			
		</fieldset>
	</fieldset>
	
	<hr class="linhaSeparadoraDetalhamento"/>
	
	<fieldset class="conteinerBloco">
	    <fieldset class="coluna2 detalhamentoColunaLarga">
			<c:if test="${listaSegmentosAssociados ne null}">
			<legend class="conteinerBlocoTitulo">Segmentos</legend>
				<span class="itemDetalhamentoLista">
			   		<c:forEach items="${listaSegmentosAssociados}" var="segmento">
			   			<c:out value="${segmento.descricao}"/><br />
				    </c:forEach> 
			    </span>
			</c:if>
		</fieldset>
		
	    <fieldset class="colunaFinal2">
			<c:if test="${listaSituacoesAssociados ne null}">
			<legend class="conteinerBlocoTitulo">Situa��es de Ponto de Consumo</legend>
				<span class="itemDetalhamentoLista">
			   		<c:forEach items="${listaSituacoesAssociados}" var="situacao">
			   			<c:out value="${situacao.descricao}"/><br />
				    </c:forEach> 
			    </span>
			</c:if>
		</fieldset>
	</fieldset>
</fieldset>

<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol" value="Voltar" type="button" onclick="voltar();">    
    <input name="button" class="bottonRightCol2 botaoGrande1" id="botaoAlterar" value="Alterar" type="button" onclick="alterar();">
</fieldset>

</form:form> 