<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>
<%@ taglib tagdir="/WEB-INF/tags/token" prefix="token" %>


<h1 class="tituloInterno">Incluir Perfil de Parcelamento<a class="linkHelp" href="<help:help>/inclusoalteraodoperfildeparcelamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Informe os dados abaixo e clique em <span class="destaqueOrientacaoInicial">Incluir</span> para finalizar.</p>

<form:form method="post" action="incluirPerfilParcelamento" name="perfilParcelamentoForm"> 

<script>

	$(document).ready(function(){
		$("input#concedeDescontoSim").click(function () { 
			animatedcollapse.show('conteinerDescontoMaximo');			
		});
		
		$("input#concedeDescontoNao").click(function () { 
			animatedcollapse.hide('conteinerDescontoMaximo');
			$("#valorDesconto").val('');			
		})
		
		$("input#aplicaJurosSim").click(function () { 
			animatedcollapse.show('ConteinerAplicaJuros');			
		});
		
		if ($("input#aplicaJurosSim").prop("checked")) {
			animatedcollapse.show('ConteinerAplicaJuros');	
		}
		
		if ($("input#concedeDescontoSim").prop("checked")) {
			animatedcollapse.show('conteinerDescontoMaximo');	
		}
		
		if ($("input#aplicaCorrecaoMonetariaSim").prop("checked")) {
			$("#idIndiceFinanceiro").prop("disabled", false);	
		}
		
		$("input#aplicaJurosNao").click(function () { 
			animatedcollapse.hide('ConteinerAplicaJuros');
			$("#taxaJuros").val('');			
			document.getElementById("idAmortizacao").selectedIndex = 0;
			document.forms[0].concedeDesconto[1].checked = 'false';
			document.getElementById("idIndiceFinanceiro").selectedIndex = 0;
		});
		
		$("input#aplicaCorrecaoMonetariaSim").click(function () { 
			$("#idIndiceFinanceiro").prop("disabled", false);
		});
		
		$("input#aplicaCorrecaoMonetariaNao").click(function () {
			$("#idIndiceFinanceiro").prop("disabled", true);
			document.getElementById("idIndiceFinanceiro").selectedIndex = 0;
		});
		
		// Datepicker
		$(".campoData").datepicker({minDate: '+0d', changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
	
		$("img").css('marginTop', '30');
	});

	animatedcollapse.addDiv('conteinerDescontoMaximo', 'fade=0,speed=400,hide=1');
	animatedcollapse.addDiv('ConteinerAplicaJuros', 'fade=0,speed=400,hide=1');
	
	function limparFormulario(){
		document.forms[0].descricao.value = "";
		document.forms[0].descricaoAbreviada.value = "";	
		document.forms[0].maximoParcelas.value = "";	
		document.forms[0].idAmortizacao.value = "-1";	
		document.forms[0].concedeDescontoNao.checked = 'false';
		document.forms[0].valorDesconto.value = "";	
		document.forms[0].taxaJuros.value = "";	
		document.forms[0].aplicaJurosSim.checked = 'true';
		document.forms[0].perfilVigenciaInicial.value = "";
		document.forms[0].perfilVigenciaFinal.value = "";
		document.forms[0].aplicaCorrecaoMonetariaNao.checked = 'false';
		document.forms[0].idIndiceFinanceiro.value = '-1';
		animatedcollapse.show('ConteinerAplicaJuros');
		animatedcollapse.hide('conteinerDescontoMaximo');
		$("#idIndiceFinanceiro").prop("disabled", true);

		moveAllOptionsEspecial(document.forms[0].idSegmentosAssociados,document.forms[0].segmentosDisponiveis,true);	
		moveAllOptionsEspecial(document.forms[0].idSituacoesAssociadas,document.forms[0].situacoesDisponiveis,true);
	}	
	function cancelar() {
		location.href = '<c:url value="/exibirPesquisaPerfilParcelamento"/>';
	}

	function incluir(){
		var listaSegmentos = document.getElementById('idSegmentosAssociados');		
		if (listaSegmentos != undefined) {
			for (i=0; i<listaSegmentos.length; i++){
				listaSegmentos.options[i].selected = true;
			}
		}
		var listaSituacoes = document.getElementById('idSituacoesAssociadas');		
		if (listaSituacoes != undefined) {
			for (i=0; i<listaSituacoes.length; i++){
				listaSituacoes.options[i].selected = true;
			}
		}
		
		var taxaJuros = $('#taxaJuros').val();
		$('#taxaJuros').val(taxaJuros.replace(",", "."));
		
		var valorDesconto = $('#valorDesconto').val();
		$('#valorDesconto').val(valorDesconto.replace(",", "."));
		
		submeter('perfilParcelamentoForm', 'incluirPerfilParcelamento');
	}

	function init () {					
		var concedeDesconto = "<c:out value='${perfilParcelamento.indicadorDesconto}'/>";
		if(concedeDesconto == "true"){
			animatedcollapse.show('conteinerDescontoMaximo');						
		} else {
			animatedcollapse.hide('conteinerDescontoMaximo');
		}
		var aplicaJuros = "<c:out value='${perfilParcelamento.indicadorJuros}'/>";
		if(aplicaJuros == "true"){
			animatedcollapse.show('ConteinerAplicaJuros');						
		} else {
			animatedcollapse.hide('ConteinerAplicaJuros');
		}
		
		var aplicaCorrecaoMonetaria = "<c:out value='${perfilParcelamento.indicadorCorrecaoMonetaria}'/>";
		if(aplicaCorrecaoMonetaria == "true"){
			$("#idIndiceFinanceiro").prop("disabled", false);						
		} else {
			$("#idIndiceFinanceiro").prop("disabled", true);
		}

	}

	addLoadEvent(init);


	
</script>

<input name="postBack" type="hidden" id="postBack" value="true">
<input type="hidden" name="habilitado" id="habilitadoSim" value="true" >
<fieldset id="dadosPerfilParcelamento" class="conteinerPesquisarIncluirAlterar">
	<fieldset class="coluna2">
		<label class="rotulo campoObrigatorio" for="descricao"><span class="campoObrigatorioSimbolo">* </span>Descri��o:</label>
		<input class="campoTexto" type="text" id="descricao" name="descricao" maxlength="40" size="40" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${perfilParcelamento.descricao}"><br />
		<label class="rotulo campoObrigatorio" for="descricaoAbreviada"><span class="campoObrigatorioSimbolo">* </span>Descri��o Abreviada:</label>
		<input class="campoTexto" type="text" id="descricaoAbreviada" name="descricaoAbreviada" maxlength="5" size="6" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${perfilParcelamento.descricaoAbreviada}"><br />	
		<label class="rotulo campoObrigatorio" for="maximoParcelas"><span class="campoObrigatorioSimbolo">* </span>N� M�ximo de Parcelas:</label>
		<input class="campoTexto" type="text" id="maximoParcelas" name="maximoParcelas" maxlength="2" size="3"  onkeypress="return formatarCampoInteiro(event,2);" value="${maximoParcelas}"><br />		
		<br/><br/>		

		<label class="rotulo campoObrigatorio" id="rotuloPerfilVigenciaInicial" for="perfilVigenciaInicial"><span class="campoObrigatorioSimbolo">* </span>Vig�ncia inicial:</label>
		<input type="text" id="perfilVigenciaInicial" name="vigenciaInicial" class="campoData" size="10" maxlength="10" value="${vigenciaInicial}"/><br />
		
		<label class="rotulo" id="rotuloPerfilVigenciaFinal" for="perfilVigenciaFinal">Vig�ncia final:</label>
		<input type="text" id="perfilVigenciaFinal" name="vigenciaFinal" class="campoData" size="10" maxlength="10" value="${vigenciaFinal}"/><br />
	</fieldset>
	
	<fieldset class="colunaFinal2">
		<fieldset>
			<label class="rotulo campoObrigatorio"><span class="campoObrigatorioSimbolo">* </span>Concede Desconto:</label>
			<input class="campoRadio" type="radio" name="indicadorDesconto" id="concedeDescontoSim" value="true" <c:if test="${perfilParcelamento.indicadorDesconto eq 'true'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="concedeDescontoSim">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorDesconto" id="concedeDescontoNao" value="false" <c:if test="${perfilParcelamento.indicadorDesconto eq 'false' or empty perfilParcelamento.indicadorDesconto}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="concedeDescontoNao">N�o</label><br class="quebraRadio" />
			<fieldset id="conteinerDescontoMaximo">
				<label class="rotulo campoObrigatorio" for="valorDesconto"><span class="campoObrigatorioSimbolo">* </span>Desconto M�ximo:</label>
				<input class="campoTexto campoHorizontal" type="text" id="valorDesconto" name="valorDesconto" maxlength="9" size="9" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,7,2);" value="${perfilParcelamento.valorDesconto}">
				<label class="rotuloHorizontal rotuloInformativo" for="valorDesconto">%</label>
			</fieldset>
		</fieldset>
		
		<fieldset>
			<label class="rotulo campoObrigatorio" ><span class="campoObrigatorioSimbolo">* </span>Aplica Juros:</label>
			<input class="campoRadio" type="radio" name="indicadorJuros" id="aplicaJurosSim" value="true" <c:if test="${perfilParcelamento.indicadorJuros eq 'true' or empty perfilParcelamento.indicadorJuros}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="aplicaJurosSim">Sim</label>
			<input class="campoRadio" type="radio" name="indicadorJuros" id="aplicaJurosNao" value="false" <c:if test="${perfilParcelamento.indicadorJuros eq 'false'}">checked="checked"</c:if>>
			<label class="rotuloRadio" for="aplicaJurosNao">N�o</label><br class="quebraRadio" />
			<fieldset id="ConteinerAplicaJuros">
				<label class="rotulo campoObrigatorio" for="taxaJuros"><span class="campoObrigatorioSimbolo">* </span>Taxa de Juros:</label>
				<input class="campoTexto campoHorizontal" type="text" name="taxaJuros" id="taxaJuros" maxlength="6" size="9" onblur="aplicarMascaraNumeroDecimal(this,2);" onkeypress="return formatarCampoDecimalPositivo(event,this,3,2);" value="${perfilParcelamento.taxaJuros}">
				<label class="rotuloHorizontal rotuloInformativo" for="taxaJuros">%</label><br class="quebraLinha" />
				<label class="rotulo campoObrigatorio" for="idAmortizacao">Sistema <br/> de Amortiza��o:</label>
				<select name="tipoTabela" id="idAmortizacao" class="campoSelect" style="margin-top: 15px">
			    	<option value="-1">Selecione</option>				
					<c:forEach items="${listaAmortizacao}" var="amortizacao">
						<option value="<c:out value="${amortizacao.chavePrimaria}"/>" <c:if test="${perfilParcelamento.tipoTabela.chavePrimaria == amortizacao.chavePrimaria}">selected="selected"</c:if>>
							<c:out value="${amortizacao.descricao}"/>
						</option>		
					</c:forEach>	
			    </select>				
				
			</fieldset>
				<label class="rotulo rotulo2Linhas" >Aplica Corre��o Monet�ria:</label>
				<input class="campoRadio" type="radio" name="indicadorCorrecaoMonetaria" id="aplicaCorrecaoMonetariaSim" value="true" <c:if test="${perfilParcelamento.indicadorCorrecaoMonetaria eq 'true'}">checked="checked"</c:if>>
				<label class="rotuloRadio">Sim</label>
				<input class="campoRadio" type="radio" name="indicadorCorrecaoMonetaria" id="aplicaCorrecaoMonetariaNao" value="false" <c:if test="${perfilParcelamento.indicadorCorrecaoMonetaria eq 'false' or empty perfilParcelamento.indicadorCorrecaoMonetaria}">checked="checked"</c:if>>
				<label class="rotuloRadio">N�o</label><br class="quebraRadio" />
				
					<label class="rotulo rotulo2Linhas campoObrigatorio"><span class="campoObrigatorioSimbolo2">* </span>�ndice Financeiro:</label>
					<select name="indiceFinanceiro" id="idIndiceFinanceiro" class="campoSelect" disabled="disabled">
				    	<option value="-1">Selecione</option>				
						<c:forEach items="${listaIndiceFinanceiro}" var="indiceFinanceiro">
							<option value="<c:out value="${indiceFinanceiro.chavePrimaria}"/>" <c:if test="${perfilParcelamento.indiceFinanceiro.chavePrimaria == indiceFinanceiro.chavePrimaria}">selected="selected"</c:if>>
								<c:out value="${indiceFinanceiro.descricao}"/>
							</option>		
						</c:forEach>	
				    </select>
								
			
		</fieldset>
		
	</fieldset>
	
	<p class="legenda"><span class="campoObrigatorioSimbolo">* </span>campos obrigat�rios para Inclus�o de Perfil de Parcelamento.</p>
	
	<hr class="linhaSeparadora"/>
	
	<!-- Associa��o Segmentos-->
    <fieldset class="conteinerBloco">
    	<fieldset class="conteinerLista1">
		    <legend class="conteinerBlocoTitulo">Segmentos</legend>
			<label class="rotulo rotuloVertical" id="rotuloSegmentosDisponiveis" for="segmentosDisponiveis">Dispon�veis:</label><br />
			<select id="segmentosDisponiveis" class="campoList listaCurta1a campoVertical" multiple="multiple"
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].idSegmentosAssociados,true);">
				<c:forEach items="${listaSegmentos}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" title="<c:out value="${segmento.descricao}"/>"> <c:out value="${segmento.descricao}"/></option>		
				</c:forEach>
			</select>
			
			<fieldset class="conteinerBotoesCampoList1a">
				<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" 
					onClick="moveSelectedOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].idSegmentosAssociados,true);">
				<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].segmentosDisponiveis,document.forms[0].idSegmentosAssociados,true);">
				<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
					onClick="moveSelectedOptionsEspecial(document.forms[0].idSegmentosAssociados,document.forms[0].segmentosDisponiveis,true);">
				<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].idSegmentosAssociados,document.forms[0].segmentosDisponiveis,true);">
			</fieldset>
		</fieldset>
		
		<fieldset class="camposSelecionados">
			<label id="rotuloSegmentosSelecionados" class="rotulo rotuloListaLonga2" for="idSegmentosAssociados"><span class="campoObrigatorioSimbolo">* </span>Associados:</label><br />
			<select id="idSegmentosAssociados" class="campoList listaCurta2a campoVertical" name="idSegmentosAssociados" multiple="multiple" 
				onDblClick="moveSelectedOptionsEspecial(document.forms[0].idSegmentosAssociados,document.forms[0].segmentosDisponiveis,true);">
				<c:forEach items="${listaSegmentosAssociados}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" title="<c:out value="${segmento.descricao}"/>"><c:out value="${segmento.descricao}"/></option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
	<br /><br />
	<!-- Associa��o Situa��o Ponto Consumo-->
    <fieldset class="conteinerBloco">
    	<fieldset class="conteinerLista1">
		    <legend class="conteinerBlocoTitulo">Situa��es de Ponto de Consumo</legend>
			<label class="rotulo rotuloVertical" id="rotuloSituacoesDisponiveis" for="situacoesDisponiveis">Dispon�veis:</label><br />
			<select id="situacoesDisponiveis" class="campoList listaCurta1a campoVertical" multiple="multiple"
			onDblClick="moveSelectedOptionsEspecial(document.forms[0].situacoesDisponiveis,document.forms[0].idSituacoesAssociadas,true);">
				<c:forEach items="${listaSituacoes}" var="situacao">
					<option value="<c:out value="${situacao.chavePrimaria}"/>" title="<c:out value="${situacao.descricao}"/>"> <c:out value="${situacao.descricao}"/></option>		
				</c:forEach>
			</select>
			
			<fieldset class="conteinerBotoesCampoList1a">
				<input type="button" name="right" value="&gt;&gt;" class="bottonRightCol" id="botaoDireita" 
					onClick="moveSelectedOptionsEspecial(document.forms[0].situacoesDisponiveis,document.forms[0].idSituacoesAssociadas,true);">
				<input type="button" name="right" value="Todos &gt;&gt;" class="bottonRightCol botoesLargosIE7" id="botaoDireitaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].situacoesDisponiveis,document.forms[0].idSituacoesAssociadas,true);">
				<input type="button" name="left" value="&lt;&lt;" class="bottonRightCol botoesCampoListDir" id="botaoDirTop"
					onClick="moveSelectedOptionsEspecial(document.forms[0].idSituacoesAssociadas,document.forms[0].situacoesDisponiveis,true);">
				<input type="button" name="left" value="Todos &lt;&lt;" class="bottonRightCol botoesCampoListDir botoesLargosIE7" id="botaoEsquerdaTodos"
					onClick="moveAllOptionsEspecial(document.forms[0].idSituacoesAssociadas,document.forms[0].situacoesDisponiveis,true);">
			</fieldset>
		</fieldset>
		
		<fieldset class="camposSelecionados">
			<label id="rotuloSituacoesSelecionados" class="rotulo rotuloListaLonga2" for="idSituacoesAssociadas"><span class="campoObrigatorioSimbolo">* </span>Associados:</label><br />
			<select id="idSituacoesAssociadas" class="campoList listaCurta2a campoVertical" name="idSituacoesAssociadas" multiple="multiple" 
				onDblClick="moveSelectedOptionsEspecial(document.forms[0].idSituacoesAssociadas,document.forms[0].situacoesDisponiveis,true);">
				<c:forEach items="${listaSituacoesAssociados}" var="situacao">
					<option value="<c:out value="${situacao.chavePrimaria}"/>" title="<c:out value="${situacao.descricao}"/>"><c:out value="${situacao.descricao}"/></option>
				</c:forEach>
			</select>
		</fieldset>
	</fieldset>
</fieldset>
  
<fieldset class="conteinerBotoes"> 
    <input name="Button" class="bottonRightCol2" value="Cancelar" type="button" onclick="cancelar();">
    <input name="Button" class="bottonRightCol2 bottonLeftColUltimo" value="Limpar" type="button" onclick="limparFormulario();">
    <vacess:vacess param="incluirPerfilParcelamento">
    	<input name="button" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" value="Incluir" type="button" onclick="incluir();">
    </vacess:vacess>
</fieldset>
<token:token></token:token>
</form:form> 