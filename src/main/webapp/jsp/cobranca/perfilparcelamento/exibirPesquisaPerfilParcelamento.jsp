<!--
 Copyright (C) <2011> GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s

 Este programa � um software livre; voc� pode redistribu�-lo e/ou
 modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 publicada pela Free Software Foundation; vers�o 2 da Licen�a.

 O GGAS � distribu�do na expectativa de ser �til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.

 Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 junto com este programa; se n�o, escreva para Free Software	
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS - Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/help/help.tld" prefix="help" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ taglib uri="/WEB-INF/controleacesso/vacess.tld" prefix="vacess" %>



<h1 class="tituloInterno">Pesquisar Perfis de Parcelamento<a class="linkHelp" href="<help:help>/consultaperfisdeparcelamento.htm</help:help>" target="right" onclick="exibirJDialog('#janelaHelp');"></a></h1>
<p class="orientacaoInicial">Para pesquisar um registro espec�fico, informe os dados nos campos abaixo e clique em <span class="destaqueOrientacaoInicial">Pesquisar</span>, ou clique apenas em <span class="destaqueOrientacaoInicial">Pesquisar</span> para exibir todos.<br />
Para incluir um novo modelo clique em <span class="destaqueOrientacaoInicial">Incluir</span></p>

<form:form method="post" action="pesquisarPerfilParcelamento" name="perfilParcelamentoForm">

	<script language="javascript">

		$(document).ready(function(){	
			// Datepicker
		   	$(".campoData").datepicker({changeYear: true, showOn: 'button', buttonImage: '<c:url value="/imagens/calendario.gif"/>', buttonImageOnly: true, buttonText: 'Exibir Calend�rio', dateFormat: 'dd/mm/yy'});
		});
		
	
		function limparFormulario() {
			$(":text").val("");
			$("select").val("-1");
			$("#habilitadoAtivo").attr("checked","checked");
		}	
			
		function incluir() {		
			location.href = '<c:url value="/exibirInclusaoPerfilParcelamento"/>';
		}
		
		function alterarPerfilParcelamento(chave) {
			var selecao = verificarSelecaoApenasUm();
			var vigenciaInicial = $('#vigenciaInicial').val();
			var vigenciaFinal = $('#vigenciaFinal').val();
			$('#vigenciaInicialHidden').val(vigenciaInicial);
			$('#vigenciaFinalHidden').val(vigenciaFinal);
			if (selecao == true) {	
				document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
				submeter("perfilParcelamentoForm", "exibirAlteracaoPerfilParcelamento");
		    }
		}
		
		function detalharPerfilParcelamento(chave){
			document.forms[0].chavePrimaria.value = chave;
			submeter("perfilParcelamentoForm", "exibirDetalhamentoPerfilParcelamento");
		}	
			
		function removerPerfilParcelamento(){		
			var selecao = verificarSelecao();
			if (selecao == true) {
				var retorno = confirm('<fmt:message key="PERGUNTA_ACAO_EXCLUIR"/>');
				if(retorno == true) {
					submeter('perfilParcelamentoForm', 'excluirPerfilParcelamento');
				}
	    	}
		}	
		
	</script>
	
	<input name="acao" type="hidden" id="acao" value="pesquisarPerfilParcelamento">
	<input name="chavePrimaria" type="hidden" id="chavePrimaria" >
	<input name="vigenciaInicialHidden" type="hidden" id="vigenciaInicialHidden" value="${vigenciaInicialHidden}">
	<input name="vigenciaFinalHidden" type="hidden" id="vigenciaFinalHidden" value="${vigenciaFinalHidden}">

	<fieldset id="pesquisarPerfilParcelamento" class="conteinerPesquisarIncluirAlterar">
		<fieldset class="coluna">
			<label class="rotulo" for="descricao" >Descri��o:</label>
			<input class="campoTexto" id="descricaoModelo" type="text" name="descricao" maxlength="40" size="41" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${perfilParcelamento.descricao}" /><br />
			
			<label class="rotulo" for="descricaoAbreviada" >Descri��o Abreviada:</label>
			<input class="campoTexto" id="descricaoAbreviada" type="text" name="descricaoAbreviada" maxlength="20" size="20" onkeyup="return validarCriteriosParaCampo(this, '<c:out value="${isCaixaAlta}"/>', '<c:out value="${isPermiteCaracteresEspeciais}"/>', 'formatarCampoNome(event)');" value="${perfilParcelamento.descricaoAbreviada}" ><br />
			<br/><br/>
		
			<label class="rotulo" id="rotuloPerfilVigenciaInicialPesquisa" for="perfilVigenciaInicialPesquisa">Vig�ncia inicial:</label>
			<input type="text" id="perfilVigenciaInicialPesquisa" name="vigenciaInicial" class="campoData" size="10" maxlength="10" value="${vigenciaInicial}"/><br />
			<br/>
			
			<label class="rotulo" id="rotuloPerfilVigenciaFinalPesquisa" for="perfilVigenciaFinalPesquisa">Vig�ncia final:</label>
			<input type="text" id="perfilVigenciaFinalPesquisa" name="vigenciaFinal" class="campoData" size="10" maxlength="10" value="${vigenciaFinal}"/>
		</fieldset>
			
		<fieldset class="colunaFinal">
			<label class="rotulo" for="idSegmento">Segmento:</label>
			<select name="idSegmento" id="idSegmento" class="campoSelect">
		    	<option value="-1">Selecione</option>				
				<c:forEach items="${listaSegmentos}" var="segmento">
					<option value="<c:out value="${segmento.chavePrimaria}"/>" <c:if test="${idSegmento == segmento.chavePrimaria}">selected="selected"</c:if>>
						<c:out value="${segmento.descricao}"/>
					</option>		
				</c:forEach>	
		    </select><br /><br />
		
			<label class="rotulo rotuloHorizontal" for="habilitado">Indicador de Uso:</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoAtivo" value="true" <c:if test="${habilitado eq 'true'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitadoAtivo">Ativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitadoInativo" value="false" <c:if test="${habilitado eq 'false'}">checked</c:if>>
			<label class="rotuloRadio" for="habilitadoInativo">Inativo</label>
			<input class="campoRadio" type="radio" name="habilitado" id="habilitado" value="" <c:if test="${empty habilitado}">checked</c:if>>
			<label class="rotuloRadio" for="habilitado">Todos</label><br class="quebraLinha"/>
		</fieldset>	
		
	    <fieldset class="conteinerBotoesPesquisarDirFixo">
	    	<vacess:vacess param="pesquisarPerfilParcelamento">
	    		<input name="Button" class="bottonRightCol2" id="botaoPesquisar" value="Pesquisar" type="submit">
	    	</vacess:vacess>		
			<input name="Button" class="bottonRightCol2 bottonRightColUltimo" value="Limpar" type="button" onclick="limparFormulario();">		
		</fieldset>
	</fieldset>
	
	<c:if test="${listaPerfil ne null}">
		<hr class="linhaSeparadoraPesquisa" />
		<display:table class="dataTableGGAS dataTableCabecalho2Linhas" name="listaPerfil" sort="list" id="perfilParcelamento" decorator="br.com.ggas.util.DisplayTagGenericoDecorator" pagesize="15" excludedParams="org.apache.struts.taglib.html.TOKEN acao" requestURI="pesquisarPerfilParcelamento">
	        <display:column style="width: 25px" sortable="false" title="<input type='checkbox' name='checkAllAuto' id='checkAllAuto'/>"> 
      			<input type="checkbox" name="chavesPrimarias" value="${perfilParcelamento.chavePrimaria}">
	     	</display:column>
	     	<display:column style="width: 30px" title="Ativo">
		     	<c:choose>
					<c:when test="${perfilParcelamento.habilitado == true}">
						<img  alt="Ativo" title="Ativo" src="<c:url value="/imagens/success_icon.png"/>" border="0">
					</c:when>
					<c:otherwise>
						<img alt="Inativo" title="Inativo" src="<c:url value="/imagens/cancel16.png"/>" border="0">
					</c:otherwise>				
				</c:choose>
			 </display:column>
	     	
	     	
	        <display:column sortable="true" sortProperty="descricao" title="Descri��o" headerClass="tituloTabelaEsq" style="text-align: left">
	            <a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${perfilParcelamento.descricao}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="descricaoAbreviada" title="Descri��o Abreviada" headerClass="tituloTabelaEsq" style="text-align: left; width: 180px">
	        	<a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${perfilParcelamento.descricaoAbreviada}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="maximoParcelas" title="M�x. de<br />Parcelas" style="width: 80px">
	        	<a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${perfilParcelamento.maximoParcelas}"/>
	            </a>
	        </display:column>
	        <display:column sortable="true" sortProperty="tipoTabela.descricao" title="Amortiza��o" style="width: 100px">
	        	<a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<c:out value="${perfilParcelamento.tipoTabela.descricao}"/>
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="vigenciaInicial" title="Vig�ncia<br />Inicial" style="width: 80px">
	        	<a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<fmt:formatDate value="${perfilParcelamento.vigenciaInicial}" type="both" pattern="dd/MM/yyyy" />
	            </a>
	        </display:column>
	        
	        <display:column sortable="true" sortProperty="vigenciaFinal" title="Vig�ncia<br />Final" style="width: 80px">
	        	<a href="javascript:detalharPerfilParcelamento(<c:out value='${perfilParcelamento.chavePrimaria}'/>);"><span class="linkInvisivel"></span>
	            	<fmt:formatDate value="${perfilParcelamento.vigenciaFinal}" type="both" pattern="dd/MM/yyyy" />
	            </a>
	        </display:column>
	    </display:table>
	</c:if>
	
	<fieldset class="conteinerBotoes">
    	<c:if test="${not empty listaPerfil}">
    		<vacess:vacess param="exibirAlteracaoPerfilParcelamento">
   				<input name="buttonAlterar" value="Alterar" class="bottonRightCol2" id="botaoAlterar" onclick="alterarPerfilParcelamento();" type="button">
   			</vacess:vacess>
   			<vacess:vacess param="excluirPerfilParcelamento">
				<input name="buttonRemover" value="Remover" class="bottonRightCol2 bottonLeftColUltimo" onclick="removerPerfilParcelamento()" type="button">
			</vacess:vacess>
		</c:if>
		<vacess:vacess param="exibirInclusaoPerfilParcelamento">
			<input name="button" value="Incluir" class="bottonRightCol2 botaoGrande1" id="botaoIncluir" onclick="incluir();" type="button">
		</vacess:vacess>
	</fieldset>
	    
</form:form>



	