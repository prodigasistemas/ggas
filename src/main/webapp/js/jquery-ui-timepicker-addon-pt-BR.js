$.timepicker.regional['pt-BR'] = {
		timeOnlyTitle: 'Escolha a hora',
		timeText: 'Hor&aacuterio',
		hourText: 'Hora(s)',
		minuteText: 'Minuto(s)',
		secondText: 'Segundo(s)',
		millisecText: 'Milissegundo(s)',
		timezoneText: 'Fuso Horário',
		currentText: 'Agora',
		closeText: 'Selecionar',
		timeFormat: 'HH:mm',
		amNames: ['AM', 'A'],
		pmNames: ['PM', 'P'],
		format:'DD/MM/YYYY HH:mm',
		isRTL: false
	};
	$.timepicker.setDefaults($.timepicker.regional['pt-BR']);