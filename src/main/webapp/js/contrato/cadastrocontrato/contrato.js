/*
 * Arquivos contendo as fun��es javascript utilizadas nas funcionalidades de contrato 
 * de forma igual.
 * 
 */


function habilitarChecksPontoConsumo(){
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	var existeSelecionado = false;
	for(var i=0; i<pontosConsumo.length; i++){			
		if(document.getElementById(pontosConsumo[i].id).checked){
			existeSelecionado = true
			break;
		}	
	}

	if(!existeSelecionado){
		for(var i=0; i<pontosConsumo.length; i++){			
			document.getElementById(pontosConsumo[i].id).disabled = false;	
		}
		document.getElementById("chaveSegmentoSelecionado").value = '';
		document.getElementById("chaveRamoAtividadeSelecionado").value = '';
	}
}

function desabilitarChecksPontoConsumo(/* id do elemento chave da prim�ria do ponto de consumo*/ chavePontoConsumo){
	var idSegmentoSelecionado = obterChaveSegmentoPontoConsumo(chavePontoConsumo);
			
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	for(var i=0; i<pontosConsumo.length; i++){			
		if(obterChaveSegmentoPontoConsumo(pontosConsumo[i].id) != idSegmentoSelecionado){
			document.getElementById(pontosConsumo[i].id).disabled = true;
		} else {
			document.getElementById(pontosConsumo[i].id).disabled = false;
		}
	} 
}

function qtdePontosConsumoSelecionado(){
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	var retorno = 0;
	for(var i=0; i<pontosConsumo.length; i++){			
		if(document.getElementById(pontosConsumo[i].id).checked){
			retorno++;
		}
	} 
	return retorno;
}

function obterChaveSegmentoPontoConsumo(/* chave prim�ria do ponto de consumo*/ chavePontoConsumo){
	var idSegmentoSelecionado = 0;
	var idChaveSegmento = chavePontoConsumo.replace('chavePrimariaPontoConsumo','chavePrimariaSegmento');
	alert(idChaveSegmento);
	idSegmentoSelecionado = document.getElementById(idChaveSegmento).value;			
	return idSegmentoSelecionado;
}

function obterChaveRamoAtividadePontoConsumo(/* chave prim�ria do ponto de consumo*/ chavePontoConsumo){
	var idRamoAtividadeSelecionado = 0;
	var idChaveRamoAtividade = chavePontoConsumo.replace('chavePrimariaPontoConsumo','chavePrimariaRamoAtividade');
	idRamoAtividadeSelecionado = document.getElementById(idChaveRamoAtividade).value;		
	return idRamoAtividadeSelecionado;
}

function carregarFaixasPressaoPorSegmento(){
	var idPontoConsumoSelecionado = document.getElementById('idPontoConsumo').value;
	var chaveSegmentoSelecionado = 0;
		
	if((idPontoConsumoSelecionado != undefined) && (idPontoConsumoSelecionado != '')){
		chaveSegmentoSelecionado = chaveSegmentoSelecionado = obterChaveSegmentoPontoConsumo('chavePrimariaPontoConsumo'+idPontoConsumoSelecionado);
	}
		
	var selectFaixasPressao = document.getElementById("faixaPressaoFornecimento");
    
	selectFaixasPressao.length=0;
  	var novaOpcao = new Option("Selecione","-1");
  	selectFaixasPressao.options[selectFaixasPressao.length] = novaOpcao;

  	var faixaPressaoSelecionada = '';
  	var idFaixaPressaoFornecimento = document.getElementById('idFaixaPressaoFornecimento');
  	if(idFaixaPressaoFornecimento != null && idFaixaPressaoFornecimento.value != "") {
  		faixaPressaoSelecionada = idFaixaPressaoFornecimento.value;
  	}
	if(chaveSegmentoSelecionado > 0){
		AjaxService.listarFaixasPressaoFornecimentoPorSegmento( chaveSegmentoSelecionado, {
			callback: function(faixas) {            		      		         		
            	for (key in faixas){
                	var novaOpcao = new Option(faixas[key], key);
                	selectFaixasPressao.options[selectFaixasPressao.length] = novaOpcao;                    	
                }
            	if(faixaPressaoSelecionada != ''){
            		selectFaixasPressao.value = faixaPressaoSelecionada;      
            		if(funcaoExiste('carregarFatorUnicoCorrecao')) {
        				carregarFatorUnicoCorrecao();
        			}          		                		
                }
            }, async:false}
        );
	} else {
		if(funcaoExiste('carregarFatorUnicoCorrecao')) {
			carregarFatorUnicoCorrecao();
		}
	}
}

function carregarSugestaoAmostragemPCSPorRamoAtividade(){
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	var selectAmostragensPCSDisponiveis = document.getElementById("locaisAmostragemDisponiveis");
	var selectAmostragensPCSAssociados = document.getElementById("idsLocalAmostragemAssociados");

	var listarPorRamoSegmento = false;		
	var chaveRamoAtividadeSelecionado = document.getElementById("chaveRamoAtividadeSelecionado").value;
	var chaveSegmentoSelecionado = 0;
	
	if(existeOutroRamoAtividadeSelecionado(chaveRamoAtividadeSelecionado)){			
		listarPorRamoSegmento = true;									
	}
			
	if(chaveRamoAtividadeSelecionado > 0 && !listarPorRamoSegmento){
		listarPorRamoSegmento = true;
		AjaxService.listarAmostragensPCSPorRamoAtividade( chaveRamoAtividadeSelecionado, { 
				callback: function(amostragens) {
          			moveAllOptionsEspecial(selectAmostragensPCSAssociados,selectAmostragensPCSDisponiveis,true);            		      		         		
                	for (key in amostragens){
                		listarPorRamoSegmento = false;
                    	for (var i=0; i < selectAmostragensPCSDisponiveis.length; i++){                        	
							if(key == selectAmostragensPCSDisponiveis.options[i].value){		
								// Retira dos dispon�veis 
								selectAmostragensPCSDisponiveis.options[i].selected = true;
								removeSelectedOptions(selectAmostragensPCSDisponiveis);

								// Acrescenta nos dispon�veis
								var novaOpcao = new Option(amostragens[key], key);
								selectAmostragensPCSAssociados.options[selectAmostragensPCSAssociados.length] = novaOpcao; 
								break;
							} else {
								selectAmostragensPCSDisponiveis.options[i].selected = false;
							}               		
                    	}
                    }
            }, async:false}
        );
	}

	if(listarPorRamoSegmento){
		chaveSegmentoSelecionado = document.getElementById("chaveSegmentoSelecionado").value;
		AjaxService.listarAmostragensPCSPorSegmento( chaveSegmentoSelecionado, { 
			callback: function(amostragens) {
      			moveAllOptionsEspecial(selectAmostragensPCSAssociados,selectAmostragensPCSDisponiveis,true);            		      		         		
            	for (key in amostragens){                		
                	for (var i=0; i < selectAmostragensPCSDisponiveis.length; i++){                        	
						if(key == selectAmostragensPCSDisponiveis.options[i].value){		
							// Retira dos dispon�veis 
							selectAmostragensPCSDisponiveis.options[i].selected = true;
							removeSelectedOptions(selectAmostragensPCSDisponiveis);

							// Acrescenta nos dispon�veis
							var novaOpcao = new Option(amostragens[key], key);
							selectAmostragensPCSAssociados.options[selectAmostragensPCSAssociados.length] = novaOpcao; 
							break;
						} else {
							selectAmostragensPCSDisponiveis.options[i].selected = false;
						}               		
                	}
                }
            }, async:false}
        );
	}	
}

function carregarSugestaoIntervalosPCSPorRamoAtividade(){
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	var selectIntervalosPCSDisponiveis = document.getElementById("intervalosAmostragemDisponiveis");
	var selectIntervalosPCSAssociados = document.getElementById("idsIntervaloAmostragemAssociados");

	var listarPorRamoSegmento = false;		
	var chaveRamoAtividadeSelecionado = document.getElementById("chaveRamoAtividadeSelecionado").value;
	var chaveSegmentoSelecionado = 0;
	
	if(existeOutroRamoAtividadeSelecionado(chaveRamoAtividadeSelecionado)){			
		listarPorRamoSegmento = true;									
	}
			
	if(chaveRamoAtividadeSelecionado > 0 && !listarPorRamoSegmento){
		listarPorRamoSegmento = true;
		AjaxService.listarIntervalosPCSPorRamoAtividade( chaveRamoAtividadeSelecionado, { 
				callback: function(intervalos) {
          			moveAllOptionsEspecial(selectIntervalosPCSAssociados,selectIntervalosPCSDisponiveis,true);            		      		         		
                	for (key in intervalos){
                		listarPorRamoSegmento = false;
                    	for (var i=0; i < selectIntervalosPCSDisponiveis.length; i++){                        	
							if(key == selectIntervalosPCSDisponiveis.options[i].value){		
								// Retira dos dispon�veis 
								selectIntervalosPCSDisponiveis.options[i].selected = true;
								removeSelectedOptions(selectIntervalosPCSDisponiveis);

								// Acrescenta nos dispon�veis
								var novaOpcao = new Option(intervalos[key], key);
								selectIntervalosPCSAssociados.options[selectIntervalosPCSAssociados.length] = novaOpcao; 
								break;
							} else {
								selectIntervalosPCSDisponiveis.options[i].selected = false;
							}               		
                    	}
                    }
            }, async:false}
        );
	}

	if(listarPorRamoSegmento){
		chaveSegmentoSelecionado = document.getElementById("chaveSegmentoSelecionado").value;
		AjaxService.listarIntervalosPCSPorSegmento( chaveSegmentoSelecionado, { 
			callback: function(intervalos) {
      			moveAllOptionsEspecial(selectIntervalosPCSAssociados,selectIntervalosPCSDisponiveis,true);            		      		         		
            	for (key in intervalos){                		
                	for (var i=0; i < selectIntervalosPCSDisponiveis.length; i++){                        	
						if(key == selectIntervalosPCSDisponiveis.options[i].value){		
							// Retira dos dispon�veis 
							selectIntervalosPCSDisponiveis.options[i].selected = true;
							removeSelectedOptions(selectIntervalosPCSDisponiveis);

							// Acrescenta nos dispon�veis
							var novaOpcao = new Option(intervalos[key], key);
							selectIntervalosPCSAssociados.options[selectIntervalosPCSAssociados.length] = novaOpcao; 
							break;
						} else {
							selectIntervalosPCSDisponiveis.options[i].selected = false;
						}               		
                	}
                }
            }, async:false}
        );
	}	
}

function existeOutroRamoAtividadeSelecionado(chaveRamoAtividadeSelecionado){
	var pontosConsumo = document.getElementsByName('chavesPrimariasPontoConsumo');
	var existe = false;
	for(var i=0; i<pontosConsumo.length; i++){			
		if(document.getElementById(pontosConsumo[i].id).checked 
				&&  chaveRamoAtividadeSelecionado != obterChaveRamoAtividadePontoConsumo(pontosConsumo[i].id)) {
			existe = true;
			break;
		}	
	}
	return existe;
}

function mostrarAsterisco(valor,nomeCampo) {
	var underline = "_";
	if (valor != undefined) {
		for (var i = 0; i < nomeCampo.length; i++) {
			var descricaoLabel = $("label[for="+nomeCampo[i]+"]").html();
			// Se o campo pai tem algum valor.
			if (valor > 0 || valor == "true" || valor == "false" || (valor != "" && valor.indexOf(underline) < 0)) {
				// Se o campo pai mudar de valor Nao adiciona outro asterisco, pois j� existe.
				if ($("#span"+nomeCampo[i]).length == 0) {
					var span = "<span id='span"+ nomeCampo[i] +"' class='campoObrigatorioSimbolo2'>* </span>";
					$("label[for="+nomeCampo[i]+"]").html(span + descricaoLabel);
					$("label[for="+nomeCampo[i]+"]").addClass("campoObrigatorio");
				}

			// Se o campo pai Nao tem valor.
			} else {
				var classe = $("#span"+nomeCampo[i]).attr("class");
				// S� remove caso o label tenha asterisco preto.
				if (classe == "campoObrigatorioSimbolo2") {
					$("#span"+nomeCampo[i]).remove();
					$("label[for="+nomeCampo[i]+"]").removeClass("campoObrigatorio");
				}
			}
		}
	}
}