	function permitirSelecaoObrigatorio(campo1, campo2){
		var campoSel = eval('document.forms[0].' + campo1);
		var campoObg = eval('document.forms[0].' + campo2);
		if (campoSel.type == 'checkbox' && campoSel.checked == false) { 
			if (campoObg.type == 'checkbox') {
				campoObg.checked = false
			}	
		}
	}
	
	function verificarSelecaoObrigatorio(campo1, campo2){
		var campoSel = eval('document.forms[0].' + campo2);
		var campoObg = eval('document.forms[0].' + campo1);
		if (campoSel.type == 'checkbox' && campoSel.checked == true) {
			return true 
		} else {
			if (campoObg.type == 'checkbox') {
				campoObg.checked = false
			}
		}
	}

	function adicionarEvento( obj, type, fn ) {
		  if ( obj.attachEvent ) {
		    obj['e'+type+fn] = fn;
		    obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
		    obj.attachEvent( 'on'+type, obj[type+fn] );
		  } else
		    obj.addEventListener( type, fn, false );
		}
	
	function marcarDesmarcarCheckBoxsObrigatoriedade(camposSelecionados, camposObrigatorios, marcarTodos){
		for (var i = 0; i < camposSelecionados.length; i++) {
			var campoSelecionado = document.getElementById(camposSelecionados[i]).checked;	
			if(document.getElementById(camposObrigatorios[i]) != null){		
				if (campoSelecionado != null && campoSelecionado == true && marcarTodos == true) {				
					document.getElementById(camposObrigatorios[i]).checked = true;
				} else {
					document.getElementById(camposObrigatorios[i]).checked = false;
				}			
			}
		}	
	}