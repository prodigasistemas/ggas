$(document).ready(() => {
    converterCampoCaixaAlta();
});


function converterCampoCaixaAlta() {
    $("form input[type='text'], form textarea.form-control").on("keyup", function(event) {
        var start = event.target.selectionStart;
        var end = event.target.selectionEnd;
        $(this).val(event.target.value.toUpperCase());
        event.target.setSelectionRange(start, end);
    });
}