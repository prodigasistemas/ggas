var elementosClicados = [];
$(document).ready(function () {
    $(".campoData").datepicker({
        changeYear: true,
        maxDate: '+0d',
        showOn: 'button',
        buttonImage: $('#imagemCalendario').val(),
        buttonImageOnly: true,
        buttonText: 'Exibir Calend�rio',
        dateFormat: 'dd/mm/yy'
    });

    // Dialog
    $("#dialogGerarAutorizacaoServico").dialog({
        autoOpen: false,
        modal: true,
        width: 1000,
        resizeble: false,
        draggable: false
    });

    $("#dialogAlterarPrevisaoEncerramento").dialog({
        autoOpen: false,
        modal: true,
        width: 1000,
        resizeble: false,
        draggable: false
    });

    $("#dialog-confirm").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Sim": function () {
                $(this).dialog("close");
                $("#comAS").val("true");
                submeter('chamadoForm', 'imprimirChamado');
            },
            "Nao": function () {
                $(this).dialog("close");
                $("#comAS").val("false");
                submeter('chamadoForm', 'imprimirChamado');
            }
        }
    });

    $("#cpfCliente").inputmask("999.999.999-99", {placeholder: "_"});
    $("#cnpjCliente").inputmask("99.999.999/9999-99", {placeholder: "_"});
    $("#cpfSolicitante").inputmask("999.999.999-99", {placeholder: "_"});
    $("#cnpjSolicitante").inputmask("99.999.999/9999-99", {placeholder: "_"});

    desabilitarCpfCnpj();

    $("#buttonAlterarCadastroCliente").prop("disabled", true);
    $("#buttonAlterarDataVencimento").prop("disabled", true);
    $("#cepPontoConsumoChamado").inputmask("99999-999", {placeholder: "_"});

    var siglaUnidadeFederativa = $("#chamadoVO.siglaUnidadeFederacaoPontoConsumoChamado").val();
    carregarMunicipios(siglaUnidadeFederativa);
    var nomeMunicipio = $("#chamadoVO.nomeMunicipioPontoConsumoChamado").val();
    carregarBairros(nomeMunicipio);
    var nomeBairro = $("#chamadoVO.bairroPontoConsumoChamado").val();
    $("#nomeMunicipioPontoConsumoChamado").val(nomeMunicipio);
    $("#bairroPontoConsumoChamado").val(nomeBairro);

    var chaveChamadoSegmento = $("#chamadoVO.idSegmentoChamado").val();
    $("#idSegmentoChamado").val(chaveChamadoSegmento);

    var chaveChamadoTipo = $("#idChamadoTipo").val();
    carregarChamadoAssunto(chaveChamadoTipo);

    restaurarEstadoFiltros();

    $('input[name=chavesPrimarias]').change(function() {

        var elementoClicado = {
            'elemento': $(this).attr('id'),
            'id': $(this).val(),
            'permiteAbrirEmLote': $(this).data('permite-abrir-em-lote'),
            'status': $(this).data('status'),
            'imovelPai': $(this).data('imovel-pai'),
            'marcado': this.checked
        };
        var adicionado = false;
        for (var i in elementosClicados) {
            if (elementosClicados[i].id === elementoClicado.id) {
                elementosClicados[i] = elementoClicado;
                adicionado = true;
                break;
            }
        }
        if (!adicionado) {
            elementosClicados.push(elementoClicado);
        }
    });
});

function retiraMascara() {
    var cpfSolicitante = document.forms[0].cpfSolicitante.value;
    if (cpfSolicitante != null && cpfSolicitante.indexOf(".") != -1 && cpfSolicitante.indexOf("-") != -1) {
        cpfSolicitante = cpfSolicitante.replace(/\./g, "")
        document.forms[0].cpfSolicitante.value = cpfSolicitante.replace("-", "")
    }
}

function pesquisar() {
    salvarEstadosFiltros();
    retiraMascara();
    verificarTipoPessoa();
    submeter('chamadoForm', 'pesquisarChamado');
}

function salvarEstadosFiltros() {
    try {
        sessionStorage.setItem('aba-chamado', $('#aba-chamado').attr('class'));
        sessionStorage.setItem('caret-aba-chamado', $('#caret-aba-chamado').attr('class'));

        sessionStorage.setItem('aba-pessoa-fisica', $('#aba-pessoa-fisica').attr('class'));
        sessionStorage.setItem('caret-aba-pessoa-fisica', $('#caret-aba-pessoa-fisica').attr('class'));

        sessionStorage.setItem('aba-pessoa-juridica', $('#aba-pessoa-juridica').attr('class'));
        sessionStorage.setItem('caret-aba-pessoa-juridica', $('#caret-aba-pessoa-juridica').attr('class'));

        sessionStorage.setItem('aba-localizacao-ponto-consumo', $('#aba-localizacao-ponto-consumo').attr('class'));
        sessionStorage.setItem('caret-aba-localizacao-ponto-consumo', $('#caret-aba-localizacao-ponto-consumo').attr('class'));

        sessionStorage.setItem('aba-imovel', $('#aba-imovel').attr('class'));
        sessionStorage.setItem('caret-aba-imovel', $('#caret-aba-imovel').attr('class'));

        sessionStorage.setItem('aba-contrato', $('#aba-contrato').attr('class'));
        sessionStorage.setItem('caret-aba-contrato', $('#caret-aba-contrato').attr('class'));
    } catch (e) {
        console.error('browser Nao suporte session storage');
    }
}

function restaurarEstadoFiltros() {
    try {
        if (sessionStorage.getItem('aba-chamado') !== null) {
            $('#aba-chamado').attr('class', sessionStorage.getItem('aba-chamado'));
            $('#caret-aba-chamado').attr('class', sessionStorage.getItem('caret-aba-chamado'));
        }
        if (sessionStorage.getItem('aba-pessoa-fisica') !== null) {
            $('#aba-pessoa-fisica').attr('class', sessionStorage.getItem('aba-pessoa-fisica'));
            $('#caret-aba-pessoa-fisica').attr('class', sessionStorage.getItem('caret-aba-pessoa-fisica'));
        }
        if (sessionStorage.getItem('aba-pessoa-juridica') !== null) {
            $('#aba-pessoa-juridica').attr('class', sessionStorage.getItem('aba-pessoa-juridica'));
            $('#caret-aba-pessoa-juridica').attr('class', sessionStorage.getItem('caret-aba-pessoa-juridica'));
        }
        if (sessionStorage.getItem('aba-localizacao-ponto-consumo') !== null) {
            $('#aba-localizacao-ponto-consumo').attr('class', sessionStorage.getItem('aba-localizacao-ponto-consumo'));
            $('#caret-aba-localizacao-ponto-consumo').attr('class', sessionStorage.getItem('caret-aba-localizacao-ponto-consumo'));
        }
        if (sessionStorage.getItem('aba-imovel') !== null) {
            $('#aba-imovel').attr('class', sessionStorage.getItem('aba-imovel'));
            $('#caret-aba-imovel').attr('class', sessionStorage.getItem('caret-aba-imovel'));
        }
        if (sessionStorage.getItem('aba-contrato') !== null) {
            $('#aba-contrato').attr('class', sessionStorage.getItem('aba-contrato'));
            $('#caret-aba-contrato').attr('class', sessionStorage.getItem('caret-aba-contrato'));
        }
    } catch (e) {
        console.error('browser Nao suporte session storage');
    }
}

function verificarTipoPessoa() {
    var cpfCliente = document.forms[0].cpfCliente.value;
    var nomeCliente = document.forms[0].nomeCliente.value;
    var rgCliente = document.forms[0].rgCliente.value;
    var numeroPassaporte = document.forms[0].numeroPassaporte.value;

    var cnpjCliente = document.forms[0].cnpjCliente.value;
    var nomeFantasia = document.forms[0].nomeFantasiaCliente.value;

    if (cpfCliente != "" || nomeCliente != "" || rgCliente != "" || numeroPassaporte != "") {
        document.forms[0].codigoTipoPessoa.value = '1'; // Pessoa Fisica
    }
    if (cnpjCliente != "" || nomeFantasia != "") {
        document.forms[0].codigoTipoPessoa.value = '2'; // Pessoa Juridica
    }
    if ((cpfCliente != "" || nomeCliente != "" || rgCliente != "" || numeroPassaporte != "") && (cnpjCliente != "" || nomeFantasia != "")) {
        document.forms[0].codigoTipoPessoa.value = null;
    }
}

function detalharChamado(chave) {
    document.forms[0].chavePrimaria.value = chave;
    submeter('chamadoForm', 'exibirDetalhamentoChamado');
}

function exibirAlterarChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirAlteracaoChamadoInclusao');
    }

}

function copiarChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'copiarChamado');
    }
}

function exibirTramitacaoChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirTramitacaoChamado');
    }

}

function tramitarChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirTramitacaoChamado');
    }
}

function exibirReiteracaoChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirReiteracaoChamado');
    }
}

function exibirReativacaoChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirReativacaoChamado');
    }
}

function exibirReaberturaChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirReaberturaChamado');
    }
}

function exibirEncerrarChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirEncerramentoChamado');
    }
}

function exibirEncerrarEmLoteChamado() {

    var selecionados = [];
    var imovelPai = 0;
    var erro = '';

    for (var elemento in elementosClicados) {

        if (elementosClicados[elemento].marcado) {
            // if (imovelPai > 0 && checkElemento.data('imovel-pai') != imovelPai) {
            //     erro = 'Para encerrar chamados em lotes, � necess�rio todos os im�veis dos chamados selecionados, terem o mesmo im�vel pai!';
            //     return false;
            // }
            if (!elementosClicados[elemento].permiteAbrirEmLote) {
                erro = 'Existem chamados na sele��o que Nao permitem encerrar em lote!';
                break;
            }
            if (elementosClicados[elemento].status === 'FINALIZADO') {
                erro = 'Existem chamados na sele��o que j� foram encerrados!';
                break;
            }

            selecionados.push(elementosClicados[elemento].id);
            imovelPai = elementosClicados[elemento].imovelPai;
        }
    }


    if (erro.length > 0) {
        $('#modalEncerrarEmLoteBody').html(erro);
        $('#modalEncerrarEmLote').modal('show');
    } else if (selecionados.length <= 1) {
        $('#modalEncerrarEmLoteBody').html('� necess�rio selecionar mais de um chamado para encerrar em lote!');
        $('#modalEncerrarEmLote').modal('show');
    } else {
        document.forms[0].chavesPrimariasLote.value = selecionados;
        submeter('chamadoForm', 'exibirEncerramentoEmLoteChamado');
    }
}

function exibirCapturarChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirCapturarChamado');
    }
}


function carregarResponsavel(chaveUnidadeOrganizacional) {
    var noCache = "noCache=" + new Date().getTime();
    $("#divResponsavel").load("carregarResponsavel?chaveUnidadeOrganizacional=" + chaveUnidadeOrganizacional + "&" + noCache);
}

function carregarChamadoAssunto(chaveChamadoTipo) {
    if (chaveChamadoTipo === undefined || chaveChamadoTipo <= 0) {
        $("#chamadoAssunto").empty();
        $("#chamadoAssunto").append('<option value="-1">Selecione</option>');
    } else {
        $('#chamadoAssunto').attr('disabled', true);
        $("#divAssunto").load("carregarChamadoAssuntoPesquisa?chaveChamadoTipo=" + chaveChamadoTipo, function () {
            $('#chamadoAssunto').attr('disabled', false);
        });
    }
}

function carregarChamadoTipo(chaveChamadoSegmento) {
    if (chaveChamadoSegmento === undefined || chaveChamadoSegmento <= 0) {
        $("#idChamadoTipo").empty();
        $("#idChamadoTipo").append('<option value="-1">Selecione</option>');
        $("#idChamadoTipo").val(-1);
    } else {
        $("#idChamadoTipo").attr('disabled', true);
        $("#divTipos").load("carregarChamadoTipo?chaveChamadoSegmento=" + chaveChamadoSegmento, function () {
            $("#idChamadoTipo").attr('disabled', false);
            carregarChamadoAssunto(-1);
        });
    }
}

function incluir() {
    $("#buttonIncluir").attr("disabled", "disabled");
    submeter('chamadoForm', 'exibirInclusaoChamado');
}

function incluirChamadosEmLote() {
    $("#buttonIncluirEmLote").attr("disabled", "disabled");
    submeter('chamadoForm', 'exibirInclusaoChamadoEmLote');
}

function limparFormulario() {
    limparFormularios(document.chamadoForm);
    document.forms['chamadoForm'].condominioImovel[2].checked = true;
    document.forms['chamadoForm'].indicadorAgenciaReguladora[2].checked = true;
    document.getElementById("cnpjSolicitante").disabled = false;
    document.getElementById("cpfSolicitante").disabled = false;
    document.forms['chamadoForm'].listaStatus[0].checked = "checked";
    document.forms['chamadoForm'].listaStatus[1].checked = "checked";
    $("#unidadesVisualizadora").val([]);
}

function desabilitarCpfCnpj() {
    var cpfSolicitante = document.forms['chamadoForm'].cpfSolicitante.value;
    var cnpjSolicitante = document.forms['chamadoForm'].cnpjSolicitante.value;

    if (cpfSolicitante != "") {
        document.getElementById("cnpjSolicitante").disabled = true;
    }
    if (cpfSolicitante == "___.___.___-__" || cpfSolicitante == '') {
        document.getElementById("cnpjSolicitante").disabled = false;
    }

    if (cnpjSolicitante != "") {
        document.getElementById("cpfSolicitante").disabled = true;
    }
    if (cnpjSolicitante == "__.___.___/____-__" || cnpjSolicitante == '') {
        document.getElementById("cpfSolicitante").disabled = false;
    }

}

function imprimirChamadoDetalhar() {
	document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
	var selecao = verificarSelecao();

	if(selecao == true){
	    if ($($("input[name=chavesPrimarias]:checked")[0]).parents("tr").find(".comAS").text().trim() == "SIM") {
	      $("#dialog-confirm").dialog('open');
	    } else {
	      $("#comAS").val("false");
	      submeter('chamadoForm', 'imprimirChamados');
	    }
	}
    
}

function exibirDialogGerarAutorizacaoServico() {
    $("#corpoGridServicoTipo").html("");
    var selecao = verificarSelecaoApenasUm();
    var chaveChamado;
    var chaveAssunto = null;
    if (selecao == true) {
        chaveChamado = obterValorUnicoCheckboxSelecionado();
        var cont = 0;
        var param = "";

        AjaxService.obterListaServicoTipoPorChamado(chaveChamado, chaveAssunto, {
            callback: function (listaServicoTipo) {
                if (listaServicoTipo.length > 0) {
                    AjaxService.listarEquipes({
                        callback: function (entidade) {

                            var combo = $('#listaEquipe');
                            combo.html('<option value="-1">Selecione</option>');

                            for (key in entidade) {
                                var o = new Option(entidade[key], key);
                                combo.append(o);
                            }

                            for (key in listaServicoTipo) {
                                var servicoTipo = listaServicoTipo[cont];

                                if ((cont + 2) % 2 == 0) {
                                    param = "odd";
                                } else {
                                    param = "even";
                                }

                                var chavePrimaria = servicoTipo['chavePrimaria'];
                                var descricao = servicoTipo['descricao'];
                                var prioridade = servicoTipo['prioridade'];
                                var regulamentado = servicoTipo['regulamentado'];

                                AjaxService.verificarRestricaoServico(chavePrimaria, chaveChamado, null, chaveAssunto, {
                                    callback: function (servicoRestrito) {

                                        var inner = '';
                                        inner = inner + '<tr class=' + param + '>';
                                        if (servicoRestrito) {

                                            inner = inner + '<td ><input type="checkbox" disabled name="chavesPrimariasDialogDisabled" value="' + chavePrimaria + '"/></td>';
                                        } else {
                                            inner = inner + '<td ><input type="checkbox" checked name="chavesPrimariasDialog" value="' + chavePrimaria + '"/></td>';
                                        }
                                        inner = inner + '<td>' + descricao + '</td>';
                                        inner = inner + '<td>' + prioridade + '</td>';
                                        inner = inner + '<td>' + regulamentado + '</td>';
                                        if (servicoRestrito) {
                                            inner = inner + '<td>' + 'Sim' + '</td>';
                                        } else {
                                            inner = inner + '<td>' + 'Nao' + '</td>';
                                        }

                                        inner = inner + '</tr>';

                                        $("#corpoGridServicoTipo").append(inner);
                                        cont++;
                                        exibe = true;

                                    }, async: false
                                });
                            }
                        }
                        , async: false
                    });
                } else {
                    submeter('chamadoForm', 'gerarAutorizacaoServico');
                }
            }, async: false
        });
        if (cont > 0) {
            $("#dialogGerarAutorizacaoServico").parent().css({position: "relative"}).end().dialog('open');
        }

    }
}

function selectAll(check) {
    if (check.checked) {
        $('input[name="chavesPrimariasDialog"]').attr('checked', true);
    } else {
        $('input[name="chavesPrimariasDialog"]').attr('checked', false);
    }
}

/**
 * Verifica se algum item foi selecionado na lista
 */
function verificarSelecaoDialog() {
    if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
        alert("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
        return false;
    }
    return true;
}

function gerarAutorizacaoServico() {
    var selecao = verificarSelecaoDialog();

    if ($("#listaEquipe :selected").val() == "-1") {
        alert("Selecione uma equipe para designar a(s) AS");
    } else {
        if (selecao == true) {
            exibirLoading('#botaoGerar');
            var listaChecked = new Array();
            var i = 0;
            $('input[name="chavesPrimariasDialog"]').each(function () {
                if (this.checked) {
                    listaChecked[i] = $(this).val();
                    i++;
                }
            });
            document.getElementById("chavesPrimariasDialog").value = listaChecked;
            document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
            document.getElementById("idTurnoPreferencia").value = $("#turnoPreferencia :selected").val();

            submeter('chamadoForm', 'gerarAutorizacaoServico');
        }
    }
}

function exibirResponderQuestionario() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirResponderQuestionario');
    }
}

function responderQuestionario(chavePrimaria) {
    document.forms[0].chavePrimaria.value = chavePrimaria;
    submeter('chamadoForm', 'exibirResponderQuestionario');
}

function exibirAlteracaoCadastroClienteChamado() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirAlteracaoCadastroClienteChamado');
    }
}

function exibirSolicitacaoAlteracaoDataVencimento() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirSolicitacaoAlteracaoDataVencimento');
    }
}

function exibirSolicitacaoAlteracaoDataPrev() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {

        if ($($($("#chamado").find("input[name='chavesPrimarias']:checked")[0]).parents("tr")[0]).find("a.linkStatusChamado").text().trim() == "FINALIZADO") {
            alert("Nao � poss�vel alterar a data de previs�o de encerramento para chamados finalizados.");
            return
        }

        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        $("#dialogAlterarPrevisaoEncerramento").parent().css({position: "relative"}).end().dialog('open');
    }
}

function alterarDataEncerramento() {

    var chavePrimaria = obterValorUnicoCheckboxSelecionado();

    if (validarAlteracaoDataEncerramento()) {

        var idMotivo = $("#idMotivo :selected").val();
        $("#idMotivo :selected").attr('selected', false);
        var dataPrevisaoEncerramento = $("#dataPrevisaoEncerramento").val();
        $("#dataPrevisaoEncerramento").val("");
        var descricao = $("#descricao").val();
        $("#descricao").val("");


        AjaxService.alterarDataPrevisaoEncerramento(chavePrimaria, descricao, idMotivo, dataPrevisaoEncerramento, {
            callback: function (retorno) {
                $("#dialogAlterarPrevisaoEncerramento").dialog("close");
                $('html, body').animate({scrollTop: '0px'}, 300);
                $('.mensagensSpring').removeAttr('style');
                if (!retorno) {
                    $('.mensagensSpring').html("<div class='failure notification hideit'><p><strong>Erro: </strong>Erro ao alterar Chamado.</p></div>");
                } else {
                    $('.mensagensSpring').html("<div class='mensagemSucesso notification hideit'><p><strong>Sucesso: </strong>Chamado Alterado com sucesso.</p></div>");
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            }, async: false
        });
    } else {
        alert("Faltou preencher o(s) campo(s) obrigat�rio(s)");
    }
}

function validarAlteracaoDataEncerramento() {
    var retorno = true;

    if ($("#idMotivo :selected").val() == "-1") {
        retorno = false;
    }

    if ($("#dataPrevisaoEncerramento").val() == "") {
        retorno = false;
    }

    if ($("#descricao").val() == "") {
        retorno = false
    }

	if($("#dataResolucao").val().length < 1){
		retorno = false;
	}

    return retorno;
}

function exibirQuestionario(chavePrimaria) {
    window.open('exibirRespostaQuestionario?chavePrimariaChamado=' + chavePrimaria, 'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

//Fun??o para carregar os munic?pios ap?s selecionar a UF
function carregarMunicipios(siglaUnidadeFederativa) {

    var selectMunicipio = document.getElementById("nomeMunicipioPontoConsumoChamado");

    selectMunicipio.length = 0;
    var novaOpcao = new Option("Selecione", "");
    selectMunicipio.options[selectMunicipio.length] = novaOpcao;

    if (siglaUnidadeFederativa != "") {
        selectMunicipio.disabled = false;
        $("#nomeMunicipioPontoConsumoChamado").removeClass("campoDesabilitado");
        AjaxService.consultarMunicipiosPorUnidadeFederativaPorSiglaChamado(siglaUnidadeFederativa,
            {
                callback: function (listaMunicipio) {
                    for (key in listaMunicipio) {
                        var novaOpcao = new Option(key, key);
                        selectMunicipio.options[selectMunicipio.length] = novaOpcao;
                    }
                }, async: false
            }
        );

    } else {
        //selectMunicipio.disabled = true;
        //$("#nomeMunicipioPontoConsumo").addClass("campoDesabilitado");
    }
}

//Fun??o para carregar os bairros ap?s selecionar o munic?pio
function carregarBairros(nomeMunicipio) {

    var selectBairro = document.getElementById("bairroPontoConsumoChamado");

    selectBairro.length = 0;
    var novaOpcao = new Option("Selecione", "");
    selectBairro.options[selectBairro.length] = novaOpcao;

    if (nomeMunicipio != "") {
        selectBairro.disabled = false;
        $("#bairroPontoConsumoChamado").removeClass("bairroPontoConsumoChamado");
        AjaxService.consultarBairrosPorMunicipio(nomeMunicipio,
            {
                callback: function (listaBairro) {
                    for (key in listaBairro) {
                        var novaOpcao = new Option(key, listaBairro[key]);
                        selectBairro.options[selectBairro.length] = novaOpcao;
                    }
                }, async: false
            }
        );

    } else {
        selectBairro.disabled = true;
    }
}

function exibirLoading(idBotaoOrigem) {
    $(idBotaoOrigem).attr("disabled", "disabled");
    exibirIndicador();
    $("#indicadorProcessamento").css("position", "fixed");
}


function toggleClass(id) {
    $(id).toggleClass('fa-caret-up');
    $(id).toggleClass('fa-caret-down');
}


$(document).keyup(function (e) {
    if (e.which == 27) {
        $("#botaoGerar").removeAttr("disabled");
    }
});

function enviarEmail(protocolo, emailSolicitante, emailCliente, chamadoAssunto, dataInicioAbertura, dataPrevisaoEncerramento, status, nomeCliente, prioridadeEmail){
    var email = "";
    
    email = prioridadeEnvio(emailSolicitante, emailCliente, prioridadeEmail);
    if (email != "" && email != null){
		AjaxService.enviarEmailChamado(protocolo, email.trim().replace(",", ";"), chamadoAssunto, dataInicioAbertura, dataPrevisaoEncerramento, status, nomeCliente,
			function(emailEnviado){					
				if (emailEnviado == true){
					alert("E-mail enviado com sucesso!");
				}else{
					alert("Erro ao enviar o e-mail, procure o TI!");
				}
			});
	}else{
		alert('Cliente ou Solicitante sem e-mail cadastrado!');
	}
}

function prioridadeEnvio(emailSolicitante, emailCliente, prioridadeEmail){
        var ambos = "";
        if (prioridadeEmail == "CLIENTE"){
            return emailCliente;    
        } else if (prioridadeEmail == "SOLICITANTE"){
           return emailSolicitante;          
        } else if (prioridadeEmail == "AMBOS"){  
           if(emailCliente != "" && emailSolicitante != ""){
                return emailCliente + ";" + emailSolicitante;
           }else if(emailSolicitante != ""){
               ambos += emailSolicitante;
           } else if (emailCliente != ""){
               ambos += emailCliente;
           } 
           return ambos;
        }
}

function desabilitarCombobox() {
    $('#idChamadoTipo').attr('disabled', true);
    $('#chamadoAssunto').attr('disabled', true);
}


function carregarTurnosEquipe() {

	let chaveEquipe = $("#listaEquipe").val();

	var selectTurnoPreferencia = document.getElementById("turnoPreferencia");
	var idTurnoPreferencia = $("#idTurnoPreferencia").val();

	selectTurnoPreferencia.length = 0;

	var novaOpcao = new Option("Selecione", "-1");
	selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;


	AjaxService.listaTurnoPorEquipe(chaveEquipe , {
		callback: function(listaTurnos) {

			for (key in listaTurnos) {

				var novaOpcao = new Option(listaTurnos[key], key);
				if (key == idTurnoPreferencia) {
					novaOpcao.selected = true;
				}
				selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;
			}

		}
	});
}
