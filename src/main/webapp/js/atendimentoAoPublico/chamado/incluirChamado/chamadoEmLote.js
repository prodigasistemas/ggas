/*
 * Copyright (C) <2019> GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
 *
 * Este arquivo � parte do GGAS, um sistema de gest�o comercial de Servi�os de Distribui��o de G�s
 *
 * Este programa � um software livre; voc� pode redistribu�-lo e/ou
 * modific�-lo sob os termos de Licen�a P�blica Geral GNU, conforme
 * publicada pela Free Software Foundation; vers�o 2 da Licen�a.
 *
 * O GGAS � distribu�do na expectativa de ser �til,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia impl�cita de
 * COMERCIALIZA��O ou de ADEQUA��O A QUALQUER PROP�SITO EM PARTICULAR.
 * Consulte a Licen�a P�blica Geral GNU para obter mais detalhes.
 *
 * Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral GNU
 * junto com este programa; se Nao, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS ? Sistema de Gest�o Comercial (Billing) de Servi�os de Distribui��o de G�s
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 */

$(document).ready(function () {
    removerAsteriscos();

    $("input[type=hidden]").removeAttr("disabled");
    $("input[name=checkPontoConsumo]").removeAttr("disabled");

    $("#dialogGerarAutorizacaoServico").dialog({
        autoOpen: false,
        modal: true,
        width: 1000,
        resizable: false,
        draggable: false
    });

    exibirUnidadesQuePodemVisualizar();
});

function exibirUnidadesQuePodemVisualizar() {
    /**
     * Paliativo para:
     * O campo ?Unidades que podem visualizar? mostra a op??o ?3 selecionados? mas fica desabilitado para sele??o:
     *
     * O problema ocorre somente para dispositivos m?veis (chrome), n?o sendo poss�vel visualizar as unidades que est?o selecionados
     * O ideal ? substituir o select default por https://silviomoreto.github.io/bootstrap-select/ para que a exibi??o dos que
     * est?o selecionados seja vis?vel sempre.
     */
    $('#divUnidadeOrganizacionalVisualizadoras').on('click touch', function () {
        if ($('#unidadeOrganizacionalVisualizadoras').attr('disabled') !== undefined) {

            var unidadesSelecioandasTemp = $('#unidadeOrganizacionalVisualizadoras option:selected').text().split('\n');

            $('#ul-unidades-podem-visualizar').empty();
            for (var iU in unidadesSelecioandasTemp) {
                if (unidadesSelecioandasTemp[iU].trim() !== '') {
                    $('#ul-unidades-podem-visualizar').append('<li class="list-group-item">'+unidadesSelecioandasTemp[iU].trim()+'</li>');
                }
            }

            $('#modal-unidade-podem-visualizar').modal('show');
        }
    });
}

function exibirDialogGerarAutorizacaoServico(fluxo) {
    $("#corpoGridServicoTipo").html("");
    var chaveChamado = $("#chavePrimaria").val();
    var cont = 0;
    var param = "";
    var chaveAssunto = null;

    if ($("#chamadoAssunto :selected").size() > 0) {
        chaveAssunto = $("#chamadoAssunto :selected").val();
    } else {
        alert("Selecione o Assunto do Chamado");
    }

    var chaveImovel = document.forms[0].imovel.value;

    AjaxService.obterListaServicoTipoPorChamado(chaveChamado, chaveAssunto, {
            callback: function (listaServicoTipo) {
                if (listaServicoTipo.length > 0) {
                    AjaxService.listarEquipes({
                        callback: function (entidade) {

                            var combo = $('#listaEquipe');
                            combo.html('<option value="-1">Selecione</option>');

                            for (key in entidade) {
                                var o = new Option(entidade[key], key);
                                combo.append(o);
                            }

                            for (key in listaServicoTipo) {
                                var servicoTipo = listaServicoTipo[cont];

                                if ((cont + 2) % 2 == 0) {
                                    param = "odd";
                                } else {
                                    param = "even";
                                }

                                var chavePrimaria = servicoTipo['chavePrimaria'];
                                var descricao = servicoTipo['descricao'];
                                var prioridade = servicoTipo['prioridade'];
                                var regulamentado = servicoTipo['regulamentado'];

                                AjaxService.verificarRestricaoServico(chavePrimaria, chaveChamado, chaveImovel, chaveAssunto, {
                                    callback: function (servicoRestrito) {

                                        var inner = '';
                                        inner = inner + '<tr class=' + param + '>';
                                        if (servicoRestrito) {

                                            inner = inner + '<td ><input type="checkbox" disabled name="chavesPrimariasDialogDisabled" value="' + chavePrimaria + '"/></td>';
                                        } else {
                                            inner = inner + '<td ><input type="checkbox" checked name="chavesPrimariasDialog" value="' + chavePrimaria + '"/></td>';
                                        }
                                        inner = inner + '<td>' + descricao + '</td>';
                                        inner = inner + '<td>' + prioridade + '</td>';
                                        inner = inner + '<td>' + regulamentado + '</td>';
                                        if (servicoRestrito) {
                                            inner = inner + '<td>' + 'Sim' + '</td>';
                                        } else {
                                            inner = inner + '<td>' + 'Nao' + '</td>';
                                        }

                                        inner = inner + '</tr>';

                                        $("#corpoGridServicoTipo").append(inner);


                                        cont++;

                                        exibe = true;

                                    }, async: false
                                });
                            }

                        }
                        , async: false
                    });
                } else if (fluxo != undefined && fluxo != "alteracaoRascunho") {
                    $("form :input").removeAttr("disabled");
                    submeterFormChamado('salvarAgendar');
                } else {
                    alert("Nao existe Autoriza��o de Servi�o para esse chamado");
                }
            }, async: false
        }
    );
    $('#listaEquipe').removeAttr('disabled');
}

function salvarGerar() {
    if ($("#listaEquipe :selected").size() > 0 && $("#listaEquipe :selected").val() != -1) {
        exibirLoading("#botaoGerar");
        document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
        $("form :input").removeAttr("disabled");
        submeterFormChamado('salvarAgendar');
    } else {
        verificarSelecaoDialog();
        alert("Selecione uma equipe para designar a(s) AS");
    }
}

function selectAll(check) {
    if (check.checked) {
        $('input[name="chavesPrimariasDialog"]').attr('checked', true);
    } else {
        $('input[name="chavesPrimariasDialog"]').attr('checked', false);
    }
}

/**
 * Verifica se algum item foi selecionado na lista
 */
function verificarSelecaoDialog() {
    if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
        alert("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
        return false;
    }
    return true;
}

function gerarAutorizacaoServico() {
    var selecao = verificarSelecaoDialog();

    if ($("#listaEquipe :selected").val() == "-1") {
        alert("Selecione uma equipe para designar a(s) AS");
        if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
            alert("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
            return false;
        }
    } else {
        if (selecao == true) {
            exibirLoading('#botaoGerar');
            var listaChecked = new Array();
            var i = 0;
            $('input[name="chavesPrimariasDialog"]').each(function () {
                if (this.checked) {
                    listaChecked[i] = $(this).val();
                    i++;
                }
            });
            document.getElementById("chavesPrimariasDialog").value = listaChecked;
            document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
            submeter('chamadoForm', 'gerarAutorizacaoServico');
        }
    }
}

function removerAsteriscos() {
    $('span[id="spanTipoChamado"]').remove();
    $('span[id="spanChamadoAssunto"]').remove();
    $('span[id="spanCanalAtendimento"]').remove();
    $('span[id="spanTipoManifestacao"]').remove();
    $("#spanUnidadeOrganizacional").html('');
    $('span[id="spanNumeroContrato"]').remove();
    $("#spanDescricao").html('');
    $("#spanResponsavel").html('');
}

function incluir() {
    exibirLoading("#botaoIncluir");
    document.forms[0].rascunho.value = false;
    //if (!validarUnidadesVisualizadoras()) {
      //  ocultarLoading('#botaoIncluir');
        //return;
    //}

    var chaveAssunto = document.forms[0].chamadoAssunto.value;
    var chaveImovel = document.forms[0].imovel.value;

    verificarGarantiaServicos("#botaoIncluir", function() {
        exibirLoading("#botaoIncluir");
        AjaxService.verificarGeracaoAutomaticaASChamado(chaveAssunto, {
            callback: function (geraASAutomatico) {

                if (geraASAutomatico) {
                    ocultarLoading('#botaoIncluir');
                    exibirDialogGerarAutorizacaoServico("incluir");
                } else {
                    $("form :input").removeAttr("disabled");
                    submeterFormChamado('incluirChamadoEmLote');
                }
            },
            async: false
        });
    })

}

function exibirAlterarChamado() {
    submeter('chamadoForm', 'exibirAlteracaoChamadoInclusao');
}

function exibirAlterarChamadoGrid() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirAlteracaoChamadoInclusao');
    }
}


function alterarRascunhoChamado() {
    $("form :input").removeAttr("disabled");
    submeter('chamadoForm', 'alterarChamado');
}

function copiarChamado() {
    submeter('chamadoForm', 'copiarChamado');
}

function copiarChamadoGrid() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'copiarChamado');
    }
}

function exibirTramitacaoChamado() {
    submeter('chamadoForm', 'exibirTramitacaoChamado');
}

function tramitarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoTramitar");
    submeter('chamadoForm', 'tramitarChamado');
}

function exibirReiteracaoChamado() {
    submeter('chamadoForm', 'exibirReiteracaoChamado');
}

function exibirEncerrarChamado() {
    submeter('chamadoForm', 'exibirEncerramentoChamado');
}

function encerrarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoEncerrar");
    submeter('chamadoForm', 'encerrarChamado');
}

function reiterarChamado() {
    $("#buttonReiterar").attr("disabled", "disabled");
    exibirLoading("#botaoReiterar");
    submeter('chamadoForm', 'reiterarChamado');
}

function exibirReativacaoChamado() {
    submeter('chamadoForm', 'exibirReativacaoChamado');
}

function reativarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoReativar");
    submeter('chamadoForm', 'reativarChamado');
}

function exibirReaberturaChamado() {
    submeter('chamadoForm', 'exibirReaberturaChamado');
}

function reabrirChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoReabrir");
    submeter('chamadoForm', 'reabrirChamado');
}

function exibirCapturarChamado() {
    $("form :input").removeAttr("disabled");
    submeter('chamadoForm', 'exibirCapturarChamado');
}

function capturarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoCapturar");
    submeter('chamadoForm', 'capturarChamado');
}

function carregarResponsavel(chaveUnidadeOrganizacional) {
    var noCache = "noCache=" + new Date().getTime();
    $("#divResponsavel").load("carregarResponsavel?chaveUnidadeOrganizacional=" + chaveUnidadeOrganizacional + "&" + noCache);
}

function cancelar() {

    if (document.forms['chamadoForm'].usuarioResponsavel != undefined) {
        document.getElementById("usuarioResponsavel").value = '';
    }
    submeter('chamadoForm', 'exibirPesquisaChamado');
}

function voltar() {

    if (document.forms['chamadoForm'].usuarioResponsavel != undefined) {
        document.getElementById("usuarioResponsavel").value = '';
    }
    document.forms[0].fluxoVoltar.value = "fluxoVoltar";
    window.close();
}

function limparFormExpecifico() {
    document.forms['chamadoForm'].descricao.value = '';
    document.forms['chamadoForm'].idMotivo.value = '-1';
}

function limparFormTramitacao() {
    document.forms['chamadoForm'].descricao.value = '';
    $(document.forms[0].unidadeOrganizacional).find("option").attr("selected", false);
    document.forms[0].usuarioResponsavel.value = -1;
}

function limparFormRascunho() {
    submeter('chamadoForm', 'exibirAlteracaoRascunhoChamado');
}

function atualizarListaTipoChamado(idSegmento) {
    var noCache = "noCache=" + new Date().getTime();
    $("#divTiposChamados").load("carregarTiposChamado?idSegmento=" + idSegmento + "&" + noCache);
    $("#divAssuntos").html("<label for='chamadoAssunto'>" +
        "Assunto do Chamado: <span class='text-danger'>*</span></label>" +
        "<select id='chamadoAssunto' name='chamadoAssunto' class='form-control form-control-sm' " +
        "onchange=onChangeAssuntoChamado(this.value); verificarGarantia()>" +
        "<option value=''>Selecione o tipo de chamado...</option>"+
        "</select>");
}

function atualizarAssuntos(chave) {
    $('#chamadoAssunto').attr('disabled', true);

    var noCache = "noCache=" + new Date().getTime();
    var chaveAssunto = document.forms[0].chaveAssunto.value;
    $("#divAssuntos").load("carregarAssuntos?chavePrimaria=" + chave + "&chaveAssunto=" + chaveAssunto + "&" + noCache, function () {

        $('#chamadoAssunto').attr('disabled', false);

        if (campoPreenchido('#chamadoAssunto')) {
            onChangeAssuntoChamado($('#chamadoAssunto').val());
        }
    });
}

function atualizarAssuntosLiberado() {
    var chaveCliente = document.forms[0].chaveCliente.value;

    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    $('#chamadoAssunto').attr('disabled', true);

    var chave = $('#chamadoTipo').val();
    var chaveAssunto = document.forms[0].chaveAssunto.value;
    var noCache = "noCache=" + new Date().getTime();
    exibirLoading('');
    $("#divAssuntos").load("carregarAssuntos?chavePrimaria=" + chave + "&chaveAssunto=" + chaveAssunto +"&chavePrimariaCliente="+chaveCliente+"&chavePrimariaImovel="+chavePrimariaImovel+ "&" + noCache, function () {

        $('#chamadoAssunto').attr('disabled', false);

        ocultarLoading('');
        if (campoPreenchido('#chamadoAssunto')) {
            onChangeAssuntoChamado($('#chamadoAssunto').val());
        }
    });
}

function carregarUnidadeOrganizacional(chave) {
    if (chave != "") {
        carregarOrientacao(chave);
        var noCache = "noCache=" + new Date().getTime();
        $("#divUnidadeOrganizacional").load("carregarUnidadeOrganizacional?chavePrimariaAssunto=" + chave + "&" + noCache);
    }
}

function carregarPrazo(chavePrimaria) {

    AjaxService.carregarPrazoPrevisto(chavePrimaria, {
        callback: function (quantidadeHorasPrevistaAtendimento) {
            $("#divPrazo").val(quantidadeHorasPrevistaAtendimento);
        },
        async: true
    });
}

function carregarOrientacao(chaveAssunto) {

    AjaxService.carregarOrientacaoAssunto(chaveAssunto, {
        callback: function (orientacao) {
            $("#orientacaoAssunto").val(orientacao);
        },
        async: true
    });
}

function selecionaCanalAtendimento(chaveAssunto) {

    AjaxService.obterCanalAtendimetno(chaveAssunto, {
        callback: function (canalAtendimento) {
            if (canalAtendimento != "") {
                $("#canalAtendimento option").attr("selected", false);
                $("#canalAtendimento option[value='" + canalAtendimento + "']").attr("selected", true);
            }
        },
        async: true
    });
}

function carregarClientePorContrato(numeroContrato) {

    $(".notification.failure.hideit").trigger("click");

    if (numeroContrato != null && trim(numeroContrato) != "") {
        var url = "carregarClientePorContrato?numeroContrato=" + numeroContrato;
        carregarFragmento('divCliente', url);
        var chavePrimariaCliente = document.forms[0].chaveCliente.value;

        $("#divContrato").load("carregarContratoPorNumero?numeroContrato=" + numeroContrato,
            function () {
                var chavePrimariaContrato = document.forms[0].chaveContrato.value;

                $("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato=" + chavePrimariaContrato + "&numeroContrato=" + numeroContrato,
                    function () {
                        if ($("input[name='chaveImovel']").size() == 1) {

                            $("input[name='chaveImovel']").trigger("click");
                            verificarGarantia();
                        }
                    });
            }
        );


        $("#gridChamadosCliente").load("carregarChamadosClienteImovel?chavePrimariaCliente=" + chavePrimariaCliente);

        document.forms[0].cliente.value = chavePrimariaCliente;
    }
}

function detalharChamado(chave) {
    document.forms[0].chavePrimaria.value = chave;
    popup = window.open('exibirDetalhamentoChamado?&chavePrimaria=' + chave, 'popup', 'height=750,width=1080,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarPontoConsumo(chavePontoConsumo, descricao) {
    document.forms[0].pontoConsumo.value = chavePontoConsumo;
    carregarFragmento('divCliente', "carregarClientePorPontoConsumo?chavePontoConsumo=" + chavePontoConsumo);
    carregarFragmento('gridChamadosCliente', "carregarChamadosClienteImovel?chavePrimariaCliente=" + $("#cliente").val());
    carregarFragmento('divContrato', "carregarContratoPorPontoConsumo?chavePontoConsumo=" + chavePontoConsumo);
    carregarFragmento('gridChamadoTabelaPontoConsumo', "carregarPontosConsumoClienteImovel?chavePrimariaPontoConsumo=" + chavePontoConsumo);

}

function carregaroInformacaoPontoConsumo(chavePontoConsumo) {
    document.forms[0].idPontoConsumo.value = chavePontoConsumo;
}


function pesquisar() {
    var noCache = "noCache=" + new Date().getTime();
    var numeroImovel = document.forms[0].numeroImovel.value;
    var complemento = encodeURI(document.forms[0].complemento.value);
    var nome = encodeURI(document.forms[0].nomeImovel.value);
    var matricula = document.forms[0].matricula.value;
    var numeroCep = document.getElementById('cepImovel').value;

    $("#gridImoveis").load("consultarImovel?numeroImovel=" + numeroImovel +
        "&complemento=" + complemento +
        "&nome=" + nome +
        "&matricula=" + matricula +
        "&numeroCep=" + numeroCep,
        function () {

            limparContratoECliente();

            var chaveImovel = -1;
            limparPontoConsumo();

        });
}

function limparContratoECliente() {

    document.forms[0].chaveCliente.value = -1;
    document.forms[0].chaveContrato.value = -1;
    document.forms[0].contrato.value = "";
    document.forms[0].nome.value = "";
    document.forms[0].nomeFantasia.value = "";
    document.forms[0].cpfCnpj.value = "";
    document.forms[0].numeroPassaporte.value = "";
    document.forms[0].email.value = "";
    document.forms[0].telefone.value = "";

}

function limparPontoConsumo() {

    $('#chavePontoConsumo').val('');
    var fluxoExecucao = "fluxoInclusao";
    $("#gridChamadoPontosConsumo").load("limparPontosConsumo?&fluxoExecucao=" + fluxoExecucao);
}

function carregarPontosConsumo(chavePrimariaImovel) {
    $(".notification.failure.hideit").hide();
    $(".notification.mensagemAlerta.hideit").hide();

    var fluxoExecucao = "fluxoInclusao";
    var idContrato = '';
    limparContratoECliente();
    $("#gridChamadoPontosConsumo").load("carregarPontosConsumo?chavePrimariaImovel=" + chavePrimariaImovel + "&fluxoExecucao=" +
        fluxoExecucao + "&chavePrimariaContrato=" + idContrato,
        function () {

            if ($("input[name='checkPontoConsumo']").size() == 1) {
                $("input[name='checkPontoConsumo']").trigger("click");
            }
            else {
                $("input[name='checkPontoConsumo']").each(function () {
                    var pontoconsumo = '<c:out value="${chamado.pontoConsumo.chavePrimaria}" />';
                    if ($(this).val() == pontoconsumo) {
                        $(this).trigger("click");
                    }
                });
            }

            verificarErroAlerta();

        });

    document.forms[0].imovel.value = chavePrimariaImovel;
}

function verificarErroAlerta() {

    if ($("input[name='matricula']").each(function () {
        var verificarErroAlerta = '<c:out value="${chamado.imovel.chavePrimaria}" />';
        if (verificarErroAlerta != 0) {
            $(".mensagensSpring").show();

            $(".notification.failure.hideit").click(function () {
                $(this).fadeOut(700);
            });
            $(".notification.mensagemAlerta.hideit").click(function () {
                $(this).fadeOut(700);
            });
        }
    })) ;
}

function imprimirChamadoDetalhar() {

    if ($("#servicoAutorizacao").find("tr.odd").length > 0 || $("#servicoAutorizacao").find("tr.odd").length > 0) {
        $("#dialog-confirm").modal('show');
    } else {
        $("#comAS").val("false");
        submeter('chamadoForm', 'imprimirChamado');
    }
}

function exibirResponderQuestionario() {
    submeter('chamadoForm', 'exibirResponderQuestionario');
}

function exibirSolicitacaoAlteracaoDataVencimento() {
    submeter('chamadoForm', 'exibirSolicitacaoAlteracaoDataVencimento');
}

function exibirAlteracaoCadastroClienteChamado() {
    submeter('chamadoForm', 'exibirAlteracaoCadastroClienteChamado');
}

function salvarAgendar() {

    var chaveAssunto = document.forms[0].chamadoAssunto.value;
    var chaveImovel = document.forms[0].imovel.value;

   // if (!validarInformacoesGasista() || !validarUnidadesVisualizadoras()) {
     //   ocultarLoading('#botaoSalvarAgendar');
       // return;
    //}

    exibirDialogGerarAutorizacaoServico("salvarAgendar");

}

var windows = {};

function imprimirArquivo(chaveChamadoHistorico) {

    if (windows['popup'] != undefined && windows['popup'] != null) {
        windows['popup'].close();
    }
    var url = 'imprimirArquivoChamado';
    windows['popup'] = window.open(url + "?chaveChamadoHistorico=" + chaveChamadoHistorico, 'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function carregarResponsavelPorChamadoAssunto(chaveChamadoAssunto) {

    if (chaveChamadoAssunto != "") {
        var noCache = "noCache=" + new Date().getTime();
        $("#divResponsavelPorChamadoAssunto").load("carregarResponsavelPorChamadoAssunto?chaveChamadoAssunto=" + chaveChamadoAssunto + "&" + noCache);

    }
}

function carregarPrazoDiferenciado(chaveChamadoAssunto) {
    if (chaveChamadoAssunto != "") {
        var noCache = "noCache=" + new Date().getTime();
        $("#divPrazoDiferenciado").load("carregarPrazoDiferenciado?chaveChamadoAssunto=" + chaveChamadoAssunto + "&" + noCache,

            function () {
                if ($("#indicadorPrazoDiferenciado").val() == 'true') {

                    if ($("#fluxoDetalhamento").val() != "true") {
                        $('#dataPrevisaoEncerramento').removeAttr('disabled');
                    }
                    $("#dataPrevisaoEncerramento").inputmask("99/99/9999 99:99", {placeholder: "_", greedy: false});
                    $("#dataPrevisaoEncerramento").datetimepicker({
                        minDate: '+0d',
                        showOn: 'button',
                        buttonImage: $('#imagemCalendario').val(),
                        buttonImageOnly: true,
                        buttonText: 'Exibir Calend�rio',
                        dateFormat: 'dd/mm/yy',
                        timeFormat: 'HH:mm'
                    });

                } else {
                    $("#dataPrevisaoEncerramento").datetimepicker('destroy');
                    $('#dataPrevisaoEncerramento').val("");
                    $('#dataPrevisaoEncerramento').attr('disabled', 'disabled');
                }
            }
        );

    }
}

function carregarUnidadesVisualizadorasPadrao(chaveChamadoAssunto) {
    AjaxService.carregarUnidadesVisualizadorasPadrao(chaveChamadoAssunto, {
        callback: function (retorno) {
            if (retorno['indicadorUnidadeVisualizadora'] == "true") {
                $("#unidadeOrganizacionalVisualizadoras option").removeAttr("selected");
                $("#unidadeOrganizacionalVisualizadoras option").each(function () {
                    var option = $(this);
                    var idsUnidadesVisualizadoras = retorno['idsUnidadesVisualizadoras'].split(",");
                    if (idsUnidadesVisualizadoras.indexOf(option.val()) != -1) {
                        option.attr("selected", "selected");
                    }
                });
                $("#flagUnidadesVisualizadorasSim").attr("checked", "checked");
                $("#divUnidadeOrganizacionalVisualizadoras").show();
            } else {
                $("#flagUnidadesVisualizadorasNao").attr("checked", "checked");
                $("#divUnidadeOrganizacionalVisualizadoras").hide();
            }
        }, async: true
    });
}

function adicionarEmail() {
    if (descricaoEmail.value != null && descricaoEmail.value != " ") {
        var url = encodeURI("adicionarEmail?descricaoEmail=" + descricaoEmail.value);
        carregarFragmento('gridChamadoEmail', url);

        iniciarDatatable('#table-grid-email');

    }
    //$("#gridChamadoEmail").load("adicionarEmail?descricaoEmail="+descricaoEmail);
}

function limparElemento(element) {
    element.value = '';
}

function alterarChamadoEmail() {
    var indexListaChamadoEmail = document.forms[0].indexListaChamadoEmail.value;

    var url = encodeURI("alterarChamadoEmail?indexListaChamadoEmail=" + indexListaChamadoEmail + "&descricaoEmail=" + descricaoEmail.value);
    carregarFragmento('gridChamadoEmail', url);

    iniciarDatatable('#table-grid-email');

    $('.loading').hide();
    $('#table-grid-email').css({'opacity': 1, 'display': 'table'});
}

function mostraOculta(elemento) {

    if (elemento.style.display == 'none') {

        elemento.style.display = 'block';
    } else {
        elemento.style.display = 'none';
    }
}

function exibirPopupInclusaoCliente() {
    popup = window.open('popupExibirInclusaoCliente?', 'popup', 'height=750,width=1000,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

}

function exibirPopupInclusaoImovel() {
    popup = window.open('popupExibirInclusaoImovel?', 'popup', 'height=750,width=1000,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

}

function mudarEstadoUnidadesVisualizadoras(radio) {
    if (radio.value == "true") {
        $("#unidadeOrganizacionalVisualizadoras option").attr('selected', false);
        $("#divUnidadeOrganizacionalVisualizadoras").show();
    } else {
        $("#divUnidadeOrganizacionalVisualizadoras").hide();
    }
}

function validarInformacoesGasista() {
    var retorno = true;

    if ($("input[name='indicadorAcionamento']").val() != undefined && !$("input[name='indicadorAcionamento']").is(":checked")) {
        alert("� preciso informar se houve necessidade de acionamento de gasista ou plantonista.")
        return false;
    }

    if ($("#indicadorAcionamentoSim").val() != undefined && $("#indicadorAcionamentoSim").is(":checked")) {
        if (!$("#acionamentoPlantonista").is(":checked") && !$("#acionamentoGasista").is(":checked")) {
            alert("� preciso selecionar quem foi acionado (Gasista ou Plantonista).")
            retorno = false;
        }
        if ($("#informacaoAdicional").val().trim().length == 0) {
            alert("� preciso adicionar as informa��es do acionamento.");
            retorno = false;
        }
    }

    return retorno;
}

function validarUnidadesVisualizadoras() {
    var retorno = true;

    if ($("#flagUnidadesVisualizadorasSim").is(":checked")) {
        if ($("#unidadeOrganizacionalVisualizadoras :selected").size() == 0) {
            alert("Selecione as Unidades Organizacionais que podem visualizar o chamado.");
            retorno = false;
        }
    }

    return retorno;
}

function validarFormulario() {
    var valido = true;
    if ($('#chamadoTipo').val() === '') {
        $('#chamadoTipo').addClass('is-invalid');
        $('#chamadoTipo').focus();
        valido = false;
    }

    if ($('#chamadoAssunto').val() === '') {
        $('#chamadoAssunto').addClass('is-invalid');
        $('#chamadoAssunto').focus();
        valido = false;
    }

    return valido;
}

function submeterFormChamado(action) {
    submeter('chamadoForm', action);
}

/**
 *
 * @param id_dom
 * @returns true se o campo tiver sido preenchido
 */
function campoPreenchido(id_dom) {
    return $(id_dom).val() !== '' && $(id_dom).val() !== undefined && $(id_dom).val() !== null;
}

/**
 * Preencher os outros campos com o valor atual do assunto chamado
 * Verifica o assunto para habilitar o bot?o Mudan?a de Titularidade
 * @param valor_assunto_chamado
 */
function onChangeAssuntoChamado(valor_assunto_chamado) {
    carregarPrazo(valor_assunto_chamado);
    carregarUnidadeOrganizacional(valor_assunto_chamado);
    carregarResponsavelPorChamadoAssunto(valor_assunto_chamado);
    carregarPrazoDiferenciado(valor_assunto_chamado);
    carregarUnidadesVisualizadorasPadrao(valor_assunto_chamado);
    selecionaCanalAtendimento(valor_assunto_chamado);
}

function copiarObservacao() {

    try {

        var disabledProp = $("#orientacaoAssunto").attr('disabled');
        $("#orientacaoAssunto").attr('disabled', false);
        $("#orientacaoAssunto").select();
        document.execCommand("copy");
        $("#orientacaoAssunto").attr('disabled', disabledProp);
        /**
         * Estilo ? aplicado porque algum evento ? disparado e o estilo do componente ? modificado como se Nao estivesse desabilitado
         */
        $("#orientacaoAssunto").css({'background-color': '#e9ecef', 'border': '1px solid #ced4da'});

        if ($("#descricao").val().trim() === '') {
            $("#descricao").val($("#orientacaoAssunto").val());
        } else {
            $("#modal-colar-observacao").modal('show');
        }

    } catch (e) {
        console.error('Nao foi poss�vel copiar', e);
    }
}

function confirmarColaObservacao() {
    $("#descricao").val($("#orientacaoAssunto").val());
    $("#modal-colar-observacao").modal('hide');
}

function exibirLoading(idBotaoOrigem) {
    $(idBotaoOrigem).attr("disabled", "disabled");
    exibirIndicador();
    $("#indicadorProcessamento").css("position", "fixed");
}


/**
 * Fun??o necess?ria caso a chamada anterior que exibiu o loading Nao recarregue a p?gina
 * @param idBotaoOrigem o id do bot?o que origina a chamada
 */
function ocultarLoading(idBotaoOrigem) {
    $(idBotaoOrigem).removeAttr("disabled");
    esconderIndicador();
}

$(document).keyup(function (e) {
    if (e.which == 27) {
        $("#botaoAlterar").removeAttr("disabled");
        $("#botaoIncluir").removeAttr("disabled");
        $("#botaosalvarAgendar").removeAttr("disabled");
        $("#botaoTramitar").css("background-color", "");
        $("#botaoTramitar").removeAttr("disabled");
        $("#botaoReabrir").removeAttr("disabled");
        $("#botaoReabrir").css("background-color", "");
        $("#botaoReiterar").removeAttr("disabled");
        $("#botaoReiterar").css("background-color", "");
        $("#botaoReativar").removeAttr("disabled");
        $("#botaoReativar").css("background-color", "");
        $("#botaoCapturar").removeAttr("disabled");
        $("#botaoCapturar").css("background-color", "");
        $("#botaoEncerrar").removeAttr("disabled");
        $("#botaoEncerrar").css("background-color", "");
        $("#botaoGerar").removeAttr("disabled");
    }
});

function imprimirChamado(comAs) {
    $("#dialog-confirm").modal('hide');
    $("#comAS").val(comAs);
    submeter('chamadoForm', 'imprimirChamado');
}
function imprimirComAs() {
    imprimirChamado('true');
}
function imprimirSemAs() {
    imprimirChamado('false');
}

function exibirPopupMudancaTitularidade() {

	 var chamadoTela = document.forms[0].chamadoTela.value;
	 var idContrato = document.forms[0].chaveContrato.value;

	 AjaxService.verificarPontoConsumoAtivo(idContrato,
				function(pontoAtivo){
					var pontoConsumoAtivo = pontoAtivo;

					if (pontoConsumoAtivo == true){
							window.open('exibirPopupPontoConsumoContrato?&chavePrimaria='+idContrato+'&chamadoTela='+chamadoTela, 'window','height=750,width=1200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
						}else{

							if(confirm("Nao existe ponto de consumo ativo para o contrato selecionado. Deseja encerrar o contrato?")) {
								window.open('exibirPopupPontoConsumoContrato?&chavePrimaria='+idContrato+'&chamadoTela='+chamadoTela, 'window','height=750,width=1200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
							}
					}

				}
			 );
}


function verificarGarantiaServicos(id, callback) {
    verificarGarantia(null, id, callback);
}
function verificarGarantia(chavePrimariaCliente, idBotao, callback) {

    var chaveCliente;
    if (chavePrimariaCliente == null) {
        chaveCliente = document.forms[0].chaveCliente ? document.forms[0].chaveCliente.value : null;
    } else {
        chaveCliente = chavePrimariaCliente;
    }

    var chamadoAssunto = $("#chamadoAssunto :selected").val();
    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    function executarCallback(){
        if (callback != null)
            callback();
    }

    if (chaveCliente != '' && chaveCliente > 0 && chamadoAssunto != null && chamadoAssunto > 0 && chavePrimariaImovel != '' && chavePrimariaImovel > 0) {
        AjaxService.garantiaVigente(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
            callback: function (liberado) {


                $('#footer-disponivel').hide();
                $('#footer-indisponivel').hide();
                $('#notificacao-servico-indisponivel').hide();
                $('#notificacao-servico-disponivel').hide();

                $('#titulo-servicos-garantia-indisponivel').hide();
                $('#titulo-servicos-garantia-disponivel').hide();

                ocultarLoading(idBotao);
                if (liberado) {
                    $('#titulo-servicos-garantia-disponivel').show();
                    $('#footer-disponivel').show();
                    $('#notificacao-servico-disponivel').show();

                    if (callback != null) {
                        $('#button-salvar-servico-disponivel').one("click", function (event) {
                            if (idBotao != undefined) {
                                exibirLoading(idBotao);
                                executarCallback();
                            }
                        });
                        if (idBotao != undefined) {
                            servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                        }
                    }

                } else {
                    $('#titulo-servicos-garantia-indisponivel').show();
                    $('#footer-indisponivel').show();
                    $('#notificacao-servico-indisponivel').show();
                    servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                }

            },
            async: true
        });
    } else {
        executarCallback();
    }
}

function servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback) {
    AjaxService.servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
        callback: function (tipos_servicos) {

            function getIniciarTR() {
                return '<tr style="border-bottom: 1px solid #dbdddc;">';
            }

            function __getIlimitado(){
                return '<td>Ilimitado</td>';
            }

            function getPrazoGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    return '<td>' + tipos_servicos[i].prazoGarantia + '</td>';

                } else {
                    return __getIlimitado();
                }
            }

            function getNaGarantia() {
                if (tipos_servicos[i].naGarantia != null) {
                    if (tipos_servicos[i].naGarantia) {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" />' + ' Dispon�vel</td>';
                    } else {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'cancel16.png" />' + ' <span style="color: red">Indispon�vel</span></td>';
                    }
                } else {
                    return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" /> Ilimitado</td>';
                }
            }

            function getDescricao() {
                return '<td>' + tipos_servicos[i].descricao + '</td>';
            }
            function getNumeroMaximoExecucoes() {
                if (tipos_servicos[i].numeroMaximoExecucoes != null) {
                    return '<td>' + tipos_servicos[i].numeroMaximoExecucoes + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getNumeroExecucoes() {
                if (tipos_servicos[i].numeroExecucoesDisponiveis != null) {
                    return '<td>' + tipos_servicos[i].numeroExecucoesDisponiveis + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getDataLimiteGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    if (tipos_servicos[i].dataLimiteGarantia == null) {
                        return '<td>Nao iniciado</td>';
                    } else {
                        return '<td>' + tipos_servicos[i].dataLimiteGarantia + '</td>';
                    }
                } else {
                    return __getIlimitado();
                }
            }

            function getDataExecucacao() {
                if (tipos_servicos[i].dataExecucacao != null) {
                    return '<td>' + tipos_servicos[i].dataExecucacao + '</td>';
                } else {
                    return '<td>Nao executado</td>';
                }
            }

            function getTerminarTR() {
                return '</tr>';
            }

            if (tipos_servicos.length > 0) {
                var imagem = $('#url_imagens').val();
                var styleImage = 'style="margin-bottom: 2px;"';
                for (var i in tipos_servicos) {
                    var inner = getIniciarTR();

                    inner += getNaGarantia();
                    inner += getDescricao();
                    inner += getNumeroMaximoExecucoes();
                    inner += getNumeroExecucoes();
                    inner += getDataExecucacao();
                    inner += getPrazoGarantia();
                    inner += getDataLimiteGarantia();
                    inner += getTerminarTR();

                    $("#corpoGridServicoTipoGarantia").append(inner);
                }
                $("#dialogGarantiaServico").modal('show');
            } else if (callback != null){
                callback();
            }
        },
        async: true
    });
}
