
$(document).ready(function () {

    var fluxoDetalhamento = $('#fluxoDetalhamento').val();
    var fluxoAlteracao = $('#fluxoAlteracao').val();
    var fluxoInclusao = $('#fluxoInclusao').val();
    var fluxoTramitacao = $('#fluxoTramitacao').val();
    var fluxoAlteracaoRascunho = $('#fluxoAlteracaoRascunho').val();
    var fluxoReiteracao = $('#fluxoReiteracao').val();
    var fluxoReativacao = $('#fluxoReativacao').val();
    var fluxoReabertura = $('#fluxoReabertura').val();
    var fluxoEncerramento = $('#fluxoEncerramento').val();
    var fluxoCapturar = $('#fluxoCapturar').val();
    var popup;
    var IndicadorObrigatorio = $('#IndicadorObrigatorio').val();
    var exibirPassaporte = $('#exibirPassaporte').val();

    $('#dataPrevisaoEncerramento').attr('disabled', 'disabled');

    if (fluxoInclusao != "") {
        var chaveChamadoAssunto = $("[name='chamado.chamadoAssunto.chavePrimaria']").val();
        if (chaveChamadoAssunto != "" && chaveChamadoAssunto != undefined) {
            carregarResponsavelPorChamadoAssunto(chaveChamadoAssunto);
            carregarPrazoDiferenciado(chaveChamadoAssunto);
        }
    }

    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();
    if (chavePrimariaImovel != null && chavePrimariaImovel != undefined) {
        carregarPontosConsumo(chavePrimariaImovel);
    }

    if (IndicadorObrigatorio == "2") {
        $('span[id="spanTipoManifestacao"]').remove();

    }

    if (exibirPassaporte == "2") {
        $("#divPassaporte").hide();
    }

    if (fluxoInclusao) {
        $('#dataResolucao').attr('disabled', 'disabled');
        $('#prazoPrevisto').attr('disabled', 'disabled');
        $("#indicadorPendenciaSim").removeAttr("disabled");
        $("#indicadorPendenciaNao").removeAttr("disabled");
    }

    if($('#fluxoAlteracaoRascunho').val() == "true"){
        $("#indicadorAcionamentoSim").removeAttr("disabled");
        $("#indicadorAcionamentoNao").removeAttr("disabled");
        $("#acionamentoGasista").removeAttr("disabled");
        $("#acionamentoPlantonista").removeAttr("disabled");
        $("#informacaoAdicional").removeAttr("disabled");
    }



    if (fluxoInclusao == "" && fluxoAlteracaoRascunho == "") {
        $("form :input").not("input[type=button]").not('button').attr("disabled", "disabled");
        $("input[type=submit]").removeAttr("disabled");
        $("#bottonChamadoContato").attr("disabled", "disabled");
        $("#nomeContato").removeAttr("disabled");
        $("#codigoDDD").removeAttr("disabled");
        $("#foneContato").removeAttr("disabled");
        $("#ramalContatoBootstrap").removeAttr("disabled");
        $("#descricaoArea").removeAttr("disabled");
        $("#emailContato").removeAttr("disabled");
        $("#tipoContato").removeAttr("disabled");
        $("#profissaoContato").removeAttr("disabled");
        removerAsteriscos();
    }

    $("input[type=hidden]").removeAttr("disabled");
    $("input[name=checkPontoConsumo]").removeAttr("disabled");

    if (fluxoDetalhamento == "") {
        $("#descricaoAnexo").removeAttr("disabled");
        $(".file").removeAttr("disabled");
        $(".campoFile").removeAttr("disabled");
    }

    if (fluxoAlteracao != "") {
        document.getElementById("descricao").disabled = false;
        document.getElementById("descricaoEmail").disabled = false;
        document.getElementById("bottonAlterarEmail").disabled = true;
        document.getElementById("unidadeOrganizacional").disabled = true;
        document.getElementById("chamadoAssunto").disabled = true;
        document.getElementById("bottonChamadoContato").disabled = false;
        if (document.getElementById("divGridContato") != null) {
            document.getElementById("divGridContato").style.display = 'none';
        }

        setTimeout(function(){
            document.getElementById("unidadeOrganizacional").disabled = true;
        },1000);

        var chaveCliente = $("#chaveCliente").val();

        if ($("#chaveCliente").val() != "") {
            var url = encodeURI("exibirContatoCliente?chavePrimariaCliente=" + chaveCliente);
            carregarFragmento('gridAbaChamadoContato', url);
        }


        if ($('#exibirMensagemTela').val() == 'true') {
            $("div.mensagensSpring").show();
        }

        $("#spanDescricao").html('*&nbsp;');
        $("#indicadorPendenciaSim").removeAttr("disabled");
        $("#indicadorPendenciaNao").removeAttr("disabled");

    }
    if (fluxoTramitacao) {
        $('#unidadeOrganizacionalVisualizadoras').removeAttr('disabled');
        $('#unidadeOrganizacional').removeAttr('disabled');
        $('#descricao').removeAttr('disabled');
        $('#usuarioResponsavel').removeAttr('disabled');
        $("#spanDescricao").html('*&nbsp;');
        $('#spanUnidadeOrganizacional').html('*&nbsp;');
        $('#chamadoAssunto').attr('disabled', 'disabled');

    }
    if (fluxoReiteracao || fluxoReativacao || fluxoReabertura || fluxoEncerramento || fluxoCapturar) {
        $("#spanDescricao").html('*&nbsp;');
        $('#descricao').removeAttr('disabled');
        $('#indicadorAgenciaReguladoraSim').removeAttr('disabled');
        $('#indicadorAgenciaReguladoraNao').removeAttr('disabled');
    }
    if (fluxoReativacao || fluxoReabertura || fluxoEncerramento) {
        $('#idMotivo').removeAttr('disabled');
    }
    if (fluxoEncerramento) {
        $("#dataResolucao").inputmask("99/99/9999 99:99", {placeholder: "_", greedy: false});
        $("#dataResolucao").datetimepicker({
            maxDate: '+0d',
            showOn: 'button',
            buttonImage: $('#imagemCalendario').val(),
            buttonImageOnly: true,
            buttonText: 'Exibir Calendário',
            dateFormat: 'dd/mm/yy',
            timeFormat: 'HH:mm'
        });
        $('#dataResolucao').removeAttr('disabled');
    }
    if (fluxoCapturar) {
        $("#spanResponsavel").html('*&nbsp;');
    }

    var indicadorVerificarVazamento = $("[name='chamado.chamadoAssunto.indicadorVerificarVazamento']").val() == "true";

    if (fluxoEncerramento && indicadorVerificarVazamento) {
        $('#indicadorVazamentoConfirmadoSim').removeAttr('disabled');
        $('#indicadorVazamentoConfirmadoNao').removeAttr('disabled');
    }

    var chamadoTipo = $('#chamadoTipo.chavePrimaria').val();
    if (chamadoTipo != null && chamadoTipo != "" && chamadoTipo != undefined) {
        var chavePrimaria = chamadoTipo;
        atualizarAssuntos(chavePrimaria);
    }

    if (fluxoAlteracao != "" && fluxoAlteracaoRascunho == "") {
        document.getElementById("alterarRascunho").disabled = true;
    }

    if (fluxoAlteracao != "" || fluxoAlteracaoRascunho != "") {
        $("#usuarioResponsavel").attr("disabled", "disabled");
        $("#usuarioResponsavelNome").attr("disabled", "disabled");
        $("#spanResponsavel").remove();
    }

    $("#dialogGerarAutorizacaoServico").dialog({
        autoOpen: false,
        modal: true,
        width: 1000,
        resizable: false,
        draggable: false
    });

    $(".notification.failure.hideit").show();
    $(".notification.mensagemAlerta.hideit").show();

    if (campoPreenchido('#chamadoTipo') && campoPreenchido('#chaveAssuntoChavePrimaria')) {
        atualizarAssuntos($('#chamadoTipo').val());
        carregarResponsavelPorChamadoAssunto('0');
    }

    habilitarDesabilitarBotaoCopiarObservacao();
    exibirUnidadesQuePodemVisualizar();
    
    $('#turnoPreferencia').attr('disabled', false);

    
	var idMotivo = $("#idMotivo");
	if(idMotivo.val() != -1){
		if(idMotivo.val() == "483" || idMotivo.val() == "482"){
			$("#dataResolucao").attr("disabled",true);
			$("#dataResolucao").parent().find("img").addClass("hide")
			$("#dataResolucao").val("");
		}else{
			$("#dataResolucao").attr("disabled",false);
			$("#dataResolucao").parent().find("img").removeClass("hide")
		}
	}else{
		$("#dataResolucao").attr("disabled",false);
		$("#dataResolucao").parent().find("img").removeClass("hide")
		$("#dataResolucao").val("");
	}
	
	
	if (fluxoAlteracao) {
		$("#gridTituloAberto").find('input, button, select').prop('disabled', false);
	}

});

function exibirUnidadesQuePodemVisualizar() {
    /**
     * Paliativo para:
     * O campo ?Unidades que podem visualizar? mostra a op??o ?3 selecionados? mas fica desabilitado para sele??o:
     *
     * O problema ocorre somente para dispositivos m?veis (chrome), n?o sendo poss�vel visualizar as unidades que est?o selecionados
     * O ideal ? substituir o select default por https://silviomoreto.github.io/bootstrap-select/ para que a exibi??o dos que
     * est?o selecionados seja vis?vel sempre.
     */
    $('#divUnidadeOrganizacionalVisualizadoras').on('click touch', function () {
        if ($('#unidadeOrganizacionalVisualizadoras').attr('disabled') !== undefined) {
            console.log('unidadeOrganizacionalVisualizadoras', event);

            var unidadesSelecioandasTemp = $('#unidadeOrganizacionalVisualizadoras option:selected').text().split('\n');

            $('#ul-unidades-podem-visualizar').empty();
            for (var iU in unidadesSelecioandasTemp) {
                if (unidadesSelecioandasTemp[iU].trim() !== '') {
                    $('#ul-unidades-podem-visualizar').append('<li class="list-group-item">'+unidadesSelecioandasTemp[iU].trim()+'</li>');
                }
            }

            $('#modal-unidade-podem-visualizar').modal('show');
        }
    });
}

function exibirDialogGerarAutorizacaoServico(fluxo) {
    $("#corpoGridServicoTipo").html("");
    var chaveChamado = $("#chavePrimaria").val();
    var cont = 0;
    var param = "";
    var chaveAssunto = null;
    
    
    if (typeof fluxo === "undefined" || fluxo == "incluir" || fluxo == "salvarAgendar") {
		chaveChamado = "";
	}

    if ($("#chamadoAssunto :selected").size() > 0) {
        chaveAssunto = $("#chamadoAssunto :selected").val();
    } else {
        alert("Selecione o Assunto do Chamado");
    }

    var chaveImovel = document.forms[0].imovel.value;

    AjaxService.obterListaServicoTipoPorChamado(chaveChamado, chaveAssunto, {
            callback: function (listaServicoTipo) {
                if (listaServicoTipo.length > 0) {
                    AjaxService.listarEquipes({
                        callback: function (entidade) {

                            var combo = $('#listaEquipe');
                            combo.html('<option value="-1">Selecione</option>');

                            for (key in entidade) {
                                var o = new Option(entidade[key], key);
                                combo.append(o);
                                console.log(o);
                            }

                            for (key in listaServicoTipo) {
                                var servicoTipo = listaServicoTipo[cont];

                                if ((cont + 2) % 2 == 0) {
                                    param = "odd";
                                } else {
                                    param = "even";
                                }

                                var chavePrimaria = servicoTipo['chavePrimaria'];
                                var descricao = servicoTipo['descricao'];
                                var prioridade = servicoTipo['prioridade'];
                                var regulamentado = servicoTipo['regulamentado'];

                                AjaxService.verificarRestricaoServico(chavePrimaria, chaveChamado, chaveImovel, chaveAssunto, {
                                    callback: function (servicoRestrito) {

                                        var inner = '';
                                        inner = inner + '<tr class=' + param + '>';
                                        if (servicoRestrito) {

                                            inner = inner + '<td ><input type="checkbox" disabled name="chavesPrimariasDialogDisabled" value="' + chavePrimaria + '"/></td>';
                                        } else {
                                            inner = inner + '<td ><input type="checkbox" checked name="chavesPrimariasDialog" value="' + chavePrimaria + '"/></td>';
                                        }
                                        inner = inner + '<td>' + descricao + '</td>';
                                        inner = inner + '<td>' + prioridade + '</td>';
                                        inner = inner + '<td>' + regulamentado + '</td>';
                                        if (servicoRestrito) {
                                            inner = inner + '<td>' + 'Sim' + '</td>';
                                        } else {
                                            inner = inner + '<td>' + 'Nao' + '</td>';
                                        }

                                        inner = inner + '</tr>';

                                        $("#corpoGridServicoTipo").append(inner);


                                        cont++;

                                        exibe = true;

                                    }, async: false
                                });
                                
                                carregarChaveEquipePrioritaria(chavePrimaria);
                                carregarTurnosEquipe();
                            }

                        }
                        , async: false
                    });
                } else if (fluxo != undefined && fluxo != "alteracaoRascunho") {
                    $("form :input").removeAttr("disabled");
                    submeterFormChamado('salvarAgendar');
                } else {
                    alert("Nao existe Autoriza��o de Servi�o para esse chamado");
                }
            }, async: false
        }
    );
    if ($("input[name='chavesPrimariasDialog']").size() > 1) {
        if (fluxo == "salvarAgendar") {
			
			if($("#divPrazo").val() != '' && $("#divPrazo").val() <= 12) {
				 $("#botaoGerar").attr("onclick", "incluirChamadoASAutomatica()");
			} else {
	            $("#botaoGerar").attr("onclick", "salvarGerar()");
			}
			
            //$("input[name='chavesPrimariasDialog']").attr('disabled','disabled');
            //$("#checkTable").attr('disabled','disabled');
        } else if (fluxo == "incluir") {
            $("#botaoGerar").attr("onclick", "incluirChamadoASAutomatica()");
            //$("input[name='chavesPrimariasDialog']").attr('disabled','disabled');
            //$("#checkTable").attr('disabled','disabled');
        } else if (fluxo == "alteracaoRascunho") {
            $("#botaoGerar").attr("onclick", "alterarChamadoASAutomatica()");
            //$("input[name='chavesPrimariasDialog']").attr('disabled','disabled');
            //$("#checkTable").attr('disabled','disabled');
        }
        $("#dialogGerarAutorizacaoServico").parent().css({position: "relative"}).end().dialog('open');
    } else if (fluxo == "incluir") {

        alert("Tipo de chamado n\u00E3o possui servi\u00E7os para gera\u00E7\u00E3o autom\u00E1tica ou servi\u00E7os s\u00E3o restritos para este im\u00F3vel.");
        $("form :input").removeAttr("disabled");
        submeterFormChamado('incluirChamado');
    } else if (fluxo == "salvarAgendar" || fluxo == "alteracaoRascunho") {

        alert("Tipo de chamado n\u00E3o possui servi\u00E7os para gera\u00E7\u00E3o autom\u00E1tica ou servi\u00E7os s\u00E3o restritos para este im\u00F3vel.");

    }
    $('#listaEquipe').removeAttr('disabled');
}

function salvarGerar() {
	
    if ($("#listaEquipe :selected").size() > 0 && $("#listaEquipe :selected").val() != -1) {
        exibirLoading("#botaoGerar");
        document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();  
      	document.getElementById("idTurnoPreferencia").value = $("#turnoPreferencia :selected").val();

        $("form :input").removeAttr("disabled");
        
        obterChavesPrimariasDialogChecked();
        
        submeterFormChamado('salvarAgendar');
    } else {
        verificarSelecaoDialog();
        alert("Selecione uma equipe para designar a(s) AS");
    }
}
function obterChavesPrimariasDialogChecked(){
    var listaChecked = new Array();
    var i = 0;
    $('input[name="chavesPrimariasDialog"]').each(function () {
        if (this.checked) {
            listaChecked[i] = $(this).val();
            i++;
        }
    });
    
    document.getElementById("chavesPrimariasDialog").value = listaChecked;
}

function selectAll(check) {
	var checkTodos = $("#checkTable");
	
	checkTodos.change(function () {
		if ($(this).is(':checked') ){
			$('input:checkbox').prop("checked", true);
		}else if (!$(this).is(':checked') ){
			$('input:checkbox').prop("checked", false);
		}
	});
	uncheckAllUncheck();
	checkAllCheck();
}

/**
 * Desabilita o check todos depois que desmarcou um item.
 */
function uncheckAllUncheck(){
	$('input[name="chavesPrimariasDialog"]:checked').change(function () {
    	if($('input:checkbox').is(':checked', false)){
	    	$("#checkTable").prop("checked", false);
	    }
    	else{
    		$('input:checkbox').prop("checked", true);
    	}
	}); 
}


/**
 * Habilita o check todos depois que marcou todos os itens.
 */
function checkAllCheck(){
	var checkbox =  $('input[name="chavesPrimariasDialog"]');
	var listaChecked = new Array();
    var i = 0;
	
    $('input[name="chavesPrimariasDialog"]').each(function () {
        if (this.checked) {
            listaChecked[i] = $(this).val();
            i++;
        }
   });
	
	checkbox.change(function (){
		if($('input[name="chavesPrimariasDialog"]').is(':checked')){
			if(i === $('input[name="chavesPrimariasDialog"]:checked').length){
				$("#checkTable").prop("checked", true);
			}
		}
	});
}

/**
 * Verifica se algum item foi selecionado na lista
 */
function verificarSelecaoDialog() {
    if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
        alert("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
        return false;
    }
    return true;
}

function gerarAutorizacaoServico() {
	var selecao = verificarSelecaoDialog();

	if ($("#listaEquipe :selected").val() == "-1") {
		alert("Selecione uma equipe para designar a(s) AS");
		if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
			alert("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
			return false;
		}
	} else {

		if (selecao == true) {
			exibirLoading('#botaoGerar');
			var listaChecked = new Array();
			var i = 0;
			$('input[name="chavesPrimariasDialog"]').each(function() {
				if (this.checked) {
					listaChecked[i] = $(this).val();
					i++;
				}
			});
			document.getElementById("chavesPrimariasDialog").value = listaChecked;
			document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
			document.getElementById("idTurnoPreferencia").value = $("#turnoPreferencia :selected").val();


			document.getElementById("isGerarAsAutomatica").value = "true";

			submeter('chamadoForm', 'gerarAutorizacaoServico');


		}
	}
}

function submetAutorizacaoService() {

}

function incluirChamadoASAutomatica() {
	
	var retorno = verificarServicoTipoMesmaOperacao();
	
	if(retorno != null && !retorno) {
		return false;
	}

	selecao = true;
	if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
		selecao = false;
	}

	if (selecao == true) {
		if ($("#listaEquipe :selected").val() == "-1") {
			alert("Selecione uma equipe para designar a(s) AS");
		} else {

			var listaChecked = new Array();
			var i = 0;
			$('input[name="chavesPrimariasDialog"]').each(function() {
				if (this.checked) {
					listaChecked[i] = $(this).val();
					i++;
				}
			});
			document.getElementById("chavesPrimariasDialog").value = listaChecked;
			document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
			document.getElementById("idTurnoPreferencia").value = $("#turnoPreferencia :selected").val();
			$("form :input").removeAttr("disabled");

			document.getElementById("isGerarAsAutomatica").value = "true";
			submeterFormChamado('incluirChamado');

		}
	} else {
		submeterFormChamado('incluirChamado');
	}
}

function alterarChamadoASAutomatica() {
    selecao = true;
    if (!$('input[name="chavesPrimariasDialog"]:checked').length > 0) {
        selecao = false;
    }

    if (selecao == true) {
        if ($("#listaEquipe :selected").val() == "-1") {
            alert("Selecione uma equipe para designar a(s) AS");
        } else {

            var listaChecked = new Array();
            var i = 0;
            $('input[name="chavesPrimariasDialog"]').each(function () {
                if (this.checked) {
                    listaChecked[i] = $(this).val();
                    i++;
                }
            });
            document.getElementById("chavesPrimariasDialog").value = listaChecked;
            document.getElementById("idEquipe").value = $("#listaEquipe :selected").val();
            submeter('chamadoForm', 'alterarChamado');
        }
    } else {
        submeter('chamadoForm', 'alterarChamado');
    }
}

function removerAsteriscos() {
    $('span[id="spanTipoChamado"]').remove();
    $('span[id="spanChamadoAssunto"]').remove();
    $('span[id="spanCanalAtendimento"]').remove();
    $('span[id="spanTipoManifestacao"]').remove();
    $("#spanUnidadeOrganizacional").html('');
    $('span[id="spanNumeroContrato"]').remove();
    $("#spanDescricao").html('');
    $("#spanResponsavel").html('');
}

function incluir() {
	
	var retorno = verificarServicoTipoMesmaOperacao();
	
	if(retorno != null && !retorno) {
		return false;
	}
	
    exibirLoading("#botaoIncluir");
    document.forms[0].rascunho.value = false;

    if (document.forms[0].cliente.value == "") {
        document.forms[0].cliente.value = document.forms[0].chaveCliente.value;
    }

    if ($("#fluxoTramitacao").val() == "true" && !validarUnidadesVisualizadoras()) {
       ocultarLoading('#botaoIncluir');
       return;
    }

    var chaveAssunto = document.forms[0].chamadoAssunto.value;
    var chaveImovel = document.forms[0].imovel.value;

    verificarGarantiaServicos("#botaoIncluir", function() {
        exibirLoading("#botaoIncluir");
        AjaxService.verificarGeracaoAutomaticaASChamado(chaveAssunto, {
            callback: function (geraASAutomatico) {

                if (geraASAutomatico) {
                // if (true) {
                    ocultarLoading('#botaoIncluir');
                    exibirDialogGerarAutorizacaoServico("incluir");
                } else {
                    $("form :input").removeAttr("disabled");
                    submeterFormChamado('incluirChamado');
                }
            },
            async: false
        });
    })

}

function incluirCopiar() {
	
	var retorno = verificarServicoTipoMesmaOperacao();
	
	if(retorno != null && !retorno) {
		return false;
	}
	
    document.forms[0].rascunho.value = false;
    submeterFormChamado('incluirChamadoCopiar');
}

function incluirRascunho() {
	
	var retorno = verificarServicoTipoMesmaOperacao();
	
	if(retorno != null && !retorno) {
		return false;
	}	
	
    exibirLoading("#botaoIncluirRascunho");
    document.forms[0].rascunho.value = true;
    submeterFormChamado('incluirChamado');
}

function exibirAlterarChamado() {
    submeter('chamadoForm', 'exibirAlteracaoChamadoInclusao');
}

function exibirAlterarChamadoGrid() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'exibirAlteracaoChamadoInclusao');
    }
}

function alterarChamado() {
    $("form :input").removeAttr("disabled");
    document.getElementById("fluxoAlteracaoRascunho").value = false;
    exibirLoading("#botaoAlterar");

    verificarGarantiaServicos('#botaoAlterar', function() { submeter('chamadoForm', 'alterarChamado')} );
}


function alterarRascunhoChamado() {
    $("form :input").removeAttr("disabled");
    submeter('chamadoForm', 'alterarChamado');
}

function copiarChamado() {
    submeter('chamadoForm', 'copiarChamado');
}

function copiarChamadoGrid() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoForm', 'copiarChamado');
    }
}

function exibirTramitacaoChamado() {
    submeter('chamadoForm', 'exibirTramitacaoChamado');
}

function tramitarChamado() {
    console.log('tramitar');
    if($('#unidadeOrganizacional').val()){
        $("form :input").removeAttr("disabled");
        exibirLoading("#botaoTramitar");
        submeter('chamadoForm', 'tramitarChamado');
    }else{
        alert('Selecione uma unidade organizacional');
    }

}

function exibirReiteracaoChamado() {
    submeter('chamadoForm', 'exibirReiteracaoChamado');
}

function exibirEncerrarChamado() {
    submeter('chamadoForm', 'exibirEncerramentoChamado');
}

function encerrarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoEncerrar");
    submeter('chamadoForm', 'encerrarChamado');
}

function reiterarChamado() {
    $("#buttonReiterar").attr("disabled", "disabled");
    exibirLoading("#botaoReiterar");
    submeter('chamadoForm', 'reiterarChamado');
}

function exibirReativacaoChamado() {
    submeter('chamadoForm', 'exibirReativacaoChamado');
}

function reativarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoReativar");
    submeter('chamadoForm', 'reativarChamado');
}

function exibirReaberturaChamado() {
    submeter('chamadoForm', 'exibirReaberturaChamado');
}

function reabrirChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoReabrir");
    submeter('chamadoForm', 'reabrirChamado');
}

function exibirCapturarChamado() {
    $("form :input").removeAttr("disabled");
    submeter('chamadoForm', 'exibirCapturarChamado');
}

function capturarChamado() {
    $("form :input").removeAttr("disabled");
    exibirLoading("#botaoCapturar");
    submeter('chamadoForm', 'capturarChamado');
}

function carregarResponsavel(chaveUnidadeOrganizacional) {
    var noCache = "noCache=" + new Date().getTime();
    if(chaveUnidadeOrganizacional != ""){
    	$("#usuarioResponsavel").load("carregarResponsavel?chaveUnidadeOrganizacional=" + chaveUnidadeOrganizacional + "&" + noCache);
    }
}

function cancelar() {

    if (document.forms['chamadoForm'].usuarioResponsavel != undefined) {
        document.getElementById("usuarioResponsavel").value = '';
    }
    $("#fluxoVoltar").val("true");
//    submeter('chamadoForm', 'exibirPesquisaChamado');
	submeter("chamadoForm",  "pesquisarChamado");

}

function voltar() {
	$("#fluxoVoltar").val("true");
	submeter("chamadoForm",  "pesquisarChamado");
}

function limparFormulario() {
    submeter('chamadoForm', 'exibirInclusaoChamado?acao=' + "Limpar");
}

function limparFormExpecifico() {
    document.forms['chamadoForm'].descricao.value = '';
    document.forms['chamadoForm'].idMotivo.value = '-1';
}

function limparFormTramitacao() {
    document.forms['chamadoForm'].descricao.value = '';
    $(document.forms[0].unidadeOrganizacional).find("option").attr("selected", false);
    document.forms[0].usuarioResponsavel.value = -1;
}

function limparFormRascunho() {
    submeter('chamadoForm', 'exibirAlteracaoRascunhoChamado');
}

function atualizarListaTipoChamado(idSegmento) {
    var noCache = "noCache=" + new Date().getTime();
    $("#divTiposChamados").load("carregarTiposChamado?idSegmento=" + idSegmento + "&" + noCache);
    $("#divAssuntos").html("<label for='chamadoAssunto'>" +
        "Assunto do Chamado: <span class='text-danger'>*</span></label>" +
        "<select id='chamadoAssunto' name='chamadoAssunto' class='form-control form-control-sm' " +
        "onchange=onChangeAssuntoChamado(this.value); verificarGarantia()>" +
        "<option value=''>Selecione o tipo de chamado...</option>"+
        "</select>");
}

function atualizarAssuntos(chave) {
    $('#chamadoAssunto').attr('disabled', true);

    var noCache = "noCache=" + new Date().getTime();
    var chaveAssunto = document.forms[0].chaveAssunto.value;
    var fluxoInclusao = document.forms[0].fluxoInclusao.value;
    $("#divAssuntos").load("carregarAssuntos?chavePrimaria=" + chave + "&chaveAssunto=" + chaveAssunto + "&" + noCache + "&fluxoInclusao=" + fluxoInclusao, function () {

        if ($("#fluxoDetalhamento").val() == "true" || $("#fluxoTramitacao").val() == "true" || $('#fluxoAlteracao').val() ==  "true") {
            $('#chamadoAssunto').attr('disabled', true);
        } else {
            $('#chamadoAssunto').attr('disabled', false);
        }

        if (campoPreenchido('#chamadoAssunto')) {
            onChangeAssuntoChamado($('#chamadoAssunto').val());
        }
    });
}

function atualizarAssuntosLiberado() {
    var chaveCliente = document.forms[0].chaveCliente.value;

    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    $('#chamadoAssunto').attr('disabled', true);

    var chave = $('#chamadoTipo').val();
    var chaveAssunto = document.forms[0].chaveAssunto.value;
    var noCache = "noCache=" + new Date().getTime();
    exibirLoading('');
    $("#divAssuntos").load("carregarAssuntos?chavePrimaria=" + chave + "&chaveAssunto=" + chaveAssunto +"&chavePrimariaCliente="+chaveCliente+"&chavePrimariaImovel="+chavePrimariaImovel+ "&" + noCache, function () {

        if ($("#fluxoDetalhamento").val() == "true" || $("#fluxoTramitacao").val() == "true" || $('#fluxoAlteracao').val() ==  "true") {
            $('#chamadoAssunto').attr('disabled', true);
        } else {
            $('#chamadoAssunto').attr('disabled', false);
        }

        ocultarLoading('');
        if (campoPreenchido('#chamadoAssunto')) {
            onChangeAssuntoChamado($('#chamadoAssunto').val());
        }
    });
}

function carregarUnidadeOrganizacional(chave) {
    if (chave != "") {
        carregarOrientacao(chave);
        carregarAcionamentoGasista(chave);
        var noCache = "noCache=" + new Date().getTime();
        var idUnidadeOrganizacionalChamado = $('#idUnidadeOrganizacionalChamado').val();
        $("#divUnidadeOrganizacional").load("carregarUnidadeOrganizacional?chavePrimariaAssunto=" + chave +
            "&idUnidadeOrganizacionalChamado=" + idUnidadeOrganizacionalChamado + "&" + noCache);
    }
}

function carregarPrazo(chavePrimaria) {

    AjaxService.carregarPrazoPrevisto(chavePrimaria, {
        callback: function (quantidadeHorasPrevistaAtendimento) {
            $("#divPrazo").val(quantidadeHorasPrevistaAtendimento);
        },
        async: true
    });
}

function carregarOrientacao(chaveAssunto) {

    AjaxService.carregarOrientacaoAssunto(chaveAssunto, {
        callback: function (orientacao) {
            $("#orientacaoAssunto").val(orientacao);
            habilitarDesabilitarBotaoCopiarObservacao();
        },
        async: true
    });
}

function selecionaCanalAtendimento(chaveAssunto) {

    AjaxService.obterCanalAtendimetno(chaveAssunto, {
        callback: function (canalAtendimento) {
            if (canalAtendimento != "") {
                if(fluxoDetalhamento){
                    if(!$('#canalAtendimento').val()){
                        $("#canalAtendimento option").attr("selected", false);
                        $("#canalAtendimento option[value='" + canalAtendimento + "']").attr("selected", true);
                    }
                }else{
                    $("#canalAtendimento option").attr("selected", false);
                    $("#canalAtendimento option[value='" + canalAtendimento + "']").attr("selected", true);
                }
            }
        },
        async: true
    });
}

function carregarAcionamento() {

    if ($('#exibirAcionamentoGasista').val() == 'true') {


        var infoAcionamento = "";
        var span = "";
        var disabled = " disabled='disabled' ";

        var informacao = $("#informacaoAdicionalInfo").val();

        if ($('#habilitaAcionamento').val() == 'true') {
            disabled = "";
        }

        if($('#fluxoAlteracaoRascunho').val() == "true"){
            disabled = "";
        }



        var fluxoInclusao = $('#fluxoInclusao').val();
        if (fluxoInclusao != "") {
            span = "<span class='campoObrigatorioSimbolo'>* </span>";
        }

        var isGasista = $("[name='chamado.indicadorAcionamentoGasista']").val() == "true";
        var checkedGasista = isGasista ? "checked='checked'" : '';
        var isPlantonista = $("[name='chamado.indicadorAcionamentoPlantonista']").val() == "true";
        var checkedPlantonista = isPlantonista ? "checked='checked'" : '';

        var checkedOuGasista = ($("[name='chamado.indicadorAcionamentoGasista']").val() || $("[name='chamado.indicadorAcionamentoPlantonista']").val()) ? "checked='checked'" : '';
        var checkedEPlantonista = (!$("[name='chamado.indicadorAcionamentoGasista']").val() && !$("[name='chamado.indicadorAcionamentoPlantonista']").val()) ? "checked='checked'" : '';

        if ($("[name='chamado.indicadorAcionamentoPlantonista']").val() || $("[name='chamado.indicadorAcionamentoGasista']").val()) {
            infoAcionamento = "<input id='acionamentoGasista' class='campoRadio' type='checkbox' name='indicadorAcionamentoGasista' " + checkedGasista + " " + disabled + " />" +
                "<label class='rotuloRadio' for='acionamentoGasista'> Operador</label>" +
                "<input id='acionamentoPlantonista' class='campoRadio' type='checkbox' name='indicadorAcionamentoPlantonista' " + checkedPlantonista + " " + disabled + " />" +
                "<label class='rotuloRadio' for='acionamentoPlantonista'> Plantonista</label>" +
                "<label class='rotulo rotuloVertical rotuloCampoList' for='informacaoAdicional'>" + span + "Informações adicionais: </label>" +
                "<textarea class='campoTexto' name='informacaoAdicional'  id='informacaoAdicional' cols='35' rows='6' maxlength='1000' " + disabled + "> " + informacao + "</textarea>";
        }

        if($("[name='chamado.indicadorAcionamentoPlantonista']").val() == "true"){
            $('#acionamentoGasista').prop("checked", true);
        }

        if($("[name='chamado.indicadorAcionamentoGasista']").val() == "true"){
            $('#acionamentoPlantonista').prop("checked", true);
        }




        var checkAcionarGasista = $('#habilitaAcionamento').val() ? 'checked' : '';

        $("#divInformacaoGasista").html("<div class='col-md-12'>" +
            "<label>Houve necessidade de acionamento de gasista ou plantonista?</label>" +
            "<div class='custom-control custom-radio custom-control-inline'>" +
            "<input id='indicadorAcionamentoSim' class='custom-control-input' type='radio' value='true' onclick='mudarInformacaoGasista(this)' name='indicadorAcionamento' " + checkAcionarGasista + " " + disabled + " />" +
            "<label class='custom-control-label' for='indicadorAcionamentoSim'>Sim</label>" +
            "</div>" +
            "<div class='custom-control custom-radio custom-control-inline'>" +
            "<input id='indicadorAcionamentoNao' class='custom-control-input' type='radio' value='false' onclick='mudarInformacaoGasista(this)' name='indicadorAcionamento' " + checkedEPlantonista + " " + disabled + " /> " +
            "<label class='custom-control-label' for='indicadorAcionamentoNao'>Nao</label>" +
            "</div></div>" +
            "<div id='detalhamentoGasista'>" + infoAcionamento + "</div>");
            
          if (fluxoInclusao != "" || $('#fluxoAlteracaoRascunho').val() == "true" || $('#fluxoAlteracao').val() == "true" ) {
	           $("#horaAcionamento").removeAttr("disabled");
	           $("#informacaoAdicional").removeAttr("disabled");
	           $("#indicadorAcionamentoSim").removeAttr("disabled");
	           $("#indicadorAcionamentoNao").removeAttr("disabled");
	           $("#acionamentoGasista").removeAttr("disabled");
	           $("#acionamentoPlantonista").removeAttr("disabled");
	        }
    }

}

function carregarAcionamentoGasista(chaveAssunto) {

    AjaxService.possuiInformacaoAcionamentoGasista(chaveAssunto, {
        callback: function (informacaoAcionamentoGasista) {
            if (informacaoAcionamentoGasista != undefined && informacaoAcionamentoGasista) {


                var checkIndicadorGasista = $("[name='chamado.indicadorAcionamentoPlantonista']").val() == "true" || $("[name='chamado.indicadorAcionamentoGasista']").val() == "true" ? 'checked' : '';
                var checkIndicadorGasistaFalse = $("[name='chamado.indicadorAcionamentoPlantonista']").val() == "false" && $("[name='chamado.indicadorAcionamentoGasista']").val() == "false" ? 'checked' : '';
                var fluxoInclusao = $('#fluxoInclusao').val();
                var desabilitado ='';

                if (fluxoInclusao != "") {}else{
                    desabilitado = 'disabled'
                }

                if($('#fluxoAlteracaoRascunho').val() == "true"){
                    desabilitado = "";
                }

                $("#divInformacaoGasista").html("<div class='col-md-12'>" +
                    "<label>Houve necessidade de acionamento de gasista ou plantonista?</label>" +
                    "<div class='custom-control custom-radio custom-control-inline'>" +
                    "<input id='indicadorAcionamentoSim' class='custom-control-input' type='radio' value='true' onclick='mudarInformacaoGasista(this)' name='indicadorAcionamento' " + checkIndicadorGasista + " " + desabilitado + "/>" +
                    "<label class='custom-control-label' for='indicadorAcionamentoSim'>Sim</label>" +
                    "</div>" +
                    "<div class='custom-control custom-radio custom-control-inline'>" +
                    "<input id='indicadorAcionamentoNao' class='custom-control-input' type='radio' value='false' onclick='mudarInformacaoGasista(this)' name='indicadorAcionamento' " + checkIndicadorGasistaFalse + " " + desabilitado + " />" +
                    "<label class='custom-control-label' for='indicadorAcionamentoNao'>Nao</label>" +
                    "</div>" +
                    "</div>" +
                    "<div id='detalhamentoGasista'></div>");

		          if (fluxoInclusao != "" || $('#fluxoAlteracaoRascunho').val() == "true" || $('#fluxoAlteracao').val() == "true" ) {
			           $("#horaAcionamento").removeAttr("disabled");
			           $("#informacaoAdicional").removeAttr("disabled");
			           $("#indicadorAcionamentoSim").removeAttr("disabled");
			           $("#indicadorAcionamentoNao").removeAttr("disabled");
			           $("#acionamentoGasista").removeAttr("disabled");
			           $("#acionamentoPlantonista").removeAttr("disabled");
			        }


            } else {
                $("#divInformacaoGasista").html("");
            }

            if($("[name='chamado.indicadorAcionamentoPlantonista']").val() == "true"){
                $('#acionamentoGasista').prop("checked", true);
            }

            if($("[name='chamado.indicadorAcionamentoGasista']").val() == "true"){
                $('#acionamentoPlantonista').prop("checked", true);
            }


            if(checkIndicadorGasista){
                $( "#indicadorAcionamentoSim" ).click();
                $('#informacaoAdicional').val($('#informacaoAdicionalInfo').val());
            }

        },
        async: true
    });
}

function mudarInformacaoGasista(radio) {
    if (radio.value == "true") {

        var span = "";

        var fluxoInclusao = $('#fluxoInclusao').val();
        var desabilitado ='';

        if (fluxoInclusao != "") {
            span = "<span class='text-danger'>*</span>";
        }else{
            desabilitado = 'disabled'
        }

        if($('#fluxoAlteracaoRascunho').val() == "true"){
            desabilitado = "";
        } 
        
        var horaAcionamento = new Date().getHours() + "";
        var minutoAcionamento = new Date().getMinutes()+ "";
        horaAcionamento.length < 2 ? horaAcionamento = "0" + horaAcionamento : horaAcionamento = horaAcionamento;
        minutoAcionamento.length < 2 ? minutoAcionamento = "0" + minutoAcionamento : minutoAcionamento = minutoAcionamento;
        minutoAcionamento.length < 1 ? minutoAcionamento = "00" + minutoAcionamento : minutoAcionamento = minutoAcionamento;
        
        var horarioAcionamento = $("#horaAcionamentos").val() !== undefined ? $("#horaAcionamentos").val()  : "";
        //horarioAcionamento = horaAcionamento + ":" + minutoAcionamento; 

        $("#detalhamentoGasista").html("<div class='form-row'>" +
            "<div class='custom-control custom-checkbox custom-control-inline' style='margin-left: 4px'>" +
            "<input id='acionamentoGasista' class='custom-control-input' type='checkbox' name='indicadorAcionamentoGasista' "+ desabilitado +"/>" +
            "<label class='custom-control-label' for='acionamentoGasista'> Operador</label>" +
            "</div>" +
            "<div class='custom-control custom-checkbox custom-control-inline'>" +
            "<input id='acionamentoPlantonista' class='custom-control-input' type='checkbox' name='indicadorAcionamentoPlantonista'"+ desabilitado +"/>" +
            "<label class='custom-control-label' for='acionamentoPlantonista'> Plantonista</label>" +
            "</div>" +
            "</div>" +
            "<div class='form-row'>"+
            "<div class='col-md-12'>"+
    	        "<label for='dataAcionamento'>Data de acionamento:</label>"+
    	        "<div class='input-group input-group-sm'>"+
    	            "<input type='text' class='form-control form-control-sm' disabled "+
    	                   "id='dataAcionamento'"+
    	                   "name='dataAcionamento' placeholder='Data de acionamento'"+
    	                   "maxlength='10' size='10'"+
    	                   "/>"+
    	        "</div>"+
            "</div>"+
            "</div>"+
            "<div class='form-row'>"+
            	"<div class='col-md-12'>"+
                	"<label for='horaAcionamento'>Horário Acionamento:</label>"+
                    "<div class='input-group input-group-sm'>"+
                    	"<input type='text' class='form-control form-control-sm' disabled " +
                        	"id='horaAcionamento'"+
                            "name='horaAcionamento' placeholder='Horário de Acionamento'"+
                            "maxlength='16' size='16'"+
                            "value='" + horarioAcionamento + "'/>"+
                     "</div>"+
              	"</div>"+
            "</div>"+       
            "<div class='form-row'>" +
            "<div class='col-md-12'>" +
            "<label for='informacaoAdicional'>Informações adicionais: " + span + "</label>" +
            "<textarea  class='form-control form-control-sm' name='informacaoAdicional' id='informacaoAdicional' cols='35' rows='6' maxlength='1000' onblur='this.value = removerEspacoInicialFinal(this.value);'" +
            "onkeypress='return formatarCampoTextoLivreComLimite(event,this,800);' onpaste='return formatarCampoTextoLivreComLimite(event,this,800);' "+ desabilitado +"></textarea>" +
            "</div>" +
            "</div>");
     
            $("#horaAcionamento").inputmask("99:99", {placeholder: "_", greedy: false});


            if($("[name='chamado.indicadorAcionamentoGasista']").val() == "true"){
                $('#acionamentoGasista').prop("checked", true);
            }

            if($("[name='chamado.indicadorAcionamentoPlantonista']").val() == "true"){
                $('#acionamentoPlantonista').prop("checked", true);
            }
            
            
	        if (fluxoInclusao != "" || $('#fluxoAlteracaoRascunho').val() == "true" || $('#fluxoAlteracao').val() == "true" ) {
	           $("#horaAcionamento").removeAttr("disabled");
	           $("#informacaoAdicional").removeAttr("disabled");
	           $("#indicadorAcionamentoSim").removeAttr("disabled");
	           $("#indicadorAcionamentoNao").removeAttr("disabled");
	           $("#acionamentoGasista").removeAttr("disabled");
	           $("#acionamentoPlantonista").removeAttr("disabled");
	           $("#dataAcionamento").removeAttr("disabled");
	        }

    } else {
        $("#detalhamentoGasista").html("");
    }
    
    $('#dataAcionamento').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

function carregarClientePorContrato(numeroContrato) {

    $(".notification.failure.hideit").trigger("click");
    if (numeroContrato != null && trim(numeroContrato) != "") {
        var url = "carregarClientePorContrato?numeroContrato=" + numeroContrato;
        carregarFragmento('divCliente', url);
        var chavePrimariaCliente = document.forms[0].chaveCliente.value;

        $("#divContrato").load("carregarContratoPorNumero?numeroContrato=" + numeroContrato,
            function () {
                var chavePrimariaContrato = document.forms[0].chaveContrato.value;
                $("#gridImoveis").load("carregarImovelPorContrato?chavePrimariaContrato=" + chavePrimariaContrato + "&numeroContrato=" + numeroContrato,
                    function () {
                        if ($("input[name='chaveImovel']").size() == 1) {

                            $("input[name='chaveImovel']").trigger("click");
                            verificarGarantia();
                        }
                    });
            }
        );


        $("#gridChamadosCliente").load("carregarChamadosClienteImovel?chavePrimariaCliente=" + chavePrimariaCliente);

        document.forms[0].cliente.value = chavePrimariaCliente;
    }
}

function detalharChamado(chave) {
    document.forms[0].chavePrimaria.value = chave;
    popup = window.open('exibirDetalhamentoChamado?&chavePrimaria=' + chave +'&popUp=yes', 'popup', 'height=750,width=1080,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');oltar
    
}

function selecionarPontoConsumo(chavePontoConsumo, descricao) {
    document.forms[0].pontoConsumo.value = chavePontoConsumo;
    carregarFragmento('divCliente', "carregarClientePorPontoConsumo?chavePontoConsumo=" + chavePontoConsumo);
    carregarFragmento('gridChamadosCliente', "carregarChamadosClienteImovel?chavePrimariaCliente=" + $("#cliente").val());
    carregarFragmento('divContrato', "carregarContratoPorPontoConsumo?chavePontoConsumo=" + chavePontoConsumo);
    carregarFragmento('gridChamadoTabelaPontoConsumo', "carregarPontosConsumoClienteImovel?chavePrimariaPontoConsumo=" + chavePontoConsumo);
    const abaTituloController = new window.AbaTitulosController();
    
	abaTituloController.pesquisarTitulo($("#cpfCnpj").val());
    
    
    let idImovel = $("#idImovel").val();
//    $("#endereco").hide(); 
    if(idImovel != null && idImovel != ""){
		if( confirm("Deseja replicar os dados do cliente para o solicitante?") ){
			$("#nomeSolicitante").val($("#nome").val());
			$("#cpfCnpjSolicitante").val($("#cpfCnpj").val());
			$("#emailSolicitante").val($("#email").val());
			$("#telefoneSolicitante").val($("#telefone").val());
			$("#rgSolicitante").val($("#rg").val());
		    
		}else{
			$("#nomeSolicitante").val("");
			$("#cpfCnpjSolicitante").val("");
			$("#emailSolicitante").val("");
			$("#telefoneSolicitante").val("");
			$("#rgSolicitante").val("");
		}
    }

}

function carregaroInformacaoPontoConsumo(chavePontoConsumo) {
    document.forms[0].idPontoConsumo.value = chavePontoConsumo;
}


function pesquisar() {
    var noCache = "noCache=" + new Date().getTime();
    var numeroImovel = document.forms[0].numeroImovel.value;
    var complemento = encodeURI(document.forms[0].complemento.value);
    var nome = encodeURI(document.forms[0].nomeImovel.value);
    var matricula = document.forms[0].matricula.value;
    var numeroCep = document.getElementById('cepImovel').value;
    $("#gridImoveis").load("consultarImovel?numeroImovel=" + numeroImovel +
        "&complemento=" + complemento +
        "&nome=" + nome +
        "&matricula=" + matricula +
        "&numeroCep=" + numeroCep,
        function () {
            limparContratoECliente();

            var chaveImovel = -1;
            limparPontoConsumo();

        });
}

function limparContratoECliente() {

    document.forms[0].chaveCliente.value = -1;
    document.forms[0].chaveContrato.value = -1;
    document.forms[0].contrato.value = "";
    document.forms[0].nome.value = "";
    document.forms[0].nomeFantasia.value = "";
    document.forms[0].cpfCnpj.value = "";
    document.forms[0].numeroPassaporte.value = "";
    document.forms[0].email.value = "";
    document.forms[0].telefone.value = "";

}

function limparPontoConsumo() {

    $('#chavePontoConsumo').val('');
    var fluxoExecucao = "fluxoInclusao";
    $("#gridChamadoPontosConsumo").load("limparPontosConsumo?&fluxoExecucao=" + fluxoExecucao);
}

function carregarPontosConsumo(chavePrimariaImovel) {
    $(".notification.failure.hideit").hide();
    $(".notification.mensagemAlerta.hideit").hide();

    var fluxoExecucao = "fluxoInclusao";
    var idContrato = '';
    limparContratoECliente();
    $("#gridChamadoPontosConsumo").load("carregarPontosConsumo?chavePrimariaImovel=" + chavePrimariaImovel + "&fluxoExecucao=" +
        fluxoExecucao + "&chavePrimariaContrato=" + idContrato,
        function () {

            if ($("input[name='checkPontoConsumo']").size() == 1) {
                $("input[name='checkPontoConsumo']").trigger("click");
            }
            else {
                $("input[name='checkPontoConsumo']").each(function () {
                        $(this).trigger("click");
                       return false;
                });
            }
            verificarErroAlerta();
            if(!$('#contrato').val()){
                $("[name='Cliente nao localizado.']").remove();
            }

        });


    document.forms[0].imovel.value = chavePrimariaImovel;
}

function verificarErroAlerta() {

    if ($("input[name='matricula']").each(function () {
        var verificarErroAlerta = '<c:out value="${chamado.imovel.chavePrimaria}" />';
        if (verificarErroAlerta != 0) {
            $(".mensagensSpring").show();

            $(".notification.failure.hideit").click(function () {
                $(this).fadeOut(700);
            });
            $(".notification.mensagemAlerta.hideit").click(function () {
                $(this).fadeOut(700);
            });
        }
    })) ;
}

function imprimirChamadoDetalhar() {

    if ($("#servicoAutorizacao").find("tr.odd").length > 0 || $("#servicoAutorizacao").find("tr.odd").length > 0) {
        $("#dialog-confirm").modal('show');
    } else {
        $("#comAS").val("false");
        submeter('chamadoForm', 'imprimirChamado');
    }
}

function exibirResponderQuestionario() {
    submeter('chamadoForm', 'exibirResponderQuestionario');
}

function exibirSolicitacaoAlteracaoDataVencimento() {
    submeter('chamadoForm', 'exibirSolicitacaoAlteracaoDataVencimento');
}

function exibirAlteracaoCadastroClienteChamado() {
    submeter('chamadoForm', 'exibirAlteracaoCadastroClienteChamado');
}

function salvarAgendar() {

    var chaveAssunto = document.forms[0].chamadoAssunto.value;
    var chaveImovel = document.forms[0].imovel.value;

    if ($("#fluxoTramitacao").val() == "true" && !validarUnidadesVisualizadoras()) {
        ocultarLoading('#botaoSalvarAgendar');
        return;
    }

    exibirDialogGerarAutorizacaoServico("salvarAgendar");

}

var windows = {};

function imprimirArquivo(chaveChamadoHistorico) {

    if (windows['popup'] != undefined && windows['popup'] != null) {
        windows['popup'].close();
    }
    var url = 'imprimirArquivoChamado';
    windows['popup'] = window.open(url + "?chaveChamadoHistorico=" + chaveChamadoHistorico, 'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function carregarResponsavelPorChamadoAssunto(chaveChamadoAssunto) {

    if (chaveChamadoAssunto != "") {
        var noCache = "noCache=" + new Date().getTime();
        $("#divResponsavelPorChamadoAssunto").load("carregarResponsavelPorChamadoAssunto?chaveChamadoAssunto=" + chaveChamadoAssunto + "&" + noCache);

    }
}

function carregarPrazoDiferenciado(chaveChamadoAssunto) {
    if (chaveChamadoAssunto != "") {
        var noCache = "noCache=" + new Date().getTime();
        $("#divPrazoDiferenciado").load("carregarPrazoDiferenciado?chaveChamadoAssunto=" + chaveChamadoAssunto + "&" + noCache,

            function () {
                if ($("#indicadorPrazoDiferenciado").val() == 'true') {

                    if ($("#fluxoDetalhamento").val() != "true") {
                        $('#dataPrevisaoEncerramento').removeAttr('disabled');
                    }
                    $("#dataPrevisaoEncerramento").inputmask("99/99/9999 99:99", {placeholder: "_", greedy: false});
                    $("#dataPrevisaoEncerramento").datetimepicker({
                        minDate: '+0d',
                        showOn: 'button',
                        buttonImage: $('#imagemCalendario').val(),
                        buttonImageOnly: true,
                        buttonText: 'Exibir Calend�rio',
                        dateFormat: 'dd/mm/yy',
                        timeFormat: 'HH:mm'
                    });

                } else {
                    $("#dataPrevisaoEncerramento").datetimepicker('destroy');
                    $('#dataPrevisaoEncerramento').val("");
                    $('#dataPrevisaoEncerramento').attr('disabled', 'disabled');
                }
            }
        );

    }
}

function carregarUnidadesVisualizadorasPadrao(chaveChamadoAssunto) {
	
	var fluxoInclusao = $('#fluxoInclusao').val();
    var fluxoTramitacao = $('#fluxoTramitacao').val();
    
    if(fluxoInclusao != '' || fluxoTramitacao != '') {
		
		if(fluxoTramitacao != '') {
		    AjaxService.carregarUnidadesVisualizadorasPadrao(chaveChamadoAssunto, {
		        callback: function (retorno) {
		            if (retorno['indicadorUnidadeVisualizadora'] == "true") {
		                $("#unidadeOrganizacionalVisualizadoras option").removeAttr("selected");
		                $("#unidadeOrganizacionalVisualizadoras option").each(function () {
		                    var option = $(this);
		                    var idsUnidadesVisualizadoras = retorno['idsUnidadesVisualizadoras'].split(",");
		                    if (idsUnidadesVisualizadoras.indexOf(option.val()) != -1) {
		                        option.attr("selected", "selected");
		                    }
		                });
		                $("#flagUnidadesVisualizadorasSim").attr("checked", "checked");
		                $("#divUnidadeOrganizacionalVisualizadoras").show();
		            } else {
		                $("#alterarUnidadesVisualizadoras").attr("checked", "checked");
		            	$("#flagUnidadesVisualizadorasNao").attr("checked", "checked");
		                $("#divUnidadeOrganizacionalVisualizadoras").hide();
		            }
		
		
		        },  async: true
		
		    });
		} else {
			AjaxService.carregarUnidadesVisualizadorasPadraoDescricao(chaveChamadoAssunto, {
		        callback: function (retorno) {
		            if (retorno['indicadorUnidadeVisualizadora'] == "true") {
						var descricaoUnidadesVisualizadoras = retorno['descricaoUnidadesVisualizadoras'];
						$('#descricaoUnidadesVisualizadoras').text('');
						$('#descricaoUnidadesVisualizadoras').text(descricaoUnidadesVisualizadoras);
		                $("#flagUnidadesVisualizadorasSim").attr("checked", "checked");
		                $("#divUnidadeOrganizacionalVisualizadoras").show();
		            } else {
		                $("#alterarUnidadesVisualizadoras").attr("checked", "checked");
		            	$("#flagUnidadesVisualizadorasNao").attr("checked", "checked");
		                $("#divUnidadeOrganizacionalVisualizadoras").hide();
		            }
		
		
		        },  async: true
		    });
		}
	}
	
}

function adicionarEmail() {

    var email = descricaoEmail.value;
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");

    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
    {

        alert("Nao � um endere�o de e-mail v�lido");
        $('#descricaoEmail').val('');
        return false;
    }

    if (descricaoEmail.value != null && descricaoEmail.value != " ") {
        var url = encodeURI("adicionarEmail?descricaoEmail=" + descricaoEmail.value);
        carregarFragmento('gridChamadoEmail', url);

        iniciarDatatable('#table-grid-email');

    }
    //$("#gridChamadoEmail").load("adicionarEmail?descricaoEmail="+descricaoEmail);
}

function limparElemento(element) {
    element.value = '';
}

function alterarChamadoEmail() {
    var indexListaChamadoEmail = document.forms[0].indexListaChamadoEmail.value;

    var url = encodeURI("alterarChamadoEmail?indexListaChamadoEmail=" + indexListaChamadoEmail + "&descricaoEmail=" + descricaoEmail.value);
    carregarFragmento('gridChamadoEmail', url);

    iniciarDatatable('#table-grid-email');

    $('.loading').hide();
    $('#table-grid-email').css({'opacity': 1, 'display': 'table'});
}

function desabilitarHabilitarBotton(botton, botton2) {

    /*document.getElementById(botton).disabled = false;
    document.getElementById(botton2).disabled = true;
    */
}

function mostraOculta(elemento) {

    if (elemento.style.display == 'none') {

        elemento.style.display = 'block';
    } else {
        elemento.style.display = 'none';
    }
}

function exibirPopupInclusaoCliente() {
    popup = window.open('popupExibirInclusaoCliente?', 'popup', 'height=750,width=1000,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

}

function exibirPopupInclusaoImovel() {
    popup = window.open('popupExibirInclusaoImovel?', 'popup', 'height=750,width=1000,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');

}

function mudarEstadoUnidadesVisualizadoras(radio) {
    if (radio.value == "true") {
        $("#unidadeOrganizacionalVisualizadoras option").attr('selected', false);
        $("#divUnidadeOrganizacionalVisualizadoras").show();
    } else {
        $("#divUnidadeOrganizacionalVisualizadoras").hide();
    }
}

function validarInformacoesGasista() {
    var retorno = true;

    if ($("input[name='indicadorAcionamento']").val() != undefined && !$("input[name='indicadorAcionamento']").is(":checked")) {
        alert("� preciso informar se houve necessidade de acionamento de gasista ou plantonista.")
        return false;
    }
    
    if ($("input[name='indicadorAcionamento']").val() != undefined && $("#manifestacao").val() == "") {
        alert("É preciso informar a Manifestação! ")
        return false;
    }
    
    if($("input[id='indicadorAcionamentoSim']").is(":checked") && !validarHorario($("#horaAcionamento").val())) {
		alert("Horário inválido!")
        return false;
	}

    if ($("#indicadorAcionamentoSim").val() != undefined && $("#indicadorAcionamentoSim").is(":checked")) {
        if (!$("#acionamentoPlantonista").is(":checked") && !$("#acionamentoGasista").is(":checked")) {
            alert("� preciso selecionar quem foi acionado (Gasista ou Plantonista).")
            retorno = false;
        }
        if ($("#informacaoAdicional").val().trim().length == 0) {
            alert("� preciso adicionar as informa��es do acionamento.");
            retorno = false;
        }
    }

    return retorno;
}

function validarUnidadesVisualizadoras() {
    var retorno = true;

    if ($("#flagUnidadesVisualizadorasSim").is(":checked")) {
        if ($("#unidadeOrganizacionalVisualizadoras :selected").size() == 0) {
            alert("Selecione as Unidades Organizacionais que podem visualizar o chamado.");
            retorno = false;
        }
    }

    return retorno;
}

function validarFormulario() {
    var valido = true;
    if ($('#chamadoTipo').val() === '') {
        $('#chamadoTipo').addClass('is-invalid');
        $('#chamadoTipo').focus();
        valido = false;
    }

    if ($('#chamadoAssunto').val() === '') {
        $('#chamadoAssunto').addClass('is-invalid');
        $('#chamadoAssunto').focus();
        valido = false;
    }

    return valido;
}

function submeterFormChamado(action) {
    submeter('chamadoForm', action);
}

/**
 *
 * @param id_dom
 * @returns true se o campo tiver sido preenchido
 */
function campoPreenchido(id_dom) {
    return $(id_dom).val() !== '' && $(id_dom).val() !== undefined && $(id_dom).val() !== null;
}

/**
 * Preencher os outros campos com o valor atual do assunto chamado
 * Verifica o assunto para habilitar o bot?o Mudan?a de Titularidade
 * @param valor_assunto_chamado
 */
function onChangeAssuntoChamado(valor_assunto_chamado) {
    carregarPrazo(valor_assunto_chamado);
    carregarUnidadeOrganizacional(valor_assunto_chamado);
    carregarResponsavelPorChamadoAssunto(valor_assunto_chamado);
    carregarPrazoDiferenciado(valor_assunto_chamado);
    carregarUnidadesVisualizadorasPadrao(valor_assunto_chamado);
    selecionaCanalAtendimento(valor_assunto_chamado);
    verificarAssunto();
}

function copiarObservacao() {

    try {

        var disabledProp = $("#orientacaoAssunto").attr('disabled');
        $("#orientacaoAssunto").attr('disabled', false);
        $("#orientacaoAssunto").select();
        document.execCommand("copy");
        $("#orientacaoAssunto").attr('disabled', disabledProp);
        /**
         * Estilo ? aplicado porque algum evento ? disparado e o estilo do componente ? modificado como se Nao estivesse desabilitado
         */
        $("#orientacaoAssunto").css({'background-color': '#e9ecef', 'border': '1px solid #ced4da'});

        if ($("#descricao").val().trim() === '') {
            $("#descricao").val($("#orientacaoAssunto").val());
        } else {
            $("#modal-colar-observacao").modal('show');
        }

    } catch (e) {
        console.error('Nao foi poss�vel copiar', e);
    }
}

function habilitarDesabilitarBotaoCopiarObservacao() {
    if ($("#fluxoDetalhamento").val() == "true" || $("#descricao").attr('disabled') !== undefined || $("#orientacaoAssunto").val().trim() === '') {
        $('#btn-copiar-observacao').attr('disabled', true);
    } else {
        $('#btn-copiar-observacao').attr('disabled', false);
    }
}

function confirmarColaObservacao() {
    $("#descricao").val($("#orientacaoAssunto").val());
    $("#modal-colar-observacao").modal('hide');
}

function exibirLoading(idBotaoOrigem) {
    $(idBotaoOrigem).attr("disabled", "disabled");
    exibirIndicador();
    $("#indicadorProcessamento").css("position", "fixed");
}


/**
 * Fun??o necess?ria caso a chamada anterior que exibiu o loading Nao recarregue a p?gina
 * @param idBotaoOrigem o id do bot?o que origina a chamada
 */
function ocultarLoading(idBotaoOrigem) {
    $(idBotaoOrigem).removeAttr("disabled");
    esconderIndicador();
}

$(document).keyup(function (e) {
    if (e.which == 27) {
        $("#botaoAlterar").removeAttr("disabled");
        $("#botaoIncluir").removeAttr("disabled");
        $("#botaosalvarAgendar").removeAttr("disabled");
        $("#botaoTramitar").css("background-color", "");
        $("#botaoTramitar").removeAttr("disabled");
        $("#botaoReabrir").removeAttr("disabled");
        $("#botaoReabrir").css("background-color", "");
        $("#botaoReiterar").removeAttr("disabled");
        $("#botaoReiterar").css("background-color", "");
        $("#botaoReativar").removeAttr("disabled");
        $("#botaoReativar").css("background-color", "");
        $("#botaoCapturar").removeAttr("disabled");
        $("#botaoCapturar").css("background-color", "");
        $("#botaoEncerrar").removeAttr("disabled");
        $("#botaoEncerrar").css("background-color", "");
        $("#botaoGerar").removeAttr("disabled");
    }
});

function imprimirChamado(comAs) {
    $("#dialog-confirm").modal('hide');
    $("#comAS").val(comAs);
    submeter('chamadoForm', 'imprimirChamado');
}
function imprimirComAs() {
    imprimirChamado('true');
}
function imprimirSemAs() {
    imprimirChamado('false');
}

function verificarAssunto(){

	var chamadoDescricao = trim($("#chamadoAssunto").find(":selected").text());

	if (chamadoDescricao == "MUDAN\u00C7A DE TITULARIDADE RESIDENCIAL"){
		document.getElementById('botaoMudancaTitularidade').disabled=false;  
	}else{
	    if(document.getElementById('botaoMudancaTitularidade')) {
            document.getElementById('botaoMudancaTitularidade').disabled = true;
        }
	}
}

function exibirPopupMudancaTitularidade() {

	 var chamadoTela = document.forms[0].chamadoTela.value;
	 var idContrato = document.forms[0].chaveContrato.value;

	 AjaxService.verificarPontoConsumoAtivo(idContrato,
				function(pontoAtivo){
					var pontoConsumoAtivo = pontoAtivo;

					if (pontoConsumoAtivo == true){
							window.open('exibirPopupPontoConsumoContrato?&chavePrimaria='+idContrato+'&chamadoTela='+chamadoTela, 'window','height=750,width=1200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
						}else{

							if(confirm("Nao existe ponto de consumo ativo para o contrato selecionado. Deseja encerrar o contrato?")) {
								window.open('exibirPopupPontoConsumoContrato?&chavePrimaria='+idContrato+'&chamadoTela='+chamadoTela, 'window','height=750,width=1200,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
							}
					}

				}
			 );

}


function verificarGarantiaServicos(id, callback) {
    verificarGarantia(null, id, callback);
}
function verificarGarantia(chavePrimariaCliente, idBotao, callback) {

    var chaveCliente;
    if (chavePrimariaCliente == null) {
        chaveCliente = document.forms[0].chaveCliente.value;
    } else {
        chaveCliente = chavePrimariaCliente;
    }

    var chamadoAssunto = $("#chamadoAssunto :selected").val();
    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    function executarCallback(){
        if (callback != null)
            callback();
    }

    if (chaveCliente != '' && chaveCliente > 0 && chamadoAssunto != null && chamadoAssunto > 0 && chavePrimariaImovel != '' && chavePrimariaImovel > 0) {
        AjaxService.garantiaVigente(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
            callback: function (liberado) {


                $('#footer-disponivel').hide();
                $('#footer-indisponivel').hide();
                $('#notificacao-servico-indisponivel').hide();
                $('#notificacao-servico-disponivel').hide();

                $('#titulo-servicos-garantia-indisponivel').hide();
                $('#titulo-servicos-garantia-disponivel').hide();

                ocultarLoading(idBotao);
                if (liberado) {
                    $('#titulo-servicos-garantia-disponivel').show();
                    $('#footer-disponivel').show();
                    $('#notificacao-servico-disponivel').show();

                    if (callback != null) {
                        $('#button-salvar-servico-disponivel').one("click", function (event) {
                            if (idBotao != undefined) {
                                exibirLoading(idBotao);
                                executarCallback();
                            }
                        });
                        if (idBotao != undefined) {
                            servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel);
                        }
                    }

                } else {
                    $('#titulo-servicos-garantia-indisponivel').show();
                    $('#footer-indisponivel').show();
                    $('#notificacao-servico-indisponivel').show();
                    servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel);
                }

            },
            async: true
        });
    } else {
        executarCallback();
    }
}

function enviarEmail(protocolo, emailCliente, chamadoAssunto, dataInicioAbertura, dataPrevisaoEncerramento, status, nomeCliente){
	if (emailCliente != ""){
		AjaxService.enviarEmailChamado(protocolo, emailCliente, chamadoAssunto, dataInicioAbertura, dataPrevisaoEncerramento, status, nomeCliente,
			function(emailEnviado){					
				if (emailEnviado == true){
						alert("E-mail enviado com sucesso!");
					}else{
						alert("Erro ao enviar o e-mail, procure o TI!");
						}
				}
		 );
	}else{
		alert('Cliente sem e-mail cadastrado!');
	}
		
}

function servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel) {
    AjaxService.servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
        callback: function (tipos_servicos) {
            $("#corpoGridServicoTipoGarantia").html("");
            $('#corpoGridServicoTipoGarantia').empty();

            function getIniciarTR() {
                return '<tr style="border-bottom: 1px solid #dbdddc;">';
            }

            function __getIlimitado(){
                return '<td>Ilimitado</td>';
            }

            function getPrazoGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    return '<td>' + tipos_servicos[i].prazoGarantia + '</td>';

                } else {
                    return __getIlimitado();
                }
            }

            function getNaGarantia() {
                if (tipos_servicos[i].naGarantia != null) {
                    if (tipos_servicos[i].naGarantia) {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" />' + ' Na garantia</td>';
                    } else {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'cancel16.png" />' + ' <span style="color: red">Garantia expirada</span></td>';
                    }
                } else {
                    return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" /> Ilimitado</td>';
                }
            }

            function getDescricao() {
                return '<td>' + tipos_servicos[i].descricao + '</td>';
            }
            function getNumeroMaximoExecucoes() {
                if (tipos_servicos[i].numeroMaximoExecucoes != null) {
                    return '<td>' + tipos_servicos[i].numeroMaximoExecucoes + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getNumeroExecucoes() {
                if (tipos_servicos[i].numeroExecucoesDisponiveis != null) {
                    return '<td>' + tipos_servicos[i].numeroExecucoesDisponiveis + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getDataLimiteGarantia() {
                if (tipos_servicos[i].dataLimiteGarantia != null || tipos_servicos[i].prazoGarantia != null) {
                    if (tipos_servicos[i].dataLimiteGarantia == null) {
                        return '<td>Nao iniciado</td>';
                    } else {
                        return '<td>' + tipos_servicos[i].dataLimiteGarantia + '</td>';
                    }
                } else {
                    return __getIlimitado();
                }
            }

            function getTerminarTR() {
                return '</tr>';
            }

            if (tipos_servicos.length > 0) {
                var imagem = $('#url_imagens').val();
                var styleImage = 'style="margin-bottom: -4px;"';
                for (var i in tipos_servicos) {
                    var inner = getIniciarTR();

                    inner += getNaGarantia();
                    inner += getDescricao();
                    inner += getNumeroMaximoExecucoes();
                    inner += getNumeroExecucoes();
                    inner += getDataLimiteGarantia();
                    inner += getPrazoGarantia();
                    inner += getTerminarTR();

                    $("#corpoGridServicoTipoGarantia").append(inner);
                }
                $("#dialogGarantiaServico").modal('show');
            }
        },
        async: true
    });
}

function verificarGarantiaServicos(id, callback) {
    verificarGarantia(null, id, callback);
}
function verificarGarantia(chavePrimariaCliente, idBotao, callback) {

    var chaveCliente;
    if (chavePrimariaCliente == null) {
        chaveCliente = document.forms[0].chaveCliente.value;
    } else {
        chaveCliente = chavePrimariaCliente;
    }

    var chamadoAssunto = $("#chamadoAssunto :selected").val();
    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    function executarCallback(){
        if (callback != null)
            callback();
    }

    if (chaveCliente != '' && chaveCliente > 0 && chamadoAssunto != null && chamadoAssunto > 0 && chavePrimariaImovel != '' && chavePrimariaImovel > 0) {
        AjaxService.garantiaVigente(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
            callback: function (liberado) {


                $('#footer-disponivel').hide();
                $('#footer-indisponivel').hide();
                $('#notificacao-servico-indisponivel').hide();
                $('#notificacao-servico-disponivel').hide();

                $('#titulo-servicos-garantia-indisponivel').hide();
                $('#titulo-servicos-garantia-disponivel').hide();

                ocultarLoading(idBotao);
                if (liberado) {
                    $('#titulo-servicos-garantia-disponivel').show();
                    $('#footer-disponivel').show();
                    $('#notificacao-servico-disponivel').show();

                    if (callback != null) {
                        $('#button-salvar-servico-disponivel').one("click", function (event) {
                            if (idBotao != undefined) {
                                exibirLoading(idBotao);
                                executarCallback();
                            }
                        });
                        if (idBotao != undefined) {
                            servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                        }
                    }

                } else {
                    $('#titulo-servicos-garantia-indisponivel').show();
                    $('#footer-indisponivel').show();
                    $('#notificacao-servico-indisponivel').show();
                    servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                }

            },
            async: true
        });
    } else {
        executarCallback();
    }
}

function servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback) {
    AjaxService.servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
        callback: function (tipos_servicos) {
            $('#corpoGridServicoTipoGarantia').empty();

            function getIniciarTR() {
                return '<tr style="border-bottom: 1px solid #dbdddc;">';
            }

            function __getIlimitado(){
                return '<td>Ilimitado</td>';
            }

            function getPrazoGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    return '<td>' + tipos_servicos[i].prazoGarantia + '</td>';

                } else {
                    return __getIlimitado();
                }
            }

            function getNaGarantia() {
                if (tipos_servicos[i].naGarantia != null) {
                    if (tipos_servicos[i].naGarantia) {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" />' + ' Dispon�vel</td>';
                    } else {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'cancel16.png" />' + ' <span style="color: red">Indispon�vel</span></td>';
                    }
                } else {
                    return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" /> Ilimitado</td>';
                }
            }

            function getDescricao() {
                return '<td>' + tipos_servicos[i].descricao + '</td>';
            }
            function getNumeroMaximoExecucoes() {
                if (tipos_servicos[i].numeroMaximoExecucoes != null) {
                    return '<td>' + tipos_servicos[i].numeroMaximoExecucoes + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getNumeroExecucoes() {
                if (tipos_servicos[i].numeroExecucoesDisponiveis != null) {
                    return '<td>' + tipos_servicos[i].numeroExecucoesDisponiveis + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getDataLimiteGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    if (tipos_servicos[i].dataLimiteGarantia == null) {
                        return '<td>Nao iniciado</td>';
                    } else {
                        return '<td>' + tipos_servicos[i].dataLimiteGarantia + '</td>';
                    }
                } else {
                    return __getIlimitado();
                }
            }

            function getDataExecucacao() {
                if (tipos_servicos[i].dataExecucacao != null) {
                    return '<td>' + tipos_servicos[i].dataExecucacao + '</td>';
                } else {
                    return '<td>Nao executado</td>';
                }
            }

            function getTerminarTR() {
                return '</tr>';
            }

            if (tipos_servicos.length > 0) {
                var imagem = $('#url_imagens').val();
                var styleImage = 'style="margin-bottom: 2px;"';
                for (var i in tipos_servicos) {
                    var inner = getIniciarTR();

                    inner += getNaGarantia();
                    inner += getDescricao();
                    inner += getNumeroMaximoExecucoes();
                    inner += getNumeroExecucoes();
                    inner += getDataExecucacao();
                    inner += getPrazoGarantia();
                    inner += getDataLimiteGarantia();
                    inner += getTerminarTR();

                    $("#corpoGridServicoTipoGarantia").append(inner);
                }
                $("#dialogGarantiaServico").modal('show');
            } else if (callback != null){
                callback();
            }
        },
        async: true
    });
}

function verificarGarantiaServicos(id, callback) {
    verificarGarantia(null, id, callback);
}
function verificarGarantia(chavePrimariaCliente, idBotao, callback) {

    var chaveCliente;
    if (chavePrimariaCliente == null) {
        chaveCliente = document.forms[0].chaveCliente.value;
    } else {
        chaveCliente = chavePrimariaCliente;
    }

    var chamadoAssunto = $("#chamadoAssunto :selected").val();
    var chavePrimariaImovel = $("input[name=chaveImovel]:checked").val();

    if (chavePrimariaImovel == null) {
        chavePrimariaImovel = $("#chaveImovel").val();
    }

    function executarCallback(){
        if (callback != null)
            callback();
    }

    if (chaveCliente != '' && chaveCliente > 0 && chamadoAssunto != null && chamadoAssunto > 0 && chavePrimariaImovel != '' && chavePrimariaImovel > 0) {
        AjaxService.garantiaVigente(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
            callback: function (liberado) {


                $('#footer-disponivel').hide();
                $('#footer-indisponivel').hide();
                $('#notificacao-servico-indisponivel').hide();
                $('#notificacao-servico-disponivel').hide();

                $('#titulo-servicos-garantia-indisponivel').hide();
                $('#titulo-servicos-garantia-disponivel').hide();

                ocultarLoading(idBotao);
                if (liberado) {
                    $('#titulo-servicos-garantia-disponivel').show();
                    $('#footer-disponivel').show();
                    $('#notificacao-servico-disponivel').show();

                    if (callback != null) {
                        $('#button-salvar-servico-disponivel').one("click", function (event) {
                            if (idBotao != undefined) {
                                exibirLoading(idBotao);
                                executarCallback();
                            }
                        });
                        if (idBotao != undefined) {
                            servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                        }
                    }

                } else {
                    $('#titulo-servicos-garantia-indisponivel').show();
                    $('#footer-indisponivel').show();
                    $('#notificacao-servico-indisponivel').show();
                    servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback);
                }

            },
            async: true
        });
    } else {
        executarCallback();
    }
}

function servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, callback) {
    AjaxService.servicosChamadoAssuntoComPrazos(chaveCliente, chamadoAssunto, chavePrimariaImovel, {
        callback: function (tipos_servicos) {
            $('#corpoGridServicoTipoGarantia').empty();
            function getIniciarTR() {
                return '<tr style="border-bottom: 1px solid #dbdddc;">';
            }

            function __getIlimitado(){
                return '<td>Ilimitado</td>';
            }

            function getPrazoGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    return '<td>' + tipos_servicos[i].prazoGarantia + '</td>';

                } else {
                    return __getIlimitado();
                }
            }

            function getNaGarantia() {
                if (tipos_servicos[i].naGarantia != null) {
                    if (tipos_servicos[i].naGarantia) {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" />' + ' Dispon�vel</td>';
                    } else {
                        return '<td><img ' + styleImage + ' src="' + imagem + 'cancel16.png" />' + ' <span style="color: red">Indispon�vel</span></td>';
                    }
                } else {
                    return '<td><img ' + styleImage + ' src="' + imagem + 'success_icon.png" /> Ilimitado</td>';
                }
            }

            function getDescricao() {
                return '<td>' + tipos_servicos[i].descricao + '</td>';
            }
            function getNumeroMaximoExecucoes() {
                if (tipos_servicos[i].numeroMaximoExecucoes != null) {
                    return '<td>' + tipos_servicos[i].numeroMaximoExecucoes + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getNumeroExecucoes() {
                if (tipos_servicos[i].numeroExecucoesDisponiveis != null) {
                    return '<td>' + tipos_servicos[i].numeroExecucoesDisponiveis + '</td>';
                } else {
                    return __getIlimitado();
                }
            }

            function getDataLimiteGarantia() {
                if (tipos_servicos[i].prazoGarantia != null) {
                    if (tipos_servicos[i].dataLimiteGarantia == null) {
                        return '<td>Nao iniciado</td>';
                    } else {
                        return '<td>' + tipos_servicos[i].dataLimiteGarantia + '</td>';
                    }
                } else {
                    return __getIlimitado();
                }
            }

            function getDataExecucacao() {
                if (tipos_servicos[i].dataExecucacao != null) {
                    return '<td>' + tipos_servicos[i].dataExecucacao + '</td>';
                } else {
                    return '<td>Nao executado</td>';
                }
            }

            function getTerminarTR() {
                return '</tr>';
            }

            if (tipos_servicos.length > 0) {
                var imagem = $('#url_imagens').val();
                var styleImage = 'style="margin-bottom: 2px;"';
                for (var i in tipos_servicos) {
                    var inner = getIniciarTR();

                    inner += getNaGarantia();
                    inner += getDescricao();
                    inner += getNumeroMaximoExecucoes();
                    inner += getNumeroExecucoes();
                    inner += getDataExecucacao();
                    inner += getPrazoGarantia();
                    inner += getDataLimiteGarantia();
                    inner += getTerminarTR();

                    $("#corpoGridServicoTipoGarantia").append(inner);
                }
                $("#dialogGarantiaServico").modal('show');
            } else if (callback != null){
                callback();
            }
        },
        async: true
    });
}


function enviarEmailHistoricoChamado(idChamado){
	AjaxService.enviarEmailHistoricoChamado(idChamado,
		function(emailEnviado){					
			if (emailEnviado == true){
				alert("E-mail enviado com sucesso!");
			}else{
				alert("Erro ao enviar o e-mail, procure o setor de TI!");
			}
		}
	 );
}

function mascaraCampoCpfCnpj(){	
    var options = {
    	    onKeyPress: function (cpf, ev, el, op) {
    	        var masks = ['000.000.000-000', '00.000.000/0000-00'];
    	        $('.cpfOuCnpj').mask((cpf.length > 14) ? masks[1] : masks[0], op);
    	    }
    	}
    	$('.cpfOuCnpj').length > 14 ? $('.cpfOuCnpj').mask('00.000.000/0000-00', options) : $('.cpfOuCnpj').mask('000.000.000-00#', options);	
}

function mascaraCampoTelefone(){
	jQuery("input.telefone")
	.focusout(function (event) {  
	    var target, phone, element;  
	    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
	    phone = target.value.replace(/\D/g, '');
	    element = $(target);  
	    element.unmask();  

	    if(phone.length == 11) {  
	        element.mask("(99) 99999-9999");  
	    } else {  
	        element.mask("(99) 9999-99999");  
	    }  
	});
}

function mascaraCampoRg(valor){
	if(valor.length == 10) {  
		$('.rg').mask("AA.AAA.AAA-AA");  
	} else {  
		$('.rg').mask("AA.AAA.AAA-AAA");  
	}  
}

function carregarTurnosEquipe() {

	let chaveEquipe = $("#listaEquipe").val();

	var selectTurnoPreferencia = document.getElementById("turnoPreferencia");
	var idTurnoPreferencia = $("#idTurnoPreferencia").val();

	selectTurnoPreferencia.length = 0;

	var novaOpcao = new Option("Selecione", "-1");
	selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;


	AjaxService.listaTurnoPorEquipe(chaveEquipe , {
		callback: function(listaTurnos) {

			for (key in listaTurnos) {

				var novaOpcao = new Option(listaTurnos[key], key);
				if (key == idTurnoPreferencia) {
					novaOpcao.selected = true;
				}
				selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;
			}

		}
	});
}

function carregarChaveEquipePrioritaria(chavePrimaria) {
	AjaxService.carregarChaveEquipePrioritaria(chavePrimaria, {
        callback: function (chaveEquipe) {
            $("#listaEquipe").val(chaveEquipe);
        },
        async: false
    });
}


function verificarServicoTipoMesmaOperacao() {
	let idChamadoAssunto = $("#chamadoAssunto").val();
	let idPontoConsumo = $("#pontoConsumo").val();
	let confirmacao = true;
	
	if(idChamadoAssunto == '-1' || idPontoConsumo == '') {
		return true;
	}
	
	AjaxService.consultarAssuntoChamadoMesmaOperacaoTipoServico(idChamadoAssunto, idPontoConsumo, {
		callback: function(retorno) {
			confirmacao = retorno;
		}, async: false
	});
	
	if(confirmacao) {
		confirmacao = confirm('Há uma AS com outro serviço para a mesma operação no medidor deseja prosseguir?');
	} else {
		return null;
	}

    return confirmacao;
}


