$(document).ready(function(){
    var botaoAlterarAssunto = document.getElementById("botaoAlterarAssunto");
    // botaoAlterarAssunto.disabled = true;

    obterListaServicos();

    init();

    var maxLength = 30;
    $('#servicoTipo > option').text(function(i, text) {
        text = text.trim();
        if (text.length > maxLength) {
            return text.substr(0, maxLength) + '...';
        }
    });

    bloquearOperacoes();
    validarServicos();
    carregarIndicadorRegulatorio();
});


function desabilitarCredito() {
    document.getElementById("rubricaCredito").disabled=true;
    document.getElementById("rubricaCredito").value=-1;
    $('#spanRubricaCredito').hide();
}

function desabilitarDebito() {
    document.getElementById("rubricaDebito").disabled=true;
    document.getElementById("rubricaDebito").value=-1;
    $('#spanRubricaDebito').hide();
}

function habilitarCredito() {
    document.getElementById("rubricaCredito").disabled=false;
    $('#spanRubricaCredito').show();
}

function habilitarDebito() {
    document.getElementById("rubricaDebito").disabled=false;
    $('#spanRubricaDebito').show();
}

function limparCaixaAssuntos(){

    desabilitarCredito();
    desabilitarDebito();

    document.forms["chamadoAssuntoForm"].descricaoAssunto.value="";
    document.forms["chamadoAssuntoForm"].orientacaoAssunto.value="";
    document.forms["chamadoAssuntoForm"].quantidadeHorasPrevistaAtendimento.value="";
    document.forms["chamadoAssuntoForm"].indicadorDiaCorridoUtil[1].checked=true;
    document.forms["chamadoAssuntoForm"].unidadeOrganizacional.value=-1;
    document.forms["chamadoAssuntoForm"].canalAtendimento.value=-1;
    document.forms["chamadoAssuntoForm"].questionario.value=-1;
    document.forms["chamadoAssuntoForm"].usuarioResponsavel.value=-1;
    document.forms["chamadoAssuntoForm"].usuarioResponsavel.disabled=true;
    document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorRgSolicitanteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorCpfCnpjSolicitanteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorTramitacao[1].checked=true;
    document.forms["chamadoAssuntoForm"].servicoTipo.value=-1;
    document.forms["chamadoAssuntoForm"].indicadorNomeSolicitanteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorDebito[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorCredito[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorTelefoneSolicitanteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorEmailSolicitanteObrigatorio[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorGeraServicoAutorizacao[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorFechamentoAutomatico[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorPrazoDiferenciado[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorUnidadesOrganizacionalVisualizadora[1].checked=true;
    $("[name='enviarEmailCadastro'][value='RESPONSAVEL']").prop("checked", true);
    $("[name='enviarEmailCadastro'][value='UNIDADES_VISUALIZADORAS']").prop("checked", true);
    $("[name='enviarEmailEncerrar'][value='RESPONSAVEL']").prop("checked", true);
    $("[name='enviarEmailEncerrar'][value='UNIDADES_VISUALIZADORAS']").prop("checked", true);
    $("[name='enviarEmailReiterar'][value='RESPONSAVEL']").prop("checked", true);
    $("[name='enviarEmailReiterar'][value='UNIDADES_VISUALIZADORAS']").prop("checked", true);
    $("[name='enviarEmailTramitar'][value='RESPONSAVEL']").prop("checked", true);
    $("[name='enviarEmailTramitar'][value='UNIDADES_VISUALIZADORAS']").prop("checked", true);
    $("[name='enviarEmailPrioridadeProtocolo'][value='AMBOS']").prop("checked",true);
    $("[name='enviarEmailPrioridadeProtocolo'][value='CLIENTE']").prop("checked",true);
    $("[name='enviarEmailPrioridadeProtocolo'][value='SOLICITANTE']").prop("checked",true);
    document.forms["chamadoAssuntoForm"].indicadorVerificaRestServ[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorEncerraAutoASRel[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[1].checked=true;
    document.forms["chamadoAssuntoForm"].indicadorAcionamentoGasista[1].checked=true;
    document.forms["chamadoAssuntoForm"].permiteAbrirEmLote[1].checked=true;
    document.forms["chamadoAssuntoForm"].nomeRelatorioRegulatorio.value="";
    bloquearOperacoes();

    $("#unidadeOrganizacionalVisualizadoras :selected").removeAttr("selected");
    $("#divUnidadeOrganizacionalVisualizadoras").hide();

    limparSessaoServicos();
}

function limparFormulario(){
    document.forms["chamadoAssuntoForm"].descricao.value = "";
    limparCaixaAssuntos();
}

function voltar() {
    submeter('chamadoAssuntoForm','exibirPesquisaChamadoAssunto');
}

function incluir() {
    location.href = 'inserirServicoTipoPrioridade';
}

function salvar(){
    document.forms["chamadoAssuntoForm"].descricao.value;
    submeter('chamadoAssuntoForm','inserirChamadoAssunto');
}

function adicionarServico(){

    var noCache = "noCache=" + new Date().getTime();
    var chave = document.forms["chamadoAssuntoForm"].servicoTipo.value;
    var url = "adicionarServico?servicoTipo="+chave+"&"+noCache;
    if(chave != "-1"){
        carregarFragmento('gridComponentesServicoTipo',url)
    }else{
        alert("Escolha um servi�o para adicionar.");
    }

    document.getElementById("servicoTipo").value=-1;
}


function removerServicoTipo(indexServicoTipo){
    var noCache = "noCache=" + new Date().getTime();
    var url = "removerServico?indexServicoTipo="+indexServicoTipo+"&"+noCache;
    carregarFragmento('gridComponentesServicoTipo',url);
}

function alterarOrdemListaServicos(indexServicoTipo, acao){
    var noCache = "noCache=" + new Date().getTime();
    var url = "alterarOrdemListaServicos?indexServicoTipo="+indexServicoTipo+"&"+"acao="+acao+"&"+noCache;
    carregarFragmento('gridComponentesServicoTipo',url);
}

function validarServicos() {
    if ($('#servicoTipo').length > 1) {
        document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[0].disabled = true;
        document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[1].disabled = true;
        document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[0].disabled = true;
        document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[1].disabled = true;
    } else {
        document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[0].disabled = false;
        document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[1].disabled = false;
        document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[0].disabled = false;
        document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[1].disabled = false;
    }
}

function carregarAssunto(acao){
    var indexList = -1;

    if(acao != "incluir"){
        indexList = document.forms[0].indexList.value
    }

    var chavePrimaria = document.forms[0].chavePrimaria.value;
    var noCache = "noCache=" + new Date().getTime();
    var descricaoAssunto = document.forms["chamadoAssuntoForm"].descricaoAssunto.value;
    var quantidadeHorasPrevistaAtendimento = document.forms["chamadoAssuntoForm"].quantidadeHorasPrevistaAtendimento.value;
    var rubricaCredito = document.forms["chamadoAssuntoForm"].rubricaCredito.value;
    var rubricaDebito = document.forms["chamadoAssuntoForm"].rubricaDebito.value;
    var questionario = document.forms["chamadoAssuntoForm"].questionario.value;
    var unidadeOrganizacional = document.forms["chamadoAssuntoForm"].unidadeOrganizacional.value;
    var canalAtendimento = document.forms["chamadoAssuntoForm"].canalAtendimento.value;
    var usuarioResponsavel = document.forms["chamadoAssuntoForm"].usuarioResponsavel.value;
    var orientacao = document.forms["chamadoAssuntoForm"].orientacaoAssunto.value;
    var nomeRelatorioRegulatorio = document.forms["chamadoAssuntoForm"].nomeRelatorioRegulatorio.value;
    var enviarEmailCadastro = $("[name='enviarEmailCadastro']:checked").toArray().map(function(el) {
        return el.value;
    });
    var enviarEmailTramitar = $("[name='enviarEmailTramitar']:checked").toArray().map(function(el) {
        return el.value;
    });
    var enviarEmailReiterar = $("[name='enviarEmailReiterar']:checked").toArray().map(function(el) {
        return el.value;
    });
    var enviarEmailEncerrar = $("[name='enviarEmailEncerrar']:checked").toArray().map(function(el) {
        return el.value;
    });
    var enviarEmailPrioridadeProtocolo = $("[name='enviarEmailPrioridadeProtocolo']:checked").toArray().map(function(el) {
        return el.value;
    });

    var indicadorCredito = "";
    if(document.forms["chamadoAssuntoForm"].indicadorCredito[0].checked==true){
        indicadorCredito = document.forms["chamadoAssuntoForm"].indicadorCredito[0].value;
    }else if (document.forms["chamadoAssuntoForm"].indicadorCredito[1].checked==true){
        indicadorCredito = document.forms["chamadoAssuntoForm"].indicadorCredito[1].value;
    }

    var indicadorDebito = "";

    if(document.forms["chamadoAssuntoForm"].indicadorDebito[0].checked==true){
        indicadorDebito = document.forms["chamadoAssuntoForm"].indicadorDebito[0].value;
    }else if (document.forms["chamadoAssuntoForm"].indicadorDebito[1].checked==true){
        indicadorDebito = document.forms["chamadoAssuntoForm"].indicadorDebito[1].value;
    }

    var indicadorClienteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[0].checked==true){
        indicadorClienteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[0].value;
    }else if (document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[1].checked==true){
        indicadorClienteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorClienteObrigatorio[1].value;
    }

    var indicadorImovelObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[0].checked==true){
        indicadorImovelObrigatorio = document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[1].checked==true){
        indicadorImovelObrigatorio = document.forms["chamadoAssuntoForm"].indicadorImovelObrigatorio[1].value;
    }

    var indicadorRgSolicitanteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorRgSolicitanteObrigatorio[0].checked==true){
        indicadorRgSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorRgSolicitanteObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorRgSolicitanteObrigatorio[1].checked==true){
        indicadorRgSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorRgSolicitanteObrigatorio[1].value;
    }

    var indicadorCpfCnpjSolicitanteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorCpfCnpjSolicitanteObrigatorio[0].checked==true){
        indicadorCpfCnpjSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorCpfCnpjSolicitanteObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorCpfCnpjSolicitanteObrigatorio[1].checked==true){
        indicadorCpfCnpjSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorCpfCnpjSolicitanteObrigatorio[1].value;
    }

    var indicadorUOVisualizadora = "false";
    if ($("#indicadorUnidadesOrganizacionalVisualizadoraSim").is(":checked")){
        indicadorUOVisualizadora = "true";
    }

    var indicadorTramitacao = "";
    if(document.forms["chamadoAssuntoForm"].indicadorTramitacao[0].checked==true){
        indicadorTramitacao = document.forms["chamadoAssuntoForm"].indicadorTramitacao[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorTramitacao[1].checked==true){
        indicadorTramitacao = document.forms["chamadoAssuntoForm"].indicadorTramitacao[1].value;
    }

    var indicadorNomeSolicitanteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorNomeSolicitanteObrigatorio[0].checked==true){
        indicadorNomeSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorNomeSolicitanteObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorNomeSolicitanteObrigatorio[1].checked==true){
        indicadorNomeSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorNomeSolicitanteObrigatorio[1].value;
    }


    var indicadorDiaCorridoUtil = "";

    if(document.forms["chamadoAssuntoForm"].indicadorDiaCorridoUtil[0].checked==true){
        indicadorDiaCorridoUtil = document.forms["chamadoAssuntoForm"].indicadorDiaCorridoUtil[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorDiaCorridoUtil[1].checked==true){
        indicadorDiaCorridoUtil = document.forms["chamadoAssuntoForm"].indicadorDiaCorridoUtil[1].value;
    }

    var indicadorTelefoneSolicitanteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorTelefoneSolicitanteObrigatorio[0].checked==true){
        indicadorTelefoneSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorTelefoneSolicitanteObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorTelefoneSolicitanteObrigatorio[1].checked==true){
        indicadorTelefoneSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorTelefoneSolicitanteObrigatorio[1].value;
    }

    var indicadorEmailSolicitanteObrigatorio = "";

    if(document.forms["chamadoAssuntoForm"].indicadorEmailSolicitanteObrigatorio[0].checked==true){
        indicadorEmailSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorEmailSolicitanteObrigatorio[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorEmailSolicitanteObrigatorio[1].checked==true){
        indicadorEmailSolicitanteObrigatorio = document.forms["chamadoAssuntoForm"].indicadorEmailSolicitanteObrigatorio[1].value;
    }

    var indicadorGeraServicoAutorizacao = "";

    if(document.forms["chamadoAssuntoForm"].indicadorGeraServicoAutorizacao[0].checked==true){
        indicadorGeraServicoAutorizacao = document.forms["chamadoAssuntoForm"].indicadorGeraServicoAutorizacao[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorGeraServicoAutorizacao[1].checked==true){
        indicadorGeraServicoAutorizacao = document.forms["chamadoAssuntoForm"].indicadorGeraServicoAutorizacao[1].value;
    }

    var indicadorFechamentoAutomatico = "";

    if(document.forms["chamadoAssuntoForm"].indicadorFechamentoAutomatico[0].checked==true){
        indicadorFechamentoAutomatico = document.forms["chamadoAssuntoForm"].indicadorFechamentoAutomatico[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorFechamentoAutomatico[1].checked==true){
        indicadorFechamentoAutomatico = document.forms["chamadoAssuntoForm"].indicadorFechamentoAutomatico[1].value;
    }

    var indicadorPrazoDiferenciado = "";

    if(document.forms["chamadoAssuntoForm"].indicadorPrazoDiferenciado[0].checked==true){
        indicadorPrazoDiferenciado = document.forms["chamadoAssuntoForm"].indicadorPrazoDiferenciado[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorPrazoDiferenciado[1].checked==true){
        indicadorPrazoDiferenciado = document.forms["chamadoAssuntoForm"].indicadorPrazoDiferenciado[1].value;
    }

    var indicadorVerificaRestServ = "";

    if(document.forms["chamadoAssuntoForm"].indicadorVerificaRestServ[0].checked==true){
        indicadorVerificaRestServ = document.forms["chamadoAssuntoForm"].indicadorVerificaRestServ[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorVerificaRestServ[1].checked==true){
        indicadorVerificaRestServ = document.forms["chamadoAssuntoForm"].indicadorVerificaRestServ[1].value;
    }

    var indicadorEncerraAutoASRel = "";

    if(document.forms["chamadoAssuntoForm"].indicadorEncerraAutoASRel[0].checked==true){
        indicadorEncerraAutoASRel = document.forms["chamadoAssuntoForm"].indicadorEncerraAutoASRel[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorEncerraAutoASRel[1].checked==true){
        indicadorEncerraAutoASRel = document.forms["chamadoAssuntoForm"].indicadorEncerraAutoASRel[1].value;
    }

    var indicadorVerificarVazamento = "";

    if(document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[0].checked==true){
        indicadorVerificarVazamento = document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[0].value;
    }else if(document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[1].checked==true){
        indicadorVerificarVazamento = document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[1].value;
    }

    var uoVisualizadoras = new Array();
    if($("#unidadeOrganizacionalVisualizadoras :selected").size() > 0 && $("#indicadorUnidadesOrganizacionalVisualizadoraSim").is(":checked")){
        $("#unidadeOrganizacionalVisualizadoras :selected").each(function(){
            uoVisualizadoras.push($(this).val());
        });
    }
    var indicadorAcionamentoGasista = "false";
    if ($("#indicadorAcionamentoGasistaSim").is(":checked")){
        indicadorAcionamentoGasista = "true";
    }

    var permiteAbrirEmLote = "false";
    if ($("#permiteAbrirEmLoteSim").is(":checked")){
        permiteAbrirEmLote = "true";
    }

    var indicadorComGarantia = "false";
    if ($("#indicadorComGarantiaSim").is(":checked")){
        indicadorComGarantia = "true";
    }
    var indicadorRegulatorio = null;
    if($("#indicadorRegulatorioValores").val()!= ""){
    	indicadorRegulatorio = $("#indicadorRegulatorioValores").val();
    }
    
    $.post( "adicionarAssunto", {
        "descricaoAssunto" : descricaoAssunto,
        "quantidadeHorasPrevistaAtendimento" : quantidadeHorasPrevistaAtendimento,
        "indicadorCredito" : indicadorCredito,
        "indicadorDebito" : indicadorDebito,
        "rubricaCredito" : rubricaCredito,
        "rubricaDebito" : rubricaDebito,
        "indicadorDiaCorridoUtil" : indicadorDiaCorridoUtil,
        "unidadeOrganizacional" : unidadeOrganizacional,
        "usuarioResponsavel" : usuarioResponsavel,
        "indicadorClienteObrigatorio" : indicadorClienteObrigatorio,
        "indicadorImovelObrigatorio" : indicadorImovelObrigatorio,
        "indicadorRgSolicitanteObrigatorio" : indicadorRgSolicitanteObrigatorio,
        "indicadorCpfCnpjSolicitanteObrigatorio" : indicadorCpfCnpjSolicitanteObrigatorio,
        "indicadorTramitacao" : indicadorTramitacao,
        "indicadorNomeSolicitanteObrigatorio" : indicadorNomeSolicitanteObrigatorio,
        "indicadorTelefoneSolicitanteObrigatorio" : indicadorTelefoneSolicitanteObrigatorio,
        "indicadorEmailSolicitanteObrigatorio" : indicadorEmailSolicitanteObrigatorio,
        "indicadorGeraServicoAutorizacao" : indicadorGeraServicoAutorizacao,
        "indicadorFechamentoAutomatico" : indicadorFechamentoAutomatico,
        "indicadorPrazoDiferenciado" : indicadorPrazoDiferenciado,
        "chavePrimaria" : chavePrimaria,
        "indexList" : indexList,
        "indicadorUOVisualizadora" : indicadorUOVisualizadora,
        "noCache" : noCache,
        "uoVisualizadoras" : uoVisualizadoras,
        "indicadorVerificaRestServ" : indicadorVerificaRestServ,
        "indicadorEncerraAutoASRel" : indicadorEncerraAutoASRel,
        "indicadorVerificarVazamento" : indicadorVerificarVazamento,
        "orientacao" : orientacao,
        "indicadorAcionamentoGasista" : indicadorAcionamentoGasista,
        "permiteAbrirEmLote": permiteAbrirEmLote,
        "questionario" : questionario,
        "canalAtendimento" : canalAtendimento,
        "enviarEmailCadastro" : enviarEmailCadastro,
        "enviarEmailTramitar" : enviarEmailTramitar,
        "enviarEmailReiterar" : enviarEmailReiterar,
        "enviarEmailEncerrar" : enviarEmailEncerrar,
        "enviarEmailPrioridadeProtocolo" : enviarEmailPrioridadeProtocolo,
        "indicadorComGarantia": indicadorComGarantia,
        "indicadorRegulatorioValores" : indicadorRegulatorio,
        "nomeRelatorioRegulatorio" : nomeRelatorioRegulatorio

    }).done(function( retorno ) {
        retorno = $.trim(retorno);
        if (retorno.indexOf('<div class="notification failure ') == 0) {
            $('.mensagensSpring').removeAttr('style');
            $('.mensagensSpring').html(retorno);
            $("html, body").animate({ scrollTop: $('html').offset().top }, 1000);
        }else{
            $("#gridComponentesAssunto").html(retorno);

            if (indexList != -1){
                document.getElementById("botaoAlterarAssunto").disabled=true;
                document.getElementById("botaoIncluirAssunto").disabled=false;
            }

            limparCaixaAssuntos();
            limparSessaoServicos();
            $('#spanRubricaCredito').hide();
            $('#spanRubricaDebito').hide();
            document.getElementById('rubricaCredito').disabled=true;
            document.getElementById('rubricaDebito').disabled=true;
        }
    });
}

function limpar(){
    $(':input','#chamadoAssuntoForm')
        .not(':button, :submit, :reset, :hidden, #descricao')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function limparSessaoServicos(){
    var chave=1;
    var noCache = "noCache=" + new Date().getTime();
    $("#gridComponentesServicoTipo").load("removerTodosServicos?chave="+chave+"&"+noCache)
}



function habilitaRubrica(elem){

    if(document.getElementById("indicadorDebito").value==elem){
        document.getElementById("servicoTipo").disabled=true;
    }else{
        document.getElementById("servicoTipo").disabled=false;
    }
}



function removeSpecialCharSimple(strToReplace) {
    strSChar = "!@#$%�&*()_-+=/?��������";
    strNoSChars = "a";
    var newStr = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (strSChar.indexOf(strToReplace.charAt(i)) != -1) {
            newStr += strNoSChars.substr(strSChar.search(strToReplace.substr(i,1)),1);
        } else {
            newStr += strToReplace.substr(i,1);
        }
    }
    return newStr;
}


function obterListaServicos(){

    var cliente = document.getElementById("indicadorClienteObrigatorio1").checked;
    var imovel = document.getElementById("indicadorImovelObrigatorio1").checked;

    AjaxService.obterListaServicos( cliente+";"+imovel,
        function(pontos) {
            var selectOperacoes = document.getElementById("servicoTipo");
            removeChildrenFromNode(selectOperacoes);
            for (key in pontos){
                var id = key;
                var novaOpcao = new Option(pontos[key], key);
                novaOpcao.label = pontos[key];
                selectOperacoes.appendChild(novaOpcao);
            }
            ordenar();
            selectOperacoes.insertBefore(new Option("Selecione", "-1"), selectOperacoes.firstChild );
            $("#servicoTipo").get(0).selectedIndex = 0;
        }
    );

}

function ordenar(){
    $("#servicoTipo").html($('#servicoTipo option').sort(function(x, y) {
        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
    }));
}

function removeChildrenFromNode(node) {
    if(node.hasChildNodes()) {
        while(node.childNodes.length >= 1 ) {
            node.removeChild(node.firstChild);
        }
    }
}

function init() {

    if(document.forms["chamadoAssuntoForm"].indicadorCredito[1].checked){
        desabilitarCredito();
    }else{
        habilitarCredito();
    }
    if(document.forms["chamadoAssuntoForm"].indicadorDebito[1].checked){
        desabilitarDebito();
    }else{
        habilitarDebito();
    }

}

function carregarResponsavel(chaveUnidadeOrganizacional){

    if(chaveUnidadeOrganizacional != "-1"){
        var noCache = "noCache=" + new Date().getTime();
        var chaveUsuarioResponsavel = 0;
        $("#divResponsavelAssunto").load("carregarResponsavelAssunto?chaveUnidadeOrganizacional="+chaveUnidadeOrganizacional+"&chaveUsuarioResponsavel="+chaveUsuarioResponsavel+"&"+noCache);
        $("#usuarioResponsavel").prop("disabled", false);
    }else{
        $("#usuarioResponsavel").val("-1");
        $("#usuarioResponsavel").prop("disabled", true);
    }
}

function mudarEstadoUnidadesVisualizadoras(radio){
    if(radio.value == "true"){
        $("#divUnidadeOrganizacionalVisualizadoras").show();
    }else{
        $("#divUnidadeOrganizacionalVisualizadoras").hide();
    }
}

function bloquearOperacoes() {
    if (document.forms["chamadoAssuntoForm"].permiteAbrirEmLote[0].checked) {
        document.forms["chamadoAssuntoForm"].indicadorAcionamentoGasista[1].checked = true;
        document.forms["chamadoAssuntoForm"].indicadorVerificarVazamento[1].checked = true;

        $("input[name=indicadorAcionamentoGasista]").prop("disabled", true);
        $("input[name=indicadorVerificarVazamento]").prop("disabled", true);
    } else {
        desbloquearOperacoes();
    }
}

function desbloquearOperacoes() {
    $("input[name=indicadorAcionamentoGasista]").prop("disabled", false);
    $("input[name=indicadorVerificarVazamento]").prop("disabled", false);
}

function carregarIndicadorRegulatorio(){
	var opcao = $("input[name=possuiIndicadorRegulatorio]").attr('checked');

	if(opcao){
		$("#divIndicadorRegulatorio").show();
	}else{
		$("#indicadorRegulatorioValores").val("");
		$("#divIndicadorRegulatorio").hide();
	}
}

