function exibirDetalhamento(chave){
    document.forms["chamadoAssuntoForm"].chavePrimaria.value = chave;
    submeter('chamadoAssuntoForm','exibirDetalhamentoChamadoAssunto');

}

function removerChamadoTipo(){

    var selecao = verificarSelecao();

    if (selecao == true) {
        var retorno = confirm("Deseja remover os itens selecionados?\nEsta Opera��o Nao poder� ser desfeita.");
        if(retorno == true) {
            document.forms["chamadoAssuntoForm"].chavesPrimarias.value = obterValorUnicoCheckboxSelecionado();
            submeter('chamadoAssuntoForm', 'removerChamadoTipo');
        }
    }
}

function incluir() {
    location.href = "exibirInclusaoChamadoAssunto";
}

function alterar() {
    var selecao = verificarSelecaoApenasUm();
    if (selecao == true) {
        document.forms["chamadoAssuntoForm"].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
        submeter('chamadoAssuntoForm','exibirAlteracaoChamadoAssunto');
    }

}

function limparFormulario(){
    document.getElementById('descricao').value = "";
    document.forms['chamadoAssuntoForm'].habilitado[0].checked = true;
}

function limparCampos () {
    document.getElementById('descricao').value = "";
    document.forms['chamadoAssuntoForm'].habilitado[0].checked = true;
    document.forms['chamadoAssuntoForm'].idSegmentoChamadoTipo.value = -1
    document.forms['chamadoAssuntoForm'].categoriaChamado.value = -1;
}
