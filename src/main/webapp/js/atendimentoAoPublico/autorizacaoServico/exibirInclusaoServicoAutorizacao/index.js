$(document).ready(function () {
    var fluxoDetalhamento = $('#fluxoDetalhamento').val();
    var fluxoAlteracao = $('#fluxoAlteracao').val();
    var fluxoInclusao = $('#fluxoInclusao').val();
    var fluxoRemanejar = $('#fluxoRemanejar').val();
    var fluxoExecucao = $('#fluxoExecucao').val();
    var fluxoEncerramento = $('#fluxoEncerramento').val();

    if (fluxoAlteracao) {
        $(".campoData2").datepicker({
            changeYear: true,
            showOn: 'button',
            buttonImage: 'imagens/calendario.gif',
            buttonImageOnly: true,
            buttonText: 'Exibir Calend�rio',
            dateFormat: 'dd/mm/yy'
        });
    }


    carregarPontosConsumo();
    
    verificarNecessidadeNovoMedidor($("#idServicoTipo").val());

    if (fluxoInclusao == "") {
        $("form:not(#modalResponderFormulario_form) :input").not("input[type=button]").not('button').attr("disabled", "disabled");
        $("input[type=submit]").removeAttr("disabled");
        $('.acoes-cancelar').removeAttr('disabled');
        $('#botaoVoltar').removeAttr('disabled');
        $('#botaoRemanejar').removeAttr('disabled');

        removerAsteriscos();
    }
    $("input[type=hidden]").removeAttr("disabled");
    $("input[name=checkPontoConsumo]").removeAttr("disabled");

    if (fluxoAlteracao != "") {
        document.getElementById("descricao").disabled = false;
        $("#spanDescricao").html('*&nbsp;');
        $('span[id="spanServicoTipo"]').remove();
        $('span[id="spanTipoPrioridade"]').remove();
        $('#quantidadeMaterial').removeAttr('disabled');
        $('#material').removeAttr('disabled');
        $('#equipamento').removeAttr('disabled');
        $('#dataPrevisaoEncerramento').removeAttr('disabled');
        $("#dataPrevisaoEncerramento").inputmask("99/99/9999 99:99", {placeholder: "_", greedy:false});
        $("#dataPrevisaoEncerramento").datetimepicker({
                        minDate: '+0d',
                        showOn: 'button',
                        buttonImage: $('#imagemCalendario').val(),
                        buttonImageOnly: true,
                        buttonText: 'Exibir Calendário',
                        dateFormat: 'dd/mm/yy',
                        timeFormat: 'HH:mm'
                    });
        $('#numeroProtocolo').removeAttr('disabled');
        $('#numeroOS').removeAttr('disabled');


        $('#dataSolicitacaoRamal').removeAttr('disabled');
        criarDatePicker('#dataSolicitacaoRamal');
        $("#dataSolicitacaoRamal").inputmask("99/99/9999", {placeholder: "_"});
        
        
        $("#numeroSerie").removeAttr('disabled');
        $("#marca").removeAttr('disabled');
        $("#tipo").removeAttr('disabled');
        $("#marcaModelo").removeAttr('disabled');
        $("#modoUsoNormal").removeAttr('disabled');
        $("#modoUsoVirtual").removeAttr('disabled');
        $("#modoUsoIndependente").removeAttr('disabled');
        $("#modoUsoTodos").removeAttr('disabled');

    }

    if (fluxoDetalhamento == "") {
        $(".file").removeAttr("disabled");
        $(".campoFile").removeAttr("disabled");
        $('#descricaoAnexo').removeAttr('disabled');
        $(".colunaEsq2").css("padding-left", "122px");
        carregarTurnosEquipe();
    }

    if (fluxoDetalhamento) {
        $('#botaoLimparMaterial').attr("disabled", "disabled");
        $('#botaoIncluirMaterial').attr("disabled", "disabled");
        $('#botaoAlterarMaterial').attr("disabled", "disabled");
        $('#botaoIncluirEquipamento').attr("disabled", "disabled");
        $("#spanPrevisaoEncerramento").html('');
        $("#spanTipoPrioridade").html('');
        $("#spanMotivo").html('');
    }

    if (fluxoRemanejar) {
        $('span[id="spanTipoPrioridade"]').remove();
        $('span[id="spanPrevisaoEncerramento"]').remove();
        $("#spanDescricao").html('*&nbsp;');
        $("#spanEquipe").html('*&nbsp;');
        $('#descricao').removeAttr('disabled');
        $('#equipe').removeAttr('disabled');
        $('#botaoLimparMaterial').attr("disabled", "disabled");
        $('#botaoIncluirMaterial').attr("disabled", "disabled");
        $('#botaoAlterarMaterial').attr("disabled", "disabled");
        $('#botaoIncluirEquipamento').attr("disabled", "disabled");
    }

    var indicadorEncerramentoAuto = $("[name='servicoAutorizacao.servicoTipo.indicadorEncerramentoAuto']").val();;

    if (fluxoExecucao || fluxoEncerramento || (fluxoExecucao && indicadorEncerramentoAuto)) {
        $("#spanPrevisaoEncerramento").html('');
        $("#spanTipoPrioridade").html('');
        $('#quantidadeMaterial').removeAttr('disabled');
        $('#material').removeAttr('disabled');
        $('#equipamento').removeAttr('disabled');
        $('#servicoAutorizacaoMotivoEncerramento').removeAttr('disabled');
        $("#descricao").removeAttr('disabled');
        $("input[name=indicadorVazamentoConfirmado]").attr('disabled', false)
        $("#unidadeOrganizacional").attr('disabled', false)
        $("#executante").attr('disabled', false)
        $("#turnoPreferencia").attr('disabled', true)
    }

    if (fluxoExecucao && indicadorEncerramentoAuto != true) {
        $('#indicadorSucessoExecucaoSim').removeAttr('disabled');
        $('#indicadorSucessoExecucaoNao').removeAttr('disabled');
    }

    if (fluxoExecucao) {
        $("#spanDescricao").html('*&nbsp;');
        $("#indicadorSucessoExecucaoSim").prop("checked", true);
    }

    var chamadoTipo = $("[name='chamadoTipo.chavePrimaria']").val();
    if (chamadoTipo != null && chamadoTipo != "") {
        var chavePrimaria = chamadoTipo;
        atualizarAssuntos(chavePrimaria);
    }

    if (fluxoInclusao) {
        $("#dataPrevisaoEncerramento").attr("disabled", "disabled");
        $("#turnoPreferencia").attr("disabled", "disabled");
        $("#dataPrevisaoEncerramento").inputmask("99/99/9999 99:99", {placeholder: "_", greedy:false});
        $("#dataExecucao").attr("disabled", "disabled");
        criarDatePicker('#dataSolicitacaoRamal');
        $("#dataSolicitacaoRamal").inputmask("99/99/9999", {placeholder: "_"});
        
        if ($("#chaveImovel").val() != '') {
   	 		document.forms[0].imovel.value = $("#chaveImovel").val();		
   		}
    }

    var motivoEncerramento = $("[name='servicoAutorizacao.servicoAutorizacaoMotivoEncerramento']").val();
    if (fluxoDetalhamento && (motivoEncerramento == null || motivoEncerramento == '')) {
        $("#divMotivo").hide();
    }

    if (fluxoExecucao || fluxoAlteracao) {
        $("#equipe").removeAttr("disabled");
    }

    if (fluxoExecucao || fluxoEncerramento) {

        $(".notification.failure.hideit").show();
        $("#dataExecucao").removeAttr("disabled");
        $("#dataExecucao").inputmask("99/99/9999 99:99", {placeholder: "_"});
        $("#dataExecucao").datetimepicker({
            maxDate: '+0d',
            showOn: 'button',
            buttonImage: 'imagens/calendario.png',
            buttonImageOnly: true,
            buttonText: 'Exibir Calend�rio',
            dateFormat: 'dd/mm/yy',
            timeFormat: 'HH:mm'
        });
    }
    
    $("#dialog-confirm").dialog({
        autoOpen: false,
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Sim": function () {
                $(this).dialog("close");
                $("#comChamado").val("true");
                submeter('servicoAutorizacaoForm', 'imprimirServicoAutorizacao');
            },
            "Nao": function () {
                $(this).dialog("close");
                $("#comChamado").val("false");
                submeter('servicoAutorizacaoForm', 'imprimirServicoAutorizacao');
            }
        }
    });

    var maxLength = 100;
    $('#servicoTipo > option').text(function (i, text) {
        text = text.trim();
        if (text.length > maxLength) {
            return text.substr(0, maxLength) + '...';
        }
    });
    //carregarImovel();
    
   /* if (fluxoAlteracao) {
		$("#divMobile").find('input, button, select').prop('disabled', false);
	} else {
	} */

	$("#divMobile").find('input, button, select').prop('disabled', true);
    $(".notification.failure.hideit").show();
    $(".notification.mensagemAlerta.hideit").show();

});

function criarDatePicker(seletor) {
    $(seletor).datepicker({
        changeYear: true,
        maxDate: '+0d',
        showOn: 'button',
        buttonImage: 'imagens/calendario.png',
        buttonImageOnly: true,
        buttonText: 'Exibir Calend�rio',
        dateFormat: 'dd/mm/yy'
    });
}

function removerAsteriscos() {
    $('span[id="spanServicoTipo"]').remove();
    $('span[id="spanEquipe"]').html('');
    $('span[id="spanCanalAtendimento"]').remove();
    $("#spanUnidadeOrganizacional").html('');
    $('span[id="spanNumeroContrato"]').remove();
    $("#spanDescricao").html('');
    $("#spanResponsavel").html('');
    $("#spanResponsavel").html('');
}

function incluir() {
	if (document.forms[0].nome.value == "") {
		document.forms[0].cliente.value = null;
	}

	let abaMedidorDisponivel = $('[href="#contentTabMedidor"]').closest('li').css("display");


	document.getElementById("isGerarAsAutomatica").value = "true";

	if(verificarLeituraMovimentoCiclo() && verificarServicoTipoMesmaOperacao()) {
		if (abaMedidorDisponivel != "none") {
			verificarMedidorSelecionado();
		} else {
			submeter('servicoAutorizacaoForm', 'incluirServicoAutorizacao');
		}
	}


}

function verificarServicoTipoMesmaOperacao() {
	let idServicoTipo = $("#servicoTipo").val();
	let idPontoConsumo = $("#pontoConsumo").val();
	let confirmacao = true;
	
	if(idServicoTipo == '-1' || idPontoConsumo == '') {
		return true;
	}
	
	AjaxService.consultarMesmaOperacaoServicoTipo(idPontoConsumo, idServicoTipo, {
		callback: function(retorno) {
			confirmacao = retorno;
		}, async: false
	});
	
	if(confirmacao) {
		confirmacao = confirm('Há uma AS com outro serviço para a mesma operação no medidor deseja prosseguir?');
	} else {
		confirmacao = true;
	}
	
    return confirmacao;;
}


function verificarLeituraMovimentoCiclo() {
	let idServicoTipo = $("#servicoTipo").val();
	let idPontoConsumo = $("#pontoConsumo").val();
	let confirmacao = true;
	
	if(idServicoTipo == '-1' || idPontoConsumo == '') {
		return true;
	}
	
	AjaxService.verificarImovelCicloLeitura(idPontoConsumo, idServicoTipo, {
		callback: function(retorno) {
			confirmacao = retorno;
		}, async: false
	});
	
	if(confirmacao) {
		confirmacao = confirm('Esse ponto de consumo está em ciclo de leitura deseja continuar?');
		
		if(confirmacao) {
			$("#isPontoCicloLeitura").val(true);
		}
		
	} else {
		confirmacao = true;
	}
	
    return confirmacao;
}

function verificarMedidorSelecionado() {
	let valorMedidor = $("#medidor").val();
	if (valorMedidor != "") {
		submeter('servicoAutorizacaoForm', 'incluirServicoAutorizacao');
	} else {
		if (confirm('Deseja inserir a AS sem o medidor?')) {
			submeter('servicoAutorizacaoForm', 'incluirServicoAutorizacao');
		}
	}
}

function imprimirServicoAutorizacao() {
    document.forms["servicoAutorizacaoForm"].idServicoAutorizacao.value = $("[name='servicoAutorizacao.chavePrimaria']").val();
    if ($("#chamadoHistorico").find("tr.odd").length > 0 || $("#servicoAutorizacao").find("tr.odd").length > 0) {
        $("#dialog-confirm").dialog('open');
    } else {
        $("#comChamado").val("false");
        submeter('servicoAutorizacaoForm', 'imprimirServicoAutorizacao');
    }
}

function exibirAlterarServicoAutorizacao() {
    $("form :input").removeAttr("disabled");
    submeter('servicoAutorizacaoForm', 'exibirAlteracaoServicoAutorizacao');
}

function exibirRemanejarServicoAutorizacao() {
    submeter('servicoAutorizacaoForm', 'exibirRemanejarServicoAutorizacao');
}

function remanejarServicoAutorizacao() {
    submeter('servicoAutorizacaoForm', 'remanejarServicoAutorizacao');
}

function alterarServicoAutorizacaoEmExecucao() {
    var retorno = confirm('Deseja alterar o status das autoriza��es de servi�o para Em Execu��o?');
    if (retorno == true) {
        submeter('servicoAutorizacaoForm', 'alterarServicoAutorizacaoEmExecucao');
    }

}


function alterarServicoAutorizacao() {
    $("form :input").removeAttr("disabled");
    if (document.forms[0].nome.value == "") {
        document.forms[0].cliente.value = null;
    }
    submeter('servicoAutorizacaoForm', 'alterarServicoAutorizacao');
}

function exibirExecutarServicoAutorizacao() {
    $("form :input").removeAttr("disabled");
    submeter('servicoAutorizacaoForm', 'exibirExecutarServicoAutorizacao');
}

function executarServicoAutorizacao() {
    if (validarVerificacaoVazamento()) {
        $("form :input").removeAttr("disabled");
        submeter('servicoAutorizacaoForm', 'executarServicoAutorizacao');
    }
}

function validarVerificacaoVazamento() {
    var retorno = true;

    if ($("input[name=indicadorVazamentoConfirmado]").size() > 0 && $("input[name=indicadorVazamentoConfirmado]:checked").size() == 0) {
        alert("Preenchimento do campo Vazamento confirmado � obrigat�rio");
        retorno = false;
    }

    return retorno;
}

function exibirEncerrarServicoAutorizacao() {
    $("form :input").removeAttr("disabled");
    submeter('servicoAutorizacaoForm', 'exibirEncerrarServicoAutorizacao');
}

function encerrarServicoAutorizacao() {
	if (validarVerificacaoVazamento() && validarDataResolucao()) {
		$("form :input").removeAttr("disabled");
		checarOperacaoMedidorCorreta();
		submeter('servicoAutorizacaoForm', 'encerrarServicoAutorizacao');
	}
}

function validarDataResolucao() {
	var motivoEncerramento = $('#servicoAutorizacaoMotivoEncerramento').val();
	var dataExecucao = $('#dataExecucao').val();

	if (motivoEncerramento == 2 && dataExecucao == "") {
		alert("Necessário preencher data de execução");
		return false;
	}

	return true;

}


function carregarResponsavel(chaveUnidadeOrganizacional) {
    var noCache = "noCache=" + new Date().getTime();
    $("#divResponsavel").load("carregarResponsavel?chaveUnidadeOrganizacional=" + chaveUnidadeOrganizacional + "&" + noCache);
}

function cancelar() {
    submeter('servicoAutorizacaoForm', 'exibirPesquisaServicoAutorizacao');
}

function voltar() {
	$("#fluxoVoltar").val(true);
    submeter('servicoAutorizacaoForm', 'pesquisarServicoAutorizacao');
}

function atualizarAssuntos(chave) {
    var noCache = "noCache=" + new Date().getTime();
    var chaveAssunto = document.forms[0].chaveAssunto.value;
    $("#divAssuntos").load("carregarAssuntos?chavePrimaria=" + chave + "&chaveAssunto=" + chaveAssunto + "&" + noCache);
}

function pesquisarImovel() {
    var noCache = "noCache=" + new Date().getTime();
    $("#gridImoveis").load("consultarImovelOld");
}

function consultarMedidorDisponivel() {
	let numeroSerie = encodeURI(document.forms[0].numeroSerie.value);
    let marcaMedidor = document.forms[0].marcaMedidor.value;
    let tipoMedidor = document.forms[0].tipoMedidor.value;
    let modelo = document.forms[0].modelo.value;
    let modoUso = document.forms[0].modoUso.value;
    
    let url = "consultarMedidorDisponivel?numeroSerie="+numeroSerie+"&marcaMedidor="+marcaMedidor+"&tipoMedidor="+tipoMedidor+"&modelo="+modelo+"&modoUso="+modoUso;
	
    $("#gridMedidores").load(url, null, () => {
		iniciarDatatable('#table-grid-medidor', {
			destroy: true,
			columnDefs: [
				{ orderable: false, targets: 0 }
			]
		});
        if ($("input[name='chaveMedidor']").size() == 1) {
        	$("input[name='chaveMedidor']").trigger("click");
        } else {
			$("input[name='chaveMedidor']").each(function () {
            	$(this).trigger("click");
                return false;
            });
         }
    });
}


function selecionarMedidor(chaveMedidor){
	document.forms[0].medidor.value = chaveMedidor;
}

function carregarClientePorContratoServicoAutorizacao(numeroContrato) {
    if (numeroContrato != null && trim(numeroContrato) != "") {
        var url = "carregarClientePorContratoServicoAutorizacao?numeroContrato=" + numeroContrato;
        carregarFragmento('divCliente', url);
        var chavePrimariaCliente = document.forms[0].chaveCliente.value;

        $("#divContrato").load("carregarContratoPorNumero?numeroContrato=" + numeroContrato,
            function () {
                var chavePrimariaContrato = document.forms[0].chaveContrato.value;
                $("#gridImoveis").load("carregarImovelPorContratoAS?chavePrimariaContrato=" + chavePrimariaContrato + "&numeroContrato=" + numeroContrato,
                    function () {
                        if ($("input[name='chaveImovel']").size() == 1) {

                            $("input[name='chaveImovel']").trigger("click");
                        }
                    });
            }
        );
        $("#gridChamadosCliente").load("carregarClienteImovelServicoAutorizacao?chavePrimariaCliente=" + chavePrimariaCliente);
        document.forms[0].cliente.value = chavePrimariaCliente;
    }
}

function exibirDetalhamento(chave) {
    document.forms[0].chavePrimaria.value = chave;
    submeter('servicoAutorizacaoForm', 'exibirDetalhamentoServicoAutorizacao');
}

function selecionarPontoConsumo(chavePontoConsumo, descricao) {
	
    document.forms[0].pontoConsumo.value = chavePontoConsumo;
    //document.forms[0].descricaoPontoConsumo.value = descricao;
    carregarFragmento('gridChamadosCliente', "carregarClienteImovelServicoAutorizacao?chavePrimariaPontoConsumo=" + chavePontoConsumo);
    carregarFragmento('divCliente', "carregarClientePorPontoConsumoServicoAutorizacao?chavePontoConsumo=" + chavePontoConsumo);
    carregarFragmento('divContrato', "carregarContratoPorPontoConsumo?chavePontoConsumo=" + chavePontoConsumo);
    document.forms[0].cliente.value = document.forms[0].chaveCliente.value;


    $(".notification.failure.hideit").hide();
    $(".notification.mensagemAlerta.hideit").click(function () {
        $(this).fadeOut(700);
    });
    
}

function carregarPontosConsumo(chavePrimariaImovel) {
    $(".notification.failure.hideit").hide();
    $(".notification.mensagemAlerta.hideit").hide();

    var fluxoExecucao = "fluxoInclusao";
    var idContrato = '';

    // if (chavePrimariaImovel !== undefined) {
        $("#gridPontosConsumo").load("carregarPontosConsumoServicoAutorizacao?chavePrimariaImovel=" + chavePrimariaImovel + "&fluxoExecucao=" + fluxoExecucao + "&chavePrimariaContrato=" + idContrato,

            function () {

                if ($("input[name='checkPontoConsumo']").size() == 1) {
                    $("input[name='checkPontoConsumo']").trigger("click");
                    		
                }
                else {
	                $("input[name='checkPontoConsumo']").each(function () {
	                        $(this).trigger("click");
	                       return false;
	                });
                }

                verificarErroAlerta();
                
            });
    // }
    
    document.forms[0].imovel.value = chavePrimariaImovel;
    //document.forms[0].contrato.value = "";
}

function verificarErroAlerta() {
    $(".notification.failure.hideit").show();
    $(".notification.mensagemAlerta.hideit").show();

    if ($("input[name='matricula']").each(function () {
        var verificarErroAlerta = $('#imovel').val();
        if (verificarErroAlerta != 0) {
            $(".mensagensSpring").show();

            $(".notification.failure.hideit").click(function () {
                $(this).fadeOut(700);
            });
            $(".notification.mensagemAlerta.hideit").click(function () {
                $(this).fadeOut(700);
            });
        }
    })) ;
}


function carregarPrioridade(chavePrimaria) {
    var noCache = "noCache=" + new Date().getTime();
    var url = "carregarPrioridade?servicoTipo=" + chavePrimaria + "&" + noCache;

    if (chavePrimaria != "-1") {
        carregarFragmento('inputServicoTipoPrioridade', url);
    }
}

function verificarNecessidadeNovoMedidor(chavePrimaria) {

	AjaxService.verificarNecessidadeNovoMedidor(chavePrimaria, {
		callback: function(retorno) {
			if (retorno) {
				$('[href="#contentTabMedidor"]').closest('li').show();
			} else {
				$('[href="#contentTabMedidor"]').closest('li').hide();
			}
		}, async: false
	});
}

function carregarChaveEquipePrioritaria(chavePrimaria) {
	AjaxService.carregarChaveEquipePrioritaria(chavePrimaria, {
        callback: function (chaveEquipe) {
            $("#equipe").val(chaveEquipe);
        },
        async: false
    });
}

function adicionarServico() {

    var noCache = "noCache=" + new Date().getTime();
    var chave = document.forms["chamadoAssuntoForm"].servicoTipo.value;
    var url = "adicionarServico?servicoTipo=" + chave + "&" + noCache;
    if (chave != "-1") {
        carregarFragmento('gridComponentesServicoTipo', url);
    } else {
        document.getElementById("teste").value = 0;
        alert("Escolha um servi�o para adicionar.");
    }

    document.getElementById("servicoTipo").value = -1;
}

function limparFormularioInclusao() {
    //limparFormularios(document.servicoAutorizacaoForm);
    submeter('servicoAutorizacaoForm', 'exibirInclusaoServicoAutorizacao');
}

function limparFormularioAlteracao() {
    document.getElementById("descricao").value = "";
    document.getElementById("dataPrevisaoEncerramento").value = "";
}

function limparFormularioRemanejar() {
    document.getElementById("descricao").value = "";
    document.getElementById("equipe").value = -1;
}

var windows = {};

function imprimirArquivo(chaveServicoAutorizacaoHistorico, mobile) {

	if (windows['popup'] != undefined && windows['popup'] != null) {
        windows['popup'].close();
    }
    
    
    var url = 'imprimirArquivoServicoAutorizacao';
    windows['popup'] = window.open(url + "?chaveServicoAutorizacaoHistorico=" + chaveServicoAutorizacaoHistorico + "&mobile=" + mobile ,'popup', 'height=700,width=830,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function carregaroInformacaoPontoConsumo(chavePontoConsumo) {
    document.forms[0].idPontoConsumo.value = chavePontoConsumo;
}

function carregarOrientacao(chaveServicoTipo) {

    AjaxService.carregarOrientacaoServicoTipo(chaveServicoTipo, {
        callback: function (orientacao) {
            $("#orientacaoServicoTipo").val(orientacao);
        },
        async: false
    });
}

function questionarioRespondido() {
    $("#responderFormulario").val("Formul�rio Respondido");
    $("#responderFormulario").toggleClass("btn-success", true);
}

function carregarFuncionarios() {
    var idUnidadeOrganizacional = $("#unidadeOrganizacional").val();
    var selectFunc = document.getElementById("executante");

    selectFunc.length = 0;
    $('#executante').append(new Option("Selecione", "-1"));

    AjaxService.consultarFuncionarios(idUnidadeOrganizacional,
        function (funcionarios) {
            for (key in funcionarios) {
                var novaOpcao = new Option(funcionarios[key], key);
                selectFunc.options[selectFunc.length] = novaOpcao;
            }
            $('#executante option').sort(function (a, b) {
                if (a.innerHTML == 'Selecione') {
                    return -1;
                } else if (b.innerHTML == 'Selecione') {
                    return 1;
                }
                return (a.innerHTML > b.innerHTML) ? 1 : -1;
            }).appendTo('#executante');
            $('#executante').val(-1);
        });
}

function desabilitarBotoes() {
    $('#botaoAlterar').attr('disabled', false);
    $('#botaoEmExecucao').attr('disabled', false);
    $('#botaoExecutar').attr('disabled', false);
    $('#botaoRemanejar').attr('disabled', false);
    $('#botaoEncerrar').attr('disabled', false);
    $('#buttonImprimir').attr('disabled', false);
};

function checarOperacaoMedidorCorreta() {
	
	var idOperacaoMedidor = $("#idOperacaoMedidor").val();
	var idPontoConsumo = $("#idPontoConsumo").val();


	if("1" === idOperacaoMedidor){
	    AjaxService.checarOperacaoMedidorCorreta(idOperacaoMedidor, idPontoConsumo, {
	        callback: function (idOperacaoMedidorRetornado) {
	            if(idOperacaoMedidorRetornado != null) {
	            	$("#idOperacaoMedidor").val(idOperacaoMedidorRetornado);
	            } else {
	            	$("#tipoAssociacao").val("");
	            }
	        },
	        async: false
	    });
	}
}


function carregarTurnosEquipe() {

	let fluxoDetalhamento = $('#fluxoDetalhamento').val();
	let chaveEquipe = $("#equipe").val();

	var selectTurnoPreferencia = document.getElementById("turnoPreferencia");
	var idTurnoPreferencia = $("#idTurnoPreferencia").val();

	selectTurnoPreferencia.length = 0;

	var novaOpcao = new Option("Selecione", "-1");
	selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;


	if (fluxoDetalhamento == "") {
		selectTurnoPreferencia.disabled = false;
	}

	AjaxService.listaTurnoPorEquipe(chaveEquipe , {
		callback: function(listaTurnos) {

			for (key in listaTurnos) {

				var novaOpcao = new Option(listaTurnos[key], key);
				if (key == idTurnoPreferencia) {
					novaOpcao.selected = true;
				}
				selectTurnoPreferencia.options[selectTurnoPreferencia.length] = novaOpcao;
			}

		}
	});
}
