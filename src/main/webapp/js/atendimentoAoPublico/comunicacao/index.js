function exibirAlterarLoteComunicacao(){
	if(verificarSelecaoApenasUm()){
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		if(validarRetornoDocumento(document.forms[0].chavePrimaria.value)){
			if(confirmarEmissaoCartas()){
				submeter("comunicacaoForm","exibirAlteracaoLoteComunicacao");
			}
		}else{
			alert("Lote nao disponivel para alterar");
		}
	}
}

function validarRetornoDocumento(chavePrimaria){

	return AjaxService.verificarRetornoDocumento(chavePrimaria,{
		callback: function(validarRetorno){
		},
		async: false
	});
}

function confirmarEmissaoCartas(){
	if(confirm("Confirma que nao houve emissao de cartas deste lote?")){
		return true;
	}else{
		return false;
	}
}