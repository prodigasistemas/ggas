/**
 * fastLiveFilter jQuery plugin 1.0.3
 * 
 * Copyright (c) 2011, Anthony Bush
 * License: <http://www.opensource.org/licenses/bsd-license.php>
 * Project Website: http://anthonybush.com/projects/jquery_fast_live_filter/
 **/
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
    return function( elem ) {
    	var texto = strReplaceChr(jQuery(elem).text().toUpperCase());
    	var textoBusca = strReplaceChr(arg.toUpperCase());
        return texto.toUpperCase().indexOf(textoBusca) >= 0;
    };
});

function strReplaceChr(strToReplace) {
	strToReplace = strToReplace.replace("\u00C3", "A");
	strToReplace = strToReplace.replace("\u00C1", "A");
	strToReplace = strToReplace.replace("\u00CD", "I");
	strToReplace = strToReplace.replace("\u00D4", "O");
	strToReplace = strToReplace.replace("\u00D3", "O");
	strToReplace = strToReplace.replace("\u00CA", "E");
    strToReplace = strToReplace.replace("\u00C9", "E");
    strToReplace = strToReplace.replace("\u00C7", "C");
    strToReplace = strToReplace.replace("\u00DA", "U");
    return strToReplace;
}



jQuery.fn.fastLiveFilter = function(list, options) {
	// Options: input, list, timeout, callback
	
	options = options || {};
	list = jQuery(list);
	var input = this;
	var timeout = options.timeout || 0;
	var callback = options.callback || function() {};
	
	var keyTimeout;
	
	// NOTE: because we cache lis & len here, users would need to re-init the plugin
	// if they modify the list in the DOM later.  This doesn't give us that much speed
	// boost, so perhaps it's not worth putting it here.
	var lis = list.children();
	var len = lis.length;
	var oldDisplay = len > 0 ? lis[0].style.display : "block";
	callback(len); // do a one-time callback on initialization to make sure everything's in sync
	
	input.change(function() {
		var filter = $(this).val();
		        if(filter) {
		          $childrenMatches = $(list).find('a:Contains(' + filter + ')');
		          $matches = $(list).find('li:Contains(' + filter + ')').parent();
		          $('ul', list).not($matches).slideUp();
		          $matches.show();
		          $childrenMatches.css("background-color","#a9c0d0");
		          $('a', list).not($childrenMatches).removeAttr('style');
		        } else {
		          $(list).find("ul").hide();
		        }


	return false;
	}).keydown(function() {
		// TODO: one point of improvement could be in here: currently the change event is
		// invoked even if a change does not occur (e.g. by pressing a modifier key or
		// something)
		clearTimeout(keyTimeout);
		keyTimeout = setTimeout(function() { input.change(); }, timeout);
	});
	return this; // maintain jQuery chainability
}


