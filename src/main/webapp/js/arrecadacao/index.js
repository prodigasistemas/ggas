$().ready(function () {
    $("input#codigoConvenio").keyup(apenasNumeros);
    $("input#sequenciaCobrancaGerado,#sequencialCobrancaInicio,#sequencialCobrancaFim").keyup(apenasNumeros);
});

function limparFormularioArrecadadorConvenio() {
    var $arrecadadorConvenioForm = $("#arrecadadorConvenio");
    limparFormularios($arrecadadorConvenioForm);
}

function cancelarArrecadadorConvenio(url) {
    location.href = url;
}


$(document)
    .ready(
        function () {
            atualizarEstadoCampoLayout();
            $("#tipoConvenio").change(function () {
                var str = $('#tipoConvenio :selected').text();
                atualizarEstadoCampoLayout();
                if (~str.toLowerCase().indexOf("boleto")) {
                    $("#arrecadadorCarteiraCobranca").removeAttr("disabled");
                } else {
                    $("#arrecadadorCarteiraCobranca").attr("disabled", "disabled");
                }
            });
        });

function isSelecionadoTipoConvenioDebitoAutomatico() {
    var idDebitoAutomatico = $("#idTipoConvenioDebitoAutomatico").val();
    var idTipoConvenioSelecionado = $('#tipoConvenio').val();
    console.log('id conv>' + idDebitoAutomatico, ' id select: ' + idTipoConvenioSelecionado)
    return idTipoConvenioSelecionado == idDebitoAutomatico;
}

function verificarCampoLayout() {
    if (!isSelecionadoTipoConvenioDebitoAutomatico()) {
        $("#layoutFaturaDigital").val("");
    }
}


function atualizarEstadoCampoLayout() {
    var $idLayoutFatura = $("#fieldLayoutFatura");
    if (isSelecionadoTipoConvenioDebitoAutomatico()) {
        $idLayoutFatura.show();
    } else {
        $idLayoutFatura.hide();
    }
}
