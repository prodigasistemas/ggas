$(document).ready(function() {
	$("#cpf").inputmask("999.999.999-99", {
		placeholder : "_"
	});
	$("#cnpj").inputmask("99.999.999/9999-99", {
		placeholder : "_"
	});
	/*$("#inscricaoEstadual").inputmask(
			"${mascaraInscricaoEstadual}", {
				placeholder : "_"
	});*/

	$("#botaoPesquisar").click(
		function() {
			submeter("clienteForm",
					"pesquisarCliente");
		});
	$('thead').addClass('thead-ggas-bootstrap text-center');	
});

function removerCliente() {
	var selecao = verificarSelecao();
	if (selecao == true) {
		var retorno = confirm("Deseja remover os itens selecionados?\nEsta Opera\u00E7\u00E3o n\u00E3o poder\u00E1 ser desfeita.");
		if (retorno == true) {
			submeter('clienteForm', 'removerCliente');
		}
	}
}

function alterarCliente() {
	var selecao = verificarSelecaoApenasUm();
	document.forms[0].status.value = false;
	if (selecao == true) {
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter("clienteForm",	"exibirAlteracaoCliente");
	}
}

function detalharCliente(chave) {
	document.forms[0].chavePrimaria.value = chave;
	submeter("clienteForm",
			"exibirDetalhamentoCliente");
}

function incluir() {
	submeter("clienteForm","exibirInclusaoCliente");
}

function limparAbaPessoaFisica() {
	document.getElementById("cpf").value = "";
	document.getElementById("passaporte").value = "";
}

function limparAbaPessoaJuridica() {
	document.getElementById("").value = "";
}

function limparFormulario() {
    console.log("limpar form");
    document.forms['clienteForm'].nome.value = "";
    document.forms['clienteForm'].nomeFantasia.value = "";
    document.forms['clienteForm'].cnpj.value = "";
    document.forms['clienteForm'].cpf.value = "";    
    document.forms['clienteForm'].passaporte.value = "";
    document.forms['clienteForm'].inscricaoEstadual.value = "";
    document.forms['clienteForm'].inscricaoMunicipal.value = "";
    document.forms['clienteForm'].inscricaoRural.value = "";
    document.forms['clienteForm'].cep.value = "";
    document.forms['clienteForm'].inscricaoMunicipal.value = "";
    document.forms['clienteForm'].inscricaoRural.value = "";
    document.forms['clienteForm'].habilitado[0].checked = true;
    document.forms['clienteForm'].tipoCliente[2].checked = true;
    document.forms['clienteForm'].descricaoAtividadeEconomica.value = "";
    document.forms['clienteForm'].descricaoCompletaAtividadeEconomica.value = "";
}

var popup;

function exibirPopupPesquisaAtividadeEconomica() {
	popup = window
			.open('exibirPesquisaAtividadeEconomicaPopup?postBack=true',
				  'popup',
				  'height=370,width=700,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function selecionarAtividadeEconomica(idSelecionado) {
	var idAtividadeEconomia = document
			.getElementById("idAtividadeEconomia");
	var descricaoAtividadeEconomica = document
			.getElementById("descricaoAtividadeEconomica");

	if (popup != null) {
		popup.close;
	}

	if (idSelecionado != '') {
		AjaxService
				.obterAtividadeEconomicaPorChave(
						idSelecionado,
						{
							callback : function(atividade) {
								if (atividade != null) {
									idAtividadeEconomia.value = atividade["chavePrimaria"];
									descricaoAtividadeEconomica.value = atividade["descricao"];
								}
							},
							async : false
						}

				);
	} else {
		idAtividadeEconomia.value = "";
		descricaoAtividadeEconomica.value = "";
	}
	document.getElementById("descricaoCompletaAtividadeEconomica").value = idAtividadeEconomia.value
			+ ' - ' + descricaoAtividadeEconomica.value;
}