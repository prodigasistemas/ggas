$( document ).ready(function() {
		$(".pontoSemNumeroSequencia").parent().css("background" , "#FFF");
		
	});
	
	function limparFormulario(){
		$(":text").val("");
		$("select").val("-1");
	}


	function cancelar() {
		submeter('rotaForm', 'voltarRota');
	}

	function recarregarRotasRemanejamento(elem) {

		var idPeriodicidade = elem.value;
		var idRotaMantida = "${rotaForm.chavePrimaria}";	

		var selectRotas = document.getElementById("idRotaRemanejamento");
		var idRotaRemanejamento = "${processoForm.idRotaRemanejamento}";		  	
	
		selectRotas.length=0;
		var novaOpcao = new Option("Selecione","-1");
		selectRotas.options[selectRotas.length] = novaOpcao;
			AjaxService.listarRotaPorPeriodicidade(idRotaMantida, idPeriodicidade,function(rotas) {            		      		         		
       		for (key in rotas){
           		 var novaOpcao = new Option(rotas[key], key);
            	 if (key == idRotaRemanejamento){
            		novaOpcao.selected = true;
           		 }
        	selectRotas.options[selectRotas.length] = novaOpcao;		            			            	
    	}
    	ordernarSelect(selectRotas)
		}
	);	

	}

	function remanejarPontosConsumoRota(){	
	
		var idRotaRemanejamento = document
				.getElementById('idRotaRemanejamento').value;
		var contador = 0;
		$("fieldset.pontos-consumo-filhos input[type=checkbox][name='chavesPrimarias']:checked").each(function() {
			contador++;
		});
		
		if (contador != 0 && idRotaRemanejamento != '' ){
			AjaxService
				.verificarNumeroMaximoPontosConsumo(
						idRotaRemanejamento,
						contador,
						function(isExcedeuLimite) {
							
							if (isExcedeuLimite == true) {
                                alert(mensagensRota.maximoPontoExtrapoladoRemanejamento);								
								
                                AjaxService.initMapaContador(function(){});
							} else {
								submeter('rotaForm',
										'remanejarPontosConsumoRota');
							}
						});
		}else{
			submeter('rotaForm',
			'remanejarPontosConsumoRota');
		}
	}

	function moverPontoConsumo() {
		submeter('rotaForm',
			'moverPontoConsumo');
	}

	

function removerTodosPontosConsumoAssociados() {
	var lista = document.getElementById('idPontosConsumoAssociados');
	for (i = 0; i < lista.length; i++) {
		lista.options[i].selected = true;
		desassociarPontoConsumo(lista.options[i], i);
		i--;
	}
}

function removerPontosConsumoAssociados() {
	var lista = document.getElementById('idPontosConsumoAssociados');
	for (i = 0; i < lista.length; i++) {
		if (lista.options[i].selected == true) {
			desassociarPontoConsumo(lista.options[i], i);
			i--;
		}
	}
}

function selecionarItensSubmit() {

	var associados = document.getElementById('idPontosConsumoAssociados');
	for (i = 0; i < associados.length; i++) {
		associados.options[i].selected = true;
	}
	var remanejaveis = document
			.getElementById('idPontosConsumoRemanejaveis');
	for (i = 0; i < remanejaveis.length; i++) {
		remanejaveis.options[i].selected = true;
	}
	var remanejados = document.getElementById('idPontosConsumoRemanejados');
	for (i = 0; i < remanejados.length; i++) {
		remanejados.options[i].selected = true;
	}
	var disponiveis = document.getElementById('pontosConsumoDisponiveis');
	for (i = 0; i < disponiveis.length; i++) {
		disponiveis.options[i].selected = true;
	}

}

function desassociarPontoConsumo(option, index) {
	if (option.className == 'associadosOrigem') {
		moveSelectedOption(index,
				document.forms[0].idPontosConsumoAssociados,
				document.forms[0].idPontosConsumoRemanejaveis, true);
	} else {
		moveSelectedOption(index,
				document.forms[0].idPontosConsumoAssociados,
				document.forms[0].pontosConsumoDisponiveis, true);
	}
}

function desassociarRemanejamentoPontoConsumo() {
	var existePontosNaoRemanejaveis = false;
	var lista = document.getElementById('idPontosConsumoRemanejados');
	for (i = 0; i < lista.length; i++) {
		if (lista.options[i].selected == true) {
			if (lista.options[i].className != 'remanejadosOrigem') {
				moveSelectedOption(i,
						document.forms[0].idPontosConsumoRemanejados,
						document.forms[0].idPontosConsumoRemanejaveis, true);
				i--;
			} else {
				existePontosNaoRemanejaveis = true
			}
		}
	}
	if (existePontosNaoRemanejaveis) {
		alert('<fmt:message key="ERRO_NEGOCIO_ROTAS_NAO_REMANEJAVEIS"/>');
	}
}

function associarRemanejamentoPontoConsumo() {
	if (document.getElementById('idRotaRemanejamento').value != '-1') {
		moveSelectedOptionsEspecial(
				document.forms[0].idPontosConsumoRemanejaveis,
				document.forms[0].idPontosConsumoRemanejados, true);
	}
}

function associarRemanejamentoTodosPontosConsumo() {
	if (document.getElementById('idRotaRemanejamento').value != '-1') {
		moveAllOptionsEspecial(
				document.forms[0].idPontosConsumoRemanejaveis,
				document.forms[0].idPontosConsumoRemanejados, true);
	}
}

function atualizarContagemPontosAssociados() {
	atualizarContagemPontos(document
			.getElementById('quantidadePontosAssociados'), document
			.getElementById('idPontosConsumoAssociados').length);
}

function atualizarContagemPontosRemanejados() {
	atualizarContagemPontos(document
			.getElementById('quantidadePontosRemanejados'), document
			.getElementById('idPontosConsumoRemanejados').length);
}

function atualizarContagemPontos(elemContador, quantidade) {
	elemContador.innerHTML = quantidade;
}

function carregarPontoConsumo() {	
	
	var idPeriodicidadeCombo = document.getElementById("idPeriodicidadeCombo");
	var idTipoLeituraCombo = document.getElementById("idTipoLeituraCombo");

   	carregarGrupo(idPeriodicidadeCombo, idTipoLeituraCombo);
   	
	
}

function existeCampo(idPontoConsumo) {

	var retorno = false;
	var campo = document.forms[0].idPontosConsumoAssociados;

	for (i = 0; i < campo.length; i++) {

		if (idPontoConsumo == campo[i].value) {
			retorno = true;
			break;
		}

	}

	return retorno;
}

function carregarGrupo(codPeriodicidade, codTipoLeitura) {
	var selectGrupos = document.getElementById("idGrupoFaturamento");
	var idGrupo = "${rota.grupoFaturamento.chavePrimaria}";

	selectGrupos.length = 0;
	var novaOpcao = new Option("Selecione", "-1");
	selectGrupos.options[selectGrupos.length] = novaOpcao;
	AjaxService.listarGrupoFaturamento(codPeriodicidade.value,
			codTipoLeitura.value,

			function(grupos) {
				for (key in grupos) {
					var novaOpcao = new Option(grupos[key], key);

					if (key == idGrupo) {
						novaOpcao.selected = true;
					}

					selectGrupos.options[selectGrupos.length] = novaOpcao;
				}

			});

}

function verificarRotaAssociadaPontoConsumo() {
	var chavePrimaria = document.getElementById("chavePrimaria");

	AjaxService
			.consultarPontoConsumoPorRota(
					chavePrimaria.value,
					function(pontos) {
						for (key in pontos) {

							document.getElementById("idTipoLeituraCombo").className = 'campoDesabilitado';
							document.getElementById("idTipoLeituraCombo").disabled = true;

							document.getElementById('idPeriodicidadeCombo').className = 'campoDesabilitado';
							document.getElementById('idPeriodicidadeCombo').disabled = true;
						}

					});

}

function controlePeriodicidade() {

	var associados = document.getElementById('idPontosConsumoAssociados');
	var remanejaveis = document
			.getElementById('idPontosConsumoRemanejaveis');
	var remanejados = document.getElementById('idPontosConsumoRemanejados');

	if (associados.length > 0 || remanejaveis.length > 0) {
		document.getElementById('idPeriodicidadeCombo').disabled = true;
		document.getElementById("idTipoLeituraCombo").disabled = true;
	} else {
		document.getElementById('idPeriodicidadeCombo').disabled = false;
		document.getElementById("idTipoLeituraCombo").disabled = false;
	}

}

function init() {
	atualizarContagemPontosAssociados();
	atualizarContagemPontosRemanejados();
	carregarPontoConsumo();
	controlePeriodicidade();
}

addLoadEvent(init);

function exibirPopupAdicionarPontoConsumoRota(){
	popup = window.open('exibirPopupPesquisaPontoConsumoRota?postBack=true&','popup','height=700,width=920,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function adicionarPontoConsumoRota(listaIdSelecionado) {
	
	var valorMaximo = document.getElementById('numeroMaximoPontosConsumo').value;	
	var contador = 0;
	$("input[type=checkbox][id='chavesPrimarias']").each(function() { contador++; });
	var contador2 = 0;
	$("input[type=hidden][id='idPC']").each(function() { contador2++; });
	
	if(valorMaximo != '' && (contador+contador2) > valorMaximo){
		alert('<fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/>' );
		//$('.mensagensSpring').append('<div class="notification failure hideit"><p><strong>Erro: </strong><fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/></p></div>')
		return;
	}
	
	if (listaIdSelecionado != '' && listaIdSelecionado != undefined) {	
		$("#listaIdsPontoConsumoRota").val(listaIdSelecionado);
		submeter('rotaForm', 'adicionarPontoConsumoRotaAlteracao');	
	}
}

function removerPontoConsumo(chave) {
	$("#chavePontoRemover").val(chave);
	submeter('rotaForm', 'removerPontoConsumoRotaAlteracao');
}
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function ordenar() {
	var selecionou = false
	var idsSelecionados = []
		$("input:checked").each(function(){
	    	if ($(this).attr("value") != "0" && isNumber($(this).attr("value"))){
	    		selecionou = true
	    		idsSelecionados.push(parseInt($(this).attr("value")));
	    	}
		})
		if (!selecionou) {
			alert ("Selecione um ou mais pontos de consumo para realizar o opera\u00E7\u00E3o!");
			return false;
		}else{
			$('#chavesPrimariasOrdenacaoPontoConsumo').val(idsSelecionados.join(','))
			$('#ordenarTodos').val('false')
			submeter('rotaForm','ordenarPontosConsumo');
		}
}

function ordenarTodos() {
	submeter('rotaForm','ordenarPontosConsumo?ordenarTodos=true');
}

function removerPontoConsumoAgrupado(chave) {
	 
	var ids = chave.split(',');
	console.log(ids)
	$("#listaChavePontoRemover").val(ids);
	submeter('rotaForm', 'removerPontoConsumoRotaInclusaoAgrupado');
}




function adicionaRemoveIdImovel(acao, chavePontoConsumo, imovelCondominio, idImovel){
	 
	if (acao.checked){
		if (chavePontoConsumo == '0'){
			abrePopupConfirmacaoImportacaoImovelPai()
			idImovelSelecionado = idImovel
			idImovelCondominioSelecionado = imovelCondominio
		}
	}else{
		if (chavePontoConsumo == '0'){
			idImovelSelecionado = idImovel
			idImovelCondominioSelecionado = imovelCondominio
			adicionarPontoConsumoRotaConfirmacaoNao()
		}
	}
}

function abrePopupConfirmacaoImportacaoImovelPai(){
	$("#dialogImovelPai").dialog();
}






function expandeImoveisFilhos(acao, id){
	if (acao.innerHTML == '+'){
		try{
			$('.expandido').each(
	                function() {
	                    $(this).hide();
	        });
			}catch(e){
				
			}
			try{
			$('.expandeRetraiImovel').each(
	                function() {
	                    $(this).html('+');
	        });
			}catch(e){
						
					}
			
		acao.innerHTML = '-'
		$('#' + id).show()
		$('#' + id).addClass('expandido');
		try{
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).show()
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).focus()
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).hide()
		}catch(e){
			
		}
	}else{
		acao.innerHTML = '+'
		$('#' + id).removeClass('expandido');
		$('#' + id).hide()
	}
}


function adicionarPontoConsumoRotaConfirmacaoNao(){
	$("#dialogImovelPai").dialog('close');
	try{
		$('.'+ idImovelSelecionado).each(
                function() {
                        $(this).prop("checked", false);
                
            });
		$('.'+ idImovelCondominioSelecionado).each(
                function() {
                        $(this).prop("checked", false);
                
            });
	
	}catch(e){
		
	}
	
	idImovelSelecionado = null
} 


function adicionarPontoConsumoRotaConfirmacaoSim(){
	//var selecao = verificarSelecao();
	// if (selecao == true) {
		try{
        $('.'+ idImovelSelecionado).each(
                function() {
                        $(this).prop("checked", true);
                
            });
        $('.'+ idImovelCondominioSelecionado).each(
                function() {
                        $(this).prop("checked", true);
                
            });
		}catch(e){
			
		}
    //}
	$("#dialogImovelPai").dialog('close');
}

 

function ajustaGrid(){
	try{
		$( "#pontoConsumoRota_wrapper div:last-child" )[$( "#pontoConsumoRota_wrapper div:last-child" ).length - 4].style = "position:absolute;width: 96.5%;bottom: -43px;"
	}catch(e){
	}
	try{
	//	$('#pontoConsumoRota').html($('#pontoConsumoRota').html().replace('text-center sorting',''))
		}catch(e){
	}
		try{
		//	$('#pontoConsumo').html($('#pontoConsumo').html().replace('text-center sorting',''))
	}catch(e){
		}
	 
}
jQuery('.tabelaInterna').css('width',(jQuery(window).width() - 19) + 'px')

$( window ).bind("resize", function(){
	jQuery('.tabelaInterna').css('width',(jQuery(window).width() - 19) + 'px')
	$( "#pontoConsumoRota_wrapper div:last-child" )[$( "#pontoConsumoRota_wrapper div:last-child" ).length - 4].style = 'position:absolute'
});
try{
	//setTimeout(ajustaGrid(), 3000)
	setTimeout(function(){ ajustaGrid(); }, 500);
}catch(e){
	
}

function ordenaTodosPontosConsumo() {
	$('#ordenarTodos').val('true')
	submeter('rotaForm','ordenarPontosConsumo');
}
