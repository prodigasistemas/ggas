var marcaDesmarca = false
$( document ).ready(function() {
	$("#checkAllPontoConsumoRotaSequenciados").click(function(){
		marcaDesmarca = !marcaDesmarca
		$(this).prop("checked", marcaDesmarca);
		if (marcaDesmarca)
			$("#gridPontoConsumoRotaSequenciados tbody input[type='checkbox']:enabled").attr("checked", marcaDesmarca);
		else
			$("#gridPontoConsumoRotaSequenciados tbody input[type='checkbox']:enabled").removeAttr("checked");
	});

	$("#gridPontoConsumoRotaSequenciados .pagelinks a").click(function(e){
		paginacaoAjax("paginacaoRotaSequenciados", "gridPontoConsumoRotaSequenciados", e, this);
	});
	
	iniciarDatatable("#pontoConsumo");
	
});



function expandeImoveisFilhosSequenciados(acao, id){
	if (acao.innerHTML == '+'){
		try{
			$('.expandidoSequenciado').each(
	                function() {
	                    $(this).hide();
	        });
			}catch(e){
				
			}
			try{
			$('.expandeRetraiImovelSequenciado').each(
	                function() {
	                    $(this).html('+');
	        });
			}catch(e){
						
					}
			
		acao.innerHTML = '-'
		$('#' + id).show()
		$('#' + id).addClass('expandidoSequenciado');
		try{
		//	$('#' + id.replace('ImoveissequenciadosFilhos','InputImovelFilho')).show()
		//	$('#' + id.replace('ImoveissequenciadosFilhos','InputImovelFilho')).focus()
		//	$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).hide()
		}catch(e){
			
		}
	}else{
		acao.innerHTML = '+'
		$('#' + id).removeClass('expandido');
		$('#' + id).hide()
	}
	
	iniciarDatatable("#pontoConsumoAgrupadoTabela" + id)
}



var idImovelSelecionadoSequencial;
var idImovelCondominioSelecionadoSequencial
var tabelaSelecionada;

function adicionaRemoveIdImovelSequencial(acao, chavePontoConsumo, imovelCondominio, idImovel, idTabela){
	 
	if (acao.checked){
		if (chavePontoConsumo == '0'){
			abrePopupConfirmacaoImportacaoImovelPaiSequenciado()
			idImovelSelecionadoSequencial = idImovel
			idImovelCondominioSelecionadoSequencial = imovelCondominio
		}
	}else{
		if (chavePontoConsumo == '0'){
			idImovelSelecionadoSequencial = idImovel
			idImovelCondominioSelecionadoSequencial = imovelCondominio
			adicionarPontoConsumoRotaConfirmacaoNaoSequenciado()
		}
	}
	tabelaSelecionada = idTabela;
}

function abrePopupConfirmacaoImportacaoImovelPaiSequenciado(){
	$("#dialogImovelPaiSequenciado").dialog();
}






function adicionarPontoConsumoRotaConfirmacaoNaoSequenciado(){
	$("#dialogImovelPaiSequenciado").dialog('close');
	try{
	/*	$('.'+ idImovelSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", false);
                
            });
		$('.'+ idImovelCondominioSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", false);
                
            });*/
		$("#" + tabelaSelecionada+" ." + idImovelSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", false);
                
            });
	
	}catch(e){
		
	}
	
	idImovelSelecionadoSequencial = null
} 


function adicionarPontoConsumoRotaConfirmacaoSimSequenciado(){
	//var selecao = verificarSelecao();
	// if (selecao == true) {
		try{
      /*  $('.'+ idImovelSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", true);
                
            });
        $('.'+ idImovelCondominioSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", true);
                
            });*/
			$("#" + tabelaSelecionada+" ." + idImovelSelecionadoSequencial).each(
                function() {
                        $(this).prop("checked", true);
                
            });
		}catch(e){
			
		}
    //}
	$("#dialogImovelPaiSequenciado").dialog('close');
}







function ajustaGridSequenciado(){
	try{
		$( "#pontoConsumo_wrapper div:last-child" )[$( "#pontoConsumo_wrapper div:last-child" ).length - 4].style = "position:absolute;width: 96.5%;bottom: 0;margin-bottom: -43px;"
			
		//	$( "#pontoConsumo_wrapper div:last-child" ).css("position","absolute");
		//$( "#pontoConsumo_wrapper div:last-child" ).css("bottom","0");
		//$( "#pontoConsumo_wrapper div:last-child" ).css("margin-bottom","-43px");
	    
	}catch(e){
	}
	try{
	//	$('#pontoConsumoRota').html($('#pontoConsumoRota').html().replace('text-center sorting',''))
		$('#pontoConsumo_wrapper').css('height', '300px')
		
		
		}catch(e){
	}
		try{
		//	$('#pontoConsumo').html($('#pontoConsumo').html().replace('text-center sorting',''))
			
			
	}catch(e){
		}
	 
}

function ordenatabela(id, acao, coluna, tipoOrdenacao){
	 
	if ($(acao).hasClass('sorting_asc')){
		$(acao).removeClass('sorting_asc')
		$(acao).addClass('sorting_desc')
		sortTable($('#' + id),'asc', coluna, tipoOrdenacao);
	}else if ($(acao).hasClass('sorting_desc')){
		$(acao).removeClass('sorting_desc')
		$(acao).addClass('sorting_asc')
		sortTable($('#' + id),'desc', coluna, tipoOrdenacao);
	}else{
		$(acao).addClass('sorting_desc')
		sortTable($('#' + id),'asc', coluna, tipoOrdenacao);
	}
}

function sortTable(table, order, coluna, tipoOrdenacao) {
	 
    var asc   = order === 'asc',
        tbody = table.find('tbody');

    tbody.find('tr').sort(function(a, b) {
        if (asc) {
        	if (tipoOrdenacao == 1){
        		if 	(parseFloat($('td:nth-child(' + coluna + ')', a).text()) > (parseFloat($('td:nth-child(' + coluna + ')', b).text())))
        			return -1;
        		else return 1;
        	}else{
        		return $('td:nth-child(' + coluna + ')', a).text().localeCompare($('td:nth-child(' + coluna + ')', b).text());
        	}
            
        } else {
        	if (tipoOrdenacao == 1){
        		if 	(parseFloat($('td:nth-child(' + coluna + ')', b).text()) > (parseFloat($('td:nth-child(' + coluna + ')', a).text())))
        			return -1;
        		else return 1;
        	}else{
        		return $('td:nth-child(' + coluna + ')', b).text().localeCompare($('td:nth-child(' + coluna + ')', a).text());
        	}
            
        }
    }).appendTo(tbody);
}


//jQuery('.tabelaInternaSequencial').css('width',(jQuery(window).width() - 19) + 'px')

$( window ).bind("resize", function(){
	//jQuery('.tabelaInternaSequencial').css('width',(jQuery(window).width() - 19) + 'px')
	$( "#pontoConsumo_wrapper div:last-child" )[$( "#pontoConsumo_wrapper div:last-child" ).length - 4].style = 'position:absolute'
});
try{
	//setTimeout(ajustaGrid(), 3000)
	setTimeout(function(){ ajustaGridSequenciado(); }, 500);
}catch(e){
	
}
