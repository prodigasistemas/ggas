var marcaDesmarca = false
$( document ).ready(function() {
	$("#checkAllPontoConsumoRota").click(function(){
		marcaDesmarca = !marcaDesmarca
		$(this).prop("checked", !marcaDesmarca);
		if (marcaDesmarca){
		   $("#idGridPontoConsumoRota tbody input[type='checkbox']:enabled").attr("checked", $("._thead input[name='checkAllPontoConsumoRota'],#checkAllPontoConsumoRota").is(":checked"));
		}else{
			
			   $("#idGridPontoConsumoRota tbody input[type='checkbox']:enabled").removeAttr("checked")
			   
			  
		}
		 $('#pontoConsumoRota input').each(
		         function(){
		             if ($(this).prop( "checked")) 
		             	$(this).prop("checked", false);
		             else $(this).prop("checked", true);               
		           }
		)
			
	});
	$("#gridPontoConsumoRota .pagelinks a").click(function(e){
		paginacaoAjax("paginacaoRota", "gridPontoConsumoRota", e, this);
	});
	
	iniciarDatatable("#pontoConsumoRota");
});