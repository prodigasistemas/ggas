$(document).ready(function(){
    iniciarDatatable('#rota', {"order" : [[3, 'asc'], [2, 'asc']]});
});



function alterarRota(){
	
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {	
		document.forms[0].chavePrimaria.value = obterValorUnicoCheckboxSelecionado();
		submeter('rotaForm', 'exibirAlteracaoRota');
    }
}

function detalharRota(chave){
	document.forms[0].chavePrimaria.value = chave;
	submeter("rotaForm", "exibirDetalhamentoRota");
}

function incluir() {
	submeter('rotaForm', 'exibirInclusaoRota');
}

function limparFormulario(){
	$(":text").val("");
	$("select").val("-1");
	$("#ativo").attr("checked","checked");
}
