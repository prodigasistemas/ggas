$( document ).ready(function() {
	$(".pontoSemNumeroSequencia").parent().css("background" , "#FFF");
});

function limparFormulario(){
	$(":text").val("");
	$("select").val("-1");
	$('#posicaoOrdenacao').val("");
}

function cancelar() {
	submeter('rotaForm', 'pesquisarRota');
}

function associarPontosConsumoRota(){
	desabilitarBotoesPontoConsumo();
	atualizarContagemPontosAssociados();
	var lista2 = document.getElementById('pontosConsumoDisponiveis');		
	for (i=0; i<lista2.length; i++){
		lista2.options[i].selected = true;
	}
	
	var lista = document.getElementById('idPontosConsumoAssociados');		
	for (i=0; i<lista.length; i++){
		lista.options[i].selected = true;
	}
	
	
	var valorMaximo = document.getElementById('numeroMaximoPontosConsumo').value;		
	if(valorMaximo != '' && lista.length > valorMaximo){
		alert('<fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/>' );
		return;
	}
	submeter('rotaForm', 'associarPontosConsumoRota');
}




function adicionaRemoveIdImovel(acao, chavePontoConsumo, imovelCondominio, idImovel){
	if (acao.checked){
		if (chavePontoConsumo == '0'){
			abrePopupConfirmacaoImportacaoImovelPai()
			idImovelSelecionado = idImovel
			idImovelCondominioSelecionado = imovelCondominio
		}
	}else{
		if (chavePontoConsumo == '0'){
			idImovelSelecionado = idImovel
			idImovelCondominioSelecionado = imovelCondominio
			adicionarPontoConsumoRotaConfirmacaoNao()
		}
	}
}

function abrePopupConfirmacaoImportacaoImovelPai(){
	$("#dialogImovelPai").dialog();
}


function carregarGrupo(codPeriodicidade, codTipoLeitura){
  	var selectGrupos = document.getElementById("idGrupoFaturamento");
  	var idGrupo = "${rotaForm.grupoFaturamento.chavePrimaria}";
    	
  	selectGrupos.length=0;
  	var novaOpcao = new Option("Selecione","-1");
  	selectGrupos.options[selectGrupos.length] = novaOpcao;
   	AjaxService.listarGrupoFaturamento(codPeriodicidade.value, codTipoLeitura.value,
   			
       	function(grupos) {
           	for (key in grupos){
                var novaOpcao = new Option(grupos[key], key);
                selectGrupos.options[selectGrupos.length] = novaOpcao;
        	}
         
    	}
    );
   
}

function carregarPontoConsumo() {	
	
	var idPeriodicidade = document.getElementById("idPeriodicidade");
	var idTipoLeitura = document.getElementById("idTipoLeitura");

   	carregarGrupo(idPeriodicidade, idTipoLeitura);
   	
	
}

 

function atualizarContagemPontosAssociados(){
	if (document.getElementById('idPontosConsumoAssociados')){
		atualizarContagemPontos(document.getElementById('quantidadePontosAssociados'), document.getElementById('idPontosConsumoAssociados').length);	
	}
	
}

function atualizarContagemPontos(elemContador, quantidade){
	elemContador.innerHTML = quantidade;
}

function init () {
	atualizarContagemPontosAssociados();
}

addLoadEvent(init);

function exibirPopupAdicionarPontoConsumoRota(){
	popup = window.open('exibirPopupPesquisaPontoConsumoRota?postBack=true&','popup','height=700,width=920,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no ,modal=yes');
}

function adicionarPontoConsumoRota(listaIdSelecionado) {

	var valorMaximo = document.getElementById('numeroMaximoPontosConsumo').value;	
	var contador = 0;
	$("input[type=hidden][id='idPC']").each(function() { contador++; });
	
	if(valorMaximo != '' && contador >= valorMaximo){
		alert('<fmt:message key="ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO"/>' );
		return;
	}
	
	if (listaIdSelecionado != '' && listaIdSelecionado != undefined) {	
		$("#listaIdsPontoConsumoRota").val(listaIdSelecionado);
		submeter('rotaForm', 'adicionarPontoConsumoRotaInclusao');	
	}
}

function removerPontoConsumo(chave) {
	$("#chavePontoRemover").val(chave);
	submeter('rotaForm', 'removerPontoConsumoRotaInclusao');
}


function removerPontoConsumoAgrupado(chave) {
	 
	var ids = chave.split(',');
	console.log(ids)
	$("#listaChavePontoRemover").val(ids);
	submeter('rotaForm', 'removerPontoConsumoRotaInclusaoAgrupado');
}

function moverPontoConsumo() {
	$("#exibirInclusao").val(true);
	var selecao = verificarSelecaoApenasUm();
	if (selecao == true) {
		submeter('rotaForm','moverPontoConsumo');
	}
}


function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function ordenar() {
	var selecionou = false
	var idsSelecionados = []
		$("input:checked").each(function(){
	    	if ($(this).attr("value") != "0" && isNumber($(this).attr("value"))){
	    		selecionou = true
	    		idsSelecionados.push(parseInt($(this).attr("value")));
	    	}
		})
		if (!selecionou) {
			alert ("Selecione um ou mais pontos de consumo para realizar o opera\u00E7\u00E3o!");
			return false;
		}else{
			$('#chavesPrimariasOrdenacaoPontoConsumo').val(idsSelecionados.join(','))
			$('#ordenarTodos').val('false')
			submeter('rotaForm','ordenarPontosConsumo');
		}
}


function ordenarTodos() {
	submeter('rotaForm','ordenarPontosConsumo?ordenarTodos=true');
}







function expandeImoveisFilhos(acao, id){
	if (acao.innerHTML == '+'){
		try{
			$('.expandido').each(
	                function() {
	                    $(this).hide();
	        });
			}catch(e){
				
			}
			try{
			$('.expandeRetraiImovel').each(
	                function() {
	                    $(this).html('+');
	        });
			}catch(e){
						
					}
			
		acao.innerHTML = '-'
		$('#' + id).show()
		$('#' + id).addClass('expandido');
		try{
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).show()
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).focus()
			$('#' + id.replace('Imoveisfilhos','InputImovelFilho')).hide()
		}catch(e){
			
		}
	}else{
		acao.innerHTML = '+'
		$('#' + id).removeClass('expandido');
		$('#' + id).hide()
	}
}


function adicionarPontoConsumoRotaConfirmacaoNao(){
	$("#dialogImovelPai").dialog('close');
	try{
		$('.'+ idImovelSelecionado).each(
                function() {
                        $(this).prop("checked", false);
                
            });
		$('.'+ idImovelCondominioSelecionado).each(
                function() {
                        $(this).prop("checked", false);
                
            });
	
	}catch(e){
		
	}
	
	idImovelSelecionado = null
} 


function adicionarPontoConsumoRotaConfirmacaoSim(){
	//var selecao = verificarSelecao();
	// if (selecao == true) {
		try{
        $('.'+ idImovelSelecionado).each(
                function() {
                        $(this).prop("checked", true);
                
            });
        $('.'+ idImovelCondominioSelecionado).each(
                function() {
                        $(this).prop("checked", true);
                
            });
		}catch(e){
			
		}
    //}
	$("#dialogImovelPai").dialog('close');
}





function ajustaGrid(){
	try{
		$( "#pontoConsumoRota_wrapper div:last-child" )[$( "#pontoConsumoRota_wrapper div:last-child" ).length - 4].style = "position:absolute;width: 96.5%;bottom: -43px;"
	}catch(e){
	}
	try{
		//$('#pontoConsumoRota').html($('#pontoConsumoRota').html().replace('text-center sorting',''))
		}catch(e){
	}
		try{
			//debugger
			//$('#pontoConsumo').html($('#pontoConsumo').html().replace('text-center sorting',''))
	}catch(e){
		}
	 
}
jQuery('.tabelaInterna').css('width',(jQuery(window).width() - 19) + 'px')

$( window ).bind("resize", function(){
	jQuery('.tabelaInterna').css('width',(jQuery(window).width() - 19) + 'px')
	$( "#pontoConsumoRota_wrapper div:last-child" )[$( "#pontoConsumoRota_wrapper div:last-child" ).length - 4].style = 'position:absolute'
});
try{
	//setTimeout(ajustaGrid(), 3000)
	setTimeout(function(){ ajustaGrid(); }, 500);
}catch(e){
	
}


function ordenaTodosPontosConsumo(){
	$('#ordenarTodos').val('true')
	submeter('rotaForm','ordenarPontosConsumo');
}
