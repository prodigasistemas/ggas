/**
 * Função que recebe como argumentos o campo a ser formatado e a expressão regular e substitui
 * o valor do campo removendo os caracteres inválidos.
 */
function formatarCampoComExpressao(campo, regExp){
	valor = campo.value;	
	campo.value = valor.replace(regExp,'');
}

function formatarCampoNumeroEndereco(e, elem){
	var retorno = true;
	var reDigits = /[0-9a-zA-Z]+$/;
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
    if (!validarPadrao(reDigits,key) && !isTeclaControle(e.keyCode, e.charCode)) {
    	if(elem.value == '' && (key == 's' || key == 'S')){
    		elem.value = 'S/N';
    	}
    	retorno = false;    	
    } else {
    	if(elem.value == 'S/N' && !validarPadrao(/[\s\b]+$/,key) && !isTeclaControle(e.keyCode, e.charCode)){
    		retorno = false;
    	} else {
    		if(elem.value == 'S/N' && validarPadrao(/[\b]+$/,key)){
    			elem.value = '';
    		}
    	}
    }
    return retorno;
}

function formatarCampoNumeroEnderecoOnInput(e) {
    if (validarPadrao(/\d+/g, e.target.value)) {
        e.target.value = e.target.value.replace(/\D+/g, "");
    } else {
        if(e.target.value.toLowerCase() == 's' || e.target.value.toLowerCase().startsWith("s/n")) {
            e.target.value = 'S/N';
        } else {
            e.target.value = '';
        }
    }
}

function formatarCampoInteiroNegativosPositivos(e, tamanhoMax){	
	
	var reDigits =/^[-+]?\d*$/;
	var estouroTamanho = false;		
	if(tamanhoMax != undefined){
		var inteiro = obterParteInteira(eventTarget(e).value);	
		if(inteiro.length >= tamanhoMax){			
			estouroTamanho = true;			
		}
	}
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return ((validarPadrao(reDigits,key) && !estouroTamanho) || isTeclaControle(e.keyCode, e.charCode));
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas números.
 */
function formatarCampoInteiro(e, tamanhoMax){	
	var reDigits = /[0-9]+$/;
	var estouroTamanho = false;		
	if(tamanhoMax != undefined){
		var inteiro = obterParteInteira(eventTarget(e).value);	
		if(inteiro.length >= tamanhoMax){			
			estouroTamanho = true;			
		}
	}
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return ((validarPadrao(reDigits,key) && !estouroTamanho) || isTeclaControle(e.keyCode, e.charCode));
}

/**
 * Função a ser utilizada no evento 'onkeyup' para verificar se o conteudo colado no campo possui apenas numeros.
 */
function validarCampoInteiroCopiarColar(campo){
	if (campo.value!=""){
		var strNumeros = "01233456789";
		var tam = campo.value.length;
		var texto = campo.value;
		for (i = 0; i < tam; i++){
			if (strNumeros.indexOf(texto.charAt(i))==-1){
				campo.value='';
				break;
			}
		}
	}
}

/**
 * Função que retorna o elemento(ex. input) alvo do evento(compatível com IE e Firefox)
 */
function eventTarget(event) {
  event = event || window.event;            
  return(event.target || event.srcElement);
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas caracteres alfanuméricos.
 */
function formatarCampoAlfaNumerico(e){	
	var reDigits = /[a-z-A-Z0-9]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));
}

/**
 * Não permitir digitar caracteres especiais exceto o hífen e espaço
 * @param e
 * @returns {Boolean}
 */
function formatarCampoAlfaNumericoSemCaracteresEspeciais(e){	
	var reDigits = /^[a-z\u00C0-\u00ff A-Z0-9]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));
}

/**
 * Função combinada com a anterior para não permitir caracteres especiais exceto o espaço
 * @param e
 * @returns
 */

function campoSemHifen(e){
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	if(key == '-'){
		return false;	
	}
    return formatarCampoAlfaNumericoSemCaracteresEspeciais(e);
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas caracteres alfanuméricos, exceto o "-"(hífen).
 */
function formatarCampoAlfaNumericoSemHifen(e){
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	if(key == '-'){
		return false;	
	}
    return formatarCampoAlfaNumerico(e);
}

function formatarCampoTelefone(campo){
	var reDigits = /\D+$/;
	formatarCampoComExpressao(campo, reDigits);	
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas letras(sem acentuação), números, espaço e hífen.
 */
function formatarCampoNome(e){
	var reDigits = /[A-Za-z0-9\s\-\b]+$/;	
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);	
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

function validarCampoNomeSetorComercial(campo){
	
	var reDigits = /^\s*/;
	campo.value=campo.value.replace(reDigits,"");
    campo.value=campo.value.toUpperCase();
    if(!campo.value.match(/[A-Za-z0-9\s\@\.\b]+$/)) {
    	campo.value = "";
    }

}


function validarCampoArrobaPonto(campo){
	
	var reDigits = /^\s*/;
	campo.value=campo.value.replace(reDigits,"");
    campo.value=campo.value.toUpperCase();
    if(!campo.value.match(/[A-Za-z0-9\s\@\.\b]+$/)) {
    	campo.value = "";
    }

}


function formatarCampoNomeSetorComercial(e){
	var reDigits = /[A-Za-z0-9\s\@\.\b]+$/;	
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);	
    return (validarPadrao(reDigits,key) || isTeclaControleSetorComercial(e.keyCode, e.charCode));	
}


function formatarCampoArrobaPonto(e){
	var reDigits = /[A-Za-z0-9\s\@\.\b]+$/;	
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);	
    return (validarPadrao(reDigits,key) || isTeclaControleArrobaPonto(e.keyCode, e.charCode));	
}


/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas letras(com e sem acentuação), números, espaço, hífen, ponto e vírgula.
 */
function formatarCampoTexto(e){
	var reDigits = /[A-Za-zá-úÁ-Ú0-9\s\-\b\.\,]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

function formatarCampoTextoNomeQuestionario(e){
//	var reDigits = /[A-Za-zá-úÁ-Ú0-9\s\-\b\.\,?ç]+$/;		
	var reDigits = /[^@#$%¨&*()_+\;\[\]\\/\!\\\?\|\>}<={^]/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo com qualquer caracter menos o digitado no reDigits.
 */
function formatarCampoPergunta(e){
//	var reDigits = /[A-Za-zá-úÁ-Ú0-9\s\b\.\,\?]+$/;		
	var reDigits = /[^@#$%¨&*()_+\[\]\\!\\\=\|\>}<{^]/;	
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}



/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas letras(com e sem acentuação), números, espaço, hífen, ponto e vírgula respeitando o limite de caracteres.
 */
function formatarCampoTextoComLimite(e, elem, limiteCaracteres){
	var estouroLimite = false;
	var reDigits = /[A-Za-zá-úÁ-Ú0-9\s\-\b\.\,]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	if(elem.value.length == limiteCaracteres && validarPadrao(/[A-Za-zá-úÁ-Ú0-9\s\-\.\,]+$/,key)){
		estouroLimite = true;		
	}
		
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroLimite;	
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário dê como entrada
 * no campo qualquer texto livre, respeitando o limite de caracteres.
 */
function formatarCampoTextoLivreComLimite(e, elem, limiteCaracteres) {

	var estouroLimite = false;
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);

	if(elem.value.length >= limiteCaracteres) {
		estouroLimite = true;
		alert('Limite m\u00E1ximo: ' + limiteCaracteres + ' caracteres.');
	}
	
	// Caso o tamanho seja maior que o limite, permite apenas apagar (backspace ou delete)
    return (e.keyCode == 8 || e.keyCode == 46 || !estouroLimite);
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas letras sem acentuação), números, hífen, ponto e arroba.
 */
function formatarCampoEmail(e){
	var reDigits = /[A-Za-z0-9\_\-\@\.]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

function formatarCampoEmailMultiplo(e){
	var reDigits = /[A-Za-z0-9\_\-\@\.\,\;\s]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas letras sem acentuação), números, hífen, ponto e arroba.
 */
function formatarCampoCodigoSetorComercial(e){
	var reDigits = /[0-9\@\.]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode));	
}

function isTeclaControleSetorComercial(keycode, charcode) {
	retorno = false;

	if(charcode == 0 && (keycode == 8 || keycode == 9 || keycode == 13 || (keycode >= 16 && keycode <= 20) 
		|| keycode == 27 )){
		retorno = true;	
	}

	return retorno;
}

function isTeclaControleArrobaPonto(keycode, charcode) {
	retorno = false;

	if(charcode == 0 && (keycode == 8 || keycode == 9 || keycode == 13 || (keycode >= 16 && keycode <= 20) 
		|| keycode == 27 )){
		retorno = true;	
	}

	return retorno;
}

function isTeclaControle(keycode, charcode) {
	retorno = false;

	if(charcode == 0 && (keycode == 8 || keycode == 9 || keycode == 13 || (keycode >= 16 && keycode <= 20) 
		|| keycode == 27 || (keycode >= 33 && keycode <= 40) || keycode == 45 || keycode == 46)){
		retorno = true;	
	}

	return retorno;
}

function selecionarRestricoes(){
	if($("input[name='indicadorRestricaoServico']:checked").val()){
		$("#idsServicoTipoRestricaoSalvos option").attr("selected",'true');
	}
}

/**
 * Função que recebe como argumentos o nome do formulário e a ação e realiza o 'submit'.
 */
function submeter(nomeDoForm, acao, target){
	selecionarRestricoes();
	document.forms[nomeDoForm].action = acao;
	if (target != undefined) {
		document.forms[nomeDoForm].target = target;
	} else {
		document.forms[nomeDoForm].target = "";
	}
 	document.forms[nomeDoForm].submit();
}

/**
 * Função que recebe o identificado de uma div e abre ou oculta o filtro(div) qualquer.
 */
function abrirOcultarFiltro(divId) {

        var tabela = document.getElementById(divId);
        var img = document.getElementById('abrirOcultarFiltroImg'); 
        
        //abrir ou ocultar a tabela da pesquisa
        if(tabela.style.display=='none'){
        	 tabela.style.display = '';
        	 if (img != undefined) {
        	  	img.src = "imagens/ColapseIcon.png";
        	 	img.title = "Ocultar"
        		 img.alt = "Ocultar";
        	 }
        	
        } else {
        	tabela.style.display = 'none';
        	 if (img != undefined) {
        	 	img.src = "imagens/ExpandIcon.png";
        		img.title = "Exibir";
        		img.alt = "Exibir";
        	 }
        }
   
}

function exibirOcultarElemento(id) {
	var obj = document.getElementById(id);
	if (obj != undefined) {
		obj.style.display = (obj.style.display == '') ? 'none' : '';
	}
}

function exibirElemento(id) {
	var obj = document.getElementById(id);
	if (obj != undefined) {
		obj.style.display = '';
	}
}

function ocultarElemento(id) {
	var obj = document.getElementById(id);
	if (obj != undefined) {
		obj.style.display = 'none';
	}
}
 
function MM_formtCep(e,src,mask) {
        if(window.event) { _TXT = e.keyCode; } 
        else if(e.which) { _TXT = e.which; }
        if(_TXT > 47 && _TXT < 58) { 
  var i = src.value.length; var saida = mask.substring(0,1); var texto = mask.substring(i)
  if (texto.substring(0,1) != saida) { src.value += texto.substring(0,1); } 
     return true; } else { if (_TXT != 8) { return false; } 
  else { return true; }
        }
}

 /**
  * Valida se o valor informado está de acordo com o padrão informado.
  * @param padrao Expressão regular que representa o padrão.
  * @param texto O texto para ser validado.
  * @return true se o valor informado é válido, false caso contrário.
  */

function validarPadrao(padrao, texto) {
	var re = new RegExp(padrao);
  	return re.test(texto);
}
       
/**  
 * Função para aplicar máscara em campos de texto
 * Copyright (c) 2008, Dirceu Bimonti Ivo - http://www.bimonti.net 
 * All rights reserved. 
 * @constructor  
 */ 
 
/* Version 0.27 */

/**  
  * Função Principal 
  * @param w - O elemento que será aplicado (normalmente this).
  * @param e - O evento para capturar a tecla e cancelar o backspace.
  * @param m - A máscara a ser aplicada.
  * @param r - Se a máscara deve ser aplicada da direita para a esquerda. Veja Exemplos.
  * @param a - 
  * @returns null  
  */
function maskIt(w,e,m,r,a){
    
    // Cancela se o evento for Backspace
    if (!e) var e = window.event
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    
    // Variáveis da função
    var txt  = (!r) ? w.value.replace(/[^\d]+/gi,'') : w.value.replace(/[^\d]+/gi,'').reverse();
    var mask = (!r) ? m : m.reverse();
    var pre  = (a ) ? a.pre : "";
    var pos  = (a ) ? a.pos : "";
    var ret  = "";

    if(code == 9 || code == 8 || txt.length == mask.replace(/[^#]+/g,'').length) return false;

    // Loop na máscara para aplicar os caracteres
    for(var x=0,y=0, z=mask.length;x<z && y<txt.length;){
        if(mask.charAt(x)!='#'){
            ret += mask.charAt(x); x++;
        } else{
            ret += txt.charAt(y); y++; x++;
        }
    }
    
    // Retorno da função
    ret = (!r) ? ret : ret.reverse()    
    w.value = pre+ret+pos;
}

// Novo método para o objeto 'String'
String.prototype.reverse = function(){
    return this.split('').reverse().join('');
};

/**
 * Obtem o valor do checkbos selecionado
 */
function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

/**
 * Adciona uma funcao ao evento onload da pagina
 */
function addLoadEvent(func) {
  var onloadFunction = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (onloadFunction) {
        onloadFunction();
      }
      func();
    }
  }
}

/**
 * Retira espaços em branco antes e depois de um texto
 */
function trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

/**
 * Verifica se algum item foi selecionado na lista
 */
function verificarSelecao() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag <= 0) {
			alert ("Selecione um ou mais registros para realizar a opera\u00E7\u00E3o!");
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}
/**
 * Manter a aba selecionada
 */
function manterAbaSelecionada(form, abaId) {
	var obj = form.abaId;
	if (obj != undefined) {
		obj.value = abaId;
	}
}

function verificarQuantidadeItensSelecionados() {
	var flag = 0;
	var form = document.forms[0];
	
	var map = new Object(); 
	var array = new Array(); 
	var count = 0;

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
					array[count] = form.chavesPrimarias[i].value;
					count++;
				}
			}
		} 
	}
	
	map["quantidadeItensSelecionados"] = flag;
	map["arrayItensSelecionados"] = array;  
	
	return map;
}

/**
 * Verifica se apenas um item foi selecionado na lista
 */
function verificarSelecaoApenasUm() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag != 1) {
			alert ("Selecione apenas um registro para realizar a opera\u00E7\u00E3o!");
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

/**
 * Verifica se apenas um item foi selecionado na lista
 * @param chaves Name do input das chaves selecionadas para submeter.
 */
function verificarSelecaoApenasUmGeral(chaves) {
	var flag = 0;
	var chavesSelecao = document.getElementsByName(chaves);
	
	if (chavesSelecao != undefined && chavesSelecao.length > 0) {
		var total = chavesSelecao.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(chavesSelecao[i].checked == true){
					flag++;
				}
			}
		} else {
			if(chavesSelecao.checked == true){
				flag++;
			}
		}
		
		if (flag != 1) {
			alert ("Selecione um registro para realizar a opera\u00E7\u00E3o!");
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

/**
 * Obtém os valores dos checkboxes selecionados.
 */
function obterValoresCheckboxesSelecionados() {
	var indices = [];
	
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){		
					indices.push(form.chavesPrimarias[i].value);
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				indices.push(form.chavesPrimarias.value);
			}
		}
	}
	
	return indices;
	
}

/**
 * Obtém o valor do único checkbox selecionado.
 */
function obterValorUnicoCheckboxSelecionado() {
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){		
					return form.chavesPrimarias[i].value;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				return form.chavesPrimarias.value;
			}
		}	
	}
}

function obterValorUnicoCheckboxSelecionadoGeral(chaves) {
	var form = document.forms[0];
	var chavesSelecao = document.getElementsByName(chaves);
	
	if (chavesSelecao != undefined && chavesSelecao.length > 0) {
		var total = chavesSelecao.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(chavesSelecao[i].checked == true){		
					return chavesSelecao[i].value;
				}
			}
		} else {
			if(chavesSelecao.checked == true){
				return chavesSelecao.value;
			}
		}	
	}
}

/**
 * Função que obtém a posição inicial de seleção em um elemento
 */
function getCaretPos(elem) {
	if (typeof elem.selectionStart != 'undefined') {
		return elem.selectionStart;
	} else { 
		if (document.selection) {
			return Math.abs(document.selection.createRange().moveStart('character', -1000000));
		}	
	}
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas números.
 * DEPRECATED - USAR FUNÇÂO AUTONUMERIC DO JQUERY
 */
function formatarCampoDecimal(e,elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\-\b\,\.]+$/;
	var reDigits = /[0-9\b\-\,]+$/;			
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	// Permite ou não a entrada do sinal negativo para valores decimais
	if(key == '-' && getCaretPos(elem) > 0){
		reDigits = /[0-9\b\,]+$/;	
	}
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
				
	if(validarPadrao(/[0-9]+$/,key)){
		var inteiro = obterParteInteira(elem.value);	
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf(',') > 0 && getCaretPos(elem) > elem.value.indexOf(',') ){
				estouroInteiro = false;
			}
		}
		var decimal = obterParteDecimal(elem.value);		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf(',') > 0 && getCaretPos(elem) <= elem.value.indexOf(',') ){
				estouroDecimal = false;
			}
		}
	}
	
	if(validarPadrao(/[\,]+$/,key)){
		var separador = ',';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
	}
		
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroInteiro && !estouroDecimal && !estouroVirgula && !estouroPonto;
}

function formatarCampoDecimalPonto(e,elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\-\b\,\.]+$/;
	var reDigits = /[0-9\b\-\.]+$/;			
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	// Permite ou não a entrada do sinal negativo para valores decimais
	if(key == '-' && getCaretPos(elem) > 0){
		reDigits = /[0-9\b\.]+$/;	
	}
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
				
	if(validarPadrao(/[0-9]+$/,key)){
		var inteiro = obterParteInteira(elem.value);	
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf('.') > 0 && getCaretPos(elem) > elem.value.indexOf('.') ){
				estouroInteiro = false;
			}
		}
		var decimal = obterParteDecimalPonto(elem.value);		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf('.') > 0 && getCaretPos(elem) <= elem.value.indexOf('.') ){
				estouroDecimal = false;
			}
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
	}
		
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroInteiro && !estouroDecimal && !estouroVirgula && !estouroPonto;
}

function formatarCampoDecimalPontoNaoPermiteNegativo(e,elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\-\b\,\.]+$/;
	var reDigits = /[0-9\b\.]+$/;			
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
				
	if(validarPadrao(/[0-9]+$/,key)){
		var inteiro = obterParteInteira(elem.value);	
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf('.') > 0 && getCaretPos(elem) > elem.value.indexOf('.') ){
				estouroInteiro = false;
			}
		}
		var decimal = obterParteDecimalPonto(elem.value);		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf('.') > 0 && getCaretPos(elem) <= elem.value.indexOf('.') ){
				estouroDecimal = false;
			}
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
	}
		
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroInteiro && !estouroDecimal && !estouroVirgula && !estouroPonto;
}


/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas números.
 * DEPRECATED - USAR FUNÇÂO AUTONUMERIC DO JQUERY
 */
function formatarCampoDecimalNegativo(e,elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\-\b\,\.]+$/;
	var reDigits = /[0-9\b\-\,]+$/;			
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	// Permite ou não a entrada do sinal negativo para valores decimais
	if(key == '-' && getCaretPos(elem) > 0){
		reDigits = /[0-9\b\,]+$/;	
	}
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
	var valorNegativo = false;
	
	if(validarPadrao(/[0-9]+$/,key)){
		
		var inteiro = obterParteInteira(elem.value);
		
		if ((elem.value.substring(0,1)) == '-'){
			valorNegativo = true;
		}
		if (valorNegativo) {
			quantidadeInteiros = quantidadeInteiros + 1;
		}
		
		
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf(',') > 0 && getCaretPos(elem) > elem.value.indexOf(',') ){
				estouroInteiro = false;
			}
		}
		
		var decimal = obterParteDecimal(elem.value.substring(1));		
		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf(',') > 0 && getCaretPos(elem) <= elem.value.indexOf(',') ){
				estouroDecimal = false;
			}
		}
	}
	
	if(validarPadrao(/[\,]+$/,key)){
		var separador = ',';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
	}

    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroInteiro && !estouroDecimal && !estouroVirgula && !estouroPonto;
}

/**
 * Função a ser utilizada no evento 'onkeypress' para permitir que o usuário de como entrada
 * no campo apenas números positivos.
 * DEPRECATED - USAR FUNÇÂO AUTONUMERIC DO JQUERY
 */
function formatarCampoDecimalPositivo(e,elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\b\,\.]+$/;	
	var reDigits = /[0-9\b\,]+$/;	
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
				
	if(validarPadrao(/[0-9]+$/,key)){
		var inteiro = obterParteInteira(elem.value);	
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf(',') > 0 && getCaretPos(elem) > elem.value.indexOf(',') ){
				estouroInteiro = false;
			}
		}
		var decimal = obterParteDecimal(elem.value);		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf(',') > 0 && getCaretPos(elem) <= elem.value.indexOf(',') ){
				estouroDecimal = false;
			}
		}
	}
	
	if(validarPadrao(/[\,]+$/,key)){
		var separador = ',';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	}
	
	if(validarPadrao(/[\.]+$/,key)){
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
	}
	
    return (validarPadrao(reDigits,key) || isTeclaControle(e.keyCode, e.charCode)) && !estouroInteiro && !estouroDecimal && !estouroVirgula && !estouroPonto;
}

function formatarCampoDecimalVirgulaPorPontoNaoPermiteNegativo(elem,quantidadeInteiros,quantidadeDecimais){	
	// var reDigits = /[0-9\s\-\b\,\.]+$/;
	elem.value = elem.value.replace(",", ".");			
	
	var estouroInteiro = false;
	var estouroDecimal = false;
	var estouroPonto = false;
	var estouroVirgula = false;
				

		var inteiro = obterParteInteira(elem.value);	
		if(inteiro.length >= quantidadeInteiros){			
			estouroInteiro = true;
			// Se foi digitado após da vígula, desconsiderar um estouro de inteiro
			if(estouroInteiro && elem.value.indexOf('.') > 0 && getCaretPos(elem) > elem.value.indexOf('.') ){
				estouroInteiro = false;
			}
		}
		var decimal = obterParteDecimalPonto(elem.value);		
		if(decimal.length >= quantidadeDecimais){
			estouroDecimal = true;
			// Se foi digitado antes da vígula, desconsiderar um estouro de decimal
			if(estouroDecimal && elem.value.indexOf('.') > 0 && getCaretPos(elem) <= elem.value.indexOf('.') ){
				estouroDecimal = false;
			}
		}
	
	
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {			
			if (elem.value.substr(j-1,1) == separador){
				estouroVirgula = true;
				break;
			}
		}
		if(!estouroVirgula && obterParteInteira(elem.value) == ''){
			elem.value = '0' + elem.value;
		}
	
		var separador = '.';	
		for (var j=0; j<elem.value.length; j++) {	
			if (elem.value.substr(j-1,1) == separador){
				estouroPonto = true;
				break;
			}
		}
		if(!estouroPonto && obterParteInteira(elem.value) == ''){
			return false;
		}
		
    return true;
}

function obterParteInteira(valor){
	var retorno = '';
	var separador = ',';
	valor = valor + '';
	//alert(separadorParam);
	//if (separador!=null || separador!=undefined) {
	//	separador = separadorParam;
	//}
	for (var j=1; j<=valor.length; j++) {
		var dig = valor.substr(j-1,1);	
		if (dig == separador){
			break;
		} else {
			if (dig != '.') {		
				retorno += dig;			
			}
		}
	}		
	return retorno;
}

function obterParteInteiraSeparadoPorPonto(valor){
	var retorno = '';
	var separador = '.';
	valor = valor + '';

	for (var j=1; j<=valor.length; j++) {
		var dig = valor.substr(j-1,1);	
		if (dig == separador){
			break;
		} else {
			if (dig != '.') {		
				retorno += dig;			
			}
		}
	}		
	return retorno;
}


function obterParteDecimal(valor){
	var retorno = '';
	var separador = ',';	
	for (var j=1; j<=valor.length; j++) {	
		var temp = valor.substr(j-1,1);		
		if (valor.substr(j-1,1) == separador){
			retorno = valor.substr(j,valor.length);
			break;
		}
	}			
	return retorno;
}

function obterParteDecimalPonto(valor){
	var retorno = '';
	var separador = '.';	
	for (var j=1; j<=valor.length; j++) {	
		var temp = valor.substr(j-1,1);		
		if (valor.substr(j-1,1) == separador){
			retorno = valor.substr(j,valor.length);
			break;
		}
	}			
	return retorno;
}

function obterParteDecimalSeparadoPorPonto(valor){
	var retorno = '';
	var separador = '.';
	valor = valor+'';
	for (var j=1; j<=valor.length; j++) {	
		var temp = valor.substr(j-1,1);		
		if (valor.substr(j-1,1) == separador){
			retorno = valor.substr(j,valor.length);
			break;
		}
	}			
	return retorno;
}

function sortOptions(selElem) {
	var tmpAry = new Array();
    for (var i=0;i<selElem.options.length;i++) {
    	tmpAry[i] = new Array();
        tmpAry[i][0] = selElem.options[i].text;
        tmpAry[i][1] = selElem.options[i].value;
   	}
    tmpAry.sort();
    while (selElem.options.length > 0) {
    	selElem.options[0] = null;
    }
    for (var i=0;i<tmpAry.length;i++) {
    	var op = new Option(tmpAry[i][0], tmpAry[i][1]);
        selElem.options[i] = op;
    }
    return;
}

function clearOptionsFast(selectObj) {
	var selectParentNode = selectObj.parentNode;
	var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
	selectParentNode.replaceChild(newSelectObj, selectObj);
	return newSelectObj;
	
}

/**
 * Ordena elementos do tipo SELECT pelo 'text'. Para que o option 'Selecione' fique como primeiro 
 * da lista este deve ter o valor ''(vazio) ou '-1'.
 */
function ordernarSelect(obj) {
	var listaObjetos = new Array();
	
	if (!hasOptions(obj)) { 
		return; 
	}
	
	for (var i=0; i<obj.options.length; i++) {
		 novaOpcao = new Option( obj.options[i].text, obj.options[i].value, obj.options[i].defaultSelected, obj.options[i].selected);
		 novaOpcao.title = obj.options[i].title;
		 listaObjetos[listaObjetos.length] = novaOpcao;
	}
	
	if (listaObjetos.length==0) { 
		return; 
	}
	
	listaObjetos = listaObjetos.sort( 
		function(a,b) { 
			if ((a.text+"") < (b.text+"") || (a.value ==  '' || a.value ==  '-1')) { 
				return -1; 
			}
			if ((a.text+"") > (b.text+"") || (b.value ==  '' || b.value ==  '-1')) { 
				return 1; 
			}
				return 0;
			} 
		);

	for (var i=0; i<listaObjetos.length; i++) {
		novaOpcao = new Option(listaObjetos[i].text, listaObjetos[i].value, listaObjetos[i].defaultSelected, listaObjetos[i].selected);
		novaOpcao.title = listaObjetos[i].title;
		obj.options[i] = novaOpcao;
	}
}

function hasOptions(obj) {
	if (obj!=null && obj.options!=null) { 
		return true; 
	}
	return false;
}
/*
function letraMaiuscula(e){
	var reDigits = /[A-Za-zá-ú]+$/;		
	var whichCode = (window.event) ? e.keyCode : e.which;
	key = String.fromCharCode(whichCode);
	
	if(validarPadrao(reDigits,key)){
		key = 		
	}		
}*/

function letraMaiuscula(elem){
	elem.value = elem.value.toUpperCase();
}

function formataValorMonetario(campo,tammax){
	var valorAuxiliar = "";
	digitosValidos = "0123456789" ;
	
	valor = campo.value;
	if(valor != undefined){	
		//retira digitos não numéricos
			for (i=0;i<valor.length;i++){
				if(digitosValidos.indexOf(valor.charAt(i))>=0) {
					valorAuxiliar += valor.charAt(i);
				}
			}
	
		//verifica tamanho
		if(tammax > 0 && tammax < valorAuxiliar.length)
			valorAuxiliar = valorAuxiliar.substring(0,tammax);
	
		//retira zeros desnecessários no início do número
		while (valorAuxiliar.length > 3 && valorAuxiliar.charAt(0) == "0")
			valorAuxiliar = valorAuxiliar.substring(1);
	
		valor = valorAuxiliar;
		digitosNumericos = valor.length;
	
		//insere pontos decimais
		for(i = 1;i<=(digitosNumericos/3);i++)
			valor = valor.substring(0,digitosNumericos + 1 - 3*i) +
				(i==1?',':'.') +
				valor.substring(digitosNumericos + 1 - 3*i);
					
		campo.value = valor;
	}
}
Date.prototype.date = function(a) {
	if (m = /(\d{4})-(\d{1,2})-(\d{1,2})(?:\s(\d{1,2}):(\d{1,2}):(\d{1,2}))?/.exec(a)) {
		var f = ['', 'setFullYear', 'setMonth', 'setDate', 'setHours', 'setMinutes', 'setSeconds'], t = 0;

		for (var i=1; i < m.length; i++)
			if (m[i] && (t = parseInt(m[i][0] == '0' ? m[i][1] : m[i])))
				eval('this.'+f[i]+'('+ (i == 2 ? --t : t) +')');
	}

	return this;
}

Date.prototype.format = function(a) {
	var f = {
		d: ((t = this.getDate().toString()) && t.length == 1) ? "0" + t : t,
		j: this.getDate(),
		m: ((t = (this.getMonth() + 1).toString()) && t.length == 1) ? "0" + t : t,
		n: this.getMonth() + 1,
		Y: this.getFullYear(),
		y: this.getFullYear().toString().substring(2),
		H: ((t = this.getHours().toString()) && t.length == 1) ? "0" + t : t,
		G: this.getHours(),
		i: ((t = this.getMinutes().toString()) && t.length == 1) ? "0" + t : t,
		s: ((t = this.getSeconds().toString()) && t.length == 1) ? "0" + t : t,
		u: this.getMilliseconds()
	}, b = a;

	for (var i=0; i < a.length; i++)
		if ((t = a.substr(i, 1)) && f[t])
			b = b.replace(t, f[t]);

	return b;
}

function verificarSelecaoPontoConsumo() {
		var flag = 0;
		var form = document.forms[0];
	
		if (form != undefined && form.chavesPrimariasPontoConsumo != undefined) {
			var total = form.chavesPrimariasPontoConsumo.length;
			if (total != undefined) {
				for (var i = 0; i< total; i++) {
					if(form.chavesPrimariasPontoConsumo[i].checked == true){
						flag++;
					}
				}
			} else {
				if(form.chavesPrimariasPontoConsumo.checked == true){
					flag++;
				}
			}
		
			if (flag <= 0) {
				alert ("Selecione um ou mais pontos de consumo para realizar o opera\u00E7\u00E3o!");
				return false;
			}
			
		} else {
			return false;
		}
		
		return true;
}

/**
 * Função que aplica a máscara de número a um valor inteiro de um campo do tipo texto.
 */
function aplicarMascaraNumeroInteiro(campo){
	var numeroComMascara = '';
	var valor = replaceAll(campo.value, '.', '');
	numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(obterParteInteira(valor));
	
	campo.value = numeroComMascara;
}

function aplicarMascaraCampoHora(campo){
	var valor = replaceAll(campo.value, ':', '');
	campo.value = aplicarMascaraHora(valor);
}

function aplicarMascaraHora(numero){
	var numeroComMascara = '';
	
	if(numero.length<5){
		for (var j=0; j<numero.length; j++) {
			var dig = numero.substr(j,1);
			if (j==2){
				numeroComMascara = numeroComMascara + ':' + dig;
			}else{
				numeroComMascara = numeroComMascara + dig;
			}
		}	
	}
	return numeroComMascara;
}

/**
 * Função que aplica a máscara de número decimal a um valor decimal de um campo do tipo texto, considerando
 * a quantidade casas decimais informada.
 */
function aplicarMascaraNumeroDecimal(campo,quantidadeDecimais){
	var numeroComMascara = '';
		
	if(campo.value != ''){
		
		var valor = replaceAll(campo.value, '.', '');
		
		var inteiros = obterParteInteira(valor);
		inteiros = parseFloat(inteiros)+'';
				
		if (inteiros != 0) {
			numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
		} else {
			numeroComMascara = '0';
		}
		
		var decimais = obterParteDecimal(campo.value);
		if(campo.value != ''){	
			if(decimais.length > 0 && numeroComMascara.length == 0){
				numeroComMascara = 0;
			}
			numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
		}
		
	}
	
	if(valor != undefined){
		if(valor.charAt(0) == '-'){
			campo.value =  valor;
		}else{
			campo.value = numeroComMascara;
		}
	}
}

function aplicarMascaraNumeroDecimalNaoAplicarMascaraParteInteira(campo,quantidadeDecimais){
	var numeroComMascara = '';
		
	if(campo.value != ''){
		
		var valor = replaceAll(campo.value, '.', '');
		
		var inteiros = obterParteInteira(valor);
		inteiros = parseFloat(inteiros)+'';
				
		numeroComMascara = inteiros;
		
		var decimais = obterParteDecimal(campo.value);
		if(campo.value != ''){	
			if(decimais.length > 0 && numeroComMascara.length == 0){
				numeroComMascara = 0;
			}
			numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
		}
		
	}
	
	if(valor != undefined){
		if(valor.charAt(0) == '-'){
			campo.value =  valor;
		}else{
			campo.value = numeroComMascara;
		}
	}
}


function replaceAll(string, token, newtoken) {
	while (string.indexOf(token) != -1) {
 		string = string.replace(token, newtoken);
	}
	return string;
}

/**
 * Função que aplica a máscara de número decimal a um valor decimal de um campo do tipo texto, considerando
 * a quantidade casas decimais informada.
 * Ao contrario da função anterior 'aplicarMascaraNumeroDecimal' se a quantidade de decimais for menor que o tamanho
 * da parte decimal passada o número é truncado com a quantidade passada e não por 2.
 */
function aplicarMascaraNumeroDecimalTruncado(campo,quantidadeDecimais) {
	var numeroComMascara = '';
	
	var inteiros = obterParteInteira(campo.value);
	numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
	var decimais = obterParteDecimal(campo.value);
	if(campo.value != ''){	
		if(decimais.length > 0 && numeroComMascara.length == 0){
			numeroComMascara = 0;
		}
		
		//numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
		var decimaisComMascara = decimais;
		
		while(decimaisComMascara.length < quantidadeDecimais){
			decimaisComMascara += '0';
		}
		
		if(decimaisComMascara.length > quantidadeDecimais){
			decimaisComMascara = decimaisComMascara.substr(0,quantidadeDecimais);
		}
		numeroComMascara = numeroComMascara + ',' + decimaisComMascara;
	}
	
	campo.value = numeroComMascara;
}

function aplicarMascaraNumeroDecimalTruncadoSeparadorDecimalPonto(campo,quantidadeDecimais) {
	var numeroComMascara = '';
	
	var inteiros = obterParteInteiraSeparadoPorPonto(campo.value);
	numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
	var decimais = obterParteDecimalSeparadoPorPonto(campo.value);
	if(campo.value != ''){	
		if(decimais.length > 0 && numeroComMascara.length == 0){
			numeroComMascara = 0;
		}
		
		//numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
		var decimaisComMascara = decimais;
		
		while(decimaisComMascara.length < quantidadeDecimais){
			decimaisComMascara += '0';
		}
		
		if(decimaisComMascara.length > quantidadeDecimais){
			decimaisComMascara = decimaisComMascara.substr(0,quantidadeDecimais);
		}
		numeroComMascara = numeroComMascara + ',' + decimaisComMascara;
	}
	
	campo.value = numeroComMascara;
}

/**
 * Função auxiliar que aplica a máscara de número inteiro a um valor inteiro.
 */
function aplicarMascaraParteInteira(numero){
	var numeroComMascara = '';
	var count = 0;
	for (var j=numero.length; j>=1; j--) {
		var dig = numero.substr(j-1,1);	
		if (count == 3){
			numeroComMascara = '.' + numeroComMascara;				
			count = 0;
		}
		numeroComMascara = dig + numeroComMascara;
		count++;				
	}	

	return numeroComMascara;
}

/**
 * Função auxiliar que aplica a máscara de número decimal a um valor decimal, considerando
 * a quantidade de casa informada.
 */
function aplicarMascaraParteDecimal(decimais, quantidadeDecimais){
	var decimaisComMascara = decimais;
	
	while(decimaisComMascara.length < quantidadeDecimais){
		decimaisComMascara += '0';
	}
	
	if(decimaisComMascara.length > quantidadeDecimais){
		decimaisComMascara = decimaisComMascara.substr(0, quantidadeDecimais);
	}

	return decimaisComMascara;
}

/**
 * Função responsável por verificar se uma função existe.
 * @param func O nome da função
 * @return True ou False
 */
function funcaoExiste(func) {
	if (typeof eval('window.'+ func) == 'function') {		
		return true;	
	} else {
		return false;
	}
}

/**
 * Função responsável por converter uma string em um float.
 * @param valor Valor a ser convertido.
 * @return O valor formatado como float.
 */
function converterStringParaFloat(valor) {
	if(valor != null && valor != "") {
		while(valor.indexOf(".") != -1) {
			valor = valor.replace(".","");
		}
		valor = valor.replace(",",".");
	}
	return parseFloat(valor);
}

/**
 * Função que aplica a máscara de número decimal a um valor decimal de uma variável, considerando
 * a quantidade casas decimais informada.
 */
function formatarValorDecimalVariavel(valor, quantidadeDecimais) {
	var numeroComMascara = '';
	
	var inteiros = obterParteInteira(valor);
	numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
	var decimais = obterParteDecimal(valor);
	if(valor != ''){	
		if(decimais.length > 0 && numeroComMascara.length == 0){
			numeroComMascara = 0;
		}
		numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
	}
	
	valor = numeroComMascara;
	return valor;
}

/**
 * Função que aplica a máscara de número decimal a um valor decimal separado por ponto e sem 
 * separação de milhar de uma variável, considerando a quantidade casas decimais informada.
 */
function formatarValorDecimalVariavelSeparadoPorPonto(valor, quantidadeDecimais) {
	var numeroComMascara = '';
	
	var inteiros = obterParteInteiraSeparadoPorPonto(valor);
	numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
	var decimais = obterParteDecimalSeparadoPorPonto(valor);
	if(valor != ''){	
		if(decimais.length > 0 && numeroComMascara.length == 0){
			numeroComMascara = 0;
		}
		numeroComMascara = numeroComMascara + ',' + aplicarMascaraParteDecimal(decimais, quantidadeDecimais);
	}
	
	valor = numeroComMascara;
	return valor;
}

/** 
* Função responsável por exibir o popup do jdialog mantendo o estado do scroll. 
* @param seletorDialog O seletor do dialog * @return
*/
function exibirJDialog(seletorDialog) {
	$(seletorDialog).parent().css({position:"relative"}).end().dialog('open');
}

function toUpperCase(campo) {
	if(campo != undefined && campo.value != undefined){
		campo.value = campo.value.toUpperCase();
	}
}

/** 
* Função responsável por retornar a posição top atual do scroll * @return A posição
*/

function f_scrollTop() {
	return f_filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}
/** 
* Função responsável por filtrar os dados resultantes da fução f_scrollTop 
* @param n_win * @param n_docel * @param n_body * @return
*/
function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

//INICIO - TKT #597

function validarCriteriosParaCampo(campo, criterioCaixaAlta, criterioChar, funcao){
	
	pos = getCaretPosition (campo);
		
	if (criterioCaixaAlta=="1"){
		campo.value = campo.value.toUpperCase();
	}
	if (criterioChar!="1"){
		campo.value = replaceSpecialChars(campo.value);
		campo.value = removerExtraEspacoInicialParametros(campo.value);
	}
	
	setCaretPosition(campo, pos);
}

/*
** Retorna a posição do cursor no campo texto
** param oField -- campo input
** return iCaretPos -- inteiro com a posição do cursor no campo
*/
function getCaretPosition (oField) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    oField.focus();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange();

    // Move selection start to 0 position
    oSel.moveStart('character', -oField.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (oField.selectionStart || oField.selectionStart == '0')
    iCaretPos = oField.selectionStart;

  // Return results
  return iCaretPos;
}

function setCaretPosition(elem, caretPos) {

    if(elem != null) {
        if(elem.createTextRange) {
            var range = elem.createTextRange();
            range.move('character', caretPos);
            range.select();
        }
        else {
            if(elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(caretPos, caretPos);
            }
            else
                elem.focus();
        }
    }
}

//Array de objectos de qual caracter deve substituir seu par com acentos 
var specialChars = [
{val:"a",let:"áàãâä"},
{val:"e",let:"éèêë"},
{val:"i",let:"íìîï"},
{val:"o",let:"óòõôö"},
{val:"u",let:"úùûü"},
{val:"c",let:"ç"},
{val:"A",let:"ÁÀÃÂÄ"},
{val:"E",let:"ÉÈÊË"},
{val:"I",let:"ÍÌÎÏ"},
{val:"O",let:"ÓÒÕÔÖ"},
{val:"U",let:"ÚÙÛÜ"},
{val:"C",let:"Ç"},
{val:"",let:"?!()"}
];

// Função para substituir caractesres especiais.
//@param {str} string
//@return String
function replaceSpecialChars(str) {
	var $spaceSymbol = ' ';
	var regex;
	var returnString = str;
	for (var i = 0; i < specialChars.length; i++) {
		regex = new RegExp("["+specialChars[i].let+"]", "g");
		returnString = returnString.replace(regex, specialChars[i].val);
		regex = null;
	}
	return returnString.replace(/\s/g,$spaceSymbol);
};

//FIM - TKT #597


//função para obter os parâmetros da URL

function obterParametroURL(parameter) {  
    var loc = location.search.substring(1, location.search.length);   
    var param_value = false;   
    var params = loc.split("&");   
    for (i=0; i<params.length;i++) {   
        param_name = params[i].substring(0,params[i].indexOf('='));   
        if (param_name == parameter) {                                          
            param_value = params[i].substring(params[i].indexOf('=')+1)   
        }   
    }   
    if (param_value) {   
        return param_value;   
    }   
    else {   
        return false;   
    }   
}

function primeiraLetraMaiuscula(campo)
{
    val = campo.value;
    newVal = val
        .toLowerCase()
        .replace(/[^A-Za-záâàãäéêèëíîìïóõòôöúùûüç][A-Za-záâàãäéêèëíîìïóõòôöúùûü]/g, function(m){
        	
	        return m.toUpperCase()
	        })
        .replace(/[0-9][A-Za-záâàãäéêèëíîìïóõòôöúùûü]/g, function(m){
	        return m.toUpperCase()})
        .replace(/( (da|das|e|de|do|dos|para|na|nas|no|nos|a|com|sem) )/gi, function(m){
	        return m.toLowerCase()})
        .replace(/^./, 
    	function(m){
        		 return m.toUpperCase()
	    })
   
    if (val != newVal)
    {
        campo.value = newVal;
    }
}

function verificaApenasLetras (campo) {
	
    if (!isNaN (campo.value)) {

    	campo.value = "";
    }

 }


function mascaraPercentual (campo){
	//campo = eval (objeto);

	separador = ',';
	conjunto1 = 3;
	if (campo.value.length == conjunto1){
	campo.value = campo.value + separador;}
}

function contains(array, valor) {
    var tam = array.length;
    var count = 0;
     
    while (count < tam) {
        if (array[count] == valor){
        	return true;
        	break;
        }
        count++;
    }
    return false;
}

function removerElementoLista(lista, indice) {
    lista.splice(indice, 1);
}

function moverElementoLista(lista, indiceInicial, indiceFinal) {
    var elemento = lista[indiceInicial];
    lista.splice(indiceInicial, 1);
    lista.splice(indiceFinal, 0, elemento);
}

/**
 * Verifica se apenas um item foi selecionado na lista sem exibir mensagem de alerta
 */
function verificarSelecaoApenasUmSemMensagem() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag != 1) {
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

function verificarSelecaoApenasUmSemMensagemGeral(chaves) {
	var flag = 0;
	var chavesSelecao = document.getElementsByName(chaves);
	
	if (chavesSelecao != undefined && chavesSelecao.length > 0) {
		var total = chavesSelecao.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(chavesSelecao[i].checked == true){
					flag++;
				}
			}
		} else {
			if(chavesSelecao.checked == true){
				flag++;
			}
		}
		
		if (flag != 1) {
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

//Função para aceitar apenas números.
function apenasNumeros() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/\D/g,"");    
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
}

//limpa todos os campos, inclusive abas. Padrões devem ser configurados.
function limparFormularios(ele) {
	
    tags = ele.getElementsByTagName('input');
    for(i = 0; i < tags.length; i++) {
        switch(tags[i].type) {
            case 'password':
            case 'text':
                tags[i].value = '';
                break;
            case 'checkbox':
            case 'radio':
                tags[i].checked = false;
                break;
        }
    }
   
    tags = ele.getElementsByTagName('select');
    for(i = 0; i < tags.length; i++) {
        if(tags[i].type == 'select-one') {
            tags[i].selectedIndex = 0;
        }
        else {
            for(j = 0; j < tags[i].options.length; j++) {
                tags[i].options[j].selected = -1;
            }
        }
    }

    tags = ele.getElementsByTagName('textarea');
    for(i = 0; i < tags.length; i++) {
        tags[i].value = '';
    }
   
}

/**
 * Verifica se algum item foi selecionado na lista sem exibir mensagem de alerta
 */
function verificarSelecaoSemMensagem() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag <= 0) {
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

//Função para impedir e remover caractesres especiais.
function removeExtra() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/[^0-9a-zA-Z]/g,"");       
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
}

//Função para impedir e remover caractesres especiais e aceita espaço.
function removeExtraManterEspaco() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/[^0-9a-zA-Z ]/g,"");    
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
}


//Função para aceitar apenas números.
function apenasNumeros() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/\D/g,"");    
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
    
//Remover espaço em branco do início do texto    
    function removerEspacoInicio(elem){

    	return elem.replace(/^\s{1}/,"");

    }
    
    function trim(str) {
    	 var initVal = $(this).val();
    	return initVal.replace(/^\s+|\s+$/g,"");
    }

  
    
}
//Recebe a máscara e transformar para JQuery aceitar.
function validarMascaraJQueryJS(str) {
    var initVal = $(str).val();
    outputVal = initVal.replace(/[0-9a-zA-Z]/g,"#");       
    if (initVal != outputVal) {
        $(str).val(outputVal);
    }
}

//Atribui a máscara um formato melhor aceitável na JSP.
function formatarMascarasJSP(mascara){
	
	var mask = mascara;
	var mascaraFormatada = '';
	for (i = 0; i < mask.length; i++) {
		if(mask.charAt(i) == '.') {
			mascaraFormatada += ".";
		} else if(mask.charAt(i) == '-') {
			mascaraFormatada += "-";
		} else if(mask.charAt(i) == '*') {
			mascaraFormatada += "*";
		} else {
			mascaraFormatada += "9";
		}
	}
	return mascaraFormatada;
}

//Valida o número inserido com a máscara, retornando o número sem máscara.
function validarNumeroMascara(mascara, numeroFormatado) {
 	
	//prepara variável pra validar se está totalmente preenchido.
	var tamanhoNumeroInseridoComFormatacao = replaceAll(numeroFormatado, '_', '').length;
	
	if(mascara.length > tamanhoNumeroInseridoComFormatacao){
	    var numero = false;		
	}else{		
		var numero = numeroFormatado.replace(/\D/g,"");
	}
	return numero;
}

function encode64(input) {
    input = escape(input);
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    do {
       chr1 = input.charCodeAt(i++);
       chr2 = input.charCodeAt(i++);
       chr3 = input.charCodeAt(i++);

       enc1 = chr1 >> 2;
       enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
       enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
       enc4 = chr3 & 63;

       if (isNaN(chr2)) {
          enc3 = enc4 = 64;
       } else if (isNaN(chr3)) {
          enc4 = 64;
       }

       output = output +
          keyStr.charAt(enc1) +
          keyStr.charAt(enc2) +
          keyStr.charAt(enc3) +
          keyStr.charAt(enc4);
       chr1 = chr2 = chr3 = "";
       enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);

    return output;
 }

function decode64(input) {
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
       alert("There were invalid base64 characters in the input text.\n" +
             "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
             "Expect errors in decoding.");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
       enc1 = keyStr.indexOf(input.charAt(i++));
       enc2 = keyStr.indexOf(input.charAt(i++));
       enc3 = keyStr.indexOf(input.charAt(i++));
       enc4 = keyStr.indexOf(input.charAt(i++));

       chr1 = (enc1 << 2) | (enc2 >> 4);
       chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
       chr3 = ((enc3 & 3) << 6) | enc4;

       output = output + String.fromCharCode(chr1);

       if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
       }
       if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
       }

       chr1 = chr2 = chr3 = "";
       enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);

    return unescape(output);
 }

function encodeArvoreHTML(text){
	var html = text;
	//substituindo "(" por código ascii para enviar para a action
	while(html.indexOf("(") != -1){
		html = html.replace("(", "#40");
	}
	
	//substituindo ")" por código ascii para enviar para a action
	while(html.indexOf(")") != -1){
		html = html.replace(")", "#41");
	}
	
	return encodeURIComponent(html);
}

function decodeArvoreHTML(text){
	var html = decodeURIComponent(text);
	//substituindo "(" por código ascii para enviar para a action
	while(html.indexOf("#40") != -1){
		html = html.replace("#40", "(");
	}
	
	//substituindo ")" por código ascii para enviar para a action
	while(html.indexOf("#41") != -1){
		html = html.replace("#41", ")");
	}	
	
	return html;
}


function validardataAtual(objeto){

	var valor = objeto.value;
	var dia  = parseInt(valor.substring(0,2),10);
	var mes  = parseInt(valor.substring(3,5),10);
	var ano  = parseInt(valor.substring(6,10),10);

	var dataAtual = new Date();
	var dataInformada = new Date(ano + "-" + mes + "-" + dia);

	if(dataInformada > dataAtual){
		objeto.value = '';
		alert('A data de solicitação do ramal informada é maior que a data atual.');
	}

}

function validaData(objeto) {
	
	var valor = objeto.value;
	var mValores = "312831303130313130313031"
	var retorno = false;
	var lastDate = 0

	if (valor == "") return true;
	if (valor == "__/__/____") return true;
	valor = valor.replace("___", "");
	valor = valor.replace("__", "");
	valor = valor.replace("_", "");
	if (valor.length < 10) retorno = false;
	if (valor.substr(6, 4) < 1800) {
		alert("Data Invalida.")
		objeto.value = '';
		objeto.focus();
		objeto.select();
		return false;
	}

    dia  = parseInt(valor.substring(0,2),10);		// pega o dia
	mes  = parseInt(valor.substring(3,5),10); 		// pega o mês
	ano  = parseInt(valor.substring(6,10),10);		// pega o ano
	

	
	if (mes == 2){
		if(ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0) { //ano bissexto 
			lastDate = 29;
		} else {
			lastDate = 28;
		}
	} else {
		lastDate = mValores.substring((mes-1)*2, (mes-1)*2+2)
	}
	
	if (valor.length < 8){
		retorno = false
	} else if ((valor.substring(2,3) != "/" ) || (valor.substring(5,6) != "/") ) {
		retorno = false
	} else if ( (isNaN(dia)) || (isNaN(mes)) || ( isNaN(ano)) ) {
		retorno = false
	} else if ( (mes > 12) || (mes <= 0) ){
		retorno = false
	} else if ( (dia > lastDate) || (dia <=0) ){
		retorno = false
	} else if (valor.substring(6,10) < 4){
		retorno = false
	} else {		
		retorno = true
	}

	if (!retorno){
		objeto.value = '';
		objeto.focus();
		objeto.select();
		alert("Data Invalida.")
	}

	return retorno;
	
}

// Verifica se dois campos de intervalo de data foram preenchidos corretamente
function validarDatas(dataInicial,dataFinal,botao){
	if($("#" + dataInicial).val() != "" && $("#" + dataInicial).val() != "__/__/____" && 
		/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + dataInicial).val()) &&
		$("#" + dataFinal).val() != "" && $("#" + dataFinal).val() != "__/__/____" && 
		/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/.test($("#" + dataFinal).val())){
		
		var partes = $("#" + dataInicial).val().split("/");
		var dataInicio = new Date(partes[2], partes[1] - 1, partes[0]);
		partes = $("#" + dataFinal).val().split("/");
		var dataFim = new Date(partes[2], partes[1] - 1, partes[0]);
		if (dataFim < dataInicio){
			return false;
		}else{
			return true;			
		}
	} else {
		return false;
	}
}

function formatarData(date) {
    var d = new Date(date || Date.now());
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();
    
    if (day.length < 2) {
    	day = '0' + day;
    }

    if (month.length < 2) {
    	month = '0' + month;
    }

    return [day, month, year].join('/');
}

function criarData(stringData) {
	var dataArray = stringData.split("/");
	var dia = dataArray[0];
	var mes = dataArray[1] - 1;
	var ano = dataArray[2];
	return new Date(ano, mes, dia);
}

function criarDataSemHora(stringData) {
	var dataArray = stringData.split("/");
	var dia = dataArray[0];
	var mes = dataArray[1] - 1;
	var ano = dataArray[2];
	return new Date(ano, mes, dia).setHours(0, 0, 0, 0);
}

function removerEspacoInicialFinal(valor){
	return valor.replace(/^\s+|\s+$/g, '');
}

function removerEspacoInicialFinalETruncar(valor,tamanhoMaximo){
	var valorSemEspaco = valor.replace(/^\s+|\s+$/g, '');
	return valorSemEspaco.substring(0,tamanhoMaximo);
}

// Remove espaços no início do texto.
function removeExtraEspacoInicial() {
    var initVal = $(this).val();
    outputVal = initVal.replace(/[^0-9a-zA-Z ]/g,"");
    outputVal = outputVal.replace(/^\s+/,"");
    if (initVal != outputVal) {
        $(this).val(outputVal);
    }
}

function removerExtraEspacoInicialParametros(texto){
	outputVal = texto.replace(/[^0-9a-zA-Z ]/g,"");
	outputVal = outputVal.replace(/^\s+/,"");
	 if (texto != outputVal) {
		 return outputVal;
	 }
	 return texto;
}

function removerEspacoInicioComLetraMaiuscula(elem){
	elem.value = elem.value.replace(/^\s{1}/,"");
	elem.value = elem.value.toUpperCase();
}

function mascaraNumeroDecimal(campo, posicaoVirgula, tamanhoMaximo){
	campo.value = campo.value.replace(/\D/g,"");   
	if (campo.value.length > posicaoVirgula){
		var s = "";
		s = campo.value.substr(0, posicaoVirgula);
		s = s.replace(',','');
		s = s.replace('.','');
		s += ",";
		campo.value = campo.value.replace(',','');
		campo.value = campo.value.replace('.','');
		s += campo.value.substr(posicaoVirgula);
		campo.value = s;
	}
	campo.value = campo.value.substr(0, tamanhoMaximo);
	if (campo.value.substr(0,1) == '0')
		campo.value = campo.value.substr(1, tamanhoMaximo);	
		
}

if(typeof String.prototype.trim !== 'function') {
	  String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g, ''); 
	  }
	}



function mascaraCpfCnpj(o,f){
    v_obj=o
    v_fun=f
    setTimeout('execmascara()',1)
}
 
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
 
function cpfCnpj(v){
 
    //Remove tudo o que não é dígito
    v=v.replace(/\D/g,"")
 
    if (v.length <= 11) { //CPF
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{3})(\d)/,"$1.$2")
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
        v=v.replace(/(\d{3})(\d)/,"$1.$2")
 
        //Coloca um hífen entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
 
    } else { //CNPJ
 
        //Coloca ponto entre o segundo e o terceiro dígitos
        v=v.replace(/^(\d{2})(\d)/,"$1.$2")
 
        //Coloca ponto entre o quinto e o sexto dígitos
        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
 
        //Coloca uma barra entre o oitavo e o nono dígitos
        v=v.replace(/\.(\d{3})(\d)/,".$1/$2")
 
        //Coloca um hífen depois do bloco de quatro dígitos
        v=v.replace(/(\d{4})(\d)/,"$1-$2")
 
    }
 
    return v
 
}

function listarChavesPrimariasSelecionadas() {
	var flag = 0;
	var form = document.forms[0];
	var listaChaves = new Array();
	
	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					listaChaves.push(form.chavesPrimarias[i].value)
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag <= 0) {
			alert ("Selecione um ou mais registros para realizar a operação!");
			return false;
		}
		
	} else {
		return false;
	}
	
	return listaChaves;
}

function validarAnoMes(idCampo) {
	var re = new RegExp("^(([0-9]{4})(/|-)?(0([1-9])|1[0-2]))$");
	return re.test($("#"+idCampo).val());
}

function corrigirPosicaoDatepicker() {
	$(".ui-datepicker-trigger").css({"float": "left", "margin-top": "6px"});
}

function stringVazia(str) {
	return (!str || 0 === str.length);
}

function arredondar2Digitos(valor) {
	return (Math.round(valor * 100) / 100);
}

function arredondarParaBaixo2Digitos(valor) {
	return (Math.floor(valor * 100) / 100);
}

function aplicarLocalePtBrNumero(valor) {
	return valor.toLocaleString("pt-BR", 
		{minimumFractionDigits : 2, 
			maximumFractionDigits : 2});
}

// FUNCOES GERAIS DE INSERIR E ATUALIZAR TARIFA COMECO

function verificarDadosTarifaFaixas(){
	var quantidadeLinhas = $('table#dadosFaixa tr:last').index();
	for (i = 0; i <= quantidadeLinhas; i++) {
		var parcelaTemValor = !stringVazia($("#valor_compra_parcela_vvsi_" + i).val())
			|| !stringVazia($("#margem_valor_agregado_parcela_vvsi_" + i).val());
		if(!stringVazia($("#valor_variavel_sem_imposto_" + i).val())){				
			setCamposDisabled($("#valor_variavel_sem_imposto_" + i), parcelaTemValor);
			setCamposDisabled($("[id$='parcela_vvsi_" + i + "']"), !parcelaTemValor);
		}
	}
}

function setCamposDisabled(elementos, disabled){
	if(disabled){
		elementos.attr("disabled", "disabled");
		elementos.removeAttr("style");
	} else {
		elementos.removeAttr("disabled");
	}
}

function desabilitarCampos() {
	for (var i = 0; i < arguments.length; i++) {
		$('#' + arguments[i])
			.addClass('campoDesabilitado')
			.attr('disabled', 'disabled');
	}
}

function habilitarCampos() {
	for (var i = 0; i < arguments.length; i++) {
		$('#' + arguments[i])
			.removeClass('campoDesabilitado')
			.removeAttr('disabled', 'disabled');
	}
}

function aplicarMascaraNumeroDecimalCalcularVvsi(campo, quantidadeDecimais, index){
	aplicarMascaraNumeroDecimal(campo, quantidadeDecimais);
	//calculo do valor variavel sem imposto
	var areBlank = true;
	var vvsi = 0;
	$("[id$='parcela_vvsi_" + index + "']").each(function () {
		var novoValor = $(this).val();
		if (!stringVazia(novoValor)){
			areBlank = false;
			vvsi += converterStringParaFloat(novoValor);
		}
	});
	
	var elementoSoma = $("[id='valor_variavel_sem_imposto_" + index + "']");
	if(!areBlank){
		vvsi = vvsi.toFixed(quantidadeDecimais); 
		elementoSoma.val(vvsi.toString().replace(/\./g, ","));
		aplicarMascaraNumeroDecimal(elementoSoma[0], quantidadeDecimais);
	} else {
		elementoSoma.val("");
	}
	setCamposDisabled(elementoSoma, !areBlank);
	
}

function aplicarMascaraNumeroDecimalVvsiFixo(campo, quantidadeDecimais, index){
	aplicarMascaraNumeroDecimal(campo, quantidadeDecimais);
	
	var elementosParcela = $("[id$='parcela_vvsi_" + index + "']");
	setCamposDisabled(elementosParcela, !stringVazia(campo.value));
}

function verificarDadosTarifaFaixasDesconto(){
	$("[id^=desconto_]").blur();
}

function aplicarMascaraNumeroDecimalCalcularVtad(campo, quantidadeDecimais, 
		idElementoValorSemImposto, idElementoDescontoSemImposto, 
		idElementoValorTarifaAplicadoDesconto, idSelectTipoDesconto){
	aplicarMascaraNumeroDecimal(campo, quantidadeDecimais);
	//calculo do valor da tarifa aplicados os descontos
	var valorVariavelSemImposto = converterStringParaFloat(
			$("#" + idElementoValorSemImposto).text().trim());
	var descontoVariavelSemImposto = 0;
	if(!stringVazia($("#" + idElementoDescontoSemImposto).val())){
		descontoVariavelSemImposto = converterStringParaFloat(
				$("#" + idElementoDescontoSemImposto).val());
	}
	
	var valorTarifaAplicadoDesconto = 0;
	if($("#" + idSelectTipoDesconto + " option:selected").text().trim()=="VALOR($)"){
		valorTarifaAplicadoDesconto = 
			valorVariavelSemImposto - descontoVariavelSemImposto;
	} else if($("#" + idSelectTipoDesconto + " option:selected").text().trim()=="PERCENTUAL(%)"){
		valorTarifaAplicadoDesconto = 
			valorVariavelSemImposto * ((100 - descontoVariavelSemImposto)/100);
	}
	
	var sinalValorTarifaAplicadoDesconto = "";
	if(valorTarifaAplicadoDesconto < 0){
		valorTarifaAplicadoDesconto *= -1;
		sinalValorTarifaAplicadoDesconto = "-";
	}
	var elementoValorTarifaAplicadoDesconto = $("#" + idElementoValorTarifaAplicadoDesconto);
	elementoValorTarifaAplicadoDesconto.val(valorTarifaAplicadoDesconto.toString().replace(/\./g, ","));
	aplicarMascaraNumeroDecimal(elementoValorTarifaAplicadoDesconto[0], quantidadeDecimais);
	var valorTarifaAplicadoDescontoFormatado = elementoValorTarifaAplicadoDesconto.val();
	elementoValorTarifaAplicadoDesconto.val(
			sinalValorTarifaAplicadoDesconto + valorTarifaAplicadoDescontoFormatado);
}

function definirCamposEscondidos(){
	var quantidadeFaixas = $('table#dadosFaixa tr:last').index();
	for (i=0; i <= quantidadeFaixas; i++){			
		definirCamposEscondidosPorLinha(i);
	}
}

function definirCamposEscondidosPorLinha(linha){
	definirCampoEscondido("valor_compra_parcela_vvsi", linha);
	definirCampoEscondido("margem_valor_agregado_parcela_vvsi", linha);
	definirCampoEscondido("valor_variavel_sem_imposto", linha);
}

function definirCampoEscondido(idCampo, linha){
	$("#" + idCampo + "_escondido_" + linha).val($("#" + idCampo + "_" + linha).val());
}

function formatarValoresTarifa(){
	$(".valorConteinerTarifa").each( function() {
		aplicarMascaraNumeroDecimal(this, getQtdCasasDecimais());;
	});
}

//replica valores de valor compra e margem valor agregado
function replicarValoresVcMva() {
	var valorReplicanteVc = $("#replicante_valor_compra").val();
	var valorReplicanteMva = $("#replicante_margem_valor_agregado").val()
	
	var camposVc = $("[id^=valor_compra_parcela_vvsi_]");
	var camposMva = $("[id^=margem_valor_agregado_parcela_vvsi_]");
	
	if (!stringVazia(valorReplicanteVc)) {
		aplicarValor(camposVc, camposMva, valorReplicanteVc);
	}
	if (!stringVazia(valorReplicanteMva)) {
		aplicarValor(camposMva, camposVc, valorReplicanteMva);
	}
	
}

function aplicarValor(camposColuna, camposOutraColuna, valor) {
	camposColuna.val(valor);
	camposColuna.removeAttr("disabled");
	camposOutraColuna.removeAttr("disabled");
	camposColuna.blur();
}

//FUNCOES GERAIS DE INSERIR E ATUALIZAR TARIFA FIM

//FUNCOES GERAIS DE GERAR BOLETO COMECO

function carregarDialogGerarBoleto() {
	$("#dialogGerarBoleto").dialog({
		autoOpen: false,
		width: 1000,
		modal: true,
		minHeight: 90, 
		resizable: false
	});
}
	
function exibirDialogGerarBoletoFatura() {
	exibirDialogGerarBoleto(false, 'gerarBoletoFatura', 
			'fatura', 'faturas');
}

function exibirDialogGerarBoletoNotaDebito() {
	exibirDialogGerarBoleto(true, 'gerarBoletoNotaDebito', 
			'nota', 'notas');
}

function exibirDialogGerarBoleto(verificarTipoDocumentoNotaDebito, 
		nomeMetodo, objetoSingular, objetoPlural) {
	
	var exibe = false;
	
	var chavesSelecionadas = new Array();
	var checkboxesSelecionados = $('input[name=chavesPrimarias]:checked');
	var verificadorTipoDocumentoNotaDebito = true;
	var verificadorSituacaoPagamentoPendenteOuParcialmenteQuitado = true;
	
	checkboxesSelecionados.each(function() {
		var codigoFatura = $(this).val();
		if(verificarTipoDocumentoNotaDebito){			
			var tipoDocumento =  $('#tipo_documento_' + codigoFatura).text().trim();
			if(tipoDocumento != "NOTA DE DEBITO") {
				verificadorTipoDocumentoNotaDebito = false;
			}
		}
		var situacaoPagamento =  $('#situacao_pagamento_' + codigoFatura).text().trim().toUpperCase();
		if(situacaoPagamento != "PENDENTE"
				&& situacaoPagamento != "PARCIALMENTE QUITADO") {
			verificadorSituacaoPagamentoPendenteOuParcialmenteQuitado = false;
		}
		chavesSelecionadas.push(codigoFatura);
	});
	
	if (chavesSelecionadas.length > 0 
			&& verificadorSituacaoPagamentoPendenteOuParcialmenteQuitado
			&& (!verificarTipoDocumentoNotaDebito 
					|| verificadorTipoDocumentoNotaDebito)) {
		
		var chavesFaturasNaoVencidas = new Array();
		var chavesFaturasVencidas = new Array();
	    AjaxService.obterListaFaturaDialogGerarBoleto( chavesSelecionadas, {
	        callback: function(listaFaturas) {
		       	answer = listaFaturas;
	       		$('#pontoConsumo tr:not(:first)').remove();
				var param = '';
				var cont = 0;
				$('#corpoListaFaturasPendentes').html('');
				
				for (key in listaFaturas) {
					var fatura = listaFaturas[cont];
					
					if ((cont + 2) % 2 == 0){
						param = "odd";
					} else {
						param = "even";
					}
					
					var numeroFatura = fatura['numeroFatura'];
					var descricaoFaturaPontoConsumo = fatura['descricaoFaturaPontoConsumo'];
					var dataEmissaoFatura = fatura['dataEmissaoFatura'];
					var dataVencimentoFatura = fatura['dataVencimentoFatura'];
					var faturaVencida = fatura['faturaVencida'];
					var valorDocumentoCobranca = fatura['valorDocumentoCobranca'];
					var valorRecebimento = fatura['valorRecebimento'];
					var valorConciliado = fatura['valorConciliado'];
					var saldo = fatura['saldo'];
					var documentoFiscal = fatura['documentoFiscal'];
					
					var inner = '';
					inner = inner + '<tr class=' + param + '>';
			       	inner = inner + '<td>' + numeroFatura + '</td>';
			       	inner = inner + '<td>' + descricaoFaturaPontoConsumo + '</td>';
			       	inner = inner + '<td>' + dataEmissaoFatura + '</td>';
			       	inner = inner + '<td id="data_vencimento_fatura_' + numeroFatura + '">' + dataVencimentoFatura + '</td>';
			       	inner = inner + '<td>' + valorDocumentoCobranca + '</td>';
			       	inner = inner + '<td>' + valorRecebimento + '</td>';
			       	inner = inner + '<td>' + valorConciliado + '</td>';
			       	inner = inner + '<td id="saldo_corrigido_fatura_' + numeroFatura + '">' + saldo + '</td>';
					inner = inner + '<td>';
					inner = inner + '<input class="campoData campoHorizontal"  type="text" id="dataNovaDataVencimento_' + numeroFatura + '" name="dataNovaDataVencimento_' + numeroFatura + '" maxlength="10" >';
					if (faturaVencida == "false"){
						chavesFaturasNaoVencidas.push(numeroFatura);
					} else {
						chavesFaturasVencidas.push(numeroFatura);
					}
					inner = inner + '</td>';
					inner = inner + '<td ><a href="javascript:' + nomeMetodo + '(' + numeroFatura + "," + documentoFiscal + ');">';
					inner = inner + '<img src="' + getPathIconeImpressora() + '"  alt="Imprimir 2ª Via" title="Imprimir 2ª Via" border="0" style="margin-top: 5px;" >';
					inner = inner + '</a></td>';
					
			       	inner = inner + '</tr>';
	
					$("#corpoListaFaturasPendentes").append(inner);
					cont++;
					exibe = true;
				}
			} , async: false}
		);
	    
		if (exibe){
			$("#dialogGerarBoleto").dialog('open');
			$("[id^=dataNovaDataVencimento_]").datepicker({
				changeYear: true, 
				showOn: 'button', 
				buttonImage: getPathIconeCalendario(), 
				buttonImageOnly: true, 
				buttonText: 'Exibir Calendário', 
				dateFormat: 'dd/mm/yy', 
				minDate: '+0d', 
				onSelect: function (stringData, objeto) {
					var codigoFatura = this.id.split("_")[1];
		   			atualizarSaldoCorrigido(stringData, codigoFatura);
		   	    }});
			for (var i = 0; i < chavesFaturasNaoVencidas.length; i++) {
				$("#dataNovaDataVencimento_" + chavesFaturasNaoVencidas[i])
					.datepicker( "option", "disabled", true );
			}
			var stringDataHoje = '';
			AjaxService
			.getDataHoje({callback: function(stringDataHojeServidor) {
				stringDataHoje = stringDataHojeServidor;
			} , async: false });
			for (var i = 0; i < chavesFaturasVencidas.length; i++) {
				$("#dataNovaDataVencimento_" + chavesFaturasVencidas[i])
					.datepicker("setDate", stringDataHoje)
					.attr('readonly','readonly');
			}
			corrigirPosicaoDatepicker();
		}
	
		$("#popupGerarBoleto").css({position:"relative"}).end().dialog('open');
	} else {
		if (verificarTipoDocumentoNotaDebito 
				&& !verificadorTipoDocumentoNotaDebito) {				
			alert('Alguma das notas escolhidas não é nota de débito.');
		} else {				
			if (!verificadorSituacaoPagamentoPendenteOuParcialmenteQuitado) {
				alert('Alguma das ' + objetoPlural + ' escolhidas não está pendente ou parcialmente quitada.');
			} else {
				alert('Escolha pelo menos uma ' + objetoSingular + ' para gerar o boleto.');
			}
		}
	}
	
}

function closeDialogGerarBoleto(){
	$("#dialogGerarBoleto").dialog('close');
}

function gerarBoletoFatura(idFatura,idDocumentoFiscal) {
	gerarFaturaPendente(idFatura,
			'faturaForm', 'imprimirGerarFaturaPendente');
}

function gerarBoletoNotaDebito(idFatura) {
	AjaxService
	.getDataHoje({callback: function(stringDataHoje) {
		
		var stringDataVencimento = $('#data_vencimento_fatura_' + idFatura).text();
		var dataVencimento = criarDataSemHora(stringDataVencimento);
		
		var dataHoje = criarDataSemHora(stringDataHoje);
		
		if (dataVencimento >= dataHoje) {
			
			document.getElementById('chavePrimaria').value = idFatura;
			document.getElementById('novaDataVencimento').value = '';
			submeter('notaDebitoCreditoForm','imprimirGerarFaturaPendente');
			
		} else {
			
			var stringDataNovaVencimento = $('#dataNovaDataVencimento_' + idFatura).val();
			var dataNovaVencimento = criarDataSemHora(stringDataNovaVencimento);
			
			if(dataNovaVencimento >= dataHoje){
				document.getElementById('chavePrimaria').value = idFatura;
				document.getElementById('novaDataVencimento').value = stringDataNovaVencimento;
				submeter('notaDebitoCreditoForm','imprimirGerarFaturaPendente');
			} else {
				alert('A data de vencimento informada é anterior a data de hoje.')
			}
		
		}
	} , async: false });
}

function gerarFaturaPendente(idFatura, 
	idForm, idActionPath) {
	
	AjaxService
	.getDataHoje({callback: function(stringDataHoje) {
		
		var stringDataVencimento = $('#data_vencimento_fatura_' + idFatura).text();
		var dataVencimento = criarDataSemHora(stringDataVencimento);
		
		var dataHoje = criarDataSemHora(stringDataHoje);
		
		if (dataVencimento >= dataHoje) {
			
			document.getElementById('chavePrimaria').value = idFatura;
			document.getElementById('novaDataVencimento').value = '';
			submeter(idForm, idActionPath);
			
		} else {
			
			var stringDataNovaVencimento = $('#dataNovaDataVencimento_' + idFatura).val();
			var dataNovaVencimento = criarDataSemHora(stringDataNovaVencimento);
			
			if(dataNovaVencimento >= dataHoje){
				document.getElementById('chavePrimaria').value = idFatura;
				document.getElementById('novaDataVencimento').value = stringDataNovaVencimento;
				submeter(idForm, idActionPath);
			} else {
				alert('A data de vencimento informada é anterior a data de hoje.')
			}
		
		}
	} , async: false });
	
}

function atualizarSaldoCorrigido(stringData, codigoFatura) {
	
	var chavesPrimariasFaturas = new Array();
	chavesPrimariasFaturas.push(codigoFatura);
			
	AjaxService.atualizarValoresCorrigidos( 
		chavesPrimariasFaturas, stringData, true, {
			callback: function(listaFaturas) {
				
				for (var i = 0; i < listaFaturas.length; i++) {
					var fatura = listaFaturas[i];
					
					var chavePrimaria = fatura['chavePrimaria'];
					var saldoCorrigido = fatura['saldoCorrigido'];
					
					$("#saldo_corrigido_fatura_" + chavePrimaria).text(saldoCorrigido);
					
				}
			} , async: false
		}
	);
	
}

//FUNCOES GERAIS DE GERAR BOLETO FIM

function paginacaoAjax(action, idDiv, event, object){
	event.preventDefault();
	idDiv = "#" + idDiv;
	var url = $(object).attr("href");
	url = action + "&" + url.substring(url.indexOf("?")+1,url.lenght);
	$(idDiv).load(url);
}

function exibirOpcoesExportacao(){
	exibirJDialog("#exportarRelatorio");
}

function configurarModalExportacao(opcoes){
	$("#exportarRelatorio").dialog(opcoes);
}

function scrollToElemento(mensagemElement) {
    $('html').animate({
        scrollTop: ($(mensagemElement).first().offset().top - $(mensagemElement).height())
    }, 500);
}

/**
 * Funções globais para manter compatibilidade com arquivos que não foram modificados para typescript
 * @param id
 * @param cfgs configurações adicionais / sobrescrita do default do Datatables
 */
function iniciarDatatable(id, cfgs) {
    $('.loading').hide();
    $(id).DataTable($.extend({
        "destroy" : true,
        "responsive": true,
        "paginate": true,
        "order": [],
        "stateSave": true,
        "lengthMenu": [[10, 25, 50, 100, 200, 300, 500, 1000, 2000, 2500], [10, 25, 50, 100, 200, 300, 500, 1000, 2000, 2500]],
        "searching": true,
        "processing": true,
        "serverSide": false,
        "deferRender": false,
        "columnDefs": [],
        "language": {
            "lengthMenu": "_MENU_ registros por página",
            "zeroRecords": "Nenhum registro foi encontrado",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "0 registros",
            "infoFiltered": "(Filtrado de _MAX_ registros totais)",
            "search": "Procurar:",
            "processing": "Processando...",
            "paginate": {
                "previous": "Anterior",
                "next": "Próximo"
            }
        }
    }, cfgs));

    $(id).css({'opacity': 1, 'display': 'table'});
}




function desabilitarAcaoSpinnerInputNumber() {
    $('form').on('focus', '.input-sem-spinner', function(e) {
        $(this).on('wheel', function(e) {
            e.preventDefault();
        });
    });

    $('form').on('blur', '.input-sem-spinner', function(e) {
        $(this).off('wheel');
    });

    $('form').on('keydown', '.input-sem-spinner', function(e) {
        if ( e.which == 38 || e.which == 40 )
            e.preventDefault();
    });
}


/**
 * Verifica se algum item foi selecionado na lista
 */
function verificarSelecaoItemLote() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimariasItem != undefined) {
		var total = form.chavesPrimariasItem.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimariasItem[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimariasItem.checked == true){
				flag++;
			}
		}
	
		if (flag != 1) {
			alert ("Selecione apenas um registro para realizar a opera\u00E7\u00E3o!");
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}



function validarHorario(horario) {
    var padrao = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(horario);

    return padrao;
  }
  
function verificarSelecaoSemMensagemGeral() {
	var flag = 0;
	var form = document.forms[0];

	if (form != undefined && form.chavesPrimarias != undefined) {
		var total = form.chavesPrimarias.length;
		if (total != undefined) {
			for (var i = 0; i< total; i++) {
				if(form.chavesPrimarias[i].checked == true){
					flag++;
				}
			}
		} else {
			if(form.chavesPrimarias.checked == true){
				flag++;
			}
		}
	
		if (flag <= 0) {
			return false;
		}
		
	} else {
		return false;
	}
	
	return true;
}

function aplicarMascaraNumeroDecimalPonto(campo, quantidadeDecimais) {
    var numeroComMascara = '';
    
    var inteiros = obterParteInteiraSeparadoPorPonto(campo.value);
    numeroComMascara = numeroComMascara + aplicarMascaraParteInteira(inteiros);
    
    var decimais = obterParteDecimalSeparadoPorPonto(campo.value);
    if (campo.value != '') {    
        if (decimais.length > 0 && numeroComMascara.length == 0) {
            numeroComMascara = '0';
        }
        
        var decimaisComMascara = decimais;
        
        while (decimaisComMascara.length < quantidadeDecimais) {
            decimaisComMascara += '0';
        }
        
        if (decimaisComMascara.length > quantidadeDecimais) {
            decimaisComMascara = decimaisComMascara.substr(0, quantidadeDecimais);
        }
        
        numeroComMascara = numeroComMascara + '.' + decimaisComMascara;
    }
    
    campo.value = numeroComMascara;
}
