Date.prototype.date = function(a) {
	if (m = /(\d{4})-(\d{1,2})-(\d{1,2})(?:\s(\d{1,2}):(\d{1,2}):(\d{1,2}))?/.exec(a)) {
		var f = ['', 'setFullYear', 'setMonth', 'setDate', 'setHours', 'setMinutes', 'setSeconds'], t = 0;

		for (var i=1; i < m.length; i++)
			if (m[i] && (t = parseInt(m[i][0] == '0' ? m[i][1] : m[i])))
				eval('this.'+f[i]+'('+ (i == 2 ? --t : t) +')');
	}

	return this;
}

Date.prototype.format = function(a) {
	var f = {
		d: ((t = this.getDate().toString()) && t.length == 1) ? "0" + t : t,
		j: this.getDate(),
		m: ((t = (this.getMonth() + 1).toString()) && t.length == 1) ? "0" + t : t,
		n: this.getMonth() + 1,
		Y: this.getFullYear(),
		y: this.getFullYear().toString().substring(2),
		H: ((t = this.getHours().toString()) && t.length == 1) ? "0" + t : t,
		G: this.getHours(),
		i: ((t = this.getMinutes().toString()) && t.length == 1) ? "0" + t : t,
		s: ((t = this.getSeconds().toString()) && t.length == 1) ? "0" + t : t,
		u: this.getMilliseconds()
	}, b = a;

	for (var i=0; i < a.length; i++)
		if ((t = a.substr(i, 1)) && f[t])
			b = b.replace(t, f[t]);

	return b;
}
Date.prototype.indicadorSolicitante = 'false';
Date.prototype.comentario = '';