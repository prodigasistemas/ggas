<%-- <%@ tag language="java" pageEncoding="ISO-8859-1"%> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="form" required="false" %>
<%@ attribute name="withRadio" required="true" description="boolean - define se ir� apresentar o radiobutton 'Pesquisa Im�vel'. Marque true para apresentar ou false para n�o apresentar" %>

<fieldset id="pesquisarImovel" class="colunaDir3">
	<c:if test="${withRadio == 'true'}">
		<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${map.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);">
		<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend>
	</c:if>
	<div class="pesquisarImovelFundo">
		<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p>
		<input name="idImovel" type="hidden" id="idImovel" value="${map.idImovel}">
		<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${map.nomeFantasiaImovel}">
		<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${map.matriculaImovel}">
		<input name="numeroImovel" type="hidden" id="numeroImovel" value="${map.numeroImovel}">
		<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${map.cidadeImovel}">
		<input name="condominio" type="hidden" id="condominio" value="${map.condominio}">
	
		<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br />
		<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label>
		<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${map.nomeFantasiaImovel}"><br />
		<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label>
		<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${map.matriculaImovel}"><br />
		<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label>
		<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${map.numeroImovel}"><br />
		<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label>
		<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${map.cidadeImovel}"><br />
		<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label>
		<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label>
		<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label>
	</div>
</fieldset>
<%-- <c:if test="${form!=''}"> --%>
<!-- 	<fieldset id="pesquisarImovel" class="colunaDir3"> -->
<%-- 		<input id="indicadorPesquisaImovel" class="campoRadio" type="radio" value="indicadorPesquisaImovel" name="indicadorPesquisa" <c:if test="${form.map.indicadorPesquisa eq 'indicadorPesquisaImovel'}">checked="checked"</c:if> onclick="desabilitarPesquisaOposta(this);"> --%>
<!-- 		<legend class="legendIndicadorPesquisa">Pesquisar Im�vel</legend> -->
<!-- 		<div class="pesquisarImovelFundo"> -->
<!-- 			<p class="orientacaoInterna">Clique em <span class="destaqueOrientacaoInicial">Pesquisar Imovel</span> para selecionar um Im�vel.</p> -->
<%-- 			<input name="idImovel" type="hidden" id="idImovel" value="${form.map.idImovel}"> --%>
<%-- 			<input name="nomeFantasiaImovel" type="hidden" id="nomeFantasiaImovel" value="${form.map.nomeFantasiaImovel}"> --%>
<%-- 			<input name="matriculaImovel" type="hidden" id="matriculaImovel" value="${form.map.matriculaImovel}"> --%>
<%-- 			<input name="numeroImovel" type="hidden" id="numeroImovel" value="${form.map.numeroImovel}"> --%>
<%-- 			<input name="cidadeImovel" type="hidden" id="cidadeImovel" value="${form.map.cidadeImovel}"> --%>
<%-- 			<input name="condominio" type="hidden" id="condominio" value="${form.map.condominio}"> --%>
		
<!-- 			<input name="Button" id="botaoPesquisarImovel" class="bottonRightCol" title="Pesquisar Imovel"  value="Pesquisar Imovel" onclick="exibirPopupPesquisaImovel();" type="button"><br /><br /> -->
<!-- 			<label class="rotulo" id="rotuloNomeFantasia" for="nomeImovelTexto">Descri��o:</label> -->
<%-- 			<input class="campoDesabilitado" type="text" id="nomeImovelTexto" name="nomeImovelTexto"  maxlength="50" size="37" disabled="disabled" value="${form.map.nomeFantasiaImovel}"><br /> --%>
<!-- 			<label class="rotulo" id="rotuloMatriculaTexto" for="matriculaImovelTexto">Matr�cula:</label> -->
<%-- 			<input class="campoDesabilitado" type="text" id="matriculaImovelTexto" name="matriculaImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${form.map.matriculaImovel}"><br /> --%>
<!-- 			<label class="rotulo" id="rotuloNumeroTexto" for="numeroImovelTexto">N�mero:</label> -->
<%-- 			<input class="campoDesabilitado" type="text" id="numeroImovelTexto" name="numeroImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${form.map.numeroImovel}"><br /> --%>
<!-- 			<label class="rotulo" id="rotuloCidadeTexto" for="cidadeImovelTexto">Cidade:</label> -->
<%-- 			<input class="campoDesabilitado" type="text" id="cidadeImovelTexto" name="cidadeImovelTexto"  maxlength="18" size="18" disabled="disabled" value="${form.map.cidadeImovel}"><br /> --%>
<!-- 			<label class="rotulo" id="rotuloindicadorCondominioAmbosTexto" for="indicadorCondominioAmbosImovelTexto">O im�vel � condom�nio:</label> -->
<%-- 			<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto1" name="indicadorCondominioImovelTexto" value="true" disabled="disabled" <c:if test="${form.map.condominio == 'true'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto1">Sim</label> --%>
<%-- 			<input class="campoRadio" type="radio" id="indicadorCondominioImovelTexto2" name="indicadorCondominioImovelTexto" value="false" disabled="disabled" <c:if test="${form.map.condominio == 'false'}">checked="checked"</c:if>><label class="rotuloRadio" for="indicadorImovelTexto2">N�o</label> --%>
<!-- 		</div> -->
<!-- 	</fieldset> -->
<%-- </c:if> --%>