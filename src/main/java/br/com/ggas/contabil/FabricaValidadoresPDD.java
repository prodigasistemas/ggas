/*
 * 
 */
package br.com.ggas.contabil;

import br.com.ggas.geral.exception.NegocioException;

/**
 * Esta fabrica cria validadores que implementam as regras da lei 13097/2015 -
 * SEÇÃO 3 ART 8º INCISO 7º.
 */
public interface FabricaValidadoresPDD {

	/**
	 * Cria um validador para conferência da regra semestral de pdd. O validador
	 * retornado é capaz de identificar quando uma fatura atende os seguintes
	 * critérios: 0 < valor total <=
	 * {@link br.com.ggas.util.Constantes#PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL
	 * PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL}, vencida há mais de seis meses,
	 * usando como referência a data presente no parâmetro
	 * {@link br.com.ggas.util.Constantes#PARAMETRO_REFERENCIA_CONTABIL
	 * PARAMETRO_REFERENCIA_CONTABIL}.
	 * 
	 * @return the validador pdd
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ValidadorPDD criarValidadorPDDSemestral() throws NegocioException;

	/**
	 * Cria um validador para conferência da regra anual de pdd. O validador
	 * retornado é capaz de identificar quando uma fatura atende os seguintes
	 * critérios:
	 * {@link br.com.ggas.util.Constantes#PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL
	 * PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL} < valor total <=
	 * {@link br.com.ggas.util.Constantes#PARAMETRO_PDD_VALOR_LIMITE_ANUAL
	 * PARAMETRO_PDD_VALOR_LIMITE_ANUAL}, vencida há mais de um ano, usando como
	 * referência a data presente no parâmetro
	 * {@link br.com.ggas.util.Constantes#PARAMETRO_REFERENCIA_CONTABIL
	 * PARAMETRO_REFERENCIA_CONTABIL}.
	 * 
	 * @return the validador pdd
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ValidadorPDD criarValidadorPDDAnual() throws NegocioException;

}
