/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Lançamento Contábil Sintético 
 *
 */
public interface LancamentoContabilSintetico extends EntidadeNegocio {

	String BEAN_ID_LANCAMENTO_CONTABIL_SINTETICO = "lancamentoContabilSintetico";

	String DATA_GERACAO = "LANC_CONTAB_SINTETICO_DATA_GERACAO";

	String DATA_CONTABIL = "LANC_CONTAB_SINTETICO_DATA_CONTABIL";

	String VALOR = "LANC_CONTAB_SINTETICO_VALOR";

	String EVENTO_COMERCIAL = "EVENTO_COMERCIAL";

	/**
	 * @return the dataGeracao
	 */
	Date getDataGeracao();

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	void setDataGeracao(Date dataGeracao);

	/**
	 * @return the dataContabil
	 */
	Date getDataContabil();

	/**
	 * @param dataContabil
	 *            the dataContabil to set
	 */
	void setDataContabil(Date dataContabil);

	/**
	 * @return the eventoComercial
	 */
	EventoComercial getEventoComercial();

	/**
	 * @param eventoComercial
	 *            the eventoComercial to set
	 */
	void setEventoComercial(EventoComercial eventoComercial);

	/**
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return the lancamentoItemContabil
	 */
	LancamentoItemContabil getLancamentoItemContabil();

	/**
	 * @param lancamentoItemContabil
	 *            the lancamentoItemContabil to
	 *            set
	 */
	void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil);

	/**
	 * @return the numeroCNPJ
	 */
	String getNumeroCNPJ();

	/**
	 * @param numeroCNPJ
	 *            the numeroCNPJ to set
	 */
	void setNumeroCNPJ(String numeroCNPJ);

	/**
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(BigDecimal valor);

	/**
	 * @return the
	 *         listaLancamentoContabilAnalitico
	 */
	Collection<LancamentoContabilAnalitico> getListaLancamentoContabilAnalitico();

	/**
	 * @param listaLancamentoContabilAnalitico
	 *            the
	 *            listaLancamentoContabilAnalitico
	 *            to set
	 */
	void setListaLancamentoContabilAnalitico(Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico);

	/**
	 * @return the tributo
	 */
	Tributo getTributo();

	/**
	 * @param tributo
	 *            the tributo to set
	 */
	void setTributo(Tributo tributo);

	/**
	 * @param contaBancaria - Set objeto conta bancaria.
	 */
	void setContaBancaria(ContaBancaria contaBancaria);

	/**
	 * @return ContaBancaria - Retorna Objeto Conta Bancaria.
	 */
	ContaBancaria getContaBancaria();

	/**
	 * @param contaContabilCredito - Set Conta contábil de credito.
	 */
	void setContaContabilCredito(ContaContabil contaContabilCredito);

	/**
	 * @return ContaContabil - Retorna objeto Conta Contábil de Credito.
	 */
	ContaContabil getContaContabilCredito();

	/**
	 * @param contaContabilDebito - Set Conta contábil de débito.
	 */
	void setContaContabilDebito(ContaContabil contaContabilDebito);

	/**
	 * @return ContaContabil - Retorna objeto Conta Contábil.
	 */
	ContaContabil getContaContabilDebito();

	/**
	 * @param historico - Set historico. 
	 */
	void setHistorico(String historico);

	/**
	 * @return String - Retorna o histórico.
	 */
	String getHistorico();

	/**
	 * @return Boolean - Retornar estado indicador integrado.
	 */
	boolean isIndicadorIntegrado();

	/**
	 * @param indicadorIntegrado - Set Indicador integrado.
	 */
	void setIndicadorIntegrado(boolean indicadorIntegrado);

	/**
	 * @return String - Retornar auxiliar de débito.
	 */
	String getContaAuxiliarDebito();

	/**
	 * @param contaAuxiliarDebito - Set Conta auxiliar de débito.
	 */
	void setContaAuxiliarDebito(String contaAuxiliarDebito);

	/**
	 * @return String - Retorna Conta Auxiliar de Credito.
	 */
	String getContaAuxiliarCredito();

	/**
	 * @param contaAuxiliarCredito - Set Conta auxiliar crédito.
	 */
	void setContaAuxiliarCredito(String contaAuxiliarCredito);

}
