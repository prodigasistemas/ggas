/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;

/**
 * Classe responsável pela representação da entidade LancamentoContabil
 *
 */
public class LancamentoContabilVO implements Serializable {

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final long serialVersionUID = 5944594533238717646L;

	private Date dataRealizacaoEvento;

	private EventoComercial eventoComercial;

	private Segmento segmento;

	private LancamentoItemContabil lancamentoItemContabil;

	private Tributo tributo;

	private String cnpj;

	private String contaAuxiliarDebito;

	private String contaAuxiliarCredito;

	private String historicoContabil;

	private ContaBancaria contaBancaria;

	private Collection<LancamentoContabilAnaliticoVO> listaLancamentosContabeisAnaliticos = new HashSet<>();

	private DadosAuditoria dadosAuditoria;

	private EventoComercialLancamento eventoComercialLancamento;

	/**
	 * @return the dataRealizacaoEvento
	 */
	public Date getDataRealizacaoEvento() {
		Date data = null;
		if (dataRealizacaoEvento != null) {
			data = (Date) dataRealizacaoEvento.clone();
		}
		return data;
	}

	/**
	 * @param dataRealizacaoEvento
	 *            the dataRealizacaoEvento to set
	 */
	public void setDataRealizacaoEvento(Date dataRealizacaoEvento) {
		if(dataRealizacaoEvento != null) {
			this.dataRealizacaoEvento = (Date) dataRealizacaoEvento.clone();
		} else {
			this.dataRealizacaoEvento = null;
		}
	}

	/**
	 * @return the eventoComercial
	 */
	public EventoComercial getEventoComercial() {

		return eventoComercial;
	}

	/**
	 * @param eventoComercial
	 *            the eventoComercial to set
	 */
	public void setEventoComercial(EventoComercial eventoComercial) {

		this.eventoComercial = eventoComercial;
	}

	/**
	 * @return the segmento
	 */
	public Segmento getSegmento() {

		return segmento;
	}

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/**
	 * @return the lancamentoItemContabil
	 */
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	/**
	 * @param lancamentoItemContabil
	 *            the lancamentoItemContabil to
	 *            set
	 */
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	/**
	 * @return the tributo
	 */
	public Tributo getTributo() {

		return tributo;
	}

	/**
	 * @param tributo
	 *            the tributo to set
	 */
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/**
	 * @return the cnpj
	 */
	public String getCnpj() {

		return cnpj;
	}

	/**
	 * @param cnpj
	 *            the cnpj to set
	 */
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	/**
	 * @return the
	 *         listaLancamentosContabeisAnaliticos
	 */
	public Collection<LancamentoContabilAnaliticoVO> getListaLancamentosContabeisAnaliticos() {

		return listaLancamentosContabeisAnaliticos;
	}

	/**
	 * @param listaLancamentosContabeisAnaliticos
	 *            the
	 *            listaLancamentosContabeisAnaliticos
	 *            to set
	 */
	public void setListaLancamentosContabeisAnaliticos(Collection<LancamentoContabilAnaliticoVO> listaLancamentosContabeisAnaliticos) {

		this.listaLancamentosContabeisAnaliticos = listaLancamentosContabeisAnaliticos;
	}

	/**
	 * @return the valorTotal
	 */
	public BigDecimal getValorTotal() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(this.listaLancamentosContabeisAnaliticos != null) {
			for (LancamentoContabilAnaliticoVO lcAnalitico : listaLancamentosContabeisAnaliticos) {
				retorno = retorno.add(lcAnalitico.getValor()).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
			}
		}
		return retorno;
	}

	/**
	 * @return the dadosAuditoria
	 */
	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}

	/**
	 * @param dadosAuditoria
	 *            teh dadosAuditoria to set
	 */
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}

	/**
	 * @param contaBancaria
	 */
	public void setContaBancaria(ContaBancaria contaBancaria) {

		this.contaBancaria = contaBancaria;
	}

	/**
	 * @return
	 */
	public ContaBancaria getContaBancaria() {

		return contaBancaria;
	}

	public String getHistoricoContabil() {

		return historicoContabil;
	}

	public void setHistoricoContabil(String historicoContabil) {

		this.historicoContabil = historicoContabil;
	}

	public String getContaAuxiliarDebito() {

		return contaAuxiliarDebito;
	}

	public void setContaAuxiliarDebito(String contaAuxiliarDebito) {

		this.contaAuxiliarDebito = contaAuxiliarDebito;
	}

	public String getContaAuxiliarCredito() {

		return contaAuxiliarCredito;
	}

	public void setContaAuxiliarCredito(String contaAuxiliarCredito) {

		this.contaAuxiliarCredito = contaAuxiliarCredito;
	}

	public EventoComercialLancamento getEventoComercialLancamento() {

		return eventoComercialLancamento;
	}

	public void setEventoComercialLancamento(EventoComercialLancamento eventoComercialLancamento) {

		this.eventoComercialLancamento = eventoComercialLancamento;
	}

}
