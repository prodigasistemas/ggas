/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/07/2013 11:28:31
 @author asoares
 */

package br.com.ggas.contabil;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ControladorContaContabil.
 */
public interface ControladorContaContabil extends ControladorNegocio {

	/** The bean id controlador conta contabil. */
	String BEAN_ID_CONTROLADOR_CONTA_CONTABIL = "controladorContaContabil";

	/** The bean id conta contabil. */
	String BEAN_ID_CONTA_CONTABIL = "contaContabil";

	/** The erro negocio valor diferente especificado. */
	String ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO = "ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO";

	/** The erro negocio interacoes existente. */
	String ERRO_NEGOCIO_INTERACOES_EXISTENTE = 
					"A Conta Contábil não pode ser removida, pois existe relações com outra(s) entidade(s) no sistema.";

	/**
	 * Listar conta contabil.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	Collection<ContaContabil> listarContaContabil(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Obter conta contabil.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the conta contabil
	 * @throws Exception
	 *             the exception
	 */
	public ContaContabil obterContaContabil(Map<String, Object> filtro);

	/**
	 * Gets the classe conta contabil.
	 *
	 * @return Class - Retorna classe Conta contabil.
	 */
	public Class<?> getClasseContaContabil();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#criar()
	 */
	@Override
	public EntidadeNegocio criar();

	/**
	 * Validar dados conta contabil.
	 * 
	 * @param contaContabil
	 *            the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosContaContabil(ContaContabil contaContabil) throws NegocioException;

	/**
	 * Conta contabil.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContaContabil contaContabil(long chavePrimaria) throws NegocioException;

	/**
	 * Obter conta contabil.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContaContabil obterContaContabil(long chavePrimaria) throws NegocioException;

	/**
	 * Atualizar conta contabil.
	 * 
	 * @param contaContabil
	 *            the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarContaContabil(ContaContabil contaContabil) throws NegocioException, ConcorrenciaException;

	/**
	 * Listar conta contabil.
	 * 
	 * @return the collection
	 */
	Collection<ContaContabil> listarContaContabil();

	/**
	 * Inserir conta contabil.
	 * 
	 * @param contaContabil
	 *            the conta contabil
	 * @return the long
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Long inserirContaContabil(ContaContabil contaContabil) throws GGASException;

}
