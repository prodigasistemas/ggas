
package br.com.ggas.contabil.impl;

import java.util.Map;

import br.com.ggas.contabil.ProvisaoDevDuvMotivoBaixa;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelo motivo de baixa.
 */
public class ProvisaoDevDuvMotivoBaixaImpl extends EntidadeNegocioImpl implements ProvisaoDevDuvMotivoBaixa {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String descricao;

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
