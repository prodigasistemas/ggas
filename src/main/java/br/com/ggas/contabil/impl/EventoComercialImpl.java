/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 *
 */
class EventoComercialImpl extends EntidadeNegocioImpl implements EventoComercial {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final String DESCRICAO_VALOR_M3 = "m3";

	/**
	 * 
	 */
	private static final long serialVersionUID = 3531961234356691933L;

	private String descricao;

	private Modulo modulo;

	private Tabela tabelaReferencia;

	private String indicadorComplemento;

	private String descricaoValor;

	private Collection<EventoComercialLancamentoImpl> eventoComercialLancamentoList = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.lancamentocontabil.EventoComercial
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.lancamentocontabil.EventoComercial
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.EventoComercial#getModulo
	 * ()
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.EventoComercial#setModulo
	 * (br.com.ggas.controleacesso.Modulo)
	 */
	@Override
	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.lancamentocontabil.EventoComercial
	 * #getClasseReferencia()
	 */
	@Override
	public Tabela getTabelaReferencia() {

		return tabelaReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.lancamentocontabil.EventoComercial
	 * #setClasseReferencia(java.lang.String)
	 */
	@Override
	public void setTabelaReferencia(Tabela tabelaReferencia) {

		this.tabelaReferencia = tabelaReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(modulo == null) {
			stringBuilder.append(MODULO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tabelaReferencia == null) {
			stringBuilder.append(TABELA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	@Override
	public void setIndicadorComplemento(String indicadorComplemento) {

		this.indicadorComplemento = indicadorComplemento;
	}

	@Override
	public String getIndicadorComplemento() {

		return indicadorComplemento;
	}

	@Override
	public void setDescricaoValor(String descricaoValor) {

		this.descricaoValor = descricaoValor;
	}

	@Override
	public String getDescricaoValor() {

		return descricaoValor;
	}

	public Collection<EventoComercialLancamentoImpl> getEventoComercialLancamentoList() {

		return eventoComercialLancamentoList;
	}

	public void setEventoComercialLancamentoList(Collection<EventoComercialLancamentoImpl> eventoComercialLancamentoList) {

		this.eventoComercialLancamentoList = eventoComercialLancamentoList;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj instanceof EventoComercial) {
			return ((EventoComercial) obj).getChavePrimaria() == getChavePrimaria();
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#hashCode()
	 */
	@Override
	public int hashCode() {

		return (int) getChavePrimaria();
	}

	@Override
	public boolean isDescricaoValorM3() {
		return StringUtils.equals(descricaoValor, DESCRICAO_VALOR_M3);
	}
	

}
