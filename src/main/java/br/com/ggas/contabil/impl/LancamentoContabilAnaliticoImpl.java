/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.contabil.LancamentoContabilAnalitico;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pela implementação dos métodos relacionados ao Lançamento Analítico 
 *
 */
public class LancamentoContabilAnaliticoImpl extends EntidadeNegocioImpl implements LancamentoContabilAnalitico {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 3421607336298855932L;

	private LancamentoContabilSintetico lancamentoContabilSintetico;

	private BigDecimal valor;

	private Long codigoObjeto;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico
	 * #getLancamentoContabilSintetico()
	 */
	@Override
	public LancamentoContabilSintetico getLancamentoContabilSintetico() {

		return lancamentoContabilSintetico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico
	 * #setLancamentoContabilSintetico
	 * (br.com.ggas.
	 * contabil.LancamentoContabilSintetico)
	 */
	@Override
	public void setLancamentoContabilSintetico(LancamentoContabilSintetico lancamentoContabilSintetico) {

		this.lancamentoContabilSintetico = lancamentoContabilSintetico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico#getValor()
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico
	 * #setValor(java.math.BigDecimal)
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico
	 * #getCodigoObjeto()
	 */
	@Override
	public Long getCodigoObjeto() {

		return codigoObjeto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilAnalitico
	 * #setCodigoObjeto(java.lang.Long)
	 */
	@Override
	public void setCodigoObjeto(Long codigoObjeto) {

		this.codigoObjeto = codigoObjeto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(lancamentoContabilSintetico == null) {
			stringBuilder.append(LANC_CONTAB_SINTETICO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(codigoObjeto == null) {
			stringBuilder.append(CODIGO_OBJETO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valor == null) {
			stringBuilder.append(VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
