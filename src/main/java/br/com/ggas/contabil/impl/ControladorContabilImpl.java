/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil.impl;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import com.google.common.collect.ImmutableSet;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.contabil.FabricaValidadoresPDD;
import br.com.ggas.contabil.LancamentoContabilAnalitico;
import br.com.ggas.contabil.LancamentoContabilAnaliticoVO;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.contabil.LancamentoContabilSinteticoVO;
import br.com.ggas.contabil.LancamentoContabilVO;
import br.com.ggas.contabil.ProvisaoDevDuvMotivoBaixa;
import br.com.ggas.contabil.ProvisaoDevedoresDuvidosos;
import br.com.ggas.contabil.ReclassificadorFaturas;
import br.com.ggas.contabil.ValidadorPDD;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaTributacao;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.LogProcessosUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe responsável por implementar o Controlador Contábil 
 *
 */
public class ControladorContabilImpl extends ControladorNegocioImpl implements ControladorContabil {

	private static final String CONTA_BANCARIA = "contaBancaria";

	private static final String INCLUIR_DEBITO_A_REALIZAR = "INCLUIR_DEBITO_A_REALIZAR_";

	private static final String INCLUIR_CREDITO_A_REALIZAR = "INCLUIR_CREDITO_A_REALIZAR_";

	private static final String CANCELAR_DEBITO_A_REALIZAR = "CANCELAR_DEBITO_A_REALIZAR_";

	private static final String LONGO_PRAZO = "LONGO_PRAZO";

	private static final String CURTO_PRAZO = "CURTO_PRAZO";

	private static final String TRANSFERENCIA_CREDITO = "TRANSFERENCIA_CREDITO_";

	private static final String CANCELAR_CREDITO_A_REALIZAR = "CANCELAR_CREDITO_A_REALIZAR_";

	private static final String LONGO_PARA_CURTO_PRAZO = "LONGO_PARA_CURTO_PRAZO";

	private static final String SERVICO = "SERVICO_";

	private static final String PRODUTO = "PRODUTO_";

	private static final String _LONGO_PRAZO = "_LONGO_PRAZO";

	private static final String _CURTO_PRAZO = "_CURTO_PRAZO";

	private static final String _A_REALIZAR = "_A_REALIZAR";

	private static final String _SERVICO = "_SERVICO";

	private static final String _PRODUTO = "_PRODUTO";

	private static final String _DEBITO = "_DEBITO";

	private static final String _CREDITO = "_CREDITO";

	private static final String ID_MODULO = "idModulo";

	private static final String ID_CREDITO1 = "idCredito1";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ID_SEGMENTO1 = "idSegmento1";

	private static final String ID_TRIBUTO1 = "idTributo1";

	private static final String ID_DEBITO1 = "idDebito1";

	private static final String EVENTO_COMERCIAL = "eventoComercial";

	private static final String DATA_GERACAO = "dataGeracao";

	private static final String TRIBUTO = "tributo";

	private static final String LANCAMENTO_ITEM_CONTABIL = "lancamentoItemContabil";

	private static final String EVENTO_COMERCIAL_DESCRICAO = "eventoComercialDescricao";

	private static final String DESCRICAO_CONTA_AUXILIAR_CREDITO = "descricaoContaAuxiliarCredito";

	private static final String DESCRICAO_HISTORICO = "descricaoHistorico";

	private static final String DESCRICAO_CONTA_AUXILIAR_DEBITO = "descricaoContaAuxiliarDebito";

	private static final String INDICADOR_INTEGRADO = "indicadorIntegrado";

	private static final String DATA_FIM = "Data Fim";

	private static final String DATA_CONTABIL = "dataContabil";

	private static final String SEGMENTO = "segmento";

	private static final String COLECAO_PAGINADA = "colecaoPaginada";

	private static final String HABILITADO = "habilitado";

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final String ID_CONTA_BANCARIA = "idContaBancaria";

	private static final int BASE_DIAS_MES = 30;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int ESCALA = 30;

	private static final int LIMITE_SEGUNDOS = 59;

	private static final int LIMITE_MINUTOS = 59;

	private static final int LIMITE_HORAS = 23;

	private StringBuilder logProcessamento;
	
	private Map<Long, EventoComercial> cacheEventoComercial = new HashMap<>();

	private static final Logger LOG = Logger.getLogger(ControladorContabilImpl.class);

	private static final String FILTRO_DATA_INTERVALO_CONTABIL_INICIAL = "dataIntervaloContabilInicial";

	private static final String FILTRO_DATA_INTERVALO_CONTABIL_FINAL = "dataIntervaloContabilFinal";

	private static final String FILTRO_ID_MODULO = ID_MODULO;

	private static final String FILTRO_ID_EVENTO_COMERCIAL = "idEventoComercial";

	private static final String FILTRO_ID_LANCAMENTO_CONTABIL_ITEM = "idLancamentoItemContabil";

	private static final String FILTRO_ID_SEGMENTO = ID_SEGMENTO;

	private static final String FILTRO_CHAVE_PRIMARIA = "chavePrimaria";
	
	private static final String CONTA_AUXILIAR_DEBITO = "contaAuxiliarDebito";
	
	private static final String CONTA_AUXILIAR_CREDITO = "contaAuxiliarCredito";
	
	private static final String ARQUIVO_PROPRIEDADES_EVENTO_COMERCIAL = "eventoComercial.properties";

	private static Map<String, String> mapaCodigoEventoComercial;

	private static final Class<?> CLASSE_ITEM_FATURA = ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);

	private static final Class<?> CLASSE_CREDITO_DEBITO = ServiceLocator.getInstancia().getClassPorID(
					CreditoDebitoNegociado.BEAN_ID_CREDITO_DEBITO_NEGOCIADO);
	

	static {
		inicializarPropriedades();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return criarLancamentoContabilSintetico();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return getClasseEntidadeLancamentoContabilSintetico();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #criarLancamentoContabilSintetico()
	 */
	@Override
	public LancamentoContabilSintetico criarLancamentoContabilSintetico() {

		return (LancamentoContabilSintetico) ServiceLocator.getInstancia().getBeanPorID(
						LancamentoContabilSinteticoImpl.BEAN_ID_LANCAMENTO_CONTABIL_SINTETICO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#criarProvisaoDevedoresDuvidosos()
	 */
	@Override
	public ProvisaoDevedoresDuvidosos criarProvisaoDevedoresDuvidosos() {

		return (ProvisaoDevedoresDuvidosos) ServiceLocator.getInstancia().getBeanPorID(
						ProvisaoDevedoresDuvidososImpl.BEAN_ID_PROVISAO_DEVEDORES_DUVIDOSOS);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #criarLancamentoContabilAnalitico()
	 */
	@Override
	public LancamentoContabilAnalitico criarLancamentoContabilAnalitico() {

		return (LancamentoContabilAnalitico) ServiceLocator.getInstancia().getBeanPorID(
						LancamentoContabilAnaliticoImpl.BEAN_ID_LANCAMENTO_CONTABIL_ANALITICO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #criarEventoComercial()
	 */
	@Override
	public EventoComercial criarEventoComercial() {

		return (EventoComercial) ServiceLocator.getInstancia().getBeanPorID(EventoComercial.BEAN_ID_EVENTO_COMERCIAL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #criarEventoComercialLancamento()
	 */
	@Override
	public EventoComercialLancamento criarEventoComercialLancamento() {

		return (EventoComercialLancamento) ServiceLocator.getInstancia().getBeanPorID(
						EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #getClasseEntidadeLancamentoContabilSintetico
	 * ()
	 */
	@Override
	public Class<?> getClasseEntidadeLancamentoContabilSintetico() {

		return ServiceLocator.getInstancia().getClassPorID(LancamentoContabilSintetico.BEAN_ID_LANCAMENTO_CONTABIL_SINTETICO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #getClasseEntidadeLancamentoContabilAnalitico
	 * ()
	 */
	@Override
	public Class<?> getClasseEntidadeLancamentoContabilAnalitico() {

		return ServiceLocator.getInstancia().getClassPorID(LancamentoContabilAnalitico.BEAN_ID_LANCAMENTO_CONTABIL_ANALITICO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #getClasseEntidadeEventoComercial()
	 */
	@Override
	public Class<?> getClasseEntidadeEventoComercial() {

		return ServiceLocator.getInstancia().getClassPorID(EventoComercial.BEAN_ID_EVENTO_COMERCIAL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #getClasseEntidadeEventoComercialLancamento
	 * ()
	 */
	@Override
	public Class<?> getClasseEntidadeEventoComercialLancamento() {

		return ServiceLocator.getInstancia().getClassPorID(EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #getClasseEntidadeContaContabil()
	 */
	public Class<?> getClasseEntidadeContaContabil() {

		return ServiceLocator.getInstancia().getClassPorID(ContaContabil.BEAN_ID_CONTA_CONTABIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#getClasseEntidadeProvisaoDevedoresDuvidosos()
	 */
	@Override
	public Class<?> getClasseEntidadeProvisaoDevedoresDuvidosos() {

		return ServiceLocator.getInstancia().getClassPorID(ProvisaoDevedoresDuvidosos.BEAN_ID_PROVISAO_DEVEDORES_DUVIDOSOS);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#getClasseEntidadeProvisaoDevDuvMotivoBaixa()
	 */
	@Override
	public Class<?> getClasseEntidadeProvisaoDevDuvMotivoBaixa() {

		return ServiceLocator.getInstancia().getClassPorID(ProvisaoDevDuvMotivoBaixa.BEAN_ID_PROVISAO_DEV_DUV_MOTIVO_BAIXA);

	}

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	public Class<?> getClasseEntidadeModulo() {

		return ServiceLocator.getInstancia().getClassPorID(Modulo.BEAN_ID_MODULO);
	}

	public Class<?> getClasseEntidadeRecebimento() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	public Class<?> getClasseEntidadeFaturaGeral() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaGeral.BEAN_ID_FATURA_GERAL);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadeLancamentoItemContabil() {

		return ServiceLocator.getInstancia().getClassPorID(LancamentoItemContabil.BEAN_ID_LANCAMENTO_ITEM_CONTABIL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterLancamentoContabilSintetico(long,
	 * java.lang.String[])
	 */
	@Override
	public LancamentoContabilSintetico obterLancamentoContabilSintetico(long chavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		return (LancamentoContabilSintetico) super.obter(chavePrimaria, getClasseEntidadeLancamentoContabilSintetico(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#obterContaContabil(long)
	 */
	@Override
	public ContaContabil obterContaContabil(long chavePrimaria) throws NegocioException {

		return (ContaContabil) super.obter(chavePrimaria, getClasseEntidadeContaContabil());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterLancamentoContabilAnalitico(long,
	 * java.lang.String[])
	 */
	@Override
	public LancamentoContabilAnalitico obterLancamentoContabilAnalitico(long chavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		return (LancamentoContabilAnalitico) super.obter(chavePrimaria, getClasseEntidadeLancamentoContabilAnalitico(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterEventoComercial(long,
	 * java.lang.String[])
	 */
	@Override
	public EventoComercial obterEventoComercial(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		EventoComercial retorno = null;
		if (chavePrimaria != null) {
			if(getCacheEventoComercial().containsKey(chavePrimaria)) {
				
				retorno = getCacheEventoComercial().get(chavePrimaria);
			}else{
			
				retorno = (EventoComercial) super.obter(chavePrimaria, getClasseEntidadeEventoComercial(), propriedadesLazy);
				getCacheEventoComercial().put(chavePrimaria, retorno);
			}
		}
		return retorno;
	}

	/**
	 * Obter evento comercial lancamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the evento comercial lancamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterEventoComercialLancamento(long,
	 * java.lang.String[])
	 */
	public EventoComercialLancamento obterEventoComercialLancamento(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		EventoComercialLancamento retorno = null;
		if (chavePrimaria != null) {
			retorno = (EventoComercialLancamento) super
							.obter(chavePrimaria, getClasseEntidadeEventoComercialLancamento(), propriedadesLazy);
		}
		return retorno;
	}

	/**
	 * Obter evento comercial lancamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the evento comercial lancamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private EventoComercialLancamento obterEventoComercialLancamento(Long chavePrimaria) throws NegocioException {

		EventoComercialLancamento retorno = null;
		if (chavePrimaria != null) {
			retorno = (EventoComercialLancamento) super.obter(chavePrimaria, getClasseEntidadeEventoComercialLancamento());
		}
		return retorno;
	}

	/**
	 * Obter conta contabil.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterContaContabil(long,
	 * java.lang.String[])
	 */
	public ContaContabil obterContaContabil(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		ContaContabil retorno = null;
		if (chavePrimaria != null) {
			retorno = (ContaContabil) super.obter(chavePrimaria, getClasseEntidadeContaContabil(), propriedadesLazy);
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#listarContaContabil()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ContaContabil> listarContaContabil() throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeContaContabil().getSimpleName());
		hql.append(" order by numeroConta asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		Collection<ContaContabil> listaContaContabil = new ArrayList<>();
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		String mascara = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_CONTABILIDADE_MASCARA_NUMERO_CONTA);
		for (ContaContabil contaContabil : (Collection<ContaContabil>) query.list()) {
			contaContabil.setNumeroConta(Util.formatarMascaraTruncada(mascara, contaContabil.getNumeroConta()));
			listaContaContabil.add(contaContabil);
		}
		return listaContaContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterChaveEventoComercial
	 * (java.lang.String)
	 */
	@Override
	public Long obterChaveEventoComercial(String codigoEventoComercial) {

		String chave = mapaCodigoEventoComercial.get(codigoEventoComercial);

		if (chave == null) {
			return null;
		} else {
			return Long.valueOf(chave);
		}
 		
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#obterProvisaoDevDuvMotivoBaixa(java.lang.Long, java.lang.String[])
	 */
	@Override
	public ProvisaoDevDuvMotivoBaixa obterProvisaoDevDuvMotivoBaixa(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (ProvisaoDevDuvMotivoBaixa) super.obter(chavePrimaria, this.getClasseEntidadeProvisaoDevDuvMotivoBaixa(), propriedadesLazy);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #inserirLancamentoContabilSintetico
	 * (br.com.ggas.contabil.LancamentoContabilVO)
	 */
	@Override
	public void inserirLancamentoContabilSintetico(LancamentoContabilVO lancamentoContabilVO) throws NegocioException,
					ConcorrenciaException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		
		boolean indicadorContabilizacaoLancamentos = controladorParametroSistema.obterIndicadorContabilizacaoLancamentoContabil();
		
		if (possuiIndicadorRegimeContabilCompetencia(lancamentoContabilVO) && indicadorContabilizacaoLancamentos) {

			// O Sistema obtém a data corrente
			Date dataAtual = Calendar.getInstance().getTime();

			DateTime dataAtualdt = new DateTime(dataAtual);
			dataAtualdt = Util.zerarHorario(dataAtualdt);
			dataAtual = dataAtualdt.toDate();

			Date dataContabil = null;
			if (lancamentoContabilVO != null) {
				if (lancamentoContabilVO.getDataRealizacaoEvento() == null) {
					dataContabil = dataAtual;
				} else {
					dataContabil = lancamentoContabilVO.getDataRealizacaoEvento();
				}
			}

			DateTime dataContabildt = new DateTime(dataContabil);
			dataContabildt = Util.zerarHorario(dataContabildt);
			dataContabil = dataContabildt.toDate();

			if (lancamentoContabilVO != null && lancamentoContabilVO.getValorTotal() != null
					&& lancamentoContabilVO.getValorTotal().compareTo(new BigDecimal(0)) > 0) {
				Map<String, Object> mapaParametros = montarFiltroPesquisaAtributosUnicos(lancamentoContabilVO, dataAtual, dataContabil);

				Collection<LancamentoContabilSintetico> colecaoLCS = this.consultarLancamentoContabilSinteticoInserir(mapaParametros);
				if (!colecaoLCS.isEmpty()) {
					// O subfluxo Acumula
					// Contabilização é iniciado
					if (colecaoLCS.size() == 1) {
						this.atualizarContabilizacao(colecaoLCS.iterator().next(), lancamentoContabilVO);
					} else {
						throw new NegocioException(
										"Não foi possível inserir o Lançamento Contábil pois existem mais de um Lançamentos com atributos idênticos.");
					}
				} else {
					// O subfluxo inclui
					// contabilização é iniciado
					LancamentoContabilSintetico lancamentoContabilSintetico = this.montarLancamentoContabilSintetico(lancamentoContabilVO);
					lancamentoContabilSintetico.setDataGeracao(dataAtual);
					lancamentoContabilSintetico.setDataContabil(dataContabil);
					lancamentoContabilSintetico.setDadosAuditoria(lancamentoContabilVO.getDadosAuditoria());
					lancamentoContabilSintetico.setIndicadorIntegrado(Boolean.FALSE);
					this.inserir(lancamentoContabilSintetico);
				}
			}
		}

	}
	
	/**
	 * Insere ou atualiza a contabilização de um
	 * {@link LancamentoContabilSintetico}.
	 * 
	 * @param lancamentosContabeis
	 *            - {@link Map}
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@Override
	public void contabilizarLancamentoContabilSintetico(
			Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas, LancamentoContabilVO lancamentoContabilVO)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		
		boolean indicadorContabilizacaoLancamentos = controladorParametroSistema.obterIndicadorContabilizacaoLancamentoContabil();
		
		if (possuiIndicadorRegimeContabilCompetencia(lancamentoContabilVO) && indicadorContabilizacaoLancamentos) {

			Date dataAtual = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());

			Date dataContabil = dataAtual;

			if (lancamentoContabilVO != null && lancamentoContabilVO.getDataRealizacaoEvento() != null) {
				dataContabil = DataUtil.gerarDataHmsZerados(lancamentoContabilVO.getDataRealizacaoEvento());
			}

			if (lancamentoContabilVO != null && NumeroUtil.maiorQueZero(lancamentoContabilVO.getValorTotal())) {

				Map<String, Object> mapaParametros = montarFiltroPesquisaAtributosUnicos(lancamentoContabilVO,
						dataAtual, dataContabil);

				LancamentoContabilSintetico lancamentoContabilSintetico = this
						.consultarLancamentoContabilSintetico(contabilizacoesAcumuladas, mapaParametros);

				if (lancamentoContabilSintetico != null) {
					getSession().evict(lancamentoContabilSintetico);
					this.acumularContabilizacao(lancamentoContabilSintetico, lancamentoContabilVO);
				} else {
					lancamentoContabilSintetico = this.montarLancamentoContabilSintetico(lancamentoContabilVO,
							dataAtual, dataContabil);
					this.inserir(lancamentoContabilSintetico);
				}
				contabilizacoesAcumuladas.put(lancamentoContabilSintetico.getChavePrimaria(),
						lancamentoContabilSintetico);

			}
		}
	}
	
	/**
	 * Método responsável por consultar uma coleção de {@link LancamentoContabilSintetico}, retornando o primeiro, caso exista apenas um.
	 * Caso o {@link LancamentoContabilSintetico} já exista no mapa de lançamentos por chave primária, passado por parâmetro, retorna a
	 * entidade existente no mapa.
	 * 
	 * @param lancamentosContabeis - {@link Map}
	 * @param mapaParametros - {@link Map}
	 * @return {@link LancamentoContabilSintetico}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private LancamentoContabilSintetico consultarLancamentoContabilSintetico(Map<Long, LancamentoContabilSintetico> lancamentosContabeis,
			Map<String, Object> mapaParametros) throws NegocioException {

		LancamentoContabilSintetico lancamentoContabilSintetico = null;

		Collection<LancamentoContabilSintetico> colecaoLancamentoContabil =
				this.consultarLancamentoContabilSinteticoInserir(mapaParametros);

		Iterator<LancamentoContabilSintetico> iterator = colecaoLancamentoContabil.iterator();
		
		if (!Util.isNullOrEmpty(colecaoLancamentoContabil)) {
//			if (colecaoLancamentoContabil.size() > 1) {
//				throw new NegocioException(Constantes.ERRO_NEGOCIO_MAIS_DE_UM_LANCAMENTO, Boolean.TRUE);
//			} else {
				lancamentoContabilSintetico = iterator.next();
				if (lancamentosContabeis.containsKey(lancamentoContabilSintetico.getChavePrimaria())) {
					lancamentoContabilSintetico = lancamentosContabeis.get(lancamentoContabilSintetico.getChavePrimaria());
				//}
			}
		}

		return lancamentoContabilSintetico;
	}
	
	
	/**
	 * Se o {@link LancamentoContabilVO} não for nulo, e seu {@link EventoComercialLancamento} for nulo ou o atributo
	 * {@code indicadorRegimeContabilCompetencia} deste for igual a {@code true}, retorna o valor {@code true}.
	 * 
	 * @param lancamentoContabilVO
	 * @return {@link Boolean}
	 */
	private Boolean possuiIndicadorRegimeContabilCompetencia(LancamentoContabilVO lancamentoContabilVO) {
		Boolean possuiIndicador = Boolean.FALSE;
		if (lancamentoContabilVO != null) {
			possuiIndicador = lancamentoContabilVO.getEventoComercialLancamento() == null
					|| lancamentoContabilVO.getEventoComercialLancamento().getIndicadorRegimeContabilCompetencia().equals(Boolean.TRUE);
		}
		return possuiIndicador;
	}
	
	/**
	 * Constrói uma entidade do tipo {@link LancamentoContabilSintetico}, utilizando os atributos de {@link LancamentoContabilVO},
	 * a data de geração e contábil, passados por parâmetro.
	 * 
	 * @param lancamentoContabilVO - {@link LancamentoContabilVO}
	 * @param dataGeracao - {@link Date}
	 * @param dataContabil - {@link Date}
	 * @return {@link LancamentoContabilSintetico}
	 */
	private LancamentoContabilSintetico montarLancamentoContabilSintetico(LancamentoContabilVO lancamentoContabilVO, Date dataGeracao,
			Date dataContabil) {

		LancamentoContabilSintetico lancamentoContabilSintetico = this.montarLancamentoContabilSintetico(lancamentoContabilVO);
		
		lancamentoContabilSintetico.setDataGeracao(dataGeracao);
		lancamentoContabilSintetico.setDataContabil(dataContabil);

		lancamentoContabilSintetico.setDadosAuditoria(lancamentoContabilVO.getDadosAuditoria());
		lancamentoContabilSintetico.setIndicadorIntegrado(Boolean.FALSE);

		return lancamentoContabilSintetico;
	}

	/**
	 * Método responsável por monta um filtro de
	 * pesquisa com o conjunto único de atributos
	 * de LancamentoContabilSintetico.
	 * 
	 * @param lancamentoContabilVO
	 *            o VO de lancamento Contabil
	 * @param dataAtual
	 *            a data atual do sistema ou data
	 *            de geração
	 * @param dataContabil
	 *            a data contábil
	 * @return filtro de pesquisa
	 */
	@Override
	public Map<String, Object> montarFiltroPesquisaAtributosUnicos(LancamentoContabilVO lancamentoContabilVO, Date dataAtual,
					Date dataContabil) {

		Map<String, Object> mapaParametros = new HashMap<>();

		mapaParametros.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);

		mapaParametros.put(DATA_GERACAO, dataAtual);

		if (dataContabil != null) {
			mapaParametros.put(DATA_CONTABIL, dataContabil);
		}
		if (lancamentoContabilVO.getEventoComercial() != null) {
			mapaParametros.put(EVENTO_COMERCIAL, lancamentoContabilVO.getEventoComercial());
		}
		if (lancamentoContabilVO.getSegmento() != null) {
			mapaParametros.put(SEGMENTO, lancamentoContabilVO.getSegmento());
		}
		if (lancamentoContabilVO.getLancamentoItemContabil() != null) {
			mapaParametros.put(LANCAMENTO_ITEM_CONTABIL, lancamentoContabilVO.getLancamentoItemContabil());
		}
		if (lancamentoContabilVO.getTributo() != null) {
			mapaParametros.put(TRIBUTO, lancamentoContabilVO.getTributo());
		}
		if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarDebito())) {
			mapaParametros.put(CONTA_AUXILIAR_DEBITO, lancamentoContabilVO.getContaAuxiliarDebito());
		}
		if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarCredito())) {
			mapaParametros.put(CONTA_AUXILIAR_CREDITO, lancamentoContabilVO.getContaAuxiliarCredito());
		}
		if (lancamentoContabilVO.getContaBancaria() != null) {
			mapaParametros.put(CONTA_BANCARIA, lancamentoContabilVO.getContaBancaria());
		}
		mapaParametros.put(INDICADOR_INTEGRADO, Boolean.FALSE);		

		return mapaParametros;
	}

	/**
	 * Método responsável por criar e popular um
	 * LancamentoContabilSintetico apartir de um
	 * LancamentoContabilVO.
	 * 
	 * @param lancamentoContabilVO
	 *            o VO de
	 *            LancamentoContabilSintetico
	 * @return o LancamentoContabilSintetico
	 *         criado
	 */
	private LancamentoContabilSintetico montarLancamentoContabilSintetico(LancamentoContabilVO lancamentoContabilVO) {

		LancamentoContabilSintetico retorno = null;

		if (lancamentoContabilVO != null) {
			retorno = this.criarLancamentoContabilSintetico();

			if (lancamentoContabilVO.getEventoComercial() != null) {
				retorno.setEventoComercial(lancamentoContabilVO.getEventoComercial());
			}

			if (lancamentoContabilVO.getSegmento() != null) {
				retorno.setSegmento(lancamentoContabilVO.getSegmento());
			}

			if (lancamentoContabilVO.getLancamentoItemContabil() != null) {
				retorno.setLancamentoItemContabil(lancamentoContabilVO.getLancamentoItemContabil());
			}

			if (lancamentoContabilVO.getTributo() != null) {
				retorno.setTributo(lancamentoContabilVO.getTributo());
			}

			if (!StringUtils.isEmpty(lancamentoContabilVO.getCnpj())) {
				retorno.setNumeroCNPJ(lancamentoContabilVO.getCnpj());
			}

			if (lancamentoContabilVO.getContaBancaria() != null) {
				retorno.setContaBancaria(lancamentoContabilVO.getContaBancaria());
			}

			if (lancamentoContabilVO.getValorTotal() != null) {
				retorno.setValor(lancamentoContabilVO.getValorTotal());
			}

			if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarDebito())) {
				retorno.setContaAuxiliarDebito(lancamentoContabilVO.getContaAuxiliarDebito());
			}

			if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarCredito())) {
				retorno.setContaAuxiliarCredito(lancamentoContabilVO.getContaAuxiliarCredito());
			}

			if (!StringUtils.isEmpty(lancamentoContabilVO.getHistoricoContabil())) {
				retorno.setHistorico(lancamentoContabilVO.getHistoricoContabil());
			}

			if (lancamentoContabilVO.getListaLancamentosContabeisAnaliticos() != null
							&& !lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().isEmpty()) {

				retorno.setListaLancamentoContabilAnalitico(this.montarListaLancamentoContabilAnalitico(
								lancamentoContabilVO.getListaLancamentosContabeisAnaliticos(), lancamentoContabilVO.getDadosAuditoria()));
			}
		}

		return retorno;
	}

	/**
	 * Método responsável criar e montar uma lista
	 * de LancamentoContabilAnalitico apartir de
	 * uma lista de LancamentoContabilAnaliticoVO.
	 * 
	 * @param listaLancamentosContabeisAnaliticos
	 *            a lista de
	 *            LancamentoContabilAnaliticoVO
	 * @param dadosAuditoria
	 *            o dados auditoria
	 * @return o conjunto criado de
	 *         LancamentoContabilAnalitico
	 */
	private Set<LancamentoContabilAnalitico> montarListaLancamentoContabilAnalitico(
					Collection<LancamentoContabilAnaliticoVO> listaLancamentosContabeisAnaliticos, DadosAuditoria dadosAuditoria) {

		Set<LancamentoContabilAnalitico> retorno = new HashSet<>();

		if (listaLancamentosContabeisAnaliticos != null) {
			for (LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO : listaLancamentosContabeisAnaliticos) {
				retorno.add(this.montarLancamentoContabilAnalitico(lancamentoContabilAnaliticoVO, dadosAuditoria));
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por criar e montar um
	 * único LancamentoContabilAnalitico apartir
	 * de um lancamentoContabilAnaliticoVO.
	 * 
	 * @param lancamentoContabilAnaliticoVO
	 *            o lancamentoContabilAnaliticoVO
	 * @param dadosAuditoria
	 *            o objeto dadosAuditoria
	 * @return o LancamentoContabilAnalitico
	 *         criado
	 */
	private LancamentoContabilAnalitico montarLancamentoContabilAnalitico(LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO,
					DadosAuditoria dadosAuditoria) {

		LancamentoContabilAnalitico lancamentoContabilAnalitico = this.criarLancamentoContabilAnalitico();

		if (lancamentoContabilAnaliticoVO.getValor() != null) {
			lancamentoContabilAnalitico.setValor(lancamentoContabilAnaliticoVO.getValor());
		}
		if (lancamentoContabilAnaliticoVO.getCodigoObjeto() != null) {
			lancamentoContabilAnalitico.setCodigoObjeto(lancamentoContabilAnaliticoVO.getCodigoObjeto());
		}
		if (dadosAuditoria != null) {
			lancamentoContabilAnalitico.setDadosAuditoria(dadosAuditoria);
			lancamentoContabilAnalitico.setUltimaAlteracao(Calendar.getInstance().getTime());
			lancamentoContabilAnalitico.setHabilitado(true);
		}

		return lancamentoContabilAnalitico;
	}

	/**
	 * Método responsável por acumular o valor de
	 * um LancamentoContabilSintetico e
	 * atualiza-lo.
	 * 
	 * @param lcSintetico
	 *            o LancamentoContabilSintetico
	 * @param lancamentoContabilVO
	 *            the lancamento contabil vo
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void atualizarContabilizacao(LancamentoContabilSintetico lcSintetico, LancamentoContabilVO lancamentoContabilVO)
					throws NegocioException, ConcorrenciaException {

		this.acumularContabilizacao(lcSintetico, lancamentoContabilVO);
		
		this.atualizar(lcSintetico);
	}
	
	/**
	 * Método responsável por atualizar uma coleção de entidades do tipo {@link LancamentoContabilSintetico}
	 * 
	 * @param lancamentosContabeis - {@link Map}
	 * @throws NegocioException 
	 **/
	@Override
	public void atualizarContabilizacoesAcumuladas(Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas) throws NegocioException {

		super.atualizarColecao(contabilizacoesAcumuladas.values());
	}
	
	
	/**
	 * O valor recuperado da contabilização é
	 * somado com o valor informado como parâmetro
	 * de entrada e atualiza as informações das
	 * contas auxiliares de crédito e débito
	 * 
	 * @param lcSintetico - {@link LancamentoContabilSintetico}
	 * @param lancamentoContabilVO - {@link LancamentoContabilVO}
	 */
	private void acumularContabilizacao(LancamentoContabilSintetico lcSintetico, LancamentoContabilVO lancamentoContabilVO) {

		LancamentoContabilSintetico lcsMontar = this.montarLancamentoContabilSintetico(lancamentoContabilVO);
		lcSintetico.setListaLancamentoContabilAnalitico(lcsMontar.getListaLancamentoContabilAnalitico());

		lcSintetico.setValor(lcSintetico.getValor().add(lancamentoContabilVO.getValorTotal()));
		lcSintetico.setDadosAuditoria(lancamentoContabilVO.getDadosAuditoria());

		if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarDebito())) {
			lcSintetico.setContaAuxiliarDebito(lancamentoContabilVO.getContaAuxiliarDebito());
		}

		if (!StringUtils.isEmpty(lancamentoContabilVO.getContaAuxiliarCredito())) {
			lcSintetico.setContaAuxiliarCredito(lancamentoContabilVO.getContaAuxiliarCredito());
		}		
	}

	/**
	 * Método responsável por consultar os
	 * LancamentoContabilSintetico apartir de um
	 * filtro antes de inserir.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os LancamentoContabilSintetico que
	 *         se enquadram nos parâmetros
	 *         passados no filtro
	 */
	@SuppressWarnings({"unchecked", "unused"})
	@Override
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoInserir(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(getClasseEntidade());

		if (filtro != null) {

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
			
			Boolean indicadorIntegrado = (Boolean) filtro.get(INDICADOR_INTEGRADO);
			if (indicadorIntegrado != null) {
				criteria.add(Restrictions.eq(INDICADOR_INTEGRADO, indicadorIntegrado));
			}

			Date dataGeracao = (Date) filtro.get(DATA_GERACAO);
			if (dataGeracao != null) {
				DateTime dataGeracaodt = new DateTime(dataGeracao);
				dataGeracaodt = Util.zerarHorario(dataGeracaodt);
				criteria.add(Restrictions.eq(DATA_GERACAO, dataGeracaodt.toDate()));
			}

			Date dataContabil = (Date) filtro.get(DATA_CONTABIL);
			if (dataContabil != null) {
				DateTime dataContabildt = new DateTime(dataContabil);
				dataContabildt = Util.zerarHorario(dataContabildt);
				criteria.add(Restrictions.eq(DATA_CONTABIL, dataContabildt.toDate()));
			}

			EventoComercial eventoComercial = (EventoComercial) filtro.get(EVENTO_COMERCIAL);
			if (eventoComercial != null) {
				criteria.add(Restrictions.eq("eventoComercial.chavePrimaria", eventoComercial.getChavePrimaria()));
			}

			Segmento segmento = (Segmento) filtro.get(SEGMENTO);
			if (segmento != null) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento.getChavePrimaria()));
			} else {
				criteria.add(Restrictions.isNull(SEGMENTO));
			}

			LancamentoItemContabil lancamentoItemContabil = (LancamentoItemContabil) filtro.get(LANCAMENTO_ITEM_CONTABIL);
			if (lancamentoItemContabil != null) {
				criteria.add(Restrictions.eq("lancamentoItemContabil.chavePrimaria", lancamentoItemContabil.getChavePrimaria()));
			}

			Tributo tributo = (Tributo) filtro.get(TRIBUTO);
			if (tributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", tributo.getChavePrimaria()));
			}

			String contaAuxiliar = (String) filtro.get("contaAuxiliar");
			if (contaAuxiliar != null) {
				criteria.add(Restrictions.eq("contaAuxiliar", contaAuxiliar));
			}
			
			String contaAuxiliarDebito = (String) filtro.get(CONTA_AUXILIAR_DEBITO);
			if (contaAuxiliarDebito != null) {
				criteria.add(Restrictions.eq(CONTA_AUXILIAR_DEBITO, contaAuxiliarDebito));
			}
			
			String contaAuxiliarCredito = (String) filtro.get(CONTA_AUXILIAR_CREDITO);
			if (contaAuxiliarCredito != null) {
				criteria.add(Restrictions.eq(CONTA_AUXILIAR_CREDITO, contaAuxiliarCredito));
			}				      	

			String cnpj = (String) filtro.get("cnpj");
			if (!StringUtils.isEmpty(cnpj)) {
				criteria.add(Restrictions.eq("numeroCNPJ", Util.removerCaracteresEspeciais(cnpj)));
			}

			ContaBancaria contaBancaria = (ContaBancaria) filtro.get(CONTA_BANCARIA);
			if (contaBancaria != null) {
				criteria.add(Restrictions.eq("contaBancaria.chavePrimaria", contaBancaria.getChavePrimaria()));
			}

			criteria.setFetchMode(EVENTO_COMERCIAL, FetchMode.JOIN);
			criteria.setFetchMode("eventoComercial.modulo", FetchMode.JOIN);
			criteria.setFetchMode(LANCAMENTO_ITEM_CONTABIL, FetchMode.JOIN);
			criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);
			criteria.setFetchMode(TRIBUTO, FetchMode.JOIN);

			criteria.addOrder(Order.desc(DATA_CONTABIL));

			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey(COLECAO_PAGINADA)) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}
			} else {
				paginacaoPadrao = true;
			}
		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarLancamentoContabilSinteticoPorRecebimento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoPorRecebimento(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeLancamentoContabilAnalitico().getSimpleName());
		hql.append(" as lancamentoAnalitico, ");
		hql.append(getClasseEntidadeLancamentoContabilSintetico().getSimpleName());
		hql.append(" as lancamentoSintetico, ");
		hql.append(getClasseEntidadeEventoComercial().getSimpleName());
		hql.append(" as eventoComercial, ");
		hql.append(getClasseEntidadeDocumentoFiscal().getSimpleName());
		hql.append(" as documentoFiscal, ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" as fatura, ");
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append(" as segmento, ");
		hql.append(getClasseEntidadeModulo().getSimpleName());
		hql.append(" as modulo, ");
		hql.append(getClasseEntidadeLancamentoItemContabil().getSimpleName());
		hql.append(" as lancamentoItemContabil, ");
		hql.append(getClasseEntidadeRecebimento().getSimpleName());
		hql.append(" as recebimento, ");
		hql.append(getClasseEntidadeFaturaGeral().getSimpleName());
		hql.append(" as faturaGeral ");

		hql.append(" where lancamentoSintetico.chavePrimaria = lancamentoAnalitico.lancamentoContabilSintetico.chavePrimaria");
		hql.append(" and eventoComercial.chavePrimaria = lancamentoSintetico.eventoComercial.chavePrimaria ");
		hql.append(" and eventoComercial.tabelaReferencia.chavePrimaria = :codigoTabela ");
		hql.append(" and fatura.chavePrimaria = documentoFiscal.fatura.chavePrimaria ");
		hql.append(" and lancamentoAnalitico.codigoObjeto = recebimento.chavePrimaria");

		hql.append(" and recebimento.faturaGeral.chavePrimaria = faturaGeral.chavePrimaria");
		hql.append(" and faturaGeral.faturaAtual.chavePrimaria = fatura.chavePrimaria");

		hql.append(" and segmento.chavePrimaria = lancamentoSintetico.segmento.chavePrimaria");
		hql.append(" and lancamentoItemContabil.chavePrimaria = lancamentoSintetico.lancamentoItemContabil.chavePrimaria");

		hql.append(" and modulo.chavePrimaria = eventoComercial.modulo.chavePrimaria");

		Long idEventoComercial = (Long) filtro.get(FILTRO_ID_EVENTO_COMERCIAL);
		if (idEventoComercial != null) {
			hql.append(" and eventoComercial.chavePrimaria = :idEventoComercial");
		}

		Long idModulo = (Long) filtro.get(FILTRO_ID_MODULO);
		if (idModulo != null) {
			hql.append(" and eventoComercial.modulo.chavePrimaria = :idModulo");
		}

		Long idSegmento = (Long) filtro.get(FILTRO_ID_SEGMENTO);
		if (idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento");
		}

		Long idLancamentoItemContabil = (Long) filtro.get(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM);
		if (idLancamentoItemContabil != null) {
			hql.append(" and lancamentoItemContabil.chavePrimaria = :idLancamentoItemContabil");
		}

		String numeroNotaFiscalInicial = (String) filtro.get("numeroNotaFiscalInicial");
		String numeroNotaFiscalFinal = (String) filtro.get("numeroNotaFiscalFinal");
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && !StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			hql.append(" and (documentoFiscal.numero between :codigoNotaInicial and :codigoNotaFinal) ");
		}
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			hql.append(" and (documentoFiscal.numero = :codigoNotaInicial) ");
		}

		Date dataContabilInicial = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_INICIAL);
		Date dataContabilFinal = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_FINAL);
		if (dataContabilInicial != null && dataContabilFinal != null) {
			hql.append(" and (lancamentoSintetico.dataContabil between :dataContabilInicial and :dataContabilFinal) ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaFaturamento = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_CODIGO_TABELA_RECEBIMENTO);

		query.setParameter("codigoTabela", Long.parseLong(constanteSistemaFaturamento.getValor()));

		if (dataContabilInicial != null && dataContabilFinal != null) {
			Util.adicionarRestricaoDataSemHora(query, dataContabilInicial, "dataContabilInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataContabilFinal, "dataContabilFinal", Boolean.FALSE);
		}
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && !StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			query.setParameter("codigoNotaInicial", Long.parseLong(numeroNotaFiscalInicial));
			query.setParameter("codigoNotaFinal", Long.parseLong(numeroNotaFiscalFinal));
		}
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			query.setParameter("codigoNotaInicial", Long.parseLong(numeroNotaFiscalInicial));
		}
		if (idEventoComercial != null) {
			query.setParameter(FILTRO_ID_EVENTO_COMERCIAL, idEventoComercial);
		}
		if (idModulo != null) {
			query.setParameter(ID_MODULO, idModulo);
		}
		if (idSegmento != null) {
			query.setParameter(ID_SEGMENTO, idSegmento);
		}
		if (idLancamentoItemContabil != null) {
			query.setParameter(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM, idLancamentoItemContabil);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarLancamentoContabilSinteticoPorFaturamento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LancamentoContabilSinteticoVO> consultarLancamentoContabilSinteticoPorFaturamento(Map<String, Object> filtro)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeLancamentoContabilAnalitico().getSimpleName());
		hql.append(" as lancamentoAnalitico, ");
		hql.append(getClasseEntidadeLancamentoContabilSintetico().getSimpleName());
		hql.append(" as lancamentoSintetico, ");
		hql.append(getClasseEntidadeEventoComercial().getSimpleName());
		hql.append(" as eventoComercial, ");
		hql.append(getClasseEntidadeDocumentoFiscal().getSimpleName());
		hql.append(" as documentoFiscal, ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" as fatura, ");
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append(" as segmento, ");
		hql.append(getClasseEntidadeModulo().getSimpleName());
		hql.append(" as modulo, ");
		hql.append(getClasseEntidadeLancamentoItemContabil().getSimpleName());
		hql.append(" as lancamentoItemContabil ");

		hql.append(" where lancamentoSintetico.chavePrimaria = lancamentoAnalitico.lancamentoContabilSintetico.chavePrimaria");
		hql.append(" and eventoComercial.chavePrimaria = lancamentoSintetico.eventoComercial.chavePrimaria ");
		hql.append(" and eventoComercial.tabelaReferencia.chavePrimaria = :codigoTabela ");
		hql.append(" and fatura.chavePrimaria = documentoFiscal.fatura.chavePrimaria ");
		hql.append(" and lancamentoAnalitico.codigoObjeto = documentoFiscal.fatura.chavePrimaria");

		hql.append(" and segmento.chavePrimaria = lancamentoSintetico.segmento.chavePrimaria");
		hql.append(" and lancamentoItemContabil.chavePrimaria = lancamentoSintetico.lancamentoItemContabil.chavePrimaria");

		hql.append(" and modulo.chavePrimaria = eventoComercial.modulo.chavePrimaria");

		Long idEventoComercial = (Long) filtro.get(FILTRO_ID_EVENTO_COMERCIAL);
		if (idEventoComercial != null) {
			hql.append(" and eventoComercial.chavePrimaria = :idEventoComercial");
		}

		Long idModulo = (Long) filtro.get(FILTRO_ID_MODULO);
		if (idModulo != null) {
			hql.append(" and eventoComercial.modulo.chavePrimaria = :idModulo");
		}

		Long idSegmento = (Long) filtro.get(FILTRO_ID_SEGMENTO);
		if (idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento");
		}

		Long idLancamentoItemContabil = (Long) filtro.get(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM);
		if (idLancamentoItemContabil != null) {
			hql.append(" and lancamentoItemContabil.chavePrimaria = :idLancamentoItemContabil");
		}

		String numeroNotaFiscalInicial = (String) filtro.get("numeroNotaFiscalInicial");
		String numeroNotaFiscalFinal = (String) filtro.get("numeroNotaFiscalFinal");
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && !StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			hql.append(" and (documentoFiscal.numero between :codigoNotaInicial and :codigoNotaFinal) ");
		}
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			hql.append(" and (documentoFiscal.numero = :codigoNotaInicial) ");
		}

		Date dataContabilInicial = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_INICIAL);
		Date dataContabilFinal = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_FINAL);
		if (dataContabilInicial != null && dataContabilFinal != null) {
			hql.append(" and (lancamentoSintetico.dataContabil between :dataContabilInicial and :dataContabilFinal) ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaFaturamento = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_CODIGO_TABELA_FATURAMENTO);

		query.setParameter("codigoTabela", Long.parseLong(constanteSistemaFaturamento.getValor()));

		if (dataContabilInicial != null && dataContabilFinal != null) {
			Util.adicionarRestricaoDataSemHora(query, dataContabilInicial, "dataContabilInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataContabilFinal, "dataContabilFinal", Boolean.FALSE);
		}

		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && !StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			query.setParameter("codigoNotaInicial", Long.parseLong(numeroNotaFiscalInicial));
			query.setParameter("codigoNotaFinal", Long.parseLong(numeroNotaFiscalFinal));
		}
		if (!StringUtils.isEmpty(numeroNotaFiscalInicial) && StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			query.setParameter("codigoNotaInicial", Long.parseLong(numeroNotaFiscalInicial));
		}
		if (idEventoComercial != null) {
			query.setParameter(FILTRO_ID_EVENTO_COMERCIAL, idEventoComercial);
		}
		if (idModulo != null) {
			query.setParameter(ID_MODULO, idModulo);
		}
		if (idSegmento != null) {
			query.setParameter(ID_SEGMENTO, idSegmento);
		}
		if (idLancamentoItemContabil != null) {
			query.setParameter(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM, idLancamentoItemContabil);
		}

		List<Object> listaObjetoLancamentoContabil = query.list();
		listaObjetoLancamentoContabil.addAll(this.consultarLancamentoContabilSinteticoPorRecebimento(filtro));

		List<LancamentoContabilSinteticoVO> listaLancamentoContabilSinteticoVO = new ArrayList<>();
		List<LancamentoContabilSintetico> listaLancamentoContabilSintetico = new ArrayList<>();
		LancamentoContabilSinteticoVO lancamentoContabilSinteticoVO = null;
		int cont = 0;
		for (Iterator<Object> iterator = listaObjetoLancamentoContabil.iterator(); iterator.hasNext();) {
			Object[] lancamentoContabilSintetico = (Object[]) iterator.next();

			for (int i = 0; i < lancamentoContabilSintetico.length; i++) {
				if (lancamentoContabilSintetico[i].getClass().equals(LancamentoContabilSinteticoImpl.class)) {
					listaLancamentoContabilSintetico.add((LancamentoContabilSintetico) lancamentoContabilSintetico[i]);
					this.popularContaCreditoDebitoLancamentoSintetico(listaLancamentoContabilSintetico);
					lancamentoContabilSinteticoVO = this
									.montarLancamentoContabilSinteticoVO((LancamentoContabilSintetico) lancamentoContabilSintetico[i]);
					listaLancamentoContabilSinteticoVO.add(lancamentoContabilSinteticoVO);
					listaLancamentoContabilSintetico.clear();
				}
				if (lancamentoContabilSintetico[i].getClass().getSimpleName().equals(DocumentoFiscal.class.getSimpleName() + "Impl")
								&& ((DocumentoFiscal) lancamentoContabilSintetico[i]).getNumero() != null) {
					listaLancamentoContabilSinteticoVO.get(cont).setNumeroNotaFiscal(
									((DocumentoFiscal) lancamentoContabilSintetico[i]).getNumero());
				}
			}
			cont++;

		}

		return listaLancamentoContabilSinteticoVO;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#temLancamentoContabilSinteticoPraFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Boolean temLancamentoContabilSinteticoPraFatura(Fatura fatura) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeLancamentoContabilAnalitico().getSimpleName());
		hql.append(" as lancamentoAnalitico, ");
		hql.append(getClasseEntidadeLancamentoContabilSintetico().getSimpleName());
		hql.append(" as lancamentoSintetico, ");
		hql.append(getClasseEntidadeEventoComercial().getSimpleName());
		hql.append(" as eventoComercial ");
		hql.append(" where lancamentoSintetico.chavePrimaria = lancamentoAnalitico.lancamentoContabilSintetico.chavePrimaria");
		hql.append(" and eventoComercial.chavePrimaria = lancamentoSintetico.eventoComercial.chavePrimaria ");
		hql.append(" and eventoComercial.tabelaReferencia.chavePrimaria = :codigoTabela ");
		hql.append(" and lancamentoAnalitico.codigoObjeto = :codigoFatura");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaFaturamento = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_CODIGO_TABELA_FATURAMENTO);

		query.setParameter("codigoTabela", Long.parseLong(constanteSistemaFaturamento.getValor()));
		query.setParameter("codigoFatura", fatura.getChavePrimaria());

		List<Object> listaObjetoLancamentoContabil = query.list();

		Boolean temLancamentoContabil = Boolean.FALSE;
		if (listaObjetoLancamentoContabil != null && !listaObjetoLancamentoContabil.isEmpty()) {
			temLancamentoContabil = Boolean.TRUE;
		}

		return temLancamentoContabil;

	}

	/**
	 * Montar lancamento contabil sintetico vo.
	 * 
	 * @param lancamentoContabilSintetico
	 *            the lancamento contabil sintetico
	 * @return the lancamento contabil sintetico vo
	 */
	@SuppressWarnings("unused")
	private LancamentoContabilSinteticoVO montarLancamentoContabilSinteticoVO(LancamentoContabilSintetico lancamentoContabilSintetico) {

		LancamentoContabilSinteticoVO lancamentoContabilSinteticoVO = new LancamentoContabilSinteticoVO();

		if (lancamentoContabilSintetico.getChavePrimaria() > 0) {
			lancamentoContabilSinteticoVO.setChavePrimaria(lancamentoContabilSintetico.getChavePrimaria());
		}
		if (lancamentoContabilSintetico.getDataGeracao() != null) {
			lancamentoContabilSinteticoVO.setDataGeracao(lancamentoContabilSintetico.getDataGeracao());
		}
		if (lancamentoContabilSintetico.getDataContabil() != null) {
			lancamentoContabilSinteticoVO.setDataContabil(lancamentoContabilSintetico.getDataContabil());
		}
		if (lancamentoContabilSintetico.getEventoComercial() != null) {
			lancamentoContabilSinteticoVO.setEventoComercial(lancamentoContabilSintetico.getEventoComercial());
		}
		if (lancamentoContabilSintetico.getSegmento() != null) {
			lancamentoContabilSinteticoVO.setSegmento(lancamentoContabilSintetico.getSegmento());
		}
		if (lancamentoContabilSintetico.getLancamentoItemContabil() != null) {
			lancamentoContabilSinteticoVO.setLancamentoItemContabil(lancamentoContabilSintetico.getLancamentoItemContabil());
		}
		if (lancamentoContabilSintetico.getTributo() != null) {
			lancamentoContabilSinteticoVO.setTributo(lancamentoContabilSintetico.getTributo());
		}
		if (lancamentoContabilSintetico.getContaContabilDebito() != null) {
			lancamentoContabilSinteticoVO.setContaContabilDebito(lancamentoContabilSintetico.getContaContabilDebito());
		}
		if (lancamentoContabilSintetico.getContaContabilCredito() != null) {
			lancamentoContabilSinteticoVO.setContaContabilCredito(lancamentoContabilSintetico.getContaContabilCredito());
		}
		if (lancamentoContabilSintetico.getValor() != null) {
			lancamentoContabilSinteticoVO.setValor(lancamentoContabilSintetico.getValor());
		}
		if (lancamentoContabilSintetico.getListaLancamentoContabilAnalitico() != null) {
			lancamentoContabilSinteticoVO.setListaLancamentoContabilAnalitico(lancamentoContabilSintetico
							.getListaLancamentoContabilAnalitico());
		}

		return lancamentoContabilSinteticoVO;
	}
	
	@Override
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoHabilitadoNaoIntegrado() {
		
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put(INDICADOR_INTEGRADO, Boolean.FALSE);

		return consultarLancamentoContabilSintetico(filtro);
	}
	/**
	 * Método responsável por consultar os
	 * LancamentoContabilSintetico apartir de um
	 * filtro.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os LancamentoContabilSintetico que
	 *         se enquadram nos parâmetros
	 *         passados no filtro
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSintetico(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(getClasseEntidade());

		if (filtro != null) {

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

			Boolean indicadorIntegrado = (Boolean) filtro.get(INDICADOR_INTEGRADO);
			if (indicadorIntegrado != null) {
				criteria.add(Restrictions.eq(INDICADOR_INTEGRADO, indicadorIntegrado));
			}

			Date dataGeracao = (Date) filtro.get(DATA_GERACAO);
			if (dataGeracao != null) {
				DateTime dataGeracaodt = new DateTime(dataGeracao);
				dataGeracaodt = Util.zerarHorario(dataGeracaodt);
				criteria.add(Restrictions.eq(DATA_GERACAO, dataGeracaodt.toDate()));
			}

			Date dataContabil = (Date) filtro.get(DATA_CONTABIL);
			if (dataContabil != null) {
				DateTime dataContabildt = new DateTime(dataContabil);
				dataContabildt = Util.zerarHorario(dataContabildt);
				criteria.add(Restrictions.eq(DATA_CONTABIL, dataContabildt.toDate()));
			}

			EventoComercial eventoComercial = (EventoComercial) filtro.get(EVENTO_COMERCIAL);
			if (eventoComercial != null) {
				criteria.add(Restrictions.eq("eventoComercial.chavePrimaria", eventoComercial.getChavePrimaria()));
			}

			Segmento segmento = (Segmento) filtro.get(SEGMENTO);
			if (segmento != null) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento.getChavePrimaria()));
			}

			LancamentoItemContabil lancamentoItemContabil = (LancamentoItemContabil) filtro.get(LANCAMENTO_ITEM_CONTABIL);
			if (lancamentoItemContabil != null) {
				criteria.add(Restrictions.eq("lancamentoItemContabil.chavePrimaria", lancamentoItemContabil.getChavePrimaria()));
			}

			Tributo tributo = (Tributo) filtro.get(TRIBUTO);
			if (tributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", tributo.getChavePrimaria()));
			}

			String cnpj = (String) filtro.get("cnpj");
			if (!StringUtils.isEmpty(cnpj)) {
				criteria.add(Restrictions.eq("numeroCNPJ", Util.removerCaracteresEspeciais(cnpj)));
			}

			ContaBancaria contaBancaria = (ContaBancaria) filtro.get(CONTA_BANCARIA);
			if (contaBancaria != null) {
				criteria.add(Restrictions.eq("contaBancaria.chavePrimaria", contaBancaria.getChavePrimaria()));
			}

			// filtros da tela de consulta
			Date dataContabilInicial = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_INICIAL);
			if (dataContabilInicial != null) {

				criteria.add(Restrictions.ge(DATA_CONTABIL, dataContabilInicial));
			}

			Date dataContabilFinal = (Date) filtro.get(FILTRO_DATA_INTERVALO_CONTABIL_FINAL);
			DateTime dataFinal = new DateTime(dataContabilFinal);
			dataFinal = dataFinal.withHourOfDay(LIMITE_HORAS);
			dataFinal = dataFinal.withMinuteOfHour(LIMITE_MINUTOS);
			dataFinal = dataFinal.withSecondOfMinute(LIMITE_SEGUNDOS);
			if (dataContabilFinal != null) {

				criteria.add(Restrictions.le(DATA_CONTABIL, dataFinal.toDate()));
			}

			Long IdEventoComercial = (Long) filtro.get(FILTRO_ID_EVENTO_COMERCIAL);
			if (IdEventoComercial != null) {
				criteria.add(Restrictions.eq("eventoComercial.chavePrimaria", IdEventoComercial));
			}

			Long IdModulo = (Long) filtro.get(FILTRO_ID_MODULO);
			if (IdModulo != null) {
				criteria.createCriteria(EVENTO_COMERCIAL).add(Restrictions.eq("modulo.chavePrimaria", IdModulo));
			}

			Long IdContabilItem = (Long) filtro.get(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM);
			if (IdContabilItem != null) {
				criteria.add(Restrictions.eq("lancamentoItemContabil.chavePrimaria", IdContabilItem));
			}

			Long idTributo = (Long) filtro.get("idTributo");
			if (idTributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", idTributo));
			}

			Long idSegmento = (Long) filtro.get(FILTRO_ID_SEGMENTO);
			if (idSegmento != null) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			}

			Long chavePrimaria = (Long) filtro.get(FILTRO_CHAVE_PRIMARIA);
			if (chavePrimaria != null) {
				criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
			}

			criteria.createAlias("eventoComercial.modulo", "modulo");

			criteria.setFetchMode(EVENTO_COMERCIAL, FetchMode.JOIN);
			criteria.setFetchMode("eventoComercial.modulo", FetchMode.JOIN);
			criteria.setFetchMode(LANCAMENTO_ITEM_CONTABIL, FetchMode.JOIN);
			criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);
			criteria.setFetchMode(TRIBUTO, FetchMode.JOIN);

			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey(COLECAO_PAGINADA)) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);

				colecaoPaginada.adicionarOrdenacaoEspecial(DATA_CONTABIL, new OrdenacaoEspecialDataContabil());

				colecaoPaginada.adicionarOrdenacaoEspecial(DATA_CONTABIL, new OrdenacaoEspecialDataContabil());
				colecaoPaginada.adicionarOrdenacaoEspecial("contaContabilDebito", new OrdenacaoEspecialContaContabilDebito());
				colecaoPaginada.adicionarOrdenacaoEspecial("contaContabilCredito", new OrdenacaoEspecialContaContabilCredito());
				colecaoPaginada.adicionarOrdenacaoEspecial("eventoComercial.modulo", new OrdenacaoEspecialDescricaoModulo());
				colecaoPaginada.adicionarOrdenacaoEspecial(SEGMENTO, new OrdenacaoEspecialDescricaoSegmento());
				colecaoPaginada.adicionarOrdenacaoEspecial("eventoComercial.descricao", new OrdenacaoEspecialDescricaoEventoComercial());

				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}
			} else {
				paginacaoPadrao = true;
			}

			if (paginacaoPadrao) {

				criteria.addOrder(Order.desc(DATA_CONTABIL));
				criteria.addOrder(Order.asc("modulo.descricao"));
			}
		}

		return criteria.list();
	}

	/**
	 * Método responsável por consultar
	 * LancamentoContabilAnalitico por
	 * LancamentoContabilSintetico.
	 * 
	 * @param lancamentoContabilSintetico
	 *            the lancamento contabil sintetico
	 * @return os LancamentoContabilSintetico que
	 *         se enquadram nos parâmetros
	 *         passados no filtro
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LancamentoContabilAnalitico> consultarLancamentoContabilAnaliticoPorLancamentoContabilSintetico(
					LancamentoContabilSintetico lancamentoContabilSintetico) {

		Criteria criteria = this.createCriteria(getClasseEntidadeLancamentoContabilAnalitico());

		criteria.add(Restrictions.eq("lancamentoContabilSintetico.chavePrimaria", lancamentoContabilSintetico.getChavePrimaria()));

		return criteria.list();
	}

	/**
	 * Método responsável por consultar todos os
	 * eventos comerciais apartir de um filtro.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os EventoComercial que se enquadram
	 *         nos parâmetros passados no filtro
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EventoComercial> consultarTodosEventosComerciais(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(getClasseEntidadeEventoComercial());

		if (filtro != null) {

			if (filtro.get(ID_MODULO) != null) {
				Long idModulo = Long.parseLong(filtro.get(ID_MODULO).toString());
				criteria.add(Restrictions.eq("modulo.chavePrimaria", idModulo));
			}

			String descricao = (String) filtro.get(EVENTO_COMERCIAL_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike("descricao", Util.formatarTextoConsulta(descricao)));
			}

			criteria.setFetchMode("modulo", FetchMode.JOIN);
			criteria.addOrder(Order.asc("descricao"));
		}

		return criteria.list();
	}

	/**
	 * Checks if is filtro evento comercial lancamento.
	 * 
	 * @param filtro the filtro
	 * @return the boolean
	 */
	public Boolean isFiltroEventoComercialLancamento(Map<String, Object> filtro) {
		
		Boolean retorno = Boolean.FALSE;

		if (filtro.containsKey(EVENTO_COMERCIAL_DESCRICAO)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(ID_MODULO)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(ID_DEBITO1)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(ID_TRIBUTO1)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(ID_SEGMENTO1)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(ID_CREDITO1)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey("idItemContabil1")) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_DEBITO)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_CREDITO)) {
			return Boolean.TRUE;
		}

		if (filtro.containsKey(DESCRICAO_HISTORICO)) {
			return Boolean.TRUE;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarTodosEventosComerciaisPorFiltro(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EventoComercial> consultarTodosEventosComerciaisPorFiltro(Map<String, Object> filtro) {

		StringBuilder hql = new StringBuilder();

		Query query = null;

		hql.append("select distinct eventoComercial ");
		hql.append(FROM);
		hql.append(getClasseEntidadeEventoComercial().getSimpleName());
		hql.append(" eventoComercial");
		hql.append(" left join eventoComercial.eventoComercialLancamentoList as eventoComercialLancamentoList");
		hql.append(WHERE);
		hql.append(" eventoComercial.habilitado = true");

		if (filtro.containsKey(EVENTO_COMERCIAL_DESCRICAO)) {
			hql.append(" and lower(eventoComercial.descricao) like :eventoComercialDescricao");
		}

		if (filtro.containsKey(ID_MODULO)) {
			Long idModulo = (Long) filtro.get(ID_MODULO);

			if (idModulo > 0) {
				hql.append(" and eventoComercial.modulo.chavePrimaria = :eventoComercialModulo");
			}
		}

		if (filtro.containsKey(ID_DEBITO1)) {
			Long idDebito = (Long) filtro.get(ID_DEBITO1);

			if (idDebito > 0) {
				hql.append(" and eventoComercialLancamentoList.contaContabilDebito.chavePrimaria = :contaContabilDebito");
			}
		}

		if (filtro.containsKey(ID_TRIBUTO1)) {
			Long idTributo = (Long) filtro.get(ID_TRIBUTO1);

			if (idTributo > 0) {
				hql.append(" and eventoComercialLancamentoList.tributo.chavePrimaria = :idTributo");
			}
		}

		if (filtro.containsKey(ID_SEGMENTO1)) {
			Long idSegmento = (Long) filtro.get(ID_SEGMENTO1);

			if (idSegmento > 0) {
				hql.append(" and eventoComercialLancamentoList.segmento.chavePrimaria = :idSegmento");
			}
		}

		if (filtro.containsKey(ID_CREDITO1)) {
			Long idCredito1 = (Long) filtro.get(ID_CREDITO1);

			if (idCredito1 > 0) {
				hql.append(" and eventoComercialLancamentoList.contaContabilCredito.chavePrimaria = :idCredito");
			}
		}

		if (filtro.containsKey("idItemContabil1")) {
			Long idItemContabil = (Long) filtro.get("idItemContabil1");

			if (idItemContabil > 0) {
				hql.append(" and eventoComercialLancamentoList.lancamentoItemContabil.chavePrimaria = :idItemContabil");
			}
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_DEBITO)) {
			hql.append(" and lower(eventoComercialLancamentoList.descricaoContaAuxiliarDebito) like :descricaoContaAuxiliarDebito");
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_CREDITO)) {
			hql.append(" and lower(eventoComercialLancamentoList.descricaoContaAuxiliarCredito) like :descricaoContaAuxiliarCredito");
		}

		if (filtro.containsKey(DESCRICAO_HISTORICO)) {
			hql.append(" and lower(eventoComercialLancamentoList.descricaoHistorico) like :descricaoHistorico");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro.containsKey(DESCRICAO_HISTORICO)) {
			query.setParameter(DESCRICAO_HISTORICO, "%" + filtro.get(DESCRICAO_HISTORICO).toString().toLowerCase() + "%");
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_DEBITO)) {
			query.setParameter(DESCRICAO_CONTA_AUXILIAR_DEBITO, "%" + filtro.get(DESCRICAO_CONTA_AUXILIAR_DEBITO).toString().toLowerCase()
							+ "%");
		}

		if (filtro.containsKey(DESCRICAO_CONTA_AUXILIAR_CREDITO)) {
			query.setParameter(DESCRICAO_CONTA_AUXILIAR_CREDITO, "%" + filtro.get(DESCRICAO_CONTA_AUXILIAR_CREDITO).toString().toLowerCase()
							+ "%");
		}

		if (filtro.containsKey("idItemContabil1")) {
			Long idItemContabil = (Long) filtro.get("idItemContabil1");

			if (idItemContabil > 0) {
				query.setParameter("idItemContabil", idItemContabil);
			}
		}

		if (filtro.containsKey(ID_CREDITO1)) {
			Long idCredito = (Long) filtro.get(ID_CREDITO1);

			if (idCredito > 0) {
				query.setParameter("idCredito", idCredito);
			}
		}

		if (filtro.containsKey(ID_SEGMENTO1)) {
			Long idSegmento = (Long) filtro.get(ID_SEGMENTO1);

			if (idSegmento > 0) {
				query.setParameter(ID_SEGMENTO, idSegmento);
			}
		}

		if (filtro.containsKey(ID_TRIBUTO1)) {
			Long idTributo = (Long) filtro.get(ID_TRIBUTO1);

			if (idTributo > 0) {
				query.setParameter("idTributo", idTributo);
			}
		}

		if (filtro.containsKey(ID_DEBITO1)) {
			Long idDebito = (Long) filtro.get(ID_DEBITO1);

			if (idDebito > 0) {
				query.setParameter("contaContabilDebito", idDebito);
			}
		}

		if (filtro.containsKey(EVENTO_COMERCIAL_DESCRICAO)) {
			query.setParameter(EVENTO_COMERCIAL_DESCRICAO, "%" + filtro.get(EVENTO_COMERCIAL_DESCRICAO).toString().toLowerCase() + "%");
		}

		if (filtro.containsKey(ID_MODULO)) {
			Long idModulo = (Long) filtro.get(ID_MODULO);

			if (idModulo > 0) {
				query.setParameter("eventoComercialModulo", idModulo);
			}
		}

		return query.list();
	}

	/**
	 * Método responsável por inicializar o mapa
	 * de código do arquivo
	 * eventoComercial.properties
	 */
	private static void inicializarPropriedades() {

		try {
			Properties propriedades = new Properties();
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES_EVENTO_COMERCIAL);
			propriedades.load(stream);

			mapaCodigoEventoComercial = new HashMap<>();
			for (Entry<Object, Object> entry : propriedades.entrySet()) {
				mapaCodigoEventoComercial.put(String.valueOf(entry.getKey()).trim(), String.valueOf(entry.getValue()).trim());
			}
			stream.close();
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #obterChaveEventoComercial(java.lang.Class,
	 * br.com.ggas.faturamento.rubrica.Rubrica,
	 * br.com.ggas.contabil.impl.OperacaoContabil,
	 * java.lang.Boolean)
	 */
	@Override
	public String obterChaveEventoComercial(Class<?> classe, Rubrica rubrica, OperacaoContabil operacao, Boolean isCurtoPrazo)
					throws NegocioException {

		String retorno = null;

		if (classe != null && rubrica != null && operacao != null) {
			if (operacao.equals(OperacaoContabil.PARCELAMENTO)) {
				retorno = montarChaveEventoComercialParcelamento(classe, rubrica, isCurtoPrazo);
			} else if (classe == CLASSE_ITEM_FATURA) {
				retorno = montarChaveEventoComercialFatura(rubrica, operacao);
			} else if (classe == CLASSE_CREDITO_DEBITO) {
				retorno = montarChaveEventoComercialCreditoDebito(rubrica, operacao, isCurtoPrazo);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por montar a chave do
	 * evento comercial para operações de Crédito
	 * de Débito a Realizar.
	 * 
	 * @param rubrica
	 *            a rubrica
	 * @param operacao
	 *            a operação
	 * @param isCurtoPrazo
	 *            indicador de curto/longo prazo
	 * @return a chave do evento comercial
	 */
	private String montarChaveEventoComercialCreditoDebito(Rubrica rubrica, OperacaoContabil operacao, Boolean isCurtoPrazo) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		boolean isProduto = rubrica.getFinanciamentoTipo().getDescricaoAbreviada()
						.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_PRODUTO_ABREVIADA);
		boolean isCredito = rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO));

		String chaveEvento = "";

		if (operacao.equals(OperacaoContabil.INCLUIR_CREDITO_DEBITO)) {

			if (isCredito) {
				chaveEvento += INCLUIR_CREDITO_A_REALIZAR;
			} else {
				chaveEvento += INCLUIR_DEBITO_A_REALIZAR;
			}
			if (isProduto) {
				chaveEvento += PRODUTO;
			} else {
				chaveEvento += SERVICO;
			}

			if (isCurtoPrazo) {
				chaveEvento += CURTO_PRAZO;
			} else {
				chaveEvento += LONGO_PRAZO;
			}

		} else if (operacao.equals(OperacaoContabil.CANCELAR_CREDITO_DEBITO)) {

			if (isCredito) {
				chaveEvento += CANCELAR_CREDITO_A_REALIZAR;
			} else {
				chaveEvento += CANCELAR_DEBITO_A_REALIZAR;
			}
			if (isProduto) {
				chaveEvento += PRODUTO;
			} else {
				chaveEvento += SERVICO;
			}

			if (isCurtoPrazo) {
				chaveEvento += CURTO_PRAZO;
			} else {
				chaveEvento += LONGO_PRAZO;
			}

		} else if (operacao.equals(OperacaoContabil.TRASFERIR_CREDITO_DEBITO)) {

			if (isCredito) {
				chaveEvento += CANCELAR_CREDITO_A_REALIZAR;
			} else {
				chaveEvento += TRANSFERENCIA_CREDITO;
			}
			if (isProduto) {
				chaveEvento += PRODUTO;
			} else {
				chaveEvento += SERVICO;
			}

			chaveEvento += LONGO_PARA_CURTO_PRAZO;
		}

		return "EVENTO_COMERCIAL_" + chaveEvento;
	}

	/**
	 * Método responsável por obter a chave do evento comercial para operações de Fatura.
	 * 
	 * @param rubrica a rubrica
	 * @param operacao a operação
	 * @return chave do evento comercial
	 */
	private String montarChaveEventoComercialFatura(Rubrica rubrica, OperacaoContabil operacao) {

		String retorno;

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String chaveEvento = "";
		if (!operacao.equals(OperacaoContabil.CONCILIAR_NOTA_DEBITO)) {

			if (!operacao.equals(OperacaoContabil.INCLUIR_NOTA) && !operacao.equals(OperacaoContabil.CANCELAR_NOTA)) {

				if (rubrica.getItemFatura() != null && rubrica.getItemFatura().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS))) {

					chaveEvento = "_CONSUMO_GAS";
				} else if (rubrica.getItemFatura() != null && rubrica.getItemFatura().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO))) {

					chaveEvento = "_MARGEM";
				} else if (rubrica.getItemFatura() != null && rubrica.getItemFatura().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE))) {

					chaveEvento = "_TRANSPORTE";
				} else if (rubrica.getFinanciamentoTipo() != null
						&& rubrica.getFinanciamentoTipo()
								.getChavePrimaria() == Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PARCELAMENTO))
						&& (operacao.equals(OperacaoContabil.INCLUIR_FATURA) || operacao.equals(OperacaoContabil.CANCELAR_FATURA))) {

					chaveEvento = "_PARCELAMENTO";
				}
			}

			if ("".equals(chaveEvento)) {
				
				if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO))) {
					chaveEvento = _CREDITO;
				} else if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO))) {
					chaveEvento = _DEBITO;
				} else if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
						.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO_PENALIDADE))) {
					chaveEvento = _DEBITO;
				}

				if (rubrica.getFinanciamentoTipo() != null) {
					String descricao = rubrica.getFinanciamentoTipo().getDescricaoAbreviada();
					if (descricao.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_PRODUTO_ABREVIADA)) {
						chaveEvento += _PRODUTO;
					} else {
						chaveEvento += _SERVICO;
					}
				}
			}
		}

		retorno = "EVENTO_COMERCIAL_" + operacao + chaveEvento;
		return retorno;
	}

	/**
	 * Montar chave evento comercial parcelamento.
	 * 
	 * @param classe
	 *            the classe
	 * @param rubrica
	 *            the rubrica
	 * @param isCurtoPrazo
	 *            the is curto prazo
	 * @return the string
	 */
	private String montarChaveEventoComercialParcelamento(Class<?> classe, Rubrica rubrica, Boolean isCurtoPrazo) {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String chaveEvento = "PARCELAMENTO";

		if (classe == CLASSE_ITEM_FATURA) {
			if (rubrica.getItemFatura() != null
							&& rubrica.getItemFatura().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS))) {

				chaveEvento += "_CONSUMO_GAS";
			} else if (rubrica.getItemFatura() != null
							&& rubrica.getItemFatura().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO))) {

				chaveEvento += "_MARGEM";
			} else if (rubrica.getItemFatura() != null
							&& rubrica.getItemFatura().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE))) {

				chaveEvento += "_TRANSPORTE";
			} else if (rubrica.getFinanciamentoTipo() != null
							&& rubrica.getFinanciamentoTipo().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PARCELAMENTO))) {

				chaveEvento += "_DEBITO_PARCELAMENTO";
			} else if (rubrica.getLancamentoItemContabil() != null
							&& rubrica.getLancamentoItemContabil().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO))) {

				chaveEvento += "_JUROS_CORRECAO";
			}

			if ("PARCELAMENTO".equals(chaveEvento)) {
				if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
								.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO))) {
					chaveEvento += _CREDITO;
				} else if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
								.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO))) {
					chaveEvento += _DEBITO;
				}

				if (rubrica.getFinanciamentoTipo() != null
								&& rubrica.getFinanciamentoTipo().getDescricaoAbreviada()
												.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_PRODUTO_ABREVIADA)) {
					chaveEvento += _PRODUTO;
				} else if (rubrica.getFinanciamentoTipo() != null
								&& rubrica.getFinanciamentoTipo().getDescricaoAbreviada()
												.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_SERVICO_ABREVIADA)) {
					chaveEvento += _SERVICO;
				}
			}
		} else if (classe == CLASSE_CREDITO_DEBITO) {
			String tipoBem = _PRODUTO;

			if (rubrica.getFinanciamentoTipo().getDescricaoAbreviada().equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_SERVICO_ABREVIADA)) {
				tipoBem = _SERVICO;
			}

			if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == Long
					.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO))) {
				chaveEvento += _CREDITO + tipoBem + _A_REALIZAR;
			} else {
				chaveEvento += _DEBITO + tipoBem + _A_REALIZAR;
			}
		}

		if (isCurtoPrazo) {
			chaveEvento += _CURTO_PRAZO;
		} else {
			chaveEvento += _LONGO_PRAZO;
		}

		return "EVENTO_COMERCIAL_" + chaveEvento;
	}

	/**
	 * Encerrar referencia contabil.
	 * 
	 * @param logProcessamento
	 *            the log processamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Override
	public void encerrarReferenciaContabil(DadosAuditoria dadosAuditoria) throws IllegalAccessException,
					InstantiationException, InvocationTargetException, NoSuchMethodException, GGASException {

		logProcessamento.append("Encerrar Referencia Contábil...\r\n");

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		Integer codigoFaturaItemGas = Integer.parseInt(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		Integer referenciaContabil = Integer.parseInt(controladorParametroSistema.obterValorDoParametroPorCodigo(
						Constantes.PARAMETRO_REFERENCIA_CONTABIL).toString());

		Collection<Fatura> listaFaturas = controladorFatura.consultarFaturasPorDataLeituraInformada(referenciaContabil, null,
						" fatura.pontoConsumo.chavePrimaria, fatura.numeroCiclo ");

		BigDecimal percentualPerdaVolumeCompraGas = calcularPercentualPerdasVolumeCompraGas(listaFaturas, referenciaContabil);

		if (!listaFaturas.isEmpty()) {
			Fatura faturaAnterior = listaFaturas.iterator().next();
			BigDecimal consumo = new BigDecimal(0);
			Integer qtdDiasConsumo = 0;
			BigDecimal valorConsumo = new BigDecimal(0);

			for (Fatura fatura : listaFaturas) {

				PontoConsumo pontoConsumoAnterior = faturaAnterior.getPontoConsumo();
				PontoConsumo pontoConsumo = fatura.getPontoConsumo();
				if (pontoConsumoAnterior == null) {
					Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
									.getChavePrimaria());
					pontoConsumoAnterior = listaFaturasAgrupadas.iterator().next().getPontoConsumo();
				}
				if (pontoConsumo == null) {
					Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
									.getChavePrimaria());
					pontoConsumo = listaFaturasAgrupadas.iterator().next().getPontoConsumo();
				}

				if (pontoConsumoAnterior.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
					// Acumulando valores

					if (fatura.getHistoricoConsumo() == null) {
						// fatura agrupada
						Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
										.getChavePrimaria());
						qtdDiasConsumo = qtdDiasConsumo + listaFaturasAgrupadas.iterator().next().getHistoricoConsumo().getDiasConsumo();
						for (Fatura faturaAgrupada : listaFaturasAgrupadas) {
							consumo = consumo.add(faturaAgrupada.getHistoricoConsumo().getConsumoApurado());
						}
					} else {
						// fatura sem ser agrupada
						consumo = consumo.add(fatura.getHistoricoConsumo().getConsumoApurado());
						qtdDiasConsumo = qtdDiasConsumo + fatura.getHistoricoConsumo().getDiasConsumo();
					}

					for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {

						if (faturaItem.getRubrica().getItemFatura() != null
										&& faturaItem.getRubrica().getItemFatura().getChavePrimaria() == codigoFaturaItemGas) {

							valorConsumo = valorConsumo.add(faturaItem.getValorTotal());

						}

					}
					faturaAnterior = fatura;

				} else {
					String ultimoDia;
					Date dataFim;
					int qtdDiasRestantes = 0;
					// obtendo dias restantes
					if (faturaAnterior.getHistoricoConsumo() == null) {
						Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
										.getChavePrimaria());
						ultimoDia = Util.obterUltimoDiaMes(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
										.getHistoricoAtual().getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
						dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
						qtdDiasRestantes = Util.intervaloDatas(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
										.getHistoricoAtual().getDataLeituraFaturada(), dataFim);
					} else {
						ultimoDia = Util.obterUltimoDiaMes(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
										.getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
						dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
						qtdDiasRestantes = Util.intervaloDatas(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
										.getDataLeituraFaturada(), dataFim);
					}

					// calculando o valor medio e
					// o consumo medio
					BigDecimal consumoMedio = consumo.divide(new BigDecimal(qtdDiasConsumo), ESCALA, RoundingMode.HALF_UP);
					BigDecimal valorMedio = valorConsumo.divide(new BigDecimal(qtdDiasConsumo), ESCALA, RoundingMode.HALF_UP);

					// inserindo o lancamento
					// contabil
					inserirLancamentoContabilVOBatch(faturaAnterior, "EVENTO_COMERCIAL_INCLUIR_VOLUME_NAO_FATURADO",
									consumoMedio.multiply(new BigDecimal(qtdDiasRestantes)).multiply(percentualPerdaVolumeCompraGas)
													.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP), dadosAuditoria);
					inserirLancamentoContabilVOBatch(faturaAnterior, "EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA",
									valorMedio.multiply(new BigDecimal(qtdDiasRestantes)).multiply(percentualPerdaVolumeCompraGas)
													.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP), dadosAuditoria);

					// Zerando variáveis para
					// processar nova fatura
					consumo = new BigDecimal(0);
					qtdDiasConsumo = 0;
					valorConsumo = new BigDecimal(0);

					// Acumulando valores

					if (fatura.getHistoricoConsumo() == null) {
						// fatura agrupada
						Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
										.getChavePrimaria());
						qtdDiasConsumo = qtdDiasConsumo + listaFaturasAgrupadas.iterator().next().getHistoricoConsumo().getDiasConsumo();
						for (Fatura faturaAgrupada : listaFaturasAgrupadas) {
							consumo = consumo.add(faturaAgrupada.getHistoricoConsumo().getConsumoApurado());
						}
					} else {
						// fatura sem ser agrupada
						consumo = consumo.add(fatura.getHistoricoConsumo().getConsumoApurado());
						qtdDiasConsumo = qtdDiasConsumo + fatura.getHistoricoConsumo().getDiasConsumo();
					}

					for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
						if (faturaItem.getRubrica().getItemFatura() != null
										&& faturaItem.getRubrica().getItemFatura().getChavePrimaria() == codigoFaturaItemGas) {

							valorConsumo = valorConsumo.add(faturaItem.getValorTotal());

						}

					}
					faturaAnterior = fatura;
				}

			}

			// obtendo dias restantes
			String ultimoDia;
			Date dataFim;
			int qtdDiasRestantes = 0;
			// obtendo dias restantes
			if (faturaAnterior.getHistoricoConsumo() == null) {
				Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
								.getChavePrimaria());
				ultimoDia = Util.obterUltimoDiaMes(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo().getHistoricoAtual()
								.getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
				dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
				qtdDiasRestantes = Util.intervaloDatas(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo().getHistoricoAtual()
								.getDataLeituraFaturada(), dataFim);
			} else {
				ultimoDia = Util.obterUltimoDiaMes(faturaAnterior.getHistoricoConsumo().getHistoricoAtual().getDataLeituraFaturada(),
								Constantes.FORMATO_DATA_BR);
				dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
				qtdDiasRestantes = Util.intervaloDatas(faturaAnterior.getHistoricoConsumo().getHistoricoAtual().getDataLeituraFaturada(),
								dataFim);
			}

			// calculando o valor medio e o
			// consumo medio
			BigDecimal consumoMedio = consumo.divide(new BigDecimal(qtdDiasConsumo), ESCALA, RoundingMode.HALF_UP);
			BigDecimal valorMedio = valorConsumo.divide(new BigDecimal(qtdDiasConsumo), ESCALA, RoundingMode.HALF_UP);

			// inserindo o lancamento contabil
			inserirLancamentoContabilVOBatch(
							faturaAnterior,
							"EVENTO_COMERCIAL_INCLUIR_VOLUME_NAO_FATURADO",
							consumoMedio.multiply(new BigDecimal(qtdDiasRestantes)).multiply(percentualPerdaVolumeCompraGas)
											.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP), dadosAuditoria);
			inserirLancamentoContabilVOBatch(
							faturaAnterior,
							"EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA",
							valorMedio.multiply(new BigDecimal(qtdDiasRestantes)).multiply(percentualPerdaVolumeCompraGas)
											.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP), dadosAuditoria);

		}

		logProcessamento.append("Atualizar Parametro Sistema Referencia Contábil...\r\n");
		ParametroSistema parametroAnoMesReferencia = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL);

		Integer anoMesAcrescido = Util.adicionarMesEmAnoMes(Integer.valueOf(parametroAnoMesReferencia.getValor()));
		parametroAnoMesReferencia.setValor(anoMesAcrescido.toString());
		parametroAnoMesReferencia.setDadosAuditoria(dadosAuditoria);

		controladorParametroSistema.atualizar(parametroAnoMesReferencia);

	}

	/**
	 * Calcular percentual perdas volume compra gas.
	 * 
	 * @param listaFaturas
	 *            the lista faturas
	 * @param referenciaContabil
	 *            the referencia contabil
	 * @return the big decimal
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private BigDecimal calcularPercentualPerdasVolumeCompraGas(Collection<Fatura> listaFaturas, Integer referenciaContabil)
					throws GGASException {

		BigDecimal percentual = new BigDecimal(1);
		ControladorPrecoGas controladorPrecoGas = (ControladorPrecoGas) ServiceLocator.getInstancia().getBeanPorID(
						ControladorPrecoGas.BEAN_ID_CONTROLADOR_PRECO_GAS);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		String codigoItemGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("dataFimEmissao",
						Util.ultimoHorario(new DateTime(Util.obterUltimoDiaMesDoAnoMesReferencia(referenciaContabil.toString()))).toDate());
		filtro.put("dataInicioEmissao",
						Util.zerarHorario(new DateTime(Util.obterPrimeiroDiaMesDoAnoMesReferencia(referenciaContabil.toString()))).toDate());
		Collection<PrecoGas> listaPrecoGas = controladorPrecoGas.consultarPrecoGas(filtro);

		if (listaPrecoGas != null && !listaPrecoGas.isEmpty()) {
			BigDecimal totalConsumoGasComprado = new BigDecimal(0);
			for (PrecoGas precoGas : listaPrecoGas) {
				for (PrecoGasItem precoGasItem : precoGas.getPrecoGasItens()) {
					if (precoGasItem.getItemFatura().getChavePrimaria() == Long.parseLong(codigoItemGas)) {
						totalConsumoGasComprado = totalConsumoGasComprado.add(precoGasItem.getVolume());
					}
				}
			}

			BigDecimal totalConsumoGasFaturadoProvisionado = new BigDecimal(0);
			BigDecimal totalConsumoGasFaturado = new BigDecimal(0);
			BigDecimal totalConsumoGasProvisionado = new BigDecimal(0);
			if (!listaFaturas.isEmpty()) {
				Fatura faturaAnterior = listaFaturas.iterator().next();
				BigDecimal consumo = new BigDecimal(0);
				Integer qtdDiasConsumo = 0;

				for (Fatura fatura : listaFaturas) {

					PontoConsumo pontoConsumoAnterior = faturaAnterior.getPontoConsumo();
					PontoConsumo pontoConsumo = fatura.getPontoConsumo();
					if (pontoConsumoAnterior == null) {
						Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
										.getChavePrimaria());
						pontoConsumoAnterior = listaFaturasAgrupadas.iterator().next().getPontoConsumo();
					}
					if (pontoConsumo == null) {
						Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
										.getChavePrimaria());
						pontoConsumo = listaFaturasAgrupadas.iterator().next().getPontoConsumo();
					}

					if (pontoConsumoAnterior.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
						// Acumulando valores

						if (fatura.getHistoricoConsumo() == null) {
							// fatura agrupada
							Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
											.getChavePrimaria());
							qtdDiasConsumo = qtdDiasConsumo
											+ listaFaturasAgrupadas.iterator().next().getHistoricoConsumo().getDiasConsumo();
							for (Fatura faturaAgrupada : listaFaturasAgrupadas) {
								consumo = consumo.add(faturaAgrupada.getHistoricoConsumo().getConsumoApurado());
							}
						} else {
							// fatura sem ser
							// agrupada
							consumo = consumo.add(fatura.getHistoricoConsumo().getConsumoApurado());
							qtdDiasConsumo = qtdDiasConsumo + fatura.getHistoricoConsumo().getDiasConsumo();
						}
						faturaAnterior = fatura;

					} else {
						String ultimoDia;
						Date dataFim;
						int qtdDiasRestantes = 0;
						// obtendo dias restantes
						if (faturaAnterior.getHistoricoConsumo() == null) {
							Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura
											.consultarFaturaPorFaturaAgrupamento(faturaAnterior.getChavePrimaria());
							ultimoDia = Util.obterUltimoDiaMes(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
											.getHistoricoAtual().getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
							dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
							qtdDiasRestantes = Util.intervaloDatas(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
											.getHistoricoAtual().getDataLeituraFaturada(), dataFim);
						} else {
							ultimoDia = Util.obterUltimoDiaMes(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
											.getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
							dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
							qtdDiasRestantes = Util.intervaloDatas(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
											.getDataLeituraFaturada(), dataFim);
						}

						// calculando o valor
						// medio e o consumo medio
						BigDecimal consumoMedio = consumo.divide(new BigDecimal(qtdDiasConsumo), ESCALA, RoundingMode.HALF_UP);
						BigDecimal consumoMensal = consumoMedio.multiply(
										new BigDecimal(Util.obterQuantidadeDiasPeriodo(referenciaContabil))).setScale(DUAS_CASAS_DECIMAIS,
										RoundingMode.HALF_UP);
						BigDecimal consumoProvisionado = consumoMedio.multiply(new BigDecimal(qtdDiasRestantes)).setScale(DUAS_CASAS_DECIMAIS,
										RoundingMode.HALF_UP);
						BigDecimal consumoFaturado = consumoMensal.subtract(consumoProvisionado);

						totalConsumoGasFaturadoProvisionado = totalConsumoGasFaturadoProvisionado.add(consumoMensal);
						totalConsumoGasProvisionado = totalConsumoGasProvisionado.add(consumoProvisionado);
						totalConsumoGasFaturado = totalConsumoGasFaturado.add(consumoFaturado);

						// Zerando variáveis para
						// processar nova fatura
						consumo = new BigDecimal(0);
						qtdDiasConsumo = 0;

						// Acumulando valores
						if (fatura.getHistoricoConsumo() == null) {
							// fatura agrupada
							Collection<Fatura> listaFaturasAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura
											.getChavePrimaria());
							qtdDiasConsumo = qtdDiasConsumo
											+ listaFaturasAgrupadas.iterator().next().getHistoricoConsumo().getDiasConsumo();
							for (Fatura faturaAgrupada : listaFaturasAgrupadas) {
								consumo = consumo.add(faturaAgrupada.getHistoricoConsumo().getConsumoApurado());
							}
						} else {
							// fatura sem ser
							// agrupada
							consumo = consumo.add(fatura.getHistoricoConsumo().getConsumoApurado());
							qtdDiasConsumo = qtdDiasConsumo + fatura.getHistoricoConsumo().getDiasConsumo();
						}
						faturaAnterior = fatura;
					}
				}
				String ultimoDia;
				Date dataFim;
				int qtdDiasRestantes = 0;
				// obtendo dias restantes
				if (faturaAnterior.getHistoricoConsumo() == null) {
					Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
									.getChavePrimaria());
					ultimoDia = Util.obterUltimoDiaMes(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo().getHistoricoAtual()
									.getDataLeituraFaturada(), Constantes.FORMATO_DATA_BR);
					dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
					qtdDiasRestantes = Util.intervaloDatas(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
									.getHistoricoAtual().getDataLeituraFaturada(), dataFim);
				} else {
					ultimoDia = Util.obterUltimoDiaMes(faturaAnterior.getHistoricoConsumo().getHistoricoAtual().getDataLeituraFaturada(),
									Constantes.FORMATO_DATA_BR);
					dataFim = Util.converterCampoStringParaData(DATA_FIM, ultimoDia, Constantes.FORMATO_DATA_BR);
					qtdDiasRestantes = Util.intervaloDatas(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
									.getDataLeituraFaturada(), dataFim);
				}

				// calculando o valor medio e o
				// consumo medio
				BigDecimal consumoMedio = consumo.divide(new BigDecimal(qtdDiasConsumo), BASE_DIAS_MES, RoundingMode.HALF_UP);
				BigDecimal consumoMensal = consumoMedio.multiply(new BigDecimal(Util.obterQuantidadeDiasPeriodo(referenciaContabil)))
								.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
				BigDecimal consumoProvisionado =
						consumoMedio.multiply(new BigDecimal(qtdDiasRestantes)).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);

				Integer referenciaLeituraAnterior;
				if (faturaAnterior.getHistoricoConsumo() == null) {
					Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(faturaAnterior
									.getChavePrimaria());
					referenciaLeituraAnterior = Util.obterAnoMes(listaFaturasAntAgrupadas.iterator().next().getHistoricoConsumo()
									.getHistoricoAtual().getDataLeituraAnterior());
				} else {
					referenciaLeituraAnterior = Util.obterAnoMes(faturaAnterior.getHistoricoConsumo().getHistoricoAtual()
									.getDataLeituraAnterior());
				}

				BigDecimal consumoFaturado = new BigDecimal(0);
				if (referenciaContabil.toString().equals(referenciaLeituraAnterior.toString())) {
					consumoFaturado = consumo;
				} else {
					consumoFaturado = consumoMensal.subtract(consumoProvisionado);
				}

				totalConsumoGasFaturadoProvisionado = totalConsumoGasFaturadoProvisionado.add(consumoMensal);
				totalConsumoGasProvisionado = totalConsumoGasProvisionado.add(consumoProvisionado);
				totalConsumoGasFaturado = totalConsumoGasFaturado.add(consumoFaturado);
			}

			if (totalConsumoGasFaturadoProvisionado.compareTo(totalConsumoGasComprado) > 0) {

				if (totalConsumoGasFaturado.compareTo(totalConsumoGasComprado) > 0) {
					percentual = new BigDecimal(0);
				} else {
					percentual = totalConsumoGasComprado.subtract(totalConsumoGasFaturado);
					percentual = percentual.divide(totalConsumoGasProvisionado, BASE_DIAS_MES, RoundingMode.HALF_UP);
				}
			}
		}

		return percentual;
	}

	/**
	 * Inserir lancamento contabil vo batch.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param eventoComercial
	 *            the evento comercial
	 * @param valor
	 *            the valor
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void inserirLancamentoContabilVOBatch(Fatura fatura, String eventoComercial, BigDecimal valor, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();
		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		String referenciaContabil = controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL)
						.toString();
		lancamentoContabilVO.setDataRealizacaoEvento(Util.obterUltimoDiaMesDoAnoMesReferencia(referenciaContabil));

		if (fatura.getSegmento() == null) {
			Collection<Fatura> listaFaturasAntAgrupadas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
			lancamentoContabilVO.setSegmento(listaFaturasAntAgrupadas.iterator().next().getSegmento());
		} else {
			lancamentoContabilVO.setSegmento(fatura.getSegmento());
		}
		lancamentoContabilVO.setContaBancaria(null);
		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setDadosAuditoria(dadosAuditoria);
		
		if (fatura.getCliente() != null) {
			lancamentoContabilVO.setCnpj(fatura.getCliente().getCnpj());
		}

		// Como pegar o lancamentoItemContabil

		lancamentoContabilVO.setEventoComercial(controladorContabil.obterEventoComercial(controladorContabil
						.obterChaveEventoComercial(eventoComercial)));

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setCodigoObjeto(fatura.getChavePrimaria());
		lancamentoContabilAnaliticoVO.setValor(valor.setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP));

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(fatura.getCliente());
		ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.CODIGO_TIPO_OPERACAO_SAIDA);
		EntidadeConteudo tipoOperacaoSaida = controladorEntidadeConteudo
						.obterTipoOperacaoDocumentoFiscal(Long.valueOf(parametro.getValor()));
		listaObjetos.add(controladorFatura.obterDocumentoFiscalPorFatura(fatura, tipoOperacaoSaida));
		controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = this.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = this.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);

		if ("EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA".equals(eventoComercial)) {
			registrarLancamentoContabilTributoBatch(lancamentoContabilVO, fatura, valor);
		}

	}

	/**
	 * Registra os lançamentos contábeis
	 * referentes aos tributos da fatura.
	 * 
	 * @param lancamentoContabilVO
	 *            Valores gerais do lançamento
	 *            contábil
	 * @param fatura
	 *            Fatura alvo
	 * @param valor
	 *            the valor
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void registrarLancamentoContabilTributoBatch(LancamentoContabilVO lancamentoContabilVO, Fatura fatura, BigDecimal valor)
					throws NegocioException, ConcorrenciaException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Collection<FaturaTributacao> tributacoes = controladorFatura.consultarFaturaTributacaoPorFatura(fatura.getChavePrimaria());

		if (tributacoes != null && !tributacoes.isEmpty()) {
			lancamentoContabilVO.setEventoComercial(this.obterEventoComercial(this
							.obterChaveEventoComercial("EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA_TRIBUTO")));

			for (FaturaTributacao tributacao : tributacoes) {
				lancamentoContabilVO.setTributo(tributacao.getTributo());
				lancamentoContabilVO.setLancamentoItemContabil(null);

				LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
				lancamentoContabilAnaliticoVO.setCodigoObjeto(fatura.getChavePrimaria());
				lancamentoContabilAnaliticoVO.setValor(valor.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP)
						.multiply(tributacao.getPercentualAliquota())
						.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

				lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
				lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

				// ------------ popular conta Auxiliar e historico contabil
				List<Object> listaObjetos = new ArrayList<>();
				listaObjetos.add(fatura.getCliente());
				ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.CODIGO_TIPO_OPERACAO_SAIDA);
				EntidadeConteudo tipoOperacaoSaida = controladorEntidadeConteudo.obterTipoOperacaoDocumentoFiscal(Long.valueOf(parametro
								.getValor()));
				listaObjetos.add(controladorFatura.obterDocumentoFiscalPorFatura(fatura, tipoOperacaoSaida));
				this.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
				// ---------------------------------------------------

				// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil
				// --------
				Map<String, Object> filtro = this.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
				EventoComercialLancamento eventoComercialLancamento = this.consultarEventoComercialLancamentoPorFiltro(filtro);
				lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
				// ----------------------------------------------------------------------------------------------------------------------------

				this.inserirLancamentoContabilSintetico(lancamentoContabilVO);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil
	 * #processarReclassificacaoPerdaContabil
	 * (java.lang.StringBuilder,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void processarReclassificacaoPerdaContabil(DadosAuditoria dadosAuditoria) throws GGASException {
		ReclassificadorFaturas reclassificador = new ReclassificadorFaturasImpl();
		reclassificador.setValidadores(criarValidadoresPDD());
		reclassificador.setLogProcessamento(logProcessamento);
		reclassificador.setDadosAuditoria(dadosAuditoria);
		reclassificador.reclassificarFaturas();
	}

	private Collection<ValidadorPDD> criarValidadoresPDD() throws NegocioException {
		FabricaValidadoresPDD fabricaValidadores = new FabricaValidadoresPDDImpl();
		ValidadorPDD validadorAnual = fabricaValidadores.criarValidadorPDDAnual(); 
		ValidadorPDD validadorSemestral = fabricaValidadores.criarValidadorPDDSemestral(); 
		return ImmutableSet.of(validadorAnual, validadorSemestral);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#relizarBaixaContasPDD(long, java.lang.String, br.com.ggas.contabil.EventoComercial)
	 */
	@Override
	public void relizarBaixaContasPDD(long idFatura, String codigoOperacao, EventoComercial eventoComercialBaixa) throws NegocioException,
					ConcorrenciaException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Fatura fatura = (Fatura) controladorFatura.obter(idFatura);

		if (fatura != null && fatura.getProvisaoDevedoresDuvidosos() && StringUtils.isNotBlank(codigoOperacao)
						&& eventoComercialBaixa != null) {

			ControladorConstanteSistema constanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
							ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			Long idProvisaoDevDuvMotivoBaixa = Long.valueOf(constanteSistema.obterConstantePorCodigo(codigoOperacao).getValor());

			ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
							ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

			ProvisaoDevDuvMotivoBaixa provisaoDevDuvMotivoBaixa = controladorContabil
							.obterProvisaoDevDuvMotivoBaixa(idProvisaoDevDuvMotivoBaixa);

			Collection<ProvisaoDevedoresDuvidosos> collProvDevDuvidosos = this.listarProvisaoDevedoresDuvidososFatura(idFatura);

			ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
							.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			Integer refContabil = Integer.valueOf(String.valueOf(controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL)));

			for (ProvisaoDevedoresDuvidosos provisaoDevedoresDuvidosos : collProvDevDuvidosos) {

				provisaoDevedoresDuvidosos.setDataBaixa(new Date());
				provisaoDevedoresDuvidosos.setProvisaoDevDuvMotivoBaixa(provisaoDevDuvMotivoBaixa);
				provisaoDevedoresDuvidosos.setAnoMesReferenciaBaixa(refContabil);
				provisaoDevedoresDuvidosos.setEventoComercialBaixa(eventoComercialBaixa);

				this.atualizar(provisaoDevedoresDuvidosos, this.getClasseEntidadeProvisaoDevedoresDuvidosos());

			}

		}

	}

	/**
	 * Listar provisao devedores duvidosos fatura.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<ProvisaoDevedoresDuvidosos> listarProvisaoDevedoresDuvidososFatura(long idFatura) throws NegocioException,
					ConcorrenciaException {

		Criteria criteria = this.createCriteria(this.getClasseEntidadeProvisaoDevedoresDuvidosos());
		criteria.createAlias("fatura", "fatura", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("fatura.chavePrimaria", idFatura));

		return criteria.list();

	}

	/**
	 * Registrar provisao devedores duvidosos classificar perdas.
	 *
	 * @param fatura the fatura
	 * @param dadosAuditoria the dados auditoria
	 * @param eventoComercialInclusao the evento comercial inclusao
	 * @throws GGASException the GGAS exception
	 */
	@Override
	public void registrarProvisaoDevedoresDuvidososClassificarPerdas(Fatura fatura, DadosAuditoria dadosAuditoria,
			EventoComercial eventoComercialInclusao) throws GGASException {
		
		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorArrecadacao controladorArrecadacao =
				(ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		ProvisaoDevedoresDuvidosos provDevDuvidosos = this.criarProvisaoDevedoresDuvidosos();
		provDevDuvidosos.setDadosAuditoria(dadosAuditoria);
		provDevDuvidosos.setFatura(fatura);
		provDevDuvidosos.setEventoComercialInclusao(eventoComercialInclusao);
		provDevDuvidosos.setEventoComercialBaixa(null);

		if (fatura.getSegmento() == null) {

			Collection<Fatura> listaFaturas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());

			if (listaFaturas != null && !listaFaturas.isEmpty()) {

				provDevDuvidosos.setSegmentoPontoConsumo(listaFaturas.iterator().next().getSegmento());
			}

		} else {

			provDevDuvidosos.setSegmentoPontoConsumo(fatura.getSegmento());
		}

		DebitosACobrar debitosACobrarVO = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, new Date(),
				fatura.getValorTotal(), Boolean.TRUE, Boolean.TRUE);

		provDevDuvidosos.setAnoMesReferenciaContabil(Integer.valueOf(
				String.valueOf(controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL))));

		provDevDuvidosos.setValor(fatura.getValorTotal());

		if (debitosACobrarVO != null) {

			provDevDuvidosos.setValorMulta(debitosACobrarVO.getMulta());

			provDevDuvidosos.setValorJuros(debitosACobrarVO.getJurosMora());

		}

		provDevDuvidosos.setAnoMesReferenciaBaixa(null);

		provDevDuvidosos.setDataBaixa(null);

		provDevDuvidosos.setProvisaoDevDuvMotivoBaixa(null);

		super.inserir(provDevDuvidosos);
	}

	// Método para registrar o lançamento contábil
	/**
	 * Registrar lancamento contabil classificar perdas.
	 *
	 * @param fatura            the fatura
	 * @param dadosAuditoria            the dados auditoria
	 * @param eventoComercial the evento comercial
	 * @throws NegocioException             the negocio exception
	 * @throws ConcorrenciaException             the concorrencia exception
	 */
	// de classificação de perda
	public void registrarLancamentoContabilClassificarPerdas(Fatura fatura, DadosAuditoria dadosAuditoria, EventoComercial eventoComercial)
					throws NegocioException, ConcorrenciaException {

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();

		lancamentoContabilVO.setDadosAuditoria(dadosAuditoria);
		lancamentoContabilVO.setEventoComercial(eventoComercial);
		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setLancamentoItemContabil(null);
		if (fatura.getSegmento() == null) {
			Collection<Fatura> listaFaturas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
			if (listaFaturas != null && !listaFaturas.isEmpty()) {
				lancamentoContabilVO.setSegmento(listaFaturas.iterator().next().getSegmento());
			}
		} else {
			lancamentoContabilVO.setSegmento(fatura.getSegmento());
		}

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setCodigoObjeto(fatura.getChavePrimaria());
		lancamentoContabilAnaliticoVO.setValor(fatura.getValorTotal());

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

		String referenciaContabil = controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL)
						.toString();

		lancamentoContabilVO.setDataRealizacaoEvento(Util.obterUltimoDiaMesDoAnoMesReferencia(referenciaContabil));

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(fatura.getCliente());
		ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.CODIGO_TIPO_OPERACAO_SAIDA);
		EntidadeConteudo tipoOperacaoSaida = controladorEntidadeConteudo
						.obterTipoOperacaoDocumentoFiscal(Long.valueOf(parametro.getValor()));
		listaObjetos.add(controladorFatura.obterDocumentoFiscalPorFatura(fatura, tipoOperacaoSaida));
		this.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = this.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = this.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#popularContaCreditoDebitoLancamentoSintetico(java.util.Collection)
	 */
	@Override
	public String popularContaCreditoDebitoLancamentoSintetico(
					Collection<LancamentoContabilSintetico> listaLancamentoContabilSintetico)
					throws NegocioException {

		StringBuilder builder = new StringBuilder();
		
		for (LancamentoContabilSintetico lancamentoContabilSintetico : listaLancamentoContabilSintetico) {

			Map<String, Object> filtro = new HashMap<>();
			EventoComercial eventoComercial = lancamentoContabilSintetico.getEventoComercial();
			filtro.put(FILTRO_ID_EVENTO_COMERCIAL, eventoComercial.getChavePrimaria());
			StringBuilder linhaInconsistencia = new StringBuilder();
			linhaInconsistencia.append(Constantes.STRING_TABULACAO);
			linhaInconsistencia.append(eventoComercial.getDescricao());
			
			Segmento segmento = lancamentoContabilSintetico.getSegmento();
			if (segmento != null) {
				linhaInconsistencia.append(Constantes.STRING_HIFEN_ESPACO);
				linhaInconsistencia.append(LogProcessosUtil
								.gerarMensagemSegmento(segmento.getDescricao()));
				filtro.put(ID_SEGMENTO, segmento.getChavePrimaria());
			}
			
			LancamentoItemContabil lancamentoItemContabil = 
							lancamentoContabilSintetico.getLancamentoItemContabil();
			if (lancamentoItemContabil != null) {
				linhaInconsistencia.append(Constantes.STRING_HIFEN_ESPACO);
				linhaInconsistencia.append(LogProcessosUtil.gerarMensagemItemContabil(
								lancamentoItemContabil.getDescricao()));
				filtro.put(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM, 
								lancamentoItemContabil.getChavePrimaria());
			}
			
			Tributo tributo = lancamentoContabilSintetico.getTributo();
			if (tributo != null) {
				linhaInconsistencia.append(Constantes.STRING_HIFEN_ESPACO);
				linhaInconsistencia.append(LogProcessosUtil.gerarMensagemTributo(
								tributo.getDescricao()));
				filtro.put("idTributo", tributo.getChavePrimaria());
			}
			if (lancamentoContabilSintetico.getContaBancaria() != null) {
				filtro.put(ID_CONTA_BANCARIA, 
								lancamentoContabilSintetico.getContaBancaria().getChavePrimaria());
			}

			EventoComercialLancamento eventoComercialLancamento = 
							this.consultarEventoComercialLancamentoPorFiltro(filtro);
			
			if (eventoComercialLancamento == null) {
				filtro.remove(ID_CONTA_BANCARIA);
				eventoComercialLancamento = 
								this.consultarEventoComercialLancamentoPorFiltro(filtro);
			}

			if (eventoComercialLancamento != null) {
				lancamentoContabilSintetico.setContaContabilCredito(
								eventoComercialLancamento.getContaContabilCredito());
				lancamentoContabilSintetico.setContaContabilDebito(
								eventoComercialLancamento.getContaContabilDebito());
			} else {
				builder.append(linhaInconsistencia.toString());
				builder.append(Constantes.PULA_LINHA);
			}

		}

		return builder.toString();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarEventoComercialLancamentoPorFiltro(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unused")
	public EventoComercialLancamento consultarEventoComercialLancamentoPorFiltro(Map<String, Object> filtro) throws NegocioException {

		Long idEventoComercial = (Long) filtro.get(FILTRO_ID_EVENTO_COMERCIAL);
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		
		if (idSegmento == null) {
			idSegmento = 4L;
		}
		Long idLancamentoItemContabil = (Long) filtro.get(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM);
		Long idTributo = (Long) filtro.get("idTributo");
		Long idContaBancaria = (Long) filtro.get(ID_CONTA_BANCARIA);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeEventoComercialLancamento().getSimpleName());
		hql.append(" eventoComercialLancamento ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilDebito ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilCredito ");

		hql.append(" where 1=1 ");

		if (idEventoComercial != null) {
			hql.append(" and eventoComercialLancamento.eventoComercial.chavePrimaria = :idEventoComercial ");
		}

		// Pesquisa por segmento, caso seja nulo pesquisa por idSegmento = 4L
		hql.append(" and eventoComercialLancamento.segmento.chavePrimaria = :idSegmento ");
	

		if (idLancamentoItemContabil != null) {
			hql.append(" and eventoComercialLancamento.lancamentoItemContabil.chavePrimaria = :idLancamentoItemContabil ");
		}

		if (idTributo != null) {
			hql.append(" and eventoComercialLancamento.tributo.chavePrimaria = :idTributo ");
		}

		if (idContaBancaria != null) {
			hql.append(" and eventoComercialLancamento.contaBancaria.chavePrimaria = :idContaBancaria ");
		} else {
			hql.append(" and eventoComercialLancamento.contaBancaria is null ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idEventoComercial != null) {
			query.setParameter(FILTRO_ID_EVENTO_COMERCIAL, idEventoComercial);
		}

		if (idSegmento != null) {
			query.setParameter(ID_SEGMENTO, idSegmento);
		}

		if (idLancamentoItemContabil != null) {
			query.setParameter(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM, idLancamentoItemContabil);
		}

		if (idTributo != null) {
			query.setParameter("idTributo", idTributo);
		}

		if (idContaBancaria != null) {
			query.setParameter(ID_CONTA_BANCARIA, idContaBancaria);
		}

		@SuppressWarnings("unchecked")
		Collection<EventoComercialLancamento> lista = query.list();

		if (lista == null || lista.isEmpty()) {
			return null;
		} else if (lista.size() > 1) {
			return lista.iterator().next();
		}

		return (EventoComercialLancamento) query.uniqueResult();

	}

	/**
	 * Consulta uma coleção de entidades do tipo
	 * {@link EventoComercialLancamento}, utilizando como filtro de pesquisa, a
	 * chave primária da entidade {@link Segmento}.Retorna um mapa de entidades
	 * do tipo {@link EventoComercialLancamento}, mapeadas por atributos da
	 * mesma. Os seguintes atributos compõem a chave do mapa:
	 * {@link EventoComercial}, {@link LancamentoItemContabil}, {@link Tributo}
	 * e {@link ContaBancaria}.
	 * 
	 * @param chavePrimariaSegmento
	 *            - {@link Long}
	 * @return {@link MultiKeyMap}
	 */
	@Override
	public MultiKeyMap agruparEventoComercialLancamentoPorSegmento(Long chavePrimariaSegmento) {

		Collection<EventoComercialLancamento> listaEventoComercialLancamento = this
				.consultarEventoComercialLancamentoPorSegmento(chavePrimariaSegmento);

		return this.indexarEventoComercialLancamentoPorAtributos(listaEventoComercialLancamento);
	}

	/**
	 * Cria um mapa de entidades do tipo {@link EventoComercialLancamento},
	 * mapeadas por atributos da mesma. Os seguintes atributos compõem a chave
	 * do mapa: {@link EventoComercial}, {@link LancamentoItemContabil},
	 * {@link Tributo} e {@link ContaBancaria}.
	 * 
	 * @param listaEventoComercialLancamento - {@link Collection}
	 * @return {@link MultiKeyMap}
	 */
	private MultiKeyMap indexarEventoComercialLancamentoPorAtributos(
			Collection<EventoComercialLancamento> listaEventoComercialLancamento) {

		MultiKeyMap mapaEventoComercialLancamento = new MultiKeyMap();

		if (!Util.isNullOrEmpty(listaEventoComercialLancamento)) {
			for (EventoComercialLancamento eventoComercialLancamento : listaEventoComercialLancamento) {

				mapaEventoComercialLancamento.put(eventoComercialLancamento.getEventoComercial(),
						eventoComercialLancamento.getLancamentoItemContabil(), eventoComercialLancamento.getTributo(),
						eventoComercialLancamento.getContaBancaria(), eventoComercialLancamento);
			}
		}
		return mapaEventoComercialLancamento;
	}

	/**
	 * Obtém de um {@link MultiKeyMap}, uma entidade do tipo
	 * {@link EventoComercialLancamento}, utilizando como chaves, os atributos
	 * {@link EventoComercial}, {@link ContaBancaria}, {@link Tributo},
	 * {@link LancamentoItemContabil} do {@link EventoComercialLancamento}.
	 * 
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @param eventoComercialLancamentoPorAtributos
	 *            - {@link MultiKeyMap}
	 * @return {@link EventoComercialLancamento}
	 */
	@Override
	public EventoComercialLancamento obterEventoComercialLancamentoPorAtributosMapeados(LancamentoContabilVO lancamentoContabilVO,
					MultiKeyMap eventoComercialLancamentoPorAtributos) {

		EventoComercialLancamento eventoComercialLancamento = null;
		if (Util.isAllNotNull(lancamentoContabilVO, eventoComercialLancamentoPorAtributos)) {
			EventoComercial eventoComercial = lancamentoContabilVO.getEventoComercial();
			ContaBancaria contaBancaria = lancamentoContabilVO.getContaBancaria();
			Tributo tributo = lancamentoContabilVO.getTributo();
			LancamentoItemContabil itemContabil = lancamentoContabilVO.getLancamentoItemContabil();
			eventoComercialLancamento = (EventoComercialLancamento) eventoComercialLancamentoPorAtributos.get(eventoComercial, itemContabil,
							tributo, contaBancaria);
		}

		return eventoComercialLancamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarEventoComercialLancamento()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EventoComercialLancamento> consultarEventoComercialLancamento() {

		StringBuilder hql = new StringBuilder();

		hql.append(" select eventoComercialLancamento from ");
		hql.append(getClasseEntidadeEventoComercialLancamento().getSimpleName());
		hql.append(" eventoComercialLancamento ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilCredito contaContabilCredito ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilDebito contaContabilDebito ");
		hql.append(" inner join fetch eventoComercialLancamento.lancamentoItemContabil itemContabil ");
		hql.append(" inner join fetch eventoComercialLancamento.eventoComercial eventoComercial ");
		hql.append(" inner join fetch eventoComercialLancamento.segmento segmento ");
		hql.append(" inner join fetch eventoComercialLancamento.tributo tributo ");
		hql.append(" inner join fetch eventoComercial.modulo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarEventoComercialLancamentoPorEvento(long)
	 */
	@Override
	@SuppressWarnings({"unchecked"})
	public Collection<EventoComercialLancamento> consultarEventoComercialLancamentoPorEvento(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeEventoComercialLancamento().getSimpleName());
		hql.append(" eventoComercialLancamento ");
		hql.append(" inner join fetch eventoComercialLancamento.eventoComercial eventoComercial ");
		hql.append(" inner join fetch eventoComercial.modulo ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilCredito ");
		hql.append(" inner join fetch eventoComercialLancamento.contaContabilDebito ");
		hql.append(" left join fetch eventoComercialLancamento.lancamentoItemContabil ");
		hql.append(" left join fetch eventoComercialLancamento.segmento ");
		hql.append(" left join fetch eventoComercialLancamento.tributo ");
		hql.append(WHERE);
		hql.append(" eventoComercialLancamento.habilitado = true and");
		hql.append(" eventoComercial.chavePrimaria = :chave ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chave", chavePrimaria);

		return query.list();

	}

	/**
	 * Classe privada reponsável por criar uma
	 * ordenação especial Data Contábil
	 * 
	 * @author ccavalcanti
	 */
	public class OrdenacaoEspecialDataContabil implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc(DATA_CONTABIL));
			} else {
				criteria.addOrder(Order.desc(DATA_CONTABIL));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}
	/**
	 *	Classe responsável pela ordenação especial.
	 *
	 */
	public class OrdenacaoEspecialContaContabilDebito implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("contaContabilDebito.nomeConta"));
			} else {
				criteria.addOrder(Order.desc("contaContabilDebito.nomeConta"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}
	/**
	 *	Classe responsável pela ordenação espeial de conta crédito 
	 *
	 */
	public class OrdenacaoEspecialContaContabilCredito implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("contaContabilCredito.nomeConta"));
			} else {
				criteria.addOrder(Order.desc("contaContabilCredito.nomeConta"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}
	/**
	 *	Classe responsável pela ordenação especial da descrição do modulo.  
	 *
	 */
	public class OrdenacaoEspecialDescricaoModulo implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("eventoComercial.modulo.descricao"));
			} else {
				criteria.addOrder(Order.desc("eventoComercial.modulo.descricao"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}
	/**
	 *	Classe responsável pela ordenação da descrição do segmento. 
	 *
	 */
	public class OrdenacaoEspecialDescricaoSegmento implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("segmento.descricao"));
			} else {
				criteria.addOrder(Order.desc("segmento.descricao"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}
	/**
	 *	Classe responsável pela ordenação especial do evento comercial.  
	 *
	 */
	public class OrdenacaoEspecialDescricaoEventoComercial implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("eventoComercial.descricao"));
			} else {
				criteria.addOrder(Order.desc("eventoComercial.descricao"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#removerEventoComercialLancamento(java.lang.Long)
	 */
	@Override
	public void removerEventoComercialLancamento(Long chavePrimaria) throws NegocioException {

		EventoComercialLancamento eventoComercialLancamento = this.obterEventoComercialLancamento(chavePrimaria);

		super.remover(eventoComercialLancamento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#obterEventoComercialLancamento(long)
	 */
	@Override
	public EventoComercialLancamento obterEventoComercialLancamento(long chavePrimaria) throws NegocioException {

		return (EventoComercialLancamento) super.obter(chavePrimaria, getClasseEntidadeEventoComercialLancamento());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#atualizarEventoComercialLancamento(br.com.ggas.contabil.EventoComercialLancamento)
	 */
	@Override
	public void atualizarEventoComercialLancamento(EventoComercialLancamento evento) throws NegocioException, ConcorrenciaException {

		this.atualizar(evento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#removerEventosComerciaisLancamento(java.util.Collection, java.util.Collection)
	 */
	@Override
	public void removerEventosComerciaisLancamento(Collection<EventoComercialLancamento> lista,
					Collection<EventoComercialLancamento> listaCompleta) throws NegocioException {

		if (lista == null) {
			throw new NegocioException(Constantes.ERRO_DUPLA_SUBMISSAO, EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO);
		} else {
			for (int i = 0; i < lista.size(); i++) {
				EventoComercialLancamento evento = ((List<EventoComercialLancamento>) lista).get(i);
				super.inserir(evento);
			}

			for (int i = 0; i < listaCompleta.size(); i++) {
				EventoComercialLancamento evento = ((List<EventoComercialLancamento>) listaCompleta).get(i);
				super.remover(evento);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contabil.ControladorContabil#validarAlteracaoEventoComercialLancamento(br.com.ggas.contabil.EventoComercialLancamento,
	 * java.util.Collection)
	 */
	@Override
	public void validarAlteracaoEventoComercialLancamento(EventoComercialLancamento lancamento, Collection<EventoComercialLancamento> lista) {

		for (EventoComercialLancamento evento : lista) {
			if (evento.getSegmento() != null
							&& lancamento.getSegmento() != null
							&& evento.getSegmento().getChavePrimaria() == lancamento.getSegmento().getChavePrimaria()
							&& evento.getTributo() != null
							&& lancamento.getTributo() != null
							&& evento.getTributo().getChavePrimaria() == lancamento.getTributo().getChavePrimaria()
							&& evento.getLancamentoItemContabil() != null
							&& lancamento.getLancamentoItemContabil() != null
							&& evento.getLancamentoItemContabil().getChavePrimaria() == lancamento.getLancamentoItemContabil()
											.getChavePrimaria()
							&& evento.getContaContabilCredito() != null
							&& lancamento.getContaContabilCredito() != null
							&& evento.getContaContabilCredito().getChavePrimaria() == lancamento.getContaContabilCredito()
											.getChavePrimaria() && evento.getContaContabilDebito() != null
							&& lancamento.getContaContabilDebito() != null
							&& evento.getContaContabilDebito().getChavePrimaria() == lancamento.getContaContabilDebito().getChavePrimaria()
							&& evento.getDescricaoHistorico() != null && lancamento.getDescricaoHistorico() != null
							&& (evento.getDescricaoHistorico().equalsIgnoreCase(lancamento.getDescricaoHistorico()))
							&& evento.getDescricaoContaAuxiliarDebito() != null && lancamento.getDescricaoContaAuxiliarDebito() != null
							&& (evento.getDescricaoContaAuxiliarDebito().equalsIgnoreCase(lancamento.getDescricaoContaAuxiliarDebito()))
							&& evento.getDescricaoContaAuxiliarCredito() != null && lancamento.getDescricaoContaAuxiliarCredito() != null
							&& (evento.getDescricaoContaAuxiliarCredito().equalsIgnoreCase(lancamento.getDescricaoContaAuxiliarCredito()))) {
				// Metódo incompleto, ainda falta implementação, remover este comentário assim que ela for feita.
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#validarNovoEventoComercialLancamento(br.com.ggas.contabil.EventoComercialLancamento,
	 * java.util.Collection)
	 */
	@Override
	public void validarNovoEventoComercialLancamento(EventoComercialLancamento lancamento, Collection<EventoComercialLancamento> lista)
					throws NegocioException {

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		if (lancamento.getContaContabilCredito() == null && lancamento.getContaContabilDebito() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ContaContabil.CONTA_CONTABIL_DEBITO + ", "
							+ ContaContabil.CONTA_CONTABIL_CREDITO);
		} else if (lancamento.getContaContabilCredito() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ContaContabil.CONTA_CONTABIL_CREDITO);
		} else if (lancamento.getContaContabilDebito() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ContaContabil.CONTA_CONTABIL_DEBITO);
		}

		for (EventoComercialLancamento evento : lista) {

			if ((evento.getSegmento() != null && lancamento.getSegmento() != null && evento.getSegmento().getChavePrimaria() == lancamento
							.getSegmento().getChavePrimaria())
							&& (evento.getTributo() != null && lancamento.getTributo() != null && evento.getTributo().getChavePrimaria() == lancamento
											.getTributo().getChavePrimaria())
							&& (evento.getLancamentoItemContabil() != null && lancamento.getLancamentoItemContabil() != null && evento
											.getLancamentoItemContabil().getChavePrimaria() == lancamento.getLancamentoItemContabil()
											.getChavePrimaria())) {

				throw new NegocioException(Constantes.ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXISTENTE,
								EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO);
			}
		}

		for (EventoComercialLancamento evento : controladorContabil.consultarEventoComercialLancamentoPorEvento(lancamento
						.getEventoComercial().getChavePrimaria())) {

			if ((evento.getSegmento() != null && lancamento.getSegmento() != null && evento.getSegmento().getChavePrimaria() == lancamento
							.getSegmento().getChavePrimaria())
							&& (evento.getTributo() != null && lancamento.getTributo() != null && evento.getTributo().getChavePrimaria() == lancamento
											.getTributo().getChavePrimaria())
							&& (evento.getLancamentoItemContabil() != null && lancamento.getLancamentoItemContabil() != null && evento
											.getLancamentoItemContabil().getChavePrimaria() == lancamento.getLancamentoItemContabil()
											.getChavePrimaria())) {

				throw new NegocioException(Constantes.ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXISTENTE,
								EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarEventoComercialLancamentoPorFiltro(br.com.ggas.contabil.LancamentoContabilVO)
	 */
	@Override
	@SuppressWarnings({"unchecked"})
	public Map<String, Object> consultarEventoComercialLancamentoPorFiltro(LancamentoContabilVO lancamentoContabilVO) {

		Map<String, Object> filtro = new HashMap<>();

		if (lancamentoContabilVO.getEventoComercial() != null) {
			filtro.put(FILTRO_ID_EVENTO_COMERCIAL, lancamentoContabilVO.getEventoComercial().getChavePrimaria());
		}
		if (lancamentoContabilVO.getSegmento() != null) {
			filtro.put(ID_SEGMENTO, lancamentoContabilVO.getSegmento().getChavePrimaria());
		}
		if (lancamentoContabilVO.getLancamentoItemContabil() != null) {
			filtro.put(FILTRO_ID_LANCAMENTO_CONTABIL_ITEM, lancamentoContabilVO.getLancamentoItemContabil().getChavePrimaria());
		}
		if (lancamentoContabilVO.getTributo() != null) {
			filtro.put("idTributo", lancamentoContabilVO.getTributo().getChavePrimaria());
		}
		if (lancamentoContabilVO.getContaBancaria() != null) {
			filtro.put(ID_CONTA_BANCARIA, lancamentoContabilVO.getContaBancaria().getChavePrimaria());
		}

		return filtro;
	}
	

	/**
	 * Consulta uma entidade do tipo {@link EventoComercialLancamento} e popula os atributos de descrição de conta auxiliar, crédito e
	 * débito, e descrição de histórico contabil de um {@link LancamentoContabilVO}.
	 * 
	 * @param eventoComercialLancamento - {@link EventoComercialLancamento}
	 * @param lancamentoContabilVO - {@link LancamentoContabilVO}
	 * @param objetos - {@link List}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public void popularContaAuxiliarHistoricoContabil(LancamentoContabilVO lancamentoContabilVO, List<Object> objetos)
			throws NegocioException {

		try {
			Map<String, Object> filtro = this.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
			EventoComercialLancamento eventoComercialLancamento = this.consultarEventoComercialLancamentoPorFiltro(filtro);
			this.popularContaAuxiliarHistoricoContabil(eventoComercialLancamento, lancamentoContabilVO, objetos, null);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException("popularContaAuxiliarHistoricoContabil: " + e.getMessage());
		}
	}

	/**
	 * Popula os atributos de descrição de conta auxiliar, crédito e débito, e descrição de histórico contabil de um
	 * {@link LancamentoContabilVO}, a partir dos mesmos atributos de descrição de uma entidade do tipo {@link EventoComercialLancamento}.
	 * 
	 * @param eventoComercialLancamento - {@link EventoComercialLancamento}
	 * @param lancamentoContabilVO - {@link LancamentoContabilVO}
	 * @param objetos - {@link List}
	 * @param colunasPorCodigoVariavel - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public void popularContaAuxiliarHistoricoContabil(EventoComercialLancamento eventoComercialLancamento,
			LancamentoContabilVO lancamentoContabilVO, List<Object> objetos, Map<String, Coluna> colunasPorCodigoVariavel)
			throws NegocioException {

		try {
			if (eventoComercialLancamento != null) {
				if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoContaAuxiliarDebito())) {
					String descricaoContaAuxiliarDebito = traduzirVariavel(eventoComercialLancamento, colunasPorCodigoVariavel,
							eventoComercialLancamento.getDescricaoContaAuxiliarDebito(), objetos);
					lancamentoContabilVO.setContaAuxiliarDebito(descricaoContaAuxiliarDebito);
				}
				if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoContaAuxiliarCredito())) {
					String descricaoContaAuxiliarCredito = traduzirVariavel(eventoComercialLancamento, colunasPorCodigoVariavel,
							eventoComercialLancamento.getDescricaoContaAuxiliarCredito(), objetos);
					lancamentoContabilVO.setContaAuxiliarCredito(descricaoContaAuxiliarCredito);
				}
				if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoHistorico())) {
					String descricaoHistorico = traduzirVariavel(eventoComercialLancamento, colunasPorCodigoVariavel,
							eventoComercialLancamento.getDescricaoHistorico(), objetos);
					lancamentoContabilVO.setHistoricoContabil(descricaoHistorico);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException("popularContaAuxiliarHistoricoContabil: " + e.getMessage());
		}
	}
	
	/**
	 * Para cada {@link EventoComercialLancamento}, da coleção passada por parâmetro, realiza um "split" utilizando o separador "#", nas
	 * descrições de histórico e conta auxiliar de crédito e debito e as agrupa em uma estrutura de elementos não duplicados.
	 * 
	 * @param listaEventoComercialLancamento - {@link Collection}
	 * @return {@link Set}
	 */
	@Override
	public Set<String> getDescricoesEventoComercialLancamento(Collection<EventoComercialLancamento> listaEventoComercialLancamento) {

		String separador = "#";
		Set<String> codigos = new HashSet<>();

		for (EventoComercialLancamento eventoComercialLancamento : listaEventoComercialLancamento) {
			if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoContaAuxiliarDebito())) {
				codigos.addAll(Arrays.asList(eventoComercialLancamento.getDescricaoContaAuxiliarDebito().split(separador)));
			}
			if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoContaAuxiliarCredito())) {
				codigos.addAll(Arrays.asList(eventoComercialLancamento.getDescricaoContaAuxiliarCredito().split(separador)));
			}
			if (!StringUtils.isEmpty(eventoComercialLancamento.getDescricaoHistorico())) {
				codigos.addAll(Arrays.asList(eventoComercialLancamento.getDescricaoHistorico().split(separador)));
			}
		}

		return codigos;
	}

	/**
	 * Traduzir variavel.
	 * 
	 * @param eventoComercialLancamento the evento comercial lancamento
	 * @param colunasPorCodigoVariavel - {@link Map}
	 * @param texto the texto
	 * @param objetos the objetos
	 * @return the string
	 * @throws NegocioException the negocio exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws NoSuchMethodException the no such method exception
	 * @throws SecurityException the security exception
	 */
	@SuppressWarnings({ "unchecked" })
	public String traduzirVariavel(EventoComercialLancamento eventoComercialLancamento, Map<String, Coluna> colunasPorCodigoVariavel,
			String texto, List<Object> objetos)
			throws NegocioException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		ControladorAuditoria controladorAuditoria =
				(ControladorAuditoria) ServiceLocator.getInstancia().getBeanPorID(ControladorAuditoria.BEAN_ID_CONTROLADOR_AUDITORIA);
		String retorno = "";
		String[] possiveisVariaveis = texto.split("#");
		Map<String, String> variaveisValor = new HashMap<>();
		List<String> listaVariaveis = new ArrayList<>();
		for (int i = 0; i < possiveisVariaveis.length; i++) {

			if (i % 2 != 0) {
				Coluna coluna = null;

				if (colunasPorCodigoVariavel != null) {
					coluna = colunasPorCodigoVariavel.get(possiveisVariaveis[i]);
				} else {
					coluna = controladorAuditoria.obterColunaTabelaPorVariavel(possiveisVariaveis[i]);
				}

				for (Object objeto : objetos) {

					if (objeto != null) {

						String nomeClasseVariavel = coluna.getTabela().getNomeClasse();
						String nomeClasseObjeto = objeto.getClass().getSuperclass().getName().replace(".impl", "").replace("Impl", "");
						if (coluna.getTabela().getNomeClasse().equals(Util.buscarInterface(objeto.getClass()).getName())
								|| nomeClasseVariavel.equals(nomeClasseObjeto)) {

							Object valorMetodoObj = null;
							Class<?> interfacePendente = null;
							String nomeMetodo = Util.montarNomeGet(coluna.getNomePropriedade());

							if (!("chavePrimaria".equals(coluna.getNomePropriedade()) || "versao".equals(coluna.getNomePropriedade())
									|| "ultimaAlteracao".equals(coluna.getNomePropriedade())
									|| HABILITADO.equals(coluna.getNomePropriedade()))) {
								interfacePendente = Util.buscarInterface(objeto.getClass());
							} else {
								interfacePendente = Util.buscarInterface(EntidadeNegocio.class);
							}

							valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(objeto);
							variaveisValor.put(possiveisVariaveis[i], String.valueOf(valorMetodoObj));
							listaVariaveis.add(possiveisVariaveis[i]);
						}
					}
				}
			}

		}
		retorno = texto;
		for (String variavel : listaVariaveis) {
			retorno = retorno.replace("#" + variavel + "#", variaveisValor.get(variavel));
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContabil#consultarEventoComercialLancamentoPorSegmento(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EventoComercialLancamento> consultarEventoComercialLancamentoPorSegmento(Long idSegmento) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select eventoComercialLancamento from ");
		hql.append(getClasseEntidadeEventoComercialLancamento().getSimpleName());
		hql.append(" eventoComercialLancamento ");
		hql.append(" inner join fetch eventoComercialLancamento.segmento segmento ");
		hql.append(WHERE);
		hql.append(" segmento.chavePrimaria = :idSegmento ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_SEGMENTO, idSegmento);

		return query.list();

	}

	public Map<Long, EventoComercial> getCacheEventoComercial() {

		return cacheEventoComercial;
	}

	public void setCacheEventoComercial(Map<Long, EventoComercial> cacheEventoComercial) {

		this.cacheEventoComercial = cacheEventoComercial;
	}

	@Override
	public StringBuilder getLogProcessamento() {
		return logProcessamento;
	}

	@Override
	public void setLogProcessamento(StringBuilder logProcessamento) {
		this.logProcessamento = logProcessamento;
	}
}
