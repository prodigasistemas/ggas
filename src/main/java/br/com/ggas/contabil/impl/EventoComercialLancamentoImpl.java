/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelos atributos e  implementação dos métodos de Evento Comercial 
 *
 */
public class EventoComercialLancamentoImpl extends EntidadeNegocioImpl implements EventoComercialLancamento {

	private static final long serialVersionUID = 3531961234356691933L;

	private EventoComercial eventoComercial;

	private Segmento segmento;

	private LancamentoItemContabil lancamentoItemContabil;

	private Tributo tributo;

	private ContaBancaria contaBancaria;

	private ContaContabil contaContabilDebito;

	private ContaContabil contaContabilCredito;

	private String descricaoHistorico;

	private String descricaoContaAuxiliarDebito;

	private String descricaoContaAuxiliarCredito;

	private Boolean indicadorRegimeContabilCompetencia;

	@Override
	public EventoComercial getEventoComercial() {

		return eventoComercial;
	}

	@Override
	public void setEventoComercial(EventoComercial eventoComercial) {

		this.eventoComercial = eventoComercial;
	}

	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	@Override
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	@Override
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	@Override
	public Tributo getTributo() {

		return tributo;
	}

	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	@Override
	public ContaBancaria getContaBancaria() {

		return contaBancaria;
	}

	@Override
	public void setContaBancaria(ContaBancaria contaBancaria) {

		this.contaBancaria = contaBancaria;
	}

	@Override
	public ContaContabil getContaContabilDebito() {

		return contaContabilDebito;
	}

	@Override
	public void setContaContabilDebito(ContaContabil contaContabilDebito) {

		this.contaContabilDebito = contaContabilDebito;
	}

	@Override
	public ContaContabil getContaContabilCredito() {

		return contaContabilCredito;
	}

	@Override
	public void setContaContabilCredito(ContaContabil contaContabilCredito) {

		this.contaContabilCredito = contaContabilCredito;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getDescricaoHistorico() {

		return descricaoHistorico;
	}

	@Override
	public void setDescricaoHistorico(String descricaoHistorico) {

		this.descricaoHistorico = descricaoHistorico;
	}

	@Override
	public String getDescricaoContaAuxiliarDebito() {

		return descricaoContaAuxiliarDebito;
	}

	@Override
	public void setDescricaoContaAuxiliarDebito(String descricaoContaAuxiliarDebito) {

		this.descricaoContaAuxiliarDebito = descricaoContaAuxiliarDebito;
	}

	@Override
	public String getDescricaoContaAuxiliarCredito() {

		return descricaoContaAuxiliarCredito;
	}

	@Override
	public void setDescricaoContaAuxiliarCredito(String descricaoContaAuxiliarCredito) {

		this.descricaoContaAuxiliarCredito = descricaoContaAuxiliarCredito;
	}

	@Override
	public Boolean getIndicadorRegimeContabilCompetencia() {

		return indicadorRegimeContabilCompetencia;
	}

	@Override
	public void setIndicadorRegimeContabilCompetencia(Boolean indicadorRegimeContabilCompetencia) {

		this.indicadorRegimeContabilCompetencia = indicadorRegimeContabilCompetencia;
	}

}
