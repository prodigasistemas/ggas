/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.LancamentoContabilAnalitico;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados ao Lançamento Contábil Sintético. 
 *
 */
public class LancamentoContabilSinteticoImpl extends EntidadeNegocioImpl implements LancamentoContabilSintetico {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -3349242384970557635L;

	private Date dataGeracao;

	private Date dataContabil;

	private EventoComercial eventoComercial;

	private Segmento segmento;

	private LancamentoItemContabil lancamentoItemContabil;

	private Tributo tributo;

	private String numeroCNPJ;

	private BigDecimal valor;

	private Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico;

	private ContaBancaria contaBancaria;

	// atributos não persistidos apenas para
	// apresentação na consulta
	private ContaContabil contaContabilCredito;

	private ContaContabil contaContabilDebito;

	private String historico;

	private String contaAuxiliarDebito;

	private String contaAuxiliarCredito;

	private boolean indicadorIntegrado;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #getDataGeracao()
	 */
	@Override
	public Date getDataGeracao() {
		Date data = null;
		if(dataGeracao != null) {
			data = (Date) dataGeracao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setDataGeracao(java.util.Date)
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {
		if(dataGeracao != null) {
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #getDataContabil()
	 */
	@Override
	public Date getDataContabil() {
		Date data = null;
		if(dataContabil != null) {
			data = (Date) dataContabil.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setDataContabil(java.util.Date)
	 */
	@Override
	public void setDataContabil(Date dataContabil) {
		if(dataContabil != null) {
			this.dataContabil = (Date) dataContabil.clone();
		} else {
			this.dataContabil = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #getEventoComercial()
	 */
	@Override
	public EventoComercial getEventoComercial() {

		return eventoComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setEventoComercial
	 * (br.com.ggas.contabil.EventoComercial)
	 */
	@Override
	public void setEventoComercial(EventoComercial eventoComercial) {

		this.eventoComercial = eventoComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico#getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setSegmento(br.com
	 * .ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #getLancamentoItemContabil()
	 */
	@Override
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setLancamentoItemContabil
	 * (br.com.ggas.faturamento
	 * .LancamentoItemContabil)
	 */
	@Override
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico#getTributo()
	 */
	@Override
	public Tributo getTributo() {

		return tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setTributo(br.com
	 * .ggas.faturamento.tributo.Tributo)
	 */
	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico#getNumeroCNPJ()
	 */
	@Override
	public String getNumeroCNPJ() {

		return numeroCNPJ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setNumeroCNPJ(java.lang.String)
	 */
	@Override
	public void setNumeroCNPJ(String numeroCNPJ) {

		this.numeroCNPJ = numeroCNPJ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico#getValor()
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setValor(java.math.BigDecimal)
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #getListaLancamentoContabilAnalitico()
	 */
	@Override
	public Collection<LancamentoContabilAnalitico> getListaLancamentoContabilAnalitico() {

		return listaLancamentoContabilAnalitico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.
	 * LancamentoContabilSintetico
	 * #setListaLancamentoContabilAnalitico
	 * (java.util.Collection)
	 */
	@Override
	public void setListaLancamentoContabilAnalitico(Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico) {

		this.listaLancamentoContabilAnalitico = listaLancamentoContabilAnalitico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(dataGeracao == null) {
			stringBuilder.append(DATA_GERACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataContabil == null) {
			stringBuilder.append(DATA_CONTABIL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valor == null) {
			stringBuilder.append(VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(eventoComercial == null) {
			stringBuilder.append(EVENTO_COMERCIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/**
	 * @param contabancaria
	 */
	@Override
	public void setContaBancaria(ContaBancaria contaBancaria) {

		this.contaBancaria = contaBancaria;
	}

	/**
	 * @return
	 */
	@Override
	public ContaBancaria getContaBancaria() {

		return contaBancaria;
	}

	@Override
	public void setContaContabilCredito(ContaContabil contaContabilCredito) {

		this.contaContabilCredito = contaContabilCredito;
	}

	@Override
	public ContaContabil getContaContabilCredito() {

		return contaContabilCredito;
	}

	@Override
	public void setContaContabilDebito(ContaContabil contaContabilDebito) {

		this.contaContabilDebito = contaContabilDebito;
	}

	@Override
	public ContaContabil getContaContabilDebito() {

		return contaContabilDebito;
	}

	@Override
	public String getHistorico() {

		return historico;
	}

	@Override
	public void setHistorico(String historico) {

		this.historico = historico;
	}

	@Override
	public boolean isIndicadorIntegrado() {

		return indicadorIntegrado;
	}

	@Override
	public void setIndicadorIntegrado(boolean indicadorIntegrado) {

		this.indicadorIntegrado = indicadorIntegrado;
	}

	@Override
	public String getContaAuxiliarDebito() {

		return contaAuxiliarDebito;
	}

	@Override
	public void setContaAuxiliarDebito(String contaAuxiliarDebito) {

		this.contaAuxiliarDebito = contaAuxiliarDebito;
	}

	@Override
	public String getContaAuxiliarCredito() {

		return contaAuxiliarCredito;
	}

	@Override
	public void setContaAuxiliarCredito(String contaAuxiliarCredito) {

		this.contaAuxiliarCredito = contaAuxiliarCredito;
	}

}
