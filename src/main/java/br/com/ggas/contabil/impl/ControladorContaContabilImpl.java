/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.contabil.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.ControladorContaContabil;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

class ControladorContaContabilImpl extends ControladorNegocioImpl implements ControladorContaContabil {

	private static final String WHERE = " where ";
	private static final String FROM = " from ";
	private static final String CAMPO_NUMERO_CONTA = "numeroConta";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContaContabil.BEAN_ID_CONTA_CONTABIL);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContaContabil.BEAN_ID_CONTA_CONTABIL);
	}

	@Override
	public Class<?> getClasseContaContabil() {

		return ServiceLocator.getInstancia().getClassPorID(ContaContabil.BEAN_ID_CONTA_CONTABIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#listarContaContabil(java.util.Map)
	 */
	@Override
	public Collection<ContaContabil> listarContaContabil(Map<String, Object> filtro) throws NegocioException {

		Query query = null;
		String numeroConta = (String) filtro.get(CAMPO_NUMERO_CONTA);
		String nomeConta = (String) filtro.get("nomeConta");
		Boolean habilitado = (Boolean) filtro.get("habilitado");

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseContaContabil().getSimpleName());
		hql.append(" contaContabil");
		hql.append(" where 1=1");

		if (numeroConta != null && !numeroConta.isEmpty()) {
			hql.append(" and replace(numeroConta, '.', '') like '%");
			hql.append(numeroConta.replaceAll("_", ""));
			hql.append("%' ");
		}

		if (nomeConta != null && !nomeConta.isEmpty()) {
			hql.append(" and nomeConta like '%");
			hql.append(nomeConta);
			hql.append("%' ");
		}
		if (habilitado != null) {
			hql.append(" and habilitado = :habilitado");
		}

		hql.append(" order by numeroConta asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (habilitado != null) {
			query.setBoolean("habilitado", habilitado);
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#obterContaContabil(java.util.Map)
	 */
	@Override
	public ContaContabil obterContaContabil(Map<String, Object> filtro) {

		String numeroConta = (String) filtro.get(CAMPO_NUMERO_CONTA);
		Criteria criteria = createCriteria(ContaContabil.class);
		criteria.add(Restrictions.eq(CAMPO_NUMERO_CONTA, numeroConta));

		return (ContaContabil) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorContaContabil#validarDadosContaContabil(br.com.ggas.contabil.ContaContabil)
	 */
	@Override
	public void validarDadosContaContabil(ContaContabil contaContabil) throws NegocioException {

		Map<String, Object> errosContaContabil = contaContabil.validarDados();

		if (errosContaContabil != null && !errosContaContabil.isEmpty()) {
			throw new NegocioException(errosContaContabil);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#obterContaContabil(long)
	 */
	@Override
	public ContaContabil obterContaContabil(long chavePrimaria) throws NegocioException {

		return (ContaContabil) super.obter(chavePrimaria, getClasseContaContabil());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#contaContabil(long)
	 */
	@Override
	public ContaContabil contaContabil(long chavePrimaria) throws NegocioException {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#atualizarContaContabil(br.com.ggas.contabil.ContaContabil)
	 */
	@Override
	public void atualizarContaContabil(ContaContabil contaContabil) throws NegocioException, ConcorrenciaException {

		this.atualizar(contaContabil, ContaContabilImpl.class);
	}

	/**
	 * Validar chave estrangeira.
	 * 
	 * @param nomeColuna the nome coluna
	 * @param nomeTabela the nome tabela
	 * @param chaveEstrangeira the chave estrangeira
	 * @return the boolean
	 */
	public Boolean validarChaveEstrangeira(String nomeColuna, String nomeTabela, Long chaveEstrangeira) {

		StringBuilder hql;
		Query query;
		hql = new StringBuilder();
		hql.append(" select count(*)  ");
		if (nomeTabela.equals(Segmento.BEAN_ID_SEGMENTO)) {
			hql.append(" from segmento");
			hql.append(WHERE);
			hql.append(nomeColuna);
			hql.append(" = :chaveEstrangeira and segm_in_uso = 1");
			
		} else if (nomeTabela.equals(LancamentoItemContabil.BEAN_ID_LANCAMENTO_ITEM_CONTABIL)) {
			hql.append(" from LANCAMENTO_CONTABIL_ITEM");
			hql.append(WHERE);
			hql.append(nomeColuna);
			hql.append(" = :chaveEstrangeira and laci_in_uso = 1");
			
		} else if (nomeTabela.equals(EventoComercialLancamento.BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO)) {
			hql.append(" from EVENTO_COMERCIAL_LANCAMENTO ");
			hql.append(WHERE);
			hql.append(nomeColuna);
			hql.append(" = :chaveEstrangeira and evcl_in_uso = 1");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		query.setLong("chaveEstrangeira", chaveEstrangeira);
		return "0".equals(query.uniqueResult().toString());
	}

	/**
	 * Validar remocao entidade.
	 * 
	 * @param nomeColuna
	 *            the nome coluna
	 * @param nomeTabela
	 *            the nome tabela
	 * @param chaveEstrangeira
	 *            the chave estrangeira
	 * @return the boolean
	 */
	public Boolean validarRemocaoEntidade(String nomeColuna, String nomeTabela, Long chaveEstrangeira) {

		List<String> listaNomeTabela = null;
		Boolean possuiRegistro = Boolean.FALSE;

		StringBuilder hql;
		Query query;
		hql = new StringBuilder();
		hql.append(" select table_name  ");
		hql.append(" from all_tab_columns ");
		hql.append(" where column_name = :nomeColuna and table_name != :nomeTabela ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());

		query.setString("nomeColuna", nomeColuna);
		query.setString("nomeTabela", nomeTabela);

		listaNomeTabela = query.list();

		if (listaNomeTabela != null && !listaNomeTabela.isEmpty()) {

			for (Iterator<String> iterator = listaNomeTabela.iterator(); iterator.hasNext();) {

				String nomeTabelaAssociada = iterator.next();
				possuiRegistro = this.validarChaveEstrangeira(nomeColuna, nomeTabelaAssociada, chaveEstrangeira);

				if (possuiRegistro) {
					break;
				}
			}
		}
		return possuiRegistro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#listarContaContabil()
	 */
	@Override
	public Collection<ContaContabil> listarContaContabil() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseContaContabil().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by nomeConta ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Conta ja existe.
	 * 
	 * @param contaContabil
	 *            the conta contabil
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void contaJaExiste(ContaContabil contaContabil) throws GGASException {

		Query query = null;

		String numeroConta = contaContabil.getNumeroConta();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseContaContabil().getSimpleName());
		hql.append(" where numeroConta = :numeroConta");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (numeroConta != null && !numeroConta.isEmpty()) {
			query.setString(CAMPO_NUMERO_CONTA, numeroConta);
		}

		if (query.uniqueResult() != null) {
			throw new GGASException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contabil.ControladorContaContabil#inserirContaContabil(br.com.ggas.contabil.ContaContabil)
	 */
	@Override
	public Long inserirContaContabil(ContaContabil contaContabil) throws GGASException {

		contaJaExiste(contaContabil);

		return super.inserir(contaContabil);
	}

}
