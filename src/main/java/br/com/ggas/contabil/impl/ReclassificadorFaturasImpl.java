package br.com.ggas.contabil.impl;

import java.util.Collection;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.ReclassificadorFaturas;
import br.com.ggas.contabil.ValidadorPDD;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável por implementar os métodos relacionados a Reclassificação das Faturas
 *
 */
public class ReclassificadorFaturasImpl implements ReclassificadorFaturas {

	private StringBuilder logProcessamento;
	
	private Collection<ValidadorPDD> validadores;
	
	private DadosAuditoria dadosAuditoria;
	
	private ControladorFatura controladorFatura;
	
	private ControladorContabil controladorContabil;
	
	/**
	 * Constrói uma instância desta classe obtendo o controlador fatura e o controlador contábil.
	 *
	 */
	public ReclassificadorFaturasImpl() {
		controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
				ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
				ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.contabil.ReclassificadorFaturas#reclassificarFaturas()
	 */
	@Override
	public void reclassificarFaturas() throws GGASException {
		
		logProcessamento.append("Reclassificação Perda Contábil...\r\n");
		logProcessamento.append("Obtendo lista de faturas para reclassificação de perda contábil. \r\n");
		Collection<Fatura> listaFaturas = controladorFatura.consultarFaturasAReclassificar();
		
		for (Fatura fatura : listaFaturas) {
			logProcessamento.append("Processando Fatura ");
			logProcessamento.append(fatura.getChavePrimaria());
			logProcessamento.append("\r\n");
			processarFatura(fatura);
		}
	}

	private void processarFatura(Fatura fatura) throws GGASException {
		for(ValidadorPDD validador : validadores) {
			logProcessamento.append(validador.getMensagemVerificacao());
			if(validador.caracterizaPDD(fatura)) {
				persistirPDD(fatura, validador.getEventoComercial());
			}
		}
	}

	private void persistirPDD(Fatura fatura, EventoComercial eventoComercial) throws GGASException {
		
		atualizarFatura(fatura);
		registrarLancamentoContabil(fatura, eventoComercial);
	}

	private void registrarLancamentoContabil(Fatura fatura, EventoComercial eventoComercial) throws GGASException {
		
		logProcessamento.append("Registrando lançamento contábil de perda da Fatura ");
		logProcessamento.append(fatura.getChavePrimaria());
		logProcessamento.append("\r\n");
		controladorContabil.registrarLancamentoContabilClassificarPerdas(fatura, dadosAuditoria, eventoComercial);
		controladorContabil.registrarProvisaoDevedoresDuvidososClassificarPerdas(fatura, dadosAuditoria, eventoComercial);
		logProcessamento.append("Lançamento contábil registrado. \r\n");
	}

	private void atualizarFatura(Fatura fatura) throws ConcorrenciaException, NegocioException {
		
		logProcessamento.append("Atualizando a Fatura ");
		logProcessamento.append(fatura.getChavePrimaria());
		logProcessamento.append("\r\n");
		fatura.setRegistroPerda(Boolean.TRUE);
		fatura.setProvisaoDevedoresDuvidosos(Boolean.TRUE);
		fatura.setDadosAuditoria(dadosAuditoria);
		controladorFatura.atualizar(fatura);
	}

	@Override
	public StringBuilder getLogProcessamento() {
		return logProcessamento;
	}

	@Override
	public void setLogProcessamento(StringBuilder logProcessamento) {
		this.logProcessamento = logProcessamento;
	}

	@Override
	public Collection<ValidadorPDD> getValidadores() {
		return validadores;
	}

	@Override
	public void setValidadores(Collection<ValidadorPDD> validadores) {
		this.validadores = validadores;
	}

	@Override
	public DadosAuditoria getDadosAuditoria() {
		return dadosAuditoria;
	}

	@Override
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {
		this.dadosAuditoria = dadosAuditoria;
	}
	
}
