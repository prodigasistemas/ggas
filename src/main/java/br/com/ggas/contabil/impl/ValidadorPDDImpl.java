package br.com.ggas.contabil.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Period;

import com.google.common.collect.Range;

import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.ValidadorPDD;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável por válidar PDDI
 */
public class ValidadorPDDImpl implements ValidadorPDD {
	
	private Range<BigDecimal> limites;
	
	private Period periodo;
	
	private String valorParametroReferenciaContabil;
	
	private String mensagemVerificacao;
	
	private EventoComercial eventoComercial;
	
	/**
	 * Constrói uma instância desta classe obtendo controlador parametro sistema e controlador parametro sistema.
	 * 
	 *@throws NegocioException
	 *			the negocio exception
	 */
	public ValidadorPDDImpl() throws NegocioException {
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) 
				ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		valorParametroReferenciaContabil = controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL)
				.toString();
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.contabil.ValidadorPDD#caracterizaPDD(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public boolean caracterizaPDD(Fatura fatura) {
		Date dataLimite = new DateTime(
				Util.obterUltimoDiaMesDoAnoMesReferencia(valorParametroReferenciaContabil))
				.minus(periodo)
				.toDate();
		return fatura.getDataVencimento().before(dataLimite) &&
				limites.contains(fatura.getValorTotal());
	}
	
	@Override
	public Range<BigDecimal> getLimites() {
		return limites;
	}
	
	@Override
	public void setLimites(Range<BigDecimal> limites) {
		this.limites = limites;
	}

	@Override
	public Period getPeriodo() {
		return periodo;
	}

	@Override
	public void setPeriodo(Period periodo) {
		this.periodo = periodo;
	}

	@Override
	public String getMensagemVerificacao() {
		return mensagemVerificacao;
	}

	@Override
	public void setMensagemVerificacao(String mensagemVerificacao) {
		this.mensagemVerificacao = mensagemVerificacao;
	}

	@Override
	public EventoComercial getEventoComercial() {
		return eventoComercial;
	}

	@Override
	public void setEventoComercial(EventoComercial eventoComercial) {
		this.eventoComercial = eventoComercial;
	}

}
