package br.com.ggas.contabil.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import org.joda.time.Period;

import com.google.common.collect.Range;

import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.FabricaValidadoresPDD;
import br.com.ggas.contabil.ValidadorPDD;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável pela Fabrica Validadores
 *
 */
public class FabricaValidadoresPDDImpl implements FabricaValidadoresPDD {
	
	private static final String EVENTO_COMERCIAL_PERDA_CREDITO_ANUAL = "EVENTO_COMERCIAL_PERDA_CREDITO_ANUAL";

	private static final String EVENTO_COMERCIAL_PERDA_CREDITO_SEMESTRAL = "EVENTO_COMERCIAL_PERDA_CREDITO_SEMESTRAL";

	private static final int PDD_VENCIMENTO_MESES = 6;

	private static final int PDD_VENCIMENTO_ANOS = 1;
	
	/** The Constant MENSAGEM_VALIDACAO_PDD_SEMESTRAL. */
	public static final String MENSAGEM_VALIDACAO_PDD_SEMESTRAL = "Verificando perda semestral. \r\n";
	
	/** The Constant MENSAGEM_VALIDACAO_PDD_ANUAL. */
	public static final String MENSAGEM_VALIDACAO_PDD_ANUAL = "Verificando perda anual. \r\n";
	
	private ControladorParametroSistema controladorParametroSistema;
	private ControladorContabil controladorContabil;

	private DecimalFormat formatador;
	
	/**
	 * Constrói uma instância desta classe obtendo o formatador, o controlador parametro sistema
	 * e controlador contabil.
	 *
	 */
	public FabricaValidadoresPDDImpl() {
		formatador = new DecimalFormat(Constantes.FORMATO_VALOR_MONETARIO_BR, DecimalFormatSymbols.getInstance(Constantes.LOCALE_PADRAO));
		controladorParametroSistema = (ControladorParametroSistema)ServiceLocator
				.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		controladorContabil = (ControladorContabil)ServiceLocator
				.getInstancia()
				.getBeanPorID(ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.contabil.FabricaValidadoresPDD#criarValidadorPDDAnual()
	 */
	@Override
	public ValidadorPDD criarValidadorPDDAnual() throws NegocioException {
		ValidadorPDD validadorAnual = new ValidadorPDDImpl();
		validadorAnual.setLimites(criarLimitesPDDAnual());
		validadorAnual.setPeriodo(Period.years(PDD_VENCIMENTO_ANOS));
		validadorAnual.setMensagemVerificacao(MENSAGEM_VALIDACAO_PDD_ANUAL);
		validadorAnual.setEventoComercial(obterEventoComercialPDD(EVENTO_COMERCIAL_PERDA_CREDITO_ANUAL));
		return validadorAnual;
	}
	

	/* (non-Javadoc)
	 * @see br.com.ggas.contabil.FabricaValidadoresPDD#criarValidadorPDDSemestral()
	 */
	@Override
	public ValidadorPDD criarValidadorPDDSemestral() throws NegocioException {
		ValidadorPDD validadorSemestral = new ValidadorPDDImpl();
		validadorSemestral.setLimites(criarLimitesPDDSemestral());
		validadorSemestral.setPeriodo(Period.months(PDD_VENCIMENTO_MESES));
		validadorSemestral.setMensagemVerificacao(MENSAGEM_VALIDACAO_PDD_SEMESTRAL);
		validadorSemestral.setEventoComercial(obterEventoComercialPDD(EVENTO_COMERCIAL_PERDA_CREDITO_SEMESTRAL));
		return validadorSemestral;
	}
	
	private EventoComercial obterEventoComercialPDD(String codigoEventoComercial) throws NegocioException {
		return controladorContabil.obterEventoComercial(
				controladorContabil.obterChaveEventoComercial(codigoEventoComercial));
	}

	private Range<BigDecimal> criarLimitesPDDAnual() throws NegocioException {
		return Range.openClosed(
				obterLimite(Constantes.PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL),
				obterLimite(Constantes.PARAMETRO_PDD_VALOR_LIMITE_ANUAL));
	}

	private Range<BigDecimal> criarLimitesPDDSemestral() throws NegocioException {
		return Range.openClosed(
				BigDecimal.ZERO, 
				obterLimite(Constantes.PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL));
	}
	
	private BigDecimal obterLimite(String codigoConstante) throws NegocioException {
		String limiteStr = controladorParametroSistema
				.obterValorDoParametroPorCodigo(codigoConstante)
				.toString();
		try {
			return new BigDecimal(formatador.parse(limiteStr).toString());
		} catch (ParseException e) {
			throw new NegocioException(e);
		}
	}

	public DecimalFormat getFormatador() {
		return formatador;
	}

	public void setFormatador(DecimalFormat formatador) {
		this.formatador = formatador;
	}
	
}
