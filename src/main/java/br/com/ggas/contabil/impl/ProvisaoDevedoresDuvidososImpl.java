/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.ProvisaoDevDuvMotivoBaixa;
import br.com.ggas.contabil.ProvisaoDevedoresDuvidosos;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável por implementar os métodos relacionados a Provisão de Devedores Duvidosos
 *
 */
public class ProvisaoDevedoresDuvidososImpl extends EntidadeNegocioImpl implements ProvisaoDevedoresDuvidosos {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Fatura fatura;

	private EventoComercial eventoComercialInclusao;

	private EventoComercial eventoComercialBaixa;

	private Segmento segmentoPontoConsumo;

	private Integer anoMesReferenciaContabil;

	private BigDecimal valor;

	private BigDecimal valorMulta;

	private BigDecimal valorJuros;

	private Date dataBaixa;

	private Integer anoMesReferenciaBaixa;

	private ProvisaoDevDuvMotivoBaixa provisaoDevDuvMotivoBaixa;

	@Override
	public Fatura getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	@Override
	public EventoComercial getEventoComercialInclusao() {

		return eventoComercialInclusao;
	}

	@Override
	public void setEventoComercialInclusao(EventoComercial eventoComercialInclusao) {

		this.eventoComercialInclusao = eventoComercialInclusao;
	}

	@Override
	public EventoComercial getEventoComercialBaixa() {

		return eventoComercialBaixa;
	}

	@Override
	public void setEventoComercialBaixa(EventoComercial eventoComercialBaixa) {

		this.eventoComercialBaixa = eventoComercialBaixa;
	}

	@Override
	public Segmento getSegmentoPontoConsumo() {

		return segmentoPontoConsumo;
	}

	@Override
	public void setSegmentoPontoConsumo(Segmento segmentoPontoConsumo) {

		this.segmentoPontoConsumo = segmentoPontoConsumo;
	}

	@Override
	public Integer getAnoMesReferenciaContabil() {

		return anoMesReferenciaContabil;
	}

	@Override
	public void setAnoMesReferenciaContabil(Integer anoMesReferenciaContabil) {

		this.anoMesReferenciaContabil = anoMesReferenciaContabil;
	}

	@Override
	public BigDecimal getValor() {

		return valor;
	}

	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	@Override
	public BigDecimal getValorMulta() {

		return valorMulta;
	}

	@Override
	public void setValorMulta(BigDecimal valorMulta) {

		this.valorMulta = valorMulta;
	}

	@Override
	public BigDecimal getValorJuros() {

		return valorJuros;
	}

	@Override
	public void setValorJuros(BigDecimal valorJuros) {

		this.valorJuros = valorJuros;
	}

	@Override
	public Date getDataBaixa() {

		Date data = null;
		if(dataBaixa != null) {
			data = (Date) dataBaixa.clone();
		}
		return data;
	}

	@Override
	public void setDataBaixa(Date dataBaixa) {
		if(dataBaixa != null){
			this.dataBaixa = (Date) dataBaixa.clone();
		} else {
			this.dataBaixa = null;
		}
	}

	@Override
	public Integer getAnoMesReferenciaBaixa() {

		return anoMesReferenciaBaixa;
	}

	@Override
	public void setAnoMesReferenciaBaixa(Integer anoMesReferenciaBaixa) {

		this.anoMesReferenciaBaixa = anoMesReferenciaBaixa;
	}

	@Override
	public ProvisaoDevDuvMotivoBaixa getProvisaoDevDuvMotivoBaixa() {

		return provisaoDevDuvMotivoBaixa;
	}

	@Override
	public void setProvisaoDevDuvMotivoBaixa(ProvisaoDevDuvMotivoBaixa provisaoDevDuvMotivoBaixa) {

		this.provisaoDevDuvMotivoBaixa = provisaoDevDuvMotivoBaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
