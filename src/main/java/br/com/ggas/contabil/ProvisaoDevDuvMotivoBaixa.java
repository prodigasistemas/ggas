
package br.com.ggas.contabil;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ProvisaoDevDuvMotivoBaixa.
 */
public interface ProvisaoDevDuvMotivoBaixa extends EntidadeNegocio {

	/** The bean id provisao dev duv motivo baixa. */
	String BEAN_ID_PROVISAO_DEV_DUV_MOTIVO_BAIXA = "provisaoDevDuvMotivoBaixa";

	/** The descricao. */
	String DESCRICAO = "PROV_DEV_DUV_MOTIVO_BAIXA_DESCRICAO";

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao the new descricao
	 */
	void setDescricao(String descricao);

}
