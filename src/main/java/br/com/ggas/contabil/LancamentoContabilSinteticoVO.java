/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;

/**
 * Classe responsável pela representação da entidade LancamentoContabilSintetico
 *
 */
public class LancamentoContabilSinteticoVO implements Serializable {

	private static final long serialVersionUID = -3897177845318827985L;

	private long chavePrimaria;

	private Date dataGeracao;

	private Date dataContabil;

	private EventoComercial eventoComercial;

	private Segmento segmento;

	private LancamentoItemContabil lancamentoItemContabil;

	private Tributo tributo;

	private String cnpj;

	private ContaContabil contaContabilCredito;

	private ContaContabil contaContabilDebito;

	private BigDecimal valor;

	private Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico = new HashSet<>();

	private Long numeroNotaFiscal;

	/**
	 * @return the eventoComercial
	 */
	public EventoComercial getEventoComercial() {

		return eventoComercial;
	}

	/**
	 * @param eventoComercial
	 *            the eventoComercial to set
	 */
	public void setEventoComercial(EventoComercial eventoComercial) {

		this.eventoComercial = eventoComercial;
	}

	/**
	 * @return the segmento
	 */
	public Segmento getSegmento() {

		return segmento;
	}

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/**
	 * @return the lancamentoItemContabil
	 */
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	/**
	 * @param lancamentoItemContabil
	 *            the lancamentoItemContabil to
	 *            set
	 */
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	/**
	 * @return the tributo
	 */
	public Tributo getTributo() {

		return tributo;
	}

	/**
	 * @param tributo
	 *            the tributo to set
	 */
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/**
	 * @return the cnpj
	 */
	public String getCnpj() {

		return cnpj;
	}

	/**
	 * @param cnpj
	 *            the cnpj to set
	 */
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	public Date getDataGeracao() {
		Date data = null;
		if (dataGeracao != null) {
			data = (Date) dataGeracao.clone();
		}
		return data;
	}

	public void setDataGeracao(Date dataGeracao) {
		if(dataGeracao != null) {
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
	}

	public Date getDataContabil() {
		Date data = null;
		if (dataContabil != null) {
			data = (Date) dataContabil.clone();
		}
		return data;
	}

	public void setDataContabil(Date dataContabil) {
		if(dataContabil != null) {
			this.dataContabil = (Date) dataContabil.clone();
		} else {
			this.dataContabil = null;
		}
	}

	public Long getNumeroNotaFiscal() {

		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(Long numeroNotaFiscal) {

		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	public Collection<LancamentoContabilAnalitico> getListaLancamentoContabilAnalitico() {

		return listaLancamentoContabilAnalitico;
	}

	public void setListaLancamentoContabilAnalitico(Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico) {

		this.listaLancamentoContabilAnalitico = listaLancamentoContabilAnalitico;
	}

	public BigDecimal getValor() {

		return valor;
	}

	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	public long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public ContaContabil getContaContabilCredito() {

		return contaContabilCredito;
	}

	public void setContaContabilCredito(ContaContabil contaContabilCredito) {

		this.contaContabilCredito = contaContabilCredito;
	}

	public ContaContabil getContaContabilDebito() {

		return contaContabilDebito;
	}

	public void setContaContabilDebito(ContaContabil contaContabilDebito) {

		this.contaContabilDebito = contaContabilDebito;
	}

}
