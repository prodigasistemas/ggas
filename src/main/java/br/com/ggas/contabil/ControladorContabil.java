/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contabil;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiKeyMap;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * The Interface ControladorContabil.
 */
public interface ControladorContabil extends ControladorNegocio {

	/** The bean id controlador contabil. */
	String BEAN_ID_CONTROLADOR_CONTABIL = "controladorContabil";

	/** The campos obrigatorios. */
	String CAMPOS_OBRIGATORIOS = "Campos Obrigatório não preenchidos";

	/**
	 * Criar lancamento contabil sintetico.
	 * 
	 * @return the LancamentoContabilSintetico
	 */
	LancamentoContabilSintetico criarLancamentoContabilSintetico();

	/**
	 * Criar lancamento contabil analitico.
	 * 
	 * @return the LancamentoContabilAnalitico
	 */
	LancamentoContabilAnalitico criarLancamentoContabilAnalitico();

	/**
	 * Criar evento comercial.
	 * 
	 * @return the EventoComercial
	 */
	EventoComercial criarEventoComercial();

	/**
	 * Criar evento comercial lancamento.
	 * 
	 * @return the EventoComercial
	 */
	EventoComercialLancamento criarEventoComercialLancamento();

	/**
	 * Criar provisao devedores duvidosos.
	 * 
	 * @return the ProvisaoDevedoresDuvidosos
	 */
	ProvisaoDevedoresDuvidosos criarProvisaoDevedoresDuvidosos();

	/**
	 * Gets the classe entidade lancamento contabil sintetico.
	 *
	 * @return the LancamentoContabilSintetico class
	 */
	Class<?> getClasseEntidadeLancamentoContabilSintetico();

	/**
	 * Gets the classe entidade lancamento contabil analitico.
	 *
	 * @return the LancamentoContabilAnalitico class
	 */
	Class<?> getClasseEntidadeLancamentoContabilAnalitico();

	/**
	 * Gets the classe entidade evento comercial.
	 *
	 * @return the EventoComercial class
	 */
	Class<?> getClasseEntidadeEventoComercial();

	/**
	 * Gets the classe entidade evento comercial lancamento.
	 *
	 * @return the EventoComercialLancamento class
	 */
	Class<?> getClasseEntidadeEventoComercialLancamento();

	/**
	 * Gets the classe entidade provisao devedores duvidosos.
	 *
	 * @return the ProvisaoDevedoresDuvidosos class
	 */
	Class<?> getClasseEntidadeProvisaoDevedoresDuvidosos();

	/**
	 * Gets the classe entidade provisao dev duv motivo baixa.
	 *
	 * @return the ProvisaoDevDuvMotivoBaixa
	 */
	Class<?> getClasseEntidadeProvisaoDevDuvMotivoBaixa();

	/**
	 * Obter lancamento contabil sintetico.
	 * 
	 * @param chavePrimaria
	 *            a chave do LancamentoContabilSintetico
	 * @param propriedadesLazy
	 *            as propriedades non lazy
	 * @return the LancamentoContabilSintetico
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	LancamentoContabilSintetico obterLancamentoContabilSintetico(long chavePrimaria, String... propriedadesLazy)
			throws NegocioException;

	/**
	 * Obter lancamento contabil analitico.
	 * 
	 * @param chavePrimaria
	 *            a chave do LancamentoContabilAnalitico
	 * @param propriedadesLazy
	 *            as propriedades non lazy
	 * @return the LancamentoContabilAnalitico
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	LancamentoContabilAnalitico obterLancamentoContabilAnalitico(long chavePrimaria, String... propriedadesLazy)
			throws NegocioException;

	/**
	 * Obter evento comercial.
	 * 
	 * @param chavePrimaria
	 *            a chave do EventoComercial
	 * @param propriedadesLazy
	 *            as propriedades non lazy
	 * @return the EventoComercial
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	EventoComercial obterEventoComercial(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Obter provisao dev duv motivo baixa.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the provisao dev duv motivo baixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ProvisaoDevDuvMotivoBaixa obterProvisaoDevDuvMotivoBaixa(Long chavePrimaria, String... propriedadesLazy)
			throws NegocioException;

	/**
	 * Recuera do mapa de eventos o valor da chave do evento comercial.
	 * 
	 * @param codigoEventoComercial
	 *            Código de mapeamento
	 * @return valorChave
	 */
	Long obterChaveEventoComercial(String codigoEventoComercial);

	/**
	 * Inserir lancamento contabil sintetico.
	 * 
	 * @param lancamentoContabilVO
	 *            o VO de LancamentoContabilSintetico
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void inserirLancamentoContabilSintetico(LancamentoContabilVO lancamentoContabilVO)
			throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar lancamento contabil sintetico habilitado nao integrado.
	 *
	 * @return the collection
	 */
	Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoHabilitadoNaoIntegrado();

	/**
	 * Método responsável por consultar os LancamentoContabilSintetico apartir
	 * de um filtro.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os LancamentoContabilSintetico que se enquadram nos parâmetros
	 *         passados no filtro
	 */
	Collection<LancamentoContabilSintetico> consultarLancamentoContabilSintetico(Map<String, Object> filtro);

	/**
	 * Método responsável por consultar LancamentoContabilAnalitico por
	 * LancamentoContabilSintetico.
	 * 
	 * @param lancamentoContabilSintetico
	 *            the lancamento contabil sintetico
	 * @return the collection
	 */
	Collection<LancamentoContabilAnalitico> consultarLancamentoContabilAnaliticoPorLancamentoContabilSintetico(
			LancamentoContabilSintetico lancamentoContabilSintetico);

	/**
	 * Método responsável por consultar todos os eventos comerciais apartir de
	 * um filtro.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os EventoComercial que se enquadram nos parâmetros passados no
	 *         filtro
	 */
	Collection<EventoComercial> consultarTodosEventosComerciais(Map<String, Object> filtro);

	/**
	 * Método responsável por obter a chave do evento comercial de acordo com a
	 * entidade e a operação a realizar.
	 * 
	 * @param classe
	 *            a classe da entidade
	 * @param rubrica
	 *            a rubrica
	 * @param operacao
	 *            a operação
	 * @param isCurtoPrazo
	 *            indicador de curto prazo
	 * @return a chave do evento comercial
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	String obterChaveEventoComercial(Class<?> classe, Rubrica rubrica, OperacaoContabil operacao, Boolean isCurtoPrazo)
			throws NegocioException;

	/**
	 * Método responsável pelas operações do batch
	 * EncerrarReferenciaContabilFaturamento.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void encerrarReferenciaContabil(DadosAuditoria dadosAuditoria) throws IllegalAccessException,
			InstantiationException, InvocationTargetException, NoSuchMethodException, GGASException;

	/**
	 * Método para processar a classificação contábil de perda.
	 *
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void processarReclassificacaoPerdaContabil(DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por popular as contas credito e de debito na lista de
	 * lancamentos contabeis sintetico.
	 *
	 * @param listaLancamentoContabilSintetico
	 *            the lista lancamento contabil sintetico
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String popularContaCreditoDebitoLancamentoSintetico(
			Collection<LancamentoContabilSintetico> listaLancamentoContabilSintetico) throws NegocioException;

	/**
	 * Consultar evento comercial lancamento.
	 * 
	 * @return the collection
	 */
	Collection<EventoComercialLancamento> consultarEventoComercialLancamento();

	/**
	 * Consultar lancamento contabil sintetico por faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<LancamentoContabilSinteticoVO> consultarLancamentoContabilSinteticoPorFaturamento(
			Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar lancamento contabil sintetico por recebimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoPorRecebimento(
			Map<String, Object> filtro);

	/**
	 * Método responsável por realizar a baixa das contas PDD.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @param codigoOperacao
	 *            the codigo operacao
	 * @param eventoComercialBaixa
	 *            the evento comercial baixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void relizarBaixaContasPDD(long idFatura, String codigoOperacao, EventoComercial eventoComercialBaixa)
			throws NegocioException, ConcorrenciaException;

	/**
	 * Listar conta contabil.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ContaContabil> listarContaContabil() throws NegocioException;

	/**
	 * Consultar evento comercial lancamento por evento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EventoComercialLancamento> consultarEventoComercialLancamentoPorEvento(long chavePrimaria)
			throws NegocioException;

	/**
	 * Remover evento comercial lancamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerEventoComercialLancamento(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter evento comercial lancamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the evento comercial lancamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EventoComercialLancamento obterEventoComercialLancamento(long chavePrimaria) throws NegocioException;

	/**
	 * Atualizar evento comercial lancamento.
	 * 
	 * @param evento
	 *            the evento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarEventoComercialLancamento(EventoComercialLancamento evento)
			throws NegocioException, ConcorrenciaException;

	/**
	 * Remover eventos comerciais lancamento.
	 * 
	 * @param lista
	 *            the lista
	 * @param listaCompleta
	 *            the lista completa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerEventosComerciaisLancamento(Collection<EventoComercialLancamento> lista,
			Collection<EventoComercialLancamento> listaCompleta) throws NegocioException;

	/**
	 * Obter conta contabil.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the conta contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContaContabil obterContaContabil(long chavePrimaria) throws NegocioException;

	/**
	 * Consultar todos eventos comerciais por filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<EventoComercial> consultarTodosEventosComerciaisPorFiltro(Map<String, Object> filtro);

	/**
	 * Validar novo evento comercial lancamento.
	 * 
	 * @param lancamento
	 *            the lancamento
	 * @param lista
	 *            the lista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarNovoEventoComercialLancamento(EventoComercialLancamento lancamento,
			Collection<EventoComercialLancamento> lista) throws NegocioException;

	/**
	 * Validar alteracao evento comercial lancamento.
	 * 
	 * @param lancamento
	 *            the lancamento
	 * @param lista
	 *            the lista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlteracaoEventoComercialLancamento(EventoComercialLancamento lancamento,
			Collection<EventoComercialLancamento> lista) throws NegocioException;

	/**
	 * Consultar evento comercial lancamento por filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the evento comercial lancamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EventoComercialLancamento consultarEventoComercialLancamentoPorFiltro(Map<String, Object> filtro)
			throws NegocioException;

	/**
	 * Consultar evento comercial lancamento por filtro.
	 * 
	 * @param lancamentoContabilVO
	 *            the lancamento contabil vo
	 * @return the map
	 */
	Map<String, Object> consultarEventoComercialLancamentoPorFiltro(LancamentoContabilVO lancamentoContabilVO);

	/**
	 * Consulta uma entidade do tipo {@link EventoComercialLancamento} e popula
	 * os atributos de descrição de conta auxiliar, crédito e débito, e
	 * descrição de histórico contabil de um {@link LancamentoContabilVO}.
	 * 
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @param objetos
	 *            - {@link List}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	void popularContaAuxiliarHistoricoContabil(LancamentoContabilVO lancamentoContabilVO, List<Object> objetos)
			throws NegocioException;

	/**
	 * Consultar evento comercial lancamento por segmento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<EventoComercialLancamento> consultarEventoComercialLancamentoPorSegmento(Long chavePrimaria);

	/**
	 * Tem lancamento contabil sintetico pra fatura.
	 * 
	 * @param fatura
	 *            the fatura
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Boolean temLancamentoContabilSinteticoPraFatura(Fatura fatura) throws NegocioException;

	/**
	 * Registrar provisao devedores duvidosos classificar perdas.
	 *
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param eventoComercialInclusao
	 *            the evento comercial inclusao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void registrarProvisaoDevedoresDuvidososClassificarPerdas(Fatura fatura, DadosAuditoria dadosAuditoria,
			EventoComercial eventoComercialInclusao) throws GGASException;

	/**
	 * Registrar lancamento contabil classificar perdas.
	 *
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param eventoComercial
	 *            the evento comercial
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void registrarLancamentoContabilClassificarPerdas(Fatura fatura, DadosAuditoria dadosAuditoria,
			EventoComercial eventoComercial) throws NegocioException, ConcorrenciaException;

	/**
	 * Gets the log processamento.
	 *
	 * @return the log processamento
	 */
	StringBuilder getLogProcessamento();

	/**
	 * Sets the log processamento.
	 *
	 * @param logProcessamento
	 *            the new log processamento
	 */
	void setLogProcessamento(StringBuilder logProcessamento);

	/**
	 * Obtém de um {@link MultiKeyMap}, uma entidade do tipo
	 * {@link EventoComercialLancamento}, utilizando como chaves, os atributos
	 * {@link EventoComercial}, {@link ContaBancaria}, {@link Tributo},
	 * {@link LancamentoItemContabil} do {@link EventoComercialLancamento}.
	 * 
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @param eventoComercialLancamentoPorAtributos
	 *            - {@link MultiKeyMap}
	 * @return {@link EventoComercialLancamento}
	 */
	EventoComercialLancamento obterEventoComercialLancamentoPorAtributosMapeados(
			LancamentoContabilVO lancamentoContabilVO, MultiKeyMap eventoComercialLancamentoPorAtributos);

	/**
	 * Consulta uma coleção de entidades do tipo
	 * {@link EventoComercialLancamento}, utilizando como filtro de pesquisa, a
	 * chave primária da entidade {@link Segmento}.Retorna um mapa de
	 * {@link EventoComercialLancamento} por atributos do mesmo. Os seguintes
	 * atributos compõem a chave do mapa: {@link EventoComercial},
	 * {@link LancamentoItemContabil}, {@link Tributo} e {@link ContaBancaria}.
	 * 
	 * @param chavePrimariaSegmento
	 *            - {@link Long}
	 * @return {@link MultiKeyMap}
	 */
	MultiKeyMap agruparEventoComercialLancamentoPorSegmento(Long chavePrimariaSegmento);

	/**
	 * Popula os atributos de descrição de conta auxiliar, crédito e débito, e
	 * descrição de histórico contabil de um {@link LancamentoContabilVO}, a
	 * partir dos mesmos atributos de descrição de uma entidade do tipo
	 * {@link EventoComercialLancamento}.
	 * 
	 * @param eventoComercialLancamento
	 *            - {@link EventoComercialLancamento}
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @param objetos
	 *            - {@link List}
	 * @param colunasPorCodigoVariavel
	 *            - {@link Map}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	void popularContaAuxiliarHistoricoContabil(EventoComercialLancamento eventoComercialLancamento,
			LancamentoContabilVO lancamentoContabilVO, List<Object> objetos,
			Map<String, Coluna> colunasPorCodigoVariavel) throws NegocioException;

	/**
	 * Para cada {@link EventoComercialLancamento}, da coleção passada por
	 * parâmetro, realiza um "split" utilizando o separador "#", nas descrições
	 * de histórico e conta auxiliar de crédito e debito e as agrupa em uma
	 * estrutura de elementos não duplicados.
	 * 
	 * @param listaEventoComercialLancamento
	 *            - {@link Collection}
	 * @return {@link Set}
	 */
	Set<String> getDescricoesEventoComercialLancamento(
			Collection<EventoComercialLancamento> listaEventoComercialLancamento);

	/**
	 * Método responsável por atualizar uma coleção de entidades do tipo
	 * {@link LancamentoContabilSintetico}
	 * 
	 * @param contabilizacoesAcumuladas
	 *            - {@link Map}
	 * @throws NegocioException
	 * 				- {@link NegocioException}
	 **/
	void atualizarContabilizacoesAcumuladas(Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas)
			throws NegocioException;

	/**
	 * Insere ou atualiza a contabilização de um
	 * {@link LancamentoContabilSintetico}.
	 * 
	 * @param contabilizacoesAcumuladas
	 *            - {@link Map}
	 * @param lancamentoContabilVO
	 *            - {@link LancamentoContabilVO}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	void contabilizarLancamentoContabilSintetico(Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas,
			LancamentoContabilVO lancamentoContabilVO) throws NegocioException;

	/**
	 * Método responsável por consultar os LancamentoContabilSintetico apartir
	 * de um filtro antes de inserir.
	 * 
	 * @param filtro
	 *            o filtro de pesquisa
	 * @return os LancamentoContabilSintetico que se enquadram nos parâmetros
	 *         passados no filtro
	 */
	Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoInserir(Map<String, Object> filtro);

	/**
	 * Método responsável por monta um filtro de
	 * pesquisa com o conjunto único de atributos
	 * de LancamentoContabilSintetico.
	 * 
	 * @param lancamentoContabilVO
	 *            o VO de lancamento Contabil
	 * @param dataAtual
	 *            a data atual do sistema ou data
	 *            de geração
	 * @param dataContabil
	 *            a data contábil
	 * @return filtro de pesquisa
	 */
	Map<String, Object> montarFiltroPesquisaAtributosUnicos(LancamentoContabilVO lancamentoContabilVO, Date dataAtual,
			Date dataContabil);

}
