/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao lançamento de evento comercial.
 *
 */
public interface EventoComercialLancamento extends EntidadeNegocio {

	String BEAN_ID_EVENTO_COMERCIAL_LANCAMENTO = "eventoComercialLancamento";

	/**
	 * @return EventoComercial - retorna evento comercial.
	 */
	EventoComercial getEventoComercial();

	/**
	 * @param eventoComercial - Set evento comercial.
	 */
	void setEventoComercial(EventoComercial eventoComercial);

	/**
	 * @return Segmento - Retorna segmento.
	 */
	Segmento getSegmento();

	/**
	 * @param segmento - Set segmento.
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return LancamentoItemContabil - Retorna lançamento item contabil.
	 */
	LancamentoItemContabil getLancamentoItemContabil();

	/**
	 * @param lancamentoItemContabil - Set lançamento item contabil.
	 */
	void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil);

	/**
	 * @return Tributo - Retorna tributo.
	 */
	Tributo getTributo();

	/**
	 * @param tributo - Set tributo.
	 */
	void setTributo(Tributo tributo);

	/**
	 * @return ContaBancaria - Retorna Conta bancária.
	 */
	ContaBancaria getContaBancaria();

	/**
	 * @param contaBancaria - Set Conta bancária.
	 */
	void setContaBancaria(ContaBancaria contaBancaria);

	/**
	 * @return ContaContabil - Retorna conta contabil debito.
	 */
	ContaContabil getContaContabilDebito();

	/**
	 * @param contaContabilDebito - Set conta contabil debito.
	 */
	void setContaContabilDebito(ContaContabil contaContabilDebito);

	/**
	 * @return ContaContabil - Retorna objeto conta contabil.
	 */
	ContaContabil getContaContabilCredito();

	/**
	 * @param contaContabilCredito - Set Conta contabil de crédito.
	 */
	void setContaContabilCredito(ContaContabil contaContabilCredito);

	/**
	 * @return String - Set descrição histórico.
	 */
	String getDescricaoHistorico();

	/**
	 * @param descricaoHistorico - Set Descrição histórico.
	 */
	void setDescricaoHistorico(String descricaoHistorico);

	/**
	 * @return String - Retorna descrição conta auxiliar de débito.
	 */
	String getDescricaoContaAuxiliarDebito();

	/**
	 * @param descricaoContaAuxiliarDebito - Set descrição conta auxiliar de débito.
	 */
	void setDescricaoContaAuxiliarDebito(String descricaoContaAuxiliarDebito);

	/**
	 * @return String - Set descrição conta auxiliar de credito.
	 */
	String getDescricaoContaAuxiliarCredito();

	/**
	 * @param descricaoContaAuxiliarCredito - Set descrição conta auxiliar de crédito.
	 */
	void setDescricaoContaAuxiliarCredito(String descricaoContaAuxiliarCredito);

	/**
	 * @return Boolean - Retorna indicador regime contabil competencia.
	 */
	Boolean getIndicadorRegimeContabilCompetencia();

	/**
	 * @param indicadorRegimeContabilCompetencia - Set indicador Regime contabil competencia.
	 */
	void setIndicadorRegimeContabilCompetencia(Boolean indicadorRegimeContabilCompetencia);

}
