package br.com.ggas.contabil;

import br.com.ggas.faturamento.Fatura;
import com.google.common.collect.Range;
import org.joda.time.Period;

import java.math.BigDecimal;

/**
 * The Interface ValidadorPDD.
 */
public interface ValidadorPDD {

	/**
	 * Identifica quando uma fatura atende os seguintes critérios: valor total
	 * dentro dos limites especificados pelo método setLimites, e data de
	 * vencimento menor que a data presente no parâmetro REFERENCIA_CONTABIL
	 * menos o periodo passado em {@link #setPeriodo(Period) setPeriodo}.
	 * 
	 * @param fatura
	 *            the fatura
	 * @return true, if successful
	 */
	boolean caracterizaPDD(Fatura fatura);

	/**
	 * Gets the periodo.
	 *
	 * @return the periodo
	 */
	Period getPeriodo();

	/**
	 * Sets the periodo.
	 *
	 * @param periodo the new periodo
	 */
	void setPeriodo(Period periodo);

	/**
	 * Gets the limites.
	 *
	 * @return the limites
	 */
	Range<BigDecimal> getLimites();

	/**
	 * Sets the limites.
	 *
	 * @param limites the new limites
	 */
	void setLimites(Range<BigDecimal> limites);

	/**
	 * Gets the mensagem verificacao.
	 *
	 * @return the mensagem verificacao
	 */
	String getMensagemVerificacao();

	/**
	 * Sets the mensagem verificacao.
	 *
	 * @param mensagemProcessamento the new mensagem verificacao
	 */
	void setMensagemVerificacao(String mensagemProcessamento);

	/**
	 * Gets the evento comercial.
	 *
	 * @return the evento comercial
	 */
	EventoComercial getEventoComercial();

	/**
	 * Sets the evento comercial.
	 *
	 * @param eventoComercial the new evento comercial
	 */
	void setEventoComercial(EventoComercial eventoComercial);

}
