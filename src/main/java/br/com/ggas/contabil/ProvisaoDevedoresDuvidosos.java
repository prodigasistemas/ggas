/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contabil;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ProvisaoDevedoresDuvidosos.
 */
public interface ProvisaoDevedoresDuvidosos extends EntidadeNegocio {

	/** The bean id provisao devedores duvidosos. */
	String BEAN_ID_PROVISAO_DEVEDORES_DUVIDOSOS = "provisaoDevedoresDuvidosos";

	/** The evento comercial inclusao. */
	String EVENTO_COMERCIAL_INCLUSAO = "PROV_DEV_DUVIDOSOS_EVENTO_COMERCIAL_INCLUSAO";

	/** The evento comercial baixa. */
	String EVENTO_COMERCIAL_BAIXA = "PROV_DEV_DUVIDOSOS_EVENTO_COMERCIAL_BAIXA";

	/** The segmento ponto consumo. */
	String SEGMENTO_PONTO_CONSUMO = "PROV_DEV_DUVIDOSOS_SEGMENTO_PONTO_CONSUMO";

	/** The ano mes referencia contabil. */
	String ANO_MES_REFERENCIA_CONTABIL = "PROV_DEV_DUVIDOSOS_ANO_MES_REFERENCIA_CONTABIL";

	/** The valor. */
	String VALOR = "PROV_DEV_DUVIDOSOS_VALOR";

	/** The valor multa. */
	String VALOR_MULTA = "PROV_DEV_DUVIDOSOS_VALOR_MULTA";

	/** The valor juros. */
	String VALOR_JUROS = "PROV_DEV_DUVIDOSOS_VALOR_JUROS";

	/** The data baixa. */
	String DATA_BAIXA = "PROV_DEV_DUVIDOSOS_DATA_BAIXA";

	/** The ano mes referencia baixa. */
	String ANO_MES_REFERENCIA_BAIXA = "PROV_DEV_DUVIDOSOS_ANO_MES_REFERENCIA_BAIXA";

	/** The motivo baixa. */
	String MOTIVO_BAIXA = "PROV_DEV_DUVIDOSOS_MOTIVO_BAIXA";

	/**
	 * Gets the fatura.
	 *
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * Sets the fatura.
	 *
	 * @param fatura the new fatura
	 */
	void setFatura(Fatura fatura);

	/**
	 * Gets the evento comercial inclusao.
	 *
	 * @return the evento comercial inclusao
	 */
	EventoComercial getEventoComercialInclusao();

	/**
	 * Sets the evento comercial inclusao.
	 *
	 * @param eventoComercialInclusao the new evento comercial inclusao
	 */
	void setEventoComercialInclusao(EventoComercial eventoComercialInclusao);

	/**
	 * Gets the evento comercial baixa.
	 *
	 * @return the evento comercial baixa
	 */
	EventoComercial getEventoComercialBaixa();

	/**
	 * Sets the evento comercial baixa.
	 *
	 * @param eventoComercialBaixa the new evento comercial baixa
	 */
	void setEventoComercialBaixa(EventoComercial eventoComercialBaixa);

	/**
	 * Gets the segmento ponto consumo.
	 *
	 * @return the segmento ponto consumo
	 */
	Segmento getSegmentoPontoConsumo();

	/**
	 * Sets the segmento ponto consumo.
	 *
	 * @param segmentoPontoConsumo the new segmento ponto consumo
	 */
	void setSegmentoPontoConsumo(Segmento segmentoPontoConsumo);

	/**
	 * Gets the ano mes referencia contabil.
	 *
	 * @return the ano mes referencia contabil
	 */
	Integer getAnoMesReferenciaContabil();

	/**
	 * Sets the ano mes referencia contabil.
	 *
	 * @param anoMesReferenciaContabil the new ano mes referencia contabil
	 */
	void setAnoMesReferenciaContabil(Integer anoMesReferenciaContabil);

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	void setValor(BigDecimal valor);

	/**
	 * Gets the valor multa.
	 *
	 * @return the valor multa
	 */
	BigDecimal getValorMulta();

	/**
	 * Sets the valor multa.
	 *
	 * @param valorMulta the new valor multa
	 */
	void setValorMulta(BigDecimal valorMulta);

	/**
	 * Gets the valor juros.
	 *
	 * @return the valor juros
	 */
	BigDecimal getValorJuros();

	/**
	 * Sets the valor juros.
	 *
	 * @param valorJuros the new valor juros
	 */
	void setValorJuros(BigDecimal valorJuros);

	/**
	 * Gets the data baixa.
	 *
	 * @return the data baixa
	 */
	Date getDataBaixa();

	/**
	 * Sets the data baixa.
	 *
	 * @param dataBaixa the new data baixa
	 */
	void setDataBaixa(Date dataBaixa);

	/**
	 * Gets the ano mes referencia baixa.
	 *
	 * @return the ano mes referencia baixa
	 */
	Integer getAnoMesReferenciaBaixa();

	/**
	 * Sets the ano mes referencia baixa.
	 *
	 * @param anoMesReferenciaBaixa the new ano mes referencia baixa
	 */
	void setAnoMesReferenciaBaixa(Integer anoMesReferenciaBaixa);

	/**
	 * Gets the provisao dev duv motivo baixa.
	 *
	 * @return the provisao dev duv motivo baixa
	 */
	ProvisaoDevDuvMotivoBaixa getProvisaoDevDuvMotivoBaixa();

	/**
	 * Sets the provisao dev duv motivo baixa.
	 *
	 * @param provisaoDevDuvMotivoBaixa the new provisao dev duv motivo baixa
	 */
	void setProvisaoDevDuvMotivoBaixa(ProvisaoDevDuvMotivoBaixa provisaoDevDuvMotivoBaixa);

}
