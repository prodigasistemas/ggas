package br.com.ggas.contabil;

import java.util.Collection;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;

/**
 * The Interface ReclassificadorFaturas.
 */
public interface ReclassificadorFaturas {

	/**
	 * Reclassifica todas as faturas da base que atendem os critérios
	 * especificados nos validadores passados em
	 * {@link #setValidadores(Collection)}.
	 *
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void reclassificarFaturas() throws GGASException;

	/**
	 * Sets the validadores.
	 *
	 * @param validadores the new validadores
	 */
	void setValidadores(Collection<ValidadorPDD> validadores);

	/**
	 * Gets the validadores.
	 *
	 * @return the validadores
	 */
	Collection<ValidadorPDD> getValidadores();

	/**
	 * Sets the log processamento.
	 *
	 * @param logProcessamento the new log processamento
	 */
	void setLogProcessamento(StringBuilder logProcessamento);

	/**
	 * Gets the log processamento.
	 *
	 * @return the log processamento
	 */
	StringBuilder getLogProcessamento();

	/**
	 * Sets the dados auditoria.
	 *
	 * @param dadosAuditoria the new dados auditoria
	 */
	void setDadosAuditoria(DadosAuditoria dadosAuditoria);

	/**
	 * Gets the dados auditoria.
	 *
	 * @return the dados auditoria
	 */
	DadosAuditoria getDadosAuditoria();

}
