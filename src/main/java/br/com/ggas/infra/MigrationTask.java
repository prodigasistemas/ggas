package br.com.ggas.infra;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

/**
 * Classe responsável pela execução dos scripts de carga inicial.
 * 
 * @author arthur.carvalho
 *
 */
public class MigrationTask {

	private static ApplicationContext context;

	private static final Logger LOG = Logger.getLogger("migration");

	public static void main(String[] args) throws Exception {

		String sqlPath = "";
		try {
			sqlPath = args[0];
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.log(Level.WARNING, "O diretório de arquivos .sql não foi informado.", e);
		}

		context = new ClassPathXmlApplicationContext("file:src/main/webapp/WEB-INF/spring-config-test.xml");
		MigrationDB migration = (MigrationDB) context.getBean("migrationDB");
		StringBuilder log = migration.carregar(sqlPath);
		gravarLog(log);

		if (log.toString().contains("ERROR")) {
			LOG.info("Ocorreu um Erro ao Executar Scripts SQL. Por favor, verique os logs na pasta o usuário");
			System.exit(1);
		}

		System.exit(0); // NOSONAR {Função necessária para finalizar a task quando invocada via console}
	}

	private static void gravarLog(StringBuilder log) throws IOException {

		BasicDataSource dataSource = (BasicDataSource) context.getBean("dataSource");
		String user = dataSource.getUsername();

		String home = System.getProperty("user.home");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH@mm#");
		String date = sdf.format(new Date());

		FileWriter fw = null;
		try {
			fw = new FileWriter(home + File.separator + "UpdateDb_" + user + "_" + // NOSONAR
					date.replace("@", "h").replace("#", "min") + ".log");
			fw.write(log + "\n");
		} finally {
			if (fw != null) {
				fw.close(); // NOSONAR {Função necessária para finalizar a task quando invocada via console}
			}
		}

	}
}
