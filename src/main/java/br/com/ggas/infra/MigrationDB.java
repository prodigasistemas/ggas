
package br.com.ggas.infra;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Util;

/**
 * Classe responsável por inserir os scripts sql da aplicação.
 * 
 * @author arthur.carvalho
 *
 */
@Component("migrationDB")
public class MigrationDB {

	private static final Logger LOG = Logger.getLogger(MigrationDB.class);

	private Map<String, String> mapa = new TreeMap<String, String>();

	@Autowired(required = false)
	protected BasicDataSource dataSource;

	private JdbcTemplate jdbcTemplate;

	private String statement = null;

	private StringBuilder logArquivo = null;

	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;

	/**
	 * Carregar.
	 * 
	 * @param sqlPath - {@link String}
	 * @return StringBuilder logArquivo
	 * @throws SQLException the SQL Exception
	 */
	public StringBuilder carregar(String sqlPath) throws SQLException {

		logArquivo = new StringBuilder();

		if (dataSource != null) {
			jdbcTemplate = new JdbcTemplate(dataSource);

			List<String> historico = verificar();
			mapa = lerScripts(sqlPath);
			Map<String, String> order = getMapaOrdenado(mapa);
			Set<Entry<String, String>> nomes = order.entrySet();

			boolean cont = true;
			for (Entry<String, String> entry : nomes) {
				try {
					cont = updateDB(historico, entry.getValue(), cont);
				} catch (SQLException e) {
					LOG.error(e.getMessage(), e);
					logArquivo.append(e.getMessage());
					logArquivo.append("<ERROR>\n");
					break;
				}
			}

			atualizarUsuario();
		}

		return logArquivo;
	}

	private Map<String, String> getMapaOrdenado(Map<String, String> mapa) {

		Map<String, String> retorno = new TreeMap<>();
		for (String key : mapa.keySet()) {
			retorno.put(tratarVersao(key), key);
		}

		return retorno;
	}

	private String tratarVersao(String key) {

		if (key.startsWith("GGAS_SCRIPT_INICIAL")) {
			return key;
		}

		final String prefix = "GGAS_VER-";

		String[] aux = key.replace(prefix, "").split("_");
		String[] ver = aux[0].split("\\.");

		String versao = casasDecimais(ver);

		String retorno = prefix.concat(versao).concat("_").concat(aux[1]);

		int i = 2;
		while (i < aux.length) {
			retorno = retorno.concat("_").concat(aux[i]);
			i++;
		}

		return retorno;
	}

	private String casasDecimais(String[] ver) {

		String retorno = "";
		String aux = "";

		for (String s : ver) {
			if (!retorno.isEmpty()) {
				retorno = retorno.concat(".");
			}
			aux = s;

			while (aux.length() < 3) {
				aux = "0".concat(aux);
			}

			retorno = retorno.concat(aux);
		}
		return retorno;
	}

	private void atualizarUsuario() {

		Usuario usuario;
		try {
			usuario = controladorUsuario.buscar("admin");

			if (usuario != null) {

				Calendar c = Calendar.getInstance();
				c.setTime(new Date());

				c.add(Calendar.MONTH, 2);
				usuario.setDataExpirarSenha(c.getTime());

				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH, -10);
				usuario.setDataCriacaoSenha(c.getTime());

				c.add(Calendar.DAY_OF_MONTH, 1);
				usuario.setUltimaAlteracao(c.getTime());

				controladorUsuario.atualizar(usuario);

				logArquivo.append("\nUsuário admin atualizado!");
			}
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			logArquivo.append("\nUsuário admin NÃO atualizado!");
		} catch (ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
			logArquivo.append("\nUsuário admin NÃO atualizado!");
		} catch (SQLGrammarException e) {
			LOG.error(e.getMessage(), e);
			logArquivo.append(e.getMessage());
		}
	}

	private boolean updateDB(List<String> historico, String n, boolean cont) throws SQLException {

		if (cont) {
			logArquivo.append("\n").append(n);
			String[] sqls = mapa.get(n).split(";");

			if (!(historico != null && historico.contains(n))) {
				LOG.info("Executando arquivo: "+ n);

				try {
					for (String sql : sqls) {
						statement = sql.replace("#", ";").trim();
						if (!StringUtils.isEmpty(sql.trim())) {
							jdbcTemplate.execute(statement);
						}
					}
					logArquivo.append(" \t << EXECUTADO >>");
					atualizarLog(n);
				} catch (DataAccessException e) {

					logArquivo.append("\n\n<ERROR>\nFalhou -- ").append(n).append("\n").append(statement);
					if (statement.trim().length() > 3) {
						cont = false;
						throw new SQLException(e);
					}
				}
			}
		}

		return cont;
	}

	private void atualizarLog(String n) throws SQLException {

		String sql = "select count(*) from UPDATE_DB_LOG";
		String insert = "insert into UPDATE_DB_LOG values(?,?,?,?)";
		Long qnt = jdbcTemplate.queryForLong(sql);
		jdbcTemplate.update(insert, qnt, n, true, new Date());
	}

	private List<String> verificar() throws SQLException {

		List<String> arquivos = new ArrayList<>();
		String create =
				"CREATE TABLE UPDATE_DB_LOG (  ID NUMBER(9) " + ", ARQUIVO VARCHAR2(128) , COMPLETE VARCHAR2(20) , DATA TIMESTAMP )";

		String select = "SELECT ARQUIVO FROM UPDATE_DB_LOG";
		try {
			arquivos = jdbcTemplate.queryForList(select, String.class);

		} catch (DataAccessException e) {
			LOG.debug(e.getMessage(), e);
			String insert = "insert into UPDATE_DB_LOG values(?,?,?,?)";
			jdbcTemplate.execute(create);
			jdbcTemplate.update(insert, 1, "BANCO CRIADO", true, new Date());

		}

		return arquivos;
	}

	private Map<String, String> lerScripts(String sqlPath) {

		Map<String, String> mapaLerScripts = new TreeMap<>();

		String path = sqlPath;

		if(StringUtils.isBlank(sqlPath)) {
			path = getClass().getClassLoader().getResource("sql").getFile();
			path = path.replace("%20", " ").replace("/bin/sql", "/sql").replace("\\bin\\sql", "\\sql")
					.replace("/build/classes/main/sql", "/sql").replace("\\build\\classes\\main\\sql", "\\sql");
		}

		File f = Util.getFile(path);

		f.isDirectory();
		f.getAbsolutePath();
		f.canRead();

		File[] arquivos = f.listFiles();
		if (arquivos != null) {
			for (final File fileEntry : arquivos) {
				if (fileEntry.getName().startsWith("GGAS_Ver")
						|| fileEntry.getName().startsWith("GGAS_SCRIPT_INICIAL_ORACLE_02_ESTRUTURA_CONSTRAINTS_CARGA_INICIAL")) {
					mapaLerScripts.put(fileEntry.getName().toUpperCase(), obterConteudo(fileEntry));
				}
			}
		}

		return mapaLerScripts;
	}

	private String obterConteudo(File f) {

		String[] ignore = { "SEGMENT CREATION IMMEDIATE", "PCTFREE", "STORAGE(INITIAL", "PCTINCREASE", "TABLESPACE" };

		StringBuilder sb = new StringBuilder();
		boolean ignoreRead = false;

		if (!f.isDirectory()) {
			BufferedReader bf = null;
			try {
				bf = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF8"));
				String linha = null;

				while ((linha = bf.readLine()) != null) {

					if (!linha.startsWith("commit") && !linha.startsWith("COMMIT") && !linha.startsWith(" --") && !linha.startsWith("-")
							&& !linha.startsWith("REM INSERTING") && !linha.startsWith("SET DEFINE OFF")) {

						String sql = linha.replace("\"GGAS_ADMIN\".", "").replace("GGAS_ADMIN.", "").replace("\"", "").replace(";", "#")
								.replace("(GGAS->ERP; ERP<-GGAS; GGAS<->ERP)", "(GGAS<->ERP)").trim();
						if (sql.endsWith("#")) {
							sql = sql.substring(0, sql.length() - 1) + ";";
						}

						if (!sql.isEmpty() && !"".equals(sql) && !ignoreRead) {

							if (sql.contains(ignore[0])) {
								sb.append(")");
								ignoreRead = true;
							} else {

								if (!(linha.startsWith("COMMENT ON")
										&& (linha.contains("\"NFE\"") || linha.contains("POTA_IN_ICMS_VL_TARIFA")))) {

									if (sql.indexOf("--") >= 0) {
										sql = sql.substring(0, sql.indexOf("--"));
									}
									sb.append(sql).append(" ");
								}
							}

						}
						if (sql.trim().endsWith(";") && ignoreRead) {
							ignoreRead = false;
							sb.append(";");
						}
					}
				}

			} catch (FileNotFoundException e) {
				LOG.info(e.getMessage(), e);
			} catch (IOException e) {
				LOG.info(e.getMessage(), e);
			} finally {
				try {
					if (bf != null) {
						bf.close(); // NOSONAR {Função necessária para finalizar
									// a task quando invocada via console}
					}
				} catch (IOException e) {
					LOG.info(e.getMessage(), e);
				}
			}
		}

		return sb.toString();
	}
}
