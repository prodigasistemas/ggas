package br.com.ggas.infra.processos;

import javax.sql.DataSource;

/**
 * Classe responsável pela representação das propriedades de um {@link DataSource}.
 * @author orube
 *
 */
public class DataSourceProperties {

	private final String schema;

	private final String passwd;

	private final String url;

	/**
	 * Construtor de {@link DataSourceProperties}.
	 * @param schema - {@link String}
	 * @param passwd - {@link String}
	 * @param url - {@link String}
	 */
	public DataSourceProperties(String schema, String passwd, String url) {
		this.schema = schema;
		this.passwd = passwd;
		this.url = url;
	}

	public String getSchema() {
		return schema;
	}

	public String getPasswd() {
		return passwd;
	}

	public String getUrl() {
		return url;
	}

}
