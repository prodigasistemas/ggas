package br.com.ggas.infra.processos;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Classe responsável por carregar e executar um bean de {@link BatchExecutor}.
 * 
 * @author orube
 *
 */
public class BatchExecutorTask {

	private static final int PROCESS_ID_ARG = 3;

	private static final int DB_URL_ARG = 2;

	private static final int DB_PASSWD_ARG = 1;

	private static final int DB_USER_ARG = 0;

	private ApplicationContext context;

	private static final String CONTEXT_FILE = "spring-config-batch.xml";

	/**
	 * Construtor de {@link BatchExecutorTask}
	 * 
	 * @param dataSourceProperties - {@link DataSourceProperties}
	 */
	public BatchExecutorTask(DataSourceProperties dataSourceProperties) {
		BatchConfiguration.setDataSourceProperties(dataSourceProperties);
		context = new ClassPathXmlApplicationContext(CONTEXT_FILE);
	}
	
	/**
	 * Método responsável pela execução de um processo a partir do seu identificador
	 * único.
	 * 
	 * @param processId - {@link Long}
	 * @return dados do processo - {@link BatchVO}
	 */
	public BatchVO execute(Long processId) {
		BatchExecutor executor = (BatchExecutor) context.getBean("batchExecutor");
		return executor.run(processId);
	}

	/**
	 * Função Main para execução de métodos em {@link BatchExecutorTask}.
	 * 
	 * @param args - {@link String[]}
	 */
	public static void main(String[] args) {

		String user = args[DB_USER_ARG];
		String pass = args[DB_PASSWD_ARG];
		String url = args[DB_URL_ARG];
		Long processId = Long.valueOf(args[PROCESS_ID_ARG]);

		DataSourceProperties dataSource = new DataSourceProperties(user, pass, url);

		BatchExecutorTask batchExecutorTask = new BatchExecutorTask(dataSource);
		batchExecutorTask.execute(processId);

		System.exit(0);

	}
}
