package br.com.ggas.infra.processos;

import br.com.ggas.batch.Processo;

/**
 * Classe responsável pela representação dos dados de um processo batch.
 * @author orube
 *
 */
public class BatchVO {

	private Long id;

	private String usuario;

	private String descricao;

	private String logErro;

	private String logExecucao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLogErro() {
		return logErro;
	}

	public void setLogErro(String logErro) {
		this.logErro = logErro;
	}

	public String getLogExecucao() {
		return logExecucao;
	}

	public void setLogExecucao(String logExecucao) {
		this.logExecucao = logExecucao;
	}

	/**
	 * Método responsável por converter um processo em um {@link BatchVO}.
	 * @param processo - {@link Processo}
	 * @return batchVO - {@link BatchVO}
	 */
	public static BatchVO toVO(Processo processo) {
		BatchVO vo = new BatchVO();
		vo.setDescricao(processo.getDescricao());
		vo.setId(processo.getChavePrimaria());
		vo.setLogErro(String.valueOf(processo.getLogErro()));
		vo.setLogExecucao(String.valueOf(processo.getLogExecucao()));
		vo.setUsuario(processo.getUsuario().getLogin());
		return vo;
	}
}
