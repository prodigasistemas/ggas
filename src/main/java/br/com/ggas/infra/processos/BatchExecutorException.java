package br.com.ggas.infra.processos;

/**
 * Exceção que representa uma falha no executor de processos batch.
 * @author orube
 *
 */
public class BatchExecutorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3359285256663025599L;

	/**
	 * Construtor de {@link BatchExecutorException}
	 * @param message - {@link String}
	 * @param throwable - {@link Throwable}
	 */
	public BatchExecutorException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
