package br.com.ggas.infra.processos;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

import br.com.ggas.batch.Batch;

/**
 * {@link Configuration} do Spring responsável pelos recursos do Hibernate
 * utilizados para execução de um {@link Batch} em {@link BatchExecutorTask}.
 * 
 * @author orube
 *
 */
@Configuration
public class BatchConfiguration {

	@Autowired
	private ResourceLoader resourceLoader;

	private static DataSourceProperties dataSourceProperties;

	/**
	 * Responsável por criar um {@link LocalSessionFactoryBean}
	 * 
	 * @return localSessionFactoryBean - {@link LocalSessionFactoryBean}
	 * @throws IOException - {@link IOException}
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() throws IOException {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setMappingLocations(getHibernateMappingResources());
		localSessionFactoryBean.setDataSource(this.dataSource());
		localSessionFactoryBean.setHibernateProperties(hibernateProperties());
		return localSessionFactoryBean;

	}

	/**
	 * Responsável por criar um {@link HibernateTransactionManager}
	 * 
	 * @return hibernateTransactionManager - {@link HibernateTransactionManager}
	 * @throws IOException - {@link IOException}
	 */
	@Bean
	public HibernateTransactionManager transactionManager() throws IOException {
		return new HibernateTransactionManager(this.sessionFactory().getObject());
	}

	/**
	 * Responsável por criar um {@link DataSource}
	 * 
	 * @return dataSource - {@link DataSource}
	 */
	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl(dataSourceProperties.getUrl());
		dataSource.setUsername(dataSourceProperties.getSchema());
		dataSource.setPassword(dataSourceProperties.getPasswd());

		return dataSource;
	}

	private final Properties hibernateProperties() {
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		hibernateProperties.setProperty("hibernate.c3p0.max_size", "10");
		hibernateProperties.setProperty("hibernate.c3p0.min_size", "1");
		hibernateProperties.setProperty("hibernate.jdbc.batch_size", "100");
		hibernateProperties.setProperty("hibernate.jdbc.batch_versioned_data", "true");
		hibernateProperties.setProperty("hibernate.order_inserts", "true");
		hibernateProperties.setProperty("hibernate.order_updates", "true");
		hibernateProperties.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.EhCacheProvider");
		hibernateProperties.setProperty("hibernate.c3p0.acquire_increment", "1");
		hibernateProperties.setProperty("hibernate.c3p0.idle_test_period", "100");
		hibernateProperties.setProperty("hibernate.c3p0.max_statements", "0");
		hibernateProperties.setProperty("hibernate.c3p0.timeout", "1000");
		hibernateProperties.setProperty("hibernate.show_sql", "false");
		hibernateProperties.setProperty("hibernate.format_sql", "false");
		hibernateProperties.setProperty("hibernate.use_sql_comments", "false");
		hibernateProperties.setProperty("hibernate.generate_statistics", "false");
		hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", "false");
		hibernateProperties.setProperty("hibernate.cache.use_structured_entries", "true");
		hibernateProperties.setProperty("hibernate.cache.use_minimal_puts", "true");

		return hibernateProperties;
	}

	private Resource[] getHibernateMappingResources() throws IOException {
		return ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources("br/com/ggas/**/*.hbm.xml");
	}

	public static void setDataSourceProperties(DataSourceProperties dataSourceProperties) {
		BatchConfiguration.dataSourceProperties = dataSourceProperties;
	}
}
