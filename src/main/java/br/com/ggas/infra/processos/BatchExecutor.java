package br.com.ggas.infra.processos;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import br.com.ggas.util.ProcessadorBatch;

/**
 * Classe responsável pela execução de um processo batch.
 * 
 * @author orube
 *
 */
@Component("batchExecutor")
public class BatchExecutor {

	private static final Logger LOG = Logger.getLogger(BatchExecutor.class);

	private ProcessadorBatch processadorBatch;

	/**
	 * Construtor de {@link BatchExecutor}.
	 */
	public BatchExecutor() {
		processadorBatch = new ProcessadorBatch();
	}

	/**
	 * Método responsável pela execução de um processo a partir do seu identificador
	 * único.
	 * 
	 * @param processId - {@link Long}
	 * @return dados do processo - {@link BatchVO}
	 */
	public BatchVO run(Long processId) {
		BatchVO vo = null;
		try {
			vo = BatchVO.toVO(processadorBatch.executarProcesso(processId));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new BatchExecutorException(e.getMessage(), e);
		}
		return vo;
	}
}
