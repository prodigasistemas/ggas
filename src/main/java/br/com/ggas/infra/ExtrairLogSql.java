package br.com.ggas.infra;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import br.com.ggas.util.Util;

/**
 * Classe responsável por extrair logs do hibernate "statements.log"
 * e extrair os comandos SQL que foram executados no banco.
 * 
 * @author arthur.carvalho
 *
 */
public class ExtrairLogSql {
 
	private static final String REGEX_TIMESTAMP = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";
	private static LinkedList<SqlCommand> sqls = new LinkedList<>();
	private static LinkedList<String> parametros = new LinkedList<>();
	private static Boolean extrairSelects = null;
	
	private static final Logger LOG = Logger.getLogger(ExtrairLogSql.class);

	/**
	 * Método invocado pela task Gradle
	 * 
	 * @param args - {@link String[]}
	 */
	public static void main(String[] args) {
		

		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choose a directory to save your file: ");
		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		int returnValue = jfc.showSaveDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			if (jfc.getSelectedFile().isFile()) {
				LOG.info("You selected the directory: " + jfc.getSelectedFile());
			}
			int r = JOptionPane.showConfirmDialog(null, "Deseja capturar os Statements do tipo SELECT");
			if (r == 0) {
				extrairSelects = true;
			} else if (r == 1) {
				extrairSelects = false;
			}

			String content;
			try {
				content = extrairSQL(jfc.getSelectedFile());
				gravarArquivo(content);
			} catch (IOException e) {
				LOG.info(e.getMessage(), e);
			}
		}
		JOptionPane.showMessageDialog(null, "FIM");

	}

	private static void gravarArquivo(String content) throws IOException {
		
		String path = FileSystemView.getFileSystemView().getHomeDirectory() + "/sql.log";
		FileUtils.writeStringToFile(Util.getFile(path), content);
	}

	private static String extrairSQL(File selectedFile) throws IOException {
		
		List<String> linhas = FileUtils.readLines(selectedFile);

		StringBuilder sb = new StringBuilder("");
		
		for (String line : linhas) {
			if (line.contains("org.hibernate.SQL")) {
				String[] tokens = line.split("org.hibernate.SQL ");

				limparCache();

				SqlCommand command = montarSql(tokens);
				sqls.add(command);

				sb.append(tokens[tokens.length - 1]).append("\n");
			}
			if (line.contains("binding parameter ")) {
				String[] tokens = line.split("binding parameter ");
				String parametro = extrairParametro(tokens[tokens.length - 1]);
				parametros.add(parametro);

				sb.append(tokens[tokens.length - 1]).append("\n");
			}
		}

		
		

		return montarConteudo(sqls);

	}

	private static String montarConteudo(LinkedList<SqlCommand> sqls) {

		StringBuilder sb = new StringBuilder();

		for (SqlCommand sql : sqls) {
			if (extrairSelects) {
				sb.append(sql).append(";\n");
			} else {
				if (sql.getTipo() != null && !sql.getTipo().equals(SqlCommand.SELECT)) {
					sb.append(sql).append(";\n");
				}
			}
		}
		return sb.toString();
	}

	private static void limparCache() {
		if (!sqls.isEmpty()) {
			sqls.getLast().setParametros(parametros);
		}
		parametros = new LinkedList<>();

	}

	private static String extrairParametro(String p) {

		String retorno = null;
		String[] parametros = p.split(" ");
		int index = Integer.parseInt(parametros[0].replace("[", "").replace("]", ""));

		while (ExtrairLogSql.parametros.size() < index - 1) {
			ExtrairLogSql.parametros.add("[BLANK]@@@@ ");
		}

		retorno = tratarTimeStamp(p, parametros);
		if (p.contains("[DATE]")) {
			retorno =  "[DATE]@@@@CURRENT_TIMESTAMP";
		}else if (p.contains("[BIT]")) {
			retorno =  "[BIT]@@@@" + parametros[parametros.length - 1];
		}else if (p.contains("[VARCHAR]")) {
			StringBuilder sb = new StringBuilder();
			for (int i = 4; i < parametros.length; i++) {
				sb.append(parametros[i]).append(" ");
			}
			retorno = "[VARCHAR]@@@@" + sb.toString();
		}else{
			retorno = parametros[parametros.length - 3].trim() + "@@@@" + parametros[parametros.length - 1];
		}
		return retorno;

	}

	private static String tratarTimeStamp(String p, String[] parametros) {
		
		String retorno = null;
		
		if (p.contains("TIMESTAMP")) {

			if ("<null>".equals(parametros[4])) {
				retorno = "[TIMESTAMP]@@@@null";
			} else if (parametros[4].matches(REGEX_TIMESTAMP)) {
				retorno =  "[TIMESTAMP]@@@@TO_TIMESTAMP('" + parametros[4] + " " + parametros[5] + "', 'YYYY-MM-DD HH24:MI:SS')";
			} else {
				StringBuilder sb = new StringBuilder();
				try {
					sb.append(parametros[9]).append("-").append(converterMes(parametros[5])).append("-").append(parametros[6]).append(" ")
							.append(parametros[7]);
				} catch (Exception e) {
					LOG.info(p, e);
					retorno =  "[TIMESTAMP]@@@@CURRENT_TIMESTAMP";
				}
				retorno =  "[TIMESTAMP]@@@@TO_TIMESTAMP('" + sb.toString() + "', 'YYYY-MM-DD HH24:MI:SS')";
			}

		}
		return retorno;
	}

	private static String converterMes(String mes) {

		SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.ENGLISH);
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime( sdf.parse(mes) );
			int m = calendar.get(Calendar.MONTH) + 1;
		
			
			if (m < 10) {
				return "0" + m;
			} else {
				return m + "";
			}
		} catch (ParseException e) {
			return "00";
		}
	}

	private static SqlCommand montarSql(String[] tokens) {

		String sql = tokens[tokens.length - 1].substring(3);
		SqlCommand command = new SqlCommand();
		command.setTokens(sql.split("\\?"));

		return command;
	}

}
