package br.com.ggas.infra;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import br.com.ggas.geral.exception.NegocioException;

/**
 * Classe implementada para realizar a exclusão de um usuário na base de dados.
 *
 */
public class DropTestDBTask {

	private static final Logger LOG = Logger.getLogger(DropTestDBTask.class);
	private static ResourceBundle bundle = ResourceBundle.getBundle("hibernate-test");
	private static final String DELETE_USER = "DROP USER NOME_BANCO CASCADE";
	private static final String GRANT = "GRANT DBA TO NOME_BANCO";
	private static final String ALTER_SYSTEM_QUERY = "ALTER SYSTEM KILL SESSION '{SID},{SERIAL}' IMMEDIATE";
	private static final String GGAS_ADMIN = "GGAS_ADMIN";
	private static final String NOME_BANCO = "NOME_BANCO";
	private static final String MENSAGEM_ERRO = "O usuário GGAS_ADMIN não pode ser excluído.";
	private static final String MENSAGEM_ERRO_EXCLUIR = "Não foi possível excluir o usuário.";	
	private static final String USERNAME_PROPERTY = "hibernate.connection.username";
	private static final int LOGIN_TIMEOUT = 1000;
	private static final int IDLE_CONNECTION_PERIOD = 1000;
	private static final int TIMEOUT_EXECUTE_QUERY = 3;
	private static final int INDEX_SID = 1;
	private static final int INDEX_SERIAL = 2;
	private static String user = null;
	private static Connection conn = getConnection();

	/**
	 * Main utilizada pela "Task" do Gradle: dropTestDB
	 * 
	 * @param args - {@link String[]}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public static void main(String[] args) {

		if (bundle != null) {
			user = bundle.getString(USERNAME_PROPERTY);
			if (!user.toUpperCase().startsWith(GGAS_ADMIN)) {
				exluirUsuario();
				recriarUsuario();
			} else {
				throw new IllegalStateException(MENSAGEM_ERRO);
			}
		} else {
			LOG.info("Não foi recriado usuário");
		}
	}

	private static void recriarUsuario() {
		StringBuilder createUser = new StringBuilder("CREATE USER NOME_BANCO IDENTIFIED BY NOME_BANCO DEFAULT ")
				.append("TABLESPACE GGAS_DADOS TEMPORARY TABLESPACE TEMP ACCOUNT UNLOCK  PROFILE DEFAULT ")
				.append("QUOTA  UNLIMITED ON GGAS_DADOS");

		PreparedStatement stmtCreateUser = null;
		PreparedStatement stmtGrantPermission = null;
		IllegalStateException exception = null;

		String user = bundle.getString(USERNAME_PROPERTY);
		try {
			stmtCreateUser = conn.prepareStatement(createUser.toString().replace(NOME_BANCO, user));
			stmtGrantPermission = conn.prepareStatement(GRANT.replace(NOME_BANCO, user));
			stmtCreateUser.execute();
			stmtGrantPermission.execute();
			LOG.info("Usuário criado");
		} catch (SQLException e) {
			LOG.info(e.getMessage(), e);
			exception = new IllegalStateException(MENSAGEM_ERRO, e);
		}

		fecharRecursos(stmtCreateUser);
		fecharRecursos(stmtGrantPermission);

		if (exception != null) {
			throw exception;
		}
	}

	private static void exluirUsuario() {

		excluirConexoes();

		PreparedStatement stmt = null;
		IllegalStateException exception = null;

		try {
			String user = bundle.getString(USERNAME_PROPERTY);

			LOG.info("Excluindo Usuário");

			stmt = conn.prepareStatement(DELETE_USER.replace(NOME_BANCO, user));
			stmt.execute();

			LOG.info("Usuário Excluído");
		} catch (SQLException e) {
			LOG.info(e.getMessage(), e);
			if( !e.getMessage().startsWith("ORA-01918")){
				exception = new IllegalStateException(MENSAGEM_ERRO_EXCLUIR);				
			}
		}

		fecharRecursos(stmt);

		if (exception != null ) {
			throw exception;
		}

	}

	private static void excluirConexoes() {

		String sessionsQuery = "SELECT SID, Serial#, UserName FROM V$Session WHERE UserName = ?";

		String user = bundle.getString(USERNAME_PROPERTY);

		PreparedStatement statementQuery = null;
		Statement statementAlterSystem = null;
		ResultSet resultSet = null;

		try {
			statementQuery = conn.prepareStatement(sessionsQuery);
			statementQuery.setString(1, user);

			resultSet = statementQuery.executeQuery();

			statementAlterSystem = conn.createStatement();

			while (resultSet.next()) {
				Long sid = resultSet.getLong(INDEX_SID);
				Long serial = resultSet.getLong(INDEX_SERIAL);
				
				statementAlterSystem.addBatch(ALTER_SYSTEM_QUERY.replace("{SID}", String.valueOf(sid))
						.replace("{SERIAL}", String.valueOf(serial)));

			}
			statementAlterSystem.executeBatch();
			LOG.info("waiting...");
			TimeUnit.SECONDS.sleep(TIMEOUT_EXECUTE_QUERY);
		} catch (SQLException e) {
			LOG.info(e.getMessage(), e);
		} catch (InterruptedException e) {
			LOG.info(e.getMessage(), e);
		}

		fecharRecursos(resultSet);
		fecharRecursos(statementQuery);
		fecharRecursos(statementAlterSystem);

	}

	private static void fecharRecursos(AutoCloseable resource) {

		try {
			if (resource != null) {
				resource.close();
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private static Connection getConnection() {

		ComboPooledDataSource dataSource = new ComboPooledDataSource();

		Connection connection = null;

		try {
			dataSource.setUser(GGAS_ADMIN);
			dataSource.setPassword(GGAS_ADMIN);
			dataSource.setDriverClass(bundle.getString("hibernate.connection.driver_class"));
			dataSource.setJdbcUrl(bundle.getString("hibernate.connection.url"));
			dataSource.setIdleConnectionTestPeriod(IDLE_CONNECTION_PERIOD);
			dataSource.setLoginTimeout(LOGIN_TIMEOUT);
			connection = dataSource.getConnection();
		} catch (SQLException | PropertyVetoException e) {
			LOG.info(e.getMessage(), e);
		}

		return connection;
	}

}
