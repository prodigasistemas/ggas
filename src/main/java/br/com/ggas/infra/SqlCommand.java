package br.com.ggas.infra;

import java.util.List;

/**
 * Classe que facilita a abstração de comando SQL Esta Classe é utilizada pela task extrairLogSql
 * 
 * @author arthur.carvalho
 *
 */
public class SqlCommand {

	private static final String TOKEN_NULL = "<null>";
	public static final String SELECT = "SELECT";
	public static final String UPDATE = "UPDATE";
	public static final String INSERT = "INSERT";
	public static final String DELETE = "DELETE";

	private String tipo;
	private String[] tokens;
	private List<String> parametros;

	/**
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return array de tokens
	 */
	public String[] getTokens() {
		return tokens;
	}

	/**
	 * @return lista de parametros
	 */
	public List<String> getParametros() {
		return parametros;
	}

	/**
	 * @param parametros
	 */
	public void setParametros(List<String> parametros) {
		this.parametros = parametros;
	}

	/**
	 * @param tokens
	 */
	public void setTokens(String[] tokens) {
		this.tokens = tokens;

		if (tokens[0].startsWith("select")) {
			tipo = SELECT;
		}
		if (tokens[0].startsWith("update")) {
			tipo = UPDATE;
		}
		if (tokens[0].startsWith("insert")) {
			tipo = INSERT;
		}
		if (tokens[0].startsWith("delete")) {
			tipo = DELETE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder retorno = new StringBuilder();

		if (tokens.length > 0) {
			retorno.append(tokens[0]);
			int i = 1;
			if (parametros != null && !parametros.isEmpty()) {
				for (String p : parametros) {
					String param = tratarTipoParametro(p);
					retorno.append(param);
					if (tokens.length > i) {
						retorno.append(tokens[i]);
					}
					i++;
				}
			}
		}
		return retorno.toString();
	}

	/**
	 * @param p
	 * @return
	 */
	private String tratarTipoParametro(String p) {

		String retorno = "CURRENT_TIMESTAMP";

		String[] token = p.split("@@@@");
		if (p.startsWith("[INTEGER]") || p.startsWith("[NUMERIC]") || p.startsWith("[BIGINT]")) {
			retorno = tratarNumero(token[token.length - 1]);

		} else if (p.startsWith("[BIT]")) {
			retorno = tratarBit(token[token.length - 1]);

		} else if (p.startsWith("[VARCHAR]")) {
			if (token.length == 1) {
				retorno = "''";
			} else {
				retorno = tratarVarchar(token[token.length - 1]);
			}

		} else if (p.startsWith("[TIMESTAMP]") || p.startsWith("[DATE]")) {
			retorno = tratarTimeStamp(token[token.length - 1]);

		} else if (p.startsWith("[BLANK]")) {
			retorno = "''";
		}
		return retorno;
	}

	/**
	 * @param number
	 * @return
	 */
	private String tratarNumero(String number) {

		if (number.startsWith(TOKEN_NULL)) {
			return "null";
		} else {
			return number;
		}
	}

	/**
	 * @param timestamp
	 * @return
	 */
	private String tratarTimeStamp(String timestamp) {

		if (timestamp.startsWith(TOKEN_NULL)) {
			return "null";
		} else {
			return timestamp;
		}

	}

	/**
	 * @param varchar
	 * @return
	 */
	private String tratarVarchar(String varchar) {

		if (varchar.startsWith(TOKEN_NULL)) {
			return "null";
		} else {
			return "'" + varchar + "'";
		}

	}

	/**
	 * @param bit
	 * @return
	 */
	private String tratarBit(String bit) {
		if ("true".equals(bit) ) {
			return "1";
		} else if ("false".equals(bit) ) {
			return "0";
		}
		return "null";
	}

}
