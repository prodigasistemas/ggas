
package br.com.ggas.webservice;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade LevantamentoMercadoTipoCombustivel para transferência
 *
 */
@XmlRootElement
public class LevantamentoMercadoTipoCombustivelTO {

	private Long chavePrimaria;

	private TipoComboBoxTO redeEstado;

	private TipoComboBoxTO fornecedor;

	private TipoComboBoxTO modalidadeUso;

	private TipoComboBoxTO periodicidade;

	private TipoComboBoxTO tipoBotijao;

	private TipoComboBoxTO redeMaterial;

	private String preco;

	private String consumo;

	private String redeTempo;

	private boolean central;

	private String localizacaoCentral;

	private String localizacaoAbrigo;

	private boolean ventilacao;

	private String observacoes;
	
	private boolean habilitado;
	
	private TipoComboBoxTO tipoCombustivel;

	private Collection<LevantamentoMercadoTipoCombustivelAparelhoTO> listaAparelhos = 
					new ArrayList<LevantamentoMercadoTipoCombustivelAparelhoTO>();

	public Collection<LevantamentoMercadoTipoCombustivelAparelhoTO> getListaAparelhos() {

		return listaAparelhos;
	}

	public void setListaAparelhos(Collection<LevantamentoMercadoTipoCombustivelAparelhoTO> listaAparelhos) {

		this.listaAparelhos = listaAparelhos;
	}

	public TipoComboBoxTO getRedeEstado() {

		return redeEstado;
	}

	public void setRedeEstado(TipoComboBoxTO redeEstado) {

		this.redeEstado = redeEstado;
	}

	public TipoComboBoxTO getFornecedor() {

		return fornecedor;
	}

	public void setFornecedor(TipoComboBoxTO fornecedor) {

		this.fornecedor = fornecedor;
	}

	public TipoComboBoxTO getModalidadeUso() {

		return modalidadeUso;
	}

	public void setModalidadeUso(TipoComboBoxTO modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	public TipoComboBoxTO getPeriodicidade() {

		return periodicidade;
	}

	public void setPeriodicidade(TipoComboBoxTO periodicidade) {

		this.periodicidade = periodicidade;
	}

	public TipoComboBoxTO getTipoBotijao() {

		return tipoBotijao;
	}

	public void setTipoBotijao(TipoComboBoxTO tipoBotijao) {

		this.tipoBotijao = tipoBotijao;
	}

	public TipoComboBoxTO getRedeMaterial() {

		return redeMaterial;
	}

	public void setRedeMaterial(TipoComboBoxTO redeMaterial) {

		this.redeMaterial = redeMaterial;
	}

	public String getPreco() {

		return preco;
	}

	public void setPreco(String preco) {

		this.preco = preco;
	}

	public String getConsumo() {

		return consumo;
	}

	public void setConsumo(String consumo) {

		this.consumo = consumo;
	}

	public String getRedeTempo() {

		return redeTempo;
	}

	public void setRedeTempo(String redeTempo) {

		this.redeTempo = redeTempo;
	}

	public boolean isCentral() {

		return central;
	}

	public void setCentral(boolean central) {

		this.central = central;
	}

	public String getLocalizacaoCentral() {

		return localizacaoCentral;
	}

	public void setLocalizacaoCentral(String localizacaoCentral) {

		this.localizacaoCentral = localizacaoCentral;
	}

	public String getLocalizacaoAbrigo() {

		return localizacaoAbrigo;
	}

	public void setLocalizacaoAbrigo(String localizacaoAbrigo) {

		this.localizacaoAbrigo = localizacaoAbrigo;
	}

	public boolean isVentilacao() {

		return ventilacao;
	}

	public void setVentilacao(boolean ventilacao) {

		this.ventilacao = ventilacao;
	}

	public String getObservacoes() {

		return observacoes;
	}

	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}

	public TipoComboBoxTO getTipoCombustivel() {

		return tipoCombustivel;
	}

	public void setTipoCombustivel(TipoComboBoxTO tipoCombustivel) {

		this.tipoCombustivel = tipoCombustivel;
	}

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}


	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}
}
