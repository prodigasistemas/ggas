/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 22/01/2014 18:05:03
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade FaixaRendaFamiliar para transferência
 *
 */
@XmlRootElement
public class FaixaRendaFamiliarTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8113765399080574649L;

	private Long chavePrimaria;

	private Double valorFaixaInicio;

	private Double valorFaixaFim;

	public Double getValorFaixaInicio() {

		return valorFaixaInicio;
	}

	public void setValorFaixaInicio(Double valorFaixaInicio) {

		this.valorFaixaInicio = valorFaixaInicio;
	}

	public Double getValorFaixaFim() {

		return valorFaixaFim;
	}

	public void setValorFaixaFim(Double valorFaixaFim) {

		this.valorFaixaFim = valorFaixaFim;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

}
