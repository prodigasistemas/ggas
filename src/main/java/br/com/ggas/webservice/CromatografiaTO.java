/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/04/2015 10:29:20
 @author lacavalcanti
 */

package br.com.ggas.webservice;

import java.math.BigDecimal;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade Cromatografia para transferência
 *
 */
@XmlRootElement
public class CromatografiaTO {

	private Long chavePrimaria;

	private String data;

	private BigDecimal pressao;

	private BigDecimal temperatura;

	private String fatorCompressibilidade;

	private Set<CityGateTO> conjuntoCityGate;

	private BigDecimal pcs;

	private BigDecimal agua;

	private BigDecimal iButano;

	private BigDecimal nHeptano;

	private BigDecimal argonio;

	private BigDecimal iPentano;

	private BigDecimal nHexano;

	private BigDecimal dioxidoCarbono;

	private BigDecimal metano;

	private BigDecimal nitrogenio;

	private BigDecimal etano;

	private BigDecimal monoxidoCarbono;

	private BigDecimal nNonano;

	private BigDecimal helio;

	private BigDecimal nButano;

	private BigDecimal nOctano;

	private BigDecimal hidrogenio;

	private BigDecimal nDecano;

	private BigDecimal nPentano;

	private BigDecimal oxigenio;

	private BigDecimal propano;

	private BigDecimal sulfetoHidrogenio;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	public BigDecimal getPressao() {

		return pressao;
	}

	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	public BigDecimal getTemperatura() {

		return temperatura;
	}

	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	public String getFatorCompressibilidade() {

		return fatorCompressibilidade;
	}

	public void setFatorCompressibilidade(String fatorCompressibilidade) {

		this.fatorCompressibilidade = fatorCompressibilidade;
	}

	public Set<CityGateTO> getConjuntoCityGate() {

		return conjuntoCityGate;
	}

	public void setConjuntoCityGate(Set<CityGateTO> conjuntoCityGate) {

		this.conjuntoCityGate = conjuntoCityGate;
	}

	public BigDecimal getPcs() {

		return pcs;
	}

	public void setPcs(BigDecimal pcs) {

		this.pcs = pcs;
	}

	public BigDecimal getAgua() {

		return agua;
	}

	public void setAgua(BigDecimal agua) {

		this.agua = agua;
	}

	public BigDecimal getIButano() {

		return iButano;
	}

	public void setIButano(BigDecimal iButano) {

		this.iButano = iButano;
	}

	public BigDecimal getNHeptano() {

		return nHeptano;
	}

	public void setNHeptano(BigDecimal nHeptano) {

		this.nHeptano = nHeptano;
	}

	public BigDecimal getArgonio() {

		return argonio;
	}

	public void setArgonio(BigDecimal argonio) {

		this.argonio = argonio;
	}

	public BigDecimal getIPentano() {

		return iPentano;
	}

	public void setIPentano(BigDecimal iPentano) {

		this.iPentano = iPentano;
	}

	public BigDecimal getNHexano() {

		return nHexano;
	}

	public void setNHexano(BigDecimal nHexano) {

		this.nHexano = nHexano;
	}

	public BigDecimal getDioxidoCarbono() {

		return dioxidoCarbono;
	}

	public void setDioxidoCarbono(BigDecimal dioxidoCarbono) {

		this.dioxidoCarbono = dioxidoCarbono;
	}

	public BigDecimal getMetano() {

		return metano;
	}

	public void setMetano(BigDecimal metano) {

		this.metano = metano;
	}

	public BigDecimal getNitrogenio() {

		return nitrogenio;
	}

	public void setNitrogenio(BigDecimal nitrogenio) {

		this.nitrogenio = nitrogenio;
	}

	public BigDecimal getEtano() {

		return etano;
	}

	public void setEtano(BigDecimal etano) {

		this.etano = etano;
	}

	public BigDecimal getMonoxidoCarbono() {

		return monoxidoCarbono;
	}

	public void setMonoxidoCarbono(BigDecimal monoxidoCarbono) {

		this.monoxidoCarbono = monoxidoCarbono;
	}

	public BigDecimal getNNonano() {

		return nNonano;
	}

	public void setNNonano(BigDecimal nNonano) {

		this.nNonano = nNonano;
	}

	public BigDecimal getHelio() {

		return helio;
	}

	public void setHelio(BigDecimal helio) {

		this.helio = helio;
	}

	public BigDecimal getNButano() {

		return nButano;
	}

	public void setNButano(BigDecimal nButano) {

		this.nButano = nButano;
	}

	public BigDecimal getNOctano() {

		return nOctano;
	}

	public void setNOctano(BigDecimal nOctano) {

		this.nOctano = nOctano;
	}

	public BigDecimal getHidrogenio() {

		return hidrogenio;
	}

	public void setHidrogenio(BigDecimal hidrogenio) {

		this.hidrogenio = hidrogenio;
	}

	public BigDecimal getNDecano() {

		return nDecano;
	}

	public void setNDecano(BigDecimal nDecano) {

		this.nDecano = nDecano;
	}

	public BigDecimal getNPentano() {

		return nPentano;
	}

	public void setNPentano(BigDecimal nPentano) {

		this.nPentano = nPentano;
	}

	public BigDecimal getOxigenio() {

		return oxigenio;
	}

	public void setOxigenio(BigDecimal oxigenio) {

		this.oxigenio = oxigenio;
	}

	public BigDecimal getPropano() {

		return propano;
	}

	public void setPropano(BigDecimal propano) {

		this.propano = propano;
	}

	public BigDecimal getSulfetoHidrogenio() {

		return sulfetoHidrogenio;
	}

	public void setSulfetoHidrogenio(BigDecimal sulfetoHidrogenio) {

		this.sulfetoHidrogenio = sulfetoHidrogenio;
	}

}
