/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 13/01/2014 17:21:20
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * Classe responsável pela representação da entidade PontoConsumo para transferência.
 *
 */
@XmlRootElement
public class PontoConsumoTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1890190228501778805L;

	private Long chavePrimaria;

	private String descricao;

	private Integer vencimentoVigente;

	private Integer vencimentoSolicitado;

	private List<Integer> vencimentosPossiveis;

	private String situacaoPontoConsumo;

	private ContratoPontoConsumoTO contratoPontoConsumoTO;

	private String segmento;

	private String ramoAtividade;

	private Boolean indUsaProgramacaoConsumo;

	private TipoComboBoxTO segmentoCombo;

	private TipoComboBoxTO ramoAtividadePontoConsumo;

	private TipoComboBoxTO modalidadeUsoPontoConsumo;

	private String cepPontoConsumo;

	private String numeroPontoConsumo;

	private String complementoPontoConsumo;

	private String referenciaPontoConsumo;

	private TipoComboBoxTO quadraPontoConsumo;

	private TipoComboBoxTO faceQuadraPontoConsumo;

	private TipoComboBoxTO rotaPontoConsumo;

	private String latGrau;

	private String longGrau;

	private String enderecoRemoto;

	private boolean habilitadoPontoConsumo;

	private CepTO cep;

	private String numeroImovel;

	private String codigoLegado;

	public TipoComboBoxTO getSegmentoCombo() {

		return segmentoCombo;
	}

	public void setSegmentoCombo(TipoComboBoxTO segmentoCombo) {

		this.segmentoCombo = segmentoCombo;
	}

	public String getNumeroPontoConsumo() {

		return numeroPontoConsumo;
	}

	public void setNumeroPontoConsumo(String numeroPontoConsumo) {

		this.numeroPontoConsumo = numeroPontoConsumo;
	}

	public String getLatGrau() {

		return latGrau;
	}

	public void setLatGrau(String latGrau) {

		this.latGrau = latGrau;
	}

	public String getLongGrau() {

		return longGrau;
	}

	public void setLongGrau(String longGrau) {

		this.longGrau = longGrau;
	}

	public String getEnderecoRemoto() {

		return enderecoRemoto;
	}

	public void setEnderecoRemoto(String enderecoRemoto) {

		this.enderecoRemoto = enderecoRemoto;
	}

	public Boolean getIndUsaProgramacaoConsumo() {

		return indUsaProgramacaoConsumo;
	}

	public void setIndUsaProgramacaoConsumo(Boolean indUsaProgramacaoConsumo) {

		this.indUsaProgramacaoConsumo = indUsaProgramacaoConsumo;
	}

	public ContratoPontoConsumoTO getContratoPontoConsumoTO() {

		return contratoPontoConsumoTO;
	}

	public void setContratoPontoConsumoTO(ContratoPontoConsumoTO contratoPontoConsumoTO) {

		this.contratoPontoConsumoTO = contratoPontoConsumoTO;
	}

	public String getRamoAtividade() {

		return ramoAtividade;
	}

	public void setRamoAtividade(String ramoAtividade) {

		this.ramoAtividade = ramoAtividade;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public Integer getVencimentoVigente() {

		return vencimentoVigente;
	}

	public void setVencimentoVigente(Integer vencimentoVigente) {

		this.vencimentoVigente = vencimentoVigente;
	}

	public Integer getVencimentoSolicitado() {

		return vencimentoSolicitado;
	}

	public void setVencimentoSolicitado(Integer vencimentoSolicitado) {

		this.vencimentoSolicitado = vencimentoSolicitado;
	}

	public List<Integer> getVencimentosPossiveis() {

		return vencimentosPossiveis;
	}

	public void setVencimentosPossiveis(List<Integer> vencimentosPossiveis) {

		this.vencimentosPossiveis = vencimentosPossiveis;
	}

	public String getSituacaoPontoConsumo() {

		return situacaoPontoConsumo;
	}

	public void setSituacaoPontoConsumo(String situacaoPontoConsumo) {

		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public TipoComboBoxTO getRamoAtividadePontoConsumo() {

		return ramoAtividadePontoConsumo;
	}

	public void setRamoAtividadePontoConsumo(TipoComboBoxTO ramoAtividadePontoConsumo) {

		this.ramoAtividadePontoConsumo = ramoAtividadePontoConsumo;
	}

	public TipoComboBoxTO getModalidadeUsoPontoConsumo() {

		return modalidadeUsoPontoConsumo;
	}

	public void setModalidadeUsoPontoConsumo(TipoComboBoxTO modalidadeUsoPontoConsumo) {

		this.modalidadeUsoPontoConsumo = modalidadeUsoPontoConsumo;
	}

	public String getCepPontoConsumo() {

		return cepPontoConsumo;
	}

	public void setCepPontoConsumo(String cepPontoConsumo) {

		this.cepPontoConsumo = cepPontoConsumo;
	}

	public String getComplementoPontoConsumo() {

		return complementoPontoConsumo;
	}

	public void setComplementoPontoConsumo(String complementoPontoConsumo) {

		this.complementoPontoConsumo = complementoPontoConsumo;
	}

	public String getReferenciaPontoConsumo() {

		return referenciaPontoConsumo;
	}

	public void setReferenciaPontoConsumo(String referenciaPontoConsumo) {

		this.referenciaPontoConsumo = referenciaPontoConsumo;
	}

	public TipoComboBoxTO getQuadraPontoConsumo() {

		return quadraPontoConsumo;
	}

	public void setQuadraPontoConsumo(TipoComboBoxTO quadraPontoConsumo) {

		this.quadraPontoConsumo = quadraPontoConsumo;
	}

	public TipoComboBoxTO getFaceQuadraPontoConsumo() {

		return faceQuadraPontoConsumo;
	}

	public void setFaceQuadraPontoConsumo(TipoComboBoxTO faceQuadraPontoConsumo) {

		this.faceQuadraPontoConsumo = faceQuadraPontoConsumo;
	}

	public TipoComboBoxTO getRotaPontoConsumo() {

		return rotaPontoConsumo;
	}

	public void setRotaPontoConsumo(TipoComboBoxTO rotaPontoConsumo) {

		this.rotaPontoConsumo = rotaPontoConsumo;
	}

	public boolean isHabilitadoPontoConsumo() {

		return habilitadoPontoConsumo;
	}

	public void setHabilitadoPontoConsumo(boolean habilitadoPontoConsumo) {

		this.habilitadoPontoConsumo = habilitadoPontoConsumo;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getCodigoLegado() {

		return codigoLegado;
	}

	public void setCodigoLegado(String codigoLegado) {

		this.codigoLegado = codigoLegado;
	}

	public CepTO getCep() {
		return cep;
	}

	public void setCep(CepTO cep) {
		this.cep = cep;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
}
