/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.webservice.negocio;

import org.apache.commons.io.FilenameUtils;

import java.text.MessageFormat;

/**
 * Classe utilitária de serviço do GGAS
 * @author jose.victor@logiquesistemas.com.br
 */
public final class ServicoUtil {

	private ServicoUtil() {
	}

	/**
	 * Formata a descrição da agência virtual a fim de tornar a descrição mais completa
	 * @param nomeCliente nome do cliente
	 * @param descricaoAgencia descrição com as informações modificadas pela agência
	 * @param recurso recurso acessado na agêcia virtual
	 * @return retorna a descrição da agência virtual formatada
	 */
	public static String formatarDescricaoChamadoAgencia(String nomeCliente, String descricaoAgencia, String recurso) {
		return new MessageFormat("{0} realizou {1} via Agência Virtual\r\n{2}")
				.format(new Object[]{nomeCliente, recurso, descricaoAgencia});
	}

	/**
	 * Trunca o nome do arquivo em uma quantidade máxima de caracteres
	 * Exemplo de uso:
	 *   truncarNomeArquivo("exemplo.pdf", 6) = "ex.pdf'
	 *
	 * @param nomeArquivo nome do arquivo a ser truncado, exemplo: teste.pdf
	 * @param numeroMaxCaracteres número máximo de caracteres
	 * @return retorna o nome do arquivo truncado
	 */
	public static String truncarNomeArquivo(String nomeArquivo, int numeroMaxCaracteres) {
		String nomeArquivoNormalizado = nomeArquivo;

		if (nomeArquivoNormalizado.length() > numeroMaxCaracteres) {
			final String extensao = FilenameUtils.getExtension(nomeArquivo);
			final int qtdCaracteresARemover = Math.max(nomeArquivoNormalizado.length() - numeroMaxCaracteres, 0);
			final String baseName = FilenameUtils.getBaseName(nomeArquivo);

			nomeArquivoNormalizado = baseName.substring(0, baseName.length() - qtdCaracteresARemover) + '.' + extensao;
		}

		return nomeArquivoNormalizado;
	}

}
