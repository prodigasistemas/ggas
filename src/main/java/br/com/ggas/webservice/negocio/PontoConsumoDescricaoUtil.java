/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.webservice.negocio;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.webservice.PontoConsumoDescricaoTO;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe utilitária para métodos relacionados ao DTO {@link PontoConsumoDescricaoTO}
 * @author jose.victor@logiquesistemas.com.br
 */
public class PontoConsumoDescricaoUtil {

	/**
	 * Construtor privado a fim de evitar a instanciação
	 */
	private PontoConsumoDescricaoUtil() {
	}

	/**
	 * Converte uma coleção de {@link ContratoPontoConsumo} em uma coleção de {@link PontoConsumoDescricaoTO}
	 * @param contratos lista de contratos a serem convertidos
	 * @return retorna os DTOs referentes aos contratos fornecidos
	 */
	public static List<PontoConsumoDescricaoTO> converter(Collection<ContratoPontoConsumo> contratos) {
		List<PontoConsumoDescricaoTO> retorno = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(contratos)) {
			retorno = contratos.stream()
					.map(PontoConsumoDescricaoUtil::converterToDTO)
					.collect(Collectors.toList());
		}

		return retorno;
	}

	/**
	 * Contém lógica para conversão de um {@link ContratoPontoConsumo} em {@link PontoConsumoDescricaoTO}
	 * @param contrato contrato
	 * @return retorna o DTO referente
	 */
	private static PontoConsumoDescricaoTO converterToDTO(ContratoPontoConsumo contrato) {
		PontoConsumoDescricaoTO consumoTO = new PontoConsumoDescricaoTO();
		consumoTO.setChavePrimaria(contrato.getPontoConsumo().getChavePrimaria());
		consumoTO.setDescricao(contrato.getPontoConsumo().getDescricao());
		consumoTO.setSituacao(contrato.getPontoConsumo().getSituacaoConsumo().getDescricao());
		consumoTO.setEnderecoCompleto(contrato.getPontoConsumo().getEnderecoFormatado());
		return consumoTO;
	}
}
