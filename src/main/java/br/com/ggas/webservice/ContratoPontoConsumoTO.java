/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/02/2014 10:49:04
 @author ifrancisco
 */

package br.com.ggas.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ifrancisco
 */
@XmlRootElement
public class ContratoPontoConsumoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3715037873144853681L;

	private String medidaPressao;

	private String unidadePressao;

	private String medidaPressaoMinima;

	private String unidadePressaoMinima;

	private String medidaPressaoMaxima;

	private String unidadePressaoMaxima;

	private String medidaPressaoLimite;

	private String unidadePressaoLimite;

	private String medidaVazaoInstantanea;

	private String unidadeVazaoInstantanea;

	private String medidaVazaoMaximaInstantanea;

	private String unidadeVazaoMaximaInstantanea;

	private String medidaVazaoMinimaInstantanea;

	private String unidadeVazaoMinimaInstantanea;

	private String medidaFornecimentoMaxDiaria;

	private String unidadeFornecimentoMaxDiaria;

	private String medidaFornecimentoMinDiaria;

	private String unidadeFornecimentoMinDiaria;

	private String medidaFornecimentoMinMensal;

	private String unidadeFornecimentoMinMensal;

	private String medidaFornecimentoMinAnual;

	private String unidadeFornecimentoMinAnual;

	private String numeroFatorCorrecao;

	private String numeroDiasVencimento;

	private String acaoAnormalidadeConsumoSemLeitura;

	private String tipoMedicao;

	private Boolean indicadorParadaProgramada;

	private String quantidadeAnosParadaCliente;

	private String quantidadeTotalParadaCliente;

	private String quantidadeMaximaAnoParadaCliente;

	private String diasAntecedentesParadaCliente;

	private String diasConsecutivosParadaCliente;

	private String quantidadeAnosParadaCDL;

	private String quantidadeTotalParadaCDL;

	private String quantidadeMaximaAnoParadaCDL;

	private String diasAntecedentesParadaCDL;

	private String diasConsecutivosParadaCDL;

	private String dataInicioTeste;

	private String dataFimTeste;

	private String numeroImovel;

	private String complementoEndereco;

	private String emailEntrega;

	private Boolean indicadorCorretorVazao;

	private String medidaConsumoTeste;

	private String prazoConsumoTeste;

	private String diaGarantiaConversao;

	private String dataInicioGarantiaConversao;

	private String dataFimGarantiaConversao;

	private String regimeConsumo;

	private String cep;

	private String emailOperacional;

	private String codigoDDDFaxOperacional;

	private String numeroFaxOperacional;

	private String numeroHoraInicial;

	private String contratoCompra;

	private String dataConsumoTesteExcedido;

	private String medidaConsumoTesteUsado;

	private String periodicidade;

	private String faixaPressaoFornecimento;

	private String tamanhoIntervaloPCSPorCPC;

	private boolean isEmiteNotaFiscalEletronica;

	private List<String> listaLocalAmostragemPCS = new ArrayList<String>();

	private List<String> listaLocalAmostragemAssociados = new ArrayList<String>();

	private List<String> listaIntervaloPCS = new ArrayList<String>();

	private List<String> listaIntervaloPCSAssociados = new ArrayList<String>();

	private Collection<ContratoPontoConsumoItemFaturamentoTO> listaContratoPontoConsumoItemFaturamentoTO = 
					new HashSet<ContratoPontoConsumoItemFaturamentoTO>();

	private Collection<ClienteTO> listaClientesTO = new ArrayList<ClienteTO>();

	private Collection<ModalidadeConsumoFaturamentoTO> listaModalidadeConsumoFaturaTO = new ArrayList<ModalidadeConsumoFaturamentoTO>();

	public Collection<ContratoPontoConsumoItemFaturamentoTO> getListaContratoPontoConsumoItemFaturamentoTO() {

		return listaContratoPontoConsumoItemFaturamentoTO;
	}

	public void setListaContratoPontoConsumoItemFaturamentoTO(
					Collection<ContratoPontoConsumoItemFaturamentoTO> listaContratoPontoConsumoItemFaturamentoTO) {

		this.listaContratoPontoConsumoItemFaturamentoTO = listaContratoPontoConsumoItemFaturamentoTO;
	}

	public Collection<ClienteTO> getListaClientesTO() {

		return listaClientesTO;
	}

	public void setListaClientesTO(Collection<ClienteTO> listaClientesTO) {

		this.listaClientesTO = listaClientesTO;
	}

	public String getMedidaPressao() {

		return medidaPressao;
	}

	public void setMedidaPressao(String medidaPressao) {

		this.medidaPressao = medidaPressao;
	}

	public String getUnidadePressao() {

		return unidadePressao;
	}

	public void setUnidadePressao(String unidadePressao) {

		this.unidadePressao = unidadePressao;
	}

	public String getMedidaPressaoMinima() {

		return medidaPressaoMinima;
	}

	public void setMedidaPressaoMinima(String medidaPressaoMinima) {

		this.medidaPressaoMinima = medidaPressaoMinima;
	}

	public String getUnidadePressaoMinima() {

		return unidadePressaoMinima;
	}

	public void setUnidadePressaoMinima(String unidadePressaoMinima) {

		this.unidadePressaoMinima = unidadePressaoMinima;
	}

	public String getMedidaPressaoMaxima() {

		return medidaPressaoMaxima;
	}

	public void setMedidaPressaoMaxima(String medidaPressaoMaxima) {

		this.medidaPressaoMaxima = medidaPressaoMaxima;
	}

	public String getUnidadePressaoMaxima() {

		return unidadePressaoMaxima;
	}

	public void setUnidadePressaoMaxima(String unidadePressaoMaxima) {

		this.unidadePressaoMaxima = unidadePressaoMaxima;
	}

	public String getMedidaPressaoLimite() {

		return medidaPressaoLimite;
	}

	public void setMedidaPressaoLimite(String medidaPressaoLimite) {

		this.medidaPressaoLimite = medidaPressaoLimite;
	}

	public String getUnidadePressaoLimite() {

		return unidadePressaoLimite;
	}

	public void setUnidadePressaoLimite(String unidadePressaoLimite) {

		this.unidadePressaoLimite = unidadePressaoLimite;
	}

	public String getMedidaVazaoInstantanea() {

		return medidaVazaoInstantanea;
	}

	public void setMedidaVazaoInstantanea(String medidaVazaoInstantanea) {

		this.medidaVazaoInstantanea = medidaVazaoInstantanea;
	}

	public String getUnidadeVazaoInstantanea() {

		return unidadeVazaoInstantanea;
	}

	public void setUnidadeVazaoInstantanea(String unidadeVazaoInstantanea) {

		this.unidadeVazaoInstantanea = unidadeVazaoInstantanea;
	}

	public String getMedidaVazaoMaximaInstantanea() {

		return medidaVazaoMaximaInstantanea;
	}

	public void setMedidaVazaoMaximaInstantanea(String medidaVazaoMaximaInstantanea) {

		this.medidaVazaoMaximaInstantanea = medidaVazaoMaximaInstantanea;
	}

	public String getUnidadeVazaoMaximaInstantanea() {

		return unidadeVazaoMaximaInstantanea;
	}

	public void setUnidadeVazaoMaximaInstantanea(String unidadeVazaoMaximaInstantanea) {

		this.unidadeVazaoMaximaInstantanea = unidadeVazaoMaximaInstantanea;
	}

	public String getMedidaVazaoMinimaInstantanea() {

		return medidaVazaoMinimaInstantanea;
	}

	public void setMedidaVazaoMinimaInstantanea(String medidaVazaoMinimaInstantanea) {

		this.medidaVazaoMinimaInstantanea = medidaVazaoMinimaInstantanea;
	}

	public String getUnidadeVazaoMinimaInstantanea() {

		return unidadeVazaoMinimaInstantanea;
	}

	public void setUnidadeVazaoMinimaInstantanea(String unidadeVazaoMinimaInstantanea) {

		this.unidadeVazaoMinimaInstantanea = unidadeVazaoMinimaInstantanea;
	}

	public String getMedidaFornecimentoMaxDiaria() {

		return medidaFornecimentoMaxDiaria;
	}

	public void setMedidaFornecimentoMaxDiaria(String medidaFornecimentoMaxDiaria) {

		this.medidaFornecimentoMaxDiaria = medidaFornecimentoMaxDiaria;
	}

	public String getUnidadeFornecimentoMaxDiaria() {

		return unidadeFornecimentoMaxDiaria;
	}

	public void setUnidadeFornecimentoMaxDiaria(String unidadeFornecimentoMaxDiaria) {

		this.unidadeFornecimentoMaxDiaria = unidadeFornecimentoMaxDiaria;
	}

	public String getMedidaFornecimentoMinDiaria() {

		return medidaFornecimentoMinDiaria;
	}

	public void setMedidaFornecimentoMinDiaria(String medidaFornecimentoMinDiaria) {

		this.medidaFornecimentoMinDiaria = medidaFornecimentoMinDiaria;
	}

	public String getUnidadeFornecimentoMinDiaria() {

		return unidadeFornecimentoMinDiaria;
	}

	public void setUnidadeFornecimentoMinDiaria(String unidadeFornecimentoMinDiaria) {

		this.unidadeFornecimentoMinDiaria = unidadeFornecimentoMinDiaria;
	}

	public String getMedidaFornecimentoMinMensal() {

		return medidaFornecimentoMinMensal;
	}

	public void setMedidaFornecimentoMinMensal(String medidaFornecimentoMinMensal) {

		this.medidaFornecimentoMinMensal = medidaFornecimentoMinMensal;
	}

	public String getUnidadeFornecimentoMinMensal() {

		return unidadeFornecimentoMinMensal;
	}

	public void setUnidadeFornecimentoMinMensal(String unidadeFornecimentoMinMensal) {

		this.unidadeFornecimentoMinMensal = unidadeFornecimentoMinMensal;
	}

	public String getMedidaFornecimentoMinAnual() {

		return medidaFornecimentoMinAnual;
	}

	public void setMedidaFornecimentoMinAnual(String medidaFornecimentoMinAnual) {

		this.medidaFornecimentoMinAnual = medidaFornecimentoMinAnual;
	}

	public String getUnidadeFornecimentoMinAnual() {

		return unidadeFornecimentoMinAnual;
	}

	public void setUnidadeFornecimentoMinAnual(String unidadeFornecimentoMinAnual) {

		this.unidadeFornecimentoMinAnual = unidadeFornecimentoMinAnual;
	}

	public String getNumeroFatorCorrecao() {

		return numeroFatorCorrecao;
	}

	public void setNumeroFatorCorrecao(String numeroFatorCorrecao) {

		this.numeroFatorCorrecao = numeroFatorCorrecao;
	}

	public String getNumeroDiasVencimento() {

		return numeroDiasVencimento;
	}

	public void setNumeroDiasVencimento(String numeroDiasVencimento) {

		this.numeroDiasVencimento = numeroDiasVencimento;
	}

	public String getAcaoAnormalidadeConsumoSemLeitura() {

		return acaoAnormalidadeConsumoSemLeitura;
	}

	public void setAcaoAnormalidadeConsumoSemLeitura(String acaoAnormalidadeConsumoSemLeitura) {

		this.acaoAnormalidadeConsumoSemLeitura = acaoAnormalidadeConsumoSemLeitura;
	}

	public String getTipoMedicao() {

		return tipoMedicao;
	}

	public void setTipoMedicao(String tipoMedicao) {

		this.tipoMedicao = tipoMedicao;
	}

	public Boolean getIndicadorParadaProgramada() {

		return indicadorParadaProgramada;
	}

	public void setIndicadorParadaProgramada(Boolean indicadorParadaProgramada) {

		this.indicadorParadaProgramada = indicadorParadaProgramada;
	}

	public String getQuantidadeAnosParadaCliente() {

		return quantidadeAnosParadaCliente;
	}

	public void setQuantidadeAnosParadaCliente(String quantidadeAnosParadaCliente) {

		this.quantidadeAnosParadaCliente = quantidadeAnosParadaCliente;
	}

	public String getQuantidadeTotalParadaCliente() {

		return quantidadeTotalParadaCliente;
	}

	public void setQuantidadeTotalParadaCliente(String quantidadeTotalParadaCliente) {

		this.quantidadeTotalParadaCliente = quantidadeTotalParadaCliente;
	}

	public String getQuantidadeMaximaAnoParadaCliente() {

		return quantidadeMaximaAnoParadaCliente;
	}

	public void setQuantidadeMaximaAnoParadaCliente(String quantidadeMaximaAnoParadaCliente) {

		this.quantidadeMaximaAnoParadaCliente = quantidadeMaximaAnoParadaCliente;
	}

	public String getDiasAntecedentesParadaCliente() {

		return diasAntecedentesParadaCliente;
	}

	public void setDiasAntecedentesParadaCliente(String diasAntecedentesParadaCliente) {

		this.diasAntecedentesParadaCliente = diasAntecedentesParadaCliente;
	}

	public String getDiasConsecutivosParadaCliente() {

		return diasConsecutivosParadaCliente;
	}

	public void setDiasConsecutivosParadaCliente(String diasConsecutivosParadaCliente) {

		this.diasConsecutivosParadaCliente = diasConsecutivosParadaCliente;
	}

	public String getQuantidadeAnosParadaCDL() {

		return quantidadeAnosParadaCDL;
	}

	public void setQuantidadeAnosParadaCDL(String quantidadeAnosParadaCDL) {

		this.quantidadeAnosParadaCDL = quantidadeAnosParadaCDL;
	}

	public String getQuantidadeTotalParadaCDL() {

		return quantidadeTotalParadaCDL;
	}

	public void setQuantidadeTotalParadaCDL(String quantidadeTotalParadaCDL) {

		this.quantidadeTotalParadaCDL = quantidadeTotalParadaCDL;
	}

	public String getQuantidadeMaximaAnoParadaCDL() {

		return quantidadeMaximaAnoParadaCDL;
	}

	public void setQuantidadeMaximaAnoParadaCDL(String quantidadeMaximaAnoParadaCDL) {

		this.quantidadeMaximaAnoParadaCDL = quantidadeMaximaAnoParadaCDL;
	}

	public String getDiasAntecedentesParadaCDL() {

		return diasAntecedentesParadaCDL;
	}

	public void setDiasAntecedentesParadaCDL(String diasAntecedentesParadaCDL) {

		this.diasAntecedentesParadaCDL = diasAntecedentesParadaCDL;
	}

	public String getDiasConsecutivosParadaCDL() {

		return diasConsecutivosParadaCDL;
	}

	public void setDiasConsecutivosParadaCDL(String diasConsecutivosParadaCDL) {

		this.diasConsecutivosParadaCDL = diasConsecutivosParadaCDL;
	}

	public String getDataInicioTeste() {

		return dataInicioTeste;
	}

	public void setDataInicioTeste(String dataInicioTeste) {

		this.dataInicioTeste = dataInicioTeste;
	}

	public String getDataFimTeste() {

		return dataFimTeste;
	}

	public void setDataFimTeste(String dataFimTeste) {

		this.dataFimTeste = dataFimTeste;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getComplementoEndereco() {

		return complementoEndereco;
	}

	public void setComplementoEndereco(String complementoEndereco) {

		this.complementoEndereco = complementoEndereco;
	}

	public String getEmailEntrega() {

		return emailEntrega;
	}

	public void setEmailEntrega(String emailEntrega) {

		this.emailEntrega = emailEntrega;
	}

	public Boolean getIndicadorCorretorVazao() {

		return indicadorCorretorVazao;
	}

	public void setIndicadorCorretorVazao(Boolean indicadorCorretorVazao) {

		this.indicadorCorretorVazao = indicadorCorretorVazao;
	}

	public String getMedidaConsumoTeste() {

		return medidaConsumoTeste;
	}

	public void setMedidaConsumoTeste(String medidaConsumoTeste) {

		this.medidaConsumoTeste = medidaConsumoTeste;
	}

	public String getPrazoConsumoTeste() {

		return prazoConsumoTeste;
	}

	public void setPrazoConsumoTeste(String prazoConsumoTeste) {

		this.prazoConsumoTeste = prazoConsumoTeste;
	}

	public String getDiaGarantiaConversao() {

		return diaGarantiaConversao;
	}

	public void setDiaGarantiaConversao(String diaGarantiaConversao) {

		this.diaGarantiaConversao = diaGarantiaConversao;
	}

	public String getDataInicioGarantiaConversao() {

		return dataInicioGarantiaConversao;
	}

	public void setDataInicioGarantiaConversao(String dataInicioGarantiaConversao) {

		this.dataInicioGarantiaConversao = dataInicioGarantiaConversao;
	}

	public String getRegimeConsumo() {

		return regimeConsumo;
	}

	public void setRegimeConsumo(String regimeConsumo) {

		this.regimeConsumo = regimeConsumo;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getEmailOperacional() {

		return emailOperacional;
	}

	public void setEmailOperacional(String emailOperacional) {

		this.emailOperacional = emailOperacional;
	}

	public String getCodigoDDDFaxOperacional() {

		return codigoDDDFaxOperacional;
	}

	public void setCodigoDDDFaxOperacional(String codigoDDDFaxOperacional) {

		this.codigoDDDFaxOperacional = codigoDDDFaxOperacional;
	}

	public String getNumeroFaxOperacional() {

		return numeroFaxOperacional;
	}

	public void setNumeroFaxOperacional(String numeroFaxOperacional) {

		this.numeroFaxOperacional = numeroFaxOperacional;
	}

	public String getNumeroHoraInicial() {

		return numeroHoraInicial;
	}

	public void setNumeroHoraInicial(String numeroHoraInicial) {

		this.numeroHoraInicial = numeroHoraInicial;
	}

	public String getContratoCompra() {

		return contratoCompra;
	}

	public void setContratoCompra(String contratoCompra) {

		this.contratoCompra = contratoCompra;
	}

	public String getDataConsumoTesteExcedido() {

		return dataConsumoTesteExcedido;
	}

	public void setDataConsumoTesteExcedido(String dataConsumoTesteExcedido) {

		this.dataConsumoTesteExcedido = dataConsumoTesteExcedido;
	}

	public String getMedidaConsumoTesteUsado() {

		return medidaConsumoTesteUsado;
	}

	public void setMedidaConsumoTesteUsado(String medidaConsumoTesteUsado) {

		this.medidaConsumoTesteUsado = medidaConsumoTesteUsado;
	}

	public String getPeriodicidade() {

		return periodicidade;
	}

	public void setPeriodicidade(String periodicidade) {

		this.periodicidade = periodicidade;
	}

	public String getFaixaPressaoFornecimento() {

		return faixaPressaoFornecimento;
	}

	public void setFaixaPressaoFornecimento(String faixaPressaoFornecimento) {

		this.faixaPressaoFornecimento = faixaPressaoFornecimento;
	}

	public boolean isEmiteNotaFiscalEletronica() {

		return isEmiteNotaFiscalEletronica;
	}

	public void setEmiteNotaFiscalEletronica(boolean isEmiteNotaFiscalEletronica) {

		this.isEmiteNotaFiscalEletronica = isEmiteNotaFiscalEletronica;
	}

	public String getDataFimGarantiaConversao() {

		return dataFimGarantiaConversao;
	}

	public void setDataFimGarantiaConversao(String dataFimGarantiaConversao) {

		this.dataFimGarantiaConversao = dataFimGarantiaConversao;
	}

	public List<String> getListaLocalAmostragemPCS() {

		return listaLocalAmostragemPCS;
	}

	public void setListaLocalAmostragemPCS(List<String> listaLocalAmostragemPCS) {

		this.listaLocalAmostragemPCS = listaLocalAmostragemPCS;
	}

	public List<String> getListaLocalAmostragemAssociados() {

		return listaLocalAmostragemAssociados;
	}

	public void setListaLocalAmostragemAssociados(List<String> listaLocalAmostragemAssociados) {

		this.listaLocalAmostragemAssociados = listaLocalAmostragemAssociados;
	}

	public List<String> getListaIntervaloPCS() {

		return listaIntervaloPCS;
	}

	public void setListaIntervaloPCS(List<String> listaIntervaloPCS) {

		this.listaIntervaloPCS = listaIntervaloPCS;
	}

	public List<String> getListaIntervaloPCSAssociados() {

		return listaIntervaloPCSAssociados;
	}

	public void setListaIntervaloPCSAssociados(List<String> listaIntervaloPCSAssociados) {

		this.listaIntervaloPCSAssociados = listaIntervaloPCSAssociados;
	}

	public String getTamanhoIntervaloPCSPorCPC() {

		return tamanhoIntervaloPCSPorCPC;
	}

	public void setTamanhoIntervaloPCSPorCPC(String tamanhoIntervaloPCSPorCPC) {

		this.tamanhoIntervaloPCSPorCPC = tamanhoIntervaloPCSPorCPC;
	}

	public Collection<ModalidadeConsumoFaturamentoTO> getListaModalidadeConsumoFaturaTO() {

		return listaModalidadeConsumoFaturaTO;
	}

	public void setListaModalidadeConsumoFaturaTO(Collection<ModalidadeConsumoFaturamentoTO> listaModalidadeConsumoFaturaTO) {

		this.listaModalidadeConsumoFaturaTO = listaModalidadeConsumoFaturaTO;
	}

}
