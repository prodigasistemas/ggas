
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade NotaDebito para transferência
 *
 */
@XmlRootElement
public class NotaDebitoTO {

	private Long chavePrimaria;

	private String emissaoInicioNotaDebito;

	private String emissaoFimNotaDebito;

	private String cancelamento;// boleto

	private String sisStatus;

	private String emissao;// boleto

	private String emissaoInicio;// boleto

	private String emissaoFim;// boleto

	private String vencimentoInicio;

	private String vencimentoFim;

	private String numeroDocumento;

	private String valor;

	private String dataEmissao;

	private String dataVencimento;

	private String situacaoPagamento;

	private String numeroDocumentoFiscal;

	private String numeroSerie;

	private String chavePrimariaDocumentoFiscal;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getEmissaoInicioNotaDebito() {

		return emissaoInicioNotaDebito;
	}

	public void setEmissaoInicioNotaDebito(String emissaoInicioNotaDebito) {

		this.emissaoInicioNotaDebito = emissaoInicioNotaDebito;
	}

	public String getEmissaoFimNotaDebito() {

		return emissaoFimNotaDebito;
	}

	public void setEmissaoFimNotaDebito(String emissaoFimNotaDebito) {

		this.emissaoFimNotaDebito = emissaoFimNotaDebito;
	}

	public String getCancelamento() {

		return cancelamento;
	}

	public void setCancelamento(String cancelamento) {

		this.cancelamento = cancelamento;
	}

	public String getSisStatus() {

		return sisStatus;
	}

	public void setSisStatus(String sisStatus) {

		this.sisStatus = sisStatus;
	}

	public String getEmissao() {

		return emissao;
	}

	public void setEmissao(String emissao) {

		this.emissao = emissao;
	}

	public String getEmissaoInicio() {

		return emissaoInicio;
	}

	public void setEmissaoInicio(String emissaoInicio) {

		this.emissaoInicio = emissaoInicio;
	}

	public String getEmissaoFim() {

		return emissaoFim;
	}

	public void setEmissaoFim(String emissaoFim) {

		this.emissaoFim = emissaoFim;
	}

	public String getVencimentoInicio() {

		return vencimentoInicio;
	}

	public void setVencimentoInicio(String vencimentoInicio) {

		this.vencimentoInicio = vencimentoInicio;
	}

	public String getVencimentoFim() {

		return vencimentoFim;
	}

	public void setVencimentoFim(String vencimentoFim) {

		this.vencimentoFim = vencimentoFim;
	}

	public String getNumeroDocumento() {

		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {

		this.numeroDocumento = numeroDocumento;
	}

	public String getValor() {

		return valor;
	}

	public void setValor(String valor) {

		this.valor = valor;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getDataVencimento() {

		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public String getSituacaoPagamento() {

		return situacaoPagamento;
	}

	public void setSituacaoPagamento(String situacaoPagamento) {

		this.situacaoPagamento = situacaoPagamento;
	}

	public String getNumeroDocumentoFiscal() {

		return numeroDocumentoFiscal;
	}

	public void setNumeroDocumentoFiscal(String numeroDocumentoFiscal) {

		this.numeroDocumentoFiscal = numeroDocumentoFiscal;
	}

	public String getNumeroSerie() {

		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	public String getChavePrimariaDocumentoFiscal() {

		return chavePrimariaDocumentoFiscal;
	}

	public void setChavePrimariaDocumentoFiscal(String chavePrimariaDocumentoFiscal) {

		this.chavePrimariaDocumentoFiscal = chavePrimariaDocumentoFiscal;
	}

}
