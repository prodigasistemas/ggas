
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade RedeInternaImovel para transferência
 *
 */
@XmlRootElement
public class RedeInternaImovelTO {

	private Long chavePrimaria;

	private TipoComboBoxTO redeMaterial;

	private Integer quantidadePrumada;

	private Integer quantidadeReguladorHall;

	private boolean ventilacaoHall;

	private boolean ventilacaoApartamento;

	private TipoComboBoxTO redeDiametroCentral;

	private TipoComboBoxTO redeDiametroPrumada;

	private TipoComboBoxTO redeDiametroPrumadaApartamento;

	public TipoComboBoxTO getRedeMaterial() {

		return redeMaterial;
	}

	public void setRedeMaterial(TipoComboBoxTO redeMaterial) {

		this.redeMaterial = redeMaterial;
	}

	public Integer getQuantidadePrumada() {

		return quantidadePrumada;
	}

	public void setQuantidadePrumada(Integer quantidadePrumada) {

		this.quantidadePrumada = quantidadePrumada;
	}

	public Integer getQuantidadeReguladorHall() {

		return quantidadeReguladorHall;
	}

	public void setQuantidadeReguladorHall(Integer quantidadeReguladorHall) {

		this.quantidadeReguladorHall = quantidadeReguladorHall;
	}

	public boolean getVentilacaoHall() {

		return ventilacaoHall;
	}

	public void setVentilacaoHall(boolean ventilacaoHall) {

		this.ventilacaoHall = ventilacaoHall;
	}

	public boolean getVentilacaoApartamento() {

		return ventilacaoApartamento;
	}

	public void setVentilacaoApartamento(boolean ventilacaoApartamento) {

		this.ventilacaoApartamento = ventilacaoApartamento;
	}

	public TipoComboBoxTO getRedeDiametroCentral() {

		return redeDiametroCentral;
	}

	public void setRedeDiametroCentral(TipoComboBoxTO redeDiametroCentral) {

		this.redeDiametroCentral = redeDiametroCentral;
	}

	public TipoComboBoxTO getRedeDiametroPrumada() {

		return redeDiametroPrumada;
	}

	public void setRedeDiametroPrumada(TipoComboBoxTO redeDiametroPrumada) {

		this.redeDiametroPrumada = redeDiametroPrumada;
	}

	public TipoComboBoxTO getRedeDiametroPrumadaApartamento() {

		return redeDiametroPrumadaApartamento;
	}

	public void setRedeDiametroPrumadaApartamento(TipoComboBoxTO redeDiametroPrumadaApartamento) {

		this.redeDiametroPrumadaApartamento = redeDiametroPrumadaApartamento;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

}
