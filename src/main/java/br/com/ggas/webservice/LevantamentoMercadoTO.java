
package br.com.ggas.webservice;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade LancamentoMercado para transferência
 *
 */
@XmlRootElement
public class LevantamentoMercadoTO {

	private Long chavePrimaria;

	private ImovelTO imovelTO;

	private TipoComboBoxTO segmento;

	private String vigenciaInicio;

	private String vigenciaFim;

	private boolean unidadesConsumidoras;

	private String quantidadeUnidadesConsumidoras;

	private boolean habilitado;

	private PropostaTO propostaTO;

	private Collection<LevantamentoMercadoTipoCombustivelTO> listaTipoCombustivel = new ArrayList<LevantamentoMercadoTipoCombustivelTO>();

	private TipoComboBoxTO status;

	private AgendaVisitaAgenteTO agendaVisitaAgente;
	
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public ImovelTO getImovelTO() {

		return imovelTO;
	}

	public void setImovelTO(ImovelTO imovelTO) {

		this.imovelTO = imovelTO;
	}

	public TipoComboBoxTO getSegmento() {

		return segmento;
	}

	public void setSegmento(TipoComboBoxTO segmento) {

		this.segmento = segmento;
	}

	public String getVigenciaInicio() {

		return vigenciaInicio;
	}

	public void setVigenciaInicio(String vigenciaInicio) {

		this.vigenciaInicio = vigenciaInicio;
	}

	public String getVigenciaFim() {

		return vigenciaFim;
	}

	public void setVigenciaFim(String vigenciaFim) {

		this.vigenciaFim = vigenciaFim;
	}

	public boolean isUnidadesConsumidoras() {

		return unidadesConsumidoras;
	}

	public void setUnidadesConsumidoras(boolean unidadesConsumidoras) {

		this.unidadesConsumidoras = unidadesConsumidoras;
	}

	public String getQuantidadeUnidadesConsumidoras() {

		return quantidadeUnidadesConsumidoras;
	}

	public void setQuantidadeUnidadesConsumidoras(String quantidadeUnidadesConsumidoras) {

		this.quantidadeUnidadesConsumidoras = quantidadeUnidadesConsumidoras;
	}

	public Collection<LevantamentoMercadoTipoCombustivelTO> getListaTipoCombustivel() {

		return listaTipoCombustivel;
	}

	public void setListaTipoCombustivel(Collection<LevantamentoMercadoTipoCombustivelTO> listaTipoCombustivel) {

		this.listaTipoCombustivel = listaTipoCombustivel;
	}

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	public PropostaTO getPropostaTO() {

		return propostaTO;
	}

	public void setPropostaTO(PropostaTO propostaTO) {

		this.propostaTO = propostaTO;
	}

	public TipoComboBoxTO getStatus() {

		return status;
	}

	public void setStatus(TipoComboBoxTO status) {

		this.status = status;
	}


	/**
	 * @return the agendaVisitaAgente
	 */
	public AgendaVisitaAgenteTO getAgendaVisitaAgente() {

		return agendaVisitaAgente;
	}


	/**
	 * @param agendaVisitaAgente the agendaVisitaAgente to set
	 */
	public void setAgendaVisitaAgente(AgendaVisitaAgenteTO agendaVisitaAgente) {

		this.agendaVisitaAgente = agendaVisitaAgente;
	}

}
