
package br.com.ggas.webservice;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Classe responsável pela representação da entidade ContainerListas para transferência
 *
 */
@XmlRootElement
public class ContainerListasTO {

	private Collection<TipoComboBoxTO> listaPavimentoCalcadaTO;

	private Collection<TipoComboBoxTO> listaPavimentoRuaTO;

	private Collection<TipoComboBoxTO> listaPadraoConstrucaoTO;

	private Collection<TipoComboBoxTO> listaAreaContruidaTO;

	private Collection<TipoComboBoxTO> listaPerfilImovelTO;

	private Collection<TipoComboBoxTO> listaTIpoBotijaoTO;

	private Collection<TipoComboBoxTO> listaModalidadeMedicaoTO;

	private Collection<TipoComboBoxTO> listaRedeMaterialTO;

	private Collection<TipoComboBoxTO> listaRedeDiametroTO;

	private Collection<TipoComboBoxTO> listaTipoContatoTO;

	private Collection<TipoComboBoxTO> listaProfissaoTO;

	private Collection<TipoComboBoxTO> listaSegmentoTO;

	private TipoComboBoxTO situacaoImovel;

	private Collection<TipoComboBoxTO> listaRamoAtividadeTO;

	private Collection<TipoComboBoxTO> listaAgentes;

	private Collection<TipoComboBoxTO> listaRotas;

	private Collection<TipoComboBoxTO> listaModalidadeUsoTo = new ArrayList<TipoComboBoxTO>();

	private Collection<TipoComboBoxTO> fornecedores;

	private Collection<TipoComboBoxTO> aparelhos;

	private Collection<TipoComboBoxTO> fontesEnergeticas;

	private Collection<TipoComboBoxTO> listaEstadoRede;

	private Collection<TipoComboBoxTO> listaPeriodicidades;

	private Collection<TipoComboBoxTO> listaFuncionarios;

	private Collection<TipoComboBoxTO> listaUnidadesConsumo;

	private Collection<TipoComboBoxTO> listaTarifas;
	
	private Collection<TipoComboBoxTO> listaSituacaoProposta;

	private String consumoMedioAnual;

	private String consumoMedioMensal;

	private String valorPrecoPago;

	private String valorGastoMensal;

	private String valorMensalGasto;

	private String valorMedioTarifa;

	public Collection<TipoComboBoxTO> getFornecedores() {

		return fornecedores;
	}

	public void setFornecedores(Collection<TipoComboBoxTO> fornecedores) {

		this.fornecedores = fornecedores;
	}

	public Collection<TipoComboBoxTO> getAparelhos() {

		return aparelhos;
	}

	public void setAparelhos(Collection<TipoComboBoxTO> aparelhos) {

		this.aparelhos = aparelhos;
	}

	public Collection<TipoComboBoxTO> getFontesEnergeticas() {

		return fontesEnergeticas;
	}

	public void setFontesEnergeticas(Collection<TipoComboBoxTO> fontesEnergeticas) {

		this.fontesEnergeticas = fontesEnergeticas;
	}

	public Collection<TipoComboBoxTO> getListaEstadoRede() {

		return listaEstadoRede;
	}

	public void setListaEstadoRede(Collection<TipoComboBoxTO> listaEstadoRede) {

		this.listaEstadoRede = listaEstadoRede;
	}

	public Collection<TipoComboBoxTO> getListaPeriodicidades() {

		return listaPeriodicidades;
	}

	public void setListaPeriodicidades(Collection<TipoComboBoxTO> listaPeriodicidades) {

		this.listaPeriodicidades = listaPeriodicidades;
	}

	public Collection<TipoComboBoxTO> getListaPavimentoCalcada() {

		return listaPavimentoCalcadaTO;
	}

	public void setListaPavimentoCalcada(Collection<TipoComboBoxTO> listaPavimentoCalcadaTO) {

		this.listaPavimentoCalcadaTO = listaPavimentoCalcadaTO;
	}

	public Collection<TipoComboBoxTO> getListaPavimentoCalcadaTO() {

		return listaPavimentoCalcadaTO;
	}

	public void setListaPavimentoCalcadaTO(Collection<TipoComboBoxTO> listaPavimentoCalcadaTO) {

		this.listaPavimentoCalcadaTO = listaPavimentoCalcadaTO;
	}

	public Collection<TipoComboBoxTO> getListaPavimentoRuaTO() {

		return listaPavimentoRuaTO;
	}

	public void setListaPavimentoRuaTO(Collection<TipoComboBoxTO> listaPavimentoRuaTO) {

		this.listaPavimentoRuaTO = listaPavimentoRuaTO;
	}

	public Collection<TipoComboBoxTO> getListaPadraoConstrucaoTO() {

		return listaPadraoConstrucaoTO;
	}

	public void setListaPadraoConstrucaoTO(Collection<TipoComboBoxTO> listaPadraoConstrucaoTO) {

		this.listaPadraoConstrucaoTO = listaPadraoConstrucaoTO;
	}

	public Collection<TipoComboBoxTO> getListaAreaContruidaTO() {

		return listaAreaContruidaTO;
	}

	public void setListaAreaContruidaTO(Collection<TipoComboBoxTO> listaAreaContruidaTO) {

		this.listaAreaContruidaTO = listaAreaContruidaTO;
	}

	public Collection<TipoComboBoxTO> getListaPerfilImovelTO() {

		return listaPerfilImovelTO;
	}

	public void setListaPerfilImovelTO(Collection<TipoComboBoxTO> listaPerfilImovelTO) {

		this.listaPerfilImovelTO = listaPerfilImovelTO;
	}

	public Collection<TipoComboBoxTO> getListaTIpoBotijaoTO() {

		return listaTIpoBotijaoTO;
	}

	public void setListaTIpoBotijaoTO(Collection<TipoComboBoxTO> listaTIpoBotijaoTO) {

		this.listaTIpoBotijaoTO = listaTIpoBotijaoTO;
	}

	public Collection<TipoComboBoxTO> getListaModalidadeMedicaoTO() {

		return listaModalidadeMedicaoTO;
	}

	public void setListaModalidadeMedicaoTO(Collection<TipoComboBoxTO> listaModalidadeMedicaoTO) {

		this.listaModalidadeMedicaoTO = listaModalidadeMedicaoTO;
	}

	public Collection<TipoComboBoxTO> getListaRedeMaterialTO() {

		return listaRedeMaterialTO;
	}

	public void setListaRedeMaterialTO(Collection<TipoComboBoxTO> listaRedeMaterialTO) {

		this.listaRedeMaterialTO = listaRedeMaterialTO;
	}

	public Collection<TipoComboBoxTO> getListaRedeDiametroTO() {

		return listaRedeDiametroTO;
	}

	public void setListaRedeDiametroTO(Collection<TipoComboBoxTO> listaRedeDiametroTO) {

		this.listaRedeDiametroTO = listaRedeDiametroTO;
	}

	public Collection<TipoComboBoxTO> getListaTipoContatoTO() {

		return listaTipoContatoTO;
	}

	public void setListaTipoContatoTO(Collection<TipoComboBoxTO> listaTipoContatoTO) {

		this.listaTipoContatoTO = listaTipoContatoTO;
	}

	public Collection<TipoComboBoxTO> getListaProfissaoTO() {

		return listaProfissaoTO;
	}

	public void setListaProfissaoTO(Collection<TipoComboBoxTO> listaProfissaoTO) {

		this.listaProfissaoTO = listaProfissaoTO;
	}

	public Collection<TipoComboBoxTO> getListaSegmentoTO() {

		return listaSegmentoTO;
	}

	public void setListaSegmentoTO(Collection<TipoComboBoxTO> listaSegmentoTO) {

		this.listaSegmentoTO = listaSegmentoTO;
	}

	public TipoComboBoxTO getSituacaoImovel() {

		return situacaoImovel;
	}

	public void setSituacaoImovel(TipoComboBoxTO situacaoImovel) {

		this.situacaoImovel = situacaoImovel;
	}

	public Collection<TipoComboBoxTO> getListaRamoAtividadeTO() {

		return listaRamoAtividadeTO;
	}

	public void setListaRamoAtividadeTO(Collection<TipoComboBoxTO> listaRamoAtividadeTO) {

		this.listaRamoAtividadeTO = listaRamoAtividadeTO;
	}

	public Collection<TipoComboBoxTO> getListaRotas() {

		return listaRotas;
	}

	public void setListaRotas(Collection<TipoComboBoxTO> listaRotas) {

		this.listaRotas = listaRotas;
	}

	public Collection<TipoComboBoxTO> getListaAgentes() {

		return listaAgentes;
	}

	public void setListaAgentes(Collection<TipoComboBoxTO> listaAgentes) {

		this.listaAgentes = listaAgentes;
	}

	public Collection<TipoComboBoxTO> getListaModalidadeUsoTo() {

		return listaModalidadeUsoTo;
	}

	public void setListaModalidadeUsoTo(Collection<TipoComboBoxTO> listaModalidadeUsoTo) {

		this.listaModalidadeUsoTo = listaModalidadeUsoTo;
	}

	public Collection<TipoComboBoxTO> getListaFuncionarios() {

		return listaFuncionarios;
	}

	public void setListaFuncionarios(Collection<TipoComboBoxTO> listaFuncionarios) {

		this.listaFuncionarios = listaFuncionarios;
	}

	public Collection<TipoComboBoxTO> getListaUnidadesConsumo() {

		return listaUnidadesConsumo;
	}

	public void setListaUnidadesConsumo(Collection<TipoComboBoxTO> listaUnidadesConsumo) {

		this.listaUnidadesConsumo = listaUnidadesConsumo;
	}

	public Collection<TipoComboBoxTO> getListaTarifas() {

		return listaTarifas;
	}

	public void setListaTarifas(Collection<TipoComboBoxTO> listaTarifas) {

		this.listaTarifas = listaTarifas;
	}

	public String getConsumoMedioAnual() {

		return consumoMedioAnual;
	}

	public void setConsumoMedioAnual(String consumoMedioAnual) {

		this.consumoMedioAnual = consumoMedioAnual;
	}

	public String getConsumoMedioMensal() {

		return consumoMedioMensal;
	}

	public void setConsumoMedioMensal(String consumoMedioMensal) {

		this.consumoMedioMensal = consumoMedioMensal;
	}

	public String getValorPrecoPago() {

		return valorPrecoPago;
	}

	public void setValorPrecoPago(String valorPrecoPago) {

		this.valorPrecoPago = valorPrecoPago;
	}

	public String getValorGastoMensal() {

		return valorGastoMensal;
	}

	public void setValorGastoMensal(String valorGastoMensal) {

		this.valorGastoMensal = valorGastoMensal;
	}

	public String getValorMedioTarifa() {

		return valorMedioTarifa;
	}

	public void setValorMedioTarifa(String valorMedioTarifa) {

		this.valorMedioTarifa = valorMedioTarifa;
	}

	public String getValorMensalGasto() {

		return valorMensalGasto;
	}

	public void setValorMensalGasto(String valorMensalGasto) {

		this.valorMensalGasto = valorMensalGasto;
	}

	public Collection<TipoComboBoxTO> getListaSituacaoProposta() {
		return listaSituacaoProposta;
	}

	public void setListaSituacaoProposta(Collection<TipoComboBoxTO> listaSituacaoProposta) {
		this.listaSituacaoProposta = listaSituacaoProposta;
	}

}
