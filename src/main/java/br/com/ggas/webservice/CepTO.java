/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 13:47:11
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade Cep para transferência
 *
 */
@XmlRootElement
public class CepTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7109744333022741441L;

	private Long chavePrimaria;

	private String uf;

	private String nomeMunicipio;

	private String cep;

	private String logradouro;

	private String tipoLogradouro;

	private String bairro;

	private String intervaloNumeracao;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getUf() {

		return uf;
	}

	public void setUf(String uf) {

		this.uf = uf;
	}

	public String getNomeMunicipio() {

		return nomeMunicipio;
	}

	public void setNomeMunicipio(String nomeMunicipio) {

		this.nomeMunicipio = nomeMunicipio;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getLogradouro() {

		return logradouro;
	}

	public void setLogradouro(String logradouro) {

		this.logradouro = logradouro;
	}

	public String getTipoLogradouro() {

		return tipoLogradouro;
	}

	public void setTipoLogradouro(String tipoLogradouro) {

		this.tipoLogradouro = tipoLogradouro;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getIntervaloNumeracao() {

		return intervaloNumeracao;
	}

	public void setIntervaloNumeracao(String intervaloNumeracao) {

		this.intervaloNumeracao = intervaloNumeracao;
	}

}
