/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/02/2014 14:28:23
 @author ifrancisco
 */

package br.com.ggas.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * ContratoPontoConsumoItemFaturamentoTO
 */
@XmlRootElement
public class ContratoPontoConsumoItemFaturamentoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6275676589568480194L;

	private Long chavePrimariaItemFatura;

	private String itemDesc;

	private String tarifa;

	private String diaVencimento;

	private String opcao;

	private String apos;

	private String vencimentoEmDiaUtil;

	private String dataReferenciaCambial;

	private String percminimoQDC;

	private String diaCotacao;

	private boolean depositoIdentificado;

	private boolean indicadorVencDiaNaoUtil;

	public String getItemDesc() {

		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {

		this.itemDesc = itemDesc;
	}

	public String getTarifa() {

		return tarifa;
	}

	public void setTarifa(String tarifa) {

		this.tarifa = tarifa;
	}

	public String getDiaVencimento() {

		return diaVencimento;
	}

	public void setDiaVencimento(String diaVencimento) {

		this.diaVencimento = diaVencimento;
	}

	public String getOpcao() {

		return opcao;
	}

	public void setOpcao(String opcao) {

		this.opcao = opcao;
	}

	public String getApos() {

		return apos;
	}

	public void setApos(String apos) {

		this.apos = apos;
	}

	public String getVencimentoEmDiaUtil() {

		return vencimentoEmDiaUtil;
	}

	public void setVencimentoEmDiaUtil(String vencimentoEmDiaUtil) {

		this.vencimentoEmDiaUtil = vencimentoEmDiaUtil;
	}

	public Long getChavePrimariaItemFatura() {

		return chavePrimariaItemFatura;
	}

	public void setChavePrimariaItemFatura(Long chavePrimariaItemFatura) {

		this.chavePrimariaItemFatura = chavePrimariaItemFatura;
	}

	public String getDataReferenciaCambial() {

		return dataReferenciaCambial;
	}

	public void setDataReferenciaCambial(String dataReferenciaCambial) {

		this.dataReferenciaCambial = dataReferenciaCambial;
	}

	public String getPercminimoQDC() {

		return percminimoQDC;
	}

	public void setPercminimoQDC(String percminimoQDC) {

		this.percminimoQDC = percminimoQDC;
	}

	public String getDiaCotacao() {

		return diaCotacao;
	}

	public void setDiaCotacao(String diaCotacao) {

		this.diaCotacao = diaCotacao;
	}

	public boolean isDepositoIdentificado() {

		return depositoIdentificado;
	}

	public void setDepositoIdentificado(boolean depositoIdentificado) {

		this.depositoIdentificado = depositoIdentificado;
	}

	public boolean isIndicadorVencDiaNaoUtil() {

		return indicadorVencDiaNaoUtil;
	}

	public void setIndicadorVencDiaNaoUtil(boolean indicadorVencDiaNaoUtil) {

		this.indicadorVencDiaNaoUtil = indicadorVencDiaNaoUtil;
	}

}
