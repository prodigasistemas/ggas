
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade LevantamentoMercadoTipoCombustivelAparelho para transferência
 *
 */
@XmlRootElement
public class LevantamentoMercadoTipoCombustivelAparelhoTO {

	private Long chavePrimaria;

	private TipoComboBoxTO tipoAparelho;

	private boolean habilitado;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public TipoComboBoxTO getTipoAparelho() {

		return tipoAparelho;
	}

	public void setTipoAparelho(TipoComboBoxTO tipoAparelho) {

		this.tipoAparelho = tipoAparelho;
	}

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

}
