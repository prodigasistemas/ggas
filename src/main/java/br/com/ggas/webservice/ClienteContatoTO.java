/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 15:07:49
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade ClienteContato para transferência
 *
 */
@XmlRootElement
public class ClienteContatoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4222218748224802868L;

	private Long chavePrimaria;

	private TipoComboBoxTO tipoContato;

	private String nomeContato;

	private Integer codigoDDD;

	private Integer foneContato;

	private Integer ramalContato;

	private TipoComboBoxTO profissao;

	private String descricaoArea;

	private String email;

	private boolean principal;

	private Long chavePrimariaTipoAcao;

	private TipoComboBoxTO tipoAcao;

	private boolean indicadorExclusao;

	private boolean indicadorRegistroExistente;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public TipoComboBoxTO getTipoContato() {

		return tipoContato;
	}

	public void setTipoContato(TipoComboBoxTO tipoContato) {

		this.tipoContato = tipoContato;
	}

	public String getNomeContato() {

		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {

		this.nomeContato = nomeContato;
	}

	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	public Integer getFoneContato() {

		return foneContato;
	}

	public void setFoneContato(Integer foneContato) {

		this.foneContato = foneContato;
	}

	public Integer getRamalContato() {

		return ramalContato;
	}

	public void setRamalContato(Integer ramalContato) {

		this.ramalContato = ramalContato;
	}

	public TipoComboBoxTO getProfissao() {

		return profissao;
	}

	public void setProfissao(TipoComboBoxTO profissao) {

		this.profissao = profissao;
	}

	public String getDescricaoArea() {

		return descricaoArea;
	}

	public void setDescricaoArea(String descricaoArea) {

		this.descricaoArea = descricaoArea;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public boolean isPrincipal() {

		return principal;
	}

	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	public Long getChavePrimariaTipoAcao() {

		return chavePrimariaTipoAcao;
	}

	public void setChavePrimariaTipoAcao(Long chavePrimariaTipoAcao) {

		this.chavePrimariaTipoAcao = chavePrimariaTipoAcao;
	}

	public TipoComboBoxTO getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(TipoComboBoxTO tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	public boolean isIndicadorExclusao() {

		return indicadorExclusao;
	}

	public void setIndicadorExclusao(boolean indicadorExclusao) {

		this.indicadorExclusao = indicadorExclusao;
	}

	public boolean isIndicadorRegistroExistente() {

		return indicadorRegistroExistente;
	}

	public void setIndicadorRegistroExistente(boolean indicadorRegistroExistente) {

		this.indicadorRegistroExistente = indicadorRegistroExistente;
	}

}
