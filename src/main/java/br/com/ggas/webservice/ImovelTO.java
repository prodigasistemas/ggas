
package br.com.ggas.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * Classe responsável pela representação da entidade Imovel para transferência
 *
 */
@XmlRootElement
public class ImovelTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4320341880917973475L;

	private Long chavePrimaria;

	private TipoComboBoxTO quadra;

	private TipoComboBoxTO quadraFace;

	private String enderecoReferencia;

	private TipoComboBoxTO areaConstruidaFaixa;

	private TipoComboBoxTO pavimentoCalcada;

	private TipoComboBoxTO pavimentoRua;

	private TipoComboBoxTO situacaoImovel;

	private TipoComboBoxTO perfilImovel;

	private TipoComboBoxTO padraoConstrucao;

	private TipoComboBoxTO tipoBotijao;

	private TipoComboBoxTO modalidadeMedicaoImovel;

	private Integer numeroLote;

	private Integer numeroSublote;

	private Integer numeroTestada;

	private Integer quantidadeUnidadeConsumidora;

	private Integer quantidadePontoConsumo;

	private Integer numeroSequenciaLeitura;

	private Integer quantidadeBloco;

	private Integer quantidadeAndar;

	private Integer quantidadeBanheiro;

	private Integer quantidadeApartamentoAndar;

	private String numeroImovel;

	private String descricaoComplemento;

	private String nome;

	private String cep;

	private String dataEntrega;

	private boolean condominio;

	private boolean portaria;

	private boolean valvulaBloqueio;

	private boolean redePreexistente;

	private TipoComboBoxTO rota;

	private String dataPrevisaoEncerramentoObra;

	private boolean indicadorObraTubulacao;

	private boolean indicadorRestricaoServico;
	
	private List<ContatoImovelTO> contatos = new ArrayList<ContatoImovelTO>();

	private boolean habilitado;

	private TipoComboBoxTO agente;

	private RedeInternaImovelTO redeInternaImovelTO;

	private List<ImovelRamoAtividadeTO> unidadesConsumidoras = new ArrayList<ImovelRamoAtividadeTO>();

	private String endereco;

	private List<PontoConsumoTO> listaPontoConsumo = new ArrayList<PontoConsumoTO>();

	private String foneContato;

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;

	}

	public Long getChavePrimaria() {

		return this.chavePrimaria;
	}

	public TipoComboBoxTO getQuadra() {

		return quadra;
	}

	public void setQuadra(TipoComboBoxTO quadra) {

		this.quadra = quadra;
	}

	public TipoComboBoxTO getQuadraFace() {

		return quadraFace;
	}

	public void setQuadraFace(TipoComboBoxTO quadraFace) {

		this.quadraFace = quadraFace;
	}

	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	public TipoComboBoxTO getAreaConstruidaFaixa() {

		return areaConstruidaFaixa;
	}

	public void setAreaConstruidaFaixa(TipoComboBoxTO areaConstruidaFaixa) {

		this.areaConstruidaFaixa = areaConstruidaFaixa;
	}

	public TipoComboBoxTO getPavimentoCalcada() {

		return pavimentoCalcada;
	}

	public void setPavimentoCalcada(TipoComboBoxTO pavimentoCalcada) {

		this.pavimentoCalcada = pavimentoCalcada;
	}

	public TipoComboBoxTO getPavimentoRua() {

		return pavimentoRua;
	}

	public void setPavimentoRua(TipoComboBoxTO pavimentoRua) {

		this.pavimentoRua = pavimentoRua;
	}

	public TipoComboBoxTO getSituacaoImovel() {

		return situacaoImovel;
	}

	public void setSituacaoImovel(TipoComboBoxTO situacaoImovel) {

		this.situacaoImovel = situacaoImovel;
	}

	public TipoComboBoxTO getPerfilImovel() {

		return perfilImovel;
	}

	public void setPerfilImovel(TipoComboBoxTO perfilImovel) {

		this.perfilImovel = perfilImovel;
	}

	public TipoComboBoxTO getPadraoConstrucao() {

		return padraoConstrucao;
	}

	public void setPadraoConstrucao(TipoComboBoxTO padraoConstrucao) {

		this.padraoConstrucao = padraoConstrucao;
	}

	public TipoComboBoxTO getTipoBotijao() {

		return tipoBotijao;
	}

	public void setTipoBotijao(TipoComboBoxTO tipoBotijao) {

		this.tipoBotijao = tipoBotijao;
	}

	public TipoComboBoxTO getModalidadeMedicaoImovel() {

		return modalidadeMedicaoImovel;
	}

	public void setModalidadeMedicaoImovel(TipoComboBoxTO modalidadeMedicaoImovel) {

		this.modalidadeMedicaoImovel = modalidadeMedicaoImovel;
	}

	public Integer getNumeroLote() {

		return numeroLote;
	}

	public void setNumeroLote(Integer numeroLote) {

		this.numeroLote = numeroLote;
	}

	public Integer getNumeroSublote() {

		return numeroSublote;
	}

	public void setNumeroSublote(Integer numeroSublote) {

		this.numeroSublote = numeroSublote;
	}

	public Integer getNumeroTestada() {

		return numeroTestada;
	}

	public void setNumeroTestada(Integer numeroTestada) {

		this.numeroTestada = numeroTestada;
	}

	public Integer getQuantidadeUnidadeConsumidora() {

		return quantidadeUnidadeConsumidora;
	}

	public void setQuantidadeUnidadeConsumidora(Integer quantidadeUnidadeConsumidora) {

		this.quantidadeUnidadeConsumidora = quantidadeUnidadeConsumidora;
	}

	public Integer getQuantidadePontoConsumo() {

		return quantidadePontoConsumo;
	}

	public void setQuantidadePontoConsumo(Integer quantidadePontoConsumo) {

		this.quantidadePontoConsumo = quantidadePontoConsumo;
	}

	public Integer getNumeroSequenciaLeitura() {

		return numeroSequenciaLeitura;
	}

	public void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura) {

		this.numeroSequenciaLeitura = numeroSequenciaLeitura;
	}

	public Integer getQuantidadeBloco() {

		return quantidadeBloco;
	}

	public void setQuantidadeBloco(Integer quantidadeBloco) {

		this.quantidadeBloco = quantidadeBloco;
	}

	public Integer getQuantidadeAndar() {

		return quantidadeAndar;
	}

	public void setQuantidadeAndar(Integer quantidadeAndar) {

		this.quantidadeAndar = quantidadeAndar;
	}

	public Integer getQuantidadeBanheiro() {

		return quantidadeBanheiro;
	}

	public void setQuantidadeBanheiro(Integer quantidadeBanheiro) {

		this.quantidadeBanheiro = quantidadeBanheiro;
	}

	public Integer getQuantidadeApartamentoAndar() {

		return quantidadeApartamentoAndar;
	}

	public void setQuantidadeApartamentoAndar(Integer quantidadeApartamentoAndar) {

		this.quantidadeApartamentoAndar = quantidadeApartamentoAndar;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getDescricaoComplemento() {

		return descricaoComplemento;
	}

	public void setDescricaoComplemento(String descricaoComplemento) {

		this.descricaoComplemento = descricaoComplemento;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getDataEntrega() {

		return dataEntrega;
	}

	public void setDataEntrega(String dataEntrega) {

		this.dataEntrega = dataEntrega;
	}

	public boolean getCondominio() {

		return condominio;
	}

	public void setCondominio(boolean condominio) {

		this.condominio = condominio;
	}

	public boolean getPortaria() {

		return portaria;
	}

	public void setPortaria(boolean portaria) {

		this.portaria = portaria;
	}

	public boolean getValvulaBloqueio() {

		return valvulaBloqueio;
	}

	public void setValvulaBloqueio(boolean valvulaBloqueio) {

		this.valvulaBloqueio = valvulaBloqueio;
	}

	public boolean getRedePreexistente() {

		return redePreexistente;
	}

	public void setRedePreexistente(boolean redePreexistente) {

		this.redePreexistente = redePreexistente;
	}

	public TipoComboBoxTO getRota() {

		return rota;
	}

	public void setRota(TipoComboBoxTO rota) {

		this.rota = rota;
	}

	public String getDataPrevisaoEncerramentoObra() {

		return dataPrevisaoEncerramentoObra;
	}

	public void setDataPrevisaoEncerramentoObra(String dataPrevisaoEncerramentoObra) {

		this.dataPrevisaoEncerramentoObra = dataPrevisaoEncerramentoObra;
	}

	public boolean getIndicadorObraTubulacao() {

		return indicadorObraTubulacao;
	}

	public void setIndicadorObraTubulacao(boolean indicadorObraTubulacao) {

		this.indicadorObraTubulacao = indicadorObraTubulacao;
	}

	public TipoComboBoxTO getAgente() {

		return agente;
	}

	public void setAgente(TipoComboBoxTO agente) {

		this.agente = agente;
	}

	public RedeInternaImovelTO getRedeInternaImovelTO() {

		return redeInternaImovelTO;
	}

	public void setRedeInternaImovelTO(RedeInternaImovelTO redeInternaImovelTO) {

		this.redeInternaImovelTO = redeInternaImovelTO;
	}

	public List<ContatoImovelTO> getContatos() {

		return contatos;
	}

	public void setContatos(List<ContatoImovelTO> contatos) {

		this.contatos = contatos;
	}

	public List<ImovelRamoAtividadeTO> getUnidadesConsumidoras() {

		return unidadesConsumidoras;
	}

	public void setUnidadesConsumidoras(List<ImovelRamoAtividadeTO> unidadesConsumidoras) {

		this.unidadesConsumidoras = unidadesConsumidoras;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public List<PontoConsumoTO> getListaPontoConsumo() {

		return listaPontoConsumo;
	}

	public void setListaPontoConsumo(List<PontoConsumoTO> listaPontoConsumo) {

		this.listaPontoConsumo = listaPontoConsumo;
	}

	/**
	 * @return the foneContato
	 */
	public String getFoneContato() {

		return foneContato;
	}

	/**
	 * @param foneContato
	 *            the foneContato to set
	 */
	public void setFoneContato(String foneContato) {

		this.foneContato = foneContato;
	}

	public boolean isIndicadorRestricaoServico() {
		return indicadorRestricaoServico;
	}

	public void setIndicadorRestricaoServico(boolean indicadorRestricaoServico) {
		this.indicadorRestricaoServico = indicadorRestricaoServico;
	}
}
