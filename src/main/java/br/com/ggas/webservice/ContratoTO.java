/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 13/01/2014 17:18:51
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.SerializationUtils;
/**
 * 
 * Classe responsável pela representação da entidade Contrato para transferência
 *
 */
@XmlRootElement
public class ContratoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 574365608697813095L;

	private Long chavePrimaria;

	private String numeroContrato;

	private String numeroAnteriorContrato;

	private String numeroAditivo;

	private String descricaoAditivo;

	private String dataAditivo;

	private String situacaoContrato;

	private String dataAssinatura;

	private String numeroEmpenho;

	private Date vencimentoObrigContrato;

	private boolean definicaoAnoContrato;

	private boolean renovacaoAutomatica;

	private String valorContrato;

	private boolean faturamentoAgrupado;

	private boolean emissaoFatuaAgrupada;

	private String formaCobranca;

	private boolean indicadorMultaAtraso;

	private boolean indicadorJurosMora;

	private String indiceCorrecaoMonetaria;

	private String tipoGarantiaFinanceira;

	private String descGarantiaFinanc;

	private String valorGarantiaFinanceira;

	private String garantiaFinanceiraRenovada;

	private Long chavePrimariaTitularContrato;

	private List<String> listaQdc = new ArrayList<String>();

	private String titularContrato;

	private List<PontoConsumoTO> listaPontoConsumoTO;

	private String cpfCnpjCliente;

	private String rgCliente;

	private String emailPrincialCliente;

	private String emailSecundarioCliente;

	private String codigoDDDCliente;

	private String telefoneCliente;

	private boolean agrupamentoCobranca;

	private boolean usaProgramacaoDeConsumo;

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getTitularContrato() {

		return titularContrato;
	}

	public void setTitularContrato(String titularContrato) {

		this.titularContrato = titularContrato;
	}

	public List<PontoConsumoTO> getListaPontoConsumoTO() {

		return listaPontoConsumoTO;
	}

	public void setListaPontoConsumoTO(List<PontoConsumoTO> listaPontoConsumoTO) {

		this.listaPontoConsumoTO = listaPontoConsumoTO;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getNumeroAnteriorContrato() {

		return numeroAnteriorContrato;
	}

	public void setNumeroAnteriorContrato(String numeroAnteriorContrato) {

		this.numeroAnteriorContrato = numeroAnteriorContrato;
	}

	public String getNumeroAditivo() {

		return numeroAditivo;
	}

	public void setNumeroAditivo(String numeroAditivo) {

		this.numeroAditivo = numeroAditivo;
	}

	public String getDescricaoAditivo() {

		return descricaoAditivo;
	}

	public void setDescricaoAditivo(String descricaoAditivo) {

		this.descricaoAditivo = descricaoAditivo;
	}

	public String getSituacaoContrato() {

		return situacaoContrato;
	}

	public void setSituacaoContrato(String situacaoContrato) {

		this.situacaoContrato = situacaoContrato;
	}

	public String getDataAssinatura() {

		return dataAssinatura;
	}

	public void setDataAssinatura(String dataAssinatura) {

		this.dataAssinatura = dataAssinatura;
	}

	public String getNumeroEmpenho() {

		return numeroEmpenho;
	}

	public void setNumeroEmpenho(String numeroEmpenho) {

		this.numeroEmpenho = numeroEmpenho;
	}

	public Date getVencimentoObrigContrato() {

		return (Date) SerializationUtils.clone(vencimentoObrigContrato);
	}

	public void setVencimentoObrigContrato(Date vencimentoObrigContrato) {

		this.vencimentoObrigContrato = vencimentoObrigContrato;
	}

	public boolean isDefinicaoAnoContrato() {

		return definicaoAnoContrato;
	}

	public void setDefinicaoAnoContrato(boolean definicaoAnoContrato) {

		this.definicaoAnoContrato = definicaoAnoContrato;
	}

	public boolean isRenovacaoAutomatica() {

		return renovacaoAutomatica;
	}

	public void setRenovacaoAutomatica(boolean renovacaoAutomatica) {

		this.renovacaoAutomatica = renovacaoAutomatica;
	}

	public String getValorContrato() {

		return valorContrato;
	}

	public void setValorContrato(String valorContrato) {

		this.valorContrato = valorContrato;
	}

	public boolean isFaturamentoAgrupado() {

		return faturamentoAgrupado;
	}

	public void setFaturamentoAgrupado(boolean faturamentoAgrupado) {

		this.faturamentoAgrupado = faturamentoAgrupado;
	}

	public boolean isEmissaoFatuaAgrupada() {

		return emissaoFatuaAgrupada;
	}

	public void setEmissaoFatuaAgrupada(boolean emissaoFatuaAgrupada) {

		this.emissaoFatuaAgrupada = emissaoFatuaAgrupada;
	}

	public String getFormaCobranca() {

		return formaCobranca;
	}

	public void setFormaCobranca(String formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	public String getValorGarantiaFinanceira() {

		return valorGarantiaFinanceira;
	}

	public void setValorGarantiaFinanceira(String valorGarantiaFinanceira) {

		this.valorGarantiaFinanceira = valorGarantiaFinanceira;
	}

	public List<String> getListaQdc() {

		return listaQdc;
	}

	public void setListaQdc(List<String> listaQdc) {

		this.listaQdc = listaQdc;
	}

	public String getDataAditivo() {

		return dataAditivo;
	}

	public void setDataAditivo(String dataAditivo) {

		this.dataAditivo = dataAditivo;
	}

	public boolean isIndicadorMultaAtraso() {

		return indicadorMultaAtraso;
	}

	public void setIndicadorMultaAtraso(boolean indicadorMultaAtraso) {

		this.indicadorMultaAtraso = indicadorMultaAtraso;
	}

	public boolean isIndicadorJurosMora() {

		return indicadorJurosMora;
	}

	public void setIndicadorJurosMora(boolean indicadorJurosMora) {

		this.indicadorJurosMora = indicadorJurosMora;
	}

	public String getIndiceCorrecaoMonetaria() {

		return indiceCorrecaoMonetaria;
	}

	public void setIndiceCorrecaoMonetaria(String indiceCorrecaoMonetaria) {

		this.indiceCorrecaoMonetaria = indiceCorrecaoMonetaria;
	}

	public String getTipoGarantiaFinanceira() {

		return tipoGarantiaFinanceira;
	}

	public void setTipoGarantiaFinanceira(String tipoGarantiaFinanceira) {

		this.tipoGarantiaFinanceira = tipoGarantiaFinanceira;
	}

	public String getDescGarantiaFinanc() {

		return descGarantiaFinanc;
	}

	public void setDescGarantiaFinanc(String descGarantiaFinanc) {

		this.descGarantiaFinanc = descGarantiaFinanc;
	}

	public String getGarantiaFinanceiraRenovada() {

		return garantiaFinanceiraRenovada;
	}

	public void setGarantiaFinanceiraRenovada(String garantiaFinanceiraRenovada) {

		this.garantiaFinanceiraRenovada = garantiaFinanceiraRenovada;
	}

	public Long getChavePrimariaTitularContrato() {

		return chavePrimariaTitularContrato;
	}

	public void setChavePrimariaTitularContrato(Long chavePrimariaTitularContrato) {

		this.chavePrimariaTitularContrato = chavePrimariaTitularContrato;
	}

	public String getCpfCnpjCliente() {

		return cpfCnpjCliente;
	}

	public void setCpfCnpjCliente(String cpfCnpjCliente) {

		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	public String getRgCliente() {

		return rgCliente;
	}

	public void setRgCliente(String rgCliente) {

		this.rgCliente = rgCliente;
	}

	public String getEmailPrincialCliente() {

		return emailPrincialCliente;
	}

	public void setEmailPrincialCliente(String emailPrincialCliente) {

		this.emailPrincialCliente = emailPrincialCliente;
	}

	public String getEmailSecundarioCliente() {

		return emailSecundarioCliente;
	}

	public void setEmailSecundarioCliente(String emailSecundarioCliente) {

		this.emailSecundarioCliente = emailSecundarioCliente;
	}

	public String getTelefoneCliente() {

		return telefoneCliente;
	}

	public void setTelefoneCliente(String telefoneCliente) {

		this.telefoneCliente = telefoneCliente;
	}

	public String getCodigoDDDCliente() {

		return codigoDDDCliente;
	}

	public void setCodigoDDDCliente(String codigoDDDCliente) {

		this.codigoDDDCliente = codigoDDDCliente;
	}

	public boolean isAgrupamentoCobranca() {

		return agrupamentoCobranca;
	}

	public void setAgrupamentoCobranca(boolean agrupamentoCobranca) {

		this.agrupamentoCobranca = agrupamentoCobranca;
	}

	public boolean getUsaProgramacaoDeConsumo() {

		return usaProgramacaoDeConsumo;
	}

	public void setUsaProgramacaoDeConsumo(boolean usaProgramacaoDeConsumo) {

		this.usaProgramacaoDeConsumo = usaProgramacaoDeConsumo;
	}

}
