/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 13/01/2014 17:21:20
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.ggas.cadastro.cliente.dominio.ClienteAnexoTO;
/**
 * 
 * Classe responsável pela representação da entidade DadosSolicitante para transferência
 *
 */
@XmlRootElement
public class DadosSolicitanteTO {

	private Long chavePrimariaPontoConsumo;

	private Long chavePrimariaContrato;

	private String nomeSolicitante;

	private String cpfCnpjSolicitante;

	private String rgSolicitante;

	private String telefoneSolicitante;

	private String telefoneSecundario;

	private String emailSolicitante;

	private String dddTelefone;

	private String dddTelefoneSecundario;

	private String logradouro;

	private String numeroImovel;

	private String complementoImovel;

	private String cep;

	private String pontoReferenciaImovel;

	private String observacao;

	private String data;

	private Long naturezaImovel;

	private String tipoComunicado;

	private Long chavePrimariaCliente;

	private String confirmacaoEmail;

	private String nomeImovel;

	private List<ClienteAnexoTO> anexos = new ArrayList<ClienteAnexoTO>();

	public Long getChavePrimariaPontoConsumo() {

		return chavePrimariaPontoConsumo;
	}

	public void setChavePrimariaPontoConsumo(Long chavePrimariaPontoConsumo) {

		this.chavePrimariaPontoConsumo = chavePrimariaPontoConsumo;
	}

	public Long getChavePrimariaContrato() {

		return chavePrimariaContrato;
	}

	public void setChavePrimariaContrato(Long chavePrimariaContrato) {

		this.chavePrimariaContrato = chavePrimariaContrato;
	}

	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	public String getCpfCnpjSolicitante() {

		return cpfCnpjSolicitante;
	}

	public void setCpfCnpjSolicitante(String cpfCnpjSolicitante) {

		this.cpfCnpjSolicitante = cpfCnpjSolicitante;
	}

	public String getRgSolicitante() {

		return rgSolicitante;
	}

	public void setRgSolicitante(String rgSolicitante) {

		this.rgSolicitante = rgSolicitante;
	}

	public String getTelefoneSolicitante() {

		return telefoneSolicitante;
	}

	public void setTelefoneSolicitante(String telefoneSolicitante) {

		this.telefoneSolicitante = telefoneSolicitante;
	}

	public String getEmailSolicitante() {

		return emailSolicitante;
	}

	public void setEmailSolicitante(String emailSolicitante) {

		this.emailSolicitante = emailSolicitante;
	}

	public String getDddTelefone() {

		return dddTelefone;
	}

	public void setDddTelefone(String dddTelefone) {

		this.dddTelefone = dddTelefone;
	}

	public String getLogradouro() {

		return logradouro;
	}

	public void setLogradouro(String logradouro) {

		this.logradouro = logradouro;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getComplementoImovel() {

		return complementoImovel;
	}

	public void setComplementoImovel(String complementoImovel) {

		this.complementoImovel = complementoImovel;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getPontoReferenciaImovel() {

		return pontoReferenciaImovel;
	}

	public void setPontoReferenciaImovel(String pontoReferenciaImovel) {

		this.pontoReferenciaImovel = pontoReferenciaImovel;
	}

	public String getObservacao() {

		return observacao;
	}

	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	public Long getNaturezaImovel() {

		return naturezaImovel;
	}

	public void setNaturezaImovel(Long naturezaImovel) {

		this.naturezaImovel = naturezaImovel;
	}

	public String getTipoComunicado() {

		return tipoComunicado;
	}

	public void setTipoComunicado(String tipoComunicado) {

		this.tipoComunicado = tipoComunicado;
	}

	public Long getChavePrimariaCliente() {

		return chavePrimariaCliente;
	}

	public void setChavePrimariaCliente(Long chavePrimariaCliente) {

		this.chavePrimariaCliente = chavePrimariaCliente;
	}

	public String getConfirmacaoEmail() {

		return confirmacaoEmail;
	}

	public void setConfirmacaoEmail(String confirmacaoEmail) {

		this.confirmacaoEmail = confirmacaoEmail;
	}

	public String getTelefoneSecundario() {

		return telefoneSecundario;
	}

	public void setTelefoneSecundario(String telefoneSecundario) {

		this.telefoneSecundario = telefoneSecundario;
	}

	public String getDddTelefoneSecundario() {

		return dddTelefoneSecundario;
	}

	public void setDddTelefoneSecundario(String dddTelefoneSecundario) {

		this.dddTelefoneSecundario = dddTelefoneSecundario;
	}

	public String getNomeImovel() {

		return nomeImovel;
	}

	public void setNomeImovel(String nomeImovel) {

		this.nomeImovel = nomeImovel;
	}

	public List<ClienteAnexoTO> getAnexos() {

		return anexos;
	}

	public void setAnexos(List<ClienteAnexoTO> anexos) {

		this.anexos = anexos;
	}

}
