
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade ContatoImovel para transferência
 *
 */
@XmlRootElement
public class ContatoImovelTO {

	private Long chavePrimaria;

	private String nome;

	private Integer ddd;

	private String fone;

	private String ramal;

	private String email;

	private boolean principal;

	private TipoComboBoxTO tipoContato;

	private String area;

	private TipoComboBoxTO profissao;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public Integer getDDD() {

		return ddd;
	}

	public void setDDD(Integer dDD) {

		ddd = dDD;
	}

	public String getFone() {

		return fone;
	}

	public void setFone(String fone) {

		this.fone = fone;
	}

	public String getRamal() {

		return ramal;
	}

	public void setRamal(String ramal) {

		this.ramal = ramal;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public TipoComboBoxTO getTipoContato() {

		return tipoContato;
	}

	public void setTipoContato(TipoComboBoxTO tipoContato) {

		this.tipoContato = tipoContato;
	}

	public String getArea() {

		return area;
	}

	public void setArea(String area) {

		this.area = area;
	}

	public TipoComboBoxTO getProfissao() {

		return profissao;
	}

	public void setProfissao(TipoComboBoxTO profissao) {

		this.profissao = profissao;
	}

	public boolean isPrincipal() {

		return principal;
	}

	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}
}
