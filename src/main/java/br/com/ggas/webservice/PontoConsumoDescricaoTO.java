/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 22/01/2014 19:24:57
 @author mroberto
 */

package br.com.ggas.webservice;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade PontoConsumoDescricao para transferência
 *
 */
@XmlRootElement
public class PontoConsumoDescricaoTO {

	private Long chavePrimaria;

	private String descricao;

	private String situacao;

	private String enderecoCompleto;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getSituacao() {

		return situacao;
	}

	public void setSituacao(String situacao) {

		this.situacao = situacao;
	}

	public String getEnderecoCompleto() {

		return enderecoCompleto;
	}

	public void setEnderecoCompleto(String enderecoCompleto) {

		this.enderecoCompleto = enderecoCompleto;
	}

	@Override
	public boolean equals(Object o) {
		boolean equals;

		if (this == o) {
			equals = true;
		} else if (o == null || getClass() != o.getClass()) {
			equals = false;
		} else {
			PontoConsumoDescricaoTO that = (PontoConsumoDescricaoTO) o;

			equals = new EqualsBuilder().append(chavePrimaria, that.chavePrimaria)
					.append(descricao, that.descricao)
					.append(situacao, that.situacao)
					.append(enderecoCompleto, that.enderecoCompleto)
					.isEquals();
		}

		return equals;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(chavePrimaria).append(descricao)
				.append(situacao).append(enderecoCompleto)
				.toHashCode();
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setUseClassName(false);
		style.setUseIdentityHashCode(false);

		return new ToStringBuilder(this, style)
				.appendSuper(super.toString())
				.append("chavePrimaria", chavePrimaria)
				.append("descricao", descricao)
				.append("situacao", situacao)
				.append("enderecoCompleto", enderecoCompleto)
				.toString();
	}
}
