/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 22/01/2014 09:40:19
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.SerializationUtils;

import br.com.ggas.cadastro.cliente.dominio.ClienteAnexoTO;
/**
 * 
 * Classe responsável pela representação da entidade Cliente para transferência
 *
 */
@XmlRootElement
public class ClienteTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2404269769303697886L;

	// Cliente
	private Long chavePrimaria;

	private String nome;

	private String nomeAbreviado;

	private String emailPrincipal;

	private String emailSecundario;

	private String cpf;

	private String rg;

	private String dataEmissaoRG;

	private String numeroPassaporte;

	private String dataNascimento;

	private String nomeMae;

	private String nomePai;

	private String nomeFantasia;

	private String cnpj;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private String inscricaoRural;

	private TipoClienteTO tipoCliente;

	private TipoComboBoxTO orgaoExpedidor;

	private TipoComboBoxTO clienteSituacao;

	private TipoComboBoxTO unidadeFederacao;

	private TipoComboBoxTO nacionalidade;

	private TipoComboBoxTO profissao;

	private TipoComboBoxTO sexo;

	private TipoComboBoxTO clienteResponsavel;

	private FaixaRendaFamiliarTO rendaFamiliar;

	private AtividadeEconomicaTO atividadeEconomica;

	private String regimeRecolhimento;

	private GrupoEconomicoTO grupoEconomicoTO;

	private String enderecos;

	private Integer indexLista;

	private Integer indexListaAnexoAbaEndereco;

	private Integer indexListaAnexoAbaIdentificacao;

	private ClienteAnexoTO clienteAnexoTO;

	private ClienteContatoTO clienteContatoTO;

	private ClienteFoneTO clienteFoneTO;

	private ClienteEnderecoTO clienteEnderecoTO;

	// ClienteEndereco
	private Collection<ClienteEnderecoTO> enderecosTO = new HashSet<ClienteEnderecoTO>();

	// ClienteFone
	private Collection<ClienteFoneTO> fonesTO = new HashSet<ClienteFoneTO>();

	// ContatoCliente
	private Collection<ClienteContatoTO> contatosTO = new HashSet<ClienteContatoTO>();

	// ClienteAnexo
	private Collection<ClienteAnexoTO> anexosTO = new HashSet<ClienteAnexoTO>();

	// Responsabilidade
	private String tipoResponsabilidade;

	private String dataInicioRelacao;

	private String dataFimRelacao;

	private Date ultimaAlteracao;

	private String senha;

	private Date dataUltimoAcesso;

	public TipoComboBoxTO getOrgaoExpedidor() {

		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(TipoComboBoxTO orgaoExpedidor) {

		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	public String getEmailPrincipal() {

		return emailPrincipal;
	}

	public void setEmailPrincipal(String emailPrincipal) {

		this.emailPrincipal = emailPrincipal;
	}

	public String getEmailSecundario() {

		return emailSecundario;
	}

	public void setEmailSecundario(String emailSecundario) {

		this.emailSecundario = emailSecundario;
	}

	public String getCpf() {

		return cpf;
	}

	public void setCpf(String cpf) {

		this.cpf = cpf;
	}

	public String getRg() {

		return rg;
	}

	public void setRg(String rg) {

		this.rg = rg;
	}

	public String getDataEmissaoRG() {

		return dataEmissaoRG;
	}

	public void setDataEmissaoRG(String dataEmissaoRG) {

		this.dataEmissaoRG = dataEmissaoRG;
	}

	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	public String getDataNascimento() {

		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {

		this.dataNascimento = dataNascimento;
	}

	public String getNomeMae() {

		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {

		this.nomeMae = nomeMae;
	}

	public String getNomePai() {

		return nomePai;
	}

	public void setNomePai(String nomePai) {

		this.nomePai = nomePai;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {

		return cnpj;
	}

	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoRural() {

		return inscricaoRural;
	}

	public void setInscricaoRural(String inscricaoRural) {

		this.inscricaoRural = inscricaoRural;
	}

	public String getRegimeRecolhimento() {

		return regimeRecolhimento;
	}

	public void setRegimeRecolhimento(String regimeRecolhimento) {

		this.regimeRecolhimento = regimeRecolhimento;
	}

	public TipoClienteTO getTipoCliente() {

		return tipoCliente;
	}

	public void setTipoCliente(TipoClienteTO tipoCliente) {

		this.tipoCliente = tipoCliente;
	}

	public TipoComboBoxTO getClienteSituacao() {

		return clienteSituacao;
	}

	public void setClienteSituacao(TipoComboBoxTO clienteSituacao) {

		this.clienteSituacao = clienteSituacao;
	}

	public TipoComboBoxTO getUnidadeFederacao() {

		return unidadeFederacao;
	}

	public void setUnidadeFederacao(TipoComboBoxTO unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}

	public TipoComboBoxTO getNacionalidade() {

		return nacionalidade;
	}

	public void setNacionalidade(TipoComboBoxTO nacionalidade) {

		this.nacionalidade = nacionalidade;
	}

	public TipoComboBoxTO getProfissao() {

		return profissao;
	}

	public void setProfissao(TipoComboBoxTO profissao) {

		this.profissao = profissao;
	}

	public TipoComboBoxTO getSexo() {

		return sexo;
	}

	public void setSexo(TipoComboBoxTO sexo) {

		this.sexo = sexo;
	}

	public FaixaRendaFamiliarTO getRendaFamiliar() {

		return rendaFamiliar;
	}

	public void setRendaFamiliar(FaixaRendaFamiliarTO rendaFamiliar) {

		this.rendaFamiliar = rendaFamiliar;
	}

	public AtividadeEconomicaTO getAtividadeEconomica() {

		return atividadeEconomica;
	}

	public void setAtividadeEconomica(AtividadeEconomicaTO atividadeEconomica) {

		this.atividadeEconomica = atividadeEconomica;
	}

	public Collection<ClienteEnderecoTO> getEnderecosTO() {

		return enderecosTO;
	}

	public void setEnderecosTO(Collection<ClienteEnderecoTO> enderecosTO) {

		this.enderecosTO = enderecosTO;
	}

	public Collection<ClienteFoneTO> getFonesTO() {

		return fonesTO;
	}

	public void setFonesTO(Collection<ClienteFoneTO> fonesTO) {

		this.fonesTO = fonesTO;
	}

	public GrupoEconomicoTO getGrupoEconomicoTO() {

		return grupoEconomicoTO;
	}

	public void setGrupoEconomicoTO(GrupoEconomicoTO grupoEconomicoTO) {

		this.grupoEconomicoTO = grupoEconomicoTO;
	}

	public Collection<ClienteContatoTO> getContatosTO() {

		return contatosTO;
	}

	public void setContatosTO(Collection<ClienteContatoTO> contatosTO) {

		this.contatosTO = contatosTO;
	}

	public Collection<ClienteAnexoTO> getAnexosTO() {

		return anexosTO;
	}

	public void setAnexosTO(Collection<ClienteAnexoTO> anexosTo) {

		this.anexosTO = anexosTo;
	}

	public String getTipoResponsabilidade() {

		return tipoResponsabilidade;
	}

	public void setTipoResponsabilidade(String tipoResponsabilidade) {

		this.tipoResponsabilidade = tipoResponsabilidade;
	}

	public String getDataInicioRelacao() {

		return dataInicioRelacao;
	}

	public void setDataInicioRelacao(String dataInicioRelacao) {

		this.dataInicioRelacao = dataInicioRelacao;
	}

	public String getDataFimRelacao() {

		return dataFimRelacao;
	}

	public void setDataFimRelacao(String dataFimRelacao) {

		this.dataFimRelacao = dataFimRelacao;
	}

	public String getEnderecos() {

		return enderecos;
	}

	public void setEnderecos(String enderecos) {

		this.enderecos = enderecos;
	}

	public TipoComboBoxTO getClienteResponsavel() {

		return clienteResponsavel;
	}

	public void setClienteResponsavel(TipoComboBoxTO clienteResponsavel) {

		this.clienteResponsavel = clienteResponsavel;
	}

	public Integer getIndexLista() {

		return indexLista;
	}

	public void setIndexLista(Integer indexLista) {

		this.indexLista = indexLista;
	}

	public Integer getIndexListaAnexoAbaEndereco() {

		return indexListaAnexoAbaEndereco;
	}

	public void setIndexListaAnexoAbaEndereco(Integer indexListaAnexoAbaEndereco) {

		this.indexListaAnexoAbaEndereco = indexListaAnexoAbaEndereco;
	}

	public Integer getIndexListaAnexoAbaIdentificacao() {

		return indexListaAnexoAbaIdentificacao;
	}

	public void setIndexListaAnexoAbaIdentificacao(Integer indexListaAnexoAbaIdentificacao) {

		this.indexListaAnexoAbaIdentificacao = indexListaAnexoAbaIdentificacao;
	}

	public ClienteAnexoTO getClienteAnexoTO() {

		return clienteAnexoTO;
	}

	public void setClienteAnexoTO(ClienteAnexoTO clienteAnexoTO) {

		this.clienteAnexoTO = clienteAnexoTO;
	}

	public ClienteContatoTO getClienteContatoTO() {

		return clienteContatoTO;
	}

	public void setClienteContatoTO(ClienteContatoTO clienteContatoTO) {

		this.clienteContatoTO = clienteContatoTO;
	}

	public ClienteFoneTO getClienteFoneTO() {

		return clienteFoneTO;
	}

	public void setClienteFoneTO(ClienteFoneTO clienteFoneTO) {

		this.clienteFoneTO = clienteFoneTO;
	}

	public ClienteEnderecoTO getClienteEnderecoTO() {

		return clienteEnderecoTO;
	}

	public void setClienteEnderecoTO(ClienteEnderecoTO clienteEnderecoTO) {

		this.clienteEnderecoTO = clienteEnderecoTO;
	}

	public Date getUltimaAlteracao() {

		return (Date) SerializationUtils.clone(ultimaAlteracao);
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {

		this.ultimaAlteracao = ultimaAlteracao;
	}

	public String getSenha() {

		return senha;
	}

	public void setSenha(String senha) {

		this.senha = senha;
	}

	public Date getDataUltimoAcesso() {

		return (Date) SerializationUtils.clone(dataUltimoAcesso);
	}

	public void setDataUltimoAcesso(Date dataUltimoAcesso) {

		this.dataUltimoAcesso = dataUltimoAcesso;
	}

}
