/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 26/02/2014 12:02:38
 @author ifrancisco
 */

package br.com.ggas.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * ModalidadeConsumoFaturamentoTO
 */
@XmlRootElement
public class ModalidadeConsumoFaturamentoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5437977767567887068L;

	private Long chavePrimaria;

	private String modalidade;

	private String dataVigenciaQDC;

	private Collection<String> listaContratoPontoConsumoModalidadeQDC = new ArrayList<String>();

	private String prazoRevizaoQDC;

	private boolean qdsMaiorQDC;

	private String variacaoSuperiorQDC;

	private String diasAntecSolicConsumo;

	private String numMesesSolicConsumo;

	private boolean confirmacaoAutomaticaQDS;

	private boolean indicadorProgramacaoConsumo;

	private Collection<String> listaCompromissoTakeOrPayVO = new ArrayList<String>();

	private Collection<String> listaCompmissoShipOrPayVO = new ArrayList<String>();

	private boolean recuperacaoAutoTakeOrPay;

	private String percDuranteRetiradaQPNR;

	private String percFimRetiradaQPNR;

	private String dataInicioRetiradaQPNR;

	private String dataFimRetiradaQPNR;

	private String consumoReferenciaShipOrPay;

	private String margemVariacaoShipOrPay;

	private String tempoValidadeRetiradaQPNR;

	private String dataQDC;

	private String volumeQDC;

	private Integer horaLimiteProgramacaoDiaria;

	private Integer horaLimiteProgIntradiaria;

	public String getDataVigenciaQDC() {

		return dataVigenciaQDC;
	}

	public void setDataVigenciaQDC(String dataVigenciaQDC) {

		this.dataVigenciaQDC = dataVigenciaQDC;
	}

	public Collection<String> getListaContratoPontoConsumoModalidadeQDC() {

		return listaContratoPontoConsumoModalidadeQDC;
	}

	public void setListaContratoPontoConsumoModalidadeQDC(Collection<String> listaContratoPontoConsumoModalidadeQDC) {

		this.listaContratoPontoConsumoModalidadeQDC = listaContratoPontoConsumoModalidadeQDC;
	}

	public String getPrazoRevizaoQDC() {

		return prazoRevizaoQDC;
	}

	public void setPrazoRevizaoQDC(String prazoRevizaoQDC) {

		this.prazoRevizaoQDC = prazoRevizaoQDC;
	}

	public boolean getQdsMaiorQDC() {

		return qdsMaiorQDC;
	}

	public void setQdsMaiorQDC(boolean qdsMaiorQDC) {
		//Método vazio.
	}

	public String getVariacaoSuperiorQDC() {

		return variacaoSuperiorQDC;
	}

	public void setVariacaoSuperiorQDC(String variacaoSuperiorQDC) {

		this.variacaoSuperiorQDC = variacaoSuperiorQDC;
	}

	public String getDiasAntecSolicConsumo() {

		return diasAntecSolicConsumo;
	}

	public void setDiasAntecSolicConsumo(String diasAntecSolicConsumo) {

		this.diasAntecSolicConsumo = diasAntecSolicConsumo;
	}

	public String getNumMesesSolicConsumo() {

		return numMesesSolicConsumo;
	}

	public void setNumMesesSolicConsumo(String numMesesSolicConsumo) {

		this.numMesesSolicConsumo = numMesesSolicConsumo;
	}

	public boolean getConfirmacaoAutomaticaQDS() {

		return confirmacaoAutomaticaQDS;
	}

	public void setConfirmacaoAutomaticaQDS(boolean confirmacaoAutomaticaQDS) {

		this.confirmacaoAutomaticaQDS = confirmacaoAutomaticaQDS;
	}

	public boolean getIndicadorProgramacaoConsumo() {

		return indicadorProgramacaoConsumo;
	}

	public void setIndicadorProgramacaoConsumo(boolean indicadorProgramacaoConsumo) {

		this.indicadorProgramacaoConsumo = indicadorProgramacaoConsumo;
	}

	public Collection<String> getListaCompromissoTakeOrPayVO() {

		return listaCompromissoTakeOrPayVO;
	}

	public void setListaCompromissoTakeOrPayVO(Collection<String> listaCompromissoTakeOrPayVO) {

		this.listaCompromissoTakeOrPayVO = listaCompromissoTakeOrPayVO;
	}

	public boolean getRecuperacaoAutoTakeOrPay() {

		return recuperacaoAutoTakeOrPay;
	}

	public void setRecuperacaoAutoTakeOrPay(boolean recuperacaoAutoTakeOrPay) {

		this.recuperacaoAutoTakeOrPay = recuperacaoAutoTakeOrPay;
	}

	public String getPercDuranteRetiradaQPNR() {

		return percDuranteRetiradaQPNR;
	}

	public void setPercDuranteRetiradaQPNR(String percDuranteRetiradaQPNR) {

		this.percDuranteRetiradaQPNR = percDuranteRetiradaQPNR;
	}

	public String getPercFimRetiradaQPNR() {

		return percFimRetiradaQPNR;
	}

	public void setPercFimRetiradaQPNR(String percFimRetiradaQPNR) {

		this.percFimRetiradaQPNR = percFimRetiradaQPNR;
	}

	public String getDataInicioRetiradaQPNR() {

		return dataInicioRetiradaQPNR;
	}

	public void setDataInicioRetiradaQPNR(String dataInicioRetiradaQPNR) {

		this.dataInicioRetiradaQPNR = dataInicioRetiradaQPNR;
	}

	public String getDataFimRetiradaQPNR() {

		return dataFimRetiradaQPNR;
	}

	public void setDataFimRetiradaQPNR(String dataFimRetiradaQPNR) {

		this.dataFimRetiradaQPNR = dataFimRetiradaQPNR;
	}

	public String getConsumoReferenciaShipOrPay() {

		return consumoReferenciaShipOrPay;
	}

	public void setConsumoReferenciaShipOrPay(String consumoReferenciaShipOrPay) {

		this.consumoReferenciaShipOrPay = consumoReferenciaShipOrPay;
	}

	public String getMargemVariacaoShipOrPay() {

		return margemVariacaoShipOrPay;
	}

	public void setMargemVariacaoShipOrPay(String margemVariacaoShipOrPay) {

		this.margemVariacaoShipOrPay = margemVariacaoShipOrPay;
	}

	public String getTempoValidadeRetiradaQPNR() {

		return tempoValidadeRetiradaQPNR;
	}

	public void setTempoValidadeRetiradaQPNR(String tempoValidadeRetiradaQPNR) {

		this.tempoValidadeRetiradaQPNR = tempoValidadeRetiradaQPNR;
	}

	public String getModalidade() {

		return modalidade;
	}

	public void setModalidade(String modalidade) {

		this.modalidade = modalidade;
	}

	public Collection<String> getListaCompmissoShipOrPayVO() {

		return listaCompmissoShipOrPayVO;
	}

	public void setListaCompmissoShipOrPayVO(Collection<String> listaCompmissoShipOrPayVO) {

		this.listaCompmissoShipOrPayVO = listaCompmissoShipOrPayVO;
	}

	public String getDataQDC() {

		return dataQDC;
	}

	public void setDataQDC(String dataQDC) {

		this.dataQDC = dataQDC;
	}

	public String getVolumeQDC() {

		return volumeQDC;
	}

	public void setVolumeQDC(String volumeQDC) {

		this.volumeQDC = volumeQDC;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public Integer getHoraLimiteProgramacaoDiaria() {

		return horaLimiteProgramacaoDiaria;
	}

	public void setHoraLimiteProgramacaoDiaria(Integer horaLimiteProgramacaoDiaria) {

		this.horaLimiteProgramacaoDiaria = horaLimiteProgramacaoDiaria;
	}

	public Integer getHoraLimiteProgIntradiaria() {

		return horaLimiteProgIntradiaria;
	}

	public void setHoraLimiteProgIntradiaria(Integer horaLimiteProgIntradiaria) {

		this.horaLimiteProgIntradiaria = horaLimiteProgIntradiaria;
	}

}
