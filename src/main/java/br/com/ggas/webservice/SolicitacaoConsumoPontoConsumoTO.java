/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 16/01/2015 10:51:59
 @author aantonio
 */

package br.com.ggas.webservice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.SerializationUtils;

import br.com.ggas.util.Mapeavel;
/**
 * 
 * Classe responsável pela representação da entidade SolicitacaoConsumoPontoConsumo para transferência.
 *
 */
@XmlRootElement
public class SolicitacaoConsumoPontoConsumoTO implements Serializable, Comparable<SolicitacaoConsumoPontoConsumoTO>, Mapeavel<String> {

	private static final long serialVersionUID = -3017101326693090845L;

	private Date dateSolicitacao;

	private String dataSolicitacao;

	private BigDecimal valorQDC;

	private BigDecimal valorQDCMaxima;

	private BigDecimal valorQDS;

	private BigDecimal valorQPNR;

	private BigDecimal valorQDP;

	private BigDecimal valorQR;

	private Boolean indicadorAceite;

	private Date dataOperacao;

	private String descricaoMotivoNaoAceite;

	private boolean indicadorSolicitante;

	private String comentario;

	private String penalidade;

	private String nomeUsuario;

	private String dataFormatada;

	private boolean indicadorEdit;

	private Long numeroProtocolo;

	private String indicadorTipoProgramacao;

	private BigDecimal valorPenalidadeMaior;

	private BigDecimal valorPenalidadeMenor;

	private Date ultimaAlteracao;

	private String habilitado;

	private BigDecimal percentualVarSuperiorQDC;

	private BigDecimal valorPercentualCobRetMaior;

	private BigDecimal valorPercentualRetMaior;

	private BigDecimal valorPercentualCobRetMenor;

	private BigDecimal valorPercentualRetMenor;

	private Long idContrato;

	private Long idContratoPontoConsumoModalidade;

	private int sequenciaApuracaoVolume;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #getContratoPontoConsumoModalidade()
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setContratoPontoConsumoModalidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade)
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDescricaoMotivoNaoAceite()
	 */
	public String getDescricaoMotivoNaoAceite() {

		return descricaoMotivoNaoAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setDescricaoMotivoNaoAceite
	 * (java.lang.String)
	 */
	public void setDescricaoMotivoNaoAceite(String descricaoMotivoNaoAceite) {

		this.descricaoMotivoNaoAceite = descricaoMotivoNaoAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDataSolicitacao()
	 */
	public String getDataSolicitacao() {

		return dataSolicitacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setDataSolicitacao(java.util.Date)
	 */
	public void setDataSolicitacao(String dataSolicitacao) {

		this.dataSolicitacao = dataSolicitacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDC()
	 */
	public BigDecimal getValorQDC() {

		return valorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDC(java.math.BigDecimal)
	 */
	public void setValorQDC(BigDecimal valorQDC) {

		this.valorQDC = valorQDC;
	}

	/**
	 * @return the valorQDCMaxima
	 */
	public BigDecimal getValorQDCMaxima() {

		return valorQDCMaxima;
	}

	/**
	 * @param valorQDCMaxima the valorQDCMaxima to set
	 */
	public void setValorQDCMaxima(BigDecimal valorQDCMaxima) {

		this.valorQDCMaxima = valorQDCMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDS()
	 */
	public BigDecimal getValorQDS() {

		return valorQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDS(java.math.BigDecimal)
	 */
	public void setValorQDS(BigDecimal valorQDS) {

		this.valorQDS = valorQDS;
	}

	/**
	 * Verifica se o valor é nulo se não for adiciona ao
	 * ValorQDS. 
	 * @param valorQDS {@link BigDecimal} 
	 */
	public void adicionarValorQDS(BigDecimal valorQDS) {

		if (this.valorQDS == null) {
			this.valorQDS = BigDecimal.ZERO;
		}
		this.valorQDS = this.valorQDS.add(valorQDS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQPNR()
	 */
	public BigDecimal getValorQPNR() {

		return valorQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQPNR(java.math.BigDecimal)
	 */
	public void setValorQPNR(BigDecimal valorQPNR) {

		this.valorQPNR = valorQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDP()
	 */
	public BigDecimal getValorQDP() {

		return valorQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDP(java.math.BigDecimal)
	 */
	public void setValorQDP(BigDecimal valorQDP) {

		this.valorQDP = valorQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getIndicadorAceite()
	 */
	public Boolean getIndicadorAceite() {

		return indicadorAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setIndicadorAceite(java.lang.Boolean)
	 */
	public void setIndicadorAceite(Boolean indicadorAceite) {

		this.indicadorAceite = indicadorAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDataOperacao()
	 */
	public Date getDataOperacao() {

		return (Date) SerializationUtils.clone(dataOperacao);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setDataOperacao(java.util.Date)
	 */
	public void setDataOperacao(Date dataOperacao) {

		this.dataOperacao = dataOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo#getUsuario()
	 * /*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #isIndicadorSolicitante()
	 */
	public boolean isIndicadorSolicitante() {

		return indicadorSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setIndicadorSolicitante(boolean)
	 */
	public void setIndicadorSolicitante(boolean indicadorSolicitante) {

		this.indicadorSolicitante = indicadorSolicitante;
	}

	public BigDecimal getValorQR() {

		return valorQR;
	}

	public void setValorQR(BigDecimal valorQR) {

		this.valorQR = valorQR;
	}

	/**
	 * Verífica se o valorQR é nulo se não for
	 * recebe esse valor.
	 * 
	 * @param valorQR {@link BigDecimal} 
	 */
	public void adicionarValorQR(BigDecimal valorQR) {

		if (this.valorQR == null) {
			this.valorQR = BigDecimal.ZERO;
		}
		this.valorQR = this.valorQR.add(valorQR);
	}

	public String getComentario() {

		return comentario;
	}

	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	public String getPenalidade() {

		return penalidade;
	}

	public void setPenalidade(String penalidade) {

		this.penalidade = penalidade;
	}

	public String getNomeUsuario() {

		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {

		this.nomeUsuario = nomeUsuario;
	}

	public String getDataFormatada() {

		return dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {

		this.dataFormatada = dataFormatada;
	}

	public boolean getIndicadorEdit() {

		return indicadorEdit;
	}

	public void setIndicadorEdit(boolean indicadorEdit) {

		this.indicadorEdit = indicadorEdit;
	}

	public Long getNumeroProtocolo() {

		return numeroProtocolo;
	}

	public void setNumeroProtocolo(Long numeroProtocolo) {

		this.numeroProtocolo = numeroProtocolo;
	}

	public String getIndicadorTipoProgramacao() {

		return indicadorTipoProgramacao;
	}

	public void setIndicadorTipoProgramacao(String indicadorTipoProgramacao) {

		this.indicadorTipoProgramacao = indicadorTipoProgramacao;
	}

	public BigDecimal getValorPenalidadeMaior() {

		return valorPenalidadeMaior;
	}

	public void setValorPenalidadeMaior(BigDecimal valorPenalidadeMaior) {

		this.valorPenalidadeMaior = valorPenalidadeMaior;
	}

	public BigDecimal getValorPenalidadeMenor() {

		return valorPenalidadeMenor;
	}

	public void setValorPenalidadeMenor(BigDecimal valorPenalidadeMenor) {

		this.valorPenalidadeMenor = valorPenalidadeMenor;
	}

	public Date getUltimaAlteracao() {

		return (Date) SerializationUtils.clone(ultimaAlteracao);
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {

		this.ultimaAlteracao = ultimaAlteracao;
	}

	public String getHabilitado() {

		return habilitado;
	}

	public void setHabilitado(String habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * @return the percentualVarSuperiorQDC
	 */
	public BigDecimal getPercentualVarSuperiorQDC() {

		return percentualVarSuperiorQDC;
	}

	/**
	 * @param percentualVarSuperiorQDC the percentualVarSuperiorQDC to set
	 */
	public void setPercentualVarSuperiorQDC(BigDecimal percentualVarSuperiorQDC) {

		this.percentualVarSuperiorQDC = percentualVarSuperiorQDC;
	}

	/**
	 * @return the valorPercentualCobRetMaior
	 */
	public BigDecimal getValorPercentualCobRetMaior() {

		return valorPercentualCobRetMaior;
	}

	/**
	 * @param valorPercentualCobRetMaior the valorPercentualCobRetMaior to set
	 */
	public void setValorPercentualCobRetMaior(BigDecimal valorPercentualCobRetMaior) {

		this.valorPercentualCobRetMaior = valorPercentualCobRetMaior;
	}

	/**
	 * @return the valorPercentualRetMaior
	 */
	public BigDecimal getValorPercentualRetMaior() {

		return valorPercentualRetMaior;
	}

	/**
	 * @param valorPercentualRetMaior the valorPercentualRetMaior to set
	 */
	public void setValorPercentualRetMaior(BigDecimal valorPercentualRetMaior) {

		this.valorPercentualRetMaior = valorPercentualRetMaior;
	}

	/**
	 * @return the valorPercentualCobRetMenor
	 */
	public BigDecimal getValorPercentualCobRetMenor() {

		return valorPercentualCobRetMenor;
	}

	/**
	 * @param valorPercentualCobRetMenor the valorPercentualCobRetMenor to set
	 */
	public void setValorPercentualCobRetMenor(BigDecimal valorPercentualCobRetMenor) {

		this.valorPercentualCobRetMenor = valorPercentualCobRetMenor;
	}

	/**
	 * @return the valorPercentualRetMenor
	 */
	public BigDecimal getValorPercentualRetMenor() {

		return valorPercentualRetMenor;
	}

	/**
	 * @param valorPercentualRetMenor the valorPercentualRetMenor to set
	 */
	public void setValorPercentualRetMenor(BigDecimal valorPercentualRetMenor) {

		this.valorPercentualRetMenor = valorPercentualRetMenor;
	}

	/**
	 * @return the dateSolicitacao
	 */
	public Date getDateSolicitacao() {

		return (Date) SerializationUtils.clone(dateSolicitacao);
	}

	/**
	 * @param dateSolicitacao the dateSolicitacao to set
	 */
	public void setDateSolicitacao(Date dateSolicitacao) {

		this.dateSolicitacao = dateSolicitacao;
	}

	/**
	 * @return the idContrato
	 */
	public Long getIdContrato() {

		return idContrato;
	}

	/**
	 * @param idContrato the idContrato to set
	 */
	public void setIdContrato(Long idContrato) {

		this.idContrato = idContrato;
	}

	/**
	 * @return the idContratoPontoConsumoModalidade
	 */
	public Long getIdContratoPontoConsumoModalidade() {

		return idContratoPontoConsumoModalidade;
	}

	/**
	 * @param idContratoPontoConsumoModalidade the idContratoPontoConsumoModalidade to set
	 */
	public void setIdContratoPontoConsumoModalidade(Long idContratoPontoConsumoModalidade) {

		this.idContratoPontoConsumoModalidade = idContratoPontoConsumoModalidade;
	}

	/**
	 * @return the sequenciaApuracaoVolume
	 */
	public int getSequenciaApuracaoVolume() {

		return sequenciaApuracaoVolume;
	}

	/**
	 * @param sequenciaApuracaoVolume the sequenciaApuracaoVolume to set
	 */
	public void setSequenciaApuracaoVolume(int sequenciaApuracaoVolume) {

		this.sequenciaApuracaoVolume = sequenciaApuracaoVolume;
	}

	@Override
	public int compareTo(SolicitacaoConsumoPontoConsumoTO o) {

		if (this.sequenciaApuracaoVolume < o.getSequenciaApuracaoVolume()) {
			return -1;
		} else if (this.sequenciaApuracaoVolume > o.getSequenciaApuracaoVolume()) {
			return 1;
		}
		if (this.dateSolicitacao.before(o.getDateSolicitacao())) {
			return -1;
		} else if (this.dateSolicitacao.after(o.getDateSolicitacao())) {
			return 1;
		}
		return 0;
	}


	@Override
	public String getChaveMapeadora() {

		return this.getDataSolicitacao();
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		if (dataSolicitacao == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + dataSolicitacao.hashCode();
		}

		if (dateSolicitacao == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + dateSolicitacao.hashCode();
		}

		if (idContrato == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + idContrato.hashCode();
		}

		if (idContratoPontoConsumoModalidade == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + idContratoPontoConsumoModalidade.hashCode();
		}
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitacaoConsumoPontoConsumoTO other = (SolicitacaoConsumoPontoConsumoTO) obj;
		if (dataSolicitacao == null) {
			if (other.dataSolicitacao != null) {
				return false;
			}
		} else if (!dataSolicitacao.equals(other.dataSolicitacao)) {
			return false;
		}
		if (dateSolicitacao == null) {
			if (other.dateSolicitacao != null) {
				return false;
			}
		} else if (!dateSolicitacao.equals(other.dateSolicitacao)) {
			return false;
		}
		if (idContrato == null) {
			if (other.idContrato != null) {
				return false;
			}
		} else if (!idContrato.equals(other.idContrato)) {
			return false;
		}
		if (idContratoPontoConsumoModalidade == null) {
			if (other.idContratoPontoConsumoModalidade != null) {
				return false;
			}
		} else if (!idContratoPontoConsumoModalidade.equals(other.idContratoPontoConsumoModalidade)) {
			return false;
		}
		return true;
	}
	
	
	

}
