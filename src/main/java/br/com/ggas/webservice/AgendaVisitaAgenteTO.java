
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade AgendaVisitaAgente para transferência
 *
 */
@XmlRootElement
public class AgendaVisitaAgenteTO {

	private Long chavaPramaria;

	private String dataVisita;

	private String horaVisita;

	private String observacoes;
	
	
	public String getObservacoes() {
	
		return observacoes;
	}


	public void setObservacoes(String observacoes) {
	
		this.observacoes = observacoes;
	}


	public String getDataVisita() {

		return dataVisita;
	}

	public void setDataVisita(String dataVisita) {

		this.dataVisita = dataVisita;
	}

	public String getHoraVisita() {

		return horaVisita;
	}

	public void setHoraVisita(String horaVisita) {

		this.horaVisita = horaVisita;
	}

	/**
	 * @return the chavaPramaria
	 */
	public Long getChavaPramaria() {

		return chavaPramaria;
	}

	/**
	 * @param chavaPramaria
	 *            the chavaPramaria to set
	 */
	public void setChavaPramaria(Long chavaPramaria) {

		this.chavaPramaria = chavaPramaria;
	}

}
