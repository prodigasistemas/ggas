
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author crsilva
 *
 */
/**
 * @author crsilva
 */
@XmlRootElement
public class PropostaTO {

	private long chavePrimaria;

	private boolean habilitado;

	private String numeroProposta;

	private String versaoProposta;

	private String dataEmissao;

	private String dataVigencia;

	private String dataEntrega;

	private String percentualTIR;

	private String valorMaterial;

	private String valorMaoDeObra;

	private String valorInvestimentoTexto;

	private boolean indicadorParticipacao;

	private String percentualCliente;

	private String valorCliente;

	private String quantidadeParcela;

	private String valorParcela;

	private String percentualJuros;

	private String consumoMedioAnual;

	private String consumoMedioMensal;

	private String consumoUnidadeConsumidora;

	private String anoMesReferenciaPreco;

	private String valorPrecoPago;

	private String valorGastoMensal;

	private TipoComboBoxTO situacaoProposta;

	private ImovelTO imovelTO;

	private TipoComboBoxTO unidadeConsumoAnual;

	private TipoComboBoxTO unidadeConsumoMensal;

	private String nomePlanilha;

	private String tipoPlanilha;

	private String economiaMensalGN;

	private String economiaAnualGN;

	private String percentualEconomia;

	private String comentario;

	private String dataApresentacaoCondominio;

	private String dataEleicaoSindico;

	private TipoComboBoxTO periodicidadeJuros;

	private boolean indicadorMedicao;

	private String quantidadeApartamentos;

	private String consumoMedioMensalGN;

	private String consumoMedioAnualGN;

	private String volumeDiarioEstimadoGN;

	private TipoComboBoxTO segmento;

	private String valorMensalGastoGN;

	private String valorParticipacaoCDL;

	private TipoComboBoxTO fiscal;

	private TipoComboBoxTO vendedor;

	private TipoComboBoxTO modalidadeUso;

	private TipoComboBoxTO tarifa;

	private String numeroCompletoProposta;

	private String anoProposta;

	private String valorMedioGN;

	private long chavePrimariaLM;

	public long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(long chavaPrimaria) {

		this.chavePrimaria = chavaPrimaria;
	}

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	public String getNumeroProposta() {

		return numeroProposta;
	}

	public void setNumeroProposta(String numeroProposta) {

		this.numeroProposta = numeroProposta;
	}

	public String getVersaoProposta() {

		return versaoProposta;
	}

	public void setVersaoProposta(String versaoProposta) {

		this.versaoProposta = versaoProposta;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getDataVigencia() {

		return dataVigencia;
	}

	public void setDataVigencia(String dataVigencia) {

		this.dataVigencia = dataVigencia;
	}

	public String getDataEntrega() {

		return dataEntrega;
	}

	public void setDataEntrega(String dataEntrega) {

		this.dataEntrega = dataEntrega;
	}

	public String getPercentualTIR() {

		return percentualTIR;
	}

	public void setPercentualTIR(String percentualTIR) {

		this.percentualTIR = percentualTIR;
	}

	public String getValorMaterial() {

		return valorMaterial;
	}

	public void setValorMaterial(String valorMaterial) {

		this.valorMaterial = valorMaterial;
	}

	public String getValorMaoDeObra() {

		return valorMaoDeObra;
	}

	public void setValorMaoDeObra(String valorMaoDeObra) {

		this.valorMaoDeObra = valorMaoDeObra;
	}

	public String getValorInvestimento() {

		return valorInvestimentoTexto;
	}

	public void setValorInvestimento(String valorInvestimento) {

		this.valorInvestimentoTexto = valorInvestimento;
	}

	public boolean isIndicadorParticipacao() {

		return indicadorParticipacao;
	}

	public void setIndicadorParticipacao(boolean indicadorParticipacao) {

		this.indicadorParticipacao = indicadorParticipacao;
	}

	public String getPercentualCliente() {

		return percentualCliente;
	}

	public TipoComboBoxTO getFiscal() {

		return fiscal;
	}

	public void setFiscal(TipoComboBoxTO fiscal) {

		this.fiscal = fiscal;
	}

	public TipoComboBoxTO getVendedor() {

		return vendedor;
	}

	public void setVendedor(TipoComboBoxTO vendedor) {

		this.vendedor = vendedor;
	}

	public void setPercentualCliente(String percentualCliente) {

		this.percentualCliente = percentualCliente;
	}

	public String getValorCliente() {

		return valorCliente;
	}

	public void setValorCliente(String valorCliente) {

		this.valorCliente = valorCliente;
	}

	public String getQuantidadeParcela() {

		return quantidadeParcela;
	}

	public void setQuantidadeParcela(String quantidadeParcela) {

		this.quantidadeParcela = quantidadeParcela;
	}

	public String getValorParcela() {

		return valorParcela;
	}

	public void setValorParcela(String valorParcela) {

		this.valorParcela = valorParcela;
	}

	public String getPercentualJuros() {

		return percentualJuros;
	}

	public void setPercentualJuros(String percentualJuros) {

		this.percentualJuros = percentualJuros;
	}

	public String getConsumoMedioAnual() {

		return consumoMedioAnual;
	}

	public void setConsumoMedioAnual(String consumoMedioAnual) {

		this.consumoMedioAnual = consumoMedioAnual;
	}

	public String getConsumoMedioMensal() {

		return consumoMedioMensal;
	}

	public void setConsumoMedioMensal(String consumoMedioMensal) {

		this.consumoMedioMensal = consumoMedioMensal;
	}

	public String getConsumoUnidadeConsumidora() {

		return consumoUnidadeConsumidora;
	}

	public void setConsumoUnidadeConsumidora(String consumoUnidadeConsumidora) {

		this.consumoUnidadeConsumidora = consumoUnidadeConsumidora;
	}

	public String getAnoMesReferenciaPreco() {

		return anoMesReferenciaPreco;
	}

	public void setAnoMesReferenciaPreco(String anoMesReferenciaPreco) {

		this.anoMesReferenciaPreco = anoMesReferenciaPreco;
	}

	public String getValorPrecoPago() {

		return valorPrecoPago;
	}

	public void setValorPrecoPago(String valorPrecoPago) {

		this.valorPrecoPago = valorPrecoPago;
	}

	public String getValorGastoMensal() {

		return valorGastoMensal;
	}

	public void setValorGastoMensal(String valorGastoMensal) {

		this.valorGastoMensal = valorGastoMensal;
	}

	public TipoComboBoxTO getSituacaoProposta() {

		return situacaoProposta;
	}

	public void setSituacaoProposta(TipoComboBoxTO situacaoProposta) {

		this.situacaoProposta = situacaoProposta;
	}

	public ImovelTO getImovelTO() {

		return imovelTO;
	}

	public void setImovelTO(ImovelTO imovelTO) {

		this.imovelTO = imovelTO;
	}

	public TipoComboBoxTO getUnidadeConsumoAnual() {

		return unidadeConsumoAnual;
	}

	public void setUnidadeConsumoAnual(TipoComboBoxTO unidadeConsumoAnual) {

		this.unidadeConsumoAnual = unidadeConsumoAnual;
	}

	public TipoComboBoxTO getUnidadeConsumoMensal() {

		return unidadeConsumoMensal;
	}

	public void setUnidadeConsumoMensal(TipoComboBoxTO unidadeConsumoMensal) {

		this.unidadeConsumoMensal = unidadeConsumoMensal;
	}

	public String getNomePlanilha() {

		return nomePlanilha;
	}

	public void setNomePlanilha(String nomePlanilha) {

		this.nomePlanilha = nomePlanilha;
	}

	public String getTipoPlanilha() {

		return tipoPlanilha;
	}

	public void setTipoPlanilha(String tipoPlanilha) {

		this.tipoPlanilha = tipoPlanilha;
	}

	public String getEconomiaMensalGN() {

		return economiaMensalGN;
	}

	public void setEconomiaMensalGN(String economiaMensalGN) {

		this.economiaMensalGN = economiaMensalGN;
	}

	public String getEconomiaAnualGN() {

		return economiaAnualGN;
	}

	public void setEconomiaAnualGN(String economiaAnualGN) {

		this.economiaAnualGN = economiaAnualGN;
	}

	public String getPercentualEconomia() {

		return percentualEconomia;
	}

	public void setPercentualEconomia(String percentualEconomia) {

		this.percentualEconomia = percentualEconomia;
	}

	public String getComentario() {

		return comentario;
	}

	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	public String getDataApresentacaoCondominio() {

		return dataApresentacaoCondominio;
	}

	public void setDataApresentacaoCondominio(String dataApresentacaoCondominio) {

		this.dataApresentacaoCondominio = dataApresentacaoCondominio;
	}

	public String getDataEleicaoSindico() {

		return dataEleicaoSindico;
	}

	public void setDataEleicaoSindico(String dataEleicaoSindico) {

		this.dataEleicaoSindico = dataEleicaoSindico;
	}

	public TipoComboBoxTO getPeriodicidadeJuros() {

		return periodicidadeJuros;
	}

	public void setPeriodicidadeJuros(TipoComboBoxTO periodicidadeJuros) {

		this.periodicidadeJuros = periodicidadeJuros;
	}

	public boolean isIndicadorMedicao() {

		return indicadorMedicao;
	}

	public void setIndicadorMedicao(boolean indicadorMedicao) {

		this.indicadorMedicao = indicadorMedicao;
	}

	public String getQuantidadeApartamentos() {

		return quantidadeApartamentos;
	}

	public void setQuantidadeApartamentos(String quantidadeApartamentos) {

		this.quantidadeApartamentos = quantidadeApartamentos;
	}

	public String getConsumoMedioMensalGN() {

		return consumoMedioMensalGN;
	}

	public void setConsumoMedioMensalGN(String consumoMedioMensalGN) {

		this.consumoMedioMensalGN = consumoMedioMensalGN;
	}

	public String getConsumoMedioAnualGN() {

		return consumoMedioAnualGN;
	}

	public void setConsumoMedioAnualGN(String consumoMedioAnualGN) {

		this.consumoMedioAnualGN = consumoMedioAnualGN;
	}

	public String getVolumeDiarioEstimadoGN() {

		return volumeDiarioEstimadoGN;
	}

	public void setVolumeDiarioEstimadoGN(String volumeDiarioEstimadoGN) {

		this.volumeDiarioEstimadoGN = volumeDiarioEstimadoGN;
	}

	public TipoComboBoxTO getSegmento() {

		return segmento;
	}

	public void setSegmento(TipoComboBoxTO segmento) {

		this.segmento = segmento;
	}

	public String getValorMensalGastoGN() {

		return valorMensalGastoGN;
	}

	public void setValorMensalGastoGN(String valorMensalGastoGN) {

		this.valorMensalGastoGN = valorMensalGastoGN;
	}

	public String getValorParticipacaoCDL() {

		return valorParticipacaoCDL;
	}

	public void setValorParticipacaoCDL(String valorParticipacaoCDL) {

		this.valorParticipacaoCDL = valorParticipacaoCDL;
	}

	public TipoComboBoxTO getModalidadeUso() {

		return modalidadeUso;
	}

	public void setModalidadeUso(TipoComboBoxTO modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	public TipoComboBoxTO getTarifa() {

		return tarifa;
	}

	public void setTarifa(TipoComboBoxTO tarifa) {

		this.tarifa = tarifa;
	}

	public String getNumeroCompletoProposta() {

		return numeroCompletoProposta;
	}

	public void setNumeroCompletoProposta(String numeroCompletoProposta) {

		this.numeroCompletoProposta = numeroCompletoProposta;
	}

	public String getAnoProposta() {

		return anoProposta;
	}

	public void setAnoProposta(String anoProposta) {

		this.anoProposta = anoProposta;
	}

	public String getValorMedioGN() {

		return valorMedioGN;
	}

	public void setValorMedioGN(String valorMedioGN) {

		this.valorMedioGN = valorMedioGN;
	}

	public long getChavePrimariaLM() {

		return chavePrimariaLM;
	}

	public void setChavePrimariaLM(long chavePrimariaLM) {

		this.chavePrimariaLM = chavePrimariaLM;
	}

}
