
package br.com.ggas.webservice;

import static java.util.Arrays.asList;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.swing.ImageIcon;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.google.common.collect.Iterables;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAlteracaoVencimento;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoAnexo;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteContato;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteEndereco;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteTelefone;
import br.com.ggas.cadastro.cliente.dominio.ChamadoClienteAnexo;
import br.com.ggas.cadastro.cliente.dominio.ClienteAnexoTO;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.impl.GrupoEconomicoImpl;
import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.PavimentoCalcada;
import br.com.ggas.cadastro.imovel.PavimentoRua;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoAgrupadoVO;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoDetalhadoVO;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoVencimento;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.AgendaVisitaAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoItemFaturamentoImpl;
import br.com.ggas.contrato.contrato.impl.EnderecoTemporarioFaturamento;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.contrato.programacao.impl.SolicitacaoConsumoPontoConsumoImpl;
import br.com.ggas.contrato.programacao.impl.TempSolicitacaoConsumo;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.SituacaoProposta;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.UsuarioImpl;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadeVO;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;
import br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia;
import br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.nfe.ControladorNfe;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.relatorio.RelatorioSolicitacaoConsumoVO;
import br.com.ggas.util.BigDecimalUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.util.tarefaassincrona.TarefaAssincronaUtil;
import br.com.ggas.web.AjaxService;
import br.com.ggas.webservice.negocio.CepUtil;
import br.com.ggas.webservice.negocio.PontoConsumoDescricaoUtil;
import br.com.ggas.webservice.negocio.ServicoUtil;

/**
 * The Class Servico.
 */
@WebService(name = "ws")
@Service("ws")
public class Servico extends SpringBeanAutowiringSupport {

 private static final String CONTATOS = "contatos";

private static final String FONES = "fones";

private static final String ENDERECOS = "enderecos";

/**
  * The Constant LOG.
  */
 private static final Logger LOG = Logger.getLogger(Servico.class);
	/**
	 * The Constant EXIBIR_FILTROS.
	 */
	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
 /**
  * The Constant APROVADA.
  */
 private static final String APROVADA = "APROVADA";

 /**
  * The Constant REJEITADA.
  */
 private static final String REJEITADA = "REJEITADA";
 private static final int NUM_CARACTERES_CPF = 11;
 private static final int NUMERO_MAX_CARACTERES_ANEXO = 50;

	/**
  * The fachada atributo.
  */
 protected Fachada fachadaAtributo = Fachada.getInstancia();

 /**
  * The Constant APARELHOS_TO.
  */
 private static final String APARELHOS_TO = "aparelhosTO";

 /**
  * The Constant FORNECEDORES.
  */
 private static final String FORNECEDORES = "fornecedores";

 /**
  * The Constant FONTES_ENERGETICAS_TO.
  */
 private static final String FONTES_ENERGETICAS_TO = "fontesEnergeticasTO";

 /**
  * The Constant LISTA_ESTADO_REDE_TO.
  */
 private static final String LISTA_ESTADO_REDE_TO = "listaEstadoRedeTO";

 /**
  * The Constant LISTA_REDE_MATERIAL_TO.
  */
 private static final String LISTA_REDE_MATERIAL_TO = "listaRedeMaterialTO";

 /**
  * The Constant SEGMENTOS_TO.
  */
 private static final String SEGMENTOS_TO = "segmentosTO";

 /**
  * The Constant LISTA_MODALIDADE_USO_TO.
  */
 private static final String LISTA_MODALIDADE_USO_TO = "listaModalidadeUsoTO";

 /**
  * The Constant LISTA_TIPO_CILINDRO_TO.
  */
 private static final String LISTA_TIPO_CILINDRO_TO = "listaTipoCilindroTO";

 /**
  * The Constant LISTA_PERIODICIDADES_TO.
  */
 private static final String LISTA_PERIODICIDADES_TO = "listaPeriodicidadesTO";

 /**
  * The Constant CHAVE_PRIMARIA_LM.
  */
 private static final String CHAVE_PRIMARIA_LM = "chavePrimariaLM";

 /**
  * The Constant DESCRICAO_IMOVEL.
  */
 private static final String DESCRICAO_IMOVEL = "nome";

 /**
  * The Constant COMPLEMENTO_IMOVEL.
  */
 private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

 /**
  * The Constant NUMERO_IMOVEL.
  */
 private static final String NUMERO_IMOVEL = "numeroImovel";

 /**
  * The Constant CHAVE_PRIMARIA.
  */
 private static final String CHAVE_PRIMARIA = "chavePrimaria";

 /**
  * The Constant INDICADOR_CONDOMINIO_AMBOS.
  */
 private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";

 /**
  * The Constant HABILITADO.
  */
 private static final String HABILITADO = "habilitado";

 /**
  * The controlador contrato.
  */
 @Autowired
 @Qualifier("controladorContrato")
 private ControladorContrato controladorContrato;

 /**
  * The controlador nfe.
  */
 @Autowired
 @Qualifier("controladorNfe")
 private ControladorNfe controladorNfe;

 /**
  * The controlador cliente.
  */
 @Autowired
 @Qualifier("controladorCliente")
 private ControladorCliente controladorCliente;

 /**
  * The controlador cobranca.
  */
 @Autowired
 @Qualifier("controladorCobranca")
 private ControladorCobranca controladorCobranca;

 /**
  * The controlador endereco.
  */
 @Autowired
 @Qualifier("controladorEndereco")
 private ControladorEndereco controladorEndereco;

 /**
  * The controlador chamado.
  */
 @Autowired
 private ControladorChamado controladorChamado;

 /**
  * The controlador segmento.
  */
 @Autowired
 @Qualifier("controladorSegmento")
 private ControladorSegmento controladorSegmento;

 /**
  * The controlador tabela auxiliar.
  */
 @Autowired
 @Qualifier("controladorTabelaAuxiliar")
 private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

 /**
  * The controlador unidade organizacional.
  */
 @Autowired
 @Qualifier("controladorUnidadeOrganizacional")
 private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

 /**
  * The controlador chamado assunto.
  */
 @Autowired
 private ControladorChamadoAssunto controladorChamadoAssunto;

 /**
  * The controlador usuario.
  */
 @Autowired
 @Qualifier("controladorUsuario")
 private ControladorUsuario controladorUsuario;

 /**
  * The controlador ponto consumo.
  */
 @Autowired
 @Qualifier("controladorPontoConsumo")
 private ControladorPontoConsumo controladorPontoConsumo;

 /**
  * The controlador constante sistema.
  */
 @Autowired
 @Qualifier("controladorConstanteSistema")
 private ControladorConstanteSistema controladorConstanteSistema;

 /**
  * The controlador entidade conteudo.
  */
 @Autowired
 @Qualifier("controladorEntidadeConteudo")
 private ControladorEntidadeConteudo controladorEntidadeConteudo;

 /**
  * The controlador fatura.
  */
 @Autowired
 @Qualifier("controladorFatura")
 private ControladorFatura controladorFatura;

 /**
  * The controlador arrecadacao.
  */
 @Autowired
 @Qualifier("controladorArrecadacao")
 private ControladorArrecadacao controladorArrecadacao;

 /**
  * The controlador parametro sistema.
  */
 @Autowired
 @Qualifier("controladorParametroSistema")
 private ControladorParametroSistema controladorParametroSistema;

 /**
  * The controlador auditoria.
  */
 @Autowired
 @Qualifier("controladorAuditoria")
 private ControladorAuditoria controladorAuditoria;

 /**
  * The controlador historico consumo.
  */
 @Autowired
 @Qualifier("controladorHistoricoConsumo")
 private ControladorHistoricoConsumo controladorHistoricoConsumo;

 /**
  * The controlador devolucao.
  */
 @Autowired
 @Qualifier("controladorDevolucao")
 private ControladorDevolucao controladorDevolucao;

 /**
  * The controlador empresa.
  */
 @Autowired
 @Qualifier("controladorEmpresa")
 private ControladorEmpresa controladorEmpresa;

 /**
  * The controlador municipio.
  */
 @Autowired
 @Qualifier("controladorMunicipio")
 private ControladorMunicipio controladorMunicipio;

 /**
  * The controlador acesso.
  */
 @Autowired
 @Qualifier("controladorAcesso")
 private ControladorAcesso controladorAcesso;

 /**
  * The controlador processo.
  */
 @Autowired
 @Qualifier("controladorProcesso")
 private ControladorProcesso controladorProcesso;

 /**
  * The controlador leiturista.
  */
 @Autowired
 @Qualifier("controladorLeiturista")
 private ControladorLeiturista controladorLeiturista;

 /**
  * The controlador dados medicao.
  */
 @Autowired
 @Qualifier("controladorDadosMedicao")
 private ControladorDadosMedicao controladorDadosMedicao;

 /**
  * The controlador leitura movimento.
  */
 @Autowired
 @Qualifier("controladorLeituraMovimento")
 private ControladorLeituraMovimento controladorLeituraMovimento;

 /**
  * The controlador programacao.
  */
 @Autowired
 @Qualifier("controladorProgramacao")
 private ControladorProgramacao controladorProgramacao;

 /**
  * The controlador apuracao penalidade.
  */
 @Autowired
 @Qualifier("controladorApuracaoPenalidade")
 private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

 /**
  * The controlador cromatografia.
  */
 @Autowired
 @Qualifier("controladorCromatografia")
 private ControladorCromatografia controladorCromatografia;

 /**
  * The controlador documento cobranca.
  */
 @Autowired
 @Qualifier("controladorDocumentoCobranca")
 private ControladorDocumentoCobranca controladorDocumentoCobranca;

 @Autowired
 @Qualifier("controladorMedidor")
 private ControladorMedidor controladorMedidor;

 /**
  * The Constant PDF.
  */
 public static final int PDF = 1;

 /**
  * The Constant EXEL.
  */
 public static final int EXEL = 2;

 /**
  * The Constant WORD.
  */
 public static final int WORD = 3;

 /**
  * The Constant PROGRAMACAO.
  */
 public static final String PROGRAMACAO = "1";

 /**
  * The Constant SOLICITACAO.
  */
 public static final String SOLICITACAO = "2";

 /**
  * The Constant QDSXQDP.
  */
 public static final String QDSXQDP = "3";

 /**
  * The Constant HISTORICO.
  */
 public static final String HISTORICO = "4";

 private ControladorConstanteSistema getControladorConstante() {
  return ServiceLocator.getInstancia().getControladorConstanteSistema();
 }

 /**
  * Tratar mensagem.
  *
  * @param e the e
  * @return the long
  * @throws Exception the exception
  */
 private Long tratarMensagem(Exception e) throws Exception {

  if (e instanceof NegocioException) {
   NegocioException ex = (NegocioException) e;
   ex.getErros();

   throw new Exception(MensagemUtil.gerarMensagemErro(ex));

  } else if (e instanceof GGASException) {
   GGASException ex = (GGASException) e;
   if (ex.getChaveErro().isEmpty()) {
    throw new java.lang.Exception(ex.getMessage());
   }

   throw new java.lang.Exception(ex.getChaveErro());
  }

  throw new Exception("Entre em contato com o administrador do Sistema");

 }

 /**
  * Verificar numero contrato.
  *
  * @param numeroContrato the numero contrato
  * @return the contrato to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "verificarNumeroContrato")
 @RequestWrapper(localName = "verificarNumeroContratoRequest")
 @ResponseWrapper(localName = "verificarNumeroContratoResposta")
 public ContratoTO verificarNumeroContrato(@WebParam(name = "numeroContrato") String numeroContrato) throws Exception {

  Collection<Contrato> colecaoContratos;
  try {
   colecaoContratos = controladorContrato.consultarContratoPorNumero(numeroContrato);
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new java.lang.Exception(e.getMessage(), e);
  }

  if (colecaoContratos != null && !colecaoContratos.isEmpty()) {
   return this.popularContratoTO(colecaoContratos.iterator().next());
  }

  return null;
 }

 /**
  * Popular contrato to.
  *
  * @param contrato the contrato
  * @return the contrato to
  * @throws Exception the exception
  */
 private ContratoTO popularContratoTO(Contrato contrato) throws Exception {

  SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
  ContratoTO contratoTO = new ContratoTO();
  contratoTO.setChavePrimaria(contrato.getChavePrimaria());
  contratoTO.setNumeroContrato(contrato.getNumeroCompletoContrato());
  contratoTO.setAgrupamentoCobranca(contrato.getAgrupamentoCobranca());
  contratoTO.setTitularContrato(contrato.getClienteAssinatura().getNome());
  contratoTO.setChavePrimariaTitularContrato(contrato.getClienteAssinatura().getChavePrimaria());
  contratoTO.setRgCliente(contrato.getClienteAssinatura().getRg());
  contratoTO.setEmailPrincialCliente(contrato.getClienteAssinatura().getEmailPrincipal());
  if (contrato.getDataAssinatura() != null) {
	  contratoTO.setDataAssinatura(formatador.format(contrato.getDataAssinatura()));
  }
  if (contrato.getClienteAssinatura().getCpf() != null && !contrato.getClienteAssinatura().getCpf().isEmpty()) {
   contratoTO.setCpfCnpjCliente(contrato.getClienteAssinatura().getCpfFormatado());
  } else if (contrato.getClienteAssinatura().getCnpj() != null && !contrato.getClienteAssinatura().getCnpj().isEmpty()) {
   contratoTO.setCpfCnpjCliente(contrato.getClienteAssinatura().getCnpjFormatado());
  }

  if (contrato.getClienteAssinatura().getFones() != null && !contrato.getClienteAssinatura().getFones().isEmpty()) {
   for (ClienteFone fone : contrato.getClienteAssinatura().getFones()) {
    if (fone.getIndicadorPrincipal()) {
     contratoTO.setCodigoDDDCliente(fone.getCodigoDDD().toString());
     contratoTO.setTelefoneCliente(fone.getNumero().toString());
     break;
    }
   }
  }
  contratoTO = popularListaPontoConsumoTO(contratoTO);

  contratoTO.setUsaProgramacaoDeConsumo(false);
  if (!CollectionUtils.isEmpty(contratoTO.getListaPontoConsumoTO())) {
   for (PontoConsumoTO pontoConsumoTo : contratoTO.getListaPontoConsumoTO()) {
    if (pontoConsumoTo.getIndUsaProgramacaoConsumo() && !contratoTO.getUsaProgramacaoDeConsumo()) {
     contratoTO.setUsaProgramacaoDeConsumo(true);
    }
   }
  }

  return contratoTO;
 }

 /**
  * Consultar cliente.
  *
  * @param idCliente the id cliente
  * @return the cliente to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarCliente")
 @RequestWrapper(localName = "consultarClienteRequest")
 @ResponseWrapper(localName = "consultarClienteResposta")
 public ClienteTO consultarCliente(@WebParam(name = "idCliente") Long idCliente) throws Exception {

  Cliente cliente = null;

  try {
   cliente = (Cliente) controladorCliente.obter(idCliente, ENDERECOS, FONES, CONTATOS, "anexos");

  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new java.lang.Exception(e.getMessage(), e);
  }

  if (cliente != null) {

   return this.popularClienteTO(cliente);
  }

  return null;
 }

	/**
	 * Popular cliente to.
	 *
	 * @param cliente {@link Cliente}
	 * @return clienteTO {@link ClienteTO}
	 */
	private ClienteTO popularClienteTO(Cliente cliente) {

		ClienteTO clienteTO = new ClienteTO();

		// preencher Cliente
		preencherCliente(cliente, clienteTO);

		// preencher ClienteEndereco
		if (cliente.getEnderecos() != null && !cliente.getEnderecos().isEmpty()) {
			clienteTO.setEnderecosTO(this.popularListaEnderecos(cliente.getEnderecos()));
		} else {
			clienteTO.setEnderecosTO(null);
		}

		// preencher ClienteFone
		if (cliente.getFones() != null && !cliente.getFones().isEmpty()) {
			clienteTO.setFonesTO(this.popularListaClienteFoneTO(cliente.getFones()));
		} else {
			clienteTO.setFonesTO(null);
		}

		// preencher ContatoCliente
		if (cliente.getContatos() != null && !cliente.getContatos().isEmpty()) {
			clienteTO.setContatosTO(this.popularListaClienteContatoTO(cliente.getContatos()));
		} else {
			clienteTO.setContatosTO(null);
		}

		// preencher Anexo
		if (cliente.getAnexos() != null && !cliente.getAnexos().isEmpty()) {
			clienteTO.setAnexosTO(this.popularListaClienteAnexoTO(cliente.getAnexos()));
		} else {
			clienteTO.setAnexosTO(null);
		}

		clienteTO.setSenha(cliente.getSenha());

		clienteTO.setDataUltimoAcesso(cliente.getDataUltimoAcesso());

		return clienteTO;
	}

 /**
  * @param cliente
  * @param clienteTO
  */
 private void preencherCliente(Cliente cliente, ClienteTO clienteTO) {
  clienteTO.setChavePrimaria(cliente.getChavePrimaria());
  clienteTO.setNome(cliente.getNome());
  clienteTO.setNomeAbreviado(cliente.getNomeAbreviado());
  clienteTO.setEmailPrincipal(cliente.getEmailPrincipal());
  clienteTO.setEmailSecundario(cliente.getEmailSecundario());

  if (cliente.getTipoCliente() != null) {
   clienteTO.setTipoCliente(this.popularTipoClienteTO(cliente.getTipoCliente().getDescricao(),
     cliente.getTipoCliente().getChavePrimaria(), cliente.getTipoCliente().getTipoPessoa().getDescricao(), cliente
       .getTipoCliente().getTipoPessoa().getCodigo()));
  } else {
   clienteTO.setTipoCliente(null);
  }
  clienteTO.setCpf(cliente.getCpf());
  clienteTO.setRg(cliente.getRg());
  clienteTO.setCnpj(cliente.getCnpj());
  clienteTO.setNumeroPassaporte(cliente.getNumeroPassaporte());

  if (cliente.getDataEmissaoRG() != null) {
   clienteTO.setDataEmissaoRG(Util.converterDataParaString(cliente.getDataEmissaoRG()));
  }
  if (cliente.getDataNascimento() != null) {
   clienteTO.setDataNascimento(Util.converterDataParaString(cliente.getDataNascimento()));
  }

  clienteTO.setNomeMae(cliente.getNomeMae());
  clienteTO.setNomePai(cliente.getNomePai());
  clienteTO.setNomeFantasia(cliente.getNomeFantasia());
  clienteTO.setInscricaoEstadual(cliente.getInscricaoEstadual());
  clienteTO.setInscricaoMunicipal(cliente.getInscricaoMunicipal());
  clienteTO.setInscricaoRural(cliente.getInscricaoRural());
  if (cliente.getEnderecoPrincipal() != null) {
   clienteTO.setEnderecos(cliente.getEnderecoPrincipal().getEnderecoFormatadoBairro());
  } else {
   clienteTO.setEnderecos(null);
  }
  if (cliente.getClienteSituacao() != null) {
   clienteTO.setClienteSituacao(this.popularListaTipoComboBoxTO(cliente.getClienteSituacao()
     .getDescricao(), cliente.getClienteSituacao().getChavePrimaria()));
  } else {
   clienteTO.setClienteSituacao(null);
  }

  if (cliente.getOrgaoExpedidor() != null) {
   clienteTO.setOrgaoExpedidor(this.popularListaTipoComboBoxTO(cliente.getOrgaoExpedidor()
     .getDescricao(), cliente.getOrgaoExpedidor().getChavePrimaria()));
  } else {
   clienteTO.setOrgaoExpedidor(null);
  }

  if (cliente.getUnidadeFederacao() != null) {
   clienteTO.setUnidadeFederacao(this.popularListaTipoComboBoxTO(cliente.getUnidadeFederacao()
     .getDescricao(), cliente.getUnidadeFederacao().getChavePrimaria()));
  } else {
   clienteTO.setUnidadeFederacao(null);
  }

  if (cliente.getNacionalidade() != null) {
   clienteTO.setNacionalidade(this.popularListaTipoComboBoxTO(cliente.getNacionalidade()
     .getDescricao(), cliente.getNacionalidade().getChavePrimaria()));
  } else {
   clienteTO.setNacionalidade(null);
  }

  if (cliente.getProfissao() != null) {
   clienteTO.setProfissao(this.popularListaTipoComboBoxTO(cliente.getProfissao().getDescricao(),
     cliente.getProfissao().getChavePrimaria()));
  } else {
   clienteTO.setProfissao(null);
  }

  if (cliente.getSexo() != null) {
   clienteTO.setSexo(this.popularListaTipoComboBoxTO(cliente.getSexo().getDescricao(), cliente.getSexo()
     .getChavePrimaria()));
  } else {
   clienteTO.setSexo(null);
  }

  if (cliente.getRendaFamiliar() != null) {
   clienteTO.setRendaFamiliar(this.popularListaFaixaRendaFamiliarTO(cliente.getRendaFamiliar()
     .getValorFaixaInicio().doubleValue(), cliente.getRendaFamiliar().getValorFaixaFim().doubleValue(), cliente
     .getRendaFamiliar().getChavePrimaria()));
  } else {
   clienteTO.setRendaFamiliar(null);
  }

  if (cliente.getAtividadeEconomica() != null) {
   clienteTO.setAtividadeEconomica(this.popularListaAtividadeEconomicaTO(cliente
     .getAtividadeEconomica().getDescricao(), cliente.getAtividadeEconomica().getCodigo(), cliente
     .getAtividadeEconomica().getCodigoOriginal(), cliente.getAtividadeEconomica().getChavePrimaria()));
  } else {
   clienteTO.setAtividadeEconomica(null);
  }

  clienteTO.setRegimeRecolhimento(cliente.getRegimeRecolhimento());

  if (cliente.getGrupoEconomico() != null) {
   clienteTO.setGrupoEconomicoTO(this.popularGrupoEconomicoTO(cliente.getGrupoEconomico()));
  } else {
   clienteTO.setGrupoEconomicoTO(null);
  }
 }

 /**
  * Popular grupo economico to.
  *
  * @param grupoEconomico the grupo economico
  * @return the grupo economico to
  */
 private GrupoEconomicoTO popularGrupoEconomicoTO(GrupoEconomico grupoEconomico) {

  GrupoEconomicoTO grupoEconomicoTO = new GrupoEconomicoTO();
  grupoEconomicoTO.setChavePrimaria(grupoEconomico.getChavePrimaria());
  grupoEconomicoTO.setDescricao(grupoEconomico.getDescricao());

  return grupoEconomicoTO;
 }

 /**
  * Popular lista cliente anexo to.
  *
  * @param anexos the anexos
  * @return the collection
  */
 private Collection<ClienteAnexoTO> popularListaClienteAnexoTO(Collection<ClienteAnexo> anexos) {

  Collection<ClienteAnexoTO> listaClienteAnexoTO = new ArrayList<ClienteAnexoTO>();

  for (ClienteAnexo clienteAnexo : anexos) {

   ClienteAnexoTO clienteAnexoTO = new ClienteAnexoTO();
   clienteAnexoTO.setChavePrimaria(clienteAnexo.getChavePrimaria());
   clienteAnexoTO.setDescricaoAnexo(clienteAnexo.getDescricaoAnexo());
   clienteAnexoTO.setDocumentoAnexo(clienteAnexo.getDocumentoAnexo());

   if (clienteAnexo.getAbaAnexo() != null) {
    clienteAnexoTO.setAbaAnexo(this.popularListaTipoComboBoxTO(clienteAnexo.getAbaAnexo()
      .getDescricao(), clienteAnexo.getAbaAnexo().getChavePrimaria()));
   } else {
    clienteAnexoTO.setAbaAnexo(null);
   }

   if (clienteAnexo.getTipoAnexo() != null) {
    clienteAnexoTO.setTipoAnexo(this.popularListaTipoComboBoxTO(clienteAnexo.getTipoAnexo()
      .getDescricao(), clienteAnexo.getTipoAnexo().getChavePrimaria()));
   } else {
    clienteAnexoTO.setTipoAnexo(null);
   }
   clienteAnexoTO.setIndicadorRegistroExistente(true);

   clienteAnexoTO.setIndicadorRegistroExistente(true);
   listaClienteAnexoTO.add(clienteAnexoTO);

  }

  return listaClienteAnexoTO;
 }

 /**
  * Popular lista cliente contato to.
  *
  * @param contatos the contatos
  * @return the collection
  */
 private Collection<ClienteContatoTO> popularListaClienteContatoTO(Collection<ContatoCliente> contatos) {

  Collection<ClienteContatoTO> listaClienteContatoTO = new ArrayList<ClienteContatoTO>();

  for (ContatoCliente contatoCliente : contatos) {

   ClienteContatoTO clienteContatoTO = new ClienteContatoTO();
   clienteContatoTO.setChavePrimaria(contatoCliente.getChavePrimaria());
   clienteContatoTO.setCodigoDDD(contatoCliente.getCodigoDDD());
   clienteContatoTO.setDescricaoArea(contatoCliente.getDescricaoArea());
   clienteContatoTO.setEmail(contatoCliente.getEmail());
   clienteContatoTO.setFoneContato(contatoCliente.getFone());
   clienteContatoTO.setNomeContato(contatoCliente.getNome());
   clienteContatoTO.setPrincipal(contatoCliente.isPrincipal());

   if (contatoCliente.getProfissao() != null) {
    clienteContatoTO.setProfissao(this.popularListaTipoComboBoxTO(contatoCliente.getProfissao().getDescricao(), contatoCliente
      .getProfissao().getChavePrimaria()));
   }

   clienteContatoTO.setRamalContato(contatoCliente.getRamal());

   if (contatoCliente.getTipoContato() != null) {
    clienteContatoTO.setTipoContato(this.popularListaTipoComboBoxTO(contatoCliente.getTipoContato().getDescricao(),
      contatoCliente.getTipoContato().getChavePrimaria()));
   }

   clienteContatoTO.setIndicadorRegistroExistente(true);
   listaClienteContatoTO.add(clienteContatoTO);
  }

  return listaClienteContatoTO;
 }

 /**
  * Popular lista cliente fone to.
  *
  * @param fones the fones
  * @return the collection
  * @throws Exception the exception
  */
 private Collection<ClienteFoneTO> popularListaClienteFoneTO(Collection<ClienteFone> fones) {

  Collection<ClienteFoneTO> listaClienteFoneTO = new ArrayList<ClienteFoneTO>();
  for (ClienteFone clienteFone : fones) {

   ClienteFoneTO clienteFoneTO = new ClienteFoneTO();
   clienteFoneTO.setChavePrimariaTelefone(clienteFone.getChavePrimaria());
   clienteFoneTO.setCodigoDDD(clienteFone.getCodigoDDD());
   clienteFoneTO.setIndicadorPrincipal(clienteFone.getIndicadorPrincipal());
   clienteFoneTO.setNumeroFone(clienteFone.getNumero());
   clienteFoneTO.setRamalFone(clienteFone.getRamal());

   if (clienteFone.getTipoFone() != null) {
    clienteFoneTO.setTipoFone(this.popularListaTipoComboBoxTO(clienteFone.getTipoFone().getDescricao(), clienteFone
      .getTipoFone().getChavePrimaria()));
   }

   clienteFoneTO.setIndicadorRegistroExistente(true);
   clienteFoneTO.setIndicadorRegistroExistente(true);
   listaClienteFoneTO.add(clienteFoneTO);
  }

  return listaClienteFoneTO;
 }

 /**
  * Popular lista enderecos.
  *
  * @param enderecos the enderecos
  * @return the collection
  * @throws Exception the exception
  */
 private Collection<ClienteEnderecoTO> popularListaEnderecos(Collection<ClienteEndereco> enderecos) {

  Collection<ClienteEnderecoTO> listaClienteEnderecoTO = new ArrayList<ClienteEnderecoTO>();
  for (ClienteEndereco clienteEndereco : enderecos) {

   ClienteEnderecoTO clienteEnderecoTO = new ClienteEnderecoTO();
   clienteEnderecoTO.setChavePrimaria(clienteEndereco.getChavePrimaria());
   clienteEnderecoTO.setCaixaPostal(clienteEndereco.getCaixaPostal());

   if (clienteEndereco.getCep() != null) {
    clienteEnderecoTO.setCep(CepUtil.converter(clienteEndereco.getCep()));
   }

   clienteEnderecoTO.setComplemento(clienteEndereco.getComplemento());

   clienteEnderecoTO.setEnderecoReferencia(clienteEndereco.getEnderecoReferencia());
   clienteEnderecoTO.setNumeroEndereco(clienteEndereco.getNumero());

   if (clienteEndereco.getTipoEndereco() != null) {
    clienteEnderecoTO.setTipoEndereco(this.popularListaTipoComboBoxTO(clienteEndereco
      .getTipoEndereco().getDescricao(), clienteEndereco.getTipoEndereco().getChavePrimaria()));
   } else {
    clienteEnderecoTO.setTipoEndereco(null);
   }

   if (clienteEndereco.getMunicipio() != null) {
    clienteEnderecoTO.setChavePrimariaMunicipio(clienteEndereco.getMunicipio().getChavePrimaria());
   } else {
    clienteEnderecoTO.setChavePrimariaMunicipio(null);
   }
   clienteEnderecoTO.setIndicadorRegistroExistente(true);
   clienteEnderecoTO.setCorrespondencia(clienteEndereco.getCorrespondencia());
   listaClienteEnderecoTO.add(clienteEnderecoTO);
  }
  return listaClienteEnderecoTO;
 }

 /**
  * Consultar lista tipo cliente.
  *
  * @return the collection
  * @throws GGASException the exception
  */
 @WebMethod(operationName = "consultarListaTipoCliente")
 @RequestWrapper(localName = "consultarListaTipoClienteRequest")
 @ResponseWrapper(localName = "consultarListaTipoClienteResposta")
 public Collection<TipoClienteTO> consultarListaTipoCliente() throws GGASException {

  Collection<TipoClienteTO> listaTipoClienteTO = new ArrayList<TipoClienteTO>();
  Collection<TipoCliente> listaTipoCliente = null;

  try {
   listaTipoCliente = controladorCliente.listarTipoCliente();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaTipoCliente != null && !listaTipoCliente.isEmpty()) {
   for (TipoCliente tipoCliente : listaTipoCliente) {

    TipoClienteTO tipoClienteTO = this.popularTipoClienteTO(tipoCliente.getDescricao(), tipoCliente.getChavePrimaria(),
      tipoCliente.getTipoPessoa().getDescricao(), tipoCliente.getTipoPessoa().getCodigo());
    listaTipoClienteTO.add(tipoClienteTO);
   }
  }

  return listaTipoClienteTO;
 }

 /**
  * Consultar lista tipo anexo.
  *
  * @return the collection
  * @throws GGASException the exception
  */
 @WebMethod(operationName = "consultarListaTipoAnexo")
 @RequestWrapper(localName = "consultarListaTipoAnexoRequest")
 @ResponseWrapper(localName = "consultarListaTipoAnexoResposta")
 public Collection<TipoComboBoxTO> consultarListaTipoAnexo() throws GGASException {

  Collection<TipoComboBoxTO> listaTipoClienteTO = new ArrayList<TipoComboBoxTO>();
  Collection<EntidadeConteudo> listaTipoAnexo = null;

  try {
   ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TIPO_ANEXO);
   listaTipoAnexo = controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));

  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaTipoAnexo != null && !listaTipoAnexo.isEmpty()) {
   for (EntidadeConteudo tipoAnexo : listaTipoAnexo) {

    TipoComboBoxTO tipoAnexoTO = this.popularListaTipoComboBoxTO(tipoAnexo.getDescricao(), tipoAnexo.getChavePrimaria());
    listaTipoClienteTO.add(tipoAnexoTO);
   }
  }

  return listaTipoClienteTO;
 }

 /**
  * Consultar lista grupo economico.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaGrupoEconomico")
 @RequestWrapper(localName = "consultarListaGrupoEconomicoRequest")
 @ResponseWrapper(localName = "consultarListaGrupoEconomicoResposta")
 public Collection<GrupoEconomicoTO> consultarListaGrupoEconomico() throws GGASException {

  Collection<GrupoEconomicoTO> listaGrupoEconomicoTO = new ArrayList<GrupoEconomicoTO>();
  Collection<GrupoEconomico> listaGrupoEconomico = null;

  try {
   listaGrupoEconomico = controladorCliente.listarGrupoEconomico();
  } catch (NegocioException e) {
   throw new GGASException(e.getMessage(), e);
  }

  if (listaGrupoEconomico != null && !listaGrupoEconomico.isEmpty()) {
   for (GrupoEconomico grupoEconomico : listaGrupoEconomico) {

    GrupoEconomicoTO grupoEconomicoTO = this.popularGrupoEconomicoTO(grupoEconomico);
    listaGrupoEconomicoTO.add(grupoEconomicoTO);
   }
  }

  return listaGrupoEconomicoTO;
 }

 /**
  * Popular lista tipo combo box to.
  *
  * @param descricao     the descricao
  * @param chavePrimaria the chave primaria
  * @return the tipo combo box to
  */
 private TipoComboBoxTO popularListaTipoComboBoxTO(String descricao, Long chavePrimaria) {

  TipoComboBoxTO tipoComboBoxTO = new TipoComboBoxTO();
  tipoComboBoxTO.setDescricao(descricao);
  tipoComboBoxTO.setChavePrimaria(chavePrimaria);

  return tipoComboBoxTO;
 }

 /**
  * Popular tipo cliente to.
  *
  * @param descricao           the descricao
  * @param chavePrimaria       the chave primaria
  * @param descricaoTipoPessoa the descricao tipo pessoa
  * @param tipoPessoa          the tipo pessoa
  * @return the tipo cliente to
  */
 private TipoClienteTO popularTipoClienteTO(String descricao, Long chavePrimaria, String descricaoTipoPessoa, Integer tipoPessoa) {

  TipoClienteTO tipoClienteTO = new TipoClienteTO();
  tipoClienteTO.setChavePrimaria(chavePrimaria);
  tipoClienteTO.setDescricao(descricao);
  tipoClienteTO.setDescricaoTipoPessoa(descricaoTipoPessoa);
  tipoClienteTO.setTipoPessoa(tipoPessoa);

  return tipoClienteTO;
 }

 /**
  * Consultar lista tipos endereco.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaTiposEndereco")
 @RequestWrapper(localName = "consultarListaTiposEnderecoRequest")
 @ResponseWrapper(localName = "consultarListaTiposEnderecoResposta")
 public Collection<TipoComboBoxTO> consultarListaTiposEndereco() throws GGASException {

  Collection<TipoComboBoxTO> listaTiposEnderecoTO = new ArrayList<TipoComboBoxTO>();
  Collection<TipoEndereco> listaTiposEndereco = null;

  try {
   listaTiposEndereco = controladorCliente.listarTipoEndereco();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaTiposEndereco != null && !listaTiposEndereco.isEmpty()) {
   for (TipoEndereco tiposEndereco : listaTiposEndereco) {

    TipoComboBoxTO tipoEnderecoTO = this.popularListaTipoComboBoxTO(tiposEndereco.getDescricao(),
      tiposEndereco.getChavePrimaria());
    listaTiposEnderecoTO.add(tipoEnderecoTO);
   }
  }

  return listaTiposEnderecoTO;
 }

 /**
  * Consultar lista tipo contato.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaTipoContato")
 @RequestWrapper(localName = "consultarListaTipoContatoRequest")
 @ResponseWrapper(localName = "consultarListaTipoContatoResposta")
 public Collection<TipoComboBoxTO> consultarListaTipoContato() throws GGASException {

  Collection<TipoComboBoxTO> listaTipoContatoTO = new ArrayList<TipoComboBoxTO>();
  Collection<TipoContato> listaTiposContato = null;

  try {
   listaTiposContato = controladorCliente.listarTipoContato();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaTiposContato != null && !listaTiposContato.isEmpty()) {
   for (TipoContato tiposContato : listaTiposContato) {

    TipoComboBoxTO tipoContatoTO = this
      .popularListaTipoComboBoxTO(tiposContato.getDescricao(), tiposContato.getChavePrimaria());
    listaTipoContatoTO.add(tipoContatoTO);
   }
  }

  return listaTipoContatoTO;
 }

 /**
  * Consultar lista tipo fone.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaTipoFone")
 @RequestWrapper(localName = "consultarListaTipoFoneRequest")
 @ResponseWrapper(localName = "consultarListaTipoFoneResposta")
 public Collection<TipoComboBoxTO> consultarListaTipoFone() throws GGASException {

  Collection<TipoComboBoxTO> listaTipoFoneTO = new ArrayList<TipoComboBoxTO>();
  Collection<TipoFone> listaTiposTelefone = null;

  try {
   listaTiposTelefone = controladorCliente.listarTipoFone();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaTiposTelefone != null && !listaTiposTelefone.isEmpty()) {
   for (TipoFone tiposTelefone : listaTiposTelefone) {

    TipoComboBoxTO tipoFoneTO = this.popularListaTipoComboBoxTO(tiposTelefone.getDescricao(), tiposTelefone.getChavePrimaria());
    listaTipoFoneTO.add(tipoFoneTO);
   }
  }

  return listaTipoFoneTO;
 }

 /**
  * Consultar lista profissao.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaProfissao")
 @RequestWrapper(localName = "consultarListaProfissaoRequest")
 @ResponseWrapper(localName = "consultarListaProfissaoResposta")
 public Collection<TipoComboBoxTO> consultarListaProfissao() throws GGASException {

  Collection<TipoComboBoxTO> listaProfissaoTO = new ArrayList<TipoComboBoxTO>();
  Collection<Profissao> listaProfissao = null;

  try {
   listaProfissao = controladorCliente.listarProfissao();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaProfissao != null && !listaProfissao.isEmpty()) {
   for (Profissao profissao : listaProfissao) {

    TipoComboBoxTO profissaoTO = this.popularListaTipoComboBoxTO(profissao.getDescricao(), profissao.getChavePrimaria());
    listaProfissaoTO.add(profissaoTO);
   }
  }

  return listaProfissaoTO;
 }

 /**
  * Consultar lista orgao expedidor.
  *
  * @return the collection
  * @throws GGASException {@link GGASException}
  */
 @WebMethod(operationName = "consultarListaOrgaoExpedidor")
 @RequestWrapper(localName = "consultarListaOrgaoExpedidorRequest")
 @ResponseWrapper(localName = "consultarListaOrgaoExpedidorResposta")
 public Collection<TipoComboBoxTO> consultarListaOrgaoExpedidor() throws GGASException {

  Collection<TipoComboBoxTO> listaOrgaoExpedidorTO = new ArrayList<TipoComboBoxTO>();
  Collection<OrgaoExpedidor> listaOrgaoExpedidor = null;

  try {
   listaOrgaoExpedidor = controladorCliente.listarOrgaoExpedidor();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }

  if (listaOrgaoExpedidor != null && !listaOrgaoExpedidor.isEmpty()) {
   for (OrgaoExpedidor orgaoExpedidor : listaOrgaoExpedidor) {

    TipoComboBoxTO profissaoTO = this.popularListaTipoComboBoxTO(orgaoExpedidor.getDescricao(),
      orgaoExpedidor.getChavePrimaria());
    listaOrgaoExpedidorTO.add(profissaoTO);
   }
  }

  return listaOrgaoExpedidorTO;
 }

 /**
  * Consultar lista unidade federacao.
  *
  * @return the collection
  */
 @WebMethod(operationName = "consultarListaUnidadeFederacao")
 @RequestWrapper(localName = "consultarListaUnidadeFederacaoRequest")
 @ResponseWrapper(localName = "consultarListaUnidadeFederacaoResposta")
 public Collection<TipoComboBoxTO> consultarListaUnidadeFederacao() {

  Collection<TipoComboBoxTO> listaUFTO = new ArrayList<TipoComboBoxTO>();
  Collection<UnidadeFederacao> listaUF = null;

  listaUF = controladorEndereco.listarTodasUnidadeFederacao();

  if (listaUF != null && !listaUF.isEmpty()) {
   for (UnidadeFederacao unidadeFederacao : listaUF) {

    TipoComboBoxTO profissaoTO = this.popularListaTipoComboBoxTO(unidadeFederacao.getDescricao(),
      unidadeFederacao.getChavePrimaria());
    listaUFTO.add(profissaoTO);
   }
  }

  return listaUFTO;
 }

 /**
  * Consultar lista faixa renda familiar.
  *
  * @return the collection
  * @throws NegocioException {@link NegocioException}
  */
 @WebMethod(operationName = "consultarListaFaixaRendaFamiliar")
 @RequestWrapper(localName = "consultarListaFaixaRendaFamiliarRequest")
 @ResponseWrapper(localName = "consultarListaFaixaRendaFamiliarResposta")
 public Collection<FaixaRendaFamiliarTO> consultarListaFaixaRendaFamiliar() throws NegocioException {

  Collection<FaixaRendaFamiliarTO> listaFaixaRendaFamiliarTO = new ArrayList<FaixaRendaFamiliarTO>();
  Collection<FaixaRendaFamiliar> listaFaixaRendaFamiliar = null;

  listaFaixaRendaFamiliar = controladorCliente.listarFaixaRendaFamiliar();

  if (listaFaixaRendaFamiliar != null && !listaFaixaRendaFamiliar.isEmpty()) {
   for (FaixaRendaFamiliar faixaRendaFamiliar : listaFaixaRendaFamiliar) {

    FaixaRendaFamiliarTO faixaRendaFamiliarTO = this.popularListaFaixaRendaFamiliarTO(faixaRendaFamiliar.getValorFaixaInicio()
      .doubleValue(), faixaRendaFamiliar.getValorFaixaFim().doubleValue(), faixaRendaFamiliar.getChavePrimaria());
    listaFaixaRendaFamiliarTO.add(faixaRendaFamiliarTO);
   }
  }

  return listaFaixaRendaFamiliarTO;
 }

 /**
  * Popular lista faixa renda familiar to.
  *
  * @param valorFaixaInicio the valor faixa inicio
  * @param valorFaixaFim    the valor faixa fim
  * @param chavePrimaria    the chave primaria
  * @return the faixa renda familiar to
  */
 private FaixaRendaFamiliarTO popularListaFaixaRendaFamiliarTO(Double valorFaixaInicio, Double valorFaixaFim, Long chavePrimaria) {

  FaixaRendaFamiliarTO faixaRendaFamiliarTO = new FaixaRendaFamiliarTO();
  faixaRendaFamiliarTO.setValorFaixaInicio(valorFaixaInicio);
  faixaRendaFamiliarTO.setValorFaixaFim(valorFaixaFim);
  faixaRendaFamiliarTO.setChavePrimaria(chavePrimaria);

  return faixaRendaFamiliarTO;
 }

 /**
  * Consultar lista cliente situacao.
  *
  * @return the collection
  * @throws NegocioException the exception
  */
 @WebMethod(operationName = "consultarListaClienteSituacao")
 @RequestWrapper(localName = "consultarListaClienteSituacaoRequest")
 @ResponseWrapper(localName = "consultarListaClienteSituacaoResposta")
 public Collection<TipoComboBoxTO> consultarListaClienteSituacao() throws NegocioException {

  Collection<TipoComboBoxTO> listaClienteSituacaoTO = new ArrayList<TipoComboBoxTO>();
  Collection<ClienteSituacao> listaClienteSituacao = null;

  listaClienteSituacao = controladorCliente.listarClienteSituacao();

  if (listaClienteSituacao != null && !listaClienteSituacao.isEmpty()) {
   for (ClienteSituacao clienteSituacao : listaClienteSituacao) {

    TipoComboBoxTO clienteSituacaoTO = this.popularListaTipoComboBoxTO(clienteSituacao.getDescricao(),
      clienteSituacao.getChavePrimaria());
    listaClienteSituacaoTO.add(clienteSituacaoTO);
   }
  }

  return listaClienteSituacaoTO;
 }

 /**
  * Consultar lista atividade economica.
  *
  * @return the collection
  * @throws NegocioException the exception
  */
 @WebMethod(operationName = "consultarListaAtividadeEconomica")
 @RequestWrapper(localName = "consultarListaAtividadeEconomicaRequest")
 @ResponseWrapper(localName = "consultarListaAtividadeEconomicaResposta")
 public Collection<AtividadeEconomicaTO> consultarListaAtividadeEconomica() throws NegocioException {

  Collection<AtividadeEconomicaTO> listaAtividadeEconomicaTO = new ArrayList<AtividadeEconomicaTO>();
  Collection<AtividadeEconomica> listaAtividadeEconomica = null;

  listaAtividadeEconomica = controladorCliente.listarAtividadeEconomica();

  if (listaAtividadeEconomica != null && !listaAtividadeEconomica.isEmpty()) {
   for (AtividadeEconomica atividadeEconomica : listaAtividadeEconomica) {

    AtividadeEconomicaTO atividadeEconomicaTO = this.popularListaAtividadeEconomicaTO(atividadeEconomica.getDescricao(),
      atividadeEconomica.getCodigo(), atividadeEconomica.getCodigoOriginal(),
      atividadeEconomica.getChavePrimaria());
    listaAtividadeEconomicaTO.add(atividadeEconomicaTO);
   }
  }

  return listaAtividadeEconomicaTO;
 }

 /**
  * Popular lista atividade economica to.
  *
  * @param descricao      the descricao
  * @param codigo         the codigo
  * @param codigoOriginal the codigo original
  * @param chavePrimaria  the chave primaria
  * @return the atividade economica to
  */
 private AtividadeEconomicaTO popularListaAtividadeEconomicaTO(String descricao, String codigo, Long codigoOriginal, Long chavePrimaria) {

  AtividadeEconomicaTO atividadeEconomicaTO = new AtividadeEconomicaTO();

  atividadeEconomicaTO.setDescricao(descricao);
  atividadeEconomicaTO.setCodigo(codigo);
  atividadeEconomicaTO.setCodigoOriginal(codigoOriginal);
  atividadeEconomicaTO.setChavePrimaria(chavePrimaria);

  return atividadeEconomicaTO;
 }

 /**
  * Consultar lista pessoa sexo.
  *
  * @return the collection
  * @throws NegocioException the exception
  */
 @WebMethod(operationName = "consultarListaPessoaSexo")
 @RequestWrapper(localName = "consultarListaPessoaSexoRequest")
 @ResponseWrapper(localName = "consultarListaPessoaSexoResposta")
 public Collection<TipoComboBoxTO> consultarListaPessoaSexo() throws NegocioException {

  Collection<TipoComboBoxTO> listaPessoaSexoTO = new ArrayList<TipoComboBoxTO>();
  Collection<PessoaSexo> listaPessoaSexo = null;

  listaPessoaSexo = controladorCliente.listarPessoaSexo();

  if (listaPessoaSexo != null && !listaPessoaSexo.isEmpty()) {
   for (PessoaSexo pessoaSexo : listaPessoaSexo) {

    TipoComboBoxTO pessoaSexoTO = this.popularListaTipoComboBoxTO(pessoaSexo.getDescricao(), pessoaSexo.getChavePrimaria());
    listaPessoaSexoTO.add(pessoaSexoTO);
   }
  }

  return listaPessoaSexoTO;
 }

 /**
  * Consultar lista nacionalidade.
  *
  * @return the collection
  * @throws NegocioException the exception
  */
 @WebMethod(operationName = "consultarListaNacionalidade")
 @RequestWrapper(localName = "consultarListaNacionalidadeRequest")
 @ResponseWrapper(localName = "consultarListaNacionalidadeResposta")
 public Collection<TipoComboBoxTO> consultarListaNacionalidade() throws NegocioException {

  Collection<TipoComboBoxTO> listaNacionalidadeTO = new ArrayList<TipoComboBoxTO>();
  Collection<Nacionalidade> listaNacionalidade = null;

  listaNacionalidade = controladorCliente.listarNacionalidade();

  if (listaNacionalidade != null && !listaNacionalidade.isEmpty()) {
   for (Nacionalidade nacionalidade : listaNacionalidade) {

    TipoComboBoxTO nacionalidadeTO = this.popularListaTipoComboBoxTO(nacionalidade.getDescricao(),
      nacionalidade.getChavePrimaria());
    listaNacionalidadeTO.add(nacionalidadeTO);
   }
  }

  return listaNacionalidadeTO;
 }

 /**
  * Popular lista ponto consumo to.
  *
  * @param contratoTO the contrato to
  * @return the contrato to
  * @throws GGASException the exception
  */
 public ContratoTO popularListaPontoConsumoTO(ContratoTO contratoTO) throws GGASException {

  List<PontoConsumoTO> listaPontoConsumoTO = null;
  try {
   listaPontoConsumoTO = this.consultarContratoPontoConsumo(contratoTO.getChavePrimaria());
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new GGASException(e.getMessage(), e);
  }
  contratoTO.setListaPontoConsumoTO(listaPontoConsumoTO);

  return contratoTO;
 }

 /**
  * Consultar contrato ponto consumo.
  *
  * @param idContrato the id contrato
  * @return the list
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "consultarContratoPontoConsumo")
 @RequestWrapper(localName = "consultarContratoPontoConsumoRequest")
 @ResponseWrapper(localName = "consultarContratoPontoConsumoResposta")
 public List<PontoConsumoTO> consultarContratoPontoConsumo(
   @WebParam(name = "idContrato") Long idContrato)
   throws GGASException {

  List<PontoConsumoTO> listaPontoConsumoTO = null;

  Collection<ContratoPontoConsumo> colecaoContratosPontoConsumo;
  try {
   colecaoContratosPontoConsumo = controladorContrato.listarContratoPontoConsumo(idContrato);
  } catch (Exception e) {

   throw new GGASException(MensagemUtil.gerarMensagemErro(e));

  }

  if (colecaoContratosPontoConsumo != null && !colecaoContratosPontoConsumo.isEmpty()) {

   listaPontoConsumoTO = new ArrayList<PontoConsumoTO>();

   for (ContratoPontoConsumo contratoPontoConsumo : colecaoContratosPontoConsumo) {

    PontoConsumoTO pontoConsumoTO = this.popularPontoConsumoTO(contratoPontoConsumo);
    listaPontoConsumoTO.add(pontoConsumoTO);
   }
  }

  return listaPontoConsumoTO;
 }

	/**
	 * Popular ponto consumo to.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @return the ponto consumo to
	 * @throws NegocioException the negocio exception
	 */
	private PontoConsumoTO popularPontoConsumoTO(
			ContratoPontoConsumo contratoPontoConsumo)
			throws NegocioException {

		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();

		List<Integer> listaVencimentosPossiveis = new ArrayList<Integer>();

		List<PontoConsumoVencimento> listaPontoConsumoVencimento =
				(List<PontoConsumoVencimento>) controladorPontoConsumo
						.listarPontoConsumoVencimento(pontoConsumo.getChavePrimaria());
		for (PontoConsumoVencimento pontoConsumoVencimento : listaPontoConsumoVencimento) {
			listaVencimentosPossiveis.add(pontoConsumoVencimento.getDiaDisponivel());
		}

		PontoConsumoTO pontoConsumoTO = new PontoConsumoTO();
		String descricaoImovel = pontoConsumo.getImovel().getNome();
		String descricaoPontoConsumo = pontoConsumo.getDescricao();
		pontoConsumoTO.setDescricao(descricaoImovel + " / " + descricaoPontoConsumo);
		pontoConsumoTO.setSituacaoPontoConsumo(pontoConsumo.getSituacaoConsumo().getDescricao());
		pontoConsumoTO.setVencimentosPossiveis(listaVencimentosPossiveis);
		Optional.ofNullable(pontoConsumo.getCep()).ifPresent(cep -> pontoConsumoTO.setCep(CepUtil.converter(cep)));
		pontoConsumoTO.setNumeroImovel(pontoConsumo.getNumeroImovel());
		pontoConsumoTO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
		pontoConsumoTO.setSegmento(Optional.ofNullable(pontoConsumo.getSegmento()).map(s -> s.getDescricao()).orElse(null));

		Long itemFaturaGas = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento =
				controladorContrato.consultarContratoPontoConsumoItemFaturamento(
						contratoPontoConsumo.getChavePrimaria(), itemFaturaGas);

		if (contratoPontoConsumoItemFaturamento != null) {
			pontoConsumoTO.setVencimentoVigente(
					contratoPontoConsumoItemFaturamento.getNumeroDiaVencimento());
		}

		pontoConsumoTO.setChavePrimaria(pontoConsumo.getChavePrimaria());

		if (pontoConsumo.getSegmento() != null) {
			pontoConsumoTO.setIndUsaProgramacaoConsumo(
					pontoConsumo.getSegmento().getUsaProgramacaoConsumo());
		}

		return pontoConsumoTO;
	}

 /**
  * Consultar nfe.
  *
  * @param nfeTO             the nfe to
  * @param chavePontoConsumo the chave ponto consumo
  * @param chaveContrato     the chave contrato
  * @return the collection
  * @throws NegocioException {@link NegocioException}
  */
 @WebMethod(operationName = "consultarNfe")
 @RequestWrapper(localName = "consultarNfeRequest")
 @ResponseWrapper(localName = "consultarNfeResposta")
 public Collection<NfeTO> consultarNfe(@WebParam(name = "nfeTO") NfeTO nfeTO,
                                       @WebParam(name = "chavePontoConsumo") List<Long> chavePontoConsumo,
                                       @WebParam(name = "chaveContrato") Long chaveContrato)
   throws GGASException {

  Collection<NfeTO> colecaoNfeTo = new ArrayList<NfeTO>();
  Collection<DocumentoFiscal> colecaoNfe;
  try {
   colecaoNfe = controladorFatura.consultaDocumentoFiscal(nfeTO, chavePontoConsumo);
  } catch (NegocioException e) {
   throw new GGASException(e.getMessage(), e);
  }

  if (colecaoNfe != null && !colecaoNfe.isEmpty()) {
   for (DocumentoFiscal notaFiscal : colecaoNfe) {

    NfeTO nfe = this.popularNfeTO(notaFiscal);
    colecaoNfeTo.add(nfe);
   }
   return colecaoNfeTo;
  }

  return null;
 }

 /**
  * Popular nfe to.
  *
  * @param documentoFiscal the documento fiscal
  * @return the nfe to
  */
 private NfeTO popularNfeTO(DocumentoFiscal documentoFiscal) {

  String valorTotal = String.valueOf(documentoFiscal.getValorTotal());

  NfeTO nfeTO = new NfeTO();
  nfeTO.setDocumentoFiscal(documentoFiscal.getNumero());
  nfeTO.setSerie(documentoFiscal.getSerie().getNumero());
  nfeTO.setValorNotaFiscal(valorTotal);
  nfeTO.setSisStatus(documentoFiscal.getFatura().getSituacaoPagamento().getDescricao());
  nfeTO.setchavePrimariaDocumentoFiscal(documentoFiscal.getChavePrimaria());
  if (documentoFiscal.getDataEmissao() != null) {
   nfeTO.setEmissao(Util.converterDataParaStringSemHora(documentoFiscal.getDataEmissao(), Constantes.FORMATO_DATA_BR));
  }
  if (documentoFiscal.getHoraSaida() != null) {
   nfeTO.setCancelamento(Util.converterDataParaStringSemHora(documentoFiscal.getHoraSaida(), Constantes.FORMATO_DATA_BR));
  }
  if (documentoFiscal.getIndicadorTipoNota() != null && documentoFiscal.getIndicadorTipoNota() != 1
    && documentoFiscal.getIndicadorTipoNota() != 0) {
   nfeTO.setIndicadorNfe(true);
   nfeTO.setChavePrimariaNfe(Long.parseLong(String.valueOf(documentoFiscal.getIndicadorTipoNota())));
  }
  if (documentoFiscal.getFatura().getDataVencimento() != null) {
   nfeTO.setDataVencimento(documentoFiscal.getFatura().getDataVencimentoFormatada());
  }
  return nfeTO;
 }

 /**
  * Consultar declaracao quitacao.
  *
  * @param codigoValidacao the codigo validacao
  * @param idCliente       the id cliente
  * @param anoReferencia   the ano referencia
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarDeclaracaoQuitacao")
 @RequestWrapper(localName = "consultarDeclaracaoQuitacaoRequest")
 @ResponseWrapper(localName = "consultarDeclaracaoQuitacaoResposta")
 public byte[] consultarDeclaracaoQuitacao(@WebParam(name = "codigoValidacao") String codigoValidacao,
                                           @WebParam(name = "idCliente") Long idCliente,
                                           @WebParam(name = "anoReferencia") Long anoReferencia) throws java.lang.Exception {

  Collection<Fatura> faturasPendentes = controladorCobranca.consultarFaturasNaoPagasCliente(idCliente, anoReferencia.toString());

  if (faturasPendentes != null && !faturasPendentes.isEmpty()) {
   throw new Exception("Existe(m) fatura(s) não quitada(s) em anos anteriores.");
  }

  byte[] relatorio = controladorCobranca.consultarDeclaracaoQuitacao(codigoValidacao, idCliente, anoReferencia);

  if (codigoValidacao != null && !codigoValidacao.trim().isEmpty() && relatorio == null) {
   throw new Exception("Nenhum registro encontrado.");
  }
  if (relatorio == null) {

   relatorio = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnualGeral(idCliente, anoReferencia);
   if (relatorio == null) {
    throw new Exception("Não foram encontradas faturas pagas para no ano de " + anoReferencia + ".");
   }

  }

  return relatorio;
 }

 /**
  * Consultar autenticacao declaracao anual.
  *
  * @param codigoValidacao the codigo validacao
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarAutenticacaoDeclaracaoAnual")
 @RequestWrapper(localName = "consultarAutenticacaoDeclaracaoAnualRequest")
 @ResponseWrapper(localName = "consultarAutenticacaoDeclaracaoAnualResposta")
 public byte[] consultarAutenticacaoDeclaracaoAnual(@WebParam(name = "codigoValidacao") String codigoValidacao)
   throws java.lang.Exception {

  byte[] relatorio = controladorCobranca.consultarDeclaracaoQuitacao(codigoValidacao, null, null);

  if (codigoValidacao != null && !codigoValidacao.trim().isEmpty() && relatorio == null) {
   throw new Exception("Nenhum registro encontrado.");
  }
  if (relatorio == null) {
   throw new Exception("Não existe Declaração Anual de Débitos para o Código: " + codigoValidacao + ".");
  }

  return relatorio;
 }

	/**
	 * Salvar chamado alteracao cliente.
	 *
	 * @param clienteTO     the cliente to
	 * @param chaveContrato the chave contrato
	 * @param descricao     descricao
	 * @return the long
	 * @throws Exception the exception
	 */
	@WebMethod(operationName = "salvarChamadoAlteracaoCliente")
	@RequestWrapper(localName = "salvarChamadoAlteracaoClienteRequest")
	@ResponseWrapper(localName = "salvarChamadoAlteracaoClienteResposta")
	public Long salvarChamadoAlteracaoCliente(@WebParam(name = "clienteTO") ClienteTO clienteTO,
			@WebParam(name = "chaveContrato") Long chaveContrato, @WebParam(name = "descricao") String descricao)
			throws java.lang.Exception {

		Long retorno = 0L;
		ChamadoAlteracaoCliente chamadoAlteracaoCliente = null;
		boolean foiAlterado = false;
		try {
			chamadoAlteracaoCliente = this.popularChamadoAlteracaoCliente(clienteTO, descricao);
			Contrato contrato = (Contrato) controladorContrato.obter(chaveContrato);
			foiAlterado = chamadoAlteracaoCliente != null && chamadoAlteracaoCliente.getChamado() != null;
			if (foiAlterado) {
				chamadoAlteracaoCliente.getChamado().setContrato(contrato);
				
				if (chamadoAlteracaoCliente.getChamado().getProtocolo() != null) {
					retorno = chamadoAlteracaoCliente.getChamado().getProtocolo().getNumeroProtocolo();
				}
			}
			controladorChamado.inserirChamadoAlteracaoCliente(chamadoAlteracaoCliente);

		} catch (GGASException e) {
			return tratarMensagem(e);
		}
		return retorno;
	}

 /**
  * Popular chamado alteracao cliente.
  *
  * @param clienteTO the cliente to
  * @param descricaoAgencia descrição das modificações realizadas na agência virtual
  * @return the chamado alteracao cliente
  * @throws Exception the exception
  */
 private ChamadoAlteracaoCliente popularChamadoAlteracaoCliente(ClienteTO clienteTO, String descricaoAgencia) throws Exception {

  ChamadoAlteracaoCliente chamadoAlteracaoCliente = new ChamadoAlteracaoCliente();

  preencheEntidadeCamposSimplesClienteTO(clienteTO, chamadoAlteracaoCliente);

  if (clienteTO.getDataEmissaoRG() != null && !clienteTO.getDataEmissaoRG().isEmpty()) {
   chamadoAlteracaoCliente.setDataEmissaoRG(Util.converterCampoStringParaData("Data", clienteTO.getDataEmissaoRG(),
     Constantes.FORMATO_DATA_BR));
  }

  if (clienteTO.getDataNascimento() != null && !clienteTO.getDataNascimento().isEmpty()) {
   chamadoAlteracaoCliente.setDataNascimento(Util.converterCampoStringParaData("Data", clienteTO.getDataNascimento(),
     Constantes.FORMATO_DATA_BR));
  }

  if (clienteTO.getTipoCliente() != null && clienteTO.getTipoCliente().getChavePrimaria() != null
    && clienteTO.getTipoCliente().getChavePrimaria() > 0) {

   TipoCliente tipoCliente = (TipoCliente) controladorCliente.criarTipoCliente();
   tipoCliente.setChavePrimaria(clienteTO.getTipoCliente().getChavePrimaria());
   chamadoAlteracaoCliente.setTipoCliente(tipoCliente);
  }

  if (clienteTO.getOrgaoExpedidor() != null && clienteTO.getOrgaoExpedidor().getChavePrimaria() != null
    && clienteTO.getOrgaoExpedidor().getChavePrimaria() > 0) {

   OrgaoExpedidor orgaoExpedidor = (OrgaoExpedidor) controladorCliente.criarOrgaoExpedidor();
   orgaoExpedidor.setChavePrimaria(clienteTO.getOrgaoExpedidor().getChavePrimaria());
   chamadoAlteracaoCliente.setOrgaoExpedidor(orgaoExpedidor);
  }

  if (clienteTO.getUnidadeFederacao() != null && clienteTO.getUnidadeFederacao().getChavePrimaria() != null
    && clienteTO.getUnidadeFederacao().getChavePrimaria() > 0) {

   UnidadeFederacao unidadeFederacao = (UnidadeFederacao) controladorCliente.criarUnidadeFederacao();
   unidadeFederacao.setChavePrimaria(clienteTO.getUnidadeFederacao().getChavePrimaria());
   chamadoAlteracaoCliente.setUnidadeFederacao(unidadeFederacao);
  }

  if (clienteTO.getNacionalidade() != null && clienteTO.getNacionalidade().getChavePrimaria() != null
    && clienteTO.getNacionalidade().getChavePrimaria() > 0) {

   Nacionalidade nacionalidade = (Nacionalidade) controladorCliente.criarNacionalidade();
   nacionalidade.setChavePrimaria(clienteTO.getNacionalidade().getChavePrimaria());
   chamadoAlteracaoCliente.setNacionalidade(nacionalidade);
  }

  if (clienteTO.getProfissao() != null && clienteTO.getProfissao().getChavePrimaria() != null
    && clienteTO.getProfissao().getChavePrimaria() > 0) {

   Profissao profissao = (Profissao) controladorCliente.criarProfissao();
   profissao.setChavePrimaria(clienteTO.getProfissao().getChavePrimaria());
   chamadoAlteracaoCliente.setProfissao(profissao);
  }

  if (clienteTO.getSexo() != null && clienteTO.getSexo().getChavePrimaria() != null && clienteTO.getSexo().getChavePrimaria() > 0) {

   PessoaSexo pessoaSexo = (PessoaSexo) controladorCliente.criarPessoaSexo();
   pessoaSexo.setChavePrimaria(clienteTO.getSexo().getChavePrimaria());
   chamadoAlteracaoCliente.setSexo(pessoaSexo);
  }

  if (clienteTO.getRendaFamiliar() != null && clienteTO.getRendaFamiliar().getChavePrimaria() != null
    && clienteTO.getRendaFamiliar().getChavePrimaria() > 0) {

   FaixaRendaFamiliar faixaRendaFamiliar = (FaixaRendaFamiliar) controladorCliente.criarFaixaRendaFamiliar();
   faixaRendaFamiliar.setChavePrimaria(clienteTO.getRendaFamiliar().getChavePrimaria());
   chamadoAlteracaoCliente.setRendaFamiliar(faixaRendaFamiliar);
  }

  if (clienteTO.getGrupoEconomicoTO() != null && clienteTO.getGrupoEconomicoTO().getChavePrimaria() != null
    && clienteTO.getGrupoEconomicoTO().getChavePrimaria() > 0) {

   GrupoEconomico grupoEconomico = new GrupoEconomicoImpl();
   grupoEconomico.setChavePrimaria(clienteTO.getGrupoEconomicoTO().getChavePrimaria());
   chamadoAlteracaoCliente.setGrupoEconomico(grupoEconomico);
  }

  if (clienteTO.getChavePrimaria() != null && clienteTO.getChavePrimaria() > 0) {
   Cliente cliente = (Cliente) controladorCliente.criar();
   cliente.setChavePrimaria(clienteTO.getChavePrimaria());
   chamadoAlteracaoCliente.setCliente(cliente);
  }

  if (clienteTO.getFonesTO() != null && !clienteTO.getFonesTO().isEmpty()) {

   chamadoAlteracaoCliente.setFonesChamados(this.popularChamadoAlteracaoClienteFoneChamado(clienteTO.getFonesTO()));
  }

  if (clienteTO.getContatosTO() != null && !clienteTO.getContatosTO().isEmpty()) {

   chamadoAlteracaoCliente.setContatosChamados(this.popularChamadoAlteracaoClienteContatoChamado(clienteTO.getContatosTO()));
  }

  if (clienteTO.getEnderecosTO() != null && !clienteTO.getEnderecosTO().isEmpty()) {

   chamadoAlteracaoCliente.setEnderecosChamados(this.popularChamadoAlteracaoClienteEnderecosChamado(clienteTO.getEnderecosTO()));
  }

  if (clienteTO.getAnexosTO() != null && !clienteTO.getAnexosTO().isEmpty()) {

   chamadoAlteracaoCliente.setAnexosChamados(this.popularChamadoClienteAnexoChamado(clienteTO.getAnexosTO()));
  }
  DadosSolicitanteTO dadosSolicitanteTO = this.popularDadosSolicitanteTO(clienteTO, ServicoUtil.formatarDescricaoChamadoAgencia(
  		clienteTO.getNome(), descricaoAgencia, "Alteração Cadastral do Cliente"));

  if (clienteTO.getChavePrimaria() != null && clienteTO.getChavePrimaria() > 0) {
   dadosSolicitanteTO.setChavePrimariaCliente(clienteTO.getChavePrimaria());
  }
  Chamado chamado = this.popularChamado(dadosSolicitanteTO, null, Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO);
  if (clienteTO.getChavePrimaria() != null && clienteTO.getChavePrimaria() > 0) {
   chamado.setCliente(controladorCliente.obterCliente(clienteTO.getChavePrimaria(), new String[]{}));
  }
  chamadoAlteracaoCliente.setChamado(chamado);

  return chamadoAlteracaoCliente;

 }

 private void preencheEntidadeCamposSimplesClienteTO(ClienteTO clienteTO, ChamadoAlteracaoCliente chamadoAlteracaoCliente) {
	chamadoAlteracaoCliente.setNome(clienteTO.getNome());
	chamadoAlteracaoCliente.setNomeAbreviado(clienteTO.getNomeAbreviado());
	chamadoAlteracaoCliente.setEmailPrincipal(clienteTO.getEmailPrincipal());
	chamadoAlteracaoCliente.setEmailSecundario(clienteTO.getEmailSecundario());
	chamadoAlteracaoCliente.setCpf(clienteTO.getCpf());
	chamadoAlteracaoCliente.setRg(clienteTO.getRg());
	chamadoAlteracaoCliente.setNumeroPassaporte(clienteTO.getNumeroPassaporte());
	chamadoAlteracaoCliente.setNomeMae(clienteTO.getNomeMae());
	chamadoAlteracaoCliente.setNomePai(clienteTO.getNomePai());
	chamadoAlteracaoCliente.setNomeFantasia(clienteTO.getNomeFantasia());
	chamadoAlteracaoCliente.setCnpj(clienteTO.getCnpj());
	chamadoAlteracaoCliente.setInscricaoEstadual(clienteTO.getInscricaoEstadual());
	chamadoAlteracaoCliente.setInscricaoMunicipal(clienteTO.getInscricaoMunicipal());
	chamadoAlteracaoCliente.setInscricaoRural(clienteTO.getInscricaoRural());
	chamadoAlteracaoCliente.setRegimeRecolhimento(clienteTO.getRegimeRecolhimento());
 }

 /**
  * Popular chamado cliente anexo chamado.
  *
  * @param anexosTO the anexos to
  * @return the collection
  */
 private Collection<ChamadoClienteAnexo> popularChamadoClienteAnexoChamado(Collection<ClienteAnexoTO> anexosTO) {

  Collection<ChamadoClienteAnexo> listaChamadoClienteAnexo = null;
  listaChamadoClienteAnexo = new HashSet<ChamadoClienteAnexo>();

  for (ClienteAnexoTO clienteAnexoTO : anexosTO) {

   ChamadoClienteAnexo chamadoClienteAnexo = new ChamadoClienteAnexo();
   chamadoClienteAnexo.setAbaAnexo(this.popularEntidadeConteudo(clienteAnexoTO.getAbaAnexo()));
   chamadoClienteAnexo.setTipoAnexo(this.popularEntidadeConteudo(clienteAnexoTO.getTipoAnexo()));

   chamadoClienteAnexo.setTipoAcao(this.popularEntidadeConteudo(clienteAnexoTO.getTipoAcao()));
   chamadoClienteAnexo.setDescricaoAnexo(clienteAnexoTO.getDescricaoAnexo());
   chamadoClienteAnexo.setDocumentoAnexo(clienteAnexoTO.getDocumentoAnexo());
   if (clienteAnexoTO.getChavePrimaria() != null && clienteAnexoTO.getChavePrimaria() > 0) {
    chamadoClienteAnexo.setClienteAnexo(this.popularClienteAnexo(clienteAnexoTO.getChavePrimaria()));
   }
   chamadoClienteAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());
   chamadoClienteAnexo.setHabilitado(Boolean.TRUE);

   listaChamadoClienteAnexo.add(chamadoClienteAnexo);

  }
  return listaChamadoClienteAnexo;
 }

 /**
  * Popular chamado alteracao cliente enderecos chamado.
  *
  * @param enderecos the enderecos
  * @return the collection
  * @throws Exception the exception
  */
 private Collection<ChamadoAlteracaoClienteEndereco> popularChamadoAlteracaoClienteEnderecosChamado(
   Collection<ClienteEnderecoTO> enderecos) throws Exception {

  Collection<ChamadoAlteracaoClienteEndereco> listaChamadoAlteracaoClienteEndereco = null;
  listaChamadoAlteracaoClienteEndereco = new HashSet<ChamadoAlteracaoClienteEndereco>();

  for (ClienteEnderecoTO clienteEnderecoTO : enderecos) {

   ChamadoAlteracaoClienteEndereco chamadoAlteracaoClienteEndereco = new ChamadoAlteracaoClienteEndereco();
   chamadoAlteracaoClienteEndereco.setCaixaPostal(clienteEnderecoTO.getCaixaPostal());
   chamadoAlteracaoClienteEndereco.setCep(this.popularCep(clienteEnderecoTO.getCep()));

   chamadoAlteracaoClienteEndereco.setTipoAcao(this.popularEntidadeConteudo(clienteEnderecoTO.getTipoAcao()));
   chamadoAlteracaoClienteEndereco.setEnderecoReferencia(clienteEnderecoTO.getEnderecoReferencia());
   chamadoAlteracaoClienteEndereco.setNumero(clienteEnderecoTO.getNumeroEndereco());
   chamadoAlteracaoClienteEndereco.setTipoEndereco(this.popularTipoEndereco(clienteEnderecoTO.getTipoEndereco()));

   if (clienteEnderecoTO.getCep() != null) {

    String nomeMunicipio = StringUtils.trimToEmpty(clienteEnderecoTO.getCep().getNomeMunicipio());
    String uf = StringUtils.trimToEmpty(clienteEnderecoTO.getCep().getUf());
    Municipio municipio = controladorMunicipio.obterMunicipioPorNomeUF(nomeMunicipio, uf);

    if (municipio != null) {
     chamadoAlteracaoClienteEndereco.setMunicipio(municipio);
    } else {
     throw new NegocioException("Município não encontrado na base de dados.");
    }

   }

   if (clienteEnderecoTO.getChavePrimaria() != null && clienteEnderecoTO.getChavePrimaria() > 0) {
    chamadoAlteracaoClienteEndereco.setClienteEndereco(this.popularClienteEndereco(clienteEnderecoTO.getChavePrimaria()));
   }
   chamadoAlteracaoClienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());
   chamadoAlteracaoClienteEndereco.setHabilitado(Boolean.TRUE);

   listaChamadoAlteracaoClienteEndereco.add(chamadoAlteracaoClienteEndereco);

  }
  return listaChamadoAlteracaoClienteEndereco;
 }

 /**
  * Popular chamado alteracao cliente fone chamado.
  *
  * @param fones the fones
  * @return the collection
  */
 private Collection<ChamadoAlteracaoClienteTelefone> popularChamadoAlteracaoClienteFoneChamado(Collection<ClienteFoneTO> fones) {

  Collection<ChamadoAlteracaoClienteTelefone> listaChamadoAlteracaoClienteTelefone = null;
  listaChamadoAlteracaoClienteTelefone = new HashSet<ChamadoAlteracaoClienteTelefone>();

  for (ClienteFoneTO clienteFoneTO : fones) {

   ChamadoAlteracaoClienteTelefone chamadoAlteracaoClienteTelefone = new ChamadoAlteracaoClienteTelefone();
   chamadoAlteracaoClienteTelefone.setCodigoDDD(clienteFoneTO.getCodigoDDD());
   if (clienteFoneTO.isIndicadorPrincipal()) {
    chamadoAlteracaoClienteTelefone.setIndicadorPrincipal(Boolean.TRUE);
   } else {
    chamadoAlteracaoClienteTelefone.setIndicadorPrincipal(Boolean.FALSE);
   }

   chamadoAlteracaoClienteTelefone.setNumero(clienteFoneTO.getNumeroFone());
   chamadoAlteracaoClienteTelefone.setTipoAcao(this.popularEntidadeConteudo(clienteFoneTO.getTipoAcao()));
   chamadoAlteracaoClienteTelefone.setRamal(clienteFoneTO.getRamalFone());
   chamadoAlteracaoClienteTelefone.setTipoFone(this.popularTipoFone(clienteFoneTO.getTipoFone()));
   if (clienteFoneTO.getChavePrimariaTelefone() != null && clienteFoneTO.getChavePrimariaTelefone() > 0) {
    chamadoAlteracaoClienteTelefone.setClienteFone(this.popularClienteFone(clienteFoneTO.getChavePrimariaTelefone()));
   }
   chamadoAlteracaoClienteTelefone.setUltimaAlteracao(Calendar.getInstance().getTime());
   chamadoAlteracaoClienteTelefone.setHabilitado(Boolean.TRUE);

   listaChamadoAlteracaoClienteTelefone.add(chamadoAlteracaoClienteTelefone);

  }
  return listaChamadoAlteracaoClienteTelefone;
 }

 /**
  * Popular chamado alteracao cliente contato chamado.
  *
  * @param contatos the contatos
  * @return the collection
  */
 private Collection<ChamadoAlteracaoClienteContato> popularChamadoAlteracaoClienteContatoChamado(Collection<ClienteContatoTO> contatos) {

  Collection<ChamadoAlteracaoClienteContato> listaChamadoAlteracaoClienteContato = null;
  listaChamadoAlteracaoClienteContato = new HashSet<ChamadoAlteracaoClienteContato>();

  for (ClienteContatoTO clienteContatoTO : contatos) {

   ChamadoAlteracaoClienteContato chamadoAlteracaoClienteContato = new ChamadoAlteracaoClienteContato();

   chamadoAlteracaoClienteContato.setCodigoDDD(clienteContatoTO.getCodigoDDD());
   chamadoAlteracaoClienteContato.setDescricaoArea(clienteContatoTO.getDescricaoArea());
   chamadoAlteracaoClienteContato.setEmail(clienteContatoTO.getEmail());
   chamadoAlteracaoClienteContato.setFone(clienteContatoTO.getFoneContato());
   chamadoAlteracaoClienteContato.setNome(clienteContatoTO.getNomeContato());
   chamadoAlteracaoClienteContato.setTipoAcao(this.popularEntidadeConteudo(clienteContatoTO.getTipoAcao()));
   chamadoAlteracaoClienteContato.setPrincipal(clienteContatoTO.isPrincipal());
   chamadoAlteracaoClienteContato.setRamal(clienteContatoTO.getRamalContato());
   if (clienteContatoTO.getChavePrimaria() != null && clienteContatoTO.getChavePrimaria() > 0) {
    chamadoAlteracaoClienteContato.setContatoCliente(this.popularContatoCliente(clienteContatoTO.getChavePrimaria()));
   }
   chamadoAlteracaoClienteContato.setUltimaAlteracao(Calendar.getInstance().getTime());
   chamadoAlteracaoClienteContato.setHabilitado(Boolean.TRUE);

   if (clienteContatoTO.getProfissao() != null && clienteContatoTO.getProfissao().getChavePrimaria() != null) {
    chamadoAlteracaoClienteContato.setProfissao(this.popularProfissao(clienteContatoTO.getProfissao()));
   } else {
    chamadoAlteracaoClienteContato.setProfissao(null);
   }
   chamadoAlteracaoClienteContato.setTipoContato(this.popularTipoContato(clienteContatoTO.getTipoContato()));

   listaChamadoAlteracaoClienteContato.add(chamadoAlteracaoClienteContato);

  }

  return listaChamadoAlteracaoClienteContato;
 }

 /**
  * Popular entidade conteudo.
  *
  * @param tipoComboBoxTO the tipo combo box to
  * @return the entidade conteudo
  */
 private EntidadeConteudo popularEntidadeConteudo(TipoComboBoxTO tipoComboBoxTO) {

  EntidadeConteudo entidadeConteudo = null;
  if (tipoComboBoxTO != null && tipoComboBoxTO.getChavePrimaria() != null) {
   entidadeConteudo = (EntidadeConteudo) controladorCliente.criarTipoAcao();
   entidadeConteudo.setChavePrimaria(tipoComboBoxTO.getChavePrimaria());
  }

  return entidadeConteudo;

 }


 /**
  * Popular tipo contato.
  *
  * @param tipoComboBoxTO the tipo combo box to
  * @return the tipo contato
  */
 private TipoContato popularTipoContato(TipoComboBoxTO tipoComboBoxTO) {

  TipoContato tipoContato = (TipoContato) controladorCliente.criarTipoContato();
  tipoContato.setChavePrimaria(tipoComboBoxTO.getChavePrimaria());

  return tipoContato;

 }

 /**
  * Popular profissao.
  *
  * @param tipoComboBoxTO the tipo combo box to
  * @return the profissao
  */
 private Profissao popularProfissao(TipoComboBoxTO tipoComboBoxTO) {

  Profissao profissao = (Profissao) controladorCliente.criarProfissao();
  profissao.setChavePrimaria(tipoComboBoxTO.getChavePrimaria());

  return profissao;

 }

 /**
  * Popular contato cliente.
  *
  * @param chavePrimariaContatoCliente the chave primaria contato cliente
  * @return the contato cliente
  */
 private ContatoCliente popularContatoCliente(Long chavePrimariaContatoCliente) {

  ContatoCliente contatoCliente = (ContatoCliente) controladorCliente.criarContatoCliente();
  contatoCliente.setChavePrimaria(chavePrimariaContatoCliente);

  return contatoCliente;

 }

 /**
  * Popular cliente fone.
  *
  * @param chavePrimariaClienteFone the chave primaria cliente fone
  * @return the cliente fone
  */
 private ClienteFone popularClienteFone(Long chavePrimariaClienteFone) {

  ClienteFone clienteFone = (ClienteFone) controladorCliente.criarClienteFone();
  clienteFone.setChavePrimaria(chavePrimariaClienteFone);

  return clienteFone;

 }

 /**
  * Popular cliente anexo.
  *
  * @param chavePrimariaClienteAnexo the chave primaria cliente anexo
  * @return the cliente anexo
  */
 private ClienteAnexo popularClienteAnexo(Long chavePrimariaClienteAnexo) {

  ClienteAnexo clienteAnexo = (ClienteAnexo) controladorCliente.criarClienteAnexo();
  clienteAnexo.setChavePrimaria(chavePrimariaClienteAnexo);

  return clienteAnexo;

 }

 /**
  * Popular cliente endereco.
  *
  * @param chavePrimariaClienteEndereco the chave primaria cliente endereco
  * @return the cliente endereco
  */
 private ClienteEndereco popularClienteEndereco(Long chavePrimariaClienteEndereco) {

  ClienteEndereco clienteEndereco = (ClienteEndereco) controladorCliente.criarClienteEndereco();
  clienteEndereco.setChavePrimaria(chavePrimariaClienteEndereco);

  return clienteEndereco;

 }

 /**
  * Popular tipo endereco.
  *
  * @param tipoComboBoxTO the tipo combo box to
  * @return the tipo endereco
  */
 private TipoEndereco popularTipoEndereco(TipoComboBoxTO tipoComboBoxTO) {

  TipoEndereco tipoEndereco = (TipoEndereco) controladorCliente.criarTipoEndereco();
  tipoEndereco.setChavePrimaria(tipoComboBoxTO.getChavePrimaria());

  return tipoEndereco;

 }

 /**
  * Popular tipo fone.
  *
  * @param tipoComboBoxTO the tipo combo box to
  * @return the tipo fone
  */
 private TipoFone popularTipoFone(TipoComboBoxTO tipoComboBoxTO) {

  TipoFone tipoFone = (TipoFone) controladorCliente.criarTipoFone();
  tipoFone.setChavePrimaria(tipoComboBoxTO.getChavePrimaria());

  return tipoFone;

 }

 /**
  * Popular cep.
  *
  * @param cepTO the cep to
  * @return the cep
  */
 private Cep popularCep(CepTO cepTO) {

  Cep cep = (Cep) controladorCliente.criarCep();
  cep.setChavePrimaria(cepTO.getChavePrimaria());
  cep.setBairro(cepTO.getBairro());
  cep.setCep(cepTO.getCep());
  cep.setIntervaloNumeracao(cepTO.getIntervaloNumeracao());
  cep.setLogradouro(cepTO.getLogradouro());

  cep.setNomeMunicipio(cepTO.getNomeMunicipio());
  cep.setTipoLogradouro(cepTO.getTipoLogradouro());
  cep.setUf(cepTO.getUf());

  return cep;
 }

 /**
  * Consulta ponto consumo periodo agrupado.
  *
  * @param chavePrimaria the chave primaria
  * @param dataInicio1   the data inicio1
  * @param dataFim1      the data fim1
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultaPontoConsumoAgrupado")
 @RequestWrapper(localName = "consultaPontoConsumoAgrupadoRequest")
 @ResponseWrapper(localName = "consultaPontoConsumoAgrupadoResposta")
 public Collection<PontoConsumoAgrupadoTO> consultaPontoConsumoPeriodoAgrupado(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria,
                                                          @WebParam(name = "dataInicio") String dataInicio1,
                                                          @WebParam(name = "dataFim") String dataFim1) throws Exception {

  List<PontoConsumoAgrupadoTO> consumoAgrupadoTO = null;

  try {
   Date dataInicio = Util.converterCampoStringParaData("Agencia Virtual", dataInicio1, "dd/MM/yyyy");
   Date dataFim = Util.converterCampoStringParaData("Agencia Virtual", dataFim1, "dd/MM/yyyy");

   controladorDevolucao.validarDataDevolucaoInicialFinal(dataInicio1, dataFim1);

   List<Object> lista = (ArrayList<Object>) controladorHistoricoConsumo.consultarHistoricoAgrupadoAnoMes(chavePrimaria,
     dataInicio, dataFim);

   consumoAgrupadoTO = new ArrayList<PontoConsumoAgrupadoTO>();

   Object[] arrayObjeto = null;

   for (int i = 0; i < lista.size(); i++) {

    arrayObjeto = (Object[]) lista.get(i);

    Integer anoMes = (Integer) arrayObjeto[0];
    BigDecimal consumoInformado = (BigDecimal) arrayObjeto[1];
    BigDecimal consumoReal = (BigDecimal) arrayObjeto[2];

    PontoConsumoAgrupadoTO consumo = new PontoConsumoAgrupadoTO();

    consumo.setAnoMesLeitura(String.valueOf(Util.formatarAnoMes(anoMes)));
    consumo.setConsumoInformado(consumoInformado);
    consumo.setConsumoReal(consumoReal);

    consumoAgrupadoTO.add(consumo);
   }

  } catch (Exception e) {

   throw new Exception(MensagemUtil.gerarMensagemErro(e));
  }

  return consumoAgrupadoTO;
 }

 /**
  * Consulta ponto consumo descricao.
  *
  * @param chavePrimaria the chave primaria
  * @return the collection
  * @throws NegocioException {@link NegocioException}
  */
 @WebMethod(operationName = "consultaPontoConsumoDescricao")
 @RequestWrapper(localName = "consultaPontoConsumoDescricaoRequest")
 @ResponseWrapper(localName = "consultaPontoConsumoDescricaoResposta")
 public Collection<PontoConsumoDescricaoTO> consultaPontoConsumoDescricao(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria)
		 throws NegocioException {

  Collection<ContratoPontoConsumo> contratosPontoConsumo = controladorContrato.listarContratoPontoConsumo(chavePrimaria);

  List<PontoConsumoDescricaoTO> listaConsumoTO = new ArrayList<PontoConsumoDescricaoTO>();

  for (ContratoPontoConsumo cpc : contratosPontoConsumo) {
   PontoConsumoDescricaoTO consumoTO = new PontoConsumoDescricaoTO();
   consumoTO.setChavePrimaria(cpc.getPontoConsumo().getChavePrimaria());
   consumoTO.setDescricao(cpc.getPontoConsumo().getDescricao());
   consumoTO.setSituacao(cpc.getPontoConsumo().getSituacaoConsumo().getDescricao());
   listaConsumoTO.add(consumoTO);
  }

  return listaConsumoTO;
 }

	/**
	 * Consulta ponto consumo religacao interrupcao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param tipo          the tipo
	 * @return the collection
	 * @throws NegocioException the exception
	 */
	@WebMethod(operationName = "consultaPontoConsumoReligacaoInterrupcao")
	@RequestWrapper(localName = "consultaPontoConsumoReligacaoInterrupcaoRequest")
	@ResponseWrapper(localName = "consultaPontoConsumoReligacaoInterrupcaoResposta")
	public Collection<PontoConsumoDescricaoTO> consultaPontoConsumoReligacaoInterrupcao(
			@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria, @WebParam(name = "tipo") String tipo) throws NegocioException {

		Collection<ContratoPontoConsumo> contratosPontoConsumo;
		if ("religacao".equalsIgnoreCase(tipo)) {
			contratosPontoConsumo = controladorContrato.listarContratoPontoConsumoSituacaoInativo(chavePrimaria);
		} else {
			contratosPontoConsumo = controladorContrato.listarContratoPontoConsumoPorSituacao(chavePrimaria,
					asList(SituacaoConsumo.ATIVO.toUpperCase(), SituacaoConsumo.AGUARDANDO_ATIVACAO_DB.toUpperCase()));
		}

		return PontoConsumoDescricaoUtil.converter(contratosPontoConsumo);
	}

 /**
  * Consulta ponto consumo periodo.
  *
  * @param chavePrimaria the chave primaria
  * @param dataInicio1   the data inicio1
  * @param dataFim1      the data fim1
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultaPontoConsumoPeriodo")
 @RequestWrapper(localName = "consultaPontoConsumoPeriodoRequest")
 @ResponseWrapper(localName = "consultaPontoConsumoPeriodoResposta")
 public Collection<PontoConsumoDetalhadoTO> consultaPontoConsumoPeriodo(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria,
                                                                        @WebParam(name = "dataInicio") String dataInicio1,
                                                                        @WebParam(name = "dataFim") String dataFim1) throws Exception {

  List<PontoConsumoDetalhadoTO> consumoDetalhadoTO = new ArrayList<PontoConsumoDetalhadoTO>();
  try {

   Date dataInicio = Util.converterCampoStringParaData("Agencia Virtual", dataInicio1, "dd/MM/yyyy");
   Date dataFim = Util.converterCampoStringParaData("Agencia Virtual", dataFim1, "dd/MM/yyyy");

   Collection<HistoricoConsumo> listaConsumo = controladorHistoricoConsumo.consultarHistoricoMedicao(chavePrimaria, dataInicio,
     dataFim);

   for (HistoricoConsumo consumo : listaConsumo) {
    PontoConsumoDetalhadoTO pontoConsumoTO = new PontoConsumoDetalhadoTO();

    pontoConsumoTO.setAnoMesLeitura(consumo.getAnoMesFaturamentoFormatado());
    pontoConsumoTO.setNumeroLeituraInformada(consumo.getHistoricoAtual().getNumeroLeituraInformada());
    pontoConsumoTO.setConsumoInformado(consumo.getConsumo());
    pontoConsumoTO.setConsumoReal(consumo.getConsumoApurado());
    pontoConsumoTO.setFatorPTZCorretor(consumo.getHistoricoAtual().getFatorPTZCorretor());
    pontoConsumoTO.setFatorCorretor(consumo.getFatorCorrecao());

    consumoDetalhadoTO.add(pontoConsumoTO);

   }

  } catch (Exception e) {
   tratarMensagem(e);
  }
  return consumoDetalhadoTO;
 }

 /**
  * Gerar relatorio ponto consumo periodo.
  *
  * @param chavePrimariaContrato       the chave primaria contrato
  * @param chavesPrimariasPontoConsumo the chaves primarias ponto consumo
  * @param dataInicio1                 the data inicio1
  * @param dataFim1                    the data fim1
  * @param exibirConsolidado           the exibir consolidado
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "gerarRelatorioPontoConsumoPeriodo")
 @RequestWrapper(localName = "gerarRelatorioPontoConsumoPeriodoRequest")
 @ResponseWrapper(localName = "gerarRelatorioPontoConsumoPeriodoResposta")
 public byte[] gerarRelatorioPontoConsumoPeriodo(@WebParam(name = "chavePrimariaContrato") Long chavePrimariaContrato,
                                                 @WebParam(name = "chavesPrimariasPontoConsumo") String chavesPrimariasPontoConsumo,
                                                 @WebParam(name = "dataInicio") String dataInicio1,
                                                 @WebParam(name = "dataFim") String dataFim1,
                                                 @WebParam(name = "exibirConsolidado") boolean exibirConsolidado) throws Exception {

  String[] chaves = chavesPrimariasPontoConsumo.split(",");

  Long[] chavesPri = new Long[chaves.length];
  for (int i = 0; i < chaves.length; i++) {
   if (!chaves[i].isEmpty()) {
    chavesPri[i] = Long.valueOf(chaves[i]);
   }
  }

  byte[] relatorio = null;
  Map<String, Object> parametros = new HashMap<String, Object>();
  List<PontoConsumoVO> listaPontoConsumo = new ArrayList<PontoConsumoVO>();
  Date dataInicio = Util.converterCampoStringParaData("Agencia Virtual", dataInicio1, "dd/MM/yyyy");
  Date dataFim = Util.converterCampoStringParaData("Agencia Virtual", dataFim1, "dd/MM/yyyy");
  for (int i = 0; i < chavesPri.length; i++) {
   Collection<ContratoPontoConsumo> contratosPontoConsumo = controladorContrato.listarContratoPontoConsumoPorPonto(chavesPri[i],
     chavePrimariaContrato);
   for (ContratoPontoConsumo cpc : contratosPontoConsumo) {
    PontoConsumoVO pontoConsumoVO = new PontoConsumoVO();
    pontoConsumoVO.setDescricao(cpc.getPontoConsumo().getDescricao());
    pontoConsumoVO.setSituacao(cpc.getPontoConsumo().getSituacaoConsumo().getDescricao());
    pontoConsumoVO.setListaHistoricoConsumoPeriodo(popularPontoConsumoHistoricoVO((List<HistoricoConsumo>) controladorHistoricoConsumo
      .consultarHistoricoMedicao(cpc.getPontoConsumo().getChavePrimaria(), dataInicio, dataFim)));
    listaPontoConsumo.add(pontoConsumoVO);
   }
  }
  List<Object> lista = (ArrayList<Object>) controladorHistoricoConsumo.consultarHistoricoAgrupadoAnoMes(chavePrimariaContrato,
    dataInicio, dataFim);
  parametros.put("exibirConsolidado", exibirConsolidado);
  Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
  parametros.put("periodo", dataInicio1 + " a " + dataFim1);
  parametros.put("nomeEmpresa", empresa.getCliente().getNome());
  parametros.put("enderecoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoLogradouro());
  parametros.put("cepEmpresa", empresa.getCliente().getEnderecoPrincipal().getCep().getCep());
  parametros.put("cidadeEstadoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
  Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
  parametros.put("logo", logo);
  Contrato contrato = (Contrato) controladorContrato.obter(chavePrimariaContrato);
  Cliente cliente = contrato.getClienteAssinatura();
  parametros.put("nomeRazao", cliente.getNome());
  String cpfCnpj = "";
  if (cliente.getCpfFormatado() != null) {
   cpfCnpj = cliente.getCpfFormatado();
  } else {
   cpfCnpj = cliente.getCnpjFormatado();
  }
  parametros.put("cpfCnpj", cpfCnpj);
  parametros.put("enderecoCliente", cliente.getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
  parametros.put("cepCliente", cliente.getEnderecoPrincipal().getCep().getCep());
  parametros.put("bairroCliente", cliente.getEnderecoPrincipal().getEnderecoFormatadoBairro());
  parametros.put("municipioCliente", cliente.getEnderecoPrincipal().getMunicipio().getDescricao());
  parametros.put("ufCliente", cliente.getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getSigla());
  parametros.put("listaPontoConsumo", listaPontoConsumo);

  if (lista != null && !lista.isEmpty()) {
   relatorio = RelatorioUtil.gerarRelatorioPDF(popularPontoConsumoAgrupadoVO(lista), parametros, "relatorioConsumoPeriodo.jasper");
  } else {
   relatorio = null;
  }

  return relatorio;
 }

 /**
  * Popular ponto consumo historico vo.
  *
  * @param lista the lista
  * @return the list
  */
 private List<PontoConsumoDetalhadoVO> popularPontoConsumoHistoricoVO(List<HistoricoConsumo> lista) {

  List<PontoConsumoDetalhadoVO> consumoDetalhadoTO = new ArrayList<PontoConsumoDetalhadoVO>();
  for (HistoricoConsumo consumo : lista) {
   PontoConsumoDetalhadoVO pontoConsumoTO = new PontoConsumoDetalhadoVO();
   pontoConsumoTO.setAnoMesLeitura(consumo.getAnoMesFaturamentoFormatado());
   pontoConsumoTO.setNumeroLeituraInformada(consumo.getHistoricoAtual().getNumeroLeituraInformada());
   pontoConsumoTO.setConsumoInformado(consumo.getConsumo());
   pontoConsumoTO.setConsumoReal(consumo.getConsumoApurado());
   pontoConsumoTO.setFatorPTZCorretor(consumo.getHistoricoAtual().getFatorPTZCorretor());
   consumoDetalhadoTO.add(pontoConsumoTO);

  }

  return consumoDetalhadoTO;
 }

 /**
  * Popular ponto consumo agrupado vo.
  *
  * @param lista the lista
  * @return the list
  */
 private List<PontoConsumoAgrupadoVO> popularPontoConsumoAgrupadoVO(List<Object> lista) {

  List<PontoConsumoAgrupadoVO> consumoAgrupadoVOList = new ArrayList<PontoConsumoAgrupadoVO>();

  Object[] arrayObjeto = null;

  for (int i = 0; i < lista.size(); i++) {

   arrayObjeto = (Object[]) lista.get(i);

   Integer anoMes = (Integer) arrayObjeto[0];
   BigDecimal consumoInformado = (BigDecimal) arrayObjeto[1];
   BigDecimal consumoReal = (BigDecimal) arrayObjeto[2];
   BigDecimal numeroLeituraInformada = (BigDecimal) arrayObjeto[3];
   Double fatorPTZCorretor = (Double) arrayObjeto[4];

   PontoConsumoAgrupadoVO consumo = new PontoConsumoAgrupadoVO();

   consumo.setAnoMesLeitura(String.valueOf(Util.formatarAnoMes(anoMes)));
   consumo.setConsumoInformado(consumoInformado);
   consumo.setConsumoReal(consumoReal);
   consumo.setLeituraInformada(numeroLeituraInformada);
   consumo.setFatorCorrecao(fatorPTZCorretor);
   consumoAgrupadoVOList.add(consumo);
  }
  return consumoAgrupadoVOList;

 }

 /**
  * Consultar natureza imovel.
  *
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarNaturezaImovel")
 @RequestWrapper(localName = "consultarNaturezaImovelRequest")
 @ResponseWrapper(localName = "consultarNaturezaImovelResposta")
 public Collection<TipoComboBoxTO> consultarNaturezaImovel() throws Exception {

  Collection<TipoComboBoxTO> listaNaturezaImovelTO = new ArrayList<TipoComboBoxTO>();
  Collection<Segmento> listaSegmento = null;

  try {
   listaSegmento = controladorSegmento.listarSegmento();
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new java.lang.Exception(e.getMessage(), e);
  }

  if (listaSegmento != null && !listaSegmento.isEmpty()) {
   for (Segmento segmento : listaSegmento) {

    TipoComboBoxTO naturezaImovelTO = this.popularListaTipoComboBoxTO(segmento.getDescricao(), segmento.getChavePrimaria());
    listaNaturezaImovelTO.add(naturezaImovelTO);
   }
  }

  return listaNaturezaImovelTO;
 }

 /**
  * Popular dados imovel solicitante.
  *
  * @param dadosSolicitanteTO the dados solicitante to
  * @return the string builder
  * @throws NegocioException the negocio exception
  */
 @Transactional
 private StringBuilder popularDadosImovelSolicitante(DadosSolicitanteTO dadosSolicitanteTO) throws NegocioException {

  StringBuilder dadosImovel = new StringBuilder();
  Segmento segmento = null;

  if (dadosSolicitanteTO.getData() != null && !dadosSolicitanteTO.getData().isEmpty()) {
   dadosImovel.append("Data: ");
   dadosImovel.append(dadosSolicitanteTO.getData());
  }

  if (dadosSolicitanteTO.getTipoComunicado() != null && !dadosSolicitanteTO.getTipoComunicado().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append("Tipo de Comunicado: ");
   dadosImovel.append(dadosSolicitanteTO.getTipoComunicado());
  }

  if (dadosSolicitanteTO.getLogradouro() != null && !dadosSolicitanteTO.getLogradouro().isEmpty()) {
   dadosImovel.append("Endereço: ");
   dadosImovel.append(dadosSolicitanteTO.getLogradouro());
  }

  if (dadosSolicitanteTO.getNumeroImovel() != null && !dadosSolicitanteTO.getNumeroImovel().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append(dadosSolicitanteTO.getNumeroImovel());
  }

  if (dadosSolicitanteTO.getComplementoImovel() != null && !dadosSolicitanteTO.getComplementoImovel().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append(dadosSolicitanteTO.getComplementoImovel());
  }

  if (dadosSolicitanteTO.getCep() != null && !dadosSolicitanteTO.getCep().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append("CEP: ");
   dadosImovel.append(dadosSolicitanteTO.getCep());
  }

  if (dadosSolicitanteTO.getPontoReferenciaImovel() != null && !dadosSolicitanteTO.getPontoReferenciaImovel().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append(dadosSolicitanteTO.getPontoReferenciaImovel());
  }

  if (dadosSolicitanteTO.getNaturezaImovel() != null) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   segmento = (Segmento) controladorSegmento.obter(dadosSolicitanteTO.getNaturezaImovel());
   dadosImovel.append("Segmento: ").append(segmento.getDescricao());
  }

  if (dadosSolicitanteTO.getNomeImovel() != null && !dadosSolicitanteTO.getNomeImovel().isEmpty()) {
   dadosImovel.append(Constantes.STRING_VIRGULA_ESPACO);
   dadosImovel.append(" Nome do Imovel : ");
   dadosImovel.append(dadosSolicitanteTO.getNomeImovel());
  }

  return dadosImovel;
 }

	/**
	 * Popular chamado.
	 *
	 * @param dadosSolicitanteTO    the dados solicitante to
	 * @param dadosImovel           the dados imovel
	 * @param unidadeOrganizacional the unidade organizacional
	 * @param assunto               the assunto
	 * @return the chamado
	 * @throws Exception the exception
	 */
	@Transactional
	private Chamado popularChamado(DadosSolicitanteTO dadosSolicitanteTO, StringBuilder dadosImovel, String assunto) throws Exception {

		Chamado chamado = new Chamado();
		if (dadosSolicitanteTO.getChavePrimariaPontoConsumo() != null && dadosSolicitanteTO.getChavePrimariaPontoConsumo() > 0) {

			PontoConsumo pontoconsumo = controladorPontoConsumo.buscarPontoConsumo(dadosSolicitanteTO.getChavePrimariaPontoConsumo());
			chamado.setImovel(pontoconsumo.getImovel());
			chamado.setPontoConsumo(pontoconsumo);

			chamado.setContrato((Contrato) controladorContrato.obter(dadosSolicitanteTO.getChavePrimariaContrato()));

			chamado.setCliente(controladorContrato.consultarClientePorPontoConsumo(dadosSolicitanteTO.getChavePrimariaPontoConsumo(),
					dadosSolicitanteTO.getChavePrimariaContrato()));

		}

		if (dadosSolicitanteTO.getChavePrimariaContrato() != null && dadosSolicitanteTO.getChavePrimariaContrato() > 0) {
			chamado.setContrato((Contrato) controladorContrato.obter(dadosSolicitanteTO.getChavePrimariaContrato()));
		}

		if (dadosSolicitanteTO.getChavePrimariaCliente() != null && dadosSolicitanteTO.getChavePrimariaCliente() > 0) {
			chamado.setCliente(controladorCliente.obterCliente(dadosSolicitanteTO.getChavePrimariaCliente()));
		}

		chamado.setNomeSolicitante(dadosSolicitanteTO.getNomeSolicitante());
		chamado.setCpfCnpjSolicitante(dadosSolicitanteTO.getCpfCnpjSolicitante());
		chamado.setRgSolicitante(dadosSolicitanteTO.getRgSolicitante());
		chamado.setTelefoneSolicitante(Util.coalescenciaNula(dadosSolicitanteTO.getDddTelefone(), StringUtils.EMPTY)
				+ dadosSolicitanteTO.getTelefoneSolicitante());
		chamado.setEmailSolicitante(dadosSolicitanteTO.getEmailSolicitante());
		if (chamado.getTelefoneSolicitante() != null) {
			chamado.setDescricao("Telefone de Contato: " + chamado.getTelefoneSolicitante());
		}
		if (dadosSolicitanteTO.getDddTelefoneSecundario() != null && dadosSolicitanteTO.getTelefoneSecundario() != null) {
			chamado.setDescricao(chamado.getDescricao() + "\r\n" + dadosSolicitanteTO.getDddTelefoneSecundario() + "-"
					+ dadosSolicitanteTO.getTelefoneSecundario());
		}

		if (dadosImovel != null) {
			chamado.setDescricao(dadosSolicitanteTO.getObservacao() + "\r\n----------------------\r\n"
					+ chamado.getDescricao()  + "\r\n" + dadosImovel.toString());
		} else {
			chamado.setDescricao(dadosSolicitanteTO.getObservacao() + "\r\n----------------------\r\n" + chamado.getDescricao());
		}

		chamado.setHabilitado(Boolean.TRUE);

		chamado.setProtocolo(controladorChamado.gerarProtocolo(null));
		ConstanteSistema usuario = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_USUARIO_AGENCIA_VIRTUAL);
		chamado.setUsuarioResponsavel(controladorUsuario.obterUsuario(Long.parseLong(usuario.getValor())));

		ConstanteSistema canalAtendimento = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_CANAL_ATENDIMENTO_AGENCIA_VIRTUAL);
		chamado.setCanalAtendimento((CanalAtendimento) controladorTabelaAuxiliar.obter(Long.parseLong(canalAtendimento.getValor()),
				CanalAtendimentoImpl.class, Boolean.TRUE));

		if (assunto != null && !assunto.isEmpty()) {
			ConstanteSistema constanteChamado = controladorConstanteSistema.obterConstantePorCodigo(assunto);
			ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(Long.valueOf(constanteChamado.getValor()));
			chamado.setChamadoAssunto(chamadoAssunto);
			chamado.setChamadoTipo(chamadoAssunto.getChamadoTipo());
			chamado.setDataPrevisaoEncerramento(controladorChamado.obterDataPrevisao(chamadoAssunto));

			chamado.getChamadoAssunto().setIndicadorImovelObrigatorio(Boolean.FALSE);

			if (Constantes.C_CHAMADO_ASSUNTO_AGV_SOLICITAR_LIGACAO.equals(assunto)) {
				chamado.getChamadoAssunto().setIndicadorClienteObrigatorio(Boolean.FALSE);
			}

			chamado.setUnidadeOrganizacional(chamadoAssunto.getUnidadeOrganizacional());
		}

		Collection<ClienteAnexoTO> anexos = dadosSolicitanteTO.getAnexos();

		if (anexos != null) {
			for (ClienteAnexoTO clienteAnexoTO : anexos) {
				byte[] documentoAnexo = clienteAnexoTO.getDocumentoAnexo();
				String descricaoAnexo = clienteAnexoTO.getDescricaoAnexo();
				ChamadoHistoricoAnexo chamadoHistoricoAnexo = new ChamadoHistoricoAnexo();
				chamadoHistoricoAnexo.setUltimaAlteracao(new Date());
				chamadoHistoricoAnexo.setDescricaoAnexo(descricaoAnexo);
				chamadoHistoricoAnexo.setDocumentoAnexo(documentoAnexo);
				chamadoHistoricoAnexo.setNomeArquivo(ServicoUtil.truncarNomeArquivo(clienteAnexoTO.getNomeArquivo(),
						NUMERO_MAX_CARACTERES_ANEXO));
				chamado.getListaAnexos().add(chamadoHistoricoAnexo);
			}
		}

		controladorChamado.inserirChamado(chamado, Boolean.FALSE, null, new HashMap<String, Object>(), null);


		enviarProtocolo(chamado);
		return chamado;

	}

 /**
  * Solicitar interrupcao religacao gas.
  *
  * @param dadosSolicitanteTO the dados solicitante to
  * @param solicitacao        the solicitacao
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "solicitarInterrupcaoReligacaoGas")
 @RequestWrapper(localName = "solicitarInterrupcaoReligacaoGasRequest")
 @ResponseWrapper(localName = "solicitarInterrupcaoReligacaoGasResposta")
 public Long solicitarInterrupcaoReligacaoGas(@WebParam(name = "dadosSolicitanteTO") DadosSolicitanteTO dadosSolicitanteTO,
                                              @WebParam(name = "solicitacao") String solicitacao) throws Exception {

  Chamado chamado = null;

  try {

   String constanteChamadoAssunto = null;

   if (solicitacao != null && !solicitacao.isEmpty() && "religacao".equalsIgnoreCase(solicitacao)) {
    constanteChamadoAssunto = Constantes.C_CHAMADO_ASSUNTO_AGV_SOLICITAR_RELIGACAO;
   } else {
    constanteChamadoAssunto = Constantes.C_CHAMADO_ASSUNTO_AGV_SOLICITAR_INTERRUPCAO;
   }

   StringBuilder dadosImovel = this.popularDadosImovelSolicitante(dadosSolicitanteTO);
   chamado = this.popularChamado(dadosSolicitanteTO, dadosImovel, constanteChamadoAssunto);

  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   this.tratarMensagem(e);
  } catch (GGASException e) {
   return tratarMensagem(e);
  }

  return chamado.getProtocolo().getNumeroProtocolo();

 }

 /**
  * Solicitacao ligacao gas.
  *
  * @param dadosSolicitanteTO the dados solicitante to
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "solicitacaoLigacaoGas")
 @RequestWrapper(localName = "solicitacaoLigacaoGasRequest")
 @ResponseWrapper(localName = "solicitacaoLigacaoGasResposta")
 public Long solicitacaoLigacaoGas(@WebParam(name = "dadosSolicitanteTO") DadosSolicitanteTO dadosSolicitanteTO) throws Exception {

  Chamado chamado = null;
  try {
   List<PontoConsumo> lista = null;
   lista = controladorPontoConsumo.listarPontoConsumoPorCliente(dadosSolicitanteTO.getCpfCnpjSolicitante());
   if (lista != null && !lista.isEmpty()) {
    throw new Exception("Já existe um ponto de consumo cadastrado e ativo para o CPF/CNPJ informado.");
   }

   StringBuilder dadosImovel = this.popularDadosImovelSolicitante(dadosSolicitanteTO);
   chamado = this.popularChamado(dadosSolicitanteTO, dadosImovel, Constantes.C_CHAMADO_ASSUNTO_AGV_SOLICITAR_LIGACAO);

  } catch (NegocioException ne) {
   return this.tratarMensagem(ne);
  } catch (GGASException e) {
   LOG.error(e.getMessage(), e);
   throw new java.lang.Exception(e.getMessage().toString());
  }

  return chamado.getProtocolo().getNumeroProtocolo();
 }

 /**
  * Popular dados solicitante to.
  *
  * @param clienteTO the cliente to
  * @param descricao the descricao
  * @return the dados solicitante to
  */
 private DadosSolicitanteTO popularDadosSolicitanteTO(ClienteTO clienteTO, String descricao) {

  DadosSolicitanteTO dadosSolicitanteTO = new DadosSolicitanteTO();
  dadosSolicitanteTO.setNomeSolicitante(clienteTO.getNome());

  if (clienteTO.getCpf() != null && !clienteTO.getCpf().isEmpty()) {
   dadosSolicitanteTO.setCpfCnpjSolicitante(clienteTO.getCpf());
  } else {
   dadosSolicitanteTO.setCpfCnpjSolicitante(clienteTO.getCnpj());
  }

  dadosSolicitanteTO.setRgSolicitante(clienteTO.getRg());

  if (clienteTO.getFonesTO() != null && !clienteTO.getFonesTO().isEmpty()) {

   ClienteFoneTO clienteFoneTO = clienteTO.getFonesTO().iterator().next();
   dadosSolicitanteTO.setTelefoneSolicitante(clienteFoneTO.getCodigoDDD() + "-" + clienteFoneTO.getNumeroFone());
  }

  if (clienteTO.getEmailPrincipal() != null) {
   dadosSolicitanteTO.setEmailSolicitante(clienteTO.getEmailPrincipal());
  } else {
   dadosSolicitanteTO.setEmailSolicitante(clienteTO.getEmailSecundario());
  }

  if (!CollectionUtils.isEmpty(clienteTO.getAnexosTO())) {
  	dadosSolicitanteTO.setAnexos(new ArrayList<>(clienteTO.getAnexosTO()));
  }

  dadosSolicitanteTO.setObservacao(descricao);

  return dadosSolicitanteTO;
 }

 /**
  * Fale conosco.
  *
  * @param dadosSolicitanteTO the dados solicitante to
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "faleConosco")
 @RequestWrapper(localName = "faleConoscoRequest")
 @ResponseWrapper(localName = "faleConoscoResposta")
 public Long faleConosco(@WebParam(name = "dadosSolicitanteTO") DadosSolicitanteTO dadosSolicitanteTO) throws Exception {

  Chamado chamado = null;

  try {
   StringBuilder dadosImovel = this.popularDadosImovelSolicitante(dadosSolicitanteTO);
   chamado = this.popularChamado(dadosSolicitanteTO, dadosImovel, Constantes.C_CHAMADO_ASSUNTO_AGV_FALE_CONOSCO);

  } catch (GGASException e) {

   return tratarMensagem(e);

  }

  return chamado.getProtocolo().getNumeroProtocolo();
 }

 /**
  * Consultar tipo comunicado.
  *
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarTipoComunicado")
 @RequestWrapper(localName = "consultarTipoComunicadoRequest")
 @ResponseWrapper(localName = "consultarTipoComunicadoResposta")
 public Collection<TipoComboBoxTO> consultarTipoComunicado() throws Exception {

  Collection<TipoComboBoxTO> listaTipoComunicadoTO = new ArrayList<TipoComboBoxTO>();
  Collection<EntidadeConteudo> listaEntidadeConteudo = null;

  try {
   ConstanteSistema constanteSistema = controladorConstanteSistema
     .obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_TIPO_FALE_CONOSCO);
   listaEntidadeConteudo = controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new java.lang.Exception(e.getMessage());
  }

  if (listaEntidadeConteudo != null && !listaEntidadeConteudo.isEmpty()) {
   for (EntidadeConteudo entidade : listaEntidadeConteudo) {

    TipoComboBoxTO tipoComunicadoTO = this.popularListaTipoComboBoxTO(entidade.getDescricao(), entidade.getChavePrimaria());
    listaTipoComunicadoTO.add(tipoComunicadoTO);
   }
  }

  return listaTipoComunicadoTO;
 }

 /**
  * Salvar alteracao data vencimento.
  *
  * @param listaPontoConsumoTO the lista ponto consumo to
  * @param chaveContrato       the chave contrato
  * @param descricao       descricao
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "salvarAlteracaoDataVencimento")
 @RequestWrapper(localName = "salvarAlteracaoDataVencimentoRequest")
 @ResponseWrapper(localName = "salvarAlteracaoDataVencimentoResposta")
 public Long salvarAlteracaoDataVencimento(@WebParam(name = "listaPontoConsumoTO") List<PontoConsumoTO> listaPontoConsumoTO,
                                           @WebParam(name = "chaveContrato") Long chaveContrato,
                                           @WebParam(name = "descricao") String descricao) throws Exception {

  String alteraVencimentoAutomatico = controladorParametroSistema.obterParametroAlteraVencimentoFaturaAutomatico().getValor();

  String alteraVencimentoComDebitoEmAtrazo = controladorParametroSistema.obterParametroAlteraVencimentoFaturaDebitoAtraso()
    .getValor();

  int prazoAlteraVencimentoFatura = controladorParametroSistema.obterParametroPrazoAlteraVencimentoFaturaAutomatico()
    .getValorInteger();

  Contrato contrato = (Contrato) controladorContrato.obter(chaveContrato);
  ClienteTO clienteTO = popularClienteTO(contrato.getClienteAssinatura());

  Date dataAtual = Calendar.getInstance().getTime();
  int mesAtual = dataAtual.getMonth() + 1;
  int anoAtual = dataAtual.getYear();
  int diaAtual = dataAtual.getDay();
  for (PontoConsumoTO pontoConsumoTO : listaPontoConsumoTO) {

   ChamadoAlteracaoVencimento chamadoAlteracaoVencimento = controladorChamado.obterUltimoChamadoAlteracaoVencimento(pontoConsumoTO
     .getChavePrimaria());
   if (chamadoAlteracaoVencimento != null) {
    int mesUltimaAlteracao = chamadoAlteracaoVencimento.getUltimaAlteracao().getMonth() + 1;
    int anoUltimaAlteracao = chamadoAlteracaoVencimento.getUltimaAlteracao().getYear();
    int diaUltimaAlteracao = chamadoAlteracaoVencimento.getUltimaAlteracao().getDay();
    int resultado = mesUltimaAlteracao - mesAtual;
    if ((prazoAlteraVencimentoFatura >= resultado || resultado == 0) && (anoAtual == anoUltimaAlteracao && resultado == 0)
      || (diaUltimaAlteracao >= diaAtual && prazoAlteraVencimentoFatura == resultado)
      || prazoAlteraVencimentoFatura > resultado) {

     throw new Exception("Sua  última alteração de vencimento foi "
       + new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR).format(chamadoAlteracaoVencimento.getUltimaAlteracao())
       + " a próxima alteração só é permitida nos próximos " + prazoAlteraVencimentoFatura + " meses");
    }
   }

   if ("2".equalsIgnoreCase(alteraVencimentoComDebitoEmAtrazo)) {
    Map<String, Object> filtro = new HashMap<String, Object>();
    filtro.put("idCliente", clienteTO.getChavePrimaria());
    filtro.put("idPontoConsumo", pontoConsumoTO.getChavePrimaria());
    Collection<Fatura> listaFatura = fachadaAtributo.consultarFaturasVencidasPontoConsumo(filtro, null);

    if (listaFatura != null && !listaFatura.isEmpty()) {
     throw new Exception("Não é possível alterar a data de vencimento, visto que existem débitos em aberto");
    }

   }
  }

  DadosSolicitanteTO dadosSolicitanteTO = popularDadosSolicitanteTO(clienteTO,
		  ServicoUtil.formatarDescricaoChamadoAgencia(clienteTO.getNome(), descricao, "Alteração Data de Vencimento"));
  StringBuilder dadosImovel = popularDadosImovelSolicitante(dadosSolicitanteTO);

  dadosSolicitanteTO.setChavePrimariaCliente(clienteTO.getChavePrimaria());
  Chamado chamado = popularChamado(dadosSolicitanteTO, dadosImovel, Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_VENCIMENTO);
  chamado.setContrato(contrato);
  List<Long> listIdPontoConsumo = new ArrayList<Long>();
  for (PontoConsumoTO pontoConsumoTO : listaPontoConsumoTO) {
   ChamadoAlteracaoVencimento chamadoAlteracaoVencimento = new ChamadoAlteracaoVencimento();
   chamadoAlteracaoVencimento.setChamado(chamado);
   chamadoAlteracaoVencimento.setPontoConsumo((PontoConsumo) controladorPontoConsumo.obter(pontoConsumoTO.getChavePrimaria()));
   chamadoAlteracaoVencimento.setUltimaAlteracao(Calendar.getInstance().getTime());
   chamadoAlteracaoVencimento.setDiaSolicitado(pontoConsumoTO.getVencimentoSolicitado());
   chamadoAlteracaoVencimento.setDiaVigente(pontoConsumoTO.getVencimentoVigente());
   controladorChamado.inserirChamadoAlteracaoVencimento(chamadoAlteracaoVencimento);
   listIdPontoConsumo.add(pontoConsumoTO.getChavePrimaria());
  }

  if ("1".equals(alteraVencimentoAutomatico)) {

   for (Long idPontoConsumo : listIdPontoConsumo) {

    ChamadoAlteracaoVencimento chamadoAlteracaoVencimento = controladorChamado.obterChamadoAlteracaoVencimento(
      chamado.getChavePrimaria(), idPontoConsumo);

    Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));
    ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumo(chamado.getContrato(),
      chamadoAlteracaoVencimento.getPontoConsumo());

    if (contratoPontoConsumo != null) {

     contrato.getContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria())
       .getContratoPontoConsumoItemFaturamento(itemFaturaGas)
       .setNumeroDiaVencimento(chamadoAlteracaoVencimento.getDiaSolicitado());

     controladorContrato.atualizar(contrato, ContratoPontoConsumoItemFaturamentoImpl.class);
    }

   }

   controladorChamado.encerrarChamado(chamado, new HashMap<String, Object>());
  }
  return chamado.getProtocolo().getNumeroProtocolo();
 }

 /**
  * Imprimir arquivo nfe.
  *
  * @param chaveNfe    the chave nfe
  * @param tipoArquivo the tipo arquivo
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "imprimirArquivoNfe")
 @RequestWrapper(localName = "imprimirArquivoNfeRequest")
 @ResponseWrapper(localName = "imprimirArquivoNfeResposta")
 public byte[] imprimirArquivoNfe(@WebParam(name = "chaveNfe") Long chaveNfe, @WebParam(name = "tipoArquivo") String tipoArquivo)
   throws Exception {

  Nfe nfe = (Nfe) controladorNfe.obter(chaveNfe);

  byte[] arquivoNfe = null;

  if ("XML".equals(tipoArquivo)) {
   arquivoNfe = nfe.getDsArquivoXml();
  } else {
   arquivoNfe = nfe.getDanfePdf();
  }

  return arquivoNfe;
 }

 /**
  * Imprimir boleto.
  *
  * @param numeroDocumentoFiscal the numero documento fiscal
  * @param numeroSerie           the numero serie
  * @param chaveContrato         the chave contrato
  * @param tipoArquivo           the tipo arquivo
  * @param chaveNotaDebito       the chave nota debito
  * @param vencimentoData        the data vencimento
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "imprimirBoleto")
 @RequestWrapper(localName = "imprimirBoletoRequest")
 @ResponseWrapper(localName = "imprimirBoletoResposta")
 public byte[] imprimirBoleto(@WebParam(name = "numeroDocumentoFiscal") Long numeroDocumentoFiscal,
                              @WebParam(name = "numeroSerie") String numeroSerie, @WebParam(name = "chaveContrato") Long chaveContrato,
                              @WebParam(name = "tipoArquivo") String tipoArquivo, @WebParam(name = "chaveNotaDebito") Long chaveNotaDebito,
                              @WebParam(name = "dataVencimento") String vencimentoData) throws Exception {

  String dataVencimento = vencimentoData;
  byte[] arquivo = null;
  if ("BOLETONotaDebito".equals(tipoArquivo)) {
   Fatura notaDebito = (Fatura) controladorFatura.obter(chaveNotaDebito);

   Parcelamento parcelamento = notaDebito.getParcelamento();
   if (parcelamento != null) {
    dataVencimento = Util.converterDataParaStringSemHora(new Date(), Constantes.FORMATO_DATA_BR);
   }

   try {
    arquivo = controladorFatura.gerarBoletoBancarioPadrao(notaDebito, Boolean.TRUE,
      Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO, dataVencimento);
   } catch (GGASException e) {
    tratarMensagem(e);
   }
  } else {
   DocumentoFiscal documentoFiscal = controladorFatura.obterDocumentoFiscalPorNumeroeSerie(numeroDocumentoFiscal, numeroSerie);

   if (documentoFiscal == null) {
    return null;
   }
   try {

    if ("BOLETO".equals(tipoArquivo)) {
     arquivo = controladorFatura.gerarBoletoBancarioPadrao(documentoFiscal.getFatura(), Boolean.TRUE, null,
       dataVencimento);
    } else {
     arquivo = controladorFatura.gerarEmissaoFaturaComLayout(documentoFiscal.getFatura(), null, null, null, null, null,
       BigDecimal.ZERO, documentoFiscal);
    }

   } catch (GGASException e) {
    tratarMensagem(e);
   }
  }
  return arquivo;
 }

 /**
  * Consultar lista situacao pagamento.
  *
  * @return the list
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarListaSituacaoPagamento")
 @RequestWrapper(localName = "consultarListaSituacaoPagamentoRequest")
 @ResponseWrapper(localName = "consultarListaSituacaoPagamentoResposta")
 public List<EntidadeConteudoTO> consultarListaSituacaoPagamento() throws Exception {

  ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

  List<EntidadeConteudo> listaEntidadeConteudo = (List<EntidadeConteudo>) controladorEntidadeConteudo
    .listarEntidadeConteudo(constante.getClasse());

  List<EntidadeConteudoTO> listaEntidadeConteudoTO = new ArrayList<EntidadeConteudoTO>();

  for (EntidadeConteudo entidadeConteudo : listaEntidadeConteudo) {
   EntidadeConteudoTO entidadeTO = new EntidadeConteudoTO();
   entidadeTO.setChavePrimaria(entidadeConteudo.getChavePrimaria());
   entidadeTO.setDescricao(entidadeConteudo.getDescricao());
   listaEntidadeConteudoTO.add(entidadeTO);
  }

  return listaEntidadeConteudoTO;

 }

 /**
  * Consultar contrato.
  *
  * @param numeroContrato the numero contrato
  * @return the contrato to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarContrato")
 @RequestWrapper(localName = "consultarContratoRequest")
 @ResponseWrapper(localName = "consultarContratoResposta")
 public ContratoTO consultarContrato(
   @WebParam(name = "numeroContrato") String numeroContrato)
   throws java.lang.Exception {

  Collection<Contrato> listaContrato =
    controladorContrato.consultarContratoPorNumero(numeroContrato);
  Contrato contrato = listaContrato.iterator().next();
  Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
    Fachada.getInstancia().listarContratoPontoConsumo(
      contrato.getChavePrimaria());
  ContratoTO contratoTO = converterContratoContratoTO(contrato);
  contratoTO.setListaPontoConsumoTO(
    converterContratoPontoConsumoPontoConsumoTO(
      listaContratoPontoConsumo));

  return contratoTO;
 }

 /**
  * Converter contrato contrato to.
  *
  * @param contrato the contrato
  * @return the contrato to
  */
 private ContratoTO converterContratoContratoTO(Contrato contrato) {

  ContratoTO contratoTO = new ContratoTO();
  contratoTO.setChavePrimaria(contrato.getChavePrimaria());
  if (contrato.getDataAditivo() != null) {
   contratoTO.setDataAditivo(
     Util.converterDataParaString(contrato.getDataAditivo()));
  }
  if (contrato.getDataAssinatura() != null) {
   contratoTO.setDataAssinatura(Util.converterDataParaString(contrato.getDataAssinatura()));
  }
  contratoTO.setDescGarantiaFinanc(contrato.getDescricaoGarantiaFinanceira());
  contratoTO.setDescricaoAditivo(contrato.getDescricaoAditivo());

  if (contrato.getTipoAgrupamento() != null) {
   contratoTO.setFaturamentoAgrupado(true);
  }
  if (contrato.getFormaCobranca() != null) {
   contratoTO.setFormaCobranca(contrato.getFormaCobranca().getDescricao());
  }
  if (contrato.getRenovacaoGarantiaFinanceira() != null
    && contrato.getRenovacaoGarantiaFinanceira()) {

   contratoTO.setGarantiaFinanceiraRenovada("Renovável");
  } else {
   contratoTO.setGarantiaFinanceiraRenovada("Renovada");
  }
  if (contrato.getIndiceFinanceiro() != null) {
   contratoTO.setIndiceCorrecaoMonetaria(
     contrato.getIndiceFinanceiro().getDescricaoAbreviada());
  }
  if (contrato.getGarantiaFinanceira() != null) {
   contratoTO.setTipoGarantiaFinanceira(
     contrato.getGarantiaFinanceira().getDescricao());
  }
  if (contrato.getValorGarantiaFinanceira() != null) {
   contratoTO.setValorGarantiaFinanceira(
     contrato.getValorGarantiaFinanceira() + "");
  }
  if (contrato.getIndicadorJuros() != null) {
   contratoTO.setIndicadorJurosMora(
     contrato.getIndicadorJuros());
  }
  if (contrato.getIndicadorMulta() != null) {
   contratoTO.setIndicadorMultaAtraso(
     contrato.getIndicadorMulta());
  }

  if (contrato.getNumeroAditivo() != null) {
   contratoTO.setNumeroAditivo(contrato.getNumeroAditivo() + "");
  }

  if (contrato.getNumeroAnterior() != null) {
   contratoTO.setNumeroAnteriorContrato(contrato.getNumeroAnterior() + "");
  }
  if (contrato.getNumeroCompletoContrato() != null) {
   contratoTO.setNumeroContrato(contrato.getNumeroCompletoContrato() + "");
  }
  if (contrato.getNumeroCompletoContrato() != null) {
   contratoTO.setNumeroContrato(contrato.getNumeroCompletoContrato() + "");
  }

  contratoTO.setNumeroEmpenho(contrato.getNumeroEmpenho());
  contratoTO.setRenovacaoAutomatica(contrato.getRenovacaoAutomatica());
  contratoTO.setSituacaoContrato(contrato.getSituacao().getDescricao());
  if (contrato.getValorContrato() != null) {
   contratoTO.setValorContrato(contrato.getValorContrato() + "");
  }

  contratoTO.setDefinicaoAnoContrato(contrato.getIndicadorAnoContratual());
  contratoTO.setVencimentoObrigContrato(contrato.getDataVencimentoObrigacoes());
  if (contrato.getListaContratoQDC() != null && !contrato.getListaContratoQDC().isEmpty()) {
   for (ContratoQDC contratoQDC : contrato.getListaContratoQDC()) {
    contratoTO.getListaQdc().add(
      Util.converterDataParaString(contratoQDC.getData()) + " | "
        + contratoQDC.getMedidaVolume());
   }
  }
  return contratoTO;
 }

 /**
  * Converter contrato ponto consumo ponto consumo to.
  *
  * @param listaContratoPontoConsumo the lista contrato ponto consumo
  * @return the list
  * @throws Exception the exception
  */
 private List<PontoConsumoTO> converterContratoPontoConsumoPontoConsumoTO(Collection<ContratoPontoConsumo> listaContratoPontoConsumo)
   throws Exception {

  List<PontoConsumoTO> lista = new ArrayList<PontoConsumoTO>();
  for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
   PontoConsumoTO pontoConsumoTO = new PontoConsumoTO();
   pontoConsumoTO.setDescricao(contratoPontoConsumo.getPontoConsumo().getDescricao());
   pontoConsumoTO.setChavePrimaria(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
   pontoConsumoTO.setSegmento(contratoPontoConsumo.getPontoConsumo().getSegmento().getDescricao());
   pontoConsumoTO.setRamoAtividade(contratoPontoConsumo.getPontoConsumo().getRamoAtividade().getDescricao());
   popularDadosAbasTelaContrato(pontoConsumoTO, contratoPontoConsumo);
   lista.add(pontoConsumoTO);
  }

  return lista;
 }

 /**
  * Popular dados abas tela contrato.
  *
  * @param pontoConsumoTO       the ponto consumo to
  * @param contratoPontoConsumo the contrato ponto consumo
  * @throws Exception the exception
  */
 private void popularDadosAbasTelaContrato(PontoConsumoTO pontoConsumoTO, ContratoPontoConsumo contratoPontoConsumo) throws Exception {

  Fachada fachada = Fachada.getInstancia();
  Locale locale = new Locale(Constantes.FORMATO_VALOR_BR);

  ContratoPontoConsumoTO contratoPontoConsumoTO = new ContratoPontoConsumoTO();
  // aba principal
  abaPrincipal(contratoPontoConsumo, contratoPontoConsumoTO);

  // Aba Dados Tecnicos
  abaDadosTecnicos(contratoPontoConsumo, fachada, contratoPontoConsumoTO);

  // ------------------------------------- dados aba consumo-----------------------------------
  // lista ao qual esta associado
  List<EntidadeConteudo> listaAmostragemAssociada = (List<EntidadeConteudo>) fachada
    .consultarLocalAmostragemPCSPorCPC(contratoPontoConsumo);
  // lista geral total
  List<EntidadeConteudo> listaAmostragem = (List<EntidadeConteudo>) fachada.listarLocalAmostragemPCS();

  if (listaAmostragemAssociada != null) {
   for (EntidadeConteudo entidadeConteudo : listaAmostragemAssociada) {
    contratoPontoConsumoTO.getListaLocalAmostragemAssociados().add(entidadeConteudo.getDescricao());
    // exibe somente os associados
   }
   if (listaAmostragem != null) {
    listaAmostragem.removeAll(listaAmostragemAssociada);
    // removo os que ja estão associados
   }
  }
  if (listaAmostragem != null) {
   for (EntidadeConteudo entidadeConteudo : listaAmostragem) {
    contratoPontoConsumoTO.getListaLocalAmostragemPCS().add(entidadeConteudo.getDescricao());
    // exibe os que não estao associados
   }
  }

  // intervalo

  List<IntervaloPCS> listaIntervaloPCSAssociados = (List<IntervaloPCS>) fachada.consultarIntervaloPCSPorCPC(contratoPontoConsumo);
  List<IntervaloPCS> listaIntervaloPCS = (List<IntervaloPCS>) fachada.listarIntervaloPCS();
  if (listaIntervaloPCSAssociados != null) {
   for (IntervaloPCS intervaloPCS : listaIntervaloPCSAssociados) {
    contratoPontoConsumoTO.getListaIntervaloPCSAssociados().add(intervaloPCS.getDescricao());
   }
  }
  if (listaIntervaloPCS != null && listaIntervaloPCSAssociados != null) {
   listaIntervaloPCS.removeAll(listaIntervaloPCSAssociados);
   for (IntervaloPCS intervaloPCS : listaIntervaloPCS) {
    contratoPontoConsumoTO.getListaIntervaloPCS().add(intervaloPCS.getDescricao());
   }
  }

  Integer tamanhoReducao = fachada.obterTamanhoIntervaloPCSPorCPC(contratoPontoConsumo);
  if (tamanhoReducao != null) {
   contratoPontoConsumoTO.setTamanhoIntervaloPCSPorCPC(String.valueOf(tamanhoReducao));
  }
  if (contratoPontoConsumo.getNumeroHoraInicial() != null) {
   contratoPontoConsumoTO.setNumeroHoraInicial(String.valueOf(contratoPontoConsumo.getNumeroHoraInicial()));
  }
  if (contratoPontoConsumo.getRegimeConsumo() != null) {
   contratoPontoConsumoTO.setRegimeConsumo(String.valueOf(contratoPontoConsumo.getRegimeConsumo().getDescricao()));
  }
  if (contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() != null && contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria() != null) {

   contratoPontoConsumoTO.setMedidaFornecimentoMaxDiaria(Util.converterCampoValorDecimalParaString("Fornecimento Máximo Diário",
     contratoPontoConsumo.getMedidaFornecimentoMaxDiaria(), locale, 2));
   contratoPontoConsumoTO.setUnidadeFornecimentoMaxDiaria(String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria()
     .getDescricaoAbreviada()));
  }
  if (contratoPontoConsumo.getMedidaFornecimentoMinDiaria() != null && contratoPontoConsumo.getUnidadeFornecimentoMinDiaria() != null) {

   contratoPontoConsumoTO.setMedidaFornecimentoMinDiaria(Util.converterCampoValorDecimalParaString("Fornecimento Mínimo Diário",
     contratoPontoConsumo.getMedidaFornecimentoMinDiaria(), locale, 2));
   contratoPontoConsumoTO.setUnidadeFornecimentoMinDiaria(String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMinDiaria()
     .getDescricaoAbreviada()));
  }
  if (contratoPontoConsumo.getMedidaFornecimentoMinMensal() != null && contratoPontoConsumo.getUnidadeFornecimentoMinMensal() != null) {

   contratoPontoConsumoTO.setMedidaFornecimentoMinMensal(Util.converterCampoValorDecimalParaString("Fornecimento Mínimo Mensal",
     contratoPontoConsumo.getMedidaFornecimentoMinMensal(), locale, 2));
   contratoPontoConsumoTO.setUnidadeFornecimentoMinMensal(contratoPontoConsumo.getUnidadeFornecimentoMinMensal()
     .getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getMedidaFornecimentoMinAnual() != null && contratoPontoConsumo.getUnidadeFornecimentoMinAnual() != null) {

   contratoPontoConsumoTO.setMedidaFornecimentoMinAnual(Util.converterCampoValorDecimalParaString("Fornecimento Mínimo Anual",
     contratoPontoConsumo.getMedidaFornecimentoMinAnual(), locale, 2));
   contratoPontoConsumoTO.setUnidadeFornecimentoMinAnual(contratoPontoConsumo.getUnidadeFornecimentoMinAnual()
     .getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getNumeroFatorCorrecao() != null) {
   contratoPontoConsumoTO.setNumeroFatorCorrecao(Util.converterCampoValorDecimalParaString(
     ContratoPontoConsumo.FATOR_UNICO_CORRECAO, contratoPontoConsumo.getNumeroFatorCorrecao(),
     Constantes.LOCALE_PADRAO, Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL));
  }
  List<AcaoAnormalidadeLeitura> lista = (List<AcaoAnormalidadeLeitura>) fachada.listarAcaoAnormalidadeComLeitura();
  String descricaoAcao = "Não Ocorre";

  if (contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura() != null) {
   for (AcaoAnormalidadeLeitura acaoAnormalidadeLeitura : lista) {
    if (acaoAnormalidadeLeitura.getChavePrimaria() == contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura()
      .getChavePrimaria()) {
     descricaoAcao = acaoAnormalidadeLeitura.getDescricao();
     break;
    }
   }
   contratoPontoConsumoTO.setAcaoAnormalidadeConsumoSemLeitura(descricaoAcao);
  }

  // --------------------------------------- aba Faturamento----------------------------------------------------------------------

  abaFaturamento(contratoPontoConsumo, fachada, contratoPontoConsumoTO);

  // aba Modalidades
  String valorTakeOrPay = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
  Penalidade penalidadeTakeOrPay = fachada.obterPenalidade(Long.parseLong(valorTakeOrPay));

  String valorShipOrPay = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
  Penalidade penalidadeShipOrPay = fachada.obterPenalidade(Long.parseLong(valorShipOrPay));

  Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = fachada
    .listarModalidadesPorContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
  if (listaContratoPontoConsumoModalidade != null && !listaContratoPontoConsumoModalidade.isEmpty()) {

   for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {

    ModalidadeConsumoFaturamentoTO modalidadeConsumoFaturamentoTO =
      montarModalidadeConsumoFaturamentoTO(fachada, penalidadeTakeOrPay,
        penalidadeShipOrPay, contratoPontoConsumoModalidade);

    contratoPontoConsumoTO.getListaModalidadeConsumoFaturaTO().add(
      modalidadeConsumoFaturamentoTO);
   }
  }
  // aba responsabilidade
  Collection<ClienteTO> listaClienteTO = new ArrayList<ClienteTO>();

  List<ContratoCliente> listaContratoCliente =
    (List<ContratoCliente>) fachada.listarContratoClientePorPontoConsumo(
      contratoPontoConsumo.getPontoConsumo().getChavePrimaria(),
      contratoPontoConsumo.getContrato().getChavePrimaria());

  for (ContratoCliente contratoCliente : listaContratoCliente) {

   String enderecos = ENDERECOS;
   String fones = FONES;
   String contatos = CONTATOS;
   String[] listaInicializar = {enderecos, fones, contatos};

   ClienteTO clienteTO = this.popularClienteTO(
     (Cliente) controladorCliente.obter(
       contratoCliente.getCliente().getChavePrimaria(),
       listaInicializar));

   if (clienteTO != null) {
    if (contratoCliente.getRelacaoFim() != null) {
     clienteTO.setDataFimRelacao(
       Util.converterDataParaString(
         contratoCliente.getRelacaoFim()));
    }
    clienteTO.setDataInicioRelacao(
      Util.converterDataParaString(
        contratoCliente.getRelacaoInicio()));

    clienteTO.setTipoResponsabilidade(
      contratoCliente.getResponsabilidade().getDescricao());

    listaClienteTO.add(clienteTO);
   }
  }

  contratoPontoConsumoTO.setListaClientesTO(listaClienteTO);
  pontoConsumoTO.setContratoPontoConsumoTO(contratoPontoConsumoTO);

 }

 /**
  * @param contratoPontoConsumo
  * @param fachada
  * @param contratoPontoConsumoTO
  * @throws GGASException
  */
 private void abaFaturamento(ContratoPontoConsumo contratoPontoConsumo, Fachada fachada,
                             ContratoPontoConsumoTO contratoPontoConsumoTO) throws GGASException {
  if (contratoPontoConsumo.getCep() != null) {
   contratoPontoConsumoTO.setCep(contratoPontoConsumo.getCep().getCep());
  }
  if (contratoPontoConsumo.getNumeroImovel() != null) {
   contratoPontoConsumoTO.setNumeroImovel(contratoPontoConsumo.getNumeroImovel().toString());
  }
  if (contratoPontoConsumo.getComplementoEndereco() != null) {
   contratoPontoConsumoTO.setComplementoEndereco(contratoPontoConsumo.getComplementoEndereco());
  }
  if (contratoPontoConsumo.getEmailEntrega() != null) {
   contratoPontoConsumoTO.setEmailEntrega(contratoPontoConsumo.getEmailEntrega());
  }
  String descricaoContratoCompra = "";
  if (contratoPontoConsumo.getContratoCompra() != null) {
   List<EntidadeConteudo> listaContratoCompra = (List<EntidadeConteudo>) fachada.obterListaContratoCompra();
   for (EntidadeConteudo entidadeConteudo : listaContratoCompra) {
    if (contratoPontoConsumo.getContratoCompra().getChavePrimaria() == entidadeConteudo.getChavePrimaria()) {
     descricaoContratoCompra = entidadeConteudo.getDescricao();
    }
   }
   contratoPontoConsumoTO.setContratoCompra(descricaoContratoCompra);
  }

  if (contratoPontoConsumo.getPeriodicidade() != null) {
   contratoPontoConsumoTO.setPeriodicidade(contratoPontoConsumo.getPeriodicidade().getDescricaoAbreviada());
  }

  Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento =
    fachada.listarItensVencimentoPorContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
  if (listaContratoPontoConsumoItemFaturamento != null && !listaContratoPontoConsumoItemFaturamento.isEmpty()) {

   Collection<ContratoPontoConsumoItemFaturamentoTO>
     listaContratoPontoConsumoItemFaturamentoTO =
     new HashSet<ContratoPontoConsumoItemFaturamentoTO>();

   for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : listaContratoPontoConsumoItemFaturamento) {
    ContratoPontoConsumoItemFaturamentoTO contratoPontoConsumoItemFaturamentoTO = new ContratoPontoConsumoItemFaturamentoTO();
    if (contratoPontoConsumoItemFaturamento.getFaseReferencia() != null) {
     contratoPontoConsumoItemFaturamentoTO.setApos(contratoPontoConsumoItemFaturamento.getFaseReferencia().getDescricao());
    }

    contratoPontoConsumoItemFaturamentoTO.setDiaVencimento(contratoPontoConsumoItemFaturamento.getNumeroDiaVencimento() + "");
    contratoPontoConsumoItemFaturamentoTO.setItemDesc(contratoPontoConsumoItemFaturamento.getItemFatura().getDescricao());
    contratoPontoConsumoItemFaturamentoTO.setOpcao(contratoPontoConsumoItemFaturamento.getOpcaoFaseReferencia().getDescricao());
    String vencimento = "Não";
    if (contratoPontoConsumoItemFaturamento.getVencimentoDiaUtil() != null &&
      contratoPontoConsumoItemFaturamento.getVencimentoDiaUtil()) {
     vencimento = "Sim";
    }
    contratoPontoConsumoItemFaturamentoTO.setVencimentoEmDiaUtil(vencimento);
    contratoPontoConsumoItemFaturamentoTO.setTarifa(contratoPontoConsumoItemFaturamento.getTarifa().getDescricao());
    if (contratoPontoConsumoItemFaturamento.getDiaCotacao() != null) {
     contratoPontoConsumoItemFaturamentoTO.setDiaCotacao(contratoPontoConsumoItemFaturamento.getDiaCotacao().getDescricao());
    }
    if (contratoPontoConsumoItemFaturamento.getDataReferenciaCambial() != null) {
     contratoPontoConsumoItemFaturamentoTO.setDataReferenciaCambial(contratoPontoConsumoItemFaturamento
       .getDataReferenciaCambial().getDescricao());
    }
    if (contratoPontoConsumoItemFaturamento.getPercminimoQDC() != null) {
     contratoPontoConsumoItemFaturamentoTO.setPercminimoQDC(contratoPontoConsumoItemFaturamento.getPercminimoQDC() + "");
    }
    if (contratoPontoConsumoItemFaturamento.getVencimentoDiaUtil() != null) {
     contratoPontoConsumoItemFaturamentoTO
       .setIndicadorVencDiaNaoUtil(contratoPontoConsumoItemFaturamento.getVencimentoDiaUtil());
    }
    if (contratoPontoConsumoItemFaturamento.getIndicadorDepositoIdentificado() != null) {
     contratoPontoConsumoItemFaturamentoTO.setDepositoIdentificado(contratoPontoConsumoItemFaturamento
       .getIndicadorDepositoIdentificado());
    }

    listaContratoPontoConsumoItemFaturamentoTO.add(contratoPontoConsumoItemFaturamentoTO);

   }

   contratoPontoConsumoTO.setListaContratoPontoConsumoItemFaturamentoTO(listaContratoPontoConsumoItemFaturamentoTO);
  }
 }

 /**
  * @param contratoPontoConsumo
  * @param fachada
  * @param contratoPontoConsumoTO
  * @throws GGASException
  */
 private void abaDadosTecnicos(ContratoPontoConsumo contratoPontoConsumo, Fachada fachada,
                               ContratoPontoConsumoTO contratoPontoConsumoTO) throws GGASException {
  if (contratoPontoConsumo.getMedidaVazaoInstantanea() != null) {
   contratoPontoConsumoTO.setMedidaVazaoInstantanea(Util.converterCampoValorParaString(
     contratoPontoConsumo.getMedidaVazaoInstantanea(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadeVazaoInstantanea() != null) {
   contratoPontoConsumoTO
     .setUnidadeVazaoInstantanea(String.valueOf(contratoPontoConsumo.getUnidadeVazaoInstantanea().getDescricaoAbreviada()));
  }
  if (contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() != null) {
   contratoPontoConsumoTO.setMedidaVazaoMinimaInstantanea(Util.converterCampoValorParaString(
     contratoPontoConsumo.getMedidaVazaoMinimaInstantanea(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea() != null) {
   contratoPontoConsumoTO
     .setUnidadeVazaoMinimaInstantanea(contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea().getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
   contratoPontoConsumoTO.setMedidaVazaoMaximaInstantanea(Util.converterCampoValorParaString(
     contratoPontoConsumo.getMedidaVazaoMaximaInstantanea(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() != null) {
   contratoPontoConsumoTO
     .setUnidadeVazaoMaximaInstantanea(contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getMedidaPressao() != null && contratoPontoConsumo.getUnidadePressao() != null) {
   FaixaPressaoFornecimento faixa = fachada.obterFaixaPressaoFornecimento(contratoPontoConsumo.getPontoConsumo().getSegmento(),
     contratoPontoConsumo.getMedidaPressao(), contratoPontoConsumo.getUnidadePressao());
   if (faixa != null) {
    contratoPontoConsumoTO.setFaixaPressaoFornecimento(faixa.getDescricaoFormatada());
   }
  }
  if (contratoPontoConsumo.getMedidaPressaoMinima() != null) {
   contratoPontoConsumoTO.setMedidaPressaoMinima(Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaPressaoMinima(),
     Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadePressaoMinima() != null) {
   contratoPontoConsumoTO.setUnidadePressaoMinima(contratoPontoConsumo.getUnidadePressaoMinima().getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getMedidaPressaoMaxima() != null) {
   contratoPontoConsumoTO.setMedidaPressaoMaxima(Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaPressaoMaxima(),
     Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadePressaoMaxima() != null) {
   contratoPontoConsumoTO.setUnidadePressaoMaxima(contratoPontoConsumo.getUnidadePressaoMaxima().getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getMedidaPressaoLimite() != null) {
   contratoPontoConsumoTO.setMedidaPressaoLimite(Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaPressaoLimite(),
     Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getUnidadePressaoLimite() != null) {
   contratoPontoConsumoTO.setUnidadePressaoLimite(contratoPontoConsumo.getUnidadePressaoLimite().getDescricaoAbreviada());
  }
  if (contratoPontoConsumo.getQuantidadeAnosParadaCliente() != null) {
   contratoPontoConsumoTO.setQuantidadeAnosParadaCliente(contratoPontoConsumo.getQuantidadeAnosParadaCliente().toString());
  }
  if (contratoPontoConsumo.getQuantidadeTotalParadaCliente() != null) {
   contratoPontoConsumoTO.setQuantidadeTotalParadaCliente(contratoPontoConsumo.getQuantidadeTotalParadaCliente().toString());
  }
  if (contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente() != null) {
   contratoPontoConsumoTO.setQuantidadeMaximaAnoParadaCliente(contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente()
     .toString());
  }
  if (contratoPontoConsumo.getDiasAntecedentesParadaCliente() != null) {
   contratoPontoConsumoTO.setDiasAntecedentesParadaCliente(contratoPontoConsumo.getDiasAntecedentesParadaCliente().toString());
  }
  if (contratoPontoConsumo.getDiasConsecutivosParadaCliente() != null) {
   contratoPontoConsumoTO.setDiasConsecutivosParadaCliente(contratoPontoConsumo.getDiasConsecutivosParadaCliente().toString());
  }
  if (contratoPontoConsumo.getQuantidadeAnosParadaCDL() != null) {
   contratoPontoConsumoTO.setQuantidadeAnosParadaCDL(contratoPontoConsumo.getQuantidadeAnosParadaCDL().toString());
  }
  if (contratoPontoConsumo.getQuantidadeTotalParadaCDL() != null) {
   contratoPontoConsumoTO.setQuantidadeTotalParadaCDL(contratoPontoConsumo.getQuantidadeTotalParadaCDL().toString());
  }
  if (contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL() != null) {
   contratoPontoConsumoTO.setQuantidadeMaximaAnoParadaCDL(contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL().toString());
  }
  if (contratoPontoConsumo.getDiasAntecedentesParadaCDL() != null) {
   contratoPontoConsumoTO.setDiasAntecedentesParadaCDL(contratoPontoConsumo.getDiasAntecedentesParadaCDL().toString());
  }
  if (contratoPontoConsumo.getDiasConsecutivosParadaCDL() != null) {
   contratoPontoConsumoTO.setDiasConsecutivosParadaCDL(contratoPontoConsumo.getDiasConsecutivosParadaCDL().toString());
  }
 }

 /**
  * @param contratoPontoConsumo
  * @param contratoPontoConsumoTO
  */
 private void abaPrincipal(ContratoPontoConsumo contratoPontoConsumo,
                           ContratoPontoConsumoTO contratoPontoConsumoTO) {
  if (contratoPontoConsumo.getDataInicioTeste() != null) {
   contratoPontoConsumoTO.setDataInicioTeste(Util.converterDataParaStringSemHora(contratoPontoConsumo.getDataInicioTeste(),
     Constantes.FORMATO_DATA_BR));
  }

  if (contratoPontoConsumo.getDataFimTeste() != null) {
   contratoPontoConsumoTO.setDataFimTeste(Util.converterDataParaStringSemHora(contratoPontoConsumo.getDataFimTeste(),
     Constantes.FORMATO_DATA_BR));
  }
  if (contratoPontoConsumo.getPrazoConsumoTeste() != null) {
   contratoPontoConsumoTO.setPrazoConsumoTeste(contratoPontoConsumo.getPrazoConsumoTeste().toString());
  }

  if (contratoPontoConsumo.getMedidaConsumoTeste() != null) {
   contratoPontoConsumoTO.setMedidaConsumoTeste(Util.converterCampoValorParaString(
     contratoPontoConsumo.getMedidaConsumoTeste(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
  }
  if (contratoPontoConsumo.getCodigoDDDFaxOperacional() != null) {
   contratoPontoConsumoTO.setCodigoDDDFaxOperacional(contratoPontoConsumo.getCodigoDDDFaxOperacional().toString());
  }
  if (contratoPontoConsumo.getNumeroFaxOperacional() != null) {
   contratoPontoConsumoTO.setNumeroFaxOperacional(contratoPontoConsumo.getNumeroFaxOperacional().toString());
  }
  if (contratoPontoConsumo.getEmailOperacional() != null) {
   contratoPontoConsumoTO.setEmailOperacional(contratoPontoConsumo.getEmailOperacional());
  }
  if (contratoPontoConsumo.getDataInicioGarantiaConversao() != null) {
   contratoPontoConsumoTO.setDataInicioGarantiaConversao(Util.converterDataParaStringSemHora(
     contratoPontoConsumo.getDataInicioGarantiaConversao(), Constantes.FORMATO_DATA_BR));
  }
  if (contratoPontoConsumo.getDiaGarantiaConversao() != null) {
   contratoPontoConsumoTO.setDiaGarantiaConversao(contratoPontoConsumo.getDiaGarantiaConversao().toString());
  }

  if (contratoPontoConsumo.getDataInicioGarantiaConversao() != null && contratoPontoConsumo.getDiaGarantiaConversao() != null) {
   String dataFimString = "";
   Date dataFim = Util.incrementarDataComQuantidadeDias(contratoPontoConsumo.getDataInicioGarantiaConversao(),
     contratoPontoConsumo.getDiaGarantiaConversao());
   if (dataFim != null) {
    dataFimString = Util.converterDataParaString(dataFim);
   }

   contratoPontoConsumoTO.setDataFimGarantiaConversao(dataFimString);
  }
 }

 /**
  * Montar modalidade consumo faturamento TO.
  *
  * @param fachada                        the fachada
  * @param penalidadeTakeOrPay            the penalidade take or pay
  * @param penalidadeShipOrPay            the penalidade ship or pay
  * @param contratoPontoConsumoModalidade the contrato ponto consumo modalidade
  * @return the modalidade consumo faturamento TO
  * @throws GGASException the GGAS exception
  */
 private ModalidadeConsumoFaturamentoTO montarModalidadeConsumoFaturamentoTO(
   Fachada fachada, Penalidade penalidadeTakeOrPay,
   Penalidade penalidadeShipOrPay,
   ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
   throws GGASException {

  ModalidadeConsumoFaturamentoTO modalidadeConsumoFaturamentoTO =
    new ModalidadeConsumoFaturamentoTO();

  if (contratoPontoConsumoModalidade.getContratoModalidade() != null) {
   modalidadeConsumoFaturamentoTO.setModalidade(
     contratoPontoConsumoModalidade.getContratoModalidade().getDescricao());

   modalidadeConsumoFaturamentoTO.setChavePrimaria(
     contratoPontoConsumoModalidade.getChavePrimaria());
  }
  Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC =
    contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC();

  validaListaContratoPontoConsumoModalidadeQDC(modalidadeConsumoFaturamentoTO,
    listaContratoPontoConsumoModalidadeQDC);

  if (contratoPontoConsumoModalidade.getDataFimRetiradaQPNR() != null) {

   modalidadeConsumoFaturamentoTO.setDataFimRetiradaQPNR(Util.converterDataParaString(
     contratoPontoConsumoModalidade.getDataFimRetiradaQPNR()));
  }
  if (contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR() != null) {

   modalidadeConsumoFaturamentoTO.setDataInicioRetiradaQPNR(Util.converterDataParaString(
     contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR()));
  }

  if (contratoPontoConsumoModalidade.getDiasAntecedenciaQDS() != null) {

   modalidadeConsumoFaturamentoTO.setDiasAntecSolicConsumo(contratoPontoConsumoModalidade.getDiasAntecedenciaQDS() + "");
  }
  if (contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR() != null) {
   modalidadeConsumoFaturamentoTO.setTempoValidadeRetiradaQPNR(contratoPontoConsumoModalidade
     .getAnosValidadeRetiradaQPNR() + "");
  }

  modalidadeConsumoFaturamentoTO.setIndicadorProgramacaoConsumo(
    contratoPontoConsumoModalidade.getIndicadorProgramacaoConsumo());

  if (contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC() != null) {
   modalidadeConsumoFaturamentoTO.setQdsMaiorQDC(
     contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC());
  } else {
   modalidadeConsumoFaturamentoTO.setQdsMaiorQDC(Boolean.FALSE);
  }

  if (contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC() != null) {
   modalidadeConsumoFaturamentoTO.setVariacaoSuperiorQDC(
     contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC() + "");
  } else {
   modalidadeConsumoFaturamentoTO.setVariacaoSuperiorQDC("");
  }

  if (contratoPontoConsumoModalidade.getIndicadorConfirmacaoAutomaticaQDS() != null) {
   modalidadeConsumoFaturamentoTO.setConfirmacaoAutomaticaQDS(contratoPontoConsumoModalidade.getIndicadorConfirmacaoAutomaticaQDS());
  } else {
   modalidadeConsumoFaturamentoTO.setConfirmacaoAutomaticaQDS(Boolean.FALSE);
  }

  if (contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica() != null) {
   modalidadeConsumoFaturamentoTO.setRecuperacaoAutoTakeOrPay(contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica());
  } else {
   modalidadeConsumoFaturamentoTO.setRecuperacaoAutoTakeOrPay(Boolean.FALSE);
  }
  modalidadeConsumoFaturamentoTO.setNumMesesSolicConsumo(contratoPontoConsumoModalidade.getMesesQDS() + "");
  modalidadeConsumoFaturamentoTO.setPercDuranteRetiradaQPNR(NumeroUtil
    .converterPercentualParaBigDecimalString(contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR()));

  modalidadeConsumoFaturamentoTO.setPercFimRetiradaQPNR(NumeroUtil
    .converterPercentualParaBigDecimalString(contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR()));

  if (contratoPontoConsumoModalidade.getHoraLimiteProgramacaoDiaria() != null) {
   modalidadeConsumoFaturamentoTO.setHoraLimiteProgramacaoDiaria(contratoPontoConsumoModalidade
     .getHoraLimiteProgramacaoDiaria());
  }

  if (contratoPontoConsumoModalidade.getHoraLimiteProgIntradiaria() != null) {
   modalidadeConsumoFaturamentoTO.setHoraLimiteProgIntradiaria(contratoPontoConsumoModalidade
     .getHoraLimiteProgIntradiaria());
  }

  if (contratoPontoConsumoModalidade.getPrazoRevisaoQDC() != null) {
   modalidadeConsumoFaturamentoTO.setPrazoRevizaoQDC(Util.converterDataParaString(
     contratoPontoConsumoModalidade.getPrazoRevisaoQDC()));
  }

  if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade() != null) {
   Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = fachada
     .listarConsumoPenalidadePorContratoPontoConsumoModalidade(contratoPontoConsumoModalidade
       .getChavePrimaria());
   for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaContratoPontoConsumoPenalidade) {

    if ((contratoPontoConsumoPenalidade.getPenalidade() != null)
      && (contratoPontoConsumoPenalidade.getPenalidade().getChavePrimaria() == penalidadeTakeOrPay
      .getChavePrimaria())) {
     modalidadeConsumoFaturamentoTO.getListaCompromissoTakeOrPayVO().add(
       contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getDescricao()
         + "|"
         + contratoPontoConsumoPenalidade.getConsumoReferencia().getDescricao()
         + "|"
         + BigDecimalUtil.converterPercentualParaBigDecimal(contratoPontoConsumoPenalidade
         .getPercentualMargemVariacao()) + "%");
    } else if ((contratoPontoConsumoPenalidade.getPenalidade() != null)
      && (contratoPontoConsumoPenalidade.getPenalidade().getChavePrimaria() == penalidadeShipOrPay
      .getChavePrimaria())) {
     modalidadeConsumoFaturamentoTO.getListaCompmissoShipOrPayVO().add(
       contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getDescricao()
         + "|"
         + contratoPontoConsumoPenalidade.getConsumoReferencia().getDescricao()
         + "|"
         + BigDecimalUtil.converterPercentualParaBigDecimal(contratoPontoConsumoPenalidade
         .getPercentualMargemVariacao()) + "%");
    }
   }
  }
  return modalidadeConsumoFaturamentoTO;
 }

 private void validaListaContratoPontoConsumoModalidadeQDC(
   ModalidadeConsumoFaturamentoTO modalidadeConsumoFaturamentoTO,
   Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC) {
  if (listaContratoPontoConsumoModalidadeQDC != null) {
   for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaContratoPontoConsumoModalidadeQDC) {
    modalidadeConsumoFaturamentoTO.getListaContratoPontoConsumoModalidadeQDC().add(
      Util.converterDataParaString(contratoPontoConsumoModalidadeQDC.getDataVigencia())
        + " | "
        + contratoPontoConsumoModalidadeQDC.getMedidaVolume());

   }
  }
 }

 /**
  * Consultar tipo fone.
  *
  * @param tipoFoneTO the tipo fone to
  * @return the tipo combo box to
  */
 public TipoComboBoxTO consultarTipoFone(TipoComboBoxTO tipoFoneTO) {

  try {
   TipoFone tipoFone = (TipoFone) controladorCliente.obter(tipoFoneTO.getChavePrimaria(), TipoFoneImpl.class);

   tipoFoneTO.setDescricao(tipoFone.getDescricao());
  } catch (NegocioException e) {
   Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
  }

  return tipoFoneTO;
 }

 /**
  * Consultar constante sistema.
  *
  * @param codigo the codigo
  * @return the string
  */
 @WebMethod(operationName = "consultarConstanteSistema")
 @RequestWrapper(localName = "consultarConstanteSistemaRequest")
 @ResponseWrapper(localName = "consultarConstanteSistemaResposta")
 public String consultarConstanteSistema(@WebParam(name = "codigo") String codigo) {

  return controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(codigo);
 }

 /**
  * Consultar entidade conteudo.
  *
  * @param codigo the codigo
  * @return the tipo combo box to
  */
 @WebMethod(operationName = "consultarEntidadeConteudo")
 @RequestWrapper(localName = "consultarEntidadeConteudoRequest")
 @ResponseWrapper(localName = "consultarEntidadeConteudoResposta")
 public TipoComboBoxTO consultarEntidadeConteudo(@WebParam(name = "codigo") String codigo) {

  String codigoConstante = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(codigo);
  EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(codigoConstante));

  TipoComboBoxTO tipoAcao = new TipoComboBoxTO();
  tipoAcao.setChavePrimaria(entidadeConteudo.getChavePrimaria());
  tipoAcao.setDescricao(entidadeConteudo.getDescricao());

  return tipoAcao;
 }

 /**
  * Consultar atividade economica.
  *
  * @param codigoOriginal the codigo original
  * @param codigo         the codigo
  * @param descricao      the descricao
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarAtividadeEconomica")
 @RequestWrapper(localName = "consultarAtividadeEconomicaRequest")
 @ResponseWrapper(localName = "consultarAtividadeEconomicaResposta")
 public Collection<AtividadeEconomicaTO> consultarAtividadeEconomica(@WebParam(name = "codigoOriginal") Long codigoOriginal,
                                                                     @WebParam(name = "codigo") String codigo,
                                                                     @WebParam(name = "descricao") String descricao) throws Exception {

  Collection<AtividadeEconomicaTO> listaAtividadeEconomicaTO = new ArrayList<AtividadeEconomicaTO>();
  Collection<AtividadeEconomica> listaAtividadeEconomica = null;

  Map<String, Object> filtro = new HashMap<String, Object>();
  if (codigoOriginal != null) {
   filtro.put("codigoOriginal", codigoOriginal);
  }
  if (codigo != null && !codigo.isEmpty()) {
   filtro.put("codigo", codigo);
  }
  if (descricao != null && !descricao.isEmpty()) {
   filtro.put("descricao", descricao);
  }

  listaAtividadeEconomica = controladorCliente.consultarAtividadeEconomica(filtro);

  if (listaAtividadeEconomica != null && !listaAtividadeEconomica.isEmpty()) {
   for (AtividadeEconomica atividadeEconomica : listaAtividadeEconomica) {

    AtividadeEconomicaTO atividadeEconomicaTO = this.popularListaAtividadeEconomicaTO(atividadeEconomica.getDescricao(),
      atividadeEconomica.getCodigo(), atividadeEconomica.getCodigoOriginal(),
      atividadeEconomica.getChavePrimaria());
    listaAtividadeEconomicaTO.add(atividadeEconomicaTO);
   }
  }

  return listaAtividadeEconomicaTO;
 }

 /**
  * Consultar cliente pessoa juridica.
  *
  * @param cnpj the cnpj
  * @param nome the nome
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarClientePessoaJuridica")
 @RequestWrapper(localName = "consultarClientePessoaJuridicaRequest")
 @ResponseWrapper(localName = "consultarClientePessoaJuridicaResposta")
 public Collection<ClienteTO> consultarClientePessoaJuridica(@WebParam(name = "cnpj") String cnpj, @WebParam(name = "nome") String nome)
   throws Exception {

  List<Cliente> listaCliente = (List<Cliente>) controladorCliente.consultarClientesPessoaJuridica(cnpj, nome);

  List<ClienteTO> listaClienteTO = new ArrayList<ClienteTO>();
  if (listaCliente != null && !listaCliente.isEmpty()) {
   for (Cliente cliente : listaCliente) {
    listaClienteTO.add(popularClienteTO(cliente));
   }
  }

  return listaClienteTO;

 }

 /**
  * Consultar tipo pessoa.
  *
  * @param chaveTipoCliente the chave tipo cliente
  * @return the integer
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarTipoPessoa")
 @RequestWrapper(localName = "consultarTipoPessoaRequest")
 @ResponseWrapper(localName = "consultarTipoPessoaResposta")
 public Integer consultarTipoPessoa(@WebParam(name = "chaveTipoCliente") Long chaveTipoCliente) throws Exception {

  TipoCliente tipoCliente = controladorCliente.obterTipoClientePorIdTipoCliente(chaveTipoCliente);

  return tipoCliente.getTipoPessoa().getCodigo();
 }

 /**
  * Verificar chamado aberto por chamado assunto.
  *
  * @param assunto        the assunto
  * @param numeroContrato the numero contrato
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "verificarChamadoAbertoPorChamadoAssunto")
 @RequestWrapper(localName = "verificarChamadoAbertoPorChamadoAssuntoRequest")
 @ResponseWrapper(localName = "verificarChamadoAbertoPorChamadoAssuntoResposta")
 public Long verificarChamadoAbertoPorChamadoAssunto(@WebParam(name = "assunto") String assunto,
                                                     @WebParam(name = "numeroContrato") Integer numeroContrato) throws Exception {

  return controladorChamado.verificarChamadoAbertoPorChamadoAssunto(assunto, numeroContrato);
 }

 /**
  * Consultar endereco.
  *
  * @param uf         the uf
  * @param municipio  the municipio
  * @param logradouro the logradouro
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarEndereco")
 @RequestWrapper(localName = "consultarEnderecoRequest")
 @ResponseWrapper(localName = "consultarEnderecoResposta")
 public Collection<CepTO> consultarEndereco(@WebParam(name = "uf") String uf, @WebParam(name = "municipio") String municipio,
                                            @WebParam(name = "logradouro") String logradouro) throws Exception {

  Collection<Cep> cep = controladorEndereco.consultarCeps(uf, municipio, logradouro);
  Collection<CepTO> tos = new ArrayList<CepTO>();

  for (Cep c : cep) {
   CepTO to = new CepTO();
   Util.copyProperties(to, c);
   tos.add(to);
  }
  return tos;
 }

 /**
  * Consultar cep.
  *
  * @param numeroCep the numero cep
  * @return the cep to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarCep")
 @RequestWrapper(localName = "consultarCepRequest")
 @ResponseWrapper(localName = "consultarCepResposta")
 public CepTO consultarCep(@WebParam(name = "numeroCep") String numeroCep) throws Exception {

  Cep cep = null;
  CepTO cepTO = new CepTO();
  try {
   cep = controladorEndereco.obterCep(numeroCep);

   if (cep != null) {
    Util.copyProperties(cepTO, cep);
   }
  } catch (Exception e) {
   tratarMensagem(e);
  }

  return cepTO;
 }

 /**
  * Verificar chamado aberto por chamado assunto ponto consumo.
  *
  * @param assunto        the assunto
  * @param numeroContrato the numero contrato
  * @param pontoConsumo   the ponto consumo
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "verificarChamadoAbertoPorChamadoAssuntoPontoConsumo")
 @RequestWrapper(localName = "verificarChamadoAbertoPorChamadoAssuntoPontoConsumoRequest")
 @ResponseWrapper(localName = "verificarChamadoAbertoPorChamadoAssuntoPontoConsumoResposta")
 public Long verificarChamadoAbertoPorChamadoAssuntoPontoConsumo(@WebParam(name = "assunto") String assunto,
                                                                 @WebParam(name = "numeroContrato") Integer numeroContrato,
                                                                 @WebParam(name = "pontoConsumo") Long pontoConsumo) throws Exception {

  return controladorChamado.verificarChamadoAbertoPorChamadoAssuntoPontoConsumo(assunto, numeroContrato, pontoConsumo);
 }

 /**
  * Obter parametro por codigo.
  *
  * @param codigoParameto the codigo parameto
  * @param cacheable      the cacheable
  * @return the string
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterParametroPorCodigo")
 @RequestWrapper(localName = "obterParametroPorCodigoRequest")
 @ResponseWrapper(localName = "obterParametroPorCodigoResposta")
 public String obterParametroPorCodigo(@WebParam(name = "codigoParameto") String codigoParameto,
                                       @WebParam(name = "cacheable") boolean cacheable) throws Exception {

  ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(codigoParameto, cacheable);

  return parametro.getValor();
 }

 /**
  * Verificar cliente.
  *
  * @param cpfCnpj the cpf cnpj
  * @param email   the email
  * @param senha   the senha
  * @return the list
  * @throws Exception the exception
  */
 @WebMethod(operationName = "verificarCliente")
 @RequestWrapper(localName = "verificarClienteRequest")
 @ResponseWrapper(localName = "verificarClienteResposta")
 public List<ContratoTO> verificarCliente(@WebParam(name = "cpfCnpj") String cpfCnpj, @WebParam(name = "email") String email,
                                          @WebParam(name = "senha") String senha) throws Exception {

  Map<String, Object> filtro = new HashMap<String, Object>();

  if (Util.removerCaracteresEspeciais(cpfCnpj).length() <= NUM_CARACTERES_CPF) {
   filtro.put("cpf", cpfCnpj);
  } else if (Util.removerCaracteresEspeciais(cpfCnpj).length() > NUM_CARACTERES_CPF) {
   filtro.put("cnpj", cpfCnpj);
  }
  if (email != null && !email.isEmpty() && "".equals(email)) {
   filtro.put("email", email);
  }

  List<Cliente> clientes = (List<Cliente>) controladorCliente.consultarClientes(filtro);
  if (clientes.isEmpty()) {
   throw new Exception("Verifique se os campos foram informados corretamente .");
  }
  ClienteTO cliente = popularClienteTO(clientes.get(0));

  List<Contrato> contrato = controladorContrato.obterListaContratoPorCliente(cliente.getChavePrimaria());
  List<ContratoTO> contratoTo = new ArrayList<ContratoTO>();

  if (contrato != null && !contrato.isEmpty()) {
   for (int i = 0; i < contrato.size(); i++) {
    contratoTo.add(i, this.popularContratoTO(contrato.get(i)));
   }

  } else {

   throw new Exception("O cliente não possui contrato");
  }

  if (StringUtils.isNotEmpty(senha)) {

   String senhaCriptografada = Util.criptografarSenha(senha, senha, Constantes.HASH_CRIPTOGRAFIA);

   if (senhaCriptografada.equals(cliente.getSenha())) {
    return contratoTo;
   } else {
    throw new Exception("Senha Incorreta!");
   }
  }

  return contratoTo;

 }

 /**
  * Criar senha cliente.
  *
  * @param cpfCnpj the cpf cnpj
  * @param email   the email
  * @throws Exception the exception
  */
 @WebMethod(operationName = "criarSenhaCliente")
 @RequestWrapper(localName = "criarSenhaClienteRequest")
 @ResponseWrapper(localName = "criarSenhaClienteResposta")
 public void criarSenhaCliente(@WebParam(name = "cpfCnpj") String cpfCnpj, @WebParam(name = "email") String email) throws Exception {

  ParametroSistema parametroSistema = controladorParametroSistema
    .obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
  ParametroSistema tamanhoMaximoSenha = controladorParametroSistema.obterParametroPorCodigo(Constantes.TAMANHO_MAXIMO_SENHA_AGENCIA);

  String senhaCriptografada = Util.criptografarSenha(cpfCnpj, String.valueOf(Calendar.getInstance().getTimeInMillis()),
    Constantes.HASH_CRIPTOGRAFIA);
  String subSenhaCriptografada = senhaCriptografada.substring(0, Integer.parseInt(tamanhoMaximoSenha.getValor()));
  String senhaCriptografadaCompleta = Util.criptografarSenha(subSenhaCriptografada, subSenhaCriptografada,
    Constantes.HASH_CRIPTOGRAFIA);

  Cliente cliente = null;
  Map<String, Object> filtro = new HashMap<String, Object>();
  if (Util.removerCaracteresEspeciais(cpfCnpj).length() <= NUM_CARACTERES_CPF) {
   filtro.put("cpf", cpfCnpj);
  } else if (Util.removerCaracteresEspeciais(cpfCnpj).length() > NUM_CARACTERES_CPF) {
   filtro.put("cnpj", cpfCnpj);
  }
  List<Cliente> clientes = (List<Cliente>) controladorCliente.consultarClientes(filtro);
  cliente = controladorCliente.obterCliente(clientes.get(0).getChavePrimaria(), Cliente.PROPRIEDADES_LAZY);
  cliente.setSenha(senhaCriptografadaCompleta);
  cliente.setDataUltimoAcesso(null);
  controladorCliente.atualizar(cliente);

  String mensagem = "Login: " + cpfCnpj + "\n" + " Sua senha é: " + subSenhaCriptografada;
  controladorUsuario.enviarEmail(parametroSistema.getValor(), email, "Senha para acesso da Agência Virtual.", mensagem);
 }

 /**
  * Alterar senha cliente.
  *
  * @param cpfCnpj   the cpf cnpj
  * @param senha     the senha
  * @param novaSenha the nova senha
  * @return the string
  * @throws Exception the exception
  */
 @WebMethod(operationName = "alterarSenhaCliente")
 @RequestWrapper(localName = "alteraSenhaClienteRequest")
 @ResponseWrapper(localName = "alteraSenhaClienteResposta")
 public String alterarSenhaCliente(@WebParam(name = "cpfCnpj") String cpfCnpj, @WebParam(name = "senha") String senha,
                                   @WebParam(name = "novaSenha") String novaSenha) throws Exception {

  if (senha.equals(novaSenha)) {
   throw new Exception("A nova senha não pode ser identetica a atual !");
  }

  ParametroSistema tamanhoMaximoSenha = controladorParametroSistema.obterParametroPorCodigo(Constantes.TAMANHO_MAXIMO_SENHA_AGENCIA);
  ParametroSistema tamanhoMinimoSenha = controladorParametroSistema.obterParametroPorCodigo(Constantes.TAMANHO_MINIMO_SENHA_AGENCIA);
  ParametroSistema caracterMaiusculoAg = controladorParametroSistema
    .obterParametroPorCodigo(Constantes.CODIGO_CARACTERE_MAIUSCULO_AGENCIA);
  ParametroSistema caracterMinusculoAg = controladorParametroSistema
    .obterParametroPorCodigo(Constantes.CODIGO_CARACTERE_MINUSCULO_AGENCIA);

  if (novaSenha.length() > Integer.parseInt(tamanhoMaximoSenha.getValor())) {
   throw new Exception(" No Máximo " + tamanhoMaximoSenha.getValor() + " caracteres para senha! ");
  } else if (novaSenha.length() < Integer.parseInt(tamanhoMinimoSenha.getValor())) {
   throw new Exception(" No Minimo " + tamanhoMinimoSenha.getValor() + " caracteres para senha! ");
  }

  char caracter;
  boolean letra = true;
  if ("1".equals(caracterMinusculoAg.getValor())) {
   for (int i = 0; i < novaSenha.length(); i++) {
    caracter = novaSenha.charAt(i);
    if ((caracter >= 'a') && (caracter <= 'z')) {
     letra = false;
    }
   }
   if (letra) {
    throw new Exception("A senha deve conter no minimo 1 caracter Minusculo.");
   }
  }
  letra = true;
  if ("1".equals(caracterMaiusculoAg.getValor())) {
   for (int i = 0; i < novaSenha.length(); i++) {
    caracter = novaSenha.charAt(i);
    if ((caracter >= 'A') && (caracter <= 'Z')) {
     letra = false;
    }
   }
   if (letra) {
    throw new Exception("A senha deve conter no mínimo 1 caracter Maiusculo.");
   }
  }

  String senhaCriptografadaCompleta = Util.criptografarSenha(novaSenha, novaSenha, Constantes.HASH_CRIPTOGRAFIA);

  Cliente cliente = null;
  Map<String, Object> filtro = new HashMap<String, Object>();
  if (Util.removerCaracteresEspeciais(cpfCnpj).length() <= NUM_CARACTERES_CPF) {
   filtro.put("cpf", cpfCnpj);
  } else if (Util.removerCaracteresEspeciais(cpfCnpj).length() > NUM_CARACTERES_CPF) {
   filtro.put("cnpj", cpfCnpj);
  }
  try {
   List<Cliente> clientes = (List<Cliente>) controladorCliente.consultarClientes(filtro);
   cliente = controladorCliente.obterCliente(clientes.get(0).getChavePrimaria(), Cliente.PROPRIEDADES_LAZY);
   cliente.setDataUltimoAcesso(Calendar.getInstance().getTime());
   cliente.setSenha(senhaCriptografadaCompleta);
   controladorCliente.atualizar(cliente);
   return "Senha Alterada com Sucesso!";
  } catch (NegocioException e) {
   LOG.error(e.getMessage(), e);
   throw new Exception("Erro ao alterar a senha, por favor verifique se os dados estão corretos.");
  }

 }

 /**
  * Inserir endereco temporario faturamento.
  *
  * @param enrederoTemporarioFaturamentoTo the enredero temporario faturamento to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "inserirEnderecoTemporarioFaturamento")
 @RequestWrapper(localName = "inserirEnderecoTemporarioFaturamentoRequest")
 @ResponseWrapper(localName = "inserirEnderecoTemporarioFaturamentoResposta")
 public void inserirEnderecoTemporarioFaturamento(
   @WebParam(name = "enderecoTemporarioFaturamentoTo") EnderecoTemporarioFaturamentoTO enrederoTemporarioFaturamentoTo)
   throws Exception {

  ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumoPorContratoPontoConsumo(
    enrederoTemporarioFaturamentoTo.getChaveContrato(), enrederoTemporarioFaturamentoTo.getChavePontoConsumo());
  Cep cep = controladorEndereco.obterCep(enrederoTemporarioFaturamentoTo.getCep().getCep());

  EnderecoTemporarioFaturamento enderecoTemporarioFaturamento = new EnderecoTemporarioFaturamento();
  enderecoTemporarioFaturamento.setCep(cep);
  enderecoTemporarioFaturamento.setContratoPontoConsumo(contratoPontoConsumo);
  enderecoTemporarioFaturamento.setNumeroImovel(enrederoTemporarioFaturamentoTo.getNumeroImovel());
  enderecoTemporarioFaturamento.setComplemento(enrederoTemporarioFaturamentoTo.getComplemento());
  enderecoTemporarioFaturamento.setEmail(enrederoTemporarioFaturamentoTo.getEmail());
  controladorEndereco.inserirEnderecoTemporarioFaturamento(enderecoTemporarioFaturamento);
 }

 /**
  * Deletar endereco temporario faturamento.
  *
  * @param enrederoTemporarioFaturamentoTo the enredero temporario faturamento to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "deletarEnderecoTemporarioFaturamento")
 @RequestWrapper(localName = "deletarEnderecoTemporarioFaturamentoRequest")
 @ResponseWrapper(localName = "deletarEnderecoTemporarioFaturamentoResposta")
 public void deletarEnderecoTemporarioFaturamento(
   @WebParam(name = "enderecoTemporarioFaturamentoTo") EnderecoTemporarioFaturamentoTO enrederoTemporarioFaturamentoTo)
   throws Exception {

  ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumoPorContratoPontoConsumo(
    enrederoTemporarioFaturamentoTo.getChaveContrato(), enrederoTemporarioFaturamentoTo.getChavePontoConsumo());

  if (contratoPontoConsumo != null) {
   EnderecoTemporarioFaturamento enderecoTemporarioFaturamento = controladorEndereco
     .obterEnderecoTemporarioFaturamento(contratoPontoConsumo);
   if (enderecoTemporarioFaturamento != null) {

    controladorEndereco.removerEnderecoTemporarioFaturamento(enderecoTemporarioFaturamento);
   }
  }
 }

 /**
  * Enviar protocolo.
  *
  * @param chamado the chamado
  * @throws Exception the exception
  */
 private void enviarProtocolo(Chamado chamado) throws Exception {

  String mensagem = montarMensagem(chamado);

  TarefaAssincronaUtil.enviarProtocolo(chamado, mensagem);

 }

 /**
  * Montar mensagem.
  *
  * @param chamado the chamado
  * @return the string
  * @throws NegocioException the negocio exception
  */
 private String montarMensagem(Chamado chamado) throws NegocioException {

  String mensagem = "";

  ParametroSistema mensagenSms = controladorParametroSistema.obterParametroPorCodigo(Constantes.MENSAGEN_SMS);

  if (StringUtils.isNotEmpty(mensagenSms.getValor())) {
   mensagem = mensagenSms.getValor() + chamado.getProtocolo().getNumeroProtocolo();
  } else {
   mensagem = "Protocolo: " + chamado.getProtocolo().getNumeroProtocolo();
  }
  return mensagem;
 }

 /**
  * Verificar diretorio arquivo dados medicao.
  *
  * @return the string
  * @throws NegocioException the negocio exception
  */
 private String verificarDiretorioArquivoDadosMedicao() throws NegocioException {

  String diretorioArquivosUpload = (String) controladorParametroSistema
    .obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_IMPORTAR_LEITURA_UPLOAD);

  File diretorioLeitura = Util.getFile(diretorioArquivosUpload);
  if (!diretorioLeitura.exists()) {
   diretorioLeitura.mkdirs();
  }

  return diretorioArquivosUpload;
 }

 /**
  * Atualizar usuario leiturista.
  *
  * @param loginUsuario the login usuario
  * @param senhaUsuario the senha usuario
  * @param novaSenha    the nova senha
  * @return the boolean
  * @throws Exception the exception
  */
 @WebMethod(operationName = "atualizarUsuarioLeiturista")
 @RequestWrapper(localName = "atualizarUsuarioLeituristaRequest")
 @ResponseWrapper(localName = "atualizarUsuarioLeituristaResponse")
 public Boolean atualizarUsuarioLeiturista(@WebParam(name = "loginUsuario") String loginUsuario,
                                           @WebParam(name = "senhaUsuario") String senhaUsuario,
                                           @WebParam(name = "novaSenha") String novaSenha) throws Exception {

  Boolean retorno = null;
  try {

   retorno = controladorUsuario.alterarSenha(loginUsuario, senhaUsuario, novaSenha, novaSenha, getDadosAuditoria());

  } catch (Exception e) {
   LOG.error(e.getStackTrace(), e);
  }

  return retorno;
 }

 /**
  * Autenticar usuario leiturista.
  *
  * @param loginUsuario the login usuario
  * @param senhaUsuario the senha usuario
  * @return the usuario TO
  * @throws Exception the exception
  */
 @WebMethod(operationName = "autenticarUsuarioLeiturista")
 @RequestWrapper(localName = "autenticarUsuarioLeituristaRequest")
 @ResponseWrapper(localName = "autenticarUsuarioLeituristaResponse")
 public UsuarioTO autenticarUsuarioLeiturista(@WebParam(name = "loginUsuario") String loginUsuario,
                                              @WebParam(name = "senhaUsuario") String senhaUsuario) throws Exception {

  UsuarioTO usuarioTO = null;
  try {
   Usuario usuario = this.montarUsuario(loginUsuario, senhaUsuario);
   usuario = controladorAcesso.autenticarUsuario(usuario);
   if (usuario != null) {
    usuarioTO = new UsuarioTO();
    usuarioTO.setLogin(usuario.getLogin());
    usuarioTO.setSenha(usuario.getSenha());
    Funcionario funcionario = (Funcionario) ServiceLocator.getInstancia().getControladorFuncionario()
      .obter(usuario.getFuncionario().getChavePrimaria());
    usuarioTO.setNome(funcionario.getNome());
    Map<String, Object> filtro = new HashMap<String, Object>();
    filtro.put("idFuncionario", funcionario.getChavePrimaria());
    Collection<Leiturista> leituristas = controladorLeiturista.consultarLeituristas(filtro);
    Leiturista leiturista = leituristas.iterator().next();
    usuarioTO.setChaveLeiturista(leiturista.getChavePrimaria());
   }
  } catch (Exception e) {
   LOG.error(e.getStackTrace(), e);
  }

  return usuarioTO;
 }

 /**
  * Montar usuario.
  *
  * @param loginUsuario the login usuario
  * @param senhaUsuario the senha usuario
  * @return the usuario
  * @throws NegocioException the negocio exception
  */
 private Usuario montarUsuario(@WebParam(name = "loginUsuario") String loginUsuario, @WebParam(name = "senhaUsuario") String senhaUsuario)
   throws NegocioException {

  Usuario usuario = new UsuarioImpl();
  usuario.setLogin(loginUsuario);
  usuario.setSenha(senhaUsuario);
  usuario.setDadosAuditoria(getDadosAuditoria());

  return usuario;
 }

 /**
  * Gets the dados auditoria.
  *
  * @return the dados auditoria
  * @throws NegocioException the negocio exception
  */
 private DadosAuditoria getDadosAuditoria() throws NegocioException {

  String enderecoIP = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP);
  ConstanteSistema constante = controladorConstanteSistema
    .obterConstantePorCodigo(Constantes.C_OPERACAO_SISTEMA_IMPORTAR_LEITURA_UPLOAD);
  return Util.getDadosAuditoria(null, controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor())), enderecoIP);
 }

 /**
  * Obter rota por leiturista.
  *
  * @param chaveLeiturista the chave leiturista
  * @return the collection
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterRotaPorLeiturista")
 @RequestWrapper(localName = "obterRotaPorLeituristaRequest")
 @ResponseWrapper(localName = "obterRotaPorLeituristaResponse")
 public Collection<RotaTO> obterRotaPorLeiturista(@WebParam(name = "chaveLeiturista") Long chaveLeiturista) throws GGASException {

  Collection<RotaTO> rotasTO = new ArrayList<RotaTO>();

  Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chaveLeiturista);

  Collection<Rota> rotas = ServiceLocator.getInstancia().getControladorRota().listarRotaPorLeiturista(leiturista);
  RotaTO rotaTO = null;
  for (Rota rota : rotas) {
   rotaTO = new RotaTO();
   rotaTO.setNumeroRota(rota.getNumeroRota());
   rotasTO.add(rotaTO);
  }

  return rotasTO;
 }


 /**
  * Obter arquivo dados medicao leiturista.
  *
  * @param loginUsuario        the login usuario
  * @param chaveLeiturista     the chave leiturista
  * @param leituraPrevistaData the data leitura prevista
  * @param numeroRota          the numero rota
  * @return the string
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterArquivoDadosMedicaoLeiturista")
 @RequestWrapper(localName = "obterArquivoDadosMedicaoLeituristaRequest")
 @ResponseWrapper(localName = "obterArquivoDadosMedicaoLeituristaResponse")
 public String obterArquivoDadosMedicaoLeiturista(@WebParam(name = "loginUsuario") String loginUsuario,
                                                  @WebParam(name = "chaveLeiturista") Long chaveLeiturista,
                                                  @WebParam(name = "dataLeituraPrevista") String leituraPrevistaData,
                                                  @WebParam(name = "numeroRota") String numeroRota) throws Exception {

  String dataLeituraPrevista = leituraPrevistaData;
  String arquivoRetorno = null;
  FileOutputStream fileOutputStream = null;

  try {

   Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chaveLeiturista);

   arquivoRetorno = controladorDadosMedicao.obterArquivoLeituraMovimentoPorLeiturista(leiturista, dataLeituraPrevista, numeroRota);

   String diretorioArquivosUpload = this.verificarDiretorioArquivoDadosMedicao();

   dataLeituraPrevista = dataLeituraPrevista.replace("/", "");
   String nomeArquivo = loginUsuario + "_" + dataLeituraPrevista;
   String nomeArquivoGravacao = diretorioArquivosUpload + nomeArquivo + ".xml";

   fileOutputStream = new FileOutputStream(nomeArquivoGravacao); //NOSONAR
   fileOutputStream.write(arquivoRetorno.getBytes());
   fileOutputStream.close();

  } catch (Exception e) {
   LOG.error(e.getStackTrace(), e);
   Logger.getLogger("ERROR").debug(e.getStackTrace());
  } finally {

   if (fileOutputStream != null) {
    fileOutputStream.close();
   }

  }

  return arquivoRetorno;
 }

 /**
  * Salvar dados medicao.
  *
  * @param arquivoDadosMedicao the arquivo dados medicao
  * @param loginUsuario        the login usuario
  * @param senhaUsuario        the senha usuario
  * @throws Exception the exception
  */
 @WebMethod(operationName = "salvarDadosMedicao")
 @RequestWrapper(localName = "salvarDadosMedicaoRequest")
 @ResponseWrapper(localName = "salvarDadosMedicaoResponse")
 public void salvarDadosMedicao(@WebParam(name = "arquivoDadosMedicao") String arquivoDadosMedicao,
                                @WebParam(name = "loginUsuario") String loginUsuario, @WebParam(name = "senhaUsuario") String senhaUsuario)
   throws Exception {

  byte[] arquivo = arquivoDadosMedicao.getBytes();

  String diretorioArquivosUpload = this.verificarDiretorioArquivoDadosMedicao();

  String nomeArquivoGravacao = diretorioArquivosUpload + Calendar.getInstance().getTimeInMillis() + "_" + "leiturista_"
    + loginUsuario + ".xml";

  FileOutputStream fileOutputStream = null;
  try {
   fileOutputStream = new FileOutputStream(nomeArquivoGravacao); //NOSONAR
   fileOutputStream.write(arquivo);
   fileOutputStream.close();
  } catch (Exception e) {
   LOG.error(e.getMessage(), e);
  } finally {
   if (fileOutputStream != null) {
    fileOutputStream.close();
   }
  }

  Map<String, String> parametros = new HashMap<String, String>();

  parametros.put("arquivo", nomeArquivoGravacao);

  ConstanteSistema constante = controladorConstanteSistema
    .obterConstantePorCodigo(Constantes.C_OPERACAO_SISTEMA_IMPORTAR_LEITURA_UPLOAD);
  Long chaveOperacao = Long.parseLong(constante.getValor());

  Operacao operacao = controladorAcesso.obterOperacaoSistema(chaveOperacao);
  Processo processo = new ProcessoImpl();
  processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
  processo.setHabilitado(true);
  processo.setOperacao(operacao);
  processo.setDescricao(operacao.getDescricao() + ": " + nomeArquivoGravacao);
  processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
  processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);

  Usuario usuario = controladorUsuario.buscar(loginUsuario);
  processo.setUsuario(usuario);
  if (usuario != null && usuario.getFuncionario() != null) {
   Funcionario funcionario = processo.getUsuario().getFuncionario();
   processo.setUsuarioExecucao(funcionario.getNome());
   if (funcionario.getUnidadeOrganizacional() != null) {
    processo.setUnidadeOrganizacionalExecucao(
            funcionario.getUnidadeOrganizacional().getDescricao());
   }
  }
  processo.setParametros(parametros);
  processo.setDiaNaoUtil(true);

  String enderecoIP = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP);
  DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(usuario, operacao, enderecoIP);
  processo.setDadosAuditoria(dadosAuditoria);
  LOG.info("################## PROCESSO INSERIDO! ##################");

  controladorProcesso.inserir(processo);

 }

	/**
	 * Obter contrato ponto consumo programacao consumo.
	 *
	 * @param numeroContrato the numero contrato
	 * @return the collection
	 * @throws Exception the exception
	 */
	@WebMethod(operationName = "obterContratoPontoConsumoProgramacaoConsumo")
	@RequestWrapper(localName = "obterContratoPontoConsumoProgramacaoConsumoRequest")
	@ResponseWrapper(localName = "obterContratoPontoConsumoProgramacaoConsumoResposta")
	public Collection<PontoConsumoDescricaoTO> obterContratoPontoConsumoProgramacaoConsumo(
			@WebParam(name = "numeroContrato") String numeroContrato) throws Exception {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(numeroContrato)) {
			filtro.put("numeroContrato", numeroContrato);
		}

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
				controladorContrato.consultarContratoPontoConsumoProgramacaoConsumo(filtro, true);

		return PontoConsumoDescricaoUtil.converter(listaContratoPontoConsumo);
	}

 /**
  * Converter lista solicitacao consumo TO.
  *
  * @param hashMapSolicitacaoConsumoPontoConsumo the hash map solicitacao consumo ponto consumo
  * @return the map
  * @throws Exception the exception
  */
 private Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> converterListaSolicitacaoConsumoTO(
   Map<Long, Collection<SolicitacaoConsumoPontoConsumo>> hashMapSolicitacaoConsumoPontoConsumo) throws Exception {

  Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
    hashMapSolicitacaoConsumoPontoConsumoTO =
    new HashMap<Long, List<SolicitacaoConsumoPontoConsumoTO>>();

  for (Entry<Long, Collection<SolicitacaoConsumoPontoConsumo>> entry : hashMapSolicitacaoConsumoPontoConsumo.entrySet()) {

   Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = entry.getValue();

   List<SolicitacaoConsumoPontoConsumoTO> solicitacaoConsumoTO = converterSolicitacaoConsumoTO(listaSolicitacaoConsumo);

   hashMapSolicitacaoConsumoPontoConsumoTO.put(entry.getKey(), solicitacaoConsumoTO);

  }

  return hashMapSolicitacaoConsumoPontoConsumoTO;

 }

 /**
  * Converter solicitacao consumo to.
  *
  * @param listaSolicitacaoConsumo the lista solicitacao consumo
  * @return the list
  * @throws Exception the exception
  */
 private List<SolicitacaoConsumoPontoConsumoTO> converterSolicitacaoConsumoTO(
   Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo) throws Exception {

  List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO = new ArrayList<SolicitacaoConsumoPontoConsumoTO>();

  if (listaSolicitacaoConsumo != null && !listaSolicitacaoConsumo.isEmpty()) {

   for (SolicitacaoConsumoPontoConsumo solicitacao : listaSolicitacaoConsumo) {

    SolicitacaoConsumoPontoConsumoTO solicitacaoTO = new SolicitacaoConsumoPontoConsumoTO();

    if (solicitacao.getDataSolicitacao() != null) {
     solicitacaoTO.setDateSolicitacao(solicitacao.getDataSolicitacao());

     solicitacaoTO.setDataSolicitacao(Util.converterDataParaStringSemHora(solicitacao.getDataSolicitacao(),
       Constantes.FORMATO_DATA_BR));

     solicitacaoTO.setDataFormatada(Util.converterCampoDataParaStringComDiaDaSemana(solicitacao.getDataSolicitacao()));
    }

    if (solicitacao.getValorQDC() != null) {
     solicitacaoTO.setValorQDC(solicitacao.getValorQDC());
    }

    if (solicitacao.getValorQDS() != null) {
     solicitacaoTO.setValorQDS(solicitacao.getValorQDS());
    }

    if (solicitacao.getValorQPNR() != null) {
     solicitacaoTO.setValorQPNR(solicitacao.getValorQPNR());
    }

    if (solicitacao.getValorQDP() != null) {
     solicitacaoTO.setValorQDP(solicitacao.getValorQDP());
    }

    if (solicitacao.getDataOperacao() != null) {
     solicitacaoTO.setDataOperacao(solicitacao.getDataOperacao());
    }

    if (solicitacao.getIndicadorAceite() != null) {
     if ((solicitacao.getValorQDS() == null) && (solicitacao.getValorQPNR() == null)) {
      solicitacaoTO.setIndicadorAceite(Boolean.FALSE);
     } else {
      solicitacaoTO.setIndicadorAceite(solicitacao.getIndicadorAceite());
     }
    } else {
     if (solicitacao.getDescricaoMotivoNaoAceite() != null) {
      solicitacaoTO.setDescricaoMotivoNaoAceite(solicitacao.getDescricaoMotivoNaoAceite());
     }
    }

    if (solicitacao.getUsuario() != null) {
     solicitacaoTO.setNomeUsuario(solicitacao.getUsuario().getLogin());
    }

    if (solicitacao.getChamado() != null) {
     solicitacaoTO.setNumeroProtocolo(solicitacao.getChamado().getProtocolo().getNumeroProtocolo());
    }

    if (solicitacao.getIndicadorTipoProgramacaoConsumo() != null) {
     solicitacaoTO.setIndicadorTipoProgramacao(solicitacao.getIndicadorTipoProgramacaoConsumo());
    }

    if (solicitacao.getUltimaAlteracao() != null) {
     solicitacaoTO.setUltimaAlteracao(solicitacao.getUltimaAlteracao());
    }

    if (solicitacao.isHabilitado()) {
     solicitacaoTO.setHabilitado("1");
    } else {
     solicitacaoTO.setHabilitado("0");
    }

    solicitacaoTO.setIndicadorEdit(true);

    ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = solicitacao.getContratoPontoConsumoModalidade();
    Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = contratoPontoConsumoModalidade
      .getListaContratoPontoConsumoPenalidade();

    Map<Long, ContratoPontoConsumoPenalidade> hashmapContratoPontoConsumoPenalidade = Util
      .trasformarParaMapa(listaContratoPontoConsumoPenalidade);

    ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeAMaior = hashmapContratoPontoConsumoPenalidade
      .get(getValorConstanteRetiradaAMaior());
    ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeAMenor = hashmapContratoPontoConsumoPenalidade
      .get(getValorConstanteRetiradaAMenor());

    solicitacaoTO.setIdContrato(contratoPontoConsumoModalidade.getContratoPontoConsumo().getContrato().getChavePrimaria());

    solicitacaoTO.setIdContratoPontoConsumoModalidade(contratoPontoConsumoModalidade.getChavePrimaria());

    solicitacaoTO.setSequenciaApuracaoVolume(contratoPontoConsumoModalidade.getContratoModalidade()
      .getSequenciaApuracaoVolume());

    BigDecimal percentualVarSuperiorQDC = contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC();
    BigDecimal valorQDC = solicitacaoTO.getValorQDC();

    if (percentualVarSuperiorQDC != null) {
     solicitacaoTO.setPercentualVarSuperiorQDC(percentualVarSuperiorQDC);
     if (valorQDC != null) {
      solicitacaoTO.setValorQDCMaxima(NumeroUtil.adicionarPercentual(valorQDC, percentualVarSuperiorQDC));
     }
    } else {
     solicitacaoTO.setPercentualVarSuperiorQDC(BigDecimal.ZERO);
     solicitacaoTO.setValorQDCMaxima(valorQDC);
    }

    if (contratoPontoConsumoPenalidadeAMaior != null) {
     solicitacaoTO.setValorPercentualCobRetMaior(contratoPontoConsumoPenalidadeAMaior.getValorPercentualCobRetMaiorMenor());
     solicitacaoTO.setValorPercentualRetMaior(contratoPontoConsumoPenalidadeAMaior.getValorPercentualRetMaiorMenor());
    }

    if (contratoPontoConsumoPenalidadeAMenor != null) {
     solicitacaoTO.setValorPercentualCobRetMenor(contratoPontoConsumoPenalidadeAMenor.getValorPercentualCobRetMaiorMenor());
     solicitacaoTO.setValorPercentualRetMenor(contratoPontoConsumoPenalidadeAMenor.getValorPercentualRetMaiorMenor());
    }

    listaSolicitacaoConsumoTO.add(solicitacaoTO);
   }
  }

  return listaSolicitacaoConsumoTO;
 }

 /**
  * Listar solicitacao consumo ponto consumo to.
  *
  * @param idPontoConsumo         the id ponto consumo
  * @param chaveModalidadeConsumo the chave modalidade consumo
  * @param ano                    the ano
  * @param mes                    the mes
  * @return the list
  * @throws Exception the exception
  */
 @WebMethod(operationName = "listarSolicitacaoConsumoPontoConsumoTO")
 @RequestWrapper(localName = "listarSolicitacaoConsumoPontoConsumoTORequest")
 @ResponseWrapper(localName = "listarSolicitacaoConsumoPontoConsumoTOResposta")
 public List<SolicitacaoConsumoPontoConsumoTO> listarSolicitacaoConsumoPontoConsumoTO(
   @WebParam(name = "idPontoConsumo") Long idPontoConsumo,
   @WebParam(name = "chaveModalidadeConsumo") Long chaveModalidadeConsumo, @WebParam(name = "ano") Integer ano,
   @WebParam(name = "mes") Integer mes) throws Exception {

  ContratoPontoConsumo contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
  Collection<ContratoPontoConsumoModalidade> listaPontoConsumoModadelidade = controladorContrato
    .listarContratoPontoConsumoModalidadePorContratoPontoConsumo(contratoPonto.getChavePrimaria());
  Collection<ContratoPontoConsumoModalidade> listaManipulacao = new ArrayList<ContratoPontoConsumoModalidade>();
  listaManipulacao.addAll(listaPontoConsumoModadelidade);
  if (chaveModalidadeConsumo != null) {
   for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaManipulacao) {
    if (contratoPontoConsumoModalidade.getChavePrimaria() != chaveModalidadeConsumo) {
     listaPontoConsumoModadelidade.remove(contratoPontoConsumoModalidade);
    }
   }
  }

  Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorProgramacao.listarSolicitacaoConsumoPontoConsumo(
    contratoPonto, listaPontoConsumoModadelidade, ano, mes);

  List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTo = this.converterSolicitacaoConsumoTO(listaSolicitacaoConsumo);

  this.adicionarPenalidadeListaSolicitacaoConsumoTO(listaSolicitacaoConsumoTo, contratoPonto);

  return listaSolicitacaoConsumoTo;
 }

 /**
  * Listar solicitacao consumo ponto consumo to.
  *
  * @param idPontoConsumo         the id ponto consumo
  * @param chaveModalidadeConsumo the chave modalidade consumo
  * @param ano                    the ano
  * @param mes                    the mes
  * @return the list
  * @throws Exception the exception
  */
 @WebMethod(operationName = "listarSolicitacaoConsumoPontoConsumoVariacaoQdcTO")
 @RequestWrapper(localName = "listarSolicitacaoConsumoPontoConsumoVariacaoQdcTORequest")
 @ResponseWrapper(localName = "listarSolicitacaoConsumoPontoConsumoVariacaoQdcTOResposta")
 public List<SolicitacaoConsumoPontoConsumoTO> listarSolicitacaoConsumoPontoConsumoVariacaoQdcTO(
   @WebParam(name = "idPontoConsumo") Long idPontoConsumo,
   @WebParam(name = "chaveModalidadeConsumo") Long chaveModalidadeConsumo, @WebParam(name = "ano") Integer ano,
   @WebParam(name = "mes") Integer mes) throws Exception {

  ContratoPontoConsumo contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);

  Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
    hashMapSolicitacaoConsumoPontoConsumoTO =
    listarContratoPontoConsumoModalidadeAgrupadoPorIdContratoPontoConsumo(contratoPonto, ano, mes);

  this.distribuirQDR(contratoPonto.getPontoConsumo(), hashMapSolicitacaoConsumoPontoConsumoTO);

  List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTo = null;

  if (NumeroUtil.maiorQueZero(chaveModalidadeConsumo)) {

   listaSolicitacaoConsumoTo = hashMapSolicitacaoConsumoPontoConsumoTO.get(chaveModalidadeConsumo);

   this.adicionarPenalidadeListaSolicitacaoConsumoTO(listaSolicitacaoConsumoTo, contratoPonto);

   for (SolicitacaoConsumoPontoConsumoTO solicitacaoUnicaTo : listaSolicitacaoConsumoTo) {
    if (NumeroUtil.nulo(solicitacaoUnicaTo.getValorQDC())) {
     solicitacaoUnicaTo.setValorQDC(controladorContrato.obterQdcContratoValidoPorData(
       solicitacaoUnicaTo.getDateSolicitacao(), solicitacaoUnicaTo.getIdContrato()));
    }

    BigDecimal percentualVarSuperiorQDC = solicitacaoUnicaTo.getPercentualVarSuperiorQDC();

    if (NumeroUtil.naoNulo(percentualVarSuperiorQDC)) {
     solicitacaoUnicaTo.setValorQDCMaxima(NumeroUtil.adicionarPercentual(solicitacaoUnicaTo.getValorQDC(),
       percentualVarSuperiorQDC));
    } else {
     solicitacaoUnicaTo.setValorQDCMaxima(solicitacaoUnicaTo.getValorQDC());
    }
   }

  } else {

   listaSolicitacaoConsumoTo = montarListaSolicitacaoConsumoPontoConsumoInclusao(hashMapSolicitacaoConsumoPontoConsumoTO);

  }

  return listaSolicitacaoConsumoTo;

 }

 /**
  * Listar contrato ponto consumo modalidade agrupado por id contrato ponto consumo.
  *
  * @param contratoPonto the contrato ponto
  * @param ano           the ano
  * @param mes           the mes
  * @return the map
  * @throws Exception the exception
  */
 private Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> listarContratoPontoConsumoModalidadeAgrupadoPorIdContratoPontoConsumo(
   ContratoPontoConsumo contratoPonto, Integer ano, Integer mes) throws Exception {

  Collection<ContratoPontoConsumoModalidade> listaPontoConsumoModalidade = controladorContrato
    .listarContratoPontoConsumoModalidadePorContratoPontoConsumo(contratoPonto.getChavePrimaria());

  Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>
    hashMapSolicitacaoConsumoPontoConsumo =
    listarSolicitacaoConsumoPontoConsumoMapeadoPorIdContratoPontoConsumoModalidade
      (contratoPonto, listaPontoConsumoModalidade, ano, mes);

  return this.converterListaSolicitacaoConsumoTO(hashMapSolicitacaoConsumoPontoConsumo);
 }

 /**
  * Montar lista solicitacao consumo ponto consumo inclusao.
  *
  * @param hashMapSolicitacaoConsumoPontoConsumoTO the hash map solicitacao consumo ponto consumo TO
  * @return the list
  * @throws GGASException the GGAS exception
  */
 private List<SolicitacaoConsumoPontoConsumoTO> montarListaSolicitacaoConsumoPontoConsumoInclusao(
   Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> hashMapSolicitacaoConsumoPontoConsumoTO)
   throws GGASException {

  List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO =
    new ArrayList<SolicitacaoConsumoPontoConsumoTO>();

  Map<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> agruparSolicitacaoConsumoPontoConsumoToPorData =
    agruparSolicitacaoConsumoPontoConsumoToPorData(hashMapSolicitacaoConsumoPontoConsumoTO);

  Set<Entry<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>>> entrySet = agruparSolicitacaoConsumoPontoConsumoToPorData
    .entrySet();

  for (Entry<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> entry : entrySet) {

   SolicitacaoConsumoPontoConsumoTO solicitacaoTO = new SolicitacaoConsumoPontoConsumoTO();

   solicitacaoTO.setIndicadorEdit(true);

   String dataSolicitacao = entry.getKey();
   Date dateSolicitacao;
   dateSolicitacao = DataUtil.converterParaData(dataSolicitacao);

   solicitacaoTO.setDateSolicitacao(dateSolicitacao);

   solicitacaoTO.setDataSolicitacao(dataSolicitacao);

   solicitacaoTO.setDataFormatada(DataUtil.converterCampoDataParaStringComDiaDaSemana(dateSolicitacao));

   List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoPontoConsumoTO = entry.getValue();

   BigDecimal valorQdcTotal = BigDecimal.ZERO;
   BigDecimal valorQdcMaximaTotal = BigDecimal.ZERO;
   BigDecimal valorQdsTotal = BigDecimal.ZERO;
   BigDecimal valorQdpTotal = BigDecimal.ZERO;
   BigDecimal valorQpnrTotal = BigDecimal.ZERO;
   BigDecimal valorQrTotal = BigDecimal.ZERO;
   BigDecimal valorPenalidadeMaiorTotal = BigDecimal.ZERO;
   BigDecimal valorPenalidadeMenorTotal = BigDecimal.ZERO;

   if (CollectionUtils.hasUniqueObject(listaSolicitacaoConsumoPontoConsumoTO)) {

    SolicitacaoConsumoPontoConsumoTO solicitacaoUnicaTo = Util.primeiroElemento(listaSolicitacaoConsumoPontoConsumoTO);

    solicitacaoTO.setIndicadorTipoProgramacao(solicitacaoUnicaTo.getIndicadorTipoProgramacao());
    valorQdcTotal = solicitacaoUnicaTo.getValorQDC();
    if (NumeroUtil.nulo(valorQdcTotal)) {
     valorQdcTotal = controladorContrato.obterQdcContratoValidoPorData(dateSolicitacao, solicitacaoUnicaTo.getIdContrato());
    }

    BigDecimal percentualVarSuperiorQDC = solicitacaoUnicaTo.getPercentualVarSuperiorQDC();
    if (NumeroUtil.naoNulo(percentualVarSuperiorQDC)) {
     valorQdcMaximaTotal = NumeroUtil.adicionarPercentual(valorQdcTotal, percentualVarSuperiorQDC);
    } else {
     valorQdcMaximaTotal = valorQdcTotal;
    }

    valorQdsTotal = solicitacaoUnicaTo.getValorQDS();
    valorQdpTotal = solicitacaoUnicaTo.getValorQDP();
    valorQpnrTotal = solicitacaoUnicaTo.getValorQPNR();
    valorQrTotal = solicitacaoUnicaTo.getValorQR();
    valorPenalidadeMaiorTotal = solicitacaoUnicaTo.getValorPenalidadeMaior();
    valorPenalidadeMenorTotal = solicitacaoUnicaTo.getValorPenalidadeMenor();

   } else {
    for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : listaSolicitacaoConsumoPontoConsumoTO) {
     solicitacaoTO.setIndicadorTipoProgramacao(solicitacaoConsumoPontoConsumoTO.getIndicadorTipoProgramacao());
     valorQdcTotal = valorQdcTotal.add(solicitacaoConsumoPontoConsumoTO.getValorQDC());
     valorQdcMaximaTotal = valorQdcMaximaTotal.add(solicitacaoConsumoPontoConsumoTO.getValorQDCMaxima());

     valorQdsTotal = incrementar(valorQdsTotal, solicitacaoConsumoPontoConsumoTO.getValorQDS());
     valorQdpTotal = incrementar(valorQdpTotal, solicitacaoConsumoPontoConsumoTO.getValorQDP());
     valorQpnrTotal = incrementar(valorQpnrTotal, solicitacaoConsumoPontoConsumoTO.getValorQPNR());
     valorQrTotal = incrementar(valorQrTotal, solicitacaoConsumoPontoConsumoTO.getValorQR());
     valorPenalidadeMaiorTotal = incrementar(valorPenalidadeMaiorTotal, solicitacaoConsumoPontoConsumoTO.getValorPenalidadeMaior());
     valorPenalidadeMenorTotal = incrementar(valorPenalidadeMenorTotal, solicitacaoConsumoPontoConsumoTO.getValorPenalidadeMenor());
    }
   }

   solicitacaoTO.setValorQDC(valorQdcTotal);
   solicitacaoTO.setValorQDCMaxima(valorQdcMaximaTotal);
   solicitacaoTO.setValorQDS(valorQdsTotal);
   solicitacaoTO.setValorQDP(valorQdpTotal);
   solicitacaoTO.setValorQPNR(valorQpnrTotal);
   solicitacaoTO.setValorQR(valorQrTotal);
   solicitacaoTO.setValorPenalidadeMaior(valorPenalidadeMaiorTotal);
   solicitacaoTO.setValorPenalidadeMenor(valorPenalidadeMenorTotal);

   listaSolicitacaoConsumoTO.add(solicitacaoTO);

  }

  Collections.sort(listaSolicitacaoConsumoTO);

  return listaSolicitacaoConsumoTO;
 }

 private BigDecimal incrementar(BigDecimal total, BigDecimal incremento) {
  BigDecimal retorno = total;
  if (incremento == null) {
   retorno = null;
  } else {
   retorno = retorno.add(incremento);
  }
  return retorno;
 }

 /**
  * Listar solicitacao consumo ponto consumo mapeado por id contrato ponto consumo modalidade.
  *
  * @param contratoPonto               the contrato ponto
  * @param listaPontoConsumoModalidade the lista ponto consumo modalidade
  * @param ano                         the ano
  * @param mes                         the mes
  * @return the map
  * @throws NegocioException the negocio exception
  */
 private Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>
 listarSolicitacaoConsumoPontoConsumoMapeadoPorIdContratoPontoConsumoModalidade(ContratoPontoConsumo contratoPonto,
                               Collection<ContratoPontoConsumoModalidade> listaPontoConsumoModalidade, Integer ano,
                               Integer mes) throws NegocioException {

  Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>
    listasSolicitacaoConsumo =
    new HashMap<Long, Collection<SolicitacaoConsumoPontoConsumo>>();

  for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaPontoConsumoModalidade) {

   Collection<ContratoPontoConsumoModalidade> listaManipulacao = new ArrayList<ContratoPontoConsumoModalidade>();

   listaManipulacao.add(contratoPontoConsumoModalidade);

   Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorProgramacao
     .listarSolicitacaoConsumoPontoConsumo(contratoPonto, listaManipulacao, ano, mes);

   listasSolicitacaoConsumo.put(contratoPontoConsumoModalidade.getChavePrimaria(), listaSolicitacaoConsumo);

  }

  return listasSolicitacaoConsumo;
 }

 /**
  * Distribuir QDR.
  *
  * @param pontoConsumo                            the ponto consumo
  * @param hashMapSolicitacaoConsumoPontoConsumoTO the hash map solicitacao consumo ponto consumo TO
  * @throws NegocioException the negocio exception
  */
 private void distribuirQDR(PontoConsumo pontoConsumo,
                            Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> hashMapSolicitacaoConsumoPontoConsumoTO)
   throws NegocioException {

  Map<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>>
    agruparSolicitacaoConsumoPontoConsumoToPorData =
    agruparSolicitacaoConsumoPontoConsumoToPorData(hashMapSolicitacaoConsumoPontoConsumoTO);

  for (Entry<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> entry : agruparSolicitacaoConsumoPontoConsumoToPorData.entrySet()) {

   String dataSolicitacao = entry.getKey();

   BigDecimal valorTotal = null;

   HistoricoConsumo historicoConsumo = controladorHistoricoConsumo.obterHistoricoConsumoPorPontoEData(
     pontoConsumo.getChavePrimaria(), DataUtil.converterParaData(dataSolicitacao));

   if (historicoConsumo != null && historicoConsumo.getConsumoApurado() != null) {
    valorTotal = historicoConsumo.getConsumoApurado();
   } else {
    valorTotal = obterQDR(pontoConsumo, dataSolicitacao);
   }

   if (valorTotal != null) {
    List<SolicitacaoConsumoPontoConsumoTO> value = entry.getValue();
    Collections.sort(value);
    // preencher ate QDS
    for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : value) {
     BigDecimal valorQdp = solicitacaoConsumoPontoConsumoTO.getValorQDP();
     if (valorQdp == null) {
      valorQdp = BigDecimal.ZERO;
     }
     if (NumeroUtil.maiorQue(valorTotal, valorQdp)) {
      solicitacaoConsumoPontoConsumoTO.adicionarValorQR(valorQdp);
      valorTotal = valorTotal.subtract(valorQdp);
     } else {
      solicitacaoConsumoPontoConsumoTO.adicionarValorQR(valorTotal);
      valorTotal = BigDecimal.ZERO;
     }
    }
    // preencher qte QDS tolerado caso haja sobras
    if (NumeroUtil.maiorQueZero(valorTotal)) {
     for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : value) {
      BigDecimal valorPercentualRetMaior = solicitacaoConsumoPontoConsumoTO.getValorPercentualRetMaior();
      if (valorPercentualRetMaior == null) {
       valorPercentualRetMaior = BigDecimal.ZERO;
      }
      BigDecimal valorQdp = solicitacaoConsumoPontoConsumoTO.getValorQDP();
      if (valorQdp == null) {
       valorQdp = BigDecimal.ZERO;
      }
      BigDecimal valorQdsTolerado = valorQdp.multiply(valorPercentualRetMaior);
      if (NumeroUtil.maiorQue(valorTotal, valorQdsTolerado)) {
       solicitacaoConsumoPontoConsumoTO.adicionarValorQR(valorQdsTolerado);
       valorTotal = valorTotal.subtract(valorQdsTolerado);
      } else {
       solicitacaoConsumoPontoConsumoTO.adicionarValorQR(valorTotal);
       valorTotal = BigDecimal.ZERO;
      }
     }
    }
    // adicionar o restante a QDR da modalidade com maior precedencia
    if (NumeroUtil.maiorQueZero(valorTotal)) {
     SolicitacaoConsumoPontoConsumoTO solicitacaoComModalidadeMaiorPrecedencia = Util.primeiroElemento(value);
     solicitacaoComModalidadeMaiorPrecedencia.adicionarValorQR(valorTotal);
    }
   }
  }
 }

 private BigDecimal obterQDR(PontoConsumo pontoConsumo, String dataSolicitacao) throws NegocioException {
  BigDecimal qdr;
  if (pontoConsumo.getCodigoPontoConsumoSupervisorio() != null) {
   TempSolicitacaoConsumo consumo = controladorProgramacao.obterTempSolicitacaoConsmo(
     pontoConsumo.getCodigoPontoConsumoSupervisorio(),
     DataUtil.converterParaData(dataSolicitacao));
   if (consumo != null) {
    qdr = consumo.getValorQR();
   } else {
    qdr = null;
   }
  } else {
   Medidor virtual = controladorMedidor.obterInstalacaoMedidorPontoConsumo(
     pontoConsumo.getChavePrimaria()).getMedidor();
   Collection<Medidor> independentes = controladorMedidor.listaMedidoresQueCompoemMedidorVirtual(virtual);
   qdr = BigDecimal.ZERO;
   for (Medidor independente : independentes) {
    TempSolicitacaoConsumo consumo = controladorProgramacao.obterTempSolicitacaoConsmo(
      independente.getCodigoMedidorSupervisorio(), DataUtil.converterParaData(dataSolicitacao));
    if (consumo == null) {
     qdr = null;
    } else {
     if (virtual.getComposicaoVirtual().contains("+" + independente.getChavePrimaria())) {
      qdr = qdr.add(consumo.getValorQR());
     } else {
      qdr = qdr.subtract(consumo.getValorQR());
     }
    }
   }
  }
  return qdr;
 }

 /**
  * Agrupar solicitacao consumo ponto consumo to por data.
  *
  * @param hashMapSolicitacaoConsumoPontoConsumoTO the hash map solicitacao consumo ponto consumo TO
  * @return the map
  */
 private Map<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> agruparSolicitacaoConsumoPontoConsumoToPorData(
   Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> hashMapSolicitacaoConsumoPontoConsumoTO) {

  Map<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>>
    agruparSolicitacaoConsumoPontoConsumoToPorData =
    new HashMap<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>>();

  Set<Entry<Long, List<SolicitacaoConsumoPontoConsumoTO>>> entrySet = hashMapSolicitacaoConsumoPontoConsumoTO.entrySet();

  for (Entry<Long, List<SolicitacaoConsumoPontoConsumoTO>> entry : entrySet) {
   List<SolicitacaoConsumoPontoConsumoTO> value = entry.getValue();
   for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : value) {
    String dataSolicitacao = solicitacaoConsumoPontoConsumoTO.getDataSolicitacao();
    List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoPontoConsumoTO = agruparSolicitacaoConsumoPontoConsumoToPorData
      .get(dataSolicitacao);
    if (listaSolicitacaoConsumoPontoConsumoTO == null) {
     listaSolicitacaoConsumoPontoConsumoTO = new ArrayList<SolicitacaoConsumoPontoConsumoTO>();
     agruparSolicitacaoConsumoPontoConsumoToPorData.put(dataSolicitacao,
       (ArrayList<SolicitacaoConsumoPontoConsumoTO>) listaSolicitacaoConsumoPontoConsumoTO);
    }
    listaSolicitacaoConsumoPontoConsumoTO.add(solicitacaoConsumoPontoConsumoTO);
   }
  }
  return agruparSolicitacaoConsumoPontoConsumoToPorData;
 }

 /**
  * Agrupar solicitacao consumo ponto consumo to por id contrato ponto consumo modalidade.
  *
  * @param hashMapSolicitacaoConsumoPontoConsumoTO the hash map solicitacao consumo ponto consumo TO
  * @return the map
  */
 private Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
 agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade(
   Map<Long, List<SolicitacaoConsumoPontoConsumoTO>> hashMapSolicitacaoConsumoPontoConsumoTO) {

  Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
    agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade =
    new HashMap<Long, List<SolicitacaoConsumoPontoConsumoTO>>();

  Set<Entry<Long, List<SolicitacaoConsumoPontoConsumoTO>>> entrySet = hashMapSolicitacaoConsumoPontoConsumoTO.entrySet();

  for (Entry<Long, List<SolicitacaoConsumoPontoConsumoTO>> entry : entrySet) {
   List<SolicitacaoConsumoPontoConsumoTO> value = entry.getValue();
   for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : value) {

    Long idContratoPontoConsumoModalidade = solicitacaoConsumoPontoConsumoTO.getIdContratoPontoConsumoModalidade();

    List<SolicitacaoConsumoPontoConsumoTO>
      listaSolicitacaoConsumoPontoConsumoTO =
      agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade
        .get(idContratoPontoConsumoModalidade);

    if (listaSolicitacaoConsumoPontoConsumoTO == null) {
     listaSolicitacaoConsumoPontoConsumoTO = new ArrayList<SolicitacaoConsumoPontoConsumoTO>();
     agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade.put(idContratoPontoConsumoModalidade,
       listaSolicitacaoConsumoPontoConsumoTO);
    }
    listaSolicitacaoConsumoPontoConsumoTO.add(solicitacaoConsumoPontoConsumoTO);
   }
  }
  return agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade;
 }

 /**
  * Gets the valor constante retirada A menor.
  *
  * @return the valor constante retirada A menor
  * @throws NegocioException the negocio exception
  */
 private Long getValorConstanteRetiradaAMenor() throws NegocioException {

  return ServiceLocator.getInstancia().getControladorConstanteSistema().obterValorConstantePenalidadeRetiradaAMenor();
 }

 /**
  * Gets the valor constante retirada A maior.
  *
  * @return the valor constante retirada A maior
  * @throws NegocioException the negocio exception
  */
 private Long getValorConstanteRetiradaAMaior() throws NegocioException {

  return ServiceLocator.getInstancia().getControladorConstanteSistema().obterValorConstanteRetiradaAMaior();
 }

 /**
  * Atualizar solicitacao consumo ponto consumo.
  *
  * @param listaSolicitacaoConsumoTO the lista solicitacao consumo to
  * @param idPontoConsumo            the id ponto consumo
  * @param ano                       the ano
  * @param mes                       the mes
  * @param dadosSolicitanteTO        the dados solicitante to
  * @param chaveModaliade            the chave modaliade
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "atualizarSolicitacaoConsumoPontoConsumo")
 @RequestWrapper(localName = "atualizarSolicitacaoConsumoPontoConsumoRequest")
 @ResponseWrapper(localName = "atualizarSolicitacaoConsumoPontoConsumoResposta")
 public Long atualizarSolicitacaoConsumoPontoConsumo(
   @WebParam(name = "listaSolicitacaoConsumo") Collection<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO,
   @WebParam(name = "idPontoConsumo") Long idPontoConsumo, @WebParam(name = "ano") Integer ano,
   @WebParam(name = "mes") Integer mes, @WebParam(name = "dadosSolicitanteTO") DadosSolicitanteTO dadosSolicitanteTO,
   @WebParam(name = "chaveModaliade") Long chaveModaliade) throws Exception {

  Chamado chamado = null;

  StringBuilder dadosImovel = this.popularDadosImovelSolicitante(dadosSolicitanteTO);

  chamado = this.popularChamado(dadosSolicitanteTO, dadosImovel, Constantes.C_CHAMADO_ASSUNTO_AGV_PROGRAMACAO_CONSUMO);

  PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);
  ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

  Map<String, SolicitacaoConsumoPontoConsumoTO> hashListaSolicitacaoConsumoTO = Util
    .trasformarParaMapa(listaSolicitacaoConsumoTO);

  Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
    hashMapSolicitacaoConsumoPontoConsumoTO =
    listarContratoPontoConsumoModalidadeAgrupadoPorIdContratoPontoConsumo(
      contratoPontoConsumo, ano, mes);

  Map<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> agruparSolicitacaoConsumoPontoConsumoToPorData =
    agruparSolicitacaoConsumoPontoConsumoToPorData(hashMapSolicitacaoConsumoPontoConsumoTO);

  for (Entry<String, ArrayList<SolicitacaoConsumoPontoConsumoTO>> entry : agruparSolicitacaoConsumoPontoConsumoToPorData.entrySet()) {

   String dataSolicitacao = entry.getKey();

   SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoToTela = hashListaSolicitacaoConsumoTO.get(dataSolicitacao);
   BigDecimal valorQdsTotal = solicitacaoConsumoPontoConsumoToTela.getValorQDS();

   List<SolicitacaoConsumoPontoConsumoTO> value = entry.getValue();
   Collections.sort(value);
   for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : value) {
    if (valorQdsTotal != null) {

     solicitacaoConsumoPontoConsumoTO.setValorQDS(BigDecimal.ZERO);
     solicitacaoConsumoPontoConsumoTO.setIndicadorTipoProgramacao(
       solicitacaoConsumoPontoConsumoToTela.getIndicadorTipoProgramacao());
     if (solicitacaoConsumoPontoConsumoToTela.getIndicadorEdit()) {
      solicitacaoConsumoPontoConsumoTO.setIndicadorAceite(null);
     }

     if (NumeroUtil.maiorQueZero(valorQdsTotal)) {

      BigDecimal valorQdcMaxima = solicitacaoConsumoPontoConsumoTO.getValorQDCMaxima();

      if (NumeroUtil.maiorQue(valorQdsTotal, valorQdcMaxima)) {
       solicitacaoConsumoPontoConsumoTO.setValorQDS(valorQdcMaxima);
       valorQdsTotal = valorQdsTotal.subtract(valorQdcMaxima);
      } else {
       solicitacaoConsumoPontoConsumoTO.setValorQDS(valorQdsTotal);
       valorQdsTotal = BigDecimal.ZERO;
      }
     }
     if (NumeroUtil.maiorQueZero(valorQdsTotal)) {
      SolicitacaoConsumoPontoConsumoTO solicitacaoComModalidadeMenorPrecedencia = Util.ultimoElemento(value);
      solicitacaoComModalidadeMenorPrecedencia.adicionarValorQDS(valorQdsTotal);
     }
    }
   }

  }

  Map<Long, List<SolicitacaoConsumoPontoConsumoTO>>
    solicitacaoConsumoPontoConsumoToAgrupadoPorIdContratoPontoConsumoModalidade =
    agruparSolicitacaoConsumoPontoConsumoToPorIdContratoPontoConsumoModalidade(hashMapSolicitacaoConsumoPontoConsumoTO);

  for (Entry<Long, List<SolicitacaoConsumoPontoConsumoTO>> entry :
    solicitacaoConsumoPontoConsumoToAgrupadoPorIdContratoPontoConsumoModalidade.entrySet()) {
   Long idContratoPontoConsumoModalidade = entry.getKey();

   ContratoPontoConsumoModalidade contratoPontoConsumoModalide = controladorContrato
     .obterContratoPontoConsumoModalidade(idContratoPontoConsumoModalidade);

   List<SolicitacaoConsumoPontoConsumoTO> value = entry.getValue();

   Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao = this.converterSolicitacaoConsumo(value, idPontoConsumo, chamado,
     contratoPontoConsumoModalide);

   controladorProgramacao.atualizarListaSolicitacaoConsumoPontoConsumo(listaSolicitacao, idPontoConsumo, ano, mes,
     contratoPontoConsumoModalide.getChavePrimaria());
  }

  return chamado.getProtocolo().getNumeroProtocolo();

 }

 /**
  * Converter solicitacao consumo.
  *
  * @param listaSolicitacaoConsumoTO      the lista solicitacao consumo to
  * @param idPontoConsumo                 the id ponto consumo
  * @param chamado                        the chamado
  * @param contratoPontoConsumoModalidade the contrato ponto consumo modalidade
  * @return the collection
  * @throws Exception the exception
  */
 private Collection<SolicitacaoConsumoPontoConsumo> converterSolicitacaoConsumo(
   Collection<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO, Long idPontoConsumo, Chamado chamado,
   ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws Exception {

  Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao = new ArrayList<SolicitacaoConsumoPontoConsumo>();
  PontoConsumo pontoConsumo = null;
  ContratoPontoConsumoModalidade contratoPontoModalidade = null;
  if (idPontoConsumo != null && idPontoConsumo > 0) {
   pontoConsumo = controladorPontoConsumo.buscarPontoConsumo(idPontoConsumo);
  }
  if (listaSolicitacaoConsumoTO != null && !listaSolicitacaoConsumoTO.isEmpty()) {

   for (SolicitacaoConsumoPontoConsumoTO solicitacaoTO : listaSolicitacaoConsumoTO) {

    SolicitacaoConsumoPontoConsumo solicitacao = new SolicitacaoConsumoPontoConsumoImpl();

    solicitacao.setPontoConsumo(pontoConsumo);
    solicitacao.setContratoPontoConsumoModalidade(contratoPontoModalidade);

    if (solicitacaoTO.getDataSolicitacao() != null) {

     solicitacao.setDataSolicitacao(Util.converterCampoStringParaData("Data", solicitacaoTO.getDataSolicitacao(),
       Constantes.FORMATO_DATA_BR));
    }

    if (NumeroUtil.naoNulo(solicitacaoTO.getValorQDC())) {
     solicitacao.setValorQDC(solicitacaoTO.getValorQDC());
    } else {
     solicitacao.setValorQDC(controladorContrato.obterQdcContratoValidoPorData(solicitacaoTO.getDateSolicitacao(),
       solicitacaoTO.getIdContrato()));
    }
    solicitacao.setValorQDS(solicitacaoTO.getValorQDS());
    solicitacao.setValorQDP(solicitacaoTO.getValorQDP());

    if (solicitacaoTO.getIndicadorAceite() != null) {
     solicitacao.setIndicadorAceite(solicitacaoTO.getIndicadorAceite());
    }

    solicitacao.setDataOperacao(Calendar.getInstance().getTime());
    if (solicitacaoTO.getComentario() != null && !solicitacaoTO.getComentario().isEmpty()) {
     solicitacao.setComentario(solicitacaoTO.getComentario());
    }

    if (chamado != null) {
     solicitacao.setChamado(chamado);
    }
    if (contratoPontoConsumoModalidade != null) {
     solicitacao.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
    }
    if (solicitacaoTO.getIndicadorTipoProgramacao() != null) {
     solicitacao.setIndicadorTipoProgramacaoConsumo(solicitacaoTO.getIndicadorTipoProgramacao());
    }
    listaSolicitacao.add(solicitacao);
   }
  }

  return listaSolicitacao;
 }

 /**
  * Adicionar penalidade lista solicitacao consumo to.
  *
  * @param listaSolicitacaoConsumoTO the lista solicitacao consumo to
  * @param contratoPonto             the contrato ponto
  * @throws NegocioException the negocio exception
  * @throws ParseException   the parse exception
  */
 @SuppressWarnings("unused")
 private void adicionarPenalidadeListaSolicitacaoConsumoTO(List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO,
                                                           ContratoPontoConsumo contratoPonto) throws NegocioException, ParseException {

  Map<String, Object> filtro = new HashMap<String, Object>();
  SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
  filtro.put("idContrato", contratoPonto.getContrato().getChavePrimaria());
  filtro.put("idPontoConsumo", contratoPonto.getPontoConsumo().getChavePrimaria());
  filtro.put("dataApuracaoInicio", df.parse(listaSolicitacaoConsumoTO.get(0).getDataSolicitacao()));
  filtro.put("dataApuracaoFim", df.parse(listaSolicitacaoConsumoTO.get(listaSolicitacaoConsumoTO.size() - 1).getDataSolicitacao()));

  List<ApuracaoQuantidadeVO> listaApuracaoQuantidadeVO = (List<ApuracaoQuantidadeVO>) controladorApuracaoPenalidade
    .montarListaApuracaoQuantidadeVO(controladorApuracaoPenalidade.listarApuracaoQuantidadeVO(filtro));

  if (listaApuracaoQuantidadeVO != null && !listaApuracaoQuantidadeVO.isEmpty()) {

   List<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracao =
     (List<ApuracaoQuantidadePenalidadePeriodicidade>) controladorApuracaoPenalidade
       .listarApuracaoQuantidadePenalidadePeriodicidades(listaApuracaoQuantidadeVO.get(0).getPeriodoApuracao(),
         contratoPonto.getPontoConsumo().getChavePrimaria(), listaApuracaoQuantidadeVO.get(0)
           .getChavePenalidade(), contratoPonto.getContrato());
   if (listaApuracao != null && !listaApuracao.isEmpty()) {
    for (int i = 0; i < listaApuracao.size(); i++) {
     String dataApuracao = Util.converterDataParaStringSemHora(listaApuracao.get(i).getDataInicioApuracao(),
       Constantes.FORMATO_DATA_BR);
     for (int x = 0; x < listaSolicitacaoConsumoTO.size(); x++) {
      if (dataApuracao.equals(listaSolicitacaoConsumoTO.get(x).getDataSolicitacao())) {
       listaSolicitacaoConsumoTO.get(x).setPenalidade(listaApuracao.get(i).getPenalidade().getDescricao());
      }
     }
    }
   }
  }

 }

 /**
  * Gerar relatorio solicitacao consumo.
  *
  * @param listaSolicitacaoConsumoTO the lista solicitacao consumo to
  * @param idPontoConsumo            the id ponto consumo
  * @param tipoExibicao              the tipo exibicao
  * @param descicaoModalidade        the descicao modalidade
  * @param tipoExtensao              the tipo extensao
  * @param chaveModalidade           the chave modalidade
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "gerarRelatorioSolicitacaoConsumo")
 @RequestWrapper(localName = "gerarRelatorioSolicitacaoConsumoRequest")
 @ResponseWrapper(localName = "gerarRelatorioSolicitacaoConsumoResposta")
 public byte[] gerarRelatorioSolicitacaoConsumo(
   @WebParam(name = "listaSolicitacaoConsumo") List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTO,
   @WebParam(name = "idPontoConsumo") Long idPontoConsumo, @WebParam(name = "tipoExibicao") String tipoExibicao,
   @WebParam(name = "descicaoModalidade") String descicaoModalidade, @WebParam(name = "tipoExtensao") String tipoExtensao,
   @WebParam(name = "chaveModalidade") Long chaveModalidade) throws Exception {

  Map<String, Object> parametros = new HashMap<String, Object>();
  parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

  String dataInicial = listaSolicitacaoConsumoTO.get(0).getDataSolicitacao();
  parametros.put("dataInicial", dataInicial);
  int mes = Integer.parseInt(dataInicial.substring(4, 5));
  int ano = Integer.parseInt(dataInicial.substring(6, 10));
  String dataFinal = listaSolicitacaoConsumoTO.get(listaSolicitacaoConsumoTO.size() - 1).getDataSolicitacao();

  parametros.put("dataFinal", dataFinal);

  List<SolicitacaoConsumoPontoConsumoTO> listaSolicitacaoConsumoTOExibicao = new ArrayList<SolicitacaoConsumoPontoConsumoTO>();
  // Seleciona os resgistros a serem exibidos, pelo tipo de relatório QDS X QDP
  if (tipoExibicao.equals(QDSXQDP)) {
   parametros.put("nomeRelatorio", " Relatorio: Programação de Consumo");
   for (SolicitacaoConsumoPontoConsumoTO solicitacaoConsumoPontoConsumoTO : listaSolicitacaoConsumoTO) {
    if (solicitacaoConsumoPontoConsumoTO.getIndicadorAceite() != null && solicitacaoConsumoPontoConsumoTO.getIndicadorAceite()) {
     listaSolicitacaoConsumoTOExibicao.add(solicitacaoConsumoPontoConsumoTO);

    }
   }
   // historico
  } else if (tipoExibicao.equals(HISTORICO)) {
   parametros.put("nomeRelatorio", " Relatório: Histórico de Programação");
   ContratoPontoConsumo contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
   Collection<ContratoPontoConsumoModalidade> listaPontoConsumoModadelidade = controladorContrato
     .listarContratoPontoConsumoModalidadePorContratoPontoConsumo(contratoPonto.getChavePrimaria());
   Collection<ContratoPontoConsumoModalidade> listaManipulacao = new ArrayList<ContratoPontoConsumoModalidade>();
   listaManipulacao.addAll(listaPontoConsumoModadelidade);
   if (chaveModalidade != null) {
    for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaManipulacao) {
     if (contratoPontoConsumoModalidade.getChavePrimaria() != chaveModalidade) {
      listaPontoConsumoModadelidade.remove(contratoPontoConsumoModalidade);
     }
    }
   }

   Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorProgramacao
     .listarSolicitacaoConsumoPontoConsumoHistorico(contratoPonto, listaPontoConsumoModadelidade, ano, mes);

   listaSolicitacaoConsumoTOExibicao = this.converterSolicitacaoConsumoTO(listaSolicitacaoConsumo);
  } else {
   if (tipoExibicao.equals(PROGRAMACAO)) {

    parametros.put("nomeRelatorio", " Relatório: Programação de Consumo");
   } else {

    parametros.put("nomeRelatorio", " Relatório: Solicitação de Consumo");
   }

   listaSolicitacaoConsumoTOExibicao.addAll(listaSolicitacaoConsumoTO);
  }

  PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);

  Collection<RelatorioSolicitacaoConsumoVO> dados = new ArrayList<RelatorioSolicitacaoConsumoVO>();
  RelatorioSolicitacaoConsumoVO relatorioSolicitacao = new RelatorioSolicitacaoConsumoVO();

  relatorioSolicitacao.setPontoConsumo(pontoConsumo.getDescricao());
  relatorioSolicitacao.setTipoExibicao(tipoExibicao);
  relatorioSolicitacao.setColecaoVOs(listaSolicitacaoConsumoTOExibicao);
  relatorioSolicitacao.setModalidade(descicaoModalidade);
  dados.add(relatorioSolicitacao);
  // Trata o Formato de Impressão
  FormatoImpressao formatoImpressao = null;
  int caseTipo = Integer.parseInt(tipoExtensao);

  switch (caseTipo) {
   case PDF:
    formatoImpressao = FormatoImpressao.PDF;
    break;
   case EXEL:

    formatoImpressao = FormatoImpressao.XLS;
    break;
   case WORD:

    formatoImpressao = FormatoImpressao.RTF;
    break;
   default:

    formatoImpressao = FormatoImpressao.PDF; //NOSONAR {É preciso especificar um default, mesmo que seja um caso ja utilizado}
    break;
  }
  // Tipo Exibição Relatório

  parametros.put("tipoExibicao", tipoExibicao);

  // Exibir Filtros

  parametros.put(EXIBIR_FILTROS, false);

  // Logotipo Empresa
  ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
    ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
  Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();
  if (cdlEmpresa.getLogoEmpresa() != null) {
   parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
  }

  return RelatorioUtil.gerarRelatorio(dados, parametros, "relatorioSolicitacaoConsumo.jasper", formatoImpressao);

 }

 /**
  * Consulta cromatografia.
  *
  * @param chavePrimariaCityGate the chave primaria city gate
  * @param data                  the data
  * @param chavePrimaria         the chave primaria
  * @return the collection
  * @throws GGASException the exception
  */
 @WebMethod(operationName = "consultaCromatografia")
 @RequestWrapper(localName = "consultaCromatografiaRequest")
 @ResponseWrapper(localName = "consultaCromatografiaResposta")
 public Collection<CromatografiaTO> consultaCromatografia(@WebParam(name = "chavePrimariaCityGate") Long chavePrimariaCityGate,
                                                          @WebParam(name = "data") String data,
                                                          @WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria) throws GGASException {

  CityGate cityGate = new CityGateImpl();

  Cromatografia cromatografia = new Cromatografia();

  if (data != null && !"".equals(data)) {
   cromatografia.setData(Util.converterCampoStringParaData("data", data, Constantes.FORMATO_DATA_BR));
  }
  if (chavePrimariaCityGate != null) {
   cityGate.setChavePrimaria(chavePrimariaCityGate);
   if (cityGate != null) {
    cromatografia.setCityGate(cityGate);
   }
  }
  if (chavePrimaria != null) {
   cromatografia.setChavePrimaria(chavePrimaria);
  }
  Collection<Cromatografia> listaCromatografia = controladorCromatografia.consultarCromatografia(cromatografia, null);
  Collection<CromatografiaTO> listaCromatografiaTO = new ArrayList<CromatografiaTO>();

  for (Cromatografia dado : listaCromatografia) {
   CromatografiaTO cromatografiaTO = new CromatografiaTO();
   cromatografiaTO.setPcs(dado.getPcs());
   cromatografiaTO.setPressao(dado.getPressao());
   cromatografiaTO.setTemperatura(dado.getTemperatura());
   cromatografiaTO.setAgua(dado.getAgua());
   cromatografiaTO.setIButano(dado.getiButano());
   cromatografiaTO.setNHeptano(dado.getnHeptano());
   cromatografiaTO.setArgonio(dado.getArgonio());
   cromatografiaTO.setIPentano(dado.getiPentano());
   cromatografiaTO.setNHexano(dado.getnHexano());
   cromatografiaTO.setDioxidoCarbono(dado.getDioxidoCarbono());
   cromatografiaTO.setMetano(dado.getMetano());
   cromatografiaTO.setNitrogenio(dado.getNitrogenio());
   cromatografiaTO.setEtano(dado.getEtano());
   cromatografiaTO.setMonoxidoCarbono(dado.getMonoxidoCarbono());
   cromatografiaTO.setNNonano(dado.getnNonano());
   cromatografiaTO.setHelio(dado.getHelio());
   cromatografiaTO.setNButano(dado.getnButano());
   cromatografiaTO.setNOctano(dado.getnOctano());
   cromatografiaTO.setHidrogenio(dado.getHidrogenio());
   cromatografiaTO.setNDecano(dado.getnDecano());
   cromatografiaTO.setNPentano(dado.getnPentano());
   cromatografiaTO.setOxigenio(dado.getOxigenio());
   cromatografiaTO.setPropano(dado.getPropano());
   cromatografiaTO.setSulfetoHidrogenio(dado.getSulfetoHidrogenio());
   cromatografiaTO.setChavePrimaria(dado.getChavePrimaria());
   cromatografiaTO.setData(dado.getDataFormatada());
   cromatografiaTO.setTemperatura(dado.getTemperatura());
   cromatografiaTO.setPressao(dado.getPressao());
   cromatografiaTO.setFatorCompressibilidade(dado.getFatorFormatado());
   listaCromatografiaTO.add(cromatografiaTO);
  }

  return listaCromatografiaTO;
 }

 /**
  * Consulta city gate.
  *
  * @param chavePrimaria the chave primaria
  * @return the sets the
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultaCityGate")
 @RequestWrapper(localName = "consultaCityGateRequest")
 @ResponseWrapper(localName = "consultaCityGateResposta")
 public Set<CityGateTO> consultaCityGate(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria) throws Exception {

  Collection<ContratoPontoConsumo> contratosPontoConsumo = controladorContrato.listarContratoPontoConsumo(chavePrimaria);

  Set<CityGate> conjuntoCityGate = new HashSet<CityGate>();
  Set<CityGateTO> conjuntoCityGateTO = new HashSet<CityGateTO>();

  // Varre todos os Pontos de consumo do contrato e recupera os City Gates eliminando City gates repetidos
  PontoConsumo consumo;
  for (ContratoPontoConsumo cpc : contratosPontoConsumo) {
   consumo = cpc.getPontoConsumo();
   for (PontoConsumoCityGate pontoConsumoCityGate : consumo.getListaPontoConsumoCityGate()) {
    conjuntoCityGate.add(pontoConsumoCityGate.getCityGate());
   }
  }

  // Varre todos os City Gates e passa eles para CityGateTO
  for (CityGate cityGate : conjuntoCityGate) {
   CityGateTO cityGateTO = new CityGateTO();
   cityGateTO.setChavePrimaria(cityGate.getChavePrimaria());
   cityGateTO.setDescricao(cityGate.getDescricao());
   cityGateTO.setLocalidade(cityGate.getLocalidade().getDescricao());
   conjuntoCityGateTO.add(cityGateTO);
  }
  return conjuntoCityGateTO;
 }

 /**
  * Consultar quadra por cep.
  *
  * @param cep the cep
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarQuadraPorCep")
 @RequestWrapper(localName = "consultarQuadraPorCeplRequest")
 @ResponseWrapper(localName = "consultarQuadraPorCeplResposta")
 public Collection<TipoComboBoxTO> consultarQuadraPorCep(@WebParam(name = "cep") String cep) throws Exception {

  Collection<TipoComboBoxTO> listaQuadrasTO = new ArrayList<TipoComboBoxTO>();
  if (cep != null && cep.length() > 0) {
   Collection<Quadra> quadras = fachadaAtributo.listarQuadraPorCep(cep);
   for (Quadra quadra : quadras) {
    TipoComboBoxTO quadraTO = new TipoComboBoxTO();
    quadraTO.setChavePrimaria(quadra.getChavePrimaria());
    quadraTO.setDescricao(quadra.getNumeroQuadra());
    listaQuadrasTO.add(quadraTO);
   }
  }

  return listaQuadrasTO;
 }

 /**
  * Consultar quadra face pro quadra.
  *
  * @param chaveQuadra the chave quadra
  * @param cep         the cep
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarQuadraFaceProQuadra")
 @RequestWrapper(localName = "consultarQuadraFaceProQuadralRequest")
 @ResponseWrapper(localName = "consultarQuadraFaceProQuadraResposta")
 public Collection<TipoComboBoxTO> consultarQuadraFaceProQuadra(@WebParam(name = "chaveQuadra") String chaveQuadra,
                                                                @WebParam(name = "cep") String cep) throws Exception {

  Collection<TipoComboBoxTO> listaFacesTO = new ArrayList<TipoComboBoxTO>();
  if (cep != null && chaveQuadra != null) {
   Collection<QuadraFace> faces = fachadaAtributo.obterQuadraFacePorQuadra(Long.valueOf(chaveQuadra), cep);
   for (QuadraFace face : faces) {
    TipoComboBoxTO quadraTO = new TipoComboBoxTO();
    quadraTO.setChavePrimaria(face.getChavePrimaria());
    quadraTO.setDescricao(face.getQuadraFaceFormatada());
    listaFacesTO.add(quadraTO);
   }
  }

  return listaFacesTO;
 }

 /**
  * Obter dados tela inclusao alteracao.
  *
  * @param descricao the descricao
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterDadosTelaInclusaoAlteracao")
 @RequestWrapper(localName = "obterDadosTelaInclusaoAlteracaoRequest")
 @ResponseWrapper(localName = "obterDadosTelaInclusaoAlteracaoResposta")
 public ContainerListasTO obterDadosTelaInclusaoAlteracao(@WebParam(name = "descricao") String descricao) throws Exception {

  ContainerListasTO talaImovelTO = new ContainerListasTO();

  converterListas(talaImovelTO);

  TipoComboBoxTO situacaoImovel = new TipoComboBoxTO();
  if (descricao != null) {
   SituacaoImovel emProspeccao = fachadaAtributo.obterSituacaoImovelPorDescricao(descricao);
   if (emProspeccao != null) {
    situacaoImovel.setChavePrimaria(emProspeccao.getChavePrimaria());
    situacaoImovel.setDescricao(emProspeccao.getDescricao());
    talaImovelTO.setSituacaoImovel(situacaoImovel);
   }
  }

  return talaImovelTO;
 }

 /**
  * Converter listas.
  *
  * @param telaImovelTO the tela imovel to
  * @throws GGASException the GGAS exception
  */
 private void converterListas(ContainerListasTO telaImovelTO) throws GGASException {

  Collection<PavimentoCalcada> listaPavimentoCalcada = fachadaAtributo.listarPavimentoCalcada();
  Collection<PavimentoRua> listaPavimentoRua = fachadaAtributo.listarPavimentoRua();
  Collection<PadraoConstrucao> listaPadraoConstrucao = fachadaAtributo.listarPadraoConstrucao();
  Collection<AreaConstruidaFaixa> listaAreaConstruida = fachadaAtributo.listarAreaConstruidaFaixa();
  Collection<PerfilImovel> listaPerfilImovel = fachadaAtributo.listarPerfilImovel();
  Collection<TipoBotijao> listaTipoBotijao = fachadaAtributo.listarTipoBotijao();

  Collection<TipoComboBoxTO> listaPavimentoCalcadaTO = new ArrayList<TipoComboBoxTO>();
  for (PavimentoCalcada calcada : listaPavimentoCalcada) {
   TipoComboBoxTO calcadaTO = new TipoComboBoxTO();
   calcadaTO.setChavePrimaria(calcada.getChavePrimaria());
   calcadaTO.setDescricao(calcada.getDescricao());
   listaPavimentoCalcadaTO.add(calcadaTO);
  }
  telaImovelTO.setListaPavimentoCalcada(listaPavimentoCalcadaTO);

  Collection<TipoComboBoxTO> listaPavimentoRuaTO = new ArrayList<TipoComboBoxTO>();
  for (PavimentoRua rua : listaPavimentoRua) {
   TipoComboBoxTO ruaTO = new TipoComboBoxTO();
   ruaTO.setChavePrimaria(rua.getChavePrimaria());
   ruaTO.setDescricao(rua.getDescricao());
   listaPavimentoRuaTO.add(ruaTO);
  }
  telaImovelTO.setListaPavimentoRuaTO(listaPavimentoRuaTO);

  Collection<TipoComboBoxTO> listaPadraoConstrucaoTO = new ArrayList<TipoComboBoxTO>();
  for (PadraoConstrucao contrucao : listaPadraoConstrucao) {
   TipoComboBoxTO contrucaoTO = new TipoComboBoxTO();
   contrucaoTO.setChavePrimaria(contrucao.getChavePrimaria());
   contrucaoTO.setDescricao(contrucao.getDescricao());
   listaPadraoConstrucaoTO.add(contrucaoTO);
  }
  telaImovelTO.setListaPadraoConstrucaoTO(listaPadraoConstrucaoTO);

  Collection<TipoComboBoxTO> listaAreaContruidaTO = new ArrayList<TipoComboBoxTO>();
  for (AreaConstruidaFaixa faixa : listaAreaConstruida) {
   TipoComboBoxTO areaTO = new TipoComboBoxTO();
   areaTO.setChavePrimaria(faixa.getChavePrimaria());
   areaTO.setDescricao(String.valueOf(faixa.getMenorFaixa()));
   areaTO.setDescricao1(String.valueOf(faixa.getMaiorFaixa()));
   listaAreaContruidaTO.add(areaTO);
  }
  telaImovelTO.setListaAreaContruidaTO(listaAreaContruidaTO);

  Collection<TipoComboBoxTO> listaPerfilImovelTO = new ArrayList<TipoComboBoxTO>();
  for (PerfilImovel perfil : listaPerfilImovel) {
   TipoComboBoxTO perfilTO = new TipoComboBoxTO();
   perfilTO.setChavePrimaria(perfil.getChavePrimaria());
   perfilTO.setDescricao(perfil.getDescricao());
   listaPerfilImovelTO.add(perfilTO);
  }
  telaImovelTO.setListaPerfilImovelTO(listaPerfilImovelTO);

  Collection<TipoComboBoxTO> listaTIpoBotijaoTO = new ArrayList<TipoComboBoxTO>();
  for (TipoBotijao botijao : listaTipoBotijao) {
   TipoComboBoxTO botijaoTO = new TipoComboBoxTO();
   botijaoTO.setChavePrimaria(botijao.getChavePrimaria());
   botijaoTO.setDescricao(botijao.getDescricao());
   listaTIpoBotijaoTO.add(botijaoTO);
  }
  telaImovelTO.setListaTIpoBotijaoTO(listaTIpoBotijaoTO);

  converterListas2(telaImovelTO);
 }

 /**
  * Converter listas2.
  *
  * @param telaImovelTO the tela imovel to
  * @throws GGASException the GGAS exception
  */
 private void converterListas2(ContainerListasTO telaImovelTO) throws GGASException {

  ControladorAgente controladorAgente = (ControladorAgente) ServiceLocator.getInstancia().getBeanPorID("controladorAgenteImpl");

  Collection<ModalidadeMedicaoImovel> listaModalidadeMedicao = fachadaAtributo.listarModalidadeMedicaoImovel();
  Collection<RedeMaterial> listaRedeMaterial = fachadaAtributo.listarRedeMaterial();
  Collection<RedeDiametro> listaRedeDiametro = fachadaAtributo.listarRedeDiametro();
  Collection<TipoContato> listaTiposContato = fachadaAtributo.listarTipoContato();
  Collection<Profissao> listaProfissao = fachadaAtributo.listarProfissao();
  Collection<Segmento> listaSegmento = fachadaAtributo.listarSegmento();
  Collection<Agente> listaAgente = controladorAgente.obterAgentePorChaveFuncionario(null, "true");

  Collection<TipoComboBoxTO> listaModalidadeMedicaoTO = new ArrayList<TipoComboBoxTO>();
  for (ModalidadeMedicaoImovel modalidade : listaModalidadeMedicao) {
   TipoComboBoxTO modalidadeTO = new TipoComboBoxTO();
   modalidadeTO.setChavePrimaria(Long.valueOf(modalidade.getCodigo()));
   modalidadeTO.setDescricao(modalidade.getDescricao());
   listaModalidadeMedicaoTO.add(modalidadeTO);
  }
  telaImovelTO.setListaModalidadeMedicaoTO(listaModalidadeMedicaoTO);

  Collection<TipoComboBoxTO> listaRedeMaterialTO = new ArrayList<TipoComboBoxTO>();
  for (RedeMaterial material : listaRedeMaterial) {
   TipoComboBoxTO materialTO = new TipoComboBoxTO();
   materialTO.setChavePrimaria(material.getChavePrimaria());
   materialTO.setDescricao(material.getDescricao());
   listaRedeMaterialTO.add(materialTO);
  }
  telaImovelTO.setListaRedeMaterialTO(listaRedeMaterialTO);

  Collection<TipoComboBoxTO> listaRedeDiametroTO = new ArrayList<TipoComboBoxTO>();
  for (RedeDiametro diametro : listaRedeDiametro) {
   TipoComboBoxTO diametroTO = new TipoComboBoxTO();
   diametroTO.setChavePrimaria(diametro.getChavePrimaria());
   diametroTO.setDescricao(diametro.getDescricao());
   listaRedeDiametroTO.add(diametroTO);
  }
  telaImovelTO.setListaRedeDiametroTO(listaRedeDiametroTO);

  Collection<TipoComboBoxTO> listaTipoContatoTO = new ArrayList<TipoComboBoxTO>();
  for (TipoContato contato : listaTiposContato) {
   TipoComboBoxTO contatoTO = new TipoComboBoxTO();
   contatoTO.setChavePrimaria(contato.getChavePrimaria());
   contatoTO.setDescricao(contato.getDescricao());
   listaTipoContatoTO.add(contatoTO);
  }
  telaImovelTO.setListaTipoContatoTO(listaTipoContatoTO);

  Collection<TipoComboBoxTO> listaProfissaoTO = new ArrayList<TipoComboBoxTO>();
  for (Profissao profissao : listaProfissao) {
   TipoComboBoxTO profissaoTO = new TipoComboBoxTO();
   profissaoTO.setChavePrimaria(profissao.getChavePrimaria());
   profissaoTO.setDescricao(profissao.getDescricao());
   listaProfissaoTO.add(profissaoTO);
  }
  telaImovelTO.setListaProfissaoTO(listaProfissaoTO);

  Collection<TipoComboBoxTO> listaSegmentoTO = new ArrayList<TipoComboBoxTO>();
  for (Segmento segmento : listaSegmento) {
   TipoComboBoxTO segmentoTO = new TipoComboBoxTO();
   segmentoTO.setChavePrimaria(segmento.getChavePrimaria());
   segmentoTO.setDescricao(segmento.getDescricao());
   listaSegmentoTO.add(segmentoTO);
  }
  telaImovelTO.setListaSegmentoTO(listaSegmentoTO);

  Collection<TipoComboBoxTO> listaAgenteTO = new ArrayList<TipoComboBoxTO>();
  for (Agente agente : listaAgente) {
   TipoComboBoxTO agenteTO = new TipoComboBoxTO();
   agenteTO.setChavePrimaria(agente.getChavePrimaria());
   agenteTO.setDescricao(agente.getFuncionario().getNome());
   listaAgenteTO.add(agenteTO);
  }
  telaImovelTO.setListaAgentes(listaAgenteTO);

 }

 /**
  * Listar rotas por chave quadra.
  *
  * @param idQuadra the id quadra
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "listarRotasPorChaveQuadra")
 @RequestWrapper(localName = "listarRotasPorChaveQuadraRequest")
 @ResponseWrapper(localName = "listarRotasPorChaveQuadraResposta")
 public ContainerListasTO listarRotasPorChaveQuadra(@WebParam(name = "idQuadra") Long idQuadra) throws Exception {

  ContainerListasTO telaImovelTO = new ContainerListasTO();

  if (idQuadra != null && idQuadra > 0) {
   Collection<Rota> rotas = fachadaAtributo.listarRotasPorSetorComercialDaQuadra(idQuadra);
   Collection<TipoComboBoxTO> rotasTO = new ArrayList<TipoComboBoxTO>();
   for (Rota rota : rotas) {
    TipoComboBoxTO rotaTO = new TipoComboBoxTO();
    rotaTO.setChavePrimaria(rota.getChavePrimaria());
    rotaTO.setDescricao(rota.getNumeroRota());
    rotasTO.add(rotaTO);
   }
   telaImovelTO.setListaRotas(rotasTO);

  }

  return telaImovelTO;
 }

 /**
  * Incluir imovel.
  *
  * @param imovelTO        the imovel to
  * @param contatoImovelTO the contato imovel to
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "incluirImovel")
 @RequestWrapper(localName = "incluirImovelRequest")
 @ResponseWrapper(localName = "incluirImovelResposta")
 public Long incluirImovel(@WebParam(name = "imovelTO") ImovelTO imovelTO,
                           @WebParam(name = "contatoImovelTO") ContatoImovelTO contatoImovelTO) throws Exception {

  ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
    ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

  ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
    ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

  Imovel imovelConverido = controladorImovel.converterImovelTOEmImovel(imovelTO);
  SituacaoConsumo situacaoConsumo = fachadaAtributo.obterSituacaoConsumo(Long.parseLong(
    getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO)));
  if (!CollectionUtils.isEmpty(imovelTO.getListaPontoConsumo())) {
   Collection<PontoConsumo> listaPontoConsumo = new HashSet<PontoConsumo>();
   for (PontoConsumoTO pontoConsumoTO : imovelTO.getListaPontoConsumo()) {
    PontoConsumo pontoConsumo = controladorPontoConsumo.converterPontoConsumoTOEmPontoConsumo(pontoConsumoTO);
    pontoConsumo.setSituacaoConsumo(situacaoConsumo);
    pontoConsumo.setImovel(imovelConverido);
    pontoConsumo.setUltimaAlteracao(new Date());
    listaPontoConsumo.add(pontoConsumo);
   }
   imovelConverido.getListaPontoConsumo().addAll(listaPontoConsumo);
  }

  imovelConverido.setSituacaoImovel(fachadaAtributo.obterSituacaoImovelPorDescricao("EM PROSPECÇÃO"));

  imovelConverido.setHabilitado(Boolean.TRUE);

  return controladorImovel.inserirImovel(imovelConverido);
 }

 /**
  * Listar ramo atividade por segmento.
  *
  * @param idSegmento the id segmento
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "listarRamoAtividadePorSegmento")
 @RequestWrapper(localName = "listarRamoAtividadePorSegmentoRequest")
 @ResponseWrapper(localName = "listarRamoAtividadePorSegmentoResposta")
 public Collection<TipoComboBoxTO> listarRamoAtividadePorSegmento(@WebParam(name = "idSegmento") Long idSegmento) throws Exception {

  Collection<TipoComboBoxTO> listaRamoAtividadeTO = new ArrayList<TipoComboBoxTO>();
  Collection<RamoAtividade> listaRamoAtividade = null;
  if (idSegmento != null && idSegmento > 0) {
   listaRamoAtividade = fachadaAtributo.listarRamoAtividadePorSegmento(idSegmento);
   for (RamoAtividade ramo : listaRamoAtividade) {
    TipoComboBoxTO ramoTO = new TipoComboBoxTO();
    ramoTO.setChavePrimaria(ramo.getChavePrimaria());
    ramoTO.setDescricao(ramo.getDescricao());
    listaRamoAtividadeTO.add(ramoTO);
   }
  }

  return listaRamoAtividadeTO;
 }

 /**
  * Consultar imoveis.
  *
  * @param cep                      the cep
  * @param numeroImovel             the numero imovel
  * @param complementoImovel        the complemento imovel
  * @param nome                     the nome
  * @param matricula                the matricula
  * @param pontoConsumoLegado       the ponto consumo legado
  * @param indicadorCondominioAmbos the indicador condominio ambos
  * @param habilitado               the habilitado
  * @param chaveLeiturista          the chave leiturista
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarImoveis")
 @RequestWrapper(localName = "consultarImoveisRequest")
 @ResponseWrapper(localName = "consultarImoveisResposta")
 public Collection<ImovelTO> consultarImoveis(@WebParam(name = "cep") String cep, @WebParam(name = "numeroImovel") String numeroImovel,
           @WebParam(name = COMPLEMENTO_IMOVEL) String complementoImovel, @WebParam(name = "nome") String nome,
           @WebParam(name = "matricula") String matricula, @WebParam(name = "pontoConsumoLegado") String pontoConsumoLegado,
           @WebParam(name = "indicadorCondominioAmbos") String indicadorCondominioAmbos,
           @WebParam(name = HABILITADO) String habilitado,
           @WebParam(name = "chaveLeiturista") Long chaveLeiturista) throws Exception {

  ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
    ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

  Map<String, Object> filtro = obterFiltroImovel(null, nome, complementoImovel, matricula, numeroImovel, indicadorCondominioAmbos,
    habilitado, cep);

  Long chaveFuncionario = null;

  if (chaveLeiturista != null && chaveLeiturista > 0) {
   ControladorLeiturista controladorLeiturista = (ControladorLeiturista) ServiceLocator.getInstancia().getBeanPorID(
     ControladorLeiturista.BEAN_ID_CONTROLADOR_LEITURISTA);
   Leiturista leiturista = controladorLeiturista.obterLeiturista(chaveLeiturista);
   if (leiturista != null) {
    chaveFuncionario = leiturista.getFuncionario().getChavePrimaria();
   }
  }

  Collection<Imovel> listaImoveis = fachadaAtributo.listarImoveisEmProspeccao(filtro, chaveFuncionario);

  List<ImovelTO> listaImovelTO = new ArrayList<ImovelTO>();
  if (listaImoveis != null) {
   for (Imovel imovel : listaImoveis) {
    ImovelTO imovelTO = controladorImovel.converterImovelEmImovelTO(imovel);
    listaImovelTO.add(imovelTO);
   }
  }

  return listaImovelTO;
 }

 /**
  * Obter filtro imovel.
  *
  * @param chaveLM              the chave lm
  * @param descricaoImovelTexto the descricao imovel texto
  * @param complementoTexto     the complemento texto
  * @param matriculaImovelTexto the matricula imovel texto
  * @param numeroImovelTexto    the numero imovel texto
  * @param indicadorCondominio  the indicador condominio
  * @param habilitado           the habilitado
  * @param cep                  the cep
  * @return the hash map
  */
 private Map<String, Object> obterFiltroImovel(Long chaveLM, String descricaoImovelTexto, String complementoTexto,
                                               String matriculaImovelTexto, String numeroImovelTexto,
                                               String indicadorCondominio, String habilitado, String cep) {

  Map<String, Object> filtro = new HashMap<String, Object>();

  if (chaveLM != null && chaveLM > 0) {
   filtro.put(CHAVE_PRIMARIA_LM, chaveLM);
  }

  if (descricaoImovelTexto != null && descricaoImovelTexto.length() > 0) {
   filtro.put(DESCRICAO_IMOVEL, descricaoImovelTexto.toUpperCase());
  }

  if (complementoTexto != null && complementoTexto.length() > 0) {
   filtro.put(COMPLEMENTO_IMOVEL, complementoTexto.toUpperCase());
  }

  if (numeroImovelTexto != null && numeroImovelTexto.length() > 0) {
   filtro.put(NUMERO_IMOVEL, numeroImovelTexto.toUpperCase());
  }

  if (matriculaImovelTexto != null && matriculaImovelTexto.length() > 0) {
   filtro.put(CHAVE_PRIMARIA, Long.valueOf(matriculaImovelTexto));
  }

  if (indicadorCondominio != null && indicadorCondominio.length() > 0) {
   filtro.put(INDICADOR_CONDOMINIO_AMBOS, indicadorCondominio);
  }

  if (habilitado != null) {
   filtro.put(HABILITADO, habilitado);
  }

  if (!StringUtils.isEmpty(cep)) {
   filtro.put("cepImovel", cep);
  }

  return filtro;
 }

 /**
  * Obter filtro levantamento mercado.
  *
  * @param cep                      the cep
  * @param numeroImovel             the numero imovel
  * @param complementoImovel        the complemento imovel
  * @param nome                     the nome
  * @param matricula                the matricula
  * @param pontoConsumoLegado       the ponto consumo legado
  * @param indicadorCondominioAmbos the indicador condominio ambos
  * @param habilitado               the habilitado
  * @param chavePrimariaLM          the chave primaria LM
  * @param periodoVisitaInicial     the periodo visita inicial
  * @param periodoVisitaFinal       the periodo visita final
  * @param visitaAgendada           the visita agendada
  * @param idStatus                 the id status
  * @param idAgente                 the id agente
  * @param descricaoBairro          the descricao bairro
  * @param descricaoRua             the descricao rua
  * @param dataVisita               the data visita
  * @return the map
  */
 private Map<String, Object> obterFiltroLevantamentoMercado(String cep, String numeroImovel, String complementoImovel, String nome,
          String matricula, String pontoConsumoLegado, String indicadorCondominioAmbos, String habilitado, Long chavePrimariaLM,
          String periodoVisitaInicial, String periodoVisitaFinal, String visitaAgendada, Long idStatus, Long idAgente,
          String descricaoBairro, String descricaoRua, String dataVisita) {

  Map<String, Object> filtro = new HashMap<String, Object>();

  if (!StringUtils.isEmpty(cep)) {
   filtro.put("cepImovel", cep);
  }

  if (!StringUtils.isEmpty(numeroImovel)) {
   filtro.put("numeroImovel", numeroImovel);
  }

  if (!StringUtils.isEmpty(complementoImovel)) {
   filtro.put(COMPLEMENTO_IMOVEL, complementoImovel);
  }

  if (!StringUtils.isEmpty(nome)) {
   filtro.put("nome", nome);
  }

  if (!StringUtils.isEmpty(matricula)) {
   filtro.put(CHAVE_PRIMARIA, Long.valueOf(matricula));
  }

  if (!StringUtils.isEmpty(pontoConsumoLegado)) {
   filtro.put("pontoConsumoLegado", pontoConsumoLegado);
  }

  if (!StringUtils.isEmpty(indicadorCondominioAmbos)) {
   filtro.put("indicadorCondominioAmbos", indicadorCondominioAmbos);
  }

  if (!StringUtils.isEmpty(habilitado)) {
   filtro.put(HABILITADO, habilitado);
  }

  if (chavePrimariaLM != null && chavePrimariaLM > 0) {
   filtro.put("chavePrimariaLM", chavePrimariaLM);
  }

  if (idAgente != null && idAgente > 0) {
   filtro.put("idAgente", idAgente);
  }

  if (dataVisita != null && dataVisita.length() == 10) {
   filtro.put("dataVisita", dataVisita);
  }

  if (periodoVisitaInicial != null && periodoVisitaInicial.length() == 10 && periodoVisitaFinal != null
    && periodoVisitaFinal.length() == 10) {
   filtro.put("periodoVisitaInicial", periodoVisitaInicial);
   filtro.put("periodoVisitaFinal", periodoVisitaFinal);
  }

  if (idStatus != null && idStatus > 0) {
   filtro.put("idStatus", idStatus);
  }

  filtro.put("visitaAgendada", visitaAgendada);

  if (!StringUtils.isEmpty(descricaoBairro)) {
   filtro.put("bairro", descricaoBairro);
  }

  if (!StringUtils.isEmpty(descricaoRua)) {
   filtro.put("rua", descricaoRua);
  }

  return filtro;
 }

 /**
  * Alterar imovel.
  *
  * @param imovelTO        the imovel to
  * @param contatoImovelTO the contato imovel to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "alterarImovel")
 @RequestWrapper(localName = "alterarImovelRequest")
 @ResponseWrapper(localName = "alterarImovelResposta")
 public void alterarImovel(@WebParam(name = "imovelTO") ImovelTO imovelTO,
                           @WebParam(name = "contatoImovelTO") ContatoImovelTO contatoImovelTO) throws Exception {

  ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
    ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

  Imovel imovelConverido = controladorImovel.converterImovelTOEmImovel(imovelTO);

  ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
    ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

  SituacaoConsumo situacaoConsumo = fachadaAtributo.obterSituacaoConsumo(Long.parseLong(
    getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO)));
  if (!CollectionUtils.isEmpty(imovelTO.getListaPontoConsumo())) {
   Collection<PontoConsumo> listaPontoConsumo = new HashSet<PontoConsumo>();
   for (PontoConsumoTO pontoConsumoTO : imovelTO.getListaPontoConsumo()) {
    PontoConsumo pontoConsumo = controladorPontoConsumo.converterPontoConsumoTOEmPontoConsumo(pontoConsumoTO);
    if (pontoConsumo.getChavePrimaria() < 1) {
     pontoConsumo.setSituacaoConsumo(situacaoConsumo);
    }
    pontoConsumo.setUltimaAlteracao(new Date());
    pontoConsumo.setImovel(imovelConverido);
    listaPontoConsumo.add(pontoConsumo);
   }
   imovelConverido.getListaPontoConsumo().clear();
   imovelConverido.getListaPontoConsumo().addAll(listaPontoConsumo);
  }

  imovelConverido.setSituacaoImovel(fachadaAtributo.obterSituacaoImovelPorDescricao("EM PROSPECÇÃO"));

  controladorImovel.atualizar(imovelConverido);
 }

 /**
  * Listar modalidade uso.
  *
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "listarModalidadeUso")
 @RequestWrapper(localName = "listarModalidadeUsoRequest")
 @ResponseWrapper(localName = "listarModalidadeUsoResposta")
 public ContainerListasTO listarModalidadeUso() throws Exception {

  ContainerListasTO talaImovelTO = new ContainerListasTO();

  converterListas(talaImovelTO);

  Collection<EntidadeConteudo> listaModalidadeUso = fachadaAtributo.listarModalidadeUso();

  Collection<TipoComboBoxTO> listaModalidadeTO = new ArrayList<TipoComboBoxTO>();
  for (EntidadeConteudo modalidade : listaModalidadeUso) {
   TipoComboBoxTO modalidadeTO = new TipoComboBoxTO();
   modalidadeTO.setChavePrimaria(modalidade.getChavePrimaria());
   modalidadeTO.setDescricao(modalidade.getDescricao());
   listaModalidadeTO.add(modalidadeTO);
  }

  talaImovelTO.setListaModalidadeUsoTo(listaModalidadeTO);

  return talaImovelTO;
 }

 /**
  * Obter listas tela levantamento mercado.
  *
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterListasTelaLevantamentoMercado")
 @RequestWrapper(localName = "obterListasTelaLevantamentoMercadoRequest")
 @ResponseWrapper(localName = "obterListasTelaLevantamentoMercadoResposta")
 public ContainerListasTO obterListasTelaLevantamentoMercado() throws Exception {

  ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  ContainerListasTO container = new ContainerListasTO();

  Map<String, Collection<TipoComboBoxTO>> listas = controladorLevantamentoMercado.conveterListas();

  container.setFornecedores(listas.get(FORNECEDORES));
  container.setAparelhos(listas.get(APARELHOS_TO));
  container.setFontesEnergeticas(listas.get(FONTES_ENERGETICAS_TO));
  container.setListaEstadoRede(listas.get(LISTA_ESTADO_REDE_TO));
  container.setListaRedeMaterialTO(listas.get(LISTA_REDE_MATERIAL_TO));
  container.setListaSegmentoTO(listas.get(SEGMENTOS_TO));
  container.setListaModalidadeUsoTo(listas.get(LISTA_MODALIDADE_USO_TO));
  container.setListaTIpoBotijaoTO(listas.get(LISTA_TIPO_CILINDRO_TO));
  container.setListaPeriodicidades(listas.get(LISTA_PERIODICIDADES_TO));

  return container;
 }

 /**
  * Incluir levantamento mercado.
  *
  * @param levantamentoMercadoTO the levantamento mercado to
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "incluirLevantamentoMercado")
 @RequestWrapper(localName = "incluirLevantamentoMercadoRequest")
 @ResponseWrapper(localName = "incluirLevantamentoMercadoMercadoResposta")
 public Long incluirLevantamentoMercado(@WebParam(name = "levantamentoMercadoTO") LevantamentoMercadoTO levantamentoMercadoTO)
   throws Exception {

  ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado
    .converterLevantamentoMercadoTOemLevantamentoMercado(levantamentoMercadoTO);

  return controladorLevantamentoMercado.inserir(levantamentoMercado);

 }

 /**
  * Pesquisar levantamento mercado.
  *
  * @param cep                      the cep
  * @param numeroImovel             the numero imovel
  * @param complementoImovel        the complemento imovel
  * @param nome                     the nome
  * @param matricula                the matricula
  * @param pontoConsumoLegado       the ponto consumo legado
  * @param indicadorCondominioAmbos the indicador condominio ambos
  * @param habilitado               the habilitado
  * @param chavePrimariaLM          the chave primaria lm
  * @param periodoVisitaInicial     the periodo visita inicial
  * @param periodoVisitaFinal       the periodo visita final
  * @param visitaAgendada           the visita agendada
  * @param idStatus                 the id status
  * @param idAgente                 the id agente
  * @param descricaoBairro          the descricao bairro
  * @param descricaoRua             the descricao rua
  * @param dataVisita               the data visita
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "pesquisarLevantamentoMercado")
 @RequestWrapper(localName = "pesquisarLevantamentoMercadoRequest")
 @ResponseWrapper(localName = "pesquisarLevantamentoMercadoResposta")
 public Collection<LevantamentoMercadoTO> pesquisarLevantamentoMercado(@WebParam(name = "cep") String cep,
     @WebParam(name = "numeroImovel") String numeroImovel, @WebParam(name = COMPLEMENTO_IMOVEL) String complementoImovel,
     @WebParam(name = "nome") String nome, @WebParam(name = "matricula") String matricula,
     @WebParam(name = "pontoConsumoLegado") String pontoConsumoLegado,
     @WebParam(name = "indicadorCondominioAmbos") String indicadorCondominioAmbos,
     @WebParam(name = HABILITADO) String habilitado, @WebParam(name = "chavePrimariaLM") Long chavePrimariaLM,
     @WebParam(name = "periodoVisitaInicial") String periodoVisitaInicial,
     @WebParam(name = "periodoVisitaFinal") String periodoVisitaFinal,
     @WebParam(name = "visitaAgendada") String visitaAgendada, @WebParam(name = "idStatus") Long idStatus,
     @WebParam(name = "idAgente") Long idAgente, @WebParam(name = "descricaoBairro") String descricaoBairro,
     @WebParam(name = "descricaoRua") String descricaoRua, @WebParam(name = "dataVisita") String dataVisita) throws Exception {

  ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  Map<String, Object> filtro = obterFiltroLevantamentoMercado(cep, numeroImovel, complementoImovel, nome, matricula,
    pontoConsumoLegado, indicadorCondominioAmbos, habilitado, chavePrimariaLM, periodoVisitaInicial, periodoVisitaFinal,
    visitaAgendada, idStatus, idAgente, descricaoBairro, descricaoRua, dataVisita);

  Collection<LevantamentoMercado> listaLM = controladorLevantamentoMercado.listarLevantamentoMercado(filtro);

  Collection<LevantamentoMercadoTO> listaLevantamentoMercadoTO = new ArrayList<LevantamentoMercadoTO>();

  for (LevantamentoMercado levantamentoMercado : listaLM) {
   LevantamentoMercadoTO levantamentoMercadoTO = controladorLevantamentoMercado
     .converterLevantamentoMercadoEmLevantamentoMercadoTO(levantamentoMercado);
   listaLevantamentoMercadoTO.add(levantamentoMercadoTO);
  }

  return listaLevantamentoMercadoTO;
 }

 /**
  * Atualizar levantamento mercado.
  *
  * @param levantamentoMercadoTO the levantamento mercado to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "atualizarLevantamentoMercado")
 @RequestWrapper(localName = "atualizarLevantamentoMercadoRequest")
 @ResponseWrapper(localName = "atualizarLevantamentoMercadoResposta")
 public void atualizarLevantamentoMercado(@WebParam(name = "levantamentoMercadoTO") LevantamentoMercadoTO levantamentoMercadoTO)
   throws Exception {

  ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado
    .converterLevantamentoMercadoTOemLevantamentoMercado(levantamentoMercadoTO);

  controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);

 }

 /**
  * Incluir proposta.
  *
  * @param propostaTO the proposta to
  * @return the long
  * @throws Exception the exception
  */
 @WebMethod(operationName = "incluirProposta")
 @RequestWrapper(localName = "incluirPropostaRequest")
 @ResponseWrapper(localName = "incluirPropostaResposta")
 public Long incluirProposta(@WebParam(name = "propostaTO") PropostaTO propostaTO) throws Exception {

  ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
    ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

  ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  Proposta propostaConvertida = controladorProposta.converterPropostaTOemProposta(propostaTO);

  fachadaAtributo.inserirNumeroAnoProposta(propostaConvertida);

  if (propostaConvertida.getVersaoProposta() == null) {
   propostaConvertida.setVersaoProposta(1);
  }

  Long chave = controladorProposta.inserir(propostaConvertida);

  if (propostaTO.getChavePrimariaLM() > 0) {
   Proposta proposta = (Proposta) controladorProposta.obter(chave);
   LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(propostaTO
     .getChavePrimariaLM());
   levantamentoMercado.setProposta(proposta);
   controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);

  }

  return chave;
 }

 /**
  * Obter levantamento mercado.
  *
  * @param chavePrimaria the chave primaria
  * @return the levantamento mercado to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterLevantamentoMercado")
 @RequestWrapper(localName = "obterLevantamentoMercadoRequest")
 @ResponseWrapper(localName = "obterLevantamentoMercadoResposta")
 public LevantamentoMercadoTO obterLevantamentoMercado(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria) throws Exception {

  ControladorLevantamentoMercado controladorLevantamento = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  LevantamentoMercado levantamentoMercado = controladorLevantamento.obterLevantamentoMercado(chavePrimaria);

  return controladorLevantamento.converterLevantamentoMercadoEmLevantamentoMercadoTO(levantamentoMercado);
 }

 /**
  * Obter listas tela proposta.
  *
  * @param chavePrimariaLM the chave primaria lm
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterListasTelaProposta")
 @RequestWrapper(localName = "obterListasTelaPropostaRequest")
 @ResponseWrapper(localName = "obterListasTelaPropostaResposta")
 public ContainerListasTO obterListasTelaProposta(@WebParam(name = "chavePrimariaLM") Long chavePrimariaLM) throws Exception {

  ControladorLevantamentoMercado controladorLevantamentoMercado =
    (ControladorLevantamentoMercado) ServiceLocator.getInstancia().getBeanPorID("controladorLevantamentoMercado");

  ContainerListasTO container = new ContainerListasTO();

  Collection<Funcionario> listaFuncionario = fachadaAtributo.listarFuncionarios();

  Collection<TipoComboBoxTO> listaFuncionarioTO = new ArrayList<TipoComboBoxTO>();

  for (Funcionario funcionario : listaFuncionario) {
   TipoComboBoxTO funcionarioTO = new TipoComboBoxTO();
   funcionarioTO.setChavePrimaria(funcionario.getChavePrimaria());
   funcionarioTO.setDescricao(funcionario.getNome());
   listaFuncionarioTO.add(funcionarioTO);
  }

  Collection<Unidade> listaUnidadesConsumo = fachadaAtributo.listarUnidadesVolume();

  Collection<TipoComboBoxTO> listaUnidadesConsumoTO = new ArrayList<TipoComboBoxTO>();

  for (Unidade unidade : listaUnidadesConsumo) {
   TipoComboBoxTO unidadeTO = new TipoComboBoxTO();
   unidadeTO.setChavePrimaria(unidade.getChavePrimaria());
   unidadeTO.setDescricao(unidade.getDescricaoAbreviada());
   listaUnidadesConsumoTO.add(unidadeTO);
  }

  Collection<Tarifa> listaTarifas = fachadaAtributo.listarTarifas();

  Collection<TipoComboBoxTO> listaTarifaTO = new ArrayList<TipoComboBoxTO>();

  for (Tarifa tarifa : listaTarifas) {
   TipoComboBoxTO tarifaTO = new TipoComboBoxTO();
   tarifaTO.setChavePrimaria(tarifa.getChavePrimaria());
   tarifaTO.setDescricao(tarifa.getDescricao());
   listaTarifaTO.add(tarifaTO);
  }

  Collection<SituacaoProposta> situacoesIniciais = fachadaAtributo.listarSituacoesIniciaisProposta();
  Collection<TipoComboBoxTO> listaSituacaoPropostaTO = new ArrayList<TipoComboBoxTO>();

  for (SituacaoProposta situacaoProposta : situacoesIniciais) {
   TipoComboBoxTO situacaoPropostaTO = new TipoComboBoxTO();
   situacaoPropostaTO.setChavePrimaria(situacaoProposta.getChavePrimaria());
   situacaoPropostaTO.setDescricao(situacaoProposta.getDescricao());
   listaSituacaoPropostaTO.add(situacaoPropostaTO);
  }

  if (chavePrimariaLM != null && chavePrimariaLM > 0) {
   LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimariaLM);

   Map<String, BigDecimal> valores = fachadaAtributo.calcularValoresProposta(levantamentoMercado);

   container.setConsumoMedioAnual(Util.converterCampoValorParaString(valores.get("consumoMedioAnualTotal"),
     Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
   container.setConsumoMedioMensal(Util.converterCampoValorParaString(valores.get("consumoMedioMensalTotal"),
     Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
   container.setValorPrecoPago(Util.converterCampoValorParaString(valores.get("precoTotal"), Constantes.FORMATO_VALOR_MONETARIO_BR,
     Constantes.LOCALE_PADRAO));
   container.setValorGastoMensal(Util.converterCampoValorParaString(valores.get("valorMensalTotal"),
     Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
  }

  container.setListaFuncionarios(listaFuncionarioTO);
  container.setListaUnidadesConsumo(listaUnidadesConsumoTO);
  container.setListaTarifas(listaTarifaTO);
  container.setListaSituacaoProposta(listaSituacaoPropostaTO);

  return container;
 }

 /**
  * Calcular valor mensal gasto proposta.
  *
  * @param idTarifa the id tarifa
  * @param consumo  the consumo
  * @return the container listas to
  * @throws Exception the exception
  */
 @WebMethod(operationName = "calcularValorMensalGastoProposta")
 @RequestWrapper(localName = "calcularValorMensalGastoPropostaRequest")
 @ResponseWrapper(localName = "calcularValorMensalGastoPropostaResposta")
 public ContainerListasTO calcularValorMensalGastoProposta(@WebParam(name = "idTarifa") Long idTarifa,
                                                           @WebParam(name = "consumo") String consumo) throws Exception {

  AjaxService servico = new AjaxService();

  Map<String, String> valores = servico.calcularValorMensalGasto(idTarifa, consumo);

  ContainerListasTO container = new ContainerListasTO();
  container.setValorMensalGasto(valores.get("valorMensalGasto"));
  container.setValorMedioTarifa(valores.get("valorMedioTarifa"));

  return container;
 }

 /**
  * Imprimir proposta.
  *
  * @param chavePrimaria the chave primaria
  * @param numeroSerie   the numero serie
  * @return the byte[]
  * @throws Exception the exception
  */
 @WebMethod(operationName = "imprimirProposta")
 @RequestWrapper(localName = "imprimirPropostaRequest")
 @ResponseWrapper(localName = "imprimirPropostaResposta")
 public byte[] imprimirProposta(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria, @WebParam(name = "numeroSerie") String numeroSerie)
   throws Exception {

  Proposta proposta = fachadaAtributo.buscarPropostaPorChave(chavePrimaria);
  Map<String, Object> parametros = new HashMap<String, Object>();
  parametros.put("proposta", proposta);

  return fachadaAtributo.gerarPropostaPDF(parametros);
 }

 /**
  * Pesquisar propostas.
  *
  * @param idCliente      the id cliente
  * @param idImovel       the id imovel
  * @param habilitado     the habilitado
  * @param numeroProposta the numero proposta
  * @param chavePrimaria  the chave primaria
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "pesquisarPropostas")
 @RequestWrapper(localName = "pesquisarPropostasRequest")
 @ResponseWrapper(localName = "pesquisarPropostasResposta")
 public Collection<PropostaTO> pesquisarPropostas(@WebParam(name = "idCliente") Long idCliente,
                                                  @WebParam(name = "idImovel") Long idImovel,
                                                  @WebParam(name = HABILITADO) String habilitado,
                                                  @WebParam(name = "numeroProposta") String numeroProposta,
                                                  @WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria)
   throws Exception {

  ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
    ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

  Map<String, Object> filtro = new HashMap<String, Object>();
  this.prepararFiltroPesquisaProposta(filtro, idCliente, idImovel, habilitado, numeroProposta, chavePrimaria);
  Collection<Proposta> listaPropostas = fachadaAtributo.consultarPropostas(filtro);
  List<PropostaTO> listaPropostaTO = new ArrayList<PropostaTO>();

  if (listaPropostas != null) {
   for (Proposta proposta : listaPropostas) {
    PropostaTO propostaTO = controladorProposta.converterPropostaEmPropostaTO(proposta);
    listaPropostaTO.add(propostaTO);
   }
  }

  return listaPropostaTO;
 }

 /**
  * Preparar filtro pesquisa proposta.
  *
  * @param filtro         the filtro
  * @param idCliente      the id cliente
  * @param idImovel       the id imovel
  * @param habilitado     the habilitado
  * @param numeroProposta the numero proposta
  * @param chavePrimaria  the chave primaria
  * @throws Exception the exception
  */
 private void prepararFiltroPesquisaProposta(Map<String, Object> filtro, Long idCliente, Long idImovel, String habilitado,
                                             String numeroProposta, Long chavePrimaria) throws Exception {

  if ((idCliente != null) && (idCliente > 0)) {
   Map<String, Object> filtroCliente = new HashMap<String, Object>();
   filtroCliente.put("idCliente", idCliente);
   Collection<Imovel> listaImoveis = fachadaAtributo.consultarImoveis(filtroCliente);
   Collection<Long> chavesImoveis = new ArrayList<Long>();
   for (Imovel imovel : listaImoveis) {
    chavesImoveis.add(imovel.getChavePrimaria());
   }
   Long[] chaves = chavesImoveis.toArray(new Long[chavesImoveis.size()]);
   if (chaves.length > 0) {
    filtro.put("chavesImoveis", chaves);
   } else {
    Long flag = Long.valueOf("0");
    filtro.put("chavesImoveis", new Long[]{flag});
   }
  }

  if ((idImovel != null) && (idImovel > 0)) {
   filtro.put("idImovel", idImovel);
  }

  if (!StringUtils.isEmpty(habilitado)) {
   filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
  }

  if (!StringUtils.isEmpty(numeroProposta)) {
   filtro.put("numeroProposta", numeroProposta);
  }

  if (chavePrimaria != null && chavePrimaria > 0) {
   Long[] chavesPrimaria = {chavePrimaria};
   filtro.put("chavesPrimarias", chavesPrimaria);
  }
 }

 /**
  * Pesquisar clientes.
  *
  * @param nome         the nome
  * @param cnpj         the cnpj
  * @param cpf          the cpf
  * @param nomeFantasia the nome fantasia
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "pesquisarClientes")
 @RequestWrapper(localName = "pesquisarClientesRequest")
 @ResponseWrapper(localName = "pesquisarClientesResposta")
 public Collection<ClienteTO> pesquisarClientes(@WebParam(name = "nome") String nome, @WebParam(name = "cnpj") String cnpj,
                                                @WebParam(name = "cpf") String cpf,
                                                @WebParam(name = "nomeFantasia") String nomeFantasia) throws Exception {

  Map<String, Object> filtro = new HashMap<String, Object>();
  prepararFiltroCliente(filtro, nome, cpf, cnpj, nomeFantasia);
  Collection<Cliente> listaCliente = fachadaAtributo.consultarClientes(filtro);
  List<ClienteTO> listaClienteTO = new ArrayList<ClienteTO>();

  if (listaCliente != null) {
   for (Cliente cliente : listaCliente) {
    ClienteTO clienteTO = popularClienteTO(cliente);
    listaClienteTO.add(clienteTO);
   }
  }

  return listaClienteTO;
 }

 /**
  * Preparar filtro cliente.
  *
  * @param filtro       the filtro
  * @param nome         the nome
  * @param cpf          the cpf
  * @param cnpj         the cnpj
  * @param nomeFantasia the nome fantasia
  * @throws GGASException the GGAS exception
  */
 private void prepararFiltroCliente(Map<String, Object> filtro, String nome, String cpf, String cnpj, String nomeFantasia)
   throws GGASException {

  if (!StringUtils.isEmpty(nome)) {
   filtro.put("qualquerNome", nome);
  }

  if (!StringUtils.isEmpty(nomeFantasia)) {
   filtro.put("nomeFantasia", nomeFantasia);
  }

  if (!StringUtils.isEmpty(cnpj)) {
   filtro.put("cnpj", cnpj);
  }

  if (!StringUtils.isEmpty(cpf)) {
   filtro.put("cpf", cpf);
  }
 }

 /**
  * Pesquisar imoveis.
  *
  * @param nomeFantasia the nome fantasia
  * @param cep          the cep
  * @param matricula    the matricula
  * @param complemento  the complemento
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "pesquisarImoveis")
 @RequestWrapper(localName = "pesquisarImoveisRequest")
 @ResponseWrapper(localName = "pesquisarImoveisResposta")
 public Collection<ImovelTO> pesquisarImoveis(@WebParam(name = "nomeFantasia") String nomeFantasia, @WebParam(name = "cep") String cep,
                                              @WebParam(name = "matricula") String matricula,
                                              @WebParam(name = "complemento") String complemento) throws Exception {

  ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

  Map<String, Object> filtro = new HashMap<String, Object>();
  prepararFiltroImovel(filtro, nomeFantasia, cep, matricula, complemento);
  Collection<Imovel> listaImoveis = fachadaAtributo.consultarImoveis(filtro);
  List<ImovelTO> listaImovelTO = new ArrayList<ImovelTO>();

  if (listaImoveis != null) {
   for (Imovel imovel : listaImoveis) {
    ImovelTO imovelTO = controladorImovel.converterImovelEmImovelTO(imovel);
    listaImovelTO.add(imovelTO);
   }
  }

  return listaImovelTO;
 }

 /**
  * Preparar filtro imovel.
  *
  * @param filtro       the filtro
  * @param nomeFantasia the nome fantasia
  * @param cep          the cep
  * @param matricula    the matricula
  * @param complemento  the complemento
  * @throws FormatoInvalidoException the formato invalido exception
  */
 private void prepararFiltroImovel(Map<String, Object> filtro, String nomeFantasia, String cep, String matricula, String complemento)
   throws FormatoInvalidoException {

  if (!StringUtils.isEmpty(cep)) {
   filtro.put("cepImovel", cep);
  }

  if (!StringUtils.isEmpty(nomeFantasia)) {
   filtro.put("nome", nomeFantasia);
  }

  if (!StringUtils.isEmpty(matricula)) {
   filtro.put(CHAVE_PRIMARIA, Long.valueOf(matricula));
  }

  if (!StringUtils.isEmpty(complemento)) {
   filtro.put(COMPLEMENTO_IMOVEL, complemento);
  }
 }

 /**
  * Consultar nota debito.
  *
  * @param notaDebitoTO      the nota debito to
  * @param chavePontoConsumo the chave ponto consumo
  * @return the collection
  * @throws Exception the exception
  */
 @WebMethod(operationName = "consultarNotaDebito")
 @RequestWrapper(localName = "consultarNotaDebitoRequest")
 @ResponseWrapper(localName = "consultarNotaDebitoResposta")
 public Collection<NotaDebitoTO> consultarNotaDebito(@WebParam(name = "notaDebito") NotaDebitoTO notaDebitoTO,
                                                     @WebParam(name = "chavePontoConsumo") List<Long> chavePontoConsumo) throws Exception {

  Collection<NotaDebitoTO> colecaoNotaDebitoTO = new ArrayList<NotaDebitoTO>();
  Collection<Fatura> colecaoNotaDebito;
  Map<String, Object> filtro = new HashMap<String, Object>();

  popularFiltroPesquisaNotaDebito(notaDebitoTO, filtro, chavePontoConsumo);

  try {
   colecaoNotaDebito = controladorFatura.consultarNotaDebito(filtro);
  } catch (NegocioException e) {
   LOG.error(e.getStackTrace(), e);
   throw new Exception(e.getMessage());
  }

  if (colecaoNotaDebito != null && !colecaoNotaDebito.isEmpty()) {
   for (Fatura notaDebito : colecaoNotaDebito) {

    NotaDebitoTO nbTO = this.popularNotaDebitoTO(notaDebito);
    colecaoNotaDebitoTO.add(nbTO);
   }
   return colecaoNotaDebitoTO;
  }

  return null;
 }

 /**
  * Popular nota debito to.
  *
  * @param notaDebito the nota debito
  * @return the nota debito to
  * @throws NegocioException the negocio exception
  */
 private NotaDebitoTO popularNotaDebitoTO(Fatura notaDebito) throws NegocioException {

  NotaDebitoTO nbTO = new NotaDebitoTO();
  DocumentoFiscal documentoFiscal = controladorFatura.obterDocumentoFiscalPorFaturaMaisRecente(notaDebito.getChavePrimaria());
  if (documentoFiscal != null) {
   nbTO.setNumeroDocumentoFiscal(String.valueOf(documentoFiscal.getNumero()));
   nbTO.setNumeroSerie(String.valueOf(documentoFiscal.getSerie().getNumero()));
   nbTO.setChavePrimariaDocumentoFiscal(String.valueOf(documentoFiscal.getChavePrimaria()));
  }
  nbTO.setChavePrimaria(notaDebito.getChavePrimaria());
  nbTO.setValor(notaDebito.getValorTotalFormatado());
  nbTO.setDataEmissao(notaDebito.getDataEmissaoFormatada());
  nbTO.setDataVencimento(notaDebito.getDataVencimentoFormatada());
  nbTO.setSituacaoPagamento(notaDebito.getSituacaoPagamento().getDescricao());
  return nbTO;
 }

 /**
  * Popular filtro pesquisa nota debito.
  *
  * @param notaDebitoTO      the nota debito to
  * @param filtro            the filtro
  * @param chavePontoConsumo the chave ponto consumo
  */
 private void popularFiltroPesquisaNotaDebito(NotaDebitoTO notaDebitoTO, Map<String, Object> filtro, List<Long> chavePontoConsumo) {

  filtro.put("emissaoInicioNotaDebito", notaDebitoTO.getEmissaoInicioNotaDebito());
  filtro.put("emissaoFimNotaDebito", notaDebitoTO.getEmissaoFimNotaDebito());
  filtro.put("vencimentoFim", notaDebitoTO.getVencimentoFim());
  filtro.put("vencimentoInicio", notaDebitoTO.getVencimentoInicio());
  filtro.put("numeroDocumento", notaDebitoTO.getNumeroDocumento());
  filtro.put("emissao", notaDebitoTO.getEmissao());
  filtro.put("cancelamento", notaDebitoTO.getCancelamento());
  filtro.put("emissaoInicio", notaDebitoTO.getEmissaoInicio());
  filtro.put("emissaoFim", notaDebitoTO.getEmissaoFim());
  filtro.put("sisStatus", notaDebitoTO.getSisStatus());
  filtro.put("listaChavePontoConsumo", chavePontoConsumo);
 }

 /**
  * Verificar data vencimento boleto.
  *
  * @param chaveNotaDebito       the chave nota debito
  * @param numeroDocumentoFiscal the numero documento fiscal
  * @param numeroSerie           the numero serie
  * @param tipoArquivo           the tipo arquivo
  * @return the boolean
  * @throws NegocioException      the negocio exception
  * @throws NumberFormatException the number format exception
  */
 @WebMethod(operationName = "verificarDataVencimentoBoleto")
 @RequestWrapper(localName = "verificarDataVencimentoBoletoRequest")
 @ResponseWrapper(localName = "verificarDataVencimentoBoletoResposta")
 public Boolean verificarDataVencimentoBoleto(@WebParam(name = "chaveNotaDebito") String chaveNotaDebito,
                                              @WebParam(name = "numeroDocumentoFiscal") String numeroDocumentoFiscal,
                                              @WebParam(name = "numeroSerie") String numeroSerie,
                                              @WebParam(name = "tipoArquivo") String tipoArquivo)
   throws NegocioException {

  String parametroTpDoc = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);
  if ("BOLETONotaDebito".equals(tipoArquivo)) {
   if (chaveNotaDebito != null && !chaveNotaDebito.isEmpty()) {
    Collection<DocumentoCobranca> listaDocumentoCobranca = controladorDocumentoCobranca.consultarDocumentoCobrancaPelaFatura(
      Long.valueOf(chaveNotaDebito), controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc)));
    DocumentoCobranca documentoCobranca = retornaUltimoDocumentoCobrancaDaLista(listaDocumentoCobranca);
    return documentoCobranca.getDataVencimento().before(new Date());
   }
  } else {
   if (numeroDocumentoFiscal != null && !numeroDocumentoFiscal.isEmpty()) {
    DocumentoFiscal documentoFiscal = controladorFatura.obterDocumentoFiscalPorNumeroeSerie(
      Long.parseLong(numeroDocumentoFiscal), numeroSerie);
    Collection<DocumentoCobranca> listaDocumentoCobranca = controladorDocumentoCobranca.consultarDocumentoCobrancaPelaFatura(
      documentoFiscal.getFatura().getChavePrimaria(), null);
    DocumentoCobranca documentoCobranca = retornaUltimoDocumentoCobrancaDaLista(listaDocumentoCobranca);

    return documentoCobranca.getDataVencimento().before(new Date());
   }
  }
  return false;
 }

 /**
  * Retorna ultimo documento cobranca da lista.
  *
  * @param listaDocumentoCobranca the lista documento cobranca
  * @return the documento cobranca
  */
 private DocumentoCobranca retornaUltimoDocumentoCobrancaDaLista(Collection<DocumentoCobranca> listaDocumentoCobranca) {

  Iterator<DocumentoCobranca> iterator = listaDocumentoCobranca.iterator();
  DocumentoCobranca documentoCobranca = null;

  // Recupera o último documento de
  // cobrança
  if (iterator.hasNext()) {
   documentoCobranca = iterator.next();
   while (iterator.hasNext()) {
    DocumentoCobranca documentoCobrancaAuxiliar = iterator.next();
    if (Util.compararDatas(documentoCobranca.getDataEmissao(), documentoCobrancaAuxiliar.getDataEmissao()) < 0) {
     documentoCobranca = documentoCobrancaAuxiliar;
    }
   }
  }
  return documentoCobranca;
 }

 /**
  * Obter proposta pro chave.
  *
  * @param chavePrimaria the chave primaria
  * @return the proposta to
  */
 @WebMethod(operationName = "obterPropostaProChave")
 @RequestWrapper(localName = "obterPropostaProChaveRequest")
 @ResponseWrapper(localName = "obterPropostaProChaveResposta")
 public PropostaTO obterPropostaProChave(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria) {

  return obterProposta(chavePrimaria);

 }

 /**
  * Obter proposta.
  *
  * @param chavePrimaria the chave primaria
  * @return the proposta to
  */
 private PropostaTO obterProposta(Long chavePrimaria) {

  ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
    ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

  Proposta proposta = null;
  try {
   proposta = fachadaAtributo.buscarPropostaPorChave(chavePrimaria);
  } catch (GGASException e) {
   LOG.error(e.getMessage(), e);
  }

  try {
   return controladorProposta.converterPropostaEmPropostaTO(proposta);
  } catch (GGASException e) {
   LOG.error(e.getMessage(), e);
  }

  return null;

 }

 /**
  * Obter levantamento mercado por chave proposta.
  *
  * @param chaveProposta the chave proposta
  * @return the levantamento mercado to
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterLevantamentoMercadoPorChaveProposta")
 @RequestWrapper(localName = "obterLevantamentoMercadoPorChavePropostaRequest")
 @ResponseWrapper(localName = "obterLevantamentoMercadoPorChavePropostaResposta")
 public LevantamentoMercadoTO obterLevantamentoMercadoPorChaveProposta(@WebParam(name = "chaveProposta") Long chaveProposta)
   throws GGASException {

  ControladorLevantamentoMercado controladorLevantamento = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  LevantamentoMercadoTO levantamentoMercadoTO = null;
  if (chaveProposta != null && chaveProposta > 0) {
   LevantamentoMercado levantamentoMercado = controladorLevantamento.obterLevantamentoMercadoPorChaveProposta(chaveProposta);
   if (levantamentoMercado != null) {
    levantamentoMercadoTO = controladorLevantamento.converterLevantamentoMercadoEmLevantamentoMercadoTO(levantamentoMercado);
   }

  }

  return levantamentoMercadoTO;
 }

 /**
  * Atualizar proposta.
  *
  * @param propostaTO the proposta to
  * @return true, if successful
  */
 @WebMethod(operationName = "atualizarProposta")
 @RequestWrapper(localName = "atualizarPropostaRequest")
 @ResponseWrapper(localName = "atualizarPropostaResposta")
 public boolean atualizarProposta(@WebParam(name = "propostaTO") PropostaTO propostaTO) {

  ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
    ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

  try {
   Proposta propostaAtualizada = controladorProposta.converterPropostaTOemProposta(propostaTO);
   controladorProposta.atualizar(propostaAtualizada);
   return true;
  } catch (GGASException e) {
   LOG.error(e.getMessage(), e);
  }

  return false;
 }

 /**
  * Proposta.
  *
  * @param propostaTO the proposta to
  * @param situacao   the situacao
  * @return true, if successful
  * @throws NegocioException the negocio exception
  */
 @WebMethod(operationName = "atualizarSituacaoProposta")
 @RequestWrapper(localName = "atualizarSituacaoPropostaRequest")
 @ResponseWrapper(localName = "atualizarSituacaoPropostaResposta")
 public boolean Proposta(@WebParam(name = "propostaTO") PropostaTO propostaTO, @WebParam(name = "situacao") String situacao)
   throws NegocioException {

  ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
    ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

  Proposta propostaConvertida = null;
  if (REJEITADA.equalsIgnoreCase(situacao)) {
   SituacaoProposta situacaoProposta = fachadaAtributo.obterSituacaoPropostaPorDescricao(REJEITADA);
   try {
    propostaConvertida = controladorProposta.converterPropostaTOemProposta(propostaTO);
    if (situacaoProposta != null && propostaConvertida != null) {
     propostaConvertida.setSituacaoProposta(situacaoProposta);
     controladorProposta.atualizar(propostaConvertida);
    }

    return true;
   } catch (GGASException e) {
    LOG.error(e.getMessage(), e);
   }
  } else if (APROVADA.equalsIgnoreCase(situacao)) {
   SituacaoProposta situacaoProposta = fachadaAtributo.obterSituacaoPropostaPorDescricao(APROVADA);
   try {
    propostaConvertida = controladorProposta.converterPropostaTOemProposta(propostaTO);
    if (situacaoProposta != null && propostaConvertida != null) {
     propostaConvertida.setSituacaoProposta(situacaoProposta);
     controladorProposta.atualizar(propostaConvertida);
    }

    return true;
   } catch (GGASException e) {
    LOG.error(e.getMessage(), e);
   }

  }
  return false;
 }

 /**
  * Obter imovel por chave.
  *
  * @param chavePrimaria the chave primaria
  * @return the imovel to
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterImovelPorChave")
 @RequestWrapper(localName = "obterImovelPorChaveRequest")
 @ResponseWrapper(localName = "obterImovelPorChaveResposta")
 public ImovelTO obterImovelPorChave(@WebParam(name = CHAVE_PRIMARIA) Long chavePrimaria) throws GGASException {

  Imovel imovel = fachadaAtributo.buscarImovelPorChave(chavePrimaria);

  ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
    ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

  ControladorPontoConsumo controlador = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
    ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

  ImovelTO imovelTO = controladorImovel.converterImovelEmImovelTO(imovel);

  if (!CollectionUtils.isEmpty(imovel.getListaPontoConsumo())) {
   List<PontoConsumoTO> listaPontoConsumoTO = new ArrayList<PontoConsumoTO>();
   for (PontoConsumo pontoConsumo : imovel.getListaPontoConsumo()) {
    PontoConsumoTO pontoConsumoTO = controlador.converterPontoConsumoEmPontoConsumoTO(pontoConsumo);
    listaPontoConsumoTO.add(pontoConsumoTO);
   }

   imovelTO.getListaPontoConsumo().addAll(listaPontoConsumoTO);
  }

  return imovelTO;
 }

 /**
  * Obter entidade conteudo por constante sistema.
  *
  * @param constante the constante
  * @return the tipo combo box to
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterEntidadeConteudoPorConstanteSistema")
 @RequestWrapper(localName = "obterEntidadeConteudoPorConstanteSistemaRequest")
 @ResponseWrapper(localName = "obterEntidadeConteudoPorConstanteSistemaResposta")
 public TipoComboBoxTO obterEntidadeConteudoPorConstanteSistema(@WebParam(name = "constante") String constante) throws GGASException {

  EntidadeConteudo status =
    fachadaAtributo.obterEntidadeConteudo(
      Long.valueOf(fachadaAtributo.obterConstantePorCodigo(constante).getValor()));

  TipoComboBoxTO tipoComboBoxTO = null;
  if (status != null) {
   tipoComboBoxTO = new TipoComboBoxTO();
   tipoComboBoxTO.setChavePrimaria(status.getChavePrimaria());
   tipoComboBoxTO.setDescricao(status.getDescricao());
  }

  return tipoComboBoxTO;
 }

 /**
  * Incluir agenda visita agente.
  *
  * @param agendaVisitaAgenteTO the agenda visita agente to
  * @return true, if successful
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "incluirAgendaVisitaAgente")
 @RequestWrapper(localName = "incluirAgendaVisitaAgenteRequest")
 @ResponseWrapper(localName = "incluirAgendaVisitaAgenteResposta")
 public boolean incluirAgendaVisitaAgente(@WebParam(name = "agendaVisitaAgenteTO") AgendaVisitaAgenteTO agendaVisitaAgenteTO)
   throws GGASException {

  ControladorLevantamentoMercado controladorLevantamento = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  AgendaVisitaAgente agendaVisitaAgente = controladorLevantamento
    .converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(agendaVisitaAgenteTO);
  controladorLevantamento.inserirAgenda(agendaVisitaAgente);

  EntidadeConteudo statusPropostaAnalise = fachadaAtributo.obterEntidadeConteudo(Long.valueOf(fachadaAtributo.obterConstantePorCodigo(
    Constantes.C_LEVANTAMENTO_MERCADO_EM_ANALISE).getValor()));
  LevantamentoMercado levantamentoMercado = controladorLevantamento.obterLevantamentoMercadoChaveAgenda(agendaVisitaAgente
    .getChavePrimaria());
  levantamentoMercado.setStatus(statusPropostaAnalise);
  controladorLevantamento.atualizarLevantamentoMercado(levantamentoMercado);

  return true;
 }

 /**
  * Atualizar agenda visita agente.
  *
  * @param agendaVisitaAgenteTO the agenda visita agente to
  * @return true, if successful
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "atualizarAgendaVisitaAgente")
 @RequestWrapper(localName = "atualizarAgendaVisitaAgenteRequest")
 @ResponseWrapper(localName = "atualizarAgendaVisitaAgenteResposta")
 public boolean atualizarAgendaVisitaAgente(@WebParam(name = "agendaVisitaAgenteTO") AgendaVisitaAgenteTO agendaVisitaAgenteTO)
   throws GGASException {

  ControladorLevantamentoMercado controladorLevantamento = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  AgendaVisitaAgente agendaVisitaAgente = controladorLevantamento
    .converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(agendaVisitaAgenteTO);
  controladorLevantamento.atualizarAgenda(agendaVisitaAgente);

  LevantamentoMercado levantamentoMercado = controladorLevantamento.obterLevantamentoMercadoChaveAgenda(agendaVisitaAgente
    .getChavePrimaria());
  if (levantamentoMercado != null && levantamentoMercado.getStatus() == null) {
   EntidadeConteudo statusPropostaAnalise = fachadaAtributo.obterEntidadeConteudo(Long.valueOf(fachadaAtributo.obterConstantePorCodigo(
     Constantes.C_LEVANTAMENTO_MERCADO_EM_ANALISE).getValor()));
   levantamentoMercado.setStatus(statusPropostaAnalise);
   controladorLevantamento.atualizarLevantamentoMercado(levantamentoMercado);
  }

  return true;
 }

 /**
  * Obter agenda visita agente.
  *
  * @param chavePrimariaLM the chave primaria lm
  * @return the agenda visita agente to
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterAgendaVisitaAgente")
 @RequestWrapper(localName = "obterAgendaVisitaAgenteRequest")
 @ResponseWrapper(localName = "obterAgendaVisitaAgenteResposta")
 public AgendaVisitaAgenteTO obterAgendaVisitaAgente(@WebParam(name = "chavePrimariaLM") Long chavePrimariaLM) throws GGASException {

  ControladorLevantamentoMercado controladorLevantamento = (ControladorLevantamentoMercado) ServiceLocator.getInstancia()
    .getBeanPorID("controladorLevantamentoMercado");

  AgendaVisitaAgenteTO agendaVisitaAgenteTO = null;
  if (chavePrimariaLM != null && chavePrimariaLM > 0) {
   LevantamentoMercado levantamento = controladorLevantamento.obterLevantamentoMercado(chavePrimariaLM);
   if (levantamento != null && levantamento.getAgendaVisitaAgente() != null) {
    agendaVisitaAgenteTO = controladorLevantamento.converterAgendaVisitaAgenteEmAgendaVisitaAgenteTO(levantamento
      .getAgendaVisitaAgente());
   }
  }

  return agendaVisitaAgenteTO;
 }

 /**
  * Obter lista agente status tarifa.
  *
  * @param chaveSegmento the chave segmento
  * @return the container listas TO
  * @throws GGASException the GGAS exception
  */
 @WebMethod(operationName = "obterListaAgenteStatusTarifa")
 @RequestWrapper(localName = "obterListaAgenteStatusTarifaRequest")
 @ResponseWrapper(localName = "obterListaAgenteStatusTarifaResposta")
 public ContainerListasTO obterListaAgenteStatusTarifa(@WebParam(name = "chaveSegmento") Long chaveSegmento) throws GGASException {

  ControladorAgente controladorAgente = (ControladorAgente) ServiceLocator.getInstancia().getBeanPorID("controladorAgenteImpl");

  ContainerListasTO container = new ContainerListasTO();

  if (chaveSegmento != null) {
   Map<String, Object> filtro = new HashMap<String, Object>();
   filtro.put("idSegmento", chaveSegmento);
   Collection<Tarifa> tarifas = fachadaAtributo.consultarTarifas(filtro);
   Collection<TipoComboBoxTO> listaTarifaTO = new ArrayList<TipoComboBoxTO>();

   for (Tarifa tarifa : tarifas) {
    TipoComboBoxTO tarifaTO = new TipoComboBoxTO();
    tarifaTO.setChavePrimaria(tarifa.getChavePrimaria());
    tarifaTO.setDescricao(tarifa.getDescricao());
    listaTarifaTO.add(tarifaTO);
   }

   container.setListaTarifas(listaTarifaTO);
  } else {
   if (!CollectionUtils.isEmpty(controladorAgente.obterAgentePorChaveFuncionario(null, "true"))) {
    Collection<TipoComboBoxTO> listaAgenteTO = new ArrayList<TipoComboBoxTO>();
    for (Agente agente : controladorAgente.obterAgentePorChaveFuncionario(null, "true")) {
     TipoComboBoxTO combo = new TipoComboBoxTO();
     combo.setChavePrimaria(agente.getChavePrimaria());
     combo.setDescricao(agente.getFuncionario().getNome());
     listaAgenteTO.add(combo);
    }
    container.setListaAgentes(listaAgenteTO);
   }

   ControladorEntidadeConteudo controlador = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
     .getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
   ConstanteSistema constanteSistema = fachadaAtributo.obterConstantePorCodigo(Constantes.C_STATUS_LEVANTAMENTO_MERCADO);
   Collection<EntidadeConteudo> listaStatus = controlador.listarEntidadeConteudo(Long.valueOf(constanteSistema
     .getValor()));
   if (!CollectionUtils.isEmpty(listaStatus)) {
    Collection<TipoComboBoxTO> listaStatusTO = new ArrayList<TipoComboBoxTO>();
    for (EntidadeConteudo entidadeConteudo : listaStatus) {
     TipoComboBoxTO combo = new TipoComboBoxTO();
     combo.setChavePrimaria(entidadeConteudo.getChavePrimaria());
     combo.setDescricao(entidadeConteudo.getDescricao());
     listaStatusTO.add(combo);
    }
    // Usando (listaEstadoRede) atributo para carregar transportar a lista de status do levantamento de mercado.
    container.setListaEstadoRede(listaStatusTO);
   }

  }

  return container;
 }

 /**
  * Pesquisar solicitacao consumo ponto consumo modalidade.
  *
  * @param pontoConsumo o ponto de consumo
  * @return a ultima data
  */
 @WebMethod(operationName = "pesquisarSolicitacaoConsumoPontoConsumoModalidade")
 @RequestWrapper(localName = "pesquisarSolicitacaoConsumoPontoConsumoModalidadeRequest")
 @ResponseWrapper(localName = "pesquisarSolicitacaoConsumoPontoConsumoModalidadeResposta")
 public Long pesquisarSolicitacaoConsumoPontoConsumoModalidade(@WebParam(name = "pontoConsumo") Long pontoConsumo) {
  Collection<SolicitacaoConsumoPontoConsumo> solicitacoes =
    this.controladorProgramacao.pesquisarSolicitacaoConsumoPontoConsumoModalidade(pontoConsumo, null);
  Date ultimaData = Iterables.getLast(
    solicitacoes, (SolicitacaoConsumoPontoConsumo)
      this.controladorProgramacao.criarSolicitacaoConsumoPontoConsumo())
    .getDataSolicitacao();
  return ultimaData.getTime();
 }

 /**
  * Obter visibilidade das opções do menu
  *
  * @return the parametros
  * @throws Exception the exception
  */
 @WebMethod(operationName = "obterExibicaoMenu")
 @RequestWrapper(localName = "obterExibicaoMenuRequest")
 @ResponseWrapper(localName = "obterExibicaoMenuResposta")
 public List<String> obterExibicaoMenu() throws Exception {

  final String separador = ":";
  List<String> parametros = new ArrayList<>();
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA));
  parametros.add(getParametro(separador, Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO));
  return parametros;
 }

 private String getParametro(String separador, String exibirMenuAgenciaVirtualConsultaNotaFiscal) throws GGASException {
  ParametroSistema parametro;
  parametro = fachadaAtributo.obterParametroPorCodigo(exibirMenuAgenciaVirtualConsultaNotaFiscal);
  return exibirMenuAgenciaVirtualConsultaNotaFiscal + separador + parametro.getValor();
 }
}
