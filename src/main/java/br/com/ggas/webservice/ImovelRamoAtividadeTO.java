
package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade ImovelRamoAtividade para transferência
 *
 */
@XmlRootElement
public class ImovelRamoAtividadeTO {

	private Long chavePrimaria;

	private TipoComboBoxTO ramoAtividadeTO;

	private String quantidadeUnidadesConsumidoras;

	private ImovelTO imoveTO;

	private TipoComboBoxTO segmento;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public TipoComboBoxTO getRamoAtividadeTO() {

		return ramoAtividadeTO;
	}

	public void setRamoAtividadeTO(TipoComboBoxTO ramoAtividadeTO) {

		this.ramoAtividadeTO = ramoAtividadeTO;
	}

	public String getQuantidadeUnidadesConsumidoras() {

		return quantidadeUnidadesConsumidoras;
	}

	public void setQuantidadeUnidadesConsumidoras(String quantidadeUnidadesConsumidoras) {

		this.quantidadeUnidadesConsumidoras = quantidadeUnidadesConsumidoras;
	}

	public ImovelTO getImoveTO() {

		return imoveTO;
	}

	public void setImoveTO(ImovelTO imoveTO) {

		this.imoveTO = imoveTO;
	}

	public TipoComboBoxTO getSegmento() {

		return segmento;
	}

	public void setSegmento(TipoComboBoxTO segmento) {

		this.segmento = segmento;
	}

}
