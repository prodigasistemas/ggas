/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/01/2014 18:47:51
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade Nfe para transferência
 *
 */
@XmlRootElement
public class NfeTO {

	private Long chavePrimariaDocumentoFiscal;

	private Long chavePrimariaNfe;

	private String serie;

	private Long documentoFiscal;

	private String emissao;

	private String valorNotaFiscal;

	private byte[] danfePdf;

	private String sisStatus;

	private byte[] dsArquivoXml;

	private String cancelamento;

	private boolean indicadorDsArquivoXml;

	private boolean indicadorDanfePdf;

	private boolean indicadorNfe;

	private String emissaoInicio;

	private String emissaoFim;

	private String dataVencimento;

	/**
	 * Gets the chave primaria documento fiscal.
	 * 
	 * @return the chave primaria documento fiscal
	 */
	public Long getchavePrimariaDocumentoFiscal() {

		return chavePrimariaDocumentoFiscal;
	}

	/**
	 * Sets the chave primaria documento fiscal.
	 * 
	 * @param chavePrimariaDocumentoFiscal
	 *            the new chave primaria documento fiscal
	 */
	public void setchavePrimariaDocumentoFiscal(Long chavePrimariaDocumentoFiscal) {

		this.chavePrimariaDocumentoFiscal = chavePrimariaDocumentoFiscal;
	}

	public String getSerie() {

		return serie;
	}

	public void setSerie(String serie) {

		this.serie = serie;
	}

	public Long getDocumentoFiscal() {

		return documentoFiscal;
	}

	public void setDocumentoFiscal(Long documentoFiscal) {

		this.documentoFiscal = documentoFiscal;
	}

	public String getEmissao() {

		return emissao;
	}

	public void setEmissao(String emissao) {

		this.emissao = emissao;
	}

	public String getValorNotaFiscal() {

		return valorNotaFiscal;
	}

	public void setValorNotaFiscal(String valorNotaFiscal) {

		this.valorNotaFiscal = valorNotaFiscal;
	}

	public byte[] getDanfePdf() {
		byte[] retorno = null;
		if (this.danfePdf != null) {
			retorno = danfePdf.clone();
		}
		return retorno;
	}

	public void setDanfePdf(byte[] danfePdf) {

		this.danfePdf = danfePdf;
	}

	public String getSisStatus() {

		return sisStatus;
	}

	public void setSisStatus(String sisStatus) {

		this.sisStatus = sisStatus;
	}

	public byte[] getDsArquivoXml() {

		return dsArquivoXml;
	}

	public void setDsArquivoXml(byte[] dsArquivoXml) {

		this.dsArquivoXml = dsArquivoXml;
	}

	public String getCancelamento() {

		return cancelamento;
	}

	public void setCancelamento(String cancelamento) {

		this.cancelamento = cancelamento;
	}

	public boolean isIndicadorDsArquivoXml() {

		return indicadorDsArquivoXml;
	}

	public void setIndicadorDsArquivoXml(boolean indicadorDsArquivoXml) {

		this.indicadorDsArquivoXml = indicadorDsArquivoXml;
	}

	public boolean isIndicadorDanfePdf() {

		return indicadorDanfePdf;
	}

	public void setIndicadorDanfePdf(boolean indicadorDanfePdf) {

		this.indicadorDanfePdf = indicadorDanfePdf;
	}

	public boolean isIndicadorNfe() {

		return indicadorNfe;
	}

	public void setIndicadorNfe(boolean indicadorNfe) {

		this.indicadorNfe = indicadorNfe;
	}

	public Long getChavePrimariaNfe() {

		return chavePrimariaNfe;
	}

	public void setChavePrimariaNfe(Long chavePrimariaNfe) {

		this.chavePrimariaNfe = chavePrimariaNfe;
	}

	public String getEmissaoInicio() {

		return emissaoInicio;
	}

	public void setEmissaoInicio(String emissaoInicio) {

		this.emissaoInicio = emissaoInicio;
	}

	public String getEmissaoFim() {

		return emissaoFim;
	}

	public void setEmissaoFim(String emissaoFim) {

		this.emissaoFim = emissaoFim;
	}

	public String getDataVencimento() {

		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

}
