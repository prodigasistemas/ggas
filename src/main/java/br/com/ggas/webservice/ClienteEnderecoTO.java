/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 13:38:29
 @author ccavalcanti
 */

package br.com.ggas.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ClienteEnderecoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -87159101453317532L;

	private Long chavePrimaria;

	private CepTO cep;

	private String numeroEndereco;

	private String complemento;

	private Integer caixaPostal;

	private TipoComboBoxTO tipoEndereco;

	private String enderecoReferencia;

	private Long chavePrimariaMunicipio;

	private Long chavePrimariaTipoAcao;

	private boolean correspondencia;

	private TipoComboBoxTO tipoAcao;

	private boolean indicadorExclusao;

	private boolean indicadorRegistroExistente;

	private String valorCep;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public CepTO getCep() {

		return cep;
	}

	public void setCep(CepTO cep) {

		this.cep = cep;
	}

	public String getNumeroEndereco() {

		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {

		this.numeroEndereco = numeroEndereco;
	}

	public String getComplemento() {

		return complemento;
	}

	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	public Integer getCaixaPostal() {

		return caixaPostal;
	}

	public void setCaixaPostal(Integer caixaPostal) {

		this.caixaPostal = caixaPostal;
	}

	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	public TipoComboBoxTO getTipoEndereco() {

		return tipoEndereco;
	}

	public void setTipoEndereco(TipoComboBoxTO tipoEndereco) {

		this.tipoEndereco = tipoEndereco;
	}

	public Long getChavePrimariaMunicipio() {

		return chavePrimariaMunicipio;
	}

	public void setChavePrimariaMunicipio(Long chavePrimariaMunicipio) {

		this.chavePrimariaMunicipio = chavePrimariaMunicipio;
	}

	public Long getChavePrimariaTipoAcao() {

		return chavePrimariaTipoAcao;
	}

	public void setChavePrimariaTipoAcao(Long chavePrimariaTipoAcao) {

		this.chavePrimariaTipoAcao = chavePrimariaTipoAcao;
	}

	public boolean isCorrespondencia() {

		return correspondencia;
	}

	public void setCorrespondencia(boolean correspondencia) {

		this.correspondencia = correspondencia;
	}

	public TipoComboBoxTO getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(TipoComboBoxTO tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	public boolean isIndicadorExclusao() {

		return indicadorExclusao;
	}

	public void setIndicadorExclusao(boolean indicadorExclusao) {

		this.indicadorExclusao = indicadorExclusao;
	}

	public boolean isIndicadorRegistroExistente() {

		return indicadorRegistroExistente;
	}

	public void setIndicadorRegistroExistente(boolean indicadorRegistroExistente) {

		this.indicadorRegistroExistente = indicadorRegistroExistente;
	}

	public String getValorCep() {

		return valorCep;
	}

	public void setValorCep(String valorCep) {

		this.valorCep = valorCep;
	}

}
