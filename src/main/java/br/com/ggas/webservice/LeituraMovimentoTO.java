/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 31/08/2015 17:24:42
 @author vtavares
 */

package br.com.ggas.webservice;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.SerializationUtils;
/**
 * 
 * Classe responsável pela representação da entidade LeituraMovimento para transferência
 *
 */
@XmlRootElement
public class LeituraMovimentoTO {

	private Long chavePrimaria;

	private Integer anoMesFaturamento;

	private Integer ciclo;

	private Long chavePontoConsumo;

	private BigDecimal valorLeitura;

	private Date dataLeitura;

	private Long chaveAnormalidadeLeitura;

	private boolean indicadorConfirmacaoLeitura;

	private Long chaveImovel;

	private String numeroRota;

	private BigDecimal leituraAnterior;

	private String numeroSerieMedidor;

	private String endereco;

	private Long chaveSituacaoConsumo;

	private String descricaoSituacaoConsumo;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	public Integer getCiclo() {

		return ciclo;
	}

	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}

	public Long getChavePontoConsumo() {

		return chavePontoConsumo;
	}

	public void setChavePontoConsumo(Long chavePontoConsumo) {

		this.chavePontoConsumo = chavePontoConsumo;
	}

	public BigDecimal getValorLeitura() {

		return valorLeitura;
	}

	public void setValorLeitura(BigDecimal valorLeitura) {

		this.valorLeitura = valorLeitura;
	}

	public Date getDataLeitura() {

		return (Date) SerializationUtils.clone(dataLeitura);
	}

	public void setDataLeitura(Date dataLeitura) {

		this.dataLeitura = dataLeitura;
	}

	public Long getChaveAnormalidadeLeitura() {

		return chaveAnormalidadeLeitura;
	}

	public void setChaveAnormalidadeLeitura(Long chaveAnormalidadeLeitura) {

		this.chaveAnormalidadeLeitura = chaveAnormalidadeLeitura;
	}

	public boolean isIndicadorConfirmacaoLeitura() {

		return indicadorConfirmacaoLeitura;
	}

	public void setIndicadorConfirmacaoLeitura(boolean indicadorConfirmacaoLeitura) {

		this.indicadorConfirmacaoLeitura = indicadorConfirmacaoLeitura;
	}

	public Long getChaveImovel() {

		return chaveImovel;
	}

	public void setChaveImovel(Long chaveImovel) {

		this.chaveImovel = chaveImovel;
	}

	public String getNumeroRota() {

		return numeroRota;
	}

	public void setNumeroRota(String numeroRota) {

		this.numeroRota = numeroRota;
	}

	public BigDecimal getLeituraAnterior() {

		return leituraAnterior;
	}

	public void setLeituraAnterior(BigDecimal leituraAnterior) {

		this.leituraAnterior = leituraAnterior;
	}

	public String getNumeroSerieMedidor() {

		return numeroSerieMedidor;
	}

	public void setNumeroSerieMedidor(String numeroSerieMedidor) {

		this.numeroSerieMedidor = numeroSerieMedidor;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public Long getChaveSituacaoConsumo() {

		return chaveSituacaoConsumo;
	}

	public void setChaveSituacaoConsumo(Long chaveSituacaoConsumo) {

		this.chaveSituacaoConsumo = chaveSituacaoConsumo;
	}

	public String getDescricaoSituacaoConsumo() {

		return descricaoSituacaoConsumo;
	}

	public void setDescricaoSituacaoConsumo(String descricaoSituacaoConsumo) {

		this.descricaoSituacaoConsumo = descricaoSituacaoConsumo;
	}

}
