/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.webservice;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * Classe responsável pela representação da entidade EnderecoTemporarioFaturamento para transferência
 *
 */
@XmlRootElement
public class EnderecoTemporarioFaturamentoTO {

	private Long chaveContrato;

	private Long chavePontoConsumo;

	private CepTO cep;

	private String numeroImovel;

	private String complemento;
	
	private String email;

	public Long getChaveContrato() {

		return chaveContrato;
	}

	public void setChaveContrato(Long chaveContrato) {

		this.chaveContrato = chaveContrato;
	}

	public Long getChavePontoConsumo() {

		return chavePontoConsumo;
	}

	public void setChavePontoConsumo(Long chavePontoConsumo) {

		this.chavePontoConsumo = chavePontoConsumo;
	}

	public CepTO getCep() {

		return cep;
	}

	public void setCep(CepTO cep) {

		this.cep = cep;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getComplemento() {

		return complemento;
	}

	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

}

