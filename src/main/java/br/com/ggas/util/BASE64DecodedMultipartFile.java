package br.com.ggas.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

/**
 * Classe para converter FormFile para MultipartFile
 *
 */
public class BASE64DecodedMultipartFile implements MultipartFile {
	private final byte[] imgContent;
	private String contentType;
	private String name;
	private String originalFilename;

	/**
	 * Construtor
	 * 
	 * @param imgContent - byte[]
	 */
	public BASE64DecodedMultipartFile(byte[] imgContent) {
		this.imgContent = imgContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * Atribui name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
		this.originalFilename = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getOriginalFilename()
	 */
	@Override
	public String getOriginalFilename() {
		return originalFilename;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getContentType()
	 */
	@Override
	public String getContentType() {
		return contentType;
	}

	/**
	 * Atribui contentType
	 * 
	 * @param contentType
	 */
	public void setContentType(String contentType){
		this.contentType = contentType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return imgContent == null || imgContent.length == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getSize()
	 */
	@Override
	public long getSize() {
		return imgContent.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getBytes()
	 */
	@Override
	public byte[] getBytes() throws IOException {
		return imgContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#getInputStream()
	 */
	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(imgContent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.multipart.MultipartFile#transferTo(java.io.File)
	 */
	@Override
	public void transferTo(File dest) throws IOException {
		FileOutputStream stream = new FileOutputStream(dest);
		stream.write(imgContent);
		stream.close();
	}
}
