package br.com.ggas.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.geral.exception.NegocioException;

/**
 * Classe utilitária, calculadora genérica de determinados dados
 * @param <T> tipo do dado
 */
public class GenericUtil<T> {

	/**
	 * Cálcula a média de uma lista de valores
	 * @param lista lista de valores
	 * @param metodoValor método get a ser invocado
	 * @return retorna a media dos dados na lista
	 * @throws NegocioException lançada caso ocorra alguma falha
	 */
	public BigDecimal calcularMediaLista(Collection<T> lista, String metodoValor) throws NegocioException {	
		return calcularMediaLista(lista, metodoValor, 0);
	}

	/**
	 * Calcula a média em uma lista usando escala
	 * @param lista lista
	 * @param metodoValor método get
	 * @param escala escala
	 * @return retona a média
	 * @throws NegocioException exceção lançada caso ocorra alguam falha na operação
	 */
	public BigDecimal calcularMediaLista(Collection<T> lista, String metodoValor, int escala) throws NegocioException {
		
		BigDecimal soma = BigDecimal.ZERO;
		
		if(lista.isEmpty()) {
			return soma; 
		}
		
		
		for (T t : lista) {
			try {
				Method metodo = t.getClass().getMethod(metodoValor);
				Class<?> clazz = metodo.getReturnType();
				
				String retorno = clazz.cast(metodo.invoke(t)).toString();
				BigDecimal valor = new BigDecimal(retorno);
				
				soma = soma.add(valor);
			} catch (Exception e) {
				throw new NegocioException(e);
			}
		}
		
		return soma.divide(BigDecimal.valueOf(lista.size()), escala, BigDecimal.ROUND_HALF_UP);
	}
}
