/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.tarefaassincrona.ProcessDispatcher;

public class ProcessadorBatch {

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	public static final String BEAN_ID_PROCESSADOR = "processadorBatch";

	private static final Logger LOG = Logger.getLogger(ProcessadorBatch.class);

	private final ServiceLocator serviceLocator;

	private final ControladorProcesso controladorProcesso;

	/**
	 * Construtor padrão.
	 */
	public ProcessadorBatch() {

		serviceLocator = ServiceLocator.getInstancia();
		controladorProcesso = (ControladorProcesso) serviceLocator.getControladorNegocio(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
	}

	/**
	 * Método responsável por limpar os processos
	 * que ficaram presos em
	 * execução.
	 */
	public void clean() {
		try {
			LOG.info("Reiniciando os processos...");
			controladorProcesso.reiniciarProcessos();
		} catch(GGASException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Método responsável por verificar os
	 * processos pendentes e processa-los.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void run() throws GGASException {

		Collection<Long> chavesProcessosPendentes = null;

		// Obter o processos em espera
		try {
			LOG.info("Verificando processo em espera...");
			chavesProcessosPendentes = controladorProcesso.consultarPendentes();
		} catch(GGASException e) {
			LOG.error(e.getMessage(), e);
		}
		System.out.println("  ->  Tem processos pendentes? " + (chavesProcessosPendentes != null && !chavesProcessosPendentes.isEmpty()) + " [" + new Date() + "] ");
		if(chavesProcessosPendentes != null && !chavesProcessosPendentes.isEmpty()) {
			LOG.info("Capturando processos em espera: " + chavesProcessosPendentes.size());
		
			for (Iterator<Long> iterator = chavesProcessosPendentes.iterator(); iterator.hasNext();) {
				Long chaveProcesso = iterator.next();
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				System.out.println("[" + chaveProcesso + "]  ->  Processo em espera...  [" + new Date() + "] ");
				System.out.println("    ->  IS_PARALLEL_BATCH? " + Constantes.IS_PARALLEL_BATCH + " [" + new Date() + "] ");
				if(Constantes.IS_PARALLEL_BATCH) {
					System.out.println("[" + chaveProcesso + "]  ->  COM processamento paralelo [" + new Date() + "] ");
					processarEmParalelo(chaveProcesso);
				} else {
					System.out.println("[" + chaveProcesso + "]  ->  SEM processamento paralelo [" + new Date() + "] ");
					processar(chaveProcesso);
					
					System.out.println("[" + chaveProcesso + "]  ->  FIM PROCESSAMENTO [" + new Date() + "] ");
				}
			}
		}
	}

	private void processarEmParalelo(Long chaveProcesso) throws NegocioException {
		Processo processo = (Processo) controladorProcesso.obter(chaveProcesso);
		
		if (processoEmEspera(processo)) {
			
			atualizarSituacaoProcesso(SituacaoProcesso.SITUACAO_EXECUCAO, processo);

			new Thread() {
				@Override
				public void run() {
					new ProcessDispatcher().dispatch(String.valueOf(chaveProcesso));
				}
			}.start();
		}
	}

	/**
	 * Método responsável por fazer o
	 * processamento e acompanhar o resultado.
	 * 
	 * @param chaveProcesso
	 *            the chave processo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void processar(Long chaveProcesso) throws GGASException {
		System.out.println("[" + chaveProcesso + "]    ->  Iniciando processo [" + new Date() + "] ");
		Processo processo = null;

		try {
			// Consultando o processo no banco de
			// dados
			processo = verificarOperacaoProcesso(chaveProcesso);
			// Verificando se não existe outro
			// processo executando a mesma
			// operação
			
		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
			System.out.println("[" + chaveProcesso + "]      -> ERRO AO CONSULTAR SE EXISTE PROCESSO COM A MESMA OPERAÇÃO EM EXECUÇÃO! ");
			e.printStackTrace();
			processo = null;
		}
		
		// Verificando se o processo ainda
		// continua em espera
		if(processo != null) {
			System.out.println("[" + chaveProcesso + "]    ->  Iniciando execução do processo [" + new Date() + "] ");
			// Executando o processo
			processar(processo);
			System.out.println("[" + chaveProcesso + "]    ->  Fim execução do processo  [" + new Date() + "] ");
			
		}
	}

	private synchronized Processo verificarOperacaoProcesso(Long chaveProcesso) throws NegocioException {
		Processo processo = (Processo) controladorProcesso.obter(chaveProcesso);
		boolean operacaoEmExecucao = false;

		operacaoEmExecucao = controladorProcesso.existeProcessoExecutandoOperacao(processo.getOperacao());

		if ((processo != null && processo.getSituacao() == SituacaoProcesso.SITUACAO_ESPERA) && !operacaoEmExecucao) {
			processo = atualizarSituacaoProcesso(SituacaoProcesso.SITUACAO_EXECUCAO, processo);
			return processo;
		} else {
			return null;
		}
	}

	/**
	 * Método responsável executar um processo batch.
	 * @param chaveProcesso - {@link Long}
	 * @return processo - {@link Processo}
	 */
	public Processo executarProcesso(Long chaveProcesso) {
		Processo processo = null;

		try {

			processo = (Processo) controladorProcesso.obter(chaveProcesso);
			if (processo != null) {
				processar(processo);
			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
		}

		return processo;
	}

	private void processar(Processo processo) throws GGASException {
		Date dataHoraInicioProcesso = new Date();
		try {
			if (processo != null) {
				String logExecucao = controladorProcesso.executar(processo);
				if (logExecucao != null && logExecucao.length() > 0) {
					processo.setLogExecucao(logExecucao.getBytes());
				}
				processo.setSituacao(SituacaoProcesso.SITUACAO_CONCLUIDO);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			try {
				if (processo.getLogErro() == null) {
					if (e instanceof NegocioException) {
						NegocioException en = (NegocioException) e;
						String log = montarLogErroFatorPTZ(processo, en, dataHoraInicioProcesso);
						processo.setLogErro((MensagemUtil.gerarMensagemErro(e) + log).getBytes());

					} else {
						processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
					}

				}
			} catch (Exception e2) {
				LOG.info(e);
				LOG.error(e.getMessage(), e);
				processo.setLogErro(e.getMessage().getBytes());
			}
			processo.setSituacao(SituacaoProcesso.SITUACAO_ERRO);
		}

		// Finalizando o processo
		try {
			if(processo != null) {
				processo.setFim(Calendar.getInstance().getTime());
				controladorProcesso.atualizar(processo);
			}
		} catch(GGASException e) {
			LOG.error(e.getMessage(), e);
		}

		// Enviar o email para o responsável se houver
		
		if(processo != null && !StringUtils.isEmpty(processo.getEmailResponsavel())) {
						
			controladorProcesso.enviarEmail(processo.getEmailResponsavel(), montarConteudoEmail(processo));
						
		}
	}
	
	private Boolean processoEmEspera(Processo processo) {
		return processo != null && processo.getSituacao() == SituacaoProcesso.SITUACAO_ESPERA;
	}

	private Processo atualizarSituacaoProcesso(Integer situacao, Processo processo) {
		try {
			ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
							(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));
			System.out.println("[" + processo.getChavePrimaria() + "]      ->  Executanto processo...  [" + new Date() + "] ");
			processo.setInicio(Calendar.getInstance().getTime());
			processo.setSituacao(situacao);
			processo.setDadosAuditoria(dadosAuditoria);
			System.out.println("[" + processo.getChavePrimaria() + "]        ->  início: " + processo.getInicio() +  "  [" + new Date() + "] ");
			System.out.println("[" + processo.getChavePrimaria() + "]        ->  situacao: " + processo.getSituacao() +  "  [" + new Date() + "] ");
			controladorProcesso.atualizar(processo);
		} catch(GGASException e) {
			LOG.error(e.getMessage(), e);
			System.out.println("[" + processo.getChavePrimaria() + "] ERRO AO EXECUTAR PROCESSO  [" + new Date() + "] ");
			e.printStackTrace();
			processo = null;
		}
		return processo;
	}

	/**
	 * Montar conteudo email.
	 * 
	 * @param processo
	 *            the processo
	 * @return the string
	 */
	private String montarConteudoEmail(Processo processo) {

		StringBuilder conteudoEmail = new StringBuilder();
		conteudoEmail.append("Código do Processo: ").append(processo.getChavePrimaria()).append("\n");
		conteudoEmail.append("Descrição do Processo: ").append(processo.getOperacao().getDescricao()).append("\n");
		conteudoEmail.append("Início do Processo: ")
				.append(Util.converterDataHoraParaString(processo.getInicio())).append("\n");
		conteudoEmail.append("Fim do Processo: ").append(Util.converterDataHoraParaString(processo.getFim()))
				.append("\n");

		SituacaoProcesso situacao = controladorProcesso.obterSituacaoProcesso(processo.getSituacao());

		conteudoEmail.append("Situação do Processo: ").append(situacao.getDescricao()).append("\n");
		if(situacao.getCodigo() == SituacaoProcesso.SITUACAO_ERRO) {
			String motivoErro = new String(processo.getLogErro()).split("\n")[0];
			conteudoEmail.append("Motivo: ").append(motivoErro);
		}

		conteudoEmail.append("Data Inicial Agendamento: ")
				.append(Util.converterDataHoraParaString(processo.getDataInicioAgendamento())).append("\n");

		String dataFinal = "*";
		if(processo.getDataFinalAgendamento() != null) {
			dataFinal = Util.converterDataHoraParaString(processo.getDataFinalAgendamento());
		}

		conteudoEmail.append("Data Final Agendamento: ").append(dataFinal).append(" \r\n\r\n");

		if(processo.getSituacao() == SituacaoProcesso.SITUACAO_CONCLUIDO) {
			String proximaExecucao = "Processo Finalizado!";
			Processo proximoProcesso = controladorProcesso.getProximoProcessoEmEsperaPorOperacao(processo.getOperacao());

			if(proximoProcesso != null) {
				proximaExecucao = Util
								.converterDataHoraParaString(proximoProcesso.getDataInicioAgendamento());
				conteudoEmail.append("Próxima Execução: ");
				conteudoEmail.append(proximaExecucao);
			}
		}
		if(processo.isEnviarLogExecucao()) {
			conteudoEmail.append("\r\n ====================================== \r\n");
			conteudoEmail.append("Resultado do processamento: \r\n");
			conteudoEmail.append(new String(processo.getLogExecucao()));
		}
		return conteudoEmail.toString();

	}
	
	private String montarLogErroFatorPTZ(Processo processo, NegocioException en, Date inicioProcesso) throws GGASException{
		StringBuilder logExecucao = new StringBuilder();
		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		GrupoFaturamento grupoFaturamento = null;
		String idGrupoFaturamento = processo.getParametros().get("idGrupoFaturamento");
		
		if(inicioProcesso != null){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(inicioProcesso);
			String dataHora = Util
							.converterDataHoraParaString(Calendar.getInstance().getTime());
			logExecucao.append("[").append(dataHora).append("]").append("Iniciando o processamento do Batch. \r\n");
		}
		
		if(!StringUtils.isEmpty(idGrupoFaturamento)){
			grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));
			if(grupoFaturamento != null){
				logExecucao.append("Consistindo leitura e consumo para o grupo:").append(grupoFaturamento.getDescricao()).append(". \r\n");
			} else {
				logExecucao.append("Grupo faturamento nulo para o idGrupoFaturamento:").append(idGrupoFaturamento).append(". \r\n");
			}
			
		}
		if(en.getErros() != null && en.getErros().get(ID_PONTO_CONSUMO) != null){
			Long idPontoConsumo = (Long) en.getErros().get(ID_PONTO_CONSUMO);
			PontoConsumo pontoConsumo = null;
			if(idPontoConsumo != null && idPontoConsumo > 0){
				pontoConsumo = Fachada.getInstancia().buscarPontoConsumoPorChave(idPontoConsumo);
			}
			en.getErros().remove(ID_PONTO_CONSUMO);
			if(pontoConsumo != null){
				Rota rota = pontoConsumo.getRota();
				if(rota != null){
					ControladorPontoConsumo controladorPontoConsumo = 
									(ControladorPontoConsumo) ServiceLocator.getInstancia().
									getBeanPorID(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
					Collection<PontoConsumo> pontosConsumoAtivos = controladorPontoConsumo.consultarPontoConsumo(null, rota);
					logExecucao.append("Quantidade de Pontos de Consumo ativos: ").append(pontosConsumoAtivos.size()).append("\r\n");
					
					if(en.getErros().get("ERRO_NEGOCIO_HISTORICO_CONSUMO_PTZ") != null){
						logExecucao.append("Não foi possível atualizar o cronograma da Rota").append(rota.getNumeroRota())
								.append(" para a atividade CONSISTIR LEITURA E CONSUMO. \r\n");
						
						logExecucao.append("Nao foi possivel atualizar o cronograma do grupo porque o Fator PTZ e superior a 99.");
					}
				}
			}
			
		}
		
		
		return logExecucao.toString();
	}

}
