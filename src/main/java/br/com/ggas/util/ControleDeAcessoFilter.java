/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.util;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
import static br.com.ggas.util.Constantes.IS_AMBIENTE_PRODUCAO;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.GGASException;

/**
 *
 *
 */
public class ControleDeAcessoFilter extends HttpServlet implements Filter {

	private static final String ATRIBUTO_MENSAGEM_TELA = "mensagemTela";

	private static final String MENSAGEM_ACESSO_NEGADO = "Acesso negado ou sessão expirada.";

	private static final long serialVersionUID = -1164845633228355281L;

	private static final String PARAMETRO_RECURSO_LIBERADO = "recursoLiberado";

	private static final Logger LOG = Logger.getLogger(ControleDeAcessoFilter.class);

	private final Fachada fachada = Fachada.getInstancia();

	private static final String ATRIBUTO_PERMISSAO = "permissao";

	private static final String ATRIBUTO_MODULO = "modulo";

	private static final String FORWARD_INDEX = "/index.jsp";

	private static final String HOME_URL = "/exibirHome";

	private final Collection<String> recursosLiberados = new HashSet<String>();

	private static Operacao operacao = null;

	private static Permissao p = null;

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		String recursoLiberado = filterConfig.getInitParameter(PARAMETRO_RECURSO_LIBERADO);

		if (!StringUtils.isEmpty(recursoLiberado)) {
			StringTokenizer stringTokenizer = new StringTokenizer(recursoLiberado, ",");
			while (stringTokenizer.hasMoreElements()) {
				String recurso = (String) stringTokenizer.nextElement();
				recursosLiberados.add(recurso.trim());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		boolean autorizado = true;
		Boolean recursoCadastrado = Boolean.TRUE;
		Modulo modulo = null;
		String recurso = null;
		Permissao permissao = null;
		HttpSession session = null;
		Usuario usuario = this.getUsuario(request);
		recurso = this.getRecurso(request);

		if (verificarURLs(recurso)) {
			try {
				if (!StringUtils.isEmpty(recurso) && !verificarRecursosLiberados(recurso)) {
					if (usuario != null) {
						session = ((HttpServletRequest) request).getSession();
						Collection<Operacao> operacoes = obterOperacoes(session, recurso);
						if (operacoes != null && !operacoes.isEmpty()) {
							request.setAttribute("listaFavoritos", usuario.getFavoritos());

							Collection<Menu> modulos = (Collection<Menu>) session.getAttribute("modulosMenu");

							Long[] idPapelUsuario = new Long[usuario.getPapeis().size()];
							int posicao = 0;
							for (Papel currentPapel : usuario.getPapeis()) {
								idPapelUsuario[posicao] = currentPapel.getChavePrimaria();
								posicao++;
							}

							for (Menu currentMenu : modulos) {
								Util.removeMenuSemPermissaoSistema(currentMenu, idPapelUsuario);
							}

							Util.atualizaListaMenu(modulos, usuario.getFavoritos());
							for (Operacao oper : operacoes) {
								try {
									permissao = fachada.buscarPermissaoUsuario(usuario, oper);
									// consulta
									if (permissao != null) {
										autorizado = true;
										operacao = oper; // NOSONAR {Validar performance e retirar static}
										p = permissao; // NOSONAR {Validar performance e retirar static}

										break;
									}
								} catch (GGASException e) {
									LOG.error(e.getStackTrace(), e);
									autorizado = false;
								}
							}
							if (fachada.usuarioPossuiPapelAdmin(usuario)) {
								autorizado = true;

							}
						} else {
							request.setAttribute("listaFavoritos", usuario.getFavoritos());
							operacoes = fachada.buscarOperacaoPorRecurso(recurso);
							if(!operacoes.isEmpty()){
								operacao = operacoes.iterator().next();
							}

							((HttpServletResponse) response).sendRedirect(
									((HttpServletRequest) request).getContextPath() + HOME_URL + "?acessoNegado=true");

							recursoCadastrado = Boolean.FALSE;
							autorizado = Boolean.FALSE;
						}
					} else {
						autorizado = false;
					}
				} else {
					autorizado = true;

					if (!StringUtils.isEmpty(recurso)) {
						Collection<Operacao> operacoes = fachada.buscarOperacaoPorRecurso(recurso);
						// consulta
						if (operacoes.isEmpty()) {
							LOG.error(recurso + " não possui operações cadastradas. ");
						} else {
							operacao = operacoes.iterator().next();
							modulo = operacao.getModulo();
						}
					}
				}
			} catch (GGASException e) {
				LOG.error(e.getStackTrace(), e);
				autorizado = false;
			}

			if(recursoCadastrado) {
				if (autorizado) {
					request.setAttribute(ATRIBUTO_MODULO, modulo);
					request.setAttribute(ATRIBUTO_OPERACAO, operacao);
					request.setAttribute(ATRIBUTO_PERMISSAO, permissao);
					request.setAttribute("idUsuario", usuario);
				} else {
					request.setAttribute(ATRIBUTO_MENSAGEM_TELA, MENSAGEM_ACESSO_NEGADO);
					forward(request, response, FORWARD_INDEX);
					LOG.info("Acesso negado IP, URL: [" + request.getRemoteAddr() + "] ["
							+ ((HttpServletRequest) request).getRequestURL() + "]");
				}
			}

			request.setAttribute("isProduction", IS_AMBIENTE_PRODUCAO);
		}

		if (autorizado) {
			filterChain.doFilter(request, response);
		}
	}

	/**
	 * Verificar ur ls.
	 * 
	 * @param recurso the recurso
	 * @return true, if successful
	 */
	private boolean verificarURLs(String recurso) {

		return !(recurso.contains("service/ws") || recurso.contains("api/v1") || recurso.contains(".css") || recurso.contains(".js")
				|| recurso.contains(".pdf") || recurso.contains(".htm") || recurso.contains(".jsp")
				|| recurso.contains(".dwr") || verificarURLsImagem(recurso));
	}

	/**
	 * Verificar ur ls imagem.
	 * 
	 * @param recurso the recurso
	 * @return true, if successful
	 */
	private boolean verificarURLsImagem(String recurso) {
		return recurso.contains(".jpg") || recurso.contains(".png") || recurso.contains(".gif")
				|| recurso.contains(".ico");
	}

	/**
	 * Verificar recursos liberados.
	 * 
	 * @param recurso the recurso
	 * @return true, if successful
	 */
	private boolean verificarRecursosLiberados(String recurso) {

		for (String url : recursosLiberados) {
			if (recurso.contains(url)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Obter operacoes.
	 * 
	 * @param session the session
	 * @param recurso the recurso
	 * @return the collection
	 */
	private Collection<Operacao> obterOperacoes(HttpSession session, String recurso) {

		String url = recurso.replace("?", "@").replace("/", "@").split("@")[0];
		Map<String, Collection<Operacao>> mapaOperacoes = (Map<String, Collection<Operacao>>) session
				.getAttribute("mapaOperacoes");
		if (mapaOperacoes != null) {
			if (mapaOperacoes.containsKey(url)) {
				return mapaOperacoes.get(url);
			} else {
				return mapaOperacoes.get(recurso);
			}
		}
		return null;
	}

	/**
	 * Gets the recurso.
	 * 
	 * @param request the request
	 * @return the recurso
	 */
	private String getRecurso(ServletRequest request) {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String uri = httpServletRequest.getRequestURI();
		String param = httpServletRequest.getQueryString();

		if (param != null && !recursosLiberados.contains(uri.substring(6, uri.length()))) {
			String[] controle = param.split("&");
			if (controle.length != 1 && StringUtils.isNotBlank(param)) {
				uri = uri + "?" + param;
			}
		}

		String contexto = httpServletRequest.getContextPath();
		String recurso = uri.replaceAll(contexto, "");

		String recursoAjustado = recurso.substring(1);
		String[] array = recursoAjustado.split(";");

		return array[0];
	}

	/**
	 * Gets the operacao.
	 * 
	 * @return the Operacao
	 */
	public static Operacao getOperacao() {

		return operacao;
	}

	/**
	 * Gets the usuario.
	 * 
	 * @param request the request
	 * @return the usuario
	 */
	private Usuario getUsuario(ServletRequest request) {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		return (Usuario) httpServletRequest.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
	}

	/**
	 * Forward.
	 * 
	 * @param request  the request
	 * @param response the response
	 * @param url      the url
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	private void forward(ServletRequest request, ServletResponse response, String url)
			throws IOException, ServletException {

		RequestDispatcher requestDispatcher = ((HttpServletRequest) request).getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}

	/**
	 * Gets the permissao.
	 * 
	 * @return the Permissao
	 */
	public static Permissao getPermissao() {
		return p;
	}

}
