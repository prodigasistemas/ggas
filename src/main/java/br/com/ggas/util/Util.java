/*
 * Copyright (C) <2011> GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.util;

import java.awt.image.BufferedImage;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.MaskFormatter;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibm.icu.impl.Grego;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.cadastro.dadocensitario.impl.SetorCensitarioImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.geografico.impl.MicrorregiaoImpl;
import br.com.ggas.cadastro.geografico.impl.MunicipioImpl;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.cadastro.imovel.impl.AreaConstruidaFaixaImpl;
import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.cadastro.imovel.impl.PadraoConstrucaoImpl;
import br.com.ggas.cadastro.imovel.impl.PavimentoCalcadaImpl;
import br.com.ggas.cadastro.imovel.impl.PavimentoRuaImpl;
import br.com.ggas.cadastro.imovel.impl.PerfilImovelImpl;
import br.com.ggas.cadastro.imovel.impl.SituacaoImovelImpl;
import br.com.ggas.cadastro.imovel.impl.TipoBotijaoImpl;
import br.com.ggas.cadastro.imovel.impl.TipoRelacionamentoClienteImovelImpl;
import br.com.ggas.cadastro.imovel.impl.TipoSegmentoImpl;
import br.com.ggas.cadastro.localidade.impl.AreaTipoImpl;
import br.com.ggas.cadastro.localidade.impl.PerfilQuadraImpl;
import br.com.ggas.cadastro.localidade.impl.UnidadeNegocioImpl;
import br.com.ggas.cadastro.localidade.impl.ZeisImpl;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.cadastro.operacional.impl.RedeDiametroImpl;
import br.com.ggas.cadastro.operacional.impl.TroncoImpl;
import br.com.ggas.cadastro.operacional.impl.ZonaBloqueioImpl;
import br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.tributo.impl.TributoAliquotaImpl;
import br.com.ggas.faturamento.tributo.impl.TributoImpl;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.impl.FeriadoImpl;
import br.com.ggas.geral.impl.EntidadeClasseImpl;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.medicao.medidor.impl.MarcaMedidorImpl;
import br.com.ggas.medicao.medidor.impl.ModeloMedidorImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.PeriodicidadeImpl;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * The Class Util.
 */
public final class Util {

	/** Constante para representar o valor "0" em string */
	private static final String ZERO = "0";

	/** The Constant ANO_MES_VALOR_STRING_MES_INDICE_INICIAL. */
	private static final int ANO_MES_VALOR_STRING_MES_INDICE_INICIAL = 4;

	/** The Constant ANO_MES_VALOR_STRING_ANO_INDICE_INICIAL. */
	private static final int ANO_MES_VALOR_STRING_ANO_INDICE_INICIAL = 0;

	/** The Constant CICLO_ROTULO. */
	private static final String CICLO_ROTULO = "Ciclo";

	/** The Constant SEPARADOR_ROTULO_VALOR. */
	private static final String SEPARADOR_ROTULO_VALOR = ": ";

	/** The Constant SEPARADOR_PALAVRA. */
	private static final String SEPARADOR_PALAVRA = " ";

	/** The Constant ANO_MES_REFERENCIA_ROTULO. */
	private static final String ANO_MES_REFERENCIA_ROTULO = "Ano/Mês Referencia";

	/** The Constant SEPARADOR_ANO_MES. */
	private static final String SEPARADOR_ANO_MES = "/";

	/** The Constant PONTO_CHAR. */
	private static final char PONTO_CHAR = '.';

	/** The Constant SUBLINHADO_CHAR. */
	private static final char SUBLINHADO_CHAR = '_';

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Util.class);

	/** The Constant factory. */
	private static final ValidatorFactory FACTORY = Validation.buildDefaultValidatorFactory();

	/** The Constant REFLECTION_CLASS. */
	public static final String REFLECTION_CLASS = "Impl";

	/** The Constant mensagens. */
	public static final ResourceBundle MENSAGENS = ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS);

	private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

	/** A constante INSCRICAO_ESTADUAL_BA_SEGUNDO_DIGITO */
	private static final Character[] INSCRICAO_ESTADUAL_BA_SEGUNDO_DIGITO = new Character[] { '6', '7', '9' };

	private static final int LIMITE_HORA = 23;

	private static final int LIMITE_MIN_SEG = 59;
	private static final int NUMERO_CEM = 100;

	private static final int NUMERO_PARA_ADICIONAR_ZERO = 2;

	private static final int VALIDADOR_QUATORZE_CNPJ = 14;
	private static final int VALIDADOR_ONZE_CNPJ = 11;
	private static final int VALIDADOR_QUATRO_CNPJ = 4;
	private static final int VALIDADOR_48_CNPJ = 48;
	private static final int VALIDADOR_SEIS_CNPJ = 6;
	private static final int VALIDADOR_DOZE_CNPJ = 12;
	private static final int VALIDADOR_NOVE_CNPJ = 9;
	private static final int VALIDADOR_OITO_CNPJ = 8;
	private static final int VALIDADOR_DEZ_CNPJ = 10;
	private static final int VALIDADOR_CINCO_CNPJ = 5;
	private static final int VALIDADOR_SETE_CNPJ = 7;

	private static final int CONVERTER_BYTE = 1024;

	private static final int BASE_QUATRO = 4;
	private static final int VALIDADOR_ONZE_CPF = 11;
	private static final int VALIDADOR_DOIS_CPF = 2;

	private static final int VALIDADOR_DOIS_INSCRICAO_ESTADUAL_SE = 2;
	private static final int VALIDADOR_NOVE_INSCRICAO_ESTADUAL_SE = 9;
	private static final int VALIDADOR_DEZ_INSCRICAO_ESTADUAL_SE = 10;
	private static final int VALIDADOR_ONZE_INSCRICAO_ESTADUAL_SE = 11;
	private static final int VALIDADOR_OITO_INSCRICAO_ESTADUAL_SE = 8;

	private static final int VALIDADOR_DOIS_INSCRICAO_ESTADUAL_AL = 2;
	private static final int VALIDADOR_NOVE_INSCRICAO_ESTADUAL_AL = 9;
	private static final int VALIDADOR_DEZ_INSCRICAO_ESTADUAL_AL = 10;
	private static final int VALIDADOR_ONZE_INSCRICAO_ESTADUAL_AL = 11;
	private static final int VALIDADOR_OITO_INSCRICAO_ESTADUAL_AL = 8;

	private static final int VALIDADOR_DOIS_INSCRICAO_ESTADUAL_PR = 2;
	private static final int VALIDADOR_DEZ_INSCRICAO_ESTADUAL_PR = 10;
	private static final int VALIDADOR_ONZE_INSCRICAO_ESTADUAL_PR = 11;
	private static final int VALIDADOR_OITO_INSCRICAO_ESTADUAL_PR = 8;

	private static final int INDICE_QUATRO = 4;
	private static final int INDICE_SEIS = 6;
	private static final int INDICE_DOIS = 2;
	private static final int INDICE_TRES = 3;
	private static final int INDICE_SETE = 7;

	private static final int AUXILIAR_DOIS = 2;
	private static final int AUXILIAR_CINCO = 5;
	private static final int AUXILIAR_SEIS = 6;
	private static final int AUXILIAR_OITO = 8;
	private static final int AUXILIAR_NOVE = 9;
	private static final int AUXILIAR_DEZ = 10;
	private static final int AUXILIAR_DOZE = 12;
	private static final int AUXILIAR_CEM = 100;
	private static final int AUXILIAR_CINQUENTA = 50;
	private static final int BASE_DEZ = 10;

	private static final int PRIMEIRO_CICLO = 1;
	private static final String CARACTER_A_ESQUERDA_PARA_MES_DE_UM_DIGITO = "0";
	private static final int QUANTIDADE_DE_DIGITOS_PARA_MES = 2;

	public static final int ESPACO_EM_BRANCO = 32;
	public static final int APOSTOFRE = 39;
	
	public static final int DUAS_CASAS_DECIMAIS = 2;
	public static final int TRES_CASAS_DECIMAIS = 3;
	
	private Util() {
	}

	/**
	 * Método responsável por converter um string para uma data.
	 *
	 * @param rotulo  O rorulo do campo
	 * @param strData O valor
	 * @param formato O formato da data
	 * @return Uma data
	 * @throws GGASException Caso ocorra algum erro de convsersão ou formato
	 */
	public static Date converterCampoStringParaData(String rotulo, String strData, String formato)
			throws GGASException {

		Date data = null;
		SimpleDateFormat formatador = null;

		try {
			formatador = new SimpleDateFormat(formato);
			data = formatador.parse(strData);
			if (!formatador.format(data).equals(strData)) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
			}
		} catch (IllegalArgumentException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO, formato);
		} catch (ParseException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return data;
	}

	/**
	 * Método responsável por validar a hora informada.
	 *
	 * @param hora   A hora
	 * @param rotulo o rotulo do campo.
	 * @throws GGASException Caso ocorra algum erro de convsersão ou formato
	 */
	public static void validarHora(String hora, String rotulo) throws GGASException {

		SimpleDateFormat formato;
		try {
			formato = new SimpleDateFormat(Constantes.FORMATO_HORA);
			formato.setLenient(false);
			formato.parse(hora);
		} catch (ParseException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}
	}

	/**
	 * Converte a data passada por parametro para uma string sem caracteres
	 * especiais. Usada normalmente em nome de arquivos (não permite caracteres
	 * especiais).
	 *
	 * @param data Data a ser convertida.
	 * @return String com a data.
	 */
	public static String converterDataParaStringSemCaracteresEspeciais(Date data) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));
		String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(calendar.get(Calendar.MINUTE));
		String segundo = String.valueOf(calendar.get(Calendar.SECOND));

		retorno.append(adicionarZeros(dia));
		retorno.append(adicionarZeros(mes));
		retorno.append(adicionarZeros(ano));
		retorno.append(adicionarZeros(hora));
		retorno.append(adicionarZeros(minuto));
		retorno.append(adicionarZeros(segundo));

		return retorno.toString();

	}

	/**
	 * Converte a data passada por parametro para uma string no formato AnoMesDia
	 * sem caracteres especiais. Usada normalmente em nome de arquivos (não permite
	 * caracteres especiais).
	 *
	 * @param data Data a ser convertida.
	 * @return String com a data.
	 * @deprecated utilize o metodo Util.montarRelatorioContentDisposition para
	 *             montar o content-description do arquivo a ser respondido ao
	 *             browser
	 * @deprecated
	 */
	@Deprecated
	public static String converterDataParaStringAnoMesDiaSemCaracteresEspeciais(Date data) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));

		retorno.append(adicionarZeros(ano));
		retorno.append(adicionarZeros(mes));
		retorno.append(adicionarZeros(dia));

		return retorno.toString();

	}

	/**
	 * Monta o atributo content-disposition do header do html response para retornar
	 * para o browser um arquivo com o nome no formato
	 * [tituloArquivo]_[ano][mes][dia]_[hora][minuto][segundo].[formato] sendo
	 * tituloArquivo e formato passados como entrada; e ano, mes, dia, hora, minuto
	 * e segundo do momento da resposta do servidor.
	 *
	 * @param tituloArquivo    o titulo do arquivo
	 * @param formatoImpressao o objeto formato impressao do arquivo
	 * @return o atributo content-disposition do header do html response
	 */
	public static String montarRelatorioContentDisposition(String tituloArquivo, FormatoImpressao formatoImpressao) {

		return montarRelatorioContentDisposition(tituloArquivo, formatoImpressao.name());
	}

	/**
	 * Monta o atributo content-disposition do header do html response para retornar
	 * para o browser um arquivo com o nome no formato
	 * [tituloArquivo]_[ano][mes][dia]_[hora][minuto][segundo].[formato] sendo
	 * tituloArquivo e formato passados como entrada; e ano, mes, dia, hora, minuto
	 * e segundo do momento da resposta do servidor.
	 *
	 * @param tituloArquivo o titulo do arquivo
	 * @param formato       o formato do arquivo
	 * @return o atributo content-disposition do header do html response
	 */
	public static String montarRelatorioContentDisposition(String tituloArquivo, String formato) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));
		int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		int minutes = Calendar.getInstance().get(Calendar.MINUTE);
		int seconds = Calendar.getInstance().get(Calendar.SECOND);

		retorno.append("attachment; filename=");
		retorno.append(tituloArquivo);
		retorno.append(SUBLINHADO_CHAR);
		retorno.append(adicionarZeros(ano));
		retorno.append(adicionarZeros(mes));
		retorno.append(adicionarZeros(dia));
		retorno.append(SUBLINHADO_CHAR);
		retorno.append(hours);
		retorno.append(minutes);
		retorno.append(seconds);
		retorno.append(PONTO_CHAR);
		retorno.append(formato.toLowerCase());

		return retorno.toString();

	}

	/**
	 * Converter data para string dia mes ano sem caracteres especiais.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String converterDataParaStringDiaMesAnoSemCaracteresEspeciais(Date data) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));

		retorno.append(adicionarZeros(dia));
		retorno.append(adicionarZeros(mes));
		retorno.append(adicionarZeros(ano));

		return retorno.toString();

	}

	/**
	 * Converte a data passada por parametro para uma string no formato
	 * HoraMinutoSegundo sem caracteres especiais. Usada normalmente em nome de
	 * arquivos (não permite caracteres especiais).
	 *
	 * @param data Data a ser convertida.
	 * @return String com a data.
	 */
	public static String converterDataParaStringHoraMinutoSegundoSemCaracteresEspeciais(Date data) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(calendar.get(Calendar.MINUTE));
		String segundo = String.valueOf(calendar.get(Calendar.SECOND));

		retorno.append(adicionarZeros(hora));
		retorno.append(adicionarZeros(minuto));
		retorno.append(adicionarZeros(segundo));

		return retorno.toString();

	}

	/**
	 * Converte a data passada por parâmetro para uma data sem hora no formato
	 * String.
	 *
	 * @param data    the data
	 * @param mascara the mascara
	 * @return data sem hora no formato de String
	 */
	public static String converterDataParaStringSemHora(Date data, String mascara) {
		StringBuilder retorno = new StringBuilder();
		if (data != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(data);

			String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			String ano = String.valueOf(calendar.get(Calendar.YEAR));

			if (Constantes.FORMATO_DATA_HORA_BR.equals(mascara) || Constantes.FORMATO_DATA_BR.equals(mascara)) {
				retorno.append(adicionarZeros(dia));
				retorno.append("/");
				retorno.append(adicionarZeros(mes));
				retorno.append("/");
				retorno.append(adicionarZeros(ano));
			} else {
				retorno.append(adicionarZeros(ano));
				retorno.append("/");
				retorno.append(adicionarZeros(mes));
				retorno.append("/");
				retorno.append(adicionarZeros(dia));
			}
		}

		return retorno.toString();
	}

	/**
	 * Converte a data passada por parâmetro para uma data sem hora no formato
	 * String sem as barras de separação.
	 *
	 * @param data    the data
	 * @param mascara the mascara
	 * @return data sem hora no formato de String
	 */
	public static String converterDataParaStringSemHoraSemBarra(Date data, String mascara) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));

		StringBuilder retorno = new StringBuilder();
		if (Constantes.FORMATO_DATA_HORA_BR.equals(mascara) || Constantes.FORMATO_DATA_BR.equals(mascara)) {
			retorno.append(adicionarZeros(dia));
			retorno.append(adicionarZeros(mes));
			retorno.append(adicionarZeros(ano));
		} else {
			retorno.append(adicionarZeros(ano));
			retorno.append(adicionarZeros(mes));
			retorno.append(adicionarZeros(dia));
		}

		return retorno.toString();
	}

	/**
	 * Converte uma data para string. Caso data nula, é retorna uma string nula
	 * 
	 * @param data do java
	 * @return formatada no modelo brasileiro DIA/MÊS/ANO
	 */
	public static String converterDataParaString(Date data) {
		return Optional.ofNullable(data).map(d -> new SimpleDateFormat(Constantes.FORMATO_DATA_BR).format(data))
				.orElse(null);
	}

	/**
	 *
	 * @param data do java
	 * @return formatada no modelo brasileiro DIA/MÊS/ANO HORA:MINUTOR
	 */
	public static String converterDataHoraParaString(Date data) {
		return new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_BR).format(data);
	}

	/**
	 * Obter o último dia do Mês.
	 *
	 * @param data    the data
	 * @param mascara the mascara
	 * @return the string
	 */
	public static String obterUltimoDiaMes(Date data, String mascara) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));

		StringBuilder retorno = new StringBuilder();
		if (Constantes.FORMATO_DATA_HORA_BR.equals(mascara) || Constantes.FORMATO_DATA_BR.equals(mascara)) {
			retorno.append(adicionarZeros(dia));
			retorno.append("/");
			retorno.append(adicionarZeros(mes));
			retorno.append("/");
			retorno.append(adicionarZeros(ano));
		} else {
			retorno.append(adicionarZeros(ano));
			retorno.append(adicionarZeros(mes));
			retorno.append(adicionarZeros(dia));
		}

		return retorno.toString();
		
	}

	/**
	 * http://localhost:99/ggas_mantenedor/.
	 *
	 * @param ano the ano
	 * @param mes the mes
	 * @return the big decimal
	 */
	public static BigDecimal obterUltimoDiaMes(String ano, String mes) {

		Calendar calendario = Calendar.getInstance();

		calendario.set(Calendar.YEAR, Integer.parseInt(ano));
		calendario.set(Calendar.MONTH, Integer.parseInt(mes) - 1);
		int ultimoDiaMes = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);

		return new BigDecimal(ultimoDiaMes);
	}

	/**
	 * Obter primeiro dia mes.
	 *
	 * @param data    the data
	 * @param mascara the mascara
	 * @return the string
	 */
	public static String obterPrimeiroDiaMes(Date data, String mascara) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String dia = String.valueOf(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));

		StringBuilder retorno = new StringBuilder();
		if (Constantes.FORMATO_DATA_HORA_BR.equals(mascara) || Constantes.FORMATO_DATA_BR.equals(mascara)) {
			retorno.append(adicionarZeros(dia));
			retorno.append("/");
			retorno.append(adicionarZeros(mes));
			retorno.append("/");
			retorno.append(adicionarZeros(ano));
		} else {
			retorno.append(adicionarZeros(ano));
			retorno.append(adicionarZeros(mes));
			retorno.append(adicionarZeros(dia));
		}

		return retorno.toString();
	}

	/**
	 * Método responsável por retornar a data passada com a maior hora do dia.
	 *
	 * @param data Data
	 * @return data com hora, minuto e segundo alterados.
	 */
	public static Date converterDataParaDataMaiorHoraDoDia(Date data) {

		Date retorno = null;

		if (data != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(data);
			calendar.set(Calendar.HOUR_OF_DAY, LIMITE_HORA);
			calendar.set(Calendar.MINUTE, LIMITE_MIN_SEG);
			calendar.set(Calendar.SECOND, LIMITE_MIN_SEG);
			retorno = calendar.getTime();
		}

		return retorno;
	}

	/**
	 * Método que adiciona zeros a uma data ou a uma hora que ele fique com dois
	 * caracteres.
	 *
	 * @param param the param
	 * @return numero com dois caracteres
	 */
	public static String adicionarZeros(String param) {

		if (param.length() < NUMERO_PARA_ADICIONAR_ZERO) {
			return ZERO + param;
		} else {
			return param;
		}
	}

	/**
	 * Método responsável por converter um string para um valor.
	 *
	 * @param rotulo   O rotulo do campo
	 * @param strValor O valor
	 * @param formato  O formato
	 * @param locale   O Locale da aplicação
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(String rotulo, String strValor, String formato,
			Locale locale) throws FormatoInvalidoException {

		BigDecimal valor = null;
		DecimalFormat decimalFormat = new DecimalFormat(formato, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);

		try {
			valor = (BigDecimal) decimalFormat.parse(strValor);
		} catch (ParseException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Método responsável por converter um string para um valor.
	 *
	 * @param rotulo  O rorulo do campo
	 * @param valor   O valor
	 * @param formato O formato
	 * @param locale  O Locale da aplicação
	 * @return Uma valor
	 */
	public static String converterCampoValorParaString(BigDecimal valor, String formato, Locale locale) {

		DecimalFormat decimalFormat = new DecimalFormat(formato, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);
		return decimalFormat.format(valor);
	}

	/**
	 * Método responsável por criptgrafar uma senha.
	 *
	 * @param senha A senha original
	 * @param chave A senha criptografada
	 * @param hash  O hash da criptografada
	 * @return A senha criptografada
	 */
	public static String criptografarSenha(String senha, String chave, String hash) {

		String senhaCriptografada = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(hash);
			messageDigest.update(senha.getBytes());
			senhaCriptografada = stringHexa(messageDigest.digest(chave.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			LOG.error(e);
		}
		return senhaCriptografada;
	}

	/**
	 * Gera uma string dos array de bytes.
	 *
	 * @param bytes Um array de byes
	 * @return Uma string
	 */
	private static String stringHexa(byte[] bytes) {

		StringBuilder retorno = new StringBuilder();

		for (int i = 0; i < bytes.length; i++) {
			retorno.append(Integer.toHexString((((bytes[i] >> BASE_QUATRO) & 0xf) << BASE_QUATRO) | (bytes[i] & 0xf)));
		}

		return retorno.toString();
	}

	/**
	 * Método responsável por adicionar zeros a esquerda de um número.
	 *
	 * @param numero   O número
	 * @param tamanhoTotal tamanho total
	 * @return Um número com zero a esquerda
	 */
	public static String adicionarZerosEsquerdaNumero(String numero, int tamanhoTotal) {

		return StringUtils.leftPad(numero, tamanhoTotal, ZERO);
	}

	/**
	 * Método responsável por verificar se o email é válido ou não.
	 *
	 * @param valor   O valor a ser validado
	 * @param dominio O dominio do campo.
	 * @return <true> se for válido, <false> caso contrário.
	 */
	public static boolean validarDominio(String valor, String dominio) {

		Pattern pattern = Pattern.compile(dominio);
		return pattern.matcher(valor).matches();
	}

	/**
	 * Método responsável por verificar se o valor é numérico.
	 *
	 * @param valor O valor a ser verificado.
	 * @return <true> se for verdade,<false> caso contrário.
	 */
	public static boolean verificarDominioNumerico(String valor) {

		return (valor != null) && (!Util.validarDominio(valor, Constantes.EXPRESSAO_REGULAR_NUMEROS));
	}

	/**
	 * Método responsável por validar o conteúdo informado.
	 *
	 * @param conteudo O conteúdo informado
	 * @param padrao   O padrão
	 * @return boolean
	 */
	public static boolean validarPadraoDoConteudo(String conteudo, String padrao) {

		Pattern pattern = Pattern.compile(padrao);
		return pattern.matcher(conteudo).matches();
	}

	/**
	 * Método responsável por converter um string para um valor Integer.
	 *
	 * @param rotulo   O rotulo do campo
	 * @param strValor O valor
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static Integer converterCampoStringParaValorInteger(String rotulo, String strValor)
			throws FormatoInvalidoException {

		Integer valor = null;

		try {
			valor = Integer.valueOf(strValor);
		} catch (NumberFormatException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Método responsável por converter um string para um valor Integer.
	 *
	 * @param strValor O valor
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static Integer converterCampoStringParaValorInteger(String strValor) throws FormatoInvalidoException {

		Integer valor = null;
		try {
			valor = Integer.valueOf(strValor);
		} catch (NumberFormatException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO);
		}
		return valor;
	}

	/**
	 * Método responsável por converter um string para um valor Long.
	 *
	 * @param rotulo   O rotulo do campo
	 * @param strValor O valor
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static Long converterCampoStringParaValorLong(String rotulo, String strValor)
			throws FormatoInvalidoException {

		Long valor = null;

		try {
			valor = Long.valueOf(strValor);
		} catch (NumberFormatException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Método responsável por validar um String com relação ao domínio de caracteres
	 * para nomes.
	 *
	 * @param rotulo   Rótulo do campo a ser validado.
	 * @param conteudo String a ser validada.
	 * @param padrao   Padrão de caracteres válido.
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarDominioCaracteres(String rotulo, String conteudo, String padrao)
			throws FormatoInvalidoException {

		Pattern pattern = Pattern.compile(padrao);
		if (!pattern.matcher(conteudo).matches()) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}
	}

	/**
	 * Método responsável por formatar um String para consulta.
	 *
	 * @param texto String a ser formatado.
	 * @return String formatado.
	 */
	public static String formatarTextoConsulta(String texto) {

		return "%" + texto.replace("'", "''").replace(" ", "%") + "%";
	}

	/**
	 * Método responsável por formatar um valor utilizando uma mascara.
	 *
	 * @param valor   O valor que será formatado
	 * @param mascara A mascara
	 * @return O valor formatado
	 */
	public static String formatarValor(Object valor, String mascara) {

		String retorno = null;
		MaskFormatter maskFormatter;
		try {
			maskFormatter = new MaskFormatter(mascara);
			maskFormatter.setValueContainsLiteralCharacters(false);
			retorno = maskFormatter.valueToString(String.valueOf(valor));
		} catch (ParseException e) {
			LOG.error(e);
		}

		return retorno;
	}

	/**
	 * Método responsável por formatar um valor string.
	 *
	 * @param valor            O valor sem formatação
	 * @param tamanho          O tamanho da string
	 * @param qtdCasasDecimais A quantidade de casas decimais
	 * @return o valor formatado
	 */
	public static String formatarValorMonetarioString(String valor, int tamanho, int qtdCasasDecimais) {

		String numeroFormatado = null;
		String decimal = null;
		String numeroSemDecimal = null;

		if (valor != null && valor.length() == tamanho && qtdCasasDecimais > 0) {
			decimal = valor.substring(tamanho - qtdCasasDecimais);
			numeroSemDecimal = valor.substring(0, tamanho - qtdCasasDecimais);
			numeroFormatado = numeroSemDecimal + "." + decimal;
			numeroFormatado = numeroFormatado.replaceAll("^0*", "");
		}

		return numeroFormatado;
	}

	/**
	 * Método responsável por remover alguns caracteres especiais como . - /
	 *
	 * @param texto O texto que será formatado
	 * @return O texto sem os caracteres especiais
	 */
	public static String removerCaracteresEspeciais(String texto) {

		String retorno = texto;
		retorno = retorno.replace(".", "");
		retorno = retorno.replace("/", "");
		retorno = retorno.replace("-", "");
		return retorno;
	}

	/**
	 * Métoto responsável por remover todos os caracteres especiais de um texto
	 * deixando apenas números, letras ou espaços em branco.
	 * 
	 * @param texto O texto que será formatado
	 * @return O texto apenas com letras, números ou espaços
	 */
	public static String removerTodosCaracteresEspeciais(String texto) {
		return texto.replaceAll("[^A-Za-z0-9\\s]", "").trim();
	}

	/**
	 * Método responsável por remover vírgulas e pontos de um campo String.
	 *
	 * @param texto O texto que será formatado
	 * @return O texto sem os caracteres especiais
	 */
	public static String removerVirgulasPontos(String texto) {

		String retorno = texto;
		retorno = retorno.replace(".", "");
		retorno = retorno.replace(",", "");
		return retorno;
	}

	/**
	 * Método responsável por validar um cnpj.
	 *
	 * @param cnpj O CNPJ
	 * @return True se tiver correto
	 * @throws FormatoInvalidoException Caso ocorra algum erro de validação
	 */
	public static void validarCNPJ(String cnpj) throws FormatoInvalidoException {

		if (cnpj == null || cnpj.length() != VALIDADOR_QUATORZE_CNPJ) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CNPJ);
		}
		int soma = 0;
		for (int j = 0; j < VALIDADOR_ONZE_CNPJ; j++) {
			soma = soma + Integer.parseInt(String.valueOf(cnpj.charAt(j)));
		}
		if (soma == 0) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CNPJ);
		}

		int dig;
		soma = 0;
		String cnpjCalc = cnpj.substring(0, VALIDADOR_DOZE_CNPJ);

		char[] chrCnpj = cnpj.toCharArray();

		/* Primeira parte */
		for (int i = 0; i < VALIDADOR_QUATRO_CNPJ; i++) {
			if (chrCnpj[i] - VALIDADOR_48_CNPJ >= 0 && chrCnpj[i] - VALIDADOR_48_CNPJ <= VALIDADOR_NOVE_CNPJ) {
				soma += (chrCnpj[i] - VALIDADOR_48_CNPJ) * (VALIDADOR_SEIS_CNPJ - (i + 1));
			}
		}
		for (int i = 0; i < VALIDADOR_OITO_CNPJ; i++) {
			if (chrCnpj[i + VALIDADOR_QUATRO_CNPJ] - VALIDADOR_48_CNPJ >= 0
					&& chrCnpj[i + VALIDADOR_QUATRO_CNPJ] - VALIDADOR_48_CNPJ <= VALIDADOR_NOVE_CNPJ) {
				soma += (chrCnpj[i + VALIDADOR_QUATRO_CNPJ] - VALIDADOR_48_CNPJ) * (VALIDADOR_DEZ_CNPJ - (i + 1));
			}
		}
		dig = VALIDADOR_ONZE_CNPJ - (soma % VALIDADOR_ONZE_CNPJ);

		if (dig == VALIDADOR_DEZ_CNPJ || dig == VALIDADOR_ONZE_CNPJ) {
			cnpjCalc += ZERO;
		} else {
			cnpjCalc += Integer.toString(dig);
		}

		/* Segunda parte */
		soma = 0;
		for (int i = 0; i < VALIDADOR_CINCO_CNPJ; i++) {
			if (chrCnpj[i] - VALIDADOR_48_CNPJ >= 0 && chrCnpj[i] - VALIDADOR_48_CNPJ <= VALIDADOR_NOVE_CNPJ) {
				soma += (chrCnpj[i] - VALIDADOR_48_CNPJ) * (VALIDADOR_SETE_CNPJ - (i + 1));
			}
		}
		for (int i = 0; i < VALIDADOR_OITO_CNPJ; i++) {
			if (chrCnpj[i + VALIDADOR_CINCO_CNPJ] - VALIDADOR_48_CNPJ >= 0
					&& chrCnpj[i + VALIDADOR_CINCO_CNPJ] - VALIDADOR_48_CNPJ <= VALIDADOR_NOVE_CNPJ) {
				soma += (chrCnpj[i + VALIDADOR_CINCO_CNPJ] - VALIDADOR_48_CNPJ) * (VALIDADOR_DEZ_CNPJ - (i + 1));
			}
		}
		dig = VALIDADOR_ONZE_CNPJ - (soma % VALIDADOR_ONZE_CNPJ);

		if (dig == VALIDADOR_DEZ_CNPJ || dig == VALIDADOR_ONZE_CNPJ) {
			cnpjCalc += ZERO;
		} else {
			cnpjCalc += Integer.toString(dig);
		}

		if (!cnpj.equals(cnpjCalc)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CNPJ);
		}
	}

	/**
	 * Método responsável por validar um cpf.
	 *
	 * @param cpf O CPF
	 * @return True se tiver correto
	 * @throws FormatoInvalidoException Caso ocorra algum erro de validação
	 */
	public static void validarCPF(String cpf) throws FormatoInvalidoException {

		try {

			int dNumero1;
			int dNumero2;
			int digito1;
			int digito2;
			int resto;
			int digitoCPF;
			String nDigResult;

			if (cpf == null || cpf.length() != VALIDADOR_ONZE_CPF) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
			}

			java.util.regex.Pattern p = java.util.regex.Pattern.compile(cpf.charAt(0) + "{" + cpf.length() + "}");
			java.util.regex.Matcher m = p.matcher(cpf);
			if (m.find()) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
			}

			dNumero1 = dNumero2 = 0;
			digito1 = digito2 = resto = 0;

			int soma = 0;
			for (int j = 0; j < VALIDADOR_ONZE_CPF; j++) {
				soma = soma + Integer.parseInt(String.valueOf(cpf.charAt(j)));
			}
			if (soma == 0) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
			}

			for (int nCount = 1; nCount < cpf.length() - 1; nCount++) {
				digitoCPF = Integer.parseInt(cpf.substring(nCount - 1, nCount));

				// multiplique a ultima casa por 2 a
				// seguinte por 3 a seguinte por 4
				// e assim por diante.
				dNumero1 = dNumero1 + (VALIDADOR_ONZE_CPF - nCount) * digitoCPF;

				// para o segundo digito repita o
				// procedimento incluindo o primeiro
				// digito calculado no passo anterior.
				dNumero2 = dNumero2 + (12 - nCount) * digitoCPF;
			}

			// Primeiro resto da divisão por 11.
			resto = (dNumero1 % VALIDADOR_ONZE_CPF);

			// Se o resultado for 0 ou 1 o digito é 0
			// caso contrário o digito é 11
			// menos o resultado anterior.
			if (resto < VALIDADOR_DOIS_CPF) {
				digito1 = 0;
			} else {
				digito1 = VALIDADOR_ONZE_CPF - resto;
			}

			dNumero2 += VALIDADOR_DOIS_CPF * digito1;

			// Segundo resto da divisão por 11.
			resto = (dNumero2 % VALIDADOR_ONZE_CPF);

			// Se o resultado for 0 ou 1 o digito é 0
			// caso contrário o digito é 11
			// menos o resultado anterior.
			if (resto < VALIDADOR_DOIS_CPF) {
				digito2 = 0;
			} else {
				digito2 = VALIDADOR_ONZE_CPF - resto;
			}

			// Digito verificador do CPF que está
			// sendo validado.
			String nDigVerific = cpf.substring(cpf.length() - VALIDADOR_DOIS_CPF, cpf.length());

			// Concatenando o primeiro resto com o
			// segundo.
			nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

			// comparar o digito verificador do cpf
			// com o primeiro resto + o segundo
			// resto.
			if (!nDigVerific.equals(nDigResult)) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
			}
		} catch (NumberFormatException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
		}
	}

	/**
	 * Validar inscricao estadual.
	 *
	 * @param inscricao the inscricao
	 * @param estado    the estado
	 * @throws GGASException the GGAS exception
	 */
	public static void validarInscricaoEstadual(String inscricao, String estado) throws GGASException {

		if (inscricao == null) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		if (!StringUtils.isEmpty(estado)) {
			String metodoValidacaoInscricaoEstadual = "validarInscricaoEstadual" + estado;
			Method metodo = null;
			try {
				metodo = Util.class.getMethod(metodoValidacaoInscricaoEstadual, String.class);
				metodo.invoke(null, inscricao);
			} catch (SecurityException e) {
				throw new GGASException(e);
			} catch (NoSuchMethodException e) {
				throw new GGASException("O método de validação da inscrição estadual para o estado: " + estado
						+ " não foi implementado.", e);
			} catch (IllegalArgumentException e) {
				throw new GGASException(e);
			} catch (IllegalAccessException e) {
				throw new GGASException(e);
			} catch (InvocationTargetException e) {
				throw new GGASException(e);
			}
		}

	}

	/**
	 * Método responsável por verificar a inscrição estadual do Estado de Sergipe.
	 *
	 * @param inscricao A inscrição estadual
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarInscricaoEstadualSE(String inscricao) throws FormatoInvalidoException {

		if ((inscricao == null) || (inscricao.length() != VALIDADOR_NOVE_INSCRICAO_ESTADUAL_SE)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		String estado = inscricao.substring(0, VALIDADOR_DOIS_INSCRICAO_ESTADUAL_SE);

		if ((estado == null) || (!"27".equals(estado))) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		Integer valor = 0;
		for (int i = VALIDADOR_NOVE_INSCRICAO_ESTADUAL_SE; i > 1; i--) {
			String num = String.valueOf(inscricao.charAt(VALIDADOR_NOVE_INSCRICAO_ESTADUAL_SE - i));
			valor += (Integer.parseInt(num) * i);
		}

		Integer modulo11 = VALIDADOR_ONZE_INSCRICAO_ESTADUAL_SE;
		Integer resto = valor - ((valor / modulo11) * VALIDADOR_ONZE_INSCRICAO_ESTADUAL_SE);
		Integer resultado = Math.abs(modulo11 - resto);
		if (resultado == VALIDADOR_DEZ_INSCRICAO_ESTADUAL_SE || resultado == VALIDADOR_ONZE_INSCRICAO_ESTADUAL_SE) {
			resultado = 0;
		}

		String digitoVerificacao = inscricao.substring(VALIDADOR_OITO_INSCRICAO_ESTADUAL_SE,
				VALIDADOR_NOVE_INSCRICAO_ESTADUAL_SE);

		if (!digitoVerificacao.equals(String.valueOf(resultado))) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

	}

	/**
	 * Método responsável por verificar a inscrição estadual do Estado de Alagoas.
	 *
	 * @param inscricao A inscrição estadual
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarInscricaoEstadualAL(String inscricao) throws FormatoInvalidoException {

		if ((inscricao == null) || (inscricao.length() != VALIDADOR_NOVE_INSCRICAO_ESTADUAL_AL)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		String estado = inscricao.substring(0, VALIDADOR_DOIS_INSCRICAO_ESTADUAL_AL);

		if ((estado == null) || (!"24".equals(estado))) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		Integer valor = 0;
		for (int i = VALIDADOR_NOVE_INSCRICAO_ESTADUAL_AL; i > 1; i--) {
			String num = String.valueOf(inscricao.charAt(VALIDADOR_NOVE_INSCRICAO_ESTADUAL_AL - i));
			valor += (Integer.parseInt(num) * i);
		}
		valor = valor * VALIDADOR_DEZ_INSCRICAO_ESTADUAL_AL;
		Integer resto = valor - ((valor / VALIDADOR_ONZE_INSCRICAO_ESTADUAL_AL) * VALIDADOR_ONZE_INSCRICAO_ESTADUAL_AL);
		if (resto == VALIDADOR_DEZ_INSCRICAO_ESTADUAL_AL) {
			resto = 0;
		}

		String digitoVerificacao = inscricao.substring(VALIDADOR_OITO_INSCRICAO_ESTADUAL_AL,
				VALIDADOR_NOVE_INSCRICAO_ESTADUAL_AL);

		if (!digitoVerificacao.equals(String.valueOf(resto))) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

	}

	/**
	 * Método responsável por verificar a inscrição estadual do Estado do Paraná.
	 *
	 * @param inscricaoEst A inscrição estadual
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarInscricaoEstadualPR(String inscricaoEst) throws FormatoInvalidoException {

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22
		String inscricao = inscricaoEst;
		if ((inscricao == null) || (inscricao.length() != VALIDADOR_DEZ_INSCRICAO_ESTADUAL_PR)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		String controle;
		String multiplicadores;
		int digito = 0;
		int soma;

		// Identifica as duas partes da inscricao
		inscricao = inscricao.trim();
		String inscricao2 = inscricao.substring(VALIDADOR_OITO_INSCRICAO_ESTADUAL_PR);

		// Inicializa a variavel de controle
		controle = "";

		// Multiplicadores que fazem parte do
		// algoritmo de checagem
		multiplicadores = "32765432";

		for (int i = 0; i < VALIDADOR_DOIS_INSCRICAO_ESTADUAL_PR; i++) {

			soma = 0;
			for (int j = 0; j < VALIDADOR_OITO_INSCRICAO_ESTADUAL_PR; j++) {
				soma = soma + Integer.parseInt(String.valueOf(inscricao.charAt(j)))
						* Integer.parseInt(String.valueOf(multiplicadores.charAt(j)));
			}
			if (soma == 0) {
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
			}
			if (i == 1) {
				soma = soma + (VALIDADOR_DOIS_INSCRICAO_ESTADUAL_PR * digito);
			}
			digito = VALIDADOR_ONZE_INSCRICAO_ESTADUAL_PR - (soma % VALIDADOR_ONZE_INSCRICAO_ESTADUAL_PR);
			if ((digito == VALIDADOR_DEZ_INSCRICAO_ESTADUAL_PR) || (digito == VALIDADOR_ONZE_INSCRICAO_ESTADUAL_PR)) {
				digito = 0;
			}

			controle = controle.concat(String.valueOf(digito).trim());

			// Sequência de multiplicadores para
			// Cálculo do segundo dígito
			multiplicadores = "432765432";
		}
		if (!controle.equals(inscricao2)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

	}

	/**
	 * Método que verifica se existe pelo menos um número na string passada por
	 * parâmetro.
	 *
	 * @param str the str
	 * @return True caso exista, False caso não exista.
	 */
	public static Boolean existeNumeroEmString(String str) {

		Pattern pattern = Pattern.compile("[0-9]");
		Matcher matcher = pattern.matcher(str);

		return matcher.find();

	}

	/**
	 * Método responsável por validar a inserção de um rg no sistema.
	 *
	 * @param registro Uma String
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarRG(String registro) throws FormatoInvalidoException {

		if (registro == null) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.RG);
		}

		registro = Util.removerLetras(registro);
		int soma = 0;

		for (int j = 0; j < registro.length(); j++) {
			soma = soma + (Integer.parseInt(String.valueOf(registro.charAt(j))));
		}

		if (soma == 0) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.RG);
		}

	}
	
	/**
	 * Métoto responsável por remover todas as letrar de um texto
	 * deixando apenas números ou espaços em branco.
	 * 
	 * @param texto O texto que será formatado
	 * @return O texto apenas com letras ou espaços
	 */
	public static String removerLetras(String texto) {
		return texto.replaceAll("[A-Za-z\\s]", "").trim();
	}

	/**
	 * Adicionar mes em ano mes.
	 *
	 * @param anoMesLeitura the ano mes leitura
	 * @return the integer
	 */
	public static Integer adicionarMesEmAnoMes(Integer anoMesLeitura) {

		int ano = Integer.parseInt(anoMesLeitura.toString().substring(0, INDICE_QUATRO));
		int mes = Integer.parseInt(anoMesLeitura.toString().substring(INDICE_QUATRO, INDICE_SEIS));
		DateTime dateTime = new DateTime(ano, mes, 1, 0, 0, 0, 0);
		DateTime anoMes = dateTime.plusMonths(1);

		int year = anoMes.getYear();
		int month = anoMes.getMonthOfYear();

		String retorno = String.valueOf(year) + adicionarZerosEsquerdaNumero(String.valueOf(month), AUXILIAR_DOIS);
		return Integer.valueOf(retorno);
	}

	/**
	 * Diminuir mes em ano mes.
	 *
	 * @param anoMesLeitura the ano mes leitura
	 * @param qtdMes        the qtd mes
	 * @return the integer
	 */
	public static Integer diminuirMesEmAnoMes(Integer anoMesLeitura, Integer qtdMes) {

		int ano = Integer.parseInt(anoMesLeitura.toString().substring(0, INDICE_QUATRO));
		int mes = Integer.parseInt(anoMesLeitura.toString().substring(INDICE_QUATRO, INDICE_SEIS));
		DateTime dateTime = new DateTime(ano, mes, 1, 0, 0, 0, 0);
		DateTime anoMes = dateTime.minusMonths(qtdMes);

		int year = anoMes.getYear();
		int month = anoMes.getMonthOfYear();

		String retorno = String.valueOf(year) + adicionarZerosEsquerdaNumero(String.valueOf(month), AUXILIAR_DOIS);
		return Integer.valueOf(retorno);
	}

	/**
	 * Converte uma String no formato mesAno em anoMes.
	 *
	 * @param mesAno the mes ano
	 * @return the string
	 */
	public static String converterMesAnoEmAnoMes(String mesAno) {

		String anoMes = "";

		if (!StringUtils.isEmpty(mesAno)) {
			String mes;
			String ano;
			anoMes = mesAno.replaceAll("/", "");

			while (anoMes.length() < AUXILIAR_SEIS) {
				anoMes = ZERO + anoMes;
			}
			ano = anoMes.substring(INDICE_DOIS, INDICE_SEIS);
			mes = anoMes.substring(0, INDICE_DOIS);
			anoMes = ano + mes;
		}

		return anoMes;
	}

	/**
	 * Converte uma String no formato anoMes em mesAno.
	 *
	 * @param anoMes the ano mes
	 * @return the string
	 */
	public static String converterAnoMesEmMesAno(String anoMes) {

		String mesAno = "";

		if (!StringUtils.isEmpty(anoMes)) {
			String mes;
			String ano;
			mesAno = anoMes.replaceAll("/", "");

			while (mesAno.length() < AUXILIAR_SEIS) {
				mesAno = ZERO + anoMes;
			}

			ano = anoMes.substring(0, INDICE_QUATRO);
			mes = anoMes.substring(INDICE_QUATRO, INDICE_SEIS);
			mesAno = mes + ano;
		}

		return mesAno;
	}

	/**
	 * Método responsável por validar se uma data é válida.
	 *
	 * @param data    Uma data
	 * @param mascara Formato da mascara da data.
	 * @return Booleano indicando se a data é válida.
	 */
	public static boolean isDataValida(String data, String mascara) {

		boolean dataValida = true;
		DateFormat dateFormat = new SimpleDateFormat(mascara);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(data);
		} catch (ParseException ex) {
			dataValida = false;
		}
		return dataValida;
	}

	/**
	 * Método responsável por transformar um array em um string separado por
	 * virgula.
	 *
	 * @param array       o Array
	 * @param delimitador O delimitador dos elementos
	 * @return Um String separado por um delimitador
	 */
	public static String arrayParaString(Object[] array, String delimitador) {

		if (array == null || delimitador == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		if (array.length > 0) {
			result.append(array[0]);
			for (int i = 1; i < array.length; i++) {
				result.append(delimitador);
				result.append(array[i]);
			}
		}
		return result.toString();
	}

	/**
	 * Array para string.
	 *
	 * @param array       the array
	 * @param delimitador the delimitador
	 * @return the string
	 */
	public static String arrayParaString(EntidadeNegocio[] array, String delimitador) {

		if (array == null || delimitador == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		if (array.length > 0) {
			result.append(array[0].getChavePrimaria());
			for (int i = 1; i < array.length; i++) {
				result.append(delimitador);
				result.append(array[i].getChavePrimaria());
			}
		}
		return result.toString();
	}

	/**
	 * Método responsável por converter um array de string para um array de long.
	 *
	 * @param array Um array de string
	 * @return Um array de long
	 */
	public static Long[] arrayStringParaArrayLong(String[] array) {

		Long[] novoArray = new Long[array.length];
		for (int i = 0; i < array.length; i++) {
			novoArray[i] = Long.valueOf(array[i]);
		}
		return novoArray;
	}

	/**
	 * Método responsável por converter um array de string para um array de Integer.
	 *
	 * @param array Um array de string
	 * @return Um array de Integer
	 */
	public static Integer[] arrayStringParaArrayInteger(String[] array) {

		Integer[] novoArray = new Integer[array.length];
		for (int i = 0; i < array.length; i++) {
			novoArray[i] = Integer.valueOf(array[i]);
		}
		return novoArray;
	}

	/**
	 * Método responsável por formatar(MM/yyyy) um inteiro anoMês para exibição.
	 *
	 * @param anoMes Um inteiro anoMês.
	 * @return O anoMês formatado.
	 */
	public static String formatarAnoMes(Integer anoMes) {

		String anoMesTemp = null;
		if (anoMes != null) {
			anoMesTemp = anoMes.toString();
			if (anoMesTemp.length() == AUXILIAR_SEIS) {
				anoMesTemp = anoMesTemp.substring(INDICE_QUATRO, INDICE_SEIS) + "/"
						+ anoMesTemp.substring(0, INDICE_QUATRO);
			}
		}

		return anoMesTemp;
	}

	/**
	 * Método responsável por formatar(yyyy/mm) um inteiro anoMês para exibição.
	 *
	 * @param anoMes Um inteiro anoMês.
	 * @return O anoMês formatado.
	 */
	public static String formatarAnoMesComMascara(Integer anoMes) {

		String anoMesTemp = null;
		if (anoMes != null) {
			anoMesTemp = anoMes.toString();
			if (anoMesTemp.length() == AUXILIAR_SEIS) {
				anoMesTemp = anoMesTemp.substring(0, INDICE_QUATRO) + "/"
						+ anoMesTemp.substring(INDICE_QUATRO, INDICE_SEIS);
			}
		}

		return anoMesTemp;
	}

	/**
	 * Método responsável por obter o anoMês para corrente.
	 *
	 * @return O anoMês corrente.
	 */
	public static Integer obterAnoMesCorrente() {

		return obterAnoMes(Calendar.getInstance().getTime());
	}

	/**
	 * Método responsável por obter o ano e mês de uma data.
	 *
	 * @param data Data para obter o ano e mês.
	 * @return Um inteiro com o valor do ano e mês da data.
	 */
	public static Integer obterAnoMes(Date data) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		String ano = String.valueOf(calendar.get(Calendar.YEAR));
		int mesData = calendar.get(Calendar.MONTH) + 1;
		String mes = String.valueOf(mesData);
		if (mesData < AUXILIAR_DEZ) {
			mes = ZERO + mes;
		}
		return Integer.valueOf(ano + mes);
	}

	/**
	 * Método responsável por formatar(yyyy,MM,dd) uma data para ser utilizada como
	 * parâmetro de intervalo de data do componente de data do JQuery.
	 *
	 * @param data Uma data.
	 * @return Um String com a data formatada(yyyy,MM,dd) para ser utilizada no
	 *         componente do JQuery.
	 */
	public static String formatarDataParametroComponente(Date data) {

		StringBuilder dataFormatadaParametro = new StringBuilder();
		if (data != null) {
			dataFormatadaParametro.append(new DateTime(data).getYear());
			dataFormatadaParametro.append(",").append((new DateTime(data).getMonthOfYear() - 1));
			dataFormatadaParametro.append(",").append(new DateTime(data).getDayOfMonth());
		}
		return dataFormatadaParametro.toString();
	}

	/**
	 * Método responsável por converter uma coleção entidades para um array de long
	 * com as chaves primarias.
	 *
	 * @param entidades Uma coleção de entidades
	 * @return Um array de long
	 */
	public static Long[] collectionParaArrayChavesPrimarias(Collection<?> entidades) {

		List<Long> novoArray = new ArrayList<Long>();
		Object entidade = null;
		for (Iterator<?> iterator = entidades.iterator(); iterator.hasNext();) {
			entidade = iterator.next();
			if (entidade instanceof EntidadeNegocio) {
				novoArray.add(Long.valueOf(((EntidadeNegocio) entidade).getChavePrimaria()));
			}
		}
		Long[] retorno = new Long[novoArray.size()];
		novoArray.toArray(retorno);

		return retorno;
	}

	/**
	 * Método responsável por converter uma coleção entidades para uma string de
	 * chaves primarias separadas pelo separador informado.
	 *
	 * @param entidades Uma coleção de entidades
	 * @param separador Uma string para servir de separador entre os valores das
	 *                  chaves
	 * @return listaChaves
	 */
	public static String collectionParaStringChavesPrimarias(Collection<?> entidades, String separador) {

		StringBuilder listaChaves = new StringBuilder();
		String separadorLocal;

		if (separador != null && !separador.isEmpty()) {
			separadorLocal = separador;
		} else {
			separadorLocal = ",";
		}

		Object entidade = null;
		for (Iterator<?> iterator = entidades.iterator(); iterator.hasNext();) {
			entidade = iterator.next();
			if (entidade instanceof EntidadeNegocio) {
				listaChaves.append(((EntidadeNegocio) entidade).getChavePrimaria());
				listaChaves.append(separadorLocal);
			}
		}

		if (listaChaves.length() > 0) {
			return listaChaves.substring(0, listaChaves.length() - separadorLocal.length());
		} else {
			return listaChaves.toString();
		}
	}

	/**
	 * Método responsável por regredir uma referência.
	 *
	 * @param referencia             the referencia
	 * @param cicloRegredir          O ciclo
	 * @param maximoCiclosReferencia O maximo de ciclo
	 * @return Um mapa contendo a referencia e ciclo regredido
	 */
	public static Map<String, Integer> regredirReferenciaCiclo(int referencia, int cicloRegredir,
			int maximoCiclosReferencia) {

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22
		int ciclo = cicloRegredir;

		Map<String, Integer> referenciaCiclo = new HashMap<String, Integer>();
		--ciclo;
		int cicloDecrementado = ciclo;
		int referenciaDecrementada = referencia;
		if (cicloDecrementado == 0) {
			referenciaDecrementada = diminuirMesEmAnoMes(referenciaDecrementada, 1);
			cicloDecrementado = maximoCiclosReferencia;
		}

		referenciaCiclo.put("ciclo", cicloDecrementado);
		referenciaCiclo.put("referencia", referenciaDecrementada);

		return referenciaCiclo;
	}

	/**
	 * Rolar referencia ciclo. Esse metodo sera usado caso o parametro
	 * UTILIZAR_MULTIPLOS_CICLOS esteja desativado. Caso contrator o metodo
	 * {@code Util.gerarProximaReferenciaCiclo}
	 *
	 * @param referencia             the referencia
	 * @param ciclo                  the ciclo
	 * @param maximoCiclosReferencia the maximo ciclos referencia
	 * @return the map
	 */
	public static Map<String, Integer> rolarReferenciaCiclo(int referencia, int ciclo, int maximoCiclosReferencia) {

		// by gmatos on 15/10/09 10:22

		Map<String, Integer> referenciaCiclo = new HashMap<String, Integer>();
		++ciclo;
		int cicloIncrementado = ciclo;
		int referenciaIncrementada = referencia;
		if (cicloIncrementado > maximoCiclosReferencia) {
			referenciaIncrementada = adicionarMesEmAnoMes(referencia);
			cicloIncrementado = 1;
		}

		referenciaCiclo.put(Constantes.CICLO, cicloIncrementado);
		referenciaCiclo.put(Constantes.REFERENCIA, referenciaIncrementada);

		return referenciaCiclo;
	}

	/**
	 * @since 14/11/2018 Rolar referencia ciclo de acordo com a periodicidade e
	 *        sobre a data. Esse metodo sera usado caso o parametro
	 *        UTILIZAR_MULTIPLOS_CICLOS esteja ativo. Caso contrator o metodo
	 *        {@code Util.rolarReferenciaCiclo}
	 *
	 * Gerar proxiam referencia ciclo
	 * @param referencia - {@link int}
	 * @param ciclo - {@link int}
	 * @param data - {@link DateTime}
	 * @param quantidadeDias - {@link int}
	 * @return referencia ciclo 
	 */
	public static Map<String, Integer> gerarProximaReferenciaCiclo(int referencia, int ciclo, int quantidadeDias,
			DateTime data) {
		Map<String, Integer> referenciaCiclo = new HashMap<String, Integer>();
		int quantidade = quantidadeDias;
		int referenciaFinal = referencia;
		data = zerarHorario(data);

		// Se for mensal ou se for o ultimo dia do mes
		// deve-se mudar a referencia e reiniciar os ciclos
		if (verificarSeEhPeriodicidadeMensalPelaQuantidadeDeDias(quantidade)
				|| obterUltimoDiaDoMesParaData(data).compareTo(data) == 0) {
			ciclo = PRIMEIRO_CICLO;
			referenciaFinal = adicionarMesEmAnoMes(referencia);
		} else {
			ciclo++;
		}

		referenciaCiclo.put(Constantes.CICLO, ciclo);
		referenciaCiclo.put(Constantes.REFERENCIA, referenciaFinal);
		return referenciaCiclo;
	}

	/**
	 * Valida se a quantidade de dias informada eh maior do que o valor da constante
	 * QUANTIDADE_MINIMA_DE_DIAS_PARA_SER_CONSIDERADO_MENSAL
	 *
	 *
	 * @param quantidadeDeDias
	 * @return
	 */
	private static boolean verificarSeEhPeriodicidadeMensalPelaQuantidadeDeDias(int quantidadeDeDias) {
		return quantidadeDeDias > Constantes.QUANTIDADE_MINIMA_DE_DIAS_PARA_SER_CONSIDERADO_MENSAL;
	}

	/**
	 * Metodo para retornar o ultimo dia de um mes de acordo com a data informa
	 *
	 * @param data - data da qual se quer descobrir o ultimo dia do mes
	 * @return {@code DateTime} com o ultimo dia do mes
	 */
	public static DateTime obterUltimoDiaDoMesParaData(DateTime data) {
		return data.dayOfMonth().withMaximumValue();
	}
	
	/**
	 * Metodo para retornar o primeiro dia de um mes de acordo com a data informada
	 *
	 * @param data - data da qual se quer descobrir o ultimo dia do mes
	 * @return {@code DateTime} com o ultimo dia do mes
	 */
	public static DateTime obterPrimeiroDiaDoMesParaData(DateTime data) {
		return data.dayOfMonth().withMinimumValue();
	}

	/**
	 * @since 14/11/2018 Rolar referencia ciclo de acordo com a periodicidade e
	 *        sobre a data.
	 *
	 * Gerar proxima referencia ciclo
	 * @param referencia    {@link int}
	 * @param ciclo         {@link int}
	 * @param quantidadeDias {@link int}
	 * @param dataParametro {@link Date}
	 * @return Map {@link Map}
	 */
	public static Map<String, Integer> gerarProximaReferenciaCiclo(int referencia, int ciclo, int quantidadeDias,
			Date dataParametro) {
		DateTime data = new DateTime(dataParametro.getTime());
		return gerarProximaReferenciaCiclo(referencia, ciclo, quantidadeDias, data);
	}

	/**
	 * @param quantidadeDeDias {@link int}
	 * @param data             {@link DataTime}
	 * @return true - caso tenha virado
	 */
	public static boolean verificarSeVirouOMes(int quantidadeDeDias, DateTime data) {
		int anoMes = converterDataParaReferenciaAnoMes(data);
		int anoMesPosterior = converterDataParaReferenciaAnoMes(data.plusDays(quantidadeDeDias));
		return anoMesPosterior > anoMes;
	}

	/**
	 * Converte uma data qualquer para o formata de REFERENCIA (anoMes): AAAAMM
	 *
	 * @param data - {@link DateTime}
	 * @return referencia AAAAMM
	 */
	public static int converterDataParaReferenciaAnoMes(DateTime data) {
		return Integer.parseInt(
				String.format("%d%s", data.getYear(), StringUtils.leftPad(String.valueOf(data.getMonthOfYear()),
						QUANTIDADE_DE_DIGITOS_PARA_MES, CARACTER_A_ESQUERDA_PARA_MES_DE_UM_DIGITO)));
	}

	/**
	 * Regredir multiplos referencia ciclo.
	 *
	 * @param referencia             the referencia
	 * @param ciclo                  the ciclo
	 * @param maximoCiclosReferencia the maximo ciclos referencia
	 * @param numeroCiclosRegressao  the numero ciclos regressao
	 * @return the map
	 */
	public static Map<String, Integer> regredirMultiplosReferenciaCiclo(int referencia, int ciclo,
			int maximoCiclosReferencia, int numeroCiclosRegressao) {

		Map<String, Integer> referenciaCicloCalculado = new HashMap<String, Integer>();
		referenciaCicloCalculado.put("ciclo", ciclo);
		referenciaCicloCalculado.put("referencia", referencia);
		for (int i = 0; i < numeroCiclosRegressao; i++) {
			referenciaCicloCalculado = regredirReferenciaCiclo(referenciaCicloCalculado.get("referencia"),
					referenciaCicloCalculado.get("ciclo"), maximoCiclosReferencia);
		}

		return referenciaCicloCalculado;
	}

	/**
	 * Método responsável por gerar o intervalo com as datas a partir de uma data
	 * inicial até a data final.
	 *
	 * @param dataLeituraInicial Data inicial do intervalo.
	 * @param dataLeituraFinal   Data final do intervalo.
	 * @return Uma coleção de datas
	 */
	public static Collection<Date> gerarIntervaloDatas(Date dataLeituraInicial, Date dataLeituraFinal) {

		Collection<Date> datas = new HashSet<Date>();

		DateTime dataInicial = new DateTime(dataLeituraInicial);
		dataInicial = zerarHorario(dataInicial);

		DateTime dataFinal = new DateTime(dataLeituraFinal);
		dataFinal = zerarHorario(dataFinal);

		Days dias = Days.daysBetween(dataInicial, dataFinal);
		for (int i = 0; i < dias.getDays(); i++) {
			DateTime data = dataInicial.plusDays(i + 1);
			datas.add(data.toDate());
		}
		return datas;
	}

	/**
	 * Gets the data corrente.
	 *
	 * @param comHora the com hora
	 * @return the data corrente
	 */
	public static Date getDataCorrente(boolean comHora) {

		Date data = Calendar.getInstance().getTime();

		if (!comHora) {
			data = zerarHorario(new DateTime()).toDate();
		}
		return data;
	}

	/**
	 * Gets the data corrente.
	 *
	 * @return the data corrente
	 */
	public static String getDataCorrente() {

		String dataHora;
		dataHora = converterDataParaStringSemHoraSemBarra(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR);

		return dataHora;
	}

	/**
	 * Zera os atributos do horario da data informada.
	 *
	 * @param data Data a ser alterada
	 * @return dataNova
	 */
	public static DateTime zerarHorario(DateTime data) {

		DateTime dataNova = data.withHourOfDay(0);
		dataNova = dataNova.withMinuteOfHour(0);
		dataNova = dataNova.withSecondOfMinute(0);
		dataNova = dataNova.withMillisOfSecond(0);

		return dataNova;
	}

	/**
	 * Zera os atributos do horario da data informada.
	 *
	 * @param data Data a ser alterada
	 * @return dataNova
	 */
	public static DateTime ultimoHorario(DateTime data) {

		DateTime dataNova = data.withHourOfDay(LIMITE_HORA);
		dataNova = dataNova.withMinuteOfHour(LIMITE_MIN_SEG);
		dataNova = dataNova.withSecondOfMinute(LIMITE_MIN_SEG);
		dataNova = dataNova.withMillisOfSecond(LIMITE_MIN_SEG);

		return dataNova;
	}

	/**
	 * Método responsável por formatar um valor utilizando uma mascara.
	 *
	 * @param valor O valor que será formatado
	 * @return O valor formatado
	 */
	public static String formatarValorNumerico(Object valor) {

		String retorno = null;
		NumberFormat numberFormat = NumberFormat.getInstance();

		retorno = numberFormat.format(valor);

		return retorno;
	}

	/**
	 * Método responsável por converter um string para um valor Integer ano/mês.
	 *
	 * @param rotulo   O rotulo do campo
	 * @param strValor O valor ano/mês
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static Integer converterCampoStringParaValorAnoMes(String rotulo, String strValor)
			throws FormatoInvalidoException {

		Integer valor = null;

		try {
			valor = Integer.valueOf(strValor);
		} catch (NumberFormatException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		if (!validarAnoMes(valor)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Método responsável por converter um string formatado para um valor Integer
	 * ano/mês.
	 *
	 * @param rotulo   O rotulo do campo
	 * @param strValor O valor ano/mês
	 * @return Uma valor
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static Integer converterCampoStringFormatadoParaValorAnoMes(String rotulo, String strValor)
			throws FormatoInvalidoException {

		Integer valor = null;

		try {
			if (strValor.indexOf('/') != -1) {
				String strValorSemFormatacao = strValor.replace("/", "");
				valor = Integer.valueOf(strValorSemFormatacao);
			} else {
				valor = Integer.valueOf(strValor);
			}
		} catch (NumberFormatException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		if (!validarAnoMes(valor)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Valida um ano/mês informado.
	 *
	 * @param anoMes Um ano mês.
	 * @return true caso válido.
	 */
	public static boolean validarAnoMes(Integer anoMes) {

		boolean retorno = true;

		if (anoMes.toString().length() != 6) {
			retorno = false;
		} else {
			int ano = Integer.parseInt(anoMes.toString().substring(0, INDICE_QUATRO));
			int mes = Integer.parseInt(anoMes.toString().substring(INDICE_QUATRO, INDICE_SEIS));

			if (mes < 1 || mes > AUXILIAR_DOZE || ano <= 0) {
				retorno = false;
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por truncar um valor.
	 *
	 * @param valor O valor
	 * @param casas A quantidade de casas
	 * @return Um número truncado
	 */
	public static double trunc(double valor, int casas) {

		double pow = Math.pow(BASE_DEZ, casas);
		return Math.floor((valor + 0.00000001) * pow) / pow;
	}

	/**
	 * Método responsável por arredondar um valor.
	 *
	 * @param valor O valor
	 * @param casas A quantidade de casas
	 * @return Um número arredondado
	 */
	public static double round(double valor, int casas) {

		double pow = Math.pow(BASE_DEZ, casas);
		return Math.round(valor * pow) / pow;
	}

	/**
	 * Método responsável por comparar duas Datas (somente datas).
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal   the data final
	 * @return Uma coleção de datas
	 */
	public static int compararDatas(Date dataInicial, Date dataFinal) {

		DateTime data1 = new DateTime(dataInicial);
		try {
			// TODO problema encontrado na algas:
			// contrato - Util.compararDatas quando estar adicionando uma modalidade
			data1 = data1.withHourOfDay(0);
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}
		data1 = data1.withMinuteOfHour(0);
		data1 = data1.withSecondOfMinute(0);
		data1 = data1.withMillisOfSecond(0);

		DateTime data2 = new DateTime(dataFinal);
		data2 = data2.withHourOfDay(0);
		data2 = data2.withMinuteOfHour(0);
		data2 = data2.withSecondOfMinute(0);
		data2 = data2.withMillisOfSecond(0);

		return data1.compareTo(data2);
	}

	/**
	 * Método responsável por calcular a quantidade de dias entre duas datas.
	 *
	 * @param dataInicio Data inicial.
	 * @param dataFim    Data final.
	 * @return Quantidade de dias entre as datas inicial e final.
	 */
	public static int intervaloDatas(Date dataInicio, Date dataFim) {

		DateTime dataInicial = new DateTime(dataInicio);
		dataInicial = dataInicial.withHourOfDay(0);
		dataInicial = dataInicial.withMinuteOfHour(0);
		dataInicial = dataInicial.withSecondOfMinute(0);
		dataInicial = dataInicial.withMillisOfSecond(0);

		DateTime dataFinal = new DateTime(dataFim);
		dataFinal = dataFinal.withHourOfDay(0);
		dataFinal = dataFinal.withMinuteOfHour(0);
		dataFinal = dataFinal.withSecondOfMinute(0);
		dataFinal = dataFinal.withMillisOfSecond(0);

		Days dias = Days.daysBetween(dataInicial, dataFinal);
		return dias.getDays();
	}

	/**
	 * Esse método retorna a quantidade de dias do intervalo contando inclusive com
	 * as datas iniciais e finais. Ex. Intervalo 01/01/2010 a 03/01/2010 - O método
	 * retorna 3.
	 *
	 * @param dataInicio Data inicial.
	 * @param dataFim    Data Final
	 * @return Quantidade de dias entre duas datas contando inclusive com as
	 *         iniciais e finais.
	 */
	public static int quantidadeDiasIntervalo(Date dataInicio, Date dataFim) {

		int retorno = 0;
		DateTime dtInicio = new DateTime(dataInicio);
		DateTime dtFim = new DateTime(dataFim);
		DateTime dateTimeIteracao = dtInicio;
		while (!dateTimeIteracao.isAfter(dtFim)) {
			dateTimeIteracao = dateTimeIteracao.plusDays(1);
			retorno++;
		}
		return retorno;
	}

	/**
	 * Método responsável por gerar uma data sem hora.
	 *
	 * @param data A data.
	 * @return Uma data sem a hora.
	 * @deprecated utilizar DataUtil.gerarDataHmsZerados
	 * @deprecated
	 */
	@Deprecated
	public static Date gerarDataSemHora(Date data) {

		DateTime dataSemHora = new DateTime(data);
		dataSemHora = dataSemHora.withHourOfDay(0);
		dataSemHora = dataSemHora.withMinuteOfHour(0);
		dataSemHora = dataSemHora.withSecondOfMinute(0);
		dataSemHora = dataSemHora.withMillisOfSecond(0);
		return dataSemHora.toDate();
	}

	/**
	 * Converte um Integer(999999) para uma String(99/9999) passado como parametro.
	 *
	 * @param data the data
	 * @return String - 99/9999
	 */
	public static String converterDataIntegerParaDataString(Integer data) {

		StringBuilder mascara = new StringBuilder();

		if (String.valueOf(data).length() == AUXILIAR_CINCO) {
			mascara.append(ZERO);
			mascara.append(String.valueOf(data).substring(0, 1));
			mascara.append("/");
			mascara.append(String.valueOf(data).substring(1));
		} else if (String.valueOf(data).length() == AUXILIAR_SEIS) {
			mascara.append(String.valueOf(data).substring(0, INDICE_DOIS));
			mascara.append("/");
			mascara.append(String.valueOf(data).substring(INDICE_DOIS));
		}

		return mascara.toString();
	}

	/**
	 * Método responsável por converter um valor currency para string.
	 *
	 * @param valor  O valor
	 * @param locale O Locale da aplicação
	 * @return Um valor
	 */
	public static String converterCampoCurrencyParaString(BigDecimal valor, Locale locale) {

		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

		if (valor.compareTo(BigDecimal.ZERO) >= 0) {

			return numberFormat.format(valor).substring(INDICE_TRES);

		} else {

			return "-" + numberFormat.format(valor).substring(INDICE_TRES).trim();
		}
	}

	/**
	 * Método responsável por converter um valor percentual para string.
	 *
	 * @param rotulo O rorulo do campo
	 * @param valor  O valor
	 * @param locale O Locale da aplicação
	 * @return Um valor
	 */
	public static String converterCampoPercentualParaString(String rotulo, BigDecimal valor, Locale locale) {

		DecimalFormat decimalFormat = new DecimalFormat("0.00", new DecimalFormatSymbols(locale));
		return decimalFormat.format(valor);
	}

	/**
	 * Método responsável por obter o nome de um campo.
	 *
	 * @param prefixo O prefixo do campo
	 * @param campo   O campo
	 * @return Um nome do campo com prefixo
	 */
	public static String obterNomeCampoComPrefixo(String prefixo, String campo) {

		StringBuilder nomeCampoSelecao = new StringBuilder();
		if (!StringUtils.isEmpty(prefixo) && !StringUtils.isEmpty(campo)) {
			nomeCampoSelecao.append(prefixo);
			nomeCampoSelecao.append(String.valueOf(campo.charAt(0)).toUpperCase());
			nomeCampoSelecao.append(campo.substring(1, campo.length()));
		}
		return nomeCampoSelecao.toString();
	}

	/**
	 * Método responsável por obter o nome de um campo de seleção.
	 *
	 * @param campo O campo
	 * @return O nome do campo de seleção
	 */
	public static String obterNomeCampoSelecao(String campo) {

		return Util.obterNomeCampoComPrefixo("sel", campo);
	}

	/**
	 * Método responsável por obter o nome de um campo obrigatório.
	 *
	 * @param campo O campo
	 * @return O nome do campo obrigatório
	 */
	public static String obterNomeCampoObrigatorio(String campo) {

		return Util.obterNomeCampoComPrefixo("obg", campo);
	}

	/**
	 * Método responsável por obter o nome de um campo com valor fixo.
	 *
	 * @param campo O campo
	 * @return O nome do campo com valor fixo
	 */
	public static String obterNomeCampoValorFixo(String campo) {

		return Util.obterNomeCampoComPrefixo("valorFixo", campo);
	}

	/**
	 * Método responsável por obter o nome de um campo dependente.
	 *
	 * @param campo O campo
	 * @return O nome do campo de seleção
	 */
	public static String obterNomeCampoDependente(String campo) {

		return Util.obterNomeCampoComPrefixo("dep", campo);
	}

	/**
	 * Método responsável por converter um valor BigDecimal em percentual para um
	 * valor BigDecimal real a ser armazenado no BD.
	 *
	 * @param bigDecimal O valor a ser transformado
	 * @return Um valor BigDecimal real
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 */
	public static BigDecimal converterBigDecimalParaPercentual(BigDecimal bigDecimal) throws FormatoInvalidoException {

		BigDecimal valor = null;

		try {
			valor = bigDecimal.divide(new BigDecimal(NUMERO_CEM));
		} catch (ArithmeticException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, false);
		}

		return valor;
	}

	/**
	 * Converter percentual para big decimal.
	 *
	 * @param bigDecimal O valor a ser transformado
	 * @return Um valor BigDecimal em percentual
	 * @throws FormatoInvalidoException Caso ocorra algum erro de conversão ou
	 *                                  formato
	 * @deprecated utilize o metodo BigDecimalUtil.converterPercentualParaBigDecimal
	 *             Método responsável por converter um valor BigDecimal real para um
	 *             valor BigDecimal em percentual .
	 * @deprecated
	 */
	@Deprecated
	public static BigDecimal converterPercentualParaBigDecimal(BigDecimal bigDecimal) throws FormatoInvalidoException {

		BigDecimal valor = null;

		try {

			valor = bigDecimal.multiply(new BigDecimal(AUXILIAR_CEM));
		} catch (ArithmeticException e) {
			LOG.error(e);
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, false);
		}

		return valor;
	}

	/**
	 * Método responsável por converter um string para um valor decimal.
	 *
	 * @param rotulo O rótulo do campo
	 * @param valor  O valor
	 * @param locale O Locale da aplicação
	 * @return Uma valor
	 */
	public static String converterCampoValorDecimalParaString(String rotulo, BigDecimal valor, Locale locale) {

		DecimalFormat decimalFormat = obterDecimalFormatLocalizado(locale, null);
		return decimalFormat.format(valor);
	}

	/**
	 * Converter campo valor decimal para string.
	 *
	 * @param rotulo         the rotulo
	 * @param valor          the valor
	 * @param locale         the locale
	 * @param numeroDecimais the numero decimais
	 * @return the string
	 */
	public static String converterCampoValorDecimalParaString(String rotulo, BigDecimal valor, Locale locale,
			int numeroDecimais) {

		DecimalFormat decf = obterDecimalFormatLocalizado(locale, numeroDecimais);
		return decf.format(valor);
	}

	/**
	 * método que obtém um Locale para Formatacao de Decimais baseado em Localidade
	 * e numero de Digitos decimais.
	 *
	 * @param locale         the locale
	 * @param numeroDecimais the numero decimais
	 * @return the decimal format
	 */
	private static DecimalFormat obterDecimalFormatLocalizado(Locale locale, Integer numeroDecimais) {

		DecimalFormat decimalFormat = new DecimalFormat(Constantes.FORMATO_VALOR_NUMERO,
				new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);
		decimalFormat.setDecimalSeparatorAlwaysShown(true);
		if (numeroDecimais == null) {
			decimalFormat.setMinimumFractionDigits(Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
		} else {
			decimalFormat.setMinimumFractionDigits(numeroDecimais);
			decimalFormat.setMaximumFractionDigits(numeroDecimais);
		}

		return decimalFormat;
	}

	/**
	 * Método responsável por obter um valor pela posição.
	 *
	 * @param linha   A linha
	 * @param posicao A posição inicial e final
	 * @return Um texto recuperado
	 */
	public static String obterValorPosicao(String linha, int[] posicao) {

		String retorno = null;
		if (!StringUtils.isEmpty(linha)) {
			retorno = linha.substring(posicao[0] - 1, posicao[1]);
		}
		return retorno;
	}

	/**
	 * Método responsável por inicializar um array.
	 *
	 * @param array O array que será inicializado
	 */
	public static void inicializarArray(char[] array) {

		for (int i = 0; i < array.length; i++) {
			array[i] = ' ';
		}
	}

	/**
	 * Método responsável por preencher um array com um texto.
	 *
	 * @param posicao A posição inicial e final
	 * @param texto   O texto
	 * @param array   O array que será preenchido
	 */
	public static void preencherArray(int[] posicao, String texto, char[] array) {

		int j = 0;
		for (int i = posicao[0] - 1; i <= posicao[1] - 1; i++) {
			if (texto.length() > j) {
				array[i] = texto.charAt(j);
			}
			j++;
		}
	}

	/**
	 * Método responsável por listar a quantidade de anos mediante a passagem de um
	 * parametro.
	 *
	 * @param valorQuantidadeAnos A quantidade dos ultimos anos que se quer obter
	 * @return the collection
	 */
	public static Collection<String> listarAnos(String valorQuantidadeAnos) {

		Collection<String> listaAnos = new ArrayList<String>();
		Integer anoAtual = new DateTime().getYear();
		Integer quantidadeAnos;

		if (StringUtils.isEmpty(valorQuantidadeAnos)) {
			quantidadeAnos = AUXILIAR_CINQUENTA;
		} else {
			quantidadeAnos = Integer.valueOf(valorQuantidadeAnos);
		}

		for (int i = 0; i < quantidadeAnos; i++) {
			Integer ano = anoAtual - i;
			listaAnos.add(ano.toString());
		}

		return listaAnos;
	}

	/**
	 * Listar anos feriado.
	 *
	 * @param valorQuantidadeAnos the valor quantidade anos
	 * @return the collection
	 */
	public static Collection<String> listarAnosFeriado(String valorQuantidadeAnos) {

		Collection<String> listaAnos = new ArrayList<String>();
		Integer anoAtual = new DateTime().getYear();
		Integer quantidadeAnos;

		if (StringUtils.isEmpty(valorQuantidadeAnos)) {
			quantidadeAnos = AUXILIAR_CINQUENTA;
		} else {
			quantidadeAnos = Integer.valueOf(valorQuantidadeAnos);
		}

		for (int i = quantidadeAnos; i > 0; i--) {
			Integer ano = anoAtual + i;
			listaAnos.add(ano.toString());
		}

		for (int i = 0; i < quantidadeAnos + 1; i++) {
			Integer ano = anoAtual - i;
			listaAnos.add(ano.toString());
		}

		return listaAnos;
	}

	/**
	 * Método para salvar um relatório gerado em um arquivo.
	 *
	 * @param bytes         the bytes
	 * @param nomeRelatorio the nome relatorio
	 */
	public static void salvarRelatorioNoDiretorio(byte[] bytes, String nomeRelatorio) {

		if (bytes != null) {
			ByteArrayInputStream byteArrayInputStream = null;
			File pdf = null;
			FileOutputStream saida = null;
			try {
				byteArrayInputStream = new ByteArrayInputStream(bytes);

				ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

				String pathDiretorio = (String) controladorParametrosSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
				File diretorio = getFile(pathDiretorio);

				if (!diretorio.exists() && !diretorio.mkdirs()) {
					throw new NegocioException(
							ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
							Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO);
				}

				pdf = getFile(pathDiretorio + nomeRelatorio + Calendar.getInstance().getTime().getTime()
						+ Constantes.EXTENSAO_ARQUIVO_PDF);
				saida = new FileOutputStream(pdf);
				int data;
				while ((data = byteArrayInputStream.read()) != -1) {
					char caractere = (char) data;
					saida.write(caractere);
				}
				saida.flush();
				saida.close();
			} catch (Exception e) {
				throw new InfraestruturaException(e.getMessage(), e);
			}
		}
	}

	/**
	 * Método responsável por substituir os argumentos em um texto, ex: Olá {0}.
	 *
	 * @param texo               O texto que será processado
	 * @param numeroDeArgumentos O Número do arqumento
	 * @param valorDoArgumento   O valor do argumento
	 * @return Um texto com os argumentos subtituídos
	 */
	public static String substituirArgumentosDoTexto(String texo, int numeroDeArgumentos, Object valorDoArgumento) {

		StringBuilder stringBuilder = new StringBuilder(texo.length() + AUXILIAR_DEZ);
		String chave = "{" + numeroDeArgumentos + "}";
		int i = texo.indexOf(chave);
		if (i >= 0) {
			stringBuilder.append(texo.substring(0, i));
			stringBuilder.append(valorDoArgumento);
			stringBuilder.append(texo.substring(i + chave.length()));
		} else {
			stringBuilder.append(texo);
		}
		return stringBuilder.toString();
	}

	/**
	 * Método responsável por comparar dois AnoMes.
	 *
	 * @param anoMesInicial anoMes inicial.
	 * @param anoMesFinal   anoMes final.
	 * @return valor menor que 0 caso o inicial seja menor que a final, valor igual
	 *         a 0 caso o inicial seja igual ao final, valor maior que 0 caso o
	 *         inicial seja maior que o final.
	 */
	public static int compararAnoMes(Integer anoMesInicial, Integer anoMesFinal) {

		DateTime dataInicial = new DateTime();
		int ano = (anoMesInicial / AUXILIAR_CEM);
		int mes = (anoMesInicial % AUXILIAR_CEM);
		dataInicial = dataInicial.withDate(ano, mes, 1);
		DateTime dataFinal = new DateTime();
		ano = (anoMesFinal / AUXILIAR_CEM);
		mes = (anoMesFinal % AUXILIAR_CEM);
		dataFinal = dataFinal.withDate(ano, mes, 1);

		return compararDatas(dataInicial.toDate(), dataFinal.toDate());
	}

	/**
	 * Método responsável por montar uma máscada de acordo com a quantidade de casas
	 * decimais.
	 *
	 * @param qtdCasasDecimais Quantidade de casas decimais.
	 * @return Máscara.
	 */
	public static String montarMascaraPorQtdCasasDecimais(int qtdCasasDecimais) {

		StringBuilder mascara = new StringBuilder();
		mascara.append(ZERO);

		if (qtdCasasDecimais > 0) {
			mascara.append(".");
			for (int i = 1; i <= qtdCasasDecimais; i++) {
				mascara.append(ZERO);
			}
		}
		return mascara.toString();
	}

	/**
	 * Formatar data tarifa.
	 *
	 * @param dataIncio the data incio
	 * @param dataFim   the data fim
	 * @return the string
	 */
	public static String formatarDataTarifa(Date dataIncio, Date dataFim) {

		StringBuilder dataFormatada = new StringBuilder();
		dataFormatada.append("De ");
		dataFormatada.append(converterDataParaStringSemHora(dataIncio, "dd/MM/yyyy"));
		dataFormatada.append(" a ");
		dataFormatada.append(converterDataParaStringSemHora(dataFim, "dd/MM/yyyy"));

		return dataFormatada.toString();
	}

	/**
	 * Método responsável por calcular a quantidade de meses antre duas datas.
	 *
	 * @param dataInicio Data inicial.
	 * @param dataFim    Data final.
	 * @return Quantidade de meses entre a data inicial e a data final.
	 */
	public static int intervaloMeses(Date dataInicio, Date dataFim) {

		DateTime dataInicial = new DateTime(dataInicio);
		dataInicial = dataInicial.withDayOfMonth(1);
		dataInicial = dataInicial.withHourOfDay(0);
		dataInicial = dataInicial.withMinuteOfHour(0);
		dataInicial = dataInicial.withSecondOfMinute(0);
		dataInicial = dataInicial.withMillisOfSecond(0);

		DateTime dataFinal = new DateTime(dataFim);
		dataFinal = dataFinal.withDayOfMonth(1);
		dataFinal = dataFinal.withHourOfDay(0);
		dataFinal = dataFinal.withMinuteOfHour(0);
		dataFinal = dataFinal.withSecondOfMinute(0);
		dataFinal = dataFinal.withMillisOfSecond(0);

		Months months = Months.monthsBetween(dataInicial, dataFinal);

		return months.getMonths();
	}

	/**
	 * Método responsável por calcular a quantidade de anos antre duas datas.
	 *
	 * @param dataInicio Data inicial.
	 * @param dataFim    Data final.
	 * @return Quantidade de anos entre a data inicial e a data final.
	 */
	public static int intervaloAnos(Date dataInicio, Date dataFim) {

		DateTime dataInicial = new DateTime(dataInicio);
		dataInicial = dataInicial.withDayOfMonth(1);
		dataInicial = dataInicial.withHourOfDay(0);
		dataInicial = dataInicial.withMinuteOfHour(0);
		dataInicial = dataInicial.withSecondOfMinute(0);
		dataInicial = dataInicial.withMillisOfSecond(0);

		DateTime dataFinal = new DateTime(dataFim);
		dataFinal = dataFinal.withDayOfMonth(1);
		dataFinal = dataFinal.withHourOfDay(0);
		dataFinal = dataFinal.withMinuteOfHour(0);
		dataFinal = dataFinal.withSecondOfMinute(0);
		dataFinal = dataFinal.withMillisOfSecond(0);

		Years years = Years.yearsBetween(dataInicial, dataFinal);

		return years.getYears();
	}

	/**
	 * Método responsável por converter um File em byte[].
	 *
	 * @param arquivo the arquivo
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static byte[] converterFileParaByte(File arquivo) throws IOException {

		InputStream stream = null;
		try {
			stream = new FileInputStream(arquivo);

			// obtém o tamanho do arquivo
			long length = arquivo.length();

			if (length > Integer.MAX_VALUE) {
				// arquivo é muito grande
			}

			// cria um array de byte para suportar os
			// dados
			byte[] bytes = new byte[(int) length];

			// lê os bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length && (numRead = stream.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}

			// valida se todos os bytes foram lidos
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + arquivo.getName());
			}

			stream.close();
			return bytes;

		} finally {
			if (stream != null) {
				stream.close();
			}
		}
	}

	/**
	 * Método responsável por apagar um diretório com todo seu conteúdo.
	 *
	 * @param diretorio the diretorio
	 * @return true, if successful
	 */
	public static boolean apagarDiretorio(File diretorio) {

		String[] children = null;

		if (diretorio.isDirectory()) {
			children = diretorio.list();
		}

		if (children != null) {
			for (int i = 0; i < children.length; i++) {
				boolean success = apagarDiretorio(new File(diretorio, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		return diretorio.delete();
	}

	/**
	 * Método responsável por retornar Cálculo do dígito verificador do código de
	 * barras.
	 *
	 * @param cep the cep
	 * @return data com hora, minuto e segundo alterados.
	 */
	public static String calculoCodigoBarrasEndereco(String cep) {

		int tam;
		int soma;
		int numero;
		int dv;
		String tmp = cep;
		String aux2;
		String ud;
		String digito;

		tmp = tmp.replace("-", "");
		tmp = tmp.trim();

		numero = Integer.parseInt(tmp);
		tam = tmp.length();
		soma = 0;

		for (int j = 0; j < tam; j++) {
			aux2 = tmp.substring(j, j + 1);
			numero = Integer.parseInt(aux2);
			soma = soma + numero;
		}

		digito = String.valueOf(soma);
		tam = digito.length();
		ud = digito.substring(tam - 1, tam);
		numero = Integer.parseInt(ud);

		if (ZERO.equals(ud)) {
			dv = 0;
		} else {
			dv = AUXILIAR_DEZ - numero;
		}

		return tmp + dv;
	}

	/**
	 * Validar intervalo datas.
	 *
	 * @param dataInicio the data inicio
	 * @param dataFinal  the data final
	 * @throws GGASException the GGAS exception
	 */
	public static void validarIntervaloDatas(Date dataInicio, Date dataFinal) throws GGASException {

		if (dataInicio != null && dataFinal != null && dataInicio.compareTo(dataFinal) > 0) {
			throw new GGASException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
		}
	}

	/**
	 * Converte retira a barra da referência e inverte a ordem para ano mês String.
	 *
	 * @param referenciaParam the referencia param
	 * @return String referência sem barra no formato AAAAMM
	 */
	public static String formatarReferenciaMesAno(String referenciaParam) {

		String referencia = referenciaParam;

		referencia = referencia.replace("/", "");

		String ano = referencia.substring(INDICE_DOIS, INDICE_SEIS);
		String mes = referencia.substring(0, INDICE_DOIS);

		referencia = ano + mes;
		return referencia;
	}

	/**
	 * Método responsável por carregar o arquivo de properties do sistema.
	 *
	 * @param nomeArquivo the nome arquivo
	 * @return Properties.
	 * @throws GGASException the GGAS exception
	 */
	public static synchronized Properties lerArquivoPropriedades(String nomeArquivo) throws GGASException {

		Properties prop = null;
		InputStream input = null;
		Exception exception = null;

		try {
			input = Util.class.getClassLoader().getResourceAsStream(nomeArquivo);
			prop = new Properties();
			prop.load(input);
		} catch (IOException ex) {
			exception = ex;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException ex) {
					exception = suprimirExcecao(ex, exception);
				}
			}
		}
		if (exception != null) {
			throw new GGASException(exception);
		}
		return prop;
	}

	/**
	 * Método responsável por incrementar uma dia.
	 *
	 * @param data A data que será incrementada
	 * @param i    O número de dias
	 * @return Uma data
	 */
	public static Date incrementarDia(Date data, int i) {

		DateTime dateTime = new DateTime(data);
		return dateTime.plusDays(i).toDate();
	}

	/**
	 * Decrementar dia.
	 *
	 * @param data A data que será incrementada
	 * @param i    O número de dias
	 * @return Uma data
	 * @deprecated utilizar o metodo decrementarDia de DataUtil Método responsável
	 *             por incrementar uma dia.
	 */
	@Deprecated
	public static Date decrementarDia(Date data, int i) {

		DateTime dateTime = new DateTime(data);
		return dateTime.minusDays(i).toDate();
	}

	/**
	 * Método responsável por retornar uma lista com as datas dos dias
	 * intermediários entre a data de inicio e a data fim.
	 *
	 * @param dataInicio Data de inicio do intervalo
	 * @param dataFim    Data de fim do intervalo
	 * @return the collection
	 */
	public static Collection<Date> obterDatasIntermediarias(Date dataInicio, Date dataFim) {

		Collection<Date> listaDatas = new ArrayList<Date>();
		if (compararDatas(dataInicio, dataFim) < 0) {
			DateTime dateTime = new DateTime(dataInicio);

			listaDatas.add(dataInicio);
			dateTime = dateTime.plusDays(1);

			while (compararDatas(dateTime.toDate(), dataFim) < 0) {
				listaDatas.add(dateTime.toDate());
				dateTime = dateTime.plusDays(1);
			}

			listaDatas.add(dataFim);
		}
		return listaDatas;
	}

	/**
	 * Método responsável por obter o intervalo ano / mes entre data de inicio e
	 * data de fim.
	 *
	 * @param dataInicio the data inicio
	 * @param dataFim    the data fim
	 * @param isAnoCivil the is ano civil
	 * @return Map<Integer, Integer>
	 */
	public static Map<Integer, Integer> obterIntervaloAnoMesPeriodo(Date dataInicio, Date dataFim, Boolean isAnoCivil) {

		SimpleDateFormat dfAnoMes = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO_MES);
		SimpleDateFormat dfDia = new SimpleDateFormat(Constantes.FORMATO_DATA_DIA);

		Map<Integer, Integer> mapa = Util.obterIntervaloPeriodoAnoMesMaximoDias(dataInicio, dataFim);

		Integer anoMesInicio = Integer.parseInt(dfAnoMes.format(dataInicio));
		Integer anoMesFim = Integer.parseInt(dfAnoMes.format(dataFim));

		Integer diaDataInicio = Integer.parseInt(dfDia.format(dataInicio));
		Integer diaDataFim = Integer.parseInt(dfDia.format(dataFim));

		if (isAnoCivil) {

			Integer qtdaMaximaDiasDataInicio = mapa.get(anoMesInicio);

			mapa.put(anoMesInicio, (qtdaMaximaDiasDataInicio - diaDataInicio) + 1);
			mapa.put(anoMesFim, diaDataFim);

		} else {

			if (diaDataInicio >= diaDataFim) {
				Integer dias = diaDataInicio - diaDataFim;

				if (dias == 0) {
					dias = 1;
				}

				mapa.put(anoMesFim, dias);
			} else {

				mapa.remove(anoMesFim);

				Integer maiorChave = 0;

				for (Integer key : mapa.keySet()) {
					if (key > maiorChave) {
						maiorChave = key;
					}
				}

				Integer qtdaMaximaDias = mapa.get(maiorChave);
				mapa.put(maiorChave, qtdaMaximaDias - (diaDataInicio - diaDataFim));

			}

		}

		return mapa;
	}

	/**
	 * Método responsável por obter o intervalo ano / mes / dias entre data de
	 * inicio e data de fim.
	 *
	 * @param dataInicio the data inicio
	 * @param dataFim    the data fim
	 * @return the map
	 */
	public static Map<Integer, Integer> obterIntervaloPeriodoAnoMesMaximoDias(Date dataInicio, Date dataFim) {

		Map<Integer, Integer> mapa = new TreeMap<Integer, Integer>();
		SimpleDateFormat dfAnoMes = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO_MES);

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumIntegerDigits(AUXILIAR_DOIS);

		String mesAnoDataInicioString = dfAnoMes.format(dataInicio);
		String mesAnoDataFimString = dfAnoMes.format(dataFim);

		Integer anoMesInicio = Integer.parseInt(mesAnoDataInicioString);
		Integer anoMesFim = Integer.parseInt(mesAnoDataFimString);

		Integer mes = Integer.parseInt(mesAnoDataInicioString.substring(INDICE_QUATRO, INDICE_SEIS));
		Integer ano = Integer.parseInt(mesAnoDataInicioString.substring(0, INDICE_QUATRO));

		while (anoMesInicio <= anoMesFim) {

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, ano);
			cal.set(Calendar.MONTH, mes - 1);

			mapa.put(anoMesInicio, cal.getActualMaximum(Calendar.DAY_OF_MONTH));

			if (mes != AUXILIAR_DOZE) {
				mes++;
			} else {
				ano++;
				mes = 1;
			}

			anoMesInicio = Integer.parseInt(String.valueOf(ano) + nf.format(mes));
		}

		return mapa;
	}

	/**
	 * Método responsável por retornar a quantidade de dias de um periodo.
	 *
	 * @param anoMes the ano mes
	 * @return the int
	 */
	public static int obterQuantidadeDiasPeriodo(Integer anoMes) {

		Integer ano = (anoMes / AUXILIAR_CEM);
		Integer mes = (anoMes % AUXILIAR_CEM);

		Calendar cal = new GregorianCalendar(ano, mes - 1, 1);

		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Obter data inicial data fim periodo.
	 *
	 * @param anoMes the ano mes
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Date> obterDataInicialDataFimPeriodo(Integer anoMes) throws GGASException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_US_SEM_BARRA);
		Map<String, Date> map = new HashMap<String, Date>();

		String anoString = String.valueOf(anoMes).substring(0, INDICE_QUATRO);
		String mesString = String.valueOf(anoMes).substring(INDICE_QUATRO, INDICE_SEIS);

		Integer ano = Integer.parseInt(anoString);
		Integer mes = Integer.parseInt(mesString);

		Calendar cal = new GregorianCalendar(ano, mes - 1, 1);

		try {

			map.put("dataInicial", df.parse(anoString + mesString + cal.getActualMinimum(Calendar.DAY_OF_MONTH)));
			map.put("dataFinal", df.parse(anoString + mesString + cal.getActualMaximum(Calendar.DAY_OF_MONTH)));

		} catch (ParseException e) {

			throw new GGASException(e);

		}

		return map;
	}

	/**
	 * Obter ultimo dia mes do ano mes referencia.
	 *
	 * @param anoMesContabil the ano mes contabil
	 * @return the date
	 */
	public static Date obterUltimoDiaMesDoAnoMesReferencia(String anoMesContabil) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesContabil.substring(0, INDICE_QUATRO)),
				Integer.parseInt(anoMesContabil.substring(INDICE_QUATRO, INDICE_SEIS)) - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		return calendar.getTime();
	}

	/**
	 * Obter primeiro dia mes do ano mes referencia.
	 *
	 * @param anoMesContabil the ano mes contabil
	 * @return the date
	 */
	public static Date obterPrimeiroDiaMesDoAnoMesReferencia(String anoMesContabil) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesContabil.substring(0, INDICE_QUATRO)),
				Integer.parseInt(anoMesContabil.substring(INDICE_QUATRO, INDICE_SEIS)) - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

		return calendar.getTime();
	}

	/**
	 * Obter ultimo dia mes.
	 *
	 * @param ano the ano
	 * @return the date
	 */
	public static Date obterUltimoDiaMes(Integer ano) {

		Calendar cal = Calendar.getInstance(Constantes.LOCALE_PADRAO);
		cal.set(Calendar.YEAR, ano);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));

		return cal.getTime();
	}

	/**
	 * Obter primeiro dia mes.
	 *
	 * @param ano the ano
	 * @return the date
	 */
	public static Date obterPrimeiroDiaMes(Integer ano) {

		Calendar cal = Calendar.getInstance(Constantes.LOCALE_PADRAO);
		cal.set(Calendar.YEAR, ano);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));

		return cal.getTime();
	}

	/**
	 * Ordenar colecao por atributo.
	 *
	 * @param <T>         the generic type
	 * @param colecao     the colecao
	 * @param atributo    the atributo
	 * @param isCrescente the is crescente
	 * @return the list
	 */
	public static <T> List<T> ordenarColecaoPorAtributo(Collection<T> colecao, final String atributo,
			final boolean isCrescente) {

		List<T> listaOrdenada = new ArrayList<T>();
		for (T objeto : colecao) {
			listaOrdenada.add(objeto);
		}
		Collections.sort(listaOrdenada, new Comparator<T>() {

			@Override
			public int compare(Object o1, Object o2) {

				String valorCampo1 = "";
				Field campo = null;
				try {
					campo = o1.getClass().getDeclaredField(atributo);
					campo.setAccessible(true);
					if (campo.get(o1) != null) {
						valorCampo1 = campo.get(o1).toString();
					}
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					Logger.getLogger("ERROR").debug(e.getStackTrace());
				}
				String valorCampo2 = "";
				try {
					campo = o2.getClass().getDeclaredField(atributo);
					campo.setAccessible(true);
					if (campo.get(o2) != null) {
						Object valor = campo.get(o2);
						valorCampo2 = valor.toString();
						if (valor instanceof Integer || valor instanceof Long) {
							String[] valores = formatarValorOrdenacao(valorCampo1, valorCampo2);
							valorCampo1 = valores[0];
							valorCampo2 = valores[1];
						}
					}
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					Logger.getLogger("ERROR").debug(e.getStackTrace());
				}
				int resultado = 0;
				if (isCrescente) {
					resultado = valorCampo1.compareTo(valorCampo2);
				} else {
					resultado = valorCampo2.compareTo(valorCampo1);
				}
				return resultado;
			}
		});
		colecao.clear();
		for (T t : listaOrdenada) {
			colecao.add(t);
		}
		return listaOrdenada;
	}

	/**
	 * Formatar valor ordenacao.
	 *
	 * @param valorCampo1 the valor campo1
	 * @param valorCampo2 the valor campo2
	 * @return the string[]
	 */
	private static String[] formatarValorOrdenacao(final String valorCampo1, final String valorCampo2) {

		String valorCampo1Local = valorCampo1;
		String valorCampo2Local = valorCampo2;
		String[] valores = new String[AUXILIAR_DOIS];
		int tm = valorCampo2Local.length();
		if (tm < valorCampo1Local.length()) {
			valores[0] = valorCampo1Local;
			tm = valorCampo1Local.length();
			int tm2 = valorCampo2Local.length();
			for (int i = tm2; i < tm; i++) {
				valorCampo2Local = ZERO + valorCampo2Local;
			}
			valores[1] = valorCampo2Local;
		} else {
			valores[1] = valorCampo2Local;
			tm = valorCampo2Local.length();
			int tm1 = valorCampo1Local.length();
			for (int i = tm1; i < tm; i++) {
				valorCampo1Local = ZERO + valorCampo1Local;

			}
			valores[0] = valorCampo1Local;
		}
		return valores;
	}

	/**
	 * Gets the dados auditoria.
	 *
	 * @param usuario    the usuario
	 * @param operacao   the operacao
	 * @param enderecoIP the endereco ip
	 * @return the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	public static DadosAuditoria getDadosAuditoria(Usuario usuario, Operacao operacao, String enderecoIP)
			throws NegocioException {

		DadosAuditoria dadosAuditoria = (DadosAuditoria) ServiceLocator.getInstancia()
				.getBeanPorID(DadosAuditoria.BEAN_ID_DADOS_AUDITORIA);

		dadosAuditoria.setDataHora(Calendar.getInstance().getTime());
		dadosAuditoria.setIp(enderecoIP);
		if (operacao != null) {
			Menu menu = null;
			Tabela tabela = null;
			if (operacao.getMenu() != null) {
				menu = Fachada.getInstancia().obterMenu(operacao.getMenu().getChavePrimaria());
				tabela = Fachada.getInstancia().obterTabelaMenu(menu.getChavePrimaria());
				operacao.setMenu(menu);
			}
			dadosAuditoria.setOperacao(operacao);
			if (tabela != null) {
				dadosAuditoria.getOperacao().setAuditavel(tabela.getAuditavel());
			}
		}
		if (usuario != null) {
			dadosAuditoria.setUsuario(usuario);
		}

		DadosAuditoriaUtil.setDadosAuditoria(dadosAuditoria);
		return dadosAuditoria;
	}

	/**
	 * Transformar objeto para bytes.
	 *
	 * @param objeto the objeto
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static byte[] transformarObjetoParaBytes(Object objeto) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(objeto);

		return baos.toByteArray();

	}

	/**
	 * Transformar bytes para objeto.
	 *
	 * @param raw the raw
	 * @return the object
	 * @throws IOException            Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static Object transformarBytesParaObjeto(byte[] raw) throws IOException, ClassNotFoundException {

		Object retorno = null;

		if (raw != null) {
			ByteArrayInputStream bais = new ByteArrayInputStream(raw);
			ObjectInputStream ois = new ObjectInputStream(bais);
			retorno = ois.readObject();
		}

		return retorno;

	}

	/**
	 * Monta o nome do método get para o atributo informado.
	 *
	 * @param nomeAtributo Atributo alvo
	 * @return nomeGet
	 */
	public static String montarNomeGet(String nomeAtributo) {

		StringBuilder atributo = new StringBuilder(nomeAtributo);
		atributo.setCharAt(0, atributo.substring(0, 1).toUpperCase().charAt(0));
		return "get" + atributo.toString();
	}

	/**
	 * Monta o nome do método set para o atributo informado.
	 *
	 * @param nomeAtributo Atributo alvo
	 * @return nomeSet
	 */
	public static String montarNomeSet(String nomeAtributo) {

		StringBuilder atributo = new StringBuilder(nomeAtributo);
		atributo.setCharAt(0, atributo.substring(0, 1).toUpperCase().charAt(0));
		return "set" + atributo.toString();
	}

	/**
	 * Monta o nome do método is para o atributo informado.
	 *
	 * @param nomeAtributo Atributo alvo
	 * @return nomeIs
	 */
	public static String montarNomeIs(String nomeAtributo) {

		StringBuilder atributo = new StringBuilder(nomeAtributo);
		atributo.setCharAt(0, atributo.substring(0, 1).toUpperCase().charAt(0));
		return "is" + atributo.toString();
	}

	/**
	 * Recupera a interface principal implementada pela classe informada.
	 *
	 * @param classe Classe alvo
	 * @return interfacePrincipal
	 */
	public static Class<? extends Object> buscarInterface(Class<?> classe) {

		Class<?>[] interfaces = classe.getInterfaces();
		for (Class<?> i : interfaces) {
			if (classe.getSimpleName().equals(i.getSimpleName() + REFLECTION_CLASS)) {
				return i;
			}
		}
		return classe;
	}

	/**
	 * Adicionar restricao data sem hora.
	 *
	 * @param hqlQuery     the hql query
	 * @param data         the data
	 * @param labelData    the label data
	 * @param zerarHorario the zerar horario
	 */
	public static void adicionarRestricaoDataSemHora(Query hqlQuery, Date data, String labelData,
			Boolean zerarHorario) {

		DateTime date = new DateTime(data);

		if (zerarHorario) {
			date = zerarHorario(date);
		} else {
			date = ultimoHorario(date);
		}
		hqlQuery.setTimestamp(labelData, date.toDate());
	}

	/**
	 * Intervalo anos data.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String intervaloAnosData(String valor) {

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	/**
	 * Data minima maxima.
	 *
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, String> dataMinimaMaxima() throws GGASException {

		Map<String, String> datas = new HashMap<String, String>();
		String valor = (String) Fachada.getInstancia()
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String dataMinima = "";
		String dataMaxima = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		dataMinima = "01/01/" + dataInicial.getYear();
		dataMaxima = "31/12/" + dataAtual.getYear();

		datas.put("dataMinima", dataMinima);
		datas.put("dataMaxima", dataMaxima);

		return datas;
	}

	/**
	 * Obter mes ano data.
	 *
	 * @param data   the data
	 * @param mesAno the mes ano
	 * @return the int
	 */
	public static int obterMesAnoData(Date data, int mesAno) {

		TimeZone tz = TimeZone.getDefault();
		Calendar c = Calendar.getInstance(tz);
		c.setTime(data);

		return c.get(mesAno);
	}

	/**
	 * Método responsavel por retornar um Criterion IN com segurança para evitar o
	 * erro ORA-01795: maximum number of expressions in a list is 1000.
	 *
	 * @param propriedade   the propriedade
	 * @param lista         the lista
	 * @param tamanhoMaximo the tamanho maximo
	 * @return the criterion
	 */
	@SuppressWarnings("rawtypes")
	public static Criterion construirInCriterion(String propriedade, List lista, int tamanhoMaximo) {

		Criterion criterion = null;
		int listSize = lista.size();

		for (int i = 0; i < listSize; i += tamanhoMaximo) {
			List subList;
			if (listSize > i + tamanhoMaximo) {
				subList = lista.subList(i, i + tamanhoMaximo);
			} else {
				subList = lista.subList(i, listSize);
			}
			if (criterion != null) {
				criterion = Restrictions.or(criterion, Restrictions.in(propriedade, subList));
			} else {
				criterion = Restrictions.in(propriedade, subList);
			}
		}
		return criterion;
	}

	/**
	 * Incrementar data com hora e quantidade dias.
	 *
	 * @param dataInicial       the data inicial
	 * @param numeroHoraInicial the numero hora inicial
	 * @param numeroDias        the numero dias
	 * @return the map
	 */
	public static Map<String, Date> incrementarDataComHoraEQuantidadeDias(Date dataInicial, Integer numeroHoraInicial,
			int numeroDias) {

		Map<String, Date> listaDatas = new LinkedHashMap<String, Date>();

		Calendar calendarInicial = new GregorianCalendar();
		calendarInicial.setTime(dataInicial);
		int horaInicial = calendarInicial.get(Calendar.HOUR_OF_DAY);

		Calendar calendarFinal = new GregorianCalendar();
		calendarFinal.setTime(dataInicial);

		if (horaInicial < numeroHoraInicial) {

			calendarInicial.add(Calendar.DATE, -numeroDias);

		} else {
			calendarFinal.add(Calendar.DATE, numeroDias);
		}

		calendarInicial.set(Calendar.HOUR_OF_DAY, numeroHoraInicial);
		calendarInicial.set(Calendar.MINUTE, 0);
		calendarInicial.set(Calendar.SECOND, 0);
		calendarInicial.set(Calendar.MILLISECOND, 0);

		calendarFinal.set(Calendar.HOUR_OF_DAY, numeroHoraInicial);
		calendarFinal.set(Calendar.MINUTE, 0);
		calendarFinal.set(Calendar.SECOND, 0);
		calendarFinal.set(Calendar.MILLISECOND, 0);

		listaDatas.put(Constantes.DATA_INICIAL, calendarInicial.getTime());
		listaDatas.put(Constantes.DATA_FINAL, calendarFinal.getTime());

		return listaDatas;

	}

	/**
	 * Incrementar data com quantidade dias.
	 *
	 * @param data       the data
	 * @param numeroDias the numero dias
	 * @return the date
	 */
	public static Date incrementarDataComQuantidadeDias(Date data, int numeroDias) {

		Date dataAux = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.DATE, +numeroDias);
		dataAux = cal.getTime();

		return dataAux;

	}

	/**
	 * Decrementar data com quantidade dias.
	 *
	 * @param data       the data
	 * @param numeroDias the numero dias
	 * @return the date
	 */
	public static Date decrementarDataComQuantidadeDias(Date data, int numeroDias) {

		Date dataAux = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.DATE, -numeroDias);
		dataAux = cal.getTime();

		return dataAux;

	}

	/**
	 * Verificar hora fracionada.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public static boolean verificarHoraFracionada(Date data) {

		boolean horaFracionada = false;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		if (calendar.get(Calendar.MINUTE) != 0) {
			horaFracionada = true;
		}

		return horaFracionada;
	}

	/**
	 * Incrementar hora data.
	 *
	 * @param dataIncrementar the data
	 * @param numero          the numero
	 * @return the date
	 */
	public static Date incrementarHoraData(Date dataIncrementar, int numero) {

		Date data = dataIncrementar;
		Calendar cal = Calendar.getInstance();
		cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.HOUR, numero);
		data = cal.getTime();

		return data;

	}

	/**
	 * Verificar ciclo por data para primeira medicao.
	 *
	 * @param data            the data
	 * @param quantidadeCiclo the quantidade ciclo
	 * @return the integer
	 */
	public static Integer verificarCicloPorDataParaPrimeiraMedicao(Date data, Integer quantidadeCiclo) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		// obtém a quantidade de dias do mês
		int qtdDiasMesAtual = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		// obtém o dia referente a data que foi passada como parâmetro
		int diaAtual = calendar.get(Calendar.DAY_OF_MONTH);

		// obtém o tamanho do ciclo dividindo a quantidade de dias do mês pela
		// quantidade de ciclo
		int tamanhoCiclo = qtdDiasMesAtual / quantidadeCiclo;
		Integer ciclo = 1;

		while (diaAtual > tamanhoCiclo) {
			ciclo++;
			tamanhoCiclo = tamanhoCiclo + tamanhoCiclo;
		}

		return ciclo;
	}

	/**
	 * Verificar primeira hora dia.
	 *
	 * @param data              the data
	 * @param numeroHoraInicial the numero hora inicial
	 * @return true, if successful
	 */
	public static boolean verificarPrimeiraHoraDia(Date data, Integer numeroHoraInicial) {

		Boolean isPrimeiraDataDoDia = Boolean.FALSE;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		int horaInicial = calendar.get(Calendar.HOUR);

		if (horaInicial == numeroHoraInicial) {
			isPrimeiraDataDoDia = Boolean.TRUE;
		}

		return isPrimeiraDataDoDia;
	}

	/**
	 * Gerar comentario campo.
	 *
	 * @param chaveCampo the chave campo
	 * @param valorAtual the valor atual
	 * @param novoValor  the novo valor
	 * @return the string builder
	 * @throws NegocioException the negocio exception
	 */
	public static StringBuilder gerarComentarioCampo(String chaveCampo, Object valorAtual, Object novoValor)
			throws NegocioException {

		Properties prop = null;
		StringBuilder sb = new StringBuilder();

		try {
			prop = Util.lerArquivoPropriedades("mensagens.properties");
		} catch (GGASException e) {
			throw new NegocioException(e);
		}

		if ((valorAtual != null) && (novoValor != null)) {
			if (!Util.isNumero(novoValor)) {
				if (!valorAtual.equals(novoValor)) {
					sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de ").append(valorAtual.toString())
							.append(" para ").append(novoValor.toString());
				}
			} else {
				if (!(new BigDecimal(valorAtual.toString()).compareTo(new BigDecimal(novoValor.toString())) == 0)) {
					sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de ")
							.append(new BigDecimal(valorAtual.toString())).append(" para ")
							.append(new BigDecimal(novoValor.toString()));
				}
			}
		} else {
			if (valorAtual != null) {

				sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de ")
						.append(new BigDecimal(valorAtual.toString())).append(" para não informado");

			} else if (novoValor != null) {
				if (!Util.isNumero(novoValor)) {
					sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de não informado para ")
							.append(novoValor.toString());
				} else {
					sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de não informado para ")
							.append(new BigDecimal(novoValor.toString()));
				}
			}
		}

		return sb;
	}

	/**
	 * Método que verifica se o objeto passado é numérico.
	 *
	 * @param valor the valor
	 * @return retorno
	 */
	public static boolean isNumero(Object valor) {

		try {
			Double b = Double.valueOf(valor.toString());
			if (b != null) {
				return true;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;

	}

	/**
	 * Setar tipo parametro query.
	 *
	 * @param query      the query
	 * @param nomeColuna the nome coluna
	 * @param valor      the valor
	 * @param tipo       the tipo
	 * @throws GGASException the GGAS exception
	 */
	public static void setarTipoParametroQuery(Query query, String nomeColuna, String valor, Type tipo)
			throws GGASException {

		if (tipo.equals(Long.class)) {
			query.setLong(nomeColuna, Long.parseLong(valor));
		} else if (tipo.equals(Date.class) || tipo.equals(Timestamp.class)) {
			query.setTimestamp(nomeColuna,
					Util.converterCampoStringParaData(nomeColuna, valor, Constantes.FORMATO_DATA_HORA_US_COMPLETA));
		} else if (tipo.equals(Boolean.class)) {
			query.setBoolean(nomeColuna, Boolean.parseBoolean(valor));
		} else if (tipo.equals(Short.class)) {
			query.setShort(nomeColuna, Short.parseShort(valor));
		} else if (tipo.equals(Byte.class)) {
			query.setByte(nomeColuna, Byte.parseByte(valor));
		} else if (tipo.equals(Integer.class)) {
			query.setInteger(nomeColuna, Integer.parseInt(valor));
		} else if (tipo.equals(Double.class)) {
			query.setDouble(nomeColuna, Double.parseDouble(valor));
		} else if (tipo.equals(Float.class)) {
			query.setFloat(nomeColuna, Float.parseFloat(valor));
		} else if (tipo.equals(BigDecimal.class)) {
			query.setBigDecimal(nomeColuna, Util.converterCampoStringParaValorBigDecimal(nomeColuna, valor,
					Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		} else if (tipo.equals(int.class)) {
			query.setInteger(nomeColuna, Integer.parseInt(valor));
		} else if (tipo.equals(long.class)) {
			query.setLong(nomeColuna, Long.parseLong(valor));
		} else if (tipo.equals(double.class)) {
			query.setDouble(nomeColuna, Double.parseDouble(valor));
		} else if (tipo.equals(float.class)) {
			query.setFloat(nomeColuna, Float.parseFloat(valor));
		} else if (tipo.equals(short.class)) {
			query.setShort(nomeColuna, Short.parseShort(valor));
		} else if (tipo.equals(byte.class)) {
			query.setByte(nomeColuna, Byte.parseByte(valor));
		} else if (tipo.equals(boolean.class)) {
			query.setBoolean(nomeColuna, Boolean.parseBoolean(valor));
		} else if (tipo.equals(String.class)) {
			query.setString(nomeColuna, valor);
		}

	}

	/**
	 * Fechar janela.
	 *
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void fecharJanela(HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		out.write("<html>");
		out.write("<script language=\"javascript\">");
		out.write("window.opener.recarregar();");
		out.write("self.close();");
		out.write("</script>");
		out.write("</html>");
		response.flushBuffer();
	}

	/**
	 * Arredondar numero.
	 *
	 * @param numero                  the numero
	 * @param quantidadeCasasDecimais the quantidade casas decimais
	 * @return the big decimal
	 */
	public static BigDecimal arredondarNumero(BigDecimal numero, int quantidadeCasasDecimais) {

		BigDecimal numeroRetorno = BigDecimal.ZERO;

		if (numero.precision() > AUXILIAR_DEZ) {
			int diferenca = numero.precision() - AUXILIAR_DEZ;
			int novaEscala = quantidadeCasasDecimais - diferenca;

			numeroRetorno = numero.setScale(novaEscala, RoundingMode.HALF_UP);
		}

		return numeroRetorno;

	}

	/**
	 * Gets the mensagens.
	 *
	 * @return the mensagens
	 */
	public static ResourceBundle getMensagens() {

		return MENSAGENS;
	}

	/**
	 * Construir ordenacao criteria.
	 *
	 * @param criteria      the criteria
	 * @param mapaOrdenacao the mapa ordenacao
	 */
	public static void construirOrdenacaoCriteria(Criteria criteria, Map<String, SortOrderEnum> mapaOrdenacao) {

		for (Map.Entry<String, SortOrderEnum> entry : mapaOrdenacao.entrySet()) {
			String coluna = entry.getKey();
			SortOrderEnum ordenacao = entry.getValue();

			if (ordenacao.equals(SortOrderEnum.ASCENDING)) {

				criteria.addOrder(Order.asc(coluna));
			} else {

				criteria.addOrder(Order.desc(coluna));
			}

		}

	}

	/**
	 * Checks if is wrapper type.
	 *
	 * @param clazz the clazz
	 * @return true, if is wrapper type
	 */
	public static boolean isWrapperType(Class<?> clazz) {

		return WRAPPER_TYPES.contains(clazz);
	}

	/**
	 * Gets the wrapper types.
	 *
	 * @return the wrapper types
	 */
	private static Set<Class<?>> getWrapperTypes() {

		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(Void.class);
		ret.add(String.class);
		ret.add(BigDecimal.class);
		ret.add(Date.class);

		return ret;
	}

	/**
	 * Converter valores.
	 *
	 * @param clazz the clazz
	 * @param valor the valor
	 * @return the object
	 */
	public static Object converterValores(Class<?> clazz, Object valor) {

		Object valorConvertido = null;

		if (clazz == Integer.class) {
			valorConvertido = Integer.valueOf((String) valor);
		}
		if (clazz == Double.class) {
			valorConvertido = Double.valueOf((String) valor);
		}
		if (valorConvertido == null) {
			return valor;
		}

		return valorConvertido;
	}

	/**
	 * Validate.
	 *
	 * @param obj the obj
	 * @return the string
	 */
	public static String validate(Object obj) {

		String violacoes = "";
		Validator validator = FACTORY.getValidator();
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);
		Iterator<ConstraintViolation<Object>> iter = constraintViolations.iterator();
		while (iter.hasNext()) {
			violacoes = violacoes.concat(((ConstraintViolation<?>) iter.next()).getMessage());
			if (iter.hasNext()) {
				violacoes = violacoes.concat(" - ");
			}
		}
		return violacoes;

	}

	/**
	 * Validacao.
	 *
	 * @param obj                 the obj
	 * @param mapaInconsistencias the mapa inconsistencias
	 * @return the string
	 */
	public static String validacao(Object obj, Map<String, Object[]> mapaInconsistencias) {

		String descricaoErro = "";
		Validator validator = FACTORY.getValidator();
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);
		Iterator<ConstraintViolation<Object>> iterator = constraintViolations.iterator();
		ResourceBundle mensagens = ResourceBundle.getBundle("mensagens");
		String descricaoErroAux = "";

		while (iterator.hasNext()) {

			descricaoErroAux = atualizaMapaInconsistencias(mapaInconsistencias, iterator.next().getMessage());
		}

		if (descricaoErroAux != null && !descricaoErroAux.isEmpty()) {

			descricaoErro = gerarMensagemErroConsolidada(mapaInconsistencias, mensagens);
		}
		return descricaoErro;
	}

	/**
	 * Atualiza mapa inconsistencias.
	 *
	 * @param mapaInconsistencias the mapa inconsistencias
	 * @param message             the message
	 * @return the string
	 */
	public static String atualizaMapaInconsistencias(Map<String, Object[]> mapaInconsistencias, String message) {

		String descricaoErroAux;
		String[] mensagem = message.split(",");

		if (!mapaInconsistencias.containsKey(mensagem[0])) {

			Object[] listaAux = new Object[AUXILIAR_DOIS];
			listaAux[0] = 1;
			listaAux[1] = mensagem[1];
			descricaoErroAux = mensagem[1];
			mapaInconsistencias.put(mensagem[0], listaAux);
		} else {

			Object[] listaAux = mapaInconsistencias.get(mensagem[0]);
			listaAux[0] = (Integer) listaAux[0] + 1;

			descricaoErroAux = (String) listaAux[1];

			if (descricaoErroAux != null && !descricaoErroAux.isEmpty()) {

				descricaoErroAux = descricaoErroAux + ", " + mensagem[1];
			} else {

				descricaoErroAux = mensagem[1];
			}

			listaAux[1] = descricaoErroAux;

			mapaInconsistencias.put(mensagem[0], listaAux);

		}
		return descricaoErroAux;
	}

	/**
	 * Gerar mensagem erro consolidada.
	 *
	 * @param mapaInconsistencias the mapa inconsistencias
	 * @param mensagens           the mensagens
	 * @return the string
	 */
	public static String gerarMensagemErroConsolidada(Map<String, Object[]> mapaInconsistencias,
			ResourceBundle mensagens) {

		String descricaoErro = "";
		String mensagemAux = "";
		for (Map.Entry<String, Object[]> entry : mapaInconsistencias.entrySet()) {

			Object[] listaAux = entry.getValue();
			String constante = entry.getKey();

			if (!constante.equals(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA)) {

				if (constante.equals(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS)) {

					mensagemAux = Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS;

				} else if (constante.equals(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS)) {

					mensagemAux = Constantes.ERRO_DADOS_INVALIDOS;
				}

				if (mensagemAux.isEmpty()) {
					mensagemAux = constante;
				}
				String atributos = (String) listaAux[1];

				if (atributos != null && !atributos.isEmpty()) {

					if (!descricaoErro.isEmpty()) {
						descricaoErro = descricaoErro + ", "
								+ MensagemUtil.obterMensagem(mensagens, mensagemAux, atributos);
					} else {
						descricaoErro = MensagemUtil.obterMensagem(mensagens, mensagemAux, atributos);
					}
					listaAux[1] = "";
				}

			}
		}
		return descricaoErro;
	}

	/**
	 * Converte long para integer.
	 *
	 * @param numero the numero
	 * @return the integer
	 */
	public static Integer converteLongParaInteger(Long numero) {

		if (numero != null) {
			return Integer.parseInt(numero.toString());
		} else {
			return 0;
		}
	}

	/**
	 * Converte long para boolean.
	 *
	 * @param numero the numero
	 * @return the boolean
	 */
	public static Boolean converteLongParaBoolean(Long numero) {

		if (numero != null) {
			if (numero == 0) {
				return Boolean.FALSE;
			} else {
				return Boolean.TRUE;
			}
		} else {
			return null;
		}
	}

	/**
	 * Valida um mês/ano informado.
	 *
	 * @param mesAno Um mês ano
	 * @return true caso válido.
	 */
	public static boolean validarMesAno(Integer mesAno) {

		boolean retorno = true;

		if (mesAno.toString().length() != INDICE_SEIS) {
			retorno = false;
		} else {
			int mes = Integer.parseInt(mesAno.toString().substring(0, INDICE_DOIS));
			int ano = Integer.parseInt(mesAno.toString().substring(INDICE_DOIS, INDICE_SEIS));

			if (mes < 1 || mes > AUXILIAR_DOZE || ano <= 0) {
				retorno = false;
			}
		}

		return retorno;
	}

	/**
	 * Concatena mensagem erro.
	 *
	 * @param mensagem the mensagem erro
	 * @param valor    the valor
	 * @return the string
	 */
	public static String concatenaMensagemErro(String mensagem, String valor) {

		String mensagemErro = mensagem;
		if (!mensagemErro.isEmpty()) {
			mensagemErro = mensagemErro.concat(", " + valor);
		} else {
			mensagemErro = mensagemErro.concat(valor);
		}
		return mensagemErro;
	}

	/**
	 * Gets the removes the msg tabela auxiliar.
	 *
	 * @param classe the classe
	 * @return the removes the msg tabela auxiliar
	 */
	public static String getRemoveMsgTabelaAuxiliar(Class<?> classe) {

		String mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";

		if (classe.getName().equals(EntidadeClasseImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(EntidadeConteudoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(ClasseUnidadeImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(UnidadeImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(MarcaCorretorImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(MarcaMedidorImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(ModeloMedidorImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(AtividadeEconomicaImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(MicrorregiaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(UnidadeTipoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(TributoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TributoAliquotaImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PerfilQuadraImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(FeriadoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(UnidadeFederacaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(MunicipioImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TipoSegmentoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TipoClienteImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(SituacaoImovelImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(ClienteSituacaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(TroncoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(CityGateImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PavimentoCalcadaImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PavimentoRuaImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PadraoConstrucaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TipoBotijaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PerfilImovelImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(AreaConstruidaFaixaImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(RedeDiametroImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(ProfissaoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(TipoContatoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TipoRelacionamentoClienteImovelImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(GrupoFaturamentoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(UnidadeNegocioImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(CanalAtendimentoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(ZeisImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(SetorCensitarioImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(ZonaBloqueioImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(AreaTipoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		} else if (classe.getName().equals(TipoFoneImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(TipoEnderecoImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDO";
		} else if (classe.getName().equals(PeriodicidadeImpl.class.getName())) {
			mensagem = "SUCESSO_ENTIDADE_EXCLUIDA";
		}

		return mensagem;
	}

	/**
	 * Concatena emails.
	 *
	 * @param emailsConcatenar the emails
	 * @param valor            the valor
	 * @return the string
	 */
	public static String concatenaEmails(String emailsConcatenar, String valor) {

		String retorno = "";
		String emails = emailsConcatenar;
		if (emails != null && valor != null) {

			if (!emails.isEmpty() && !valor.isEmpty()) {
				emails = emails.concat("; " + valor);
			} else {
				emails = emails.concat(valor);
			}
			retorno = emails;
		} else if (emails == null) {
			retorno = valor;
		} else {
			retorno = emails;
		}

		return retorno;
	}

	/**
	 * Converte a data passada por parametro para uma string no formato HH:MM.
	 *
	 * @param data Data a ser convertida.
	 * @return String com a hora.
	 */
	public static String converterDataParaStringHoraMinutoComCaracteresEspeciais(Date data) {

		StringBuilder retorno = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(calendar.get(Calendar.MINUTE));

		retorno.append(adicionarZeros(hora));
		retorno.append(":");
		retorno.append(adicionarZeros(minuto));

		return retorno.toString();

	}

	/**
	 * Formatar mascara.
	 *
	 * @param mascara  the mascara
	 * @param valorTmp the valor
	 * @return the string
	 */
	/*
	 * Captura uma máscara e deixa o campo de uma consulta pronto para ser inserido
	 */
	public static String formatarMascara(String mascara, String valorTmp) {

		StringBuilder mascaraFormatada = new StringBuilder();

		for (int i = 0; i < mascara.length(); i++) {
			if (mascara.charAt(i) == '.') {
				mascaraFormatada.append(".");
			} else if (mascara.charAt(i) == '-') {
				mascaraFormatada.append("-");
			} else if (mascara.charAt(i) == '*') {
				mascaraFormatada.append("*");
			} else {
				mascaraFormatada.append("#");
			}
		}

		MaskFormatter mk;
		String retorno = null;
		try {
			mk = new MaskFormatter(mascaraFormatada.toString());
			mk.setValueContainsLiteralCharacters(false);
			mk.setPlaceholderCharacter('_');
			String valor = removerVirgulasPontos(valorTmp);
			retorno = mk.valueToString(valor);

		} catch (ParseException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		return retorno;

	}

	/**
	 * Formatar mascara truncada.
	 *
	 * @param mascara the mascara
	 * @param valor   the valor
	 * @return the string
	 */
	public static String formatarMascaraTruncada(String mascara, String valor) {

		StringBuilder mascaraFormatada = new StringBuilder();

		int numeroCaracteresEfetivos = valor.length();
		int contadorCaracteres = 0;
		for (int i = 0; i < mascara.length(); i++) {
			if (mascara.charAt(i) == '.') {
				mascaraFormatada.append(".");
			} else if (mascara.charAt(i) == '-') {
				mascaraFormatada.append("-");
			} else if (mascara.charAt(i) == '*') {
				mascaraFormatada.append("*");
			} else {
				mascaraFormatada.append("#");
				contadorCaracteres++;
				if (contadorCaracteres >= numeroCaracteresEfetivos) {
					break;
				}
			}
		}

		MaskFormatter mk;
		String retorno = null;
		try {
			mk = new MaskFormatter(mascaraFormatada.toString());
			mk.setValueContainsLiteralCharacters(false);
			retorno = mk.valueToString(valor);
			retorno = retorno.concat(valor.substring(contadorCaracteres, valor.length()));

		} catch (ParseException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		if (retorno != null) {
			return retorno;
		} else {
			return valor;
		}

	}

	/**
	 * Obter variaveis.
	 *
	 * @param texto the texto
	 * @return the map
	 */
	public static Map<String, String> obterVariaveis(String texto) {

		Map<String, String> variaveisValor = new HashMap<String, String>();
		if (texto != null && StringUtils.isEmpty(texto)) {
			String[] variaveis = texto.split("#");
			for (int i = 0; i < variaveis.length; i++) {
				if (i % AUXILIAR_DOIS != 0) {
					variaveisValor.put(variaveis[i], "");
				}
			}
		}

		return variaveisValor;
	}

	/**
	 * Salvar relatorio em diretorio.
	 *
	 * @param relatorio
	 * @param nome
	 * @param processo
	 */
	public static void salvarRelatorioEmDiretorio(byte[] relatorio, String nome, Processo processo) {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
				.getControladorProcessoDocumento();

		final String nomeRelatorio;

		nomeRelatorio = nome + Util.converterDataParaStringSemCaracteresEspeciais(Calendar.getInstance().getTime());

		if (relatorio != null) {
			ByteArrayInputStream byteArrayInputStream = null;
			File pdf = null;
			FileOutputStream saida = null;
			String caminhoDiretorio = "";
			try {
				byteArrayInputStream = new ByteArrayInputStream(relatorio);

				String caminhoDiretorioFatura = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.DIRETORIO_ARQUIVOS_FATURA);

				caminhoDiretorio = caminhoDiretorioFatura + "/"
				// +
				// Util.converterDataParaStringSemHoraSemBarra(dataEmissao,
				// Constantes.FORMATO_DATA_US)
						+ nomeRelatorio + "/";
				pdf = getFile(caminhoDiretorio);
				if (!pdf.exists()) {
					pdf.mkdirs();
				}

				final String diretorioDocumento = caminhoDiretorio + nomeRelatorio + Constantes.EXTENSAO_ARQUIVO_PDF;

				pdf = getFile(diretorioDocumento);

				saida = new FileOutputStream(pdf);

				ProcessoDocumento processoDocumento = controladorProcessoDocumento
						.criaEPopulaProcessoDocumento(processo, diretorioDocumento, nomeRelatorio);
				controladorProcessoDocumento.inserirProcessoDocumento(processoDocumento);

				int data;
				while ((data = byteArrayInputStream.read()) != -1) {
					char ch = (char) data;
					saida.write(ch);
				}
				saida.flush();
				saida.close();
			} catch (Exception e) {
				throw new InfraestruturaException(e.getMessage(), e);
			}
		}

	}

	/**
	 * Enviar email rotas sem cronogramas.
	 *
	 * @param processo the processo
	 * @throws GGASException the GGAS exception
	 */
	public static void enviarEmailRotasSemCronogramas(Processo processo) throws GGASException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		Collection<Rota> listaRotas = new ArrayList<Rota>();
		Collection<GrupoFaturamento> listaGrupoFaturamento = controladorRota.listarGruposFaturamentoRotas();
		// obtendo a data de hoje
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date dataHoje = new Date();
		dateFormat.format(dataHoje);

		for (GrupoFaturamento grupoFaturamento : listaGrupoFaturamento) {
			Collection<Rota> listaRotaPorGrupoFaturamento = controladorRota.listarRotasGeracaoDadosLeituraSemCronograma(
					grupoFaturamento.getChavePrimaria(), grupoFaturamento.getPeriodicidade().getChavePrimaria(), null);
			for (Rota rotaPorGrupoFaturamento : listaRotaPorGrupoFaturamento) {
				listaRotas.add(rotaPorGrupoFaturamento);
			}
		}

		String subject = "Rotas Sem Cronograma";
		StringBuilder conteudoEmail = new StringBuilder();
		conteudoEmail.append("Data do processamento: ").append(Util.converterDataHoraParaString(dataHoje))
				.append("\r\n").append("Grupo de faturamento \t Nº de rota(s) \r\n")
				.append("----------------------------------------------------------------------------------------------\r\n");

		long grupoFaturmanentoAnterior = 0L;
		for (Rota rota : listaRotas) {

			if (rota.getGrupoFaturamento().getChavePrimaria() != grupoFaturmanentoAnterior) {
				conteudoEmail.append("\r\n").append(rota.getGrupoFaturamento().getDescricao()).append("\t");
				conteudoEmail.append(rota.getNumeroRota());
			} else {
				conteudoEmail.append(", ").append(rota.getNumeroRota());
			}

			grupoFaturmanentoAnterior = rota.getGrupoFaturamento().getChavePrimaria();
		}

		JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia()
				.getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
		AtividadeSistema atividadeSistema = controladorProcesso
				.obterAtividadeSistemaPorChaveOperacao(processo.getOperacao().getChavePrimaria());

		if (processo.getEmailResponsavel() != null) {
			javaMailUtil.enviar(atividadeSistema.getDescricaoEmailRemetente(), processo.getEmailResponsavel(), subject,
					conteudoEmail.toString());
		}

	}

	/**
	 * Converter string para coordenada geografica.
	 *
	 * @param texto the texto
	 * @return the big decimal
	 */
	public static BigDecimal converterStringParaCoordenadaGeografica(String texto) {

		String novoTexto = texto.replace(",", ".");

		return new BigDecimal(novoTexto);
	}

	/**
	 * Atualiza lista menu.
	 *
	 * @param modulos       the modulos
	 * @param favoritosList the favoritos list
	 */
	public static void atualizaListaMenu(Collection<Menu> modulos, Collection<Favoritos> favoritosList) {

		List<Long> lista = populaLista(favoritosList);
		for (Menu menu : modulos) {
			for (Menu menuFilho : menu.getListaMenu()) {
				if (lista.contains(menuFilho.getChavePrimaria())) {
					menuFilho.setFavorito(true);
				} else {
					menuFilho.setFavorito(false);
				}
				for (Menu menuNeto : menuFilho.getListaMenu()) {
					if (lista.contains(menuNeto.getChavePrimaria())) {
						menuNeto.setFavorito(true);
					} else {
						menuNeto.setFavorito(false);
					}
				}
			}
		}
	}

	/**
	 * Popula lista.
	 *
	 * @param favoritos the favoritos
	 * @return the list
	 */
	public static List<Long> populaLista(Collection<Favoritos> favoritos) {

		List<Long> lista = new ArrayList<Long>();
		for (Favoritos favorito : favoritos) {
			lista.add(favorito.getMenu().getChavePrimaria());
		}
		return lista;
	}

	/**
	 * Ltrim.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String ltrim(String s) {

		int i = 0;
		while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
			i++;
		}
		return s.substring(i);
	}

	/**
	 * Rtrim.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String rtrim(String s) {

		int i = s.length() - 1;
		while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
			i--;
		}
		return s.substring(0, i + 1);
	}

	/**
	 * Validar email.
	 *
	 * @param email the email
	 * @throws NegocioException the negocio exception
	 */
	public static void validarEmail(String email) throws NegocioException {

		if (!Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
		}
		if (!Util.validarDominio(email, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
		}
	}

	/**
	 * Validar email composto.
	 *
	 * @param emailComposto the email composto
	 * @throws NegocioException the negocio exception
	 */
	public static void validarEmailComposto(String emailComposto) throws NegocioException {

		String[] emails = emailComposto.split(Constantes.SEPARADOR_EMAIL);
		for (String email : emails) {
			if (!Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
			}
			if (!Util.validarDominio(email, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
			}
		}
	}

	/**
	 * Data atual por extenso.
	 *
	 * @return the string
	 */
	public static String dataAtualPorExtenso() {

		Date data = Calendar.getInstance().getTime();
		data = zerarHorario(new DateTime()).toDate();

		StringBuilder dataFormatadaParametro = new StringBuilder();
		dataFormatadaParametro.append(new DateTime(data).getDayOfMonth());
		dataFormatadaParametro.append(" de ")
				.append(Mes.MESES_ANO.get(Integer.valueOf(new DateTime(data).getMonthOfYear())));
		dataFormatadaParametro.append(" de ").append(new DateTime(data).getYear());

		return dataFormatadaParametro.toString();
	}

	/**
	 * Array long para array string.
	 *
	 * @param array the array
	 * @return the string[]
	 */
	public static String[] arrayLongParaArrayString(Long[] array) {

		String[] novoArray = new String[array.length];
		for (int i = 0; i < array.length; i++) {
			novoArray[i] = array[i].toString();
		}
		return novoArray;
	}

	/**
	 * Validar mes ano.
	 *
	 * @param mesAno the mes ano
	 * @return true, if successful
	 */
	public static boolean validarMesAno(String mesAno) {

		boolean retorno = true;

		if (mesAno.length() != AUXILIAR_SEIS) {
			retorno = false;
		} else {
			int mes = Integer.parseInt(mesAno.substring(0, INDICE_DOIS));
			int ano = Integer.parseInt(mesAno.substring(INDICE_DOIS, INDICE_SEIS));

			if (mes < 1 || mes > AUXILIAR_DOZE || ano <= 1900) {
				retorno = false;
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por copiar todos os atributos de um Objeto origem o Objeto
	 * destino.
	 *
	 * @param origem  the origem
	 * @param destino the destino
	 * @return origem
	 * @throws IllegalAccessException    the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public static Object copyProperties(Object destino, Object origem) {

		try {
			org.springframework.beans.BeanUtils.copyProperties(origem, destino);
		} catch (Exception e) {
			LOG.debug(e.getMessage(), e);
			tratarCopyProperties(origem, destino);
		}
		return destino;
	}

	private static void tratarCopyProperties(Object origem, Object destino) {

		final BeanWrapper src = new BeanWrapperImpl(origem);
		PropertyDescriptor[] pds = src.getPropertyDescriptors();

		for (PropertyDescriptor pd : pds) {
			Object value = src.getPropertyValue(pd.getName());
			try {
				BeanUtils.copyProperty(destino, pd.getName(), value);
			} catch (Exception e) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	/**
	 * Adicionar dias data.
	 *
	 * @param data the data
	 * @param dias the dias
	 * @return the date
	 */
	public static Date adicionarDiasData(Date data, int dias) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.DAY_OF_MONTH, dias);

		return calendar.getTime();
	}

	/**
	 * Removes the menu sem permissao sistema.
	 *
	 * @param menuPai         the menu pai
	 * @param idPapeisUsuario the id papeis usuario
	 */
	public static void removeMenuSemPermissaoSistema(Menu menuPai, Long[] idPapeisUsuario) {

		if (!menuPai.getListaMenu().isEmpty()) {
			for (Menu menu : menuPai.getListaMenu()) {
				if (menu.getListaMenu().isEmpty()) {

					if (permissaoExisteEmUsuario(menu.getListaPermissao(), idPapeisUsuario)) {
						menu.setIndicadorMostrarMenu(true);
					} else {
						menu.setIndicadorMostrarMenu(false);
					}

				} else {
					removeMenuSemPermissaoSistema(menu, idPapeisUsuario);
				}
			}
		}
	}

	/**
	 * Permissao existe em usuario.
	 *
	 * @param permissaoLista  the permissao lista
	 * @param idPapeisUsuario the id papeis usuario
	 * @return true, if successful
	 */
	public static boolean permissaoExisteEmUsuario(Collection<Permissao> permissaoLista, Long[] idPapeisUsuario) {

		for (Permissao permissao : permissaoLista) {
			if (papelExisteEmPermissao(idPapeisUsuario, permissao)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Papel existe em permissao.
	 *
	 * @param idPapeisUsuario the id papeis usuario
	 * @param permissao       the permissao
	 * @return true, if successful
	 */
	public static boolean papelExisteEmPermissao(Long[] idPapeisUsuario, Permissao permissao) {

		for (Long idPapel : idPapeisUsuario) {
			if (permissao.getPapel().getChavePrimaria() == (idPapel)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Retorna extensão, a ser exibina no download, relativa ao formato de impressão
	 * informado.
	 *
	 * @param formatoImpressao Formato de impressão informado
	 * @return extensão
	 */
	public static String obterFormatoRelatorio(FormatoImpressao formatoImpressao) {

		String retorno = null;

		if (formatoImpressao.equals(FormatoImpressao.PDF)) {
			retorno = ".pdf";
		} else if (formatoImpressao.equals(FormatoImpressao.XLS)) {
			retorno = ".xls";
		} else if (formatoImpressao.equals(FormatoImpressao.RTF)) {
			retorno = ".rtf";
		}

		return retorno;
	}

	/**
	 * Obter formato impressao.
	 *
	 * @param formato the formato
	 * @return the formato impressao
	 */
	public static FormatoImpressao obterFormatoImpressao(String formato) {

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		if (formato.equalsIgnoreCase(FormatoImpressao.XLS.toString())) {
			formatoImpressao = FormatoImpressao.XLS;
		} else {
			if (formato.equalsIgnoreCase(FormatoImpressao.RTF.toString())) {
				formatoImpressao = FormatoImpressao.RTF;
			}
		}
		return formatoImpressao;
	}

	/**
	 * Retorna o valor do horário minimo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo será "30/01/2009 as 00h:00m:00s e 000ms".
	 *
	 * @param date de referencia.
	 * @return {@link Date} que representa o horário minimo para dia informado.
	 */
	public static Date lowDateTime(Date date) {

		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux);
		// zera os parametros de hour,min,sec,milisec
		return aux.getTime();
	}

	/**
	 * Retorna o valor do horário maximo para a data de referencia passada. <BR>
	 *
	 * @param date de referencia.
	 * @return {@link Date} que representa o horário maximo para dia informado.
	 */
	public static Date highDateTime(Date date) {

		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux);
		// zera os parametros de hour,min,sec,milisec
		aux.roll(Calendar.HOUR_OF_DAY, false);
		// vai para o dia seguinte
		aux.roll(Calendar.MINUTE, false);
		aux.roll(Calendar.SECOND, false);
		// reduz 1 milisegundo
		return aux.getTime();
	}

	/**
	 * Zera todas as referencias de hora, minuto, segundo e milesegundo do
	 * {@link Calendar}.
	 *
	 * @param date a ser modificado.
	 */
	public static void toOnlyDate(Calendar date) {

		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Carregar meses.
	 *
	 * @return the map
	 */
	public static Map<Integer, String> carregarMeses() {

		Map<Integer, String> mapMeses = new HashMap<Integer, String>();
		mapMeses.put(Mes.JANEIRO, "Janeiro");
		mapMeses.put(Mes.FEVEREIRO, "Fevereiro");
		mapMeses.put(Mes.MARCO, "Março");
		mapMeses.put(Mes.ABRIL, "Abril");
		mapMeses.put(Mes.MAIO, "Maio");
		mapMeses.put(Mes.JUNHO, "Junho");
		mapMeses.put(Mes.JULHO, "Julho");
		mapMeses.put(Mes.AGOSTO, "Agosto");
		mapMeses.put(Mes.SETEMBRO, "Setembro");
		mapMeses.put(Mes.OUTUBRO, "Outubro");
		mapMeses.put(Mes.NOVEMBRO, "Novembro");
		mapMeses.put(Mes.DEZEMBRO, "Dezembro");
		return mapMeses;
	}

	/**
	 * Carregar anos.
	 *
	 * @return the list
	 */
	public static List<Integer> carregarAnos() {

		List<Integer> anos = new ArrayList<Integer>();

		int ano = Calendar.getInstance().get(Calendar.YEAR);

		anos.add(ano);
		anos.add(ano + 1);

		return anos;
	}

	/**
	 * Registro nao encontrato.
	 *
	 * @throws NegocioException the negocio exception
	 */
	public static void registroNaoEncontrato() throws NegocioException {

		throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, true);
	}

	/**
	 * Validar imagem do arquivo.
	 *
	 * @param multipartFile the multipart file
	 * @param extensao      the extensao
	 * @param tamanhoTmp    the tamanho
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public static boolean validarImagemDoArquivo(MultipartFile multipartFile, String[] extensao, int tamanhoTmp)
			throws NegocioException {

		boolean retorno = false;
		int tamanho = tamanhoTmp;
		byte[] arquivo = null;
		try {
			arquivo = multipartFile.getBytes();
		} catch (FileNotFoundException e) {
			throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO, e);
		} catch (IOException e) {
			throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_ARQUIVO, e);
		}

		if (arquivo.length != 0) {
			boolean isInvalido = true;
			if (extensao != null && extensao.length > 0) {
				for (String ext : extensao) {
					if (multipartFile.getOriginalFilename().toUpperCase().endsWith(ext)) {
						isInvalido = false;
						break;
					}
				}
			}
			if (isInvalido) {
				StringBuilder tiposArquivos = new StringBuilder();
				for (String tipo : extensao) {
					tiposArquivos.append(tipo.toLowerCase());
					tiposArquivos.append(" ");
				}
				throw new NegocioException(Constantes.ERRO_ARQUIVO_EXTENSAO_INVALIDA, tiposArquivos.toString());
			}

			if (arquivo.length <= tamanho) {
				retorno = true;
			} else {
				tamanho = tamanho / CONVERTER_BYTE;
				throw new NegocioException(Constantes.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO, tamanho);
			}

		}
		return retorno;
	}

	/**
	 * Remover dias data.
	 *
	 * @param data the data
	 * @param dias the dias
	 * @return the date
	 */
	public static Date removerDiasData(Date data, int dias) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.DAY_OF_MONTH, -dias);

		return calendar.getTime();
	}

	/**
	 * Remover ano data.
	 *
	 * @param data the data
	 * @param ano  the ano
	 * @return the date
	 */
	public static Date removerAnoData(Date data, int ano) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.YEAR, -ano);
		return calendar.getTime();

	}

	/**
	 * Obter periodo trimestral.
	 *
	 * @param data the data
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Date> obterPeriodoTrimestral(Date data) throws GGASException {

		Map<String, Date> params = new HashMap<String, Date>();
		StringBuilder retornoInicio = new StringBuilder();
		StringBuilder retornoFinal = new StringBuilder();

		Date dataInicio;
		Date dataFinal;

		String dataInicioString;
		String dataFinalString;

		String diaInicio;
		String anoInicio;

		String diaFinal;
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);

		int month = cal.get(Calendar.MONTH);
		anoInicio = String.valueOf(cal.get(Calendar.YEAR));

		if (month >= Calendar.JANUARY && month <= Calendar.MARCH) {

			diaInicio = "01/01";
			diaFinal = "31/03";

			retornoInicio.append(diaInicio);
			retornoInicio.append("/");
			retornoInicio.append(anoInicio);

			retornoFinal.append(diaFinal);
			retornoFinal.append("/");
			retornoFinal.append(anoInicio);

			dataInicioString = retornoInicio.toString();
			dataFinalString = retornoFinal.toString();
			dataInicio = Util.converterCampoStringParaData("Data Inicial", dataInicioString,
					Constantes.FORMATO_DATA_BR);
			dataFinal = Util.converterCampoStringParaData("Data Inicial", dataFinalString, Constantes.FORMATO_DATA_BR);

			params.put("dataInicioTrimestre", dataInicio);
			params.put("datafinalTrimestre", dataFinal);

		} else if (month >= Calendar.APRIL && month <= Calendar.JUNE) {
			diaInicio = "01/04";
			diaFinal = "30/06";

			retornoInicio.append(diaInicio);
			retornoInicio.append("/");
			retornoInicio.append(anoInicio);

			retornoFinal.append(diaFinal);
			retornoFinal.append("/");
			retornoFinal.append(anoInicio);

			dataInicioString = retornoInicio.toString();
			dataFinalString = retornoFinal.toString();
			dataInicio = Util.converterCampoStringParaData("Data Inicial", dataInicioString,
					Constantes.FORMATO_DATA_BR);
			dataFinal = Util.converterCampoStringParaData("Data Inicial", dataFinalString, Constantes.FORMATO_DATA_BR);

			params.put("dataInicioTrimestre", dataInicio);
			params.put("datafinalTrimestre", dataFinal);

		} else if (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) {
			diaInicio = "01/07";
			diaFinal = "30/09";

			retornoInicio.append(diaInicio);
			retornoInicio.append("/");
			retornoInicio.append(anoInicio);

			retornoFinal.append(diaFinal);
			retornoFinal.append("/");
			retornoFinal.append(anoInicio);

			dataInicioString = retornoInicio.toString();
			dataFinalString = retornoFinal.toString();
			dataInicio = Util.converterCampoStringParaData("Data Inicial", dataInicioString,
					Constantes.FORMATO_DATA_BR);
			dataFinal = Util.converterCampoStringParaData("Data Inicial", dataFinalString, Constantes.FORMATO_DATA_BR);

			params.put("dataInicioTrimestre", dataInicio);
			params.put("datafinalTrimestre", dataFinal);

		} else if (month >= Calendar.OCTOBER && month <= Calendar.DECEMBER) {
			diaInicio = "01/10";
			diaFinal = "31/12";

			retornoInicio.append(diaInicio);
			retornoInicio.append("/");
			retornoInicio.append(anoInicio);

			retornoFinal.append(diaFinal);
			retornoFinal.append("/");
			retornoFinal.append(anoInicio);

			dataInicioString = retornoInicio.toString();
			dataFinalString = retornoFinal.toString();
			dataInicio = Util.converterCampoStringParaData("Data Inicial", dataInicioString,
					Constantes.FORMATO_DATA_BR);
			dataFinal = Util.converterCampoStringParaData("Data Inicial", dataFinalString, Constantes.FORMATO_DATA_BR);

			params.put("dataInicioTrimestre", dataInicio);
			params.put("datafinalTrimestre", dataFinal);
		}

		return params;

	}

	/**
	 * Gerar hash m d5 codigo fisco.
	 *
	 * @param valorContatenado the valor contatenado
	 * @return the string
	 */
	public static String gerarHashMD5CodigoFisco(String valorContatenado) {

		String codigoFisco = "";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5"); // NOSONAR {As senhas são armazenadas em hash MD5}
			md.update(valorContatenado.getBytes());
		} catch (NoSuchAlgorithmException e) {
			LOG.error(e.getMessage(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		if (md != null) {

			byte[] byteData = md.digest();

			// convert the byte to hex format method 1
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			codigoFisco = sb.toString();
		}

		return codigoFisco;

	}

	/**
	 * UT f8to iso.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String utf8ToISO(String str) {

		Charset utf8charset = Charset.forName("UTF-8");
		Charset iso88591charset = Charset.forName("ISO-8859-1");

		return converterString(str, utf8charset, iso88591charset);
	}

	/**
	 * Converte uma string no padrão ISO-8859-1 para UTF-8
	 * 
	 * @param str string a ser convertida
	 * @return string convertida
	 */
	public static String isoToUtf8(String str) {

		Charset utf8charset = Charset.forName("UTF-8");
		Charset iso88591charset = Charset.forName("ISO-8859-1");

		return converterString(str, iso88591charset, utf8charset);
	}

	private static String converterString(String str, Charset codificacaoOriginal, Charset codificacaoDestino) {
		ByteBuffer inputBuffer = ByteBuffer.wrap(str.getBytes());
		CharBuffer data = codificacaoOriginal.decode(inputBuffer);
		ByteBuffer outputBuffer = codificacaoDestino.encode(data);
		byte[] outputData = outputBuffer.array();

		return new String(outputData);
	}

	/**
	 * Recuperar chaves primarias.
	 *
	 * @param entidades the entidades
	 * @return the list
	 */
	public static List<Long> recuperarChavesPrimarias(Collection<?> entidades) {

		List<Long> novoArray = new ArrayList<Long>();
		Object entidade = null;
		for (Iterator<?> iterator = entidades.iterator(); iterator.hasNext();) {
			entidade = iterator.next();
			if (entidade instanceof EntidadeNegocio) {
				novoArray.add(Long.valueOf(((EntidadeNegocio) entidade).getChavePrimaria()));
			}
		}

		return novoArray;
	}

	/**
	 * Converter campo data para string com dia da semana.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String converterCampoDataParaStringComDiaDaSemana(Date data) {

		String strData = null;
		String aux = Util.converterDataParaStringSemHora(data, Constantes.FORMATO_DATA_BR);

		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		int day = cal.get(Calendar.DAY_OF_WEEK);

		switch (day) {
		case 1:
			strData = "Dom " + aux;
			break;

		case 2:
			strData = "Seg " + aux;
			break;

		case 3:
			strData = "Ter " + aux;
			break;

		case 4:
			strData = "Qua " + aux;
			break;

		case 5:
			strData = "Qui " + aux;
			break;

		case 6:
			strData = "Sex " + aux;
			break;

		default:
			strData = "Sab " + aux;
			break;
		}

		return strData;
	}

	/**
	 * Validar inscricao estadual ba.
	 *
	 * @param inscricaoBA the inscricao
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static void validarInscricaoEstadualBA(String inscricaoBA) throws FormatoInvalidoException {
		String inscricao = inscricaoBA;

		if ((inscricao == null) || (inscricao.length() != AUXILIAR_OITO && inscricao.length() != AUXILIAR_NOVE)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

		// variáveis de instancia
		String aux = removerCaracteresEspeciais(inscricao);

		if (aux.length() == AUXILIAR_OITO) {
			inscricao = aux.substring(0, INDICE_SEIS);
		} else {

			inscricao = aux.substring(0, INDICE_SETE);
		}
		String result = inscricao;
		int soma = 0;
		int resto = 0;
		Integer digito2 = 0;
		Integer digito1 = 0;
		String[] numeros = new String[inscricao.length() + 1];
		int multiplicador = AUXILIAR_DOIS;

		for (int i = inscricao.length(); i > 0; i--) {

			numeros[i] = String.valueOf(Integer.parseInt(inscricao.substring(i - 1, i)) * multiplicador);
			soma += Integer.parseInt(inscricao.substring(i - 1, i)) * multiplicador;
			multiplicador++;

		}
		int divisor;
		if (Arrays.asList(INSCRICAO_ESTADUAL_BA_SEGUNDO_DIGITO).contains(inscricao.charAt(1))) {
			divisor = Constantes.DIVISOR_MODULO11;
		} else {
			divisor = Constantes.DIVISOR_MODULO10;
		}

		resto = soma % divisor;
		if (divisor == Constantes.DIVISOR_MODULO11) {
			if (resto == 0 || resto == 1) {
				digito2 = 0;
			} else {
				digito2 = divisor - resto;
			}
		} else {
			if (!(resto == 0)) {

				digito2 = divisor - resto;
			} else {
				digito2 = 0;
			}
		}

		inscricao = inscricao + digito2.toString();
		multiplicador = AUXILIAR_DOIS;
		soma = 0;

		String[] numeros2 = new String[inscricao.length() + 1];

		for (int i = inscricao.length(); i > 0; i--) {

			numeros2[i] = String.valueOf(Integer.parseInt(inscricao.substring(i - 1, i)) * multiplicador);
			soma += Integer.parseInt(inscricao.substring(i - 1, i)) * multiplicador;
			multiplicador++;

		}

		resto = soma % divisor;
		if (divisor == Constantes.DIVISOR_MODULO11) {
			if (resto == 0 || resto == 1) {
				digito1 = 0;
			} else {
				digito1 = divisor - resto;
			}
		} else {
			if (!(resto == 0)) {

				digito1 = divisor - resto;
			} else {
				digito1 = 0;
			}
		}

		result = result + digito1.toString() + digito2.toString();
		if (!result.equals(aux)) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}

	}

	/**
	 * Converte Kgf/cm² para Pa.
	 *
	 * @param valor the valor
	 * @return the double
	 */
	public static Double converterQuilogramaCentimetroQuadradoParaPascal(double valor) {

		return valor * 98066.5;
	}

	/**
	 * Converte °C para K.
	 *
	 * @param valor the valor
	 * @return the double
	 */
	public static Double converterCelciusParaKelvin(double valor) {

		return valor + 273.15;
	}

	/**
	 * Atualizat valor igpm.
	 *
	 * @param dataInicio          the data inicio
	 * @param valorAserAtualizado the valor aser atualizado
	 * @param descricaoAbrevida   the descricao abrevida
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public static BigDecimal atualizatValorIGPM(Date dataInicio, BigDecimal valorAserAtualizado,
			String descricaoAbrevida) throws NegocioException {

		Collection<IndiceFinanceiroValorNominal> listaIndice = Fachada.getInstancia()
				.listaValorNominalIndiceFinaceiro(dataInicio, new Date(), descricaoAbrevida);
		BigDecimal valorReajuste = null;
		BigDecimal total = valorAserAtualizado;

		for (IndiceFinanceiroValorNominal indiceValor : listaIndice) {
			valorReajuste = total.multiply(indiceValor.getValorNominal()).add(total);
			total = valorReajuste;
		}

		return total;
	}

	/**
	 * Retorna uma lista segura para iteracao
	 *
	 * @param iterable
	 * @return iterable se a lista não for nula, caso contrário lista vazia
	 */
	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
		if (iterable == null) {
			return Collections.<T>emptyList();
		} else {
			return iterable;
		}
	}

	/**
	 * Verifica se a colecao é nula ou vazia.
	 *
	 * @param <T>        the generic type
	 * @param collection the collection
	 * @return true se colecao é nula ou vazia e false caso contrário
	 */
	public static <T> boolean isNullOrEmpty(Collection<T> collection) {

		return collection == null || collection.isEmpty() || "[null]".equals(collection.toString());
	}

	/**
	 * Checks if is all are not null.
	 *
	 * @param objs the objs
	 * @return true, if is all not null
	 */
	public static boolean isAllNotNull(Object... objs) {
		for (Object object : objs) {
			if (object == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if there is a parameter null.
	 *
	 * @param objs the objs
	 * @return true, if successful
	 */
	public static boolean hasNull(Object... objs) {
		for (Object obj : objs) {
			if (obj == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica se um numero é nulo ou zero.
	 *
	 * @param valor valor a ser testado
	 * @return true se numero é nulo ou zero e false caso contrário
	 */
	public static boolean isEmpty(Long valor) {

		return valor == null || valor == 0;
	}

	/**
	 * retorna string no formato Ano/Mês Referência: {ano}/{mes} Ciclo:
	 * {numeroCiclo} caso {numeroCiclo} seja nulo o metodo retorna Ano/Mês
	 * Referência: {ano}/{mes} geralmente utilizado em relatórios.
	 *
	 * @param anoMes      ano e mes
	 * @param numeroCiclo numero do ciclo
	 * @return string formatada com ano mes e numero do ciclo
	 */
	public static String converterAnoMesNumeroCicloParaString(Integer anoMes, Integer numeroCiclo) {

		StringBuilder buffer = new StringBuilder();
		buffer.append(ANO_MES_REFERENCIA_ROTULO);
		buffer.append(SEPARADOR_ROTULO_VALOR);
		buffer.append(anoMes.toString().substring(ANO_MES_VALOR_STRING_ANO_INDICE_INICIAL,
				ANO_MES_VALOR_STRING_MES_INDICE_INICIAL));
		buffer.append(SEPARADOR_ANO_MES);
		buffer.append(anoMes.toString().substring(ANO_MES_VALOR_STRING_MES_INDICE_INICIAL));
		if (numeroCiclo != null) {
			buffer.append(SEPARADOR_PALAVRA);
			buffer.append(CICLO_ROTULO);
			buffer.append(SEPARADOR_ROTULO_VALOR);
			buffer.append(numeroCiclo);
		}
		return buffer.toString();
	}

	/**
	 * Simula o operador nulo coalescente. Caso o primeiro argumento não seja nulo o
	 * metodo retorna ele mesmo, caso contrário, o segundo argumento sera retornado
	 *
	 * @param <T>           the generic type
	 * @param valorTeste    argumento a ser testado
	 * @param valorCasoNulo the valor caso nulo
	 * @return valorTeste, caso nao seja nulo; valorCasoNulo, caso contrario
	 */
	public static <T> T coalescenciaNula(T valorTeste, T valorCasoNulo) {

		if (valorTeste != null) {
			return valorTeste;
		} else {
			return valorCasoNulo;
		}
	}

	/**
	 * Diferenca dias entre datas.
	 *
	 * @param data1 the data1
	 * @param data2 the data2
	 * @return int dias
	 * @deprecated utilizar metodo homonimo de DataUtil Calcula diferença entre dias
	 */
	@Deprecated
	public static Integer diferencaDiasEntreDatas(Date data1, Date data2) {

		if (data1 != null && data2 != null) {
			return Days.daysBetween(new DateTime(data1), new DateTime(data2)).getDays();
		} else if (data1 == null) {
			throw new IllegalArgumentException("Data1 Inválida!");
		} else {
			throw new IllegalArgumentException("Data2 Inválida!");
		}
	}

	/**
	 * Este método se difere do método converterCampoValorDecimalParaString, pois
	 * neste método é possível espicificar a quantidade de casas decimais.
	 *
	 * @param rotulo              the rotulo
	 * @param valor               the valor
	 * @param locale              the locale
	 * @param numeroCasasDecimais the numero casas decimais
	 * @return String
	 */

	public static String converterCampoValorDecimalParaStringComCasasDecimais(String rotulo, BigDecimal valor,
			Locale locale, Integer numeroCasasDecimais) {

		DecimalFormat decimalFormat = obterDecimalFormatLocalizado(locale, numeroCasasDecimais);
		return decimalFormat.format(valor);
	}

	/**
	 * Primeiro elemento.
	 *
	 * @param <T>     the generic type
	 * @param colecao the colecao
	 * @return the t
	 */
	public static <T> T primeiroElemento(Collection<T> colecao) {

		if (CollectionUtils.isNotEmpty(colecao)) {
			return (T) CollectionUtils.get(colecao, 0);
		}
		return null;
	}

	/**
	 * Ultimo elemento.
	 *
	 * @param <T>     the generic type
	 * @param colecao the colecao
	 * @return the t
	 */
	public static <T> T ultimoElemento(Collection<T> colecao) {

		if (CollectionUtils.isNotEmpty(colecao)) {
			return (T) CollectionUtils.get(colecao, colecao.size() - 1);
		}
		return null;
	}

	/**
	 * Obtém o primeiro elemento de uma coleção, em uma mapa de chave K e coleção de
	 * elementos T.
	 *
	 * @param mapa  - {@link Map}
	 * @param chave - K
	 * @return elemento - T
	 */
	public static <K, T> T getElemento(Map<K, Collection<T>> mapa, K chave) {
		T elemento = null;
		if (mapa != null && !mapa.isEmpty()) {
			Collection<T> elementos = mapa.get(chave);
			if (!isNullOrEmpty(elementos)) {
				Iterator<T> iterator = elementos.iterator();
				elemento = iterator.next();
			}
		}
		return elemento;
	}

	/**
	 * Obtém dos objetos de uma coleção, o valor dos campos de nome especificado no
	 * parâmetro.
	 *
	 * @param colecao   - {@link Collection}
	 * @param nomeCampo - {@link String}
	 * @return valor dos campos - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public static <T, E> Collection<T> getCamposColecao(Collection<E> colecao, String nomeCampo)
			throws NegocioException {
		Collection<T> valores = new HashSet<>();

		if (!isNullOrEmpty(colecao)) {
			E primeiroElemento = primeiroElemento(colecao);
			Field campo = getCampo(nomeCampo, primeiroElemento.getClass());
			if (campo == null) {
				throw new IllegalStateException("Campo não identificado na classe.");
			}
			for (E elemento : colecao) {
				try {
					valores.add((T) campo.get(elemento));
				} catch (Exception e) {
					throw new NegocioException(e.getMessage(), e);
				}
			}
		}
		return valores;
	}

	/**
	 * Obtém um campo a partir do seu nome na classe especificada no parâmetro.
	 *
	 * @param nomeCampo - {@link String}
	 * @param classe    - {@link Class}
	 * @return campo - {@link Field}
	 */
	public static <T> Field getCampo(String nomeCampo, Class<T> classe) {

		Field campo = null;
		try {
			campo = classe.getDeclaredField(nomeCampo);
			campo.setAccessible(Boolean.TRUE);
		} catch (NoSuchFieldException e) {
			LOG.debug(e.getMessage(), e);
			if (classe.getSuperclass() != null) {
				campo = getCampo(nomeCampo, classe.getSuperclass());
			}
		}
		return campo;
	}

	/**
	 * Transforma lista de entidade negocio em hash map indexado por hash string
	 *
	 * @param lista lista de entidade negocio
	 * @return hash map indexado por hash string
	 */
	public static <T extends EntidadeNegocio> Map<String, T> trasformarParaMapaPorHashString(Collection<T> lista) {

		Map<String, T> mapa = new HashMap<String, T>();

		for (T elemento : lista) {
			String stringDataReferencia = elemento.hashString();
			mapa.put(stringDataReferencia, elemento);
		}

		return mapa;
	}

	/**
	 * Transforma lista de entidade negocio em hash map indexado por chave primaria
	 *
	 * @param <K>
	 * @param lista lista de entidade negocio
	 * @return hash map indexado por chave primaria
	 */
	public static <K, V extends Mapeavel<K>> Map<K, V> trasformarParaMapa(Collection<V> lista) {

		Map<K, V> mapa = new HashMap<K, V>();

		for (V elemento : lista) {
			K chavePrimaria = elemento.getChaveMapeadora();
			mapa.put(chavePrimaria, elemento);
		}

		return mapa;
	}

	/**
	 * Mapear.
	 *
	 * @param lista the lista
	 * @return the map
	 */
	public static Map<Long, EntidadeNegocio> mapear(Collection<EntidadeNegocio> lista) {

		Map<Long, EntidadeNegocio> mapa = new HashMap<Long, EntidadeNegocio>();

		for (EntidadeNegocio elemento : lista) {

			Long chaveAgrupadora = elemento.getChavePrimaria();
			mapa.put(chaveAgrupadora, elemento);

		}

		return mapa;
	}

	/**
	 * Mapear.
	 *
	 * @param <K>      the key type
	 * @param <T>      the generic type
	 * @param lista    the lista
	 * @param mapeador the mapeador
	 * @return the map
	 */
	public static <K, T> Map<K, T> mapear(Collection<T> lista, Mapeador<K, T> mapeador) {

		Map<K, T> mapa = new HashMap<K, T>();

		for (T elemento : lista) {

			K chaveAgrupadora = mapeador.getChaveMapeadora(elemento);
			mapa.put(chaveAgrupadora, elemento);

		}

		return mapa;
	}

	/**
	 * Agrupar.
	 *
	 * @param <K>       the key type
	 * @param <T>       the generic type
	 * @param lista     the lista
	 * @param agrupador the agrupador
	 * @return the map
	 */
	public static <K, T> Map<K, Collection<T>> agrupar(Collection<T> lista, Agrupador<T, K> agrupador) {

		Map<K, Collection<T>> agrupamento = new HashMap<K, Collection<T>>();

		for (T elemento : lista) {

			K chaveAgrupadora = agrupador.getChaveAgrupadora(elemento);
			adicionarElementoMapaCodigoLista(agrupamento, chaveAgrupadora, elemento);

		}

		return agrupamento;
	}

	/**
	 * Adicionar elemento mapa codigo lista.
	 *
	 * @param <K>      the key type
	 * @param <T>      the generic type
	 * @param mapa     the mapa
	 * @param chave    the chave
	 * @param elemento the elemento
	 */
	public static <K, T> void adicionarElementoMapaCodigoLista(Map<K, Collection<T>> mapa, K chave, T elemento) {

		Collection<T> lista = mapa.get(chave);

		if (lista == null) {
			lista = new ArrayList<T>();
			mapa.put(chave, lista);
		}

		lista.add(elemento);

	}

	/**
	 * Obter hora data.
	 *
	 * @param data        the data
	 * @param comSegundos the com segundos
	 * @return the string
	 */
	public static String obterHoraData(Date data, Boolean comSegundos) {

		StringBuilder horario = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(calendar.get(Calendar.MINUTE));
		String segundo = String.valueOf(calendar.get(Calendar.SECOND));

		if (Integer.parseInt(minuto) < AUXILIAR_DEZ) {
			minuto = ZERO + minuto;
		}

		horario.append(hora);
		horario.append(":");
		horario.append(minuto);
		if (comSegundos) {
			horario.append(":");
			horario.append(segundo);

		}
		return horario.toString();
	}

	/**
	 * Montar string sem nulos.
	 *
	 * @param delimitador the delimitador
	 * @param componentes the componentes
	 * @return the string
	 */
	public static String montarStringSemNulos(String delimitador, String... componentes) {
		Collection<String> listaComponentes = new ArrayList<String>(Arrays.asList(componentes));
		CollectionUtils.filter(listaComponentes, NotNullPredicate.getInstance());
		return StringUtils.join(listaComponentes, delimitador);
	}

	/**
	 * Suprime uma exceção, retornando a exceção resultante.
	 *
	 * @param excecao   - {@link Exception}
	 * @param suprimida - {@link Exception}
	 * @return {@link Exception}
	 */
	public static Exception suprimirExcecao(Exception excecao, Exception suprimida) {
		Exception resultante = excecao;
		if (isAllNotNull(excecao, suprimida)) {
			excecao.addSuppressed(suprimida);
		}
		return resultante;
	}

	/**
	 * Libera os recursos associados a um stream.
	 *
	 * @param recurso - {@link Closeable}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public static void limparRecursos(Closeable recurso) throws NegocioException {
		try {
			if (recurso != null) {
				recurso.close();
			}
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	/**
	 * @param dataInicio
	 * @param dataFim
	 * @return the Boolean
	 */
	public static Boolean isDataDentroIntervalo(Date data, Date dataInicio, Date dataFim) {
		return data.getTime() >= dataInicio.getTime() && data.getTime() <= dataFim.getTime();
	}

	/**
	 * @param stringAcentuada
	 * @return string sem acento
	 */
	public static String removerAcentuacao(String stringAcentuada) {
		return Normalizer.normalize(stringAcentuada, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	/**
	 * @param campo
	 * @return campo sem acento
	 */
	public static String removerAcentuacaoQuerySql(String campo) {
		return " translate(" + campo
				+ ",'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜáçéíóúàèìòùâêîôûãõëü','ACEIOUAEIOUAEIOUAOEUaceiouaeiouaeiouaoeu') ";
	}

	/**
	 * @param b
	 * @return boolean
	 */
	public static boolean isTrue(Boolean b) {
		return b != null && b;
	}

	/**
	 * @param stringBuilder
	 */
	public static void acrescentarConjuncao(StringBuilder stringBuilder) {
		if (stringBuilder.length() > 0) {
			if (stringBuilder.indexOf(" e ") > 0) {
				stringBuilder.append(", ");
			} else {
				stringBuilder.append(" e ");
			}
		}
	}

	/**
	 * Obtem o valor do array correspondente ao index ou vazio caso os parâmetros
	 * sejam inválidos.
	 *
	 * @param arr
	 * @param index - {@link Integer}
	 * @return valor - {@link String}
	 */
	public static String getValorArray(String[] arr, Integer index) {
		String valor = "";

		if (arr != null && (arr.length > 0) && (index >= 0 && index < arr.length)) {
			valor = arr[index];
		}

		return valor;
	}

	/**
	 * Obtem um metodo atraves de reflexao para a classe indicada.
	 *
	 * @param clazz          Classe do objeto - {@link Class<?>}
	 * @param methodName     Nome do metodo a ser buscado - {@link String}
	 * @param parameterTypes Atributos do metodo a ser buscado - {@link Class<?>}
	 *
	 * @return metodo - {@link Method}
	 */
	public static Method getMethodRecursive(Class<?> clazz, String methodName, Class<?>... parameterTypes)
			throws NoSuchMethodException, SecurityException {
		if (clazz != null && StringUtils.isNotBlank(methodName)) {
			return clazz.getMethod(methodName, parameterTypes);
		}
		return null;
	}

	/**
	 * Converter ISO para UTF8
	 * @param str - {@link String}
	 * @return String
	 */
	public static String converterISOtoUTF8(String str) {
		String ret = null;
		try {
			ret = new String(str.getBytes("UTF-8"), "ISO-8859-1");
		} catch (java.io.UnsupportedEncodingException e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
		return ret;
	}

	/**
	 * Retorna um File que estpa no caminho passado por parmetro
	 *
	 * @param caminhoArquivo -{@link String}
	 * @return File - {@link File}
	 */
	public static File getFile(String caminhoArquivo) {
		return new File(caminhoArquivo); // NOSONAR
	}

	/**
	 * Retorna um File que estpa no caminho passado por parmetro
	 *
	 * @param caminhoArquivo -{@link String}
	 * @param nome           -{@link String}
	 * @return File - {@link File}
	 */
	public static File getFileChild(String caminhoArquivo, String nome) {
		return new File(caminhoArquivo, nome); // NOSONAR
	}

	/**
	 * Retorna nome proprio formatado
	 *
	 * @param nomeProprio nome que sera formatado
	 * @return string formatada no formato
	 */
	public static String formatarNomeProprio(String nomeProprio) {
		StringBuilder builder = new StringBuilder(nomeProprio);

		for (int i = 1; i < builder.length(); i++) {
			char ch = builder.charAt(i);

			if (((ch >= 'A' && ch <= 'Z') || (ch >= 'Á' && ch <= 'Ú'))
					&& !(ch == ESPACO_EM_BRANCO || ch == APOSTOFRE)) {
				builder.setCharAt(i, (char) (ch + ESPACO_EM_BRANCO));
			} else {
				i++;
			}
		}
		return builder.toString().replaceAll("De ", "de ").replaceAll("Da ", "da ").replaceAll("Do ", "do ")
				.replaceAll("Das ", "das ").replaceAll("Dos ", "dos ").replaceAll("D'", "d'");
	}

	/**
	 * Retorna um FileWriter que estpa no caminho passado por parmetro
	 *
	 * @param caminhoArquivo -{@link String}
	 * @return FileWriter - {@link FileWriter}
	 * @throws IOException - {@link IOException}
	 */
	public static FileWriter getFileWriter(String caminhoArquivo) throws IOException {
		return new FileWriter(caminhoArquivo, false); // NOSONAR
	}

	/**
	 * Realiza o calculo da quantidade de divisoes realizadas para decompor o
	 * divendendo pelo divisor
	 * 
	 * @param dividendo - {@link Integer}
	 * @param divisor - {@link divisor}
	 * @return Total de divisoes, acrescendo +1 caso exista resto
	 */
	public static Integer obterTotalDivisoesDecomposicao(Integer dividendo, Integer divisor) {
		Integer totalDivisoes = 0;
		if (dividendo != null && divisor != null && divisor > 0) {
			do {
				++totalDivisoes;
				Integer mod = dividendo % divisor;
				if (dividendo.equals(mod) || dividendo == 0) {
					return totalDivisoes;
				}
				dividendo = mod;

			} while (dividendo != 0);
		}
		return totalDivisoes;
	}

	/**
	 * Arrendodamento de número para descontos da fatura
	 * 
	 * @param numero - {@link BigDecimal}
	 * @return arredondamento - {@link BigDecimal}
	 */
	public static BigDecimal arredondamentoDescontoFatura(BigDecimal numero) {
		numero = numero.setScale(TRES_CASAS_DECIMAIS, RoundingMode.HALF_UP);
		return numero.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
	}

	/**
	 * Formatacao do array de long retirando os Colchetes
	 * 
	 * @param chaves - the Long[]
	 * @return valor - the String
	 */
	public static String formatarArrayDeLongInline(Long[] chaves) {
		String valor = "";
		for (int i = 0; i < chaves.length; i++) {
			if (chaves[i] != null) {
				valor = valor.concat((chaves[i]).toString()).concat(",");
			}
		}
		if (valor.length() != 0) {
			valor = valor.substring(0, valor.length() - 1);
		}
		valor = valor.replace("[", "").replace("]", "");
		return valor;
	}

	/**
	 * Retira a mascara do CPF ou do CNPJ
	 * 
	 * @param cpfCnpj the cpfCnpj
	 * @return String the cpf ou cnpj sem mascara
	 */
	public static String retirarMascara(String cpfCnpj) {
		if (cpfCnpj != null) {
			cpfCnpj = cpfCnpj.replace(".", "");
			cpfCnpj = cpfCnpj.replace("-", "");
			if (cpfCnpj.length() < 18) {
				cpfCnpj = cpfCnpj.replace(".", "");
				cpfCnpj = cpfCnpj.replace("-", "");
				cpfCnpj = cpfCnpj.replace("/", "");
			}
		}
		return cpfCnpj;
	}

	/**
	 * Converte String para Boolean
	 * 
	 * @param valor the valor do tipo String
	 * @return Boolean the valor do tipo Booleano
	 */
	public static Boolean converterStringEmBoolean(String valor) {
		Boolean result = false;
		if (valor != null && ("true".equalsIgnoreCase(valor) || "1".equals(valor))) {
			result = true;
		}
		return result;
	}

	/**
	 * Obter Ultima semana atual de uma data
	 * 
	 * @param date the date
	 * @return Integer the semana atual da data em questao
	 */
	public static Integer obterSemanaAtualDaData(Date date) {
		int semana = 0;
		if (date != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			semana = c.get(Calendar.WEEK_OF_MONTH);
		}
		return semana;
	}

	/**
	 * Obter total de semanas de uma data
	 * 
	 * @param date the date
	 * @return Integer the semana atual da data em questao
	 */
	public static Integer obterTotalDeSemanasDoMes(Date date) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		int dia = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendario.set(Calendar.DATE, dia);

		int totalDeSemanas = calendario.get(Calendar.WEEK_OF_MONTH);

		return totalDeSemanas;
	}

	/**
	 * Obter total de semanas de um determinado mes/ano
	 * 
	 * @param anoMes the anoMes
	 * @return Integer the total de semanas atual da data em questao
	 */
	@SuppressWarnings("deprecation")
	public static Integer obterTotalDeSemanaDoAnoMes(Integer anoMes) {
		int weeks = 0;
		if (anoMes != null) {
			String anoMeses = String.valueOf(anoMes);
			Calendar calendario = Calendar.getInstance();

			String ano = anoMeses.substring(0, 4);
			Integer year = Integer.valueOf(ano);

			String mes = anoMeses.substring(4);
			Integer month = Integer.valueOf(mes);

			Integer ultimodia = montarData(anoMeses);
			Date date = new Date(year + "/" + month + "/" + ultimodia);
			calendario.setTime(date);

			weeks = calendario.get(Calendar.WEEK_OF_MONTH);
		}
		return weeks;
	}

	@SuppressWarnings("deprecation")
	private static Integer montarData(String anoMes) {
		int dia = 0;
		if (anoMes != null) {
			String ano = anoMes.substring(0, 4);
			Integer year = Integer.valueOf(ano);

			String mes = anoMes.substring(4);
			Integer month = Integer.valueOf(mes);

			Calendar calendario = Calendar.getInstance();
			Date date = new Date(year + "/" + month + "/" + 1);
			calendario.setTime(date);
			dia = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
	
		}
		return dia;
	}
	
	/**
	 * Calculo a distancia entre 2 pontos
	 * @param lat1 - {@link Double}
	 * @param lon1 - {@link Double}
	 * @param lat2 - {@link Double}
	 * @param lon2 - {@link Double}
	 * @return distancia entre 2 pontos - {@link Double}
	 */
	public static Double calculoDistancia(Double lat1, Double lon1, Double lat2, Double lon2) {
		Double theta = lon1 - lon2;
		Double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        if (dist.compareTo(1.0) > 0) {
            dist = 1.0;
        }
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		return (dist * 1.609344);
	}

	private static Double deg2rad(Double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static Double rad2deg(Double rad) {
		return (rad * 180 / Math.PI);
	}
	
	
	/**
	 * Converter uma string em big decimal
	 * @param valor - {@link String}
	 * @return valor convertido - {@link BigDecimal}
	 */
	public static BigDecimal converterStringBigDecimal(String valor) {
		return new BigDecimal(valor.replace(".", "").replace(",", "."));
	}
	

	/**
	 * Converter a url da imagem
	 * @param dadosFotoColetor 
	 * 				the dadosFotoColetor
	 * 	 * @param idVerificador 
	 * 				the idVerificador
	 * 	 * @param chavePrimaria 
	 * 				the chavePrimaria
	 * @return String
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 * @throws IOException
	 *             - {@link IOException}
	 */
	public static String converterUrl(String dadosFotoColetor, Long idVerificador, Long chavePrimaria) throws IOException, NegocioException{
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String caminhoFoto = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_REGISTRO_FOTO);
		String caminhoUrlFoto = "";
		
		File file = new File(caminhoFoto + dadosFotoColetor);
		if (!file.exists()) {
			String caminhoImagemFixa = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_FALHA_REGISTRO_FOTO);
			file = new File(caminhoImagemFixa);
			byte[] caminho = FileUtils.readFileToByteArray(file);
			caminhoUrlFoto = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(caminho);
		}
		else {
			if(chavePrimaria.equals(idVerificador)){						
				byte[] caminho = FileUtils.readFileToByteArray(file);
				caminhoUrlFoto = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(caminho);
			}
		}
		return caminhoUrlFoto;
	}
	
	public static Integer acharMaisProximoArrayInteiro(Integer[] array, int numeroAlvo) {
		int index = 0;
		int distancia = Math.abs(array[0] - numeroAlvo);

		for (int i = 1; i< array.length; i++) {
			int distanciaCalculada = Math.abs(array[i] - numeroAlvo);

			if (distanciaCalculada < distancia) {
				index = i;
				distancia = distanciaCalculada;
			}
		}

		return array[index];
	}
	
	public static Response chamarApiGoogle(String url) throws IOException {
		
		String apiKey = Constantes.CHAVE_API_GOOGLE_MAPS;
		url = url + apiKey;
		
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder()
				.url(url)
				.method("GET", null)
				.build();
		
		Response response = client.newCall(request).execute();
		
		return response;
	}
	
	public static Date adicionarHoraMinutoData(Date data, String hora, String minuto){
		
		GregorianCalendar calendario = new GregorianCalendar();
		calendario.setTime(data);
		
		calendario.set(GregorianCalendar.HOUR_OF_DAY, Integer.valueOf(hora));
		calendario.set(GregorianCalendar.MINUTE, Integer.valueOf(minuto));
		
		return calendario.getTime();

		
	}
	
	public static Map<String, Object> obterMenorDistanciaTempoEntrePontosConsumo(List<PontoConsumo> listaPontoConsumo,
			Double latitudeComparar, Double longitudeComparar, Double menorDistancia, PontoConsumo pontoConsumo,
			Long tempoDistancia) throws IOException {
		
		Collection<List<PontoConsumo>> listaParticionada = IntStream.range(0, listaPontoConsumo.size()).boxed()
				.collect(Collectors.groupingBy(particao -> (particao / 25),
						Collectors.mapping(index -> listaPontoConsumo.get(index), Collectors.toList())))
				.values();
		Map<String, Object> retorno = new HashMap<String, Object>();

		for (List<PontoConsumo> listaPontos : listaParticionada) {
			StringBuilder url = new StringBuilder();
			url.append("https://maps.googleapis.com/maps/api/distancematrix/json?destinations=");

			listaPontos.stream().forEach(p -> url.append(p.getLatitudeGrau() + "%2C" + p.getLongitudeGrau() + "%7C"));

			url.append("&origins=" + latitudeComparar + "%2C" + longitudeComparar + "&key=");

			Response resposta = Util.chamarApiGoogle(url.toString());

			String corpo = resposta.body().string();

			JsonObject jsonObject = new JsonParser().parse(corpo).getAsJsonObject();

			String status = jsonObject.get("status").getAsString();

			if ("OK".equals(status)) {
				JsonArray elements = ((JsonObject) jsonObject.getAsJsonArray("rows").get(0)).getAsJsonArray("elements");

				Integer i = 0;

				for (JsonElement element : elements) {

					JsonObject distance = (JsonObject) element.getAsJsonObject().get("distance");
					Double distancia = distance.get("value").getAsDouble();
					JsonObject duration = (JsonObject) element.getAsJsonObject().get("duration");
					Long tempo = duration.get("value").getAsLong();

					if (distancia < menorDistancia || menorDistancia.compareTo(Double.valueOf(-1.0)) == 0) {
						menorDistancia = distancia;
						tempoDistancia = tempo;
						pontoConsumo = (PontoConsumo) listaPontos.get(i);
					}
					i++;
				}

			}

		}
		
		retorno.put("menorDistancia", menorDistancia);
		retorno.put("tempoDistancia", tempoDistancia);
		retorno.put("pontoConsumo", pontoConsumo);
		
		return retorno;
	}
	
	public static Boolean checarSeArquivoExiste(String caminhoArquivo) {
		File file = new File(caminhoArquivo);
		return file.exists();
	}

	public static String converterBigDecimalValorMonetario(BigDecimal valor) {
		 NumberFormat formatador = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		 
		 String valorFormatado =  formatador.format(valor);
		 
		 return valorFormatado.replace("$", "\\$");
	}
	
	public static Long[] concatenarArrayChavesPrimarias(Long[] chavesPrimaria1, Long[] chavesPrimarias2) {
		if (chavesPrimaria1.length <= 0) {
			return chavesPrimarias2;
		} else {
			Long[] arrayConcatenado = Stream.concat(Arrays.stream(chavesPrimaria1), Arrays.stream(chavesPrimarias2))
					.toArray(Long[]::new);

			return arrayConcatenado;
		}
	}
	
	public static String codificarBase64(String senha) {
		return Base64.getEncoder().encodeToString(senha.getBytes());
	}
	
	public static String decodificarBase64(String senhaCodificada) {
        return new String(Base64.getDecoder().decode(senhaCodificada));
    }
	
	
	public static boolean validarSenhaCertificado(byte[] certificadoBytes, String senha) {
		try (ByteArrayInputStream bais = new ByteArrayInputStream(certificadoBytes)) {
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(bais, Util.decodificarBase64(senha).toCharArray());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
    public static boolean validarCnpjCertificado(byte[] certificadoBytes, String senha, String cnpj) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(certificadoBytes)) {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(bais, Util.decodificarBase64(senha).toCharArray());

            String alias = keystore.aliases().nextElement();

            X509Certificate certificado = (X509Certificate) keystore.getCertificate(alias);

            String dn = certificado.getSubjectX500Principal().getName();

            String[] atributos = dn.split(",");
            for (String atributo : atributos) {
                atributo = atributo.trim();
                if (atributo.startsWith("serialNumber=")) {
                    return StringUtils.isEmpty(atributo.replace("serialNumber=", "").trim());
                } else if (atributo.startsWith("2.5.4.5=")) { // OID para CNPJ
                    return StringUtils.isEmpty(atributo.replace("2.5.4.5=", "").trim());
                }
            }
        } catch (Exception e) {
            System.err.println("Erro ao validar o CNPJ no certificado: " + e.getMessage());
        }
        return false;
    }

    public static String converterImagemParaPNG(String caminhoJPEG) throws IOException{
		
        Path caminhoBase = Paths.get(caminhoJPEG);
        String nomeArquivoSemExtensao = caminhoBase.getFileName().toString().replaceFirst("[.][^.]+$", "");
        String novoArquivo = caminhoBase.getParent().toString() + "/" + nomeArquivoSemExtensao + ".png";
        Path caminhoFinal = Paths.get(novoArquivo);
        
        BufferedImage imagemOriginal = ImageIO.read(caminhoBase.toFile());
        ImageIO.write(imagemOriginal, "png", caminhoFinal.toFile());
        
		return novoArquivo;
	}
	
	public static Map<String, BigDecimal> obterCoordenadasEndereco(Cep endereco, String numeroEndereco) throws IOException {
		Map<String,BigDecimal> retorno = new HashMap<String, BigDecimal>();
		
		StringBuilder url = new StringBuilder();
		url.append("https://nominatim.openstreetmap.org/search?q=");
		url.append(endereco.getTipoLogradouro()).append("+");
		url.append(endereco.getLogradouro().replace(" ", "+")).append("+");
		url.append(numeroEndereco).append(",+");
		url.append(endereco.getNomeMunicipio()).append(",");
		url.append(endereco.getUf()).append(",");
		url.append("+Brasil&format=json&limit=1");
		
		Response resposta = Util.chamarApiNominatim(url.toString());		
		
		if (resposta.code() == 200) {
			String responseBody = resposta.body().string();
			JsonParser parser = new JsonParser();
			JsonArray jsonArray = parser.parse(responseBody).getAsJsonArray();

			if (jsonArray.size() > 0) {
				JsonElement elemento = jsonArray.get(0);
				BigDecimal latitude = elemento.getAsJsonObject().get("lat").getAsBigDecimal();
				BigDecimal longitude = elemento.getAsJsonObject().get("lon").getAsBigDecimal();

				retorno.put("latitude", latitude);
				retorno.put("longitude", longitude);
			}
		}
		
		return retorno;
	}

	public static Response chamarApiNominatim(String url) throws IOException {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder()
				.url(url)
				.method("GET", null)
				.build();
		
		Response response = client.newCall(request).execute();
		
		return response;
	}

}
