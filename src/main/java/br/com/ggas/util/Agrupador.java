package br.com.ggas.util;

/**
 * This is a Javadoc comment
 * @author voliveira
 *
 * @param <T, V>
 */
public interface Agrupador<T, V> {

	/**
	 * Metodo que define o objeto por qual a classe sera agrupada.
	 * 
	 * @return o objeto por qual a classe será agrupada
	 */
	V getChaveAgrupadora(T elemento);
	
}
