
package br.com.ggas.util;

/**
 * The Class ListaUtil.
 */
public class ListaUtil {
	
	/**
	 * By SONAR:
	 * Utility classes, which are a collection of static members, 
	 * are not meant to be instantiated. Java adds an implicit public 
	 * constructor to every class which does not define at least one explicitly. 
	 * Hence, at least one non-public constructor should be defined.
	 */
	private ListaUtil() {
		
	}

	/**
	 * Validar limites.
	 *
	 * @param cpAdicionados the cp adicionados
	 * @param limiteInferior the limite inferior
	 * @param limiteSuperior the limite superior
	 * @param chaveMenorQueLimiteInferior the chave menor que limite inferior
	 * @param chaveMaiorQueLimiteSuperior the chave maior que limite superior
	 * @param chaveEntidadeRotulo the chave entidade rotulo
	 * @return the string
	 */
	public static String validarLimites(Long[] cpAdicionados, int limiteInferior, 
					int limiteSuperior, String chaveMenorQueLimiteInferior, 
					String chaveMaiorQueLimiteSuperior, String chaveEntidadeRotulo) {
		
		String mensagemErro = null;
		
		if (cpAdicionados == null) {
			String entidade = MensagemUtil.obterMensagem(chaveEntidadeRotulo);
			mensagemErro = MensagemUtil.obterMensagem(chaveMenorQueLimiteInferior,
							entidade.toLowerCase(), limiteInferior);
		} else {
			if (cpAdicionados.length < limiteInferior) {
				String entidade = MensagemUtil.obterMensagem(chaveEntidadeRotulo);
				mensagemErro = MensagemUtil.obterMensagem(chaveMenorQueLimiteInferior,
								entidade.toLowerCase(), limiteInferior);
			} else {
				if (cpAdicionados.length > limiteSuperior) {
					String entidade = MensagemUtil.obterMensagem(chaveEntidadeRotulo);
					mensagemErro = MensagemUtil.obterMensagem(chaveMaiorQueLimiteSuperior,
									entidade.toLowerCase(), limiteSuperior);
				}
			}
		}
		
		return mensagemErro;
		
	}
	
	/**
	 * Gets the elment by index.
	 *
	 * @param lista the lista
	 * @param index the index
	 * @return the elment by index
	 */
	public static <T> T getElementByIndex(T[] lista, int index) {
		
		if (index >= 0 && index < lista.length) {
			return lista[index];
		}
		
		return null;
		
	}

}
