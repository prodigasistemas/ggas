/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;

/**
 * This is a Javadoc comment
 * @author gmatos
 */
public class ColecaoPaginada implements PaginatedList {

	private int fullListSize;

	private List<?> list;

	private int objectsPerPage;

	private int pageNumber;

	private String searchId;

	private String sortCriterion;

	private SortOrderEnum sortDirection;

	private int index;

	private Map<String, OrdenacaoEspecial> ordenacaoEspecial = new HashMap<String, OrdenacaoEspecial>();

	public static final String SORT = "sort";

	public static final String PAGE = "page";

	public static final String PAGE_REQUEST = "pageRequest";

	public static final String ASC = "asc";

	public static final String DESC = "desc";

	public static final String DIRECTION = "dir";

	/**
	 * @return the fullListSize
	 */
	@Override
	public int getFullListSize() {

		return fullListSize;
	}

	/**
	 * @param fullListSize
	 *            the fullListSize to set
	 */
	public void setFullListSize(int fullListSize) {

		this.fullListSize = fullListSize;
	}

	/**
	 * @return the list
	 */
	@Override
	public List<?> getList() {

		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<?> list) {

		this.list = list;
	}

	/**
	 * @return the objectsPerPage
	 */
	@Override
	public int getObjectsPerPage() {

		return objectsPerPage;
	}

	/**
	 * @param objectsPerPage
	 *            the objectsPerPage to set
	 */
	public void setObjectsPerPage(int objectsPerPage) {

		this.objectsPerPage = objectsPerPage;
	}

	/**
	 * @return the pageNumber
	 */
	@Override
	public int getPageNumber() {

		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {

		this.pageNumber = pageNumber;
	}

	/**
	 * @return the searchId
	 */
	@Override
	public String getSearchId() {

		return searchId;
	}

	/**
	 * @param searchId
	 *            the searchId to set
	 */
	public void setSearchId(String searchId) {

		this.searchId = searchId;
	}

	/**
	 * @return the sortCriterion
	 */
	@Override
	public String getSortCriterion() {

		return sortCriterion;
	}

	/**
	 * @param sortCriterion
	 *            the sortCriterion to set
	 */
	public void setSortCriterion(String sortCriterion) {

		this.sortCriterion = sortCriterion;
	}

	/**
	 * @return the sortDirection
	 */
	@Override
	public SortOrderEnum getSortDirection() {

		return sortDirection;
	}

	/**
	 * @param sortDirection
	 *            the sortDirection to set
	 */
	public void setSortDirection(SortOrderEnum sortDirection) {

		this.sortDirection = sortDirection;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {

		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {

		this.index = index;
	}

	/**
	 * @return the ordenacaoEspecial
	 */
	public Map<String, OrdenacaoEspecial> getOrdenacaoEspecial() {

		return ordenacaoEspecial;
	}

	/**
	 * @param ordenacaoEspecial
	 *            the ordenacaoEspecial to set
	 */
	public void setOrdenacaoEspecial(Map<String, OrdenacaoEspecial> ordenacaoEspecial) {

		this.ordenacaoEspecial = ordenacaoEspecial;
	}

	/**
	 * Método responsável por ordenar
	 * especialmente os campos.
	 * 
	 * @param campo
	 *            O campo da ordenaçãqo
	 * @param ordenacao
	 *            the ordenacao
	 */
	public void adicionarOrdenacaoEspecial(String campo, OrdenacaoEspecial ordenacao) {

		this.ordenacaoEspecial.put(campo, ordenacao);
	}
}
