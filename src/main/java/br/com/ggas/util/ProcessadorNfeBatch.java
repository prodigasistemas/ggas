/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.nfe.batch.EmitirNfeBatch;
import br.com.ggas.nfe.batch.RetornoNfeBatch;
import br.com.ggas.parametrosistema.ControladorParametroSistema;

public class ProcessadorNfeBatch {

	private static final String PROCESSO = "processo";

	private static final Logger LOG = Logger.getLogger(ProcessadorNfeBatch.class);

	private static final String ARQUIVO_PROPRIEDADES_OPERACAO_SISTEMA = "operacaoSistema.properties";

	private static final String OPERACAO_EMITIR_NFE = "EMITIR_NFE";

	private static final String OPERACAO_RETORNO_NFE = "RETORNO_NFE";

	private Boolean processar = Boolean.TRUE;

	private Processo processoEmitirNfe;

	private Processo processoRetornoNfe;

	/**
	 * Run.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void run() throws GGASException {

		if(this.processar) {

			this.processar = Boolean.FALSE;

			try {
				LOG.info("processando NFE .....");

				if(processoEmitirNfe == null || processoRetornoNfe == null) {

					processoEmitirNfe = this.obterProcesso(OPERACAO_EMITIR_NFE);
					processoRetornoNfe = this.obterProcesso(OPERACAO_RETORNO_NFE);
				}

				Map<String, Object> parametros = new TreeMap<String, Object>();
				parametros.put(PROCESSO, processoEmitirNfe);

				EmitirNfeBatch emitirNfeBatch = new EmitirNfeBatch();
				emitirNfeBatch.processar(parametros);

				parametros.remove(PROCESSO);
				parametros.put(PROCESSO, processoRetornoNfe);

				RetornoNfeBatch retornoNfeBatch = new RetornoNfeBatch();
				retornoNfeBatch.processar(parametros);

			} catch(Exception e) {

				this.processar = Boolean.TRUE;
				LOG.error(e.getMessage(), e);

			}

			this.processar = Boolean.TRUE;
		}
	}

	/**
	 * Obter processo.
	 * 
	 * @param operacao
	 *            the operacao
	 * @return the processo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Processo obterProcesso(String operacao) throws GGASException {

		Properties prop = Util.lerArquivoPropriedades(ARQUIVO_PROPRIEDADES_OPERACAO_SISTEMA);
		ControladorModulo controladorModulo = (ControladorModulo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorModulo.BEAN_ID_CONTROLADOR_MODULO);
		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia().getBeanPorID(
						ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
		ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia().getBeanPorID(
						ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);

		Processo processo = (Processo) controladorProcesso.criar();

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long usuarioGGas = Long.valueOf((String) controladorParametrosSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_USUARIO_GGAS));

		processo.setUsuario((Usuario) controladorUsuario.obter(usuarioGGas));
		processo.setOperacao(controladorModulo.buscarOperacaoPorChave(Long.valueOf((String) prop.get(operacao))));

		return processo;
	}

	/**
	 * Clean.
	 */
	public void clean() {
		//Método vazio.
	}

}
