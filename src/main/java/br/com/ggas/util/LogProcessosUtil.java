
package br.com.ggas.util;

/**
 * The Class LogProcessosUtil.
 */
public class LogProcessosUtil {

	/**
	 * Gerar linha rotulo numero com separador.
	 *
	 * @param chave the chave
	 * @param numero the numero
	 * @param separador the separador
	 * @return the string
	 */
	private static String gerarLinhaRotuloNumeroComSeparador(
					String chave, Integer numero, String separador) {
		StringBuilder sb = new StringBuilder();
		sb.append(Constantes.PULA_LINHA);
		sb.append(MensagemUtil.obterMensagem(chave));
		sb.append(separador);
		sb.append(numero);
		return sb.toString();
	}
	
	/**
	 * Gerar linha rotulo descricao com separador.
	 *
	 * @param chave the chave
	 * @param descricao the descricao
	 * @param separador the separador
	 * @return the string
	 */
	private static String gerarLinhaRotuloDescricaoComSeparador(
					String chave, String descricao, String separador) {
		StringBuilder sb = new StringBuilder();
		sb.append(Constantes.PULA_LINHA);
		sb.append(gerarMensagemRotuloDescricao(chave, descricao, separador, true));
		return sb.toString();
	}

	/**
	 * Gerar mensagem rotulo descricao.
	 *
	 * @param chave the chave
	 * @param descricao the descricao
	 * @return the string
	 */
	private static String gerarMensagemRotuloDescricao(
					String chave, String descricao) {
		return gerarMensagemRotuloDescricao(chave, descricao, 
						Constantes.STRING_DOIS_PONTOS_ESPACO, false);
	}
	
	/**
	 * Gerar mensagem rotulo descricao.
	 *
	 * @param chave the chave
	 * @param descricao the descricao
	 * @param separador the separador
	 * @param quebraLinha the quebra linha
	 * @return the string
	 */
	private static String gerarMensagemRotuloDescricao(
					String chave, String descricao, String separador, boolean quebraLinha) {
		StringBuilder sb = new StringBuilder();
		sb.append(MensagemUtil.obterMensagem(chave));
		sb.append(separador);
		if (quebraLinha) {
			sb.append(Constantes.PULA_LINHA);
		}			
		sb.append(descricao);
		return sb.toString();
	}
	
	/**
	 * Gerar linha log.
	 *
	 * @param chave the chave
	 * @return the string
	 */
	private static String gerarLinhaLog(String chave) {
		return gerarLinhaLog(chave, false);
	}
	
	/**
	 * Gerar linha log.
	 *
	 * @param chave the chave
	 * @param pulaLinha the pula linha
	 * @return the string
	 */
	private static String gerarLinhaLog(String chave, boolean pulaLinha) {
		StringBuilder sb = new StringBuilder();
		sb.append(Constantes.PULA_LINHA);
		sb.append(DataUtil.gerarEtiquetaDataHoraAtual());
		sb.append(Constantes.STRING_ESPACO);
		sb.append(MensagemUtil.obterMensagem(chave));
		if (pulaLinha) {
			sb.append(Constantes.PULA_LINHA);			
		}
		return sb.toString();
	}

	/**
	 * Gerar mensagem evento comercial.
	 *
	 * @param mensagem the mensagem
	 * @return the string
	 */
	public static String gerarMensagemEventoComercial(String mensagem) {
		return gerarMensagemRotuloDescricao(
						Constantes.LOG_ROTULO_EVENTO_COMERCIAL, mensagem);
	}

	/**
	 * Gerar mensagem segmento.
	 *
	 * @param mensagem the mensagem
	 * @return the string
	 */
	public static String gerarMensagemSegmento(String mensagem) {
		return gerarMensagemRotuloDescricao(
						Constantes.LOG_ROTULO_SEGMENTO, mensagem);
	}

	/**
	 * Gerar mensagem item contabil.
	 *
	 * @param mensagem the mensagem
	 * @return the string
	 */
	public static String gerarMensagemItemContabil(String mensagem) {
		return gerarMensagemRotuloDescricao(
						Constantes.LOG_ROTULO_ITEM_CONTABIL, mensagem);
	}

	/**
	 * Gerar mensagem tributo.
	 *
	 * @param mensagem the mensagem
	 * @return the string
	 */
	public static String gerarMensagemTributo(String mensagem) {
		return gerarMensagemRotuloDescricao(
						Constantes.LOG_ROTULO_TRIBUTO, mensagem);
	}

	/**
	 * Gerar log registros inconsistentes.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogRegistrosInconsistentes(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_REGISTROS_INCONSISTENTES, numero, 
						Constantes.STRING_DOIS_PONTOS_ESPACO);
	}
	
	/**
	 * Gerar log registros processados.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogRegistrosProcessados(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_REGISTROS_PROCESSSADOS, numero, 
						Constantes.STRING_DOIS_PONTOS_TABULACAO);
	}
	
	/**
	 * Gerar log registros incluidos.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogRegistrosIncluidos(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_REGISTROS_INCLUIDOS, numero, 
						Constantes.STRING_DOIS_PONTOS_TABULACAO);
	}
	
	/**
	 * Gerar log registros alterados.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogRegistrosAlterados(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_REGISTROS_ALTERADOS, numero, 
						Constantes.STRING_DOIS_PONTOS_TABULACAO);
	}
	
	/**
	 * Gerar log registros excluidos.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogRegistrosExcluidos(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_REGISTROS_EXCLUIDOS, numero, 
						Constantes.STRING_DOIS_PONTOS_TABULACAO);
	}

	/**
	 * Gerar log total inconsistencias.
	 *
	 * @param numero the numero
	 * @return the string
	 */
	public static String gerarLogTotalInconsistencias(Integer numero) {
		return LogProcessosUtil.gerarLinhaRotuloNumeroComSeparador(
						Constantes.LOG_TOTAL_INCONSISTENCIAS, numero, 
						Constantes.STRING_DOIS_PONTOS_ESPACO);
	}

	/**
	 * Gerar log inconsistencias registradas.
	 *
	 * @param descricao the descricao
	 * @return the string
	 */
	public static String gerarLogInconsistenciasRegistradas(String descricao) {
		return LogProcessosUtil.gerarLinhaRotuloDescricaoComSeparador(
						Constantes.LOG_INCONSISTENCIAS_REGISTRADAS, descricao, 
						Constantes.STRING_DOIS_PONTOS_ESPACO);
	}

	/**
	 * Gerar log lista eventos inconsistencias.
	 *
	 * @param descricao the descricao
	 * @return the string
	 */
	public static String gerarLogListaEventosInconsistencias(String descricao) {
		return LogProcessosUtil.gerarLinhaRotuloDescricaoComSeparador(
						Constantes.LOG_LISTA_EVENTOS_INCONSISTENCIAS, descricao, 
						Constantes.STRING_DOIS_PONTOS_ESPACO);
	}
	
	/**
	 * Gerar log erro sem dados.
	 *
	 * @return the string
	 */
	public static String gerarLogErroSemDados() {
		return LogProcessosUtil.gerarLinhaLog(
						Constantes.LOG_ERRO_NEGOCIO_SEM_DADOS, true);
	}
	
	/**
	 * Gerar log erro integracao lancamentos contabeis nao localizado.
	 *
	 * @return the string
	 */
	public static String gerarLogErroIntegracaoLancamentosContabeisNaoLocalizado() {
		return LogProcessosUtil.gerarLinhaLog(
						Constantes.LOG_ERRO_INTEGRACAO_LANCAMENTOS_CONTABEIS_NAO_LOCALIZADO);
	}
	
	/**
	 * Gerar inicio validacao lancamentos contabeis sinteticos.
	 *
	 * @return the string
	 */
	public static String gerarInicioValidacaoLancamentosContabeisSinteticos() {
		return LogProcessosUtil.gerarLinhaLog(
						Constantes.LOG_INICIO_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS);
	}

	/**
	 * Gerar fim validacao lancamentos contabeis sinteticos.
	 *
	 * @return the string
	 */
	public static String gerarFimValidacaoLancamentosContabeisSinteticos() {
		return LogProcessosUtil.gerarLinhaLog(
						Constantes.LOG_FIM_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS);
	}

}
