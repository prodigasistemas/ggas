/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.transform.ResultTransformer;

/**
 * @author gmatos
 */
public final class HibernateCriteriaUtil {

	/**
	 * Método responsável por paginar uma
	 * consulta.
	 * 
	 * @param colecaoPaginada
	 *            Os dados de paginação
	 * @param criteria
	 *            O criteria
	 */
	public static void paginarConsultaPorCriteria(ColecaoPaginada colecaoPaginada, Criteria criteria) {

		if(colecaoPaginada != null) {
			int totalLinhas = obterTotalDeLinhasPorCriteria(criteria);
			colecaoPaginada.setFullListSize(totalLinhas);

			if(colecaoPaginada.getOrdenacaoEspecial() != null
							&& colecaoPaginada.getOrdenacaoEspecial().containsKey(colecaoPaginada.getSortCriterion())) {
				colecaoPaginada.getOrdenacaoEspecial().get(colecaoPaginada.getSortCriterion()).createAlias(criteria);
				colecaoPaginada.getOrdenacaoEspecial().get(colecaoPaginada.getSortCriterion())
								.addOrder(criteria, colecaoPaginada.getSortDirection());
			} else {
				if(colecaoPaginada.getSortCriterion() != null) {
					if(colecaoPaginada.getSortDirection().equals(SortOrderEnum.ASCENDING)) {
						criteria.addOrder(Order.asc(colecaoPaginada.getSortCriterion()));
					} else if(colecaoPaginada.getSortDirection().equals(SortOrderEnum.DESCENDING)) {
						criteria.addOrder(Order.desc(colecaoPaginada.getSortCriterion()));
					}
				}
			}

			criteria.setFirstResult(colecaoPaginada.getIndex());
			criteria.setMaxResults(colecaoPaginada.getObjectsPerPage());
		}
	}
	

	public static void paginarConsultaPorTamanhoListaPersonalizada(ColecaoPaginada colecaoPaginada, int totalLinhas) {

		if(colecaoPaginada != null) {
			colecaoPaginada.setFullListSize(totalLinhas);
		}
	}

	/**
	 * Método responsável por obter o total de
	 * linhas por criteria.
	 * 
	 * @param criteria
	 *            O critéria
	 * @return O total de linhas
	 */
	public static Integer obterTotalDeLinhasPorCriteria(Criteria criteria) {

		CriteriaImpl cImpl = (CriteriaImpl) criteria;
		Projection originalProjection = cImpl.getProjection();
		ResultTransformer originalResultTransformer = cImpl.getResultTransformer();
		Long totalElements = 0L;

		if(originalProjection != null) {
			totalElements = (long) criteria.list().size();
		} else {
			totalElements = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
		}

		criteria.setProjection(originalProjection);
		criteria.setResultTransformer(originalResultTransformer);

		return totalElements.intValue();
	}

}
