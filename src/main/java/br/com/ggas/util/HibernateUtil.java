/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.Dialect;

/**
 *
 */
public class HibernateUtil {

	private static final Logger LOG = Logger.getLogger(HibernateUtil.class);

	private static Configuration configuration;

	private static SessionFactory sessionFactory;

	@SuppressWarnings("rawtypes")
	private static final ThreadLocal THREADSESSION = new ThreadLocal();

	@SuppressWarnings("rawtypes")
	private static final ThreadLocal THREADTRANSACTION = new ThreadLocal();

	@SuppressWarnings("rawtypes")
	private static final ThreadLocal THREADINTERCEPTOR = new ThreadLocal();

	static {
		try {
			configuration = new Configuration();
			sessionFactory = configuration.configure().buildSessionFactory();
		} catch(Exception ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * @return Retorna o atributo configuration.
	 */
	public static Configuration getConfiguration() {

		return configuration;
	}

	/**
	 * @param configuration
	 *            O valor a ser atribuído ao
	 *            atributo
	 *            configuration.
	 */
	public static void setConfiguration(Configuration configuration) {

		HibernateUtil.configuration = configuration;
	}

	/**
	 * @return Retorna o atributo sessionFactory.
	 */
	public static SessionFactory getSessionFactory() {

		return sessionFactory;
	}

	/**
	 * @param sessionFactory
	 *            O valor a ser atribuído ao
	 *            atributo
	 *            sessionFactory.
	 */
	public static void setSessionFactory(SessionFactory sessionFactory) {

		HibernateUtil.sessionFactory = sessionFactory;
	}

	/**
	 * Método responsável por obter a sessão.
	 * 
	 * @return Session
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public static Session getSession() throws HibernateException {

		Session session = (Session) THREADSESSION.get();
		try {
			if(session == null) {
				if(getInterceptor() != null) {
					session = getSessionFactory().openSession(getInterceptor());
				} else {
					session = getSessionFactory().openSession();
				}
				THREADSESSION.set(session);
			}
		} catch(HibernateException ex) {
			throw new RuntimeException(ex);
		}
		return session;
	}

	/**
	 * Método responsável por fechar a sessão.
	 * 
	 * @throws HibernateException
	 *             the hibernate exception
	 */
	@SuppressWarnings("unchecked")
	public static void closeSession() throws HibernateException {

		try {
			Session session = (Session) THREADSESSION.get();
			THREADSESSION.set(null);
			if(session != null && session.isOpen()) {
				session.close();
			}
		} catch(HibernateException ex) {
			throw new RuntimeException(ex);

		}
	}

	/**
	 * Método responsável por iniciar uma
	 * transação.
	 * 
	 * @throws HibernateException
	 *             the hibernate exception
	 */
	@SuppressWarnings("unchecked")
	public static void beginTransaction() throws HibernateException {

		Transaction transaction = (Transaction) THREADTRANSACTION.get();
		try {
			if(transaction == null) {
				transaction = getSession().beginTransaction();
				THREADTRANSACTION.set(transaction);
			}
		} catch(HibernateException ex) {
			throw new RuntimeException(ex);

		}
	}

	/**
	 * Método responsável por comitar a transação.
	 * 
	 * @throws HibernateException
	 *             the hibernate exception
	 */
	@SuppressWarnings("unchecked")
	public static void commitTransaction() throws HibernateException {

		Transaction transaction = (Transaction) THREADTRANSACTION.get();
		try {
			if(transaction != null && !transaction.wasCommitted() && !transaction.wasRolledBack()) {
				transaction.commit();
			}
			THREADTRANSACTION.set(null);
		} catch(HibernateException ex) {
			rollbackTransaction();
			throw new RuntimeException(ex);

		}
	}

	/**
	 * Método responsável por cancelar a
	 * transação.
	 * 
	 * @throws HibernateException
	 *             the hibernate exception
	 */
	@SuppressWarnings("unchecked")
	public static void rollbackTransaction() throws HibernateException {

		Transaction transaction = (Transaction) THREADTRANSACTION.get();
		try {
			THREADTRANSACTION.set(null);
			if(transaction != null && !transaction.wasCommitted() && !transaction.wasRolledBack()) {
				transaction.rollback();
			}
		} catch(HibernateException ex) {
			throw new RuntimeException(ex);

		} finally {
			closeSession();
		}
	}

	/**
	 * Método responsável por obter o schema para
	 * a criação das tabelas.
	 * 
	 * @return String O SQL
	 * @throws HibernateException
	 *             the hibernate exception
	 */
	public static String generateSchemaCreationScript() throws HibernateException {

		StringBuilder schema = new StringBuilder();
		String[] sql = null;

		sql = configuration.generateSchemaCreationScript(Dialect.getDialect(configuration.getProperties()));
		for (int i = 0; i < sql.length; i++) {
			schema.append("\n").append(sql[i]).append(";");
		}

		return schema.toString();
	}

	/**
	 * Método responsável por obter o
	 * interceptador
	 * 
	 * @return Interceptor
	 */
	private static Interceptor getInterceptor() {

		return (Interceptor) THREADINTERCEPTOR.get();
	}

	/**
	 * Método principal para executar a claasse.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		LOG.info(generateSchemaCreationScript());
	}

}
