/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import br.com.ggas.geral.exception.GGASException;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 * Classe utilirária responsável por processar templates HTML gerados pelo freemarker
 * Os tempaltes do freemarker estão na pasta templatesEmail dentro de resources
 * @author jose.victor@logiquesistemas.com.br
 */
public final class FreemarkerUtils {

	private static final Logger LOG = Logger.getLogger(FreemarkerUtils.class);
	private static Configuration cfg = new Configuration();
	private static final String TEMPLATES_FOLDER = "templatesEmail";

	private FreemarkerUtils() {
	}

	/**
	 * Processa o template freemarker informado e retorna o HTML do template em string
	 * @param nomeTemplate nome do plate a ser processado
	 * @param parametros parametros a serem usados para montar o template informado
	 * @return retorna o html do template freemarker
	 * @throws GGASException exceção lançada caso ocorra falha na geração do template
	 */
	@SuppressWarnings("squid:S4797")
	public static String processarTemplate(String nomeTemplate, Map<String, Object> parametros) throws GGASException {

		StringWriter writer = new StringWriter();

		try {
			final String templatesEmailFullPath = FreemarkerUtils.class.getClassLoader().getResource(TEMPLATES_FOLDER).getFile();
			cfg.setDirectoryForTemplateLoading(new File(templatesEmailFullPath));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template template = cfg.getTemplate(nomeTemplate);
			template.process(parametros, writer);
			writer.flush();
			writer.close();
		} catch (TemplateException | IOException e) {
			LOG.error("Falha ao processar template " + nomeTemplate, e);
			throw new GGASException(e);
		}

		return writer.toString();
	}

}
