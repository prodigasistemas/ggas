package br.com.ggas.util;

import java.io.Serializable;
import java.util.Comparator;

import br.com.ggas.cadastro.imovel.ContatoImovel;

/**
 * Classe responsável por ordenar a lista de contato do imovel
 *
 * Ordena pelo contato principal primeiro e depois segue a ordem alfabetica do nome do contato
 */
public class ContatoImovelComparator implements Comparator<ContatoImovel>, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5797915629911686258L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(ContatoImovel o1, ContatoImovel o2) {

		Boolean o1Principal = o1.isPrincipal();
		Boolean o2Principal = o2.isPrincipal();

		if (o1Principal || o2Principal) {
			return o2Principal.compareTo(o1Principal);
		} else {
			return Util.removerAcentuacao(o1.getNome()).compareTo(Util.removerAcentuacao(o2.getNome()));
		}
	}

}
