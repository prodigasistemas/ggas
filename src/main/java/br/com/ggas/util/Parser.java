/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;

/**
 * 
 *
 */
public abstract class Parser {

	/**
	 * Processar linha.
	 * 
	 * @param linha
	 *            the linha
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> processarLinha(String linha) throws GGASException {

		throw new UnsupportedOperationException("Esse método precisa ser sobrescrito");
	}

	/**
	 * Gerar linha.
	 * 
	 * @param dados
	 *            the dados
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static String gerarLinha(Map<String, Object> dados) throws GGASException {

		throw new UnsupportedOperationException("Esse método precisa ser sobrescrito");
	}

	/**
	 * Método responsável por preencher um array
	 * com um texto.
	 * 
	 * @param posicao
	 *            A posição inicial e final
	 * @param texto
	 *            O texto
	 * @param array
	 *            O array que será preenchido
	 */
	public static void preencherArray(int[] posicao, String texto, char[] array) {

		int j = 0;
		for (int i = posicao[0] - 1; i <= posicao[1] - 1; i++) {
			if(texto.length() > j) {
				array[i] = texto.charAt(j);
			}
			j++;
		}
	}

	/**
	 * Método responsável por inicializar um
	 * array.
	 * 
	 * @param array
	 *            O array que será inicializado
	 */
	public static void inicializarArray(char[] array) {

		for (int i = 0; i < array.length; i++) {
			array[i] = ' ';
		}
	}

	/**
	 * Método responsável por obter um valor pela
	 * posição.
	 * 
	 * @param linha
	 *            A linha
	 * @param posicao
	 *            A posição inicial e final
	 * @return Um texto recuperado
	 */
	public static String obterValorPosicao(String linha, int[] posicao) {

		String retorno = null;
		if(!StringUtils.isEmpty(linha)) {
			retorno = linha.substring(posicao[0] - 1, posicao[1]);
		}
		return retorno;
	}

	/**
	 * Validar campo inteiro.
	 * 
	 * @param campo
	 *            the campo
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	public static void validarCampoInteiro(String campo) throws FormatoInvalidoException {

		if(!StringUtils.isBlank(campo)) {
			Util.converterCampoStringParaValorInteger(campo);
		}
	}

	/**
	 * Validar campo data de acordo com o formato
	 * informado.
	 * 
	 * @param campo
	 *            Campo
	 * @param formato
	 *            Formato
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static void validarCampoData(String campo, String formato) throws GGASException {

		if(!StringUtils.isBlank(campo)) {
			Util.converterCampoStringParaData("Data", campo, formato);
		}
	}
}
