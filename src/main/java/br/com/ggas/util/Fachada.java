/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.joda.time.DateTime;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.ValorParcela;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoModelo;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.dominio.FuncionarioEquipeVO;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipeTurno;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVOCobranca;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.dadocensitario.ControladorSetorCensitario;
import br.com.ggas.cadastro.dadocensitario.SetorCensitario;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.ControladorServicoPrestado;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.empresa.ServicoPrestado;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.endereco.EnderecoReferencia;
import br.com.ggas.cadastro.equipamento.ControladorEquipamento;
import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.Pais;
import br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorAreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.ControladorVolumeReservatorio;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelRamoAtividade;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.ModalidadeAquecimento;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.MotivoFimRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.PavimentoCalcada;
import br.com.ggas.cadastro.imovel.PavimentoRua;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoEquipamento;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.PotenciaFixaEquipamentoVO;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RamoAtividadeAmostragemPCS;
import br.com.ggas.cadastro.imovel.RamoAtividadeIntervaloPCS;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SegmentoAmostragemPCS;
import br.com.ggas.cadastro.imovel.SegmentoIntervaloPCS;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.cadastro.imovel.VolumeReservatorio;
import br.com.ggas.cadastro.imovel.impl.ComentarioPontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoVencimento;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.localidade.AreaTipo;
import br.com.ggas.cadastro.localidade.ControladorAreaTipo;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorPerfilQuadra;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.ControladorUnidadeNegocio;
import br.com.ggas.cadastro.localidade.ControladorZeis;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.localidade.Zeis;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorErp;
import br.com.ggas.cadastro.operacional.ControladorRamal;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.operacional.ControladorZonaBloqueio;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.Ramal;
import br.com.ggas.cadastro.operacional.RedeComprimento;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.operacional.ZonaBloqueio;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.acao.AcaoCobranca;
import br.com.ggas.cobranca.acao.ControladorAcaoCobranca;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.AvisoCorteEmissao;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.parcelamento.ParcelamentoItem;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSegmento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSituacaoPontoConsumo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.ControladorContaContabil;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.contabil.LancamentoContabilAnalitico;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.contabil.LancamentoContabilSinteticoVO;
import br.com.ggas.contrato.contrato.Aba;
import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.AbaModelo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.contrato.LayoutRelatorio;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.contrato.contrato.PenalidadeDemanda;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.ContratoAnexo;
import br.com.ggas.contrato.contrato.impl.QuantidadeVolumesVO;
import br.com.ggas.contrato.contratoadequacaoparceria.ContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.impl.ContratoAdequacaoParceriaImpl;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.SituacaoProposta;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.AlcadaVO;
import br.com.ggas.controleacesso.AlteracaoAlcadaVO;
import br.com.ggas.controleacesso.AutorizacoesPendentesVO;
import br.com.ggas.controleacesso.ChaveOperacaoSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorHistoricoSenha;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.HistoricoSenha;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.ModuloFuncOperacoesVO;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.FaturamentoGrupoRotaImpressao;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.NaturezaOperacaoConfiguracao;
import br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadeVO;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.faturamento.apuracaopenalidade.impl.MigrarSaldoQPNRVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeContratadaVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeNaoRetiradaVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeRetiradaPeriodoVO;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoDetalhamentoVO;
import br.com.ggas.faturamento.creditodebito.impl.HistoricoNotaDebitoCredito;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.ControladorFaturaControleImpressao;
import br.com.ggas.faturamento.fatura.DadosGeraisItensFatura;
import br.com.ggas.faturamento.fatura.DadosItemFaturamento;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.fatura.FaturaConciliacao;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.FaturamentoGrupoRotaImpressaoImpl;
import br.com.ggas.faturamento.mensagem.ControladorMensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemCobranca;
import br.com.ggas.faturamento.mensagem.MensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemSegmento;
import br.com.ggas.faturamento.mensagem.MensagemVencimento;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.NomeclaturaComumMercosul;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.rubrica.RubricaValorRegulamentado;
import br.com.ggas.faturamento.serie.ControladorSerie;
import br.com.ggas.faturamento.tarifa.AcaoSincroniaDesconto;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.DadosFaixasTarifa;
import br.com.ggas.faturamento.tarifa.DadosTarifa;
import br.com.ggas.faturamento.tarifa.FaixasTarifaDesconto;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaPesquisar;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tarifa.impl.FiltroTarifaVigenciaDescontos;
import br.com.ggas.faturamento.tarifa.impl.TarifaVO;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.feriado.Feriado;
import br.com.ggas.geral.impl.FavoritosImpl;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.geral.AcompanhamentoIntegracaoVO;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoFuncao;
import br.com.ggas.integracao.geral.IntegracaoFuncaoTabela;
import br.com.ggas.integracao.geral.IntegracaoMapeamento;
import br.com.ggas.integracao.geral.IntegracaoParametro;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.integracao.geral.IntegracaoSistemaParametro;
import br.com.ggas.integracao.geral.impl.IntegracaoSistemaFuncaoImpl;
import br.com.ggas.integracao.geral.impl.IntegracaoSistemaParametroImpl;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.TipoComposicaoConsumo;
import br.com.ggas.medicao.consumo.TipoConsumo;
import br.com.ggas.medicao.consumo.impl.ControladorImovelPCSZ;
import br.com.ggas.medicao.consumo.impl.PlanilhaVolumePcsVO;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.MedicaoHistoricoComentario;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.medidor.CapacidadeMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.DiametroMedidor;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MedidorVirtualVO;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.MovimentacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.TipoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.TipoRota;
import br.com.ggas.medicao.rota.impl.RotaHistorico;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimentoVO;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.medicao.vazaocorretor.PressaoMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.ProtocoloComunicacao;
import br.com.ggas.medicao.vazaocorretor.TemperaturaMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.TipoMostrador;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorPressao;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorTemperatura;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoMovimentacao;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.parametrosistema.Variavel;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseConsumo;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseDemanda;
import br.com.ggas.relatorio.ControladorRelatorioClienteUnidadeConsumidora;
import br.com.ggas.relatorio.ControladorRelatorioCobrancaRecuperacaoPenalidades;
import br.com.ggas.relatorio.ControladorRelatorioConsumoClienteMedidor;
import br.com.ggas.relatorio.ControladorRelatorioDeInadimplentes;
import br.com.ggas.relatorio.ControladorRelatorioDescontosConcedidos;
import br.com.ggas.relatorio.ControladorRelatorioFaturamento;
import br.com.ggas.relatorio.ControladorRelatorioFaturasEmitidas;
import br.com.ggas.relatorio.ControladorRelatorioHistoricoCliente;
import br.com.ggas.relatorio.ControladorRelatorioPerfilConsumo;
import br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorData;
import br.com.ggas.relatorio.ControladorRelatorioPosicaoContasReceber;
import br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoAntecedencia;
import br.com.ggas.relatorio.ControladorRelatorioQuantidadesContratadas;
import br.com.ggas.relatorio.ControladorRelatorioResumoVolume;
import br.com.ggas.relatorio.ControladorRelatorioRota;
import br.com.ggas.relatorio.ControladorRelatorioTituloAbertoPorDataVencimento;
import br.com.ggas.relatorio.ControladorRelatorioTitulosAtraso;
import br.com.ggas.relatorio.ControladorRelatorioVolumesFaturadosSegmento;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro.MascaraParametro;
import br.com.ggas.relatorio.exportacaodados.ControladorConsulta;
import br.com.ggas.web.cobranca.parcelamento.ParcelaVO;
import br.com.ggas.web.contrato.contrato.CalculoIndenizacaoVO;
import br.com.ggas.web.contrato.contrato.ModalidadeConsumoFaturamentoVO;
import br.com.ggas.web.contrato.contrato.PenalidadesRetiradaMaiorMenorVO;
import br.com.ggas.web.contrato.programacao.ApuracaoRecuperacaoQuantidadeVO;
import br.com.ggas.web.contrato.programacao.ParadaProgramadaVO;
import br.com.ggas.web.contrato.proposta.FiltroProposta;
import br.com.ggas.web.faturamento.cronograma.CronogramaAtividadeSistemaVO;

/**
 * The Class Fachada.
 */
public final class Fachada {

	/** The instancia. */
	private static Fachada instancia = new Fachada();

	/** The service locator. */
	private static ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	/**
	 * Instantiates a new fachada.
	 */
	private Fachada() {

	}

	/**
	 * Gets the instancia.
	 *
	 * @return the instancia
	 */
	public static Fachada getInstancia() {

		return instancia;
	}

	/**
	 * Método responsável por consultar todos os
	 * parametros do sistema.
	 *
	 * @return Uma lista de parametros.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<ParametroSistema> consultarParametroSistema() throws GGASException {

		return this.getControladorParametroSistema().consultarParametroSistema();
	}

	/**
	 * Método responsável por retornar uma
	 * entidade de cronograma de faturamento.
	 *
	 * @param idCronogramaFaturamento id do cronograma de faturamento
	 *            a ser retornado.
	 * @return the cronograma faturamento
	 * @throws GGASException the GGAS exception
	 */
	public CronogramaFaturamento obterCronogramaFaturamento(Long idCronogramaFaturamento) throws GGASException {

		return this.getControladorCronogramaFaturamento().obterCronogramaFaturamento(idCronogramaFaturamento);
	}

	/**
	 * Método responsável por obter um parametro
	 * do sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um parametro do sistema
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParametroSistema obterParametroSistema(long chavePrimaria) throws GGASException {

		return (ParametroSistema) this.getControladorParametroSistema().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter o parâmetro
	 * através de um código.
	 *
	 * @param parametros the parametros
	 * @return O parâmetro
	 * @throws NegocioException the negocio exception
	 */
	public byte[] gerarPropostaPDF(Map<String, Object> parametros) throws NegocioException {

		return this.getControladorProposta().gerarPropostaPDF(parametros);
	}

	/**
	 * Obter parametro por codigo.
	 *
	 * @param codigo the codigo
	 * @return the parametro sistema
	 * @throws GGASException the GGAS exception
	 */
	public ParametroSistema obterParametroPorCodigo(String codigo) throws GGASException {

		return this.getControladorParametroSistema().obterParametroPorCodigo(codigo);
	}

	/**
	 * Obter constante integracao limite cliente enderecos
	 * (descricao = "P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE").
	 *
	 * @return constante integracao cadastro clientes
	 * @throws NegocioException the negocio exception
	 */
	public ParametroSistema obterParametroIntegracaoLimiteEnderecoCliente() throws NegocioException {

		return this.getControladorParametroSistema().obterParametroIntegracaoLimiteEnderecoCliente();
	}

	/**
	 * Obter tabela menu
	 * @param idMenu - {@link Menu}
	 * @return Tabela
	 * @throws NegocioException - the negocio exception
	 */
	public Tabela obterTabelaMenu(Long idMenu) throws NegocioException {

		return this.getControladorAuditoria().obterTabelaMenu(idMenu);
	}

	/**
	 * Método responsável por consultar as
	 * marcas de coletores.
	 *
	 * @param filtro contendo parametros da pesquisa
	 * @return coleção com marcas de corretor
	 * @throws GGASException the GGAS exception
	 */
	public Collection<MarcaCorretor> consultarMarcaCorretor(Map<String, Object> filtro) throws GGASException {

		return getControladorVazaoCorretor().consultarMarcaCorretor(filtro);
	}

	/**
	 * Método responsável por obter um valor do
	 * parâmetro através de um código.
	 *
	 * @param codigo
	 *            O código do parâmetro.
	 * @param variaveis
	 *            As variáveis que podem ser
	 *            usadas para obter o valor do
	 *            parametro
	 * @return O Valor do parâmetro.
	 * @throws GGASException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public Object obterValorDoParametroPorCodigo(String codigo, Variavel... variaveis) throws GGASException {

		return this.getControladorParametroSistema().obterValorDoParametroPorCodigo(codigo, variaveis);
	}

	/**
	 * Remover devolucao.
	 *
	 * @param devolucao the devolucao
	 * @throws GGASException the GGAS exception
	 */
	public void removerDevolucao(Devolucao devolucao) throws GGASException {

		this.getControladorDevolucao().remover(devolucao);
	}

	/**
	 * Método responsável por alterar um parametro
	 * do sistema.
	 *
	 * @param parametroSistema
	 *            O parametro do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarParametroSistema(ParametroSistema parametroSistema) throws GGASException {

		this.getControladorParametroSistema().atualizar(parametroSistema);
	}

	/**
	 * Método responsável por listar atividades do
	 * sistema pela operação passada por
	 * parãmetro.
	 *
	 * @param idOperacao the id operacao
	 * @return lista de Atividades Sistema
	 * @throws GGASException the GGAS exception
	 */
	public Collection<AtividadeSistema> listarAtividadeSistemaPorOperacao(Long idOperacao) throws GGASException {

		return this.getControladorCronogramaFaturamento().listarAtividadeSistemaPorOperacao(idOperacao);
	}


	/**
	 * Método resposável por autenticar um
	 * usuário.
	 *
	 * @param usuario
	 *            O Usuario
	 * @return Um usuário
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Usuario autenticarUsuario(Usuario usuario) throws GGASException {

		return this.getControladorAcesso().autenticarUsuario(usuario);
	}

	/**
	 * Método para pré-validar o logon do usuário.
	 *
	 * @param usuario the usuario
	 * @return the boolean
	 * @throws GGASException the GGAS exception
	 */
	public Boolean validarLogon(Usuario usuario) throws GGASException {

		return this.getControladorAcesso().validarLogon(usuario);
	}

	/**
	 * Método para recuperar a senha do usuário.
	 *
	 * @param login the login
	 * @param dadosAuditoria the dados auditoria
	 * @return the boolean
	 * @throws GGASException the GGAS exception
	 */
	public Boolean recuperarSenha(String login, DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorUsuario().recuperarSenha(login, dadosAuditoria);
	}

	/**
	 * Método para alterar a senha do usuário.
	 *
	 * @param login the login
	 * @param senhaAtual the senha atual
	 * @param novaSenha the nova senha
	 * @param confirmacaoNovaSenha the confirmacao nova senha
	 * @param dadosAuditoria the dados auditoria
	 * @return the boolean
	 * @throws GGASException the GGAS exception
	 */
	public Boolean alterarSenha(String login, String senhaAtual, String novaSenha, String confirmacaoNovaSenha,
					DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorUsuario().alterarSenha(login, senhaAtual, novaSenha, confirmacaoNovaSenha, dadosAuditoria);
	}

	/**
	 * Método para validar se a senha está
	 * expirada.
	 *
	 * @param usuario the usuario
	 * @throws GGASException the GGAS exception
	 */
	public void verificarSenhaExpirada(Usuario usuario) throws GGASException {

		this.getControladorUsuario().verificarSenhaExpirada(usuario);
	}

	/**
	 * Método responsável por inserir um usuário
	 * no sistema.
	 *
	 * @param usuario
	 *            Entidade Usuário do sistema.
	 * @return Chave primária do usuário inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirUsuario(Usuario usuario) throws GGASException {

		return this.getControladorUsuario().inserir(usuario);
	}

	/**
	 * Obter qdc estabelecido contrato.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicioVigencia the data inicio vigencia
	 * @param dataFimVigencia the data fim vigencia
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * /Método responsável por retornar
	 * o QDC estabelecido no contrato/
	 */
	public BigDecimal obterQDCEstabelecidoContrato(PontoConsumo pontoConsumo, Date dataInicioVigencia, Date dataFimVigencia)
					throws NegocioException {

		BigDecimal qdc = null;

		qdc = getControladorContrato().obterQDCEstabelecidoContrato(pontoConsumo, dataInicioVigencia, dataFimVigencia);

		return qdc;
	}

	/**
	 * Método responsável por inserir um
	 * historicoSenha.
	 *
	 * @param historicoSenha the historico senha
	 * @throws GGASException the GGAS exception
	 */
	public void inserirHistoricoSenha(HistoricoSenha historicoSenha) throws GGASException {

		this.getControladorHistoricoSenha().inserir(historicoSenha);
	}

	/**
	 * Método responsável por atualizar um usuário
	 * no sistema.
	 *
	 * @param usuario
	 *            Entidade Usuário do sistema.
	 * @return Chave primária do usuário inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarUsuario(Usuario usuario) throws GGASException {

		this.getControladorUsuario().atualizarUsuario(usuario);
	}

	/**
	 * Método responsável por atualizar um
	 * CreditoDebitoNegociado no sistema.
	 *
	 * @param creditoDebitoNegociado
	 *            Entidade CreditoDebitoNegociado
	 *            do sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizar(CreditoDebitoNegociado creditoDebitoNegociado) throws GGASException {

		this.getControladorCreditoDebito().atualizar(creditoDebitoNegociado,
						this.getControladorCreditoDebito().getClasseEntidadeCreditoDebitoNegociado());
	}

	/**
	 * Método responsável por verificar se o
	 * usuário possui o papel de
	 * administrador.
	 *
	 * @param usuario
	 *            O usuario logado
	 * @return Boolean True ou False
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean usuarioPossuiPapelAdmin(Usuario usuario) throws GGASException {

		return this.getControladorAcesso().possuiPapelAdmin(usuario);
	}

	/**
	 * Método responsável por buscar a permissão
	 * do usuário em um determinado
	 * modulo/recurso.
	 *
	 * @param usuario
	 *            O usuário que está tentando
	 *            acessar o recurso.
	 * @param operacao
	 *            A operacao que o usuário
	 *            pretente acessar
	 * @return Uma permissão caso o usuário possua
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Permissao buscarPermissaoUsuario(Usuario usuario, Operacao operacao) throws GGASException {

		return this.getControladorAcesso().buscarPermissao(usuario, operacao);
	}

	/**
	 * Método responsável por consultar todos os
	 * papéis dos usuários do sistema.
	 *
	 * @return Uma lista de papéis.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Papel> consultarPapeisUsuario() throws GGASException {

		return this.getControladorUsuario().consultarPapeisUsuario();
	}

	/**
	 * Método responsável por consultar os
	 * cronogramas de faturamento pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de cronogramas de
	 *         faturamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CronogramaFaturamento> consultarCronogramaFaturamento(Map<String, Object> filtro) throws GGASException {

		return getControladorCronogramaFaturamento().consultarCronogramaFaturamento(filtro);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * contrato.
	 *
	 * @param chavesPrimarias As chaves primárias dos modelos
	 *            de contrato.
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverCronogramaRota(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorRota().validarRemoverCronogramaRota(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Listar cronograma rotas.
	 *
	 * @param idAtividade the id atividade
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<CronogramaRota> listarCronogramaRotas(Long idAtividade, Long chavePrimaria) {

		return this.getControladorRota().listarCronogramasRotasPorAtividade(idAtividade, chavePrimaria);
	}

	/**
	 * Método responsável por listar atividades do
	 * sistema por cronograma passado por
	 * parãmetro.
	 *
	 * @return lista de Atividades Sistema por
	 *         Cronograma
	 * @throws GGASException the GGAS exception
	 */
	public Collection<AtividadeSistema> listarAtividadeSistemaPorCronograma() throws GGASException {

		return this.getControladorCronogramaFaturamento().listarAtividadeSistemaPorCronograma();
	}

	/**
	 * Método responsável por consultar os
	 * usuários a partir do filtro
	 * informado.
	 *
	 * @param filtro filtro contendo os parametros da
	 *            consulta.
	 * @param propriedadesLazy the propriedades lazy
	 * @return Uma lista de usuários.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Usuario> consultarUsuarios(Map<String, Object> filtro, String... propriedadesLazy) throws GGASException {

		return this.getControladorUsuario().consultarUsuarios(filtro, propriedadesLazy);
	}

	/**
	 * Método para consultar os usuários.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Usuario> consultarUsuario(Map<String, Object> filtro) throws GGASException {

		return this.getControladorUsuario().consultarUsuario(filtro);
	}

	/**
	 * Método responsável por obter um usuário do
	 * sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um usuário do sistema.
	 */
	public Usuario obterUsuario(long chavePrimaria) {

		return this.getControladorUsuario().obterUsuario(chavePrimaria);
	}

	/**
	 * Método responsável por verificar a
	 * existencia do usuário a ser removido no
	 * sistema.
	 *
	 * @param chavePrimaria
	 *            Chave Primária do usuário do
	 *            sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarUsuarioParaExclusao(Long chavePrimaria) throws GGASException {

		this.getControladorUsuario().verificarUsuarioParaExclusao(chavePrimaria);
	}

	/**
	 * Método responsável por alterar um usuário
	 * do sistema.
	 *
	 * @param usuario the usuario
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarUsuario(Usuario usuario) throws GGASException {

		this.getControladorUsuario().atualizar(usuario);
	}

	/**
	 * Método responsável por excluir um usuário
	 * do sistema.
	 *
	 * @param usuario the usuario
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void excluirUsuario(Usuario usuario) throws GGASException {

		this.getControladorUsuario().remover(usuario);
	}

	/**
	 * Inserir papel.
	 *
	 * @param papel the papel
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirPapel(Papel papel) throws GGASException {

		return this.getControladorPapel().inserir(papel);
	}

	/**
	 * Método responsável por obter um papel do
	 * usuário do sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return Um papel do usuário do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Papel obterPapel(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return this.getControladorUsuario().obterPapel(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter um papel do
	 * usuário do sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the papel
	 * @throws GGASException the GGAS exception
	 */
	public Papel obterPapel(long chavePrimaria) throws GGASException {

		return this.getControladorUsuario().obterPapel(chavePrimaria);
	}

	/**
	 * Método responsável por remover papeis do
	 * sistema.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias dos papeis
	 *            selecionados para
	 *            remoção.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerPapeis(Long[] chavesPrimarias) throws GGASException {

		this.getControladorPapel().removerPapeis(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se algum
	 * papel foi selecionado para
	 * remoção.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias dos papeis
	 *            selecionados.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverPapeis(Long[] chavesPrimarias) throws GGASException {

		this.getControladorPapel().validarRemoverPapeis(chavesPrimarias);
	}

	/**
	 * Método responsável por atualizar um papel
	 * do sistema.
	 *
	 * @param papel
	 *            Papel a ser atualizado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarPapel(Papel papel) throws GGASException {

		this.getControladorPapel().atualizar(papel);
	}

	/**
	 * Método responsável por consultar os papéis
	 * dos usuários do sistema.
	 *
	 * @param filtro the filtro
	 * @return uma coleção de papéis dos usuários
	 *         do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Papel> consultarPapeis(Map<String, Object> filtro) throws GGASException {

		return this.getControladorUsuario().consultarPapeis(filtro);
	}

	/**
	 * Método responsável por buscar a operação ao
	 * qual o recurso está
	 * associado.
	 *
	 * @param recurso
	 *            O recurso acessado
	 * @return Uma operação do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> buscarOperacaoPorRecurso(String recurso) throws GGASException {

		return this.getControladorModulo().buscarOperacaoPorRecurso(recurso);
	}

	/**
	 * Método responsável por buscar a operação
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da operação
	 * @return Uma operação do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Operacao buscarOperacao(Long chavePrimaria) throws GGASException {

		return this.getControladorModulo().buscarOperacaoPorChave(chavePrimaria);
	}

	/**
	 * Método responsável por consultar as
	 * operações batchs do sistema.
	 *
	 * @return Uma coleção de operação.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> consultarOperacoesBatch() throws GGASException {

		return this.getControladorModulo().consultarOperacoesBatch();
	}

	/**
	 * Método responsável por consultar as
	 * operações batchs.
	 *
	 * @param usuario
	 *            O usuário autenticado
	 * @return Uma colecao de operações batchs
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> consultarOperacoesBatch(Usuario usuario) throws GGASException {

		return this.getControladorModulo().consultarOperacoesBatch(usuario);
	}

	/**
	 * Método responsável por consultar Volumes de
	 * execução de contrato.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal Inicial e Final e
	 *            chavesPontoConsumo
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return Uma colecao
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SolicitacaoConsumoPontoConsumo> listarVolumesExecucaoContratoSolicitacao(Date dataInicial, Date dataFinal,
					Long[] chavesPontoConsumo) throws GGASException {

		return this.getControladorProgramacao().consultarVolumeExecucaoContratoSolicitacao(dataInicial, dataFinal, chavesPontoConsumo);
	}

	/**
	 * Método responsável por consultar Volumes de
	 * execução de contrato.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return Uma colecao
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ApuracaoQuantidade>
					listarVolumesExecucaoContratoApuracao(Date dataInicial, Date dataFinal, Long[] chavesPontoConsumo) throws GGASException {

		return this.getControladorProgramacao().consultarVolumeExecucaoContratoApuracao(dataInicial, dataFinal, chavesPontoConsumo);
	}

	/**
	 * Método responsável por consultar Volumes de
	 * execução de contrato.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return Uma colecao
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Recuperacao> listarVolumesExecucaoContratoRecuperacao(Date dataInicial, Date dataFinal, Long[] chavesPontoConsumo)
					throws GGASException {

		return this.getControladorProgramacao().consultarVolumeExecucaoContratoRecuperacao(dataInicial, dataFinal, chavesPontoConsumo);
	}

	/**
	 * Método responsável por atualizar o módulo.
	 *
	 * @param modulo
	 *            O modulo acessado
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarModulo(Modulo modulo) throws GGASException {

		this.getControladorModulo().atualizar(modulo);
	}

	/**
	 * Método responsável por inserir um módulo no
	 * sistema.
	 *
	 * @param modulo O modulo a ser inserido
	 * @return the long
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirModulo(Modulo modulo) throws GGASException {

		return this.getControladorModulo().inserir(modulo);
	}

	/**
	 * Método responsável por remover módulos do
	 * sistema.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias dos módulos
	 *            selecionados para
	 *            remoção.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerModulos(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().removerModulos(chavesPrimarias);
	}

	/**
	 * Método responsável por validar se algum
	 * modulo foi selecionado para
	 * remoção.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException the GGAS exception
	 */
	public void validarRemoverModulos(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().validarRemoverModulos(chavesPrimarias);
	}

	/**
	 * Método responsável por cria um módulo.
	 *
	 * @return the modulo
	 */
	public Modulo criarModulo() {

		return (Modulo) this.getControladorModulo().criar();
	}

	/**
	 * Método responsável por consultar os módulos
	 * a partir do filtro informado.
	 *
	 * @param filtro
	 *            filtro para consulta
	 * @return coleção de modulos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Modulo> consultarModulos(Map<String, String> filtro) throws GGASException {

		return this.getControladorModulo().consultarModulos(filtro);
	}

	/**
	 * Método responsável por consultar módulos do
	 * sistema pelas chaves
	 * primarias.
	 *
	 * @param chavesPrimarias Chaves primarias dos módulos
	 * @return coleção de modulos.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamentoPorChave(Long[] chavesPrimarias)
					throws GGASException {

		return this.getControladorHistoricoAnomaliaFaturamento().consultarHistoricoAnomaliaFaturamentoPorChaves(chavesPrimarias);
	}

	/**
	 * Método responsável por consultar módulos do
	 * sistema pelas chaves
	 * primarias.
	 *
	 * @param chavesPrimarias Chaves primarias dos módulos
	 * @return coleção de modulos.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Modulo> consultarModulosPorChaves(Long[] chavesPrimarias) throws GGASException {

		return this.getControladorModulo().consultarModulosPorChaves(chavesPrimarias);
	}

	/**
	 * Método responsável por consultar as
	 * operações batchs.
	 *
	 * @param idModulo
	 *            A chave primária de um módulo do
	 *            sistema
	 * @return Uma coleção de operações
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> consultarOperacoesBatchPorModulo(long idModulo) throws GGASException {

		return this.getControladorModulo().consultarOperacoesBatchPorModulo(idModulo);
	}

	/**
	 * Método responsável por obter um módulo do
	 * sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um módulo do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Modulo modulo(long chavePrimaria) throws GGASException {

		return (Modulo) this.getControladorModulo().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter um Contrato
	 * Ponto Consumo Modalidade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um módulo do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterContratoPontoConsumoModalidade(chavePrimaria);
	}

	/**
	 * Método responsável por consultar as
	 * operações do sistema.
	 *
	 * @param chaveModulo
	 *            Chave primária do módulo.
	 * @return coleção de operações do sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> consultarOperacoes(Long chaveModulo) throws GGASException {

		return this.getControladorModulo().consultarOperacoes(chaveModulo);
	}

	/**
	 * Método responsável por consultar as
	 * operações do sistema de acordo com um
	 * filtro de chaves primárias.
	 *
	 * @param chavesPrimarias
	 *            Filtro para a consulta
	 * @return coleção de operações do sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Operacao> consultarOperacoes(Long[] chavesPrimarias) throws GGASException {

		return this.getControladorModulo().consultarOperacoes(chavesPrimarias);
	}

	/**
	 * Método responsável por remover operações do
	 * sistema.
	 *
	 * @param chavesPrimarias
	 *            chaves primárias das operações
	 *            selecionadas para
	 *            exclusão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerOperacoes(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().removerOperacoes(chavesPrimarias);
	}

	/**
	 * Validar remover operacoes.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException the GGAS exception
	 */
	public void validarRemoverOperacoes(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().validarRemoverOperacoes(chavesPrimarias);
	}

	/**
	 * Método responsável por cria uma operação.
	 *
	 * @return the operacao
	 */
	public Operacao criarOperacao() {

		return (Operacao) this.getControladorModulo().criarOperacao();
	}

	/**
	 * Criar menu.
	 *
	 * @return the menu
	 */
	public Menu criarMenu() {

		return (Menu) this.getControladorMenu().criar();
	}

	/**
	 * Método responsável por criar um recurso.
	 *
	 * @return Um recurso do sistema
	 */
	public Recurso criarRecurso() {

		return (Recurso) this.getControladorModulo().criarRecurso();
	}

	/**
	 * Método responbsável por remover um recurso
	 * do sistema.
	 *
	 * @param chavesPrimarias
	 *            chaves primárias dos recursos
	 *            selecionados para
	 *            exclusão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerRecurso(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().removerRecursos(chavesPrimarias);
	}

	/**
	 * Método responsável por validas se algum
	 * recurso foi selecionado para
	 * exclusão.
	 *
	 * @param chavesPrimarias
	 *            chaves primárias dos recursos
	 *            selecionados para
	 *            exclusão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverRecursos(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModulo().validarRemoverRecursos(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um módulo cadastrado com a
	 * mesma descrição.
	 *
	 * @param modulo
	 *            Módulo a ser verificado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarSeExisteModuloCadastrado(Modulo modulo) throws GGASException {

		this.getControladorModulo().verificarSeExisteModuloCadastrado(modulo);
	}

	/**
	 * Método responsável por verificar se já
	 * existe uma operação com o mesmo
	 * nome e tipo cadastrada no sistema.
	 *
	 * @param modulo
	 *            Módulo onde é recuperada as
	 *            operações existentes.
	 * @param operacao
	 *            Operação a ser incluída.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarSeExisteOperacaoCadastrada(Modulo modulo, Operacao operacao) throws GGASException {

		this.getControladorModulo().verificarSeExisteOperacaoCadastrada(modulo, operacao);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um recurso com a mesma
	 * descrição cadastrado no sistema.
	 *
	 * @param operacao the operacao
	 * @param recurso the recurso
	 * @throws GGASException the GGAS exception
	 */
	public void verificarSeExisteRecursoCadastrado(Operacao operacao, Recurso recurso) throws GGASException {

		this.getControladorModulo().verificarSeExisteRecursoCadastrado(operacao, recurso);
	}

	/**
	 * Método responsável por buscar um recurso
	 * pela chave primária.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the recurso
	 * @throws GGASException the GGAS exception
	 */
	public Recurso buscarRecursoPorChave(Long chavePrimaria) throws GGASException {

		return this.getControladorModulo().buscarRecursoPorChave(chavePrimaria);
	}

	/**
	 * Método responsável por consultar os
	 * recursos do sistema através da
	 * operação.
	 *
	 * @param filtro the filtro
	 * @return coleção de recursos do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Recurso> consultarRecursos(Map<String, Object> filtro) throws GGASException {

		return this.getControladorModulo().consultarRecursos(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * processos do sistema.
	 *
	 * @param filtro Um flitro de consulta
	 * @return Uma coleção de processos
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Pair<Long, List<Processo>> consultarProcessos(Map<String, Object> filtro) throws GGASException {

		return this.getControladorProcesso().consultarProcessos(filtro);
	}

	/**
	 * Método responsável por criar um processo.
	 *
	 * @return Um novo processo
	 */
	public Processo criarProcesso() {

		return (Processo) this.getControladorProcesso().criar();
	}

	/**
	 * Método responsável por obter um processo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um novo processo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Processo obterProcesso(Long chavePrimaria) throws GGASException {

		return (Processo) this.getControladorProcesso().obter(chavePrimaria);
	}

	/**
	 * Método responsável por inserir um processo.
	 *
	 * @param processo O processo
	 * @return a chave primária
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirProcesso(Processo processo) throws GGASException {

		return this.getControladorProcesso().inserir(processo);
	}

	/**
	 * Método responsável por alterar um processo.
	 *
	 * @param processo O processo
	 * @return a chave primária
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarProcesso(Processo processo) throws GGASException {

		this.getControladorProcesso().atualizar(processo);
	}

	/**
	 * Método responsável por excluir um processo.
	 *
	 * @param processo O processo
	 * @return a chave primária
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void excluirProcesso(Processo processo) throws GGASException {

		this.getControladorProcesso().remover(processo);
	}

	/**
	 * Método responsável por obter a
	 * periodicidade do processo.
	 *
	 * @param codigo O código da periodicidade
	 * @return Uma Periodicidade
	 */
	public PeriodicidadeProcesso obterPeriodicidadeProcesso(int codigo) {

		return this.getControladorProcesso().obterPeriodicidadeProcesso(codigo);
	}

	/**
	 * Método responsável por obter todas as
	 * situações do processo.
	 *
	 * @return Uma coleção com situações
	 */
	public Collection<SituacaoProcesso> listarSituacaoProcesso() {

		return this.getControladorProcesso().listarSituacaoProcesso();
	}

	/**
	 * Método responsável por obter uma situação
	 * do processo.
	 *
	 * @param codigo O código da situação
	 * @return Uma situação
	 */
	public SituacaoProcesso obterSituacaoProcesso(int codigo) {

		return this.getControladorProcesso().obterSituacaoProcesso(codigo);
	}

	/**
	 * Método responsável por criar um Funcionário.
	 *
	 * @return Funcionário um funcionário do
	 *         sistema.
	 */
	public Funcionario criarFuncionario() {

		return (Funcionario) this.getControladorFuncionario().criar();
	}

	/**
	 * Método responsável por criar uma Fatura.
	 *
	 * @return Funcionário um funcionário do
	 *         sistema.
	 */
	public Fatura criarFatura() {

		return (Fatura) this.getControladorFatura().criar();
	}

	/**
	 * Método responsável por criar uma Empresa.
	 *
	 * @return Empresa uma empresa do sistema.
	 */
	public Empresa criarEmpresa() {

		return (Empresa) this.getControladorEmpresa().criar();
	}

	/**
	 * Método responsável por buscar o funcionário
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do funcionário
	 * @return Um Funcionário
	 * @throws NegocioException 
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Funcionario buscarFuncionarioPorChave(Long chavePrimaria) throws NegocioException {

		return (Funcionario) this.getControladorFuncionario().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a unidade
	 * organizacional pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do funcionário
	 * @return Uma UnidadeOrganizacional
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public UnidadeOrganizacional buscarUnidadeOrganizacionalPorChave(Long chavePrimaria) throws GGASException {

		return (UnidadeOrganizacional) this.getControladorUnidadeOrganizacional().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o perfil
	 * quadra pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do funcionário
	 * @return Um PerfilQuadra
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PerfilQuadra buscarPerfilQuadraPorChave(Long chavePrimaria) throws GGASException {

		return (PerfilQuadra) this.getControladorPerfilQuadra().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Funcionario.
	 *
	 * @param funcionario
	 *            Um funcionário do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarFuncionario(Funcionario funcionario) throws GGASException {

		this.getControladorFuncionario().atualizar(funcionario);
	}

	/**
	 * Método responsável por atualizar a unidade
	 * organizacional.
	 *
	 * @param unidadeOrganizacional the unidade organizacional
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) throws GGASException {

		this.getControladorUnidadeOrganizacional().atualizar(unidadeOrganizacional);
	}

	/**
	 * Método responsável por inserir um
	 * Funcionário.
	 *
	 * @param funcionario the funcionario
	 * @return chavePrimeria do funcionario
	 *         inserido
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirFuncionario(Funcionario funcionario) throws GGASException {

		return this.getControladorFuncionario().inserir(funcionario);
	}

	/**
	 * Método responsável por remover os
	 * funcionários.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos funcionários
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerFuncionarios(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorFuncionario().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover funcionários.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos funcionários
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverFuncionarios(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFuncionario().validarRemoverFuncionarios(chavesPrimarias);
	}

	/**
	 * Método responsável por consultar os
	 * funcionários pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de funcionários.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Funcionario> consultarFuncionarios(Map<String, Object> filtro) throws GGASException {

		return getControladorFuncionario().consultarFuncionarios(filtro);
	}

	/**
	 * Método responsável por obter uma empresa
	 * pela chave passada por parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return Empresa.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Empresa obterEmpresa(Long chave) throws NegocioException {

		return (Empresa) getControladorEmpresa().obter(chave);
	}

	/**
	 * Método responsável por obter um recebimento
	 * pela chave passada pelo parâmetro.
	 *
	 * @param chave the chave
	 * @return recebimento
	 * @throws NegocioException the GGAS exception
	 */
	public Recebimento obterRecebimento(Long chave) throws NegocioException {

		return (Recebimento) this.getControladorRecebimento().obter(chave);
	}

	/**
	 * Método responsável por obter um
	 * recebimento.
	 *
	 * @param chave the chave
	 * @param propriedadesLazy As propriedades que serão
	 *            carregadas via lazy
	 * @return Um recebimento
	 * @throws NegocioException Caso ocora algum erro na
	 *             execução do método.
	 */
	public Recebimento obterRecebimento(Long chave, String... propriedadesLazy) throws NegocioException {

		return (Recebimento) this.getControladorRecebimento().obter(chave, propriedadesLazy);
	}

	/**
	 * Método responsável por criar um
	 * recebimentoo.
	 *
	 * @return recebimento
	 * @throws GGASException the GGAS exception
	 */
	public Recebimento criarRecebimento() {

		return (Recebimento) this.getControladorRecebimento().criar();
	}

	/**
	 * Método responsável por inserir um
	 * recebimento.
	 *
	 * @param recebimento the recebimento
	 * @param chavesPrimariasFaturas the chaves primarias faturas
	 * @param dadosAuditoria the dados auditoria
	 * @return Chaves primárias dos recebimentos
	 *         inseridos.
	 * @throws GGASException the GGAS exception
	 */
	public Long[] inserirRecebimento(Recebimento recebimento, Long[] chavesPrimariasFaturas, DadosAuditoria dadosAuditoria)
					throws GGASException {

		return this.getControladorRecebimento().inserirRecebimento(recebimento, chavesPrimariasFaturas, dadosAuditoria);
	}

	/**
	 * Método responsável por estornar uma lista
	 * de recebimentos.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void estornarRecebimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorRecebimento().estornarRecebimento(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por obter uma unidade
	 * organizacional pela chave passada por
	 * parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return UnidadeOrganizacional.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public UnidadeOrganizacional obterUnidadeOrganizacional(Long chave) throws NegocioException {

		return (UnidadeOrganizacional) getControladorUnidadeOrganizacional().obter(chave);
	}

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de funcionários.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<UnidadeOrganizacional> consultarUnidadeOrganizacional(Map<String, Object> filtro) throws NegocioException {

		return getControladorUnidadeOrganizacional().consultarUnidadeOrganizacional(filtro);
	}

	/**
	 * Método responsável por consultar os perfis
	 * quadra pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de perils quadra.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PerfilQuadra> consultarPerfilQuadra(Map<String, Object> filtro) throws NegocioException {

		return getControladorPerfilQuadra().consultarPerfilQuadra(filtro);
	}

	/**
	 * Método responsável por consultar as
	 * Gerencias Regionais pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de gerencia regional
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */

	public Collection<GerenciaRegional> consultarGerenciaRegional(Map<String, Object> filtro) throws NegocioException {

		return getControladorGerenciaRegional().consultarGerenciaRegional(filtro);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * TipoCliente.
	 *
	 * @param tipoCliente
	 *            Um tipo de cliente do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarTipoCliente(TipoCliente tipoCliente) throws GGASException {

		this.getControladorCliente().atualizar(tipoCliente, TipoCliente.class);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * PerfilQuadra.
	 *
	 * @param perfilQuadra
	 *            Um perfil quadra do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarPerfilQuadra(PerfilQuadra perfilQuadra) throws GGASException {

		this.getControladorPerfilQuadra().atualizar(perfilQuadra, PerfilQuadra.class);
	}

	/**
	 * Método responsável por remover os perfis
	 * quadra.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos perfis cliente.
	 * @param dadosAuditoria
	 *            DadosAuditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerPerfisQuadra(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorPerfilQuadra().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por inserir uma empresa
	 * no sistema.
	 *
	 * @param empresa A empresa a ser inserida
	 * @return the long
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirEmpresa(Empresa empresa) throws NegocioException {

		return this.getControladorEmpresa().inserir(empresa);
	}

	/**
	 * Método responsável por consultar as faixas
	 * de área construída pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de faixas de área
	 *         construída.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Empresa> consultarEmpresas(Map<String, Object> filtro) throws NegocioException {

		return getControladorEmpresa().consultarEmpresas(filtro);
	}

	/**
	 * Método responsável por buscar a empresa
	 * pela chave primária.
	 *
	 * @param chavePrimaria A chave primária da empresa
	 * @return Uma empresa cadastrada
	 * @throws NegocioException the GGAS exception
	 */
	public Empresa buscarEmpresaPorChave(Long chavePrimaria) throws NegocioException {

		return (Empresa) this.getControladorEmpresa().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a gerencia
	 * regional pela chave primária.
	 *
	 * @param chavePrimaria A chave primária da gerencia
	 *            regional
	 * @return Uma gerencia reginonal
	 * @throws NegocioException the GGAS exception
	 */
	public GerenciaRegional buscarGerenciaRegionalPorChave(Long chavePrimaria) throws NegocioException {

		return (GerenciaRegional) this.getControladorGerenciaRegional().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Empresa.
	 *
	 * @param empresa Uma Empresa do sistema
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarEmpresa(Empresa empresa) throws GGASException {

		this.getControladorEmpresa().atualizar(empresa);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada alguma empresa para
	 * exclusão.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das empresas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarEmpresasSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorEmpresa().validarEmpresasSelecionadas(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas uma empresa
	 * para atualização.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das empresas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorEmpresa().validarSelecaoDeAtualizacao(chavesPrimarias);
	}

	/**
	 * Método responsável por remover as empresas.
	 *
	 * @param chavesPrimarias
	 *            Chaves das empresas.
	 * @param dadosAuditoria
	 *            Dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerEmpresas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorEmpresa().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por remover as unidades
	 * organizacionais.
	 *
	 * @param chavesPrimarias Chaves das unidades
	 *            organizacionais.
	 * @param auditoria the auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerUnidadesOrganizacionais(Long[] chavesPrimarias, DadosAuditoria auditoria) throws GGASException {

		this.getControladorUnidadeOrganizacional().remover(chavesPrimarias, auditoria);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover Unidade Organizacional.
	 *
	 * @param chavesPrimarias
	 *            Chaves das unidades
	 *            organizacionais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverUnidadeOrganizacional(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorUnidadeOrganizacional().validarRemoverUnidadeOrgazinacional(chavesPrimarias);
	}

	/**
	 * Método responsável por criar uma faixa de
	 * área contruída.
	 *
	 * @return AreaConstruidaFaixa
	 */
	public AreaConstruidaFaixa criarAreaConstruidaFaixa() {

		return (AreaConstruidaFaixa) this.getControladorAreaConstruidaFaixa().criar();
	}

	/**
	 * Criar faixa consumo variacao.
	 *
	 * @return the faixa consumo variacao
	 */
	public FaixaConsumoVariacao criarFaixaConsumoVariacao() {

		return (FaixaConsumoVariacao) this.getControladorFaixaConsumoVariacao().criar();
	}

	/**
	 * Método responsável por criar uma unidade
	 * organizacional.
	 *
	 * @return AreaConstruidaFaixa
	 */
	public UnidadeOrganizacional criarUnidadeOrganizacional() {

		return (UnidadeOrganizacional) this.getControladorUnidadeOrganizacional().criar();
	}

	/**
	 * Método responsável por criar uma gerencia
	 * regional.
	 *
	 * @return GerenciaRegional
	 */
	public GerenciaRegional criarGerenciaRegional() {

		return (GerenciaRegional) this.getControladorGerenciaRegional().criar();
	}

	/**
	 * Método responsável por criar um Erp.
	 *
	 * @return ERP
	 * @throws NegocioException the GGAS exception
	 */
	public Erp criarErp() {

		return (Erp) this.getControladorErp().criar();
	}

	/**
	 * Método responsável por inserir um Erp.
	 *
	 * @param erp the erp
	 * @return chavePrimaria de Erp inserido
	 * @throws NegocioException the Negocio exception
	 */
	public Long inserirErp(Erp erp) throws NegocioException {

		return this.getControladorErp().inserir(erp);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * ERP.
	 *
	 * @param erp Um Erp do sistema
	 * @throws ConcorrenciaException the Concorrencia exception
	 * @throws NegocioException the Negocio exception
	 */
	public void atualizarERP(Erp erp) throws ConcorrenciaException, NegocioException  {

		this.getControladorErp().atualizar(erp);
	}

	/**
	 * Método responsável por consultar as
	 * localidades das ERPs cadastradas.
	 *
	 * @return coleção das localidades das ERP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Localidade> consultarLocalidadesDasERP() throws NegocioException {

		return getControladorErp().consultarLocalidadesDasERP();
	}

	/**
	 * Método responsável por consultar as ERPs
	 * pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção ERP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Erp> consultarERP(Map<String, Object> filtro) throws NegocioException {

		return getControladorErp().consultarERP(filtro);
	}

	/**
	 * Método responsável por buscar a ERP/ERPM
	 * pela chave primária.
	 *
	 * @param chavePrimaria A chave primária da ERP/ERPM
	 * @return Uma ERP/ERPM cadastrada no sistema.
	 * @throws NegocioException the GGAS exception
	 */
	public Erp buscarERPPorChave(Long chavePrimaria) throws NegocioException {

		return (Erp) this.getControladorErp().obter(chavePrimaria);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada alguma ERP para
	 * exclusão.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das ERPs
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarERPsSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorErp().validarERPsSelecionadas(chavesPrimarias);
	}

	/**
	 * Método responsável por remover a(s) ERP(s)
	 * do sistema.
	 *
	 * @param chavesPrimarias Chaves das ERPs.
	 * @param dadosAuditoria Dados da auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerERPs(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorErp().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por consultar as faixas
	 * de área construída pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de faixas de área
	 *         construída.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AreaConstruidaFaixa> consultarAreaConstruidaFaixas(Map<String, Object> filtro) throws NegocioException {

		return getControladorAreaConstruidaFaixa().consultarAreaConstruidaFaixas(filtro);
	}

	/**
	 * Método responsável por inserir uma faixa de
	 * área construída.
	 *
	 * @param areaConstruidaFaixa the area construida faixa
	 * @return chavePrimeria da faixa de área
	 *         construída inserida
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirAreaConstruidaFaixa(AreaConstruidaFaixa areaConstruidaFaixa) throws NegocioException {

		return this.getControladorAreaConstruidaFaixa().inserir(areaConstruidaFaixa);
	}

	/**
	 * Método responsável por inserir uma unidade
	 * organizacional.
	 *
	 * @param unidadeOrganizacional the unidade organizacional
	 * @return chavePrimeria da unidade
	 *         organizacinal inserida
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) throws NegocioException {

		return this.getControladorUnidadeOrganizacional().inserir(unidadeOrganizacional);
	}

	/**
	 * Método responsável por listar os canais de
	 * atendimento disponível no sistema.
	 *
	 * @return coleção de meios de solicitação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CanalAtendimento> listarCanalAtendimento() throws NegocioException {

		return getControladorUnidadeOrganizacional().listarCanalAtendimento();
	}

	/**
	 * Método responsável por obter o meio de
	 * solicitação da unidade organizacional.
	 *
	 * @param chavePrimaria
	 *            A chave primaria do meio de
	 *            solicitação.
	 * @return meio de solicitação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CanalAtendimento obterCanalAtendimento(Long chavePrimaria) throws NegocioException {

		return getControladorUnidadeOrganizacional().obterCanalAtendimento(chavePrimaria);
	}

	/**
	 * Método responsável por listar os tipos de
	 * unidades organizacionais.
	 *
	 * @return coleção dos tipos das unidades
	 *         organizacionais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<UnidadeTipo> listarUnidadeTipo() throws NegocioException {

		return getControladorUnidadeOrganizacional().listarUnidadeTipo();
	}

	/**
	 * Listar unidades centralizadoras.
	 *
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<UnidadeOrganizacional> listarUnidadesCentralizadoras() throws NegocioException {

		return this.getControladorUnidadeOrganizacional().listarUnidadesCentralizadoras();
	}

	/**
	 * Método responsável por listar todas as
	 * unidades organizacionais superiores.
	 *
	 * @return coleção de unidades organizacionais
	 *         superiores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<UnidadeOrganizacional> listarUnidadesSuperiores() throws NegocioException {

		return getControladorUnidadeOrganizacional().listarUnidadesSuperiores();
	}

	/**
	 * Método responsável por obter o tipo da
	 * unidade organizacional.
	 *
	 * @param chavePrimaria
	 *            A chave primaria do tipo da
	 *            unidade.
	 * @return tipo da unidade organizacional.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public UnidadeTipo obterUnidadeTipo(Long chavePrimaria) throws NegocioException {

		return getControladorUnidadeOrganizacional().obterUnidadeTipo(chavePrimaria);
	}

	/**
	 * Método responsável por inserir uma gerencia
	 * regional.
	 *
	 * @param gerenciaRegional the gerencia regional
	 * @return chavePrimaria da gerencia regional
	 *         inserida
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirGerenciaRegional(GerenciaRegional gerenciaRegional) throws NegocioException {

		return this.getControladorGerenciaRegional().inserir(gerenciaRegional);
	}

	/**
	 * Método responsável por buscar a faixa de
	 * área construída pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da faixa de
	 *            área construída
	 * @return Uma faixa de área construída
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AreaConstruidaFaixa buscarAreaConstruidaFaixaPorChave(Long chavePrimaria) throws NegocioException {

		return (AreaConstruidaFaixa) this.getControladorAreaConstruidaFaixa().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * AreaConstruidaFaixa.
	 *
	 * @param areaConstruidaFaixa
	 *            Uma faixa de área construída do
	 *            sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarAreaConstruidaFaixa(AreaConstruidaFaixa areaConstruidaFaixa) throws GGASException {

		this.getControladorAreaConstruidaFaixa().atualizar(areaConstruidaFaixa);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover faixas de área construída.
	 *
	 * @param chavesPrimarias Chaves das faixas de área
	 *            construída.
	 * @throws NegocioException the GGAS exception
	 */
	public void validarRemoverAreaConstruidaFaixas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorAreaConstruidaFaixa().validarRemoverAreaConstruidaFaixas(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover gerencia(s) regional(ais).
	 *
	 * @param chavesPrimarias
	 *            Chaves das gerencias regionais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverGerenciaRegional(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorGerenciaRegional().validarRemoverGerenciaRegional(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar gerência regional.
	 *
	 * @param chavesPrimarias
	 *            Chaves das gerencias regionais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAtualizarGerenciaRegional(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorGerenciaRegional().validarAtualizarGerenciaRegional(chavesPrimarias);
	}

	/**
	 * Consultar cronograma ativ faturamento por cronograma faturamento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(Long chavePrimaria)
					throws NegocioException {

		return this.getControladorCronogramaFaturamento().consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(chavePrimaria);
	}

	/**
	 * Metodo responsavel por retornar a
	 * quantidade de rotas por grupo faturamento.
	 *
	 * @param idRota the id rota
	 * @return a quantidade de rotas do grupo
	 *         faturamento consultado
	 * @throws NegocioException the GGAS exception
	 */
	public int quantidadeLeituraPorRota(Long idRota) throws NegocioException {

		return this.getControladorPontoConsumo().quantidadeLeituraPorRota(idRota);
	}

	/**
	 * Metodo responsavel por retornar a
	 * quantidade de rotas por grupo faturamento.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @return a quantidade de rotas do grupo
	 *         faturamento consultado
	 * @throws NegocioException the GGAS exception
	 */
	public int quantidadeMedidoresPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		return this.getControladorPontoConsumo().quantidadeMedidoresPorGrupoFaturamento(idGrupoFaturamento);
	}

	/**
	 * Metodo responsavel por retornar a
	 * quantidade de rotas por grupo faturamento.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @return a quantidade de rotas do grupo
	 *         faturamento consultado
	 * @throws NegocioException the GGAS exception
	 */
	public int quantidadeRotasPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		return this.getControladorRota().quantidadeRotasPorGrupoFaturamento(idGrupoFaturamento);
	}

	/**
	 * Método responsável por remover uma ou mais
	 * Gerencia Regional.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerGerenciaRegional(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorGerenciaRegional().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por atualizar uma
	 * Gerencia Regional.
	 *
	 * @param gerenciaRegional the gerencia regional
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarGerenciaRegional(GerenciaRegional gerenciaRegional) throws GGASException {

		this.getControladorGerenciaRegional().atualizar(gerenciaRegional);
	}

	/**
	 * Método responsável por remover as faixas de
	 * área construída.
	 *
	 * @param chavesPrimarias
	 *            Chaves das faixas de área
	 *            construída.
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerAreaConstruidaFaixas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorAreaConstruidaFaixa().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por criar um
	 * VolumeReservatorio.
	 *
	 * @return VolumeReservatorio
	 * @throws NegocioException the GGAS exception
	 */
	public VolumeReservatorio criarVolumeReservatorio() {

		return (VolumeReservatorio) this.getControladorVolumeReservatorio().criar();
	}

	/**
	 * Método responsável por criar um
	 * TipoCliente.
	 *
	 * @return TipoCliente
	 */
	public TipoCliente criarTipoCliente() {

		return (TipoCliente) this.getControladorCliente().criarTipoCliente();
	}

	/**
	 * Método responsável por criar uma Tarifa.
	 *
	 * @return Tarifa
	 * @throws NegocioException the GGAS exception
	 */
	public Tarifa criarTarifa() {

		return (Tarifa) this.getControladorTarifa().criar();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaVigencia.
	 *
	 * @return TarifaVigencia {@link TarifaVigencia}
	 * @throws NegocioException {@link NegocioException}
	 */
	public TarifaVigencia criarTarifaVigencia() throws NegocioException {

		return (TarifaVigencia) this.getControladorTarifa().criarTarifaVigencia();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaVigenciaFaixa.
	 *
	 * @return TarifaVigenciaFaixa
	 * @throws NegocioException the GGAS exception
	 */
	public TarifaVigenciaFaixa criarTarifaVigenciaFaixa() throws NegocioException {

		return (TarifaVigenciaFaixa) this.getControladorTarifa().criarTarifaVigenciaFaixa();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaVigenciaDesconto.
	 *
	 * @return TarifaVigenciaDesconto
	 * @throws NegocioException the GGAS exception
	 */
	public TarifaVigenciaDesconto criarTarifaVigenciaDesconto() throws NegocioException {

		return (TarifaVigenciaDesconto) this.getControladorTarifa().criarTarifaVigenciaDesconto();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaFaixaDesconto.
	 *
	 * @return TarifaFaixaDesconto
	 * @throws NegocioException the GGAS exception
	 */
	public TarifaFaixaDesconto criarTarifaFaixaDesconto() throws NegocioException {

		return (TarifaFaixaDesconto) this.getControladorTarifa().criarTarifaFaixaDesconto();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaTributo.
	 * a
	 *
	 * @return PerfilQuadra
	 * @throws NegocioException the GGAS exception
	 */
	public TarifaVigenciaTributo criarTarifaTributo() throws NegocioException {

		return (TarifaVigenciaTributo) this.getControladorTarifa().criarTarifaTributo();
	}

	/**
	 * Método responsável por criar uma
	 * TarifaFaixaDesconto.
	 *
	 * @return the dados faixas tarifa
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosFaixasTarifa criarDadosFaixasTarifa() throws NegocioException {

		return this.getControladorTarifa().criarDadosFaixasTarifa();
	}

	/**
	 * Método responsável por criar uma
	 * SupervisorioMedicaoHoraria.
	 *
	 * @return the entidade negocio
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeNegocio criarSupervisorioMedicaoHoraria() {

		return this.getControladorSupervisorio().criarSupervisorioMedicaoHoraria();
	}

	/**
	 * Método responsável por criar um
	 * PerfilQuadra.
	 *
	 * @return PerfilQuadra
	 * @throws NegocioException the GGAS exception
	 */
	public PerfilQuadra criarPerfilQuadra() {

		return (PerfilQuadra) this.getControladorPerfilQuadra().criar();
	}

	/**
	 * Método responsável por inserir um
	 * VolumeReservatorio.
	 *
	 * @param volumeReservatorio the volume reservatorio
	 * @return chave primária do volume
	 *         reservatorio inserido
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirVolumeReservatorio(VolumeReservatorio volumeReservatorio) throws NegocioException {

		return this.getControladorVolumeReservatorio().inserir(volumeReservatorio);
	}

	/**
	 * Método responsável por consultar os volume
	 * reservatorios pelo filtro
	 * informado.
	 *
	 * @param filtro contendo os parametros da
	 *            pesquisa.
	 * @return coleção de volume reservatorios.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<VolumeReservatorio> consultarVolumeReservatorios(Map<String, Object> filtro) throws NegocioException {

		return getControladorVolumeReservatorio().consultarVolumeReservatorios(filtro);
	}

	/**
	 * Método responsável por um volume
	 * reservatorio por chave primaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the volume reservatorio
	 * @throws NegocioException the GGAS exception
	 */
	public VolumeReservatorio buscarVolumeReservatorioPorChave(Long chavePrimaria) throws NegocioException {

		return (VolumeReservatorio) this.getControladorVolumeReservatorio().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar um volume
	 * reservatorio.
	 *
	 * @param volumeReservatorio the volume reservatorio
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarVolumeReservatorio(VolumeReservatorio volumeReservatorio) throws GGASException {

		this.getControladorVolumeReservatorio().atualizar(volumeReservatorio);
	}

	/**
	 * Método responsável por remover um volume
	 * reservatorio.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria Dados da auditoria
	 * @throws NegocioException the GGAS exception
	 */
	public void removerVolumeReservatorio(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorVolumeReservatorio().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover volume reservatorios.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws NegocioException the GGAS exception
	 */
	public void validarRemoverVolumeReservatorios(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorVolumeReservatorio().validarRemoverVolumeReservatorios(chavesPrimarias);
	}


	/**
	 * Método responsável por inserir um
	 * TipoCliente.
	 *
	 * @param tipoCliente - {@link TipoCliente}
	 * @return chave primária do tipo cliente
	 *         inserido
	 * @throws NegocioException
	 * 			the Negocio Exception
	 */
	public Long inserirTipoCliente(TipoCliente tipoCliente) throws NegocioException {

		return this.getControladorCliente().inserir(tipoCliente);
	}

	/**
	 * Método responsável por inserir um
	 * PerilQuadra.
	 *
	 * @param perfilQuadra the perfil quadra
	 * @return chave primária do perfilQuadra
	 *         inserido
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirPerfilQuadra(PerfilQuadra perfilQuadra) throws NegocioException {

		return this.getControladorPerfilQuadra().inserir(perfilQuadra);
	}

	/**
	 * Método responsável por listar todas as
	 * unidades federação.
	 *
	 * @return coleção de UnidadeFederacao.
	 */
	public Collection<UnidadeFederacao> listarTodasUnidadeFederacao() {

		return getControladorEndereco().listarTodasUnidadeFederacao();
	}

	/**
	 * Método responsável por consultar as
	 * localidades pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Localidade> consultarLocalidades(Map<String, Object> filtro) throws NegocioException {

		return getControladorLocalidade().consultarLocalidades(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * municípios pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de municipios.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Municipio> consultarMunicipios(Map<String, Object> filtro) throws NegocioException {

		return getControladorMunicipio().consultarMunicipios(filtro);
	}
	
	
	/**
	 * Método responsável por consultar os
	 * bairros pelo filtro informado.
	 * @param nomeMunicipio
	 * 			nome do município para filtrar os bairros
	 * @return Collection<String>
	 * 			nome dos bairros
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<String> consultarBairros(String nomeMunicipio) {

		return this.getControladorEndereco().consultarBairros(nomeMunicipio);
	}

	/**
	 * Método responsável por buscar a localidade
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da localidade.
	 * @return Uma localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Localidade buscarLocalidadeChave(Long chavePrimaria) throws NegocioException {

		return (Localidade) this.getControladorLocalidade().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o imóvel pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do imóvel.
	 * @return Um Imovel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Imovel buscarImovelPorChave(Long chavePrimaria) throws NegocioException {

		return (Imovel) this.getControladorImovel().obter(chavePrimaria, "quadraFace", "quadraFace.endereco", "quadraFace.endereco.cep",
						"quadra");
	}

	/**
	 * Método responsável por buscar o município
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do município.
	 * @return Um muncípio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Municipio buscarMunicipioChave(Long chavePrimaria) throws NegocioException {

		return (Municipio) this.getControladorMunicipio().obter(chavePrimaria);
	}

	/**
	 * Método responsável por criar uma
	 * Localidade.
	 *
	 * @return Localidade uma localidade do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Localidade criarLocalidade() {

		return (Localidade) this.getControladorLocalidade().criar();
	}

	/**
	 * Método responsável por criar um Ramo
	 * Atividade.
	 *
	 * @return RamoAtividade um ramo atividade do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RamoAtividade criarRamoAtividade() {

		return (RamoAtividade) this.getControladorRamoAtividade().criar();
	}

	/**
	 * Método responsável por inserir um Ramo
	 * Atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @return chavePrimaria do ramo atividade
	 *         inserido.
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException {

		return this.getControladorRamoAtividade().inserir(ramoAtividade);
	}

	/**
	 * Método responsável por consultar os Ramos
	 * Atividades pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de ramo atividade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */

	public Collection<RamoAtividade> consultarRamoAtividade(Map<String, Object> filtro) throws NegocioException {

		return getControladorRamoAtividade().consultarRamoAtividade(filtro);
	}

	/**
	 * Método responsável por consultar o(s)
	 * Ramal(is) pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de Ramal
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */

	public Collection<Ramal> consultarRamal(Map<String, Object> filtro) throws NegocioException {

		return getControladorRamal().consultarRamal(filtro);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas um ramal para
	 * atualização.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias dos ramais
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarSelecaoDeAtualizacaoRamal(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorRamal().validarSelecaoDeAtualizacao(chavesPrimarias);
	}

	/**
	 * Método responsável por obter um ramal pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            Chave primária do tronco.
	 * @return Ramal.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Ramal buscarRamalPorChave(Long chavePrimaria) throws NegocioException {

		return (Ramal) this.getControladorRamal().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Ramal.
	 *
	 * @param ramal Um Ramal do sistema
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarRamal(Ramal ramal) throws GGASException {

		this.getControladorRamal().atualizar(ramal);
	}

	/**
	 * Método responsável por remover os ramais.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos ramais.
	 * @param dadosAuditoria
	 *            Dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerRamais(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorRamal().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada algum ramal para
	 * exclusão.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias dos Ramais
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRamaisSelecionados(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorRamal().validarRamaisSelecionados(chavesPrimarias);
	}

	/**
	 * Método responsável por criar um Municipio.
	 *
	 * @return Municipio um município do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Municipio criarMunicipio() {

		return (Municipio) this.getControladorMunicipio().criar();
	}

	/**
	 * Método responsável por criar um
	 * SetorComercial.
	 *
	 * @return SetorComercial um setor comercial
	 *         do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SetorComercial criarSetorComercial() {

		return (SetorComercial) this.getControladorSetorComercial().criar();
	}

	/**
	 * Método responsável por criar uma Quadra.
	 *
	 * @return quadra uma quadra do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Quadra criarQuadra() {

		return (Quadra) this.getControladorQuadra().criar();
	}

	/**
	 * Método responsável por criar uma Face da
	 * Quadra.
	 *
	 * @return quadra uma face da quadra do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public QuadraFace criarQuadraFace() throws NegocioException {

		return (QuadraFace) this.getControladorQuadra().criarQuadraFace();
	}

	/**
	 * Método responsável por inserir um setor
	 * comercial.
	 *
	 * @param setorComercial the setor comercial
	 * @return chavePrimeria do setor comercial
	 *         inserido.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirSetorComercial(SetorComercial setorComercial) throws NegocioException {

		return this.getControladorSetorComercial().inserir(setorComercial);
	}

	/**
	 * Método responsável por inserir uma quadra
	 * no sisema.
	 *
	 * @param quadra
	 *            Quadra a ser inserida no
	 *            sistema.
	 * @return chavePrimeria Chave da quadra
	 *         inserida.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirQuadra(Quadra quadra) throws NegocioException {

		return this.getControladorQuadra().inserir(quadra);
	}

	/**
	 * Método responsável por consultar os setores
	 * comerciais pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de setores comerciais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SetorComercial> consultarSetorComercial(Map<String, Object> filtro) throws NegocioException {

		return getControladorSetorComercial().consultarSetorComercial(filtro);
	}

	/**
	 * Método responsável por consultar os setores
	 * censitarios pelo filtro
	 * informado.
	 *
	 * @param filtro contendo parametros da pesquisa
	 * @return coleção de setores censitarios
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<SetorCensitario> consultarSetorCensitario(Map<String, Object> filtro) throws NegocioException {

		return getControladorSetorCensitario().consultarSetorCensitario(filtro);
	}

	/**
	 * Método responsável por consultar zona
	 * bloqueio pelo filtro
	 * informado.
	 *
	 * @param filtro com paramentros da pesquisa
	 * @return coleção de zona bloqueio
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ZonaBloqueio> consultarZonaBloqueio(Map<String, Object> filtro) throws NegocioException {

		return getControladorZonaBloqueio().consultarZonaBloqueio(filtro);
	}

	/**
	 * Método responsável por consultar os tipos
	 * de areas pelo filtro
	 * informado.
	 *
	 * @param filtro contendo parametros da pesquisa
	 * @return coleção com areatipos
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<AreaTipo> consultarAreaTipo(Map<String, Object> filtro) throws NegocioException {

		return getControladorAreaTipo().consultarAreaTipo(filtro);
	}

	/**
	 * Método responsável por buscar o setor
	 * comercial pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do setor
	 *            comercial.
	 * @return Um setor comercial.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SetorComercial buscarSetorComercialChave(Long chavePrimaria) throws NegocioException {

		return (SetorComercial) this.getControladorSetorComercial().obter(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * SetorComercial.
	 *
	 * @param setorComercial
	 *            Um setor comercial do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarSetorComercial(SetorComercial setorComercial) throws GGASException {

		this.getControladorSetorComercial().atualizar(setorComercial);
	}

	/**
	 * Método responsável por remover os setores
	 * comerciais.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos setores comerciais.
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerSetoresComerciais(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorSetorComercial().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover os setores comerciais.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos setores comerciais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverSetoresComerciais(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorSetorComercial().validarRemoverSetoresComerciais(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar um setor comercial.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos setores comerciais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAtualizarSetorComercial(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorSetorComercial().validarAtualizarSetorComercial(chavesPrimarias);
	}

	/**
	 * Método responsável por criar um Segmento.
	 *
	 * @return Segmento um segmento do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Segmento criarSegmento() throws NegocioException {

		return (Segmento) this.getControladorSegmento().criar();
	}

	/**
	 * Método responsável por inserir um segmento.
	 *
	 * @param segmento the segmento
	 * @return chavePrimeria do segmento inserido.
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirSegmento(Segmento segmento) throws NegocioException {

		return this.getControladorSegmento().inserir(segmento);
	}

	/**
	 * Método responsável por consultar os
	 * segmentos pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de segmentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Segmento> consultarSegmento(Map<String, Object> filtro) throws NegocioException {

		return getControladorSegmento().consultarSegmento(filtro);
	}

	/**
	 * Consultar segmento inclusao faixa.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Segmento> consultarSegmentoInclusaoFaixa(Map<String, Object> filtro) throws NegocioException {

		return getControladorSegmento().consultarSegmentoInclusaoFaixa(filtro);
	}

	/**
	 * Método responsável por buscar o segmento
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do segmento.
	 * @return Um segmento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Segmento buscarSegmentoChave(Long chavePrimaria) throws NegocioException {

		return (Segmento) this.getControladorSegmento().obter(chavePrimaria, "segmentoAmostragemPCSs", "segmentoIntervaloPCSs",
						"ramosAtividades");

	}

	/**
	 * Método responsável por buscar o segmento
	 * pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do segmento.
	 * @return Um segmento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoAdequacaoParceria buscarContratoAdequacaoParceriaPorId(Long chavePrimaria) throws NegocioException {

		return this.getControladorContratoAdequacaoParceria().consultarContratoAdequacaoParceriaPorChave(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Segmento.
	 *
	 * @param segmento Um segmento do sistema
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarSegmento(Segmento segmento, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorSegmento().atualizarSegmento(segmento, dadosAuditoria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Segmento.
	 *
	 * @param segmento Um segmento do sistema
	 * @param dadosAuditoria the dados auditoria
	 * @return the long
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirSegmentoRamoAtividade(Segmento segmento, DadosAuditoria dadosAuditoria) throws NegocioException {

		return this.getControladorSegmento().inserirSegmentoRamoAtividade(segmento, dadosAuditoria);
	}

	/**
	 * Método responsável por remover os
	 * segmentos.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos segmentos.
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerSegmentos(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorSegmento().removerSegmentos(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover os segmentos.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos segmentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverSegmentos(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorSegmento().validarRemoverSegmentos(chavesPrimarias);
	}

	/**
	 * Método responsável por listar todos os
	 * tipos de segmento.
	 *
	 * @return Coleção de tipos de segmento.
	 */
	public Collection<TipoSegmento> listarTodosTipoSegmento() {

		return this.getControladorSegmento().listarTodosTipoSegmento();
	}

	/**
	 * Método responsável por obter o tipo de
	 * segmento pela chave primária informada.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            segmento.
	 * @return Um TipoSegmento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoSegmento obterTipoSegmento(Long chavePrimaria) throws NegocioException {

		return this.getControladorSegmento().obterTipoSegmento(chavePrimaria);
	}

	/**
	 * Método responsável por consultar as quadras
	 * pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção quadras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Quadra> consultarQuadras(Map<String, Object> filtro) throws NegocioException {

		return getControladorQuadra().consultarQuadras(filtro);
	}

	/**
	 * Método responsável por consultar os Zeis
	 * vinculados as quadras
	 * cadastradas.
	 *
	 * @return coleção de zeis das quadras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Zeis> consultarZeisDasQuadras() throws NegocioException {

		return getControladorQuadra().consultarZeisDasQuadras();
	}

	/**
	 * Método responsável por consultar os perfis
	 * vinculados as quadras cadastradas.
	 *
	 * @return coleção de perfis das quadras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PerfilQuadra> consultarPerfilDasQuadrasCadastradas() throws NegocioException {

		return getControladorQuadra().consultarPerfilDasQuadrasCadastradas();
	}

	/**
	 * Método responsável por consultar os perfis
	 * quadra pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de perils quadra.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Zeis> consultarZeis(Map<String, Object> filtro) throws NegocioException {

		return getControladorZeis().consultarZeis(filtro);
	}

	/**
	 * Método responsável por buscar zeis pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do zeis
	 * @return Um Zeis
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Zeis buscarZeisPorChave(Long chavePrimaria) throws NegocioException {

		return (Zeis) this.getControladorZeis().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a quadra pela
	 * chave primária.
	 *
	 * @param chavePrimaria A chave primária da quadra
	 * @return Uma quadra cadastrada no sistema.
	 * @throws NegocioException the GGAS exception
	 */
	public Quadra buscarQuadraPorChave(Long chavePrimaria) throws NegocioException {

		return (Quadra) this.getControladorQuadra().obter(chavePrimaria, "quadrasFace");
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Quadra.
	 *
	 * @param quadra Uma Quadra do sistema
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarQuadra(Quadra quadra) throws GGASException {

		this.getControladorQuadra().atualizarQuadra(quadra);
	}

	/**
	 * Método responsável por remover as quadras
	 * do sistema.
	 *
	 * @param chavesPrimarias Chaves das quadras.
	 * @param dadosAuditoria Dados da auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerQuadras(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorQuadra().removerQuadras(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada alguma quadra para
	 * exclusão.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das quadras
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarQuadrasSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorQuadra().validarQuadrasSelecionadas(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas uma Quadra para
	 * alteração.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das Quadras
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarSelecaoDeAtualizacaoQuadra(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorQuadra().validarSelecaoDeAtualizacao(chavesPrimarias);
	}

	/**
	 * Método responsável por validar os dados da
	 * entidade QuadraFace.
	 *
	 * @param quadraFace Uma QuadraFace do sistema
	 * @throws NegocioException the GGAS exception
	 */
	public void validarDadosQuadraFace(QuadraFace quadraFace) throws NegocioException {

		this.getControladorQuadra().validarDadosQuadraFace(quadraFace);
	}

	/**
	 * Método responsável por validar data atual
	 * consistir.
	 *
	 * @param historicoMedicao the historico medicao
	 * @throws NegocioException the GGAS exception
	 */
	public void validarDataAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException {

		this.getControladorHistoricoMedicao().validarDataAtualInformada(historicoMedicao);
	}

	/**
	 * Método responsável por validar leitura atual
	 * consistir.
	 *
	 * @param historicoMedicao the historico medicao
	 * @throws NegocioException the GGAS exception
	 */
	public void validarLeituraAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException {

		this.getControladorHistoricoMedicao().validarLeituraAtualInformada(historicoMedicao);
	}

	/**
	 * Método responsável por verificar se já
	 * existe uma face da quadra com o mesmo
	 * número de
	 * endereço na
	 * lista de faces da quadra.
	 *
	 * @param indexLista
	 *            O índice da face da quadra na
	 *            lista.
	 * @param listaQuadraFace
	 *            Lista de faces da quadra da
	 *            quadra.
	 * @param quadraFace
	 *            Face da quadra a ser verificada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarNumeroQuadraFace(Integer indexLista, Collection<QuadraFace> listaQuadraFace, QuadraFace quadraFace)
					throws NegocioException {

		this.getControladorQuadra().validarNumeroQuadraFace(indexLista, listaQuadraFace, quadraFace);
	}

	/**
	 * Método responsável por obter um tronco pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            Chave primária do tronco.
	 * @return Tronco.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Tronco buscarTroncoPorChave(Long chavePrimaria) throws NegocioException {

		return (Tronco) this.getControladorTronco().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma rede pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            Chave primária do rede.
	 * @return Rede.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rede buscarRedePorChave(Long chavePrimaria) throws NegocioException {

		return (Rede) this.getControladorRede().obter(chavePrimaria);
	}

	/**
	 * Método responsável por consultar troncos.
	 *
	 * @param filtro
	 *            Filtro contendo os parametros da
	 *            consulta.
	 * @return Coleção de tronco.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Tronco> consultarTronco(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorTronco().consultarTronco(filtro);
	}

	/**
	 * Método responsável por criar um ramal.
	 *
	 * @return Ramal criado.
	 */
	public Ramal criarRamal() {

		return (Ramal) this.getControladorRamal().criar();
	}

	/**
	 * Método ersponsável por inserir um ramal.
	 *
	 * @param ramal Ramal a ser inserido.
	 * @return the long
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirRamal(Ramal ramal) throws NegocioException {

		return this.getControladorRamal().inserir(ramal);
	}

	/**
	 * Método responsável por obter um CEP pela
	 * chave primária informada.
	 *
	 * @param chavePrimaria Chave primária do CEP.
	 * @return Uma entidade Cep.
	 * @throws NegocioException the GGAS exception
	 */
	public Cep obterCepPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorEndereco().obterCepPorChave(chavePrimaria);
	}

	/**
	 * Método que consulta a tabela de Ceps pela
	 * UF, Município e logradouro
	 * informados como parâmetro.
	 *
	 * @param uf the uf
	 * @param municipio the municipio
	 * @param logradouro the logradouro
	 * @return listaDeCeps
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Cep> consultarCeps(String uf, String municipio, String logradouro) throws NegocioException {

		return this.getControladorEndereco().consultarCeps(uf, municipio, logradouro);
	}

	/**
	 * Método que importa os Ceps informados no
	 * arquivo selecionado na tela de
	 * importar cep.
	 *
	 * @param arquivo InputStream
	 * @param tipoArquivo String
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Integer> importarCep(InputStream arquivo, String tipoArquivo) throws GGASException {

		return this.getControladorEndereco().importarCep(arquivo, tipoArquivo);
	}

	/**
	 * Método responsável por obter o controlador
	 * de endereço.
	 *
	 * @param cep the cep
	 * @return ControladorEndereco O controlador
	 *         de endereço.
	 * @throws NegocioException the GGAS exception
	 * @deprecated Este método só retorna um cep
	 *             mesmo que este tenha mais de um
	 *             endereço. Usar
	 *             obterCeps(String cep).
	 */
	@Deprecated
	public Cep obterCep(String cep) throws NegocioException {

		return this.getControladorEndereco().obterCep(cep);
	}

	/**
	 * Método responsável por obter todos os CEPs
	 * pelo número de CEP.
	 *
	 * @param cep Número do CEP.
	 * @param habilitado the habilitado
	 * @return Um coleção de CEPs.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<Cep> obterCeps(String cep, boolean habilitado) throws NegocioException {

		return this.getControladorEndereco().obterCeps(cep, habilitado);
	}

	/**
	 * Método responsável por consultar as redes.
	 *
	 * @param filtro Um filtro de consulta
	 * @return Uma coleção de redes
	 * @throws NegocioException Caso ocora algum erro na
	 *             execução do método.
	 */
	public Collection<Rede> consultarRedes(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorRede().consultarRedes(filtro);
	}

	/**
	 * Método responsável por consultar as redes.
	 *
	 * @return Uma coleção de redes
	 * @throws NegocioException Caso ocora algum erro na
	 *             execução do método.
	 */
	@SuppressWarnings("unchecked")
	public Collection<Rede> obterTodasAsRedes() throws NegocioException {

		return (Collection<Rede>) this.getControladorRede().obterTodas();
	}

	/**
	 * Método responsável por obter uma rede.
	 *
	 * @param chavePrimaria
	 *            A chave primária da rede
	 * @return Uma rede
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public Rede obterRede(Long chavePrimaria) throws NegocioException {

		return (Rede) this.getControladorRede().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma rede.
	 *
	 * @param chavePrimaria
	 *            A chave primária da rede
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy.
	 * @return Uma rede
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public Rede obterRede(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Rede) this.getControladorRede().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter o indicador de
	 * rede.
	 *
	 * @param codigo O código do indicador
	 * @return the rede indicador
	 * @throws NegocioException Caso ocora algum erro na
	 *             execução do método.
	 */
	public RedeIndicador obterRedeIndicador(Integer codigo) throws NegocioException {

		return this.getControladorRede().obterRedeIndicador(codigo);
	}

	/**
	 * Método responsável por criar um endereço.
	 *
	 * @return Um endereço
	 * @throws NegocioException the GGAS exception
	 */
	public Endereco criarEndereco() throws NegocioException {

		return this.getControladorEndereco().criarEndereco();
	}

	/**
	 * Método responsável por criar um cep.
	 *
	 * @return Um cep
	 * @throws NegocioException the GGAS exception
	 */
	public Cep criarCep() throws NegocioException {

		return (Cep) this.getControladorEndereco().criarCep();
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover localidade(s).
	 *
	 * @param chavesPrimarias
	 *            Chaves das localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverLocalidades(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorLocalidade().validarRemoverLocalidades(chavesPrimarias);
	}

	/**
	 * Método responsável por remover as
	 * localidades.
	 *
	 * @param chavesPrimarias
	 *            Chaves das localidades.
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerLocalidades(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorLocalidade().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por consultar os
	 * clientes pessoa jurídica.
	 *
	 * @param cnpj O CNPJ co cliente
	 * @param nome O nome do cliente
	 * @return Uma coleção de clientes
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Cliente> consultarClientesPessoaJuridica(String cnpj, String nome) throws NegocioException {

		return getControladorCliente().consultarClientesPessoaJuridica(cnpj, nome);
	}

	/**
	 * Método responsável por consultar os imoveis
	 * de acordo com o filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de imovel.
	 * @return Uma coleção de imoveis.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Imovel> consultarImoveis(Map<String, Object> filtro) throws NegocioException {

		return getControladorImovel().consultarImoveis(filtro);
	}

	/**
	 * Método que consulta os imóveis filhos do
	 * condominio passado pelo parâmetro.
	 *
	 * @param imovel the imovel
	 * @return ListaImoveis
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Imovel> obterImoveisFilhosDoCondominio(Imovel imovel) throws NegocioException {

		return getControladorImovel().obterImoveisFilhosDoCondominio(imovel);
	}

	/**
	 * Método implementado para atender a regra [RN004]: Só deverá haver um cliente por tipo de relacionamento (usuário, proprietário,
	 * responsável) associado a um imóvel.
	 * @param clienteImovel {@link - ClienteImovel}
	 * @param atualizar {@link - Boolean}
	 * @param listaClienteImovel the lista cliente imovel
	 * @throws NegocioException the GGAS exception
	 */
	public void validarClientesRelacionados(ClienteImovel clienteImovel, Collection<ClienteImovel> listaClienteImovel, Boolean atualizar)
			throws NegocioException {

		getControladorImovel().validarClientesRelacionados(clienteImovel, listaClienteImovel, atualizar);
	}

	/**
	 * Método responsável por obter o cliente
	 * principal do imovel.
	 *
	 * @param idImovel
	 *            A chave primária do imóvel
	 * @return Um Cliente Imovel
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ClienteImovel obterClienteImovelPrincipal(Long idImovel) throws NegocioException {

		return getControladorImovel().obterClienteImovelPrincipal(idImovel);
	}

	/**
	 * Método responsável por obter o cliente
	 * principal do imovel.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return Um Cliente Imovel
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente obterClientePrincipal(Long idPontoConsumo) throws NegocioException {

		return getControladorCliente().obterClientePrincipal(idPontoConsumo);
	}

	/**
	 * Método reponsável por validar se o imovel
	 * possui uma lista de ponto de consumos.
	 *
	 * @param listaPontosConsumo A lista de pontos de consumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarImovelSemPontoConsumo(Collection<PontoConsumo> listaPontosConsumo) throws NegocioException {

		getControladorImovel().validarImovelSemPontoConsumo(listaPontosConsumo);
	}

	/**
	 * Validar corretor vazao sem ponto consumo.
	 *
	 * @param listaVazaoCorretor the lista vazao corretor
	 * @throws NegocioException the negocio exception
	 */
	public void validarCorretorVazaoSemPontoConsumo(Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretor) throws NegocioException {

		getControladorVazaoCorretor().validarCorretorVazaoSemPontoConsumo(listaVazaoCorretor);
	}

	/**
	 * Método responsável por consultar os
	 * clientes de acordo com o filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de cliente.
	 * @return Uma coleção de clientes.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Cliente> consultarClientes(Map<String, Object> filtro) throws NegocioException {

		return getControladorCliente().consultarClientes(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * clientes que tem contrato de acordo com o
	 * filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de cliente.
	 * @return Uma coleção de clientes.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Cliente> consultarClientesContrato(Map<String, Object> filtro) throws NegocioException {

		return getControladorCliente().consultarClientesContrato(filtro);
	}

	/**
	 * Método responsável por remover clientes.
	 *
	 * @param chavesPrimarias As chaves primárias dos clientes
	 *            selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerClientes(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorCliente().removerClientes(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por remover IntegracaoClientes.
	 *
	 * @param chavesPrimarias As chaves primárias das IntegracaoClientes
	 *            selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerIntegracaoClientes(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorIntegracao().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * rotas.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias das rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverRotas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorRota().validarRemoverRotas(chavesPrimarias);
	}

	/**
	 * Método responsável por listar os tipos de
	 * clientes existentes no sistema.
	 *
	 * @return coleção de tipos de clientes.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<TipoCliente> listarTipoCliente() throws NegocioException {

		return getControladorCliente().listarTipoCliente();
	}

	/**
	 * Método responsável por obter um tipo de
	 * cliente.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um tipo de cliente
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoCliente obterTipoCliente(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterTipoCliente(chavePrimaria);
	}

	/**
	 * Método responsável em obter a situação do
	 * cliente.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Uma situação do cliente
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ClienteSituacao obterClienteSituacao(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterClienteSituacao(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma profissão.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma profissão
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Profissao obterProfissao(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterProfissao(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma renda
	 * familiar.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Uma faixa de renda familiar
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaixaRendaFamiliar obterFaixaRendaFamiliar(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterFaixaRendaFamiliar(chavePrimaria);
	}

	/**
	 * Método reponsável por listar os sexos.
	 *
	 * @return Uma coleção de sexo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PessoaSexo> listarPessoaSexo() throws NegocioException {

		return getControladorCliente().listarPessoaSexo();
	}

	/**
	 * Método responsável por obter o sexo.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um sexo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PessoaSexo obterPessoaSexo(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterPessoaSexo(chavePrimaria);
	}

	/**
	 * Obter nacionalidade.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return uma nacionalidade
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Nacionalidade obterNacionalidade(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterNacionalidade(chavePrimaria);
	}

	/**
	 * Obter atividade economica.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return uma atividade economica
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AtividadeEconomica obterAtividadeEconomica(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterAtividadeEconomica(chavePrimaria);
	}

	/**
	 * Obter cliente.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return um cliente
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente obterCliente(long chavePrimaria) throws NegocioException {

		return (Cliente) getControladorCliente().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter um cliente.
	 *
	 * @param chavePrimaria
	 *            A chave primaria do cliente
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy
	 * @return Um cliente
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public Cliente obterCliente(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Cliente) getControladorCliente().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por buscar o imóvel pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do imóvel.
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy.
	 * @return Um Imovel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Imovel buscarImovelPorChave(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Imovel) this.getControladorImovel().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por listar as situações
	 * dos clientes existentes no sistema.
	 *
	 * @return coleção de situações dos clientes.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ClienteSituacao> listarClienteSituacao() throws NegocioException {

		return getControladorCliente().listarClienteSituacao();
	}

	/**
	 * Método responsável por listar as unidades
	 * da federação dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de unidades federação.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<UnidadeFederacao> listarUF() throws NegocioException {

		return getControladorEndereco().listarTodasUnidadeFederacao();
	}

	/**
	 * Método responsável por obter uma unidade
	 * federação.
	 *
	 * @param chavePrimaria
	 *            o Código da unidade federação.
	 * @return Um UnidadeFederacao
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public UnidadeFederacao obterUnidadeFederacao(Long chavePrimaria) throws NegocioException {

		return getControladorEndereco().obterUnidadeFederacao(chavePrimaria);
	}

	/**
	 * Método responsável por listar as profissões
	 * dos clientes existentes no sistema.
	 *
	 * @return coleção de profissões.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Profissao> listarProfissao() throws NegocioException {

		return getControladorCliente().listarProfissao();
	}

	/**
	 * Método responsável por listar as
	 * nacionalidades dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de nacionalidades.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Nacionalidade> listarNacionalidade() throws NegocioException {

		return getControladorCliente().listarNacionalidade();
	}

	/**
	 * Método responsável por listar as atividades
	 * economicas dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de atividades economicas.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<AtividadeEconomica> listarAtividadeEconomica() throws NegocioException {

		return getControladorCliente().listarAtividadeEconomica();
	}

	/**
	 * Método responsável por listar os tipos de
	 * endereços dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de tipos de endereços.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<TipoEndereco> listarTipoEndereco() throws NegocioException {

		return getControladorCliente().listarTipoEndereco();
	}

	/**
	 * Método responsável por listar os tipos de
	 * telefones dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de tipos de telefones.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<TipoFone> listarTipoFone() throws NegocioException {

		return getControladorCliente().listarTipoFone();
	}

	/**
	 * Método responsável por listar os tipos de
	 * contatos dos clientes existentes no
	 * sistema.
	 *
	 * @return coleção de tipos de contatos.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<TipoContato> listarTipoContato() throws NegocioException {

		return getControladorCliente().listarTipoContato();
	}

	/**
	 * Método responsável por listar os orgãos
	 * expedidores.
	 *
	 * @return coleção dos orgãos expedidores.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<OrgaoExpedidor> listarOrgaoExpedidor() throws NegocioException {

		return getControladorCliente().listarOrgaoExpedidor();
	}

	/**
	 * Método responsável por obter o orgão
	 * expedidor.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um orgão expedidor
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public OrgaoExpedidor obterOrgaoExpedidor(long chavePrimaria) throws NegocioException {

		return getControladorCliente().obterOrgaoExpedidor(chavePrimaria);
	}

	/**
	 * Método responsável por listar a faixa da
	 * renda familiar.
	 *
	 * @return coleção da faixa da renda familiar
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<FaixaRendaFamiliar> listarFaixaRendaFamiliar() throws NegocioException {

		return getControladorCliente().listarFaixaRendaFamiliar();
	}

	/**
	 * Método responsável por listar os endereços
	 * de referências possíveis.
	 *
	 * @return coleção de endereços referências.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<EnderecoReferencia> listarEnderecoReferencia() throws NegocioException {

		return getControladorEndereco().listarEnderecoReferencia();
	}

	/**
	 * Método responsável por inserir um cliente
	 * no sistema.
	 *
	 * @param cliente
	 *            Cliente a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do cliente
	 *         inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirCliente(Cliente cliente) throws GGASException {

		return this.getControladorCliente().inserirCliente(cliente);
	}

	/**
	 * Método responsável por inserir uma IntegracaoCliente
	 * no sistema.
	 *
	 * @param integracaoCliente
	 *            IntegracaoCliente a ser inserido no
	 *            sistema.
	 * @return chavePrimaria Chave da IntegracaoCliente
	 *         inserida.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirIntegracaoCliente(IntegracaoCliente integracaoCliente) throws NegocioException {

		return this.getControladorIntegracao().inserir(integracaoCliente);
	}

	/**
	 * Método responsável por alterar um cliente
	 * no sistema.
	 *
	 * @param cliente
	 *            Cliente a ser inserido no
	 *            sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarCliente(Cliente cliente) throws GGASException {

		this.getControladorCliente().atualizarCliente(cliente);
	}

	/**
	 * Método responsável por alterar um cliente no sistema.
	 *
	 * @param atualizarContrato - {@link boolean}
	 * @param cliente Cliente a ser inserido no sistema.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public void alterarCliente(Cliente cliente, boolean atualizarContrato) throws GGASException {

		this.getControladorCliente().atualizarCliente(cliente, atualizarContrato);
	}

	/**
	 * Método responsável por alterar uma IntegracaoCliente no sistema.
	 *
	 * @param integracaoCliente IntegracaoCliente a ser inserida no sistema.
	 * @return chavePrimaria Chave da integracaoCliente inserida.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public void alterarIntegracaoCliente(IntegracaoCliente integracaoCliente) throws GGASException {

		this.getControladorIntegracao().atualizar(integracaoCliente);
	}

	/**
	 * Método responsável por criar um cliente.
	 *
	 * @return cliente um cliente do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente criarCliente() throws NegocioException {

		return (Cliente) this.getControladorCliente().criar();
	}

	/**
	 * Método responsável por criar uma IntegracaoCliente.
	 *
	 * @return integracaoCliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public IntegracaoCliente criarIntegracaoCliente() throws NegocioException {

		return (IntegracaoCliente) this.getControladorIntegracao().criar();
	}

	/**
	 * Método responsável por criar uma Ação de
	 * Cobrança.
	 *
	 * @return AcaoCobranca uma Ação de Cobrança
	 *         do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AcaoCobranca criarAcaoCobranca() throws NegocioException {

		return (AcaoCobranca) this.getControladorAcaoCobranca().criar();
	}

	/**
	 * Método responsável por inserir uma Ação de
	 * Cobrança no sistema.
	 *
	 * @param acaoCobranca
	 *            AcaoCobranca a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do cliente
	 *         inserido.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirAcaoCobranca(AcaoCobranca acaoCobranca) throws NegocioException {

		return this.getControladorAcaoCobranca().inserir(acaoCobranca);
	}

	/**
	 * Método responsável por alterar uma Ação de
	 * Cobrança no sistema.
	 *
	 * @param acaoCobranca
	 *            AcaoCobranca a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do cliente
	 *         inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarAcaoCobranca(AcaoCobranca acaoCobranca) throws GGASException {

		this.getControladorAcaoCobranca().atualizar(acaoCobranca);
	}

	/**
	 * Método responsável por excluir uma ou mais
	 * Ações de Cobrança no sistema.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @return chavePrimeria Chave do cliente
	 *         inserido.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerAcaoCobranca(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorAcaoCobranca().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por criar um endereço de
	 * cliente.
	 *
	 * @return clienteEndereco um endereco do
	 *         cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ClienteEndereco criarClienteEndereco() throws NegocioException {

		return (ClienteEndereco) this.getControladorCliente().criarClienteEndereco();
	}

	/**
	 * Criar cliente anexo.
	 *
	 * @return the cliente anexo
	 * @throws NegocioException the GGAS exception
	 */
	public ClienteAnexo criarClienteAnexo() throws NegocioException {

		return (ClienteAnexo) this.getControladorCliente().criarClienteAnexo();
	}

	/**
	 * Método responsável por criar um telefone de
	 * cliente.
	 *
	 * @return clienteFone um telefone do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ClienteFone criarClienteFone() throws NegocioException {

		return (ClienteFone) this.getControladorCliente().criarClienteFone();
	}

	/**
	 * Método responsável por criar um contato do
	 * cliente.
	 *
	 * @return contatoCliente um contato do
	 *         cliente do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContatoCliente criarContatoCliente() throws NegocioException {

		return (ContatoCliente) this.getControladorCliente().criarContatoCliente();
	}

	/**
	 * Método responsável por obter o tipo de
	 * contato do cliente.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo
	 *            contato.
	 * @return o tipo de contato do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoContato buscarTipoContato(Long chavePrimaria) throws NegocioException {

		return getControladorCliente().buscarTipoContato(chavePrimaria);
	}

	/**
	 * Método responsável por obter o tipo de
	 * telefone do cliente.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo do
	 *            telefone.
	 * @return Um tipo de telefone do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoFone buscarTipoFone(Long chavePrimaria) throws NegocioException {

		return getControladorCliente().buscarTipoFone(chavePrimaria);
	}

	/**
	 * Método responsável por obter o tipo de
	 * endereço do cliente.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            endereço.
	 * @return Um tipo de endereço do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoEndereco buscarTipoEndereco(Long chavePrimaria) throws NegocioException {

		return getControladorCliente().buscarTipoEndereco(chavePrimaria);
	}

	/**
	 * Método responsável por validar os dados do
	 * contato do cliente.
	 *
	 * @param contato
	 *            O contato do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosContatoCliente(ContatoCliente contato) throws NegocioException {

		this.getControladorCliente().validarDadosContatoCliente(contato);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um contato no cliente com o mesmo
	 * email.
	 *
	 * @param indexLista the index lista
	 * @param listaContatoCliente A lista contendo os contatos do
	 *            cliente a serem verificados.
	 * @param contatoCliente O contato do cliente a ser
	 *            incluído.
	 * @throws NegocioException aso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarEmailContatoExistente(Integer indexLista, Collection<ContatoCliente> listaContatoCliente,
					ContatoCliente contatoCliente) throws NegocioException {

		this.getControladorCliente().verificarEmailContatoExistente(indexLista, listaContatoCliente, contatoCliente);
	}

	/**
	 * Método responsável por validar os dados do
	 * endereço do cliente.
	 *
	 * @param clienteEndereco
	 *            O endereço do cliente.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosClienteEndereco(ClienteEndereco clienteEndereco) throws NegocioException {

		this.getControladorCliente().validarDadosClienteEndereco(clienteEndereco);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um endereço no cliente com o mesmo
	 * número.
	 *
	 * @param indexLista the index lista
	 * @param listaClienteEndereco A lista contendo os endereços do
	 *            cliente a serem verificados.
	 * @param clienteEndereco O endereco do cliente a ser
	 *            incluído.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarNumeroClienteEnderecoExistente(Integer indexLista, Collection<ClienteEndereco> listaClienteEndereco,
					ClienteEndereco clienteEndereco) throws NegocioException {

		this.getControladorCliente().verificarNumeroClienteEnderecoExistente(indexLista, listaClienteEndereco, clienteEndereco);
	}

	/**
	 * Verificar descricao anexo.
	 *
	 * @param indexLista the index lista
	 * @param listaClienteAnexo the lista cliente anexo
	 * @param clienteAnexo the cliente anexo
	 * @throws NegocioException the negocio exception
	 */
	public void verificarDescricaoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
					throws NegocioException {

		this.getControladorCliente().verificarDescricaoAnexo(indexLista, listaClienteAnexo, clienteAnexo);
	}

	/**
	 * Verificar tipo anexo anexo.
	 *
	 * @param indexLista the index lista
	 * @param listaClienteAnexo the lista cliente anexo
	 * @param clienteAnexo the cliente anexo
	 * @throws NegocioException the negocio exception
	 */
	public void verificarTipoAnexoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
					throws NegocioException {

		this.getControladorCliente().verificarTipoAnexoAnexo(indexLista, listaClienteAnexo, clienteAnexo);
	}

	/**
	 * Validar dados cliente telefone.
	 *
	 * @param clienteFone the cliente fone
	 * @throws NegocioException the GGAS exception
	 */
	public void validarDadosClienteTelefone(ClienteFone clienteFone) throws NegocioException {

		this.getControladorCliente().validarDadosClienteFone(clienteFone);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um endereço no cliente com o mesmo
	 * número.
	 *
	 * @param indexLista the index lista
	 * @param listaClienteFone the lista cliente fone
	 * @param clienteFone the cliente fone
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarNumeroTelefoneExistente(Integer indexLista, Collection<ClienteFone> listaClienteFone, ClienteFone clienteFone)
					throws NegocioException {

		this.getControladorCliente().verificarNumeroTelefoneExistente(indexLista, listaClienteFone, clienteFone);
	}

	/**
	 * Método responsável por criar um contato do
	 * imóvel.
	 *
	 * @return contatoCliente um contato do
	 *         imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContatoImovel criarContatoImovel() throws NegocioException {

		return (ContatoImovel) this.getControladorImovel().criarContatoImovel();
	}

	/**
	 * Método responsável por consultar os
	 * medidores do sistema.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de medidores do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Medidor> consultarMedidor(Map<String, Object> filtro) throws NegocioException {

		return getControladorMedidor().consultarMedidor(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * medidores do sistema.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de medidores do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Medidor> consultarMedidorDisponivel(Map<String, Object> filtro) throws NegocioException {

		return getControladorMedidor().consultarMedidorDisponivel(filtro);
	}

	/**
	 * Método responsável por consultar os vazão
	 * corretor do sistema.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de medidores do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<VazaoCorretor> consultarVazaoCorretorDisponivel(Map<String, Object> filtro) throws NegocioException {

		return getControladorVazaoCorretor().consultarVazaoCorretorDisponivel(filtro);
	}

	/**
	 * Método responsável por consultar a
	 * movimentação do medidor pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de movimentação de
	 *         medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MovimentacaoMedidor> consultarMovimentacaoMedidor(Map<String, Object> filtro) throws NegocioException {

		return getControladorMedidor().consultarMovimentacaoMedidor(filtro);
	}

	/**
	 * Método responsável por verificar se existe
	 * histórico de movimentação para o medidor
	 * selecionado.
	 *
	 * @param movimentacoes
	 *            Coleção de movimentações do
	 *            histórico de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarListaMovimentacao(Collection<MovimentacaoMedidor> movimentacoes) throws NegocioException {

		this.getControladorMedidor().verificarListaMovimentacao(movimentacoes);
	}

	/**
	 * Consultar marca medidor em uso.
	 *
	 * @param marcaMedidor the marca medidor
	 * @return the long
	 */
	public Long consultarMarcaMedidorEmUso(Long marcaMedidor) {

		return this.getControladorMedidor().consultarMarcaMedidorEmUso(marcaMedidor);
	}

	/**
	 * Consultar modelo medidor em uso.
	 *
	 * @param modeloMedidor the modelo medidor
	 * @return the long
	 */
	public Long consultarModeloMedidorEmUso(Long modeloMedidor) {

		return this.getControladorMedidor().consultarModeloMedidorEmUso(modeloMedidor);
	}

	/**
	 * Método responsável por verificar se existe
	 * histórico de instalação de medidor.
	 *
	 * @param listaInstalacaoMedidor
	 *            Coleção de Instalações de
	 *            Medidores
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarListaHistoricoInstalacaoMedidor(Collection<InstalacaoMedidor> listaInstalacaoMedidor) throws NegocioException {

		this.getControladorMedidor().verificarListaHistoricoInstalacaoMedidor(listaInstalacaoMedidor);
	}

	/**
	 * Método responsável por verificar se existe
	 * histórico de operação de
	 * medidor.
	 *
	 * @param listaHistoricoOperacaoMedidor
	 *            Coleção de histórico de operação
	 *            de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarListaHistoricoOperacaoMedidor(Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor)
					throws NegocioException {

		this.getControladorMedidor().verificarListaHistoricoOperacaoMedidor(listaHistoricoOperacaoMedidor);
	}

	/**
	 * Método que valida a seleção das devoluções
	 * para a exclusão;.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException the GGAS exception
	 */
	public void validarRemoverDevolucao(Long[] chavesPrimarias) throws GGASException {

		this.getControladorDevolucao().validarRemoverDevolucao(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * medidores.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            medidores.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverMedidores(Long[] chavesPrimarias) throws GGASException {

		this.getControladorMedidor().validarRemoverMedidores(chavesPrimarias);
	}

	/**
	 * Método responsável por remover os medidores
	 * selecionados.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            medidores selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerMedidores(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorMedidor().removerMedidores(chavesPrimarias);
	}

	/**
	 * Método responsável por cria um medidor.
	 *
	 * @return the medidor
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Medidor criarMedidor() throws NegocioException {

		return (Medidor) this.getControladorMedidor().criar();
	}

	/**
	 * Método responsável por listar os tipos dos
	 * medidores existentes no sistema.
	 *
	 * @return coleção de tipos dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoMedidor> listarTipoMedidor() throws NegocioException {

		return getControladorMedidor().listarTipoMedidor();
	}

	/**
	 * Método responsável por listar as marcas dos
	 * medidores existentes no sistema.
	 *
	 * @return coleção as marcas dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MarcaMedidor> listarMarcaMedidor() throws NegocioException {

		return getControladorMedidor().listarMarcaMedidor();
	}

	/**
	 * Método responsável por listar os modelos
	 * dos medidores existentes no sistema.
	 *
	 * @return coleção dos modelos dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ModeloMedidor> listarModeloMedidor() throws NegocioException {

		return getControladorMedidor().listarModeloMedidor();
	}

	/**
	 * Método responsável por listar fatorK.
	 *
	 * @return Coleção com fatorK.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<FatorK> listarFatorK() throws NegocioException {

		return getControladorMedidor().listarFatorK();
	}

	/**
	 * Método responsável por listar as situações
	 * dos medidores existentes no sistema.
	 *
	 * @return coleção as situações dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoMedidor> listarSituacaoMedidor() throws NegocioException {

		return getControladorMedidor().listarSituacaoMedidor();
	}

	/**
	 * Método responsável por listar os diâmetros
	 * dos medidores existentes no sistema.
	 *
	 * @return coleção os diâmetros dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<DiametroMedidor> listarDiametroMedidor() throws NegocioException {

		return getControladorMedidor().listarDiametroMedidor();
	}

	/**
	 * Método responsável por listar as faixa de
	 * temperatura de trabalho existentes no
	 * sistema.
	 *
	 * @return coleção contendo as faixa de
	 *         temperatura de trabalho existentes
	 *         no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<FaixaTemperaturaTrabalho> listarFaixaTemperaturaTrabalho() throws NegocioException {

		return getControladorMedidor().listarFaixaTemperaturaTrabalho();
	}

	/**
	 * Método responsável por inserir um corretor
	 * de vazão no sistema.
	 *
	 * @param vazaoCorretor
	 *            O corretor de vazão a ser
	 *            inserido.
	 * @return chavePrimeria do corretor de vazão
	 *         inserido
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */

	public Long inserirVazaoCorretor(VazaoCorretor vazaoCorretor) throws NegocioException {

		return this.getControladorVazaoCorretor().inserirCorretorVazao(vazaoCorretor);
	}

	/**
	 * Método responsável por alterar um corretor
	 * de vazão.
	 *
	 * @param vazaoCorretor
	 *            O corretor de vazão a ser
	 *            alterado.
	 * @return chavePrimeria do corretor de vazão
	 *         inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarVazaoCorretor(VazaoCorretor vazaoCorretor) throws GGASException {

		this.getControladorVazaoCorretor().atualizar(vazaoCorretor);
	}

	/**
	 * Método responsável por cria um corretor de
	 * vazão.
	 *
	 * @return the vazao corretor
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public VazaoCorretor criarVazaoCorretor() throws NegocioException {

		return (VazaoCorretor) this.getControladorVazaoCorretor().criar();
	}

	/**
	 * Método responsável por listar os corretores
	 * de vazão existentes no sistema.
	 *
	 * @return coleção contendo os corretores de
	 *         vazão existentes no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<VazaoCorretor> listarVazaoCorretor() throws NegocioException {

		return getControladorMedidor().listarVazaoCorretor();
	}

	/**
	 * Método responsável por listar os locais de
	 * armazenagem existentes no sistema.
	 *
	 * @return coleção contendo os locais de
	 *         armazenagem existentes no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MedidorLocalArmazenagem> listarLocalArmazenagem() throws NegocioException {

		return getControladorMedidor().listarLocalArmazenagem();
	}

	/**
	 * Método responsável por listar as
	 * capacidades dos medidores existentes no
	 * sistema.
	 *
	 * @return coleção de capacidades dos
	 *         medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CapacidadeMedidor> listarCapacidadeMedidor() throws NegocioException {

		return getControladorMedidor().listarCapacidadeMedidor();
	}

	/**
	 * Método responsável por obter a marca do
	 * medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma marca.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MarcaMedidor obterMarcaMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterMarcaMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter um medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Medidor obterMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter um medidor.
	 *
	 * @param chavePrimaria A chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return Um medidor.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Medidor obterMedidor(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return getControladorMedidor().obterMedidor(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter uma Faixa
	 * Temperatura Trabalho.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Uma FaixaTemperaturaTrabalho
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaixaTemperaturaTrabalho obterFaixaTemperaturaTrabalho(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterFaixaTemperaturaTrabalho(chavePrimaria);
	}

	/**
	 * Método responsável por obter um corretor de
	 * vazão.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um corretor de vazão.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public VazaoCorretor obterVazaoCorretor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterVazaoCorretor(chavePrimaria);
	}

	/**
	 * Obter vazao corretor.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the vazao corretor
	 * @throws NegocioException the GGAS exception
	 */
	public VazaoCorretor obterVazaoCorretor(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return getControladorMedidor().obterVazaoCorretor(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter um Medidor
	 * Local Armazenagem.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um Medidor Local Armazenagem
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MedidorLocalArmazenagem obterMedidorLocalArmazenagem(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterMedidorLocalArmazenagem(chavePrimaria);
	}

	/**
	 * Método responsável por obter o tipo do
	 * medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um Tipo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoMedidor obterTipoMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterTipoMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter o tipo de
	 * Segmento.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Um Segmento.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Segmento obterSegmento(Long chavePrimaria) throws NegocioException {

		return getControladorSegmento().obterSegmento(chavePrimaria);

	}

	/**
	 * Método responsável por obter a situação do
	 * medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return A situação do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SituacaoMedidor obterSituacaoMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterSituacaoMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter o diâmetro do
	 * medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return O diâmetro do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DiametroMedidor obterDiametroMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterDiametroMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter a capacidade
	 * do medidor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return A capacidade do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CapacidadeMedidor obterCapacidadeMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterCapacidadeMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por consultar as marcas
	 * dos corretores de vazão cadastrados.
	 *
	 * @return Uma coleção de corretor de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MarcaCorretor> consultarMarcasDosCorretoresDeVazaoCadastrados() throws NegocioException {

		return getControladorVazaoCorretor().consultarMarcasDosCorretoresDeVazaoCadastrados();
	}

	/**
	 * Método responsável por consultar o tipo
	 * mostrador dos corretores de vazão
	 * cadastrados.
	 *
	 * @return Uma coleção de tipo mostrador.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoMostrador> consultarTipoMostradorDosCorretoresDeVazaoCadastrados() throws NegocioException {

		return getControladorVazaoCorretor().consultarTipoMostradorDosCorretoresDeVazaoCadastrados();
	}

	/**
	 * Método responsável por consultar os
	 * corretores de vazão disponíveis do sistema.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<VazaoCorretor> consultarVazaoCorretor(Map<String, Object> filtro) throws NegocioException {

		return getControladorVazaoCorretor().consultarVazaoCorretor(filtro);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a exclusão
	 * de
	 * corretores de vazão.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarExcluirVazaoCorretor(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorVazaoCorretor().validarExcluirVazaoCorretor(chavesPrimarias);
	}

	/**
	 * Método responsável por excluir os
	 * corretores de vazão selecionados.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            corretores de vazão
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerVazaoCorretor(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorVazaoCorretor().removerVazaoCorretor(chavesPrimarias);
	}

	/**
	 * Método responsável por buscar o corretor de
	 * vazão pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do corretor de
	 *            vazão
	 * @return Um corretor de vazão cadastrado
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public VazaoCorretor buscarVazaoCorretorPorChave(Long chavePrimaria) throws NegocioException {

		return (VazaoCorretor) this.getControladorVazaoCorretor().obter(chavePrimaria);
	}

	/**
	 * Método responsável por listar as marcas dos
	 * corretores de vazão do sistema.
	 *
	 * @return coleção de marcas dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MarcaCorretor> listarMarcaVazaoCorretor() throws NegocioException {

		return getControladorVazaoCorretor().listarMarcaVazaoCorretor();
	}

	/**
	 * Método responsável por listar os modelos
	 * dos corretores de vazão do sistema.
	 *
	 * @return coleção de modelos dos corretores
	 *         de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ModeloCorretor> listarModeloVazaoCorretor() throws NegocioException {

		return getControladorVazaoCorretor().listarModeloVazaoCorretor();
	}

	/**
	 * Método responsável por obter a marca do
	 * corretor de vazão.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma marca.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MarcaCorretor obterMarcaVazaoCorretor(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterMarcaVazaoCorretor(chavePrimaria);
	}

	/**
	 * Método responsável por listar os protocolos
	 * de comunicação dos corretores de vazão do
	 * sistema.
	 *
	 * @return coleção de protocolos de
	 *         comunicação dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ProtocoloComunicacao> listarProtocoloComunicacao() throws NegocioException {

		return getControladorVazaoCorretor().listarProtocoloComunicacao();
	}

	/**
	 * Método responsável por obter o protocolo de
	 * comunicação.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o protocolo de comunicação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ProtocoloComunicacao obterProtocoloComunicacao(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterProtocoloComunicacao(chavePrimaria);
	}

	/**
	 * Método responsável por listar os numero de digitos
	 * para leitura dos corretores de vazão do
	 * sistema.
	 *
	 * @return coleção de protocolos de
	 *         comunicação dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Integer> listarNumeroDigitosLeitura() throws NegocioException {

		// FIXME: Utilizar o método de busca de parâmetros
		return getControladorVazaoCorretor().listarNumeroDigitosLeitura();
	}

	/**
	 * Método responsável por obter o numero de digitos para leitura.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return o protocolo de comunicação.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterNumeroDigitosLeitura(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterNumeroDigitosLeitura(chavePrimaria);
	}

	/**
	 * Método responsável por listar os Tipos de
	 * Transdutor de Pressao dos corretores de
	 * vazão do
	 * sistema.
	 *
	 * @return coleção de Tipos de Transdutor de
	 *         Pressao dos corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoTransdutorPressao> listarTipoTransdutorPressao() throws NegocioException {

		return getControladorVazaoCorretor().listarTipoTransdutorPressao();
	}

	/**
	 * Método responsável por obter o tipo de
	 * transdutor de pressão.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o tipo de transdutor de pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoTransdutorPressao obterTipoTransdutorPressao(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterTipoTransdutorPressao(chavePrimaria);
	}

	/**
	 * Método responsável por obter a pressão
	 * máxima do transdutor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return a pressão máxima do transdutor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PressaoMaximaTransdutor obterPressaoMaximaTransdutor(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterPressaoMaximaTransdutor(chavePrimaria);
	}

	/**
	 * Método responsável por obter a temperatura
	 * máxima do transdutor.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return a temperatura máxima do transdutor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TemperaturaMaximaTransdutor obterTemperaturaMaximaTransdutor(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterTemperaturaMaximaTransdutor(chavePrimaria);
	}

	/**
	 * Método responsável por obter a um Modelo de
	 * Corretor de Vazão.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return ModeloCorretor
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloCorretor obterModeloCorretor(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterModeloCorretor(chavePrimaria);
	}

	/**
	 * Método responsável por obter a um Modelo de
	 * Medidor.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return ModeloMedidor
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloMedidor obterModeloMedidor(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterModeloMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por obter um fator k.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Fator K
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FatorK obterFatorK(long chavePrimaria) throws NegocioException {

		return getControladorMedidor().obterFatorK(chavePrimaria);
	}

	/**
	 * Método responsável por listar os Tipos de
	 * Transdutor de Temperatura dos corretores de
	 * vazão
	 * do sistema.
	 *
	 * @return coleção de Tipos de Transdutor de
	 *         Temperatura dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoTransdutorTemperatura> listarTipoTransdutorTemperatura() throws NegocioException {

		return getControladorVazaoCorretor().listarTipoTransdutorTemperatura();
	}

	/**
	 * Método responsável por obter o tipo de
	 * transdutor de temperatura.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o tipo de transdutor de
	 *         temperatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoTransdutorTemperatura obterTipoTransdutorTemperatura(long chavePrimaria) throws NegocioException {

		return getControladorVazaoCorretor().obterTipoTransdutorTemperatura(chavePrimaria);
	}

	/**
	 * Método responsável por listar as unidades
	 * de temperatura do sistema.
	 *
	 * @return coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Unidade> listarUnidadesTemperatura() throws NegocioException {

		return getControladorUnidade().listarUnidadesTemperatura();
	}

	/**
	 * Método responsável por listar as unidades
	 * de temperatura do sistema.
	 *
	 * @return coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Unidade> listarUnidadesVazao() throws NegocioException {

		return this.getControladorUnidade().listarUnidadesVazao();
	}

	/**
	 * Método responsável por listar as unidades
	 * de volume do sistema.
	 *
	 * @return coleção de unidades de volume.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Unidade> listarUnidadesVolume() throws NegocioException {

		return this.getControladorUnidade().listarUnidadesVolume();
	}

	/**
	 * Método responsável por obter a unidade de
	 * medição.
	 *
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return a unidade de medição..
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Unidade obterUnidadeMedicao(long chavePrimaria) throws NegocioException {

		return (Unidade) getControladorUnidade().obter(chavePrimaria);
	}

	/**
	 * Método responsável por listar as unidades
	 * pressão do sistema.
	 *
	 * @return coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Unidade> listarUnidadesPressao() throws NegocioException {

		return getControladorUnidade().listarUnidadesPressao();
	}

	/**
	 * Método responsável por listar todas as
	 * unidades da classe extensao.
	 *
	 * @return Coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	public Collection<Unidade> listarUnidadesExtensao() throws NegocioException {

		return getControladorUnidade().listarUnidadesExtensao();
	}

	/**
	 * Método responsável por listar as
	 * temperaturas máximas do transdutor.
	 *
	 * @return Coleção de Temperatura Máxima
	 *         Transdutor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TemperaturaMaximaTransdutor> listarTemperaturaMaximaTransdutor() throws NegocioException {

		return getControladorVazaoCorretor().listarTemperaturaMaximaTransdutor();
	}

	/**
	 * Método responsável por listar as pressões
	 * máximas do transdutor.
	 *
	 * @return Coleção de Pressão Máxima
	 *         Transdutor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PressaoMaximaTransdutor> listarPressaoMaximaTransdutor() throws NegocioException {

		return getControladorVazaoCorretor().listarPressaoMaximaTransdutor();
	}

	/**
	 * Método responsável por listar os tipos
	 * mostrador dos corretores de vazão do
	 * sistema.
	 *
	 * @return coleção de tipo mostrador do
	 *         corretor de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoMostrador> listarTipoMostrador() throws NegocioException {

		return getControladorVazaoCorretor().listarTipoMostrador();
	}

	/**
	 * Método responsável por obter o tipo
	 * mostrador.
	 *
	 * @param codigoTipoMostrador the codigo tipo mostrador
	 * @return Um tipo mostrador
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoMostrador obterTipoMostrador(Integer codigoTipoMostrador) throws NegocioException {

		return getControladorVazaoCorretor().obterTipoMostrador(codigoTipoMostrador);
	}

	/**
	 * Método responsável por consultar as
	 * empresas que possuem o serviço de
	 * medição.
	 *
	 * @return Uma coleção de empresas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Empresa> listarEmpresasMedicao() throws NegocioException {

		return getControladorEmpresa().listarEmpresasMedicao();
	}

	/**
	 * Método responsável por consultar os setores
	 * comerciais.
	 *
	 * @return Uma coleção de setor comercial
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SetorComercial> listarSetoresComerciais() throws NegocioException {

		return getControladorSetorComercial().listarSetoresComerciais();
	}

	/**
	 * Método responsável por consultar os tipos
	 * de leituras das rotas cadastrados no
	 * sistema.
	 *
	 * @return Uma coleção de tipo de leitura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoLeitura> listarTiposLeitura() throws NegocioException {

		return getControladorRota().listarTiposLeitura();
	}

	/**
	 * Método responsável por consultar os
	 * leituristas das rotas cadastrados no
	 * sistema.
	 *
	 * @return Uma coleção de leituristas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Leiturista> listarLeituristas() throws NegocioException {

		return getControladorRota().listarLeituristas();
	}

	/**
	 * Método responsável por consultar as
	 * periodicidades das rotas cadastrados no
	 * sistema.
	 *
	 * @return Uma coleção de periodicidades
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Periodicidade> listarPeriodicidades() throws NegocioException {

		return getControladorRota().listarPeriodicidades();
	}

	/**
	 * Método responsável por consultar tipos das
	 * rotas cadastrados no sistema.
	 *
	 * @return Uma coleção de tipos das rotas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoRota> listarTiposRotas() throws NegocioException {

		return getControladorRota().listarTiposRotas();
	}

	/**
	 * Método responsável por consultar grupos de
	 * faturamento das rotas cadastrados no
	 * sistema.
	 *
	 * @return Uma coleção de grupos de
	 *         faturamento das rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<GrupoFaturamento> listarGruposFaturamentoRotas() throws NegocioException {

		return getControladorRota().listarGruposFaturamentoRotas();
	}

	/**
	 * Método responsável por consultar as rotas
	 * pelo filtro informado.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> consultarRota(Map<String, Object> filtro) throws NegocioException {

		return getControladorRota().consultarRota(filtro);
	}

	/**
	 * Método responsável por remover a(s) rota(s)
	 * selecionada(s).
	 *
	 * @param chavesPrimarias
	 *            A(s) chave(s) primária(s) da(s)
	 *            rota(s) selecionada(s).
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerRotas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorRota().removerRotas(chavesPrimarias);
	}

	/**
	 * Método responsável por buscar a rota pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária de rota
	 * @return Uma rota cadastrada
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rota buscarRotaPorChave(Long chavePrimaria) throws NegocioException {

		return (Rota) this.getControladorRota().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a rota pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária de rota
	 * @return Uma rota cadastrada
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rota buscarRota(Long chavePrimaria) throws NegocioException {

		return this.getControladorRota().buscarRota(chavePrimaria);
	}

	/**
	 * Método responsável por obter o controlador
	 * de rota.
	 *
	 * @return ControladorRota O controlador de
	 *         rota.
	 */
	private ControladorRota getControladorRota() {

		return (ControladorRota) serviceLocator.getControladorNegocio(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
	}

	/**
	 * Gets the controlador feriado.
	 *
	 * @return the controlador feriado
	 */
	private ControladorFeriado getControladorFeriado() {

		return (ControladorFeriado) serviceLocator.getControladorNegocio(ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);
	}

	/**
	 * Método responsável por obter o controlador
	 * do relatorio de rota.
	 *
	 * @return the controlador relatorio rota
	 */
	private ControladorRelatorioRota getControladorRelatorioRota() {

		return (ControladorRelatorioRota) serviceLocator.getControladorNegocio(ControladorRelatorioRota.BEAN_ID_CONTROLADOR_RELATORIO_ROTA);
	}

	/**
	 * Método responsável por obter o controlador
	 * do relatorio de pontos de consumo por data.
	 *
	 * @return the controlador relatorio pontos de consumo por data.
	 */
	@SuppressWarnings("unused")
	private ControladorRelatorioPontosDeConsumoPorData getControladorRelatorioPontosConsumoPorData() {

		return (ControladorRelatorioPontosDeConsumoPorData) serviceLocator
						.getControladorNegocio(ControladorRelatorioPontosDeConsumoPorData.BEAN_ID_CONTROLADOR_RELATORIO_PONTOS_CONSUMO_POR_DATA);
	}

	/**
	 * Método responsável por obter o controlador
	 * do relatorio de analise de demanda.
	 *
	 * @return the controlador relatorio analise demanda
	 */
	private ControladorRelatorioAnaliseDemanda getControladorRelatorioAnaliseDemanda() {

		return (ControladorRelatorioAnaliseDemanda) serviceLocator
						.getControladorNegocio(ControladorRelatorioAnaliseDemanda.BEAN_ID_CONTROLADOR_RELATORIO_ANALISE_DEMANDA);
	}

	/**
	 * Método responsável por cria uma Rota.
	 *
	 * @return the rota
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rota criarRota() throws NegocioException {

		return (Rota) this.getControladorRota().criar();
	}

	/**
	 * Método responsável por obter o controlador
	 * de vazão corretor.
	 *
	 * @return ControladorVazaoCorretor O
	 *         controlador de vazão corretor.
	 */
	private ControladorVazaoCorretor getControladorVazaoCorretor() {

		return (ControladorVazaoCorretor) serviceLocator.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
	}
	
	private ControladorEquipeTurno getControladorEquipeTurno() {

		return ServiceLocator.getInstancia().getControladorEquipeTurno();
	}	

	/**
	 * Método responsável por obter a situação de
	 * consumo pela descrição.
	 *
	 * @param idSituacaoConsumo the id situacao consumo
	 * @return SituacaoConsumo
	 * @throws NegocioException the GGAS exception
	 */
	public SituacaoConsumo obterSituacaoConsumo(long idSituacaoConsumo) throws NegocioException {

		return getControladorImovel().obterSituacaoConsumo(idSituacaoConsumo);
	}

	/**
	 * Método responsável por obter o controlador
	 * de medidor.
	 *
	 * @return ControladorMedidor O controlador de
	 *         medidor.
	 */
	private ControladorMedidor getControladorMedidor() {

		return (ControladorMedidor) serviceLocator.getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
	}

	/**
	 * Método responsável por inserir um medidor
	 * no sistema.
	 *
	 * @param medidor
	 *            O medidor a ser inserido.
	 * @return chavePrimeria do medidor inserido
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirMedidor(Medidor medidor) throws GGASException {

		return this.getControladorMedidor().inserirMedidor(medidor);
	}

	/**
	 * Inserir medidores em lote.
	 *
	 * @param medidor the medidor
	 * @return the long
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirMedidoresEmLote(Medidor medidor) throws NegocioException {

		return this.getControladorMedidor().inserirMedidoresEmLote(medidor);
	}

	/**
	 * Método responsável por inserir uma
	 * devolução no sistema.
	 *
	 * @param devolucao the devolucao
	 * @return id da devolução inserida
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirDevolucao(Devolucao devolucao) throws NegocioException {

		return this.getControladorDevolucao().inserir(devolucao);
	}

	/**
	 * Método responsável por alterar um medidor.
	 *
	 * @param medidor Medidor a ser alterado no
	 *            sistema.
	 * @param localArmazenagemAnterior the local armazenagem anterior
	 * @return chavePrimeria Chave do medidor.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarMedidor(Medidor medidor, MedidorLocalArmazenagem localArmazenagemAnterior) throws GGASException {

		this.getControladorMedidor().atualizarMedidor(medidor, localArmazenagemAnterior);
	}

	/**
	 * Método responsável por obter o controlador
	 * de redes.
	 *
	 * @return ControladorRede O controlador de
	 *         redes.
	 */
	private ControladorRede getControladorRede() {

		return (ControladorRede) serviceLocator.getControladorNegocio(ControladorRede.BEAN_ID_CONTROLADOR_REDE);
	}

	/**
	 * Método responsável por inserir uma
	 * localidade.
	 *
	 * @param localidade Uma localidade.
	 * @return A chave primária da localidade
	 *         inserida.
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirLocalidade(Localidade localidade) throws NegocioException {

		return this.getControladorLocalidade().inserirLocalidade(localidade);
	}

	/**
	 * Método responsável por consultar as
	 * unidades de negócio pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção unidades de negócio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<UnidadeNegocio> consultarUnidadesNegocio(Map<String, Object> filtro) throws NegocioException {

		return getControladorUnidadeNegocio().consultarUnidadesNegocio(filtro);
	}

	/**
	 * Método responsável por consultar os locais
	 * de armazenagem dos medidores pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção unidades de negócio.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MedidorLocalArmazenagem> consultarMedidorLocalArmazenagem(Map<String, Object> filtro) throws GGASException {

		return getControladorLocalidade().consultarMedidorLocalArmazenagem(filtro);
	}

	/**
	 * Método responsável por consultar as classes
	 * de localidade pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de classe de localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<LocalidadeClasse> consultarLocalidadeClasses(Map<String, Object> filtro) throws NegocioException {

		return getControladorLocalidade().consultarLocalidadeClasses(filtro);
	}

	/**
	 * Método responsável por consultar os portes
	 * de localidade pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de porte de localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<LocalidadePorte> consultarLocalidadePortes(Map<String, Object> filtro) throws NegocioException {

		return getControladorLocalidade().consultarLocalidadePortes(filtro);
	}

	/**
	 * Método responsável por buscar a unidade
	 * negócio pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da unidade de
	 *            negócio.
	 * @return Uma unidade de negócio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public UnidadeNegocio buscarUnidadeNegocioChave(Long chavePrimaria) throws NegocioException {

		return (UnidadeNegocio) this.getControladorUnidadeNegocio().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a classe de
	 * localidade pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da classe de
	 *            localidade.
	 * @return Uma classe de localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public LocalidadeClasse buscarLocalidadeClasseChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorLocalidade().obterLocalidadeClasse(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o porte de
	 * localidade pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do porte de
	 *            localidade.
	 * @return Um porte de localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public LocalidadePorte buscarLocalidadePorteChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorLocalidade().obterLocalidadePorte(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o medidor
	 * local armazenagem pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do medidor
	 *            local de armzenagem.
	 * @return Um MedidorLocalArmazenagem.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MedidorLocalArmazenagem buscarMedidorLocalArmazenagemPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorLocalidade().obterMedidorLocalArmazenagem(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o cep pelo
	 * número.
	 *
	 * @param cep
	 *            O número do cep.
	 * @return Um cep.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cep buscarCep(String cep) throws NegocioException {

		return this.getControladorEndereco().obterCep(cep);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Localidade.
	 *
	 * @param localidade the localidade
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarLocalidade(Localidade localidade) throws GGASException {

		this.getControladorLocalidade().atualizar(localidade);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar uma localidade.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos setores comerciais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAtualizarLocalidade(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorLocalidade().validarAtualizarLocalidade(chavesPrimarias);
	}

	/**
	 * Método responsável por obter uma referência
	 * de endereço pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            Chave primária da referência de
	 *            endereço.
	 * @return EnderecoReferencia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EnderecoReferencia buscarEnderecoReferenciaPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorEndereco().obterEnderecoReferenciaPorChave(chavePrimaria);
	}

	/**
	 * Método responsável por consultar os
	 * servicos prestados pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de serviços prestados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ServicoPrestado> consultarServicoPrestado(Map<String, Object> filtro) throws NegocioException {

		return getControladorServicoPrestado().consultarServicoPrestado(filtro);
	}

	/**
	 * Método responsável por consultar as quadras
	 * pelo cep passado por parâmetro.
	 *
	 * @param cep O número do cep.
	 * @return Um mapa de String
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Quadra> listarQuadraPorCep(String cep) throws NegocioException {

		return getControladorQuadra().listarQuadraPorCep(cep);
	}

	/**
	 * Método responsável por listar as situações
	 * dos imóveis existentes no
	 * sistema.
	 *
	 * @return coleção de situações dos imóveis.
	 * @throws NegocioException the NegocioException
	 */
	public Collection<SituacaoImovel> listarImovelSituacao() throws NegocioException {

		return getControladorImovel().listarImovelSituacao();
	}

	/**
	 * Método responsável por listar os tipos de
	 * relacionamento entre cliente e
	 * imóvel sistema.
	 *
	 * @return coleção de tipos de relacionamento
	 *         entre cliente e imóvel.
	 * @throws NegocioException the NegocioException exception
	 */
	public Collection<TipoRelacionamentoClienteImovel> listarTipoRelacionamentoClienteImovel() throws NegocioException {

		return getControladorImovel().listarTipoRelacionamentoClienteImovel();
	}

	/**
	 * Método responsável por listar os clientes
	 * que estão relacionados com um
	 * imóvel.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @param listarInativos Indicador que, caso true, lista
	 *            também os inativos.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ClienteImovel> listarClienteImovelPorChaveImovel(Long chavePrimariaImovel)
					throws NegocioException {

		return getControladorImovel().listarClienteImovelPorChaveImovel(chavePrimariaImovel);
	}

	/**
	 * Método responsável por listar os clientes
	 * que estão relacionados com um
	 * imóvel que possuem endereço null ou não.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @param listarInativos Indicador que, caso true, lista
	 *            também os inativos.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ClienteImovel> listarCliImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		return getControladorImovel().listarCliImovelPorChaveImovel(chavePrimariaImovel);
	}

	/**
	 * Método responsável por listar os imóveis
	 * que estão relacionados com um cliente.
	 *
	 * @param chavePrimariaCliente the chave primaria cliente
	 * @param listarInativos the listar inativos
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ClienteImovel> listarClienteImovelPorChaveCliente(Long chavePrimariaCliente, boolean listarInativos)
					throws NegocioException {

		return getControladorImovel().listarClienteImovelPorChaveCliente(chavePrimariaCliente, listarInativos);
	}

	/**
	 * Método responsável por listar os contatos
	 * que estão relacionados com um imóvel.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ContatoImovel> listarContatoImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		return getControladorImovel().listarContatoImovelPorChaveImovel(chavePrimariaImovel);
	}

	/**
	 * Método responsável por criar um
	 * relacionamento de cliente de imóvel.
	 *
	 * @return clienteImovel um um relacionamento
	 *         de cliente de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ClienteImovel criarClienteImovel() throws NegocioException {

		return (ClienteImovel) this.getControladorImovel().criarClienteImovel();
	}

	/**
	 * Método responsável por criar um ramo de
	 * atividade de imóvel.
	 *
	 * @return clienteImovel um ramo de atividade
	 *         de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ImovelRamoAtividade criarImovelRamoAtividade() throws NegocioException {

		return (ImovelRamoAtividade) this.getControladorImovel().criarImovelRamoAtividade();
	}

	/**
	 * Método responsável por criar um ponto de
	 * consumo.
	 *
	 * @return PontoConsumo de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PontoConsumo criarPontoConsumo() throws NegocioException {

		return (PontoConsumo) this.getControladorImovel().criarPontoConsumo();
	}

	/**
	 * Criar ponto consumo tributo aliquota.
	 *
	 * @return the ponto consumo tributo aliquota
	 * @throws NegocioException the GGAS exception
	 */
	public PontoConsumoTributoAliquota criarPontoConsumoTributoAliquota() throws NegocioException {

		return (PontoConsumoTributoAliquota) this.getControladorImovel().criarPontoConsumoTributoAliquota();
	}

	/**
	 * Método responsável por validar o ponto de
	 * consumo tributo aliquota.
	 *
	 * @param pontosConsumoTributoAliquota Lista de pontos de consumo
	 *            tributo aliquota
	 * @param pontoConsumoTributoAliquota the ponto consumo tributo aliquota
	 * @param indexPontoConsumoTributoAliquota the index ponto consumo tributo aliquota
	 * @throws NegocioException the GGAS exception
	 */
	public void validarPontoConsumoTributoAliquota(Collection<PontoConsumoTributoAliquota> pontosConsumoTributoAliquota,
					PontoConsumoTributoAliquota pontoConsumoTributoAliquota, Integer indexPontoConsumoTributoAliquota) throws NegocioException {

		this.getControladorImovel().validarPontoConsumoTributoAliquota(pontosConsumoTributoAliquota, pontoConsumoTributoAliquota,
						indexPontoConsumoTributoAliquota);
	}

	/**
	 * Método responsável por validar os dados do
	 * relacionamento de cliente com
	 * o imóvel.
	 *
	 * @param clienteImovel
	 *            Um relacionamento de cliente com
	 *            o imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosClienteImovel(ClienteImovel clienteImovel) throws NegocioException {

		this.getControladorImovel().validarDadosClienteImovel(clienteImovel);
	}

	/**
	 * Método responsável por obter o tipo de
	 * relacionamento de cliente com
	 * imóvel.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            relacionamento de
	 *            cliente com imóvel.
	 * @return o tipo de relacionamento de cliente
	 *         com imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoRelacionamentoClienteImovel buscarTipoRelacionamentoClienteImovel(Long chavePrimaria) throws NegocioException {

		return getControladorImovel().buscarTipoRelacionamentoClienteImovel(chavePrimaria);
	}

	/**
	 * Método responsável por listas os segmentos.
	 *
	 * @return coleção de tipos segmentos.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Segmento> listarSegmento() throws NegocioException {

		return this.getControladorSegmento().listarSegmento();
	}

	/**
	 * Método responsável por listar os ramos de
	 * atividades por segmento.
	 *
	 * @param chavePrimariaSegmento Chave primária do segmento
	 * @return coleção de ramos de atividade.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<RamoAtividade> listarRamoAtividadePorSegmento(Long chavePrimariaSegmento) throws NegocioException {

		return this.getControladorRamoAtividade().listarRamoAtividadePorSegmento(chavePrimariaSegmento);
	}

	/**
	 * Método responsável por listar os ramos de atividades por segmento no Imovel.
	 *
	 * @param idRamoAtividadePontoConsumo Id ramo atividade ponto consumo
	 * @return coleção de ramos de atividade.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<RamoAtividade> listarRamoAtividadePorSegmentoImovel(Long idRamoAtividadePontoConsumo) throws NegocioException {

		return this.getControladorRamoAtividade().listarRamoAtividadePorSegmentoImovel(idRamoAtividadePontoConsumo);
	}

	/**
	 * Método responsável por listar as unidades
	 * consumidoras.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @return coleção de unidades consumidoras.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<ImovelRamoAtividade> listarUnidadeConsumidoraPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		return this.getControladorImovel().listarUnidadeConsumidoraPorChaveImovel(chavePrimariaImovel);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo pela chave do imóvel passado por
	 * parâmetro.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @return coleção de pontos de consumo.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovel(Long chavePrimariaImovel) throws GGASException {

		return this.getControladorImovel().listarPontoConsumoPorChaveImovel(chavePrimariaImovel);
	}

	/**
	 * Listar ponto consumo por chave imovel ativos inativos.
	 *
	 * @param chavePrimariaImovel the chave primaria imovel
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovelAtivosInativos(Long chavePrimariaImovel) throws NegocioException {

		return this.getControladorImovel().listarPontoConsumoPorChaveImovelAtivosInativos(chavePrimariaImovel);
	}

	/**
	 * Carregar ponto consumo por chave imovel.
	 *
	 * @param chavePrimariaImovel the chave primaria imovel
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> carregarPontoConsumoPorChaveImovel(Long chavePrimariaImovel) throws GGASException {

		return this.getControladorImovel().carregarPontoConsumoPorChaveImovel(chavePrimariaImovel, Boolean.TRUE);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo pela chave do imóvel passado por
	 * parâmetro
	 * e que estejam aptos para simulação do
	 * cálculo de fornecimento de gás.
	 *
	 * @param chavePrimariaImovel Chave primária de um imóvel.
	 * @return coleção de pontos de consumo.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovelSimulacaoCalculo(Long chavePrimariaImovel) throws NegocioException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorChaveImovelSimulacaoCalculo(chavePrimariaImovel);
	}

	/**
	 * Método responsável por buscar o ramo de
	 * atividade pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do ramo de
	 *            atividade.
	 * @return Um segmento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RamoAtividade buscarRamoAtividadeChave(Long chavePrimaria) throws NegocioException {

		return (RamoAtividade) this.getControladorRamoAtividade().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a modalidade
	 * aquecimento pela chave primária.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Uma modalidadeAquecimento.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModalidadeAquecimento buscarModalidadeAquecimentoChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterModalidadeAquecimento(chavePrimaria);
	}

	/**
	 * Método responsável por validar os dados do
	 * ramo de atividade de imóvel.
	 *
	 * @param chavePrimariaSegmento
	 *            Chave primária de segmento.
	 * @param chavePrimariaRamoAtividade
	 *            Chave primária de ramo de
	 *            atividade .
	 * @param quantidadeEconomia
	 *            Quantidade economia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosImovelRamoAtividade(Long chavePrimariaSegmento, Long chavePrimariaRamoAtividade, Integer quantidadeEconomia)
					throws NegocioException {

		this.getControladorImovel().validarDadosImovelRamoAtividade(chavePrimariaSegmento, chavePrimariaRamoAtividade, quantidadeEconomia);
	}

	/**
	 * Método responsável por validar os dados do
	 * ponto de consumo de imóvel.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException {

		this.getControladorImovel().validarDadosPontoConsumo(pontoConsumo);
	}

	/**
	 * Verificar duplicidade endereco remoto.
	 *
	 * @param indexLista the index lista
	 * @param enderecoRemoto the endereco remoto
	 * @param listaPontoConsumo the lista ponto consumo
	 * @throws NegocioException the GGAS exception
	 */
	public void verificarDuplicidadeEnderecoRemoto(Integer indexLista, String enderecoRemoto, List<PontoConsumo> listaPontoConsumo)
					throws NegocioException {

		this.getControladorImovel().verificarDuplicidadeEnderecoRemoto(indexLista, enderecoRemoto, listaPontoConsumo);
	}

	/**
	 * Método responsável por listar os pavimentos
	 * de calçada do sistema.
	 *
	 * @return coleção de pavimentos de calçada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PavimentoCalcada> listarPavimentoCalcada() throws NegocioException {

		return getControladorImovel().listarPavimentoCalcada();
	}

	/**
	 * Método responsável por listar os pavimentos
	 * de rua do sistema.
	 *
	 * @return coleção de pavimentos de rua.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PavimentoRua> listarPavimentoRua() throws NegocioException {

		return getControladorImovel().listarPavimentoRua();
	}

	/**
	 * Método responsável por listar os padrões de
	 * construção do sistema.
	 *
	 * @return coleção de padrões de construção.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PadraoConstrucao> listarPadraoConstrucao() throws NegocioException {

		return getControladorImovel().listarPadraoConstrucao();
	}

	/**
	 * Método responsável por listar os perfis de
	 * imóvel do sistema.
	 *
	 * @return coleção de perfis de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PerfilImovel> listarPerfilImovel() throws NegocioException {

		return getControladorImovel().listarPerfilImovel();
	}

	/**
	 * Método responsável por listar as faixas de
	 * área construída.
	 *
	 * @return coleção de faixas de área
	 *         construída.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AreaConstruidaFaixa> listarAreaConstruidaFaixa() throws NegocioException {

		return getControladorAreaConstruidaFaixa().listarAreaConstruidaFaixa();
	}

	/**
	 * Método responsável por listar os tipos de
	 * botijão do sistema.
	 *
	 * @return coleção de tipos de botijão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoBotijao> listarTipoBotijao() throws NegocioException {

		return getControladorImovel().listarTipoBotijao();
	}

	/**
	 * Método responsável por listar os
	 * tipos de combustiveis.
	 *
	 * @return coleção de modalidades uso.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarTipoCombustivel() throws NegocioException {

		return getControladorImovel().listarTipoCombustivel();
	}

	/**
	 * Método responsável por listas as
	 * modalidades uso.
	 *
	 * @return coleção de modalidades uso.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarModalidadeUso() throws NegocioException {

		return getControladorImovel().listarModalidadeUso();
	}

	/**
	 * Método responsável por listar as
	 * modalidades de medição de imóvel.
	 *
	 * @return coleção de modalidades de medição
	 *         de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ModalidadeMedicaoImovel> listarModalidadeMedicaoImovel() throws NegocioException {

		return getControladorImovel().listarModalidadeMedicaoImovel();
	}

	/**
	 * Método responsável por listar as empresas
	 * classificadas como construtoras do sistema.
	 *
	 * @return coleção de empresas classificadas
	 *         como construtoras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Empresa> listarEmpresasConstrutoras() throws NegocioException {

		return getControladorEmpresa().listarEmpresasConstrutoras();
	}

	/**
	 * Método responsável por listar os materiais
	 * de rede do sistema.
	 *
	 * @return coleção de materiais de rede.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<RedeMaterial> listarRedeMaterial() throws NegocioException {

		return getControladorRede().listarRedeMaterial();
	}

	/**
	 * Método responsável por listar os diâmetros
	 * de rede do sistema.
	 *
	 * @return coleção de diâmetros de rede.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<RedeDiametro> listarRedeDiametro() throws NegocioException {

		return getControladorRede().listarRedeDiametro();
	}

	/**
	 * Método responsável por criar um imóvel.
	 *
	 * @return imovel um imóvel do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Imovel criarImovel() throws NegocioException {

		return (Imovel) this.getControladorImovel().criar();
	}

	/**
	 * Método responsável por buscar a face de
	 * quadra pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da face de
	 *            quadra
	 * @return Uma quadra cadastrada no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public QuadraFace buscarQuadraFacePorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorQuadra().obterQuadraFace(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a situação de
	 * imóvel pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da situação de
	 *            imóvel.
	 * @return Uma situação de imóvel cadastrada
	 *         no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SituacaoImovel buscarSituacaoImovelPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterSituacaoImovel(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o pavimento
	 * de calçada pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do pavimento de
	 *            calçada.
	 * @return Um pavimento de calçada cadastrado
	 *         no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PavimentoCalcada buscarPavimentoCalcadaPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterPavimentoCalcada(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o pavimento
	 * de rua pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do pavimento de
	 *            rua.
	 * @return Um pavimento de rua cadastrado no
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PavimentoRua buscarPavimentoRuaPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterPavimentoRua(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o padrão de
	 * construção pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do padrão de
	 *            construção.
	 * @return Um padrão de construção cadastrado
	 *         no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PadraoConstrucao buscarPadraoConstrucaoPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterPadraoConstrucao(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o perfil de
	 * imóvel pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do perfil de
	 *            imóvel.
	 * @return Um perfil de imóvel cadastrado no
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PerfilImovel buscarPerfilImovelPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterPerfilImovel(chavePrimaria);
	}

	/**
	 * Método responsável por buscar a modalidade
	 * de medição de imóvel pelo
	 * código.
	 *
	 * @param codigo
	 *            O código da modalidade de
	 *            medição de imóvel.
	 * @return Uma modalidade de medição de
	 *         imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModalidadeMedicaoImovel buscarModalidadeMedicaoImovelPorCodigo(Integer codigo) throws NegocioException {

		return this.getControladorImovel().obterModalidadeMedicaoImovel(codigo);
	}

	/**
	 * Método responsável por buscar o tipo de
	 * botijão pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            botijão.
	 * @return Um tipo de botijão cadastrado no
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoBotijao buscarTipoBotijaoPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorImovel().obterTipoBotijao(chavePrimaria);
	}

	/**
	 * Método responsável por criar uma rede
	 * interna de imóvel.
	 *
	 * @return Uma rede interna de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RedeInternaImovel criarRedeInternaImovel() throws NegocioException {

		return (RedeInternaImovel) this.getControladorImovel().criarRedeInternaImovel();
	}

	/**
	 * Método responsável por buscar o diâmetro de
	 * rede pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do diâmetro de
	 *            rede.
	 * @return Um diâmetro de rede cadastrado no
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RedeDiametro buscarRedeDiametroPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorRede().obterRedeDiametro(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o material de
	 * rede pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do material de
	 *            rede.
	 * @return Um material de rede cadastrado no
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RedeMaterial buscarRedeMaterialPorChave(Long chavePrimaria) throws NegocioException {

		return this.getControladorRede().obterRedeMaterial(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o motivo de
	 * fim de relacionamento entre
	 * cliente e imóvel pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do motivo de
	 *            fim de relacionamento
	 *            entre cliente e imóvel.
	 * @return Um motivo de fim de relacionamento
	 *         entre cliente e imóvel
	 *         cadastrado no sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MotivoFimRelacionamentoClienteImovel buscarMotivoFimRelacionamentoClienteImovelPorChave(Long chavePrimaria)
			throws NegocioException {

		return this.getControladorImovel().obterMotivoFimRelacionamentoClienteImovel(chavePrimaria);
	}

	/**
	 * Método responsável por listar os motivos de
	 * fim de relacionamento entre
	 * cliente e imóvel do sistema.
	 *
	 * @return coleção de motivos de fim de
	 *         relacionamento entre cliente e
	 *         imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MotivoFimRelacionamentoClienteImovel> listarMotivoFimRelacionamentoClienteImovel() throws NegocioException {

		return getControladorImovel().listarMotivoFimRelacionamentoClienteImovel();
	}

	/**
	 * Método responsável por inserir um imóvel no
	 * sistema.
	 *
	 * @param imovel
	 *            Cliente a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave primária do
	 *         imóvel inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirImovel(Imovel imovel) throws GGASException {

		return this.getControladorImovel().inserirImovel(imovel);
	}

	/**
	 * Método responsável por inserir um imóvel no sistema.
	 *
	 * @param imovel Cliente a ser inserido no sistema.
	 * @param veioDoChamado indicador de fluxo
	 * @return chavePrimeria Chave primária do imóvel inserido.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public Long inserirImovel(Imovel imovel, Boolean veioDoChamado) throws GGASException {

		return this.getControladorImovel().inserirImovel(imovel, veioDoChamado);
	}

	/**
	 * Método responsável por alterar um imóvel no
	 * sistema.
	 *
	 * @param imovel
	 *            Imóvel a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do imóvel
	 *         inserido.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarImovel(Imovel imovel) throws NegocioException {

		this.getControladorImovel().atualizarImovel(imovel);
	}

	/**
	 * Método responsável por obter o controlador
	 * de funcionário.
	 *
	 * @return ControladorFuncionario O
	 *         controlador de funcionário.
	 */
	private ControladorFuncionario getControladorFuncionario() {

		return (ControladorFuncionario) serviceLocator.getControladorNegocio(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de unidade organizacional.
	 *
	 * @return ControladorUnidadeOrganizacional O
	 *         controlador de unidade
	 *         organizacional.
	 */
	private ControladorUnidadeOrganizacional getControladorUnidadeOrganizacional() {

		return (ControladorUnidadeOrganizacional) serviceLocator
						.getControladorNegocio(ControladorUnidadeOrganizacional.BEAN_ID_CONTROLADOR_UNIDADE_ORGANIZACIONAL);
	}

	/**
	 * Gets the controlador relatorio volumes faturados segmento.
	 *
	 * @return the controlador relatorio volumes faturados segmento
	 */
	private ControladorRelatorioVolumesFaturadosSegmento getControladorRelatorioVolumesFaturadosSegmento() {

		return (ControladorRelatorioVolumesFaturadosSegmento) serviceLocator
						.getControladorNegocio(ControladorRelatorioVolumesFaturadosSegmento.BEAN_ID_CONTROLADOR_RELATORIO_VOLUMES_FATURADOS_SEGMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de perfil quadra.
	 *
	 * @return ControladorPerfilQuadra O
	 *         controlador de perfil quadra.
	 */
	private ControladorPerfilQuadra getControladorPerfilQuadra() {

		return (ControladorPerfilQuadra) serviceLocator.getControladorNegocio(ControladorPerfilQuadra.BEAN_ID_CONTROLADOR_PERFIL_QUADRA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de parâmetros.
	 *
	 * @return ControladorParametroSistema O
	 *         controlador de parâmetros
	 */
	private ControladorParametroSistema getControladorParametroSistema() {

		return (ControladorParametroSistema) serviceLocator
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de faturamento.
	 *
	 * @return ControladorCronogramaFaturamento O
	 *         controlador de faturamento
	 */
	private ControladorCronogramaFaturamento getControladorCronogramaFaturamento() {

		return (ControladorCronogramaFaturamento) serviceLocator
						.getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de faturamento.
	 *
	 * @return ControladorCronogramaFaturamento O
	 *         controlador de faturamento
	 */
	private ControladorRelatorioResumoVolume getControladorRelatorioResumoVolume() {

		return (ControladorRelatorioResumoVolume) serviceLocator
						.getControladorNegocio(ControladorRelatorioResumoVolume.BEAN_ID_CONTROLADOR_RELATORIO_RESUMO_VOLUME);
	}

	/**
	 * Método responsável por obter o controlador
	 * de usuario.
	 *
	 * @return ControladorUsuario O controlador de
	 *         usuário
	 */
	private ControladorUsuario getControladorUsuario() {

		return (ControladorUsuario) serviceLocator.getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
	}

	/**
	 * Método responsavel por obter o controlador
	 * de HistoricoSenha.
	 *
	 * @return the controlador historico senha
	 */
	private ControladorHistoricoSenha getControladorHistoricoSenha() {

		return (ControladorHistoricoSenha) serviceLocator
						.getControladorNegocio(ControladorHistoricoSenha.BEAN_ID_CONTROLADOR_HISTORICO_SENHA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de acesso.
	 *
	 * @return ControladorAcesso O controlador de
	 *         acesso
	 */
	private ControladorAcesso getControladorAcesso() {

		return (ControladorAcesso) serviceLocator.getBeanPorID(ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de módulo.
	 *
	 * @return ControladorModulo O controlador de
	 *         módulo
	 */
	public ControladorModulo getControladorModulo() {

		return (ControladorModulo) serviceLocator.getControladorNegocio(ControladorModulo.BEAN_ID_CONTROLADOR_MODULO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de processo.
	 *
	 * @return ControladorProcesso O controlador
	 *         de processo
	 */
	private ControladorProcesso getControladorProcesso() {

		return (ControladorProcesso) serviceLocator.getControladorNegocio(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de arrecadação.
	 *
	 * @return ControladorArrecadacao O
	 *         controlador de arrecadação
	 */
	private ControladorArrecadacao getControladorArrecadacao() {

		return (ControladorArrecadacao) serviceLocator.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de empresa.
	 *
	 * @return ControladorEmpresa O controlador de
	 *         empresa
	 */
	private ControladorEmpresa getControladorEmpresa() {

		return (ControladorEmpresa) serviceLocator.getControladorNegocio(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
	}

	/**
	 * Gets the controlador tabela auxiliar.
	 *
	 * @return the controlador tabela auxiliar
	 */
	private ControladorTabelaAuxiliar getControladorTabelaAuxiliar() {

		return (ControladorTabelaAuxiliar) serviceLocator
						.getControladorNegocio(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR);
	}

	/**
	 * Método responsável por obter o controlado
	 * do volume reservatorio.
	 *
	 * @return ControladorVolumeReservatorio O
	 *         controlador do volume
	 *         reservatorio.
	 */
	private ControladorVolumeReservatorio getControladorVolumeReservatorio() {

		return (ControladorVolumeReservatorio) serviceLocator
						.getControladorNegocio(ControladorVolumeReservatorio.BEAN_ID_CONTROLADOR_VOLUME_RESERVATORIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de faixa de área construída.
	 *
	 * @return ControladorAreaConstruida O
	 *         controlador de faixa de área
	 *         construída.
	 */
	private ControladorAreaConstruidaFaixa getControladorAreaConstruidaFaixa() {

		return (ControladorAreaConstruidaFaixa) serviceLocator
						.getControladorNegocio(ControladorAreaConstruidaFaixa.BEAN_ID_CONTROLADOR_AREA_CONSTRUIDA_FAIXA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de cliente.
	 *
	 * @return ControladorCliente O controlador de
	 *         cliente.
	 */
	private ControladorCliente getControladorCliente() {

		return (ControladorCliente) serviceLocator.getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
	}

	/**
	 * Gets the controlador tarifa.
	 *
	 * @return the controlador tarifa
	 */
	private ControladorTarifa getControladorTarifa() {

		return (ControladorTarifa) serviceLocator.getControladorNegocio(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
	}

	/**
	 * Gets the controlador preco gas.
	 *
	 * @return the controlador preco gas
	 */
	private ControladorPrecoGas getControladorPrecoGas() {

		return (ControladorPrecoGas) serviceLocator.getControladorNegocio(ControladorPrecoGas.BEAN_ID_CONTROLADOR_PRECO_GAS);
	}

	/**
	 * Gets the controlador tipo documento.
	 *
	 * @return the controlador tipo documento
	 */
	private ControladorTipoDocumento getControladorTipoDocumento() {

		return (ControladorTipoDocumento) serviceLocator.getControladorNegocio(ControladorTipoDocumento.BEAN_ID_CONTROLADOR_TIPO_DOCUMENTO);
	}

	/**
	 * Gets the controlador documento modelo.
	 *
	 * @return the controlador documento modelo
	 */
	private ControladorDocumentoModelo getControladorDocumentoModelo() {

		return (ControladorDocumentoModelo) serviceLocator
						.getControladorNegocio(ControladorDocumentoModelo.BEAN_ID_CONTROLADOR_DOCUMENTO_MODELO);
	}

	/**
	 * Gets the controlador documento cobranca.
	 *
	 * @return the controlador documento cobranca
	 */
	private ControladorDocumentoCobranca getControladorDocumentoCobranca() {

		return (ControladorDocumentoCobranca) serviceLocator
						.getControladorNegocio(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
	}

	/**
	 * Gets the controlador entrega documento.
	 *
	 * @return the controlador entrega documento
	 */
	private ControladorEntregaDocumento getControladorEntregaDocumento() {

		return (ControladorEntregaDocumento) serviceLocator
						.getControladorNegocio(ControladorEntregaDocumento.BEAN_ID_CONTROLADOR_ENTRAGA_DOCUMENTO);
	}

	/**
	 * Gets the controlador aviso corte.
	 *
	 * @return the controlador aviso corte
	 */
	private ControladorAvisoCorte getControladorAvisoCorte() {

		return (ControladorAvisoCorte) serviceLocator.getControladorNegocio(ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE);
	}

	/**
	 * Gets the controlador aviso corte emissao.
	 *
	 * @return the controlador aviso corte emissao
	 */
	private ControladorAvisoCorteEmissao getControladorAvisoCorteEmissao() {

		return (ControladorAvisoCorteEmissao) serviceLocator
						.getControladorNegocio(ControladorAvisoCorteEmissao.BEAN_ID_CONTROLADOR_AVISO_CORTE_EMISSAO);
	}

	/**
	 * Método responsável por obter o controlador
	 * dos Tributos.
	 *
	 * @return ControladorGerenciaRegional O
	 *         controlador de gerencia regional.
	 */
	private ControladorTributo getControladorTributo() {

		return (ControladorTributo) serviceLocator.getControladorNegocio(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de gerencia regional.
	 *
	 * @return ControladorGerenciaRegional O
	 *         controlador de gerencia regional.
	 */
	private ControladorGerenciaRegional getControladorGerenciaRegional() {

		return (ControladorGerenciaRegional) serviceLocator
						.getControladorNegocio(ControladorGerenciaRegional.BEAN_ID_CONTROLADOR_GERENCIA_REGIONAL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de muncípio.
	 *
	 * @return ControladorMunicipio O controlador
	 *         de município.
	 */
	private ControladorMunicipio getControladorMunicipio() {

		return (ControladorMunicipio) serviceLocator.getControladorNegocio(ControladorMunicipio.BEAN_ID_CONTROLADOR_MUNICIPIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de localidade.
	 *
	 * @return ControladorLocalidade O controlador
	 *         de localidade.
	 */
	private ControladorLocalidade getControladorLocalidade() {

		return (ControladorLocalidade) serviceLocator.getControladorNegocio(ControladorLocalidade.BEAN_ID_CONTROLADOR_LOCALIDADE);
	}

	/**
	 * Método responsável por obter o controlador
	 * de setor comercial.
	 *
	 * @return ControladorSetorComercial O
	 *         controlador de setor comercial.
	 */
	private ControladorSetorComercial getControladorSetorComercial() {

		return (ControladorSetorComercial) serviceLocator
						.getControladorNegocio(ControladorSetorComercial.BEAN_ID_CONTROLADOR_SETOR_COMERCIAL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de setor censitario.
	 *
	 * @return ControladorSetorCensitario O
	 *         controlador de setor censitario.
	 */
	private ControladorSetorCensitario getControladorSetorCensitario() {

		return (ControladorSetorCensitario) serviceLocator
						.getControladorNegocio(ControladorSetorCensitario.BEAN_ID_CONTROLADOR_SETOR_CENSITARIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de zona bloqueio.
	 *
	 * @return ControladorZonaBloqueio O
	 *         controlador de zona bloqueio.
	 */
	private ControladorZonaBloqueio getControladorZonaBloqueio() {

		return (ControladorZonaBloqueio) serviceLocator.getControladorNegocio(ControladorZonaBloqueio.BEAN_ID_CONTROLADOR_ZONA_BLOQUEIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de imovel.
	 *
	 * @return ControladorImovel O controlador de
	 *         imovel.
	 */
	private ControladorImovel getControladorImovel() {

		return (ControladorImovel) serviceLocator.getControladorNegocio(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de ClienteImovel.
	 *
	 * @return ControladorImovel O controlador de
	 *         ClienteImovel.
	 */
	private ControladorClienteImovel getControladorClienteImovel() {

		return (ControladorClienteImovel) serviceLocator.getControladorNegocio(ControladorClienteImovel.BEAN_ID_CONTROLADOR_CLIENTE_IMOVEL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de area tipo.
	 *
	 * @return ControladorAreaTipo O controlador
	 *         de area tipo.
	 */
	private ControladorAreaTipo getControladorAreaTipo() {

		return (ControladorAreaTipo) serviceLocator.getControladorNegocio(ControladorAreaTipo.BEAN_ID_CONTROLADOR_AREA_TIPO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de segmento.
	 *
	 * @return ControladorSegmento O controlador
	 *         de segmento.
	 */
	private ControladorSegmento getControladorSegmento() {

		return (ControladorSegmento) serviceLocator.getControladorNegocio(ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de quadra.
	 *
	 * @return ControladorQuadra O controlador de
	 *         quadra.
	 */
	private ControladorQuadra getControladorQuadra() {

		return (ControladorQuadra) serviceLocator.getControladorNegocio(ControladorQuadra.BEAN_ID_CONTROLADOR_QUADRA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Ramo Atividade.
	 *
	 * @return ControladorRamoAtividade O
	 *         controlador de ramo atividade.
	 */
	private ControladorRamoAtividade getControladorRamoAtividade() {

		return (ControladorRamoAtividade) serviceLocator.getControladorNegocio(ControladorRamoAtividade.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Papel.
	 *
	 * @return ControladorPapel Controlador de
	 *         papel.
	 */
	private ControladorPapel getControladorPapel() {

		return (ControladorPapel) serviceLocator.getControladorNegocio(ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Tronco.
	 *
	 * @return Controlador de tronco.
	 */
	private ControladorTronco getControladorTronco() {

		return (ControladorTronco) serviceLocator.getControladorNegocio(ControladorTronco.BEAN_ID_CONTROLADOR_TRONCO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de ramal.
	 *
	 * @return Controlador de ramal.
	 */
	private ControladorRamal getControladorRamal() {

		return (ControladorRamal) serviceLocator.getControladorNegocio(ControladorRamal.BEAN_ID_CONTROLADOR_RAMAL);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Zeis.
	 *
	 * @return Controlador Zeis.
	 */
	private ControladorZeis getControladorZeis() {

		return (ControladorZeis) serviceLocator.getControladorNegocio(ControladorZeis.BEAN_ID_CONTROLADOR_ZEIS);
	}

	/**
	 * Método responsável por obter o controlador
	 * de ERP.
	 *
	 * @return ControladorErp O controlador de
	 *         Erp.
	 */
	private ControladorErp getControladorErp() {

		return (ControladorErp) serviceLocator.getControladorNegocio(ControladorErp.BEAN_ID_CONTROLADOR_ERP);
	}

	/**
	 * Método responsável por obter o controlador
	 * de funcionário.
	 *
	 * @return ControladorEndereco O controlador
	 *         de endereço.
	 */
	private ControladorEndereco getControladorEndereco() {

		return (ControladorEndereco) serviceLocator.getControladorNegocio(ControladorEndereco.BEAN_ID_CONTROLADOR_ENDERECO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Unidade de Negócio.
	 *
	 * @return Controlador de Unidade de Negócio.
	 */
	private ControladorUnidadeNegocio getControladorUnidadeNegocio() {

		return (ControladorUnidadeNegocio) serviceLocator
						.getControladorNegocio(ControladorUnidadeNegocio.BEAN_ID_CONTROLADOR_UNIDADE_NEGOCIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Serviço Prestado.
	 *
	 * @return Controlador de Serviço Prestado.
	 */
	private ControladorServicoPrestado getControladorServicoPrestado() {

		return (ControladorServicoPrestado) serviceLocator
						.getControladorNegocio(ControladorServicoPrestado.BEAN_ID_CONTROLADOR_SERVICO_PRESTADO);
	}

	/**
	 * Método responsável por obter o controlador
	 * contabil.
	 *
	 * @return ControladorContabil O controlador
	 *         de faturamento
	 */
	private ControladorContabil getControladorContabil() {

		return (ControladorContabil) serviceLocator.getControladorNegocio(ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);
	}

	/**
	 * Método reponsáve por obter o
	 * ControladorRelatorioDeInadimplentes.
	 *
	 * @return ControladorRelatorioDeInadimplentes
	 */
	private ControladorRelatorioDeInadimplentes getControladorRelatorioDeInadimplentes() {

		return (ControladorRelatorioDeInadimplentes) serviceLocator
						.getControladorNegocio(ControladorRelatorioDeInadimplentes.BEAN_ID_CONTROLADOR_RELATORIO_DE_INADIMPLENTES);

	}

	/**
	 * Gets the controlador relatorio faturas emitidas.
	 *
	 * @return the controlador relatorio faturas emitidas
	 */
	private ControladorRelatorioFaturasEmitidas getControladorRelatorioFaturasEmitidas() {

		return (ControladorRelatorioFaturasEmitidas) serviceLocator
						.getControladorNegocio(ControladorRelatorioFaturasEmitidas.BEAN_ID_CONTROLADOR_RELATORIO_FATURAS_EMITIDAS);
	}

	/**
	 * Gets the controlador relatorio titulos atraso.
	 *
	 * @return the controlador relatorio titulos atraso
	 */
	private ControladorRelatorioTitulosAtraso getControladorRelatorioTitulosAtraso() {

		return (ControladorRelatorioTitulosAtraso) serviceLocator
						.getControladorNegocio(ControladorRelatorioTitulosAtraso.BEAN_ID_CONTROLADOR_RELATORIO_TITULOS_ATRASO);
	}

	/**
	 * Gets the controlador relatorio titulos aberto por data vencimento.
	 *
	 * @return the controlador relatorio titulos aberto por data vencimento
	 */
	private ControladorRelatorioTituloAbertoPorDataVencimento getControladorRelatorioTitulosAbertoPorDataVencimento() {

		ControladorNegocio controlador =
				serviceLocator
						.getControladorNegocio(ControladorRelatorioTituloAbertoPorDataVencimento.BEAN_ID_CONTROLADOR_RELATORIO_TITULOS);
		return (ControladorRelatorioTituloAbertoPorDataVencimento) controlador;
	}

	/**
	 * Gets the controlador relatorio cobranca recuperacao penalidades.
	 *
	 * @return the controlador relatorio cobranca recuperacao penalidades
	 */
	private ControladorRelatorioCobrancaRecuperacaoPenalidades getControladorRelatorioCobrancaRecuperacaoPenalidades() {

		ControladorNegocio controlador =
				serviceLocator
						.getControladorNegocio(ControladorRelatorioCobrancaRecuperacaoPenalidades.BEAN_ID_CONTROLADOR_RELATORIO_COBRANCA);
		return (ControladorRelatorioCobrancaRecuperacaoPenalidades) controlador;
	}

	/**
	 * Gets the controlador relatorio posicao contas receber.
	 *
	 * @return the controlador relatorio posicao contas receber
	 */
	private ControladorRelatorioPosicaoContasReceber getControladorRelatorioPosicaoContasReceber() {

		return (ControladorRelatorioPosicaoContasReceber) serviceLocator
						.getControladorNegocio(ControladorRelatorioPosicaoContasReceber.BEAN_ID_CONTROLADOR_RELATORIO_POSICAO_CONTAS_A_RECEBER);
	}

	/**
	 * Gets the controlador relatorio cliente unidade consumidora.
	 *
	 * @return the controlador relatorio cliente unidade consumidora
	 */
	private ControladorRelatorioClienteUnidadeConsumidora getControladorRelatorioClienteUnidadeConsumidora() {

		return (ControladorRelatorioClienteUnidadeConsumidora) serviceLocator
						.getControladorNegocio(ControladorRelatorioClienteUnidadeConsumidora.BEAN_ID_CONTROLADOR_RELATORIO_CLIENTE_UNIDADE_CONSUMIDORA);
	}

	/**
	 * Gets the controlador relatorio analise consumo.
	 *
	 * @return the controlador relatorio analise consumo
	 */
	private ControladorRelatorioAnaliseConsumo getControladorRelatorioAnaliseConsumo() {

		return (ControladorRelatorioAnaliseConsumo) serviceLocator
						.getControladorNegocio(ControladorRelatorioAnaliseConsumo.BEAN_ID_CONTROLADOR_RELATORIO_ANALISE_CONSUMO);
	}

	/**
	 * Gets the controlador relatorio quantidades contratadas.
	 *
	 * @return the controlador relatorio quantidades contratadas
	 */
	private ControladorRelatorioQuantidadesContratadas getControladorRelatorioQuantidadesContratadas() {

		return (ControladorRelatorioQuantidadesContratadas) serviceLocator
						.getControladorNegocio(ControladorRelatorioQuantidadesContratadas.BEAN_ID_CONTROLADOR_RELATORIO_QUANTIDADES_CONTRATADAS);
	}

	/**
	 * Gets the controlador relatorio faturamento.
	 *
	 * @return the controlador relatorio faturamento
	 */
	private ControladorRelatorioFaturamento getControladorRelatorioFaturamento() {

		return (ControladorRelatorioFaturamento) serviceLocator
						.getControladorNegocio(ControladorRelatorioFaturamento.BEAN_ID_CONTROLADOR_RELATORIO_FATURAMENTO);
	}

	/**
	 * Gets the controlador relatorio descontos concedidos.
	 *
	 * @return the controlador relatorio descontos concedidos
	 */
	private ControladorRelatorioDescontosConcedidos getControladorRelatorioDescontosConcedidos() {

		return (ControladorRelatorioDescontosConcedidos) serviceLocator
						.getControladorNegocio(ControladorRelatorioDescontosConcedidos.BEAN_ID_CONTROLADOR_RELATORIO_DESCONTOS_CONCEDIDOS);
	}

	/**
	 * Gets the controlador relatorio perfil consumo.
	 *
	 * @return the controlador relatorio perfil consumo
	 */
	private ControladorRelatorioPerfilConsumo getControladorRelatorioPerfilConsumo() {

		return (ControladorRelatorioPerfilConsumo) serviceLocator
						.getControladorNegocio(ControladorRelatorioPerfilConsumo.BEAN_ID_CONTROLADOR_RELATORIO_PERFIL_CONSUMO);
	}

	/**
	 * Gets the controlador relatorio prazo minimo antecedencia.
	 *
	 * @return the controlador relatorio prazo minimo antecedencia
	 */
	private ControladorRelatorioPrazoMinimoAntecedencia getControladorRelatorioPrazoMinimoAntecedencia() {

		return (ControladorRelatorioPrazoMinimoAntecedencia) serviceLocator
						.getControladorNegocio(ControladorRelatorioPrazoMinimoAntecedencia.BEAN_ID_CONTROLADOR_RELATORIO_PRAZO_MINIMO_ANTECEDENCIA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de consulta.
	 *
	 * @return ControladorConsulta O controlador
	 *         de consulta.
	 */
	private ControladorConsulta getControladorConsulta() {

		return (ControladorConsulta) serviceLocator.getControladorNegocio(ControladorConsulta.BEAN_ID_CONTROLADOR_CONSULTA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de menu.
	 *
	 * @return ControladorParametroSistema O
	 *         controlador de parâmetros
	 */
	private ControladorMenu getControladorMenu() {

		return (ControladorMenu) serviceLocator.getControladorNegocio(ControladorMenu.BEAN_ID_CONTROLADOR_MENU);
	}

	/**
	 * Método para obter o controladorAlcada.
	 *
	 * @return the controlador alcada
	 */
	private ControladorAlcada getControladorAlcada() {

		return (ControladorAlcada) serviceLocator.getControladorNegocio(ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);
	}
	
	/**
	 * Método para obter o controladorDocumentoLayout.
	 *
	 * @return the controlador documento layout
	 */
	private ControladorDocumentoLayout getControladorDocumentoLayout() {

		return (ControladorDocumentoLayout) serviceLocator
						.getControladorNegocio(ControladorDocumentoLayout.BEAN_ID_CONTROLADOR_DOCUMENTO_LAYOUT);
	}
	
	
	private ControladorServicoAutorizacao getControladorServicoAutorizacao() {
		return serviceLocator.getControladorServicoAutorizacao();
	}

	/**
	 * Método responsável por validar os dados do
	 * contato do imóvel.
	 *
	 * @param contato
	 *            O contato do imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosContatoImovel(ContatoImovel contato) throws NegocioException {

		this.getControladorImovel().validarDadosContatoImovel(contato);
	}

	/**
	 * Método responsável por verificar se já
	 * existe um contato no imóvel com o mesmo
	 * email.
	 *
	 * @param indexLista the index lista
	 * @param listaContatoImovel the lista contato imovel
	 * @param contatoImovel the contato imovel
	 * @throws NegocioException aso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarEmailContatoExistenteDoImovel(Integer indexLista, Collection<ContatoImovel> listaContatoImovel,
					ContatoImovel contatoImovel) throws NegocioException {

		this.getControladorImovel().verificarEmailContatoExistente(indexLista, listaContatoImovel, contatoImovel);
	}

	/**
	 * Método responsável por estornar devoluções
	 * selecionadas.
	 *
	 * @param filtro filtro com os valores da devolução
	 * @param dadosAuditoria dados para auditoria da devolução
	 * @throws NegocioException the negocio exception
	 */
	public void estornarDevolucao(Map<String, Object> filtro, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorDevolucao().estornarDevolucao(filtro, dadosAuditoria);
	}

	/**
	 * Método responsável por remover os imóveis
	 * selecionados.
	 *
	 * @param chavesPrimarias As chaves primárias dos imóveis
	 *            selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerImoveis(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorImovel().removerImovel(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * imóveis.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos imóveis.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverImoveis(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorImovel().validarRemoverImoveis(chavesPrimarias);
	}

	/**
	 * Método responsável por buscar o setor
	 * censitario pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do setor
	 *            censitario.
	 * @return Um setor censitario.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SetorCensitario buscarSetorCensitarioChave(Long chavePrimaria) throws NegocioException {

		return (SetorCensitario) this.getControladorSetorCensitario().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o area tipo
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da area tipo.
	 * @return Um Area Tipo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AreaTipo buscarAreaTipoChave(Long chavePrimaria) throws NegocioException {

		return (AreaTipo) this.getControladorAreaTipo().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar zona bloqueio
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária de zona
	 *            bloqueio
	 * @return Uma Zona Bloqueio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ZonaBloqueio buscarZonaBloqueioChave(Long chavePrimaria) throws NegocioException {

		return (ZonaBloqueio) this.getControladorZonaBloqueio().obter(chavePrimaria);
	}

	/**
	 * Método responsável por criar uma Rede.
	 *
	 * @return Rede uma entidade do tipo Rede do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rede criarRede() throws NegocioException {

		return (Rede) this.getControladorRede().criar();
	}

	/**
	 * Método responsável por inserir uma Rede.
	 *
	 * @param rede Uma entidade do tipo Rede.
	 * @return A chave primária da Rede inserida.
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirRede(Rede rede) throws NegocioException {

		return this.getControladorRede().inserir(rede);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover rede(s).
	 *
	 * @param chavesPrimarias
	 *            Array de Chaves das localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverRede(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorRede().validarRemoverRede(chavesPrimarias);
	}

	/**
	 * Método responsável por remover as redes.
	 *
	 * @param chavesPrimarias
	 *            Chaves das redes.
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerRede(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorRede().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * Rede.
	 *
	 * @param rede
	 *            Uma variavel de referencia do
	 *            tipo Rede do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarRede(Rede rede) throws GGASException {

		this.getControladorRede().atualizar(rede);
	}

	/**
	 * Método responsável por validar o número de
	 * série do medidor.
	 *
	 * @param numeroSerie Número de Série do medidor.
	 * @throws NegocioException the GGAS exception
	 */
	public void validarNumeroSerie(String numeroSerie) throws NegocioException {

		this.getControladorMedidor().validarNumeroSerie(numeroSerie);
	}

	/**
	 * Método responsável por obter um tipo
	 * leitura pela chave passada por parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return TipoLeitura - {@link TipoLeitura}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoLeitura obterTipoLeitura(Long chave) throws NegocioException {

		return this.getControladorRota().obterTipoLeitura(chave);
	}

	/**
	 * Método responsável por obter um grupo
	 * faturamento pela chave passada por
	 * parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return GrupoFaturamento - {@link GrupoFaturamento}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public GrupoFaturamento obterGrupoFaturamento(Long chave) throws NegocioException {

		return this.getControladorRota().obterGrupoFaturamento(chave);
	}

	/**
	 * Método responsável por obter um leiturista
	 * pela chave passada por parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return Leiturista {@link Leiturista}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Leiturista obterLeiturista(Long chave) throws NegocioException {

		return this.getControladorRota().obterLeiturista(chave);
	}

	/**
	 * Método responsável por obter a
	 * periodicidade da rota pela chave passada
	 * por parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return Periodicidade - {@link Periodicidade}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Periodicidade obterPeriodicidade(Long chave) throws NegocioException {

		return this.getControladorRota().obterPeriodicidade(chave);
	}

	/**
	 * Método responsável por obter o tipo da rota
	 * pela chave passada por parâmetro.
	 *
	 * @param chave - {@link Long}
	 *            .
	 * @return TipoRota - {@link TipoRota}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TipoRota obterTipoRota(Long chave) throws NegocioException {

		return this.getControladorRota().obterTipoRota(chave);
	}

	/**
	 * Método responsável por inserir uma rota no
	 * sistema.
	 *
	 * @param rota the rota
	 * @param pontosConsumoAssociados the pontos consumo associados
	 * @return chavePrimeria da rota inserida
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public Long inserirRota(Rota rota, Map<Long, Object> pontosConsumoAssociados) throws GGASException, IllegalAccessException,
					InvocationTargetException {

		return this.getControladorRota().inserirRota(rota, pontosConsumoAssociados);
	}

	/**
	 * Método responsável por alterar uma rota.
	 *
	 * @param rota
	 *            Rota a ser alterada no sistema.
	 * @return chavePrimeria Chave da rota.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarRota(Rota rota) throws GGASException {

		this.getControladorRota().atualizar(rota);
	}

	/**
	 * Método que gera imóveis para serem
	 * preenchidos na tela de inclusão em lote de
	 * imóveis.
	 *
	 * @param imovel the imovel
	 * @param numeroBlocos the numero blocos
	 * @param numeroAndares the numero andares
	 * @param numeroAptAndar the numero apt andar
	 * @return listaImoveis
	 * @throws NegocioException the GGAS exception
	 */
	public List<Imovel> gerarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar)
					throws NegocioException {

		return getControladorImovel().gerarImoveisEmLote(imovel, numeroBlocos, numeroAndares, numeroAptAndar);
	}

	/**
	 * Método para validar a inclusão em lote de
	 * imóveis.
	 *
	 * @param imovel the imovel
	 * @param apartamento the apartamento
	 * @param bloco the bloco
	 * @param numeroBlocos the numero blocos
	 * @param tipoUnidade the tipo unidade
	 * @param numeroSequenciaLeitura the numero sequencia leitura
	 * @exception NegocioException the GGAS exception
	 */
	public void validarInclusaoImoveisEmLote(Imovel imovel, String[] apartamento, String[] bloco, String numeroBlocos, String tipoUnidade,
					String[] numeroSequenciaLeitura) throws NegocioException {

		getControladorImovel().validarInclusaoImoveisEmLote(imovel, apartamento, bloco, numeroBlocos, tipoUnidade, numeroSequenciaLeitura);
	}


	/**
	 * Método responsável por validar se imóvel
	 * pode carregar mais imóveis em lote.
	 *
	 * @param imovel the imovel
	 * @param numeroBlocos the numero blocos
	 * @param numeroAndares the numero andares
	 * @param numeroAptAndar the numero apt andar
	 * @param inicioApartamento the inicio apartamento
	 * @param incrementoApartamento the incremento apartamento
	 * @param tipoUnidade the tipo unidade
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCarregarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar,
					String inicioApartamento, String incrementoApartamento, String tipoUnidade) throws NegocioException {

		this.getControladorImovel().validarCarregarImoveisEmLote(imovel, numeroBlocos, numeroAndares, numeroAptAndar, inicioApartamento,
						incrementoApartamento, tipoUnidade);
	}

	/**
	 * Método responsável por obter a quantidade
	 * de blocos cadastrados do imóvel.
	 *
	 * @param imovel
	 *            O imóvel condomínio.
	 * @return A quantidade de blocos cadastrados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterQtdBlocosCadastrados(Imovel imovel) throws NegocioException {

		return this.getControladorImovel().obterQtdBlocosCadastrados(imovel);
	}

	/**
	 * Método resposável por obter a quantidade de
	 * blocos que ainda podem ser cadastrados para
	 * o
	 * imóvel.
	 *
	 * @param imovel
	 *            O imóvel condomínio.
	 * @return A quantidade de blocos que ainda
	 *         podem ser cadastrados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterQtdBlocosRestantesImoveisEmLote(Imovel imovel) throws NegocioException {

		return this.getControladorImovel().obterQtdBlocosRestantesImoveisEmLote(imovel);
	}

	/**
	 * Método responsável por gerar o número dos
	 * apartamentos de um imóvel.
	 *
	 * @param numeroBlocos O número de blocos do imóvel.
	 * @param numeroAndares O número de andares de cada
	 *            bloco do imóvel.
	 * @param numeroAptAndar O número de apartamentos por
	 *            andar do imóvel.
	 * @param inicioApartamento O número do primeiro apartamento
	 *            de cada bloco do imóvel.
	 * @param incrementoApartamento O valor que é incrementado no
	 *            número dos apartamentos a cada
	 *            andar.
	 * @return Um vetor com o número dos
	 *         apartamentos de cada bloco do
	 *         imóvel.
	 * @throws NegocioException the GGAS exception
	 */
	public List<String> gerarApartamentosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar,
					String inicioApartamento, String incrementoApartamento) throws NegocioException {

		return this.getControladorImovel().gerarApartamentosImoveisEmLote(numeroBlocos, numeroAndares, numeroAptAndar, inicioApartamento,
						incrementoApartamento);
	}

	/**
	 * Método responsável por gerar o número da
	 * leitura dos imóveis em lote.
	 *
	 * @param qtdApartamentos Quantidade de apartamento
	 * @return Uma lista de numeros
	 * @throws NegocioException the GGAS exception
	 */
	public List<String> gerarNumeroLeituraImoveisEmLote(Integer qtdApartamentos) throws NegocioException {

		return this.getControladorImovel().gerarNumeroLeituraImoveisEmLote(qtdApartamentos);
	}

	/**
	 * Método responsável por replicar o nome dos
	 * blocos de um imóvel condomínio pelos
	 * apartamentos.
	 *
	 * @param numeroBlocos
	 *            O número de blocos do imóvel.
	 * @param numeroAndares
	 *            O número de andares de cada
	 *            bloco do imóvel.
	 * @param numeroAptAndar
	 *            O número de apartamentos por
	 *            andar do imóvel.
	 * @param nomeBlocos
	 *            Os nomes que serão replicados
	 *            pelos apartamentos.
	 * @return Um vetor com os nomes dos blocos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<String> gerarBlocosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar, String[] nomeBlocos)
					throws NegocioException {

		return this.getControladorImovel().gerarBlocosImoveisEmLote(numeroBlocos, numeroAndares, numeroAptAndar, nomeBlocos);
	}

	/**
	 * Método responsável por listar as rotas para
	 * geração de movimentos de
	 * leitura por setor comercial(opcional).
	 *
	 * @param idSetorComercial the id setor comercial
	 * @return Uma coleção de rotas
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> listarRotasGeracaoDadosLeituraPorSetorComercial(Long idSetorComercial) throws NegocioException {

		return getControladorRota().listarRotasGeracaoDadosLeituraPorSetorComercial(idSetorComercial);
	}

	/**
	 * Método responsável por listar as rotas para
	 * a geração de dados de leitura
	 * associadas a um Grupo Faturamento e uma
	 * Periodicidade informada.
	 *
	 * @param idGrupoFaturamento Chave primária do Grupo
	 *            Faturamento
	 * @param idPeriodicidade Chave primária da Periodicidade
	 * @param dataPrevista the data prevista
	 * @return Uma coleção de rotas.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> listarRotasGeracaoDadosLeitura(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException {

		return this.getControladorRota().listarRotasGeracaoDadosLeitura(idGrupoFaturamento, idPeriodicidade, dataPrevista);
	}

	/**
	 * Método reponsável por listar todas as Rotas
	 * que possuem cronograma em atraso.
	 *
	 * @param rotasFiltro the rotas filtro
	 * @return Uma coleção de rotas em atraso.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> listarRotasGeracaoDadosLeituraCronogramaEmAtraso(Collection<Rota> rotasFiltro) throws NegocioException {

		return this.getControladorRota().listarRotasGeracaoDadosLeituraCronogramaEmAtraso(rotasFiltro);
	}

	/**
	 * Método reponsável por listar todas as Rotas
	 * que não possuem cronograma.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @param idPeriodicidade the id periodicidade
	 * @param dataPrevista the data prevista
	 * @return Uma coleção de rotas em atraso.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> listarRotasGeracaoDadosLeituraSemCronograma(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException {

		return this.getControladorRota().listarRotasGeracaoDadosLeituraSemCronograma(idGrupoFaturamento, idPeriodicidade, dataPrevista);
	}

	/**
	 * Método responsável por obter a data
	 * prevista do roteiro baseado
	 * no cronograma da primeira rota adicionada
	 * ao roteiro.
	 *
	 * @param idRota Chave primária da rota.
	 * @param atividadeSistema the atividade sistema
	 * @return data prevista
	 * @throws NegocioException Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public Date obterDataPrevistaDoCronograma(Long idRota, AtividadeSistema atividadeSistema) throws NegocioException {

		return this.getControladorRota().obterDataPrevistaDoCronograma(idRota, atividadeSistema);
	}

	/**
	 * Método responsável por consultar Movimentos
	 * de Leitura não processados e que possuem a
	 * rota
	 * com chave primária
	 * na lista informada.
	 *
	 * @param rotas the rotas
	 * @return Uma coleção de leituraMovimento.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<LeituraMovimento> consultarMovimentosLeituraNaoProcessados(Collection<Rota> rotas) throws NegocioException {

		return this.getControladorLeituraMovimento().consultarMovimentosLeituraNaoProcessados(rotas);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a geracao de
	 * dados para
	 * leitura.
	 *
	 * @param chavesPrimarias As chaves primárias dos pontos
	 *            de consumo.
	 * @param dataPrevistaLeitura dataPrevista de Leitura
	 * @param atividadeSistema the atividade sistema
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarGerarDadosDeLeitura(Long[] chavesPrimarias, String dataPrevistaLeitura, AtividadeSistema atividadeSistema)
					throws NegocioException {

		this.getControladorLeituraMovimento().validarGerarDadosDeLeitura(chavesPrimarias, dataPrevistaLeitura, atividadeSistema);
	}

	/**NegocioException
	 * Método responsável por obter o controlador
	 * de Leitura movimento.
	 *
	 * @return Controlador de leitura movimento.
	 */
	private ControladorLeituraMovimento getControladorLeituraMovimento() {

		return (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Ponto de Consumo.
	 *
	 * @return Controlador de Ponto de Consumo.
	 */
	private ControladorPontoConsumo getControladorPontoConsumo() {

		return (ControladorPontoConsumo) serviceLocator.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Cálculo do Fornecimento de Gás.
	 *
	 * @return Controlador Cálculo do Fornecimento
	 *         de Gás.
	 */
	private ControladorCalculoFornecimentoGas getControladorCalculoFornecimentoGas() {

		return (ControladorCalculoFornecimentoGas) serviceLocator
						.getControladorNegocio(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);
	}

	/**
	 * Método responsável por consultar pontos de
	 * consumo em aberto para geração
	 * de roteiro.
	 *
	 * @param filtro O filtro com os parâmetros para
	 *            a pesquisa.
	 * @param atividadeSistema the atividade sistema
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PontoConsumo> consultarPontosConsumoEmAbertoLeitura(Map<String, Object> filtro, AtividadeSistema atividadeSistema)
					throws NegocioException {

		return getControladorPontoConsumo().consultarPontosConsumoEmAbertoLeitura(filtro, atividadeSistema);
	}

	/**
	 * Método reponsável por obter a referencia e
	 * o ciclo atual da rota do ponto de consumo.
	 *
	 * @param pontoConsumo
	 *            O ponto de consumo
	 * @return Um mapa contendo a referencia e o
	 *         ciclo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, Integer> obterReferenciaCicloAtual(PontoConsumo pontoConsumo) throws NegocioException {

		return getControladorPontoConsumo().obterReferenciaCicloAtual(pontoConsumo);
	}

	/**
	 * Método responsável por listar todos os
	 * pontos de consumo em estado de alerta.
	 *
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PontoConsumo> listarPontosConsumoEmAlerta() throws NegocioException {

		return this.getControladorPontoConsumo().listarPontosConsumoEmAlerta();
	}

	/**
	 * Método responsável por buscar a rota pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da rota.
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy.
	 * @return Uma rota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rota buscarRotaPorChave(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Rota) this.getControladorRota().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por criar um cronograma
	 * de rota.
	 *
	 * @return Um novo cronograma de rota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CronogramaRota criarCronogramaRota() throws NegocioException {

		return (CronogramaRota) this.getControladorRota().criarCronogramaRota();
	}

	/**
	 * Método responsável por obter o cronograma
	 * com referencia e ciclo atual.
	 *
	 * @param rota
	 *            Uma rota
	 * @return Um cronograma
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	public CronogramaRota obterCronogramaRotaReferenciaCicloAtual(Rota rota) throws NegocioException {

		return getControladorRota().obterCronogramaRotaReferenciaCicloAtual(rota);
	}

	/**
	 * Método responsável por validar um
	 * cronograma de rota a ser cadastrada.
	 *
	 * @param cronogramaRota
	 *            Um cronograma de rota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCronogramaRota(CronogramaRota cronogramaRota) throws NegocioException {

		this.getControladorRota().validarCronogramaRota(cronogramaRota);
	}

	/**
	 * Método responsável por listar os
	 * cronogramas que já foram realizadas de
	 * uma rota.
	 *
	 * @param idRota
	 *            Chave primária da rota.
	 * @return coleção de cronogramas de rota já
	 *         realizadas.
	 */
	public Collection<CronogramaRota> listarCronogramasRotaRealizadasPorRota(long idRota) {

		return this.getControladorRota().listarCronogramasRotaRealizadasPorRota(idRota);
	}

	/**
	 * Método responsável por retornar um novo
	 * cronograma de rota a partir de
	 * uma lista de cronogramas existentes para
	 * uma rota.
	 *
	 * @param instalacaoMedidor the instalacao medidor
	 * @param pontoConsumo the ponto consumo
	 * @param medidor the medidor
	 * @return Um cronogroma de rota.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal setarLeituraAnterior(InstalacaoMedidor instalacaoMedidor, PontoConsumo pontoConsumo, Medidor medidor)
					throws NegocioException {

		return this.getControladorHistoricoMedicao().setarLeituraAnterior(instalacaoMedidor, pontoConsumo, medidor);
	}

	/**
	 * Método responsável por retornar um novo
	 * cronograma de rota a partir de
	 * uma lista de cronogramas existentes para
	 * uma rota.
	 *
	 * @param instalacaoMedidor the instalacao medidor
	 * @param pontoConsumo the ponto consumo
	 * @param medidor the medidor
	 * @return Um cronogroma de rota.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Date setarDataLeituraAnterior(InstalacaoMedidor instalacaoMedidor, PontoConsumo pontoConsumo, Medidor medidor)
					throws NegocioException {

		return this.getControladorHistoricoMedicao().setarDataLeituraAnterior(instalacaoMedidor, pontoConsumo, medidor);
	}

	/**
	 * Método responsável por validar se o
	 * cronograma está em alerta. O alerta
	 * de cronogroma informa que existe outro
	 * cronograma com a mesma data
	 * prevista e mesmo leiturista para uma rota
	 * diferente.
	 *
	 * @param cronogramaRota
	 *            Cronograma de rota a ser
	 *            válidado
	 * @param idRota
	 *            Chave primária da rota.
	 * @return true, caso exista.
	 */
	public boolean validarCronogramaRotaEmAlerta(CronogramaRota cronogramaRota, long idRota) {

		return this.getControladorRota().validarCronogramaRotaEmAlerta(cronogramaRota, idRota);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo que não possuem número
	 * de sequência de leitura.
	 *
	 * @param grupoFaturamento the grupo faturamento
	 * @return Coleção de pontos de consumo.
	 */
	public Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(GrupoFaturamento grupoFaturamento) {

		return this.getControladorPontoConsumo().listarPontosConsumoNaoSequenciadas(grupoFaturamento);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo que não possuem número
	 * de sequência de leitura.
	 *
	 * @param idPeriodicidade the id periodicidade
	 * @param idTipoLeitura the id tipo leitura
	 * @return Coleção de pontos de consumo.
	 */
	public Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(Long idPeriodicidade, Long idTipoLeitura) {

		return this.getControladorPontoConsumo().listarPontosConsumoNaoSequenciadas(idPeriodicidade, idTipoLeitura);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo que não possuem número
	 * de sequência de leitura.
	 *
	 * @param idPeriodicidade the id periodicidade
	 * @param idTipoLeitura the id tipo leitura
	 * @return Coleção de pontos de consumo.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<GrupoFaturamento> listarGrupoFaturamento(Long idPeriodicidade, Long idTipoLeitura) throws NegocioException {

		return this.getControladorRota().listarGrupoFaturamento(idPeriodicidade, idTipoLeitura);
	}

	/**
	 * Método responsável por consultar pontos de
	 * consumo pelo filtro informado.
	 *
	 * @param filtro O filtro com os parâmetros para
	 *            a pesquisa.
	 * @param propriedadesLazy the propriedades lazy
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PontoConsumo> consultarPontosConsumo(Map<String, Object> filtro, String... propriedadesLazy) throws NegocioException {

		return this.getControladorPontoConsumo().consultarPontosConsumo(filtro, propriedadesLazy);
	}
	
	/**
	 * Obtem o ponto de consumo
	 * 
	 * @param chavePrimaria - codigo do ponto de consumo
	 * @return ponto de consumo
	 **/	
	public PontoConsumo obterPontoConsumo(long chavePrimaria) {
		return this.getControladorPontoConsumo().obterPontoConsumo(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o ponto de
	 * consumo pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do ponto de
	 *            consumo.
	 * @return Um Imovel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PontoConsumo buscarPontoConsumoPorChave(long chavePrimaria) throws NegocioException {

		return (PontoConsumo) this.getControladorPontoConsumo().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o ponto de
	 * consumo pela chave primária.
	 *
	 * @param chavePrimaria A chave primária do ponto de
	 *            consumo.
	 * @param propriedadesLazy the propriedades lazy
	 * @return Um Imovel.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PontoConsumo buscarPontoConsumoPorChave(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (PontoConsumo) this.getControladorPontoConsumo().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por listar as rotas para
	 * remanejamento de pontos de
	 * consumo. Apenas a rota que está sendo
	 * informada não será retornada.
	 *
	 * @param rota
	 *            Rota que está sendo alterada.
	 * @return Uma coleção de Rotas.
	 */
	public Collection<Rota> listarRotasRemanejamento(Rota rota) {

		return this.getControladorRota().listarRotasRemanejamento(rota);
	}

	/**
	 * Método responsável por listar as rotas para
	 * remanejamento de pontos de
	 * consumo. A rota que está sendo mantida não
	 * será retornada e só serão listadas
	 * as rotas com a periodicidade passada por
	 * parâmetro.
	 *
	 * @param idRotaMantida the id rota mantida
	 * @param idPeriodicidade the id periodicidade
	 * @return Uma coleção de Rotas.
	 */
	public Collection<Rota> listarRotasRemanejamentoPeriodicidade(long idRotaMantida, long idPeriodicidade) {

		return this.getControladorRota().listarRotasRemanejamentoPeriodicidade(idRotaMantida, idPeriodicidade);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo de uma rota.
	 *
	 * @param chavePrimariaRota
	 *            Chave primária de uma rota
	 * @return Coleção de pontos de consumo.
	 */
	public Collection<PontoConsumo> listarPontosConsumoPorChaveRota(long chavePrimariaRota) {

		return this.getControladorPontoConsumo().listarPontosConsumoPorChaveRota(chavePrimariaRota);
	}

	/**
	 * Método que retorna uma lista os pontos de
	 * consumo das chaves passadas pelo parâmetro.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return Lista de pontos de consumo
	 */
	public Collection<PontoConsumo> listarPontosConsumo(String[] chavesPrimarias) {

		return getControladorPontoConsumo().listarPontosConsumo(chavesPrimarias);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 *
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException {

		return this.getControladorHistoricoMedicao().consultarHistoricoMedicao(chavePrimariaPontoConsumo);
	}

	/**
	 * Método reponsável por verificar se houve
	 * uma concessao de crédito.
	 *
	 * @param idHistoricoMedicao
	 *            A chave primária do histórico
	 *            medição
	 * @param valorLeituraAnteriorModificado
	 *            O valor modificado da leitura
	 *            anterior
	 * @return Um array com os valores, posição
	 *         [0] a difereça creditada, [1] o
	 *         crrédito (saldo +
	 *         diferença)
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal[] verificarConcessaoCredito(Long idHistoricoMedicao, BigDecimal valorLeituraAnteriorModificado) throws NegocioException {

		return this.getControladorHistoricoMedicao().verificarConcessaoCredito(idHistoricoMedicao, valorLeituraAnteriorModificado);
	}

	/**
	 * Método responsável por calcular o crédito
	 * derado.
	 *
	 * @param historicoMedicao the historico medicao
	 * @param creditoVolume O crédito volume
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void calcularCreditoGerado(HistoricoMedicao historicoMedicao, BigDecimal creditoVolume) throws NegocioException {

		this.getControladorHistoricoMedicao().calcularCreditoGerado(historicoMedicao, creditoVolume);
	}

	/**
	 * Método responsável por obter a ultima
	 * leitura do ponto de consumo no ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave do ponto de consumo
	 * @param referencia A referência
	 * @param ciclo O ciclo
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException {

		return this.getControladorHistoricoMedicao().consultarHistoricoMedicao(chavePrimariaPontoConsumo, referencia, ciclo);
	}

	/**
	 * Método responsável por obter todos os
	 * comentários para o ponto de consumo na
	 * referência/ciclo
	 * informada.
	 *
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            A referência
	 * @param ciclo
	 *            O ciclo
	 * @return Uma coleção de Medição Histórico
	 *         Comentário
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<MedicaoHistoricoComentario> consultarMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia,
					int ciclo) throws NegocioException {

		return this.getControladorHistoricoMedicao().consultarMedicaoHistoricoComentario(chavePrimariaPontoConsumo, referencia, ciclo);
	}

	/**
	 * Método responsável por verificar se existe
	 * comentário para os parâmetros informados;.
	 *
	 * @param chavePrimariaPontoConsumo A chave do ponto de consumo
	 * @param referencia A referência
	 * @param ciclo O ciclo
	 * @return True caso exista, False caso não
	 *         exista.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean existeMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia, int ciclo) throws GGASException {

		return this.getControladorHistoricoMedicao().existeMedicaoHistoricoComentario(chavePrimariaPontoConsumo, referencia, ciclo);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e
	 * ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave primaria do ponto de
	 *            consumo
	 * @param referenciaInicial the referencia inicial
	 * @param referenciaFinal the referencia final
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, String referenciaInicial,
					String referenciaFinal) throws NegocioException {

		return this.getControladorHistoricoMedicao().consultarHistoricoAgrupadoMedicao(chavePrimariaPontoConsumo, referenciaInicial,
						referenciaFinal);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e
	 * limitado por ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave primaria do ponto de
	 *            consumo
	 * @param referenciaInicial the referencia inicial
	 * @param referenciaFinal the referencia final
	 * @param quantidadeCiclos the quantidade ciclos
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, String referenciaInicial,
					String referenciaFinal, Integer quantidadeCiclos) throws NegocioException {

		return this.getControladorHistoricoMedicao().consultarHistoricoAgrupadoMedicao(chavePrimariaPontoConsumo, referenciaInicial,
						referenciaFinal, quantidadeCiclos);
	}

	/**
	 * Método reponsável por validar se a colecao
	 * de historico do ponto de consumo está vazia.
	 *
	 * @param listaHistoricoMedicao A lista de historicos
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarPontoConsumoSemHistoricoMedicao(Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException {

		this.getControladorHistoricoMedicao().validarPontoConsumoSemHistoricoMedicao(listaHistoricoMedicao);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 *
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo) throws NegocioException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumo(chavePrimariaPontoConsumo);
	}

	/**
	 * Consultar historico consumo por historico medicao.
	 *
	 * @param chaveHistoricoMedicao the chave historico medicao
	 * @return the historico consumo
	 * @throws NegocioException the negocio exception
	 */
	public HistoricoConsumo consultarHistoricoConsumoPorHistoricoMedicao(Long chaveHistoricoMedicao) throws NegocioException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumoPorHistoricoMedicao(chaveHistoricoMedicao);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo para
	 * simulação.
	 *
	 * @param chavePrimariaPontoConsumo A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumoParaSimulacao(Long chavePrimariaPontoConsumo) throws GGASException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumoParaSimulacao(chavePrimariaPontoConsumo);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e
	 * ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave primaria do ponto de
	 *            consumo
	 * @param indicadorConsumoCiclo Indicador de consumo ciclo
	 * @param referenciaInicial the referencia inicial
	 * @param referenciaFinal the referencia final
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo, boolean indicadorConsumoCiclo,
					String referenciaInicial, String referenciaFinal) throws NegocioException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumo(chavePrimariaPontoConsumo, indicadorConsumoCiclo,
						referenciaInicial, referenciaFinal);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e
	 * ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave primaria do ponto de
	 *            consumo
	 * @param indicadorConsumoCiclo Indicador de consumo ciclo
	 * @param referenciaInicial the referencia inicial
	 * @param referenciaFinal the referencia final
	 * @param quantidadeCiclos the quantidade ciclos
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo, boolean indicadorConsumoCiclo,
					String referenciaInicial, String referenciaFinal, Integer quantidadeCiclos) throws NegocioException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumo(chavePrimariaPontoConsumo, indicadorConsumoCiclo,
						referenciaInicial, referenciaFinal, quantidadeCiclos);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e
	 * ciclo.
	 * param idPontoConsumo A chave do ponto de
	 * consumo
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param numeroCiclo the numero ciclo
	 * @param indicadorConsumoCiclo Indicador de consumo ciclo
	 * @param indicadorFaturamento the indicador faturamento
	 * @param chavePrimariaHistoricoConsumoSintetizador the chave primaria historico consumo sintetizador
	 * @return Uma coleção de histórico
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
					Boolean indicadorConsumoCiclo, Boolean indicadorFaturamento, Long chavePrimariaHistoricoConsumoSintetizador)
					throws NegocioException {

		return getControladorControladorHistoricoConsumo().consultarHistoricoConsumo(idPontoConsumo, anoMesFaturamento, numeroCiclo,
						indicadorConsumoCiclo, indicadorFaturamento, chavePrimariaHistoricoConsumoSintetizador);
	}

	/**
	 * Método reponsável por validar se a colecao
	 * de historico do ponto de consumo está vazia.
	 *
	 * @param listaHistoricoConsumo A lista de historicos
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarPontoConsumoSemHistoricoConsumo(Collection<HistoricoConsumo> listaHistoricoConsumo) throws NegocioException {

		getControladorControladorHistoricoConsumo().validarPontoConsumoSemHistoricoConsumo(listaHistoricoConsumo);
	}

	/**
	 * Método responsável por validar a selecao do
	 * grupo de faturamento.
	 *
	 * @param chavePrimaria
	 *            A chavePrimaria do grupo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarSelecaoGrupoFaturamento(Long chavePrimaria) throws NegocioException {

		getControladorControladorHistoricoConsumo().validarSelecaoGrupoFaturamento(chavePrimaria);
	}

	/**
	 * Método responsável por obter o controlador
	 * de histórico de medição.
	 *
	 * @return O controlador de historico de
	 *         médição
	 */
	private ControladorHistoricoMedicao getControladorHistoricoMedicao() {

		return (ControladorHistoricoMedicao) serviceLocator
						.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de histórico de consumo.
	 *
	 * @return O controlador de historico de
	 *         consumo
	 */
	private ControladorHistoricoConsumo getControladorControladorHistoricoConsumo() {

		return (ControladorHistoricoConsumo) serviceLocator
						.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de histórico anomalia faturamento.
	 *
	 * @return O controlador de historico de
	 *         consumo
	 */
	private ControladorHistoricoAnomaliaFaturamento getControladorHistoricoAnomaliaFaturamento() {

		return (ControladorHistoricoAnomaliaFaturamento) serviceLocator
						.getControladorNegocio(ControladorHistoricoAnomaliaFaturamento.BEAN_ID_CONTROLADOR_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de anormalidade.
	 *
	 * @return O controlador de anormalidade.
	 */
	private ControladorAnormalidade getControladorAnormalidade() {

		return (ControladorAnormalidade) serviceLocator.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
	}

	/**
	 * Método responsável por criar uma
	 * anormalidade leitura.
	 * return AnormalidadeLeitura
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAnormalidadeLeitura() {

		return getControladorAnormalidade().criarAnormalidadeLeitura();
	}

	/**
	 * Método responsável por criar uma
	 * anormalidade consumo.
	 * return AnormalidadeConsumo
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAnormalidadeConsumo() {

		return getControladorAnormalidade().criarAnormalidadeConsumo();
	}

	/**
	 * Método responsável por listas as
	 * anormalidades de leitura.
	 *
	 * @return coleção de anormalidades de
	 *         leitura.
	 */
	public Collection<AnormalidadeLeitura> listarAnormalidadesLeitura() {

		return this.getControladorAnormalidade().listarAnormalidadesLeitura();
	}

	/**
	 * Método responsável por listas as medições
	 * do supervisório.
	 *
	 * @param ocorrencia the ocorrencia
	 * @return the collection
	 */
	public Collection<SupervisorioMedicaoAnormalidade> listarSupervisorioMedicaoAnormalidade(String ocorrencia) {

		return this.getControladorSupervisorio().listarSupervisorioMedicaoAnormalidade(ocorrencia);
	}

	/**
	 * Método responsável por listas as
	 * anormalidades de leitura de Faturamento.
	 *
	 * @return coleção de anormalidades de leitura
	 *         de Faturamento.
	 * @throws NegocioException the negocio exception
	 */
	public Collection<FaturamentoAnormalidade> listarAnormalidadesFaturamento() throws NegocioException {

		return this.getControladorFaturamentoAnormalidade().listaAnormalidadeFaturamento();
	}

	public Collection<FaturamentoAnormalidade> listarAnormalidadesFaturamento(String descricao,
				Boolean habilitado, Boolean indicadorImpedeFaturamento) throws NegocioException {

		return this.getControladorFaturamentoAnormalidade().listaAnormalidadeFaturamento(descricao, habilitado, indicadorImpedeFaturamento);
	}

	/**
	 * Método responsável por listas as
	 * anormalidades de consumo.
	 *
	 * @return coleção de anormalidades de
	 *         consumo.
	 */
	public Collection<AnormalidadeConsumo> listarAnormalidadesConsumo() {

		return this.getControladorAnormalidade().listarAnormalidadesConsumo();
	}

	/**
	 * Método responsável por listas as
	 * anormalidades de consumo.
	 *
	 * @return coleção de anormalidades de
	 *         consumo.
	 */
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadesConsumoComLeitura() {

		return this.getControladorAnormalidade().listarAcaoAnormalidadeConsumoComLeitura();
	}

	/**
	 * Método responsável por listas as
	 * anormalidades de consumo.
	 *
	 * @return coleção de anormalidades de
	 *         consumo.
	 */
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadesConsumoSemLeitura() {

		return this.getControladorAnormalidade().listarAcaoAnormalidadeConsumoSemLeitura();
	}

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 *
	 * @return listaAcaoAnormalidadeLeitura
	 */
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeLeitura() {

		return getControladorAnormalidade().listarAcaoAnormalidadeLeitura();
	}

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 *
	 * @return listaAcaoAnormalidadeLeitura
	 */
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeSemLeitura() {

		return getControladorAnormalidade().listarAcaoAnormalidadeSemLeitura();
	}

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 *
	 * @return listaAcaoAnormalidadeLeitura
	 */
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeComLeitura() {

		return getControladorAnormalidade().listarAcaoAnormalidadeComLeitura();
	}

	/**
	 * Método que lista a acão anormalidade
	 * consumo.
	 *
	 * @return listaAcaoAnormalidadeConsumo
	 */
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumo() {

		return getControladorAnormalidade().listarAcaoAnormalidadeConsumo();
	}

	/**
	 * Método responsável por obter o controlador
	 * de histórico de consumo.
	 *
	 * @return O controlador de histórico de
	 *         consumo.
	 */
	private ControladorHistoricoConsumo getControladorHistoricoConsumo() {

		return (ControladorHistoricoConsumo) serviceLocator
						.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de supervisório.
	 *
	 * @return O controlador de supervisório.
	 */
	private ControladorSupervisorio getControladorSupervisorio() {

		return (ControladorSupervisorio) serviceLocator.getControladorNegocio(ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de integracao.
	 *
	 * @return O controlador de integracao.
	 */
	private ControladorIntegracao getControladorIntegracao() {

		return (ControladorIntegracao) serviceLocator.getControladorNegocio(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
	}

	/**
	 * Listar funcao.
	 *
	 * @return the collection
	 */
	public Collection<IntegracaoFuncao> listarFuncao() {

		return getControladorIntegracao().listarFuncao();
	}

	/**
	 * Listar funcao por sistema.
	 *
	 * @param chavePrimariaSistema the chave primaria sistema
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoSistemaFuncao> listarFuncaoPorSistema(Long chavePrimariaSistema) throws NegocioException {

		return getControladorIntegracao().listarFuncaoPorSistema(chavePrimariaSistema);
	}

	/**
	 * Listar tabelas.
	 *
	 * @param idFuncao the id funcao
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoFuncaoTabela> listarTabelas(Long idFuncao) throws NegocioException {

		return getControladorIntegracao().listarTabelas(idFuncao);
	}

	/**
	 * Listar colunas tabela.
	 *
	 * @param chavePrimariaTabela the chave primaria tabela
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Coluna> listarColunasTabela(Long chavePrimariaTabela) throws NegocioException {

		return getControladorIntegracao().listarColunasTabela(chavePrimariaTabela);
	}

	/**
	 * Listar classe.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<?> listarClasse() throws NegocioException {

		return getControladorIntegracao().listarClasse();
	}

	/**
	 * Listar tabela colunas.
	 *
	 * @param chavePrimariaTabela the chave primaria tabela
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Tabela> listarTabelaColunas(Long chavePrimariaTabela) throws NegocioException {

		return getControladorIntegracao().listarTabelaColunas(chavePrimariaTabela);
	}

	/**
	 * Obter funcao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<IntegracaoFuncao> obterFuncao(Long chavePrimaria) {

		return getControladorIntegracao().obterFuncao(chavePrimaria);
	}

	/**
	 * Método responsável por consultar medição
	 * do supervisório.
	 *
	 * @param filtro Map contendo os parametros da
	 *            pesquisa.
	 * @param isConsultaPorPontoConsumo the is consulta por ponto consumo
	 * @return coleção de medições do supervisório.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro,
					Boolean isConsultaPorPontoConsumo) throws NegocioException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoDiaria(filtro, isConsultaPorPontoConsumo);
	}

	/**
	 * Método responsável por consultar históricos
	 * de consumo para análise de
	 * exceções de leitura de consumo.
	 *
	 * @param filtro
	 *            Map contendo os parametros da
	 *            pesquisa.
	 * @return coleção de históricos de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumoAnaliseExcecoesLeitura(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorHistoricoConsumo().consultarHistoricoConsumoAnaliseExcecoesLeitura(filtro);
	}

	/**
	 * Método para remover da lista os pontos de
	 * consumo sem historico de consumo.
	 *
	 * @param listaPontosConsumo the lista pontos consumo
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<PontoConsumo> removerPontosConsumoDaListaSemHistoricoConsumo(Collection<PontoConsumo> listaPontosConsumo)
					throws NegocioException {

		return this.getControladorHistoricoConsumo().removerPontosConsumoDaListaSemHistoricoConsumo(listaPontosConsumo);
	}

	/**
	 * Método responsável por consultar históricos
	 * de consumo para análise de
	 * exceções de leitura de consumo.
	 *
	 * @param filtro
	 *            Map contendo os parametros da
	 *            pesquisa.
	 * @return coleção de históricos de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaFaturamento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorHistoricoAnomaliaFaturamento().consultarHistoricoAnomFaturamento(filtro);
	}

	/**
	 * Método responsável por consultar históricos
	 * de consumo para análise de
	 * exceções de leitura de consumo.
	 *
	 * @param filtro
	 *            Map contendo os parametros da
	 *            pesquisa.
	 * @return coleção de históricos de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorHistoricoAnomaliaFaturamento().consultarHistoricoAnomaliaFaturamento(filtro);
	}

	/**
	 * Método responsável por validar o Historico
	 * Anomalia para inclusão da fatura.
	 *
	 * @param listaHistoricoAnomaliaFaturamento the lista historico anomalia faturamento
	 * @throws NegocioException the negocio exception
	 */
	public void
					validarInclusaoFaturaHistoricoAnomaliaFaturamento(
									Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento) throws NegocioException {

		this.getControladorHistoricoAnomaliaFaturamento().validarInclusaoFaturaHistoricoAnomaliaFaturamento(
						listaHistoricoAnomaliaFaturamento);
	}

	/**
	 * Método responsável por validar o Historico
	 * Anomalia para inclusão da fatura.
	 *
	 * @param listaHistoricoAnomaliaFaturamento the lista historico anomalia faturamento
	 * @throws NegocioException the negocio exception
	 */
	public void validarAlteracaoFaturaHistoricoAnomaliaFaturamento(
					Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento) throws NegocioException {

		this.getControladorHistoricoAnomaliaFaturamento().validarAlteracaoFaturaHistoricoAnomaliaFaturamento(
						listaHistoricoAnomaliaFaturamento);
	}

	/**
	 * Método responsável por validar o Historico
	 * Anomalia para inclusão da fatura.
	 *
	 * @param contratoAdequacaoParceria {@link ContratoAdequacaoParceriaImpl}
	 * @param request {@link HttpServletRequest}
	 * @param isInclusao {@link Boolean}
	 * @param existsPontoConsumo {@link boolean}
	 * @param contratoSelecionado {@link Contrato}
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param isEmptyIdConsumo {@link boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void validarDadosInclusaoContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			HttpServletRequest request, boolean isInclusao, boolean existsPontoConsumo, Contrato contratoSelecionado,
			PontoConsumo pontoConsumo, boolean isEmptyIdConsumo) throws NegocioException {

		getControladorContratoAdequacaoParceria().validarDadosInclusaoContratoAdequacaoParceria(
				contratoAdequacaoParceria, request, isInclusao, existsPontoConsumo, contratoSelecionado, pontoConsumo,
				isEmptyIdConsumo);
	}

	/**
	 * Método responsável por listar todos os
	 * tipos de consumo cadastrados.
	 *
	 * @return Uma coleção com todos os tipos de
	 *         consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoConsumo> listarTiposConsumo() throws NegocioException {

		return this.getControladorHistoricoConsumo().listarTiposConsumo();
	}

	/**
	 * Método responsável por listar as rotas
	 * associadas a um grupo de
	 * faturamento informado.
	 *
	 * @param chavePrimariaGrupoFaturamento
	 *            Chave primária do grupo de
	 *            faturamento.
	 * @return Uma coleção de Rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rota> listarRotasPorGrupoFaturamento(Long chavePrimariaGrupoFaturamento) throws NegocioException {

		return this.getControladorRota().listarRotasPorGrupoFaturamento(chavePrimariaGrupoFaturamento);
	}

	/**
	 * Método responsável por validar se um imóvel
	 * está cadastrado e ativo.
	 *
	 * @param matricula
	 *            Matrícula do imóvel.
	 * @return true caso cadastrado e ativo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean existeImovelPorMatricula(Long matricula) throws NegocioException {

		return this.getControladorImovel().existeImovelPorMatricula(matricula);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * análise de consumo.
	 *
	 * @param chavesPrimariasAux the chaves primarias aux
	 * @param filtro the filtro
	 * @return O relatório gerado
	 * @throws NegocioException the GGAS exception
	 */
	public byte[] gerarRelatorioAnaliseConsumo(String[] chavesPrimariasAux, Map<String, Object> filtro) throws NegocioException {

		return this.getControladorHistoricoConsumo().gerarRelatorioAnaliseConsumo(chavesPrimariasAux, filtro);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * análise de consumo.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param filtro the filtro
	 * @return O relatório gerado
	 * @throws NegocioException the GGAS exception
	 */
	public byte[] gerarRelatorioAnaliseAnomaliaFatura(Long[] chavesPrimarias, Map<String, Object> filtro) throws NegocioException {

		return this.getControladorHistoricoAnomaliaFaturamento().gerarRelatorioAnomaliaFaturamento(chavesPrimarias, filtro);
	}

	/**
	 * Método responsável por listar as
	 * localidades ativas.
	 *
	 * @return Uma coleção de localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Localidade> listarLocalidades() throws NegocioException {

		return this.getControladorLocalidade().listarLocalidades();
	}

	/**
	 * Método responsável por obter o controlador
	 * de Leiturista.
	 *
	 * @return ControladorLeiturista O controlador
	 *         de leiturista.
	 */
	private ControladorLeiturista getControladorLeiturista() {

		return (ControladorLeiturista) serviceLocator.getControladorNegocio(ControladorLeiturista.BEAN_ID_CONTROLADOR_LEITURISTA);
	}

	/**
	 * Método responsável por consultar os
	 * leituristas de acordo com o filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Leiturista.
	 * @return Uma coleção de leituristas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Leiturista> consultarLeituristas(Map<String, Object> filtro) throws NegocioException {

		return getControladorLeiturista().consultarLeituristas(filtro);
	}

	/**
	 * Método responsável por criar um Leiturista.
	 *
	 * @return Leiturista um leiturista do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Leiturista criarLeiturista() throws NegocioException {

		return (Leiturista) this.getControladorLeiturista().criar();
	}

	/**
	 * Método responsável por inserir um
	 * leiturista no sistema.
	 *
	 * @param leiturista
	 *            Leiturista a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave primária do
	 *         leiturista inserido.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirLeiturista(Leiturista leiturista) throws NegocioException {

		return this.getControladorLeiturista().inserir(leiturista);
	}

	/**
	 * Método responsável por buscar o leiturista
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do Leiturista.
	 * @return Um Leiturista.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Leiturista buscarLeituristaPorChave(Long chavePrimaria) throws NegocioException {

		return (Leiturista) this.getControladorLeiturista().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o leiturista.
	 *
	 * @param chavePrimaria
	 *            A chave primária do Leiturista.
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy
	 * @return Um Leiturista.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Leiturista buscarLeituristaPorChave(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Leiturista) this.getControladorLeiturista().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * leituristas.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            leituristas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverLeituristas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorLeiturista().validarRemoverLeituristas(chavesPrimarias);
	}

	/**
	 * Método responsável por remover os
	 * leituristas selecionados.
	 *
	 * @param chavesPrimarias As chaves primárias dos
	 *            leituristas selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerLeituristas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorLeiturista().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por consultar instalação
	 * medidor.
	 *
	 * @param chaveMedidor
	 *            A chave Primária do medidor.
	 * @param chavePontoConsumo
	 *            A chave Primária do ponto de
	 *            consumo.
	 * @return Uma coleção de instalação medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<InstalacaoMedidor> consultarInstalacaoMedidor(Long chaveMedidor, Long chavePontoConsumo) throws NegocioException {

		return getControladorMedidor().consultarInstalacaoMedidor(chaveMedidor, chavePontoConsumo);
	}

	/**
	 * Método responsável por consultar o
	 * histórico de operação do medidor.
	 *
	 * @param chaveMedidor
	 *            A chave Primária do ponto de
	 *            consumo.
	 * @param chavePontoConsumo
	 *            A chave Primária do ponto de
	 *            consumo.
	 * @return Uma coleção de histórico de
	 *         operação do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<HistoricoOperacaoMedidor> consultarHistoricoOperacaoMedidor(Long chaveMedidor, Long chavePontoConsumo)
					throws NegocioException {

		return this.getControladorMedidor().consultarHistoricoOperacaoMedidor(chaveMedidor, chavePontoConsumo);
	}

	/**
	 * Método responsável por buscar o histórico
	 * de medição pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do histórico de
	 *            medição.
	 * @return Um histórico de medição.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoMedicao buscarHistoricoMedicaoPorChave(long chavePrimaria) throws NegocioException {

		return (HistoricoMedicao) this.getControladorHistoricoMedicao().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar o histórico
	 * de medição pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do histórico de
	 *            medição.
	 * @param propriedadesLazy
	 *            Propriedades a serem carregadas
	 *            juntamente com o
	 *            objeto.
	 * @return Um histórico de medição.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoMedicao buscarHistoricoMedicaoPorChave(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (HistoricoMedicao) this.getControladorHistoricoMedicao().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Obter historico consumo para simulacao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the historico consumo
	 * @throws GGASException the GGAS exception
	 */
	public HistoricoConsumo obterHistoricoConsumoParaSimulacao(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return this.getControladorControladorHistoricoConsumo().obterHistoricoConsumoParaSimulacao(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por criar um comentário
	 * de ponto de consumo.
	 *
	 * @return Um comentário de ponto de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ComentarioPontoConsumo criarComentarioPontoConsumo() throws NegocioException {

		return (ComentarioPontoConsumo) this.getControladorPontoConsumo().criarComentarioPontoConsumo();
	}

	/**
	 * Método responsável por criar um comentário
	 * para medição histórico.
	 *
	 * @return Um comentário para medição
	 *         histórico.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MedicaoHistoricoComentario criarMedicaoHistoricoComentario() throws NegocioException {

		return (MedicaoHistoricoComentario) this.getControladorHistoricoMedicao().criarMedicaoHistoricoComentario();
	}

	/**
	 * Criar supervisorio medicao comentario.
	 *
	 * @return the entidade negocio
	 * @throws NegocioException the GGAS exception
	 */
	public EntidadeNegocio criarSupervisorioMedicaoComentario() throws NegocioException {

		return this.getControladorSupervisorio().criarSupervisorioMedicaoComentario();
	}

	/**
	 * Método responsável por atualizar os dados
	 * de faturamento de um
	 * histórico de ponto consumo e adicionar um
	 * comentário para o historico de medição.
	 *
	 * @param pontoConsumo
	 *            Ponto de consumo impactado.
	 * @param historicoMedicao
	 *            O histórico de medição a ser
	 *            alterado.
	 * @param medicaoHistoricoComentario
	 *            O comentário de alteração do
	 *            histórico de medição.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarDadosFaturamentoLeitura(PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao,
					MedicaoHistoricoComentario medicaoHistoricoComentario) throws GGASException {

		this.getControladorHistoricoMedicao().atualizarDadosFaturamentoLeitura(pontoConsumo, historicoMedicao, medicaoHistoricoComentario, true);
	}

	/**
	 * Método responsável por buscar a
	 * anormalidade de leitura pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            de leitura.
	 * @return Uma anormalidade de leitura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AnormalidadeLeitura buscarAnormalidadeLeituraPorChave(long chavePrimaria) throws NegocioException {

		return this.getControladorAnormalidade().obterAnormalidadeLeitura(chavePrimaria);
	}

	/**
	 * Método responsável por retornar uma ação
	 * anormalidade consumo pela chavePrimaria
	 * passada por parâmetro.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma ação anormalidade consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AcaoAnormalidadeConsumo obterAcaoAnormalidadeConsumo(long chavePrimaria) throws NegocioException {

		return getControladorAnormalidade().obterAcaoAnormalidadeConsumo(chavePrimaria);
	}

	/**
	 * Método responsável por retornar uma ação
	 * anormalidade leitura pela chavePrimaria
	 * passada por parâmetro.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma ação anormalidade leitura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AcaoAnormalidadeLeitura obterAcaoAnormalidadeLeitura(long chavePrimaria) throws NegocioException {

		return getControladorAnormalidade().obterAcaoAnormalidadeLeitura(chavePrimaria);
	}

	/**
	 * Método responsável por retornar a
	 * anormalidade consumo pela chavePrimaria
	 * assada por
	 * parâmetro.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma anormalidade consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AnormalidadeConsumo obterAnormalidadeConsumo(long chavePrimaria) throws NegocioException {

		return getControladorAnormalidade().obterAnormalidadeConsumo(chavePrimaria);
	}

	/**
	 * Método responsável por retornar uma
	 * devolução pela chavePrimaria passada por
	 * parâmetro.
	 *
	 * @param chavePrimaria
	 *            A chave primária da devolução.
	 * @return Uma devolução.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Devolucao obterDevolucao(long chavePrimaria) throws NegocioException {

		return (Devolucao) this.getControladorDevolucao().obter(chavePrimaria);
	}

	/**
	 * Método responsável por acessar o método que
	 * cria um devolução.
	 *
	 * @return Devolução criada
	 * @throws NegocioException the GGAS exception
	 */
	public Devolucao criarDevolucao() throws NegocioException {

		return (Devolucao) this.getControladorDevolucao().criar();
	}

	/**
	 * Método responsável por retornar a
	 * anormalidade leitura pela chavePrimaria
	 * passada por
	 * parâmetro.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            leitura.
	 * @return Uma anormalidade leitura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AnormalidadeLeitura obterAnormalidadeLeitura(long chavePrimaria) throws NegocioException {

		return getControladorAnormalidade().obterAnormalidadeLeitura(chavePrimaria);
	}

	/**
	 * Remover anormalidades consumo.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the GGAS exception
	 */
	public void removerAnormalidadesConsumo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		getControladorAnormalidade().removerAnormalidadesConsumo(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Remover anormalidades leitura.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the GGAS exception
	 */
	public void removerAnormalidadesLeitura(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		getControladorAnormalidade().removerAnormalidadesLeitura(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por inserir uma
	 * Anormalidade Consumo.
	 *
	 * @param anormalidadeConsumo the anormalidade consumo
	 * @return chavePrimeria da
	 *         anormalidadeConsumo
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) throws NegocioException {

		return this.getControladorAnormalidade().inserir(anormalidadeConsumo);
	}

	/**
	 * Método responsável por inserir uma
	 * Anormalidade Leitura.
	 *
	 * @param anormalidadeLeitura the anormalidade leitura
	 * @return chavePrimeria da
	 *         anormalidadeLeitura
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura) throws NegocioException {

		return this.getControladorAnormalidade().inserir(anormalidadeLeitura);
	}

	/**
	 * Método responsável por buscar a
	 * anormalidade de consumo pela chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            de consumo.
	 * @return Uma anormalidade de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AnormalidadeConsumo buscarAnormalidadeConsumoPorChave(long chavePrimaria) throws NegocioException {

		return this.getControladorAnormalidade().obterAnormalidadeConsumo(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar uma
	 * anormalidade consumo.
	 *
	 * @param anormalidadeConsumo the anormalidade consumo
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) throws GGASException {

		this.getControladorAnormalidade().atualizarAnormalidadeConsumo(anormalidadeConsumo);
	}

	/**
	 * Método responsável por validar a exigência
	 * de comentário para a análise
	 * de exceção.
	 *
	 * @return true caso for exigido.
	 * @throws NegocioException the GGAS exception
	 */
	public boolean exigeComentarioAnaliseExcecao() throws NegocioException {

		return this.getControladorHistoricoMedicao().exigeComentarioAnaliseExcecao();
	}

	/**
	 * Método responsável por alterar um
	 * leiturista no sistema.
	 *
	 * @param leiturista
	 *            Leiturista a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do leiturista
	 *         inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarLeiturista(Leiturista leiturista) throws GGASException {

		this.getControladorLeiturista().atualizar(leiturista);
	}

	/**
	 * Método responsável por atualizar uma
	 * anormalidade leitura.
	 *
	 * @param anormalidadeLeitura the anormalidade leitura
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura) throws GGASException {

		this.getControladorAnormalidade().atualizarAnormalidadeLeitura(anormalidadeLeitura);
	}

	/**
	 * Consultar anormalidades consumo.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<AnormalidadeConsumo> consultarAnormalidadesConsumo(Map<String, Object> filtro) throws NegocioException {

		return getControladorAnormalidade().consultarAnormalidadesConsumo(filtro);
	}

	/**
	 * Consultar anormalidades leitura.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<AnormalidadeLeitura> consultarAnormalidadesLeitura(Map<String, Object> filtro) throws NegocioException {

		return getControladorAnormalidade().consultarAnormalidadesLeitura(filtro);
	}

	/**
	 * Metódo responsável por validar se é
	 * obrigatória a sequência de leitura na
	 * alteração do ponto de consumo.
	 *
	 * @param chavePrimariaImovel
	 *            Chave primária do imovel
	 * @return true caso obrigatória.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean exigeSequenciaLeituraAlteracaoPontoConsumo(Long chavePrimariaImovel) throws NegocioException {

		return getControladorImovel().exigeSequenciaLeituraAlteracaoPontoConsumo(chavePrimariaImovel);
	}

	/**
	 * Método responsável por listas as situações
	 * de imóvel para as
	 * funcionalidades de cadastro de imóvel.
	 *
	 * @param chaveQuadraFace the chave quadra face
	 * @param selectChavePrimaria the select chave primaria
	 * @return coleção de situações de imóvel.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoImovel> verificarSitucaoImovelPorRedeIndicador(String chaveQuadraFace, String selectChavePrimaria)
					throws GGASException {

		return getControladorImovel().verificarSitucaoImovelPorRedeIndicador(chaveQuadraFace, selectChavePrimaria);
	}

	/**
	 * Método responsável por listas as situações
	 * de imóvel para as
	 * funcionalidades de cadastro de imóvel.
	 *
	 * @return coleção de situações de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoImovel> listarSituacaoImovelInterligado() throws NegocioException {

		return getControladorImovel().listarSituacaoImovelInterligado();
	}

	/**
	 * Método responsável por obter o controlador
	 * de unidade.
	 *
	 * @return ControladorUnidade O controlador de
	 *         unidade.
	 */
	private ControladorUnidade getControladorUnidade() {

		return (ControladorUnidade) serviceLocator.getControladorNegocio(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
	}

	/**
	 * Método responsável por obter o controlador
	 * de contrato.
	 *
	 * @return ControladorContrato O controlador
	 *         de contrato.
	 */
	private ControladorContrato getControladorContrato() {

		return (ControladorContrato) serviceLocator.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
	}

	/**
	 * Método responsável por obter o contrato
	 * ativo do ponto de consumo.
	 *
	 * @param pontoConsumo Ponto de Consumo
	 * @return Contrato do ponto de consumo.
	 * @throws NegocioException Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public ContratoPontoConsumo obterContratoAtivoPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException {

		return this.getControladorContrato().obterContratoAtivoPontoConsumo(pontoConsumo);
	}

	/**
	 * Método responsável por criar uma Tronco da
	 * Rede de Distribuiçao.
	 *
	 * @return tronco um tronco da rede de
	 *         distribuição do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public RedeDistribuicaoTronco criarRedeDistribuicaoTronco() throws NegocioException {

		return (RedeDistribuicaoTronco) this.getControladorRede().criarRedeDistribuicaoTronco();
	}

	/**
	 * Método responsável por criar Preco GAs
	 * Item.
	 *
	 * @return the preco gas item
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PrecoGasItem criarPrecoGasItem() throws NegocioException {

		return this.getControladorPrecoGas().criarPrecoGasItem();
	}

	/**
	 * Validar preco gas item.
	 *
	 * @param precoGas the preco gas
	 * @param precosGasItem the precos gas item
	 * @param precoGasItem the preco gas item
	 * @throws NegocioException the GGAS exception
	 */
	public void validarPrecoGasItem(PrecoGas precoGas, Collection<PrecoGasItem> precosGasItem, PrecoGasItem precoGasItem)
					throws NegocioException {

		this.getControladorPrecoGas().validarPrecoGasItem(precoGas, precosGasItem, precoGasItem);
	}

	/**
	 * Criar preco gas.
	 *
	 * @return the preco gas
	 * @throws NegocioException the GGAS exception
	 */
	public PrecoGas criarPrecoGas() throws NegocioException {

		return (PrecoGas) this.getControladorPrecoGas().criar();
	}

	/**
	 * Inserir preco gas.
	 *
	 * @param precoGas the preco gas
	 * @return the long
	 * @throws NegocioException the GGAS exception
	 */
	public Long inserirPrecoGas(PrecoGas precoGas) throws NegocioException {

		return this.getControladorPrecoGas().inserirPrecoGas(precoGas);
	}

	/**
	 * Obter preco gas.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the preco gas
	 * @throws NegocioException the GGAS exception
	 */
	public PrecoGas obterPrecoGas(long chavePrimaria) throws NegocioException {

		return (PrecoGas) this.getControladorPrecoGas().obter(chavePrimaria);
	}

	/**
	 * Obter preco gas.
	 *
	 * @param chave the chave
	 * @param propriedadesLazy the propriedades lazy
	 * @return the preco gas
	 * @throws NegocioException the GGAS exception
	 */
	public PrecoGas obterPrecoGas(Long chave, String... propriedadesLazy) throws NegocioException {

		return (PrecoGas) getControladorPrecoGas().obter(chave, propriedadesLazy);
	}

	/**
	 * Consultar preco gas item.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<PrecoGasItem> consultarPrecoGasItem(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorPrecoGas().consultarPrecoGasItem(filtro);
	}

	/**
	 * Atualizar preco gas.
	 *
	 * @param precoGas the preco gas
	 * @throws NegocioException the GGAS exception
	 */
	public void atualizarPrecoGas(PrecoGas precoGas) throws NegocioException {

		getControladorPrecoGas().atualizarPrecoGas(precoGas);
	}

	/**
	 * Validar remover preco gas item.
	 *
	 * @param precoGasItem the preco gas item
	 * @throws NegocioException the GGAS exception
	 */
	public void validarRemoverPrecoGasItem(PrecoGasItem precoGasItem) throws NegocioException {

		this.getControladorPrecoGas().validarRemoverPrecoGasItem(precoGasItem);
	}

	/**
	 * Método responsável por validar os dados da
	 * entidade RedeDistruibuicaoTronco.
	 *
	 * @param redeTronco
	 *            Uma RedeDistribuicaoTronco do
	 *            sistema
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosRedeDistribuicaoTronco(RedeDistribuicaoTronco redeTronco) throws NegocioException {

		this.getControladorRede().validarDadosRedeDistribuicaoTronco(redeTronco);
	}

	/**
	 * Método responsável por validar um unico
	 * tronco por um unico city gate.
	 *
	 * @param redeTronco Uma RedeDistribuicaoTronco do
	 *            sistema, listaRedeTronco Uma
	 *            colecao de
	 *            RedeDistribuicaoTronco do
	 *            sistema
	 * @param listaRedeTronco the lista rede tronco
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarTroncoPorCityGate(RedeDistribuicaoTronco redeTronco, Collection<RedeDistribuicaoTronco> listaRedeTronco)
					throws NegocioException {

		this.getControladorRede().validarTroncoPorCityGate(redeTronco, listaRedeTronco);
	}

	/**
	 * Método responsável por listar os City Gates
	 * do sistema.
	 *
	 * @return coleção de City Gate.
	 */
	public Collection<CityGate> listarCityGate() {

		return getControladorTronco().listarCityGateExistente();
	}

	/**
	 * Método responsável por listar as situações
	 * dos medidores disponíveis para
	 * cadastro existentes no sistema.
	 *
	 * @return coleção as situações dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoMedidor> listarSituacaoMedidorCadastro() throws NegocioException {

		return this.getControladorMedidor().listarSituacaoMedidorCadastro();
	}

	/**
	 * Método responsável por consultar atividades
	 * econômicas.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de atividades
	 *         econômicas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AtividadeEconomica> consultarAtividadeEconomica(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorCliente().consultarAtividadeEconomica(filtro);
	}

	/**
	 * Método responsável por listar
	 * RedeDistribuicaoTronco por uma rede
	 * informada.
	 *
	 * @param rede
	 *            Uma Rede do sistema
	 * @return Colecao de RedeDistribuicaoTroncos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<RedeDistribuicaoTronco> listarRedeTroncoPorRede(Rede rede) throws NegocioException {

		return this.getControladorRede().listarRedeTroncoPorRede(rede);
	}

	/**
	 * Método responsável por buscar o city gate
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do city gate.
	 * @return Um city gate.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CityGate buscarCityGatePorChave(long chavePrimaria) throws NegocioException {

		return getControladorTronco().obterCityGate(chavePrimaria);
	}

	/**
	 * Método responsável por obter o controlador
	 * de cityGateMedicao.
	 *
	 * @return ControladorCityGateMedicao O
	 *         controlador de contrato.
	 */
	private ControladorCityGateMedicao getControladorCityGateMedicao() {

		return (ControladorCityGateMedicao) serviceLocator
						.getControladorNegocio(ControladorCityGateMedicao.BEAN_ID_CONTROLADOR_CITY_GATE_MEDICAO);
	}

	/**
	 * Lista os anos disponíveis para manutenção
	 * de PCS de um CityGate
	 * informado.
	 *
	 * @param cityGate
	 *            Um cityGate.
	 * @return Coleção de anos;
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Integer> listarAnosDisponiveisCityGateMedicao(CityGate cityGate) throws NegocioException {

		return this.getControladorCityGateMedicao().listarAnosDisponiveisCityGateMedicao(cityGate);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Proposta.
	 *
	 * @return ControladorProposta O controlador
	 *         de Proposta
	 */
	private ControladorProposta getControladorProposta() {

		return (ControladorProposta) serviceLocator.getControladorNegocio(ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);
	}

	/**
	 * Método responsável por listar as situações
	 * de proposta,.
	 *
	 * @return Uma coleção de situações de
	 *         proposta.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoProposta> listarSituacaoProposta() throws NegocioException {

		return this.getControladorProposta().listarSituacaoProposta();
	}

	/**
	 * Listar situacoes iniciais proposta.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<SituacaoProposta> listarSituacoesIniciaisProposta() throws GGASException {

		return this.getControladorProposta().listarSituacoesIniciaisProposta();
	}

	/**
	 * Método responsável por listar as tarifas de
	 * proposta.
	 *
	 * @return Uma coleção de tarifas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Tarifa> listarTarifas() throws NegocioException {

		return this.getControladorTarifa().listarTarifas();
	}

	/**
	 * Método responsável por criar uma proposta.
	 *
	 * @return Uma proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Proposta criarProposta() throws NegocioException {

		return (Proposta) this.getControladorProposta().criar();
	}

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 *
	 * @param chavePrimaria
	 *            Chave primária da situação de
	 *            proposta.
	 * @return Uma situação de proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SituacaoProposta obterSituacaoProposta(long chavePrimaria) throws NegocioException {

		return this.getControladorProposta().obterSituacaoProposta(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma tarifa de
	 * proposta.
	 *
	 * @param chavePrimaria Chave primária da tarifa.
	 * @param propriedadesLazy the propriedades lazy
	 * @return Uma tarifa de proposta.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Tarifa obterTarifa(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Tarifa) this.getControladorTarifa().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter uma .
	 *
	 * @param chavePrimaria
	 *            Chave primária .
	 * @return Uma TarifaVigenciaDesconto.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TarifaVigenciaDesconto obterTarifaVigenciaDesconto(long chavePrimaria) throws NegocioException {

		return this.getControladorTarifa().obterTarifaVigenciaDesconto(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma .
	 *
	 * @param filtro the filtro
	 * @return Uma TarifaVigenciaDesconto.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaFaixaDesconto> consultarTarifasFaixaDesconto(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorTarifa().consultarTarifasFaixaDesconto(filtro);
	}

	/**
	 * Método responsável por obter uma .
	 *
	 * @param chavePrimaria Chave primária .
	 * @param propriedadesLazy the propriedades lazy
	 * @return Uma TarifaVigenciaDesconto.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TarifaVigenciaDesconto obterTarifaVigenciaDesconto(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.getControladorTarifa().obterTarifaVigenciaDesconto(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 *
	 * @param chavePrimaria
	 *            Chave primária .
	 * @return Uma TarifaVigencia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TarifaVigencia obterTarifaVigencia(long chavePrimaria) throws NegocioException {

		return this.getControladorTarifa().obterTarifaVigencia(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 *
	 * @param filtro the filtro
	 * @return Uma TarifaVigencia.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaTributo> consultarTarifasTributo(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorTarifa().consultarTarifasTributo(filtro);
	}

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 *
	 * @return Uma TarifaVigencia.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaTributo> listarTarifasTributo() throws NegocioException {

		return this.getControladorTarifa().listarTarifasTributo();
	}

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 *
	 * @param chavePrimaria Chave primária .
	 * @param propriedadesLazy the propriedades lazy
	 * @return Uma TarifaVigencia.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TarifaVigencia obterTarifaVigencia(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.getControladorTarifa().obterTarifaVigencia(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por inserir uma proposta.
	 *
	 * @param proposta Proposta a ser inserida
	 * @return Uma proposta.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirProposta(Proposta proposta) throws NegocioException {

		return this.getControladorProposta().inserir(proposta);
	}

	/**
	 * Método responsável por consultar uma ou
	 * mais proposta de acordo com o filtro
	 * informado.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de proposta.
	 * @return Uma coleção de proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Proposta> consultarPropostas(Map<String, Object> filtro) throws NegocioException {

		return getControladorProposta().consultarPropostas(filtro);
	}

	/**
	 * Método responsável por obter o controlador
	 * de ImovelPCSZ.
	 *
	 * @return ControladorImovelPCSZ O controlador
	 *         ImovelPCSZ.
	 */
	private ControladorImovelPCSZ getControladorImovelPCSZ() {

		return (ControladorImovelPCSZ) serviceLocator.getControladorNegocio(ControladorImovelPCSZ.BEAN_ID_CONTROLADOR_IMOVEL_PCS_Z);
	}

	/**
	 * Método responsável por listar os registros
	 * de PCS e Fator Z do Imóvel.
	 *
	 * @param imovel Imóvel
	 * @param ano the ano
	 * @param mes the mes
	 * @return Coleção de ImovelPCSZ
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ImovelPCSZ> listarPCSZImovel(Imovel imovel, Integer ano, Integer mes) throws NegocioException {

		return this.getControladorImovelPCSZ().listarPCSZImovel(imovel, ano, mes);
	}

	/**
	 * Método responsável por verificar se a
	 * alteração dos dados de PCS e Fator Z é
	 * permitida.
	 *
	 * @param imovelPCSZ the imovel pcsz
	 * @return true caso permitida a alteração.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permitirAtualizacaoImovelPCSZ(ImovelPCSZ imovelPCSZ) throws NegocioException {

		return this.getControladorImovelPCSZ().permitirAtualizacaoImovelPCSZ(imovelPCSZ);
	}

	/**
	 * Método responsável por verificar se é
	 * permitida a manutenção do fator Z do imóvel
	 * informado.
	 *
	 * @param chavePrimariaImovel
	 *            A chave primária do imóvel.
	 * @return true, caso permitido.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permitirManutencaoFatorZ(long chavePrimariaImovel) throws NegocioException {

		return this.getControladorImovelPCSZ().permitirManutencaoFatorZ(chavePrimariaImovel);
	}

	/**
	 * Método responsável por criar um ImovelPCSZ.
	 *
	 * @return ImovelPCSZ Um ImovelPCSZ.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ImovelPCSZ criarImovelPCSZ() throws NegocioException {

		return (ImovelPCSZ) this.getControladorImovelPCSZ().criar();
	}

	/**
	 * Método responsável por atualizar os dados
	 * de PCS e Fator Z do Imovel.
	 *
	 * @param listaImovelPCSZ
	 *            Coleção de dados de ImovelPCSZ
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean atualizarListaImovelPCSZ(Collection<ImovelPCSZ> listaImovelPCSZ) throws GGASException {

		return this.getControladorImovelPCSZ().atualizarListaImovelPCSZ(listaImovelPCSZ);
	}

	/**
	 * Método responsável por listar os dados de
	 * medição de um city gate para um
	 * ano e mês informados.
	 *
	 * @param cityGate
	 *            Um city gate.
	 * @param ano
	 *            Um inteiro que represente um
	 *            ano.
	 * @param mes
	 *            Um inteiro que represente um
	 *            mês.
	 * @return Coleção de dados de medição de city
	 *         gate.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CityGateMedicao> listarCityGateMedicaoManutencao(CityGate cityGate, Integer ano, Integer mes) throws NegocioException {

		return this.getControladorCityGateMedicao().listarCityGateMedicaoManutencao(cityGate, ano, mes);
	}

	/**
	 * Método responsável por atualizar os dados
	 * de medição de city gate.
	 *
	 * @param listaCityGateMedicao
	 *            Coleção de dados de medição de
	 *            city gate;
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean atualizarListaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicao) throws GGASException {

		return this.getControladorCityGateMedicao().atualizarListaCityGateMedicao(listaCityGateMedicao);
	}

	/**
	 * Método responsável por criar uma
	 * CityGateMedicao.
	 *
	 * @return CityGateMedicao Um CityGateMedicao.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CityGateMedicao criarCityGateMedicao() throws NegocioException {

		return (CityGateMedicao) this.getControladorCityGateMedicao().criar();
	}

	/**
	 * Método responsável por verificar se a
	 * alteração dos dados de medição do
	 * city gate é permitida.
	 *
	 * @param dataUltimaLeitura the data ultima leitura
	 * @param dataMedicao the data medicao
	 * @return true caso permitida a alteração.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permitirAtualizacaoCityGateMedicao(Date dataUltimaLeitura, Date dataMedicao) throws NegocioException {

		return this.getControladorCityGateMedicao().permitirAtualizacaoCityGateMedicao(dataUltimaLeitura, dataMedicao);
	}

	/**
	 * Método responsável por retornar o ultimo
	 * historico de consumo das selecionadas chaves.
	 *
	 * @param cityGate the city gate
	 * @param consumoFaturado the consumo faturado
	 * @return true caso permitida a alteração.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoConsumo obterUltimoConsumoHistorico(CityGate cityGate, boolean consumoFaturado) throws NegocioException {

		return this.getControladorCityGateMedicao().obterUltimoConsumoHistorico(cityGate, consumoFaturado);
	}

	/**
	 * Método responsável por retornar a
	 * lista de chaves de rede do cityGate.
	 *
	 * @param chavePrimariaCityGate the chave primaria city gate
	 * @return true caso permitida a alteração.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<Long> obterListaChaveRedes(long chavePrimariaCityGate) throws NegocioException {

		return this.getControladorCityGateMedicao().obterListaChaveRedes(chavePrimariaCityGate);
	}

	/**
	 * Método responsável por Listar as Rotas que
	 * estão com Choques de leituristas em
	 * Cronogramas.
	 *
	 * @param cronogramaRota the cronograma rota
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<Rota> listarRotasCronogramaEmAlerta(CronogramaRota cronogramaRota) throws NegocioException {

		return this.getControladorRota().listarRotasCronogramaEmAlerta(cronogramaRota);
	}

	/**
	 * Listar situacoes ponto consumo.
	 *
	 * @return Collection de Situações de Ponto de
	 *         Consumo habilitadas
	 */
	public Collection<SituacaoConsumo> listarSituacoesPontoConsumo() {

		return this.getControladorPontoConsumo().listarSituacoesPontoConsumo();
	}

	/**
	 * Método responsável por buscar uma proposta
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da proposta
	 * @return Uma Proposta
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Proposta buscarPropostaPorChave(Long chavePrimaria) throws NegocioException {

		return (Proposta) this.getControladorProposta().obter(chavePrimaria);
	}

	/**
	 * Método responsável por buscar uma proposta.
	 *
	 * @param chavePrimaria
	 *            A chave primaria da proposta
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy
	 * @return Uma proposta
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	public Proposta buscarPropostaPorChave(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Proposta) getControladorProposta().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por alterar uma proposta
	 * no sistema.
	 *
	 * @param proposta
	 *            Proposta a ser inserida no
	 *            sistema.
	 * @return chavePrimeria Chave da proposta
	 *         inserida.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarProposta(Proposta proposta) throws GGASException {

		this.getControladorProposta().atualizar(proposta);
	}

	/**
	 * Método responsável por listar os municípios
	 * cadastrados no sistema.
	 *
	 * @return Uma coleção de municípios.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Municipio> listarMunicipios() throws NegocioException {

		return this.getControladorMunicipio().listarMunicipios();
	}

	/**
	 * Método responsável por listar as unidades
	 * da federação.
	 *
	 * @return lista de unidades federativas
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<UnidadeFederacao> listarUnidadeFederacao() throws NegocioException {

		return this.getControladorMunicipio().listarUnidadeFederacao();
	}

	/**
	 * Método responsável por obter a chave
	 * primaria da unidade federacao pela chave
	 * primaria do
	 * municipio.
	 *
	 * @param idMunicipio chave primaria do municipio
	 * @return Long chave primaria da unidade
	 *         federacao
	 * @throws NegocioException the GGAS exception
	 */
	public Long obterChavePrimariaUnidadeFederacaoPorChaveMunicipio(Long idMunicipio) throws NegocioException {

		return this.getControladorMunicipio().obterChavePrimariaUnidadeFederacaoPorChaveMunicipio(idMunicipio);
	}

	/**
	 * Método responsável por obter o controlador
	 * de entidades geográficas.
	 *
	 * @return ControladorGeografico O controlador
	 *         de entidades geográficas.
	 */
	private ControladorGeografico getControladorGeografico() {

		return (ControladorGeografico) serviceLocator.getControladorNegocio(ControladorGeografico.BEAN_ID_CONTROLADOR_GEOGRAFICO);
	}

	/**
	 * Método responsável por listar as regiões
	 * cadastradas no sistema.
	 *
	 * @return Coleção de regiões.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Regiao> listarRegioes() throws NegocioException {

		return this.getControladorGeografico().listarRegioes();
	}

	/**
	 * Método responsável por listar as
	 * microrregiões cadastradas no sistema.
	 *
	 * @return Coleção de microrregiões.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Microrregiao> listarMicrorregioes() throws NegocioException {

		return this.getControladorGeografico().listarMicrorregioes();
	}

	/**
	 * Método responsável por obter o controlador
	 * de temperatura e pressão de
	 * medição.
	 *
	 * @return
	 *         ControladorTemperaturaPressaoMedicao
	 *         O controlador de temperatura
	 *         e pressão de medição.
	 */
	private ControladorTemperaturaPressaoMedicao getControladorTemperaturaPressaoMedicao() {

		return (ControladorTemperaturaPressaoMedicao) serviceLocator
						.getControladorNegocio(ControladorTemperaturaPressaoMedicao.BEAN_ID_CONTROLADOR_TEMPERATURA_PRESSAO_MEDICAO);
	}

	/**
	 * Método responsável por listar as medições
	 * de temperatura e pressão
	 * disponíveis para manutenção.
	 *
	 * @param chaveLocalidade Uma chave primária de
	 *            localidade.
	 * @param chaveMicrorregiao Uma chave primária de
	 *            microrregiao.
	 * @param chaveMunicipio Uma chave primária de município.
	 * @param chaveRegiao Uma chave primária de região.
	 * @param dataInicial Uma data.
	 * @param chaveUnidadeFederacao Uma chave primária de
	 *            unidadeFederacao.
	 * @return Coleção de medições de temperatura
	 *         e pressão.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<TemperaturaPressaoMedicao> listarTemperaturaPressaoMedicaoManutencao(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataInicial, Long chaveUnidadeFederacao) throws NegocioException {

		return this.getControladorTemperaturaPressaoMedicao().listarTemperaturaPressaoMedicaoManutencao(chaveLocalidade, chaveMicrorregiao,
						chaveMunicipio, chaveRegiao, dataInicial, chaveUnidadeFederacao);
	}

	/**
	 * Método responsável por validar a pesquisa
	 * para manutenção de medições de
	 * temperatura e pressão.
	 *
	 * @param codigoTipoAbrangencia
	 *            O código do tipo de abrangência.
	 * @param chaveAbrangencia
	 *            A chave primária da abrangência.
	 * @param dataInicial
	 *            Uma data inicial.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public void validarPesquisaManutencaoTemperaturaPressaoMedicao(String codigoTipoAbrangencia, Long chaveAbrangencia, Date dataInicial)
					throws NegocioException {

		this.getControladorTemperaturaPressaoMedicao().validarPesquisaManutencaoTemperaturaPressaoMedicao(codigoTipoAbrangencia,
						chaveAbrangencia, dataInicial);
	}

	/**
	 * Método responsável por buscar a medição de
	 * temperatura e pressão pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da medição de
	 *            temperatura e pressão.
	 * @param propriedadesLazy
	 *            Propriedades que devem ser
	 *            carregadas juntamente com o
	 *            objeto.
	 * @return Uma medição de temperatura e
	 *         pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TemperaturaPressaoMedicao buscarTemperaturaPressaoMedicaoChave(Long chavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		return (TemperaturaPressaoMedicao) this.getControladorTemperaturaPressaoMedicao().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por criar uma
	 * TemperaturaPressaoMedicao.
	 *
	 * @return TemperaturaPressaoMedicao Uma
	 *         TemperaturaPressaoMedicao.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TemperaturaPressaoMedicao criarTemperaturaPressaoMedicao() throws NegocioException {

		return (TemperaturaPressaoMedicao) this.getControladorTemperaturaPressaoMedicao().criar();
	}

	/**
	 * Método responsável por buscar uma
	 * microrregião por chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da
	 *            microrregião.
	 * @return Uma microrregião.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Microrregiao buscarMicrorregiaoPorChave(long chavePrimaria) throws NegocioException {

		return getControladorGeografico().obterMicrorregiao(chavePrimaria);
	}

	/**
	 * Método responsável por buscar uma região
	 * por chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da região.
	 * @return Uma região.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Regiao buscarRegiaoPorChave(long chavePrimaria) throws NegocioException {

		return getControladorGeografico().obterRegiao(chavePrimaria);
	}
	
	/**
	 * Obter microrregiões por região.
	 *
	 * @param idRegiao the id regiao
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<Microrregiao> obterMicrorregioesPorRegiao(Long idRegiao) throws NegocioException {

		return getControladorGeografico().obterMicrorregioesPorRegiao(idRegiao);
	}
	
	/**
	 * Obter microrregiões por unidade federação.
	 *
	 * @param idUnidadeFederacao the id unidade federação
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<Microrregiao> obterMicrorregioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException {

		return getControladorGeografico().obterMicrorregioesPorUnidadeFederacao(idUnidadeFederacao);
	}
	
	/**
	 * Obter regiões por unidade federação.
	 *
	 * @param idUnidadeFederacao the id unidade federacao
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<Regiao> obterRegioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException {

		return getControladorGeografico().obterRegioesPorUnidadeFederacao(idUnidadeFederacao);
	}

	/**
	 * Método responsável por atualizar os dados
	 * de medição de temperatura e
	 * pressão.
	 *
	 * @param listaTemperaturaPressaoMedicao Coleção de dados de medição de
	 *            temperatura e pressão;
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean atualizarListaTemperaturaPressaoMedicao(Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao)
					throws GGASException {

		return this.getControladorTemperaturaPressaoMedicao().atualizarListaTemperaturaPressaoMedicao(listaTemperaturaPressaoMedicao);
	}

	/**
	 * Método responsável por remover os dados de
	 * uma medição de temperatura e
	 * pressão.
	 *
	 * @param temperaturaPressaoMedicao
	 *            Uma medição de temperatura e
	 *            pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerTemperaturaPressaoMedicao(TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws NegocioException {

		this.getControladorTemperaturaPressaoMedicao().remover(temperaturaPressaoMedicao);
	}

	/**
	 * Valida uma coleção de cronogramas de rotas
	 * a ser mantida.
	 *
	 * @param listaCronogramaRota the lista cronograma rota
	 * @return true caso permita.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCronogramasRotaManutencao(Collection<CronogramaRota> listaCronogramaRota) throws NegocioException {

		this.getControladorRota().validarCronogramasRotaManutencao(listaCronogramaRota);
	}

	/**
	 * Método responsável por consulta o último
	 * cronograma realizado da rota
	 * informada.
	 *
	 * @param chavePrimariaRota
	 *            Chave primária da rota.
	 * @return Um cronograma de rota.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public CronogramaRota obterUltimoCronogramaRotaRealizado(long chavePrimariaRota) throws NegocioException {

		return this.getControladorRota().obterUltimoCronogramaRotaRealizado(chavePrimariaRota);
	}

	/**
	 * Obter referencia inicial.
	 *
	 * @param anoInicial the ano inicial
	 * @param mesInicial the mes inicial
	 * @return the string
	 * @throws NegocioException the GGAS exception
	 */
	public String obterReferenciaInicial(String anoInicial, String mesInicial) throws NegocioException {

		return this.getControladorHistoricoConsumo().obterReferenciaInicial(anoInicial, mesInicial);
	}

	/**
	 * Obter referencia final.
	 *
	 * @param anoFinal the ano final
	 * @param mesFinal the mes final
	 * @return the string
	 * @throws NegocioException the GGAS exception
	 */
	public String obterReferenciaFinal(String anoFinal, String mesFinal) throws NegocioException {

		return this.getControladorHistoricoConsumo().obterReferenciaFinal(anoFinal, mesFinal);
	}

	/**
	 * Validar intervalo referencia.
	 *
	 * @param referenciaInicial the referencia inicial
	 * @param referenciaFinal the referencia final
	 * @throws NegocioException the GGAS exception
	 */
	public void validarIntervaloReferencia(String referenciaInicial, String referenciaFinal) throws NegocioException {

		this.getControladorHistoricoConsumo().validarIntervaloReferencia(referenciaInicial, referenciaFinal);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * proposta.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias das
	 *            propostas selecionadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverPropostas(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorProposta().validarRemoverPropostas(chavesPrimarias);
	}

	/**
	 * Método responsável por obter uma medição de
	 * temperatura e pressão por uma
	 * data informada.
	 *
	 * @param chaveLocalidade Uma chave primária de
	 *            localidade.
	 * @param chaveMicrorregiao Uma chave primária de
	 *            microrregiao.
	 * @param chaveMunicipio Uma chave primária de município.
	 * @param chaveRegiao Uma chave primária de região.
	 * @param dataReferencia the data referencia
	 * @return A medição de temperatura e pressão.
	 * @throws NegocioException Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public TemperaturaPressaoMedicao obterTemperaturaPressaoMedicaoVigentePorData(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataReferencia) throws NegocioException {

		return this.getControladorTemperaturaPressaoMedicao().obterTemperaturaPressaoMedicaoVigentePorData(chaveLocalidade,
						chaveMicrorregiao, chaveMunicipio, chaveRegiao, dataReferencia, null);
	}

	/**
	 * Método responsável por importar os dados de
	 * um arquivo excel.
	 *
	 * @param bytes O arquivo que será importado
	 * @param tipoArquivo O tipo do arquivo
	 * @param nomeArquivo the nome arquivo
	 * @return Um mapa contendo os valores da
	 *         proposta
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, Object> importarDadosPropostaExcel(byte[] bytes, String tipoArquivo, String nomeArquivo) throws GGASException {

		return this.getControladorProposta().importarDadosPropostaExcel(bytes, tipoArquivo, nomeArquivo);
	}

	/**
	 * Método responsável por remover proposta(s).
	 *
	 * @param chavesPrimarias As chaves primárias das
	 *            propostas selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerPropostas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorProposta().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por obter o controlador
	 * de programação.
	 *
	 * @return O controlador de programação.
	 */
	private ControladorProgramacao getControladorProgramacao() {

		return (ControladorProgramacao) serviceLocator.getControladorNegocio(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de modelo de contrato.
	 *
	 * @return O controlador de modelo de
	 *         contrato.
	 */
	private ControladorModeloContrato getControladorModeloContrato() {

		return (ControladorModeloContrato) serviceLocator
						.getControladorNegocio(ControladorModeloContrato.BEAN_ID_CONTROLADOR_MODELO_CONTRATO);
	}

	/**
	 * Método responsável por criar uma parada
	 * programada de ponto de consumo.
	 *
	 * @return Uma parada programada de ponto de
	 *         consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParadaProgramada criarParadaProgramadaPontoConsumo() throws NegocioException {

		return (ParadaProgramada) this.getControladorProgramacao().criar();
	}

	/**
	 * Método responsável por inserir uma parada
	 * programada de ponto de consumo.
	 *
	 * @param paradaProgramada
	 *            Parada programada a ser
	 *            inserida.
	 * @return Chave primária da parada
	 *         programada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirParadaProgramadaPontoConsumo(ParadaProgramada paradaProgramada) throws NegocioException {

		return this.getControladorProgramacao().inserir(paradaProgramada);
	}

	/**
	 * Método responsável por consultar as paradas
	 * programadas dos pontos de consumo.
	 *
	 * @param idPontosConsumo the id pontos consumo
	 * @param idTipoParada the id tipo parada
	 * @return Uma coleção de paradas programadas
	 *         de ponto de consumo.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ParadaProgramada> consultarParadarProgramadasEmComum(Long[] idPontosConsumo, Long idTipoParada) throws NegocioException {

		return this.getControladorProgramacao().consultarParadasProgramadasEmComum(idPontosConsumo, idTipoParada);
	}

	/**
	 * Método responsável por obter uma quadra a
	 * partir da chave primaria da quadraFace
	 * informada.
	 *
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Uma Quadra
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Quadra obterQuadraPorFaceQuadra(Long idQuadraFace) throws NegocioException {

		return getControladorImovel().obterQuadraPorFaceQuadra(idQuadraFace);
	}

	/**
	 * Obter quadra face por quadra.
	 *
	 * @param idQuadra the id quadra
	 * @param cep the cep
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<QuadraFace> obterQuadraFacePorQuadra(Long idQuadra, String cep) throws NegocioException {

		return getControladorQuadra().obterQuadraFacePorQuadra(idQuadra, cep);
	}

	/**
	 * Método responsável por obter a quadra por
	 * face da quadra.
	 *
	 * @param idQuadraFace Chave da quadra face
	 * @return Uma Quadra
	 * @throws NegocioException the GGAS exception
	 */
	public Quadra obterQuadraPorFaceQuadraPontoConsumo(Long idQuadraFace) throws NegocioException {

		return getControladorImovel().obterQuadraPorFaceQuadraPontoConsumo(idQuadraFace);
	}

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração da situação do
	 * medidor.
	 *
	 * @param chavePrimariaMedidor
	 *            A chave primária da medidor.
	 * @return true caso permita.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permiteAlteracaoSituacaoMedidor(long chavePrimariaMedidor) throws NegocioException {

		return this.getControladorMedidor().permiteAlteracaoSituacaoMedidor(chavePrimariaMedidor);
	}

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração do número de série
	 * do medidor.
	 *
	 * @param chavePrimariaMedidor
	 *            A chave primária do medidor.
	 * @return true caso permita.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permiteAlteracaoNumeroSerieMedidor(long chavePrimariaMedidor) throws NegocioException {

		return this.getControladorMedidor().permiteAlteracaoNumeroSerieMedidor(chavePrimariaMedidor);
	}

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração do número de série
	 * do corretor de vazão.
	 *
	 * @param chavePrimariaVazaoCorretor the chave primaria vazao corretor
	 * @return true caso permita.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permiteAlteracaoNumeroSerieVazaoCorretor(long chavePrimariaVazaoCorretor) throws GGASException {

		return this.getControladorVazaoCorretor().permiteAlteracaoNumeroSerieVazaoCorretor(chavePrimariaVazaoCorretor);
	}

	/**
	 * Método que valida se a data final
	 * selecionada é maior que a data inicial.
	 *
	 * @param mesInicial the mes inicial
	 * @param anoInicial the ano inicial
	 * @param mesFinal the mes final
	 * @param anoFinal the ano final
	 * @throws GGASException the GGAS exception
	 */
	public void validarIntervaloDatas(String mesInicial, String anoInicial, String mesFinal, String anoFinal) throws GGASException {

		this.getControladorHistoricoConsumo().validarIntervaloDatas(mesInicial, anoInicial, mesFinal, anoFinal);
	}

	/**
	 * Método responsável por listar os dados de
	 * solicitação de consumo do ponto de consumo.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param listaContratoPontoConsumoModalidade the lista contrato ponto consumo modalidade
	 * @param ano Ano
	 * @param mes Mes
	 * @return Coleção contendo informações das
	 *         solicitações de consumo.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo,
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Integer ano, Integer mes)
					throws NegocioException {

		return this.getControladorProgramacao().listarSolicitacaoConsumoPontoConsumo(contratoPontoConsumo,
						listaContratoPontoConsumoModalidade, ano, mes);
	}

	/**
	 * Método responsável por obter o saldo a
	 * recuperar de um ponto de consumo OU um
	 * conjunto de
	 * pontos de consumo agrupados por volume.
	 *
	 * @param contrato the contrato
	 * @param arrayIdPontoConsumo lista contendo Id do ponto de
	 *            consumo ou dos pontos de consumo
	 *            agrupados por
	 *            volume
	 * @param idApuracaoPenalidade the id apuracao penalidade
	 * @param anoMesApuracao ano /mês de apuração
	 * @param listaAQPP lista de AQPP a ser preenchida
	 * @return saldo a recuperar
	 * @throws GGASException caso ocorra algum erro
	 */
	public BigDecimal obterSaldoARecuperar(Contrato contrato, Long[] arrayIdPontoConsumo, Long idApuracaoPenalidade,
					Integer anoMesApuracao, Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP) throws GGASException {

		return this.getControladorProgramacao().obterSaldoARecuperar(contrato, arrayIdPontoConsumo, idApuracaoPenalidade, anoMesApuracao,
						listaAQPP);
	}

	/**
	 * Método responsável por criar um
	 * SolicitacaoConsumoPontoConsumo.
	 *
	 * @return SolicitacaoConsumoPontoConsumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SolicitacaoConsumoPontoConsumo criarSolicitacaoConsumoPontoConsumo() throws NegocioException {

		return (SolicitacaoConsumoPontoConsumo) this.getControladorProgramacao().criarSolicitacaoConsumoPontoConsumo();
	}

	/**
	 * Método responsável por atualizar a
	 * solicitação de consumo do ponto de consumo.
	 *
	 * @param listaSolicitacao the lista solicitacao
	 * @param idPontoConsumo the id ponto consumo
	 * @param ano the ano
	 * @param mes the mes
	 * @param chaveContratoPontoConsumoModalidade the chave contrato ponto consumo modalidade
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarListaSolicitacaoConsumoPontoConsumo(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao,
					Long idPontoConsumo, Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade) throws GGASException {

		this.getControladorProgramacao().atualizarListaSolicitacaoConsumoPontoConsumo(listaSolicitacao, idPontoConsumo, ano, mes,
						chaveContratoPontoConsumoModalidade);
	}

	/**
	 * Método responsável por validar o total das
	 * estimativas com o saldo a recuperar.
	 *
	 * @param listaValorQPNR the lista valor qpnr
	 * @param saldoARecuperar the saldo a recuperar
	 * @throws GGASException the GGAS exception
	 */
	public void validarValorTotalQPNR(String[] listaValorQPNR, BigDecimal saldoARecuperar) throws GGASException {

		this.getControladorProgramacao().validarValorTotalQPNR(listaValorQPNR, saldoARecuperar);
	}

	/**
	 * Método responsável por obter a lista de
	 * garantias financeiras.
	 *
	 * @return coleção de garantias financeiras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoGarantiaFinanceira() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoGarantiaFinanceira();
	}

	/**
	 * Obter lista situacao contrato.
	 *
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<SituacaoContrato> obterListaSituacaoContrato() throws NegocioException {

		return this.getControladorModeloContrato().obterListaSituacaoContrato();
	}

	/**
	 * Obter lista situacao contrato alterado.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SituacaoContrato> obterListaSituacaoContratoAlterado() throws NegocioException {

		return this.getControladorModeloContrato().obterListaSituacaoContratoAlterado();
	}

	/**
	 * Obter lista situacao contrato aditado.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SituacaoContrato> obterListaSituacaoContratoAditado() throws NegocioException {

		return this.getControladorModeloContrato().obterListaSituacaoContratoAditado();
	}

	/**
	 * Método responsável por listar as situações
	 * do contrato que podem ser padrão.
	 *
	 * @return Coleção de situação contrato.
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<SituacaoContrato> obterListaSituacaoContratoPadrao() throws NegocioException {

		return this.getControladorModeloContrato().obterListaSituacaoContratoPadrao();
	}

	/**
	 * Método responsável por obter a lista de
	 * regime de consumo.
	 *
	 * @return coleção de regime de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaRegimeConsumo() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaRegimeConsumo();
	}

	/**
	 * Método responsável por obter a lista de
	 * critério de uso.
	 *
	 * @return coleção de critério de uso.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaCriterioUso() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaCriterioUso();
	}

	/**
	 * Método responsável por obter a lista de
	 * forma de pagamento.
	 *
	 * @return coleção de forma de pagamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaFormaPagamento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaFormaPagamento();
	}

	/**
	 * Método responsável por obter a lista de
	 * tipo de faturamento.
	 *
	 * @return coleção de tipo de faturamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoFaturamento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoFaturamento();
	}

	/**
	 * Método responsável por obter a lista de
	 * tipo de leitura faturada.
	 *
	 * @return coleção de tipo de leitura
	 *         faturada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaFaseReferencialVencimento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaFaseReferencialVencimento();
	}

	/**
	 * Método responsável por obter a lista de
	 * dias úteis e corridos.
	 *
	 * @return coleção de tipo de dias úteis e
	 *         corridos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaOpcaoFaseRefVencimento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaOpcaoFaseRefVencimento();
	}

	/**
	 * Método responsável por obter a lista de
	 * dias úteis e corridos para ação de
	 * cobrança.
	 *
	 * @return coleção de tipo de dias úteis e
	 *         corridos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaOpcaoDias() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaOpcaoDias();
	}

	/**
	 * Método responsável por obter a lista de
	 * hora inicial do dia.
	 *
	 * @return coleção de hora inicial do dia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaHoraInicialDia() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaHoraInicialDia();
	}

	/**
	 * Método responsável por obter a lista de
	 * itens da fatura.
	 *
	 * @return coleção de itens da fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaItemFatura() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaItemFatura();
	}

	/**
	 * Método responsável por obter a lista de endereco padrão
	 * 
	 * @return coleção de itens endereco padrao
	 * @throws NegocioException
	 *             Caso ocorra algum erro na invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaEnderecoPadrao() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaEnderecoPadrao();
	}

	/**
	 * Obter lista item fatura preco gas.
	 *
	 * @return the collection
	 * @throws NegocioException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaItemFaturaPrecoGas() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaItemFaturaPrecoGas();
	}

	/**
	 * Método responsável por obter a lista de
	 * contrato de compra.
	 *
	 * @return coleção de contrato de compra
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaContratoCompra() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaContratoCompra();
	}

	/**
	 * Método responsável por obter a lista de
	 * periodicidade TOP.
	 *
	 * @return coleção de periodicidade TOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaPeriodicidadeTOP() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaPeriodicidadeTOP();
	}

	/**
	 * Obter lista periodicidade retirada maior menor.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterListaPeriodicidadeRetiradaMaiorMenor() {

		return this.getControladorEntidadeConteudo().obterListaPeriodicidadeRetiradaMaiorMenor();
	}

	/**
	 * Obter lista referencia qf parada programada.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaReferenciaQFParadaProgramada() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaReferenciaQFParadaProgramada();
	}

	/**
	 * Método responsável por obter a lista de
	 * periodicidade SOP.
	 *
	 * @return coleção de periodicidade SOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaPeriodicidadeSOP() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaPeriodicidadeSOP();
	}

	/**
	 * Método responsável por obter a lista de
	 * consumo referencial.
	 *
	 * @return coleção de consumo referencial..
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaConsumoReferencial() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaConsumoReferencial();
	}

	/**
	 * Método responsável por obter a lista de
	 * penalidade demanda sob.
	 *
	 * @return Coleção de penalidade demanda
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSob() throws NegocioException {

		return this.getControladorContrato().obterListaPenalidadeDemandaSob();
	}

	/**
	 * Método responsável por obter a lista de
	 * penalidade demanda sobre.
	 *
	 * @return Coleção de penalidade demanda
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSobre() throws NegocioException {

		return this.getControladorContrato().obterListaPenalidadeDemandaSobre();
	}

	/**
	 * Método responsável por criar um modelo de
	 * contrato.
	 *
	 * @return Um modelo de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloContrato criarModeloContrato() throws NegocioException {

		return (ModeloContrato) this.getControladorModeloContrato().criar();
	}

	/**
	 * Método responsável por criar um modelo
	 * atributo.
	 *
	 * @return Um modelo atributo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloAtributo criarModeloAtributo() throws NegocioException {

		return (ModeloAtributo) this.getControladorModeloContrato().criarModeloAtributo();
	}

	/**
	 * Método responsável por obter a entidade
	 * abaAtributo pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da entidade.
	 * @return AbaAtributo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AbaAtributo obterAbaAtributo(long chavePrimaria) throws NegocioException {

		return this.getControladorModeloContrato().obterAbaAtributo(chavePrimaria);
	}

	/**
	 * Método responsável por inserir um modelo de
	 * contrato no sistema.
	 *
	 * @param modeloContrato
	 *            O modelo de contrato a ser
	 *            inserido.
	 * @return chavePrimeria do modelo de contrato
	 *         inserido
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirModeloContrato(ModeloContrato modeloContrato) throws NegocioException {

		return this.getControladorModeloContrato().inserir(modeloContrato);
	}

	/**
	 * Método responsável por alterar um modelo de
	 * contrato no sistema.
	 *
	 * @param modeloContrato
	 *            O modelo de contrato a ser
	 *            inserido.
	 * @return chavePrimeria do modelo de contrato
	 *         inserido
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarModeloContrato(ModeloContrato modeloContrato) throws GGASException {

		this.getControladorModeloContrato().atualizar(modeloContrato);
	}

	/**
	 * Método reponsável por obter o modelo de
	 * contrato.
	 *
	 * @param chavePrimaria
	 *            A chave Primaria
	 * @return Um modelo do contrato
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloContrato obterModeloContrato(Long chavePrimaria) throws NegocioException {

		return (ModeloContrato) this.getControladorModeloContrato().obter(chavePrimaria);
	}

	/**
	 * Método reponsável por obter o modelo de
	 * contrato.
	 *
	 * @param chavePrimaria
	 *            A chave Primaria
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            carregados
	 * @return Um modelo do contrato
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ModeloContrato obterModeloContrato(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (ModeloContrato) this.getControladorModeloContrato().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por consultar Contrato
	 * para o requisito de manter parada
	 * programada.
	 *
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de Contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Contrato> consultarContratoParadaProgramada(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorContrato().consultarContratoParadaProgramada(filtro);
	}

	/**
	 * Método responsável por consultar
	 * ContratoPontoConsumo para o requisito de
	 * manter programação
	 * de consumo.
	 *
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoProgramacaoConsumo(Map<String, Object> filtro)
					throws NegocioException {

		return this.getControladorContrato().consultarContratoPontoConsumoProgramacaoConsumo(filtro);
	}

	/**
	 * Consultar contrato ponto consumo por ponto consumo recente.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the contrato ponto consumo
	 * @throws NegocioException the GGAS exception
	 */
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumoRecente(Long idPontoConsumo) throws NegocioException {

		return this.getControladorContrato().consultarContratoPontoConsumoPorPontoConsumoRecente(idPontoConsumo);
	}

	/**
	 * Método responsável por validar a quantidade
	 * de cronogramas em aberto de
	 * uma rota.
	 *
	 * @param quantidadeExistente
	 *            Quantidade de cronogramas em
	 *            aberto já existentes.
	 * @param quantidadeAdicional
	 *            Quantidade de cronogramas a
	 *            serem adicionados
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	public void validarQuantidadeCronogramasEmAberto(Integer quantidadeExistente, Integer quantidadeAdicional) throws NegocioException {

		this.getControladorRota().validarQuantidadeCronogramasEmAberto(quantidadeExistente, quantidadeAdicional);
	}

	/**
	 * Método responsável por pesquisar modelos de
	 * contrato cadastrados no sistema.
	 *
	 * @param filtro
	 *            Filtro com parâmetros a serem
	 *            considerados na pesquisa.
	 * @return Coleção de modelos de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ModeloContrato> consultarModelosContrato(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorModeloContrato().consultarModelosContrato(filtro);
	}

	/**
	 * Método responsável por listar as abas
	 * atributos do sistema.
	 *
	 * @return Coleção de abas atributos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AbaAtributo> listarAbasAtributos() throws NegocioException {

		return this.getControladorModeloContrato().listarAbasAtributos();
	}

	/**
	 * Método responsável por remover modelos de
	 * contrato.
	 *
	 * @param chavesPrimarias As chaves primárias dos modelos
	 *            de contrato selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerModelosContrato(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorModeloContrato().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se a
	 * chave selecionada para a atualização de
	 * modelos de contrato pode ser alterada.
	 *
	 * @param chavePrimaria A chave primária do modelo
	 *            de contrato.
	 * @throws GGASException the GGAS exception
	 */
	public void validarAtualizarModeloContrato(Long chavePrimaria) throws GGASException {

		this.getControladorModeloContrato().validarAtualizarModeloContrato(chavePrimaria);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * modelos de
	 * contrato.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos modelos
	 *            de contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverModelosContrato(Long[] chavesPrimarias) throws GGASException {

		this.getControladorModeloContrato().validarRemoverModelosContrato(chavesPrimarias);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo a partir da chave primária do
	 * contrato.
	 *
	 * @param chavePrimariaContrato
	 *            Chave primária do contrato.
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @return Uma coleção de pontos de consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorChaveContrato(Long chavePrimariaContrato, Long idCliente) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorChaveContrato(chavePrimariaContrato, idCliente);
	}

	/**
	 * Método responsável por consultar o
	 * ContratoPontoConsumo ativo pelo ponto de
	 * consumo.
	 *
	 * @param chavePontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return ContratoPontoConsumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long chavePontoConsumo) throws GGASException {

		return this.getControladorContrato().consultarContratoPontoConsumoPorPontoConsumo(chavePontoConsumo);
	}

	/**
	 * Método responsável por criar uma aba de
	 * modelo.
	 *
	 * @return Uma aba de modelo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AbaModelo criarAbaModelo() throws GGASException {

		return (AbaModelo) this.getControladorModeloContrato().criarAbaModelo();
	}

	/**
	 * Método responsável por listar somente uma
	 * parada para cada data de parada existente
	 * em uma
	 * lista de paradas com datas repetidas.
	 *
	 * @param listaParadasRepetidas
	 *            Lista de paradas programadas com
	 *            datas de paradas repetidas.
	 * @return Uma coleção de paradas programadas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<ParadaProgramada> listarParadasNaoRepetidas(Collection<ParadaProgramada> listaParadasRepetidas) throws GGASException {

		return this.getControladorProgramacao().listarParadasNaoRepetidas(listaParadasRepetidas);
	}

	/**
	 * Método responsável por consultar
	 * modalidades de contrato cadastradas no
	 * sistema.
	 *
	 * @return Uma coleção de modalidades de
	 *         contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoModalidade> listarContratoModalidade() throws GGASException {

		return this.getControladorContrato().listarContratoModalidade();
	}

	/**
	 * Método responsável por obter uma modalidade.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Uma modalidade
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoModalidade obterContratoModalidade(long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterContratoModalidade(chavePrimaria);
	}

	/**
	 * Método responsável por listar locais de
	 * amostragem de PCS.
	 *
	 * @return coleção de locais de amostragem de
	 *         PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarLocalAmostragemPCS() throws GGASException {

		return this.getControladorEntidadeConteudo().listarLocalAmostragemPCS();
	}

	/**
	 * Método responsável por listar intervalos de
	 * PCS.
	 *
	 * @return coleção de intervalos de PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<IntervaloPCS> listarIntervaloPCS() throws GGASException {

		return this.getControladorHistoricoConsumo().listarIntervaloPCS();
	}

	/**
	 * Método responsável por obter o intervalo do
	 * PCS.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return intervalo de PCS.
	 * @throws GGASException the GGAS exception
	 */
	public IntervaloPCS obterIntervaloPCS(Long chavePrimaria) throws GGASException {

		return this.getControladorHistoricoConsumo().obterIntervaloPCS(chavePrimaria);
	}

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo pelos pontos de
	 * consumo.
	 *
	 * @param chavesPontoConsumo
	 *            Chaves primárias dos pontos de
	 *            consumo.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumo(Long[] chavesPontoConsumo) throws GGASException {

		return this.getControladorContrato().consultarContratoPontoConsumoPorPontoConsumo(chavesPontoConsumo);
	}

	/**
	 * Método responsável por listar as modalidade
	 * do contrato de acordo com o
	 * ContratoPontoConsumo.
	 *
	 * @param chaveContratoPonto
	 *            Chave primária do
	 *            ContratoPontoConsumo.
	 * @return Uma coleção de modalidade de
	 *         contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	public Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorContratoPontoConsumo(Long chaveContratoPonto)
					throws GGASException {

		return this.getControladorContrato().listarContratoPontoConsumoModalidadePorContratoPontoConsumo(chaveContratoPonto);
	}

	/**
	 * Método responsável por validar se houve
	 * seleção do modelo de contrato.
	 *
	 * @param chaveModeloContrato
	 *            Chave primária do modelo de
	 *            contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarModeloContratoSelecionado(Long chaveModeloContrato) throws GGASException {

		this.getControladorContrato().validarModeloContratoSelecionado(chaveModeloContrato);
	}

	/**
	 * Método responsável por consultar contratos
	 * cadastrados no sistema.
	 *
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de contratos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Contrato> consultarContratos(Map<String, Object> filtro) throws GGASException {

		return this.getControladorContrato().consultarContratos(filtro);
	}


	/**
	 * Verifica se o contrato associada a chave primaria passada como parametro é faturável
	 * 
	 * @param idContrato - {@link Long}
	 * @return Se contrato é Faturavel 
	 * @throws GGASException 
	 * 			   Caso ocorra algum erro na 
	 * 			   invocação do método
	 */
	public Boolean verificarContratoFaturavel(long idContrato) throws GGASException {

		return this.getControladorContrato().verificarContratoFaturavel(idContrato);
	}


	/**
	 * Método responsável por consultar contratos adequeção parceria
	 * cadastrados no sistema.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de contratos adequação parceria.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoAdequacaoParceria> consultarContratosAdequacaoParceria(Map<String, Object> filtro) throws GGASException {

		return this.getControladorContratoAdequacaoParceria().consultarContratosAdequacaoParceria(filtro);
	}

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 *
	 * @param dados Os dados informados pelo usuário
	 * @param modeloContrato O modelo do contrato
	 * @param aba A aba que será validada
	 * @param validarDadosAditar the validar dados aditar
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato, Aba aba,
					Boolean validarDadosAditar) throws GGASException {

		this.getControladorContrato().validarCamposObrigatoriosContrato(dados, modeloContrato, aba, validarDadosAditar);
	}

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 *
	 * @param idsAssociados the ids associados
	 * @param pcs the pcs
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCampoPCS(Long[] idsAssociados, String pcs) throws GGASException {

		this.getControladorContrato().validarCampoPCS(idsAssociados, pcs);
	}

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 *
	 * @param renovacaoAutomatica the renovacao automatica
	 * @param diasRenovacao the dias renovacao
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCampoRenovacaoAutomatica(String renovacaoAutomatica, String diasRenovacao) throws GGASException {

		this.getControladorContrato().validarCampoRenovacaoAutomatica(renovacaoAutomatica, diasRenovacao);
	}

	/**
	 * Método responsável por validar os dados
	 * para os campos que possue um valor mínimo e
	 * um valor
	 * máximo.
	 *
	 * @param dados Os dados informados pelo usuário
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarCamposMinimoMaximo(Map<String, Object> dados) throws NegocioException, FormatoInvalidoException {

		this.getControladorContrato().validarCamposMinimoMaximo(dados);
	}

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 *
	 * @param dados
	 *            Os dados informados pelo usuário
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato) throws GGASException {

		this.getControladorContrato().validarCamposObrigatoriosContrato(dados, modeloContrato);
	}

	/**
	 * Método responsável por validar somente os
	 * dados dos campos informado pelo usuário.
	 *
	 * @param dados
	 *            Os dados informados pelo usuário
	 * @param campos
	 *            Os campos informados pelo
	 *            usuário
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, List<String> campos, ModeloContrato modeloContrato)
					throws GGASException {

		this.getControladorContrato().validarCamposObrigatoriosContrato(dados, campos, modeloContrato);
	}

	/**
	 * Método reponsável se houve uma seleção de
	 * pontos de consumo.
	 *
	 * @param chavesPrimarias
	 *            As chaves primarias selecionadas
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarSelecaoImovelPontoConsumo(Long[] chavesPrimarias) throws GGASException {

		this.getControladorContrato().validarSelecaoImovelPontoConsumo(chavesPrimarias);
	}

	/**
	 * Método responsável por obter a entidade aba
	 * do contrato pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da entidade.
	 * @return Aba
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Aba obterAbaContrato(long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterAbaContrato(chavePrimaria);
	}

	/**
	 * Método responsável por obter a entidade
	 * Penalidade pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da entidade.
	 * @return Penalidade
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Penalidade obterPenalidade(long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterPenalidade(chavePrimaria);
	}

	/**
	 * Método responsável por remover contratos.
	 *
	 * @param chavesPrimarias As chaves primárias dos contrato
	 *            selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerContratos(Collection<Long> chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorContrato().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * contrato.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias dos modelos
	 *            de contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverContratos(Collection<Long> chavesPrimarias) throws GGASException {

		this.getControladorContrato().validarRemoverContratos(chavesPrimarias);
	}

	/**
	 * Método responsável por criar um contrato.
	 *
	 * @return Um contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Contrato criarContrato() throws GGASException {

		return (Contrato) this.getControladorContrato().criar();
	}

	/**
	 * Método responsável por criar um Contrato
	 * QDC.
	 *
	 * @return ContratoQDC
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoQDC criarContratoQDC() throws GGASException {

		return (ContratoQDC) this.getControladorContrato().criarContratoQDC();
	}

	/**
	 * Método responsável por criar um Contrato
	 * Ponto Consumo Modalidade QDC.
	 *
	 * @return ContratoPontoConsumoModalidadeQDC
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoModalidadeQDC criarContratoPontoConsumoModalidadeQDC() throws GGASException {

		return (ContratoPontoConsumoModalidadeQDC) this.getControladorContrato().criarContratoPontoConsumoModalidadeQDC();
	}

	/**
	 * Método responsável por criar um criar
	 * Contrato Ponto Consumo Penalidade.
	 *
	 * @return ContratoPontoConsumoPenalidade
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoPenalidade criarContratoPontoConsumoPenalidade() throws GGASException {

		return (ContratoPontoConsumoPenalidade) this.getControladorContrato().criarContratoPontoConsumoPenalidade();
	}

	/**
	 * Método responsável por obter a situação do
	 * contrato.
	 *
	 * @param chavePrimaria
	 *            Chave da situação do contrato.
	 * @return Situação do contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SituacaoContrato obterSituacaoContrato(long chavePrimaria) throws GGASException {

		return getControladorContrato().obterSituacaoContrato(chavePrimaria);
	}

	/**
	 * Método responsável por obter a entidade
	 * conteúdo.
	 *
	 * @param chavePrimaria
	 *            A chave primária da entidade.
	 * @return A entidade conteúdo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterEntidadeConteudo(Long chavePrimaria) throws GGASException {

		return getControladorEntidadeConteudo().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter a entidade
	 * conteúdo por codigo.
	 *
	 * @param codigo the codigo
	 * @param classe the classe
	 * @return A entidade conteúdo.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterEntidadeConteudoPorCodigo(String codigo, String classe) throws GGASException {

		return getControladorEntidadeConteudo().obterPorCodigo(codigo, classe);
	}

	/**
	 * Método responsável por listar as diferentes
	 * versões aprovadas de proposta por um número
	 * informado.
	 *
	 * @param numeroProposta
	 *            Um número de proposta.
	 * @return Uma coleção de propostas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Proposta> listarPropostasAprovadasPorNumero(String numeroProposta) throws GGASException {

		return this.getControladorProposta().listarPropostasAprovadasPorNumero(numeroProposta);
	}

	/**
	 * Método responsável por obter uma aba de
	 * contrato pelo código.
	 *
	 * @param codigo
	 *            Código equivalente a descrição
	 *            abreviada.
	 * @return Uma aba de contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Aba obterAbaContratoPorCodigo(String codigo) throws GGASException {

		return this.getControladorContrato().obterAbaContratoPorCodigo(codigo);
	}

	/**
	 * Método responsável por obter o controlador
	 * de entidade conteúdo.
	 *
	 * @return O controlador de entidade conteúdo.
	 */
	private ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return (ControladorEntidadeConteudo) serviceLocator
						.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
	}

	/**
	 * Método responsável por criar um
	 * ContratoCliente.
	 *
	 * @return ContratoCliente
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoCliente criarContratoCliente() throws GGASException {

		return (ContratoCliente) this.getControladorContrato().criarContratoCliente();
	}

	/**
	 * Método responsável por obter a lista de
	 * responsabilidades de ContratoCliente.
	 *
	 * @return Uma coleção de responsabilidades.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaResponsabilidades() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaResponsabilidades();
	}

	/**
	 * Método responsável por validar se é
	 * permitido a seleção de uma fase de acordo
	 * com uma opção
	 * de vencimento.
	 *
	 * @param chaveOpcaoFase the chave opcao fase
	 * @return true caso permita.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean permiteFaseReferencialVencimentoPorOpcaoVencimento(long chaveOpcaoFase) throws GGASException {

		return this.getControladorEntidadeConteudo().permiteFaseReferencialVencimentoPorOpcaoVencimento(chaveOpcaoFase);
	}

	/**
	 * Método responsável por criar uma entidade
	 * contratoPontoConsumoItemVencimento.
	 *
	 * @return Um objeto
	 *         contratoPontoConsumoItemVencimento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoItemFaturamento criarContratoPontoConsumoItemVencimento() throws GGASException {

		return (ContratoPontoConsumoItemFaturamento) this.getControladorContrato().criarContratoPontoConsumoItemVencimento();
	}

	/**
	 * Método responsável por criar um Contrato
	 * Ponto Consumo Modalidade.
	 *
	 * @return ContratoPontoConsumoModalidade
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoModalidade criarContratoPontoConsumoModalidade() throws GGASException {

		return (ContratoPontoConsumoModalidade) this.getControladorContrato().criarContratoPontoConsumoModalidade();
	}

	/**
	 * Método reponsável por validar a adição de
	 * um item de faturamento.
	 *
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @param dadosItemFaturamento
	 *            Os dados do item de faturamento
	 * @param faseReferencia
	 *            A fase referência
	 * @param opcaoFaseReferencia
	 *            A opção da fase referência
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAdicaoContratoPontoConsumoItemFaturamento(ModeloContrato modeloContrato, Map<String, Object> dadosItemFaturamento,
					EntidadeConteudo faseReferencia, EntidadeConteudo opcaoFaseReferencia) throws GGASException {

		this.getControladorContrato().validarAdicaoContratoPontoConsumoItemFaturamento(modeloContrato, dadosItemFaturamento,
						faseReferencia, opcaoFaseReferencia);
	}

	/**
	 * Método responsável por obter penalidade
	 * demanda.
	 *
	 * @param chavePrimaria
	 *            A chave primária da penalidade.
	 * @return Uma penalidade
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PenalidadeDemanda obterPenalidadeDemanda(long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterPenalidadeDemanda(chavePrimaria);
	}

	/**
	 * Método responsável por validar se já existe
	 * um cliente adicionado com a mesma
	 * responsabilidade.
	 *
	 * @param listaContratoCliente
	 *            Lista de ContratoCliente já
	 *            adicionados.
	 * @param contratoCliente
	 *            ContratoCliente a ser comparado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRelacaoResponsabilidade(Collection<ContratoCliente> listaContratoCliente, ContratoCliente contratoCliente)
					throws GGASException {

		this.getControladorContrato().validarRelacaoResponsabilidade(listaContratoCliente, contratoCliente);
	}

	/**
	 * Método responsável por validar o dados de
	 * ContratoCliente na inclusão de contrato.
	 *
	 * @param contratoCliente
	 *            ContratoCliente a ser validado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosResponsabilidade(ContratoCliente contratoCliente) throws GGASException {

		this.getControladorContrato().validarDadosResponsabilidade(contratoCliente);
	}

	/**
	 * Método responsável por criar uma entidade
	 * ContratoPontoConsumo.
	 *
	 * @return Um objeto contratoPontoConsumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumo criarContratoPontoConsumo() throws GGASException {

		return (ContratoPontoConsumo) this.getControladorContrato().criarContratoPontoConsumo();
	}

	/**
	 * Método responsavel por obter uma quadraface
	 * do sistema com o citygate carregado.
	 *
	 * @param idQuadraFace the id quadra face
	 * @return uma entidade de negocio do sistema
	 *         QuadraFace
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CityGate> listarCityGatePorQuadraFace(long idQuadraFace) throws GGASException {

		return this.getControladorQuadra().listarCityGatePorQuadraFace(idQuadraFace);
	}

	/**
	 * Método responsável por criar um
	 * PontoConsumoCityGate.
	 *
	 * @return PontoConsumoCityGate
	 * @throws GGASException the GGAS exception
	 */
	public PontoConsumoCityGate criarPontoConsumoCityGate() throws GGASException {

		return (PontoConsumoCityGate) this.getControladorPontoConsumo().criarPontoConsumoCityGate();
	}

	/**
	 * Método responsável por inserir um contrato
	 * no sistema.
	 *
	 * @param contrato the contrato
	 * @return chavePrimeria do contrato inserido
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirContrato(Contrato contrato) throws GGASException {

		return this.getControladorContrato().inserirContrato(contrato);
	}

	/**
	 * Método responsável por inserir um contrato
	 * no sistema.
	 *
	 * @param contratoAdequacao the contrato adequacao
	 * @return chavePrimeria do contrato inserido
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirContratoAdequacaoParceria(ContratoAdequacaoParceria contratoAdequacao) throws GGASException {

		return this.getControladorContratoAdequacaoParceria().inserir(contratoAdequacao);
	}

	/**
	 * Método responsável por salvar parcialmente
	 * o contrato.
	 *
	 * @param contrato
	 *            Contrato a ser salvo
	 *            parcialmente.
	 * @return chave primária do contrato que foi
	 *         salvo parcialmente.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long salvarContratoParcial(Contrato contrato) throws GGASException {

		return this.getControladorContrato().salvarContratoParcial(contrato);
	}

	/**
	 * Método responsável por obter uma(s)
	 * entidade PontoConsumoCityGate a partir da
	 * chave primaria
	 * de um ponto de consumo .
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return Uma coleção de
	 *         PontoConsumoCityGate.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumoCityGate> obterPontoConsumoCityGate(Long idPontoConsumo) throws GGASException {

		return this.getControladorPontoConsumo().obterPontoConsumoCityGate(idPontoConsumo);
	}

	/**
	 * Método responsável por obter uma(s)
	 * entidade PontoConsumoCityGate a partir da
	 * chave primaria de um ponto de consumo
	 * ordenado pelo city gate.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param idCityGate the id city gate
	 * @return Uma coleção de
	 *         PontoConsumoCityGate.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumoCityGate> obterPontoConsumoCityGateOrdenadoPorCityGate(Long idPontoConsumo, Long idCityGate)
					throws GGASException {

		return this.getControladorPontoConsumo().obterPontoConsumoCityGateOrdenadoPorCityGate(idPontoConsumo, idCityGate);
	}

	/**
	 * Método reponsável por obter o Ponto de
	 * Consumo.
	 *
	 * @param chavePrimaria A chave Primaria
	 * @param propriedadesEager the propriedades eager
	 * @return Um Ponto de Consumo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PontoConsumo obterPontoConsumo(Long chavePrimaria, String... propriedadesEager) throws GGASException {

		return (PontoConsumo) this.getControladorPontoConsumo().obter(chavePrimaria, propriedadesEager);
	}

	/**
	 * Método reponsável por obter o Contrato.
	 *
	 * @param chavePrimaria
	 *            A chave Primaria
	 * @return Um Contrato
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Contrato obterContrato(Long chavePrimaria) throws GGASException {

		return (Contrato) this.getControladorContrato().obter(chavePrimaria);
	}

	/**
	 * Método reponsável por obter o Contrato.
	 *
	 * @param chavePrimaria
	 *            A chave Primaria
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            carregados
	 * @return Um Contrato
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Contrato obterContrato(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (Contrato) this.getControladorContrato().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por criar um
	 * ContratoPontoConsumoPCSAmostragem.
	 *
	 * @return the contrato ponto consumo pcs amostragem
	 * @throws GGASException Caso a quantidade de propostas
	 *             aprovadas seja nula ou igual a
	 *             zero.
	 */
	public ContratoPontoConsumoPCSAmostragem criarContratoPontoConsumoPCSAmostragem() throws GGASException {

		return (ContratoPontoConsumoPCSAmostragem) this.getControladorContrato().criarContratoPontoConsumoPCSAmostragem();
	}

	/**
	 * Método responsável por controlar a
	 * inclusão, alteração e exclusão das paradas
	 * programadas.
	 *
	 * @param chavesPrimarias Chaves primárias dos pontos de
	 *            consumo.
	 * @param listaDatasParadas Datas das paradas programadas a
	 *            serem incluídas.
	 * @param usuario the usuario
	 * @param dadosAuditoria the dados auditoria
	 * @param indicadorSolicitante the indicador solicitante
	 * @param comentario the comentario
	 * @param idTipoParada the id tipo parada
	 * @param pressoes the pressoes
	 * @param datasPressao the datas pressao
	 * @param unidadesPressao the unidades pressao
	 * @param chavesPontoConsumoParada the chaves ponto consumo parada
	 * @param dataAviso the data aviso
	 * @param volumeReferencia the volume referencia
	 * @return O status da inclusão das paradas
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ParseException the parse exception
	 */
	public int[] controlarParadasProgramadas(Long[] chavesPrimarias, String[] listaDatasParadas, Usuario usuario,
					DadosAuditoria dadosAuditoria, String indicadorSolicitante, String comentario, Long idTipoParada, String[] pressoes,
					String[] datasPressao, Long[] unidadesPressao, Long[] chavesPontoConsumoParada, String dataAviso,
					String volumeReferencia) throws GGASException, ParseException {

		return this.getControladorProgramacao().inserirParadasProgramadas(chavesPrimarias, listaDatasParadas, usuario, dadosAuditoria,
						indicadorSolicitante, comentario, idTipoParada, pressoes, datasPressao, unidadesPressao, chavesPontoConsumoParada,
						dataAviso, volumeReferencia);
	}

	/**
	 * Método responsável por atualizar a lista de
	 * paradas programadas com as datas
	 * selecionadas.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param listaDatasParadas the lista datas paradas
	 * @param usuario the usuario
	 * @param dadosAuditoria the dados auditoria
	 * @param indicadorSolicitante the indicador solicitante
	 * @param comentario the comentario
	 * @param idTipoParada the id tipo parada
	 * @param dataAviso the data aviso
	 * @param volumeFornecimento the volume fornecimento
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ParadaProgramadaVO> manterParadasProgramadas(Long[] chavesPrimarias, String[] listaDatasParadas, Usuario usuario,
					DadosAuditoria dadosAuditoria, String indicadorSolicitante, String comentario, Long idTipoParada, String dataAviso,
					String volumeFornecimento) throws GGASException {

		return this.getControladorProgramacao().manterParadasProgramadas(chavesPrimarias, listaDatasParadas, usuario, dadosAuditoria,
						indicadorSolicitante, comentario, idTipoParada, dataAviso, volumeFornecimento);
	}

	/**
	 * Método responsável por listar os contratos
	 * de ponto de consumo do contrato informado.
	 *
	 * @param chavePrimariaContrato
	 *            A chave primária do contrato.
	 * @return Lista de contrato de ponto de
	 *         consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumo(Long chavePrimariaContrato) throws GGASException {

		return this.getControladorContrato().listarContratoPontoConsumo(chavePrimariaContrato);
	}

	/**
	 * Método responsável por validar a quantidade
	 * de propostas aprovadas.
	 *
	 * @param numeroProposta
	 *            Número da proposta
	 * @param propostaAprovada
	 * 			Indicador de proposta aprovada
	 * @throws GGASException
	 *             Caso a quantidade de propostas
	 *             aprovadas seja nula ou igual a
	 *             zero.
	 */
	public void validarPropostasContrato(String numeroProposta, Boolean propostaAprovada) throws GGASException {

		this.getControladorProposta().validarPropostasContrato(numeroProposta, propostaAprovada);
	}

	/**
	 * Método responsável por aditar um contrato
	 * no sistema.
	 *
	 * @param contratoAditado
	 *            O contrato aditado a ser
	 *            inserido.
	 * @param contratoAlterado
	 *            O contrato que foi alterado.
	 * @return chavePrimeria do contrato inserido
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long aditarContrato(Contrato contratoAditado, Contrato contratoAlterado) throws GGASException {

		return this.getControladorContrato().aditarContrato(contratoAditado, contratoAlterado);
	}

	/**
	 * Método responsável por alterar um contrato
	 * no sistema.
	 *
	 * @param contrato
	 *            O contrato alterado a ser
	 *            inserido.
	 * @param contratoAlterado
	 *            O contrato que foi alterado.
	 * @return chavePrimeria do contrato inserido
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long alterarContrato(Contrato contrato, Contrato contratoAlterado) throws GGASException {

		return this.getControladorContrato().atualizarContrato(contrato, contratoAlterado);
	}

	/**
	 * Método responsável por criar um
	 * ContratoPontoConsumoPCSIntervalo.
	 *
	 * @return ContratoPontoConsumoPCSIntervalo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoPCSIntervalo criarContratoPontoConsumoPCSIntervalo() throws GGASException {

		return (ContratoPontoConsumoPCSIntervalo) this.getControladorContrato().criarContratoPontoConsumoPCSIntervalo();
	}

	/**
	 * Método responsável por listar os locais
	 * amostragem pela chave primaria do contrato
	 * ponto
	 * consumo.
	 *
	 * @param contratoPontoConsumo O contrato Ponto consumo.
	 * @return Lista de locais amostragem.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> consultarLocalAmostragemPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws GGASException {

		return this.getControladorContrato().consultarLocalAmostragemPCSPorCPC(contratoPontoConsumo);
	}

	/**
	 * Método responsável por listar os intervalos
	 * pcs pela chave primaria do contrato ponto
	 * consumo.
	 *
	 * @param contratoPontoConsumo O contrato Ponto consumo.
	 * @return Lista de intervalo pcs.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<IntervaloPCS> consultarIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws GGASException {

		return this.getControladorContrato().consultarIntervaloPCSPorCPC(contratoPontoConsumo);
	}

	/**
	 * Método responsável por obter o tamanho da
	 * Redução para Recuperação do PCS.
	 *
	 * @param contratoPontoConsumo O contrato ponto de consumo.,
	 * @return O tamanho da redução
	 * @throws GGASException the GGAS exception
	 */
	public Integer obterTamanhoIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws GGASException {

		return this.getControladorContrato().obterTamanhoIntervaloPCSPorCPC(contratoPontoConsumo);
	}

	/**
	 * Método responsável por listar os itens de
	 * vencimento de um contrato de ponto de
	 * consumo
	 * informado.
	 *
	 * @param chavePrimariaContratoPontoConsumo
	 *            A chave primária do contrato de
	 *            ponto de consumo.
	 * @return Lista de item de vencimento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumoItemFaturamento> listarItensVencimentoPorContratoPontoConsumo(
					Long chavePrimariaContratoPontoConsumo) throws GGASException {

		return this.getControladorContrato().listarItensVencimentoPorContratoPontoConsumo(chavePrimariaContratoPontoConsumo);
	}

	/**
	 * Método responsável por listar as
	 * Modalidades de um contrato de ponto de
	 * consumo informado.
	 *
	 * @param chavePrimariaContratoPontoConsumo
	 *            A chave primária do contrato de
	 *            ponto de consumo.
	 * @return Lista de Consumo Modalidade
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public Collection<ContratoPontoConsumoModalidade> listarModalidadesPorContratoPontoConsumo(Long chavePrimariaContratoPontoConsumo)
					throws GGASException {

		return this.getControladorContrato().listarModalidadesPorContratoPontoConsumo(chavePrimariaContratoPontoConsumo);
	}

	/**
	 * Método responsável por listar os Consumos
	 * Penalidades de um contrato de ponto de
	 * consumo
	 * informado.
	 *
	 * @param chavePrimariaContratoPontoConsumoModalidade the chave primaria contrato ponto consumo modalidade
	 * @return Uma coleção de Contrato Ponto
	 *         Consumo Penalidade
	 * @throws GGASException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public Collection<ContratoPontoConsumoPenalidade> listarConsumoPenalidadePorContratoPontoConsumoModalidade(
					Long chavePrimariaContratoPontoConsumoModalidade) throws GGASException {

		return this.getControladorContrato().listarConsumoPenalidadePorContratoPontoConsumoModalidade(
						chavePrimariaContratoPontoConsumoModalidade);
	}

	/**
	 * Método responsável por listar os
	 * ContratoCliente de um ponto de consumo
	 * informado.
	 *
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param idContrato the id contrato
	 * @return Uma coleção de ContratoCliente.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoCliente> listarContratoClientePorPontoConsumo(Long idPontoConsumo, Long idContrato) throws GGASException {

		return this.getControladorContrato().listarContratoClientePorPontoConsumo(idPontoConsumo, idContrato);
	}

	/**
	 * Método responsável por obter a quantidade
	 * de propostas aprovadas pelo número da
	 * proposta.
	 *
	 * @param numeroProposta
	 *            Número da proposta.
	 * @return Quantidade de propostas aprovadas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterQtdPropostasAprovadasPorNumero(String numeroProposta) throws GGASException {

		return this.getControladorProposta().obterQtdPropostasAprovadasPorNumero(numeroProposta);
	}

	/**
	 * Método responsável por obter uma quadra a
	 * partir da chave primaria da quadraFace
	 * informada.
	 *
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Uma Quadra
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Quadra obterQuadraPorQuadraFace(Long idQuadraFace) throws GGASException {

		return getControladorQuadra().obterQuadraPorFaceQuadra(idQuadraFace);
	}

	/**
	 * Método responsável por validar a data de
	 * assinatura do contrato.
	 *
	 * @param data
	 *            A data de assinatura do
	 *            contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarDataAssinaturaContrato(String data) throws GGASException {

		this.getControladorContrato().validarDataAssinaturaContrato(data);
	}
	
	/**
	 * Método responsável por validar a data de assinatura e arrecadador convênio relativo a situação de contrato.
	 *
	 * @param dataAssinatura A data de assinatura do contrato.
	 * @param arrecadadorConvenio Arrecadador convênio do contrato.
	 * @param situacaoContrato Situacao do contrato.
	 * @throws GGASException the GGAS exception
	 */
	public void validarSituacaoDataAssinaturaArrecadadador(String dataAssinatura, Long arrecadadorConvenio, Long situacaoContrato)
			throws GGASException {

		this.getControladorContrato().validarSituacaoDataAssinaturaArrecadadador( dataAssinatura,  arrecadadorConvenio,  situacaoContrato);
	}

	/**
	 * Método responsável por verificar se os
	 * pontos de consumo selecionados para a
	 * inclusão do
	 * contrato já estão
	 * em algum outro contrato com situação ativa.
	 *
	 * @param chaveContrato the chave contrato
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @throws GGASException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarPontosConsumoSelecionados(Long chaveContrato, Long[] chavesPontoConsumo) throws GGASException {

		this.getControladorContrato().validarPontosSelecionados(chaveContrato, chavesPontoConsumo);
	}

	/**
	 * Método responsável por validar a adição de
	 * uma nova penalidade de contrato de ponto de
	 * consumo em uma coleção de
	 * penalidades de um mesmo contrato de ponto
	 * de consumo.
	 *
	 * @param contratoPontoConsumoPenalidade
	 *            Uma penalidade de contrato de
	 *            ponto de consumo.
	 * @param listaContratoPontoConsumoPenalidade
	 *            Uma coleção de penalidades de
	 *            contrato de ponto de consumo
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarAdicaoPenalidade(ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade,
					Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) throws GGASException {

		this.getControladorContrato().validarAdicaoPenalidade(contratoPontoConsumoPenalidade, listaContratoPontoConsumoPenalidade);
	}

	/**
	 * Método responsável por validar os
	 * percentuais de compromisso de Take Or Pay e
	 * máximo em
	 * relação ao QDC
	 * informados.
	 *
	 * @param periodicidadePenalidade
	 *            A periodicidade da
	 *            penalidade(compromisso).
	 * @param percentualMargemVariacao
	 *            O percentual da margem de
	 *            variação Take Or Pay.
	 * @param percentualMaximoQDC
	 *            O percentual máximo em relação
	 *            ao QDC.
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarCompromissoTakeOrPayQPNR(EntidadeConteudo periodicidadePenalidade, BigDecimal percentualMargemVariacao,
					BigDecimal percentualMaximoQDC) throws GGASException {

		this.getControladorContrato().validarCompromissoTakeOrPayQPNR(periodicidadePenalidade, percentualMargemVariacao,
						percentualMaximoQDC);
	}

	/**
	 * Método que valida o percentual máximo em
	 * relação ao QDC.
	 *
	 * @param percentualMinimoQDC the percentual minimo qdc
	 * @param percentualMaximoQDC O percentual máximo informado.
	 * @throws GGASException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarPercentualMaximoQDC(BigDecimal percentualMinimoQDC, BigDecimal percentualMaximoQDC) throws GGASException {

		this.getControladorContrato().validarPercentualMaximoQDC(percentualMinimoQDC, percentualMaximoQDC);
	}

	/**
	 * Método responsável por listar as
	 * amortizações cadastradas.
	 *
	 * @return Uma coleção de amortizações.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarAmortizacoes() throws GGASException {

		return this.getControladorEntidadeConteudo().listarAmortizacoes();
	}

	/**
	 * Método responsável por obter a lsita de
	 * tipos de parada.
	 *
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoParada() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaTipoParada();
	}

	/**
	 * Método responsável por validar o dado
	 * padrão de uma atributo do modelo.
	 *
	 * @param abaAtributo
	 *            Atributo de um modelo.
	 * @param valorPadrao
	 *            Valor padrão informado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadoAtributoModeloContrato(AbaAtributo abaAtributo, String valorPadrao) throws GGASException {

		this.getControladorModeloContrato().validarDadoAtributoModeloContrato(abaAtributo, valorPadrao);
	}

	/**
	 * Obtem o modeloAtributo do modeloContrato.
	 *
	 * @param modeloContrato the modelo contrato
	 * @param nomeColunaAtributo the nome coluna atributo
	 */
	public void obterModeloAtributo(ModeloContrato modeloContrato, String nomeColunaAtributo) {

		this.getControladorModeloContrato().obterModeloAtributo(modeloContrato, nomeColunaAtributo);
	}

	/**
	 * Método que valida o percentual tarifa
	 * delivery or pay.
	 *
	 * @param percentualTarifaTOP
	 *            O percentual da tarifa delivery
	 *            or pay.
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarPercentualTarifaDOP(BigDecimal percentualTarifaTOP) throws GGASException {

		this.getControladorContrato().validarPercentualTarifaDOP(percentualTarifaTOP);
	}

	/**
	 * Método responsável por validar o tipo da
	 * parada que será inserida.
	 *
	 * @param idTipoParada Chave primária do tipo da
	 *            parada.
	 * @return the entidade conteudo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo validarTipoParada(Long idTipoParada) throws GGASException {

		return this.getControladorProgramacao().validarTipoParada(idTipoParada);
	}

	/**
	 * Método responsável por obter o controlador
	 * de faixa de pressão de fornecimento.
	 *
	 * @return O controlador de faixa de pressão
	 *         de fornecimento.
	 */
	private ControladorFaixaPressaoFornecimento getControladorFaixaPressaoFornecimento() {

		return (ControladorFaixaPressaoFornecimento) serviceLocator
						.getControladorNegocio(ControladorFaixaPressaoFornecimento.BEAN_ID_CONTROLADOR_FAIXA_PRESSAO_FORNECIMENTO);
	}

	/**
	 * Gets the controlador faixa consumo variacao.
	 *
	 * @return the controlador faixa consumo variacao
	 */
	private ControladorFaixaConsumoVariacao getControladorFaixaConsumoVariacao() {

		return (ControladorFaixaConsumoVariacao) serviceLocator
						.getControladorNegocio(ControladorFaixaConsumoVariacao.BEAN_ID_CONTROLADOR_ACESSO);
	}

	/**
	 * Método responsável por listar as faixa de
	 * pressão de fornecimento filtradas por
	 * segmento.
	 *
	 * @param segmento
	 *            Um segmento.
	 * @return Coleção de faixas de pressão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoPorSegmento(Segmento segmento) throws GGASException {

		return this.getControladorFaixaPressaoFornecimento().listarFaixaPressaoFornecimentoPorSegmento(segmento);
	}

	/**
	 * Listar faixa pressao fornecimento sem segmento associado.
	 *
	 * @return the collection
	 */
	public Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoSemSegmentoAssociado() {

		return this.getControladorFaixaPressaoFornecimento().listarFaixaPressaoFornecimentoSemSegmentoAssociado();
	}

	/**
	 * Método responsável por listar todas as
	 * faixas de pressão de fornecimento do
	 * sistema.
	 *
	 * @return Coleção de faixas de pressão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<FaixaPressaoFornecimento> listarTodasFaixasPressaoFornecimento() throws GGASException {

		return this.getControladorFaixaPressaoFornecimento().listarTodasFaixasPressaoFornecimento();
	}

	/**
	 * Método responsável por obter a faixa de
	 * pressão de fornecimento a partir de um
	 * segmento, uma
	 * medida mínima e uma
	 * unidade de pressão.
	 *
	 * @param segmento
	 *            O segmento.
	 * @param medidaMinimo
	 *            A medida mínima.
	 * @param unidadePressao
	 *            Unidade da pressão.
	 * @return A faixa de pressão de fornecimento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(Segmento segmento, BigDecimal medidaMinimo, Unidade unidadePressao)
					throws GGASException {

		return this.getControladorFaixaPressaoFornecimento().obterFaixaPressaoFornecimento(segmento, medidaMinimo, unidadePressao);
	}

	/**
	 * Método responsável por obter a faixa de
	 * pressão de fornecimento a partir da chave
	 * primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária da faixa de
	 *            pressão.
	 * @return A faixa de pressão de fornecimento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimentoPorChave(long chavePrimaria) throws GGASException {

		return (FaixaPressaoFornecimento) this.getControladorFaixaPressaoFornecimento().obter(chavePrimaria);
	}

	/**
	 * Método responsável por calcular o valor do
	 * QDC da solicitação de consumo para a data
	 * informada.
	 *
	 * @param listaContratoPontoConsumoModalidade
	 *            Lista de
	 *            ContratoPontoConsumoModalidade
	 * @param dataSolicitacao
	 *            Data da solicitação.
	 * @return O valor do QDC.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal calcularQDCSolicitacaoConsumo(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Date dataSolicitacao) throws GGASException {

		return this.getControladorProgramacao().calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade, dataSolicitacao);
	}

	/**
	 * Método responsável por listar as
	 * amostragens de PCS de um segmento
	 * informado.
	 *
	 * @param segmento
	 *            O segmento.
	 * @return Coleção de amostragens de PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarAmostragensPCSPorSegmento(Segmento segmento) throws GGASException {

		return this.getControladorSegmento().listarAmostragensPCSPorSegmento(segmento);
	}

	/**
	 * Método responsável por listar os intervalos
	 * de PCS de um segmento informado.
	 *
	 * @param segmento
	 *            O segmento.
	 * @return Coleção de intervalos de PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<IntervaloPCS> listarIntervalosPCSPorSegmento(Segmento segmento) throws GGASException {

		return this.getControladorSegmento().listarIntervalosPCSPorSegmento(segmento);
	}

	/**
	 * Método responsável por obter o controlador
	 * de auditoria.
	 *
	 * @return O controlador de auditoria.
	 */
	private ControladorAuditoria getControladorAuditoria() {

		return (ControladorAuditoria) serviceLocator.getControladorNegocio(ControladorAuditoria.BEAN_ID_CONTROLADOR_AUDITORIA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de cobrança.
	 *
	 * @return O controlador de cobrança.
	 */
	private ControladorCobranca getControladorCobranca() {

		return (ControladorCobranca) serviceLocator.getControladorNegocio(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de ação de cobrança.
	 *
	 * @return O controlador de ação de cobrança.
	 */
	private ControladorAcaoCobranca getControladorAcaoCobranca() {

		return (ControladorAcaoCobranca) serviceLocator.getControladorNegocio(ControladorAcaoCobranca.BEAN_ID_CONTROLADOR_ACAO_COBRANCA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de perfil de parcelamento.
	 *
	 * @return O controlador de perfil de
	 *         parcelamento.
	 */
	private ControladorPerfilParcelamento getControladorPerfilParcelamento() {

		return (ControladorPerfilParcelamento) serviceLocator
						.getControladorNegocio(ControladorPerfilParcelamento.BEAN_ID_CONTROLADOR_PERFIL_PARCELAMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de devolução.
	 *
	 * @return O controlador de devolucao.
	 */
	private ControladorDevolucao getControladorDevolucao() {

		return (ControladorDevolucao) serviceLocator.getControladorNegocio(ControladorDevolucao.BEAN_ID_CONTROLADOR_DEVOLUCAO);
	}

	/**
	 * Gets the controlador apuracao penalidade.
	 *
	 * @return the controlador apuracao penalidade
	 */
	private ControladorApuracaoPenalidade getControladorApuracaoPenalidade() {

		return (ControladorApuracaoPenalidade) serviceLocator
						.getControladorNegocio(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);
	}

	/**
	 * Método responsável por listar as
	 * amostragens de PCS de um ramo de atividade
	 * informado.
	 *
	 * @param ramoAtividade
	 *            O ramo de atividade.
	 * @return Coleção de amostragens de PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarAmostragensPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws GGASException {

		return this.getControladorRamoAtividade().listarAmostragensPCSPorRamoAtividade(ramoAtividade);
	}

	/**
	 * Método responsável por listar os intervalos
	 * de PCS de um ramo de atividade informado.
	 *
	 * @param ramoAtividade
	 *            O ramo de atividade.
	 * @return Coleção de intervalos de PCS.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<IntervaloPCS> listarIntervalosPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws GGASException {

		return this.getControladorRamoAtividade().listarIntervalosPCSPorRamoAtividade(ramoAtividade);
	}

	/**
	 * Método responsável por validar o percentual
	 * de participacao dos city gate pertencentes
	 * ao
	 * pontoConsumo.
	 *
	 * @param percentualCityGate Um array de string com os
	 *            percentuais de cada city gate.
	 * @throws GGASException the GGAS exception
	 */
	public void validarPercentualParticipacao(String[] percentualCityGate) throws GGASException {

		this.getControladorPontoConsumo().validarPercentualParticipacao(percentualCityGate);
	}

	/**
	 * Método responsável por obter o endereço
	 * formatado da aba de faturamento.
	 *
	 * @param cepParam Cep do endereço
	 * @param numero O número
	 * @return Endereço formatado.
	 * @throws GGASException the GGAS exception
	 */
	public String obterEnderecoFaturamentoFormatado(String cepParam, String numero) throws GGASException {

		return this.getControladorContrato().obterEnderecoFaturamentoFormatado(cepParam, numero);
	}

	/**
	 * Método responsável por fazer as validações
	 * das datas do contrato.
	 *
	 * @param contrato
	 *            O contrato a ser validade.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDatasContrato(Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDatasContrato(contrato);
	}

	/**
	 * Método responsável por fazer as validações
	 * das datas da aba principal do contrato.
	 *
	 * @param abaPrincipal
	 *            Aba principal do contrato.
	 * @param contrato
	 *            Contrato a ser validado.
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarDatasAbaPrincipal(Map<String, Object> abaPrincipal, Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDatasAbaPrincipal(abaPrincipal, contrato);
	}

	/**
	 * Método responsável por validar as datas da
	 * aba de modalidade.
	 *
	 * @param dataInicioRecuperacao Data de início da recuperação.
	 * @param dataFimRecuperacao Data máxima para recuperação.
	 * @param contrato the contrato
	 * @throws GGASException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarDatasAbaModalidade(String dataInicioRecuperacao, String dataFimRecuperacao, Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDatasAbaModalidade(dataInicioRecuperacao, dataFimRecuperacao, contrato);
	}

	/**
	 * Método responsável por listar as situações
	 * de processo permitidas na alteração de
	 * agendamento
	 * de processo.
	 *
	 * @return Uma coleção com situações.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SituacaoProcesso> listarSituacoesAlteracaoAgendamentoProcesso() throws GGASException {

		return this.getControladorProcesso().listarSituacoesAlteracaoAgendamentoProcesso();
	}

	/**
	 * Método responsável por validar se o
	 * processo pode ter seu agendamento alterado.
	 *
	 * @param chavePrimariaProcesso
	 *            A chave primária do processo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAlteracaoAgendamentoProcesso(Long chavePrimariaProcesso) throws GGASException {

		this.getControladorProcesso().validarAlteracaoAgendamentoProcesso(chavePrimariaProcesso);
	}

	/**
	 * Método responsável por validar alteção de
	 * rota inativa.
	 *
	 * @param rota the rota
	 * @param idPontosConsumoAssociados the id pontos consumo associados
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAlteracaoRotaInativa(Rota rota, Long[] idPontosConsumoAssociados) throws GGASException {

		this.getControladorRota().validarAlteracaoRotaInativa(rota, idPontosConsumoAssociados);
	}

	/**
	 * Método responsável por atualizar o
	 * agendamento de um processo.
	 *
	 * @param processo
	 *            O processo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarAgendamento(Processo processo) throws GGASException {

		this.getControladorProcesso().atualizarAgendamento(processo);
	}

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'Arrecadador' existentes ativas no sistema,
	 * carregando as entidades Banco e Cliente,
	 * caso existam.
	 *
	 * @return Coleção de Arrecadador do sistema
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Arrecadador> listarArrecadadorExistenteDescricao() throws GGASException {

		return this.getControladorArrecadacao().listarArrecadadorExistenteDescricao();
	}

	/**
	 * Obter arrecadador.
	 *
	 * @param idArrecadador the id arrecadador
	 * @return the arrecadador
	 * @throws GGASException the GGAS exception
	 */
	public Arrecadador obterArrecadador(Long idArrecadador) throws GGASException {

		return this.getControladorArrecadacao().obterArrecadador(idArrecadador);
	}

	/**
	 * Obter arrecadador.
	 *
	 * @param idArrecadador the id arrecadador
	 * @param propriedadesLazy the propriedades lazy
	 * @return the arrecadador
	 * @throws GGASException the GGAS exception
	 */
	public Arrecadador obterArrecadador(Long idArrecadador, String... propriedadesLazy) throws GGASException {

		return (Arrecadador) this.getControladorArrecadacao().obter(idArrecadador, ArrecadadorImpl.class, propriedadesLazy);
	}

	/**
	 * Método responsável por gerar movimento de
	 * débito automático.
	 *
	 * @param listaDebitoAutomaticoMovimento
	 *            lista de movimento de débito
	 *            automático.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void gerarMovimentoDebitoAutomatico(Collection<DebitoAutomaticoMovimento> listaDebitoAutomaticoMovimento) throws GGASException {

		this.getControladorArrecadacao().gerarMovimentoDebitoAutomatico(listaDebitoAutomaticoMovimento);
	}

	/**
	 * Método responsável para calcular o valor
	 * das parcelas do PRICE.
	 *
	 * @param valor the valor
	 * @param numeroPrestacoes the numero prestacoes
	 * @param taxaJuros the taxa juros
	 * @param dataInicial the data inicial
	 * @param periodicidade the periodicidade
	 * @param periodicidadeJuros the periodicidade juros
	 * @param isParcelamento the is parcelamento
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */

	public List<ValorParcela> calcularPrice(BigDecimal valor, int numeroPrestacoes, BigDecimal taxaJuros, Date dataInicial,
					Integer periodicidade, Integer periodicidadeJuros, boolean isParcelamento) throws NegocioException {

		return this.getControladorCobranca().calcularPrice(valor, numeroPrestacoes, taxaJuros, dataInicial, periodicidade,
						periodicidadeJuros, isParcelamento);
	}

	/**
	 * Método responsável para calcular o valor
	 * das parcelas no SAC.
	 *
	 * @param valor the valor
	 * @param parcelas the parcelas
	 * @param taxaJuros the taxa juros
	 * @param dataInicial the data inicial
	 * @param periodicidade the periodicidade
	 * @param periodicidadeJuros the periodicidade juros
	 * @param isParcelamento the is parcelamento
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<ValorParcela> calcularSAC(BigDecimal valor, int parcelas, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento) throws GGASException {

		return this.getControladorCobranca().calcularSAC(valor, parcelas, taxaJuros, dataInicial, periodicidade, periodicidadeJuros,
						isParcelamento);
	}

	/**
	 * Método responsável por Obter um Tipo
	 * Convênio do sistema.
	 *
	 * @param chaveEntidadeClasse A Chave Primaria do Entidade
	 *            Conteudo
	 * @return Uma Entidade Conteudo
	 * @throws GGASException the GGAS exception
	 */
	public EntidadeConteudo obterTipoConvenio(Long chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterTipoConvenio(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por obter a lista de
	 * Amortização.
	 *
	 * @return coleção de Amortização.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaAmortizacao() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaAmortizacao();
	}

	/**
	 * Método responsável por consultar as faturas
	 * de um cliente ou de um ponto de consumo.
	 *
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return Uma coleção de Fatura.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> consultarFaturasClientePontoConsumo(Long idCliente, Long idPontoConsumo) throws GGASException {

		return this.getControladorCobranca().consultarFaturasClientePontoConsumo(idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por consultar as faturas
	 * ABERTAS de um cliente ou de um ponto de
	 * consumo
	 * que sejam do tipo FATURA ou NOTA DE DEBITO.
	 *
	 * @param idCliente Chave primária do cliente.
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param quitada the quitada
	 * @return Uma coleção de Fatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> consultarFaturasAbertasClientePontoConsumo(Long idCliente, Long idPontoConsumo, Boolean quitada)
					throws GGASException {

		return this.getControladorCobranca().consultarFaturasAbertasClientePontoConsumo(idCliente, idPontoConsumo, quitada);
	}

	/**
	 * Método que retorna uma lista de faturas
	 * para a inclusão de devoluções.
	 *
	 * @param idCliente the id cliente
	 * @param idPontoConsumo the id ponto consumo
	 * @return lista de faturas
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Fatura> consultarFaturasManterDevolucao(Long idCliente, Long idPontoConsumo) throws GGASException {

		return this.getControladorDevolucao().consultarFaturasManterDevolucao(idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria de um
	 * imovel.
	 *
	 * @param chave A chave primaria de uma imovel
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorImovel(Long chave) throws GGASException {

		return this.getControladorContrato().consultarContratoPontoConsumoPorImovel(chave);
	}

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria de um
	 * imovel.
	 *
	 * @param chave A chave primaria de uma imovel
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> consultarContratoClientePontoConsumoPorImovel(Long chave) throws GGASException {

		return this.getControladorContrato().consultarContratoClientePontoConsumoPorImovel(chave);
	}

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria de um
	 * imovel.
	 *
	 * @param chave A chave primaria de uma imovel
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> consultarPontoConsumoPorImovelComContrato(Long chave) throws GGASException {

		return this.getControladorContrato().consultarPontoConsumoPorImovelComContrato(chave);
	}

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria do
	 * cliente.
	 *
	 * @param chave
	 *            A chave primaria de um cliente.
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorCliente(Long chave) throws GGASException {

		return this.getControladorContrato().consultarContratoPontoConsumoPorCliente(chave);
	}

	/**
	 * Validar filtro pesquisa pontos consumo.
	 *
	 * @param idImovel the id imovel
	 * @param idCliente the id cliente
	 * @throws GGASException the GGAS exception
	 */
	public void validarFiltroPesquisaPontosConsumo(Long idImovel, Long idCliente) throws GGASException {

		this.getControladorParcelamento().validarFiltroPesquisaPontosConsumo(idImovel, idCliente);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Recebimento.
	 *
	 * @return ControladorRecebimento O
	 *         controlador de Recebimento.
	 */
	private ControladorRecebimento getControladorRecebimento() {

		return (ControladorRecebimento) serviceLocator.getControladorNegocio(ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Credito Debito.
	 *
	 * @return ControladorCreditoDebito O
	 *         controlador de Credito Debito.
	 */
	private ControladorCreditoDebito getControladorCreditoDebito() {

		return (ControladorCreditoDebito) serviceLocator.getControladorNegocio(ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);
	}

	/**
	 * Método responsável por obter o controlador
	 * do parcelamento.
	 *
	 * @return ControladorParcelamento O
	 *         controlador do parcelamento.
	 */
	private ControladorParcelamento getControladorParcelamento() {

		return (ControladorParcelamento) serviceLocator.getControladorNegocio(ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Fatura.
	 *
	 * @return ControladorFatura O controlador de
	 *         Fatura.
	 */
	private ControladorFatura getControladorFatura() {

		return (ControladorFatura) serviceLocator.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
	}

	/**
	 * Gets the controlador fatura controle impressao.
	 *
	 * @return the controlador fatura controle impressao
	 */
	private ControladorFaturaControleImpressao getControladorFaturaControleImpressao() {

		return (ControladorFaturaControleImpressao) serviceLocator
						.getControladorNegocio(ControladorFaturaControleImpressao.BEAN_ID_CONTROLADOR_FATURA_CONTROLE_IMPRESSAO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Fatura.
	 *
	 * @return ControladorFatura O controlador de
	 *         Fatura.
	 */
	private ControladorFaturamentoAnormalidade getControladorFaturamentoAnormalidade() {

		return (ControladorFaturamentoAnormalidade) serviceLocator
						.getControladorNegocio(ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Rubrica.
	 *
	 * @return ControladorRubrica O controlador de
	 *         Rubrica.
	 */
	private ControladorRubrica getControladorRubrica() {

		return (ControladorRubrica) serviceLocator.getControladorNegocio(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);
	}

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'RecebimentoSituacao' existentes ativas no
	 * sistema.
	 *
	 * @return Coleção de RecebimentoSituacao do
	 *         sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<RecebimentoSituacao> listarRecebimentoSituacaoExistente() throws GGASException {

		return getControladorRecebimento().listarRecebimentoSituacaoExistente();
	}

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'TipoDocumento' existentes ativas no
	 * sistema.
	 *
	 * @return Coleção de TipoDocumento do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoDocumento> listarTipoDocumentoExistente() throws GGASException {

		return getControladorArrecadacao().listarTipoDocumentoExistente();
	}

	/**
	 * Retorna a uma lista de crédito débtio a
	 * realizar.
	 *
	 * @param idCliente the id cliente
	 * @param idPontoConsumo the id ponto consumo
	 * @return lista de credito débito a realizar
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosManterDevolucao(Long idCliente, Long idPontoConsumo)
					throws GGASException {

		return this.getControladorDevolucao().consultarCreditosDebitosManterDevolucao(idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por validar se algum
	 * filtro foi selecionado para a pesquisa.
	 *
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarFiltroPesquisaExtratoDebito(Long idCliente, Long idPontoConsumo) throws GGASException {

		this.getControladorCobranca().validarFiltroPesquisaExtratoDebito(idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por obter o valor do
	 * recebimento da fatura.
	 *
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return O valor do recebimento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal obterValorRecebimentoPelaFatura(Long idFatura) throws GGASException {

		return this.getControladorCobranca().obterValorRecebimentoPelaFatura(idFatura);
	}

	/**
	 * Método responsável por obter os tipos de
	 * documento.
	 *
	 * @param chavePrimaria Chave primária do tipo de
	 *            documento.
	 * @return O tipo de documento.
	 * @throws GGASException the GGAS exception
	 */
	public TipoDocumento obterTipoDocumento(Long chavePrimaria) throws GGASException {

		return this.getControladorArrecadacao().obterTipoDocumento(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma Acao
	 * Cobranca pela Chave Primária.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Uma Ação de Cobrança
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AcaoCobranca obterAcaoCobranca(Long chavePrimaria) throws GGASException {

		return this.getControladorAcaoCobranca().obterAcaoCobranca(chavePrimaria);
	}

	/**
	 * Método responsável por inserir um documento
	 * de cobrança de gerar o relatório de extrato
	 * de
	 * débitos.
	 *
	 * @param chavesFatura Chaves primárias das faturas.
	 * @param chavesCreditoDebito Chaves primárias dos créditos e
	 *            débitos.
	 * @param idCliente Chave primária do cliente.
	 * @param documentoCobranca Documento de cobrança a ser
	 *            populado e inserido.
	 * @param indicadorExtratoDebito Indica se o relatório é de
	 *            extrato de débito.
	 * @param idTipoDocumento the id tipo documento
	 * @return Um array com o documento de
	 *         cobrança de código de barras e o
	 *         documento de cobrança de
	 *         boleto bancário.
	 * @throws GGASException the GGAS exception
	 */
	public DocumentoCobranca[] inserirDocumentoCobranca(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente,
					DocumentoCobranca documentoCobranca, boolean indicadorExtratoDebito, Long idTipoDocumento)
					throws GGASException {

		return this.getControladorCobranca().inserirDocumentoCobranca(chavesFatura, chavesCreditoDebito, idCliente, documentoCobranca,
						indicadorExtratoDebito, idTipoDocumento);
	}

	/**
	 * Método responsável por criar um documento
	 * de cobrança.
	 *
	 * @return Um DocumentoCobranca
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DocumentoCobranca criarDocumentoCobranca() throws GGASException {

		return (DocumentoCobranca) this.getControladorCobranca().criarDocumentoCobranca();
	}

	/**
	 * Método responsável por obter um cliente a
	 * partir do ponto de consumo.
	 *
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return Um Cliente.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente obterClientePorPontoConsumo(Long idPontoConsumo) throws GGASException {

		return this.getControladorCliente().obterClientePorPontoConsumo(idPontoConsumo);
	}

	/**
	 * Método responsável por obter um cliente a
	 * partir do ponto de consumo do contrato
	 * ativo.
	 *
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return Um Cliente.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente obterClientePorPontoConsumoDeContratoAtivo(Long idPontoConsumo) throws GGASException {

		return this.getControladorCliente().obterClientePorPontoConsumoDeContratoAtivo(idPontoConsumo);
	}

	/**
	 * Método responsável por obter a fatura pela
	 * fatura geral.
	 *
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return FaturaGeral
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaturaGeral obterFaturaGeralPelaFatura(Long idFatura) throws GGASException {

		return this.getControladorCobranca().obterFaturaGeralPelaFatura(idFatura);
	}

	/**
	 * Obter fatura.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesEager the propriedades eager
	 * @return the fatura
	 * @throws GGASException the GGAS exception
	 */
	public Fatura obterFatura(Long chavePrimaria, String... propriedadesEager) throws GGASException {

		return (Fatura) this.getControladorCobranca().obterFatura(chavePrimaria, propriedadesEager);
	}

	/**
	 * Obter credito debito a realizar.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the credito debito a realizar
	 * @throws GGASException the GGAS exception
	 */
	public CreditoDebitoARealizar obterCreditoDebitoARealizar(Long chavePrimaria) throws GGASException {

		return (CreditoDebitoARealizar) this.getControladorCreditoDebito().obter(chavePrimaria);
	}

	/**
	 * Obter credito debito a realizar.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the credito debito a realizar
	 * @throws GGASException the GGAS exception
	 */
	public CreditoDebitoARealizar obterCreditoDebitoARealizar(Long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (CreditoDebitoARealizar) this.getControladorCreditoDebito().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter as faturas
	 * pelas chaves primárias.
	 *
	 * @param chavesFatura
	 *            Chave primária das faturas.
	 * @return Uma coleção de Fatura.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> consultarFaturas(Long[] chavesFatura) throws GGASException {

		return this.getControladorCobranca().consultarFaturas(chavesFatura);
	}

	/**
	 * Método responsável por consultar as faturas
	 * pelas chaves primárias.
	 *
	 * @param chavesFatura Chaves primárias das faturas
	 * @return Coleção de Faturas.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> consultarFaturasPorChaves(Object[] chavesFatura) throws NegocioException {

		return this.getControladorFatura().consultarFaturasPorChaves(chavesFatura);
	}

	/**
	 * Método responsável por consultar as ações e
	 * cobraças de acordo com o filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de ações e cobrança.
	 * @return Uma coleção de ações e cobrança.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AcaoCobranca> consultarAcoesCobranca(Map<String, Object> filtro) throws GGASException {

		return getControladorAcaoCobranca().consultarAcoesCobranca(filtro);
	}

	/**
	 * Método responsável por listar as Ações
	 * Cobrança.
	 *
	 * @return Uma coleção de Ações e Cobranças.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AcaoCobranca> listarAcaoCobranca() throws GGASException {

		return getControladorAcaoCobranca().listarAcaoCobranca();
	}

	/**
	 * Método responsável por criar um perfil de
	 * parcelamento.
	 *
	 * @return Um Perfil de Parcelamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParcelamentoPerfil criarParcelamentoPerfil() throws GGASException {

		return (ParcelamentoPerfil) this.getControladorPerfilParcelamento().criar();
	}

	/**
	 * Método responsável por inserir um
	 * parcelamento perfil.
	 *
	 * @param parcelamentoPerfil the parcelamento perfil
	 * @return chave primária do perfil inserido
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirParcelamentoPerfil(ParcelamentoPerfil parcelamentoPerfil) throws GGASException {

		return this.getControladorPerfilParcelamento().inserir(parcelamentoPerfil);
	}

	/**
	 * Método responsável por obter a lista de
	 * tipo valor.
	 *
	 * @return coleção de tipo valor
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoValor() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaTipoValor();
	}

	/**
	 * Método responsável por consultar o perfil
	 * do parcelamento através do filtro
	 * informado.
	 *
	 * @param filtro
	 *            Mapa contendo os filtros
	 * @return lista de perfis de parcelamento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ParcelamentoPerfil> consultarPerfilParcelamento(Map<String, Object> filtro) throws GGASException {

		return this.getControladorPerfilParcelamento().consultarPerfilParcelamento(filtro);
	}

	/**
	 * Método responsável por remover os perfis de
	 * parcelamento selecionados.
	 *
	 * @param chavesPrimarias
	 *            As chaves dos perfis a serem
	 *            excluídos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerPerfisParcelamento(Long[] chavesPrimarias) throws GGASException {

		this.getControladorPerfilParcelamento().removerPerfisParcelamento(chavesPrimarias);
	}

	/**
	 * Método responsável por obter um perfil de
	 * parcelamento..
	 *
	 * @param chavePrimaria
	 *            A chave do perfil.
	 * @return ParcelamentoPerfil.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParcelamentoPerfil obterParcelamentoPerfil(Long chavePrimaria) throws GGASException {

		return (ParcelamentoPerfil) getControladorPerfilParcelamento().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter um perfil de
	 * parcelamento..
	 *
	 * @param chavePrimaria
	 *            A chave do perfil.
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy.
	 * @return ParcelamentoPerfil.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParcelamentoPerfil obterParcelamentoPerfil(Long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (ParcelamentoPerfil) getControladorPerfilParcelamento().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por alterar um perfil de
	 * parcelamento.
	 *
	 * @param perfil
	 *            O perfil de parcelamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarPerfilParcelamento(ParcelamentoPerfil perfil) throws GGASException {

		this.getControladorPerfilParcelamento().atualizar(perfil);
	}

	/**
	 * Método responsável por consultar as
	 * devoluções.
	 *
	 * @param filtro the filtro
	 * @return lista de devoluções
	 */
	public Collection<Devolucao> consultarDevolucoes(Map<String, Object> filtro) {

		return getControladorDevolucao().consultarDevolucoes(filtro);
	}

	/**
	 * Método responsável por validar as datas da
	 * aba de responsabilidade.
	 *
	 * @param contratoCliente
	 *            ContratoCliente que contém as
	 *            datas a serem validadas.
	 * @param contrato
	 *            Contrato a ser adicionado.
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	public void validarDatasAbaResponsabilidade(ContratoCliente contratoCliente, Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDatasAbaResponsabilidade(contratoCliente, contrato);
	}

	/**
	 * Método responsável por criar um
	 * ParcelamentoPerfilSegmento.
	 *
	 * @return ParcelamentoPerfilSegmento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParcelamentoPerfilSegmento criarParcelamentoPerfilSegmento() throws GGASException {

		return (ParcelamentoPerfilSegmento) this.getControladorPerfilParcelamento().criarParcelamentoPerfilSegmento();
	}

	/**
	 * Método responsável por verificar se o
	 * tamanho de redução para recuperção de PCS é
	 * obrigatório
	 * para o contrato a
	 * partir dos intervalos de PCS informados.
	 *
	 * @param arrayIdsIntervalosPCS
	 *            Array de identificadores dos
	 *            intervalos de PCS.
	 * @return true caso obrigatório
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean isObrigatorioTamanhoReducaoRecuperacaoPCS(Long[] arrayIdsIntervalosPCS) throws GGASException {

		return this.getControladorContrato().isObrigatorioTamanhoReducaoRecuperacaoPCS(arrayIdsIntervalosPCS);
	}

	/**
	 * Método responsável por criar um
	 * ParcelamentoPerfilSituacaoPontoConsumo.
	 *
	 * @return
	 *         ParcelamentoPerfilSituacaoPontoConsumo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ParcelamentoPerfilSituacaoPontoConsumo criarParcelamentoPerfilSituacaoPontoConsumo() throws GGASException {

		return (ParcelamentoPerfilSituacaoPontoConsumo) this.getControladorPerfilParcelamento()
						.criarParcelamentoPerfilSituacaoPontoConsumo();
	}

	/**
	 * Método responsável por validar a remoção de
	 * um ponto de consumo de um imóvel.
	 *
	 * @param chavePontoConsumo
	 *            A chave primária do ponto
	 *            consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemocaoPontoConsumoImovel(long chavePontoConsumo) throws GGASException {

		this.getControladorImovel().validarRemocaoPontoConsumoImovel(chavePontoConsumo);
	}

	/**
	 * Método responsável por validar o aditamento
	 * de um contrato.
	 *
	 * @param chavePrimaria
	 *            A chave primária do de contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAditamentoContrato(Long chavePrimaria) throws GGASException {

		this.getControladorContrato().validarAditamentoContrato(chavePrimaria);
	}

	/**
	 * Método responsável por validar o aditamento
	 * de um contrato.
	 *
	 * @param chavePrimaria
	 *            A chave primária do de contrato.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAlteracaoContrato(Long chavePrimaria) throws GGASException {

		this.getControladorContrato().validarAditamentoContrato(chavePrimaria);
	}

	/**
	 * Método responsável por listar todos os
	 * tipos de documento.
	 *
	 * @return Uma coleção com todos os tipos de
	 *         documento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TipoDocumento> listarTodosTipoDocumento() throws GGASException {

		return this.getControladorArrecadacao().listarTipoDocumento();
	}

	/**
	 * Método responsável por verificar o tamanho
	 * máximo do comentário do campo Solicitações
	 * adicionais.
	 *
	 * @param comentario O comentário a ser validado.
	 * @throws GGASException the GGAS exception
	 */
	public void validarTamanhoComentario(String comentario) throws GGASException {

		this.getControladorProposta().validarTamanhoComentario(comentario);
	}

	/**
	 * Método responsável por obter o cliente pelo
	 * número do contrato.
	 *
	 * @param numeroContrato
	 *            Número do Contrato.
	 * @return Um cliente.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Cliente obterClienteAssinaturaPeloNumeroContrato(String numeroContrato) throws GGASException {

		return this.getControladorCliente().obterClienteAssinaturaPeloNumeroContrato(numeroContrato);
	}

	/**
	 * Método responsável por validar a hora
	 * referente ao campo da aba de modalidades.
	 *
	 * @param data A data atual
	 * @param hora A hora informada pelo usuário.
	 * @throws GGASException the GGAS exception
	 */
	public void validarHoraDia(DateTime data, Integer hora) throws GGASException {

		this.getControladorContrato().validarHoraDia(data, hora);
	}

	/**
	 * Método responsável por gerar relatório de
	 * declaração de quitação anual para um ponto
	 * de
	 * consumo, para
	 * todos os pontos de consumo de um cliente ou
	 * todos os pontos de consumo de um contrato.
	 *
	 * @param anoFaturas
	 *            Ano para geração da declaração.
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @param numeroContrato
	 *            Número do contrato.
	 * @return Um array de bytes do relatório.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public byte[] gerarRelatorioDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato)
					throws GGASException {

		return this.getControladorCobranca().gerarRelatorioDeclaracaoQuitacaoAnual(anoFaturas, idCliente, idPontoConsumo, numeroContrato);
	}

	/**
	 * Método responsável por validar o filtro da
	 * pesquisa para geração de declaração de
	 * quitação
	 * anual.
	 *
	 * @param anoFaturas Ano para geração das faturas.
	 * @param idCliente Chave primária do cliente.
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param numeroContrato Número do contrato.
	 * @param indicadorGeral the indicador geral
	 * @throws GGASException Caso o usuário não informe o
	 *             ano para geração da declaração
	 *             ou caso
	 *             ele não preencha algum dos
	 *             campos da pesquisa.
	 */
	public void validarFiltroDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato,
					Boolean indicadorGeral) throws GGASException {

		this.getControladorCobranca().validarFiltroDeclaracaoQuitacaoAnual(anoFaturas, idCliente, idPontoConsumo, numeroContrato,
						indicadorGeral);
	}

	/**
	 * Método responsável por validar se a soma
	 * dos QDCs das modalidades já adicionadas
	 * para uma
	 * determinada data é
	 * maior que o QDC do contrato no aditamento.
	 *
	 * @param listaModalidadeQDC Lista de QDCs dos pontos de
	 *            consumo.
	 * @param listaContratoQDC Lista de QDCs do contrato.
	 * @param dataVencimento Data de vencimento do contrato.
	 * @return A primeira data onde a soma é maior
	 *         que a QDC do contrato.
	 * @throws GGASException the GGAS exception
	 */
	public String validarAlteracaoQDCContrato(Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC,
					Collection<ContratoQDC> listaContratoQDC, Date dataVencimento) throws GGASException {

		return this.getControladorContrato().validarAlteracaoQDCContrato(listaModalidadeQDC, listaContratoQDC, dataVencimento);
	}

	/**
	 * Método responsável por validar se a
	 * modalidade já foi adicionada.
	 *
	 * @param listaModalidades
	 *            Lista de modalidades já
	 *            adicionadas.
	 * @param modalidadeConsumoFaturamentoVO
	 *            Modalidade a ser adicionada.
	 * @throws GGASException
	 *             Caso a modalidade a ser
	 *             adicionada já tenha sido
	 *             adicionada anteriormente.
	 */
	public void validarModalidadeExistente(Collection<ModalidadeConsumoFaturamentoVO> listaModalidades,
					ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO) throws GGASException {

		this.getControladorContrato().validarModalidadeExistente(listaModalidades, modalidadeConsumoFaturamentoVO);
	}

	/**
	 * Obter valor fatura.
	 *
	 * @param chaveFatura the chave fatura
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal obterValorFatura(Long chaveFatura) throws GGASException {

		return this.getControladorCobranca().obterValorFatura(chaveFatura);
	}

	/**
	 * Retorna uma lista com os tipos de
	 * devoluções.
	 *
	 * @return lista de tipo de devoluções
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarTipoDevolucao() throws GGASException {

		return getControladorEntidadeConteudo().listarTipoDevolucao();
	}

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'Recebimento' filtradas pela tela.
	 *
	 * @param filtro the filtro
	 * @return Coleção de Recebimento do sistema
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Recebimento> consultarRecebimento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorRecebimento().consultarRecebimento(filtro);
	}

	/**
	 * Método que valida se a data inicial é menor
	 * que a data final.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws GGASException the GGAS exception
	 */
	public void validarDataDevolucaoInicialFinal(String dataInicial, String dataFinal) throws GGASException {

		this.getControladorDevolucao().validarDataDevolucaoInicialFinal(dataInicial, dataFinal);
	}

	/**
	 * Método responsável por validar as datas da
	 * aba de responsabilidade.
	 *
	 * @param dataRelacaoInicio
	 *            Data de início do relacionamento
	 * @param dataRelacaoFim
	 *            Data de fim do relacionamento
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	public void validarDatasAbaRelacionamento(Date dataRelacaoInicio, Date dataRelacaoFim) throws GGASException {

		this.getControladorImovel().validarDatasAbaRelacionamento(dataRelacaoInicio, dataRelacaoFim);
	}

	/**
	 * Metodo responsavel por consultar se existe
	 * algum imovel associado ao imovel consultado.
	 *
	 * @param chavePrimariaImovelCondominio A chave primaria do imovel
	 *            consultado
	 * @return a quantidade de imoveis associados
	 *         ao imovel consultado
	 * @throws GGASException the GGAS exception
	 */
	public int quantidadeImoveisFilhos(Long chavePrimariaImovelCondominio) throws GGASException {

		return this.getControladorImovel().quantidadeImoveisFilhos(chavePrimariaImovelCondominio);
	}

	/**
	 * Método responsável por verificar a
	 * existência de historico de operacao dos
	 * medidores no
	 * sistema.
	 *
	 * @param filtro com os dados do medidor(es)
	 * @return uma lista de medidores com
	 *         historico de medicao
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Medidor> pesquisarMedidorComHistoricoOperacao(Map<String, Object> filtro) throws GGASException {

		return this.getControladorMedidor().pesquisarMedidorComHistoricoOperacao(filtro);
	}

	/**
	 * Metodo responsavel por validar a data de
	 * agendamento.
	 *
	 * @param processo the processo
	 * @param horaInicio the hora inicio
	 * @param horaFim the hora fim
	 * @throws GGASException the GGAS exception
	 */
	public void validarDataAgendamento(Processo processo, String horaInicio, String horaFim) throws GGASException {

		this.getControladorProcesso().validarDataAgendamento(processo, horaInicio, horaFim);
	}

	/**
	 * Listar faturas por chave devolucao.
	 *
	 * @param idDevolucao the id devolucao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Fatura> listarFaturasPorChaveDevolucao(long idDevolucao) throws GGASException {

		return this.getControladorDevolucao().listarFaturasPorChaveDevolucao(idDevolucao);
	}

	/**
	 * Listar creditos por chave devolucao.
	 *
	 * @param idDevolucao the id devolucao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CreditoDebitoARealizar> listarCreditosPorChaveDevolucao(long idDevolucao) throws GGASException {

		return this.getControladorDevolucao().listarCreditosPorChaveDevolucao(idDevolucao);
	}

	/**
	 * Atualizar credito debito a realizar.
	 *
	 * @param creditoDebitoARealizar the credito debito a realizar
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) throws GGASException {

		this.getControladorCreditoDebito().atualizar(creditoDebitoARealizar);
	}

	/**
	 * Atualizar fatura.
	 *
	 * @param fatura the fatura
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarFatura(Fatura fatura) throws GGASException {

		this.getControladorFatura().atualizar(fatura);
	}

	/**
	 * Atualizar recebimento.
	 *
	 * @param recebimento the recebimento
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarRecebimento(Recebimento recebimento) throws GGASException {

		this.getControladorRecebimento().atualizar(recebimento);
	}

	/**
	 * Atualizar historico anomalia fatura.
	 *
	 * @param historicoAnomaliaFaturamento the historico anomalia faturamento
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarHistoricoAnomaliaFatura(HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento) throws GGASException {

		this.getControladorHistoricoAnomaliaFaturamento().atualizar(historicoAnomaliaFaturamento);
	}

	/**
	 * Método responsável por obter o Banco pela
	 * chave primária.
	 *
	 * @param chavePrimaria chave primária do banco
	 * @return Um Banco.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Banco obterBanco(Long chavePrimaria) throws GGASException {

		return this.getControladorCobranca().obterBanco(chavePrimaria);
	}

	/**
	 * Método responsável por obter Aviso Bancário
	 * pela chave primária.
	 *
	 * @param chavePrimaria chave primária do Aviso Bancário
	 * @return Um Aviso Bancário.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public AvisoBancario obterAvisoBancario(Long chavePrimaria) throws GGASException {

		return this.getControladorArrecadacao().obterAvisoBancario(chavePrimaria);
	}

	/**
	 * Método responsável por criar Aviso Bancário.
	 *
	 * @return Um Aviso Bancário.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeNegocio criarAvisoBancario() throws GGASException {

		return this.getControladorArrecadacao().criarAvisoBancario();
	}

	/**
	 * Método responsável por obter
	 * DocumentoCobrancaItem pela chave primária.
	 *
	 * @param chavePrimaria chave primária do
	 *            DocumentoCobrancaItem
	 * @return Um DocumentoCobrancaItem.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DocumentoCobrancaItem obterDocumentoCobrancaItem(Long chavePrimaria) throws GGASException {

		return this.getControladorArrecadacao().obterDocumentoCobrancaItem(chavePrimaria);
	}

	/**
	 * Método responsável por obter FaturaGeral
	 * pela chave primária.
	 *
	 * @param chavePrimaria chave primária da FaturaGeral
	 * @return Uma FaturaGeral.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaturaGeral obterFaturaGeral(Long chavePrimaria) throws GGASException {

		return this.getControladorArrecadacao().obterFaturaGeral(chavePrimaria);
	}

	/**
	 * Alterar devolucao.
	 *
	 * @param devolucao the devolucao
	 * @throws GGASException the GGAS exception
	 */
	public void alterarDevolucao(Devolucao devolucao) throws GGASException {

		this.getControladorDevolucao().atualizar(devolucao);
	}

	/**
	 * Método responsável por obter o último
	 * sequencial do aviso bancário para o
	 * arrecadador e data
	 * informada.
	 *
	 * @param arrecadador
	 *            Arrecadador
	 * @param dataLancamento
	 *            Data de Lançamento
	 * @return Último Sequencial
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterUltimoSequencialAvisoBancario(Arrecadador arrecadador, Date dataLancamento) throws GGASException {

		return getControladorArrecadacao().obterUltimoSequencialAvisoBancario(arrecadador, dataLancamento);
	}

	/**
	 * Obter ano mes contabil.
	 *
	 * @param anoMesContabilParametro the ano mes contabil parametro
	 * @param dataLancamento the data lancamento
	 * @return the integer
	 * @throws GGASException the GGAS exception
	 */
	public Integer obterAnoMesContabil(Integer anoMesContabilParametro, Date dataLancamento) throws GGASException {

		return getControladorArrecadacao().obterAnoMesContabil(anoMesContabilParametro, dataLancamento);
	}

	/**
	 * Metodo responsável por restornar uma lista
	 * de tipos de convênio.
	 *
	 * @param chaveArrecadador
	 *            A chave do arrecadador.
	 * @return lista de tipos de convênio
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterTiposConvenioPorArrecadador(Long chaveArrecadador) throws GGASException {

		return this.getControladorRecebimento().obterTiposConvenioPorArrecadador(chaveArrecadador);
	}

	/**
	 * Obter ac convenio por arrecadador.
	 *
	 * @param chaveArrecadador the chave arrecadador
	 * @param chaveTipoConvenio the chave tipo convenio
	 * @return the arrecadador contrato convenio
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorContratoConvenio obterACConvenioPorArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws GGASException {

		return getControladorArrecadacao().obterACConvenioPorArrecadador(chaveArrecadador, chaveTipoConvenio);
	}

	/**
	 * metodo responsavel por retonar uma colecao
	 * de arrecadador contrato convenio.
	 *
	 * @param chaveArrecadador chave primaria do arrecadador
	 * @param chaveFormaArrecadacao chave primaria da forma de
	 *            arrecadacao
	 * @return uma colecao de arrecador contrato
	 *         convenio do sistema
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ArrecadadorContratoConvenio> consultarACConvenioPorArrecadador(Long chaveArrecadador, Long chaveFormaArrecadacao)
					throws GGASException {

		return getControladorArrecadacao().consultarACConvenioPorArrecadador(chaveArrecadador, chaveFormaArrecadacao);
	}

	/**
	 * Validar dados recebimento.
	 *
	 * @param idArrecadador the id arrecadador
	 * @param idFormaArrecadacao the id forma arrecadacao
	 * @param dataRecebimentoString the data recebimento string
	 * @param valorRecebimentoString the valor recebimento string
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosRecebimento(Long idArrecadador, Long idFormaArrecadacao, String dataRecebimentoString,
					String valorRecebimentoString) throws GGASException {

		getControladorRecebimento().validarDadosRecebimento(idArrecadador, idFormaArrecadacao, dataRecebimentoString,
						valorRecebimentoString);
	}

	/**
	 * Método responsável por obter a entidade
	 * conteúdo padrão de uma classe.
	 *
	 * @param chaveEntidadeClasse the chave entidade classe
	 * @return Uma EntidadeConteudo.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterEntidadeConteudoPadraoPorClasse(String chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterEntidadeConteudoPadraoPorClasse(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por comparar duas datas
	 * de vigencia incial de perfil parcelamento,
	 * para
	 * validação de alteração.
	 *
	 * @param dataVigenciaTela the data vigencia tela
	 * @param dataVigenciaBanco the data vigencia banco
	 * @throws GGASException the GGAS exception
	 */
	public void validarDataVigenciaInicialPerfilParcelamento(Date dataVigenciaTela, Date dataVigenciaBanco) throws GGASException {

		this.getControladorPerfilParcelamento().validarDataVigenciaInicialPerfilParcelamento(dataVigenciaTela, dataVigenciaBanco);
	}

	/**
	 * Método responsável por verificar se a
	 * vigencia final do perfil parcelamento já
	 * foi alterado
	 * em um perfil já vinculado.
	 *
	 * @param dataVigenciaFinal the data vigencia final
	 * @throws NegocioException the negocio exception
	 */
	public void validarPerfilParcelamentoVigenciaFinal(Date dataVigenciaFinal) throws NegocioException {

		this.getControladorPerfilParcelamento().validarPerfilParcelamentoVigenciaFinal(dataVigenciaFinal);
	}

	/**
	 * Método responsável por obter os
	 * ContratoPontoConsumo pelo cliente.
	 *
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ContratoPontoConsumo> obterContratoPontoConsumoPorCliente(Long idCliente) throws GGASException {

		return this.getControladorContrato().obterContratoPontoConsumoPorCliente(idCliente);
	}

	/**
	 * Método responsável por consultar as faturas
	 * de acordo com os parametros informados.
	 *
	 * @param idCliente Chave primária do cliente
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param parametros Parametros para obter as
	 *            situações.
	 * @param chavesFatura the chaves fatura
	 * @return Coleção de Faturas.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> consultarFaturas(Long idCliente, Long idPontoConsumo, Map<String, Object[]> parametros, Long[] chavesFatura)
					throws GGASException {

		return this.getControladorFatura().consultarFaturas(idCliente, idPontoConsumo, parametros, chavesFatura, null, Boolean.FALSE);
	}

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar através do
	 * filtro informado.
	 *
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitos(Map<String, Object> filtro) throws GGASException {

		return this.getControladorCreditoDebito().consultarCreditosDebitos(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar através das
	 * chaves
	 * informadas.
	 *
	 * @param chavesCreditoDebito
	 *            Chaves primárias do crédito
	 *            débito a realizar
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosPorChaves(Object[] chavesCreditoDebito) throws NegocioException {

		return this.getControladorCreditoDebito().consultarCreditosDebitosPorChaves(chavesCreditoDebito);
	}

	/**
	 * Método responsável por obter os dados
	 * (Faturas/Notas de Débtos/Notas de
	 * Créditos/Débitos a
	 * Cobrar/Créditos a realizar)
	 * para exibição da tela de efetuar
	 * parcelamento de débitos.
	 *
	 * @param idCliente A chave primária do cliente.
	 * @param idPontoConsumo A chave primária do ponto de
	 *            consumo
	 * @return DadosParcelamento Os dados para
	 *         exibição na tela.
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws NoSuchMethodException the no such method exception
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosParcelamento obterDadosExibicaoEfetuarParcelamento(Long idCliente, Long idPontoConsumo) throws IllegalAccessException,
					InvocationTargetException, NoSuchMethodException, GGASException {

		return this.getControladorParcelamento().obterDadosExibicaoEfetuarParcelamento(idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por consultar os
	 * parcelamentos do cliente.
	 *
	 * @param idCliente Id do cliente
	 * @param idImovel Id do imóvel.
	 * @param dataInicial Data Inicial
	 * @param dataFinal Data Final
	 * @param statusParcelamento Status do Parcelamento
	 * @param chavesPrimarias the chaves primarias
	 * @return Lista de Parcelamentos.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Parcelamento> consultarParcelamento(Long idCliente, Long idImovel, Date dataInicial, Date dataFinal,
					Long statusParcelamento, Long[] chavesPrimarias) throws GGASException {

		return this.getControladorParcelamento().consultarParcelamentoCliente(idCliente, idImovel, dataInicial, dataFinal,
						statusParcelamento, chavesPrimarias);
	}

	/**
	 * Método responsável por validar os filtros
	 * que serão usados para pesquisar um
	 * parcelamento de
	 * débito.
	 *
	 * @param dataInicial Data Inicial
	 * @param dataFinal Data Final
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarFiltroPesquisaParcelamentoDebitos(String dataInicial, String dataFinal) throws GGASException {

		this.getControladorParcelamento().validarFiltroPesquisaParcelamentoDebitos(dataInicial, dataFinal);
	}

	/**
	 * Método responsável por gerar as parcelas.
	 *
	 * @param dados Um mapa contendo as informações
	 *            do parcelamento
	 * @param chavesFatura the chaves fatura
	 * @param chavesCreditoDebito the chaves credito debito
	 * @return the dados gerais parcelas
	 * @throws GGASException the GGAS exception
	 */
	public DadosGeraisParcelas gerarParcelas(Map<String, Object> dados, Long[] chavesFatura, Long[] chavesCreditoDebito)
					throws GGASException {

		return this.getControladorParcelamento().gerarParcelas(dados, chavesFatura, chavesCreditoDebito);
	}

	/**
	 * Método responsável por instanciar um VO de
	 * DadosGeraisParcelas.
	 *
	 * @return DadosGeraisParcelas
	 * @throws GGASException the GGAS exception
	 */
	public DadosGeraisParcelas criarDadosGeraisParcelas() throws GGASException {

		return (DadosGeraisParcelas) this.getControladorParcelamento().criarDadosGeraisParcelas();
	}

	/**
	 * metodo responsavel por retornar uma agencia
	 * do sistema.
	 *
	 * @param idAgencia chave primaria da agencia
	 * @return um entidade do sistema do tipo
	 *         Banco
	 * @throws GGASException the GGAS exception
	 */
	public Agencia obterAgenciaPorChave(Long idAgencia) throws GGASException {

		return this.getControladorArrecadacao().obterAgenciaPorChave(idAgencia);
	}

	/**
	 * Obter lista agencia.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Agencia> obterListaAgencia(Map<String, Object> filtro) throws GGASException {

		return this.getControladorArrecadacao().obterListaAgencia(filtro);
	}

	/**
	 * Método responsável por alterar um
	 * Recebimento no sistema.
	 *
	 * @param recebimento
	 *            Recebimento a ser inserido no
	 *            sistema.
	 * @return chavePrimeria Chave do Recebimento
	 *         inserido.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarRecebimento(Recebimento recebimento) throws GGASException {

		this.getControladorRecebimento().atualizarRecebimento(recebimento);
	}

	/**
	 * Método responsável por alterar um
	 * Recebimento no sistema.
	 *
	 * @param recebimento Recebimento a ser inserido no
	 *            sistema.
	 * @param chavesPrimariasFaturas the chaves primarias faturas
	 * @return chavePrimeria Chave do Recebimento
	 *         inserido.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void alterarRecebimentos(Recebimento recebimento, Long[] chavesPrimariasFaturas) throws GGASException {

		this.getControladorRecebimento().atualizarRecebimentos(recebimento, chavesPrimariasFaturas);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * recibo de quitação.
	 *
	 * @param listaRecebimentos
	 *            Lista de recebimentos.
	 * @param chavesFaturas
	 *            Chaves primárias das faturas.
	 * @return Um array bytes do relatório.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public byte[] gerarRelatorioReciboQuitacao(Collection<Recebimento> listaRecebimentos, Long[] chavesFaturas) throws NegocioException {

		return this.getControladorRecebimento().gerarRelatorioReciboQuitacao(listaRecebimentos, chavesFaturas);
	}

	/**
	 * Método responsável por salvar o
	 * parcelamento do débito.
	 *
	 * @param dadosGerais VO contendo as informações
	 *            necessárias para salvar o
	 *            parcelamento
	 * @param indicadorNotaPromissoria the indicador nota promissoria
	 * @param indicadorConfissaoDivida the indicador confissao divida
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] salvarParcelamento(DadosGeraisParcelas dadosGerais, boolean indicadorNotaPromissoria, boolean indicadorConfissaoDivida)
					throws GGASException {

		return this.getControladorParcelamento().salvarParcelamento(dadosGerais, indicadorNotaPromissoria, indicadorConfissaoDivida);
	}

	/**
	 * Método Para retornar os Parcelamentos
	 * vinculados a um determinado
	 * PerfilParcelamento.
	 *
	 * @param idPerfilParcelamento the id perfil parcelamento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Parcelamento> consultarParcelamentosVinculadosPerfilParcelamento(Long idPerfilParcelamento) throws NegocioException {

		return this.getControladorParcelamento().consultarParcelamentosVinculadosPerfilParcelamento(idPerfilParcelamento);
	}

	/**
	 * Validar valor devolucao.
	 *
	 * @param valorDevolucao the valor devolucao
	 * @throws GGASException the GGAS exception
	 */
	public void validarValorDevolucao(String valorDevolucao) throws GGASException {

		this.getControladorDevolucao().validarValorDevolucao(valorDevolucao);
	}

	/**
	 * Método responsável por obter um
	 * parcelamento pela chave passada pelo
	 * parâmetro.
	 *
	 * @param chavePrimaria Chave Primária do Parcelamento
	 * @return Parcelamento Um Parcelamento
	 *         GGASException Caso ocorra algum
	 *         erro na invocação do método.
	 * @throws GGASException the GGAS exception
	 */
	public Parcelamento obterParcelamento(long chavePrimaria) throws GGASException {

		return (Parcelamento) this.getControladorParcelamento().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter um
	 * parcelamento.
	 *
	 * @param chavePrimaria
	 *            A chave do parcelamento.
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            carregadas via lazy.
	 * @return Parcelamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Parcelamento obterParcelamento(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (Parcelamento) this.getControladorParcelamento().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsavel por verificar os
	 * parametros obrigatorios passados na
	 * pesquisa de
	 * recebimentos.
	 *
	 * @param filtro O filtro da tela de pesquisa
	 * @throws GGASException 
	 */
	public void validarFiltroPesquisaRecebimento(Map<String, Object> filtro) throws GGASException {

		this.getControladorRecebimento().validarFiltroPesquisaRecebimento(filtro);
	}

	/**
	 * Método responsavel por verificar os
	 * parametros obrigatorios passados na
	 * pesquisa de
	 * recebimentos.
	 *
	 * @param filtro O filtro da tela de pesquisa
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarFiltroPesquisaHistoricoAnomalia(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException {

		this.getControladorHistoricoAnomaliaFaturamento().validarFiltroPesquisaHistoricoAnomaliaFaturamento(filtro);
	}

	/**
	 * Método responsável por listar os itens de
	 * parcelamento pela chave do parcelamento.
	 *
	 * @param idParcelamento
	 *            Chave primária do parcelamento.
	 * @return Uma coleção de ParcelamentoItem.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ParcelamentoItem> listarParcelamentoItemPorChaveParcelamento(Long idParcelamento) throws GGASException {

		return this.getControladorParcelamento().listarParcelamentoItemPorChaveParcelamento(idParcelamento);
	}

	/**
	 * Retorna uma lista com os tipos de documento
	 * para a tela de Pesquisa de Recebimento.
	 *
	 * @return lista de tipos de documento
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TipoDocumento> listarTipoDocumentoRecebimento() throws GGASException {

		return getControladorArrecadacao().listarTipoDocumentoRecebimento();
	}

	/**
	 * Retorna todas as Faturas que pertencem a um
	 * Parcelamento.
	 *
	 * @param idParcelamento chave do Parcelamento
	 * @return Lista das Faturas que pertencem ao
	 *         parcelamento
	 * @throws GGASException caso ocorra algum erro durante
	 *             a invocação do método.
	 */
	public Collection<Fatura> consultarFaturasPorParcelamento(Long idParcelamento) throws GGASException {

		return getControladorFatura().consultarFaturasPorParcelamento(idParcelamento);
	}

	/**
	 * Retorna todas os CreditoDebitoARealizar que
	 * pertencem a um Parcelamento.
	 *
	 * @param idParcelamento chave do Parcelamento
	 * @return Lista dos CreditoDebitoARealizar
	 *         que pertencem ao parcelamento
	 * @throws GGASException caso ocorra algum erro durante
	 *             a invocação do método.
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizarPorParcelamento(Long idParcelamento) throws GGASException {

		return getControladorCreditoDebito().consultarCreditoDebitoARealizarPorParcelamento(idParcelamento);
	}

	/**
	 * Método responsável por obter uma Rubrica.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Uma Rubrica
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rubrica obterRubrica(long chavePrimaria) throws GGASException {

		return (Rubrica) this.getControladorRubrica().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter um
	 * CreditoDebitoARealizar.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Um CreditoDebitoARealizar
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CreditoDebitoSituacao obterCreditoDebitoSituacao(long chavePrimaria) throws GGASException {

		return this.getControladorCobranca().obterCreditoDebitoSituacao(chavePrimaria);
	}

	/**
	 * Método responsável por instanciar um VO do
	 * tipo DadosParcelas.
	 *
	 * @return the object
	 */
	public Object criarDadosParcelas() {

		return this.getControladorParcelamento().criarDadosParcelas();
	}

	/**
	 * Método responsável por gerar um relatório
	 * de nota promissória.
	 *
	 * @param idParcelamento the id parcelamento
	 * @param colecaoDadosParcelas the colecao dados parcelas
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioNotaPromissoria(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas) throws GGASException {

		return this.getControladorParcelamento().gerarRelatorioNotaPromissoria(idParcelamento, colecaoDadosParcelas);
	}

	/**
	 * Método responsável por gerar um relatório
	 * de confissão de dívidas.
	 *
	 * @param idParcelamento the id parcelamento
	 * @param colecaoDadosParcelas the colecao dados parcelas
	 * @param listaFatura the lista fatura
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioConfissaoDividas(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas,
					Collection<Fatura> listaFatura) throws GGASException {

		return this.getControladorParcelamento().gerarRelatorioConfissaoDividas(idParcelamento, colecaoDadosParcelas, listaFatura);
	}

	/**
	 * Método responsável por consultar as
	 * periodicidades de juros do tipo Entidade
	 * Conteudo do
	 * sistema.
	 *
	 * @param chavePrimariaEntidadeClasse the chave primaria entidade classe
	 * @return uma colecao de entidade conteudo
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> consultarPeriodicidadeJuros(Long chavePrimariaEntidadeClasse) throws GGASException {

		return this.getControladorProposta().consultarPeriodicidadeJuros(chavePrimariaEntidadeClasse);
	}

	/**
	 * Método responsável por retornar todos os
	 * pontos de consumo pertencentes a um
	 * determinado
	 * cliente.
	 *
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do
	 *         cliente
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorCliente(Long idCliente) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorCliente(idCliente);
	}

	/**
	 * Método responsável por retornar todos os
	 * pontos de consumo com contrato ativo pertencentes a um
	 * determinado
	 * cliente.
	 *
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do
	 *         cliente
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorContratoAtivoECliente(Long idCliente) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorContratoAtivoECliente(idCliente);
	}

	/**
	 * Método responsável por retornar todos os
	 * pontos de consumo pertencentes a um
	 * determinado
	 * cliente.
	 *
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do
	 *         cliente
	 * @throws GGASException the GGAS exception
	 */

	public Collection<PontoConsumo> listarPontoConsumoPorChaveCliente(Long idCliente) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorChaveCliente(idCliente);
	}

	/**
	 * Método responsável por retornar todos os
	 * pontos de consumo pertencentes a um
	 * determinado
	 * cliente
	 * e que estejam aptos para simulação do
	 * cálculo de fornecimento de gás;.
	 *
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do
	 *         cliente
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorClienteSimulacaoCalculo(Long idCliente) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoPorClienteSimulacaoCalculo(idCliente);
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoCalculo() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaTipoCalculo();
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaBaseApuracao() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaBaseApuracao();
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaUnidadeMonetaria() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaUnidadeMonetaria();
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @param filtro the filtro
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaFaixa> consultarTarifasVigenciaFaixa(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().consultarTarifasVigenciaFaixa(filtro);
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @param filtro the filtro
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<TarifaVigencia> consultarTarifasVigencia(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().consultarTarifasVigencia(filtro);
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @param filtro the filtro
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaDesconto> consultarTarifasVigenciaDesconto(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().consultarTarifasVigenciaDesconto(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * descontos de uma tarifa vigencia.
	 *
	 * @param idTarifaVigencia the id tarifa vigencia
	 * @return coleção de TarifaVigenciaDesconto
	 *         para a tarifa vigencia
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TarifaVigenciaDesconto> consultarTarifasVigenciaDescontoPorTarifaVigencia(long idTarifaVigencia) throws GGASException {

		return this.getControladorTarifa().consultarTarifasVigenciaDescontoPorTarifaVigencia(idTarifaVigencia);
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 *
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigencia> listarTarifasVigencia() throws GGASException {

		return this.getControladorTarifa().listarTarifasVigencia();
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tributos.
	 *
	 * @param filtro the filtro
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Tributo> consultarTributos(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTributo().consultarTributos(filtro);
	}

	/**
	 * Método responsável por obter o item de
	 * fatura.
	 *
	 * @param chaveEntidadeClasse the chave entidade classe
	 * @return Uma um item de fatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterItemFatura(Long chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterItemFatura(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por obter o item de
	 * fatura.
	 *
	 * @param chaveTributo the chave tributo
	 * @return Uma um item de fatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Tributo obterTributo(Long chaveTributo) throws GGASException {

		return this.getControladorTributo().obterTributo(chaveTributo);
	}

	/**
	 * Método responsável por obter o item de
	 * fatura.
	 *
	 * @param chaveEntidadeClasse the chave entidade classe
	 * @return Uma um item de fatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterUnidadeMonetaria(Long chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterUnidadeMonetaria(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por obter o item de
	 * fatura.
	 *
	 * @param chaveEntidadeClasse the chave entidade classe
	 * @return Uma um item de fatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeConteudo obterTipoCalculo(Long chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterTipoCalculo(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por obter a base de
	 * apuração.
	 *
	 * @param chaveEntidadeClasse the chave entidade classe
	 * @return Uma base de apuração.
	 * @throws GGASException the GGAS exception
	 */
	public EntidadeConteudo obterBaseApuracao(Long chaveEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().obterBaseApuracao(chaveEntidadeClasse);
	}

	/**
	 * Método responsável por obter a lsita das
	 * tarifas.
	 *
	 * @param filtro the filtro
	 * @return Uma coleção de tipos de parada.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Tarifa> consultarTarifas(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().consultarTarifas(filtro);
	}

	/**
	 * Método responsável por inserir uma tarifa
	 * no sistema.
	 *
	 * @param tarifa the tarifa
	 * @return chave primária da tarifa
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifa(Tarifa tarifa) throws GGASException {

		return this.getControladorTarifa().inserir(tarifa);
	}

	/**
	 * Método responsável por inserir uma tarifa.
	 *
	 * @param tarifa the tarifa
	 * @param tarifaVigencia the tarifa vigencia
	 * @param tarifaVigenciaDesconto the tarifa vigencia desconto
	 * @param listaDadosFaixa the lista dados faixa
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		return this.getControladorTarifa().inserirTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
	}

	/**
	 * Método responsável por inserir uma
	 * tarifaVigenciaDesconto no sistema.
	 *
	 * @param tarifaVigenciaDesconto the tarifa vigencia desconto
	 * @return chave primária da
	 *         tarifaVigenciaDesconto
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException {

		return this.getControladorTarifa().inserir(tarifaVigenciaDesconto);
	}

	/**
	 * Método responsável por inserir uma
	 * tarifaVigencia no sistema.
	 *
	 * @param tarifaVigencia the tarifa vigencia
	 * @return chave primária da tarifaVigencia
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifaVigencia(TarifaVigencia tarifaVigencia) throws GGASException {

		return this.getControladorTarifa().inserir(tarifaVigencia);
	}

	/**
	 * Método responsável por inserir uma
	 * tarifaVigencia no sistema.
	 *
	 * @param tarifaFaixaDesconto the tarifa faixa desconto
	 * @return chave primária da tarifaVigencia
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifaFaixaDesconto(TarifaFaixaDesconto tarifaFaixaDesconto) throws GGASException {

		return this.getControladorTarifa().inserir(tarifaFaixaDesconto);
	}

	/**
	 * Método responsável por inserir uma
	 * tarifaVigenciaFaixa no sistema.
	 *
	 * @param tarifaVigenciaFaixa the tarifa vigencia faixa
	 * @return chave primária da
	 *         tarifaVigenciaFaixa
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTarifaVigenciaFaixa(TarifaVigenciaFaixa tarifaVigenciaFaixa) throws GGASException {

		return this.getControladorTarifa().inserir(tarifaVigenciaFaixa);
	}

	// ECOELHO FIM

	/**
	 * Método responsável por remover tarifas
	 * selecionadas.
	 *
	 * @param chavesPrimarias As chaves dos perfis a serem
	 *            excluídos.
	 * @param auditoria the auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerTarifa(Long[] chavesPrimarias, DadosAuditoria auditoria) throws GGASException {

		this.getControladorTarifa().removerTarifa(chavesPrimarias, auditoria);
	}

	/**
	 * Método responsável por consultar as
	 * rubricas do sistema.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de rubricas do sistema.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Rubrica> consultarRubrica(Map<String, Object> filtro) throws GGASException {

		return getControladorRubrica().consultarRubrica(filtro);
	}

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * rubricas.
	 *
	 * @param chavesPrimarias
	 *            As chaves primárias das
	 *            rubricas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverRubricas(Long[] chavesPrimarias) throws GGASException {

		this.getControladorRubrica().validarRemoverRubrica(chavesPrimarias);
	}

	/**
	 * Método responsável por remover as rubricas
	 * selecionados.
	 *
	 * @param chavesPrimarias As chaves primárias das rubricas
	 *            selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerRubricas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorRubrica().removerRubrica(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * metodo responsavel por listar todos as
	 * entidades financiamento tipo do sistema.
	 *
	 * @return uma colecao de financiamento tipo
	 *         do sistema
	 * @throws GGASException the GGAS exception
	 */
	public Collection<FinanciamentoTipo> listarFinanciamentoTipo() throws GGASException {

		return getControladorRubrica().listarFinanciamentoTipo();
	}

	/**
	 * metodo responsavel por listar todas as
	 * entidades lancamento item contabil do
	 * sistema.
	 *
	 * @return uma coleção de item contabil do
	 *         sistema
	 * @throws GGASException the GGAS exception
	 */
	public Collection<LancamentoItemContabil> listarLancamentoItemContabil() throws GGASException {

		return getControladorRubrica().listarLancamentoItemContabil();
	}

	/**
	 * Método responsável por listar as unidades
	 * da classe quantidade.
	 *
	 * @return coleção de unidades de quantidade.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Unidade> listarUnidadesQuantidade() throws GGASException {

		return this.getControladorUnidade().listarUnidadesQuantidade();
	}

	/**
	 * Método responsável por cria uma Rubrica.
	 *
	 * @return the rubrica
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rubrica criarRubrica() throws GGASException {

		return (Rubrica) this.getControladorRubrica().criar();
	}

	/**
	 * Método responsável por inserir uma rubrica
	 * no sistema.
	 *
	 * @param rubrica
	 *            A rubrica a ser inserida.
	 * @return chavePrimeria da rubrica inserida.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirRubrica(Rubrica rubrica) throws GGASException {

		return this.getControladorRubrica().inserirRubrica(rubrica);
	}

	/**
	 * Método responsável por obter o tipo de
	 * financiamento (rubrica).
	 *
	 * @param chavePrimaria A chave primaria
	 * @return o tipo de financiamento (rubrica)
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FinanciamentoTipo obterTipoFinanciamento(long chavePrimaria) throws GGASException {

		return getControladorRubrica().obterTipoFinanciamento(chavePrimaria);
	}

	/**
	 * Método responsável por obter o Item de
	 * Lançamento Contabil.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return o Item de Lançamento Contabil
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public LancamentoItemContabil obterLancamentoItemContabil(long chavePrimaria) throws GGASException {

		return getControladorRubrica().obterLancamentoItemContabil(chavePrimaria);
	}

	/**
	 * Método responsável por Filtrar tarifas.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaPesquisar> obterTarifaVigenciaPesquisar(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().obterTarifaVigenciaPesquisar(filtro);
	}

	/**
	 * Criar rubrica valor regulamentado.
	 *
	 * @return the rubrica valor regulamentado
	 * @throws GGASException the GGAS exception
	 */
	public RubricaValorRegulamentado criarRubricaValorRegulamentado() throws GGASException {

		return this.getControladorRubrica().criarRubricaValorRegulamentado();
	}

	/**
	 * Criar rubrica tributo.
	 *
	 * @return the rubrica tributo
	 * @throws GGASException the GGAS exception
	 */
	public RubricaTributo criarRubricaTributo() throws GGASException {

		return this.getControladorRubrica().criarRubricaTributo();
	}

	/**
	 * Obter rubrica.
	 *
	 * @param chave the chave
	 * @return the rubrica
	 * @throws GGASException the GGAS exception
	 */
	public Rubrica obterRubrica(Long chave) throws GGASException {

		return (Rubrica) getControladorRubrica().obter(chave);
	}

	/**
	 * Obter rubrica.
	 *
	 * @param chave the chave
	 * @param propriedadesLazy the propriedades lazy
	 * @return the rubrica
	 * @throws GGASException the GGAS exception
	 */
	public Rubrica obterRubrica(Long chave, String... propriedadesLazy) throws GGASException {

		return (Rubrica) getControladorRubrica().obter(chave, propriedadesLazy);
	}

	/**
	 * Consultar rubricas tributo.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<RubricaTributo> consultarRubricasTributo(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRubrica().consultarRubricasTributo(filtro);
	}

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param tarifaVigenciaParametro the tarifa vigencia parametro
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param isConsumoDrawback the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws GGASException the GGAS exception
	 */
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, TarifaVigencia tarifaVigenciaParametro,
					boolean isConsiderarDescontos, boolean isFaturaAvulso, Boolean isConsumoDrawback) throws GGASException {

		return this.getControladorCalculoFornecimentoGas().calcularValorFornecimentoGas(itemFatura, dataInicio, dataFim, consumoApurado,
						colecaoPontoConsumo, tarifaVigenciaParametro, isConsiderarDescontos, isFaturaAvulso, null, isConsumoDrawback);
	}

	/**
	 * Calcular valor fornecimento gas para testes.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param isConsumoDrawback the is consumo drawback
	 * @param valorInformado the valor informado
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String calcularValorFornecimentoGasParaTestes(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, boolean isConsiderarDescontos,
					boolean isFaturaAvulso, Boolean isConsumoDrawback, String valorInformado) throws GGASException {

		this.getControladorCalculoFornecimentoGas().calcularValorFornecimentoGas(itemFatura, dataInicio, dataFim, consumoApurado,
						colecaoPontoConsumo, isConsiderarDescontos, null, isFaturaAvulso, null, isConsumoDrawback, valorInformado, null);
		return this.getControladorCalculoFornecimentoGas().getLog();
	}

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param isConsumoDrawback the is consumo drawback
	 * @param valorInformado the valor informado
	 * @return the dados fornecimento gas vo
	 * @throws GGASException the GGAS exception
	 */
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, boolean isConsiderarDescontos,
					boolean isFaturaAvulso, Boolean isConsumoDrawback, String valorInformado) throws GGASException {

		return this.getControladorCalculoFornecimentoGas().calcularValorFornecimentoGas(itemFatura, dataInicio, dataFim, consumoApurado,
						colecaoPontoConsumo, isConsiderarDescontos, null, isFaturaAvulso, null, isConsumoDrawback, valorInformado, null);
	}

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param tarifa the tarifa
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param isConsumoDrawback the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws GGASException the GGAS exception
	 */
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Tarifa tarifa, boolean isConsiderarDescontos, boolean isFaturaAvulso,
					Boolean isConsumoDrawback) throws GGASException {

		return this.getControladorCalculoFornecimentoGas().calcularValorFornecimentoGas(itemFatura, dataInicio, dataFim, consumoApurado,
						tarifa, isConsiderarDescontos, isFaturaAvulso, null, isConsumoDrawback);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 *
	 * @param chavePrimariaHistoricoConsumo the chave primaria historico consumo
	 * @return Uma coleção de histórico
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoConsumo obterHistoricoConsumo(Long chavePrimariaHistoricoConsumo) throws GGASException {

		return getControladorControladorHistoricoConsumo().obterHistoricoConsumo(chavePrimariaHistoricoConsumo);
	}

	/**
	 * Obter historico consumo todos.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param indicadorFaturamento the indicador faturamento
	 * @param indicadorConsumoCiclo the indicador consumo ciclo
	 * @param indicadorUltimo the indicador ultimo
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<HistoricoConsumo> obterHistoricoConsumoTodos(Long idPontoConsumo, boolean indicadorFaturamento,
					boolean indicadorConsumoCiclo, boolean indicadorUltimo) throws NegocioException {

		return getControladorControladorHistoricoConsumo().obterHistoricoConsumoTodos(idPontoConsumo, indicadorFaturamento,
						indicadorConsumoCiclo, indicadorUltimo);
	}

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 *
	 * @param chavePrimariaHistoricoAnomalia the chave primaria historico anomalia
	 * @return Uma coleção de histórico
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoAnomaliaFaturamento obterHistoricoAnomaliaFaturamento(Long chavePrimariaHistoricoAnomalia) throws GGASException {

		return getControladorHistoricoAnomaliaFaturamento().obterHistoricoAnomaliaFaturamento(chavePrimariaHistoricoAnomalia);
	}

	/**
	 * Método responsável por obter um Credito
	 * Débito Negociado.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the credito debito negociado
	 * @throws GGASException the GGAS exception
	 */
	public CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria) throws GGASException {

		return this.getControladorCreditoDebito().obterCreditoDebitoNegociado(chavePrimaria);
	}

	/**
	 * Método responsável por obter um Credito
	 * Débito Negociado.
	 *
	 * @param chavePrimaria
	 *            Chave primária do
	 *            CreditoDebitoNegociado.
	 * @param propriedadesLazy
	 *            Propriedades que serão
	 *            carregadas.
	 * @return Um CreditoDebitoNegociado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return this.getControladorCreditoDebito().obterCreditoDebitoNegociado(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por obter um Credito
	 * Débito Detalhamento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the credito debito detalhamento
	 * @throws GGASException the GGAS exception
	 */
	public CreditoDebitoDetalhamento obterCreditoDebitoDetalhamento(long chavePrimaria) throws GGASException {

		return this.getControladorCreditoDebito().obterCreditoDebitoDetalhamento(chavePrimaria);
	}

	/**
	 * Método responsável por remover Crédito(s)
	 * Débito(s) a Realizar.
	 *
	 * @param chavesPrimarias As chaves primárias das
	 *            propostas selecionados.
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerCreditosDebitosARealizar(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorCreditoDebito().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por criar um
	 * CreditoDebitoARealizar.
	 *
	 * @return Um CreditoDebitoARealizar.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CreditoDebitoARealizar criarCreditoDebitoARealizar() throws GGASException {

		return (CreditoDebitoARealizar) this.getControladorCreditoDebito().criar();
	}

	/**
	 * Método responsável por criar um
	 * CreditoDebitoNegociado.
	 *
	 * @return Um CreditoDebitoARealizar.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public CreditoDebitoNegociado criarCreditoDebitoNegociado() throws GGASException {

		return (CreditoDebitoNegociado) this.getControladorCreditoDebito().criarCreditoDebitoNegociado();
	}

	/**
	 * Método responsável por consultar
	 * CreditoDebitoARealizar.
	 *
	 * @param filtro
	 *            Filtro com os parâmetros da
	 *            pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoDetalhamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamento(Map<String, Object> filtro) throws GGASException {

		return this.getControladorCreditoDebito().consultarCreditoDebitoDetalhamento(filtro);
	}

	/**
	 * Método responsável por obter uma coleçao de
	 * credito debito detalhamento pelo credito
	 * debito a
	 * realizar.
	 *
	 * @param idCreditoDebitoARealizar the id credito debito a realizar
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar)
					throws GGASException {

		return this.getControladorCreditoDebito().consultarCreditoDebitoDetalhamentoPeloCDARealizar(idCreditoDebitoARealizar);
	}

	/**
	 * Método responsável por listar Tarifas na
	 * tela de Incluir Créditos Débitos a Realizar.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Rubrica> listarRubricas() throws GGASException {

		return this.getControladorRubrica().listarRubricas();
	}

	/**
	 * Listar rubricas em uso sistema.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Rubrica> listarRubricasEmUsoSistema() throws GGASException {

		return this.getControladorRubrica().listarRubricasEmUsoSistema();
	}

	/**
	 * Método responsável por listar Crédito
	 * Origem.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CreditoOrigem> listarCreditoOrigem() throws GGASException {

		return this.getControladorCreditoDebito().listarCreditoOrigem();
	}

	/**
	 * Obter credito origem.
	 *
	 * @param idCreditoOrigem the id credito origem
	 * @return the credito origem
	 * @throws NegocioException the negocio exception
	 */
	public CreditoOrigem obterCreditoOrigem(Long idCreditoOrigem) throws NegocioException {

		return this.getControladorCreditoDebito().obterCreditoOrigem(idCreditoOrigem);
	}

	/**
	 * Método responsável por listar Periodicidade
	 * Cobrança.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaPeriodicidadeCobranca() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaPeriodicidadeCobranca();
	}

	/**
	 * Método responsável por listar Periodicidade
	 * Juros.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaPeriodicidadeJuros() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaPeriodicidadeJuros();
	}

	/**
	 * Método responsável por gerar as parcelas de
	 * crédito de débito.
	 *
	 * @param dados Dados da tela para a geração das
	 *            parcelas.
	 * @param indicadorDetalhamento the indicador detalhamento
	 * @return DadosGeraisParcelas.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosGeraisParcelas gerarParcelasCreditoDebito(Map<String, Object> dados, boolean indicadorDetalhamento) throws GGASException {

		return this.getControladorCreditoDebito().gerarParcelasCreditoDebito(dados, indicadorDetalhamento);
	}

	/**
	 * Método responsável por listar Dias após
	 * Início Cobrança.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaDiasAposInicioCobranca() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaDiasAposInicioCobranca();
	}

	/**
	 * Método responsável por persistir as
	 * parcelas de um crédito/débito a realizar.
	 *
	 * @param dados the dados
	 * @param dadosGerais the dados gerais
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] salvarCreditoDebitoARealizar(Map<String, Object> dados, DadosGeraisParcelas dadosGerais) throws GGASException {

		return this.getControladorCreditoDebito().salvarCreditoDebitoARealizar(dados, dadosGerais);
	}

	/**
	 * Método responsável por obter o
	 * creditoDebitoDetalhamento pela chave
	 * primária do
	 * creditoDebitoARealizar.
	 *
	 * @param idCreditoDebitoARealizar
	 *            Chave primária do
	 *            creditoDebitoARealizar.
	 * @return Uma coleção de
	 *         CreditoDebitoDetalhamento.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoDetalhamento> obterCDDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar) throws GGASException {

		return this.getControladorCreditoDebito().obterCDDetalhamentoPeloCDARealizar(idCreditoDebitoARealizar);
	}

	/**
	 * Método responsável por cancelar créditos e
	 * débitos.
	 *
	 * @param chavesCreditoDebito Chave primária dos
	 *            créditos/débitos a serem
	 *            cancelados.
	 * @param idMovitoCancelamento the id movito cancelamento
	 * @param dadosAuditoria Dados de auditoria.
	 * @throws GGASException Caso ocorra algume erro na
	 *             invocação do método.
	 */
	public void cancelarLancamentoCreditoDebito(Long[] chavesCreditoDebito, Long idMovitoCancelamento, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorCreditoDebito().cancelarLancamentoCreditoDebito(chavesCreditoDebito, idMovitoCancelamento, dadosAuditoria);
	}

	/**
	 * Método responsável por obter a lista de
	 * motivos de cancelamento de fatura.
	 *
	 * @return Uma coleção de motivos de
	 *         cancelamento de fatura.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaMotivoCancelamento() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaMotivoCancelamento();
	}

	/**
	 * Método responsável por consultar os
	 * CreditoDebitoNegociado.
	 *
	 * @param filtro
	 *            Filtro da pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoNegociado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoNegociado> consultarCreditoDebitoNegociado(Map<String, Object> filtro) throws GGASException {

		return this.getControladorCreditoDebito().consultarCreditoDebitoNegociado(filtro);
	}

	/**
	 * Método responsável por consultar os
	 * CreditoDebitoARealizar.
	 *
	 * @param filtro
	 *            Filtro da pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoARealizar.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizar(Map<String, Object> filtro) throws GGASException {

		return this.getControladorCreditoDebito().consultarCreditoDebitoARealizar(filtro);
	}

	/**
	 * Método responsável por obter o o saldo
	 * devedor de um CreditoDebitoNegociado.
	 *
	 * @param idCreditoDebitoNegociado
	 *            Chave primária do
	 *            CreditoDebitNegociado.
	 * @return O valor do saldo devedor do
	 *         CreditoDebitoNegociado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal obterSaldoDevedorCreditoDebitoNegociado(Long idCreditoDebitoNegociado) throws GGASException {

		return this.getControladorCreditoDebito().obterSaldoDevedorCreditoDebitoNegociado(idCreditoDebitoNegociado);
	}

	/**
	 * Método responsável por obter uma FaturaItem
	 * pela chave primária do
	 * CreditoDebitoARealizar.
	 *
	 * @param idCreditoDebitoARealizar
	 *            Chave primária do
	 *            CreditoDebitoARealizar.
	 * @return Uma FaturaItem.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaturaItem obterFaturaItemPorCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws GGASException {

		return this.getControladorFatura().obterFaturaItemPorCreditoDebitoARealizar(idCreditoDebitoARealizar);
	}

	/**
	 * Consultar rubricas valor regulamentado.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<RubricaValorRegulamentado> consultarRubricasValorRegulamentado(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorRubrica().consultarRubricasValorRegulamentado(filtro);
	}

	/**
	 * Atualizar rubrica.
	 *
	 * @param rubrica the rubrica
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarRubrica(Rubrica rubrica) throws NegocioException {

		this.getControladorRubrica().atualizarRubrica(rubrica);
	}

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de valores.
	 *
	 * @return Uma coleção de tipos de valores
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoValorTarifa() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoValorTarifa();
	}

	/**
	 * Método responsável por gerar os índices
	 * financeiros para o periodo de datas
	 * desejado ou
	 * retornar os já
	 * existentes, caso exista algum no período.
	 *
	 * @param dataInicio
	 *            Data de ínicio do período.
	 * @param dataFim
	 *            Data de fim do período.
	 * @param idIndice
	 *            Chave primária do índice
	 *            Financeiro.
	 * @return Uma coleção de
	 *         IndicaFinanceiroValorNominal.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<IndiceFinanceiroValorNominal> gerarIndicesFinanceiros(Date dataInicio, Date dataFim, Long idIndice)
					throws GGASException {

		return this.getControladorTarifa().gerarIndicesFinanceiros(dataInicio, dataFim, idIndice);
	}

	/**
	 * Método responsável por inserir novos
	 * índices financeiro e remover e atualizar os
	 * já
	 * existentes.
	 *
	 * @param datasReferencia
	 *            Array com as datas dos índices
	 *            financeiros.
	 * @param mesDatasReferencia
	 *            Array com os meses dos índices
	 *            financeiros.
	 * @param valoresNominais
	 *            Array com valores dos índices
	 *            financeiros.
	 * @param indicePopulado
	 *            IndiceFinanceiroValorNominal já
	 *            populado que será usado para
	 *            popular os demais.
	 * @param indicadorUtilizado
	 *            Array com indicador de o índice
	 *            está sendo usado.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarIndicesFinanceiros(String[] datasReferencia, String[] mesDatasReferencia, String[] valoresNominais,
					IndiceFinanceiroValorNominal indicePopulado, String[] indicadorUtilizado) throws GGASException {

		this.getControladorTarifa().atualizarIndicesFinanceiros(datasReferencia, mesDatasReferencia, valoresNominais, indicePopulado,
						indicadorUtilizado);
	}

	/**
	 * Método responsável por criar um
	 * IndiceFinanceiroValorNominal.
	 *
	 * @return IndiceFinanceiroValorNominal
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public IndiceFinanceiroValorNominal criarIndiceFinanceiroValorNominal() throws GGASException {

		return (IndiceFinanceiroValorNominal) this.getControladorTarifa().criarIndiceFinanceiroValorNominal();
	}

	/**
	 * Obter contrato ponto consumo item faturamento.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @return the contrato ponto consumo item faturamento
	 * @throws NegocioException the negocio exception
	 */
	public ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamento(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException {

		return this.getControladorParcelamento().obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);
	}

	/**
	 * Listar tarifas por ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param itemFatura the item fatura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Tarifa> listarTarifasPorPontoConsumo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura) throws NegocioException {

		return getControladorCalculoFornecimentoGas().listarTarifasPorPontoConsumo(pontoConsumo, null, itemFatura);
	}

	/**
	 * Exibir erro tarifa.
	 *
	 * @param erro the erro
	 * @throws GGASException the GGAS exception
	 */
	public void exibirErroTarifa(String erro) throws GGASException {

		this.getControladorTarifa().exibirErroTarifa(erro);
	}

	/**
	 * Método responsável por instanciar uma
	 * Fatura.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Fatura> consultarFatura(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFatura().consultarFatura(filtro);
	}

	/**
	 * Método responsável por trazer uma coleção de faturas.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Fatura> consultarFaturaPesquisa(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFatura().consultarFaturaPesquisa(filtro);
	}

	/**
	 * Método responsável por listar todas as
	 * situações de crédito débito.
	 *
	 * @return Uma coleção de
	 *         CreditoDebitoSituacao.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoSituacao> listarCreditoDebitoSituacao() throws GGASException {

		return this.getControladorCreditoDebito().listarCreditoDebitoSituacao();
	}

	/**
	 * Método responsável por instanciar uma
	 * FaturaItem.
	 *
	 * @return Uma FaturaItem.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public FaturaItem criarFaturaItem() throws GGASException {

		return (FaturaItem) this.getControladorFatura().criarFaturaItem();
	}

	/**
	 * Método responsável por inserir uma fatura.
	 *
	 * @param fatura
	 *            Fatura a ser inserida.
	 * @return A chave primária da fatura
	 *         inserida.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Long inserirFatura(Fatura fatura) throws GGASException {

		return this.getControladorFatura().inserir(fatura);
	}

	/**
	 * Método responsável por verificar se o ponto
	 * de consumo ou o tipo de nota foi
	 * selecionado.
	 *
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param indicadorCreditoDebito Indicador para saber se é nota
	 *            de crédito ou nota de débito.
	 * @param idCliente the id cliente
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarFiltroExibicaoNotasDebitoCredito(Long idPontoConsumo, String indicadorCreditoDebito, Long idCliente)
					throws GGASException {

		this.getControladorFatura().validarFiltroExibicaoNotasDebitoCredito(idPontoConsumo, indicadorCreditoDebito, idCliente);
	}

	/**
	 * Método responsável por verificar se as
	 * faturas selecionadas poderão ter suas datas
	 * de
	 * vencimento alteradas.
	 *
	 * @param chavesPrimarias
	 *            As chaves das faturas
	 *            selecionadas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarFaturasAlteracaoVencimento(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFatura().verificarFaturasAlteracaoVencimento(chavesPrimarias);
	}

	/**
	 * Método responsável por alterar a(s) data(s)
	 * de vencimento da(s) Fatura(s).
	 *
	 * @param parametros Mapa com os parametros para
	 *            alterar a(s) data(s) de
	 *            venciemnto da(s) Fatura(s).
	 * @param dadosAuditoria Dados da auditoria.
	 * @return faturas para exibição(pdf).
	 * @throws GGASException the GGAS exception
	 */
	public byte[] atualizarVencimentoFatura(Map<String, Object> parametros, DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorFatura().atualizarVencimentoFatura(parametros, dadosAuditoria);
	}

	/**
	 * Método responsável por verificar se as
	 * faturas a serem revisadas tem o mesmo
	 * motivo.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return true, caso exista motivo de revisao
	 *         nas faturas selecionadas
	 * @throws GGASException the GGAS exception
	 */
	public boolean validarRevisaoFatura(Long[] chavesPrimarias) throws GGASException {

		return this.getControladorFatura().validarRevisaoFatura(chavesPrimarias);
	}

	/**
	 * Método responsável por listar rubricas
	 * levando em consideração o tipo de
	 * Crédito/Débito.
	 *
	 * @param isTipoDebito the is tipo debito
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Rubrica> listarRubricasPorTipoCreditoDebito(boolean isTipoDebito) throws NegocioException {

		return this.getControladorRubrica().processarRubricasPortipoCreditoDebito(isTipoDebito);
	}

	/**
	 * Consultar preco gas.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PrecoGas> consultarPrecoGas(Map<String, Object> filtro) throws GGASException {

		return this.getControladorPrecoGas().consultarPrecoGas(filtro);
	}

	/**
	 * Método responsável por listar os Motivos de
	 * revisão de fatura.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<FaturaMotivoRevisao> listarMotivoRevisaoFatura() throws GGASException {

		return this.getControladorFatura().listarMotivoRevisaoFatura();
	}

	/**
	 * Método responsável por revisar ou tirar de
	 * revisão um grupo de faturas.
	 *
	 * @param chavesFaturas the chaves faturas
	 * @param idMotivoRevisao the id motivo revisao
	 * @param existeMotivoRevisao the existe motivo revisao
	 * @param dadosAuditoria the dados auditoria
	 * @param idFuncionario the id funcionario
	 * @throws GGASException the GGAS exception
	 */
	public void revisarFatura(Long[] chavesFaturas, Long idMotivoRevisao, boolean existeMotivoRevisao, DadosAuditoria dadosAuditoria,
					Long idFuncionario) throws GGASException {

		this.getControladorFatura().revisarFatura(chavesFaturas, idMotivoRevisao, existeMotivoRevisao, dadosAuditoria, idFuncionario);
	}

	/**
	 * Método responsável por remover tarifas
	 * selecionadas.
	 *
	 * @param chavesPrimarias As chaves dos perfis a serem
	 *            excluídos.
	 * @param auditoria the auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerPrecoGas(Long[] chavesPrimarias, DadosAuditoria auditoria) throws GGASException {

		this.getControladorPrecoGas().removerPrecoGas(chavesPrimarias, auditoria);
	}

	/**
	 * Método responsável por obter a lista de
	 * datas de referência cambial.
	 *
	 * @return coleção de datas de referência
	 *         cambial
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaDataReferenciaCambial() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaDataReferenciaCambial();
	}

	/**
	 * Método responsável por obter a lista dias
	 * da cotação.
	 *
	 * @return coleção de dias da cotação
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaDiaCotacao() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaDiaCotacao();
	}

	/**
	 * Método responsável por verificar se uma
	 * determinada tarifa esta associada
	 * ao Dolar.
	 *
	 * @param chavePrimariaTarifa A chave primaria da tarifa para
	 *            verificar se está relacionada ao
	 *            Dolar
	 * @return true, if successful
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public boolean verificarUnidadeMonetariaTarifaVigencia(long chavePrimariaTarifa) throws NegocioException {

		return this.getControladorTarifa().verificarUnidadeMonetariaTarifaVigencia(chavePrimariaTarifa);
	}

	/**
	 * Método responsável por validar a data de
	 * referência e o dia da cotação.
	 *
	 * @param verificarDataReferenciaDiaCotacao the verificar data referencia dia cotacao
	 * @param objetoDataReferenciaCambial the objeto data referencia cambial
	 * @param objetoDiaCotacao the objeto dia cotacao
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDataReferenciaDiaCotacao(boolean verificarDataReferenciaDiaCotacao, EntidadeConteudo objetoDataReferenciaCambial,
					EntidadeConteudo objetoDiaCotacao) throws NegocioException {

		this.getControladorContrato().validarDataReferenciaDiaCotacao(verificarDataReferenciaDiaCotacao, objetoDataReferenciaCambial,
						objetoDiaCotacao);
	}

	/**
	 * Validar filtro pesquisa fatura.
	 *
	 * @param filtro the filtro
	 * @throws GGASException the GGAS exception
	 */
	public void validarFiltroPesquisaFatura(Map<String, Object> filtro) throws GGASException {

		this.getControladorFatura().validarFiltroPesquisaFatura(filtro);
	}

	/**
	 * Método responsável por validar os dados
	 * obrigatórios para a inclusão de nota de
	 * débito e
	 * crédito.
	 *
	 * @param fatura Nota de débito e crédito.
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosNotaDebito(Fatura fatura) throws GGASException {

		this.getControladorFatura().validarDadosNotaDebito(fatura);
	}

	/**
	 * Método responsável por conciliar notas de
	 * débito e notas de crédito.
	 *
	 * @param chavesNotasDebito
	 *            Chaves primárias das notas de
	 *            débito.
	 * @param chavesNotasCredito
	 *            Chaves primárias das notas de
	 *            crédito.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void conciliarNotasDebitoCredito(Long[] chavesNotasDebito, Long[] chavesNotasCredito, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorFatura().conciliarNotasDebitoCredito(chavesNotasDebito, chavesNotasCredito, dadosAuditoria);
	}

	/**
	 * Método responsável por inserir uma nota de
	 * débito ou crédito.
	 *
	 * @param notaDebitoCredito Nota de débito ou de crédito a
	 *            ser inserida.
	 * @param chavesPrimarias Chaves primárias das notas de
	 *            débito ou crédito.
	 * @param chaveApuracaoPenalidade the chave apuracao penalidade
	 * @return Array com o relatório da nota
	 *         inserida, caso seja nota de débito
	 *         e a chave primária
	 *         da nota.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Pair<byte[], Fatura> inserirNotaDebitoCredito(Fatura notaDebitoCredito, Long[] chavesPrimarias, Long chaveApuracaoPenalidade)
					throws GGASException {

		return this.getControladorFatura().inserirNotaDebitoCredito(notaDebitoCredito, chavesPrimarias, chaveApuracaoPenalidade);
	}

	/**
	 * Cria um débito de multa recisória com o
	 * valor informado para um dado contrato.
	 * Caso já exista um débito de multa recisória
	 * para esse contrato ele será cancelado antes
	 * da
	 * inclusão do novo débito.
	 *
	 * @param idFaturaItem the id fatura item
	 * @return the rubrica
	 * @throws GGASException public Object[]
	 *             incluirDebitoMultaRecisao
	 *             (Fatura notaDebitoCredito)
	 *             throws
	 *             GGASException{
	 *             return
	 *             this.getControladorFatura
	 *             ().incluirDebitoMultaRecisao
	 *             (notaDebitoCredito);
	 *             }
	 */

	/**
	 * Método responsável por obter a Rubrica de
	 * um FaturaItem.
	 *
	 * @param idFaturaItem
	 *            Chave primaria da FaturaItem.
	 * @return Uma Rubrica.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Rubrica obterRubricaPorFaturaItem(Long idFaturaItem) throws GGASException {

		return this.getControladorRubrica().obterRubricaPorFaturaItem(idFaturaItem);
	}

	/**
	 * Método responsável por consultar as
	 * FaturaConciliacao pela chave primária do
	 * débito ou do
	 * crédito.
	 *
	 * @param idDebitoCredito
	 *            Chave primária do débito ou do
	 *            crédito.
	 * @return Uma lista de FaturaConciliacao.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<FaturaConciliacao> consultarFaturaConciliacao(Long idDebitoCredito) throws GGASException {

		return this.getControladorFatura().consultarFaturaConciliacao(idDebitoCredito);
	}

	/**
	 * Método responsável por gerar os relatório
	 * de extrato de débito.
	 *
	 * @param chavesFatura Chaves primárias das faturas.
	 * @param chavesCreditoDebito Chaves primárias dos créditos e
	 *            débitos.
	 * @param idCliente Chave primária do cliente.
	 * @param documentoCobranca Documento de cobrança a ser
	 *            populado e inserido.
	 * @return Um array de bytes com o relatório
	 *         gerado.
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarExtratoDebito(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente, DocumentoCobranca documentoCobranca)
			throws GGASException {

		return this.getControladorCobranca().gerarExtratoDebito(chavesFatura, chavesCreditoDebito, idCliente, documentoCobranca);
	}

	/**
	 * Método responsável por cancelar notas de
	 * débito e notas de crédito.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @param idMotivoCancelamento
	 *            Chave primária do motivo de
	 *            cancelamento.
	 * @param descricaoCancelamento
	 *            Complemento do motivo de
	 *            cancelamento.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void cancelarNotaDebitoCredito(Long[] chavesPrimarias, Long idMotivoCancelamento, String descricaoCancelamento,
					DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorFatura().cancelarNotaDebitoCredito(chavesPrimarias, idMotivoCancelamento, descricaoCancelamento, dadosAuditoria);
	}

	/**
	 * Método responsável por validar se o ponto
	 * de consumo possui um contrato ativo.
	 *
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarPontoConsumoSelecionado(Long idPontoConsumo) throws GGASException {

		this.getControladorFatura().validarPontoConsumoSelecionado(idPontoConsumo);
	}

	/**
	 * Método responsável por verificar se as
	 * notas de débito / crédito selecionadas
	 * possuem
	 * situação de pagamento 'Paga'.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @throws GGASException
	 *             Caso algum nota possua situação
	 *             de pagamento 'Paga'.
	 */
	public void validarSituacaoNotasConciliacao(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFatura().validarSituacaoNotasConciliacao(chavesPrimarias);
	}

	/**
	 * Método responsável por validar seleção de
	 * notas de débito / crédito.
	 *
	 * @param chavesPrimarias Chaves primárias das notas,
	 *            idCliente e idPontoConsumo.
	 * @param idCliente the id cliente
	 * @param idPontoConsumo the id ponto consumo
	 * @throws GGASException se filtros pontoConsumo e
	 *             cliente não informado e seleção
	 *             de mais de um item da
	 *             pesquisa.
	 */
	public void validarSelecaoNotas(Long[] chavesPrimarias, Long idCliente, Long idPontoConsumo) throws GGASException {

		this.getControladorFatura().validarSelecaoNotas(chavesPrimarias, idCliente, idPontoConsumo);
	}

	/**
	 * Método responsável por validar a seleção
	 * das notas de débito e crédito.
	 *
	 * @param chavesNotasDebitos
	 *            Chaves primárias das notas de
	 *            débito.
	 * @param chavesNotasCredito
	 *            Chaves primárias das notas de
	 *            crédito.
	 * @throws GGASException
	 *             Caso nenhuma nota de débito ou
	 *             crédito tenha sido selecionada
	 *             ou só um tipo de
	 *             nota tenha sido selecionada.
	 */
	public void validarNotasSelecionadas(Long[] chavesNotasDebitos, Long[] chavesNotasCredito) throws GGASException {

		this.getControladorFatura().validarNotasSelecionadas(chavesNotasDebitos, chavesNotasCredito);
	}

	/**
	 * Método responsável por calcular o saldo da
	 * fatura informada, levando em consideração o
	 * recebimento e o valor concilidado.
	 *
	 * @param fatura the fatura
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal calcularSaldoFatura(Fatura fatura) throws GGASException {

		return this.getControladorFatura().calcularSaldoFatura(fatura);
	}

	/**
	 * Calcular saldo fatura.
	 *
	 * @param operacaoGgas the operacao ggas
	 * @param fatura the fatura
	 * @param corrigirSaldo the corrigir saldo
	 * @param adicionarImpontualidade the adicionar impontualidade
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	public Pair<BigDecimal, BigDecimal> calcularSaldoFatura(boolean operacaoGgas, Fatura fatura, boolean corrigirSaldo,
					boolean adicionarImpontualidade) throws GGASException {

		return this.getControladorFatura().calcularSaldoFatura(operacaoGgas, fatura, corrigirSaldo, adicionarImpontualidade);
	}

	/**
	 * Método responsável por calcular o saldo da
	 * fatura (first) e o saldo corrigido (second), levando em consideração o
	 * recebimento, o valor concilidado e a correcao monetaria.
	 *
	 * @param fatura the fatura
	 * @param corrigirSaldo the corrigir saldo
	 * @param adicionarImpontualidade the adicionar impontualidade
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	public Pair<BigDecimal, BigDecimal> calcularSaldoFatura(Fatura fatura, boolean corrigirSaldo, boolean adicionarImpontualidade)
					throws GGASException {

		return this.getControladorFatura().calcularSaldoFatura(fatura, corrigirSaldo, adicionarImpontualidade);
	}

	/**
	 * Calcular saldo fatura.
	 *
	 * @param fatura the fatura
	 * @param dataFinalCalculo the data final calculo
	 * @param corrigirSaldo the corrigir saldo
	 * @param adicionarImpontualidade the adicionar impontualidade
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	public Pair<BigDecimal, BigDecimal> calcularSaldoFatura(Fatura fatura, Date dataFinalCalculo, boolean corrigirSaldo,
					boolean adicionarImpontualidade) throws GGASException {

		return this.getControladorFatura().calcularSaldoFatura(fatura, dataFinalCalculo, corrigirSaldo, adicionarImpontualidade);
	}

	/**
	 * Método responsável por calcular o saldo da
	 * fatura (first) e o saldo corrigido (second), levando em consideração o
	 * recebimento, o valor concilidado e a correcao monetaria ate uma data
	 * final definida.
	 *
	 * @param operacaoGgas the operacao ggas
	 * @param fatura the fatura
	 * @param dataFinalCalculo the data final calculo
	 * @param corrigirSaldo the corrigir saldo
	 * @param adicionarImpontualidade the adicionar impontualidade
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	public Pair<BigDecimal, BigDecimal> calcularSaldoFatura(boolean operacaoGgas, Fatura fatura, Date dataFinalCalculo,
					boolean corrigirSaldo, boolean adicionarImpontualidade) throws GGASException {

		return this.getControladorFatura().calcularSaldoFatura(operacaoGgas, fatura, dataFinalCalculo, corrigirSaldo,
						adicionarImpontualidade);
	}

	/**
	 * Método responsável por validar a situação
	 * das notas para cancelamento.
	 *
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @throws GGASException
	 *             Caso alguma nota esteja com
	 *             CreditoDebitoSituacao
	 *             cancelada.
	 */
	public void validarSituacaoNotasCancelamento(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFatura().validarSituacaoNotasCancelamento(chavesPrimarias);
	}

	/**
	 * Consultar pesquisa credito debito.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Map<String, Object>> consultarPesquisaCreditoDebito(Map<String, Object> filtro) throws GGASException {

		return this.getControladorCreditoDebito().consultarPesquisaCreditoDebito(filtro);
	}

	/**
	 * Consultar pesquisa credito debito parcelamento.
	 *
	 * @param filtro the filtro
	 * @param andamento the andamento
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Map<String, Object>> consultarPesquisaCreditoDebitoParcelamento(Map<String, Object> filtro, boolean andamento)
					throws GGASException {

		return this.getControladorCreditoDebito().consultarPesquisaCreditoDebitoParcelamento(filtro, andamento);
	}

	/**
	 * Método responsável por montar as parcelas
	 * do detalhamento do credito debito a
	 * realizar.
	 *
	 * @param idCreditoDebitoNegociado the id credito debito negociado
	 * @return the dados gerais parcelas
	 * @throws GGASException the GGAS exception
	 */
	public DadosGeraisParcelas montarParcelasDetalhamentoCreditoDebito(Long idCreditoDebitoNegociado) throws GGASException {

		return this.getControladorCreditoDebito().montarParcelasDetalhamento(idCreditoDebitoNegociado);
	}

	/**
	 * Validar situacao alteracao vencimento.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException the GGAS exception
	 */
	public void validarSituacaoAlteracaoVencimento(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFatura().validarSituacaoAlteracaoVencimento(chavesPrimarias);
	}

	/**
	 * Validar datas pesquisa processo.
	 *
	 * @param dataInicioAgendamento the data inicio agendamento
	 * @param dataFinalAgendamento the data final agendamento
	 * @throws GGASException the GGAS exception
	 */
	public void validarDatasPesquisaProcesso(String dataInicioAgendamento, String dataFinalAgendamento) throws GGASException {

		this.getControladorProcesso().validarDatasPesquisaProcesso(dataInicioAgendamento, dataFinalAgendamento);
	}

	/**
	 * Método responsável por obter uma
	 * TarifaPontoConsumo.
	 *
	 * @return Uma TarifaPontoConsumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public TarifaPontoConsumo criarTarifaPontoConsumo() throws NegocioException {

		return (TarifaPontoConsumo) this.getControladorTarifa().criarTarifaPontoConsumo();
	}

	/**
	 * *
	 * Método responsável por inserir, atualizar e
	 * excluir uma TarifaPontoConsumo.
	 *
	 * @param idPontoConsumo Chave primaria do ponto de
	 *            consumo
	 * @param itensTarifaPontoConsumo Coleção de TarifaPontoConsumo
	 *            para ser alterado ou inserido ou
	 *            excluido
	 * @param auditoria the auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarTarifasPontoConsumo(Long idPontoConsumo, Collection<TarifaPontoConsumo> itensTarifaPontoConsumo,
					DadosAuditoria auditoria) throws GGASException {

		this.getControladorTarifa().atualizarTarifasPontoConsumo(idPontoConsumo, itensTarifaPontoConsumo, auditoria);
	}

	/**
	 * *
	 * Método responsável por lista todos os item
	 * fatura de um determinado ponto de consumo
	 * associados a um contrato ativo.
	 *
	 * @param contratoPontoConsumo Um contratoPontoConsumo onde vai
	 *            ser utilizado informações a
	 *            respeito do contrato
	 *            e do ponto de consumo
	 * @return Uma coleção de EntidadeConteudo de
	 *         itemFatura
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarItemFaturaPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		return this.getControladorTarifa().listarItemFaturaPontoConsumo(contratoPontoConsumo);
	}

	/**
	 * *
	 * Método responsável por listar as
	 * TarifaVigencia de um determinado itemFatura
	 * onde o segmento
	 * e o mesmo do ponto de consumo.
	 *
	 * @param idItemFatura A chave primária do itemFatura
	 * @param idSegmento A chave primária do segmento
	 * @return Uma coleção de TarifaVigencia
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigencia> listarTarifaVigenciaItemFatura(Long idItemFatura, Long idSegmento) throws NegocioException {

		return this.getControladorTarifa().listarTarifaVigenciaItemFatura(idItemFatura, idSegmento);
	}

	/***
	 * Método responsável por validar dados da
	 * tela de associação temporária de tarifa a
	 * um ponto de
	 * consumo.
	 *
	 * @param dataInicial
	 *            a dataInicial da vigência de uma
	 *            tarifaPontoConsumo
	 * @param dataFinal
	 *            a dataFinal da vigência de uma
	 *            tarifaPontoConsumo
	 * @param idItemFatura
	 *            a chave primária de um
	 *            itemFatura
	 * @param idTarifa
	 *            a chave primária de uma tarifa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarDadosAdicionarAssocTempTarifaPontoConsumo(Date dataInicial, Date dataFinal, Long idItemFatura, Long idTarifa)
					throws NegocioException {

		this.getControladorTarifa().validarDadosAdicionarAssocTempTarifaPontoConsumo(dataInicial, dataFinal, idItemFatura, idTarifa);

	}

	/**
	 * *
	 * Método responsável pela menor data de
	 * vigencia de uma tarifa.
	 *
	 * @param idTarifa Chave primária de uma tarifa
	 * @return A menos data de vigência de uma
	 *         tarifa
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Date obterDataVigenciaTarifa(Long idTarifa) throws NegocioException {

		return this.getControladorTarifa().obterDataVigenciaTarifa(idTarifa);
	}

	/***
	 * Método responsável por validar se já existe
	 * uma associação para o período de vigência
	 * informado.
	 *
	 * @param listaTarifaPontoConsumo
	 *            Coleção de TarifaPontoConsumo
	 * @param itemTarifaPontoConsumoAtual
	 *            Um objeto TarifaPontoConsumo que
	 *            será verificado se está no
	 *            período de vigência
	 * @param index
	 *            O index do objeto
	 *            itemTarifaPontoConsumoAtual para
	 *            ser usado na verifiação dentro
	 *            da coleção
	 *            listaTarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarItemFaturaRepetidoTarifaPontoConsumo(Collection<TarifaPontoConsumo> listaTarifaPontoConsumo,
					TarifaPontoConsumo itemTarifaPontoConsumoAtual, Integer index) throws NegocioException {

		this.getControladorTarifa()
						.validarItemFaturaRepetidoTarifaPontoConsumo(listaTarifaPontoConsumo, itemTarifaPontoConsumoAtual, index);
	}

	/**
	 * *
	 * Método que lista as TarifaPontoConsumo.
	 *
	 * @param idPontoConsumo Chave primária de um ponto de
	 *            consumo
	 * @return Uma coleção de TarifaPontoConsumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaPontoConsumo> listarTarifaPontoConsumo(Long idPontoConsumo) throws NegocioException {

		return this.getControladorTarifa().listarTarifaPontoConsumo(idPontoConsumo);
	}

	/***
	 * Método responsável por validar se um ponto
	 * de consumo está associado a um contrato.
	 *
	 * @param itensFatura
	 *            Uma coleção de EntidadeConteudo
	 *            do tipo itemFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarPontoConsumoImovelContrato(Collection<EntidadeConteudo> itensFatura) throws NegocioException {

		this.getControladorTarifa().validarPontoConsumoImovelContrato(itensFatura);
	}

	/***
	 * Método responsável por verificar se a
	 * tarifa esta sendo usada em alguma fatura.
	 *
	 * @param tarifaPontoConsumo
	 *            A tarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarTarifaFaturamento(TarifaPontoConsumo tarifaPontoConsumo) throws NegocioException {

		this.getControladorTarifa().validarTarifaFaturamento(tarifaPontoConsumo);
	}

	/**
	 * *
	 * Método responsável por obter um
	 * ContratoPontoConsumoItemFaturamento.
	 *
	 * @param idPontoConsumo A chave primária de um ponto de
	 *            consumo
	 * @param idItemFatura A chave primária de um
	 *            itemFatura
	 * @return Um
	 *         ContratoPontoConsumoItemFaturamento
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoItemFaturamento obterTarifaPontoConsumoItemFaturamento(Long idPontoConsumo, Long idItemFatura)
					throws NegocioException {

		return this.getControladorContrato().obterTarifaPontoConsumoItemFaturamento(idPontoConsumo, idItemFatura);
	}

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas da vigencia da tarifa.
	 *
	 * @param tarifaVigencia
	 *            A tarifa vigência
	 * @return Uma coleção de dados das faixas da
	 *         tarifa.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<DadosFaixasTarifa> popularDadosTarifaVigenciaFaixa(TarifaVigencia tarifaVigencia) throws GGASException {

		return this.getControladorTarifa().popularDadosTarifaVigenciaFaixa(tarifaVigencia);
	}

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas dos descontos da tarifa.
	 *
	 * @param listaDadosFaixasTarifa
	 *            A lista de dados das faixas da
	 *            tarifa.
	 * @param tarifaVigenciaDesconto
	 *            A tarifa vigência desconto.
	 * @return Uma coleção de dados das faixas de
	 *         desconto das tarifas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException {

		return this.getControladorTarifa().popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tarifaVigenciaDesconto);
	}

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas dos descontos da tarifa sem
	 * descontos.
	 *
	 * @param listaDadosFaixasTarifa
	 *            A lista de dados das faixas da
	 *            tarifa.
	 * @return Uma coleção de dados das faixas de
	 *         desconto das tarifas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa) throws GGASException {

		return this.getControladorTarifa().popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa);
	}

	/**
	 * Método reponsável por ordenar as faixas por
	 * matriz.
	 *
	 * @param vetores O mapa com os valores para
	 *            ordenação.
	 * @param listaDadosFaixa lista de dados das faixas.
	 * @return the list
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<DadosFaixasTarifa> ordenarFaixasPorMatriz(Map<String, String[]> vetores, List<DadosFaixasTarifa> listaDadosFaixa)
					throws GGASException {

		return this.getControladorTarifa().ordenarFaixasPorMatriz(vetores, listaDadosFaixa);
	}

	/**
	 * Ordenar faixas por matriz.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<DadosFaixasTarifa> ordenarFaixasPorMatriz(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		return this.getControladorTarifa().ordenarFaixasPorMatriz(listaDadosFaixa);
	}

	/**
	 * Método responsável por ordenar as faixas
	 * para o VO.
	 *
	 * @param listaDadosFaixaTemp Lista temporária dos dados da
	 *            faixa
	 * @param listaDadosFaixaTela Lista dos dados da faixa
	 * @return the list
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<DadosFaixasTarifa> ordenarFaixasPorVO(List<DadosFaixasTarifa> listaDadosFaixaTemp,
					List<DadosFaixasTarifa> listaDadosFaixaTela) throws GGASException {

		return this.getControladorTarifa().ordenarFaixasPorVO(listaDadosFaixaTemp, listaDadosFaixaTela);
	}

	/**
	 * Método responsável por criar um Tributo
	 * Aliquota.
	 *
	 * @param chaveTributoAliquota the chave tributo aliquota
	 * @return TributoAliquota
	 * @throws GGASException the GGAS exception
	 */
	public TributoAliquota obterTributoAliquota(Long chaveTributoAliquota) throws GGASException {

		return this.getControladorTributo().obterTributoAliquota(chaveTributoAliquota);
	}

	/**
	 * Método responsável por reajustar os valores
	 * das faixas da tarifa de acordo com o valor
	 * informado.
	 *
	 * @param valorReajusteParam O valor informado
	 * @param idTipoReajusteParam a chave do tipo do reajuste
	 * @param listaDadosFaixa a lista de dados faixa
	 * @param listaDescontosFaixa e a lista de dados dos descontos
	 * @param qtdCasasDecimais the qtd casas decimais
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void reajustarFaixaTarifa(String valorReajusteParam, Long idTipoReajusteParam, Collection<DadosFaixasTarifa> listaDadosFaixa,
					Collection<DadosFaixasTarifa> listaDescontosFaixa, Integer qtdCasasDecimais) throws GGASException {

		this.getControladorTarifa().reajustarFaixaTarifa(valorReajusteParam, idTipoReajusteParam, listaDadosFaixa, listaDescontosFaixa,
						qtdCasasDecimais);
	}

	/**
	 * Método responsável por validar os campos do
	 * reajuste.
	 *
	 * @param valorReajuste
	 *            O valor informado
	 * @param idTipoReajuste
	 *            O tipo do reajuste
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarCamposReajuste(String valorReajuste, Long idTipoReajuste) throws GGASException {

		this.getControladorTarifa().validarCamposReajuste(valorReajuste, idTipoReajuste);
	}

	/**
	 * Método responsável por consultar os
	 * descontos cadastrados.
	 *
	 * @param chaveTarifa A chave da tarifa
	 * @return the list
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<TarifaFaixaDesconto> consultarDescontosCadastrados(Long chaveTarifa) throws GGASException {

		return this.getControladorTarifa().consultarDescontosCadastrados(chaveTarifa);
	}

	/**
	 * Método responsável por consultar as
	 * vigencias da tarifa.
	 *
	 * @param chaveTarifa
	 *            A chave primária da tarifa.
	 * @return lista de tarifaVigencia
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<TarifaVigencia> consultarVigenciasTarifa(Long chaveTarifa) throws GGASException {

		return this.getControladorTarifa().consultarVigenciasTarifa(chaveTarifa);
	}

	/**
	 * Método responsável por cadastrar um novo
	 * desconto para a tarifa.
	 *
	 * @param tarifaVigencia
	 *            A tarifa vigência
	 * @param tarifaVigenciaDesconto
	 *            A tarifa vigência desconto
	 * @param listaDadosFaixa
	 *            A lista de dados da faixa da
	 *            tarifa
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void cadastrarDescontos(TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		this.getControladorTarifa().cadastrarDescontos(tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
	}

	/**
	 * Método reponsável por remover o desconto
	 * cadastrado para a tarifa.
	 *
	 * @param chavePrimaria
	 *            a chave da tarifa
	 * @param dadosAuditoria
	 *            os dados da auditoria
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerDescontoCadastrado(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorTarifa().removerDescontoCadastrado(chavePrimaria, dadosAuditoria);
	}

	/**
	 * Método reponsável por remover uma vigencia
	 * cadastrada da tarifa.
	 *
	 * @param chavePrimaria a chave da tarifa vigencia
	 * @param dadosAuditoria os dados da auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerVigenciaCadastrada(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorTarifa().removerVigenciaCadastrada(chavePrimaria, dadosAuditoria);
	}

	/**
	 * Método responsável por validar a seleção da
	 * vigência e do desconto para exibição da
	 * tabela da
	 * faixas de tarifa e descontos.
	 *
	 * @param idTarifaVigencia Chave primária da tarifa
	 *            vigência
	 * @throws GGASException the GGAS exception
	 */
	public void validarVigenciaEDescontoParaExibicaoFaixas(Long idTarifaVigencia) throws GGASException {

		this.getControladorTarifa().validarVigenciaEDescontoParaExibicaoFaixas(idTarifaVigencia);
	}

	/**
	 * Método responsável por montar a lista de
	 * faixas de tarifa e desconto da tela de
	 * detalhamento.
	 *
	 * @param tarifaVigencia the tarifa vigencia
	 * @param tarifaVigenciaDesconto the tarifa vigencia desconto
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<FaixasTarifaDesconto> montarListaFaixasTarifaDesconto(TarifaVigencia tarifaVigencia,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException {

		return this.getControladorTarifa().montarListaFaixasTarifaDesconto(tarifaVigencia, tarifaVigenciaDesconto);
	}

	/**
	 * Método responsável por verificar se é para
	 * criar uma nova vigência para a tarifa a ser
	 * alterada.
	 *
	 * @param idTarifa A tarifa a ser alterada
	 * @param novaDataVigencia A nova data da vigência
	 * @return true, if successful
	 * @throws GGASException Caso ocorra algum erro na
	 *             formatação de campos.
	 */
	public boolean verificarCriacaoNovaVigencia(Long idTarifa, Date novaDataVigencia) throws GGASException {

		return this.getControladorTarifa().verificarCriacaoNovaVigencia(idTarifa, novaDataVigencia);
	}

	/**
	 * Adicionar faixa desconto.
	 *
	 * @param novaFaixa the nova faixa
	 * @param listaDadosFaixa the lista dados faixa
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<DadosFaixasTarifa> adicionarFaixaDesconto(DadosFaixasTarifa novaFaixa, List<DadosFaixasTarifa> listaDadosFaixa)
					throws GGASException {

		return this.getControladorTarifa().adicionarFaixaDesconto(novaFaixa, listaDadosFaixa);
	}

	/**
	 * Adicionar faixa final desconto.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @param indexUltimaFaixa the index ultima faixa
	 * @return the list
	 */
	public List<DadosFaixasTarifa> adicionarFaixaFinalDesconto(List<DadosFaixasTarifa> listaDadosFaixa, Long indexUltimaFaixa) {

		return this.getControladorTarifa().adicionarFaixaFinalDesconto(listaDadosFaixa, indexUltimaFaixa);

	}

	/**
	 * Valida a adição de faixa de desconto.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws GGASException the GGAS exception
	 */
	public void validarAdicaoFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		this.validarAdicaoFaixaDesconto(listaDadosFaixa, true);
	}

	/**
	 * Valida a adição de faixa de desconto.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @param validarValoresFaixas the validar valores faixas
	 * @throws GGASException the GGAS exception
	 */
	public void validarAdicaoFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws GGASException {

		this.getControladorTarifa().validarAdicaoFaixaDesconto(listaDadosFaixa, validarValoresFaixas);
	}

	/**
	 * Valida se existe mais de uma faixa com
	 * valor final zerado ou nulo.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws GGASException the GGAS exception
	 */
	public void validarFaixasZeradasTarifa(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		this.getControladorTarifa().validarFaixasZeradasTarifa(listaDadosFaixa);
	}

	/**
	 * Método responsável por atualizar uma tarifa.
	 *
	 * @param tarifa tarifa a tarifa a ser atualizada
	 * @param tarifaVigencia a vigência que vai ser
	 *            incluída/atualizada
	 * @param tarifaVigenciaDesconto tarifaVigenciaDesconto a
	 *            vigência de desconto que vai ser
	 *            incluída/atualizada
	 * @param listaDadosFaixa dados de cada faixa da vigência
	 * @param isNovaVigencia indicador se a vigência é nova
	 *            ou não
	 * @throws GGASException caso ocora algum erro durante a
	 *             execução do método
	 */
	public void atualizarTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa, boolean isNovaVigencia) throws GGASException {

		this.getControladorTarifa().atualizarTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa, isNovaVigencia);
	}

	/**
	 * Atualizar tarifa vigencia.
	 *
	 * @param tarifaVigencia the tarifa vigencia
	 * @param classe the classe
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarTarifaVigencia(TarifaVigencia tarifaVigencia, Class<?> classe) throws GGASException {

		this.getControladorTarifa().atualizar(tarifaVigencia, classe);
	}

	/**
	 * Método responsável por gerar um relatório
	 * de nota crédito ou nota de débito.
	 *
	 * @param notaInserida
	 *            Chave primária da nota que será
	 *            gerado o relatório.
	 * @param isSegundaVia
	 *            indica se o relatório a ser
	 *            gerado é segunda via.
	 * @return Um array de bytes com o relatório.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public byte[] gerarRelatorioNotaDebitoCredito(Fatura notaInserida, boolean isSegundaVia) throws GGASException {

		return this.getControladorFatura().gerarRelatorioNotaDebitoCredito(notaInserida, isSegundaVia);
	}

	/**
	 * Valida dados calcular valor fornecimento gas.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param idTarifaReal the id tarifa real
	 * @param idTarifaSimulada the id tarifa simulada
	 * @param idHistoricoConsumo the id historico consumo
	 * @param idVigencia the id vigencia
	 * @throws GGASException the GGAS exception
	 */
	public void validaDadosCalcularValorFornecimentoGas(String idPontoConsumo, Long idTarifaReal, Long idTarifaSimulada,
					Long idHistoricoConsumo, Long idVigencia) throws GGASException {

		this.getControladorCalculoFornecimentoGas().validaDadosCalcularValorFornecimentoGas(idPontoConsumo, idTarifaReal, idTarifaSimulada,
						idHistoricoConsumo, idVigencia);
	}

	/**
	 * Valida dados calcular fornecimento ficticio.
	 *
	 * @param idTarifa the id tarifa
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param totalConsumido the total consumido
	 * @throws GGASException the GGAS exception
	 */
	public void validaDadosCalcularFornecimentoFicticio(Long idTarifa, Date dataInicio, Date dataFim, String totalConsumido)
					throws GGASException {

		this.getControladorCalculoFornecimentoGas().validaDadosCalcularFornecimentoFicticio(idTarifa, dataInicio, dataFim, totalConsumido);
	}

	/**
	 * Método responsável por consultar os pontos
	 * de consumo tributo aliquota através do
	 * filtro
	 * informado.
	 *
	 * @param filtro O filtro com os parametros da
	 *            consulta
	 * @return Uma lista de Ponto de consumo
	 *         tributo aliquota
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumoTributoAliquota> consultarPontosConsumoTributoAliquota(Map<String, Object> filtro) throws GGASException {

		return this.getControladorImovel().consultarPontosConsumoTributoAliquota(filtro);
	}

	/**
	 * *
	 * Método responsável por validar se a
	 * situação do processo para cancelamento e em
	 * espera.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException 
	 */
	public void cancelarAgendamento(Long[] chavesPrimarias) throws GGASException {

		this.getControladorProcesso().cancelarAgendamento(chavesPrimarias);
	}

	/**
	 * Método responsável por obter um
	 * IndiceFinanceiro.
	 *
	 * @param chavePrimaria Chave primária do índice
	 *            financeiro.
	 * @return Um IndiceFinanceiro.
	 * @throws GGASException the GGAS exception
	 */
	public IndiceFinanceiro obterIndiceFinanceiro(Long chavePrimaria) throws GGASException {

		return this.getControladorTarifa().obterIndiceFinanceiro(chavePrimaria);
	}

	/**
	 * Método responsável por listar os índices
	 * financeiros.
	 *
	 * @return Uma coleção de IndiceFinanceiro.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<IndiceFinanceiro> listaIndiceFinanceiro() throws GGASException {

		return this.getControladorTarifa().listaIndiceFinanceiro();
	}

	/**
	 * Método responsável por retornar todos os
	 * pontos de consumo que tem o faturamento
	 * agrupado.
	 *
	 * @param idContrato A chavePrimaria do contrato
	 * @param tipoAgrupamento Tipo de Agrupamento
	 * @return Coleção dos pontos de consumo com
	 *         faturamento agrupado.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoFaturamentoAgrupado(long idContrato, EntidadeConteudo tipoAgrupamento)
					throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoFaturamentoAgrupado(idContrato, tipoAgrupamento);
	}

	/**
	 * Método responsável por obter o consumo
	 * apurado dos pontos de consumo por
	 * referência que tem o
	 * faturamento agrupado.
	 *
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param ciclo the ciclo
	 * @param listaPontoConsumoAgrupados the lista ponto consumo agrupados
	 * @return Coleção dos pontos de consumo com
	 *         faturamento agrupado.
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal obterConsumoApuradoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
					Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {

		return this.getControladorHistoricoConsumo().obterConsumoApuradoPontoConsumoPorReferencia(anoMesFaturamento, ciclo,
						listaPontoConsumoAgrupados);
	}

	/**
	 * Método responsável por obter o consumo
	 * dos pontos de consumo por
	 * referência que tem o
	 * faturamento agrupado.
	 *
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param ciclo the ciclo
	 * @param listaPontoConsumoAgrupados the lista ponto consumo agrupados
	 * @return Coleção dos pontos de consumo com
	 *         faturamento agrupado.
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal obterConsumoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
					Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {

		return this.getControladorHistoricoConsumo().obterConsumoPontoConsumoPorReferencia(anoMesFaturamento, ciclo,
						listaPontoConsumoAgrupados);
	}

	/**
	 * Método responsável por montar os dados da
	 * exibição da tela da tarifa.
	 *
	 * @param idTarifa
	 *            Chave da tarifa
	 * @return Um VO com os dados da tarifa.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosTarifa montarDadosTarifa(Long idTarifa) throws GGASException {

		return this.getControladorTarifa().montarDadosTarifa(idTarifa);
	}

	/**
	 * Verifica se tarifa esta sendo utilizada.
	 *
	 * @param idTarifa the id tarifa
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean verificaSeTarifaEstaSendoUtilizada(Long idTarifa) throws NegocioException {

		return this.getControladorTarifa().verificaSeTarifaEstaSendoUtilizada(idTarifa);
	}

	/**
	 * Método responsável por verificar se a nova
	 * vigencia está cadastrada.
	 *
	 * @param tarifaVigenciaDesconto the tarifa vigencia desconto
	 * @param idDescontoCadastrado the id desconto cadastrado
	 * @throws GGASException the GGAS exception
	 */
	public void validarNovaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto, Long idDescontoCadastrado) throws GGASException {

		this.getControladorTarifa().validarNovaVigenciaDesconto(tarifaVigenciaDesconto, idDescontoCadastrado);
	}

	/**
	 * Método responsável por validar se uma
	 * vigência foi selecionada para clonar as
	 * faixas.
	 *
	 * @param idVigencia Chave primária da vigência.
	 * @throws GGASException the GGAS exception
	 */
	public void validarSelecaoVigencia(Long idVigencia) throws GGASException {

		this.getControladorTarifa().validarSelecaoVigencia(idVigencia);
	}

	/**
	 * Método responsável por obter todas as
	 * tarifaVigenciaDesconto de uma determinada
	 * tarifaVigencia.
	 *
	 * @param chavePrimariaTarifaVigencia the chave primaria tarifa vigencia
	 * @return Uma TarifaVigenciaDesconto.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<TarifaVigenciaDesconto> obterTarifaVigenciaDescontoPorTarifaVigencia(long chavePrimariaTarifaVigencia)
					throws GGASException {

		return this.getControladorTarifa().obterTarifaVigenciaDescontoPorTarifaVigencia(chavePrimariaTarifaVigencia);
	}

	/**
	 * Carregar dados tarifa faixa desconto.
	 *
	 * @param listaDadosFaixasTarifa the lista dados faixas tarifa
	 * @param idTarifaVigencia the id tarifa vigencia
	 * @return the dados tarifa
	 * @throws GGASException the GGAS exception
	 */
	public DadosTarifa carregarDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa, Long idTarifaVigencia)
					throws GGASException {

		return this.getControladorTarifa().carregarDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, idTarifaVigencia);
	}

	/**
	 * Método responsável por multiplicar a
	 * quantidade e o valor unitário exibidos na
	 * tela de
	 * incluir credito/debito a realizar.
	 *
	 * @param valorQuantidade the valor quantidade
	 * @param valorUnitario the valor unitario
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterMultiplicacaoValorUnitario(String valorQuantidade, String valorUnitario) throws GGASException {

		return this.getControladorCreditoDebito().obterMultiplicacaoValorUnitario(valorQuantidade, valorUnitario);
	}

	/**
	 * Método responsável por obter a lista de
	 * tipos de agrupamento.
	 *
	 * @return coleção de tipos de agrupamento
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoAgrupamento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoAgrupamento();
	}

	/**
	 * Método responsável por obter o último
	 * histórico de consumo não faturado.
	 *
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @param indicadorFaturamento
	 *            Indicador para os consumos
	 *            faturados ou não.
	 * @param indicadorConsumoCiclo
	 *            Indicador do consumo ciclo.
	 * @param indicadorUltimo
	 *            Indicador para buscar os últimos
	 *            consumos.
	 * @return Um HistoricoConsumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoConsumo obterHistoricoConsumo(Long idPontoConsumo, boolean indicadorFaturamento, boolean indicadorConsumoCiclo,
					boolean indicadorUltimo) throws GGASException {

		return this.getControladorHistoricoConsumo().obterHistoricoConsumo(idPontoConsumo, indicadorFaturamento, indicadorConsumoCiclo,
						indicadorUltimo);
	}

	/**
	 * Método responsável por listar os pontos de
	 * consumo de um contrato.
	 *
	 * @param idContrato
	 *            Chave primária do contrato.
	 * @return Uma coleção de pontos de consumo.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PontoConsumo> listaPontoConsumoPorContrato(Long idContrato) throws GGASException {

		return this.getControladorPontoConsumo().listaPontoConsumoPorContrato(idContrato);
	}

	/**
	 * Método responsável por obter a ultima
	 * leitura do ponto de consumo no ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave do ponto de consumo
	 * @param referencia A referência
	 * @param ciclo O ciclo
	 * @return Uma medição
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo, int referencia, int ciclo) throws GGASException {

		return this.getControladorHistoricoMedicao().obterUltimoHistoricoMedicao(chavePrimariaPontoConsumo, referencia, ciclo);
	}

	/**
	 * Método responsável por obter o consumo do
	 * ponto de consumo no ciclo.
	 *
	 * @param chavePrimariaPontoConsumo A chave do ponto de consumo
	 * @param referencia A referência
	 * @param ciclo O ciclo
	 * @param isFaturado the is faturado
	 * @return Consumo
	 * @throws NegocioException the negocio exception
	 */

	/**
	 * @param chavePrimariaPontoConsumo
	 * @param referencia
	 * @param ciclo
	 * @param isFaturado
	 * @return
	 * @throws NegocioException
	 */
	public HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo, Integer referencia, Integer ciclo,
					Boolean isFaturado) throws NegocioException {

		return this.getControladorHistoricoMedicao().obterUltimoHistoricoMedicao(chavePrimariaPontoConsumo, referencia, ciclo, isFaturado);
	}

	/**
	 * Método responsável por criar um histórico
	 * de medição.
	 *
	 * @return Um HistoricoMedicao.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public HistoricoMedicao criarHistoricoMedicao() throws GGASException {

		return (HistoricoMedicao) this.getControladorHistoricoMedicao().criar();
	}

	/**
	 * Método responsável por consistir os dados
	 * de leitura na inclusão de fatura.
	 *
	 * @param listaHistoricoMedicao Lista de histórico de medição.
	 * @param isFaturaAvulso the is fatura avulso
	 * @throws GGASException the GGAS exception
	 */
	public void consistirLeituraFaturamento(Collection<HistoricoMedicao> listaHistoricoMedicao, Boolean isFaturaAvulso)
					throws GGASException {

		this.getControladorFatura().consistirLeituraFaturamento(listaHistoricoMedicao, isFaturaAvulso);
	}

	/**
	 * Método responsável por montar os dados para
	 * exibição da lista de ítens de fatura de um
	 * contrato.
	 *
	 * @param listaItemFaturamento Lista de de
	 *            ContratoPontoConsumoItemFaturamento
	 *            .
	 * @param dataEmissaoInformada the data emissao informada
	 * @return Um lista com as propriedades a
	 *         serem exibidas.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<DadosGeraisItensFatura> montarDadosGeraisItensFatura(
					Collection<ContratoPontoConsumoItemFaturamento> listaItemFaturamento, Date dataEmissaoInformada) throws GGASException {

		return this.getControladorFatura().montarDadosGeraisItensFatura(listaItemFaturamento, dataEmissaoInformada);
	}

	/**
	 * Obter dados inclusao fatura.
	 *
	 * @param idCliente the id cliente
	 * @param pontoConsumo the ponto consumo
	 * @return the dados resumo fatura
	 * @throws GGASException the GGAS exception
	 */
	public DadosResumoFatura obterDadosInclusaoFatura(Long idCliente, PontoConsumo pontoConsumo) throws GGASException {

		return this.getControladorFatura().obterDadosInclusaoFatura(idCliente, pontoConsumo, null);
	}

	/**
	 * Obter dados inclusao fatura.
	 *
	 * @param idCliente the id cliente
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param referencia the referencia
	 * @param ciclo the ciclo
	 * @param isFaturaEncerramento the is fatura encerramento
	 * @return the dados resumo fatura
	 * @throws GGASException the GGAS exception
	 */
	public DadosResumoFatura obterDadosInclusaoFatura(Long idCliente, Collection<PontoConsumo> listaPontoConsumo, Integer referencia,
					Integer ciclo, Boolean isFaturaEncerramento) throws GGASException {

		return this.getControladorFatura().obterDadosInclusaoFatura(idCliente, listaPontoConsumo, referencia, ciclo, isFaturaEncerramento,
						null);
	}

	/**
	 * Método responsável por obter os dados para
	 * exibição da tela de resumo da fatura.
	 *
	 * @param dadosResumoFatura Dados da tela de inclusão de
	 *            fatura.
	 * @param listaCreditoDebito the lista credito debito
	 * @param isInclusao the is inclusao
	 * @param tipoApuracao the tipo apuracao
	 * @param isSimulacao the is simulacao
	 * @param valorInformado the valor informado
	 * @return Um VO de DadosResumoFatura.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosResumoFatura obterDadosResumoFatura(DadosResumoFatura dadosResumoFatura,
					Collection<CreditoDebitoARealizar> listaCreditoDebito, boolean isInclusao, String tipoApuracao, boolean isSimulacao,
					String valorInformado) throws GGASException {

		return this.getControladorFatura().obterDadosResumoFatura(dadosResumoFatura, listaCreditoDebito, isInclusao, tipoApuracao,
						isSimulacao, valorInformado, null);
	}

	/**
	 * Método responsável por retornar uma
	 * situação de leitura.
	 *
	 * @param chavePrimaria A chave primaria da situação
	 * @return Uma situação de leitura
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public SituacaoLeitura obterSituacaoLeitura(long chavePrimaria) throws GGASException {

		return this.getControladorLeituraMovimento().obterSituacaoLeitura(chavePrimaria);
	}

	/**
	 * Método responsável por obter a lista de
	 * motivos de inclusão da fatura.
	 *
	 * @return Uma coleção de motivos de inclusão
	 *         de fatura.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaMotivoInclusao() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaMotivoInclusao();
	}

	/**
	 * Método responsável por validar a
	 * periodicidade de um histórico de consumo.
	 *
	 * @param historicoConsumo the historico consumo
	 * @throws GGASException the GGAS exception
	 */
	public void validarPeriodicidadeHistoricoConsumo(HistoricoConsumo historicoConsumo) throws GGASException {

		this.getControladorHistoricoConsumo().validarPeriodicidadeHistoricoConsumo(historicoConsumo);
	}

	/**
	 * Método responsável por validar a quantidade
	 * maxima consecutiva de um tipo de consumo de
	 * faturamento.
	 *
	 * @param historicoConsumo historico consumo usado como
	 *            base
	 * @throws GGASException the GGAS exception
	 */
	public void validarQuantidadeMaximaConsecutivaFaturamento(HistoricoConsumo historicoConsumo) throws GGASException {

		this.getControladorFatura().validarQuantidadeMaximaConsecutivaFaturamento(historicoConsumo);
	}

	/**
	 * Validar saldo remanescente credito.
	 *
	 * @param fatura the fatura
	 * @throws GGASException the GGAS exception
	 */
	public void validarSaldoRemanescenteCredito(Fatura fatura) throws GGASException {

		this.getControladorFatura().validarSaldoRemanescenteCredito(fatura);
	}

	/**
	 * Método responsável por processar a inclusão
	 * de fatura.
	 *
	 * @param dadosResumoFatura DadosResumoFatura..
	 * @param isInserirDocumentoCobranca the is inserir documento cobranca
	 * @param isBatch indicador se o faturamento é por
	 *            batch ou por tela
	 * @param dadosAuditoria the dados auditoria
	 * @return the collection
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Fatura> processarInclusaoFatura(DadosResumoFatura dadosResumoFatura, boolean isInserirDocumentoCobranca,
					boolean isBatch, DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorFatura().processarInclusaoFatura(dadosResumoFatura, isInserirDocumentoCobranca, isBatch, dadosAuditoria);

	}

	/**
	 * Método responsável por realizar a inclusão
	 * da fatura após a análise da anomalia.
	 *
	 * @param listaHistoricoAnomaliaFaturamento the lista historico anomalia faturamento
	 * @param dadosAuditoria the dados auditoria
	 * @return indicador se a fatura foi inserida
	 *         com sucesso
	 * @throws GGASException the GGAS exception
	 */
	public boolean processarInclusaoFaturaAnomaliaCorrigida(Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento,
					DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorFatura().processarInclusaoFaturaAnomaliaCorrigida(listaHistoricoAnomaliaFaturamento, dadosAuditoria);
	}

	/**
	 * *
	 * Método responsável por listas as faturas
	 * que serão canceladas.
	 *
	 * @param listaFatura the lista fatura
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Fatura> listaFaturaCancelar(Collection<Fatura> listaFatura, Long[] chavesPrimarias) throws NegocioException {

		return this.getControladorFatura().listaFaturaCancelar(listaFatura, chavesPrimarias);
	}

	/**
	 * Método responsável por cancelar as faturas
	 * selecionadas.
	 *
	 * @param chavesPrimarias Chave primária das faturas a
	 *            serem canceladas.
	 * @param idMotivoCancelamento Chave do motivo de cancelamento.
	 * @param dadosAuditoria Dados para auditoria.
	 * @param isRefaturar Indicador de refaturamento.
	 * @throws GGASException the GGAS exception
	 */
	public void cancelarFaturas(Long[] chavesPrimarias, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria, boolean isRefaturar)
					throws GGASException {

		this.getControladorFatura().cancelarFaturas(chavesPrimarias, idMotivoCancelamento, dadosAuditoria, isRefaturar,
						Constantes.C_PROV_DEV_DUV_MOTIVO_BAIXA_CANCELAMENTO);
	}

	/**
	 * Método responsável por solicitar o cancelar
	 * as faturas selecionadas.
	 * O método irá atualizar dados do documento
	 * fiscal e o sistema aguarda
	 * retorno do sistema de nfe para de fato
	 * realizar o cancelamento.
	 *
	 * @param chavesPrimarias Chave primária das faturas a
	 *            serem canceladas.
	 * @param idMotivoCancelamento Chave do motivo de cancelamento.
	 * @param dadosAuditoria Dados para auditoria.
	 * @param isRefaturar Indicador de refaturamento.
	 * @throws GGASException the GGAS exception
	 */
	public void solicitarCancelamentoFaturas(Long[] chavesPrimarias, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria,
					boolean isRefaturar) throws GGASException {

		this.getControladorFatura().solicitarCancelamentoFaturas(chavesPrimarias, idMotivoCancelamento, dadosAuditoria, isRefaturar);
	}

	/**
	 * Remover faturas.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerFaturas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorFatura().removerFaturas(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * *
	 * Valida se foi escolhido um motivo de
	 * cancelamento da fatura.
	 *
	 * @param idMotivoCancelamento chave primária do motivo de
	 *            cancelamento
	 * @throws NegocioException caso ocorra algum erro
	 */
	public void validarMotivoCancelamento(Long idMotivoCancelamento) throws NegocioException {

		this.getControladorFatura().validarMotivoCancelamento(idMotivoCancelamento);
	}

	/**
	 * Método responsável por gerar as faturas em PDF.
	 *
	 * @param fatura the fatura
	 * @param faturasFilhas the faturas filhas
	 * @param sequencial the sequencial
	 * @param lote the lote
	 * @param dadosAuditoria the dados auditoria
	 * @param ultimaFatura the ultima fatura
	 * @param acumuladoFaturas the acumulado faturas
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarEmissaoFatura(Fatura fatura, Collection<Fatura> faturasFilhas, Integer sequencial, Integer lote,
					DadosAuditoria dadosAuditoria, boolean ultimaFatura, BigDecimal acumuladoFaturas) throws GGASException {

		return this.getControladorFatura().gerarEmissaoFatura(fatura, faturasFilhas, sequencial, lote, dadosAuditoria, ultimaFatura,
						acumuladoFaturas, null, false);
	}

	/**
	 * Método responsável por identificar se um
	 * ponto de consumo está adimplente ou não até
	 * um
	 * ano/mês de referência e um ciclo.
	 *
	 * @param idPontoConsumo identificador do ponto de
	 *            consumo
	 * @return true = inadimplente; false =
	 *         adimplente
	 * @throws NegocioException caso ocorra algum erro
	 */
	public boolean verificarInadimplenciaPontoConsumo(Long idPontoConsumo) throws NegocioException {

		return this.getControladorFatura().verificarInadimplenciaPontoConsumo(idPontoConsumo);
	}

	/**
	 * Método responsável por obter a lista de
	 * tipos de mensagem.
	 *
	 * @return coleção de tipos de mensagem
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarTiposMensagem() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarTipoMensagem();
	}

	/**
	 * Método responsável por obter o controlador
	 * de mensagem faturamento.
	 *
	 * @return O controlador de mensagem
	 *         faturamento.
	 */
	private ControladorMensagemFaturamento getControladorMensagemFaturamento() {

		return (ControladorMensagemFaturamento) serviceLocator
						.getControladorNegocio(ControladorMensagemFaturamento.BEAN_ID_CONTROLADOR_MENSAGEM_FATURAMENTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de mensagem faturamento.
	 *
	 * @return O controlador de mensagem
	 *         faturamento.
	 */
	private ControladorConstanteSistema getControladorConstanteSistema() {

		return (ControladorConstanteSistema) serviceLocator
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}

	/**
	 * Método responsável por consultar mensagens
	 * de faturamento de acordo com um filtro.
	 *
	 * @param filtro o filtro utilizado na consulta
	 * @return coleção de mensagens de faturamento
	 * @throws GGASException caso ocorra algum erro na
	 *             invocação do método
	 */
	public Collection<MensagemFaturamento> consultarMensagemFaturamento(Map<String, Object> filtro) throws GGASException {

		return this.getControladorMensagemFaturamento().consultarMensagemFaturamento(filtro);
	}

	/**
	 * Método responsável por listar os Ramos de
	 * Atividade de um grupo de segmentos.
	 *
	 * @param chavesSegmentos chaves dos segmentos
	 * @return a coleção de ramos de atividades
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método
	 */
	public Collection<RamoAtividade> consultarRamoAtividadePorSegmentos(Long[] chavesSegmentos) throws NegocioException {

		return this.getControladorRamoAtividade().consultarRamoAtividadePorSegmentos(chavesSegmentos);
	}
	
	/**
	 * Método responsável por listar os tipos de convênio
	 *
	 * @return os tipos de convênio cadastrados
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método
	 */
	public Collection<EntidadeConteudo> listarTiposDeConvenio() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarTiposDeConvenio();
	} 

	/**
	 * Método responsável por listar as formas de
	 * cobrança.
	 *
	 * @return as formas de cobrança cadastradas
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método
	 */
	public Collection<EntidadeConteudo> listarFormaCobranca() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarFormaCobranca();
	}

	/**
	 * Método responsável por criar uma instância
	 * da MensagemFaturamento.
	 *
	 * @return a MensagemFaturamento criada
	 */
	public MensagemFaturamento criarMensagemFaturamento() {

		return this.getControladorMensagemFaturamento().criarMensagemFaturamento();
	}

	/**
	 * Método responsável por criar uma instância
	 * da MensagemSegmento.
	 *
	 * @return a MensagemSegmento criada
	 */
	public MensagemSegmento criarMensagemSegmento() {

		return this.getControladorMensagemFaturamento().criarMensagemSegmento();
	}

	/**
	 * Método responsável por criar uma instância
	 * da MensagemCobranca.
	 *
	 * @return a MensagemCobranca criada
	 */
	public MensagemCobranca criarMensagemCobranca() {

		return this.getControladorMensagemFaturamento().criarMensagemCobranca();
	}

	/**
	 * Método responsável por criar uma instância
	 * da MensagemVencimento.
	 *
	 * @return a MensagemVencimento criada
	 */
	public MensagemVencimento criarMensagemVencimento() {

		return this.getControladorMensagemFaturamento().criarMensagemVencimento();
	}

	/**
	 * Método responsável por inserir uma mensagem
	 * faturamento.
	 *
	 * @param mensagemFaturamento
	 *            a mensagemFaturamento a ser
	 *            inserida
	 * @return chavePrimaria da
	 *         mensagemFaturamento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método
	 */
	public Long inserirMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws GGASException {

		return this.getControladorMensagemFaturamento().inserirMensagemFaturamento(mensagemFaturamento);
	}

	/**
	 * Método responsável por obter uma mensagem
	 * faturamento por chave Primária.
	 *
	 * @param chavePrimaria chave da mensagem de faturamento
	 * @param propriedadesLazy lista de propriedades que serão
	 *            carredas
	 * @return Mensagem Faturamento consultada
	 * @throws GGASException caso ocorra algum erro durante
	 *             a invocação do método
	 */
	public MensagemFaturamento obterMensagemFaturamento(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (MensagemFaturamento) this.getControladorMensagemFaturamento().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * método responsável por remover uma Mensagem
	 * de Faturamento.
	 *
	 * @param chavesPrimarias chaves primárias das mensagens
	 *            de faturamento que serão
	 *            removidas
	 * @param dadosAuditoria dados de Auditoria
	 * @throws NegocioException Caso ocorra alguma erro durante
	 *             a invocação do método
	 */
	public void removerMensagemFaturamento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorMensagemFaturamento().removerMensagemFaturamento(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por alterar uma Mensagem
	 * de Faturamento.
	 *
	 * @param mensagemFaturamento mensagem de faturamento para
	 *            alterar
	 * @throws ConcorrenciaException caso ocorra algum erro durante
	 *             a invocação do método
	 * @throws NegocioException caso ocorra algum erro durante
	 *             a invocação do método
	 */
	public void alterarMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws ConcorrenciaException, NegocioException {

		this.getControladorMensagemFaturamento().alterarMensagemFaturamento(mensagemFaturamento);
	}

	/**
	 * Método responsável por retornar os dados da
	 * fatura para detalhamento.
	 *
	 * @param idFatura
	 *            Chave da Fatura.
	 * @param idDocumentoFiscal
	 *            Chave da Fatura.
	 * @return DadosResumoFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	public DadosResumoFatura obterDadosDetalhamentoFatura(Long idFatura, Long idDocumentoFiscal) throws NegocioException {

		return this.getControladorFatura().obterDadosDetalhamentoFatura(idFatura, idDocumentoFiscal);
	}

	/**
	 * Método responsável por retornar os dados da
	 * fatura para detalhamento.
	 *
	 * @param idFatura
	 *            Chave da Fatura.
	 * @return DadosResumoFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	public Long[] obterChavesPontosConsumoFatura(Long idFatura) throws NegocioException {

		return this.getControladorFatura().obterChavesPontosConsumoFatura(idFatura);
	}

	/**
	 * Método responsável por validar as faturas
	 * que irão ser refaturadas.
	 *
	 * @param chavesPrimarias As chaves primárias das faturas
	 * @throws GGASException the GGAS exception
	 */
	public void validarFaturasParaRefaturar(Long[] chavesPrimarias) throws GGASException {

		this.getControladorFatura().validarFaturasParaRefaturar(chavesPrimarias);
	}

	/**
	 * Validar todos pontos consumo dados informados.
	 *
	 * @param todosPontosConsumoDadosInformados the todos pontos consumo dados informados
	 * @param pontoConsumoNaoInformado the ponto consumo nao informado
	 * @throws NegocioException the negocio exception
	 */
	public void validarTodosPontosConsumoDadosInformados(Boolean todosPontosConsumoDadosInformados, String pontoConsumoNaoInformado)
					throws NegocioException {

		this.getControladorContrato().validarTodosPontosConsumoDadosInformados(todosPontosConsumoDadosInformados, pontoConsumoNaoInformado);
	}

	/**
	 * Obtem uma lista com todos os pontos de
	 * consumo que possuem contrato ativo no
	 * imovel.
	 *
	 * @param chaveImovel the chave imovel
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoComContratoAtivoPorImovel(Long chaveImovel) throws GGASException {

		return this.getControladorPontoConsumo().listarPontoConsumoComContratoAtivoPorImovel(chaveImovel);
	}

	/**
	 * Método responsável por obter os dados da
	 * fatura para exibir a tela de refaturar
	 * Fatura.
	 *
	 * @param chavePrimaria A chave da fatura
	 * @return DadosResumoFatura
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public DadosResumoFatura obterDadosRefaturarFatura(Long chavePrimaria) throws NegocioException {

		return this.getControladorFatura().obterDadosRefaturarFatura(chavePrimaria);
	}

	/**
	 * Método que calcula a os valores médios para
	 * um item de fatura, de uma determinada
	 * Tarifa,
	 * utilizada num período.
	 *
	 * @param pontoConsumo
	 *            o Ponto de Consumo
	 * @param itemFatura
	 *            o Item de Fatura
	 * @param dataInicioPeriodoMedicao
	 *            a data de início do período
	 * @param dataFimPeriodoMedicao
	 *            a data de fim do período
	 * @param isConsiderarDescontos
	 *            se deve considerar descontos no
	 *            cálculo
	 * @return Coleção de tarifaVigenciaFaixa com
	 *         o valor médio
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	public Collection<TarifaVigenciaFaixa> calcularTarifaMediaNoPeriodo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Date dataInicioPeriodoMedicao, Date dataFimPeriodoMedicao, boolean isConsiderarDescontos) throws NegocioException {

		return this.getControladorCalculoFornecimentoGas().calcularTarifaMediaNoPeriodo(pontoConsumo, itemFatura, dataInicioPeriodoMedicao,
						dataFimPeriodoMedicao, isConsiderarDescontos);
	}

	/**
	 * Método responsável por obter a lista de
	 * motivos para refaturar fatura.
	 *
	 * @return Uma coleção de motivos para
	 *         refaturar fatura.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaMotivoRefaturar() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaMotivoRefaturar();
	}

	/**
	 * Método responsável por retificar uma
	 * fatura.
	 *
	 * @param dadosResumoFatura Dados Resumo Fatura.
	 * @param dadosAuditoria Dados Auditoria.
	 * @return Faturas Refaturadas.
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Fatura> refaturarFatura(DadosResumoFatura dadosResumoFatura, DadosAuditoria dadosAuditoria) throws GGASException {

		return this.getControladorFatura().refaturarFatura(dadosResumoFatura, dadosAuditoria);
	}

	/**
	 * Método responsável por validar o filtro de
	 * pesquisa da tela de Guia de Pagamento.
	 *
	 * @param filtro the filtro
	 * @throws NegocioException the negocio exception
	 */
	public void validarFiltroPesquisaNotaDebito(Map<String, Object> filtro) throws NegocioException {

		this.getControladorFatura().validarFiltroPesquisaNotaDebito(filtro);
	}

	/**
	 * Validar excluir clientes relacionados.
	 *
	 * @param clienteImovel the cliente imovel
	 * @throws GGASException the GGAS exception
	 */
	public void validarExcluirClientesRelacionados(ClienteImovel clienteImovel) throws GGASException {

		this.getControladorImovel().validarExcluirClientesRelacionados(clienteImovel);
	}

	/**
	 * Método responsável por receber uma colecao
	 * de faturas e gerar o documento de impressão
	 * delas.
	 *
	 * @param dadosAuditoria parametro necessário para
	 *            atualização das faturas.
	 * @param listaFatura lista das faturas que terao os
	 *            documentos gerados.
	 * @return array de byte representando os
	 *         documentos de fatura.
	 * @throws GGASException the GGAS exception
	 */
	public byte[] processarDocumentoRetornoFatura(DadosAuditoria dadosAuditoria, Collection<Fatura> listaFatura) throws GGASException {

		return this.getControladorFatura().processarDocumentoRetornoFatura(dadosAuditoria, listaFatura);
	}

	/**
	 * Método responsável por capturar uma fatura
	 * já impressa e gerar uma segunda via para
	 * ela.
	 *
	 * @param fatura fatura a ser gerada a segunda
	 *            via.
	 * @param documentoFiscal the documento fiscal
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarImpressaoFaturaSegundaVia(Fatura fatura, DocumentoFiscal documentoFiscal) throws GGASException {

		return this.getControladorFatura().gerarImpressaoFaturaSegundaVia(fatura, documentoFiscal);
	}
	
	/**
	 * Método para gerar a impressão Danfe
	 * @param documentoFiscal {@link DocumentoFiscal}
	 * @return the byte[]
	 * @throws GGASException {@link GGASException}
	 */
	public byte[] gerarImpressaoDanfe(DocumentoFiscal documentoFiscal) throws GGASException {
		
		return this.getControladorFatura().gerarImpressaoDanfe(documentoFiscal);
    }

	/**
	 * Obter fatura impressao.
	 *
	 * @param codigoFatura the codigo fatura
	 * @return the fatura impressao
	 */
	public FaturaImpressao obterFaturaImpressao(Long codigoFatura) {

		return this.getControladorFatura().obterFaturaImpressao(codigoFatura);
	}

	/**
	 * Valida item fartura rubrica.
	 *
	 * @param rubrica the rubrica
	 * @throws GGASException the GGAS exception
	 */
	public void validaItemFarturaRubrica(Rubrica rubrica) throws GGASException {

		this.getControladorRubrica().validaItemFarturaRubrica(rubrica);

	}

	/**
	 * Método responsável por criar um VO
	 * DadosItemFaturamento.
	 *
	 * @return DadosItemFaturamento criado
	 * @throws NegocioException caso ocorra algum erro
	 */
	public DadosItemFaturamento criarDadosItemFaturamento() throws NegocioException {

		return this.getControladorFatura().criarDadosItemFaturamento();

	}

	/**
	 * Método responsável por validar se os
	 * intervalos entre as faixas estão corretos.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws GGASException the GGAS exception
	 */
	public void validarFaixasTarifa(Collection<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		this.validarFaixasTarifa(listaDadosFaixa, true);
	}

	/**
	 * Método responsável por validar se os
	 * intervalos entre as faixas estão corretos.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @param validarValoresFaixas the validar valores faixas
	 * @throws GGASException the GGAS exception
	 */
	public void validarFaixasTarifa(Collection<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws GGASException {

		this.getControladorTarifa().validarFaixasTarifa(listaDadosFaixa, validarValoresFaixas);
	}

	/**
	 * Método responsável por listar as tarifas
	 * relacionadas a um ponto de consumo e a um
	 * item de fatura.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chaveItemFatura the chave item fatura
	 * @return List<Tarifa>
	 * @throws GGASException the GGAS exception
	 */
	public List<Tarifa> obterTarifas(Long chavePontoConsumo, Long chaveItemFatura) throws GGASException {

		return this.getControladorTarifa().obterTarifas(chavePontoConsumo, chaveItemFatura);
	}

	/**
	 * Método responsável por listar as tarifas
	 * relacionadas a um ponto de consumo por ramo de atividade e a um
	 * item de
	 * fatura.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chaveItemFatura the chave item fatura
	 * @return List<Tarifa>
	 * @throws GGASException the GGAS exception
	 */
	public List<Tarifa> obterTarifasPorRamoAtividade(Long chavePontoConsumo, Long chaveItemFatura) throws GGASException {

		return this.getControladorTarifa().obterTarifasPorRamoAtividade(chavePontoConsumo, chaveItemFatura);
	}

	/**
	 * Método responsável por obter o valor total
	 * já abadito de um crédito.
	 *
	 * @param idCredito Chave primária do crédito.
	 * @param idFatura the id fatura
	 * @return Valor total abatido do crédito.
	 * @throws NegocioException Caso ocorra algum eror na
	 *             invocação do método.
	 */
	public BigDecimal obterValorCobradoCredito(Long idCredito, Long idFatura) throws NegocioException {

		return this.getControladorCreditoDebito().obterValorCobradoCredito(idCredito, idFatura);
	}

	/**
	 * Método responsável por obter o saldo de um
	 * crédito.
	 *
	 * @param creditoDebitoARealizar
	 *            O crédito.
	 * @return O saldo do crédito.
	 * @throws GGASException
	 *             Caso ocorra algum eror na
	 *             invocação do método.
	 */
	public BigDecimal obterSaldoCreditoDebito(CreditoDebitoARealizar creditoDebitoARealizar) throws GGASException {

		return this.getControladorCreditoDebito().obterSaldoCreditoDebito(creditoDebitoARealizar);
	}

	/**
	 * Método responsável por consultar os
	 * Documentos Fiscais através de uma Fatura.
	 *
	 * @param codigoFatura código da Fatura
	 * @return Collection<DocumentoFiscal>
	 */
	public Collection<DocumentoFiscal> obterDocumentoFiscalPorFatura(Long codigoFatura) {

		return this.getControladorFatura().obterDocumentosFiscaisPorFatura(codigoFatura);
	}

    /**
     * Método responsável por consultar os
     * Documentos Fiscais através do Id Fatura.
     *
     * @param codigoFatura código da Fatura
     * @return Collection<DocumentoFiscal>
     */
	public Collection<DocumentoFiscal> obterDocumentoFiscalPorIdFatura(Long codigoFatura) {

		return this.getControladorFatura().obterDocumentosFiscaisPorIdFatura(codigoFatura);
	}



	/**
	 * Método responsável por consultar os
	 * Documentos Fiscais através de uma lista de Faturas.
	 *
	 * @param listaFatura {@link Collection}
	 * @return Collection<DocumentoFiscal> {@link Collection}
	 */
	public Map<Long, Collection<DocumentoFiscal>> obterDocumentosFiscaisPorListaFatura(List<Fatura> listaFatura) {
		return this.getControladorFatura().obterDocumentosFiscaisPorListaFatura(listaFatura);
	}

	/**
	 * Método responsável por consultar o
	 * Documento Fiscal mais recente através de
	 * uma Fatura.
	 *
	 * @param codigoFatura código da Fatura
	 * @return Collection<DocumentoFiscal>
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoFiscal obterDocumentoFiscalPorFaturaMaisRecente(Long codigoFatura) throws NegocioException {

		return this.getControladorFatura().obterDocumentoFiscalPorFaturaMaisRecente(codigoFatura);
	}
	
	/**
	 * Método responsável por consultar o
	 * DocumentoFiscal pelo Número da Fatura
	 * @param idFatura
	 * @return
	 * @throws NegocioException
	 */
	public DocumentoFiscal obterDocumentoFiscalporNumeroFatura(Long idFatura) throws NegocioException {

		return this.getControladorFatura().obterDocumentoFiscalporNumeroFatura(idFatura);
	}

	/**
	 * Método responsável por validar a leitura e
	 * consumo ao avançar para a tela de resumo
	 * inclusão
	 * fatura.
	 *
	 * @param listaHistoricoMedicao the lista historico medicao
	 * @throws NegocioException caso ocorra algum erro
	 */
	public void validarAvancarResumoInclusaoFatura(Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException {

		this.getControladorFatura().validarAvancarResumoInclusaoFatura(listaHistoricoMedicao);
	}

	/**
	 * Método responsável por obter Instalação
	 * Medidor.
	 *
	 * @param chavePrimariaInstalacaoMedidor the chave primaria instalacao medidor
	 * @return Um módulo do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public InstalacaoMedidor obterInstalacaoMedidorPorChave(long chavePrimariaInstalacaoMedidor) throws GGASException {

		return this.getControladorMedidor().obterInstalacaoMedidorPorChave(chavePrimariaInstalacaoMedidor);
	}

	/**
	 * Método responsável por obter Instalação
	 * Medidor.
	 *
	 * @param chavePrimariaCorretorVazao the chave primaria corretor vazao
	 * @return Um módulo do sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public InstalacaoMedidor obterInstalacaoMedidorPorCorretorVazao(long chavePrimariaCorretorVazao) throws GGASException {

		return this.getControladorMedidor().obterInstalacaoMedidorPorCorretorVazao(chavePrimariaCorretorVazao);
	}

	/**
	 * Método responsável por obter o cliente
	 * principal do imovel.
	 *
	 * @param idImovel
	 *            A chave primária do imóvel
	 * @return Um Cliente Imovel
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<ClienteImovel> obterClienteImovelPorChave(Long idImovel) throws GGASException {

		return getControladorImovel().obterClienteImovelPorChave(idImovel);
	}

	/**
	 * Método responsável por consultar o
	 * histórico de operação do medidor.
	 *
	 * @param chaveVazaoCorretor the chave vazao corretor
	 * @param chavePontoConsumo A chave Primária do ponto de
	 *            consumo.
	 * @return Uma coleção de histórico de
	 *         operação do medidor.
	 * @throws NegocioException the negocio exception
	 */

	public Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoOperacaoVazaoCorretor(Long chaveVazaoCorretor,
					Long chavePontoConsumo) throws NegocioException {

		return this.getControladorVazaoCorretor().consultarHistoricoOperacaoVazaoCorretor(chaveVazaoCorretor, chavePontoConsumo);
	}

	/**
	 * Consultar historico corretor vazao.
	 *
	 * @param chaveCorretorVazao the chave corretor vazao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoCorretorVazao(Long chaveCorretorVazao) throws GGASException {

		return this.getControladorVazaoCorretor().listarHistoricoCorretorVazao(chaveCorretorVazao);
	}

	/**
	 * Consultar historico corretor vazao movimentacao.
	 *
	 * @param chaveCorretorVazao the chave corretor vazao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<VazaoCorretorHistoricoMovimentacao> consultarHistoricoCorretorVazaoMovimentacao(Long chaveCorretorVazao)
					throws GGASException {

		return this.getControladorVazaoCorretor().listarVazaoCorretorHistoricoMovimentacao(chaveCorretorVazao);
	}

	/**
	 * Método responsável por verificar se a
	 * alteração dos dados de medição de
	 * temperatura medição pressão é permitida.
	 *
	 * @param temperaturaPressaoMedicao the temperatura pressao medicao
	 * @return true caso permitida a alteração.
	 * @throws GGASException the GGAS exception
	 */
	public boolean permitirAtualizacaoTemperaturaPressaoMedicao(TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws GGASException {

		return this.getControladorTemperaturaPressaoMedicao().permitirAtualizacaoTemperaturaPressaoMedicao(temperaturaPressaoMedicao);

	}

	/**
	 * Método responsável por validar os QDcs
	 * associados ao contrato.
	 *
	 * @param abaPrincipal the aba principal
	 * @param contrato Contrato a ser validado.
	 * @throws GGASException the GGAS exception
	 */
	public void validarDataQdcContrato(Map<String, Object> abaPrincipal, Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDataQdcContrato(abaPrincipal, contrato);
	}

	/**
	 * Metodo respons[avel por montar uma lista de
	 * pontos de consumo.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<PontoConsumo> listarPontosConsumo(Map<String, Object> filtro) throws GGASException {

		return this.getControladorPontoConsumo().listarPontosConsumo(filtro);
	}

	/**
	 * Método responsável por listar as operações
	 * de um medidor.
	 *
	 * @return lista de OperacaoMedidor
	 * @throws GGASException the GGAS exception
	 */
	public List<OperacaoMedidor> listarOperacaoMedidor() throws GGASException {

		return this.getControladorMedidor().listarOperacaoMedidor();
	}

	/**
	 * Método responsável por obter um
	 * OperacaoMedidor.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return OperacaoMedidor
	 * @throws GGASException the GGAS exception
	 */
	public OperacaoMedidor obterOperacaoMedidor(long chavePrimaria) throws GGASException {

		return this.getControladorMedidor().obterOperacaoMedidor(chavePrimaria);
	}

	/**
	 * Método responsável por retornar os tipos de
	 * associações.
	 *
	 * @return the map
	 */
	public Map<Integer, String> obterTiposAssociacoes() {

		return this.getControladorMedidor().obterTiposAssociacoes();
	}

	/**
	 * Método responsável por retornar se o codigo
	 * da associação é do tipo PontoConsumo /
	 * Medidor.
	 *
	 * @param codigoAssociacao the codigo associacao
	 * @return true, if is associacao ponto consumo medidor
	 */
	public boolean isAssociacaoPontoConsumoMedidor(Integer codigoAssociacao) {

		return this.getControladorMedidor().isAssociacaoPontoConsumoMedidor(codigoAssociacao);
	}

	/**
	 * Método responsável por retornar se o codigo
	 * da associação é do tipo PontoConsumo /
	 * Corretor
	 * de Vazão.
	 *
	 * @param codigoAssociacao the codigo associacao
	 * @return true, if is associacao ponto consumo corretor vazao
	 */
	public boolean isAssociacaoPontoConsumoCorretorVazao(Integer codigoAssociacao) {

		return this.getControladorMedidor().isAssociacaoPontoConsumoCorretorVazao(codigoAssociacao);
	}

	/**
	 * Método reponsável por retornar uma lista de
	 * medidorLocalInstalacao.
	 *
	 * @return lista de medidorLocalInstalacao
	 * @throws GGASException the GGAS exception
	 */
	public List<MedidorLocalInstalacao> listarMedidorLocalInstalacao() throws GGASException {

		return this.getControladorMedidor().listarMedidorLocalInstalacao();
	}

	/**
	 * Método reponsável por retornar uma lista de
	 * medidorProtecao.
	 *
	 * @return lista de medidorProtecao
	 * @throws GGASException the GGAS exception
	 */
	public List<MedidorProtecao> listarMedidorProtecao() throws GGASException {

		return this.getControladorMedidor().listarMedidorProtecao();
	}

	/**
	 * Método reponsável por retornar uma lista de
	 * MotivoOperacaoMedidor.
	 *
	 * @return lista de MotivoOperacaoMedidor
	 * @throws GGASException the GGAS exception
	 */
	public List<MotivoOperacaoMedidor> listarMotivoOperacaoMedidor() throws GGASException {

		return this.getControladorMedidor().listarMotivoOperacaoMedidor();
	}

	/**
	 * Método reponsável por retornar um
	 * InstalacaoMedidor.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @return the instalacao medidor
	 * @throws GGASException the GGAS exception
	 */
	public InstalacaoMedidor obterInstalacaoMedidorPontoConsumo(long chavePontoConsumo) throws GGASException {

		return this.getControladorMedidor().obterInstalacaoMedidorPontoConsumo(chavePontoConsumo);
	}

	/**
	 * Obtêm um ou mais medidores através do
	 * número de série.
	 *
	 * @param numeroSerie
	 *            Número de Série do medidor.
	 * @return Um medidor.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<Medidor> obterMedidorPorNumeroSerie(String numeroSerie) throws GGASException {

		return this.getControladorMedidor().obterMedidorPorNumeroSerie(numeroSerie);
	}

	/**
	 * Obtêm o corretor de vazão através do número
	 * de série.
	 *
	 * @param numeroSerie
	 *            Número de Série do corretor.
	 * @return Uma lista de corretores de vazão.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<VazaoCorretor> obterVazaoCorretorPorNumeroSerie(String numeroSerie) throws GGASException {

		return this.getControladorMedidor().obterVazaoCorretorPorNumeroSerie(numeroSerie);
	}

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosAssociacaoMedidor(Map<String, Object> dados) throws NegocioException {

		this.getControladorMedidor().validarDadosAssociacaoMedidor(dados);
	}

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	public void validarPontoConsumoPossuiMedidor(Map<String, Object> dados) throws NegocioException {

		this.getControladorMedidor().validarPontoConsumoPossuiMedidor(dados);
	}

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um corretor de vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosAssociacaoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		this.getControladorMedidor().validarDadosAssociacaoCorretorVazao(dados);
	}

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um corretor de vazao ao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		this.getControladorMedidor().validarDadosAssociacaoCorretorVazaoMedidorIndependente(dados);
	}

	/**
	 * Inserir dados associacao ponto consumo medidor.
	 *
	 * @param dados the dados
	 * @throws Exception the exception
	 */
	public void inserirDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados) throws GGASException {

		this.getControladorMedidor().inserirDadosAssociacaoPontoConsumoMedidor(dados);
	}

	/**
	 * Inserir dados associacao medidor independente.
	 *
	 * @param dados the dados
	 * @throws Exception the exception
	 */
	public void inserirDadosAssociacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		this.getControladorMedidor().inserirDadosAssociacaoMedidorIndependente(dados);
	}

	/**
	 * Inserir dados associacao ponto consumo corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void inserirDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException {

		this.getControladorMedidor().inserirDadosAssociacaoPontoConsumoCorretorVazao(dados);
	}

	/**
	 * Insere dados da associação de medidor independente ao corretor de vazão.
	 *
	 * @param dados the dados
	 * @throws Exception the exception
	 */
	public void inserirDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		this.getControladorMedidor().inserirDadosAssociacaoCorretorVazaoMedidorIndependente(dados);
	}

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de
	 * consumo e medidor / vazaoCorretor.
	 *
	 * @param codigoAssociacao the codigo associacao
	 * @param status the status
	 * @param medidor the medidor
	 * @param corretorVazao the corretor vazao
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<OperacaoMedidor> obterOperacoesMedidorPorTipoAssociacao(Integer codigoAssociacao, String status, String medidor,
					String corretorVazao) throws GGASException {

		return this.getControladorMedidor().obterOperacoesMedidorPorTipoAssociacao(codigoAssociacao, status, medidor, corretorVazao);
	}

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * imóvel e medidor / vazaoCorretor.
	 *
	 * @param tipoAssociacao the tipo associacao
	 * @param idImovel a chave primária do imóvel
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<OperacaoMedidor> obterOperacoesMedidorPorImovelCondonimioIndividual(Integer tipoAssociacao, Long idImovel)
			throws GGASException {

		return this.getControladorMedidor().obterOperacoesMedidorPorImovelCondonimioIndividual(tipoAssociacao, idImovel);
	}

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de
	 * medidor / vazaoCorretor.
	 *
	 * @param codigoAssociacao the codigo associacao
	 * @param status the status
	 * @param medidor the medidor
	 * @param corretorVazao the corretor vazao
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<OperacaoMedidor> obterOperacoesMedidorIndependentePorTipoAssociacao(Integer codigoAssociacao, String status,
					String medidor, String corretorVazao) throws GGASException {

		return this.getControladorMedidor().obterOperacoesMedidorIndependentePorTipoAssociacao(codigoAssociacao, status, medidor,
						corretorVazao);
	}

	/**
	 * Método responsável por retornar um mapa com
	 * as informaçoes necessárias para o
	 * funcionamento
	 * do ajax dp cronogramaFaturamento.
	 *
	 * @param idGrupoFaturamento id do grupo de faturamento para
	 *            realizar a pesquisa.
	 * @param idCronogramaFaturamento the id cronograma faturamento
	 * @param mesAnoPartida the mes ano partida
	 * @param cicloPartida the ciclo partida
	 * @return mapa com os dados necessários para
	 *         o componente ajax da tela de
	 *         cronograma
	 *         faturamento.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> mapearCamposGrupoFaturamento(Long idGrupoFaturamento, Long idCronogramaFaturamento, String mesAnoPartida,
					String cicloPartida) throws GGASException {

		return this.getControladorCronogramaFaturamento().mapearCamposGrupoFaturamento(idGrupoFaturamento, idCronogramaFaturamento,
						mesAnoPartida, cicloPartida);
	}

	/**
	 * Método responsável por validar os campos
	 * obrigatórios da função Gerar Cronograma do
	 * cronograma de faturamento;.
	 *
	 * @param dados dados a serem validados.
	 * @throws GGASException the GGAS exception
	 */
	public void validarCamposObrigatoriosGerarCronogramas(Map<String, Object> dados) throws GGASException {

		this.getControladorCronogramaFaturamento().validarCamposObrigatoriosGerarCronogramas(dados);
	}

	/**
	 * Método responsável por validar os campos
	 * obrigatórios da lista de atividades para a
	 * função
	 * Gerar Cronograma do cronograma de
	 * faturamento;.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param chavesPrimariasMarcadas the chaves primarias marcadas
	 * @param duracoes the duracoes
	 * @param datasPrevistas the datas previstas
	 * @throws GGASException the GGAS exception
	 */
	public void validarListaCamposObrigatoriosGerarCronogramas(Long[] chavesPrimarias, Long[] chavesPrimariasMarcadas, Integer[] duracoes,
					String[] datasPrevistas) throws GGASException {

		this.getControladorCronogramaFaturamento().validarListaCamposObrigatoriosGerarCronogramas(chavesPrimarias, chavesPrimariasMarcadas,
						duracoes, datasPrevistas);
	}

	/**
	 * Método responsável por validar se os novos
	 * cronogramas estão na capacidade de cadastro
	 * do
	 * registro.
	 *
	 * @param idGrupoFaturamento id do faturamento dos novos
	 *            cronogramas.
	 * @param quantidadeCronogramas quantidade de cronogramas a
	 *            serem cadastrados.
	 * @throws NegocioException the negocio exception
	 */
	public void validarCronogramas(Long idGrupoFaturamento, Integer quantidadeCronogramas) throws NegocioException {

		this.getControladorCronogramaFaturamento().validarCronogramas(idGrupoFaturamento, quantidadeCronogramas);
	}

	/**
	 * Método responsável por retornar uma lista
	 * de VO de cronograma atividade que será
	 * exibida na
	 * tela de gerar Cronogramas.
	 *
	 * @param chavesPrimarias chaves primárias das atividades
	 *            de sistema
	 * @param duracoes durações da cada atividade
	 *            sistema
	 * @param datasPrevistas datas previstas de cada
	 *            atividade sistema
	 * @param chavesPrimariasMarcadas lista de chaves primarias das
	 *            atividades sistema selecionadas
	 * @param mesAnoPartida mes/ano de partida de acordo com
	 *            o grupo de faturamento
	 * @param cicloPartida ciclo de partida do grupo de
	 *            faturamento
	 * @param qtdCronogramas quantidade de cronogramas de
	 *            faturamento a serem gerados
	 * @param idGrupoFaturamento chave do grupo de faturamento
	 * @param listaDatasAtividadesIniciais the lista datas atividades iniciais
	 * @param listaDatasAtividadesFinais the lista datas atividades finais
	 * @param isPrimeiroCronograma the is primeiro cronograma
	 * @return lista de VO de cronogramas
	 *         atividade sistema
	 * @throws NegocioException the negocio exception
	 */
	public Collection<CronogramaAtividadeSistemaVO> gerarListaCronogramaAtividade(Long[] chavesPrimarias, Integer[] duracoes,
					String[] datasPrevistas, Long[] chavesPrimariasMarcadas, String mesAnoPartida, String cicloPartida,
					Integer qtdCronogramas, Long idGrupoFaturamento, String[] listaDatasAtividadesIniciais,
					String[] listaDatasAtividadesFinais, Boolean isPrimeiroCronograma) throws NegocioException {

		return this.getControladorCronogramaFaturamento().gerarListaCronogramaAtividade(chavesPrimarias, duracoes, datasPrevistas,
						chavesPrimariasMarcadas, mesAnoPartida, cicloPartida, qtdCronogramas, idGrupoFaturamento,
						listaDatasAtividadesIniciais, listaDatasAtividadesFinais, isPrimeiroCronograma);

	}

	/**
	 * Método para validar se o ano mês
	 * faturamento informado é maior ou igual ao
	 * parâmetro do
	 * sistema.
	 *
	 * @param mesAnoPartida the mes ano partida
	 * @param cicloPartida the ciclo partida
	 * @param grupoFaturamento the grupo faturamento
	 * @throws NegocioException the negocio exception
	 */
	public void validarAnoMesFaturamento(String mesAnoPartida, String cicloPartida, GrupoFaturamento grupoFaturamento)
					throws NegocioException {

		this.getControladorCronogramaFaturamento().validarAnoMesFaturamento(mesAnoPartida, cicloPartida, grupoFaturamento);
	}

	/**
	 * Método responsável por validar se existe
	 * cronograma rota realizado para um
	 * cronograma de
	 * faturamento.
	 *
	 * @param idCronogramaFaturamento the id cronograma faturamento
	 * @throws NegocioException the negocio exception
	 */
	public void validarCronogramaRealizado(Long idCronogramaFaturamento) throws NegocioException {

		this.getControladorCronogramaFaturamento().validarCronogramaRealizado(idCronogramaFaturamento);
	}

	/**
	 * Método responsável por atualizar a
	 * listaCronogramaAtividadeSistemaVO após
	 * aplicar alteração
	 * nas datas.
	 *
	 * @param listaCronogramaAtividadeSistemaVO the lista cronograma atividade sistema vo
	 * @param cronogramaAtividadeSistemaVO the cronograma atividade sistema vo
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarListaCronogramaAtividadeSistema(Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO,
					CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, Long idGrupoFaturamento) throws NegocioException {

		this.getControladorCronogramaFaturamento().atualizarListaCronogramaAtividadeSistema(listaCronogramaAtividadeSistemaVO,
						cronogramaAtividadeSistemaVO, idGrupoFaturamento);
	}

	/**
	 * Método responsável por atualizar o
	 * cronogramaFaturamento recebendo um VO de
	 * cronogramaAtividadeSistema como informação.
	 *
	 * @param cronogramaAtividadeSistemaVO the cronograma atividade sistema vo
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarCronogramaFaturamento(CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorCronogramaFaturamento().atualizarCronogramaFaturamento(cronogramaAtividadeSistemaVO, dadosAuditoria);
	}

	/**
	 * Método reponsável por inserir o Cronograma
	 * Faturamento recebendo uma lista do VO
	 * CronogramaSistema.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @param listaCronogramaAtividadeSistemaVO the lista cronograma atividade sistema vo
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void inserirCronogramaFaturamento(Long idGrupoFaturamento,
					Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorCronogramaFaturamento().inserirCronogramaFaturamento(idGrupoFaturamento, listaCronogramaAtividadeSistemaVO,
						dadosAuditoria);
	}

	/**
	 * Consultar cronogramas rotas por atividade e cronograma faturamento.
	 *
	 * @param idAtividadeSistema the id atividade sistema
	 * @param idCronogramaFaturamento the id cronograma faturamento
	 * @return the collection
	 */
	public Collection<CronogramaRota> consultarCronogramasRotasPorAtividadeECronogramaFaturamento(Long idAtividadeSistema,
					Long idCronogramaFaturamento) {

		return this.getControladorRota().consultarCronogramasRotasPorAtividadeECronogramaFaturamento(idAtividadeSistema,
						idCronogramaFaturamento);
	}

	/**
	 * Método para recuperar a Atividade de
	 * Sistema a partir da chave primária.
	 *
	 * @param chavePrimaria chave primária da atividade de
	 *            sistema
	 * @return AtividadeSistema a Atividade de
	 *         sistema
	 * @throws GGASException the GGAS exception
	 */
	public AtividadeSistema obterAtividadeSistema(long chavePrimaria) throws GGASException {

		return this.getControladorCronogramaFaturamento().obterAtividadeSistema(chavePrimaria);
	}

	/**
	 * Método responsável por consultar o último
	 * Cronograma Rota a partir de uma Rota e da
	 * Atividade
	 * de Sistema.
	 *
	 * @param idRota the id rota
	 * @param idAtividadeSistema the id atividade sistema
	 * @param referenciaCicloAnterior the referencia ciclo anterior
	 * @return the cronograma rota
	 */
	public CronogramaRota consultarCronogramaRotaAtividade(Long idRota, Long idAtividadeSistema,
					Map<String, Integer> referenciaCicloAnterior) {

		return this.getControladorRota().consultarCronogramaRotaAtividade(idRota, idAtividadeSistema, referenciaCicloAnterior);
	}

	/**
	 * Método responsável por remover um
	 * cronograma de faturamento, e suas
	 * dependências como
	 * cronogramas de atividade de faturamento e
	 * cronogramas rota, caso estejam de acordo
	 * com a regra de negócio.
	 *
	 * @param idCronogramaFaturamento the id cronograma faturamento
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerCronogramaFaturamento(Long idCronogramaFaturamento, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorCronogramaFaturamento().removerCronogramaFaturamento(idCronogramaFaturamento, dadosAuditoria);
	}

	/**
	 * Método responsável por informar se houve ou
	 * alteração em um contrato agrupado por valor
	 * para
	 * por volume.
	 *
	 * @param contrato the contrato
	 * @return true, if is alteracao contrato agrupado por valor para por volume
	 * @throws GGASException the GGAS exception
	 */
	public boolean isAlteracaoContratoAgrupadoPorValorParaPorVolume(Contrato contrato) throws GGASException {

		return this.getControladorContrato().isAlteracaoContratoAgrupadoPorValorParaPorVolume(contrato);
	}

	/**
	 * Listar ciclos.
	 *
	 * @return Collection de Ciclos possíveis do
	 *         sistema
	 */
	public Collection<Integer> listarCiclos() {

		return this.getControladorRota().listarCiclos();
	}

	/**
	 * Método responsável por validar se
	 * obrigatoriedade do email referente a
	 * atividade selecionada.
	 *
	 * @param processo the processo
	 * @throws NegocioException Caso o email responsavel do
	 *             processo for null ou string
	 *             vazia
	 */
	public void validarEmailObrigatorioAtividadeSelecionada(Processo processo) throws NegocioException {

		this.getControladorProcesso().validarEmailObrigatorioAtividadeSelecionada(processo);
	}

	/**
	 * Método responsável por criar o
	 * cronogramaRota para a Rota inserida.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @param idRota the id rota
	 * @throws NegocioException the negocio exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public void processarInclusaoRota(Long idGrupoFaturamento, Long idRota) throws NegocioException, IllegalAccessException,
					InvocationTargetException {

		this.getControladorCronogramaFaturamento().processarInclusaoRota(idGrupoFaturamento, idRota);
	}

	/**
	 * Método responsável por alterar o
	 * cronogramaRota para a Rota excluída.
	 *
	 * @param rotaAntiga the rota antiga
	 * @param rota the rota
	 * @throws NegocioException the negocio exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 */
	public void processarAlteracaoRota(Rota rotaAntiga, Rota rota) throws NegocioException, IllegalAccessException,
					InvocationTargetException {

		this.getControladorCronogramaFaturamento().processarAlteracaoRota(rotaAntiga, rota);
	}

	/**
	 * Método responsável por obter a quantidade
	 * de volumes de um contrato.
	 *
	 * @param filtro the filtro
	 * @return the quantidade volumes vo
	 * @throws GGASException the GGAS exception
	 */
	public QuantidadeVolumesVO obterQuantidadesVolumesVO(Map<String, Object> filtro) throws GGASException {

		return this.getControladorContrato().obterQuantidadesVolumesVO(filtro);
	}

	/**
	 * Obter pontos consumo contrato.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<PontoConsumo> obterPontosConsumoContrato(Map<String, Object> filtro) throws GGASException {

		return this.getControladorContrato().obterPontosConsumoContrato(filtro);
	}

	/**
	 * Obter historico aditivos.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<Contrato> obterHistoricoAditivos(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorContrato().obterHistoricoAditivos(filtro);
	}

	/**
	 * Método responsável por consultar a data de
	 * leitura do último dia do ciclo anterior.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param anoMesLeitura the ano mes leitura
	 * @param numeroCiclo the numero ciclo
	 * @return dataLeituraUltimoDiaCicloAnterior
	 * @throws GGASException the GGAS exception
	 */
	public Date consultarDataLeituraUltimoDiaCicloAnterior(PontoConsumo pontoConsumo, Integer anoMesLeitura, Integer numeroCiclo)
					throws GGASException {

		return this.getControladorHistoricoMedicao().consultarDataLeituraUltimoDiaCicloAnterior(pontoConsumo, anoMesLeitura, numeroCiclo);
	}

	/**
	 * Método para retornar um array com as
	 * situações possiveis do cronograma.
	 *
	 * @return the string[]
	 */
	public String[] obterListaSituacao() {

		return this.getControladorCronogramaFaturamento().obterListaSituacao();
	}

	/**
	 * Método responsável por pesquisar todos os
	 * imoveis que possuem historico de leitura e
	 * consumo.
	 *
	 * @param filtro the filtro
	 * @return imoveis
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Imovel> pesquisarImovelHistoricoLeituraConsumo(Map<String, Object> filtro) throws GGASException {

		return this.getControladorHistoricoConsumo().pesquisarImovelHistoricoLeituraConsumo(filtro);
	}

	/**
	 * Método responsável por listar as bases de
	 * apuração por penalidade.
	 *
	 * @return basesApuracaoPenalidadeMaiorMenor
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarBasesApuracaoPenalidadeMaiorMenor() throws GGASException {

		return this.getControladorEntidadeConteudo().listarBasesApuracaoPenalidadeMaiorMenor();
	}

	/**
	 * Carrega o contrato selecionado para
	 * encerramento ou recisão.
	 *
	 * @param idContrato Identificador do contrato.
	 * @return contrato
	 * @throws NegocioException the negocio exception
	 */
	public Contrato carregarContratoEncerramentoRescisao(Long idContrato) throws NegocioException {

		return this.getControladorContrato().carregarContratoEncerramentoRescisao(idContrato);
	}

	/**
	 * Calcula o valor total da indenizacao de
	 * recisao a partir dos valores informados.
	 *
	 * @param dadosCalculo VO com os valores para o
	 *            cálculo, onde o resultado é
	 *            armazenado.
	 * @throws NegocioException the negocio exception
	 */
	public void calcularIndenizacaoRescisao(CalculoIndenizacaoVO dadosCalculo) throws NegocioException {

		this.getControladorContrato().calcularIndenizacaoRescisao(dadosCalculo);
	}

	/**
	 * Recupera o valor do indice financeiro
	 * encontrado ou null.
	 *
	 * @param dataReferencia Mês/ano de referência do indice
	 *            a recuperar.
	 *            * @param descricaoAbreviada
	 *            Descricao abreviada do indice
	 *            cujo valor deve ser
	 *            recuperado.
	 * @param descricaoAbreviada the descricao abreviada
	 * @return valorIndice
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal buscarValorIndiceFinanceiroNominal(DateTime dataReferencia, String descricaoAbreviada) throws NegocioException {

		return this.getControladorContrato().buscarValorIndiceFinanceiroNominal(dataReferencia, descricaoAbreviada);
	}

	/**
	 * Retorna o período de consumo em meses.
	 *
	 * @param datainvestimento the datainvestimento
	 * @param dataRecisao the data recisao
	 * @return mesesPeriodoConsumo
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal calcularMesesPeriodoConsumo(DateTime datainvestimento, DateTime dataRecisao) throws NegocioException {

		return this.getControladorContrato().calcularMesesPeriodoConsumo(datainvestimento, dataRecisao);
	}

	/**
	 * Método responsável por obter a lista de
	 * tipos de multa recisoria.
	 *
	 * @return coleção de contrato de compra
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> obterListaTipoMultaRecisoria() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaTipoMultaRecisoria();
	}

	/**
	 * Monta e carrega um VO contendo os valores
	 * usados no cálculo da indenização de recisão
	 * do
	 * contrato alvo.
	 *
	 * @param contrato Contrato alvo.
	 * @param idPontoConsumo the id ponto consumo
	 * @return dadosCalculo
	 * @throws NegocioException the negocio exception
	 */
	public CalculoIndenizacaoVO prepararCalculoIndenizacaoRescisao(Contrato contrato, Long idPontoConsumo) throws NegocioException {

		return this.getControladorContrato().prepararCalculoIndenizacaoRescisao(contrato, idPontoConsumo);
	}

	/**
	 * Cancela uma nota de crédito débito do
	 * cliente referente à rubrica informada.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param rubrica Tipo de rubrica do débito
	 *            desejado.
	 * @param dadosAuditoria .
	 * @param isRefaturamento .
	 * @throws GGASException the GGAS exception
	 */
	public void cancelarNotaCreditoDebitoAnteriorPorClienteRubrica(Contrato contrato, PontoConsumo pontoConsumo, Rubrica rubrica,
					DadosAuditoria dadosAuditoria, boolean isRefaturamento) throws GGASException {

		this.getControladorFatura().cancelarNotaCreditoDebitoAnteriorPorClienteRubrica(contrato, pontoConsumo, rubrica, dadosAuditoria,
						isRefaturamento);
	}

	/**
	 * Cria uma cópia do contrato informado
	 * desvinculada do hinernate.
	 *
	 * @param contrato Contrato base.
	 * @param dadosAuditoria the dados auditoria
	 * @return cloneContrato
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InstantiationException the instantiation exception
	 * @throws NoSuchMethodException the no such method exception
	 */
	public Contrato popularAditarContrato(Contrato contrato, DadosAuditoria dadosAuditoria) throws IllegalAccessException,
					InvocationTargetException, InstantiationException, NoSuchMethodException {

		return this.getControladorContrato().popularAditarContrato(contrato, dadosAuditoria);
	}

	/**
	 * Recuperar contrato com as listas tudo
	 * carregadas.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param habilitado the habilitado
	 * @return contrato
	 */
	public Contrato obterContratoListasCarregadas(long chavePrimaria, Boolean habilitado) {

		return this.getControladorContrato().obterContratoListasCarregadas(chavePrimaria, habilitado);
	}

	/**
	 * Método responsável por listar todas as
	 * penalidades.
	 *
	 * @return listarPenalidades
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Penalidade> listarPenalidades() throws GGASException {

		return this.getControladorApuracaoPenalidade().listarPenalidades();
	}

	/**
	 * Método responsável por listar pontos de
	 * consumo.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumoVO> pesquisarPontoConsumoPenalidade(Map<String, Object> filtro) throws GGASException {

		return this.getControladorApuracaoPenalidade().pesquisarPontoConsumoPenalidade(filtro);
	}

	/**
	 * Método responsável por listar
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 *
	 * @param filtro the filtro
	 * @return listaApuracaoQuantidadePenalidadePeriodicidade
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade>
					listarApuracaoQuantidadePenalidadePeriodicidades(Map<String, Object> filtro) throws GGASException {

		return this.getControladorApuracaoPenalidade().listarApuracaoQuantidadePenalidadePeriodicidades(filtro);
	}

	/**
	 * Método para pesquisar as apurações
	 * penalidades a partir da tela de pesquisa.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<Object[]> listarApuracaoQuantidadeVO(Map<String, Object> filtro) throws GGASException {

		return this.getControladorApuracaoPenalidade().listarApuracaoQuantidadeVO(filtro);
	}

	/**
	 * método para retornar uma lista de
	 * ApuracaoQuantidadeVO para exibição na tela.
	 *
	 * @param listaObjetos the lista objetos
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ApuracaoQuantidadeVO> montarListaApuracaoQuantidadeVO(List<Object[]> listaObjetos) throws GGASException {

		return this.getControladorApuracaoPenalidade().montarListaApuracaoQuantidadeVO(listaObjetos);
	}

	/**
	 * Validar datas consultar extrato.
	 *
	 * @param dataIni the data ini
	 * @param dataFim the data fim
	 * @throws GGASException the GGAS exception
	 */
	public void validarDatasConsultarExtrato(String dataIni, String dataFim) throws GGASException {

		this.getControladorContrato().validarDatasConsultarExtrato(dataIni, dataFim);
	}

	/**
	 * Método responsável por obter
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return apuracaoQuantidadePenalidadePeriodicidade
	 * @throws GGASException the GGAS exception
	 */
	public ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long chavePrimaria)
					throws GGASException {

		return this.getControladorApuracaoPenalidade().obterApuracaoQuantidadePenalidadePeriodicidade(chavePrimaria);
	}

	/**
	 * Método responsável por obter
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 *
	 * @param idContrato the id contrato
	 * @param idPenalidade the id penalidade
	 * @param idPontoConsumo the id ponto consumo
	 * @param periodoApuracao the periodo apuracao
	 * @param idModalidade the id modalidade
	 * @return apuracaoQuantidadePenalidadePeriodicidade
	 * @throws GGASException the GGAS exception
	 */
	public ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long idContrato, Long idPenalidade,
					Long idPontoConsumo, Integer periodoApuracao, Long idModalidade) throws GGASException {

		return this.getControladorApuracaoPenalidade().obterApuracaoQuantidadePenalidadePeriodicidade(idContrato, idPenalidade,
						idPontoConsumo, periodoApuracao, idModalidade);
	}

	/**
	 * Método responsável por obter
	 * ContratoPontoConsumoModalidade.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param idContrato the id contrato
	 * @param idModalidade the id modalidade
	 * @return ContratoPontoConsumoModalidade
	 * @throws GGASException the GGAS exception
	 */
	public ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(Long idPontoConsumo, Long idContrato, Long idModalidade)
					throws GGASException {

		return this.getControladorContrato().obterContratoPontoConsumoModalidade(idPontoConsumo, idContrato, idModalidade);
	}

	/**
	 * Método responsável por obter
	 * ContratoPontoConsumoPenalidade.
	 *
	 * @param idContratoPontoConsumoModalidade the id contrato ponto consumo modalidade
	 * @param idPenalidade the id penalidade
	 * @param idPeriodicidadePenalidade the id periodicidade penalidade
	 * @param dataInicioApuracao , Date dataFinalApuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return ContratoPontoConsumoPenalidade
	 * @throws GGASException the GGAS exception
	 */
	public ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidade(Long idContratoPontoConsumoModalidade, Long idPenalidade,
					Long idPeriodicidadePenalidade, Date dataInicioApuracao, Date dataFinalApuracao) throws GGASException {

		return this.getControladorContrato().obterContratoPontoConsumoPenalidade(idContratoPontoConsumoModalidade, idPenalidade,
						idPeriodicidadePenalidade, dataInicioApuracao, dataFinalApuracao);
	}

	/**
	 * Método responsável por obter uma lista de
	 * QuandidadeContratadaVO.
	 *
	 * @param apuracao the apuracao
	 * @return lista de QuandidadeContratadaVO
	 * @throws GGASException the GGAS exception
	 */
	public Collection<QuantidadeContratadaVO> obterQuantidadeContratrada(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
					throws GGASException {

		return this.getControladorApuracaoPenalidade().obterQuantidadeContratrada(apuracao);
	}

	/**
	 * Método responsável por obter uma lista de
	 * QuantidadeRetiradaPeriodoVO.
	 *
	 * @param apuracao the apuracao
	 * @return lista de
	 *         QuantidadeRetiradaPeriodoVO
	 * @throws GGASException the GGAS exception
	 */
	public Collection<QuantidadeRetiradaPeriodoVO> obterQuantidadeRetiradaPeriodo(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
					throws GGASException {

		return this.getControladorApuracaoPenalidade().obterQuantidadeRetiradaPeriodo(apuracao);
	}

	/**
	 * Método responsável por obter uma lista de
	 * QuantidadeNaoRetiradaVO.
	 *
	 * @param apuracao the apuracao
	 * @param chavePrimariaTipoParada the chave primaria tipo parada
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<QuantidadeNaoRetiradaVO> obterQuantidadeNaoRetirada(ApuracaoQuantidadePenalidadePeriodicidade apuracao,
					long chavePrimariaTipoParada) throws GGASException {

		return this.getControladorApuracaoPenalidade().obterQuantidadeNaoRetirada(apuracao, chavePrimariaTipoParada);
	}

	/**
	 * Método responsável por listar as paradas
	 * programadas.
	 *
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @param chavesTipoParada the chaves tipo parada
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return lista de paradas programadas
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ParadaProgramada> listarParadasProgramadas(Long[] chavesPontoConsumo, Long[] chavesTipoParada, Date dataInicial,
					Date dataFinal) throws GGASException {

		return this.getControladorProgramacao().consultarParadasProgramadasPeriodo(chavesPontoConsumo, chavesTipoParada, dataInicial,
						dataFinal, null);
	}

	/**
	 * Listar apuracao quantidade penalidade periodicidades.
	 *
	 * @param periodo the periodo
	 * @param idPontoConsumo the id ponto consumo
	 * @param idPenalidade the id penalidade
	 * @param contrato the contrato
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidades(Integer periodo,
					long idPontoConsumo, long idPenalidade, Contrato contrato) throws GGASException {

		return this.getControladorApuracaoPenalidade().listarApuracaoQuantidadePenalidadePeriodicidades(periodo, idPontoConsumo,
						idPenalidade, contrato);
	}

	/**
	 * Método responsável por calcular o valor de
	 * desconto sobre o valor calculado.
	 *
	 * @param valorTotalFormatado the valor total formatado
	 * @param percentualDescontoFormatado the percentual desconto formatado
	 * @param valorFinalFormatado the valor final formatado
	 * @param totalValorFinalFormatado the total valor final formatado
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] aplicarPercentualDescontoSobreValorTotal(String valorTotalFormatado, String percentualDescontoFormatado,
					String valorFinalFormatado, String totalValorFinalFormatado) throws GGASException {

		return this.getControladorApuracaoPenalidade().aplicarPercentualDescontoSobreValorTotal(valorTotalFormatado,
						percentualDescontoFormatado, valorFinalFormatado, totalValorFinalFormatado);
	}

	/**
	 * Método responsável por calcular o
	 * percentual de desconto sobre o valor
	 * calculado.
	 *
	 * @param valorTotalFormatado the valor total formatado
	 * @param valorDescontoFormatado the valor desconto formatado
	 * @param valorFinalFormatado the valor final formatado
	 * @param totalValorFinalFormatado the total valor final formatado
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] aplicarValorDescontoSobreValorTotal(String valorTotalFormatado, String valorDescontoFormatado,
					String valorFinalFormatado, String totalValorFinalFormatado) throws GGASException {

		return this.getControladorApuracaoPenalidade().aplicarValorDescontoSobreValorTotal(valorTotalFormatado, valorDescontoFormatado,
						valorFinalFormatado, totalValorFinalFormatado);
	}

	/**
	 * Apurar.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param periodoIncial the periodo incial
	 * @param periodoFinal the periodo final
	 * @throws NegocioException the negocio exception
	 */
	public void apurar(long contrato, long pontoConsumo, Date periodoIncial, Date periodoFinal) throws NegocioException {

		this.getControladorApuracaoPenalidade().apuracaoAdapter(contrato, pontoConsumo, periodoIncial, periodoFinal);

	}

	/**
	 * Tratar penalidades.
	 *
	 * @param contrato the contrato
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param periodoIncial the periodo incial
	 * @param periodoFinal the periodo final
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void tratarPenalidades(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date periodoIncial, Date periodoFinal,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		this.getControladorApuracaoPenalidade().tratarPenalidades(contrato, listaPontoConsumo, periodoIncial, periodoFinal, dadosAuditoria,
						null);
	}

	/**
	 * Método responsável por consultar os
	 * LancamentoContabilSintetico apartir de um
	 * filtro.
	 *
	 * @param filtro o filtro de pesquisa
	 * @return os LancamentoContabilSintetico que
	 *         se enquadram nos parâmetros
	 *         passados no filtro
	 */
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSintetico(Map<String, Object> filtro) {

		return this.getControladorContabil().consultarLancamentoContabilSintetico(filtro);
	}

	/**
	 * Consultar lancamento contabil sintetico por faturamento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<LancamentoContabilSinteticoVO> consultarLancamentoContabilSinteticoPorFaturamento(Map<String, Object> filtro)
					throws NegocioException {

		return this.getControladorContabil().consultarLancamentoContabilSinteticoPorFaturamento(filtro);
	}

	/**
	 * Consultar lancamento contabil sintetico por recebimento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<LancamentoContabilSintetico> consultarLancamentoContabilSinteticoPorRecebimento(Map<String, Object> filtro)
					throws NegocioException {

		return this.getControladorContabil().consultarLancamentoContabilSinteticoPorRecebimento(filtro);
	}

	/**
	 * Método responsavel por obter lancmento
	 * contabil sintetico.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the lancamento contabil sintetico
	 * @throws NegocioException the negocio exception
	 */
	public LancamentoContabilSintetico obterLancamentoContabilSintetico(Long chavePrimaria) throws NegocioException {

		return this.getControladorContabil().obterLancamentoContabilSintetico(chavePrimaria);
	}

	/**
	 * Obter evento comercial lancamento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the evento comercial lancamento
	 * @throws NegocioException the negocio exception
	 */
	public EventoComercialLancamento obterEventoComercialLancamento(long chavePrimaria) throws NegocioException {

		return this.getControladorContabil().obterEventoComercialLancamento(chavePrimaria);
	}

	/**
	 * Atualizar evento comercial lancamento.
	 *
	 * @param evento the evento
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarEventoComercialLancamento(EventoComercialLancamento evento) throws NegocioException, ConcorrenciaException {

		this.getControladorContabil().atualizarEventoComercialLancamento(evento);
	}

	/**
	 * Remover eventos comerciais lancamento.
	 *
	 * @param lista the lista
	 * @param listaCompleta the lista completa
	 * @throws NegocioException the negocio exception
	 */
	public void removerEventosComerciaisLancamento(Collection<EventoComercialLancamento> lista,
					Collection<EventoComercialLancamento> listaCompleta) throws NegocioException {

		this.getControladorContabil().removerEventosComerciaisLancamento(lista, listaCompleta);
	}

	/**
	 * Obter conta contabil.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the conta contabil
	 * @throws NegocioException the negocio exception
	 */
	public ContaContabil obterContaContabil(long chavePrimaria) throws NegocioException {

		return this.getControladorContaContabil().obterContaContabil(chavePrimaria);
	}

	/**
	 * Método responsável por consultarLancamento
	 * ContabilAnalitico Por
	 * LancamentoContabilSintetico.
	 *
	 * @param lancamentoContabilSintetico the lancamento contabil sintetico
	 * @return the collection
	 */
	public Collection<LancamentoContabilAnalitico> consultarLancamentoContabilAnaliticoPorLancamentoContabilSintetico(
					LancamentoContabilSintetico lancamentoContabilSintetico) {

		return this.getControladorContabil()
						.consultarLancamentoContabilAnaliticoPorLancamentoContabilSintetico(lancamentoContabilSintetico);
	}

	/**
	 * Métod responsável por obter evento
	 * comercial.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the evento comercial
	 * @throws NegocioException the negocio exception
	 */
	public EventoComercial obterEventoComercial(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.getControladorContabil().obterEventoComercial(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Métod responsável por obter a chave do
	 * evento comercial.
	 *
	 * @param codigoEventoComercial the codigo evento comercial
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	public Long obterChaveEventoComercial(String codigoEventoComercial) throws NegocioException {

		return this.getControladorContabil().obterChaveEventoComercial(codigoEventoComercial);
	}

	/**
	 * Método responsável por obter todos os
	 * eventos comerciais.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<EventoComercial> consultarTodosEventosComerciais(Map<String, Object> filtro) {

		return this.getControladorContabil().consultarTodosEventosComerciais(filtro);
	}

	/**
	 * Consultar todos eventos comerciais por filtro.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<EventoComercial> consultarTodosEventosComerciaisPorFiltro(Map<String, Object> filtro) {

		return this.getControladorContabil().consultarTodosEventosComerciaisPorFiltro(filtro);
	}

	/**
	 * Remover evento comercial lancamento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @throws NegocioException the negocio exception
	 */
	public void removerEventoComercialLancamento(Long chavePrimaria) throws NegocioException {

		this.getControladorContabil().removerEventoComercialLancamento(chavePrimaria);
	}

	/**
	 * Criar evento comercial lancamento.
	 *
	 * @return the evento comercial lancamento
	 */
	public EventoComercialLancamento criarEventoComercialLancamento() {

		return this.getControladorContabil().criarEventoComercialLancamento();
	}

	/**
	 * Validar novo evento comercial lancamento.
	 *
	 * @param lancamento the lancamento
	 * @param lista the lista
	 * @throws NegocioException the negocio exception
	 */
	public void validarNovoEventoComercialLancamento(EventoComercialLancamento lancamento, Collection<EventoComercialLancamento> lista)
					throws NegocioException {

		this.getControladorContabil().validarNovoEventoComercialLancamento(lancamento, lista);
	}

	/**
	 * Validar alteracao evento comercial lancamento.
	 *
	 * @param lancamento the lancamento
	 * @param lista the lista
	 * @throws NegocioException the negocio exception
	 */
	public void
					validarAlteracaoEventoComercialLancamento(EventoComercialLancamento lancamento,
									Collection<EventoComercialLancamento> lista) throws NegocioException {

		this.getControladorContabil().validarAlteracaoEventoComercialLancamento(lancamento, lista);
	}

	/**
	 * Método responsável por criar uma
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 *
	 * @return ApuracaoQuantidadePenalidadePeriodicidade
	 */
	public ApuracaoQuantidadePenalidadePeriodicidade criarApuracaoQuantidadePenalidadePeriodicidade() {

		return this.getControladorApuracaoPenalidade().criarApuracaoQuantidadePenalidadePeriodicidade();
	}

	/**
	 * Método responsável por aplicar as
	 * penalidades.
	 *
	 * @param apuracoes the apuracoes
	 * @param dadosAuditoria the dados auditoria
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] aplicarPenalidade(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
					throws GGASException {

		return this.getControladorApuracaoPenalidade().aplicarPenalidade(apuracoes, dadosAuditoria);
	}

	/**
	 * Método responsável por validar os
	 * parametros do filtro de pesquisa de
	 * penalidades do extrato
	 * de execução do contrato.
	 *
	 * @param filtro the filtro
	 * @throws GGASException the GGAS exception
	 */
	public void validarFiltroPesquisaPenalidadesExtratoExecucaoContrato(Map<String, Object> filtro) throws GGASException {

		this.getControladorApuracaoPenalidade().validarFiltroPesquisaPenalidadesExtratoExecucaoContrato(filtro);
	}

	/**
	 * Método para obter o último historico
	 * consumo faturado.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the historico consumo
	 * @throws GGASException the GGAS exception
	 */
	public HistoricoConsumo obterUltimoHistoricoConsumoFaturado(Long idPontoConsumo) throws GGASException {

		return this.getControladorHistoricoConsumo().obterUltimoHistoricoConsumoFaturado(idPontoConsumo);
	}

	/**
	 * Obter lista penalidas por contrato.
	 *
	 * @param idContrato the id contrato
	 * @return the collection
	 */
	public Collection<ContratoPontoConsumoPenalidade> obterListaPenalidasPorContrato(Long idContrato) {

		return this.getControladorApuracaoPenalidade().obterListaPenalidasPorContrato(idContrato);
	}

	/**
	 * Teste consulta.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Contrato> testeConsulta() throws NegocioException {

		return this.getControladorContrato().obterListaContratoComPenalidades();
	}

	/**
	 * Método responsável por validar a data de
	 * vencimento da nota de débito ou crédito.
	 *
	 * @param dataVencimento Data de vencimento que será
	 *            validada.
	 * @throws NegocioException the negocio exception
	 */
	public void validarDataVencimento(Date dataVencimento) throws NegocioException {

		this.getControladorContrato().validarDataVencimento(dataVencimento);
	}

	/**
	 * Ponto consumo possui nota debito recisao.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param contrato the contrato
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long pontoConsumoPossuiNotaDebitoRecisao(Long idPontoConsumo, Contrato contrato) throws GGASException {

		return this.getControladorFatura().pontoConsumoPossuiNotaDebitoRecisao(idPontoConsumo, contrato);
	}

	/**
	 * Verificar encerramento.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param contrato the contrato
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long verificarEncerramento(Long idPontoConsumo, Contrato contrato) throws GGASException {

		return this.getControladorFatura().verificarEncerramento(idPontoConsumo, contrato);
	}

	/**
	 * Registrar nota debito multa recisoria.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param valorMulta the valor multa
	 * @param dataVencimento the data vencimento
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	public Pair<byte[], Fatura> registrarNotaDebitoMultaRecisoria(Contrato contrato, PontoConsumo pontoConsumo, BigDecimal valorMulta,
					String dataVencimento) throws GGASException {

		return this.getControladorFatura().registrarNotaDebitoMultaRecisoria(contrato, pontoConsumo, valorMulta, dataVencimento);
	}

	/**
	 * Validar encerrar rescindir contrato.
	 *
	 * @param chaveContrato the chave contrato
	 * @throws GGASException the GGAS exception
	 */
	public void validarEncerrarRescindirContrato(Long chaveContrato) throws GGASException {

		this.getControladorContrato().validarEncerrarRescindirContrato(chaveContrato);
	}

	/**
	 * Processa as penalidades associadas ao
	 * contrato informado.
	 *
	 * @param contrato the contrato
	 * @param pontosAProcessar the pontos a processar
	 * @param dadosAuditoria the dados auditoria
	 * @param logProcessamento the log processamento
	 * @throws GGASException the GGAS exception
	 */
	public void processarPenalidadesContrato(Contrato contrato, Collection<PontoConsumo> pontosAProcessar, DadosAuditoria dadosAuditoria,
					StringBuilder logProcessamento) throws GGASException {

		this.getControladorApuracaoPenalidade().processarPenalidadesContrato(contrato, pontosAProcessar, dadosAuditoria, logProcessamento);
	}

	/**
	 * Metodo responsavel por processar uma
	 * coleção de penalidades.
	 *
	 * @param apuracoes the apuracoes
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void processarPenalidades(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorApuracaoPenalidade().processarPenalidades(apuracoes, dadosAuditoria);
	}

	/**
	 * Consultar contrato ativo por contrato pai.
	 *
	 * @param chaveContratoPai the chave contrato pai
	 * @return the contrato
	 * @throws NegocioException the negocio exception
	 */
	public Contrato consultarContratoAtivoPorContratoPai(Long chaveContratoPai) throws NegocioException {

		return this.getControladorContrato().consultarContratoAtivoPorContratoPai(chaveContratoPai);
	}

	/**
	 * Método responsável por atualizar os dados
	 * das solicitacoes de consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param mapa the mapa
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Map<String, String>> atualizarDadosSolicitacoesConsumo(Long idPontoConsumo, Map<String, Map<String, String>> mapa)
					throws GGASException {

		return this.getControladorProgramacao().atualizarDadosSolicitacoesConsumo(idPontoConsumo, mapa);
	}

	/**
	 * Método para verificar se a fatura está com
	 * situação pendente.
	 *
	 * @param idFatura the id fatura
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	public boolean verificarFaturaSituacaoPendente(Long idFatura) throws GGASException {

		return this.getControladorFatura().verificarFaturaSituacaoPendente(idFatura);
	}

	/**
	 * Popular encerrar contrato.
	 *
	 * @param contrato the contrato
	 * @param dadosAuditoria the dados auditoria
	 * @return the contrato
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InstantiationException the instantiation exception
	 * @throws NoSuchMethodException the no such method exception
	 */
	public Contrato popularEncerrarContrato(Contrato contrato, DadosAuditoria dadosAuditoria) throws IllegalAccessException,
					InvocationTargetException, InstantiationException, NoSuchMethodException {

		return this.getControladorContrato().popularEncerrarContrato(contrato, dadosAuditoria);
	}

	/**
	 * Método responsável por obter um
	 * contratoPontoConsumo pela chave primaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the contrato ponto consumo
	 * @throws GGASException the GGAS exception
	 */
	public ContratoPontoConsumo obterContratoPontoConsumo(Long chavePrimaria) throws GGASException {

		return this.getControladorContrato().obterContratoPontoConsumo(chavePrimaria);
	}

	/**
	 * Método responsável por popular as contas
	 * credito e de debito na lista de lancamentos
	 * contabeis sintetico.
	 *
	 * @param listaLancamentoContabilSintetico the lista lancamento contabil sintetico
	 * @throws NegocioException the negocio exception
	 */
	public void popularContaCreditoDebitoLancamentoSintetico(Collection<LancamentoContabilSintetico> listaLancamentoContabilSintetico)
					throws NegocioException {

		this.getControladorContabil().popularContaCreditoDebitoLancamentoSintetico(listaLancamentoContabilSintetico);
	}

	/**
	 * Consultar evento comercial lancamento.
	 *
	 * @return the collection
	 */
	public Collection<EventoComercialLancamento> consultarEventoComercialLancamento() {

		return this.getControladorContabil().consultarEventoComercialLancamento();
	}

	/**
	 * Consultar evento comercial lancamento por evento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EventoComercialLancamento> consultarEventoComercialLancamentoPorEvento(long chavePrimaria) throws NegocioException {

		return this.getControladorContabil().consultarEventoComercialLancamentoPorEvento(chavePrimaria);
	}

	/**
	 * Listar conta contabil.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ContaContabil> listarContaContabil() throws NegocioException {

		return this.getControladorContabil().listarContaContabil();
	}

	/**
	 * Obter soma qdc contrato.
	 *
	 * @param idContrato the id contrato
	 * @param data the data
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal obterSomaQdcContrato(Long idContrato, Date data) throws GGASException {

		return this.getControladorContrato().obterSomaQdcContrato(idContrato, data);
	}

	/**
	 * Método.
	 *
	 * @param idContrato the id contrato
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ApuracaoRecuperacaoQuantidadeVO> obterApuracaoRecuperacaoQuantidadeVOs(Long idContrato, Date dataInicial,
					Date dataFinal, Long[] chavesPontoConsumo) throws GGASException {

		return this.getControladorApuracaoPenalidade().obterApuracaoRecuperacaoQuantidadeVOs(idContrato, dataInicial, dataFinal,
						chavesPontoConsumo);
	}

	/**
	 * Atualizar data recisao contrato.
	 *
	 * @param chaveContrato the chave contrato
	 * @param dataRecisao the data recisao
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarDataRecisaoContrato(Long chaveContrato, Date dataRecisao, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		this.getControladorContrato().atualizarDataRecisaoContrato(chaveContrato, dataRecisao, dadosAuditoria);
	}

	/**
	 * Método reponsável por listar todas as
	 * tarifas cadastradas.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TarifaVO> listarTarifaVO() throws GGASException {

		return this.getControladorTarifa().listarTarifaVO();
	}

	/**
	 * Método responsável por gerar o relatorio
	 * das tarifas cadastradas.
	 *
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] exportarRelatorioTarifasCadastradas(FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorTarifa().exportarRelatorioTarifasCadastradas(formatoImpressao);
	}

	/**
	 * Método responsável por gerar o relatorio
	 * da tarifa.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] exportarRelatorioTarifa(FiltroTarifaVigenciaDescontos filtro) throws GGASException {

		return this.getControladorTarifa().exportarRelatorioTarifa(filtro);
	}

	/**
	 * Gera o relatório de faturas emitidas com os
	 * dados de retorno da consulta realizada
	 * baseada no
	 * filtros informados.
	 *
	 * @param filtro Mapa de valores para filtragem
	 *            dos dados e parânetro para a
	 *            geração do relatório.
	 * @return bytesRelatorio
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioFaturasEmitidas(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioFaturasEmitidas().consultar(filtro);
	}

	/**
	 * Gerar relatorio faturamento.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @param exibirFiltros parametro para exibir Filtros
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioFaturamento(Map<String, Object> filtro, FormatoImpressao formatoImpressao,
					Boolean exibirFiltros) throws GGASException {

		return this.getControladorRelatorioFaturamento().gerarRelatorio(filtro, 
						formatoImpressao, exibirFiltros);
	}

	/**
	 * Metodo responsavel por obter uma entidade
	 * ContaBancaria.
	 *
	 * @param idContaBancaria the id conta bancaria
	 * @return Uma Entidade ContaBancaria do
	 *         sistema
	 * @throws GGASException the GGAS exception
	 */
	public ContaBancaria obterContaBancaria(Long idContaBancaria) throws GGASException {

		return this.getControladorArrecadacao().obterContaBancaria(idContaBancaria);
	}

	/**
	 * Método reponsável por listar todas as
	 * tarifas cadastradas.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TarifaVO> listarTarifaVigenciaVO(Map<String, Object> filtro) throws GGASException {

		return this.getControladorTarifa().listarTarifaVigenciaVO(filtro);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * análise de consumo.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioAnaliseConsumo(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorRelatorioAnaliseConsumo().gerarRelatorio(filtro, formatoImpressao);
	}

	/**
	 * Gerar relatorio descontos concedidos.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioDescontosConcedidos(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorRelatorioDescontosConcedidos().gerarRelatorio(filtro, formatoImpressao);
	}

	/**
	 * Obter empresa principal.
	 *
	 * @return the empresa
	 * @throws NegocioException the negocio exception
	 */
	public Empresa obterEmpresaPrincipal() throws NegocioException {

		return this.getControladorEmpresa().obterEmpresaPrincipal();
	}

	/**
	 * Método reponsável por gerar o relatorio de
	 * inadimplentes.
	 *
	 * @param parametros the parametros
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioDeInadimplentes(Map<String, Object> parametros) throws GGASException {

		return this.getControladorRelatorioDeInadimplentes().gerarRelatorio(parametros);

	}

	/**
	 * Método reponsável por listar os pontos de
	 * consumo.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<PontoConsumo> listarPontosConsumoRelatorioInadimplentes(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioDeInadimplentes().listarPontosConsumoRelatorioInadimplentes(filtro);

	}

	/**
	 * Gerar relatorio quantidade contratadas.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioQuantidadeContratadas(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorRelatorioQuantidadesContratadas().gerarRelatorio(filtro, formatoImpressao);
	}

	/**
	 * Consultar relatorio cliente unidade consumidora.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioClienteUnidadeConsumidora(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioClienteUnidadeConsumidora().consultar(filtro);
	}

	/**
	 * Validar filtro relatorio resumo volume.
	 *
	 * @param filtro the filtro
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarFiltroRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException {

		this.getControladorRelatorioResumoVolume().validarFiltroRelatorioResumoVolume(filtro);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * perfil de consumo.
	 *
	 * @param filtro
	 *            Filtros
	 * @param formatoImpressao
	 *            formato de impresão.
	 * @return Relatório de Perfil de consumo
	 * @throws GGASException
	 *             Caso ocorra algum erro.
	 */
	public byte[] gerarRelatorioPerfilConsumo(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorRelatorioPerfilConsumo().gerarRelatorio(filtro, formatoImpressao);
	}

	/**
	 * Gerar relatorio resumo volume.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public byte[] gerarRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException {

		return this.getControladorRelatorioResumoVolume().gerarRelatorioResumoVolume(filtro);
	}

	/**
	 * Método responsável por obter os pontos de
	 * consumo do relatório de analise de demanda.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<PontoConsumo> listarPontosConsumoRelatorioAnaliseDemanda(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioAnaliseDemanda().listarPontosConsumoRelatorioAnaliseDemanda(filtro);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * analise de demanda.
	 *
	 * @param parametros the parametros
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioAnaliseDemanda(Map<String, Object> parametros) throws GGASException {

		return this.getControladorRelatorioAnaliseDemanda().gerarRelatorio(parametros);
	}

	/**
	 * Método responsável por consultar os Papéis
	 * que se enquadram no filtro.
	 *
	 * @param filtro o filtro
	 * @return os papéis encontrados
	 */
	public Collection<Papel> consultarPapel(Map<String, Object> filtro) {

		return this.getControladorPapel().consultarPapel(filtro);
	}

	/**
	 * Método responsável por listar os
	 * funcionários da Empresa principal.
	 *
	 * @return funcionários da empresa
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Collection<Funcionario> listarFuncionariosEmpresaPrincipal() throws NegocioException {

		return this.getControladorFuncionario().listarFuncionariosEmpresaPrincipal();
	}

	/**
	 * Método responsável por listar os módulos do
	 * sistema.
	 *
	 * @param filtro the filtro
	 * @return Módulos do sistema
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Collection<Menu> listarModulos(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorMenu().listarModulos(filtro);
	}

	/**
	 * Método responsável por listar as
	 * funcionalidades de um módulo do sistema.
	 *
	 * @param idModulo chave do módulo
	 * @return Coleção de funcionalidades
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Menu> listarFuncionalidadesPorModulo(Long idModulo) throws NegocioException {

		return this.getControladorMenu().listarFuncionalidadesPorModulo(idModulo);
	}

	/**
	 * Listar situacao contrato.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SituacaoContrato> listarSituacaoContrato() throws NegocioException {

		return this.getControladorContrato().listarSituacaoContrato();
	}

	/**
	 * Método responsavel por listar todas as
	 * situações leitura.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<SituacaoLeitura> listarSituacaoLeitura() throws GGASException {

		return this.getControladorLeituraMovimento().listarSituacaoLeitura();
	}

	/**
	 * Método responsável por listar todos os
	 * tipos de composicao consumo.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TipoComposicaoConsumo> listarTipoComposicaoConsumo() throws GGASException {

		return this.getControladorHistoricoConsumo().listarTipoComposicaoConsumo();
	}

	/**
	 * Método responsável por listar todos os
	 * tipos de medicao.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TipoMedicao> listarTipoMedicao() throws GGASException {

		return this.getControladorHistoricoMedicao().listarTipoMedicao();
	}

	/**
	 * Método responsável por listar todas as
	 * entidades conteudo.
	 *
	 * @param codigoEntidadeClasse the codigo entidade classe
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarEntidadeConteudo(String codigoEntidadeClasse) throws GGASException {

		return this.getControladorEntidadeConteudo().listarEntidadeConteudo(codigoEntidadeClasse);
	}

	/**
	 * Método responsável por listar todos os
	 * tributos.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Tributo> listarTributos() throws GGASException {

		return this.getControladorTributo().listarTributos();
	}

	/**
	 * Método responsável por listar todas as
	 * esferas poder.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EsferaPoder> listarEsferaPoder() throws GGASException {

		return this.getControladorCliente().listarEsferaPoder();
	}

	/**
	 * Método responsável por validar uma remoção
	 * de consulta(s) no sistema.
	 *
	 * @param chavesPrimarias Chaves das consultas.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarRemoverConsulta(Long[] chavesPrimarias) throws GGASException {

		this.getControladorConsulta().validarRemoverConsulta(chavesPrimarias);
	}

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar consulta.
	 *
	 * @param chavesPrimarias
	 *            Chaves das consultas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarAtualizarConsulta(Long[] chavesPrimarias) throws GGASException {

		this.getControladorConsulta().validarAtualizarConsulta(chavesPrimarias);
	}

	/**
	 * Método responsável por remover uma ou mais
	 * Consulta.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerConsulta(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorConsulta().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por atualizar uma
	 * Consulta.
	 *
	 * @param consulta the consulta
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarConsulta(Consulta consulta) throws GGASException {

		this.getControladorConsulta().atualizar(consulta);
	}

	/**
	 * Método responsável por criar uma consulta.
	 *
	 * @return Consulta
	 * @throws GGASException the GGAS exception
	 */
	public Consulta criarConsulta() throws GGASException {

		return (Consulta) this.getControladorConsulta().criar();
	}

	/**
	 * Criar consulta parametro.
	 *
	 * @return the consulta parametro
	 * @throws GGASException the GGAS exception
	 */
	public ConsultaParametro criarConsultaParametro() throws GGASException {

		return (ConsultaParametro) this.getControladorConsulta().criarConsultaParametro();
	}

	/**
	 * Método responsável por inserir uma consulta
	 * no sistema.
	 *
	 * @param consulta the consulta
	 * @return chavePrimaria da consulta inserida
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirConsulta(Consulta consulta) throws GGASException {

		return this.getControladorConsulta().inserir(consulta);
	}

	/**
	 * Método responsável por buscar a consulta
	 * pela chave primária.
	 *
	 * @param chavePrimaria A chave primária da consulta
	 * @return Uma consulta
	 * @throws GGASException the GGAS exception
	 */
	public Consulta buscarConsultaPorChave(Long chavePrimaria) throws GGASException {

		return (Consulta) this.getControladorConsulta().obter(chavePrimaria);
	}

	/**
	 * Método responsável por consultar as
	 * consultas pelo filtro
	 * informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de consulta
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Consulta> consultarConsulta(Map<String, Object> filtro) throws GGASException {

		return getControladorConsulta().consultarConsulta(filtro);
	}

	/**
	 * Método responsável por buscar o modulo pela
	 * chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do modulo
	 * @return Um modulo do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Modulo buscarModuloPorChave(Long chavePrimaria) throws GGASException {

		return (Modulo) this.getControladorModulo().obter(chavePrimaria);
	}

	/**
	 * Métod responsável por obter a chave da
	 * operação do sistema.
	 *
	 * @param chaveOperacaoSistema the chave operacao sistema
	 * @return Long
	 * @throws NegocioException the negocio exception
	 */
	public Long obterChaveOperacaoSistema(ChaveOperacaoSistema chaveOperacaoSistema) throws NegocioException {

		return this.getControladorAcesso().obterChaveOperacaoSistema(chaveOperacaoSistema);
	}

	/**
	 * Recupera a consulta utilizada na exportação
	 * de dados para dispositivos móveis.
	 *
	 * @return consultaExportacao
	 * @throws NegocioException
	 *             Caso a consulta não seja
	 *             encontrada
	 */
	public Consulta buscarConsultaExportacaoDispositivosMoveis() throws NegocioException {

		return this.getControladorConsulta().buscarConsultaExportacaoDispositivosMoveis();
	}

	/**
	 * Método responsável por obter uma consulta
	 * de acordo com o tipo de exportacao ou
	 * re-exportacao
	 * pela chave do processo.
	 *
	 * @param chavePrimariaProcesso the chave primaria processo
	 * @return Uma Consulta.
	 * @throws NegocioException the negocio exception
	 */
	public Consulta obterConsultaExportacaoPorProcesso(long chavePrimariaProcesso) throws NegocioException {

		return this.getControladorConsulta().obterConsultaExportacaoPorProcesso(chavePrimariaProcesso);
	}

	/**
	 * Limitar consulta exportacao.
	 *
	 * @param consulta the consulta
	 * @param status the status
	 */
	public void limitarConsultaExportacao(Consulta consulta, long status) {

		this.getControladorConsulta().limitarConsultaExportacao(consulta, status);
	}

	/**
	 * Método responsável por listar os tipos do
	 * parametro.
	 *
	 * @return Uma coleção de tipos do parametro.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarTipoParametro() throws GGASException {

		return this.getControladorEntidadeConteudo().listarTipoParametro();
	}

	/**
	 * Método responsável por listar as mascaras
	 * do parametro.
	 *
	 * @param tipo the tipo
	 * @return Uma coleção de mascaras do
	 *         parametro.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public MascaraParametro[] listarMascaraParametro(String tipo) throws GGASException {

		return this.getControladorConsulta().listarMascaraParametro(tipo);
	}

	/**
	 * Método responsável por listar as entidades
	 * referenciadas.
	 *
	 * @return Uma coleção de entidades
	 *         referenciadas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Tabela> listarEntidadeReferenciada() throws GGASException {

		return this.getControladorConsulta().listarEntidadeReferenciada();
	}

	/**
	 * Método responsável por listar a tabela por
	 * nome.
	 *
	 * @param nomeTabela the nome tabela
	 * @return Uma coleção de entidades
	 *         referenciadas.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Coluna> obterColunasLeituraMovimentoPorNomeTabela(String nomeTabela) throws GGASException {

		return this.getControladorConsulta().obterColunasLeituraMovimentoPorNomeTabela(nomeTabela);
	}

	/**
	 * Método responsável por validar um parametro
	 * de consulta.
	 *
	 * @param consultaParametro the consulta parametro
	 * @param listaConsultaParametro the lista consulta parametro
	 * @param indice the indice
	 * @throws NegocioException the negocio exception
	 */
	public void validarConsultaParametro(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
					int indice) throws NegocioException {

		this.getControladorConsulta().validarConsultaParametro(consultaParametro, listaConsultaParametro, indice);
	}

	/**
	 * Método responsável por validar um parametro
	 * de consulta.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeClasse> listarEntidadeClasse() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarEntidadeClasse();
	}

	/**
	 * Método responsável por validar um parametro
	 * de consulta.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ClasseUnidade> listarClasseUnidade() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarClasseUnidade();
	}

	/**
	 * Método responsável obter dados do resultado
	 * da execução de uma consulta.
	 *
	 * @param consulta the consulta
	 * @param mapDadosParametro the map dados parametro
	 * @param separador the separador
	 * @return the byte[]
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public byte[] obterDadosExecucaoConsulta(Consulta consulta, Map<String, Object[]> mapDadosParametro, String separador)
					throws NegocioException {

		return this.getControladorConsulta().obterDadosExecucaoConsulta(consulta, mapDadosParametro, separador);
	}

	/**
	 * Método responsável obter uma lista dos
	 * registros da entidade referenciada
	 * escolhida.
	 *
	 * @param tabela the tabela
	 * @return the collection
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeNegocio> listarRegistrosDaEntidadeReferenciada(Tabela tabela) throws NegocioException {

		return this.getControladorConsulta().listarRegistrosDaEntidadeReferenciada(tabela);
	}

	/**
	 * Método responsável por cancelar fatura a
	 * partir do botão não faturar da tela de
	 * analise de
	 * anomalias.
	 *
	 * @param historicoAnomaliaFaturamento the historico anomalia faturamento
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void cancelarFaturaAnomalia(HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		this.getControladorFatura().cancelarFaturaAnomalia(historicoAnomaliaFaturamento, dadosAuditoria);
	}

	/**
	 * Confirmar exportacao.
	 *
	 * @param consulta the consulta
	 * @param mapDadosParametro the map dados parametro
	 * @throws NegocioException the negocio exception
	 */
	public void confirmarExportacao(Consulta consulta, Map<String, Object[]> mapDadosParametro) throws NegocioException {

		this.getControladorConsulta().confirmarExportacao(consulta, mapDadosParametro);
	}

	/**
	 * Método responsável por listar a operações
	 * de uma funcionalidade.
	 *
	 * @param idFuncionalidade chave da funcionalidade
	 * @return operações da funcionalidade
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Collection<Operacao> listarOperacoesPorFuncionalidade(Long idFuncionalidade) throws NegocioException {

		return this.getControladorAcesso().listarOperacoesPorFuncionalidade(idFuncionalidade);
	}

	/**
	 * Método responsável por listar todas as
	 * situações de cobrança débito.
	 *
	 * @return Collection<CobrancaDebitoSituacao>
	 * @throws GGASException the GGAS exception
	 */
	public Collection<CobrancaDebitoSituacao> listarCobrancaDebitoSituacao() throws GGASException {

		return this.getControladorCobranca().listarCobrancaDebitoSituacao();
	}

	/**
	 * Método responsável por salvar as alterações
	 * dos parametros do sistema.
	 *
	 * @param dados the dados
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void salvarAlteracaoParametros(Map<String, Map<String, String>> dados, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorParametroSistema().salvarAlteracaoParametros(dados, dadosAuditoria);
	}

	/**
	 * Obter modulo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the modulo
	 * @throws NegocioException the negocio exception
	 */
	public Modulo obterModulo(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Modulo) this.getControladorModulo().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Obter menu.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the menu
	 * @throws NegocioException the negocio exception
	 */
	public Menu obterMenu(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Menu) this.getControladorMenu().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por criar uma Permissao.
	 *
	 * @return Permissao
	 */
	public Permissao criarPermissao() {

		return (Permissao) this.getControladorAcesso().criarPermissao();
	}

	/**
	 * Método responsável por obter uma entidade.
	 *
	 * @param idFuncionario the id funcionario
	 * @return EntidadeBase
	 * @throws NegocioException Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	public Funcionario obterFuncionario(Long idFuncionario) throws NegocioException {

		return (Funcionario) this.getControladorFuncionario().obter(idFuncionario);
	}

	/**
	 * Método responsável por validar os campos da
	 * tela de incluir controle de acesso ao
	 * funcionário.
	 *
	 * @param funcionario {@link FuncionarioImpl}
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void validarIncluirControleAcessoFunc(FuncionarioImpl funcionario) throws NegocioException {

		this.getControladorUsuario().validarIncluirControleAcessoFunc(funcionario);
	}

	/**
	 * Validar a recuperação de senha.
	 *
	 * @param login the login
	 * @throws NegocioException the negocio exception
	 */
	public void validarRecuperarSenha(String login) throws NegocioException {

		this.getControladorUsuario().validarRecuperarSenha(login);
	}

	/**
	 * Método responsável por validar os campos da
	 * tela de incluir controle de acesso ao
	 * funcionário.
	 *
	 * @param email the email
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void verificarEmailValido(String email) throws NegocioException {

		this.getControladorUsuario().verificarEmailValido(email);
	}

	/**
	 * Método responsável por enviar um email ao
	 * funcionário.
	 *
	 * @param remetente , destinatario, assunto,
	 *            conteudoEmail
	 * @param destinatario the destinatario
	 * @param subject the subject
	 * @param conteudoEmail the conteudo email
	 * @throws GGASException the GGAS exception
	 */

	public void enviarEmailFunc(String remetente, String destinatario, String subject, String conteudoEmail) throws GGASException {

		this.getControladorUsuario().enviarEmailFunc(remetente, destinatario, subject, conteudoEmail);
	}

	/**
	 * Método responsável por consultar as
	 * permissões de um papel.
	 *
	 * @param colecaoPapeis coleção de papéis
	 * @return coleção de permissões
	 */
	public Collection<Permissao> consultarPermissoes(Collection<Papel> colecaoPapeis) {

		return getControladorAcesso().consultarPermissoes(colecaoPapeis);

	}

	/**
	 * Listar recursos usuario.
	 *
	 * @param chaveUsuario the chave usuario
	 * @return the collection
	 */
	public Collection<Recurso> listarRecursosUsuario(long chaveUsuario) {

		return getControladorAcesso().listarRecursosUsuario(chaveUsuario);
	}

	/**
	 * Método responsável por listar todas os
	 * NomeclaturaComumMercosul.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<NomeclaturaComumMercosul> listarNomeclaturaComumMercosul() throws GGASException {

		return this.getControladorRubrica().listarNomeclaturaComumMercosul();
	}

	/**
	 * Método responsável por obter
	 * NomeclaturaComumMercosul.
	 *
	 * @param idNomeclaturaComumMercosul the id nomeclatura comum mercosul
	 * @return the nomeclatura comum mercosul
	 * @throws GGASException the GGAS exception
	 */
	public NomeclaturaComumMercosul obterNomeclaturaComumMercosul(Long idNomeclaturaComumMercosul) throws GGASException {

		return this.getControladorRubrica().obterNomeclaturaComumMercosul(idNomeclaturaComumMercosul);
	}

	/**
	 * Método responsável por listar os motivos
	 * das movimentação do medidor.
	 *
	 * @return Collection<
	 *         MotivoMovimentacaoMedidor
	 *         >
	 * @throws NegocioException the negocio exception
	 */
	public Collection<MotivoMovimentacaoMedidor> listarMotivoMovimentacaoMedidor() throws NegocioException {

		return this.getControladorMedidor().listarMotivoMovimentacaoMedidor();
	}

	/**
	 * Listar numero digitos consumo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Integer> listarNumeroDigitosConsumo() throws NegocioException {

		return this.getControladorMedidor().listarNumeroDigitosConsumo();
	}

	/**
	 * Listar tipo dados endereco remoto.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> listarTipoDadosEnderecoRemoto() throws NegocioException {

		return this.getControladorMedidor().listarTipoDadosEnderecoRemoto();
	}

	/**
	 * Listar tipo integracao.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> listarTipoIntegracao() throws NegocioException {

		return this.getControladorMedidor().listarTipoIntegracao();
	}

	/**
	 * Método responsável por verificar se é
	 * permitido alterar a situacao do corretor de
	 * vazao.
	 *
	 * @param chavePrimariaVazaoCorretor the chave primaria vazao corretor
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	public boolean permiteAlteracaoSituacaoCorretorVazao(long chavePrimariaVazaoCorretor) throws GGASException {

		return this.getControladorVazaoCorretor().permiteAlteracaoSituacaoCorretorVazao(chavePrimariaVazaoCorretor);
	}

	/**
	 * Método responsável por consultar contrato
	 * por numero.
	 *
	 * @param numeroContrato the numero contrato
	 * @return Collection<EntidadeNegocio>
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Contrato> consultarContratoPorNumero(String numeroContrato) throws NegocioException {

		return this.getControladorContrato().consultarContratoPorNumero(numeroContrato);
	}

	/**
	 * Método para listar as tabelas do sistema.
	 *
	 * @return Collection<Tabela>
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Tabela> listarTabelas() throws NegocioException {

		return this.getControladorAuditoria().listarTabelas();
	}

	/**
	 * Listar tabela variavel.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Coluna> listarTabelaVariavel() throws NegocioException {

		return this.getControladorAuditoria().listarTabelaVariavel();
	}

	/**
	 * Método para listar as tabelas selecionadas
	 * do sistema.
	 *
	 * @param idTabela the id tabela
	 * @return Tabela
	 * @throws NegocioException the negocio exception
	 */
	public Tabela obterTabelaSelecionada(Long idTabela) throws NegocioException {

		return this.getControladorAuditoria().obterTabelaSelecionada(idTabela);
	}

	/**
	 * Obter tabela.
	 *
	 * @param idTabela the id tabela
	 * @return the tabela
	 * @throws NegocioException the negocio exception
	 */
	public Tabela obterTabela(Long idTabela) throws NegocioException {

		return this.getControladorAuditoria().obterTabela(idTabela);
	}

	/**
	 * Método para listar as colunas da tabela
	 * selecionada.
	 *
	 * @param idTabela the id tabela
	 * @return Collection<ColunaTabela>
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Coluna> listarColunasTabelaSelecionada(Long idTabela) throws NegocioException {

		return this.getControladorAuditoria().listarColunasTabelaSelecionada(idTabela);
	}

	/**
	 * Listar colunas tabela variavel.
	 *
	 * @param idTabela the id tabela
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Coluna> listarColunasTabelaVariavel(Long idTabela) throws NegocioException {

		return this.getControladorAuditoria().listarColunasTabelaVariavel(idTabela);
	}

	/**
	 * Método responsável por atualizar a entidade
	 * ColunaTabela.
	 *
	 * @param tabela the tabela
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarTabela(Tabela tabela) throws GGASException {

		this.getControladorAuditoria().atualizar(tabela);
	}

	/**
	 * Verificar colunas auditaveis.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param colunasAuditaveis the colunas auditaveis
	 * @param dadosAuditoria the dados auditoria
	 * @return the tabela
	 * @throws NegocioException the negocio exception
	 */
	public Tabela verificarColunasAuditaveis(Long chavePrimaria, String[] colunasAuditaveis, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		return this.getControladorAuditoria().verificarColunasAuditaveis(chavePrimaria, colunasAuditaveis, dadosAuditoria);
	}

	/**
	 * Obter recurso.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the recurso
	 * @throws NegocioException the negocio exception
	 */
	public Recurso obterRecurso(Long chavePrimaria) throws NegocioException {

		return this.getControladorModulo().buscarRecursoPorChave(chavePrimaria);
	}

	/**
	 * Método responsável por atualizar uma tarifa.
	 *
	 * @param tarifa a tarifa a ser atualizada
	 * @throws NegocioException caso ocorra algum erro
	 * @throws ConcorrenciaException caso ocorra algum erro
	 */
	public void atualizar(Tarifa tarifa) throws NegocioException, ConcorrenciaException {

		this.getControladorTarifa().atualizar(tarifa);
	}

	/**
	 * Método responsável por atualizar um
	 * parcelamento.
	 *
	 * @param parcelamento o parcelamento a ser atualizado
	 * @throws NegocioException caso ocorra algum erro
	 * @throws ConcorrenciaException caso ocorra algum erro
	 */
	public void atualizar(Parcelamento parcelamento) throws NegocioException, ConcorrenciaException {

		this.getControladorParcelamento().atualizar(parcelamento);
	}

	/**
	 * Método responsável por listar todos os
	 * módulos filhos.
	 *
	 * @return Módulos do sistema
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Collection<Menu> listarFuncionalidadesFilho() throws NegocioException {

		return this.getControladorMenu().listarFuncionalidadesFilho();
	}

	/**
	 * Método responsável por obter um menu por
	 * chave.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return Menu
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Menu obterMenu(Long chavePrimaria) throws NegocioException {

		return (Menu) this.getControladorMenu().obter(chavePrimaria);
	}

	/**
	 * Método responsável por obter uma lista de
	 * colunas.
	 *
	 * @param idMenu the id menu
	 * @return the collection
	 */
	public Collection<Coluna> obterListaColunasAlcadaPorMenu(Long idMenu) {

		return this.getControladorAlcada().obterListaColunasAlcadaPorMenu(idMenu);
	}

	/**
	 * Método reponsável por criar uma instancia
	 * de Alçada.
	 *
	 * @return the alcada
	 * @throws GGASException the GGAS exception
	 */
	public Alcada criarAlcada() throws GGASException {

		return (Alcada) this.getControladorAlcada().criar();
	}

	/**
	 * Método responsável por validar se foi
	 * sleecionado algum perfil para inserção da
	 * alçada.
	 *
	 * @param idMenu the id menu
	 * @param idPapel the id papel
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws NegocioException the negocio exception
	 */
	public void validarAlcadas(Long idMenu, Long idPapel, Date dataInicial, Date dataFinal) throws NegocioException {

		this.getControladorAlcada().validarAlcadas(idMenu, idPapel, dataInicial, dataFinal);
	}

	/**
	 * Método reponsável por montar uma lista de
	 * Papeis a partir de uma lista de Ids.
	 *
	 * @param listaIdsPapeis the lista ids papeis
	 * @return the sets the
	 * @throws NegocioException the negocio exception
	 */
	public Set<Papel> obterListaPapelPorIds(Long[] listaIdsPapeis) throws NegocioException {

		return this.getControladorPapel().obterListaPapelPorIds(listaIdsPapeis);
	}

	/**
	 * Método para inserir uma coleção de alçadas.
	 *
	 * @param listaAlcada the lista alcada
	 * @throws NegocioException the negocio exception
	 */
	public void inserirAlcadas(Collection<Alcada> listaAlcada) throws NegocioException {

		this.getControladorAlcada().inserirAlcadas(listaAlcada);
	}

	/**
	 * Método para obter a lista de alçadas por
	 * menu.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<AlcadaVO> obterListaAlcadaVO(Map<String, Object> filtro) {

		return this.getControladorAlcada().obterListaAlcadaVO(filtro);
	}

	/**
	 * Método para obter os detalhes da alçada.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Alcada> obterlistaAlcada(Map<String, Object> filtro) {

		return this.getControladorAlcada().obterlistaAlcada(filtro);
	}

	/**
	 * Método para remover alçadas.
	 *
	 * @param alcadasRemocao the alcadas remocao
	 * @throws GGASException the GGAS exception
	 */
	public void removerAlcada(String alcadasRemocao) throws GGASException {

		this.getControladorAlcada().removerAlcada(alcadasRemocao);
	}

	/**
	 * Metodo para obter a lista de Alçadas para
	 * alteração.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<AlteracaoAlcadaVO> obterListaAlteracaoAlcada(Map<String, Object> filtro) {

		return this.getControladorAlcada().obterListaAlteracaoAlcada(filtro);
	}

	/**
	 * Método para processar a alteração da alçada.
	 *
	 * @param listaAlcadas the lista alcadas
	 * @param listaValoresIniciais the lista valores iniciais
	 * @param listaValoresFinais the lista valores finais
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param habilitado the habilitado
	 * @throws GGASException the GGAS exception
	 */
	public void alterarAlcada(Collection<AlteracaoAlcadaVO> listaAlcadas, String[] listaValoresIniciais, String[] listaValoresFinais,
					Date dataInicial, Date dataFinal, Boolean habilitado) throws GGASException {

		this.getControladorAlcada().alterarAlcada(listaAlcadas, listaValoresIniciais, listaValoresFinais, dataInicial, dataFinal,
						habilitado);
	}

	/**
	 * Método para validar se já existe uma alçada
	 * com os dados informados.
	 *
	 * @param idMenu the id menu
	 * @param idColuna the id coluna
	 * @param idPapel the id papel
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws GGASException the GGAS exception
	 */
	public void validarAlcadaExistente(Long idMenu, Long idColuna, Long idPapel, Date dataInicial, Date dataFinal) throws GGASException {

		this.getControladorAlcada().validarAlcadaExistente(idMenu, idColuna, idPapel, dataInicial, dataFinal);
	}

	/**
	 * Método para obter a lista de alçadas por
	 * menu.
	 *
	 * @param idMenu the id menu
	 * @return the collection
	 */
	public Collection<AlcadaVO> obterListaAlcadasPorMenu(Long idMenu) {

		return this.getControladorAlcada().obterListaAlcadasPorMenu(idMenu);
	}

	/**
	 * Método responsável por atualizar um cliente
	 * imóvel.
	 *
	 * @param clienteImovel the cliente imovel
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarClienteImovel(ClienteImovel clienteImovel) throws GGASException {

		this.getControladorClienteImovel().atualizar(clienteImovel);
	}

	/**
	 * Método responsável por consultar um cliente
	 * imóvel.
	 *
	 * @param idImovel the id imovel
	 * @param idCliente the id cliente
	 * @param tipoRelacionamentoClienteImovel the tipo relacionamento cliente imovel
	 * @return the cliente imovel
	 * @throws NegocioException the negocio exception
	 */
	public ClienteImovel obterClienteImovel(Long idImovel, Long idCliente, Long tipoRelacionamentoClienteImovel) throws NegocioException {

		return this.getControladorClienteImovel().obterClienteImovel(idImovel, idCliente, tipoRelacionamentoClienteImovel);
	}

	/**
	 * Método responsável por inserir um cliente
	 * imóvel.
	 *
	 * @param clienteImovel the cliente imovel
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void inserirClienteImovel(ClienteImovel clienteImovel) throws NegocioException, ConcorrenciaException {

		this.getControladorClienteImovel().inserir(clienteImovel);
	}

	/**
	 * Método responsável por inserir uma lista de
	 * cliente imóvel.
	 *
	 * @param listaClienteImovel the lista cliente imovel
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void inserirClienteImovel(Collection<ClienteImovel> listaClienteImovel) throws NegocioException, ConcorrenciaException {

		this.getControladorClienteImovel().inserirClienteImovel(listaClienteImovel);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * rota.
	 *
	 * @param parametros the parametros
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioRota(Map<String, Object> parametros) throws GGASException {

		return this.getControladorRelatorioRota().gerarRelatorio(parametros);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * usuário.
	 *
	 * @param parametros the parametros
	 * @param usuario the usuario
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioUsuario(Map<String, Object> parametros, Usuario usuario) throws GGASException {

		return this.getControladorUsuario().gerarRelatorio(parametros, usuario);
	}

	/**
	 * Método responsável por validar os dados do
	 * ramo de atividades.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosRamoAtividade(RamoAtividade ramoAtividade) throws GGASException {

		this.getControladorRamoAtividade().validarDadosRamoAtividade(ramoAtividade);
	}

	/**
	 * Método responsável por gerar o relatório de
	 * Papel.
	 *
	 * @param filtro the filtro
	 * @param papel the papel
	 * @param listaPermissoes the lista permissoes
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public byte[] gerarRelatorioPapel(Map<String, Object> filtro, Papel papel, List<ModuloFuncOperacoesVO> listaPermissoes)
					throws NegocioException, FormatoInvalidoException {

		return this.getControladorPapel().gerarRelatorioPapel(filtro, papel, listaPermissoes);
	}

	/**
	 * Método responsável por listar todos os
	 * creditos debitos negociado por recebimento.
	 *
	 * @param idRecebimento the id recebimento
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Map<String, Object>> listarCreditoDebitoNegociadoPorRecebimento(Long idRecebimento) throws GGASException {

		return this.getControladorCreditoDebito().listarCreditoDebitoNegociadoPorRecebimento(idRecebimento);
	}

	/**
	 * Validar segmento.
	 *
	 * @param segmento the segmento
	 * @throws NegocioException the negocio exception
	 */
	public void validarSegmento(Long segmento) throws NegocioException {

		this.getControladorSegmento().validarSegmento(segmento);
	}

	/**
	 * Validar rota.
	 *
	 * @param rota the rota
	 * @throws NegocioException the negocio exception
	 */
	public void validarRota(Long rota) throws NegocioException {

		this.getControladorRota().validarRota(rota);
	}

	/**
	 * Validar ramo atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @throws NegocioException the negocio exception
	 */
	public void validarRamoAtividade(Long ramoAtividade) throws NegocioException {

		this.getControladorRamoAtividade().validarRamoAtividade(ramoAtividade);
	}

	/**
	 * Método responsável por obter as informaçoes
	 * do medidor instalado no ponto de consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterInformacoesMedidor(Long idPontoConsumo) throws GGASException {

		return this.getControladorMedidor().obterInformacoesMedidor(idPontoConsumo);

	}

	/**
	 * Método responsável por verificar se houve virada de medidor.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param leitura the leitura
	 * @param data the data
	 * @return the boolean
	 * @throws NegocioException {@link NegocioException}
	 */
	public Boolean verificarViradaMedidor(Long idPontoConsumo, BigDecimal leitura, Date data) throws NegocioException {

		return this.getControladorHistoricoConsumo().verificarViradaMedidor(idPontoConsumo, leitura, data);
	}

	/**
	 * Obter registros pendentes por usuario.
	 * 
	 * @param usuario Usuario
	 * @param isQtdeMinimaPendente (quantidade limite de 10 registros se for true)
	 * @return the autorizacoes pendentes vo
	 * @throws NegocioException the negocio exception
	 */
	public AutorizacoesPendentesVO obterRegistrosPendentesPorUsuario(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException {

		return this.getControladorAlcada().obterRegistrosPendentesPorUsuario(usuario, isQtdeMinimaPendente);
	}

	/**
	 * Possui alcada autorizacao creditos debitos.
	 *
	 * @param usuario the usuario
	 * @param pendente the pendente
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoCreditosDebitos(Usuario usuario, CreditoDebitoNegociado pendente) throws NegocioException {

		return this.getControladorAlcada().possuiAlcadaAutorizacaoCreditosDebitos(usuario, pendente);
	}

	/**
	 * Possui alcada autorizacao parcelamento.
	 *
	 * @param usuario the usuario
	 * @param pendente the pendente
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoParcelamento(Usuario usuario, Parcelamento pendente) throws NegocioException {

		return this.getControladorAlcada().possuiAlcadaAutorizacaoParcelamento(usuario, pendente);
	}

	/**
	 * Possui alcada autorizacao tarifa vigencia.
	 *
	 * @param usuario the usuario
	 * @param pendente the pendente
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoTarifaVigencia(Usuario usuario, TarifaVigencia pendente) throws NegocioException {

		return this.getControladorAlcada().possuiAlcadaAutorizacaoTarifaVigencia(usuario, pendente);
	}

	/**
	 * Montar parcelamento.
	 *
	 * @param dadosGerais the dados gerais
	 * @return the parcelamento
	 * @throws GGASException the GGAS exception
	 */
	public Parcelamento montarParcelamento(DadosGeraisParcelas dadosGerais) throws GGASException {

		return this.getControladorParcelamento().montarParcelamento(dadosGerais);
	}

	/**
	 * Autorizar parcelamento.
	 *
	 * @param idParcelamento the id parcelamento
	 * @param status the status
	 * @param dadosAuditoria the dados auditoria
	 * @return the parcelamento
	 * @throws GGASException the GGAS exception
	 */
	public Parcelamento autorizarParcelamento(Long idParcelamento, EntidadeConteudo status, DadosAuditoria dadosAuditoria)
					throws GGASException {

		return this.getControladorParcelamento().autorizarParcelamento(idParcelamento, status, dadosAuditoria);
	}

	/**
	 * Método responsável por buscar o município
	 * pelo nome e cep.
	 *
	 * @param nome the nome
	 * @param uf the uf
	 * @return Um muncípio.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Municipio obterMunicipioPorNomeUF(String nome, String uf) throws GGASException {

		return this.getControladorMunicipio().obterMunicipioPorNomeUF(nome, uf);
	}

	/**
	 * Método responsável por consultar o
	 * Documento Fiscal mais recente através de
	 * uma Fatura.
	 *
	 * @param chaveDocumentoFiscal the chave documento fiscal
	 * @return Collection<DocumentoFiscal>
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoFiscal obterDocumentoFiscalPorChave(Long chaveDocumentoFiscal) throws NegocioException {

		return this.getControladorFatura().obterDocumentoFiscalPorChave(chaveDocumentoFiscal);
	}

	/**
	 * Método responsável por consultar o
	 * Documento Fiscal mais recente através de
	 * uma Fatura.
	 *
	 * @param chavePrimariaPontoConsumo the chave primaria ponto consumo
	 * @return Collection<DocumentoFiscal>
	 * @throws GGASException the GGAS exception
	 */
	public void verificarDependenciaLeituraFaturamento(Collection<PontoConsumo> chavePrimariaPontoConsumo) throws GGASException {

		this.getControladorContrato().verificarDependenciaLeituraFaturamento(chavePrimariaPontoConsumo);
	}

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar para a fatura
	 * de
	 * encerramento.
	 *
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosParaFaturaEncerramento(Map<String, Object> filtro)
					throws NegocioException {

		return this.getControladorCreditoDebito().consultarCreditosDebitosParaFaturaEncerramento(filtro);
	}

	/**
	 * Atualizar data ano mes faturamento.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param dataInicioCobranca the data inicio cobranca
	 * @param creditoDebitoNegociado the credito debito negociado
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarDataAnoMesFaturamento(Long chavePontoConsumo, Date dataInicioCobranca,
					CreditoDebitoNegociado creditoDebitoNegociado, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		this.getControladorCreditoDebito().atualizarDataAnoMesFaturamento(chavePontoConsumo, dataInicioCobranca, creditoDebitoNegociado,
						dadosAuditoria);
	}

	/**
	 * Inserir tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws GGASException {

		return this.getControladorTabelaAuxiliar().inserir(tabelaAuxiliar);
	}

	/**
	 * Atualizar tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param classe the classe
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, Class<?> classe) throws GGASException {

		this.getControladorTabelaAuxiliar().atualizar(tabelaAuxiliar, classe);
	}

	/**
	 * Criar entidade classe.
	 *
	 * @return the entidade classe
	 * @throws GGASException the GGAS exception
	 */
	public EntidadeClasse criarEntidadeClasse() throws GGASException {

		return (EntidadeClasse) this.getControladorEntidadeConteudo().criarEntidadeClasse();
	}

	/**
	 * Pesquisar tabela auxiliar.
	 *
	 * @param filtro the filtro
	 * @param classe the classe
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TabelaAuxiliar> pesquisarTabelaAuxiliar(Map<String, Object> filtro, String classe) throws GGASException {

		return this.getControladorTabelaAuxiliar().pesquisarTabelaAuxiliar(filtro, classe);
	}

	/**
	 * Obter tabela auxiliar.
	 *
	 * @param chave the chave
	 * @param classe the classe
	 * @param propriedadeLazy the propriedade lazy
	 * @return the tabela auxiliar
	 * @throws GGASException the GGAS exception
	 */
	public TabelaAuxiliar obterTabelaAuxiliar(Long chave, Class<?> classe, String... propriedadeLazy) throws GGASException {

		TabelaAuxiliar tabelaAuxiliar = null;

		if (propriedadeLazy == null) {
			tabelaAuxiliar = (TabelaAuxiliar) getControladorTabelaAuxiliar().obter(chave, classe);
		} else {
			tabelaAuxiliar = (TabelaAuxiliar) getControladorTabelaAuxiliar().obter(chave, classe, propriedadeLazy);
		}

		return tabelaAuxiliar;
	}

	/**
	 * Validar dados tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		getControladorTabelaAuxiliar().validarDadosTabelaAuxiliar(tabelaAuxiliar);
	}

	/**
	 * Alterar tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param classe the classe
	 * @throws GGASException the GGAS exception
	 */
	public void alterarTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, Class<?> classe) throws GGASException {

		this.getControladorTabelaAuxiliar().atualizar(tabelaAuxiliar, classe);
	}

	/**
	 * Remover tabela auxiliar.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param classe the classe
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerTabelaAuxiliar(Long[] chavesPrimarias, Class<?> classe, DadosAuditoria dadosAuditoria) throws GGASException {

		getControladorTabelaAuxiliar().removerTabelaAuxiliar(chavesPrimarias, classe, dadosAuditoria);
	}

	/**
	 * Listar entidade conteudo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> listarEntidadeConteudo(Long chavePrimaria) throws GGASException {

		return this.getControladorEntidadeConteudo().listarEntidadeConteudo(chavePrimaria);
	}

	/**
	 * Criar unidade classe.
	 *
	 * @return the classe unidade
	 * @throws GGASException the GGAS exception
	 */
	public ClasseUnidade criarUnidadeClasse() throws GGASException {

		return (ClasseUnidade) this.getControladorEntidadeConteudo().criarUnidadeClasse();
	}

	/**
	 * Listar classe unidade habilitado.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ClasseUnidade> listarClasseUnidadeHabilitado() throws NegocioException {

		return this.getControladorUnidade().listarClasseUnidade();
	}

	/**
	 * Listar unidade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ClasseUnidade> listarUnidade(Long chavePrimaria) throws NegocioException {

		return this.getControladorUnidade().listarUnidade(chavePrimaria);
	}

	/**
	 * Criar entidade regiao.
	 *
	 * @return the regiao
	 * @throws GGASException the GGAS exception
	 */
	public Regiao criarEntidadeRegiao() throws GGASException {

		return (Regiao) this.getControladorGeografico().criarEntidadeRegiao();
	}

	/**
	 * Pesquisa descricao tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisaDescricaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisaDescricaoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Verificar tamanho descricao.
	 *
	 * @param descricao the descricao
	 * @param classe the classe
	 * @throws NegocioException the negocio exception
	 */
	public void verificarTamanhoDescricao(String descricao, String classe) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarTamanhoDescricao(descricao, classe);
	}

	/**
	 * Listar nivel unidade tipo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<String> listarNivelUnidadeTipo() throws NegocioException {

		return getControladorUnidadeOrganizacional().listarNivelUnidadeTipo();
	}

	/**
	 * Listar tipo unidade tipo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<String> listarTipoUnidadeTipo() throws NegocioException {

		return getControladorUnidadeOrganizacional().listarTipoUnidadeTipo();
	}

	/**
	 * Verificar tamanho descricao abreviada.
	 *
	 * @param descricaoAbreviada the descricao abreviada
	 * @param classe the classe
	 * @throws NegocioException the negocio exception
	 */
	public void verificarTamanhoDescricaoAbreviada(String descricaoAbreviada, String classe) throws NegocioException {

		getControladorTabelaAuxiliar().verificarTamanhoDescricaoAbreviada(descricaoAbreviada, classe);
	}

	/**
	 * Criar esfera poder.
	 *
	 * @return the esfera poder
	 */
	public EsferaPoder criarEsferaPoder() {

		return (EsferaPoder) this.getControladorCliente().criarEsferaPoder();
	}

	/**
	 * Pesquisa codigo cnae tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisaCodigoCnaeTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisaCodigoCnaeTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Pesquisa codigo tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisaCodigoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisaCodigoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Verificar data valida.
	 *
	 * @param data the data
	 * @param mascara the mascara
	 * @throws NegocioException the negocio exception
	 */
	public void verificarDataValida(String data, String mascara) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarDataValida(data, mascara);
	}

	/**
	 * Criar tributo.
	 *
	 * @return the tributo
	 */
	public Tributo criarTributo() {

		return (Tributo) this.getControladorTributo().criarTributo();
	}

	/**
	 * Criar tributo aliquota.
	 *
	 * @return the tributo aliquota
	 * @throws NegocioException the negocio exception
	 */
	public TributoAliquota criarTributoAliquota() throws NegocioException {

		return (TributoAliquota) this.getControladorTributo().criarTributoAliquota();
	}

	/**
	 * Pesquisar feriado tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarFeriadoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarFeriadoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Pesquisar tributo tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarTributoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarTributoTabelaAuxiliar(tabelaAuxiliar);
	}

	/**
	 * Verificar codigo original codigo cnae.
	 *
	 * @param codigoOriginal the codigo original
	 * @param codigoCnae the codigo cnae
	 * @throws NegocioException the negocio exception
	 */
	public void verificarCodigoOriginalCodigoCNAE(String codigoOriginal, String codigoCnae) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarCodigoOriginalCodigoCNAE(codigoOriginal, codigoCnae);
	}

	/**
	 * Consultar tributos aliquota.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<TributoAliquota> consultarTributosAliquota(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorTributo().consultarTributosAliquota(filtro);
	}

	/**
	 * Verificarpercentagem maxima.
	 *
	 * @param percentualAliquotaAux the percentual aliquota aux
	 * @throws NegocioException the negocio exception
	 */
	public void verificarpercentagemMaxima(Double percentualAliquotaAux) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarpercentagemMaxima(percentualAliquotaAux);
	}

	/**
	 * Listar paises.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Pais> listarPaises() throws GGASException {

		return this.getControladorGeografico().listarPaises();
	}

	/**
	 * Criar entidade pais.
	 *
	 * @return the pais
	 */
	public Pais criarEntidadePais() {

		return this.getControladorGeografico().criarEntidadePais();
	}

	/**
	 * Pesquisar unidade federacao tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarUnidadeFederacaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarUnidadeFederacaoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Criar entidade microrregiao.
	 *
	 * @return the microrregiao
	 */
	public Microrregiao criarEntidadeMicrorregiao() {

		return (Microrregiao) this.getControladorGeografico().criarEntidadeMicrorregiao();
	}

	/**
	 * Criar entidade municipio.
	 *
	 * @return the unidade federacao
	 */
	public UnidadeFederacao criarEntidadeMunicipio() {

		return (UnidadeFederacao) this.getControladorMunicipio().criarUnidadeFederacao();
	}

	/**
	 * Pesquisar municipio tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarMunicipioTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarMunicipioTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Consultar ceps.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Cep> consultarCeps(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorEndereco().consultarCeps(filtro);
	}

	/**
	 * Consultar cliente endereco.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ClienteEndereco> consultarClienteEndereco(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorCliente().consultarClienteEndereco(filtro);
	}

	/**
	 * Consultar feriado.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Feriado> consultarFeriado(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFeriado().consultarFeriado(filtro);
	}

	/**
	 * Criar tipo pessoa.
	 *
	 * @return the tipo pessoa
	 * @throws GGASException the GGAS exception
	 */
	public TipoPessoa criarTipoPessoa() throws GGASException {

		return (TipoPessoa) this.getControladorCliente().criarTipoPessoa();
	}

	/**
	 * Criar periodicidade.
	 *
	 * @return the periodicidade
	 * @throws GGASException the GGAS exception
	 */
	public Periodicidade criarPeriodicidade() throws GGASException {

		return (Periodicidade) this.getControladorRota().criarPeriodicidade();
	}

	/**
	 * Criar tipo leitura.
	 *
	 * @return the tipo leitura
	 * @throws GGASException the GGAS exception
	 */
	public TipoLeitura criarTipoLeitura() throws GGASException {

		return (TipoLeitura) this.getControladorRota().criarTipoLeitura();
	}

	/**
	 * Consultar mensagem segmento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<MensagemSegmento> consultarMensagemSegmento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorMensagemFaturamento().consultarMensagemSegmento(filtro);
	}

	/**
	 * Consultar imovel ramo atividade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ImovelRamoAtividade> consultarImovelRamoAtividade(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorImovel().consultarImovelRamoAtividade(filtro);
	}

	/**
	 * Consultar natureza operacao configuracao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<NaturezaOperacaoConfiguracao> consultarNaturezaOperacaoConfiguracao(Map<String, Object> filtro)
					throws NegocioException {

		return this.getControladorFatura().consultarNaturezaOperacaoConfiguracao(filtro);
	}

	/**
	 * Criar city gate.
	 *
	 * @return the city gate
	 * @throws GGASException the GGAS exception
	 */
	public CityGate criarCityGate() throws GGASException {

		return (CityGate) this.getControladorCityGateMedicao().criarCityGate();
	}

	/**
	 * Pesquisar tronco tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarTroncoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarTroncoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Consultar rede distribuicao tronco.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Rede> consultarRedeDistribuicaoTronco(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorRede().consultarRedeDistribuicaoTronco(filtro);
	}

	/**
	 * Consultar pontos consumo city gate.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumoCityGate> consultarPontosConsumoCityGate(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorPontoConsumo().consultarPontosConsumoCityGate(filtro);
	}

	/**
	 * Consultar pontos consumo city gate medicao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<CityGateMedicao> consultarPontosConsumoCityGateMedicao(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorCityGateMedicao().consultarPontosConsumoCityGateMedicao(filtro);
	}

	/**
	 * Pesquisar tipo cliente tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarTipoClienteTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarTipoClienteTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Pesquisar codigo ibge tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarCodigoIBGETabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarCodigoIBGETabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Pesquisar microrregiao tabela auxiliar.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @param titulo the titulo
	 * @throws NegocioException the negocio exception
	 */
	public void pesquisarMicrorregiaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		this.getControladorTabelaAuxiliar().pesquisarMicrorregiaoTabelaAuxiliar(tabelaAuxiliar, titulo);
	}

	/**
	 * Inserir feriado.
	 *
	 * @param listaDatasFeriado the lista datas feriado
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String inserirFeriado(String[] listaDatasFeriado, TabelaAuxiliar tabelaAuxiliar) throws GGASException {

		return this.getControladorTabelaAuxiliar().inserirFeriado(listaDatasFeriado, tabelaAuxiliar);
	}

	/**
	 * Inserir feriado replicado.
	 *
	 * @param listaTabelaAuxiliar the lista tabela auxiliar
	 * @throws GGASException the GGAS exception
	 */
	public void inserirFeriadoReplicado(Collection<TabelaAuxiliar> listaTabelaAuxiliar) throws GGASException {

		this.getControladorTabelaAuxiliar().inserirFeriadoReplicado(listaTabelaAuxiliar);
	}

	/**
	 * Ordenar lista tabela auxiliar.
	 *
	 * @param listaTabelaAuxiliar the lista tabela auxiliar
	 * @return the collection
	 */
	public Collection<TabelaAuxiliar> ordenarListaTabelaAuxiliar(Collection<TabelaAuxiliar> listaTabelaAuxiliar) {

		return this.getControladorTabelaAuxiliar().ordenarListaTabelaAuxiliar(listaTabelaAuxiliar);
	}

	/**
	 * Validar faixas zeradas area construida.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarFaixasZeradasAreaConstruida(List<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		this.getControladorTabelaAuxiliar().validarFaixasZeradasTarifa(listaDadosFaixa);
	}

	/**
	 * Validar faixas zeradas faixa consumo variacao.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarFaixasZeradasFaixaConsumoVariacao(List<FaixaConsumoVariacao> listaDadosFaixa) throws NegocioException,
					FormatoInvalidoException {

		this.getControladorFaixaConsumoVariacao().validarFaixasZeradasFaixaConsumoVariacao(listaDadosFaixa);
	}

	/**
	 * Validar faixas area construida.
	 *
	 * @param listaDadosFaixa the lista dados faixa
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarFaixasAreaConstruida(Collection<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		this.getControladorTabelaAuxiliar().validarFaixasTarifa(listaDadosFaixa);
	}

	/**
	 * Criar tabela auxiliar.
	 *
	 * @return the tabela auxiliar
	 */
	public TabelaAuxiliar criarTabelaAuxiliar() {

		return this.getControladorTabelaAuxiliar().criar();
	}

	/**
	 * Verificar area contruida.
	 *
	 * @param faixaHabilitadas the faixa habilitadas
	 * @param listaDadosFaixaAux the lista dados faixa aux
	 * @return the list
	 */
	public List<TabelaAuxiliar> verificarAreaContruida(String[] faixaHabilitadas, List<TabelaAuxiliar> listaDadosFaixaAux) {

		return this.getControladorTabelaAuxiliar().verificarAreaContruida(faixaHabilitadas, listaDadosFaixaAux);
	}

	/**
	 * Consultar cliente imovel.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ClienteImovel> consultarClienteImovel(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorClienteImovel().consultarClienteImovel(filtro);
	}

	/**
	 * Consultar fatura impressao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<FaturaImpressao> consultarFaturaImpressao(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFatura().consultarFaturaImpressao(filtro);
	}

	/**
	 * Verificar ano mes referencia.
	 *
	 * @param anoMesReferencia the ano mes referencia
	 * @throws NegocioException the negocio exception
	 */
	public void verificarAnoMesReferencia(Integer anoMesReferencia) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarAnoMesReferencia(anoMesReferencia);
	}

	/**
	 * Verificar periodicidade numero ciclos.
	 *
	 * @param numeroCiclo the numero ciclo
	 * @param periodicidade the periodicidade
	 * @throws NegocioException the negocio exception
	 */
	public void verificarPeriodicidadeNumeroCiclos(Integer numeroCiclo, Periodicidade periodicidade) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarPeriodicidadeNumeroCiclos(numeroCiclo, periodicidade);
	}

	/**
	 * Verificar cronograma grupo faturamento.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @param mesAnoPartida the mes ano partida
	 * @param numeroCiclo the numero ciclo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void verificarCronogramaGrupoFaturamento(Long idGrupoFaturamento, Integer mesAnoPartida, Integer numeroCiclo)
					throws NegocioException, ConcorrenciaException {

		this.getControladorCronogramaFaturamento().verificarCronogramaGrupoFaturamento(idGrupoFaturamento, mesAnoPartida, numeroCiclo);
	}

	/**
	 * Consultar todos modulos.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Modulo> consultarTodosModulos() throws NegocioException {

		return this.getControladorModulo().consultarTodosModulos();
	}

	/**
	 * Validar endereco remoto.
	 *
	 * @param enderecoRemotoPC the endereco remoto pc
	 * @throws NegocioException the negocio exception
	 */
	public void validarEnderecoRemoto(String enderecoRemotoPC) throws NegocioException {

		this.getControladorImovel().validarEnderecoRemoto(enderecoRemotoPC);

	}

	/**
	 * Criar rede indicador.
	 *
	 * @return the rede indicador
	 * @throws GGASException the GGAS exception
	 */
	public RedeIndicador criarRedeIndicador() throws GGASException {

		return (RedeIndicador) this.getControladorRede().criarRedeIndicador();
	}

	/**
	 * Consultar supervisorio medicao diaria.
	 *
	 * @param filtro the filtro
	 * @param ordenacao the ordenacao
	 * @param propriedadesLazy the propriedades lazy
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoDiaria(filtro, ordenacao, propriedadesLazy);

	}

	/**
	 * Consultar supervisorio medicao diaria.
	 *
	 * @param filtro the filtro
	 * @param ordenacao the ordenacao
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao)
					throws NegocioException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoDiaria(filtro, ordenacao);
	}

	/**
	 * Consultar supervisorio medicao horaria.
	 *
	 * @param filtro the filtro
	 * @param ordenacao the ordenacao
	 * @param propriedadesLazy the propriedades lazy
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoHoraria> consultarSupervisorioMedicaoHoraria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoHoraria(filtro, ordenacao, propriedadesLazy);
	}

	/**
	 * Listar todos indicadores rede.
	 *
	 * @return the collection
	 */
	public Collection<RedeIndicador> listarTodosIndicadoresRede() {

		return this.getControladorRede().listarTodosIndicadoresRede();
	}

	/**
	 * Consultar supervisorio medicao comentario.
	 *
	 * @param filtro the filtro
	 * @param ordenacao the ordenacao
	 * @param propriedadesLazy the propriedades lazy
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoComentario> consultarSupervisorioMedicaoComentario(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoComentario(filtro, ordenacao, propriedadesLazy);
	}

	/**
	 * Método responsável por criar uma
	 * SupervisorioMedicaoDiaria.
	 *
	 * @return the entidade negocio
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public EntidadeNegocio criarSupervisorioMedicaoDiaria() throws GGASException {

		return this.getControladorSupervisorio().criarSupervisorioMedicaoDiaria();
	}

	/**
	 * Método responsável por atualizar
	 * contrato adequação parceria.
	 *
	 * @param contratoAdequacaoParceria the contrato adequacao parceria
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarContratoAdequacaoParcria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws GGASException {

		getControladorContratoAdequacaoParceria().atualizarContratoAdequacaoPerceria(contratoAdequacaoParceria);
	}

	/**
	 * Remover contrato adequacao parceria.
	 *
	 * @param contratoAdequacaoParceria the contrato adequacao parceria
	 * @throws GGASException the GGAS exception
	 */
	public void removerContratoAdequacaoParceria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws GGASException {

		getControladorContratoAdequacaoParceria().removerContratoAdequacaoParceria(contratoAdequacaoParceria);
	}

	/**
	 * Checks if is exists ponto consumo in contrato adequacao parceria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ContratoAdequacaoParceria> isExistsPontoConsumoInContratoAdequacaoParceria(Long chavePrimaria)
					throws NegocioException {

		return getControladorContratoAdequacaoParceria().isExistsPontoConsumo(chavePrimaria);
	}

	/**
	 * Obter supervisorio medicao diaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the supervisorio medicao diaria
	 * @throws NegocioException the negocio exception
	 */
	public SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria) throws NegocioException {

		return this.getControladorSupervisorio().obterSupervisorioMedicaoDiaria(chavePrimaria);
	}

	/**
	 * Obter supervisorio medicao diaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the supervisorio medicao diaria
	 * @throws NegocioException the negocio exception
	 */
	public SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.getControladorSupervisorio().obterSupervisorioMedicaoDiaria(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Obter supervisorio medicao horaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 */
	public SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria) throws NegocioException {

		return this.getControladorSupervisorio().obterSupervisorioMedicaoHoraria(chavePrimaria);
	}

	/**
	 * Obter supervisorio medicao horaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 */
	public SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		return this.getControladorSupervisorio().obterSupervisorioMedicaoHoraria(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Atualizar.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param classe the classe
	 * @throws GGASException the GGAS exception
	 */
	public void atualizar(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Class<?> classe) throws GGASException {

		this.getControladorSupervisorio().atualizar(supervisorioMedicaoDiaria, classe);
	}

	/**
	 * Atualizar.
	 *
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @throws GGASException the GGAS exception
	 */
	public void atualizar(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws GGASException {

		this.getControladorSupervisorio().atualizar(supervisorioMedicaoHoraria);
	}

	/**
	 * Atualizar.
	 *
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @param classe the classe
	 * @throws GGASException the GGAS exception
	 */
	public void atualizar(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, Class<?> classe) throws GGASException {

		this.getControladorSupervisorio().atualizar(supervisorioMedicaoHoraria, classe);
	}

	/**
	 * Inserir supervisorio medicao comentario.
	 *
	 * @param supervisorioMedicaoComentario the supervisorio medicao comentario
	 * @throws GGASException the GGAS exception
	 */
	public void inserirSupervisorioMedicaoComentario(SupervisorioMedicaoComentario supervisorioMedicaoComentario) throws GGASException {

		this.getControladorSupervisorio().inserir(supervisorioMedicaoComentario);
	}

	/**
	 * Inserir supervisorio medicao horaria.
	 *
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @throws GGASException the GGAS exception
	 */
	public void inserirSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws GGASException {

		this.getControladorSupervisorio().inserir(supervisorioMedicaoHoraria);
	}

	/**
	 * Validar dados supervisorio medicao diaria.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		this.getControladorSupervisorio().validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
	}

	/**
	 * Inserir supervisorio medicao diaria.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria the parametro tabela super medicao diaria
	 * @param supervisorioMedicaoComentarioNovo the supervisorio medicao comentario novo
	 * @param listaSupervisorioMedicaoComentario the lista supervisorio medicao comentario
	 * @param possuiComentariosAnteriores the possui comentarios anteriores
	 * @param validaAlcada the valida alcada
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					ParametroSistema parametroTabelaSuperMedicaoDiaria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					Boolean validaAlcada) throws GGASException {

		return this.getControladorSupervisorio().inserirSupervisorioMedicaoDiaria(dadosAuditoria, supervisorioMedicaoDiaria,
						parametroTabelaSuperMedicaoDiaria, supervisorioMedicaoComentarioNovo, listaSupervisorioMedicaoComentario,
						possuiComentariosAnteriores, null, validaAlcada);
	}

	/**
	 * Inserir supervisorio medicao horaria.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @param parametroTabelaSuperMedicaoHoraria the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoComentarioNovo the supervisorio medicao comentario novo
	 * @param listaSupervisorioMedicaoComentario the lista supervisorio medicao comentario
	 * @param possuiComentariosAnteriores the possui comentarios anteriores
	 * @param validaAlcada the valida alcada
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirSupervisorioMedicaoHoraria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					Boolean validaAlcada) throws GGASException {

		return this.getControladorSupervisorio().inserirSupervisorioMedicaoHoraria(dadosAuditoria, supervisorioMedicaoHoraria,
						parametroTabelaSuperMedicaoHoraria, supervisorioMedicaoComentarioNovo, listaSupervisorioMedicaoComentario,
						possuiComentariosAnteriores, null, validaAlcada);
	}

	/**
	 * Tratar medicoes diarias.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicialRealizacaoLeitura the data inicial realizacao leitura
	 * @param dataFinalRealizacaoLeitura the data final realizacao leitura
	 * @param logProcessamento the log processamento
	 * @param mapaAnormalidadeSupervisorioMedicaoDiaria the mapa anormalidade supervisorio medicao diaria
	 * @param resumoSupervisorioMedicaoVO the resumo supervisorio medicao vo
	 * @throws GGASException the GGAS exception
	 */
	public void tratarMedicoesDiarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura, Date dataFinalRealizacaoLeitura,
					StringBuilder logProcessamento, Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO) throws GGASException {

		this.getControladorSupervisorio().tratarMedicoesDiarias(pontoConsumo, dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura,
						logProcessamento, mapaAnormalidadeSupervisorioMedicaoDiaria, resumoSupervisorioMedicaoVO, null);
	}

	/**
	 * Tratar medicoes horarias.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicialRealizacaoLeitura the data inicial realizacao leitura
	 * @param dataFinalRealizacaoLeitura the data final realizacao leitura
	 * @param logProcessamento the log processamento
	 * @param mapaAnormalidadeSupervisorioMedicaoHoraria the mapa anormalidade supervisorio medicao horaria
	 * @param resumoSupervisorioMedicaoVO the resumo supervisorio medicao vo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, List<Object[]>> tratarMedicoesHorarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura,
					Date dataFinalRealizacaoLeitura, StringBuilder logProcessamento,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoHoraria,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO) throws GGASException {

		return this.getControladorSupervisorio().tratarMedicoesHorarias(pontoConsumo, dataInicialRealizacaoLeitura,
						dataFinalRealizacaoLeitura, logProcessamento, mapaAnormalidadeSupervisorioMedicaoHoraria,
						resumoSupervisorioMedicaoVO, null, null);
	}

	/**
	 * Possui alcada autorizacao supervisorio medicao diaria.
	 *
	 * @param usuario the usuario
	 * @param pendente the pendente
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(Usuario usuario, SupervisorioMedicaoDiaria pendente)
					throws NegocioException {

		return this.getControladorAlcada().possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(usuario, pendente);
	}

	/**
	 * Possui alcada autorizacao supervisorio medicao horaria.
	 *
	 * @param usuario the usuario
	 * @param pendente the pendente
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(Usuario usuario, SupervisorioMedicaoHoraria pendente)
					throws NegocioException {

		return this.getControladorAlcada().possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(usuario, pendente);
	}

	/**
	 * Validar dados obrigatorios supervisorio medicao diaria.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaVO the supervisorio medicao diaria vo
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosObrigatoriosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO) throws NegocioException {

		this.getControladorSupervisorio().validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria,
						supervisorioMedicaoDiariaVO);
	}

	/**
	 * Consultar ponto consumo por rota.
	 *
	 * @param rota the rota
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumo> consultarPontoConsumoPorRota(Rota rota) throws NegocioException {

		return this.getControladorPontoConsumo().consultarPontoConsumoPorRota(rota);
	}

	/**
	 * Validar dados supervisorio medicao horaria.
	 *
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		this.getControladorSupervisorio().validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
	}

	/**
	 * Transferir supervisorio medicoes diaria.
	 *
	 * @param listaMedicoesDiarias the lista medicoes diarias
	 * @param filtro the filtro
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void transferirSupervisorioMedicoesDiaria(Collection<SupervisorioMedicaoVO> listaMedicoesDiarias, Map<String, Object> filtro)
					throws NegocioException, ConcorrenciaException, FormatoInvalidoException {

		this.getControladorSupervisorio().transferirSupervisorioMedicoesDiaria(listaMedicoesDiarias, filtro);
	}

	/**
	 * Atualizar registros remocao logica supervisorio medicao diaria.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param chavePrimariaSupervisorioMedicaoDiaria the chave primaria supervisorio medicao diaria
	 * @param ativo the ativo
	 * @param indicadorTipoMedicao the indicador tipo medicao
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao) throws GGASException {

		this.getControladorSupervisorio().atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria,
						chavePrimariaSupervisorioMedicaoDiaria, ativo, indicadorTipoMedicao);
	}

	/**
	 * Atualizar registros alterados supervisorio medicao diaria.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria the parametro tabela super medicao diaria
	 * @param chavePrimariaSupervisorioMedicaoDiaria the chave primaria supervisorio medicao diaria
	 * @param ativo the ativo
	 * @param indicadorTipoMedicao the indicador tipo medicao
	 * @param supervisorioMedicaoDiariaNovo the supervisorio medicao diaria novo
	 * @param listaSupervisorioMedicaoComentario the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoComentarioNovo the supervisorio medicao comentario novo
	 * @param possuiComentariosAnteriores the possui comentarios anteriores
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo, Boolean possuiComentariosAnteriores)
					throws GGASException {

		this.getControladorSupervisorio().atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(dadosAuditoria, supervisorioMedicaoDiaria,
						parametroTabelaSuperMedicaoDiaria, chavePrimariaSupervisorioMedicaoDiaria, ativo, indicadorTipoMedicao,
						supervisorioMedicaoDiariaNovo, listaSupervisorioMedicaoComentario, supervisorioMedicaoComentarioNovo,
						possuiComentariosAnteriores);
	}

	/**
	 * Atualizar supervisorio medicao horaria.
	 *
	 * @param filtro the filtro
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarSupervisorioMedicaoHoraria(Map<String, Object> filtro, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
					throws GGASException {

		this.getControladorSupervisorio().atualizarSupervisorioMedicaoHoraria(filtro, supervisorioMedicaoDiaria);
	}

	/**
	 * Inserir supervisorio medicao horaria.
	 *
	 * @param parametroTabelaSuperMedicaoHoraria the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoComentario the supervisorio medicao comentario
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 */
	public SupervisorioMedicaoHoraria inserirSupervisorioMedicaoHoraria(ParametroSistema parametroTabelaSuperMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoComentario supervisorioMedicaoComentario) throws NegocioException {

		return this.getControladorSupervisorio().inserirSupervisorioMedicaoHoraria(parametroTabelaSuperMedicaoHoraria,
						supervisorioMedicaoHoraria, dadosAuditoria, supervisorioMedicaoComentario);
	}

	/**
	 * Remover supervisorio medicao horaria.
	 *
	 * @param supervisorioMedicaoComentario the supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void removerSupervisorioMedicaoHoraria(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException, ConcorrenciaException {

		this.getControladorSupervisorio().removerSupervisorioMedicaoHoraria(supervisorioMedicaoComentario, supervisorioMedicaoHoraria);
	}

	/**
	 * Atualizar supervisorio medicao horaria.
	 *
	 * @param listaSupervisorioMedicaoComentario the lista supervisorio medicao comentario
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoHorariaNovo the supervisorio medicao horaria novo
	 * @param supervisorioMedicaoComentario the supervisorio medicao comentario
	 * @param parametroTabelaSuperMedicaoHoraria the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoHorariaVO the supervisorio medicao horaria vo
	 * @param supervisorioMedicaoHoraria the supervisorio medicao horaria
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public SupervisorioMedicaoHoraria atualizarSupervisorioMedicaoHoraria(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo, SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoVO supervisorioMedicaoHorariaVO,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException, ConcorrenciaException {

		return this.getControladorSupervisorio().atualizarSupervisorioMedicaoHoraria(listaSupervisorioMedicaoComentario, dadosAuditoria,
						supervisorioMedicaoHorariaNovo, supervisorioMedicaoComentario, parametroTabelaSuperMedicaoHoraria,
						supervisorioMedicaoHorariaVO, supervisorioMedicaoHoraria);
	}

	/**
	 * Atualizar registros alcada supervisorio medicao diaria.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria the parametro tabela super medicao diaria
	 * @param supervisorioMedicaoDiariaNovo the supervisorio medicao diaria novo
	 * @param listaSupervisorioMedicaoComentario the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoComentarioNovo the supervisorio medicao comentario novo
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarRegistrosAlcadaSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo) throws GGASException {

		this.getControladorSupervisorio().atualizarRegistrosAlcadaSupervisorioMedicaoDiaria(dadosAuditoria, supervisorioMedicaoDiaria,
						parametroTabelaSuperMedicaoDiaria, supervisorioMedicaoDiariaNovo, listaSupervisorioMedicaoComentario,
						supervisorioMedicaoComentarioNovo);
	}

	/**
	 * Obter contrato faturavel por ponto consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param faturavel the faturavel
	 * @return the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	public ContratoPontoConsumo obterContratoFaturavelPorPontoConsumo(Long idPontoConsumo, Boolean faturavel) throws NegocioException {

		return getControladorContrato().obterContratoFaturavelPorPontoConsumo(idPontoConsumo, faturavel);
	}

	/**
	 * Validar contrato ativo por data leitura.
	 *
	 * @param dataLeitura the data leitura
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param pontoConsumo the ponto consumo
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<String, Object> validarContratoAtivoPorDataLeitura(Date dataLeitura, ContratoPontoConsumo contratoPontoConsumo,
					PontoConsumo pontoConsumo) throws NegocioException {

		return getControladorContrato().validarContratoAtivoPorDataLeitura(dataLeitura, contratoPontoConsumo, pontoConsumo);
	}

	/**
	 * Método responsável por listar os setores comerciais para uma localidade informada.
	 *
	 * @param filtro Filtro que contém o id da localidade selecionada.
	 * @return Uma coleção de Setores Comerciais.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<SetorComercial> listarSetoresComerciaisPorLocalidade(Map<String, Object> filtro) throws GGASException {

		return this.getControladorSetorComercial().consultarSetorComercial(filtro);
	}

	/**
	 * Validar ano mes referencia ciclo.
	 *
	 * @param anoMesReferencia the ano mes referencia
	 * @param ciclo the ciclo
	 * @param pontoConsumo the ponto consumo
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param paramStatusAutorizado the param status autorizado
	 * @param ultimoHistoricoConsumo the ultimo historico consumo
	 * @param periodicidade the periodicidade
	 * @return the boolean
	 * @throws GGASException the GGAS exception
	 */
	public Boolean validarAnoMesReferenciaCiclo(Integer anoMesReferencia, Integer ciclo, PontoConsumo pontoConsumo,
					Date dataRealizacaoLeitura, String paramStatusAutorizado, HistoricoConsumo ultimoHistoricoConsumo,
					Periodicidade periodicidade) throws GGASException {

		return getControladorSupervisorio().validarAnoMesReferenciaCiclo(anoMesReferencia, ciclo, pontoConsumo, dataRealizacaoLeitura,
						paramStatusAutorizado, ultimoHistoricoConsumo, periodicidade);
	}

	/**
	 * Obter periodicidade por ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @return the periodicidade
	 * @throws NegocioException the negocio exception
	 */
	public Periodicidade obterPeriodicidadePorPontoConsumo(PontoConsumo pontoConsumo, Date dataRealizacaoLeitura) throws NegocioException {

		return getControladorContrato().obterPeriodicidadePorPontoConsumo(pontoConsumo, dataRealizacaoLeitura);
	}

	/**
	 * Listar conteudo tabela coluna.
	 *
	 * @param tabela the tabela
	 * @param listaFiltros the lista filtros
	 * @return the list
	 * @throws NegocioException
	 */
	public List<Object> listarConteudoTabelaColuna(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros) throws NegocioException{

		return this.getControladorIntegracao().listarConteudoTabelaColuna(tabela, listaFiltros);
	}

	/**
	 * Método responsável por criar um
	 * Sistema Integrante.
	 *
	 * @return the integracao sistema
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public IntegracaoSistema criarIntegracaoSistema() throws GGASException {

		return (IntegracaoSistema) this.getControladorIntegracao().criarIntegracaoSistema();
	}

	/**
	 * Criar integracao funcao.
	 *
	 * @return the integracao funcao
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoFuncao criarIntegracaoFuncao() throws GGASException {

		return (IntegracaoFuncao) this.getControladorIntegracao().criarIntegracaoFuncao();
	}

	/**
	 * Criar integracao sistema funcao.
	 *
	 * @return the integracao sistema funcao
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistemaFuncao criarIntegracaoSistemaFuncao() throws GGASException {

		return (IntegracaoSistemaFuncao) this.getControladorIntegracao().criarIntegracaoSistemaFuncao();
	}

	/**
	 * Criar integracao sistema parametro.
	 *
	 * @return the integracao sistema parametro
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistemaParametro criarIntegracaoSistemaParametro() throws GGASException {

		return (IntegracaoSistemaParametro) this.getControladorIntegracao().criarIntegracaoSistemaParametro();
	}

	/**
	 * Criar faixa pressao fornecimento.
	 *
	 * @return the faixa pressao fornecimento
	 * @throws GGASException the GGAS exception
	 */
	public FaixaPressaoFornecimento criarFaixaPressaoFornecimento() throws GGASException {

		return (FaixaPressaoFornecimento) this.getControladorFaixaPressaoFornecimento().criar();
	}

	/**
	 * Método responsável por inserir um sistema integrante.
	 *
	 * @param integracaoSistema the integracao sistema
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirIntegracaoSistema(IntegracaoSistema integracaoSistema) throws GGASException {

		return this.getControladorIntegracao().inserir(integracaoSistema);
	}

	/**
	 * Método responsável por inserir o relacionamento entre sistema integrante e função.
	 *
	 * @param integracaoSistemaFuncao the integracao sistema funcao
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirIntegracaoSistemaFuncao(IntegracaoSistemaFuncao integracaoSistemaFuncao) throws GGASException {

		return this.getControladorIntegracao().inserir(integracaoSistemaFuncao);
	}

	/**
	 * Método responsável por atualizar o relacionamento entre sistema integrante e função.
	 *
	 * @param integracaoSistemaFuncao the integracao sistema funcao
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarIntegracaoSistemaFuncao(IntegracaoSistemaFuncao integracaoSistemaFuncao) throws GGASException {

		this.getControladorIntegracao().atualizar(integracaoSistemaFuncao, IntegracaoSistemaFuncaoImpl.class);
	}

	/**
	 * Método responsável por consultar as
	 * integrações sistemas de acordo com o filtro.
	 *
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de integracao sistema.
	 * @return Uma coleção de integrações.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<IntegracaoSistema> consultarIntegracaoSistema(Map<String, Object> filtro) throws GGASException {

		return this.getControladorIntegracao().consultarIntegracaoSistema(filtro);
	}

	/**
	 * Método responsável por validar integração de contrato
	 *
	 * @param idContrato    O id do contrato que será validado
	 * @param idContratoPai O id do contrato pai
	 * @return true se o contrato estiver integrado e false caso contrário
	 * @throws GGASException O GGASException
	 */
	public Boolean validarIntegracaoContrato(Long idContrato, Long idContratoPai) throws GGASException {

		return this.getControladorIntegracao().validarIntegracaoContrato(idContrato, idContratoPai);
	}

	/**
	 * Método responsável por atualizar
	 * sistema integrante.
	 *
	 * @param integracaoSistema the integracao sistema
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarIntegracaoSistema(IntegracaoSistema integracaoSistema) throws GGASException {

		this.getControladorIntegracao().atualizarIntegracaoSistema(integracaoSistema);
	}

	/**
	 * Método responsável por verificar se a referência para integração de nota e título está configurada para contrato.
	 *
	 * @return true se a integração está ativa e false caso contrário
	 */
	public boolean integracaoPrecedenteContrato() {
		return getControladorIntegracao().integracaoPrecedenteContratoAtiva();
	}

	/**
	 * Método responsável por obter
	 * um sistema integrante.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the integracao sistema
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistema obterIntegracaoSistema(long chavePrimaria) throws GGASException {

		return getControladorIntegracao().obterIntegracaoSistema(chavePrimaria);
	}

	/**
	 * Método responsável por obter
	 * um Função das tabela.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */

	public Collection<IntegracaoFuncaoTabela> obterIntegracaoFuncaoTabela(long chavePrimaria) throws GGASException {

		return getControladorIntegracao().obterIntegracaoFuncaoTabela(chavePrimaria);
	}

	/**
	 * Método responsável por remover
	 * um sistema integrante.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerIntegracaoSistema(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorIntegracao().removerIntegracaoSistema(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Método responsável por listar os dados
	 * de IntegracaoSistemaFuncao.
	 *
	 * @param integracaoSistema the integracao sistema
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(IntegracaoSistema integracaoSistema) throws GGASException {

		return getControladorIntegracao().listarIntegracaoSistemaFuncao(integracaoSistema);
	}

	/**
	 * Método responsável por verificar
	 * se já existe determinado sistema integrante.
	 *
	 * @param filtro the filtro
	 * @throws GGASException the GGAS exception
	 */
	public void verificarSistemaIntegranteExistente(Map<String, Object> filtro) throws GGASException {

		this.getControladorIntegracao().verificarSistemaIntegranteExistente(filtro);
	}

	/**
	 * Método responsável por verificar se
	 * existe integrações ativas.
	 *
	 * @param filtro the filtro
	 * @throws GGASException the GGAS exception
	 */
	public void verificarExistenciaIntegracoesAtivas(Map<String, Object> filtro) throws GGASException {

		this.getControladorIntegracao().verificarExistenciaIntegracoesAtivas(filtro);
	}

	/**
	 * Método responsável por verificar se
	 * existe integrações relacionadas.
	 *
	 * @param filtro the filtro
	 * @throws GGASException the GGAS exception
	 */
	public void verificarExistenciaIntegracoesRelacionadas(Map<String, Object> filtro) throws GGASException {

		this.getControladorIntegracao().verificarExistenciaIntegracoesRelacionadas(filtro);
	}

	/**
	 * Obter contrato adequação
	 * parceria por ponto de consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the contrato adequacao parceria
	 * @throws NegocioException the negocio exception
	 */

	public ContratoAdequacaoParceria consultarContratoAdequacaoParceriaPorPontoConsumo(Long idPontoConsumo) throws NegocioException {

		return getControladorContratoAdequacaoParceria().consultarContratoAdequacaoParceriaPorPontoConsumo(idPontoConsumo);
	}

	/**
	 * Método responsável por validar
	 * os dados do sistema integrante
	 * SIGLA e NOME.
	 *
	 * @param integracaoSistema the integracao sistema
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosSistemaIntegrante(IntegracaoSistema integracaoSistema) throws GGASException {

		this.getControladorIntegracao().validarDadosSistemaIntegrante(integracaoSistema);
	}

	/**
	 * Listar integracao sistema.
	 *
	 * @return the collection
	 */
	public Collection<IntegracaoSistema> listarIntegracaoSistema() {

		return getControladorIntegracao().listarIntegracaoSistema();
	}

	/**
	 * Listar integracao funcao.
	 *
	 * @return the collection
	 */
	public Collection<IntegracaoFuncao> listarIntegracaoFuncao() {

		return getControladorIntegracao().listarIntegracaoFuncao();
	}

	/**
	 * Listar integracao sistema funcao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(Map<String, Object> filtro) {

		return getControladorIntegracao().listarIntegracaoSistemaFuncao(filtro);
	}

	/**
	 * Listar integracao sistema funcao.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncaoCadastroBens() throws NegocioException {

		return getControladorIntegracao().listarIntegracaoSistemaFuncaoCadastroBens();
	}

	/**
	 * Listar integracao funcao tabela.
	 *
	 * @return the collection
	 */
	public Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela() {

		return getControladorIntegracao().listarIntegracaoFuncaoTabela();
	}

	/**
	 * Consultar integracao sistema funcao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoSistemaFuncao> consultarIntegracaoSistemaFuncao(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorIntegracao().consultarIntegracaoSistemaFuncao(filtro);
	}

	/**
	 * Consultar integracao funcao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoFuncao> consultarIntegracaoFuncao(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorIntegracao().consultarIntegracaoFuncao(filtro);
	}

	/**
	 * Remover sistema funcao.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerSistemaFuncao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorIntegracao().removerSistemaFuncao(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Listar parametros associados.
	 *
	 * @param integracaoFuncao the integracao funcao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<IntegracaoParametro> listarParametrosAssociados(IntegracaoFuncao integracaoFuncao) throws GGASException {

		return this.getControladorIntegracao().listarParametrosAssociados(integracaoFuncao);
	}

	/**
	 * Listar mapeamentos associados.
	 *
	 * @param integracaoFuncao the integracao funcao
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<IntegracaoFuncaoTabela> listarMapeamentosAssociados(IntegracaoFuncao integracaoFuncao) throws GGASException {

		return this.getControladorIntegracao().listarMapeamentosAssociados(integracaoFuncao);
	}

	/**
	 * Obter integracao sistema funcao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the integracao sistema funcao
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chavePrimaria) throws GGASException {

		return getControladorIntegracao().obterIntegracaoSistemaFuncao(chavePrimaria);
	}

	/**
	 * Obter integracao sistema funcao.
	 *
	 * @param chaveSistema the chave sistema
	 * @param chaveFuncao the chave funcao
	 * @return the integracao sistema funcao
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chaveSistema, long chaveFuncao) throws GGASException {

		return getControladorIntegracao().obterIntegracaoSistemaFuncao(chaveSistema, chaveFuncao);
	}

	/**
	 * Inserir integracao sistema parametro.
	 *
	 * @param integracaoSistemaParametro the integracao sistema parametro
	 * @throws GGASException the GGAS exception
	 */
	public void inserirIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws GGASException {

		this.getControladorIntegracao().inserirIntegracaoSistemaParametro(integracaoSistemaParametro);
	}

	/**
	 * Atualizar integracao sistema parametro.
	 *
	 * @param integracaoSistemaParametro the integracao sistema parametro
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws GGASException {

		this.getControladorIntegracao().atualizarIntegracaoSistemaParametro(integracaoSistemaParametro);
	}

	/**
	 * Obter integracao sistema parametro.
	 *
	 * @param idIntegracaoSistemaParametro the id integracao sistema parametro
	 * @return the integracao sistema parametro
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoSistemaParametro obterIntegracaoSistemaParametro(Long idIntegracaoSistemaParametro) throws GGASException {

		return (IntegracaoSistemaParametro) this.getControladorIntegracao().obter(idIntegracaoSistemaParametro,
						IntegracaoSistemaParametroImpl.class);
	}

	/**
	 * Inserir integracao sistema mapeamento.
	 *
	 * @param integracaoSistemaMapeamento the integracao sistema mapeamento
	 * @throws GGASException the GGAS exception
	 */
	public void inserirIntegracaoSistemaMapeamento(IntegracaoMapeamento integracaoSistemaMapeamento) throws GGASException {

		this.getControladorIntegracao().inserirIntegracaoSistemaMapeamento(integracaoSistemaMapeamento);
	}

	/**
	 * Obter integracao funcao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the integracao funcao
	 * @throws GGASException the GGAS exception
	 */
	public IntegracaoFuncao obterIntegracaoFuncao(long chavePrimaria) throws GGASException {

		return getControladorIntegracao().obterIntegracaoFuncao(chavePrimaria);
	}

	/**
	 * Atualizar acompanhar integracao.
	 *
	 * @param entidadeNegocio the entidade negocio
	 * @param classe the classe
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarAcompanharIntegracao(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException,
					ConcorrenciaException {

		this.getControladorIntegracao().atualizarAcompanharIntegracao(entidadeNegocio, classe);
	}

	/**
	 * Consulta sistema funcao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the integracao sistema funcao
	 */
	public IntegracaoSistemaFuncao consultaSistemaFuncao(Long chavePrimaria) {

		return getControladorIntegracao().consultaSistemaFuncao(chavePrimaria);
	}

	/**
	 * Listar integracao funcao tabela.
	 *
	 * @param listaIntegracaoSistemaFuncao the lista integracao sistema funcao
	 * @param idIntegracaoFuncao the id integracao funcao
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela(
					Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao, Long idIntegracaoFuncao) throws NegocioException {

		return getControladorIntegracao().listarIntegracaoFuncaoTabela(listaIntegracaoSistemaFuncao, idIntegracaoFuncao);
	}

	/**
	 * Listar integracao mapeamento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoMapeamento> listarIntegracaoMapeamento(Map<String, Object> filtro) throws NegocioException {

		return getControladorIntegracao().listarIntegracaoMapeamento(filtro);
	}

	/**
	 * Exportar tabela integracao para csv.
	 *
	 * @param listaAcompanhamentoIntegracaoVO the lista acompanhamento integracao vo
	 * @param separador the separador
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 */
	public byte[] exportarTabelaIntegracaoParaCSV(List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO, String separador)
					throws NegocioException {

		return getControladorIntegracao().exportarTabelaIntegracaoParaCSV(listaAcompanhamentoIntegracaoVO, separador);
	}

	/**
	 * Localizar integracao mapeamento.
	 *
	 * @param idIntegracaoSistemaParametro the id integracao sistema parametro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntegracaoMapeamento> localizarIntegracaoMapeamento(Long idIntegracaoSistemaParametro) throws NegocioException {

		return getControladorIntegracao().localizarIntegracaoMapeamento(idIntegracaoSistemaParametro);
	}

	/**
	 * Validar destinatarios email.
	 *
	 * @param integracaoSistemaFuncao the integracao sistema funcao
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public void validarDestinatariosEmail(IntegracaoSistemaFuncao integracaoSistemaFuncao) throws NegocioException,
					FormatoInvalidoException {

		this.getControladorIntegracao().validarDestinatariosEmail(integracaoSistemaFuncao);
	}

	/**
	 * Remover integracao mapeamento.
	 *
	 * @param listaIntegracaoMapeamento the lista integracao mapeamento
	 * @throws NegocioException the negocio exception
	 */
	public void removerIntegracaoMapeamento(Collection<IntegracaoMapeamento> listaIntegracaoMapeamento) throws NegocioException {

		this.getControladorIntegracao().removerIntegracaoMapeamento(listaIntegracaoMapeamento);
	}

	/**
	 * Método responsável por obter uma constante
	 * através de um código.
	 *
	 * @param codigo O código da constante.
	 * @return A Constante
	 */
	public ConstanteSistema obterConstantePorCodigo(String codigo) {

		return this.getControladorConstanteSistema().obterConstantePorCodigo(codigo);
	}

	/**
	 * Obter documento fiscal por codigo fatura.
	 *
	 * @param codigoFatura the codigo fatura
	 * @param isValido the is valido
	 * @return the documento fiscal
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoFiscal obterDocumentoFiscalPorCodigoFatura(Long codigoFatura, boolean isValido) throws NegocioException {

		return this.getControladorFatura().obterDocumentoFiscalPorCodigoFatura(codigoFatura, isValido);
	}

	/**
	 * Obter lista motivo devolucao.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaMotivoDevolucao() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaMotivoDevolucao();
	}

	/**
	 * Obter dados incluir nfd fatura.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the dados resumo fatura
	 * @throws NegocioException the negocio exception
	 */
	public DadosResumoFatura obterDadosIncluirNFDFatura(Long chavePrimaria) throws NegocioException {

		return this.getControladorFatura().obterDadosIncluirNFDFatura(chavePrimaria);
	}

	/**
	 * Inserir nota fiscal devolucao.
	 *
	 * @param documentoFiscal the documento fiscal
	 * @param dadosResumoFatura the dados resumo fatura
	 * @param idFatura the id fatura
	 * @param idMotivoCancelamento the id motivo cancelamento
	 * @param consumoItem O consumo do item
	 * @param vlrUnitarioItem O valor unitário do item
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void inserirNotaFiscalDevolucao(DocumentoFiscal documentoFiscal, DadosResumoFatura dadosResumoFatura, Long idFatura,
			Long idMotivoCancelamento, Float[] consumoItem, Float[] vlrUnitarioItem, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorFatura().inserirNotaFiscalDevolucao(documentoFiscal, dadosResumoFatura, idFatura, idMotivoCancelamento,
				consumoItem, vlrUnitarioItem, dadosAuditoria);

	}

	/**
	 * Listar entidade classe.
	 *
	 * @param codigoEntidadeClasse the codigo entidade classe
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeClasse> listarEntidadeClasse(String codigoEntidadeClasse) throws NegocioException {

		return this.getControladorEntidadeConteudo().listarEntidadeClasse(codigoEntidadeClasse);
	}

	/**
	 * Gets the controlador serie.
	 *
	 * @return the controlador serie
	 */
	private ControladorSerie getControladorSerie() {

		return (ControladorSerie) serviceLocator.getControladorNegocio(ControladorSerie.BEAN_ID_CONTROLADOR_SERIE);
	}

	/**
	 * Listar serie.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Serie> listarSerie(Map<String, Object> filtro) {

		return getControladorSerie().listarSerie(filtro);
	}

	/**
	 * Criar serie.
	 *
	 * @return the serie
	 */
	public Serie criarSerie() {

		Serie serie = (Serie) this.getControladorSerie().criar();
		if (serie == null) {
			serie = (Serie) ServiceLocator.getInstancia().getBeanPorID(Serie.BEAN_ID_SERIE);
		}
		return serie;
	}

	/**
	 * Inserir serie.
	 *
	 * @param serie the serie
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	public Long inserirSerie(Serie serie) throws NegocioException {

		return this.getControladorSerie().inserir(serie);
	}

	/**
	 * Obter serie.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the serie
	 * @throws NegocioException the negocio exception
	 */
	public Serie obterSerie(Long chavePrimaria) throws NegocioException {

		return (Serie) getControladorSerie().obter(chavePrimaria, this.getControladorSerie().getClasseEntidade());
	}

	/**
	 * Alterar serie.
	 *
	 * @param serie the serie
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void alterarSerie(Serie serie) throws NegocioException, ConcorrenciaException {

		this.getControladorSerie().atualizar(serie);
	}

	/**
	 * Remover series.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	public void removerSeries(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorSerie().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Verificar serie em uso.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws NegocioException the negocio exception
	 */
	public void verificarSerieEmUso(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorSerie().verificarSerieEmUso(chavesPrimarias);
	}

	/**
	 * Existe serie em uso.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return true, if successful
	 */
	public boolean existeSerieEmUso(Long chavePrimaria) {

		return this.getControladorSerie().existeSerieEmUso(chavePrimaria);
	}

	/**
	 * Desabilitar recursos sistema.
	 *
	 * @param habilitado the habilitado
	 * @param chavePrimariaIntegSistFuncao the chave primaria integ sist funcao
	 * @throws NegocioException the negocio exception
	 */
	public void desabilitarRecursosSistema(boolean habilitado, long chavePrimariaIntegSistFuncao) throws NegocioException {

		this.getControladorIntegracao().desabilitarRecursosSistema(habilitado, chavePrimariaIntegSistFuncao);
	}

	/**
	 * Obter integracao sistema funcao situacao erro.
	 *
	 * @param intFunTabela the int fun tabela
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	public boolean obterIntegracaoSistemaFuncaoSituacaoErro(IntegracaoFuncaoTabela intFunTabela) throws GGASException {

		return getControladorIntegracao().obterIntegracaoSistemaFuncaoSituacaoErro(intFunTabela);
	}

	/**
	 * Listar conteudo tabela coluna situacao erro.
	 *
	 * @param tabela the tabela
	 * @param listaFiltros the lista filtros
	 * @param situacaoErro the situacao erro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<Object> listarConteudoTabelaColunaSituacaoErro(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros,
					Long situacaoErro) throws NegocioException {

		return this.getControladorIntegracao().listarConteudoTabelaColunaSituacaoErro(tabela, listaFiltros, situacaoErro);
	}

	/**
	 * Obter entidade por classe.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param nomeClasse the nome classe
	 * @return the object
	 * @throws NegocioException the negocio exception
	 */
	public Object obterEntidadePorClasse(Long chavePrimaria, Class<?> nomeClasse) throws NegocioException {

		return this.getControladorIntegracao().obterEntidadePorClasse(chavePrimaria, nomeClasse);

	}

	/**
	 * Obter entidade conteudo por chave.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param classeEntidade the classe entidade
	 * @return the entidade conteudo
	 * @throws NegocioException the negocio exception
	 */
	public EntidadeConteudo obterEntidadeConteudoPorChave(Long chavePrimaria, Long classeEntidade) throws NegocioException {

		return this.getControladorEntidadeConteudo().obterEntidadeConteudoPorChave(chavePrimaria, classeEntidade);

	}

	/**
	 * Obter supervisorio medicao diaria pendentes por usuario.
	 *
	 * @param filtro the filtro
	 * @param usuario the usuario
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException {

		return this.getControladorAlcada().obterSupervisorioMedicaoDiariaPendentesPorUsuario(filtro, usuario);
	}

	/**
	 * Obter supervisorio medicao horaria pendentes por usuario.
	 *
	 * @param filtro the filtro
	 * @param usuario the usuario
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException {

		return this.getControladorAlcada().obterSupervisorioMedicaoHorariaPendentesPorUsuario(filtro, usuario);

	}


	/**
	 * Obter supervisorio medicao diaria pendentes por usuario.
	 * 
	 * @param usuario the usuario
	 * @param isQtdeMinimaPendente (quantidade limite de 10 registros se for true)
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario
		(Usuario usuario,  boolean isQtdeMinimaPendente) throws NegocioException {

		return this.getControladorAlcada().obterSupervisorioMedicaoDiariaPendentesPorUsuario(usuario, isQtdeMinimaPendente);

	}

	/**
	 * Obter supervisorio medicao horária pendentes por usuario.
	 * 
	 * @param usuario the usuario
	 * @param isQtdeMinimaPendente (quantidade limite de 10 registros se for true)
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario
			(Usuario usuario,  boolean isQtdeMinimaPendente) throws NegocioException {

		return this.getControladorAlcada().obterSupervisorioMedicaoHorariaPendentesPorUsuario(usuario, isQtdeMinimaPendente);
	}

	/**
	 * Obter modulo por operacao sistema menu.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the modulo
	 * @throws NegocioException the negocio exception
	 */
	public Modulo obterModuloPorOperacaoSistemaMenu(Long chavePrimaria) throws NegocioException {

		return this.getControladorModulo().obterModuloPorOperacaoSistemaMenu(chavePrimaria);
	}

	/**
	 * Remover controle acesso func.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void removerControleAcessoFunc(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		this.getControladorUsuario().removerControleAcessoFunc(chavesPrimarias, dadosAuditoria);

	}

	/**
	 * Remover usuario.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws NegocioException the negocio exception
	 */
	public void removerUsuario(Long[] chavesPrimarias) throws NegocioException {

		this.getControladorUsuario().removerUsuario(chavesPrimarias);
	}

	/**
	 * Listar conta contabil.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws Exception the exception
	 */
	public Collection<ContaContabil> listarContaContabil(Map<String, Object> filtro) throws NegocioException {

		return getControladorContaContabil().listarContaContabil(filtro);
	}

	/**
	 * Obter conta contabil.
	 *
	 * @param filtro the filtro
	 * @return the conta contabil
	 * @throws Exception the exception
	 */
	public ContaContabil obterContaContabil(Map<String, Object> filtro) {

		return getControladorContaContabil().obterContaContabil(filtro);
	}

	/**
	 * Gets the controlador conta contabil.
	 *
	 * @return the controlador conta contabil
	 */
	private ControladorContaContabil getControladorContaContabil() {

		return (ControladorContaContabil) serviceLocator.getControladorNegocio(ControladorContaContabil.BEAN_ID_CONTROLADOR_CONTA_CONTABIL);
	}

	/**
	 * Inserir conta contabil.
	 *
	 * @param contaContabil the conta contabil
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirContaContabil(ContaContabil contaContabil) throws GGASException {

		return this.getControladorContaContabil().inserirContaContabil(contaContabil);
	}

	/**
	 * Remover conta contabil.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerContaContabil(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorContaContabil().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Validar dados conta contabil.
	 *
	 * @param contaContabil the conta contabil
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosContaContabil(ContaContabil contaContabil) throws GGASException {

		this.getControladorContaContabil().validarDadosContaContabil(contaContabil);
	}

	/**
	 * Criar conta contabil.
	 *
	 * @return the conta contabil
	 * @throws GGASException the GGAS exception
	 */
	public ContaContabil criarContaContabil() throws GGASException {

		return (ContaContabil) this.getControladorContaContabil().criar();
	}

	/**
	 * Obter conta contatil.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the conta contabil
	 * @throws GGASException the GGAS exception
	 */
	public ContaContabil obterContaContatil(Long chavePrimaria) throws GGASException {

		return this.getControladorContaContabil().obterContaContabil(chavePrimaria);
	}

	/**
	 * Atualizar conta contabil.
	 *
	 * @param contaContabil the conta contabil
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarContaContabil(ContaContabil contaContabil) throws GGASException {

		this.getControladorContaContabil().atualizarContaContabil(contaContabil);
	}

	/**
	 * Listar constantes sistema por tabela.
	 *
	 * @param idTabela the id tabela
	 * @return the list
	 */
	public List<ConstanteSistema> listarConstantesSistemaPorTabela(Long idTabela) {

		return this.getControladorConstanteSistema().listarConstantesSistemaPorTabela(idTabela);
	}

	/**
	 * Obter constante sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the constante sistema
	 * @throws NegocioException the negocio exception
	 */
	public ConstanteSistema obterConstanteSistema(Long chavePrimaria) throws NegocioException {

		return (ConstanteSistema) this.getControladorConstanteSistema().obter(chavePrimaria);
	}

	/**
	 * Checks if is contrato tipo modelo contrato complementar.
	 *
	 * @param enco the enco
	 * @return the boolean
	 * @throws NegocioException the negocio exception
	 */
	public Boolean isContratoTipoModeloContratoComplementar(EntidadeConteudo enco) throws NegocioException {

		return this.getControladorConstanteSistema().isContratoTipoModeloContratoComplementar(enco);
	}

	/**
	 * Atualizar constante sistema.
	 *
	 * @param constanteSistema the constante sistema
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarConstanteSistema(ConstanteSistema constanteSistema) throws ConcorrenciaException, NegocioException {

		this.getControladorConstanteSistema().atualizar(constanteSistema);
	}

	/**
	 * Validar existe principal.
	 *
	 * @param listaTabelaAuxiliar the lista tabela auxiliar
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @throws NegocioException the negocio exception
	 */
	public void validarExistePrincipal(Collection<TabelaAuxiliar> listaTabelaAuxiliar, TabelaAuxiliar tabelaAuxiliar)
					throws NegocioException {

		this.getControladorTabelaAuxiliar().validarExistePrincipal(listaTabelaAuxiliar, tabelaAuxiliar);
	}

	/**
	 * Tributo possui aliquota.
	 *
	 * @param chaveRubrica the chave rubrica
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean tributoPossuiAliquota(Long chaveRubrica) throws NegocioException {

		return this.getControladorTributo().rubricaPossuiTributoAliquota(chaveRubrica);
	}

	/**
	 * Buscar integracao cliente.
	 *
	 * @param cliente the cliente
	 * @return the integracao cliente
	 */
	public IntegracaoCliente buscarIntegracaoCliente(Cliente cliente) {

		return this.getControladorIntegracao().buscarIntegracaoCliente(cliente);
	}

	/**
	 * Obter supervisorio medicao anormalidade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the supervisorio medicao anormalidade
	 * @throws GGASException the GGAS exception
	 */
	public SupervisorioMedicaoAnormalidade obterSupervisorioMedicaoAnormalidade(long chavePrimaria) throws GGASException {

		return this.getControladorSupervisorio().obterSupervisorioMedicaoAnormalidade(chavePrimaria);
	}

	/**
	 * Criar evento comercial.
	 *
	 * @return the evento comercial
	 */
	public EventoComercial criarEventoComercial() {

		return this.getControladorContabil().criarEventoComercial();
	}

	/**
	 * Listar tabelas constantes.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Tabela> listarTabelasConstantes() throws NegocioException {

		return this.getControladorAuditoria().listarTabelasConstantes();
	}

	/**
	 * Consultar todos faixa consumo variacao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<FaixaConsumoVariacao> consultarTodosFaixaConsumoVariacao(Map<String, Object> filtro) {

		ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao = getControladorFaixaConsumoVariacao();

		return controladorFaixaConsumoVariacao.consultarTodosFaixaConsumoVariacao(filtro);

	}

	/**
	 * Inserir faixa consumo variacao.
	 *
	 * @param faixaConsumoVariacao the faixa consumo variacao
	 * @throws NegocioException the negocio exception
	 */
	public void inserirFaixaConsumoVariacao(FaixaConsumoVariacao faixaConsumoVariacao) throws NegocioException {

		ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao = getControladorFaixaConsumoVariacao();

		controladorFaixaConsumoVariacao.inserir(faixaConsumoVariacao);
	}

	/**
	 * Atualizar faixa consumo variacao.
	 *
	 * @param faixaConsumoVariacao the faixa consumo variacao
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarFaixaConsumoVariacao(FaixaConsumoVariacao faixaConsumoVariacao) throws NegocioException, ConcorrenciaException {

		ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao = getControladorFaixaConsumoVariacao();

		controladorFaixaConsumoVariacao.atualizar(faixaConsumoVariacao);
	}

	/**
	 * Remover faixa consumo variacao.
	 *
	 * @param faixaConsumoVariacao the faixa consumo variacao
	 * @throws NegocioException the negocio exception
	 */
	public void removerFaixaConsumoVariacao(FaixaConsumoVariacao faixaConsumoVariacao) throws NegocioException {

		ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao = getControladorFaixaConsumoVariacao();

		controladorFaixaConsumoVariacao.remover(faixaConsumoVariacao);
	}

	/**
	 * Listar constantes sistema sem tabela.
	 *
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<ConstanteSistema> listarConstantesSistemaSemTabela() throws NegocioException {

		return this.getControladorConstanteSistema().listarConstantesSistemaSemTabela();
	}

	/**
	 * Consultar faixas consumo variacao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<FaixaConsumoVariacao> consultarFaixasConsumoVariacao(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFaixaConsumoVariacao().consultarFaixasConsumoVariacao(filtro);
	}

	/**
	 * Consultar faixa pressao fornecimento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<FaixaPressaoFornecimento> consultarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFaixaPressaoFornecimento().consultarFaixaPressaoFornecimento(filtro);
	}

	/**
	 * Obter faixa pressao fornecimento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the faixa pressao fornecimento
	 * @throws NegocioException the negocio exception
	 */
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(long chavePrimaria) throws NegocioException {

		return this.getControladorFaixaPressaoFornecimento().obterFaixaPressaoFornecimento(chavePrimaria);
	}

	/**
	 * Inserir faixa pressao fornecimento.
	 *
	 * @param faixaPressaoFornecimento the faixa pressao fornecimento
	 * @throws NegocioException the negocio exception
	 */
	public void inserirFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException {

		this.getControladorFaixaPressaoFornecimento().inserirFaixaPressaoFornecimento(faixaPressaoFornecimento);
	}

	/**
	 * Validar faixas pressao fornecimento.
	 *
	 * @param listaFaixaPressaoFornecimento the lista faixa pressao fornecimento
	 * @throws NegocioException the negocio exception
	 */
	public void validarFaixasPressaoFornecimento(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento) throws NegocioException {

		this.getControladorFaixaPressaoFornecimento().validarFaixasPressaoFornecimento(listaFaixaPressaoFornecimento);
	}

	/**
	 * Criar unidade.
	 *
	 * @return the unidade
	 */
	public Unidade criarUnidade() {

		return (Unidade) getControladorUnidade().criar();
	}

	/**
	 * Atualizar faixa pressao fornecimento.
	 *
	 * @param faixaPressaoFornecimento the faixa pressao fornecimento
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws GGASException {

		this.getControladorFaixaPressaoFornecimento().atualizarFaixasPressaoFornecimento(faixaPressaoFornecimento);
	}

	/**
	 * Remover faixa pressao fornecimento.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerFaixaPressaoFornecimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorFaixaPressaoFornecimento().removerFaixasPressaoFornecimento(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Verificar faixa pressao existente.
	 *
	 * @param faixaPressaoFornecimento the faixa pressao fornecimento
	 * @throws GGASException the GGAS exception
	 */
	public void verificarFaixaPressaoExistente(FaixaPressaoFornecimento faixaPressaoFornecimento) throws GGASException {

		this.getControladorFaixaPressaoFornecimento().verificarFaixaPressaoExistente(faixaPressaoFornecimento);
	}

	/**
	 * Consultar contrato ponto consumo.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<ContratoPontoConsumo> consultarContratoPontoConsumo(Map<String, Object> filtro) throws NegocioException {

		return (List<ContratoPontoConsumo>) this.getControladorContrato().consultarContratoPontoConsumo(filtro);
	}

	/**
	 * Listar faixas pressao faturamento relacionadas.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<FaixaPressaoFornecimento> listarFaixasPressaoFaturamentoRelacionadas(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFaixaPressaoFornecimento().listarFaixasPressaoFaturamentoRelacionadas(filtro);
	}

	/**
	 * Listar faixa pressao fornecimento.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<FaixaPressaoFornecimentoVO> listarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFaixaPressaoFornecimento().listarFaixaPressaoFornecimento(filtro);
	}

	/**
	 * Obter unidade pressao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the unidade
	 * @throws NegocioException the negocio exception
	 */
	public Unidade obterUnidadePressao(long chavePrimaria) throws NegocioException {

		return (Unidade) this.getControladorUnidade().obter(chavePrimaria);
	}

	/**
	 * Listar rotas por setor comercial da quadra.
	 *
	 * @param idQuadra the id quadra
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Rota> listarRotasPorSetorComercialDaQuadra(Long idQuadra) throws NegocioException {

		return this.getControladorRota().listarRotasPorSetorComercialDaQuadra(idQuadra);
	}

	/**
	 * Validar ponto consumo e codigo legado.
	 *
	 * @param codigoLegado the codigo legado
	 * @param idPontoConsumo the id ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	public void validarPontoConsumoECodigoLegado(String codigoLegado, Long idPontoConsumo) throws NegocioException {

		getControladorPontoConsumo().validarPontoConsumoECodigoLegado(codigoLegado, idPontoConsumo);
	}

	/**
	 * Consultar ponto consumo por codigo legado.
	 *
	 * @param codigoLegado the codigo legado
	 * @return the collection
	 */
	public Collection<PontoConsumo> consultarPontoConsumoPorCodigoLegado(String codigoLegado) {

		return this.getControladorPontoConsumo().consultarPontoConsumoPorCodigoLegado(codigoLegado);
	}

	/**
	 * Obter favorito.
	 *
	 * @param idFavorito the id favorito
	 * @return the favoritos
	 * @throws NegocioException the negocio exception
	 */
	public Favoritos obterFavorito(Long idFavorito) throws NegocioException {

		return (Favoritos) this.getControladorMenu().obter(idFavorito, FavoritosImpl.class);
	}

	/**
	 * Remover favorito.
	 *
	 * @param favorito the favorito
	 * @throws NegocioException the negocio exception
	 */
	public void removerFavorito(Favoritos favorito) throws NegocioException {

		this.getControladorMenu().remover(favorito);
	}

	/**
	 * Adicionar favorito.
	 *
	 * @param favorito the favorito
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String adicionarFavorito(Favoritos favorito) throws GGASException {

		return this.getControladorMenu().inserirFavorito(favorito);
	}

	/**
	 * Método responsável por criar um item de favoritos.
	 *
	 * @return the favoritos
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Favoritos criarFavoritos() throws GGASException {

		return (Favoritos) this.getControladorMenu().criarFavoritos();
	}

	/**
	 * Alterar situacao imoveis factivel.
	 *
	 * @param idQuadraFace the id quadra face
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<Imovel> alterarSituacaoImoveisFactivel(long idQuadraFace) throws NegocioException {

		return this.getControladorQuadra().alterarSituacaoImoveisFactivel(idQuadraFace);

	}

	/**
	 * Alterar situacao imoveis potencial.
	 *
	 * @param idQuadraFace the id quadra face
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Imovel> alterarSituacaoImoveisPotencial(long idQuadraFace) throws NegocioException {

		return this.getControladorQuadra().alterarSituacaoImoveisPontencial(idQuadraFace);

	}

	/**
	 * Obter imoveis por quadra.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<Imovel> obterImoveisPorQuadra(long chavePrimaria) {

		return this.getControladorQuadra().obterImoveisPorQuadraFace(chavePrimaria);
	}

	/**
	 * Alterar situacao imovel.
	 *
	 * @param idImoveis the id imoveis
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Imovel> alterarSituacaoImovel(Long[] idImoveis) throws NegocioException {

		return this.getControladorQuadra().alterarSituacaoImoveis(idImoveis);

	}

	/**
	 * Atualizar imoveis.
	 *
	 * @param listaImoveis the lista imoveis
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarImoveis(Collection<Imovel> listaImoveis) throws ConcorrenciaException, NegocioException {

		this.getControladorImovel().atualizarColecao(listaImoveis, Imovel.class);

	}

	/**
	 * Validar duracao dias.
	 *
	 * @param duracao the duracao
	 * @param dataInicio the data inicio
	 * @param dataLimite the data limite
	 * @throws NegocioException the negocio exception
	 */
	public void validarDuracaoDias(Integer duracao, Date dataInicio, Date dataLimite) throws NegocioException {

		this.getControladorCronogramaFaturamento().validarDuracaoDias(duracao, dataInicio, dataLimite);
	}

	/**
	 * Obter periodicidade por grupo faturamento.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @return the periodicidade
	 * @throws NegocioException the negocio exception
	 */
	public Periodicidade obterPeriodicidadePorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		return this.getControladorRota().obterPeriodicidadePorGrupoFaturamento(idGrupoFaturamento);
	}

	/**
	 * Consultar cronograma atividade faturamento por cronograma faturamento.
	 *
	 * @param cronogramaFaturamento the cronograma faturamento
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(
					CronogramaFaturamento cronogramaFaturamento) throws NegocioException {

		return this.getControladorCronogramaFaturamento().consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(
						cronogramaFaturamento);
	}

	/**
	 * Obter datas sugestao cronograma.
	 *
	 * @param dataPrevistaCicloAnterior the data prevista ciclo anterior
	 * @param periodicidade the periodicidade
	 * @param listaDatasPrevistas the lista datas previstas
	 * @param listaDatasIniciais the lista datas iniciais
	 * @param listaDatasFinais the lista datas finais
	 * @param isVerificarDataPrevista the is verificar data prevista
	 * @param isPrimeiroCronograma the is primeiro cronograma
	 * @param tipoLeitura the tipo leitura
	 * @param ciclo the ciclo
	 * @param rota the rota
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterDatasSugestaoCronograma(DateTime dataPrevistaCicloAnterior, Periodicidade periodicidade,
					StringBuilder listaDatasPrevistas, StringBuilder listaDatasIniciais, StringBuilder listaDatasFinais,
					Boolean isVerificarDataPrevista, Boolean isPrimeiroCronograma, TipoLeitura tipoLeitura, Integer ciclo, Rota rota)
					throws GGASException {

		return this.getControladorCronogramaFaturamento().obterDatasSugestaoCronograma(dataPrevistaCicloAnterior, periodicidade,
						listaDatasPrevistas, listaDatasIniciais, listaDatasFinais, isVerificarDataPrevista, tipoLeitura, ciclo, rota);
	}

	/**
	 * Verificar dia util.
	 *
	 * @param data the data
	 * @param listaDatas the lista datas
	 * @return the date time
	 * @throws NegocioException the negocio exception
	 */
	public DateTime verificarDiaUtil(DateTime data, StringBuilder listaDatas) throws NegocioException {

		return this.getControladorCronogramaFaturamento().verificarDiaUtil(data, listaDatas);
	}

	/**
	 * Validar modalidade faturamento.
	 *
	 * @param selec the selec
	 * @param obrig the obrig
	 * @throws NegocioException the negocio exception
	 */
	public void validarModalidadeFaturamento(Boolean selec, Boolean obrig) throws NegocioException {

		this.getControladorModeloContrato().validarDadosRegraFaturamento(selec, obrig);

	}

	/**
	 * Listar entidade conteudo por descricao entidade classe.
	 *
	 * @param descricaoEntidadeClasse the descricao entidade classe
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> listarEntidadeConteudoPorDescricaoEntidadeClasse(String descricaoEntidadeClasse)
					throws NegocioException {

		return this.getControladorEntidadeConteudo().listarEntidadeConteudoPorDescricaoEntidadeClasse(descricaoEntidadeClasse);
	}

	/**
	 * Listar segmento amostragem pcs.
	 *
	 * @param segmento the segmento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SegmentoAmostragemPCS> listarSegmentoAmostragemPCS(Long segmento) throws NegocioException {

		return this.getControladorSegmento().listarSegmentoAmostragemPCS(segmento);
	}

	/**
	 * Listar segmento intervalo pcs.
	 *
	 * @param segmento the segmento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<SegmentoIntervaloPCS> listarSegmentoIntervaloPCS(Long segmento) throws NegocioException {

		return this.getControladorSegmento().listarSegmentoIntervaloPCS(segmento);
	}

	/**
	 * Listar ramo atividade substituicao tributaria por ramo atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<RamoAtividadeSubstituicaoTributaria> listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(
					RamoAtividade ramoAtividade) throws GGASException {

		return this.getControladorRamoAtividadeSubstituicaoTributaria().listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(
						ramoAtividade);
	}

	/**
	 * Gets the controlador ramo atividade substituicao tributaria.
	 *
	 * @return the controlador ramo atividade substituicao tributaria
	 */
	private ControladorRamoAtividadeSubstituicaoTributaria getControladorRamoAtividadeSubstituicaoTributaria() {

		return (ControladorRamoAtividadeSubstituicaoTributaria) serviceLocator
						.getControladorNegocio(ControladorRamoAtividadeSubstituicaoTributaria.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE_SUBS_TRIB);
	}

	/**
	 * Listar ramo atividade amostragem pcs por ramo atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<RamoAtividadeAmostragemPCS> listarRamoAtividadeAmostragemPCSPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException {

		return this.getControladorRamoAtividade().listarRamoAtividadeAmostragemPCSPorRamoAtividade(ramoAtividade);
	}

	/**
	 * Listar ramo atividade intervalo pcs por ramo atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<RamoAtividadeIntervaloPCS> listarRamoAtividadeIntervaloPCSPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException {

		return this.getControladorRamoAtividade().listarRamoAtividadeIntervaloPCSPorRamoAtividade(ramoAtividade);
	}

	/**
	 * Obter ramo atividade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the ramo atividade
	 * @throws GGASException the GGAS exception
	 */
	public RamoAtividade obterRamoAtividade(Long chavePrimaria) throws GGASException {

		return (RamoAtividade) this.getControladorRamoAtividade().obter(chavePrimaria);
	}

	/**
	 * Listar segmento amostragem pcs disponiveis.
	 *
	 * @param segmento the segmento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> listarSegmentoAmostragemPCSDisponiveis(Long segmento) throws NegocioException {

		return this.getControladorSegmento().listarSegmentoAmostragemPCSDisponiveis(segmento);
	}

	/**
	 * Listar segmento intervalo pcs disponiveis.
	 *
	 * @param segmento the segmento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntervaloPCS> listarSegmentoIntervaloPCSDisponiveis(Long segmento) throws NegocioException {

		return this.getControladorSegmento().listarSegmentoIntervaloPCSDisponiveis(segmento);
	}

	/**
	 * Obter segmento amostragem pcs.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the segmento amostragem pcs
	 * @throws NegocioException the negocio exception
	 */
	public SegmentoAmostragemPCS obterSegmentoAmostragemPCS(Long chavePrimaria) throws NegocioException {

		return this.getControladorSegmento().obterSegmentoAmostragemPCS(chavePrimaria);
	}

	/**
	 * Obter segmento intervalo pcs.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the segmento intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	public SegmentoIntervaloPCS obterSegmentoIntervaloPCS(Long chavePrimaria) throws NegocioException {

		return this.getControladorSegmento().obterSegmentoIntervaloPCS(chavePrimaria);
	}

	/**
	 * Criar segmento amostragem pcs.
	 *
	 * @return the segmento amostragem pcs
	 * @throws NegocioException the negocio exception
	 */
	public SegmentoAmostragemPCS criarSegmentoAmostragemPCS() throws NegocioException {

		return this.getControladorSegmento().criarSegmentoAmostragemPCS();
	}

	/**
	 * Criar segmento intervalo pcs.
	 *
	 * @return the segmento intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	public SegmentoIntervaloPCS criarSegmentoIntervaloPCS() throws NegocioException {

		return this.getControladorSegmento().criarSegmentoIntervaloPCS();
	}

	/**
	 * Obter intervalo pcs para segmento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	public IntervaloPCS obterIntervaloPCSParaSegmento(Long chavePrimaria) throws NegocioException {

		return this.getControladorSegmento().obterIntervaloPCSParaSegmento(chavePrimaria);
	}

	/**
	 * Listar ramo atividade amostragem pcs disponiveis.
	 *
	 * @param idRamoAtividade the id ramo atividade
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> listarRamoAtividadeAmostragemPCSDisponiveis(Long idRamoAtividade) throws NegocioException {

		return this.getControladorRamoAtividade().listarRamoAtividadeAmostragemPCSDisponiveis(idRamoAtividade);
	}

	/**
	 * Listar ramo atividade intervalo pcs disponiveis.
	 *
	 * @param idRamoAtividade the id ramo atividade
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IntervaloPCS> listarRamoAtividadeIntervaloPCSDisponiveis(Long idRamoAtividade) throws NegocioException {

		return this.getControladorRamoAtividade().listarRamoAtividadeIntervaloPCSDisponiveis(idRamoAtividade);
	}

	/**
	 * Criar ramo atividade substituicao tributaria.
	 *
	 * @return the ramo atividade substituicao tributaria
	 * @throws GGASException the GGAS exception
	 */
	public RamoAtividadeSubstituicaoTributaria criarRamoAtividadeSubstituicaoTributaria() throws GGASException {

		return (RamoAtividadeSubstituicaoTributaria) this.getControladorRamoAtividadeSubstituicaoTributaria().criar();
	}

	/**
	 * Criar ramo atividade amostragem pcs.
	 *
	 * @return the ramo atividade amostragem pcs
	 * @throws NegocioException the negocio exception
	 */
	public RamoAtividadeAmostragemPCS criarRamoAtividadeAmostragemPCS() throws NegocioException {

		return this.getControladorRamoAtividade().criarRamoAtividadeAmostragemPCS();
	}

	/**
	 * Criar ramo atividade intervalo pcs.
	 *
	 * @return the ramo atividade intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	public RamoAtividadeIntervaloPCS criarRamoAtividadeIntervaloPCS() throws NegocioException {

		return this.getControladorRamoAtividade().criarRamoAtividadeIntervaloPCS();
	}

	/**
	 * Obter intervalo pcs para ramo atividade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	public IntervaloPCS obterIntervaloPCSParaRamoAtividade(Long chavePrimaria) throws NegocioException {

		return this.getControladorRamoAtividade().obterIntervaloPCSParaRamoAtividade(chavePrimaria);
	}

	/**
	 * Remover ramo atividade.
	 *
	 * @param ramoAtividade the ramo atividade
	 * @throws NegocioException the negocio exception
	 */
	public void removerRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException {

		this.getControladorRamoAtividade().remover(ramoAtividade);
	}

	/**
	 * Remover segmento.
	 *
	 * @param segmento the segmento
	 * @throws NegocioException the negocio exception
	 */
	public void removerSegmento(Segmento segmento) throws NegocioException {

		this.getControladorSegmento().remover(segmento);
	}

	/**
	 * Obter ramo atividade substituicao tributaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the ramo atividade substituicao tributaria
	 * @throws NegocioException the negocio exception
	 */
	public RamoAtividadeSubstituicaoTributaria obterRamoAtividadeSubstituicaoTributaria(long chavePrimaria) throws NegocioException {

		return (RamoAtividadeSubstituicaoTributaria) this.getControladorRamoAtividadeSubstituicaoTributaria().obter(chavePrimaria,
						"ramoAtividadeAmostragemPCS", "ramoAtividadeIntervaloPCS", "ramoAtividadeSubstituicaoTributaria");
	}

	/**
	 * Remover ramo atividade substituicao tributaria.
	 *
	 * @param rast the rast
	 * @throws NegocioException the negocio exception
	 */
	public void removerRamoAtividadeSubstituicaoTributaria(RamoAtividadeSubstituicaoTributaria rast) throws NegocioException {

		this.getControladorRamoAtividadeSubstituicaoTributaria().remover(rast);
	}

	/**
	 * Listar ids pontos consumo por chave rota.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the long[]
	 */
	public Long[] listarIdsPontosConsumoPorChaveRota(Long chavePrimaria) {

		return this.getControladorPontoConsumo().listarIdsPontosConsumoPorChaveRota(chavePrimaria);

	}

	/**
	 * Remanejar ponto consumo.
	 *
	 * @param idPontosConsumoASeremRemanejados the id pontos consumo a serem remanejados
	 * @param idRotaDestino the id rota destino
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public Collection<PontoConsumo> remanejarPontoConsumo(Long[] idPontosConsumoASeremRemanejados, Long idRotaDestino)
			throws GGASException{

		return this.getControladorPontoConsumo().remanejarPontoConsumo(idPontosConsumoASeremRemanejados, idRotaDestino);
	}

	/**
	 * Obter valor constante sistema por codigo.
	 *
	 * @param codigo the codigo
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String obterValorConstanteSistemaPorCodigo(String codigo) throws GGASException {

		return this.getControladorConstanteSistema().obterValorConstanteSistemaPorCodigo(codigo);
	}

	/**
	 * Consultar grupo faturamento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<GrupoFaturamento> consultarGrupoFaturamento(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFatura().consultarGrupoFaturamento(filtro);
	}

	/**
	 * Consultar fatura controle impressao.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<FaturamentoGrupoRotaImpressao> consultarFaturaControleImpressao(Map<String, Object> filtro) {

		return this.getControladorFaturaControleImpressao().consultarFaturaControleImpressao(filtro);
	}

	/**
	 * Exige matricula funcionario.
	 *
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean exigeMatriculaFuncionario() throws NegocioException {

		return this.getControladorFuncionario().exigeMatriculaFuncionario();
	}

	/**
	 * Verificar origatoriedade matricula.
	 *
	 * @param matricula the matricula
	 * @throws NegocioException the negocio exception
	 */
	public void verificarOrigatoriedadeMatricula(String matricula) throws NegocioException {

		this.getControladorFuncionario().verificarOrigatoriedadeMatricula(matricula);
	}

	/**
	 * Verificar quantidade minima maxima periodicidade.
	 *
	 * @param tabelaAuxiliar the tabela auxiliar
	 * @throws NegocioException the negocio exception
	 */
	public void verificarQuantidadeMinimaMaximaPeriodicidade(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		this.getControladorTabelaAuxiliar().verificarQuantidadeMinimaMaximaPeriodicidade(tabelaAuxiliar);
	}

	/**
	 * Preencher mapa com lista referencia leitura.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param mapaComListaReferenciaLeitura the mapa com lista referencia leitura
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> preencherMapaComListaReferenciaLeitura(PontoConsumo pontoConsumo, Date dataRealizacaoLeitura,
					Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura) throws GGASException {

		return this.getControladorSupervisorio().preencherMapaComListaReferenciaLeitura(pontoConsumo, null, dataRealizacaoLeitura,
						mapaComListaReferenciaLeitura);
	}

	/**
	 * Obter ano mes referencia ciclo.
	 *
	 * @param mapaComListaReferenciaLeitura the mapa com lista referencia leitura
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param pontoConsumo the ponto consumo
	 * @param periodicidade the periodicidade
	 * @param calcularAnoMesReferenciaCiclo the calcular ano mes referencia ciclo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterAnoMesReferenciaCiclo(Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura,
					Date dataRealizacaoLeitura, PontoConsumo pontoConsumo, Periodicidade periodicidade,
					Boolean calcularAnoMesReferenciaCiclo) throws GGASException {

		return this.getControladorSupervisorio().obterAnoMesReferenciaCiclo(mapaComListaReferenciaLeitura, dataRealizacaoLeitura,
						pontoConsumo, periodicidade, calcularAnoMesReferenciaCiclo);
	}

	/**
	 * Listar tipo documento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<TipoDocumento> listarTipoDocumento(Map<String, Object> filtro) {

		return this.getControladorTipoDocumento().listarTipoDocumento(filtro);
	}

	/**
	 * Criar tipo documento.
	 *
	 * @return the tipo documento
	 */
	public TipoDocumento criarTipoDocumento() {

		return (TipoDocumento) this.getControladorTipoDocumento().criar();
	}

	/**
	 * Inserir tipo documento.
	 *
	 * @param tipoDocumento the tipo documento
	 * @throws NegocioException the negocio exception
	 */
	public void inserirTipoDocumento(TipoDocumento tipoDocumento) throws NegocioException {

		this.getControladorTipoDocumento().inserir(tipoDocumento);
	}

	/**
	 * Validar existencia tipo documento.
	 *
	 * @param tipoDocumento the tipo documento
	 * @throws NegocioException the negocio exception
	 */
	public void validarExistenciaTipoDocumento(TipoDocumento tipoDocumento) throws NegocioException {

		this.getControladorTipoDocumento().validarExistenciaTipoDocumento(tipoDocumento);
	}

	/**
	 * Atualizar tipo documento.
	 *
	 * @param tipoDocumento the tipo documento
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarTipoDocumento(TipoDocumento tipoDocumento) throws ConcorrenciaException, NegocioException {

		this.getControladorTipoDocumento().atualizar(tipoDocumento);
	}

	/**
	 * Listar documento modelo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<DocumentoModelo> listarDocumentoModelo() throws NegocioException {

		return (Collection<DocumentoModelo>) this.getControladorDocumentoModelo().obterTodas(true);
	}

	/**
	 * Obter documento modelo.
	 *
	 * @param modelo the modelo
	 * @return the documento modelo
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoModelo obterDocumentoModelo(Long modelo) throws NegocioException {

		return (DocumentoModelo) this.getControladorDocumentoModelo().obter(modelo);
	}

	/**
	 * Consultar documento cobranca pela fatura.
	 *
	 * @param chaveFatura the chave fatura
	 * @param tipoDocumento the tipo documento
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<DocumentoCobranca> consultarDocumentoCobrancaPelaFatura(Long chaveFatura, TipoDocumento tipoDocumento)
					throws NegocioException {

		return this.getControladorDocumentoCobranca().consultarDocumentoCobrancaPelaFatura(chaveFatura, tipoDocumento);
	}

	/**
	 * Remover tipo documento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	public void removerTipoDocumento(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException {

		Long[] chavesPrimarias = {chavePrimaria};
		this.getControladorTipoDocumento().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Verificar tipo documento em uso.
	 *
	 * @param chavePrimaria the chave primaria
	 */
	public void verificarTipoDocumentoEmUso(Long chavePrimaria) {

		throw new UnsupportedOperationException();
	}

	/**
	 * Listar entrega documento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntregaDocumento> listarEntregaDocumento(Map<String, Object> filtro) throws GGASException {

		return this.getControladorEntregaDocumento().listarEntregaDocumento(filtro);
	}

	/**
	 * Listar documento cobranca.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<DocumentoCobranca> listarDocumentoCobranca(Map<String, Object> filtro) {

		return this.getControladorCobranca().listarDocumentoCobranca(filtro);
	}

	/**
	 * Criar entrega documento.
	 *
	 * @return the entrega documento
	 */
	public EntregaDocumento criarEntregaDocumento() {

		return (EntregaDocumento) this.getControladorEntregaDocumento().criar();
	}

	/**
	 * Inserir entrega documento.
	 *
	 * @param entregaDocumento the entrega documento
	 * @throws NegocioException the negocio exception
	 */
	public void inserirEntregaDocumento(EntregaDocumento entregaDocumento) throws NegocioException {

		this.getControladorEntregaDocumento().inserir(entregaDocumento);
	}

	/**
	 * Obter documento cobranca.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the documento cobranca
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoCobranca obterDocumentoCobranca(Long chavePrimaria) throws NegocioException {

		return this.getControladorCobranca().obterDocumentoCobranca(chavePrimaria);
	}

	/**
	 * Obter entrega documento.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the entrega documento
	 * @throws NegocioException the negocio exception
	 */
	public EntregaDocumento obterEntregaDocumento(Long chavePrimaria) throws NegocioException {

		return (EntregaDocumento) this.getControladorEntregaDocumento().obter(chavePrimaria, "cliente", "cliente.enderecos",
						"situacaoEntrega", "tipoDocumento", "motivoNaoEntrega", "documentoCobranca");
	}

	/**
	 * Atualizar entrega documento.
	 *
	 * @param entregaDocumento the entrega documento
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarEntregaDocumento(EntregaDocumento entregaDocumento) throws ConcorrenciaException, NegocioException {

		this.getControladorEntregaDocumento().atualizar(entregaDocumento);
	}

	/**
	 * Remover entrega documento.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	public void removerEntregaDocumento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.getControladorEntregaDocumento().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Gets the controlador banco.
	 *
	 * @return the controlador banco
	 */
	private ControladorBanco getControladorBanco() {

		return (ControladorBanco) serviceLocator.getControladorNegocio(ControladorBanco.BEAN_ID_CONTROLADOR_BANCO);
	}

	/**
	 * Consultar banco.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Banco> consultarBanco(Map<String, Object> filtro) throws GGASException {

		return getControladorBanco().consultarBanco(filtro);
	}

	/**
	 * Listar banco.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Banco> listarBanco() throws GGASException {

		return getControladorBanco().listarBanco();
	}

	/**
	 * Criar banco.
	 *
	 * @return the banco
	 * @throws GGASException the GGAS exception
	 */
	public Banco criarBanco() throws GGASException {

		return (Banco) this.getControladorBanco().criar();
	}

	/**
	 * Inserir banco.
	 *
	 * @param banco the banco
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirBanco(Banco banco) throws GGASException {

		return this.getControladorBanco().inserir(banco);
	}


	/**
	 * Inserir banco.
	 *
	 * @param banco the banco
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirBancoInativo(Banco banco) throws GGASException {

		return this.getControladorBanco().inserirInativo(banco);
	}

	/**
	 * Remover banco.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerBanco(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorBanco().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Atualizar banco.
	 *
	 * @param banco the banco
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarBanco(Banco banco) throws GGASException {

		this.getControladorBanco().atualizar(banco);
	}

	/**
	 * Validar banco existe.
	 *
	 * @param filtro the filtro
	 * @throws NegocioException the negocio exception
	 */
	public void validarBancoExiste(Map<String, Object> filtro) throws NegocioException {

		this.getControladorBanco().validarExistente(filtro);
	}

	/**
	 * Gets the controlador arrecadador.
	 *
	 * @return the controlador arrecadador
	 */
	private ControladorArrecadador getControladorArrecadador() {

		return (ControladorArrecadador) serviceLocator.getControladorNegocio(ControladorArrecadador.BEAN_ID_CONTROLADOR_ARRECADADOR);
	}

	/**
	 * Consultar arrecadador.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Arrecadador> consultarArrecadador(Map<String, Object> filtro) throws GGASException {

		return getControladorArrecadador().consultarArrecadador(filtro);
	}

	/**
	 * Listar arrecadador.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Arrecadador> listarArrecadador() throws GGASException {

		return getControladorArrecadador().listarArrecadador();
	}

	/**
	 * Criar arrecadador.
	 *
	 * @return the arrecadador
	 * @throws GGASException the GGAS exception
	 */
	public Arrecadador criarArrecadador() throws GGASException {

		return (Arrecadador) this.getControladorArrecadador().criar();
	}

	/**
	 * Inserir arrecadador.
	 *
	 * @param arrecadador the arrecadador
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirArrecadador(Arrecadador arrecadador) throws GGASException {

		return this.getControladorArrecadador().inserir(arrecadador);
	}

	/**
	 * Obter agente arrecadador.
	 *
	 * @param idArrecadador the id arrecadador
	 * @return the arrecadador
	 * @throws GGASException the GGAS exception
	 */
	public Arrecadador obterAgenteArrecadador(Long idArrecadador) throws GGASException {

		return (Arrecadador) this.getControladorArrecadador().obter(idArrecadador);
	}

	/**
	 * Remover arrecadador.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerArrecadador(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorArrecadador().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Atualizar arrecadador.
	 *
	 * @param arrecadador the arrecadador
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarArrecadador(Arrecadador arrecadador) throws GGASException {

		this.getControladorArrecadador().atualizar(arrecadador);
	}

	/**
	 * Validar arrecadador existe.
	 *
	 * @param filtro the filtro
	 * @throws NegocioException the negocio exception
	 */
	public void validarArrecadadorExiste(Map<String, Object> filtro) throws NegocioException {

		this.getControladorArrecadador().validarExistente(filtro);
	}

	/**
	 * Validar camapos imovel em lote.
	 *
	 * @param apartamento the apartamento
	 * @param bloco the bloco
	 * @param quantidadeBanheiro the quantidade banheiro
	 * @throws NegocioException the negocio exception
	 */
	public void validarCamaposImovelEmLote(String[] apartamento, String[] bloco, String[] quantidadeBanheiro) throws NegocioException {

		this.getControladorImovel().validarCamaposImovelEmLote(apartamento, bloco, quantidadeBanheiro);
	}

	/**
	 * Gets the controlador arrecadador convenio.
	 *
	 * @return the controlador arrecadador convenio
	 */
	private ControladorArrecadadorConvenio getControladorArrecadadorConvenio() {

		return (ControladorArrecadadorConvenio) serviceLocator
						.getControladorNegocio(ControladorArrecadadorConvenio.BEAN_ID_CONTROLADOR_ARRECADADOR_CONVENIO);
	}

	/**
	 * Listar conta bancaria.
	 *
	 * @return the collection
	 */
	public Collection<ContaBancaria> listarContaBancaria() {

		return this.getControladorArrecadadorConvenio().listarContaBancaria();
	}

	/**
	 * Listar arrecadador contrato.
	 *
	 * @return the collection
	 */
	public Collection<ArrecadadorContrato> listarArrecadadorContrato() {

		return this.getControladorArrecadadorConvenio().listarArrecadadorContrato();
	}

	/**
	 * Listar carteira cobranca.
	 *
	 * @return the collection
	 */
	public Collection<ArrecadadorCarteiraCobranca> listarCarteiraCobranca() {

		return this.getControladorArrecadadorConvenio().listarCarteiraCobranca();
	}

	/**
	 * Consultar arrecadador convenio.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ArrecadadorContratoConvenio> consultarArrecadadorConvenio(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);
	}

	/**
	 * Criar arrecadador contrato convenio.
	 *
	 * @return the arrecadador contrato convenio
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorContratoConvenio criarArrecadadorContratoConvenio() throws GGASException {

		return (ArrecadadorContratoConvenio) this.getControladorArrecadadorConvenio().criar();
	}

	/**
	 * Obter arrecadador contrato convenio.
	 *
	 * @param idArrecadadorContratoConvenio the id arrecadador contrato convenio
	 * @return the arrecadador contrato convenio
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenio(Long idArrecadadorContratoConvenio) throws GGASException {

		return this.getControladorArrecadadorConvenio().obterArrecadadorConvenio(idArrecadadorContratoConvenio);
	}

	/**
	 * Obter arrecadador contrato.
	 *
	 * @param idArrecadadorContrato the id arrecadador contrato
	 * @return the arrecadador contrato
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorContrato obterArrecadadorContrato(Long idArrecadadorContrato) throws GGASException {

		return this.getControladorArrecadadorConvenio().obterArrecadadorContrato(idArrecadadorContrato);
	}

	/**
	 * Obter arrecadador carteira cobranca.
	 *
	 * @param idArrecadadorCarteiraCobranca the id arrecadador carteira cobranca
	 * @return the arrecadador carteira cobranca
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorCarteiraCobranca obterArrecadadorCarteiraCobranca(Long idArrecadadorCarteiraCobranca) throws GGASException {

		return this.getControladorArrecadadorConvenio().obterArrecadadorCarteiraCobranca(idArrecadadorCarteiraCobranca);
	}

	/**
	 * Inserir arrecadador contrato convenio.
	 *
	 * @param arrecadadorContratoConvenio the arrecadador contrato convenio
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws GGASException {

		return this.getControladorArrecadadorConvenio().inserirArrecadadorContratoConvenio(arrecadadorContratoConvenio);
	}

	/**
	 * Remover arrecadador contrato convenio.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerArrecadadorContratoConvenio(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorArrecadadorConvenio().remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Alterar arrecadador contrato convenio.
	 *
	 * @param arrecadadorContratoConvenio the arrecadador contrato convenio
	 * @throws GGASException 
	 */
	public void alterarArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws GGASException {

		this.getControladorArrecadadorConvenio().alterarArrecadadorContratoConvenio(arrecadadorContratoConvenio);
	}

	/**
	 * Tornar padrao arrecadador contrato convenio.
	 *
	 * @param chavePrimaria the chave primaria
	 * @throws GGASException 
	 */
	public void tornarPadraoArrecadadorContratoConvenio(Long chavePrimaria) throws GGASException {

		this.getControladorArrecadadorConvenio().tornarPadraoArrecadadorContratoConvenio(chavePrimaria);
	}

	/**
	 * Validar rota.
	 *
	 * @param rota the rota
	 * @throws NegocioException the negocio exception
	 */
	public void validarRota(Rota rota) throws NegocioException {

		this.getControladorRota().validarDadosEntidade(rota);
	}

	/**
	 * Consultar relatorio volumes faturados segmento.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioVolumesFaturadosSegmento(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioVolumesFaturadosSegmento().consultar(filtro);
	}

	/**
	 * Consultar relatorio titulos atraso.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioTitulosAtraso(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioTitulosAtraso().consultar(filtro);
	}

	/**
	 * Consultar relatorio prazo minimo antecedencia aviso corte.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioPrazoMinimoAntecedenciaAvisoCorte(Map<String, Object> filtro) throws GGASException {

		return getControladorRelatorioPrazoMinimoAntecedencia().gerarRelatorioAvisoCorte(filtro);
	}

	/**
	 * Consultar relatorio prazo minimo antecedencia notificacao corte.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] consultarRelatorioPrazoMinimoAntecedenciaNotificacaoCorte(Map<String, Object> filtro) throws GGASException {

		return getControladorRelatorioPrazoMinimoAntecedencia().gerarRelatorioNotificacaoCorte(filtro);
	}

	/**
	 * Gerar relatorio titulos aberto por data vencimento.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioTitulosAbertoPorDataVencimento(Map<String, Object> filtro, FormatoImpressao formatoImpressao)
					throws GGASException {

		return this.getControladorRelatorioTitulosAbertoPorDataVencimento().gerarRelatorioTituloAbertoPorDataVencimento(filtro,
						formatoImpressao);
	}

	/**
	 * Calcular valor notas debitos.
	 *
	 * @param chavesFatura the chaves fatura
	 * @param chavesCreditoDebito the chaves credito debito
	 * @param data the data
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal calcularValorNotasDebitos(Long[] chavesFatura, Long[] chavesCreditoDebito, Date data) throws GGASException {

		return this.getControladorCobranca().calcularValorTotalDebitos(chavesFatura, chavesCreditoDebito, data);

	}

	/**
	 * Gerar posicao contas receber.
	 *
	 * @param filtro the filtro
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarPosicaoContasReceber(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		return this.getControladorRelatorioPosicaoContasReceber().gerarPosicaoContasReceber(filtro, formatoImpressao);
	}

	/**
	 * Obter operacao emitir fatura.
	 *
	 * @return the operacao
	 */
	public Operacao obterOperacaoEmitirFatura() {

		return this.getControladorFaturaControleImpressao().obterOperacaoEmitirFatura();
	}

	/**
	 * Obter faturamento grupo rota impressao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the faturamento grupo rota impressao
	 * @throws NegocioException the negocio exception
	 */
	public FaturamentoGrupoRotaImpressao obterFaturamentoGrupoRotaImpressao(Long chavePrimaria) throws NegocioException {

		return (FaturamentoGrupoRotaImpressao) this.getControladorFaturaControleImpressao().obter(chavePrimaria,
						FaturamentoGrupoRotaImpressaoImpl.class, "grupoFaturamento", "rota");
	}

	/**
	 * Gets the controlador relatorio historico cliente.
	 *
	 * @return the controlador relatorio historico cliente
	 */
	private ControladorRelatorioHistoricoCliente getControladorRelatorioHistoricoCliente() {

		return (ControladorRelatorioHistoricoCliente) serviceLocator
						.getControladorNegocio(ControladorRelatorioHistoricoCliente.BEAN_ID_CONTROLADOR_RELATORIO_HISTORICO_CLIENTE);
	}

	/**
	 * Gerar relatorio historico cliente.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioHistoricoCliente(Map<String, Object> filtro) throws GGASException {

		return this.getControladorRelatorioHistoricoCliente().gerarRelatorio(filtro);
	}

	/**
	 * Consultar faturas vencidas ponto consumo.
	 *
	 * @param filtro the filtro
	 * @param acaoComando the acao comando
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Fatura> consultarFaturasVencidasPontoConsumo(Map<String, Object> filtro, AcaoComando acaoComando) 
			throws NegocioException {
		return this.getControladorFatura().consultarFaturasVencidasPontoConsumo(filtro, acaoComando);
	}

	/**
	 * Obter operacao sistema.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the operacao
	 */
	public Operacao obterOperacaoSistema(Long chavePrimaria) {

		return this.getControladorAcesso().obterOperacaoSistema(chavePrimaria);
	}

	/**
	 * Atualizar papel.
	 *
	 * @param papel the papel
	 * @param usuario the usuario
	 * @param listaNova the lista nova
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarPapel(Papel papel, Usuario usuario, Set<Permissao> listaNova) throws GGASException {

		this.getControladorPapel().atualizarPapel(papel, usuario, listaNova);
	}

	/**
	 * Consultar pontos consumo consistir leitura pronto processar.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoConsistirLeituraProntoProcessar(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoConsistirLeituraProntoProcessar(filtro);
	}

	/**
	 * Consultar pontos consumo consistir leitura processado sem anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(filtro);
	}

	/**
	 * Consultar pontos consumo consistir leitura processado anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<PontoConsumo> consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(filtro);
	}

	/**
	 * Consultar pontos consumo registrar leitura processado sem anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(filtro);
	}

	/**
	 * Consultar pontos consumo registrar leitura processado anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(filtro);
	}

	/**
	 * Consultar pontos consumo registrar leitura pronto processar.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessar(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoRegistrarLeituraProntoProcessar(filtro);
	}

	/**
	 * Consultar pontos consumo registrar leitura pronto processar coletor.
	 *
	 * @param filtro the filtro
	 * @return Coleção de ponto de consumo.
	 */
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(filtro);
	}

	/**
	 * Consultar pontos consumo faturar grupo processado sem anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(filtro);
	}

	/**
	 * Consultar pontos consumo faturar grupo processado anormalidade.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(Map<String, Object> filtro) {

		return this.getControladorPontoConsumo().consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(filtro);
	}

	/**
	 * Criar aviso corte emissao.
	 *
	 * @return the aviso corte emissao
	 */
	public AvisoCorteEmissao criarAvisoCorteEmissao() {

		return (AvisoCorteEmissao) this.getControladorAvisoCorteEmissao().criar();
	}

	/**
	 * Inserir aviso corte emissao.
	 *
	 * @param ace the ace
	 * @throws NegocioException the negocio exception
	 */
	public void inserirAvisoCorteEmissao(AvisoCorteEmissao ace) throws NegocioException {

		this.getControladorAvisoCorteEmissao().inserir(ace);
	}

	/**
	 * Consultar aviso corte emissao.
	 *
	 * @param filtroAvisoCorteEmissao the filtro aviso corte emissao
	 * @return the collection
	 */
	public Collection<AvisoCorteEmissao> consultarAvisoCorteEmissao(Map<String, Object> filtroAvisoCorteEmissao) {

		return this.getControladorAvisoCorteEmissao().consultar(filtroAvisoCorteEmissao);
	}

	/**
	 * Remover aviso corte emissao.
	 *
	 * @param ace the ace
	 * @throws NegocioException the negocio exception
	 */
	public void removerAvisoCorteEmissao(AvisoCorteEmissao ace) throws NegocioException {

		this.getControladorAvisoCorteEmissao().remover(ace);
	}

	/**
	 * Consultar faturas vencidas ponto consumo notificacao corte.
	 *
	 * @param filtro the filtro
	 * @param acaoComando the acao comando
	 * @param isNotificacao the is notificacao
	 * @param isBatchAvisoCorte the is batch aviso corte
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Fatura> consultarFaturasValidasNotificacaoCorte(Map<String, Object> filtro, AcaoComando acaoComando) 
			throws NegocioException {

		return this.getControladorAvisoCorteEmissao().consultarFaturasValidasNotificacaoCorte(filtro, acaoComando);
	}

	/**
	 * Listar aviso corte.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<AvisoCorte> listarAvisoCorte(Map<String, Object> filtro) {

		return this.getControladorAvisoCorte().consultarAvisoCorteAcompanhamentoCobranca(filtro);
	}

	/**
	 * Obter aviso corte.
	 *
	 * @param documento the documento
	 * @return the aviso corte
	 * @throws NegocioException the negocio exception
	 */
	public AvisoCorte obterAvisoCorte(Long documento) throws NegocioException {

		return (AvisoCorte) this.getControladorAvisoCorte().obter(documento);
	}

	/**
	 * Obter tipo cliente por id tipo cliente.
	 *
	 * @param idTipoCliente the id tipo cliente
	 * @return the tipo cliente
	 */
	public TipoCliente obterTipoClientePorIdTipoCliente(Long idTipoCliente) {

		return this.getControladorCliente().obterTipoClientePorIdTipoCliente(idTipoCliente);

	}

	/**
	 * Inserir ponco consumo vencimento.
	 *
	 * @param dadosPontoConsumoVencimento the dados ponto consumo vencimento
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	public void inserirPoncoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException {

		this.getControladorPontoConsumo().inserirPoncoConsumoVencimento(dadosPontoConsumoVencimento, idContrato);
	}

	/**
	 * Listar ponto consumo vencimento.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumoVencimento> listarPontoConsumoVencimento(long idPontoConsumo) throws NegocioException {

		return this.getControladorPontoConsumo().listarPontoConsumoVencimento(idPontoConsumo);
	}

	/**
	 * Remover ponto consumo vencimento.
	 *
	 * @param dadosPontoConsumoVencimento the dados ponto consumo vencimento
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	public void removerPontoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException {

		this.getControladorPontoConsumo().removerPontoConsumoVencimento(dadosPontoConsumoVencimento, idContrato);
	}

	/**
	 * Validar dados cliente anexo.
	 *
	 * @param clienteAnexo the cliente anexo
	 * @throws GGASException the GGAS exception
	 */
	public void validarDadosClienteAnexo(ClienteAnexo clienteAnexo) throws GGASException {

		this.getControladorCliente().validarDadosClienteAnexo(clienteAnexo);
	}

	/**
	 * Obter segmento perfil parcelamento.
	 *
	 * @param idPerfilParcelamento the id perfil parcelamento
	 * @param idSegmento the id segmento
	 * @return the parcelamento perfil segmento
	 */
	public ParcelamentoPerfilSegmento obterSegmentoPerfilParcelamento(Long idPerfilParcelamento, Long idSegmento) {

		return getControladorPerfilParcelamento().obterSegmentoPerfilParcelamento(idPerfilParcelamento, idSegmento);
	}

	/**
	 * Obter situacao consumo perfil parcelamento.
	 *
	 * @param idPerfilParcelamento the id perfil parcelamento
	 * @param idSituacaoConsumo the id situacao consumo
	 * @return the parcelamento perfil situacao ponto consumo
	 */
	public ParcelamentoPerfilSituacaoPontoConsumo obterSituacaoConsumoPerfilParcelamento(Long idPerfilParcelamento, Long idSituacaoConsumo) {

		return getControladorPerfilParcelamento().obterSituacaoConsumoPerfilParcelamento(idPerfilParcelamento, idSituacaoConsumo);
	}

	/**
	 * Listar pontos consumo relatorio posicao contas receber.
	 *
	 * @param filtro the filtro
	 * @return the list
	 */
	public List<PontoConsumo> listarPontosConsumoRelatorioPosicaoContasReceber(Map<String, Object> filtro) {

		return this.getControladorRelatorioPosicaoContasReceber().listarPontosConsumoRelatorioPosicaoContasReceber(filtro);
	}

	/**
	 * Remover permissao sistema.
	 *
	 * @param permissao the permissao
	 */
	public void removerPermissaoSistema(Permissao permissao) {

		getControladorPapel().removerPermissaoSistema(permissao);
	}

	/**
	 * Atualizar dias vencimento.
	 *
	 * @param listaFatura the lista fatura
	 * @param diasAtraso the dias atraso
	 * @return the collection
	 */
	public Collection<Fatura> atualizarDiasVencimento(Collection<Fatura> listaFatura, Integer diasAtraso) {

		return getControladorFatura().atualizarDiasAtraso(listaFatura, diasAtraso);

	}

	/**
	 * Emitir2 via notificacao corte.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 */
	public byte[] emitir2ViaNotificacaoCorte(Long chavePrimaria) throws IOException, GGASException {

		return this.getControladorAvisoCorte().emitir2ViaNotificacaoCorte(chavePrimaria);
	}

	/**
	 * Emitir2 via aviso corte.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 */
	public byte[] emitir2ViaAvisoCorte(Long chavePrimaria) throws IOException, GGASException {

		return this.getControladorAvisoCorte().emitir2ViaAvisoCorte(chavePrimaria);
	}

	/**
	 * Listar fatura por fatura agrupada.
	 *
	 * @param chaveFaturaAgrupada the chave fatura agrupada
	 * @return the collection
	 */
	public Collection<Fatura> listarFaturaPorFaturaAgrupada(Long chaveFaturaAgrupada) {

		return getControladorFatura().listarFaturaPorFaturaAgrupada(chaveFaturaAgrupada);
	}

	/**
	 * Buscar parcelamento por fatura.
	 *
	 * @param idFatura the id fatura
	 * @return the parcelamento
	 * @throws NegocioException the negocio exception
	 */
	public Parcelamento buscarParcelamentoPorFatura(Long idFatura) throws NegocioException {

		return getControladorParcelamento().buscarParcelamentoPorFaturaGeral(idFatura);
	}

	/**
	 * Consultar parcelamentos nota debito.
	 *
	 * @param idParcelamento the id parcelamento
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ParcelaVO> consultarParcelamentosNotaDebito(Long idParcelamento) throws GGASException {

		return getControladorParcelamento().consultarParcelamentosNotaDebito(idParcelamento);
	}

	/**
	 * Consultar parcelamentos credito a realizar.
	 *
	 * @param idParcelamento the id parcelamento
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ParcelaVO> consultarParcelamentosCreditoARealizar(Long idParcelamento) throws GGASException {

		return getControladorParcelamento().consultarParcelamentosCreditoARealizar(idParcelamento);
	}

	/**
	 * Listar fatura item por chave fatura.
	 *
	 * @param idFatura the id fatura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<FaturaItem> listarFaturaItemPorChaveFatura(Long idFatura) throws NegocioException {

		return getControladorFatura().listarFaturaItemPorChaveFatura(idFatura);
	}

	/**
	 * Carregar credito debito detalhamento por fatura.
	 *
	 * @param idFatura the id fatura
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<CreditoDebitoDetalhamentoVO> carregarCreditoDebitoDetalhamentoPorFatura(Long idFatura) throws NegocioException {

		return getControladorFatura().carregarCreditoDebitoDetalhamentoPorFatura(idFatura);
	}

	/**
	 * Validar fatura avulso.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	public void validarFaturaAvulso(PontoConsumo pontoConsumo) throws NegocioException {

		getControladorPontoConsumo().validarFaturaAvulso(pontoConsumo);

	}

	/**
	 * Criar movimentacao transferencia corretor vazao.
	 *
	 * @param vazaoCorretor the vazao corretor
	 * @param localArmazenagemOrigem the local armazenagem origem
	 * @param localArmazenagemDestino the local armazenagem destino
	 * @return the vazao corretor historico movimentacao
	 * @throws NegocioException the negocio exception
	 */
	public VazaoCorretorHistoricoMovimentacao criarMovimentacaoTransferenciaCorretorVazao(VazaoCorretor vazaoCorretor,
					MedidorLocalArmazenagem localArmazenagemOrigem, MedidorLocalArmazenagem localArmazenagemDestino)
					throws NegocioException {

		return getControladorVazaoCorretor().criarMovimentacaoTransferenciaCorretorVazao(vazaoCorretor, localArmazenagemOrigem,
						localArmazenagemDestino);
	}

	/**
	 * Inativar historico medicao.
	 *
	 * @param idHistoricoMedicao the id historico medicao
	 * @return the historico medicao
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public HistoricoMedicao inativarHistoricoMedicao(Long idHistoricoMedicao) throws NegocioException, ConcorrenciaException {

		return getControladorHistoricoMedicao().inativarHistoricoMedicao(idHistoricoMedicao);
	}

	/**
	 * Popular filtro credito debitos.
	 *
	 * @param filtro the filtro
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<String, Object> popularFiltroCreditoDebitos(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorFatura().popularFiltroCreditoDebitos(filtro);

	}

	/**
	 * Validar fatura avulsa transportadora.
	 *
	 * @param idTransportadora the id transportadora
	 * @param idVeiculo the id veiculo
	 * @param idMotorista the id motorista
	 * @throws NegocioException the negocio exception
	 */
	public void validarFaturaAvulsaTransportadora(Long idTransportadora, Long idVeiculo, Long idMotorista) throws NegocioException {

		this.getControladorFatura().validarFaturaAvulsaTransportadora(idTransportadora, idVeiculo, idMotorista);

	}

	/**
	 * Gets the controlador grupo economico.
	 *
	 * @return the controlador grupo economico
	 */
	private ControladorGrupoEconomico getControladorGrupoEconomico() {

		return (ControladorGrupoEconomico) serviceLocator
						.getControladorNegocio(ControladorGrupoEconomico.BEAN_ID_CONTROLADOR_GRUPO_ECONOMICO);
	}

	/**
	 * Consultar grupo economico.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<GrupoEconomico> consultarGrupoEconomico(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorGrupoEconomico().consultarGrupoEconomico(filtro);
	}

	/**
	 * Criar grupo economico.
	 *
	 * @return the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	public GrupoEconomico criarGrupoEconomico() throws GGASException {

		return this.getControladorGrupoEconomico().criar();
	}

	/**
	 * Criar contrato adequacao parceria.
	 *
	 * @return the contrato adequacao parceria
	 * @throws GGASException the GGAS exception
	 */
	public ContratoAdequacaoParceria criarContratoAdequacaoParceria() throws GGASException {

		return (ContratoAdequacaoParceria) this.getControladorContratoAdequacaoParceria().criar();
	}

	/**
	 * Obter grupo economico.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	public GrupoEconomico obterGrupoEconomico(Long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (GrupoEconomico) this.getControladorGrupoEconomico().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Incluir grupo economico.
	 *
	 * @param grupoEconomico the grupo economico
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long incluirGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		return this.getControladorGrupoEconomico().incluirGrupoEconomico(grupoEconomico);
	}

	/**
	 * Vincular cliente grupo economico.
	 *
	 * @param listaIdCliente the lista id cliente
	 * @param grupoEconomico the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	public void vincularClienteGrupoEconomico(Long[] listaIdCliente, GrupoEconomico grupoEconomico) throws GGASException {

		this.getControladorGrupoEconomico().vincularClienteGrupoEconomico(listaIdCliente, grupoEconomico);
	}

	/**
	 * Listar cliente por grupo economico.
	 *
	 * @param idGrupoEconomico the id grupo economico
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Cliente> listarClientePorGrupoEconomico(Long idGrupoEconomico) throws NegocioException {

		return this.getControladorCliente().listarClientePorGrupoEconomico(idGrupoEconomico);
	}

	/**
	 * Listar cliente sem grupo economico.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Cliente> listarClienteSemGrupoEconomico(Map<String, Object> filtro) throws NegocioException {

		return this.getControladorCliente().listarClienteSemGrupoEconomico(filtro);
	}

	/**
	 * Consultar cliente.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Cliente> consultarCliente(Long[] chavesPrimarias) throws NegocioException {

		return this.getControladorCliente().consultarCliente(chavesPrimarias);
	}

	/**
	 * Atualizar grupo economico.
	 *
	 * @param grupoEconomico the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	public void atualizarGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		this.getControladorGrupoEconomico().atualizarGrupoEconomico(grupoEconomico);
	}

	/**
	 * Remover grupo economico.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void removerGrupoEconomico(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorGrupoEconomico().removerGrupoEconomico(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Listar grupo economico.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<GrupoEconomico> listarGrupoEconomico() throws NegocioException {

		return this.getControladorGrupoEconomico().listarGrupoEconomico();
	}

	/**
	 * Listar cliente.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Cliente> listarCliente(Long[] chavesPrimarias) throws NegocioException {

		return this.getControladorCliente().listarCliente(chavesPrimarias);
	}

	/**
	 * Validar numero empenho.
	 *
	 * @param idCliente the id cliente
	 * @param numeroEmpenho the numero empenho
	 * @throws NegocioException the negocio exception
	 */
	public void validarNumeroEmpenho(Long idCliente, String numeroEmpenho) throws NegocioException {

		getControladorCliente().validarNumeroEmpenho(idCliente, numeroEmpenho);

	}

	/**
	 * Validar juros mora contrato.
	 *
	 * @param contrato the contrato
	 * @throws GGASException the GGAS exception
	 */
	public void validarJurosMoraContrato(Contrato contrato) throws GGASException {

		this.getControladorContrato().validarJurosMoraContrato(contrato);
	}

	/**
	 * Consultar historico consumo por historico consumo sintetizador.
	 *
	 * @param historicoConsumoSintetizador the historico consumo sintetizador
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumoPorHistoricoConsumoSintetizador(
					HistoricoConsumo historicoConsumoSintetizador) throws NegocioException {

		return getControladorHistoricoConsumo().consultarHistoricoConsumoPorHistoricoConsumoSintetizador(historicoConsumoSintetizador);
	}

	/**
	 * Obter lista arrecadador convenio.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenio() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("habilitado", "true");
		return getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);
	}
	
	/**
	 * Obter lista arrecadador convenio para boleto ou arrecadação.
	 *
	 * @return a coleção
	 * @throws NegocioException em caso de erro
	 */
	public Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioBoletoEArrecadacao() throws NegocioException {
		Collection<ArrecadadorContratoConvenio> convenios = obterListaArrecadadorConvenio();
		convenios.removeAll(obterListaArrecadadorConvenioDebitoAutomatico());
		return convenios;
	}
	
	/**
	 * Obter lista arrecadador convenio para débito automático.
	 *
	 * @return a coleção
	 * @throws NegocioException em caso de erro
	 */
	public Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioDebitoAutomatico() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("habilitado", "true");
		filtro.put("tipoConvenio", Long.valueOf(getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(
						Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO)));
		return getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);
	}

	/**
	 * Listar imovel por chave rota.
	 *
	 * @param chavePrimariaRota the chave primaria rota
	 * @return the collection
	 */
	public Collection<Imovel> listarImovelPorChaveRota(long chavePrimariaRota) {

		return this.getControladorImovel().listarImovelPorChaveRota(chavePrimariaRota);
	}

	/**
	 * Listar ponto consumo sem rota.
	 *
	 * @param filtro the filtro
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return the collection
	 */
	public Collection<PontoConsumo> listarPontoConsumoSemRota(Map<String, Object> filtro, List<Long> chavesPontoConsumo) {

		return this.getControladorImovel().listarPontoConsumoSemRota(filtro, chavesPontoConsumo);
	}

	/**
	 * Atualizar associacao ponto consumo rota.
	 *
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param listaPontoConsumoRemoverAssociacao the lista ponto consumo remover associacao
	 * @param chaveRota the chave rota
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarAssociacaoPontoConsumoRota(List<PontoConsumo> listaPontoConsumo,
					List<PontoConsumo> listaPontoConsumoRemoverAssociacao, Long chaveRota) throws ConcorrenciaException, NegocioException {

		this.getControladorRota().atualizarAssociacaoPontoConsumoRota(listaPontoConsumo, listaPontoConsumoRemoverAssociacao, chaveRota);
	}

	/**
	 * Alterar situacao imovel por indicador rede.
	 *
	 * @param listaImovel the lista imovel
	 * @param novaQuadraFace the nova quadra face
	 * @throws NegocioException the negocio exception
	 */
	public void alterarSituacaoImovelPorIndicadorRede(Collection<Imovel> listaImovel, QuadraFace novaQuadraFace) throws NegocioException {

		this.getControladorImovel().alterarSituacaoImovelPorIndicadorRede(listaImovel, novaQuadraFace);
	}

	/**
	 * Listar funcionarios.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Funcionario> listarFuncionarios() throws NegocioException {

		return this.getControladorFuncionario().listarFuncionarioPorUnidadeOrganizacional(null);
	}

	/**
	 * Listar ponto consumo por numero contrato.
	 *
	 * @param numeroContrato the numero contrato
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumo> listarPontoConsumoPorNumeroContrato(String numeroContrato) throws GGASException {

		return this.getControladorPontoConsumo().listaPontoConsumoPorNumeroContrato(numeroContrato);
	}

	/**
	 * Verificar email princiap obrigatorio.
	 *
	 * @param emailPrincipal the email principal
	 * @throws NegocioException the negocio exception
	 */
	public void verificarEmailPrinciapObrigatorio(String emailPrincipal) throws NegocioException {

		this.getControladorCliente().verificarEmailPrinciapObrigatorio(emailPrincipal);
	}

	/**
	 * Desfazer consistir leitura.
	 *
	 * @param parametros the parametros
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void desfazerConsistirLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {

		this.getControladorCronogramaFaturamento().desfazerConsistirLeitura(parametros);
	}

	/**
	 * Desfazer registrar leitura.
	 *
	 * @param parametros the parametros
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void desfazerRegistrarLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {

		this.getControladorCronogramaFaturamento().desfazerRegistrarLeitura(parametros);
	}

	/**
	 * Desfazer supervisorio.
	 *
	 * @param listaSupervisorioMedicaoVO the lista supervisorio medicao vo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void desfazerSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws NegocioException,
					ConcorrenciaException {

		this.getControladorSupervisorio().desfazerSupervisorio(listaSupervisorioMedicaoVO);
	}

	/**
	 * Desfazer consolidar supervisorio.
	 *
	 * @param listaSupervisorioMedicaoVO the lista supervisorio medicao vo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void desfazerConsolidarSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws NegocioException,
					ConcorrenciaException {

		this.getControladorSupervisorio().desfazerConsolidarSupervisorio(listaSupervisorioMedicaoVO);
	}

	/**
	 * Exportar dados moveis.
	 *
	 * @param parametros the parametros
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 */
	public byte[] exportarDadosMoveis(Map<String, Object> parametros) throws NegocioException {

		return this.getControladorConsulta().exportarDadosMoveis(parametros);
	}

	/**
	 * Popular qpnr ponto consumo.
	 *
	 * @param listaPontoConsumoVO the lista ponto consumo vo
	 * @param chavePrimaria the chave primaria
	 * @throws GGASException the GGAS exception
	 */
	public void popularQPNRPontoConsumo(Collection<PontoConsumoVO> listaPontoConsumoVO, Long chavePrimaria) throws GGASException {

		getControladorApuracaoPenalidade().popularQPNRPontoConsumo(listaPontoConsumoVO, chavePrimaria);

	}

	/**
	 * Transferir saldo qpnr.
	 *
	 * @param migrarSaldo the migrar saldo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void transferirSaldoQPNR(MigrarSaldoQPNRVO migrarSaldo) throws NegocioException, ConcorrenciaException {

		getControladorApuracaoPenalidade().transferirSaldoQPNR(migrarSaldo);
	}

	/**
	 * Validar contrato encerrado.
	 *
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	public void validarContratoEncerrado(Long idContrato) throws NegocioException {

		this.getControladorContrato().validarContratoEncerrado(idContrato);

	}

	/**
	 * Gerar relatorio cobranca recuperacao penalidades.
	 *
	 * @param filtro the filtro
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioCobrancaRecuperacaoPenalidades(Map<String, Object> filtro) throws GGASException {

		return getControladorRelatorioCobrancaRecuperacaoPenalidades().gerarRelatorioCobrancaRecuperacaoPenalidades(filtro);
	}

	/**
	 * Listar comentario solicitacao consumo.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param dataSolicitacao the data solicitacao
	 * @param chaveContratoPontoConsumoModalidade the chave contrato ponto consumo modalidade
	 * @return the collection
	 */
	public Collection<SolicitacaoConsumoPontoConsumo> listarComentarioSolicitacaoConsumo(Long chavePontoConsumo, Date dataSolicitacao,
					Long chaveContratoPontoConsumoModalidade) {

		return this.getControladorProgramacao().listarComentarioSolicitacaoConsumo(chavePontoConsumo, dataSolicitacao,
						chaveContratoPontoConsumoModalidade);
	}

	/**
	 * Atualizar penalidade modalidade.
	 *
	 * @param idModalidade the id modalidade
	 * @param idApuracaoPenalidade the id apuracao penalidade
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade>
					atualizarPenalidadeModalidade(Long idModalidade, Long idApuracaoPenalidade) throws NegocioException,
									ConcorrenciaException {

		return getControladorApuracaoPenalidade().atualizarPenalidadeModalidade(idModalidade, idApuracaoPenalidade);

	}

	/**
	 * Atualizar apuracao quantidade penalidade periodicidade.
	 *
	 * @param apuracaoQuantidadePenalidadePeriodicidade the apuracao quantidade penalidade periodicidade
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarApuracaoQuantidadePenalidadePeriodicidade(
					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade) throws ConcorrenciaException,
					NegocioException {

		getControladorApuracaoPenalidade().atualizarApuracaoQuantidadePenalidadePeriodicidade(apuracaoQuantidadePenalidadePeriodicidade);
	}

	/**
	 * Obter lista tipo modelo.
	 *
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoModelo() throws GGASException {

		return this.getControladorEntidadeConteudo().obterListaTipoModelo();
	}

	/**
	 * Validar datas contrato complementar.
	 *
	 * @param chavePrimariaPrincipal the chave primaria principal
	 * @param contrato the contrato
	 * @throws GGASException the GGAS exception
	 */
	public void validarDatasContratoComplementar(Long chavePrimariaPrincipal, Contrato contrato) throws GGASException {

		this.getControladorContrato().validarDatasContratoComplementar(chavePrimariaPrincipal, contrato);
	}

	/**
	 * Validar modelo contrato complementar.
	 *
	 * @param modeloContrato the modelo contrato
	 * @throws NegocioException the negocio exception
	 */
	public void validarModeloContratoComplementar(ModeloContrato modeloContrato) throws NegocioException {

		this.getControladorModeloContrato().validarModeloContratoComplementar(modeloContrato);

	}

	/**
	 * Consultar contrato principal e complementar.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Contrato> consultarContratoPrincipalEComplementar(Map<String, Object> filtro) throws GGASException {

		return this.getControladorContrato().consultarContratoPrincipalEComplementar(filtro);
	}

	/**
	 * Atualizar contrato.
	 *
	 * @param contrato the contrato
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void atualizarContrato(Contrato contrato) throws ConcorrenciaException, NegocioException {

		this.getControladorContrato().atualizar(contrato);
	}

	/**
	 * Obter soma qdc contrato.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param dataInicioVigencia the data inicio vigencia
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal obterSomaQdcContrato(ContratoPontoConsumo contratoPontoConsumo, Date dataInicioVigencia) throws NegocioException {

		return this.getControladorContrato().obterSomaQdcContrato(contratoPontoConsumo, dataInicioVigencia);
	}

	/**
	 * Validar dados contrato complementar.
	 *
	 * @param contrato the contrato
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosContratoComplementar(Contrato contrato) throws NegocioException {

		this.getControladorContrato().validarDadosContratoComplementar(contrato);
	}

	/**
	 * Obter lista tipo apuracao.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoApuracao() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoApuracao();
	}

	/**
	 * Obter lista preco cobranca.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaPrecoCobranca() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaPrecoCobranca();
	}

	/**
	 * Obter lista apuracao parada programada.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaApuracaoParadaProgramada() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaApuracaoParadaProgramada();
	}

	/**
	 * Obter lista apuracao parada nao programada.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaApuracaoParadaNaoProgramada() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaApuracaoParadaNaoProgramada();
	}

	/**
	 * Obter lista apuracao caso fortuito.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaApuracaoCasoFortuito() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaApuracaoCasoFortuito();
	}

	/**
	 * Obter lista apuracao falha fornecimento.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaApuracaoFalhaFornecimento() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaApuracaoFalhaFornecimento();
	}

	/**
	 * Obter lista tipo agrupamento contrato.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @throws NumberFormatException the number format exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoAgrupamentoContrato() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoAgrupamentoContrato();
	}

	/**
	 * Criar contrato penalidade.
	 *
	 * @return the contrato penalidade
	 * @throws GGASException the GGAS exception
	 */
	public ContratoPenalidade criarContratoPenalidade() throws GGASException {

		return (ContratoPenalidade) this.getControladorContrato().criarContratoPenalidade();
	}

	/**
	 * Validar adicao contrato penalidade.
	 *
	 * @param contratoPenalidade the contrato penalidade
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @throws GGASException the GGAS exception
	 */
	public void validarAdicaoContratoPenalidade(ContratoPenalidade contratoPenalidade,
					Collection<ContratoPenalidade> listaContratoPenalidade) throws GGASException {

		this.getControladorContrato().validarAdicaoContratoPenalidade(contratoPenalidade, listaContratoPenalidade);
	}

	/**
	 * Listar penalidades ret maior menor.
	 *
	 * @return the collection
	 */
	public Collection<Penalidade> listarPenalidadesRetMaiorMenor() {

		return this.getControladorApuracaoPenalidade().listarPenalidadesRetMaiorMenor();
	}

	/**
	 * Validar adicao nova penalidade retirada maior menor contrato.
	 *
	 * @param penalidadesRetiradaMaiorMenorVO the penalidades retirada maior menor vo
	 * @param listaPenalidadesRetMaiorMenorVO the lista penalidades ret maior menor vo
	 */
	public void validarAdicaoNovaPenalidadeRetiradaMaiorMenorContrato(PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO,
					Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO) {

		throw new UnsupportedOperationException();

	}

	/**
	 * Listar contrato penalidade retirada maior menor por contrato.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<ContratoPenalidade> listarContratoPenalidadeRetiradaMaiorMenorPorContrato(Long chavePrimaria) {

		return this.getControladorContrato().listarContratoPenalidadeRetiradaMaiorMenorPorContrato(chavePrimaria);
	}

	/**
	 * Obter contrato penalidade vigencia retirada maior menor.
	 *
	 * @param params the params
	 * @return the contrato penalidade
	 */
	public ContratoPenalidade obterContratoPenalidadeVigenciaRetiradaMaiorMenor(Map<String, Object> params) {

		return this.getControladorContrato().obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);
	}

	/**
	 * Obter contrato ponto consumo penalidade vigencia retirada maior menor.
	 *
	 * @param params the params
	 * @return the contrato ponto consumo penalidade
	 */
	public ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidadeVigenciaRetiradaMaiorMenor(Map<String, Object> params) {

		return this.getControladorContrato().obterContratoPontoConsumoPenalidadeVigencia(params);
	}

	/**
	 * Listar contrato penalidade take or pay por contrato.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<ContratoPenalidade> listarContratoPenalidadeTakeOrPayPorContrato(Long chavePrimaria) {

		return this.getControladorContrato().listarContratoPenalidadeTakeOrPayPorContrato(chavePrimaria);
	}

	/**
	 * Validar intervalo data vigencia com data assinatura contrato.
	 *
	 * @param contrato the contrato
	 * @param contratoPenalidade the contrato penalidade
	 * @param contratoPontoConsumoPenalidade the contrato ponto consumo penalidade
	 * @throws NegocioException the negocio exception
	 */
	public void validarIntervaloDataVigenciaComDataAssinaturaContrato(Contrato contrato, ContratoPenalidade contratoPenalidade,
					ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws NegocioException {

		this.getControladorContrato().validarIntervaloDataVigenciaComDataAssinaturaContrato(contrato, contratoPenalidade,
						contratoPontoConsumoPenalidade);
	}

	/**
	 * Obter contrato penalidade.
	 *
	 * @param idContrato the id contrato
	 * @param idPenalidade the id penalidade
	 * @param idPeriodicidadePenalidade the id periodicidade penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFimApuracao the data fim apuracao
	 * @return the contrato penalidade
	 * @throws NegocioException the negocio exception
	 */
	public ContratoPenalidade obterContratoPenalidade(Long idContrato, Long idPenalidade, Long idPeriodicidadePenalidade,
					Date dataInicioApuracao, Date dataFimApuracao) throws NegocioException {

		return this.getControladorContrato().obterContratoPenalidade(idContrato, idPenalidade, idPeriodicidadePenalidade,
						dataInicioApuracao, dataFimApuracao);
	}

	/**
	 * Aplicar tarifa media momento cobranca.
	 *
	 * @param listaPenalidades the lista penalidades
	 * @param isAplicarTarifa the is aplicar tarifa
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void aplicarTarifaMediaMomentoCobranca(Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades,
					boolean isAplicarTarifa) throws NegocioException, ConcorrenciaException {

		this.getControladorApuracaoPenalidade().aplicarTarifaMediaMomentoCobranca(listaPenalidades, isAplicarTarifa);

	}

	/**
	 * Validar recuperacao penalidade.
	 *
	 * @param valorQPNR the valor qpnr
	 * @param volumeRecuperacao the volume recuperacao
	 * @param lista the lista
	 * @param idPontoConsumo the id ponto consumo
	 * @param rubrica the rubrica
	 * @return the big decimal
	 * @throws FormatoInvalidoException the formato invalido exception
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal validarRecuperacaoPenalidade(String[] valorQPNR, String[] volumeRecuperacao,
					List<ApuracaoQuantidadePenalidadePeriodicidade> lista, Long idPontoConsumo, Rubrica rubrica)
					throws FormatoInvalidoException, NegocioException {

		return this.getControladorApuracaoPenalidade().validarRecuperacaoPenalidade(valorQPNR, volumeRecuperacao, lista, idPontoConsumo,
						rubrica);

	}

	/**
	 * Listar apuracao quantidade penalidade periodicidade por chave.
	 *
	 * @param idsApuracaoPenalidade the ids apuracao penalidade
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	public List<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidadePorChave(
					Long[] idsApuracaoPenalidade) throws NegocioException {

		return this.getControladorApuracaoPenalidade().listarApuracaoQuantidadePenalidadePeriodicidadePorChaves(idsApuracaoPenalidade);
	}

	/**
	 * Consultar contrato ponto consumo modalidades.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ContratoPontoConsumoModalidade>
					consultarContratoPontoConsumoModalidades(Contrato contrato, PontoConsumo pontoConsumo) throws NegocioException {

		return this.getControladorContrato().consultarContratoPontoConsumoModalidades(contrato, pontoConsumo);
	}

	/**
	 * Obter soma qdc contrato por periodo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param dataApuracao the data apuracao
	 * @param dataFimApuracao the data fim apuracao
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal obterSomaQdcContratoPorPeriodo(long chavePrimaria, Date dataApuracao, Date dataFimApuracao) throws NegocioException {

		return this.getControladorContrato().obterSomaQdcContratoPorPeriodo(chavePrimaria, dataApuracao, dataFimApuracao);
	}

	/**
	 * Listar nome imoveis.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 */
	public Collection<String> listarNomeImoveis(Long[] chavesPrimarias) {

		return this.getControladorImovel().listarNomeImoveis(chavesPrimarias);
	}

	/**
	 * Listar tipo contado por tipo pessoa do cliente.
	 *
	 * @param idTipoCliente the id tipo cliente
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<TipoContato> listarTipoContadoPorTipoPessoaDoCliente(String idTipoCliente) throws GGASException {

		Collection<TipoContato> listaTipoContato = new ArrayList<TipoContato>();

		if (idTipoCliente != null && !"".equals(idTipoCliente) && !"-1".equals(idTipoCliente)) {
			Long chavePrimaria = Long.valueOf(idTipoCliente);
			TipoPessoa tipoPessoa = obterTipoPessoaPorTipoCliente(chavePrimaria);

			Collection<TipoContato> listaTipoContatoAux = listarTipoContato();
			for (TipoContato tipoContato : listaTipoContatoAux) {
				int pessoaTipo = tipoContato.getPessoaTipo();
				if (pessoaTipo == tipoPessoa.getCodigo() || pessoaTipo == 0) {
					listaTipoContato.add(tipoContato);
				}
			}
		}
		return listaTipoContato;
	}

	/**
	 * Obter tipo pessoa por tipo cliente.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the tipo pessoa
	 * @throws NegocioException the negocio exception
	 */
	private TipoPessoa obterTipoPessoaPorTipoCliente(long chavePrimaria) throws NegocioException {

		TipoCliente tipoCliente = getControladorCliente().obterTipoCliente(chavePrimaria);
		return tipoCliente.getTipoPessoa();
	}

	/**
	 * Consultar city gate medicao por city gate e data.
	 *
	 * @param cityGate the city gate
	 * @param data the data
	 * @return the list
	 */
	public List<CityGateMedicao> consultarCityGateMedicaoPorCityGateEData(CityGate cityGate, Date data) {

		return getControladorCityGateMedicao().consultarCityGateMedicaoPorCityGateEData(cityGate, data);
	}

	/**
	 * Obter city gate.
	 *
	 * @param idCityGate the id city gate
	 * @return the city gate
	 * @throws GGASException the GGAS exception
	 */
	public CityGate obterCityGate(String idCityGate) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		CityGate cityGate = null;
		if (idCityGate != null && !"".equals(idCityGate)) {
			long chavePrimariaCityGate = Long.parseLong(idCityGate);
			if (chavePrimariaCityGate != 0) {
				filtro.put("chavePrimaria", chavePrimariaCityGate);
				Collection<TabelaAuxiliar> listaTabelaAuxiliar =
								pesquisarTabelaAuxiliar(filtro, "br.com.ggas.cadastro.operacional.impl.CityGateImpl");
				for (TabelaAuxiliar tabelaAuxiliar : listaTabelaAuxiliar) {
					cityGate = (CityGateImpl) tabelaAuxiliar;
				}
			}
		}
		return cityGate;
	}

	/**
	 * Gets the controlador contrato adequacao parceria.
	 *
	 * @return the controlador contrato adequacao parceria
	 */
	public ControladorContratoAdequacaoParceria getControladorContratoAdequacaoParceria() {

		return (ControladorContratoAdequacaoParceria) serviceLocator
						.getControladorNegocio(ControladorContratoAdequacaoParceria.BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA);
	}

	/**
	 * Obter historico operacao medidor.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param dataAutorizacao the data autorizacao
	 * @param idOperacaoMedidor the id operacao medidor
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(Long idPontoConsumo, Date dataAutorizacao,
					Long idOperacaoMedidor) throws NegocioException {

		return getControladorMedidor().obterHistoricoOperacaoMedidor(idPontoConsumo, dataAutorizacao, idOperacaoMedidor);
	}

	/**
	 * 
	 * @param avisosCorte
	 * @param idPontoConsumo
	 * @return Coleção de Autorização de Serviço
	 * @throws NegocioException
	 */
	public Collection<ServicoAutorizacaoVOCobranca> criarListaServicoAutorizacaoVOCobranca(Collection<AvisoCorte> avisosCorte,
			Long idPontoConsumo) throws NegocioException {

		return getControladorAvisoCorte().criarListaServicoAutorizacaoVOCobranca(avisosCorte, idPontoConsumo);
	}

	/**
	 * Calcular juros entre datas indice financeiro.
	 *
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param idIndice the id indice
	 * @param valor the valor
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal calcularJurosEntreDatasIndiceFinanceiro(Date dataInicio, Date dataFim, Long idIndice, BigDecimal valor)
					throws NegocioException {

		return this.getControladorTarifa().calcularJurosEntreDatasIndiceFinanceiro(dataInicio, dataFim, idIndice, valor);
	}

	/**
	 * Listar funcionario fiscal vendedor.
	 *
	 * @param isFuncionarioFiscal the is funcionario fiscal
	 * @param isFuncionarioVendedor the is funcionario vendedor
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Funcionario> listarFuncionarioFiscalVendedor(boolean isFuncionarioFiscal, boolean isFuncionarioVendedor)
					throws NegocioException {

		return this.getControladorFuncionario().listarFuncionarioFiscalVendedor(isFuncionarioFiscal, isFuncionarioVendedor);
	}

	/**
	 * Inserir historico nota debito credito.
	 *
	 * @param historicoNotaDebitoCredito the historico nota debito credito
	 * @throws NegocioException the negocio exception
	 */
	public void inserirHistoricoNotaDebitoCredito(HistoricoNotaDebitoCredito historicoNotaDebitoCredito) throws NegocioException {

		this.getControladorFatura().inserirHistoricoNotaDebitoCredito(historicoNotaDebitoCredito);
	}

	/**
	 * Gets the lista historico nota debito credito.
	 *
	 * @param idFatura the id fatura
	 * @return the lista historico nota debito credito
	 */
	public Collection<HistoricoNotaDebitoCredito> getListaHistoricoNotaDebitoCredito(Long idFatura) {

		return this.getControladorFatura().getListaHistoricoNotaDebitoCredito(idFatura);
	}

	/**
	 * Validar situacao pagamento fatura.
	 *
	 * @param faturas the faturas
	 * @throws NegocioException the negocio exception
	 */
	public void validarSituacaoPagamentoFatura(List<Fatura> faturas) throws NegocioException {

		this.getControladorFatura().validarSituacaoPagamentoFatura(faturas);
	}

	/**
	 * Atualizar integracao fatura.
	 *
	 * @param fatura the fatura
	 * @param tituloReceber the titulo receber
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	public void atualizarIntegracaoFatura(Fatura fatura, boolean tituloReceber) throws NegocioException, ConcorrenciaException {

		this.getControladorIntegracao().atualizarIntegracaoFatura(fatura, tituloReceber);
	}

	/**
	 * Listar solicitacao consumo diarias intradiarias.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoDiariasIntradiarias(Map<String, Object> filtro)
					throws GGASException {

		return this.getControladorProgramacao().listarSolicitacaoConsumoDiariasIntradiarias(filtro);
	}

	/**
	 * Obter quantidade fatura historico revisao por fatura.
	 *
	 * @param chaveFatura the chave fatura
	 * @return the long
	 */
	public Long obterQuantidadeFaturaHistoricoRevisaoPorFatura(Long chaveFatura) {

		return getControladorFatura().obterQuantidadeFaturaHistoricoRevisaoPorFatura(chaveFatura);
	}

	/**
	 * Obter lista modelo constrato por descricao.
	 *
	 * @param descricaoModelo the descricao modelo
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ModeloContrato> obterListaModeloConstratoPorDescricao(String descricaoModelo) throws GGASException {

		return this.getControladorModeloContrato().obterListaModeloConstratoPorDescricao(descricaoModelo);
	}

	/**
	 * Obter proxima versao modelo contrato.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the modelo contrato
	 * @throws GGASException the GGAS exception
	 */
	public ModeloContrato obterProximaVersaoModeloContrato(Long chavePrimaria) throws GGASException {

		return this.getControladorModeloContrato().obterProximaVersaoModeloContrato(chavePrimaria);
	}

	/**
	 * Listar modulos.
	 *
	 * @param usuarioLogado the usuario logado
	 * @return the collection
	 */
	public Collection<Menu> listarModulos(Usuario usuarioLogado) {

		return this.getControladorMenu().listarModulos(usuarioLogado);
	}

	/**
	 * Lista valor nominal indice finaceiro.
	 *
	 * @param dataReferenciaInicial the data referencia inicial
	 * @param dataReferenciaFinal the data referencia final
	 * @param descricaoAbreviada the descricao abreviada
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinaceiro(Date dataReferenciaInicial, Date dataReferenciaFinal,
					String descricaoAbreviada) throws NegocioException {

		return this.getControladorContrato().listaValorNominalIndiceFinaceiro(dataReferenciaInicial, dataReferenciaFinal,
						descricaoAbreviada);
	}

	/**
	 * Fator correcao indice financeiro.
	 *
	 * @param indiceFinanceiro the indice financeiro
	 * @param dataReferenciaInicial the data referencia inicial
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal fatorCorrecaoIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial) throws NegocioException {

		return this.getControladorContrato().fatorCorrecaoIndiceFinanceiro(indiceFinanceiro, dataReferenciaInicial);
	}

	/**
	 * Fator correcao indice financeiro.
	 *
	 * @param indiceFinanceiro the indice financeiro
	 * @param dataReferenciaInicial the data referencia inicial
	 * @param dataReferenciaFinal the data referencia final
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal
					fatorCorrecaoIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial, Date dataReferenciaFinal)
									throws NegocioException {

		return this.getControladorContrato().fatorCorrecaoIndiceFinanceiro(indiceFinanceiro, dataReferenciaInicial, dataReferenciaFinal);
	}

	/**
	 * Listar statusimovel.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> listarStatusimovel() {

		return this.getControladorEntidadeConteudo().obterListaStatusImovel();
	}

	/**
	 * Obter status imovel por codigo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterStatusImovelPorCodigo(Long chavePrimaria) {

		return this.getControladorEntidadeConteudo().obterStatusImovelPorCodigo(chavePrimaria);
	}

	/**
	 * Obter lista fornecedores combustiveis.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterListaFornecedoresCombustiveis() {

		return this.getControladorEntidadeConteudo().obterListaFornecedoresCombustiveis();
	}

	/**
	 * Obter lista aparelhos maior indice consumo.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterListaAparelhosMaiorIndiceConsumo() {

		return this.getControladorEntidadeConteudo().obterListaAparelhosMaiorIndiceConsumo();
	}

	/**
	 * Obter lista tipos fontes energeticas.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterListaTiposFontesEnergeticas() {

		return this.getControladorEntidadeConteudo().obterListaTiposFontesEnergeticas();
	}

	/**
	 * Obter lista estado rede.
	 *
	 * @return the collection
	 */
	public Collection<EntidadeConteudo> obterListaEstadoRede() {

		return this.getControladorEntidadeConteudo().obterListaEstadoRede();
	}

	/**
	 * Gets the controlador agente.
	 *
	 * @return the controlador agente
	 */
	private ControladorAgente getControladorAgente() {

		return (ControladorAgente) serviceLocator.getBeanPorID("controladorAgenteImpl");
	}

	/**
	 * Obter agente por chave primaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the agente
	 * @throws NegocioException the negocio exception
	 */
	public Agente obterAgentePorChavePrimaria(Long chavePrimaria) throws NegocioException {

		return this.getControladorAgente().obterAgente(chavePrimaria);
	}

	/**
	 * Enviar proposta por email.
	 *
	 * @param proposta the proposta
	 * @param bytes the bytes
	 * @throws GGASException the GGAS exception
	 */
	public void enviarPropostaPorEmail(Proposta proposta, byte[] bytes) throws GGASException {

		this.getControladorProposta().enviarPropostaPorEmail(proposta, bytes);
	}

	/**
	 * Listar imoveis em prospeccao.
	 *
	 * @param filtro the filtro
	 * @param idFuncionario the id funcionario
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Imovel> listarImoveisEmProspeccao(Map<String, Object> filtro, Long idFuncionario) throws NegocioException {

		return this.getControladorImovel().listarImoveisEmProspeccao(filtro, idFuncionario);
	}

	/**
	 * Método responsável por obter a lista de motivos de complemento de fatura.
	 *
	 * @return Uma coleção de motivos de complemento de fatura.
	 */
	public Collection<EntidadeConteudo> obterListaMotivoCcomplementoFatura() {

		return this.getControladorEntidadeConteudo().obterListaMotivoCcomplementoFatura();
	}

	/**
	 * Obter lista tipo aplicacao tributo aliquota.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoAplicacaoTributoAliquota() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoAplicacaoTributoAliquota();
	}

	/**
	 * Obter lista tipo aplicacao reducao base calculo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoAplicacaoReducaoBaseCalculo() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoAplicacaoReducaoBaseCalculo();
	}

	/**
	 * Obter situacao proposta por descricao.
	 *
	 * @author crsilva
	 *         Método responsável por obter
	 *         situação proposta pela descrição
	 * @param descricao the descricao
	 * @return SituacaoProposta
	 * @throws NegocioException the negocio exception
	 */
	public SituacaoProposta obterSituacaoPropostaPorDescricao(String descricao) throws NegocioException {

		return getControladorProposta().obterSituacaoPropostaPorDescricao(descricao);
	}

	/**
	 * Calcular valores proposta.
	 *
	 * @author crsilva
	 *         Método responsável por calcular valores vindos
	 *         do levantamento de mercado e imprimir na tela
	 *         de inclusão de proposta
	 * @param levantamentoMercado the levantamento mercado
	 * @return HashMap de valores calculados
	 */
	public Map<String, BigDecimal> calcularValoresProposta(LevantamentoMercado levantamentoMercado) {

		return this.getControladorProposta().calcularValoresProposta(levantamentoMercado);
	}

	/**
	 * Listar tarifa vigencia por tarifa.
	 *
	 * @param idTarifa the id tarifa
	 * @return the collection
	 */
	public Collection<TarifaVigencia> listarTarifaVigenciaPorTarifa(Long idTarifa) {

		return this.getControladorTarifa().listarTarifaVigenciaPorTarifa(idTarifa);
	}

	/**
	 * Listar pontos consumo por chave cliente.
	 *
	 * @param idCliente the id cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumo> listarPontosConsumoPorChaveCliente(Long idCliente) throws NegocioException {

		return this.getControladorPontoConsumo().listarPontosConsumoPorChaveCliente(idCliente);
	}

	/**
	 * Inserir numero ano proposta.
	 *
	 * @param proposta the proposta
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public void inserirNumeroAnoProposta(Proposta proposta) throws ConcorrenciaException, NegocioException {

		this.getControladorProposta().inserirNumeroAnoProposta(proposta);
	}

	/**
	 * Obter situacao imovel por descricao.
	 *
	 * @param descricao the descricao
	 * @return the situacao imovel
	 */
	public SituacaoImovel obterSituacaoImovelPorDescricao(String descricao) {

		return this.getControladorImovel().obterSituacaoImovelPorDescricao(descricao);
	}

	/**
	 * Incluir fatura complementar.
	 *
	 * @param idMotivoComplemento the id motivo complemento
	 * @param idFatura the id fatura
	 * @param volumeApurado the volume apurado
	 * @param valor the valor
	 * @param observacoesComplemento the observacoes complemento
	 * @param dadosResumoFatura the dados resumo fatura
	 * @param pontoConsumo the ponto consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param historico the historico
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	public void incluirFaturaComplementar(Long idMotivoComplemento, Long idFatura, String[] volumeApurado, String valor,
					String observacoesComplemento, DadosResumoFatura dadosResumoFatura, PontoConsumo pontoConsumo,
					ContratoPontoConsumo contratoPontoConsumo, HistoricoMedicao historico, DadosAuditoria dadosAuditoria)
					throws GGASException {

		this.getControladorFatura().incluirFaturaComplementar(idMotivoComplemento, idFatura, volumeApurado, valor, observacoesComplemento,
						dadosResumoFatura, pontoConsumo, contratoPontoConsumo, historico, dadosAuditoria);

	}

	/**
	 * Gerar boleto bancario padrao.
	 *
	 * @param faturaParam the fatura param
	 * @param ultimaFatura the ultima fatura
	 * @param tipoDocumento the tipo documento
	 * @param dataVencimento the data vencimento
	 * @param subtrairValorRecebido the subtrair valor recebido
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarBoletoBancarioPadrao(Fatura faturaParam, Boolean ultimaFatura, String tipoDocumento,
					String dataVencimento, boolean subtrairValorRecebido) throws GGASException {

		return this.getControladorFatura().gerarBoletoBancarioPadrao(faturaParam, ultimaFatura, tipoDocumento,
						dataVencimento, subtrairValorRecebido);

	}

	/**
	 * Listar rotahistorico.
	 *
	 * @return the collection
	 */
	public Collection<RotaHistorico> listarRotahistorico() {

		return this.getControladorRota().listarRotahistorico();
	}

	/**
	 * Consultar ponto consumo por medidor.
	 *
	 * @param medidor the medidor
	 * @return the ponto consumo
	 */
	public PontoConsumo consultarPontoConsumoPorMedidor(Medidor medidor) {

		return this.getControladorPontoConsumo().consultarPontoConsumoPorMedidor(medidor);
	}

	/**
	 * Consultar medidor independente.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Medidor> consultarMedidorIndependente(Map<String, Object> filtro) throws GGASException {

		return this.getControladorMedidor().consultarMedidorIndependente(filtro);
	}

	/**
	 * Consultar supervisorio medicao diaria por medidor.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiariaPorMedidor(Map<String, Object> filtro) throws GGASException {

		return this.getControladorSupervisorio().consultarSupervisorioMedicaoDiariaPorMedidor(filtro);
	}



	/**
	 * Método responsável pela conversão de
	 * medidas para a unidade definida como
	 * padrão para uso do sistema.
	 * 
	 * @param valor
	 *            Valor a ser convertido.
	 * @param unidadeOrigem
	 *            Unidade Origem.
	 * @param unidadeDestino
	 *            Unidade Destino, que também é
	 *            considerada a unidade padrão.
	 * @return Valor da unidade origem convertido
	 *         para a unidade padrão do
	 *         sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public BigDecimal converterUnidadeMedida(BigDecimal valor, Unidade unidadeOrigem, Unidade unidadeDestino) throws NegocioException {

		return this.getControladorHistoricoConsumo().converterUnidadeMedida(valor, unidadeOrigem, unidadeDestino);
	}

	/**
	 * Recupera do banco a unidade de medida em conformidade com o filtro.
	 *
	 * @param filtro Um objeto Unidade usado como modelo na consulta
	 * @return Um objeto Unidade correspondente com o modelo, ou null
	 * @throws org.hibernate.NonUniqueResultException se o filtro for genérico demais
	 */
	public Unidade obterUnidade(Unidade filtro) {

		return this.getControladorUnidade().obterUnidade(filtro);
	}

	/**
	 * Montar lista tarifa vigencia desconto na insercao.
	 *
	 * @param tarifaVigencia the tarifa vigencia
	 * @return the list
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaInsercao(TarifaVigencia tarifaVigencia)
					throws ConcorrenciaException, NegocioException {

		return this.getControladorTarifa().montarListaTarifaVigenciaDescontoNaInsercao(tarifaVigencia);
	}

	/**
	 * Montar lista tarifa vigencia desconto na atualizacao.
	 *
	 * @param tarifaVigencia the tarifa vigencia
	 * @return the list
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	public List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaAtualizacao(TarifaVigencia tarifaVigencia)
					throws ConcorrenciaException, NegocioException {

		return this.getControladorTarifa().montarListaTarifaVigenciaDescontoNaAtualizacao(tarifaVigencia);
	}

	/**
	 * Obter lista tipo vigencia substituicao tributaria.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<EntidadeConteudo> obterListaTipoVigenciaSubstituicaoTributaria() throws NegocioException {

		return this.getControladorEntidadeConteudo().obterListaTipoVigenciaSubstituicaoTributaria();
	}

	/**
	 * Acoes para sincronizacao.
	 *
	 * @param tarifaVigenciaModificada the tarifa vigencia modificada
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<AcaoSincroniaDesconto> acoesParaSincronizacao(TarifaVigencia tarifaVigenciaModificada) throws NegocioException {

		return this.getControladorTarifa().acoesParaSincronizacao(tarifaVigenciaModificada);
	}

	/**
	 * Checks if is tipo aplicacao tributo aliquota percentual.
	 *
	 * @param tipoAplicacaoTributoAliquota the tipo aplicacao tributo aliquota
	 * @return true, if is tipo aplicacao tributo aliquota percentual
	 * @throws NegocioException the negocio exception
	 */
	public boolean isTipoAplicacaoTributoAliquotaPercentual(EntidadeConteudo tipoAplicacaoTributoAliquota) throws NegocioException {

		return getControladorConstanteSistema().isTipoAplicacaoTributoAliquotaPercentual(tipoAplicacaoTributoAliquota);
	}
	
	
	/**
	 * Método responsável por obter o controlador
	 * de funcionário.
	 *
	 * @return ControladorFuncionario O
	 *         controlador de funcionário.
	 */
	private ControladorEquipamento getControladorEquipamento() {

		return (ControladorEquipamento) serviceLocator.getControladorNegocio(ControladorEquipamento.BEAN_ID_CONTROLADOR_EQUIPAMENTO);
	}
	
	
	/**
	 * Método responsável por consultar os
	 * equipamentos pelo filtro informado.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de equipamentos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<Equipamento> consultarEquipamentos(Map<String, Object> filtro) throws GGASException {

		return getControladorEquipamento().consultarEquipamentos(filtro);
	}
	
	/**
	 * Método responsável por buscar o equipamento
	 * pela chave primária.
	 *
	 * @param chavePrimaria
	 *            A chave primária do equipamento
	 * @return Um Equipamento
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Equipamento buscarEquipamentoPorChave(Long chavePrimaria) throws GGASException {

		return (Equipamento) this.getControladorEquipamento().obter(chavePrimaria);
	}
	
	
	/**
	 * Método responsável por criar um Equipamento.
	 *
	 * @return Equipamento um equipamento do
	 *         sistema.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Equipamento criarEquipamento() throws GGASException {

		return (Equipamento) this.getControladorEquipamento().criar();
	}
	
	/**
	 * Método responsável por inserir um
	 * Equipamento.
	 *
	 * @param equipamento the equipamento
	 * @return chavePrimeria do equipamento
	 *         inserido
	 * @throws GGASException the GGAS exception
	 */
	public Long inserirEquipamento(Equipamento equipamento) throws GGASException {

		return this.getControladorEquipamento().inserir(equipamento);
	}
	
	
	/**
	 * Método responsável por remover os
	 * equipamentos.
	 *
	 * @param chavesPrimarias
	 *            Chaves dos funcionários
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void removerEquipamentos(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		this.getControladorEquipamento().remover(chavesPrimarias, dadosAuditoria);
	}
	
	/**
	 * Método responsável por atualizar a entidade
	 * Equipamento.
	 *
	 * @param equipamento
	 *            Um equipamento do sistema
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarEquipamento(Equipamento equipamento) throws GGASException {

		this.getControladorEquipamento().atualizar(equipamento);
	}
	
	/**
	 * Criar ponto consumo equipamento.
	 *
	 * @return the ponto consumo equipamento
	 * @throws GGASException the GGAS exception
	 */
	public PontoConsumoEquipamento criarPontoConsumoEquipamento() throws GGASException {

		return (PontoConsumoEquipamento) this.getControladorImovel().criarPontoConsumoEquipamento();
	}
	
	/**
	 * Método responsável por obter o item de
	 * equipamento.
	 *
	 * @param chaveEquipamento the chave equipamento
	 * @return Uma um item de equipamento.
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Equipamento obterEquipamento(Long chaveEquipamento) throws GGASException {

		return this.getControladorEquipamento().obterEquipamento(chaveEquipamento);
	}
	
	
	/**
	 * Método responsável por validar o ponto de
	 * consumo equipamento.
	 *
	 * @param pontosConsumoEquipamento Lista de pontos de consumo
	 *            equipamento
	 * @param pontoConsumoEquipamento the ponto consumo equipamento
	 * @param indexPontoConsumoEquipamento the index ponto consumo equipamento
	 * @throws GGASException the GGAS exception
	 */
	public void validarPontoConsumoEquipamento(Collection<PontoConsumoEquipamento> pontosConsumoEquipamento,
			PontoConsumoEquipamento pontoConsumoEquipamento, Integer indexPontoConsumoEquipamento) throws GGASException {

		this.getControladorImovel().validarPontoConsumoEquipamento(pontosConsumoEquipamento, pontoConsumoEquipamento,
						indexPontoConsumoEquipamento);
	}
	
	/**
	 * Método responsável por validar o ponto de consumo equipamento.
	 *
	 * @param pontoConsumo Um ponto de consumo
	 * @param pontoConsumoEquipamento Um ponto de consumo equipamento
	 * @throws GGASException the GGAS exception
	 */
	public void validarPontoConsumoEEquipamento(PontoConsumo pontoConsumo, PontoConsumoEquipamento pontoConsumoEquipamento)
			throws GGASException {

		this.getControladorImovel().validarPontoConsumoEEquipamento(pontoConsumo, pontoConsumoEquipamento);
	}
	
	/**
	 * Método responsável por consultar as vazoes de equipamento residenciais.
	 *
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de vazao equipamento residencial.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<PotenciaFixaEquipamentoVO>  listarPotenciasFixas() throws GGASException {

		return getControladorEquipamento().listarPotenciasFixas();
	}
	
	/**
	 * Obter ponto consumo sem relacionamentos.
	 *
	 * @param chavePrimaria a chave primaria
	 * @return o ponto consumo
	 * @throws NegocioException
	 */
	public PontoConsumo obterPontoConsumoSemRelacionamentos(Long chavePrimaria) throws NegocioException {

		return (PontoConsumo) this.getControladorPontoConsumo().obter(chavePrimaria, "rota");
	}
	
	/**
	 * Método responsável por buscar a rota pela
	 * chave primária.
	 * 
	 * @param chavePrimaria
	 * @param propriedadesLazy
	 * @return Rota
	 * @throws GGASException
	 */
	public Rota buscarRota(Long chavePrimaria, String... propriedadesLazy) throws GGASException {

		return (Rota) this.getControladorRota().obter(chavePrimaria, propriedadesLazy);
	}

	/**
	 * Método responsável por consultar uma ou mais proposta de acordo com o filtro informado.
	 *
	 * @param filtro O filtro com os parâmetros para a pesquisa de proposta.
	 * @return Uma coleção de proposta.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public Collection<Proposta> consultarPropostas(FiltroProposta filtro) throws GGASException {

		return getControladorProposta().consultarPropostas(filtro);
	}

	/**
	 * Método responsável por criar um layout de contrato.
	 *
	 * @return Um layout de contrato.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public LayoutRelatorio criarLayoutRelatorio() {

		return (LayoutRelatorio) this.getControladorModeloContrato().criarLayoutRelatorio();
	}

	/**
	 * @return ImovelServicoTipoRestricao
	 */
	public ImovelServicoTipoRestricao criarImovelServicoTipoRestricao() {

		return (ImovelServicoTipoRestricao) this.getControladorImovel().criarImovelServicoTipoRestricao();
	}

	/**
	 * 
	 * @return listaRelatorioTokens
	 */
	public Collection<RelatorioToken> obterListaRelatorioTokens(Long chaveTipoRelatorio) {

		return this.getControladorRelatorio().obterListaRelatorioTokens(chaveTipoRelatorio);
	}

	/**
	 * Criar leiaute filtro.
	 *
	 * @return the documento layout
	 */
	public DocumentoLayout criarLeiauteFiltro() {

		return this.getControladorDocumentoLayout().criarLeiauteFiltro();
	}

	/**
	 * Obter documentos layout.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	public Collection<DocumentoLayout> obterDocumentosLayout() {

		return this.getControladorDocumentoLayout().obterDocumentosLayout();
	}

	/**
	 * Obter documento layout.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the documento layout
	 * @throws NegocioException 
	 */
	public DocumentoLayout obterDocumentoLayout(Long chavePrimaria) throws NegocioException {

		return (DocumentoLayout) this.getControladorDocumentoLayout().obter(chavePrimaria, "formaArrecadacao");
	}
	
	/**
	 *  Método responsável por validar se um dos filtros foi selecionado (Ponto de Consumo ou Cliente)
	 * 
	 * @param idCliente
	 * @param idPontoConsumo
	 * @param idImovel
	 * @throws GGASException
	 */
	public void validarExibirExtratoDebito(Long idCliente, Long idPontoConsumo, Long idImovel) throws GGASException {

		this.getControladorCobranca().validarExibirExtratoDebito(idCliente, idPontoConsumo, idImovel);
	}

	/**
	 * Listar vendedores.
	 *
	 * @return a coleção de vendedores.
	 * @throws NegocioException
	 */
	public Collection<Funcionario> listarVendedores() throws NegocioException {
		return this.getControladorFuncionario().listarVendedores();
	}

	/**
	 * Listar fiscais.
	 *
	 * @return a coleção de fiscais.
	 * @throws NegocioException
	 */
	public Collection<Funcionario> listarFiscais() throws NegocioException {
		return this.getControladorFuncionario().listarFiscais();
	}
	
	/**
	 * @return lista de equipes
	 * @throws NegocioException
	 */
	public Collection<EntidadeNegocio> listarEquipes() throws NegocioException {
		return this.getControladorEquipe().listar();
	}

	/**
	 * @param chavePrimaria
	 * @return equipe
	 * @throws NegocioException
	 */
	public Equipe obterEquipe(Long chavePrimaria) throws NegocioException {
		return this.getControladorEquipe().obterEquipe(chavePrimaria);
	}

	/**
	 * @return controlador equipe
	 */
	private ControladorEquipe getControladorEquipe(){
		return serviceLocator.getControladorEquipe();
	}
	
	/**
	 * Lista medidores que compoem medidor virtual.
	 *
	 * @param medidorVirtual o medidor virtual
	 * @return a coleção
	 * @throws NegocioException
	 */
	public Collection<Medidor> listaMedidoresQueCompoemMedidorVirtual(Medidor medidorVirtual) throws NegocioException {
		return this.getControladorMedidor().listaMedidoresQueCompoemMedidorVirtual(medidorVirtual);
	}

	/**
	 * Método responsável por obter uma contato do imóvel pela chave passada por parâmetro.
	 *
	 * @param chave .
	 * @return Imovel.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public ContatoImovel obterContatoImovel(long chave) throws GGASException {

		return getControladorImovel().obterContatoImovel(chave);
	}

	/**
	 * @return servicoTipo collection
	 */
	public Collection<ServicoTipo> consultarServicoTipoOrdenados() {

		return getControladorImovel().consultarServicoTipoOrdenados();
	}

	/**
	 * @param chavePrimaria
	 * @return ServicoTipo
	 */
	public ServicoTipo consultarServicoTipo(Long chavePrimaria) {

		return getControladorImovel().consultarServicoTipo(chavePrimaria);
	}

	/**
	 * Consultar servico tipo.
	 * 
	 * @param chavePrimariaImovel the chave primaria
	 * @return the servico tipo
	 */
	public Collection<ImovelServicoTipoRestricao> consultarImovelServicoTipoRestricaoPorImovel(Long chavePrimariaImovel) {

		return getControladorImovel().consultarImovelServicoTipoRestricaoPorImovel(chavePrimariaImovel);
	}

	/**
	 * Método responsável por obter uma contato do imóvel pela chave passada por parâmetro.
	 *
	 * @param chave .
	 * @return Imovel.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	public ContatoCliente obterContatoCliente(long chave) throws GGASException {

		return getControladorCliente().obterContatoCliente(chave);
	}

	/**
	 * Valida dados do anexo do contrato
	 * 
	 * @param contratoAnexo
	 */
	public void validarDadosContratoAnexo(ContratoAnexo contratoAnexo) throws NegocioException {
		getControladorContrato().validarDadosContratoAnexo(contratoAnexo);
	}

	/**
	 * Verifica descricao do anexo
	 * 
	 * @param indexLista
	 * @param listaAnexos
	 * @param contratoAnexo
	 */
	public void verificarDescricaoContratoAnexo(Integer indexLista, Collection<ContratoAnexo> listaAnexos, ContratoAnexo contratoAnexo)
			throws NegocioException {
		getControladorContrato().verificarDescricaoContratoAnexo(indexLista, listaAnexos, contratoAnexo);
	}

	/**
	 * Obtem o anexo do contrato
	 * 
	 * @param chavePrimaria
	 * @return contratoAnexo
	 */
	public ContratoAnexo obterContratoAnexo(long chavePrimaria) {
		return getControladorContrato().obterContratoAnexo(chavePrimaria);
	}

	/**
	 * Obtem o usuario pelo login do dominio
	 * 
	 * @param loginDominio
	 * @return usuario
	 */
	public Usuario obterUsuarioPorLoginDominio(String loginDominio) {
		return getControladorUsuario().obterUsuarioPorLoginDominio(loginDominio);
	}

	/**
	 * Obter lista questionario
	 * 
	 * @param questionaVo
	 * @return
	 * @throws NegocioException
	 */
	public Collection<Questionario> obterListaQuestionario(QuestionarioVO questionaVo) throws NegocioException {
		return serviceLocator.getControladorQuestionario().consultarQuestionario(questionaVo);
	}

	public ControladorChamadoTipo getControladorChamadoTipo() {
		return serviceLocator.getControladorChamadoTipo();
	}

	public ControladorChamadoAssunto getControladorChamadoAssunto() {
		return serviceLocator.getControladorChamadoAssunto();
	}

	/**
	 * Obter lista de anormalidadeConsumoParametro
	 * 
	 * @param anormalidadeConsumo {@link AnormalidadeConsumo}
	 * @return lista de anormalidadeConsumoParametro da anormalidadeConsumo {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<AnormalidadeSegmentoParametro> obterListaAnormalidadeSegmentoParametroPorAnormalidade(
			AnormalidadeConsumo anormalidadeConsumo) throws NegocioException {
		return serviceLocator.getControladorAnormalidade().listarAnormalidadeSegmentoParametros(anormalidadeConsumo);
	}
	
	/**
	 * Inserir AnormalidadeSegmentoParametro
	 * 
	 * @param anormalidadeSegmentoParametro {@link AnormalidadeSegmentoParametro}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void inserirAnormalidadeSegmentoParametro(AnormalidadeSegmentoParametro anormalidadeSegmentoParametro) throws NegocioException {
		serviceLocator.getControladorAnormalidade().inserir(anormalidadeSegmentoParametro);
	}

	/**
	 * Atualizar AnormalidadeSegmentoParametro
	 * 
	 * @param anormalidadeSegmentoParametro {@link AnormalidadeSegmentoParametro}
	 * @throws NegocioException {@link NegocioException}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 */
	public void atualizarAnormalidadeSegmentoParametro(AnormalidadeSegmentoParametro anormalidadeSegmentoParametro)
			throws NegocioException, ConcorrenciaException {
		serviceLocator.getControladorAnormalidade().atualizar(anormalidadeSegmentoParametro);
	}
	
	/**
	 * Remove AnormalidadeSegmentoParametro
	 * 
	 * @param anormalidadeSegmentoParametro {@link AnormalidadeSegmentoParametro}
	 * @throws NegocioException {@link NegocioException}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 */
	public void removerAnormalidadeSegmentoParametro(AnormalidadeSegmentoParametro anormalidadeSegmentoParametro)
			throws NegocioException, ConcorrenciaException {
		serviceLocator.getControladorAnormalidade().remover(anormalidadeSegmentoParametro);
	}
	
	/*
	 * Importar os Volumes e PCS de um CityGate.
	 *
	 * @param cityGate CityGate da planilha
	 * @param bytes O arquivo que será importado
	 * @param tipoArquivo O tipo do arquivo
	 * @param nomeArquivo the nome arquivo
	 * @param diaInicial
	 *            Dia Inicial pra importacao da planilha
	 * @param diaFinal
	 *            Dia Final pra importacao da planilha
	 * @param dataLeituraFaturada
	 *            Data de Leitura Faturada para verificar se atualiza ou nao a medicao
	 * @return Um mapa contendo os valores da
	 *         proposta
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PlanilhaVolumePcsVO importarVolPSCCityGateExcel(CityGate cityGate, byte[] bytes, String tipoArquivo, 
			String nomeArquivo, Integer diaInicial, Integer diaFinal, Date dataLeituraFaturada) throws GGASException {
		return this.getControladorCityGateMedicao().importarPlanilhaExcel(cityGate, bytes, tipoArquivo, nomeArquivo, 
				diaInicial, diaFinal, dataLeituraFaturada);
	}
	
	/**
	 * Inserir/atualizar os Volumes e PCS na tabela CITY_GATE_MEDICAO.
	 *
	 * @param listaCityGateMedicoes lista de medicoes pra serem salvas
	 * 
	 * @return boolean indica se todas atualizacoes foram feitas corretamente
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public void atualizarImportacaoPlanilhaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicoes)
			throws NegocioException {
		this.getControladorCityGateMedicao().atualizarImportacaoPlanilhaCityGateMedicao(listaCityGateMedicoes);
	}

	/**
	 * Obtem uma lista de medidores que compoem um medidor virtual;
	 * 
	 * @param composicaoVirtual composicao do medidor virtual em conjunto com suas operacoes
	 * @return List<MedidorVirtualVO>
	 */
	public List<MedidorVirtualVO> obterComposicaoMedidorVirtual(String composicaoVirtual) {
		return serviceLocator.getControladorMedidor().obterComposicaoMedidorVirtual(composicaoVirtual);
	}	
	
	/**
	 * Obtem o medidor atraves do ponto de consumo
	 * 
	 * @param idPontoConsumo chave primaria do ponto de consumo
	 * @return Medidor
	 */
	public Medidor obterMedidorPorPontoConsumo(Long idPontoConsumo) {
		return serviceLocator.getControladorPontoConsumo().obterMedidorPorPontoConsumo(idPontoConsumo);
	}
	
	/**
	 * Realiza o agrupamento do historico de consumo por medidores independentes
	 *
	 * @param listaHistoricoConsumo
	 *
	 * @return map com listas agrupadas por cada medidor independente
	 * @throws NegocioException 
	 */
	public Map<Long, Collection<HistoricoConsumo>> agruparHistoricoConsumoPorMedidorIndependente(
			Collection<HistoricoConsumo> listaHistoricoConsumo)  throws NegocioException{
		return serviceLocator.getControladorHistoricoConsumo().agruparHistoricoConsumoPorMedidorIndependente(listaHistoricoConsumo);
	}


	/**
	 * Gets the controlador agente.
	 *
	 * @return the controlador agente
	 */
	private ControladorRelatorioConsumoClienteMedidor getControladorRelatorioConsumoClienteMedidor() {

		return (ControladorRelatorioConsumoClienteMedidor)serviceLocator.getBeanPorID(
				ControladorRelatorioConsumoClienteMedidor.BEAN_ID_CONTROLADOR_RELATORIO_CONSUMO);
	}
	/**
	 * Gera o relaorio do historico de consumo por medidores independentes
	 *
	 * @param filtro Filtros utilizados
	 * @param formatoImpressao Formato de impressao desejado
	 *
	 * @return arquivo em bytes do relatorio
	 * @throws GGASException - {@link GGASException}
	 */
	public byte[] gerarRelatorioConsumoMedidoresIndependentes(
			Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException{
		return getControladorRelatorioConsumoClienteMedidor().gerarRelatorio(filtro, formatoImpressao);
	}
	
	/**
	 * Tras a entidade conteudo atras da descricao
	 * @param descricao utilizado descricao do status
	 *
	 * @return a entidade
	 * @throws NegocioException - {@link NegocioException}
	 */
	public EntidadeConteudo obterEntidadeConteudoLikeDescricao(String descricao) throws NegocioException {
		return getControladorEntidadeConteudo().obterEntidadeConteudoLikeDescricao(descricao);
	}
	/**
	 * Obtem o historico de consumo detalhado por medididores independentes
	 *
	 *@param idPontoConsumo - {@link Long}
	 *@param anoMesFaturamento - {@link Integer}
	 *@param numeroCiclo - {@link Integer}
	 *
	 *@return colecao com historico de consumo detalhado por medidor independente
	 *@throws GGASException - {@link GGASException}
	 */
	public Collection<HistoricoConsumo> obterHistoricoConsumoPorMedidoresIndependentes(
			Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo) throws GGASException {
		return getControladorControladorHistoricoConsumo()
				.obterHistoricoConsumoPorMedidoresIndependentes(idPontoConsumo, anoMesFaturamento, numeroCiclo);
	}

	/**
	 * Consultar detalhes da Operacao
	 * @param idOperacao {@link idOperacao}
	 * @return Operacao {@link Operacao}
	 * @throws GGASException {@link GGASException}
	 */
	public Operacao buscarOperacaoPorChave(long idOperacao) throws GGASException {

		return this.getControladorModulo().buscarOperacaoPorChave(idOperacao);
	}
	
	/**
	 * Listar Status da NFE
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<EntidadeConteudo> listarStatusNFEDocumentoFiscal() throws NegocioException {

		return this.getControladorEntidadeConteudo().listarStatusNFEDocumentoFiscal();
	}

	/**
	 * Método responsável por consultar as quadras pelo cep passado por parâmetro.
	 *
	 * @param idCep O número do cep.
	 * @return Um mapa de String
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Collection<Quadra> listarQuadraPorIdCep( Long idCep) throws GGASException {

		return getControladorQuadra().listarQuadraPorIdCep(idCep);
	}
	/**
	 * Obter quadra face por quadra.
	 *
	 * @param idQuadra
	 *            the id quadra
	 * @param idCep
	 *            the cep
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<QuadraFace> obterQuadraFacePorQuadraId(final Long idQuadra, final String idCep)
			throws NegocioException {

		return getControladorQuadra().obterQuadraFacePorQuadraIdCep(idQuadra, idCep);
	}
	
	/**
	 * Realiza a montagem e envio do email com o histórico do chamado
	 * @param idChamado  chave primária do chamado
	 * @throws GGASException {@link GGASException}
	 */
	public void enviarEmailHistoricoChamado(Long idChamado) throws GGASException {		
		serviceLocator.getControladorChamado().enviarEmailHistoricoChamado(idChamado);
	}

	/**
	 * Consulta o id do arrecadadorContratoConvenio a partir de um contrato
	 *
	 * @param idContrato O id do contrato
	 * @return O id do arrecadadorContratoConvenio
	 */
	public Long consultaridArrecadadorContratoConvenio(Long idContrato) {

		return this.getControladorContrato().consultarIdArrecadadorContratoConvenio(idContrato);
	}

	/**
	 * Consulta o banco a partir do id do contrato
	 *
	 * @param idContrato O id do contrato
	 * @return o id do banco referente ao contrato
	 */
	public Banco consultarBancoContrato(Long idContrato) {

		return this.getControladorContrato().consultarBancoContrato(idContrato);
	}
	
	/**
	 * Validar migrar contrato.
	 *
	 * @param chaveContrato the chave contrato
	 * @throws GGASException the GGAS exception
	 */
	public void validarMigracaoModeloContrato(Long chaveContrato) throws GGASException {
		this.getControladorContrato().validarMigracaoModeloContrato(chaveContrato);
	}

	/**
	 * obter Cliente Por Cpf ou Cnpj
	 *
	 * @param cpfCnpj O cpfCnpj
	 * @return o Cliente the Cliente
	 * @throws NegocioException
	 *             the NegocioException
	 */
	public Cliente obterClientePorCpfCnpj(String cpfCnpj) throws NegocioException {
		return getControladorCliente().obterClientePorCpfCnpj(cpfCnpj);
	}

	/**
	 * Obter serie por segmento.
	 *
	 * @param segmento the segmento
	 * @return the serie
	 */
	public Serie obterSeriePorSegmentoID(Segmento segmento) {
		return getControladorSerie().obterSeriePorSegmentoID(segmento);
	}
	
	public Collection<EquipeTurnoItem> obterListaEquipeTurnoItem(int mes, int ano, Long chaveEquipe) {
		return getControladorEquipeTurno().obterListaEquipeTurnoItem(mes, ano, chaveEquipe);
	}
	/**
	 * Obter equipe turno item por chave equipe.
	 *
	 * @param chaveEquipe the chaveEquipe
	 * @return the FuncionarioEquipeVO
	 * @throws NegocioException 
	 */
	public List<FuncionarioEquipeVO> obterEquipeTurnoItemPorChaveEquipe(Long chaveEquipe) throws NegocioException {
		return getControladorEquipeTurno().obterEquipeTurnoItemPorChaveEquipe(chaveEquipe);
	}
	
	/**
	 * Listar ramo atividade.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<RamoAtividade> listarRamoAtividade() throws NegocioException {

		return this.getControladorRamoAtividade().listarRamoAtividade();
	}
	
	/**
	 * 
	 * @param descricao 
	 * @return
	 * @throws NegocioException
	 */
	public Equipe consultarEquipePorDescricao(String descricao) throws NegocioException {
		return this.getControladorEquipe().consultarEquipePorDescricao(descricao);
	}
	
	
	public EntidadeNegocio criarServicoAutorizacaoTemp() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ServicoAutorizacaoTemp.BEAN_ID_SERVICO_AUTORIZACAO_TEMP);
	}
	
	
	/**
	 * Método responsável por obter uma entidade.
	 *
	 * @param idServicoAutorizacaoTemp the id idServicoAutorizacaoTemp
	 * @return EntidadeBase
	 * @throws NegocioException Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	public ServicoAutorizacaoTemp obterServicoAutorizacaoTemp(Long idServicoAutorizacaoTemp) throws NegocioException {

		return this.getControladorServicoAutorizacao().obterServicoAutorizacaoTemp(idServicoAutorizacaoTemp);
	}

	public MedidorLocalInstalacao obterMedidorLocalInstalacao(long chavePrimaria) throws NegocioException {
		return this.getControladorMedidor().obterMedidorLocalInstalacao(chavePrimaria);
	}

	public MedidorProtecao obterMedidorProtecao(long chavePrimaria) throws NegocioException{
		return this.getControladorMedidor().obterMedidorProtecao(chavePrimaria);
	}
	
	@SuppressWarnings("unchecked")
	public MotivoOperacaoMedidor obterMotivoOperacaoMedidor(long chavePrimaria) throws NegocioException {
		return this.getControladorMedidor().obterMotivoOperacaoMedidor(chavePrimaria);
	}
	
	public RedeComprimento criarRedeComprimento() throws NegocioException {

		return (RedeComprimento) this.getControladorRede().criarRedeComprimento();
	}
	
	
	/**
	 * Método responsável por obter o controlador relatorio
	 *
	 * @return O controlador de relatorio
	 */
	private ControladorRelatorio getControladorRelatorio() {

		return (ControladorRelatorio) serviceLocator
						.getControladorNegocio(ControladorRelatorio.BEAN_ID_CONTROLADOR_RELATORIO);
	}
}
