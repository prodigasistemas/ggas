package br.com.ggas.util;

/**
 * 
 * @author voliveira
 *
 * @param <T>
 */
public interface Mapeavel<T> {

	/**
	 * Metodo que define o objeto por qual a classe sera mapeada.
	 * 
	 * @return o objeto por qual a classe será mapeada
	 */
	T getChaveMapeadora();
	
}
