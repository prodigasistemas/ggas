package br.com.ggas.util;

import java.util.function.Supplier;

/**
 * Modificação de um {@link Supplier} que permite informar um throws.
 * 
 * @author orube
 *
 * @param <T> - Retorno da Função
 * @param <E> - Exceção lançada pela função
 */
@FunctionalInterface
public interface CheckedSupplier<T, E extends Exception> {
	/**
     * Obtém um resultado. Caso um erro ocorra, uma exceção é lançada.
     *
     * @return resultado
     * @throws E - {@link Exception}
     */
	public T get() throws E;
}
