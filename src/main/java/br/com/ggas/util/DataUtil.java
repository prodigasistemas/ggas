
package br.com.ggas.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * The Class DataUtil.
 */
public class DataUtil {
	
	private static final int ULTIMO_MILISSEGUNDO_SEGUNDO = 999;

	private static final int ULTIMO_MINUTO_SEGUNDO = 59;

	private static final int ULTIMO_MINUTO_HORA = 59;

	private static final int ULTIMA_HORA_DIA = 23;

	private static final Logger LOG = Logger.getLogger(DataUtil.class);

	/** The Constant NOMES_DIAS_DA_SEMANA_ORDENADOS. */
	private static final String[] NOMES_DIAS_DA_SEMANA_ORDENADOS = 
					new String[] {"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"};

	/**
	 * Menor que uma das datas.
	 *
	 * @param menor the menor
	 * @param maiores the maiores
	 * @return true, if successful
	 */
	public static boolean menorQueUmaDasDatas(Date menor, Date... maiores) {

		if (menor != null) {
			for (Date maior : maiores) {
				if (maior != null && menor.compareTo(maior) < 0) {
					return true;
				}
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;
	}

	/**
	 * Menor que.
	 *
	 * @param menor the menor
	 * @param maior the maior
	 * @return true, if successful
	 */
	public static boolean menorQue(Date menor, Date maior) {

		if (menor != null && maior != null) {
			if (menor.compareTo(maior) < 0) {
				return true;
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;
	}
	

	/**
	 * Menor ou igual que.
	 *
	 * @param menor the menor
	 * @param maior the maior
	 * @return true, if successful
	 */
	public static boolean menorOuIgualQue(Date menor, Date maior) {
		return menorOuIgualQue(menor, maior, false);
	}

	/**
	 * Menor ou igual que.
	 *
	 * @param menor the menor
	 * @param maior the maior
	 * @param ignorarHora the ignorar hora
	 * @return true, if successful
	 */
	public static boolean menorOuIgualQue(Date menor, Date maior, boolean ignorarHora) {
		
		Date menorComparacao = menor;
		Date maiorComparacao = maior;
		
		if (ignorarHora) {
			menorComparacao = gerarDataHmsZerados(menorComparacao);
			maiorComparacao = gerarDataHmsZerados(maiorComparacao);
		}

		if (menorComparacao != null && maiorComparacao != null) {
			if (menorComparacao.compareTo(maiorComparacao) <= 0) {
				return true;
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;
	}

	/**
	 * Maior que.
	 *
	 * @param maior the maior
	 * @param menor the menor
	 * @return true, if successful
	 */
	public static boolean maiorQue(Date maior, Date menor) {

		if (maior != null && menor != null) {
			if (maior.compareTo(menor) > 0) {
				return true;
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;
	}

	/**
	 * Ate hoje.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public static boolean ateHoje(Date data) {

		if (data != null) {
			Date hoje = gerarDataHmsZerados(new Date());
			Date dataSemHora = gerarDataHmsZerados(data);
			return dataSemHora.compareTo(hoje) <= 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Antes hoje.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public static boolean antesHoje(Date data) {

		if (data != null) {
			Date hoje = gerarDataHmsZerados(new Date());
			Date dataSemHora = gerarDataHmsZerados(data);
			return dataSemHora.compareTo(hoje) < 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Depois hoje.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public static boolean depoisHoje(Date data) {

		if (data != null) {
			Date hoje = gerarDataHmsZerados(new Date());
			Date dataSemHora = gerarDataHmsZerados(data);
			return dataSemHora.compareTo(hoje) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Método responsável por gerar um date time
	 * com horas, minutos e segundos zerados, ouseja,
	 * o primeiro momento do dia.
	 *
	 * @param data a data.
	 * @return Um date time sem a hora.
	 */
	private static DateTime gerarDateTimeHmsZerados(Date data) {

		DateTime dataSemHora = new DateTime(data);
		dataSemHora = dataSemHora.withHourOfDay(0);
		dataSemHora = dataSemHora.withMinuteOfHour(0);
		dataSemHora = dataSemHora.withSecondOfMinute(0);
		dataSemHora = dataSemHora.withMillisOfSecond(0);
		return dataSemHora;
	}
	
	/**
	 * Método responsável por gerar um date time
	 * com horas, minutos e segundos completos, ouseja,
	 * o ultimo momento do dia.
	 *
	 * @param data the data
	 * @return the date time
	 */
	private static DateTime gerarDateTimeHmsCheios(Date data) {

		DateTime dataSemHora = new DateTime(data);
		dataSemHora = dataSemHora.withHourOfDay(ULTIMA_HORA_DIA);
		dataSemHora = dataSemHora.withMinuteOfHour(ULTIMO_MINUTO_HORA);
		dataSemHora = dataSemHora.withSecondOfMinute(ULTIMO_MINUTO_SEGUNDO);
		dataSemHora = dataSemHora.withMillisOfSecond(ULTIMO_MILISSEGUNDO_SEGUNDO);
		return dataSemHora;
	}

	/**
	 * Metodo responsavel por gerar uma data com
	 * horas, minutos e segundos zerados, ou seja, 
	 * o primeiro momento do dia.
	 *
	 * @param data
	 *            A data.
	 * @return Uma data sem a hora.
	 */
	public static Date gerarDataHmsZerados(Date data) {

		return gerarDateTimeHmsZerados(data).toDate();
	}
	
	/**
	 * Metodo responsavel por gerar uma data com
	 * horas, minutos e segundos zerados, ou seja, 
	 * o ultimo momento do dia.
	 *
	 * @param data the data
	 * @return the date
	 */
	public static Date gerarDataHmsCheios(Date data) {

		return gerarDateTimeHmsCheios(data).toDate();
	}

	/**
	 * Método responsável por gerar uma data sem
	 * hora.
	 *
	 * @param data
	 *            A data.
	 * @return Uma data sem a hora.
	 */
	public static Date gerarDataSemHoraPrimeiroDiaMes(Date data) {

		return gerarDateTimeHmsZerados(data).withDayOfMonth(1).toDate();
	}

	/**
	 * Metodo responsavel por converter um string para uma data.
	 *
	 * @param strData O valor
	 * @return Uma data
	 * @throws FormatoInvalidoException Caso ocorra algum erro de convsersão ou formato
	 */
	public static Date parse(String strData) throws FormatoInvalidoException {

		if (StringUtils.isBlank(strData)) {
			return null;
		}
		
		try {
			SimpleDateFormat formatador = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			return formatador.parse(strData);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, strData);
		}

	}

	/**
	 * Método responsável por incrementar uma dia.
	 *
	 * @param data
	 *            A data que será incrementada
	 * @param i
	 *            O número de dias
	 * @return Uma data
	 */
	public static Date decrementarDia(Date data, int i) {

		return decrementarDiaMes(data, i, 0);

	}

	/**
	 * Método responsável por incrementar uma dia.
	 *
	 *@param decrementoDia - {@link int}
	 *@param decrementoMes - {@link int}
	 *
	 * @param data
	 *            A data que será incrementada
	 * @param i
	 *            O número de dias
	 * @return Uma data
	 */
	public static Date decrementarDiaMes(Date data, int decrementoDia, int decrementoMes) {

		DateTime dateTime = new DateTime(data);
		return dateTime.minusDays(decrementoDia).minusMonths(decrementoMes).toDate();
	}

	/**
	 * Método responsável por incrementar uma dia.
	 *
	 * @param data
	 *            A data que será incrementada
	 * @param i
	 *            O número de dias
	 * @return Uma data
	 */
	public static Date incrementarDia(Date data, int i) {

		return incrementarDiaMes(data, i, 0);
	}

	/**
	 * Método responsável por incrementar dia e mes.
	 *
	 * @param data
	 *            A data que será incrementada
	 * @param incrementoDia
	 *            O número de dias
	 * @param incrementoMes
	 *            O número de meses
	 * @return a data apos o incremento
	 */
	public static Date incrementarDiaMes(Date data, int incrementoDia, int incrementoMes) {

		DateTime dateTime = new DateTime(data);
		return dateTime.plusDays(incrementoDia).plusMonths(incrementoMes).toDate();
	}

	/**
	 * Calcula diferença de dias entre datas
	 * 
	 * @param data1 primeira data do intervalo
	 * @param data2 segunda data do intervalo
	 * @return int dias
	 */
	public static Integer diferencaDiasEntreDatas(Date data1, Date data2) {

		if (data1 != null && data2 != null) {
			return Days.daysBetween(new DateTime(data1), new DateTime(data2)).getDays();
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
	}

	/**
	 * Calcula diferença de meses entre datas
	 * 
	 * @param data1 primeira data do intervalo
	 * @param data2 segunda data do intervalo
	 * @return int months
	 */
	public static Integer diferencaMesesEntreDatas(Date data1, Date data2) {

		if (data1 != null && data2 != null) {
			return Months.monthsBetween(new DateTime(data1), new DateTime(data2)).getMonths();
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
	}

	/**
	 * Converte a data passada por parâmetro para
	 * data no formato String.
	 *
	 * @param data - {@link Date}
	 * @return data no formato de String
	 */
	public static String converterDataParaString(Date data) {

		return converterDataParaString(data, false);
	}

	/**
	 * Converte o localdatetime informado para data no formato DD/MM/YYYY
	 * @param data localdatetime
	 * @return retorna a string correspondente
	 */
	public static String converterDataParaString(LocalDateTime data) {
		return converterDataParaString(toDate(data));
	}

	/**
	 * Converte a data passada por parâmetro para
	 * uma data no formato String e também define se a informacao
	 * de horario tambem deve ser impressa.
	 *
	 * @param data data a ser impressa
	 * @param imprimirHora define se a hora minuto e segundo
	 *            tambem sera impressa
	 * @return String - retorna a data convertida em String
	 */
	public static String converterDataParaString(Date data, boolean imprimirHora) {

		String formato = null;
		if (imprimirHora) {
			formato = Constantes.FORMATO_DATA_HORA_BR;
		} else {
			formato = Constantes.FORMATO_DATA_BR;
		}
		DateFormat df = new SimpleDateFormat(formato);
		return df.format(data);
	}
	
	public static String converterDataParaStringSemSegundos(Date data) {
		String formato = null;
		formato = Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR;
		DateFormat df = new SimpleDateFormat(formato);
		return df.format(data);
	}


	/**
	 * Converter para data.
	 *
	 * @param strData the str data
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	public static Date converterParaData(String strData) throws NegocioException {

		SimpleDateFormat formatador = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		try {
			return formatador.parse(strData);
		} catch (ParseException e) {
			throw new NegocioException();
		}
	}

	/**
	 * Converter campo data para string com dia da semana.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String converterCampoDataParaStringComDiaDaSemana(Date data)  {

		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		int day = cal.get(Calendar.DAY_OF_WEEK);

		StringBuilder strData = new StringBuilder();
		strData.append(NOMES_DIAS_DA_SEMANA_ORDENADOS[day - 1]);
		strData.append(Constantes.STRING_ESPACO);
		strData.append(converterDataParaString(data));

		return strData.toString();
	}

	/**
	 * Gerar etiqueta data atual.
	 *
	 * @return the string
	 */
	public static String gerarEtiquetaDataAtual() {

		return gerarEtiquetaDataAtual(false);
	}
	
	/**
	 * Gerar data atual.
	 *
	 * @return the string
	 */
	public static String gerarDataAtual() {

		return gerarEtiquetaDataAtual().substring(1, 11);
	}

	/**
	 * Gerar etiqueta data hora atual.
	 *
	 * @return the string
	 */
	public static String gerarEtiquetaDataHoraAtual() {

		return gerarEtiquetaDataAtual(true);
	}

	/**
	 * Gerar etiqueta data atual.
	 *
	 * @param imprimirHora the imprimir hora
	 * @return the string
	 */
	public static String gerarEtiquetaDataAtual(boolean imprimirHora) {

		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(converterDataParaString(new Date(), imprimirHora));
		sb.append("]");
		return sb.toString();
	}
	
	/**
	 * Validar data.
	 *
	 * @param strData the str data
	 * @param chaveDataVazia the chave data vazia
	 * @param chaveDataInvalida the chave data invalida
	 * @return the pair
	 */
	public static Pair<Date, String> validarData(String strData, 
					String chaveDataVazia, String chaveDataInvalida) {
		
		Date data;
		if (StringUtils.isEmpty(strData)) {
			String obterMensagem = MensagemUtil.obterMensagem(chaveDataVazia);
			return new Pair<Date, String>(null, obterMensagem);
		} else {
			try {
				data = DataUtil.parse(strData);
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				String obterMensagem = MensagemUtil.obterMensagem(chaveDataInvalida);
				return new Pair<Date, String>(null, obterMensagem);
			}
		}
		
		return new Pair<Date, String>(data, null);
		
	}
	
	/**
	 * Valida data no formato String
	 * 
	 * @param strData {@link String}
	 * @return boolean {@link boolean}
	 */
	public static boolean validarData(String strData) {
		try {
			return null != DataUtil.parse(strData);
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Validar intervalo.
	 *
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the string
	 */
	public static String validarIntervalo(Date dataInicial, Date dataFinal) {
		
		if (Util.isAllNotNull(dataInicial, dataFinal) && maiorQue(dataInicial, dataFinal)) {
			return MensagemUtil.obterMensagem(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL);
		}
		
		return null;
		
	}
	
	/**
	 * Obtem quantidades de dias de um determinado mes
	 *
	 * @param anoMes - {@link Integer}
	 * @return the Integer
	 */
	public static Integer quantidadeDiasMes(Integer anoMes) {
		
		Date data = converterAnoMesEmData(anoMes);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Converte uma String no formato anoMes em
	 * data.
	 *
	 * @param anoMes the ano mes
	 * @return the string
	 * @throws GGASException 
	 */
	public static Date converterAnoMesEmData(Integer anoMes){
		
		Date retorno = null;
		
		String anoMesString = anoMes.toString();
		String dataString = "";

		if (!StringUtils.isEmpty(anoMesString)) {
			String ano;
			String mes;
			String dia = "01";						

			ano = anoMesString.substring(0, 4);
			mes = anoMesString.substring(4, 6);
			
			dataString = dia + "/" + mes+ "/" +ano;
	
			try {
				retorno = Util.converterCampoStringParaData("converterAnoMesEmData", dataString, Constantes.FORMATO_DATA_BR);
			} catch (GGASException e) {
				LOG.error(e);				
			}
		}

		return retorno;
	}
	
	/**
	 * Cria um {@link GregorianCalendar}, com a data passada por parâmetro, atribuindo o valor mínimo aos campos de hora, minuto, segundo e
	 * milissegundo, retornando a data resultante.
	 * 
	 * @param data - {@link Date}
	 * @return data com horas mínimas - {@link Date}
	 */
	public static Date minimizarHorario(Date data) {
		Date dataZerada = null;
		if (data != null) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(data);
			calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
			calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
			calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
			calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
			dataZerada = calendar.getTime();
		}
		return dataZerada;
	}

	/**
	 * Converte um localDateTime para seu correspondente em date
	 * @param localDateTime {@link LocalDateTime} que será convertido
	 * @return date equivalente
	 */
	public static Date toDate(LocalDateTime localDateTime) {

		Date resultado = null;

		if (localDateTime != null) {
			resultado = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		}

		return resultado;
	}

	/**
	 * Converte um {@link LocalDate} para seu correspondente em date
	 * O horário da data é configurado para meia noite e é utilizado o ZodeID especificado na JVM do servidor
	 * @param localDate local date
	 * @return retorna uma nova instancia de date correspondente
	 */
	public static Date toDate(LocalDate localDate) {
		Date resultado = null;

		if (localDate != null) {
			resultado = Date.from(localDate.atTime(0, 0).atZone(ZoneId.systemDefault()).toInstant());
		}

		return resultado;
	}

	/**
	 * Converte um {@link Date} para {@link LocalDateTime}
	 * @param data data a ser convertida
	 * @return data convertida
	 */
	public static LocalDateTime toLocalDateTime(Date data) {
		return Optional.ofNullable(data).map(d ->

			LocalDateTime.ofInstant(d.toInstant(), ZoneId.systemDefault())

		).orElse(null);
	}

	/**
	 * Altera o padrão de formatacao de uma data fornecida 
	 * 
	 * @param strData data em String
	 * @param patternEntrada formato da String de entrada
	 * @param partternSaida formato da String de saida
	 * @return Data formatada
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 */
	public static String formatarDataString(String strData, String patternEntrada, String partternSaida) throws FormatoInvalidoException {
		if (StringUtils.isBlank(strData)) {
			return null;
		}

		try {
			SimpleDateFormat formatadorEntrada = new SimpleDateFormat(patternEntrada);
			Date data = formatadorEntrada.parse(strData);

			SimpleDateFormat formatadorSaida = new SimpleDateFormat(partternSaida);
			return formatadorSaida.format(data);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, strData);
		}
	}

	/**
	 * Retorna a hora de uma data
	 * @param dataHora - {@link Date}
	 * @return A hora da data 
	 */
	public static Integer getHora(Date dataHora) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(dataHora); 
		return calendar.get(Calendar.HOUR_OF_DAY);
	}
	
	/**
	 * Retorna o dia do mes de uma data
	 * @param data - {@link Date}
	 * @return O dia do mes 
	 */
	public static Integer getDia(Date data) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(data); 
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Zerar horario de uma data fornecida 
	 * 
	 * @param dataCompleta - {@link Date}
	 * @return Data com horario zerado
	 * @throws ParseException {@link ParseException}
	 */
	public static Date zerarHoraParaData(Date dataCompleta){
		if(dataCompleta != null){
			SimpleDateFormat formatter = new SimpleDateFormat("yy/MM/dd 00:00:00.0");
			Date date = null;
			try {
				date =  formatter.parse(formatter.format(dataCompleta));
			} catch (ParseException e) {
				LOG.error(e);	
			}
	        return date;
		}
		return null;
	}
	
	/**
	 * Converter Date em DateTime 
	 * 
	 * @param date em Date
	 * @return Data com format DateTime
	 */
	public static DateTime converterDateEmDateTime(Date date) {
		if(date != null){
			return new DateTime(date);
		}
		return null;
	}

	/**
	 * Metodo responsavel por exibir o a data e hora atual
	 * 
	 * @return Data e hora atual.
	 */
	public static Date gerarDataHoje() {
		return new Date(System.currentTimeMillis());
	}
	
	
	public static String obterDiaSemana(Date data) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", new Locale("pt", "BR")); 
		
		return dateFormat.format(data);
	}
	

	public static Integer obterMes(Date data) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		return calendar.get(Calendar.MONTH);
	}
	
	public static Date obterPrimeiroDiaProximoMes(String anoMesReferencia) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesReferencia.substring(3, 7)),
				Integer.parseInt(anoMesReferencia.substring(0, 2)), 1);

		return calendar.getTime();
	}
	
	public static Date obterPrimeiroDiaProximosDoisMeses(String anoMesReferencia) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesReferencia.substring(3, 7)),
				Integer.parseInt(anoMesReferencia.substring(0, 2)),1);
		
		calendar.add(Calendar.MONTH,1);

		return calendar.getTime();
	}
	
	public static Date obterPrimeiroDiaAno(String anoMesReferencia) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesReferencia.substring(3, 7)), 0, 1);
		return calendar.getTime();
	}
	
	public static Date obterPrimeiroDiaMes(String anoMesReferencia) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(anoMesReferencia.substring(3, 7)),
				Integer.parseInt(anoMesReferencia.substring(0, 2)) - 1, 1);
		return calendar.getTime();
	}
	
	public static String obterNomeMesPeloNumero(Integer numeroMes) {
		return Month.of(numeroMes).getDisplayName(TextStyle.FULL, Constantes.LOCALE_PADRAO);
	}
	
	/**
	 * Converter para data hora.
	 *
	 * @param strData the str data
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	public static Date converterParaDataComHora(String strData) throws NegocioException {

		SimpleDateFormat formatador = new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
		try {
			return formatador.parse(strData);
		} catch (ParseException e) {
			throw new NegocioException();
		}
	}
	
	public static Long obterDiferencaMinutosEntreDatas(Date dataInicial, Date dataFinal) {
		return (dataFinal.getTime() - dataInicial.getTime()) /  (60 * 1000);
	}
	
	
	/**
	 * Menor ou igual que.
	 *
	 * @param menor the menor
	 * @param maior the maior
	 * @param ignorarHora the ignorar hora
	 * @return true, if successful
	 */
	public static boolean maiorOuIgualQue(Date menor, Date maior, boolean ignorarHora) {
		
		if (ignorarHora) {
			menor = gerarDataHmsZerados(menor);
			maior = gerarDataHmsZerados(maior);
		}
		
		if (maior != null && menor != null) {
			if (maior.compareTo(menor) >= 0) {
				return true;
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;

	}
	
	/**
	 * Maior que.
	 *
	 * @param maior the maior
	 * @param menor the menor
	 * @return true, if successful
	 */
	public static boolean maiorQue(Date maior, Date menor, boolean ignorarHora) {
		
		if (ignorarHora) {
			menor = gerarDataHmsZerados(menor);
			maior = gerarDataHmsZerados(maior);
		}

		if (maior != null && menor != null) {
			if (maior.compareTo(menor) > 0) {
				return true;
			}
		} else {
			throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
		}
		return false;
	}

	public static Date arredondarMinutosHora(Date dataHoraExecucao) {
		Calendar c = Calendar.getInstance();
		c.setTime(dataHoraExecucao);
		
		BigDecimal minutos = new BigDecimal(c.get(Calendar.MINUTE)).divide(new BigDecimal(10), 0, RoundingMode.HALF_UP)
				.multiply(new BigDecimal(10));		
		
		if(minutos.compareTo(new BigDecimal(60)) >= 0) {
			c.add(Calendar.HOUR_OF_DAY, 1);
			c.set(Calendar.MINUTE, 0);
		} else {
			c.set(Calendar.MINUTE, minutos.intValue());
		}
		
		return c.getTime();
	}

	public static Boolean verificarLogoffMobile(Date date, Long periodicidadeLocalizacao) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MILLISECOND, periodicidadeLocalizacao.intValue());
		Boolean resultado = DataUtil.maiorQue(new Date(), c.getTime());		
		return DataUtil.maiorQue(new Date(), c.getTime());
		
	}
	
	public static LocalTime obterHoraMinutoData(Date data) {
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        return LocalTime.of(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
	}

	public static Boolean compararHoras(String dataCompleta, String horaCompleta) {
        DateTimeFormatter formatoDataHora = DateTimeFormatter.ofPattern(Constantes.FORMATO_DATA_HORA_BR);
        LocalDateTime dateTime1 = LocalDateTime.parse(dataCompleta, formatoDataHora);
        
        DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time2 = LocalTime.parse(horaCompleta, formatoHora);
        
        return dateTime1.toLocalTime().isAfter(time2);
	}

	
	public static String setarDataCompleta(String dataCompleta, String horaCompleta, Boolean isAdicionarDia) throws NegocioException {
		String horaMinuto[] = horaCompleta.split(":");
		Calendar c = Calendar.getInstance();
		c.setTime(DataUtil.converterParaData(dataCompleta));
		c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaMinuto[0]));
		c.set(Calendar.MINUTE, Integer.valueOf(horaMinuto[1]));
		
		if(isAdicionarDia) {
			c.add(Calendar.DAY_OF_YEAR, 1);
		}
		
		return DataUtil.converterDataParaStringSemSegundos(c.getTime());
	}
	
}
