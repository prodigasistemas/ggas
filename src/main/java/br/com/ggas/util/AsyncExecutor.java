package br.com.ggas.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import br.com.ggas.faturamento.exception.ClienteIntegracaoNaoConfiguradoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Classe responsável pela execução de funções assíncronas utiliando o @Async do
 * Spring.
 * 
 * @author orube
 *
 */
@Component(value = "asyncExecutor")
public class AsyncExecutor {

	private static final Logger LOG = Logger.getLogger(AsyncExecutor.class);

	private static final Integer QUEUE_CAPACITY = 500;

	/**
	 * Método que recebe uma função que será executada de forma assíncrona. A função
	 * deve retornar um Future
	 *
	 * @param <T>  - Tipo de Retorno da Função Assíncrona
	 * @param func - {@link CheckedSupplier}
	 * @return Retorno da Função Assíncrona
	 * @throws GGASException - {@link GGASException}
	 */
	@Async
	public <T> Future<T> execute(CheckedSupplier<T, GGASException> func) throws GGASException {
		return new AsyncResult<T>(func.get());
	}

	/**
	 * Método responsável por executar funções assíncronas.
	 * @param <T> - Tipo de Retorno da Função Assíncrona
	 * @param asyncExecutor - {@link AsyncExecutor}
	 * @param functionsList - {@link List}
	 * @return retorno das funções assíncronas - {@link List}
	 * @throws GGASException - {@link GGASException}
	 */
	public static <T> List<T> execute(AsyncExecutor asyncExecutor,
			List<CheckedSupplier<T, GGASException>> functionsList) throws GGASException {
		List<T> resultList = new ArrayList<>();

		while (functionsList.size() > QUEUE_CAPACITY) {
			List<CheckedSupplier<T, GGASException>> subList = functionsList.subList(0, QUEUE_CAPACITY);

			processAsyncResult(asyncExecutor, resultList, subList);

			functionsList.subList(0, QUEUE_CAPACITY).clear();
		}

		if (!functionsList.isEmpty()) {
			processAsyncResult(asyncExecutor, resultList, functionsList);
		}

		return resultList;
	}

	private static <T> void processAsyncResult(AsyncExecutor asyncExecutor, List<T> resultList,
			List<CheckedSupplier<T, GGASException>> functionsList) throws GGASException {
		List<Future<T>> asyncResultList = new ArrayList<>();

		for (CheckedSupplier<T, GGASException> func : functionsList) {
			asyncResultList.add(asyncExecutor.execute(func));
		}

		try {
			for (Future<T> asyncResult : asyncResultList) {
				resultList.add(asyncExecutor.get(asyncResult));
			}
		} catch (Exception e) {
			throw new GGASException(e.getMessage(), e);
		}

	}

	/**
	 * Método responsável por tratar a exceção que ocorre no método assíncrono
	 * 
	 * @param <T>    - Tipo do retorno do método assíncrono
	 * @param result - {@link Future}
	 * @return retorno do método assíncrono - T
	 * @throws Exception - {@link Exception}
	 */
	public <T> T get(Future<T> result) throws Exception {

		T value = null;
		try {
			value = result.get();
		} catch (ExecutionException e) {
			LOG.error(e.getMessage(), e);
			Throwable cause = e.getCause();
			if (cause instanceof ClienteIntegracaoNaoConfiguradoException) {
				throw (ClienteIntegracaoNaoConfiguradoException) cause;
			} else if (cause instanceof NegocioException) {
				throw (NegocioException) cause;
			} else if (cause instanceof Exception) {
				throw (Exception) cause;
			}
		}
		return value;
	}
}
