/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;

/**
 * Classe responsável por enviar email a chamados atrasados
 *
 */
public class ChamadosAtrasadosBatch {

	public static final String BEAN_ID_CHAMADOS_ATRASADOS = "chamadosAtrasadosBatch";

	private static final Logger LOG = Logger.getLogger(ChamadosAtrasadosBatch.class);

	private final ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	private ControladorParametroSistema controladorParametroSistema;

	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	private ControladorChamado controladorChamado;

	/**
	 * Construtor padrão.
	 */
	public ChamadosAtrasadosBatch() {

		controladorParametroSistema = (ControladorParametroSistema) serviceLocator
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		controladorUnidadeOrganizacional = (ControladorUnidadeOrganizacional) serviceLocator
				.getControladorNegocio(ControladorUnidadeOrganizacional.BEAN_ID_CONTROLADOR_UNIDADE_ORGANIZACIONAL);
		controladorChamado = serviceLocator.getControladorChamado();
	}

	/**
	 * Método responsável por fazer enviar email com chamados atrasados
	 * 
	 * @throws NegocioException
	 */
	public void alertaChamadosAtrasados() throws NegocioException {

		LOG.info("Iniciando rotina de envio de emails para os chamados atrasados...");

		String periodicidade =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS);

		if (periodicidade != null && !"0".equals(periodicidade)) {

			int dias = Util.quantidadeDiasIntervalo(new Date(0), new Date());

			if (dias % Integer.parseInt(periodicidade) == 0) {

				try {
					enviaEmailChamadosAtrasados();
				} catch (GGASException e) {
					throw new NegocioException(e);
				}

			}
		}

		LOG.info("Finalizando rotina de envio de emails para os chamados atrasados.");
	}

	/**
	 * Envia email para os chamados atrasados
	 * 
	 * @throws GGASException
	 */
	private void enviaEmailChamadosAtrasados() throws GGASException {

		Collection<UnidadeOrganizacional> unidadesOrganizacionais =
				(Collection<UnidadeOrganizacional>) controladorUnidadeOrganizacional.obterTodas();
		JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		ParametroSistema servidorEmailConfigurado =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);
		ParametroSistema emailRemetentePadrao =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
		
		if (servidorEmailConfigurado != null && BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor())
				&& !Util.isNullOrEmpty(unidadesOrganizacionais)) {

			for (UnidadeOrganizacional unidadeOrganizacional : unidadesOrganizacionais) {
				Collection<Chamado> chamados = controladorChamado.listarChamadosAtrasadosPorUnidadeOrganizacional(unidadeOrganizacional);

				enviarEmail(mailUtil, emailRemetentePadrao.getValor(), unidadeOrganizacional, chamados);
			}
		}
		
	}

	/**
	 * @param mailUtil
	 * @param emailRemetentePadrao
	 * @param unidadeOrganizacional
	 * @param chamados
	 * @throws GGASException
	 */
	private void enviarEmail(JavaMailUtil mailUtil, String emailRemetentePadrao, UnidadeOrganizacional unidadeOrganizacional,
			Collection<Chamado> chamados) throws GGASException {

		if (StringUtils.isNotEmpty(unidadeOrganizacional.getEmailContato()) && !Util.isNullOrEmpty(chamados)) {

			String corpoEmail = this.prepararCorpoEmail(chamados);
			String assuntoEmail = "Lista de Chamados atrasados";

			mailUtil.enviar(emailRemetentePadrao, unidadeOrganizacional.getEmailContato(), assuntoEmail, corpoEmail, true, false);
		}
	}

	/**
	 * Montar corpo do email
	 * 
	 * @param chamados
	 * @return corpo do email
	 * @throws NegocioException
	 */
	private String prepararCorpoEmail(Collection<Chamado> chamados) throws NegocioException {
		StringBuilder email = new StringBuilder();

		if (Util.isNullOrEmpty(chamados)) {
			throw new NegocioException("Nenhum chamado atrasado foi encontrado.");
		}

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		email.append("<h3>Abaixo segue a lista de chamados em atraso sob sua responsabilidade ou interesse.</h3>");
		email.append("<table border='1' cellpadding='5px' cellspacing='1px'><td><strong>Protocolo</strong></td>");
		email.append("<td><strong>Responsável</strong></td><td><strong>Segmento</strong></td><td><strong>Tipo</strong></td><td><strong>Assunto</strong></td>");
		email.append("<td><strong>Data de Abertura</strong></td><td><strong>Previsão Encerramento</strong></td>");
		email.append("<td><strong>Unidade Organizacional</strong></td><td><strong>Dias em atraso</strong></td>");
		email.append("<td><strong>Última Movimentação</strong></td>");
		
		String abreTD = "<td align='center'>";
		String fechaTD = "</td>";

		for (Chamado chamado : chamados) {
			email.append("<tr>");
			
			email.append(abreTD);
			email.append(chamado.getProtocolo().getNumeroProtocolo());
			email.append(fechaTD);
			
			email.append(abreTD);
			if(chamado.getUsuarioResponsavel() != null) {
				email.append(chamado.getUsuarioResponsavel().getDescricao());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getChamadoAssunto() != null && chamado.getChamadoAssunto().getChamadoTipo() != null
					&& chamado.getChamadoAssunto().getChamadoTipo().getSegmento() != null) {
				email.append(chamado.getChamadoAssunto().getChamadoTipo().getSegmento().getDescricao());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getChamadoAssunto() != null && chamado.getChamadoAssunto().getChamadoTipo() != null) {
				email.append(chamado.getChamadoAssunto().getChamadoTipo().getDescricao());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getChamadoAssunto() != null) {
				email.append(chamado.getChamadoAssunto().getDescricao());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getProtocolo() != null) {
				email.append(formato.format(chamado.getProtocolo().getUltimaAlteracao()));
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getDataPrevisaoEncerramento() != null) {
				email.append(formato.format(chamado.getDataPrevisaoEncerramento()));
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getUnidadeOrganizacional() != null) {
				email.append(chamado.getUnidadeOrganizacional().getDescricao());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getDataPrevisaoEncerramento() != null) {
				DateTime encerramento = Util.ultimoHorario(new DateTime(chamado.getDataPrevisaoEncerramento()));
				Integer dias = Util.quantidadeDiasIntervalo(encerramento.toDate(), new Date());
				email.append(dias);
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (chamado.getUltimaAlteracao() != null) {
				email.append(formato.format(chamado.getUltimaAlteracao()));
			}
			email.append(fechaTD);

			email.append("</tr>");
		}

		email.append("</table>");

		return email.toString();
	}
}
