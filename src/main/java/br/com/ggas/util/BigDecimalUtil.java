
package br.com.ggas.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import br.com.ggas.geral.exception.FormatoInvalidoException;

/**
 * The Class BigDecimalUtil.
 */
public class BigDecimalUtil {

	/** The Constant CEM. */
	public static final BigDecimal CEM = new BigDecimal(100);

	/**
	 * verifica se a entrada e maior que zero.
	 *
	 * @param valor valor a ser verificado
	 * @return true se valor > 0, false caso contrario
	 */
	public static boolean maiorQueZero(BigDecimal valor) {

		if (valor != null) {
			return valor.compareTo(BigDecimal.ZERO) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * verifica se a entrada não é nula e maior que zero.
	 *
	 * @param valor valor a ser verificado
	 * @return true nao for mulo e valor > 0, false caso contrario
	 */
	public static boolean naoNulo(BigDecimal valor) {

		if (valor != null) {
			return maiorQueZero(valor);
		}
		return false;
	}

	/**
	 * verifica se a entrada é nula ou igual a zero.
	 *
	 * @param valor valor a ser verificado
	 * @return true nao for mulo e valor > 0, false caso contrario
	 */
	public static boolean nulo(BigDecimal valor) {

		return !naoNulo(valor);
	}

	/**
	 * verifica se a entrada e menor que zero.
	 *
	 * @param valor valor a ser verificado
	 * @return true se valor < 0, false caso contrario
	 */
	public static boolean menorQueZero(BigDecimal valor) {

		if (valor != null) {
			return valor.compareTo(BigDecimal.ZERO) < 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Verifica se valor1 e menor que valor2.
	 *
	 * @param valor1 primeiro operando do menor que
	 * @param valor2 segundo operando do menor que
	 * @return valor1 < valor2
	 */
	public static boolean menorQue(BigDecimal valor1, BigDecimal valor2) {

		if (valor1 != null && valor2 != null) {
			return valor1.compareTo(valor2) < 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Verifica se valor1 e maior que valor2.
	 *
	 * @param valor1 primeiro operando do maior que
	 * @param valor2 segundo operando do maior que
	 * @return valor1 > valor2
	 */
	public static boolean maiorQue(BigDecimal valor1, BigDecimal valor2) {

		if (valor1 != null && valor2 != null) {
			return valor1.compareTo(valor2) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Método responsável por converter um string
	 * para um valor decimal.
	 *
	 * @param valor            O valor
	 * @return Uma valor
	 */
	public static String converterCampoValorDecimalParaString(BigDecimal valor) {

		return converterCampoValorDecimalParaString(valor, null);
	}

	/**
	 * Converter campo valor decimal para string.
	 *
	 * @param valor the valor
	 * @param numeroDecimais the numero decimais
	 * @return the string
	 */
	public static String converterCampoValorDecimalParaString(BigDecimal valor, Integer numeroDecimais) {

		if (valor != null) {
			DecimalFormat decimalFormat = obterDecimalFormatLocalizado(Constantes.LOCALE_PADRAO, numeroDecimais);
			return decimalFormat.format(valor);
		}

		return StringUtils.EMPTY;

	}

	/**
	 * Metodo que soma zero ou mais parcelas do tipo BigDecimal.
	 *
	 * @param parcelas as parcelas a serem somadas
	 * @return a soma das parcelas
	 */
	public static BigDecimal soma(BigDecimal... parcelas) {

		BigDecimal soma = BigDecimal.ZERO;
		for (BigDecimal parcela : parcelas) {
			if (parcela != null) {
				soma = soma.add(parcela);
			}
		}
		return soma;
	}

	/**
	 * metodo que obtem um Locale para Formatacao
	 * de Decimais baseado em Localidade e numero
	 * de
	 * Digitos decimais.
	 *
	 * @param locale the locale
	 * @param numeroDecimais the numero decimais
	 * @return the decimal format
	 */
	private static DecimalFormat obterDecimalFormatLocalizado(Locale locale, Integer numeroDecimais) {

		DecimalFormat decimalFormat = new DecimalFormat(Constantes.FORMATO_VALOR_NUMERO, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);
		decimalFormat.setDecimalSeparatorAlwaysShown(true);
		if (numeroDecimais == null) {
			decimalFormat.setMinimumFractionDigits(Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
		} else {
			decimalFormat.setMinimumFractionDigits(numeroDecimais);
			decimalFormat.setMaximumFractionDigits(numeroDecimais);
		}

		return decimalFormat;
	}

	/**
	 * Metodo que retorna o maior dentre os argumentos.
	 * Obs.: valores nulos serao tratados como zero.
	 * 
	 * @param valores valores de entrada para se obter o maior
	 * @return o maior valor
	 */
	public static BigDecimal maior(BigDecimal... valores) {

		BigDecimal maximo = BigDecimal.ZERO;
		for (BigDecimal valor : valores) {
			if (valor != null && BigDecimalUtil.maiorQue(valor, maximo)) {
				maximo = valor;
			}
		}
		return maximo;
	}

	/**
	 * Método responsável por converter um string para um valor.
	 * 
	 * @param strValor O valor
	 * @return O valor Big Decimal
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de conversão ou formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(String strValor) throws FormatoInvalidoException {

		return converterCampoStringParaValorBigDecimal(null, strValor);
	}

	/**
	 * Método responsável por converter um string para um valor.
	 *
	 * @param rotulo the rotulo
	 * @param strValor O valor
	 * @return O valor Big Decimal
	 * @throws FormatoInvalidoException             Caso ocorra algum erro de conversão ou formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(String rotulo, String strValor) throws FormatoInvalidoException {

		if (strValor != null) {
			return converterCampoStringParaValorBigDecimal(rotulo, strValor, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Método responsável por converter um string
	 * para um valor.
	 *
	 * @param rotulo
	 *            O rotulo do campo
	 * @param strValor
	 *            O valor
	 * @param formato
	 *            O formato
	 * @param locale
	 *            O Locale da aplicação
	 * @return Uma valor
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de
	 *             conversão ou
	 *             formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(String rotulo, String strValor, String formato, Locale locale)
					throws FormatoInvalidoException {

		BigDecimal valor = null;
		DecimalFormat decimalFormat = new DecimalFormat(formato, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);

		try {
			valor = (BigDecimal) decimalFormat.parse(strValor);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Converter string formato ingles para valor big decimal.
	 *
	 * @param strValor the str valor
	 * @return the big decimal
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static BigDecimal converterStringFormatoInglesParaValorBigDecimal(String strValor) throws FormatoInvalidoException {

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);
		try {
			return (BigDecimal) decimalFormat.parse(strValor);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS);
		}
	}

	/**
	 * Método responsável por converter um valor
	 * BigDecimal real para um valor BigDecimal em
	 * percentual .
	 *
	 * @param bigDecimal
	 *            O valor a ser transformado
	 * @return Um valor BigDecimal em percentual
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de
	 *             conversão ou formato
	 */
	public static BigDecimal converterPercentualParaBigDecimal(BigDecimal bigDecimal) throws FormatoInvalidoException {

		if (bigDecimal != null) {
			try {
				return bigDecimal.multiply(new BigDecimal(100));
			} catch (ArithmeticException e) {
				Log.error(e.getStackTrace(), e);
				throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, false);
			}
		}

		return null;
	}

	/**
	 * Converter percentual para big decimal string.
	 *
	 * @param valor the valor
	 * @return the string
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static String converterPercentualParaBigDecimalString(BigDecimal valor) throws FormatoInvalidoException {

		return converterCampoValorDecimalParaString(converterPercentualParaBigDecimal(valor));
	}

}
