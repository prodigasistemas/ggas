/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

//TODO: [jgfm] Refactory 
public class Extenso {

	private List<Integer> nro;

	private BigInteger num;

	private static final String[][] QUALIFICADORES = { {"centavo", "centavos"}, {"", ""}, {"mil", "mil"}, {"milhão", "milhões"}, 
	                                            {"bilhão", "bilhões"}, {"trilhão", "trilhões"}, {"quatrilhão", "quatrilhões"}, 
	                                            {"quintilhão", "quintilhões"}, {"sextilhão", "sextilhões"}, {"septilhão", "septilhões"}};

	private static final String[][] NUMEROS = { {"zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", 
		"dez", "onze", "doze", "treze", "quatorze", "quinze", "desesseis", "desessete", "dezoito", "dezenove"}, 
		{"vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"}, 
		{"cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"}};

	/**
	 * Construtor.
	 */
	public Extenso() {

		nro = new ArrayList<Integer>();
	}

	/**
	 * Construtor.
	 * 
	 * @param dec
	 *            valor para colocar por extenso
	 */
	public Extenso(BigDecimal dec) {

		this();
		setNumber(dec);
		// by gmatos on
		// 15/10/09 10:22
	}

	/**
	 * Constructor for the Extenso object.
	 * 
	 * @param dec
	 *            valor para colocar por extenso
	 */
	public Extenso(double dec) {

		this();
		setNumber(dec);
		// by gmatos on
		// 15/10/09 10:22
	}

	/**
	 * Sets the Number attribute of the Extenso
	 * object
	 * 
	 * @param dec
	 *            The new Number value
	 */
	public void setNumber(BigDecimal dec) {

		// Converte para inteiro arredondando os
		// centavos
		num = dec.setScale(2, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100)).toBigInteger();

		// Adiciona valores
		nro.clear();
		if(num.equals(BigInteger.ZERO)) {
			// Centavos
			nro.add(Integer.valueOf(0));
			// Valor
			nro.add(Integer.valueOf(0));
		} else {
			// Adiciona centavos
			addRemainder(100);

			// Adiciona grupos de 1000
			while(!num.equals(BigInteger.ZERO)) {
				addRemainder(1000);
			}
		}
	}

	public void setNumber(double dec) {

		setNumber(BigDecimal.valueOf(dec));
	}

	/**
	 * Description of the Method.
	 * 
	 * @return Description of the Returned Value
	 */
	@Override
	public String toString() {

		StringBuilder buf = new StringBuilder();

		int ct;

		for (ct = nro.size() - 1; ct > 0; ct--) {
			// Se ja existe texto e o atual não é zero
			if(buf.length() > 0 && !ehGrupoZero(ct)) {
				buf.append(" e ");
			}
			buf.append(numToString((nro.get(ct)).intValue(), ct));
		}
		if(buf.length() > 0) {
			if(ehUnicoGrupo()) {
				buf.append(" de ");
			}
			while(buf.toString().endsWith(" ")){
				buf.setLength(buf.length() - 1);
			}
			if(ehPrimeiroGrupoUm()) {
				buf.insert(0, "h");
			}
			if(nro.size() == 2 && (nro.get(1)).intValue() == 1) {
				buf.append(" real");
			} else {
				buf.append(" reais");
			}
			if((nro.get(0)).intValue() != 0) {
				buf.append(" e ");
			}
		}
		if((nro.get(0)).intValue() != 0) {
			buf.append(numToString((nro.get(0)).intValue(), 0));
		}
		return buf.toString();
	}

	/**
	 * Eh primeiro grupo um.
	 * 
	 * @return true, if successful
	 */
	private boolean ehPrimeiroGrupoUm() {

		if((nro.get(nro.size() - 1)).intValue() == 1){ 
			return true;
		}
		return false;
	}

	/**
	 * Adds a feature to the Remainder attribute of the Extenso object.
	 * 
	 * @param divisor
	 *            The feature to be added to the Remainder attribute
	 */
	private void addRemainder(int divisor) {

		// Encontra newNum[0] = num modulo
		// divisor, newNum[1] = num dividido
		// divisor
		BigInteger[] newNum = num.divideAndRemainder(BigInteger.valueOf(divisor));

		// Adiciona modulo
		nro.add(Integer.valueOf(newNum[1].intValue()));

		// Altera numero
		num = newNum[0];
	}

	/**
	 * Description of the Method.
	 * 
	 * @return Description of the Returned Value
	 */
	private boolean ehUnicoGrupo() {

		if (nro.size() <= 3) {
			return false;
		}
		
		if (!ehGrupoZero(1) && !ehGrupoZero(2)) {
			return false;
		}
		
		boolean hasOne = false;
		for (int i = 3; i < nro.size(); i++) {
			if ((nro.get(i)).intValue() != 0) {
				if (hasOne) {
					return false;
				}
				hasOne = true;
			}
		}
		
		return true;
	}

	/**
	 * Eh grupo zero.
	 * 
	 * @param ps
	 *            the ps
	 * @return true, if successful
	 */
	boolean ehGrupoZero(int ps) {

		if(ps <= 0 || ps >= nro.size()) {
			return true;
		}
		return (nro.get(ps)).intValue() == 0;
	}

	/**
	 * Description of the Method.
	 * 
	 * @param numero
	 *            Description of Parameter
	 * @param escala
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	private String numToString(int numero, int escala) {

		int unidade = numero % 10;
		int dezena = numero % 100;
		// * nao pode
		// dividir
		// por 10
		// pois
		// verifica
		// de
		// 0..19
		int centena = numero / 100;
		StringBuilder buf = new StringBuilder();

		if(numero != 0) {
			if(centena != 0) {
				if(dezena == 0 && centena == 1) {
					buf.append(NUMEROS[2][0]);
				} else {
					buf.append(NUMEROS[2][centena]);
				}
			}

			if((buf.length() > 0) && (dezena != 0)) {
				buf.append(" e ");
			}
			if(dezena > 19) {
				dezena /= 10;
				buf.append(NUMEROS[1][dezena - 2]);
				if(unidade != 0) {
					buf.append(" e ");
					buf.append(NUMEROS[0][unidade]);
				}
			} else if(centena == 0 || dezena != 0) {
				buf.append(NUMEROS[0][dezena]);
			}

			buf.append(" ");
			if(numero == 1) {
				buf.append(QUALIFICADORES[escala][0]);
			} else {
				buf.append(QUALIFICADORES[escala][1]);
			}
		}

		return buf.toString();
	}

}
