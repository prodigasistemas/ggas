/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.util;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.hibernate.type.Type;

import br.com.ggas.auditoria.Auditoria;
import br.com.ggas.auditoria.CampoAuditado;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.auditoria.exception.ValidarAuditoriaException;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * This is a Javadoc comment
 * 
 * @author bribeiro
 */
public class AuditoriaInterceptor extends EmptyInterceptor {

	private static final Logger LOG = Logger.getLogger(AuditoriaInterceptor.class);

	private static final long serialVersionUID = 5595020431811812063L;

	private static final String ID_SESSION_FACTORY = "sessionFactory";

	private static final String OPERACAO_UPDATE = "UPDATE";

	private static final String OPERACAO_DELETE = "DELETE";

	private static final String OPERACAO_INSERT = "INSERT";

	private static final String REFLECTION_CLASS = "Impl";

	private static final String INDICADOR_CHAVE_PRIMARIA = "@";

	private static final String ESTRUTURA_GERAL_PACOTES = "br.com.ggas";

	private static final String CHAVE_PRIMARIA_DEFAULT_PROP = "chavePrimaria";

	private ServiceLocator serviceLocator;

	private Configuration hibernateConfiguration;

	/*
	 * (non-Javadoc)
	 * @see
	 * org.hibernate.EmptyInterceptor#onSave(java
	 * .lang.Object, java.io.Serializable,
	 * java.lang.Object[], java.lang.String[],
	 * org.hibernate.type.Type[])
	 */
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
					throws ValidarAuditoriaException {

		this.efetuarAuditoria(entity, OPERACAO_INSERT);
		return super.onSave(entity, id, state, propertyNames, types);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.hibernate.EmptyInterceptor#onFlushDirty
	 * (java.lang.Object, java.io.Serializable,
	 * java.lang.Object[], java.lang.Object[],
	 * java.lang.String[],
	 * org.hibernate.type.Type[])
	 */
	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames,
					Type[] types) {

		this.efetuarAuditoria(entity, OPERACAO_UPDATE);
		return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.hibernate.EmptyInterceptor#onDelete
	 * (java.lang.Object, java.io.Serializable,
	 * java.lang.Object[], java.lang.String[],
	 * org.hibernate.type.Type[])
	 */
	@Override
	public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

		this.efetuarAuditoria(entity, OPERACAO_DELETE);
		super.onDelete(entity, id, state, propertyNames, types);
	}

	/**
	 * Monta e salva o registro de auditoria do
	 * objeto informado, caso ele esteja
	 * habilitado para auditoria.
	 * 
	 * @param objeto
	 *            Objeto alvo
	 * @param tipoOperacao
	 *            Tipo de operação de persistência
	 *            que está sendo executada
	 */
	private void efetuarAuditoria(Object objeto, String tipoOperacao) {

		Map<String, Object> erros = null;
		if(objeto instanceof EntidadeNegocio) {

			EntidadeNegocio entidadeAuditavel = (EntidadeNegocio) objeto;

			List<String> atributosAuditaveis = TrilhaAuditoriaUtil.getTrilhaAuditoria(buscarInterface(entidadeAuditavel.getClass())
					.getName());

			if(atributosAuditaveis != null && !atributosAuditaveis.isEmpty() && ControleDeAcessoFilter.getPermissao()!=null) {

				Operacao operacao = getOperacao(ControleDeAcessoFilter.getPermissao());

				if(entidadeAuditavel.getDadosAuditoria() != null){
					operacao = entidadeAuditavel.getDadosAuditoria().getOperacao();
				}

				// ControleDeAcessoFilter.getOperacao(); // NOSONAR {Validar performance e retirar static}

				if(operacao != null && operacao.isAuditavel()) {

					if(entidadeAuditavel.getDadosAuditoria() == null) {
						entidadeAuditavel.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
					}

					erros = entidadeAuditavel.validarDadosAuditoria();

					if(erros != null && !erros.isEmpty()) {
						
						LOG.info("Operação: " + operacao != null ? operacao.getDescricao() : "Sem Operação");
						
						LOG.info("Erros");
						
						for (Map.Entry<String, Object> entry : erros.entrySet()) {
							LOG.info("Chave: {" + entry.getKey() + "}, Valor: {" + entry.getValue().toString() + "}");
						}
						
						LOG.info("Atributos");
						
						for(String atributo : atributosAuditaveis) {
							LOG.info(atributo);
						}
						
						throw new ValidarAuditoriaException(Constantes.ERRO_VALIDACAO_DADOS_AUDITORIA);
					}

					DadosAuditoria dadosAuditoria = entidadeAuditavel.getDadosAuditoria();

					StringBuilder logAuditoria = new StringBuilder();

					LOG.info("Preenchendo Auditoria");

					ServiceLocator serviceLocatorLocal = ServiceLocator.getInstancia();
					Auditoria auditoria = (Auditoria) serviceLocatorLocal.getBeanPorID(Auditoria.BEAN_ID_AUDITORIA);
					auditoria.setChaveEntidade(entidadeAuditavel.getChavePrimaria());
					auditoria.setClasseEntidade(entidadeAuditavel.getClass().getName());
					auditoria.setTabela(this.getTabela(entidadeAuditavel.getClass()));
					auditoria.setDataHora(Calendar.getInstance().getTime());
					auditoria.setTipoOperacao(tipoOperacao);

					auditoria.setIp(dadosAuditoria.getIp());
					auditoria.setUsuario(dadosAuditoria.getUsuario().getLogin());

					if(dadosAuditoria.getChaveAuditoriaPai() != null && dadosAuditoria.getChaveAuditoriaPai() > 0) {
						auditoria.setChavePrimariaPai(dadosAuditoria.getChaveAuditoriaPai());
					}

					String menu;
					if(dadosAuditoria.getOperacao().getMenu() != null){
						menu = dadosAuditoria.getOperacao().getMenu().getDescricao() + " - ";
					} else {
						menu = "";
					}


					auditoria.setOperacao(menu + operacao.getDescricao());

					String coluna = null;
					String valor = null;

					logAuditoria.append("Data - Hora: ");
					logAuditoria.append(Calendar.getInstance().getTime());
					logAuditoria.append("\r\n");
					logAuditoria.append("Operação: ");
					logAuditoria.append(auditoria.getOperacao());
					logAuditoria.append("\r\n");
					logAuditoria.append("Usuário: ");
					logAuditoria.append(auditoria.getUsuario());
					logAuditoria.append("\r\n");
					logAuditoria.append("IP: ");
					logAuditoria.append(auditoria.getIp());
					logAuditoria.append("\r\n");
					logAuditoria.append("Tabela: ");
					logAuditoria.append(auditoria.getTabela());
					logAuditoria.append("\r\n");
					logAuditoria.append("Modificação: ");
					logAuditoria.append(auditoria.getTipoOperacao());
					logAuditoria.append("\r\n");

					LOG.info("Inicio Aditoria dos campos");
					for (String atributo : atributosAuditaveis) {

						coluna = this.getColuna(entidadeAuditavel.getClass(), atributo);
						valor = this.getValor(entidadeAuditavel, atributo);

						if((coluna != null && coluna.length() > 0) && (valor != null && valor.length() > 0)) {

							CampoAuditado campoAuditado = (CampoAuditado) serviceLocatorLocal.getBeanPorID(CampoAuditado.BEAN_ID_CAMPO_AUDITADO);
							campoAuditado.setColuna(coluna);
							campoAuditado.setValor(valor);

							auditoria.getCamposAuditados().add(campoAuditado);

							logAuditoria.append("Coluna: ");
							logAuditoria.append(coluna);
							logAuditoria.append(" ");
							logAuditoria.append("Valor: ");
							logAuditoria.append(valor);
							logAuditoria.append("\r\n");
						}

					}
					LOG.info("Fim Aditoria dos campos");

					LOG.info("Inserindo Auditoria");
					this.inserirTrilhaAuditoria(auditoria);
					if(auditoria.getChavePrimariaPai() == null) {
						dadosAuditoria.setChaveAuditoriaPai(auditoria.getChavePrimaria());
					}
					LOG.info(logAuditoria.toString());
				}
			}
		}
	}

	/**
	 * Insere na base o registo de auditoria
	 * informado.
	 * 
	 * @param auditoria
	 *            Registro de auditoria
	 */
	private void inserirTrilhaAuditoria(Auditoria auditoria) {

		LOG.info("Abrindo Sessão Auditoria");
		SessionFactory sessionFactory = (SessionFactory) serviceLocator.getBeanPorID(ID_SESSION_FACTORY);
		Session session = (Session) sessionFactory.openSession();

		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(auditoria);
			transaction.commit();
			LOG.info("Auditoria commited");
		} catch(HibernateException e) {
			LOG.error(e);
		} finally {
			session.close();
		}

	}

	/**
	 * Gets the operacao.
	 * 
	 * @param p
	 *            the p
	 * @return the operacao
	 */
	private Operacao getOperacao(Permissao p) {

		SessionFactory sessionFactory = (SessionFactory) ServiceLocator.getInstancia().getBeanPorID(ID_SESSION_FACTORY);
		Session session = (Session) sessionFactory.openSession();

		try {
			Permissao permissao = (Permissao) session.get(p.getClass(), p.getChavePrimaria());
			return permissao.getOperacao();
		} finally {
			session.close();
		}
	}

	/**
	 * Retorna o nome da tabela para a qual a
	 * classe informada está mapeada.
	 * 
	 * @param classe
	 *            Classe alvo
	 * @return nomeTabela
	 */
	private String getTabela(Class<?> classe) {

		String tabela = null;
		PersistentClass persistentClass = null;

		this.serviceLocator = ServiceLocator.getInstancia();
		this.hibernateConfiguration = (Configuration) serviceLocator.getBeanPorID("hibernateConfiguration");
		persistentClass = hibernateConfiguration.getClassMapping(classe.getName());

		if(persistentClass != null) {
			tabela = persistentClass.getTable().getName();
		}

		return tabela;
	}

	/**
	 * Retorna o nome da coluna para a qual a
	 * propriedade na classe informada está
	 * mapeada.
	 * 
	 * @param classe
	 *            Classe alvo
	 * @param propriedade
	 *            Nome do atributo na classe
	 * @return nomeColuna
	 */
	private String getColuna(Class<?> classe, String propriedade) {

		String coluna = null;
		PersistentClass persistentClass = null;
		Object item = null;
		Property property = null;
		Iterator<?> iterator = null;

		this.serviceLocator = ServiceLocator.getInstancia();
		this.hibernateConfiguration = (Configuration) serviceLocator.getBeanPorID("hibernateConfiguration");
		persistentClass = hibernateConfiguration.getClassMapping(classe.getName());

		if(persistentClass != null) {
			if(CHAVE_PRIMARIA_DEFAULT_PROP.equals(propriedade)) {
				property = persistentClass.getIdentifierProperty();
			} else {
				property = persistentClass.getRecursiveProperty(propriedade);
			}

			iterator = property.getColumnIterator();

			while(iterator.hasNext()) {
				item = iterator.next();
				if(item instanceof Column) {
					coluna = ((Column) item).getName();
					break;
				}
			}

		}

		return coluna;
	}

	/**
	 * Recupera o valor da propriedade na entidade
	 * alvo.
	 * 
	 * @param entidadeAuditavel
	 *            Entidade alvo
	 * @param propriedade
	 *            Nome do atributo na entidade
	 *            alvo
	 * @return valor
	 */
	private String getValor(EntidadeNegocio entidadeAuditavel, String propriedade) {

		String valor = null;

		try {
			valor = BeanUtils.getProperty(entidadeAuditavel, propriedade);

			if(valor != null) {

				// Tratamento para pegar o valor
				// da chave de propriedades que
				// são entidades de negócio.
				int indice = valor.indexOf(INDICADOR_CHAVE_PRIMARIA);
				if(valor.startsWith(ESTRUTURA_GERAL_PACOTES) && indice > -1) {
					valor = BeanUtils.getNestedProperty(entidadeAuditavel, propriedade + "." + CHAVE_PRIMARIA_DEFAULT_PROP);
				} else {
					if(!propriedade.contains(".")) {
						Class<?> classeAtual = buscarInterface(entidadeAuditavel.getClass());
						Method getPropriedade = buscarMetodoPropriedade(propriedade, classeAtual);

						// Formata valores do tipo
						// data.
						if (getPropriedade != null && getPropriedade.getReturnType().equals(Date.class)) {
							Date data = (Date) getPropriedade.invoke(classeAtual.cast(entidadeAuditavel));
							valor = Util.converterDataHoraParaString(data);
						}
					}
				}
			}
		} catch(Exception e) {
			LOG.error(e);
		}

		return valor;
	}

	/**
	 * Retorna o método de acesso da classe ao
	 * atributo informado.
	 * 
	 * @param nomeAtributo
	 *            Nome do atributo alvo
	 * @param classe
	 *            Classe alvo
	 * @return the method
	 */
	private Method buscarMetodoPropriedade(String nomeAtributo, Class<?> classe) {

		Method metodoPropriedade = null;

		StringBuilder atributo = new StringBuilder(nomeAtributo);
		atributo.setCharAt(0, atributo.substring(0, 1).toUpperCase().charAt(0));
		String nome = "get" + atributo.toString();
		try {
			metodoPropriedade = classe.getMethod(nome);
		} catch(Exception e1) {
			nome = "is" + atributo.toString();
			LOG.error(e1.getMessage(), e1);
			try {
				metodoPropriedade = classe.getMethod(nome);
			} catch(Exception e2) {
				LOG.error("Método de acesso à propriedade \"" + nomeAtributo + "\" não encontrado na classe " + classe.getName(), e2);
			}
		}

		return metodoPropriedade;
	}

	/**
	 * Recupera a interface principal implementada
	 * pela classe informada.
	 * 
	 * @param classe
	 *            Classe alvo
	 * @return interfacePrincipal
	 */
	private Class<? extends Object> buscarInterface(Class<?> classe) {

		Class<?>[] interfaces = classe.getInterfaces();
		for (Class<?> i : interfaces) {
			if(classe.getSimpleName().equals(i.getSimpleName() + REFLECTION_CLASS)) {
				return i;
			}
		}
		return classe;
	}

	/**
	 * Percorre as entidades auditáveis filhas da
	 * entidade auditável informada, propagando
	 * os dados de auditoria da última.
	 * 
	 * @param entidadeAuditavel
	 *            Entidade alvo
	 *            public void
	 *            popularAuditoriaFilhos
	 *            (EntidadeNegocioAuditavel
	 *            entidadeAuditavel){
	 *            Field[] listaAtributos =
	 *            entidadeAuditavel
	 *            .getClass().getDeclaredFields();
	 *            try {
	 *            Class<?> classeAtual =
	 *            buscarInterface
	 *            (entidadeAuditavel.getClass());
	 *            Object atual = classeAtual.cast(
	 *            entidadeAuditavel);
	 *            for(Field atributo :
	 *            listaAtributos){
	 *            if (!atributo.getType().
	 *            isPrimitive()){
	 *            Object obj =
	 *            classeAtual.getMethod
	 *            (montarNomeGet
	 *            (atributo.getName()
	 *            )).invoke(classeAtual
	 *            .cast(atual));
	 *            Boolean isHibernateProxy = obj
	 *            instanceof HibernateProxy;
	 *            Boolean isGenerics =
	 *            atributo.getGenericType()
	 *            instanceof ParameterizedType;
	 *            if (obj != null &&
	 *            !isHibernateProxy && (obj
	 *            instanceof
	 *            EntidadeNegocioAuditavel ||
	 *            isGenerics)){
	 *            if (isGenerics){
	 *            try {
	 *            Collection colecao =
	 *            (Collection)obj;
	 *            if(!colecao.isEmpty() &&
	 *            colecao.iterator().next()
	 *            instanceof
	 *            EntidadeNegocioAuditavel){
	 *            for (Object object : colecao) {
	 *            recursao((
	 *            EntidadeNegocioAuditavel)object,
	 *            entidadeAuditavel
	 *            .getDadosAuditoria());
	 *            }
	 *            }
	 *            } catch
	 *            (LazyInitializationException e)
	 *            {
	 *            Logger.getLogger("ERROR").debug(e.getStackTrace());
	 *            }
	 *            }else{
	 *            recursao((
	 *            EntidadeNegocioAuditavel)obj,
	 *            entidadeAuditavel
	 *            .getDadosAuditoria());
	 *            }
	 *            }
	 *            }
	 *            }
	 *            } catch (Exception e) {
	 *            LOG.error(e);
	 *            }
	 *            }
	 */

	/**
	 * Popula a entidade auditável informada e
	 * seus filhos caso a primeira não possua
	 * dados complementares para a auditoria.
	 * 
	 * @param obj
	 *            Objeto alvo
	 * @param dadosAuditoria
	 *            Dados complementares para o
	 *            registro da auditoria
	 *            private void recursao(
	 *            EntidadeNegocioAuditavel obj,
	 *            DadosAuditoria dadosAuditoria){
	 *            if(obj.getDadosAuditoria() ==
	 *            null){
	 *            obj.setDadosAuditoria(
	 *            dadosAuditoria);
	 *            popularAuditoriaFilhos(obj);
	 *            }
	 *            }
	 */

}
