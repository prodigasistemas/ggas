/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.logprocesso.negocio.ControladorLogProcesso;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.GraficoVO;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.PieDataset;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.*;

/**
 * Classe de geração de relatórios
 *
 */
public final class RelatorioUtil {

	private static final Logger LOG = Logger.getLogger(RelatorioUtil.class);
	public static final String BASE_XML_PARA_DANFE = "/nfeProc/NFe/infNFe/det";
	public static final String LOGO_CLIENTE = "LogoCliente";

	private static final ControladorLogProcesso controladorLogProcesso = (ControladorLogProcesso) ServiceLocator.getInstancia()
			.getControladorNegocio(ControladorLogProcesso.BEAN_ID_CONTROLADOR_LOG_PROCESSO);

	private static final ControladorUsuario controladorUsuario = ServiceLocator.getInstancia().getControladorUsuario();

	private static final DadosAuditoria dadosAuditoria = (DadosAuditoria) ServiceLocator.getInstancia()
			.getBeanPorID(DadosAuditoria.BEAN_ID_DADOS_AUDITORIA);

	/**
	 * Enum com os tipos de gráficos
	 *
	 */
	public static enum TipoGrafico {
		LINHA, COLUNA, PIZZA;

		/**
		 * Listar.
		 * 
		 * @return the collection
		 */
		public static Collection<String> listar() {

			Collection<String> tiposGrafico = new ArrayList<String>();
			tiposGrafico.add(LINHA.toString());
			tiposGrafico.add(COLUNA.toString());
			tiposGrafico.add(PIZZA.toString());
			return tiposGrafico;
		}
	}

	/**
	 * Retorna os bytes do relatório PDF gerado a
	 * partir da lista de registros informada.
	 * 
	 * @param dados
	 *            Registros a exibirno relatório
	 * @param parametros
	 *            Informações adicionais
	 * @param arquivoJasper
	 *            Template para geração
	 * @return bytesRelatorio
	 * @throws InfraestruturaException
	 * @throws NegocioException 
	 */
	public static byte[] gerarRelatorioPDF(Collection<?> dados, Map<String, Object> parametros, String arquivoJasper)
					throws NegocioException {

		return gerarRelatorio(dados, parametros, arquivoJasper, FormatoImpressao.PDF);
	}

	/**
	 * Gerar Danfe
	 * @param xmlNota {@link byte}
	 * @param arquivoJasper {@link String}
	 * @param logoEmpresa - {@link String}
	 * @return bytes do relatorio
	 * @throws NegocioException the negocio exception
	 */
	public static byte[] gerarDanfe(byte[] xmlNota, String arquivoJasper, String logoEmpresa) throws NegocioException  {
		ByteArrayOutputStream pdfOut = new ByteArrayOutputStream();
		try {
			InputStream is = new ByteArrayInputStream(xmlNota);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(is);
			JRXmlDataSource xmlDataSource = new JRXmlDataSource(doc, BASE_XML_PARA_DANFE);
			InputStream modeloJasper = RelatorioUtil.class.getClassLoader().getResourceAsStream(arquivoJasper);
			JasperReport report = JasperCompileManager.compileReport(modeloJasper);
			ByteArrayOutputStream reportOut = new ByteArrayOutputStream();
			HashMap parametros = new HashMap();
			URL url = new URL(logoEmpresa);
			Image imgLogo = ImageIO.read(url);
			parametros.put(LOGO_CLIENTE, imgLogo);
			JasperFillManager.fillReportToStream(report, reportOut, parametros, xmlDataSource);
			ByteArrayInputStream reportIn = new ByteArrayInputStream(reportOut.toByteArray());
			JasperExportManager.exportReportToPdfStream(reportIn, pdfOut);
			return pdfOut.toByteArray();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FALHA_NA_GERACAO_DO_DANFE, true);
		}
	}

	/**
	 * Gerar relatorio.
	 * 
	 * @param dados
	 *            the dados
	 * @param parametros
	 *            the parametros
	 * @param arquivoJasper
	 *            the arquivo jasper
	 * @param tipo
	 *            the tipo
	 * @return the byte[]
	 * @throws InfraestruturaException
	 *             the infraestrutura exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/**
	 * Retorna os bytes do relatório gerado a
	 * partir da lista de registros informada no
	 * formato indicado.
	 * 
	 * @param dados
	 *            Registros a exibirno relatório
	 * @param parametros
	 *            Informações adicionais
	 * @param arquivoJasper
	 *            Template para geração
	 * @param tipo
	 *            Formato do relatório de saída
	 * @return bytesRelatorio
	 * @throws InfraestruturaException
	 * @throws NegocioException
	 */
	public static byte[] gerarRelatorio(Collection<?> dados, Map<String, Object> parametros, String arquivoJasper, FormatoImpressao tipo)
					throws NegocioException {

		Usuario usuario = controladorUsuario.buscar(Constantes.USUARIO_AUDITORIA);

		JasperReport jasperReport = null;
		InputStream inputStream = null;
		byte[] bytes = null;

		try {
			JRDataSource source = new JRBeanCollectionDataSource(dados);
			inputStream = RelatorioUtil.class.getClassLoader().getResourceAsStream(arquivoJasper);
			
			if (inputStream == null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO, true);
			}
			
			if(tipo != null) {

				if(tipo.equals(FormatoImpressao.PDF)) {

					jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
					bytes = JasperRunManager.runReportToPdf(jasperReport, parametros, new JRBeanCollectionDataSource(dados));
					inputStream.close();

				} else {

					JasperPrint print = JasperFillManager.fillReport(inputStream, parametros, source);
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					JRExporter exporter = null;

					if(tipo.equals(FormatoImpressao.RTF)) {

						exporter = new JRRtfExporter();
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

					} else if(tipo.equals(FormatoImpressao.CSV)) {

						exporter = new JRCsvExporter();
						exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, "|");
						exporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, "\n");
						exporter.setParameter(JRCsvExporterParameter.JASPER_PRINT, print);
						exporter.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, baos);

					} else if(tipo.equals(FormatoImpressao.XLS)) {

						exporter = new JRXlsExporter();

						exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
						exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
						exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
						exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65000);
						exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
						exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.FALSE);
						exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
						exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);

					}

					if(exporter != null) {
						exporter.exportReport();
					}

					bytes = baos.toByteArray();

					baos.flush();
					baos.close();

				}
			}

		} catch(JRException e) {
			LOG.error(e.getMessage(), e);
			dadosAuditoria.setUsuario(usuario);
			controladorLogProcesso.gerarInsetirLogProcesso(dadosAuditoria,e);

			if(e.getMessage().contains("Image read failed.")) {
				if (parametros.containsKey("imagemLogoBanco")) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_BANCO_SEM_LOGO, true);
				}
				throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_CDL_SEM_LOGO, true);
			}

			throw new NegocioException("Ocorreu um erro ao gerar o arquivo PDF", true);

		} catch(IOException e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		}

		return bytes;
	}

	/**
	 * Exibir relatorio.
	 * 
	 * @param relatorio
	 *            the relatorio
	 * @param parametros
	 *            the parametros
	 * @param entidades
	 *            the entidades
	 */
	public static void exibirRelatorio(String relatorio, Map<String, Object> parametros, Collection<?> entidades) {

		InputStream input = RelatorioUtil.class.getClassLoader().getResourceAsStream(relatorio);
		try {
			JasperReport report = (JasperReport) JRLoader.loadObject(input);
			JasperPrint print = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(entidades));
			JasperViewer.viewReport(print, false);
			JasperViewer viewer = (JasperViewer) JasperViewer.getFrames()[JasperViewer.getFrames().length - 1];
			viewer.setAlwaysOnTop(true);
		} catch(JRException e) {
			throw new InfraestruturaException(e);
		}
	}

	/**
	 * Unificar relatorios pdf.
	 * 
	 * @param relatoriosPdf
	 *            the relatorios pdf
	 * @return the byte[]
	 */
	public static byte[] unificarRelatoriosPdf(List<byte[]> relatoriosPdf) {

		byte[] pdfConcatenado = null;
		ByteArrayOutputStream outputStream = null;

		try {
			if(relatoriosPdf != null && !relatoriosPdf.isEmpty()) {
				byte[] relatorio = null;
				PdfCopy copiaPdf = null;
				Document documentoPdf = null;
				int qtdRelatorios = 0;

				Iterator<byte[]> iterator = relatoriosPdf.iterator();
				outputStream = new ByteArrayOutputStream();
				while(iterator.hasNext()) {
					relatorio = iterator.next();
					PdfReader reader = new PdfReader(relatorio);

					int qtdPaginas = reader.getNumberOfPages();
					if(qtdRelatorios == 0) {
						documentoPdf = new Document(reader.getPageSizeWithRotation(1));
						copiaPdf = new PdfCopy(documentoPdf, outputStream);
						documentoPdf.open();
					}

					PdfImportedPage page;

					for (int i = 0; i < qtdPaginas;) {
						++i;
						page = copiaPdf.getImportedPage(reader, i);
						copiaPdf.addPage(page);
					}

					PRAcroForm form = reader.getAcroForm();

					if(form != null) {
						copiaPdf.copyAcroForm(reader);
					}

					qtdRelatorios++;
				}
				documentoPdf.close();
			}
		} catch(IOException e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		} catch(DocumentException e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		}

		if(outputStream != null) {
			pdfConcatenado = outputStream.toByteArray();
		}

		return pdfConcatenado;
	}

	/**
	 * Gerar grafico barra vertical.
	 * 
	 * @param tituloGrafico
	 *            the titulo grafico
	 * @param tituloEixoX
	 *            the titulo eixo x
	 * @param tituloEixoY
	 *            the titulo eixo y
	 * @param largura
	 *            the largura
	 * @param altura
	 *            the altura
	 * @param arrayValores
	 *            the array valores
	 * @return the buffered image
	 * @throws InfraestruturaException
	 *             the infraestrutura exception
	 */
	public static BufferedImage gerarGraficoBarraVertical(String tituloGrafico, String tituloEixoX, String tituloEixoY, int largura,
					int altura, Collection<GraficoVO> arrayValores){

		BufferedImage buf = null;

		try {
			DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();

			Iterator<GraficoVO> iterator = arrayValores.iterator();
			while(iterator.hasNext()) {
				GraficoVO modelo = iterator.next();

				defaultCategoryDataset.addValue(modelo.getValor(), modelo.getChaveLinha(), modelo.getChaveColuna());
			}

			JFreeChart chart = ChartFactory.createBarChart(tituloGrafico, tituloEixoX, tituloEixoY, defaultCategoryDataset,
							PlotOrientation.VERTICAL, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);

			chart.setBorderVisible(Boolean.TRUE);
			chart.setBorderPaint(Color.black);

			buf = chart.createBufferedImage(largura, altura);

		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		}

		return buf;
	}

	/**
	 * Gerar grafico linha.
	 * 
	 * @param tituloGrafico
	 *            the titulo grafico
	 * @param tituloEixoX
	 *            the titulo eixo x
	 * @param tituloEixoY
	 *            the titulo eixo y
	 * @param largura
	 *            the largura
	 * @param altura
	 *            the altura
	 * @param arrayValores
	 *            the array valores
	 * @return the buffered image
	 * @throws InfraestruturaException
	 *             the infraestrutura exception
	 */
	public static BufferedImage gerarGraficoLinha(String tituloGrafico, String tituloEixoX, String tituloEixoY, int largura, int altura,
					Collection<GraficoVO> arrayValores){

		BufferedImage buf = null;

		try {
			DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();

			Iterator<GraficoVO> iterator = arrayValores.iterator();
			while(iterator.hasNext()) {
				GraficoVO modelo = iterator.next();

				defaultCategoryDataset.addValue(modelo.getValor(), modelo.getChaveLinha(), modelo.getChaveColuna());
			}

			JFreeChart chart = ChartFactory.createLineChart(tituloGrafico, tituloEixoX, tituloEixoY, defaultCategoryDataset,
							PlotOrientation.VERTICAL, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);

			chart.setBorderVisible(Boolean.TRUE);
			chart.setBorderPaint(Color.black);

			buf = chart.createBufferedImage(largura, altura);

		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		}

		return buf;
	}

	/**
	 * Gerar grafico pizza.
	 * 
	 * @param tituloGrafico
	 *            the titulo grafico
	 * @param mapaDados
	 *            the mapa dados
	 * @param largura
	 *            the largura
	 * @param altura
	 *            the altura
	 * @param hasLegenda
	 *            the has legenda
	 * @param hasTooltips
	 *            the has tooltips
	 * @return the buffered image
	 * @throws InfraestruturaException
	 *             the infraestrutura exception
	 */
	public static BufferedImage gerarGraficoPizza(String tituloGrafico, PieDataset mapaDados, int largura, int altura, boolean hasLegenda,
					boolean hasTooltips){

		BufferedImage buf = null;

		try {

			JFreeChart chart = ChartFactory.createPieChart(tituloGrafico, mapaDados, hasLegenda, hasTooltips, Constantes.LOCALE_PADRAO);

			chart.setBorderVisible(Boolean.TRUE);
			chart.setBorderPaint(Color.black);

			buf = chart.createBufferedImage(largura, altura);

		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestruturaException(e);
		}

		return buf;
	}

	public String getFormatoImpressaoPDF() {

		return FormatoImpressao.PDF.toString();
	}

	public String getFormatoImpressaoRTF() {

		return FormatoImpressao.RTF.toString();
	}

	public String getFormatoImpressaoXLS() {

		return FormatoImpressao.XLS.toString();
	}

}
