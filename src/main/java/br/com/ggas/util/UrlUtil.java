package br.com.ggas.util;

public class UrlUtil {
	
	private static final String URL_SERVICO_ENVIAR_SMS = "http://www.mpgateway.com/v_2_00/smspush/enviasms.aspx";
	
	public static final String SEPARADOR_SERVICO_URL_HOST_PARAMETROS = "?";
	
	public static final String SEPARADOR_SERVICO_URL_PARAMETROS = "&";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_CREDENCIAL = "CREDENCIAL=";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_PRINCIPAL_USER = "PRINCIPAL_USER=";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_AUX_USER = "AUX_USER=";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_MOBILE = "MOBILE=";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_SEND_PROJECT = "SEND_PROJECT=";
	
	private static final String URL_SERVICO_ENVIAR_SMS_ATRIBUTO_MESSAGE = "MESSAGE=";
	
	public static String montarUrlEnviarSms(
					String credential, String mainUser, String auxUser,  
					 String mobile, String sendProject, String mensagem) {

		StringBuilder sb = new StringBuilder();
		sb.append(URL_SERVICO_ENVIAR_SMS);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_CREDENCIAL, credential, true);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_PRINCIPAL_USER, mainUser, false);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_AUX_USER, auxUser, false);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_MOBILE, mobile, false);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_SEND_PROJECT, sendProject, false);
		concatenarUrlParametro(sb, URL_SERVICO_ENVIAR_SMS_ATRIBUTO_MESSAGE, mensagem, false);
		
		return sb.toString();
		
		
	}
	
	private static void concatenarUrlParametro(
					StringBuilder sb, String nomeParametro, String valorParametro, 
					boolean primeiroParametro) {
		
		if (primeiroParametro) {			
			sb.append(SEPARADOR_SERVICO_URL_HOST_PARAMETROS);
		} else {			
			sb.append(SEPARADOR_SERVICO_URL_PARAMETROS);
		}
		
		sb.append(nomeParametro);
		sb.append(valorParametro);
		
	}
	
}
