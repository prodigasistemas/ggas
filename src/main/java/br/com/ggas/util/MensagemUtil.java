/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Classe responsável por auxiliar no processo de obter, tratar e exibir as
 * mensagens do sistema.
 * 
 * @author orube
 *
 */
public class MensagemUtil {

	private static final Logger LOG = Logger.getLogger(MensagemUtil.class);

	public static final Locale LOCALE = new Locale("pt", "BR");

	public static final String STRING_NOVA_LINHA = "\r\n";

	/**
	 * Método responsável por obter mensagem a partir de uma chave passada
	 *
	 * @param chave - {@link String}
	 * @return mensagem - {@link String}
	 */
	public static String obterMensagem(String chave) {
		if (existeChave(chave)) {
			return obterMensagem(getResourcePadrao(), chave);
		}
		return chave;
	}

	/**
	 * Metodo responsavel por obter mensagem a partir da chave e dos argumentos
	 *
	 * @param chave      - {@link String}
	 * @param argumentos - {@link Object} Array
	 * @return mensagem - {@link String}
	 */
	public static String obterMensagem(String chave, Object... argumentos) {
		if (existeChave(chave)) {
			return obterMensagem(getResourcePadrao(), chave, argumentos);
		}
		return chave;
	}

	private static ResourceBundle getResourcePadrao() {

		return ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, LOCALE);
	}

	/**
	 * Método responsável por obter a mensagem do arquivo de propriedades.
	 * 
	 * @param resourceBundle O Resource Bundle
	 * @param chave          A chave
	 * @param argumentos     Os argumentos da mensagem
	 * @return String A Mensagem
	 */
	public static String obterMensagem(ResourceBundle resourceBundle, String chave, Object[] argumentos) {
		if (existeChave(chave)) {
			String mensagem = obterMensagem(resourceBundle, chave);
			for (int i = 0; i < argumentos.length; i++) {
				mensagem = Util.substituirArgumentosDoTexto(mensagem, i, argumentos[i]);
			}
			return mensagem;
		}
		return chave;
	}

	/**
	 * Método responsável por obter a mensagem do arquivo de propriedades.
	 * 
	 * @param resourceBundle O Resource Bundle
	 * @param chave          A chave
	 * @param argumento      Os argumentos da mensagem
	 * @return String A Mensagem
	 */
	public static String obterMensagem(ResourceBundle resourceBundle, String chave, Object argumento) {
		if (existeChave(chave)) {
			return obterMensagem(resourceBundle, chave, new Object[] { argumento });
		}
		return chave;
	}


	/**
	 * Método responsável por obter a mensagem do arquivo de propriedades.
	 * 
	 * @param resourceBundle O Resource Bundle
	 * @param chave          A chave
	 * @return String A mensagem
	 */
	public static String obterMensagem(ResourceBundle resourceBundle, String chave) {
		if (existeChave(chave)) {
			return resourceBundle.getString(chave);
		}
		return chave;
	}

	/**
	 * Método reponsável por obter.
	 * 
	 * @param erro the erro
	 * @return the stack trace
	 */
	public static String getStackTrace(final Throwable erro) {

		final Writer result = new StringWriter();
		LOG.error(erro.getMessage());
		return result.toString();
	}

	/**
	 * Método reponsável por retornar array de bytes das mensagens de erro geradas
	 * 
	 * @param throwable O erro
	 * @return Array de bytes da mensagem de erro
	 */
	public static byte[] gerarMensagemErroBytes(Throwable throwable) {
		return gerarMensagemErro(throwable).getBytes();
	}

	/**
	 * Método reponsável por gerar uma mensagem de erro.
	 * 
	 * @param throwable O erro
	 * @return A mensagem de erro
	 */
	public static String gerarMensagemErro(Throwable throwable) {

		String chaveErro = null;
		Object[] parametrosErro = null;
		StringBuilder mensagemErro = new StringBuilder();
		Map<String, Object> errosDeNegocio = null;
		Entry<?, ?> erro = null;

		if (throwable instanceof NegocioException) {
			chaveErro = ((NegocioException) throwable).getChaveErro();
			parametrosErro = ((NegocioException) throwable).getParametrosErro();

			errosDeNegocio = ((NegocioException) throwable).getErros();
			if (errosDeNegocio != null && !errosDeNegocio.isEmpty()) {
				for (Iterator<?> it = errosDeNegocio.entrySet().iterator(); it.hasNext();) {
					erro = (Entry<?, ?>) it.next();
					mensagemErro.append(obterMensagemErro((String) erro.getValue(), LOCALE));
					mensagemErro.append(STRING_NOVA_LINHA);
				}
			} else {
				chaveErro = ((NegocioException) throwable).getChaveErro();
				if (chaveErro != null && chaveErro.length() > 0) {
					if (parametrosErro != null && parametrosErro.length > 0) {
						Object[] parametrosErroLocalizado = new String[parametrosErro.length];
						for (int i = 0; i < parametrosErro.length; i++) {
							parametrosErroLocalizado[i] = obterMensagemErro(String.valueOf(parametrosErro[i]), LOCALE);
						}
						mensagemErro.append(MensagemUtil.obterMensagem(
								ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, LOCALE), chaveErro,
								parametrosErroLocalizado));
						mensagemErro.append(STRING_NOVA_LINHA);
					} else {
						mensagemErro.append(chaveErro);
						mensagemErro.append(STRING_NOVA_LINHA);
					}
				} else {
					if (chaveErro != null) {
						mensagemErro.append(chaveErro);
						mensagemErro.append(STRING_NOVA_LINHA);
					} else {
						mensagemErro.append(STRING_NOVA_LINHA);
						mensagemErro.append(throwable.getMessage());
						parametrosErro = throwable.getStackTrace();
						if (parametrosErro != null) {
							for (int i = 0; i < parametrosErro.length; i++) {
								mensagemErro.append(obterMensagemErro(String.valueOf(parametrosErro[i]), LOCALE));
								mensagemErro.append(STRING_NOVA_LINHA);
							}
						}
					}
				}
			}
		} else if (throwable instanceof GGASException) {
			chaveErro = ((GGASException) throwable).getChaveErro();
			if (chaveErro != null) {
				mensagemErro.append(MensagemUtil
						.obterMensagem(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, LOCALE), chaveErro));
				mensagemErro.append(STRING_NOVA_LINHA);
				parametrosErro = ((GGASException) throwable).getStackTrace();
				if (parametrosErro != null) {
					for (int i = 0; i < parametrosErro.length; i++) {
						mensagemErro.append(obterMensagemErro(String.valueOf(parametrosErro[i]), LOCALE));
						mensagemErro.append(STRING_NOVA_LINHA);
					}
				}
			} else {
				mensagemErro.append(throwable.getMessage());
				mensagemErro.append(STRING_NOVA_LINHA);
				parametrosErro = ((GGASException) throwable).getStackTrace();
				if (parametrosErro != null) {
					for (int i = 0; i < parametrosErro.length; i++) {
						mensagemErro.append(obterMensagemErro(String.valueOf(parametrosErro[i]), LOCALE));
						mensagemErro.append(STRING_NOVA_LINHA);
					}
				}
			}

		} else {
			mensagemErro.append(Constantes.ERRO_GENERICO_DE_SISTEMAS);
			parametrosErro = throwable.getStackTrace();
			if (parametrosErro != null) {
				for (int i = 0; i < parametrosErro.length; i++) {
					mensagemErro.append(obterMensagemErro(String.valueOf(parametrosErro[i]), LOCALE));
					mensagemErro.append(STRING_NOVA_LINHA);
				}
			}
		}

		// impressão log do erro
		mensagemErro.append(STRING_NOVA_LINHA);
		mensagemErro.append(STRING_NOVA_LINHA);
		mensagemErro.append(STRING_NOVA_LINHA);
		mensagemErro.append(new StringWriter().toString());

		return mensagemErro.toString();
	}

	/**
	 * Obter mensagem erro.
	 * 
	 * @param chave  the chave
	 * @param locale the locale
	 * @return the string
	 */
	public static String obterMensagemErro(String chave, Locale locale) {

		String mensagem = null;
		String chaveAux = null;
		StringBuilder stringBuilder = new StringBuilder();
		String[] arrChaves = chave.split(Constantes.STRING_VIRGULA_ESPACO);
		int countTokens = arrChaves.length;

		if (countTokens > 0) {
			for (int i = 0; i < arrChaves.length; i++) {
				chaveAux = arrChaves[i];
				try {
					if (locale != null) {
						mensagem = MensagemUtil.obterMensagem(
								ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, locale), chaveAux);
					} else {
						mensagem = chaveAux;
					}
				} catch (MissingResourceException e) {
					LOG.error(e.getMessage(), e);
					mensagem = chaveAux;
				}
				stringBuilder.append(mensagem);
				if (countTokens > 1) {
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				countTokens--;
			}
		} else {
			if (locale != null) {
				try {
					mensagem = MensagemUtil
							.obterMensagem(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, locale), chave);
				} catch (MissingResourceException e) {
					LOG.error(e.getMessage(), e);
					mensagem = chave;
				}
				stringBuilder.append(mensagem);
			} else {
				stringBuilder.append(chave);
			}
		}

		return stringBuilder.toString();
	}

	/**
	 * Método responsável por validar se a chave existe no arquivo de propriedades.
	 * 
	 * @param chave A chave
	 * @return true caso exista ou false caso contrário
	 */
	public static boolean existeChave(String chave) {

		boolean retorno = true;

		try {
			ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, LOCALE).getString(chave);
		} catch (MissingResourceException e) {
			LOG.error("Falha ao obter valor da chave", e);
			retorno = false;
		}
		return retorno;
	}

	/**
	 * Montar lista erros.
	 *
	 * @param msgErros the msg erros
	 * @return the list
	 */
	public static List<String> montarListaErros(String... msgErros) {

		List<String> listaErros = new ArrayList<>();

		for (String erro : msgErros) {
			if (erro != null) {
				listaErros.add(erro);
			}
		}

		return listaErros;

	}

}
