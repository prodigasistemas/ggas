/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.api.autenticador.Autenticador;

/**
 * Filtro da API
 *
 */
public class ApiFilter implements Filter {

	private static final String AUTH_HEADER = "Authorization";
	private static final Logger LOG = Logger.getLogger(ApiFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		Boolean possuiAutorizacao = Boolean.FALSE;
		String recurso = this.getRecurso((HttpServletRequest) request);

		if (recurso.contains("api/v1/login")) {
			possuiAutorizacao = Boolean.TRUE;
		} else {
			possuiAutorizacao = verificarTokenRequest(request);
			if (!possuiAutorizacao) {
				HttpServletResponse res = (HttpServletResponse) response;
				res.sendError(403);
			}
		}

		if (possuiAutorizacao) {
			chain.doFilter(request, response);
		}
	}
	
	/**
	 * Retorna o recurso.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @return recurso
	 */
	private String getRecurso(HttpServletRequest request) {
		return request.getRequestURI().replace(request.getContextPath(), "");
	}

	/**
	 * Verifica se o token e valido
	 * 
	 * @param request {@link ServletRequest}
	 * @return true, se for valido
	 */
	private boolean verificarTokenRequest(ServletRequest request) {
		Boolean possuiTokenValido = Boolean.FALSE;

		try {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			String authorization = httpRequest.getHeader(AUTH_HEADER);
			if (StringUtils.isNotBlank(authorization)
					&& Autenticador.getInstance().isTokenValid(authorization.split(" ")[1])) {
				possuiTokenValido = Boolean.TRUE;
			}
		} catch (Exception e) {
			LOG.error("Erro ao verificar token do request", e);
		}

		return possuiTokenValido;
	}

	@Override
	public void destroy() {
		// Método em construção
	}

}
