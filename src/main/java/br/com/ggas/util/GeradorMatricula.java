/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.exception.JDBCExceptionHelper;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.PersistentIdentifierGenerator;
import org.hibernate.type.Type;

import br.com.ggas.cobranca.boletobancario.codigobarras.DigitoAutoConferencia;
/**
 * Classe de gerador de matrículas
 *
 */
public class GeradorMatricula implements IdentifierGenerator, Configurable {

	private static final Logger LOG = Logger.getLogger(GeradorMatricula.class);

	private static final String COULD_NOT_FETCH_INITIAL_VALUE_FOR_INCREMENT_GENERATOR = 
					"could not fetch initial value for increment generator";

	private String colunaPK;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.id.IdentifierGenerator#generate(org.hibernate.engine.
	 * SessionImplementor, java.lang.Object)
	 */
	@Override
	public Serializable generate(SessionImplementor sessionImplemetor, Object object) throws HibernateException {

		return gerarMatricula(sessionImplemetor, colunaPK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.hibernate.id.Configurable#configure(org.hibernate.type.Type,
	 * java.util.Properties, org.hibernate.dialect.Dialect)
	 */
	@Override
	public synchronized void configure(Type type, Properties params, Dialect d) throws MappingException {

		colunaPK = params.getProperty(PersistentIdentifierGenerator.PK);
	}

	/**
	 * Gerar matricula.
	 * 
	 * @param session
	 *            the session
	 * @param colunaPK
	 *            the coluna pk
	 * @return the long
	 */
	private Long gerarMatricula(SessionImplementor session, String colunaPK) {

		Long identificador = this.obterIdentificador(session, colunaPK);

		if (identificador != null && identificador > 0) {
			long digitoVerificador = DigitoAutoConferencia.modulo11(String.valueOf(identificador.longValue()));
			String matricula = String.valueOf(identificador.longValue()) + String.valueOf(digitoVerificador);
			identificador = Long.parseLong(matricula);
		}

		return identificador;
	}

	/**
	 * Obter identificador.
	 * 
	 * @param session
	 *            the session
	 * @param colunaPK
	 *            the coluna pk
	 * @return the long
	 */
	private Long obterIdentificador(SessionImplementor session, String colunaPK) {

		Long sequence = null;
		String sql = Util.substituirArgumentosDoTexto(Constantes.GERADOR_MATRICULA_SQL_NEXT_ID, 0, "SQ_" + colunaPK);

		try {
			PreparedStatement preparedStatement = session.getBatcher().prepareSelectStatement(sql);
			try {
				ResultSet resultSet = preparedStatement.executeQuery();
				try {
					while (resultSet.next()) {
						sequence = resultSet.getLong("sequence");
					}
				} finally {
					resultSet.close();
				}
			} finally {
				session.getBatcher().closeStatement(preparedStatement);
			}
		} catch (SQLException sqle) {
			throw JDBCExceptionHelper.convert(session.getFactory().getSQLExceptionConverter(), sqle,
					COULD_NOT_FETCH_INITIAL_VALUE_FOR_INCREMENT_GENERATOR, sql);
		}

		if (sequence == null || sequence <= 0) {
			throw new JDBCException(COULD_NOT_FETCH_INITIAL_VALUE_FOR_INCREMENT_GENERATOR, new SQLException(sql));
		}

		return sequence;
	}

}
