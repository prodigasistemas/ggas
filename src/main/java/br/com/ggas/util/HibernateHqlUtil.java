/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;

/**
 * @author ccavalcanti
 */
public final class HibernateHqlUtil {

	/**
	 * Paginar consulta por hql.
	 * 
	 * @param colecaoPaginada
	 *            the colecao paginada
	 * @param query
	 *            the query
	 * @param classe
	 *            the classe
	 * @param hqlAuxiliar
	 *            the hql auxiliar
	 * @return the string
	 */
	public static String paginarConsultaPorHQL(ColecaoPaginada colecaoPaginada, Query query, Object classe, String hqlAuxiliar) {

		String auxiliar = null;
		if(colecaoPaginada != null) {
			int totalLinhas = obterTotalDeLinhasPorHQL(query);
			colecaoPaginada.setFullListSize(totalLinhas);

			if(colecaoPaginada.getOrdenacaoEspecial() != null
							&& colecaoPaginada.getOrdenacaoEspecial().containsKey(colecaoPaginada.getSortCriterion())) {
				auxiliar = colecaoPaginada.getOrdenacaoEspecial().get(colecaoPaginada.getSortCriterion())
								.addOrder(hqlAuxiliar, colecaoPaginada.getSortDirection(), classe);
			}
		}
		return auxiliar;
	}

	/**
	 * Obter total de linhas por hql.
	 * 
	 * @param query
	 *            the query
	 * @return the integer
	 */
	public static Integer obterTotalDeLinhasPorHQL(Query query) {

		int totalElements = 0;

		totalElements = query.list().size();

		return totalElements;
	}

	/**
	 * Adiciona a clausula where ou and a entrada.
	 * 
	 * @param hql
	 *            query hql
	 */
	public static void adicionarClausulaWhereOuAnd(StringBuilder hql) {

		if(!hql.toString().contains("where")) {
			hql.append(" where ");
		} else {
			hql.append(" and ");
		}
	}

	/**
	 * Adiciona a clausula "IN" a entrada e retorna suas propriedades
	 * 
	 * @param hql texto da query
	 * @param propriedade propriedade de comparacao
	 * @param prefixo prefixo
	 * @param lista lista de itens a serem aplicados
	 * @return Map mapa com todas as propreidades e valores 
	 */
	public static <T extends Object> Map<String, List<T>> adicionarClausulaIn(StringBuilder hql, String propriedade, String prefixo,
			List<T> lista) {
		Map<String, List<T>> retornoPropriedades = new HashMap<>();
		if (hql != null && StringUtils.isNotBlank(propriedade) && StringUtils.isNotBlank(prefixo) && CollectionUtils.isNotEmpty(lista)) {
			Integer totalCondicoes = Util.obterTotalDivisoesDecomposicao(lista.size(), 999);
			String clausulaIn = " $propriedade IN (:$tokenValor) ".replace("$propriedade", propriedade);
			String token = "TOKEN_"+prefixo+"_";

			hql.append(" ( ");
			for (int i = 0; i < totalCondicoes; i++) {
				String tokenIndex = token+Util.adicionarZerosEsquerdaNumero(String.valueOf(i), 3);
				if (i != 0) {
					hql.append(" OR ");
				}
				hql.append(clausulaIn.replace("$tokenValor", tokenIndex));

				if(lista.size() >= 999) {
					List<T> sublista = lista.subList(0, 1000);
					lista = ListUtils.subtract(lista, sublista);
					retornoPropriedades.put(tokenIndex, sublista);
				}else {
					retornoPropriedades.put(tokenIndex, lista);
				}

			}
			hql.append(" ) ");
		}
		return retornoPropriedades;
	}
	
	
	/**
	 * Adiciona as propriedades do "IN" a query
	 * 
	 * @param query objeto da query construida hql
	 * @param mapPropriedades mapa com a relação entre chaves e valores das propriedades
	 */
	public static <T extends Object> void adicionarPropriedadesClausulaIn(Query query, Map<String, List<T>> mapPropriedades) {
		if (MapUtils.isNotEmpty(mapPropriedades)) {
			for (Iterator<Entry<String, List<T>>> it = mapPropriedades.entrySet().iterator(); it.hasNext();) {
				Entry<String, List<T>> propriedade = (Entry<String, List<T>>) it.next();
				query.setParameterList(propriedade.getKey(), propriedade.getValue());
			}
		}
	}
}
