package br.com.ggas.util.tarefaassincrona;

import br.com.ggas.atendimento.chamado.dominio.Chamado;

public class TarefaAssincronaUtil {
	
	public static void enviarProtocolo(Chamado chamado, String mensagem) {
		
		EnviarProtocoloTA enviarProtocolo = new EnviarProtocoloTA();
		enviarProtocolo.setChamado(chamado);
		enviarProtocolo.setMensagem(mensagem);
		
		Thread thread = new Thread(enviarProtocolo);
		thread.start();
		
	}
	
}
