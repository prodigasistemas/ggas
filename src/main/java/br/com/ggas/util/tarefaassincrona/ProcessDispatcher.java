package br.com.ggas.util.tarefaassincrona;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável por disparar a execução de um processo batch do ggas em um
 * processo do SO.
 * 
 * @author orube
 *
 */
public class ProcessDispatcher {

	private static final int STATUS_FALHA = 3;
	private static String MSG_FALHA_EXECUCAO_PROCESSO = "Ocorreu um erro e não foi possível "
			+ "executar o processo em paralelo. Verifique o acesso ao arquivo .jar necessário para execução do processo.";
	private static final Logger LOG = Logger.getLogger(ProcessDispatcher.class);

	private ResourceBundle hibernateProperties;
	private ResourceBundle constantesProperties;

	/**
	 * Construtor de {@link ProcessDispatcher}.
	 */
	public ProcessDispatcher() {
		hibernateProperties = ResourceBundle.getBundle("hibernate");
		constantesProperties = ResourceBundle.getBundle("constantes");
	}

	/**
	 * Dispara um processo batch do ggas que executa através de um .jar em um
	 * processo do SO.
	 * 
	 * @param processId - {@link String}
	 */
	public void dispatch(String processId) {

		String url = hibernateProperties.getString("hibernate.connection.url");
		String user = hibernateProperties.getString("hibernate.connection.username");
		String pass = hibernateProperties.getString("hibernate.connection.password");

		String jarDestinationPath = String.format("%s/%s", constantesProperties.getString("PATH_GGAS_JAR"), "ggas.jar");
		String jarXmxArg = String.format("-Xmx%sm", constantesProperties.getString("XMX_ARG_GGAS_JAR"));

		ProcessBuilder processBuilder = new ProcessBuilder();

		processBuilder.command("java", jarXmxArg, "-jar", jarDestinationPath, user, pass, url, processId);
		try {

			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream(), Charset.defaultCharset()));

			String line;
			while ((line = reader.readLine()) != null) {
				LOG.info(String.format("Processo %s - ", processId) + line);
			}

			int exitCode = process.waitFor();
			LOG.info("\nExited with error code : " + exitCode);

			reader.close();

			atualizarStatusProcesso(processId, exitCode);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			atualizarStatusProcesso(processId, MensagemUtil.gerarMensagemErro(e));
		}

	}

	private void atualizarStatusProcesso(String id, int exitCode) {
		try {
			if (exitCode != 0) {
				this.atualizarStatusProcesso(id, MSG_FALHA_EXECUCAO_PROCESSO);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private void atualizarStatusProcesso(String id, String mensagem) {
		try {
			ControladorProcesso controladorProcesso = ServiceLocator.getInstancia().getControladorProcesso();
			Processo processo = (Processo) controladorProcesso.obter(Long.parseLong(id));
			processo.setSituacao(STATUS_FALHA);
			processo.setLogErro(mensagem.getBytes(Charset.defaultCharset()));
			controladorProcesso.atualizar(processo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
}
