package br.com.ggas.util.tarefaassincrona;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.UrlUtil;

public class EnviarProtocoloTA implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(EnviarProtocoloTA.class);

	private static final String ASSUNTO_EMAIL_PROTOCOLO_CHAMADO = "Número do Protocolo do Chamado.";

	private Chamado chamado;
	
	private String mensagem;

	@Override
	public void run() {
		try {
			
			enviarProtocoloEmail(chamado, mensagem);
			enviarProtocoloSms(chamado, mensagem);
			
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);			
		}
	}

	private ControladorParametroSistema getControladorParametroSistema() {

		return ServiceLocator.getInstancia().getControladorParametroSistema();
	}

	private ControladorUsuario getControladorUsuario() {

		return ServiceLocator.getInstancia().getControladorUsuario();
	}

	/**
	 * 
	 * @param chamado
	 * @param mensagemSMS
	 * @throws NegocioException
	 * @throws IOException
	 */
	private void enviarProtocoloSms(Chamado chamado, String mensagemSMS)
					throws NegocioException, IOException {
		
		String mensagem = mensagemSMS;
		if (getControladorParametroSistema().obterValorParametroEnviaSms()) {
			
			String auxUser = definirSmsAuxUser();
			definirHostPortaSms();
			
			ParametroSistema credencialSms = getControladorParametroSistema()
							.obterParametroCredencialSms();
			
			ParametroSistema projetoSms = getControladorParametroSistema()
							.obterParametroProjetoSms();
			
			if (credencialSms != null && projetoSms != null 
							&& !credencialSms.getValor().isEmpty() 
							&& !projetoSms.getValor().isEmpty()
							&& StringUtils.isNotEmpty(chamado.getTelefoneSolicitante())) {
				
				String credential = credencialSms.getValor();
				String mainUser = projetoSms.getValor();
				String mobile = "55" + chamado.getTelefoneSolicitante();
				String sendProject = "S";

				String mensagemTratada = URLEncoder.encode(mensagem, "UTF-8");
				
				String connection = UrlUtil.montarUrlEnviarSms(
								credential, mainUser, auxUser, 
								mobile, sendProject, mensagemTratada);
				
				URL url = new URL(connection);
				URLConnection con = url.openConnection();
				con.setConnectTimeout(100000);
				InputStream input = con.getInputStream();

				byte[] b = new byte[4];
				if (input.read(b, 0, b.length) > -1) { 
					String statusCode = new String(b);
					System.out.println(statusCode);
				}
			}
		}
	}

	private void definirHostPortaSms() throws NegocioException {

		ParametroSistema proxyHost = getControladorParametroSistema()
						.obterParametroProxyHostSms();
		
		ParametroSistema proxyPorta = getControladorParametroSistema()
						.obterParametroProxyPortaSms();
		
		if (proxyHost != null && proxyPorta != null 
						&& !proxyHost.getValor().isEmpty() 
						&& !proxyPorta.getValor().isEmpty()) {
			System.setProperty("http.proxyHost", proxyHost.getValor());
			System.setProperty("http.proxyPort", proxyPorta.getValor());
		}
	}

	private String definirSmsAuxUser() throws NegocioException {

		ParametroSistema usuarioAuxSms = getControladorParametroSistema()
						.obterParametroUsuarioAuxiliarSms();
		
		if (usuarioAuxSms != null && !usuarioAuxSms.getValor().isEmpty()) {
			return usuarioAuxSms.getValor();
		}
		return StringUtils.EMPTY;
	}

	private void enviarProtocoloEmail(Chamado chamado, String mensagem) 
					throws Exception {

		String emailSolicitante = chamado.getEmailSolicitante();
		
		if (StringUtils.isNotEmpty(emailSolicitante)) {
			
			String obterValorParametroEmailRemetentePadrao = 
							getControladorParametroSistema().obterValorParametroEmailRemetentePadrao();
			
			try {
				getControladorUsuario().enviarEmail(obterValorParametroEmailRemetentePadrao, emailSolicitante,
						ASSUNTO_EMAIL_PROTOCOLO_CHAMADO, mensagem);
				
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
				throw new Exception("Operação realizada com sucesso " + mensagem 
								+ ". Verificar configuções no servidor de e-mail!");

			}
		}
	}
	
	/**
	 * @return the chamado
	 */
	public Chamado getChamado() {
	
		return chamado;
	}
	
	/**
	 * @param chamado the chamado to set
	 */
	public void setChamado(Chamado chamado) {
	
		this.chamado = chamado;
	}
	
	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
	
		return mensagem;
	}
	
	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
	
		this.mensagem = mensagem;
	}
	
}
