/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.log4j.Logger;

import br.com.ggas.controleacesso.Operacao;

public class RequestWrapper extends HttpServletRequestWrapper {

	private static final String ATRIBUTO_OPERACAO = "operacao";

	private static final String ARQUIVO_PROPRIEDADES_CAMPOS_EXCECAO_XSS = "camposExcecaoXss.properties";

	private static Map<String, String> mapaCamposExcecaoXss;

	private static final Logger LOG = Logger.getLogger(RequestWrapper.class);

	static {
		inicializarPropriedadesCamposExcecaoXSS();
	}
	
	/**
	 * Instantiates a new request wrapper.
	 * 
	 * @param servletRequest
	 *            the servlet request
	 */
	public RequestWrapper(HttpServletRequest servletRequest) {

		super(servletRequest);
	}

	/**
	 * Método responsável por inicializar o mapa
	 * de código do arquivo
	 * operacaoSistema.properties
	 * 
	 * @return void
	 */
	private static void inicializarPropriedadesCamposExcecaoXSS() {

		try {
			Properties propriedades = new Properties();
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES_CAMPOS_EXCECAO_XSS);
			propriedades.load(stream);
			mapaCamposExcecaoXss = new HashMap<String, String>();
			for (Entry<Object, Object> entry : propriedades.entrySet()) {
				mapaCamposExcecaoXss.put(String.valueOf(entry.getKey()).trim(), String.valueOf(entry.getValue()).trim());
			}
			stream.close();
		} catch(Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}

	/**
	 * Obter campos excecao xss por operacao.
	 * 
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the string[]
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso#
	 * obterChaveOperacaoSistema
	 * (br.com.ggas.controleacesso
	 * .impl.ChaveOperacaoSistema operacao)
	 */
	public String[] obterCamposExcecaoXssPorOperacao(Long chaveOperacao) {

		String campos = mapaCamposExcecaoXss.get(chaveOperacao.toString());
	
		if (campos == null) {
			return null;
		} else {
			return campos.split(";");
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.ServletRequestWrapper#getParameterValues(java.lang.String)
	 */
	@Override
	public String[] getParameterValues(String parameter) {

		String[] values = super.getParameterValues(parameter);
		if(values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		boolean filtroSimples = isFiltroSimplesXSS(parameter);

		for (int i = 0; i < count; i++) {
			if(filtroSimples) {
				encodedValues[i] = simpleCleanXSS(values[i]);
			} else {
				encodedValues[i] = cleanXSS(values[i]);
			}
		}

		return encodedValues;
	}

	/**
	 * Checks if is filtro simples xss.
	 * 
	 * @param campo
	 *            the campo
	 * @return true, if is filtro simples xss
	 */
	private boolean isFiltroSimplesXSS(String campo) {

		Object operacao = this.getAttribute(ATRIBUTO_OPERACAO);
		boolean isDesabilita = false;
		if(operacao instanceof Operacao) {
			Long chavePrimariaOperacao = ((Operacao) operacao).getChavePrimaria();
			if(mapaCamposExcecaoXss.containsKey(chavePrimariaOperacao.toString())) {
				String[] campos = obterCamposExcecaoXssPorOperacao(chavePrimariaOperacao);
				if(campos != null) {
					for (String string : campos) {
						if(string.equals(campo)) {
							isDesabilita = true;
							break;
						}
					}
				}
			}
		}
		return isDesabilita;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.ServletRequestWrapper#getParameter(java.lang.String)
	 */
	@Override
	public String getParameter(String parameter) {

		String value = super.getParameter(parameter);
		if(value == null) {
			return null;
		}
		return cleanXSS(value);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServletRequestWrapper#getHeader(java.lang.String)
	 */
	@Override
	public String getHeader(String name) {

		String value = super.getHeader(name);
		if(value == null) {
			return null;
		}
		return cleanXSS(value);

	}

	/**
	 * Clean xss.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	private String cleanXSS(String value) {

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22

		simpleCleanXSS(value);
		value = value.replaceAll("<", "").replaceAll(">", "");
		value = value.replaceAll("\\(", "").replaceAll("\\)", "");
		value = value.replaceAll("'", "");
		return value;
	}

	/**
	 * Simple clean xss.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	private String simpleCleanXSS(String value) {

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22

		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("(?i)script", "");
		return value;
	}

}

