/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoAnexo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoUnidadeOrganizacional;
import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Classe utilitária da entidade de chamados
 * @author jose.victor@logiquesistemas.com.br
 */
public final class ChamadoUtil {

	private ChamadoUtil() {
	}

	/**
	 * Duplica as informações de um chamado em uma nova instância de chamado.
	 * Esse método é útil para criação de chamados em lote
	 * @param chamado chamado a ser duplicado
	 * @return retorna um novo chamado com as informações do chamado
	 */
	public static Chamado duplicarChamado(Chamado chamado) {
		Chamado novoChamado = new Chamado();
		novoChamado.setChamadoAssunto(chamado.getChamadoAssunto());
		novoChamado.setCanalAtendimento(chamado.getCanalAtendimento());
		novoChamado.setDescricao(chamado.getDescricao());
		novoChamado.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional());
		novoChamado.setChamadoTipo(chamado.getChamadoTipo());
		novoChamado.setListaUnidadeOrganizacionalVisualizadora(
				Optional.ofNullable(chamado.getListaUnidadeOrganizacionalVisualizadora()).orElse(new HashSet<>())
						.stream().map(u -> duplicarChamadoUnidade(novoChamado, u))
						.collect(Collectors.toSet())
		);
		novoChamado.setListaServicoAutorizacao(Optional.ofNullable(chamado.getListaServicoAutorizacao())
				.orElse(new HashSet<>()));
		novoChamado.setDataPrevisaoEncerramento(chamado.getDataPrevisaoEncerramento());
		novoChamado.setIndicadorPesquisa(chamado.isIndicadorPesquisa());
		novoChamado.setQuantidadeReabertura(0);
		novoChamado.setQuantidadeReativacao(0);
		novoChamado.setQuantidadeReiteracao(0);
		novoChamado.setQuantidadeTentativaContato(0);
		novoChamado.setIndicadorAgenciaReguladora(Boolean.FALSE);
		novoChamado.setUsuarioResponsavel(chamado.getUsuarioResponsavel());
		novoChamado.setListaAnexos(Optional.ofNullable(chamado.getListaAnexos())
				.map(Sets::newHashSet).orElse(new HashSet<>()).stream().map(a -> duplicarAnexos(a, novoChamado))
				.collect(Collectors.toSet()));

		novoChamado.setIndicadorUnidadesOrganizacionalVisualizadora(chamado.getIndicadorUnidadesOrganizacionalVisualizadora());
		return novoChamado;
	}

	/**
	 * Duplica entidades de anexo
	 * @param c chamado historico anexo a ser duplicado
	 * @param chamado chamado a ser utilizado como referencia
	 * @return retorna nova instancia
	 */
	private static ChamadoHistoricoAnexo duplicarAnexos(ChamadoHistoricoAnexo c, Chamado chamado) {
		final ChamadoHistoricoAnexo chamadoHistoricoAnexo = new ChamadoHistoricoAnexo();
		chamadoHistoricoAnexo.setChamado(chamado);
		chamadoHistoricoAnexo.setHabilitado(true);
		chamadoHistoricoAnexo.setDescricaoAnexo(c.getDescricaoAnexo());
		chamadoHistoricoAnexo.setDocumentoAnexo(c.getDocumentoAnexo());
		chamadoHistoricoAnexo.setNomeArquivo(c.getNomeArquivo());
		chamadoHistoricoAnexo.setUltimaAlteracao(c.getUltimaAlteracao());
		return chamadoHistoricoAnexo;
	}

	/**
	 * Duplica um {@link ChamadoUnidadeOrganizacional} com um na instância
	 * @param novoChamado chamado a ser usado como referencia
	 * @param u chamado unidade organizacional a ser duplicado
	 * @return retorna uma nova instancia do chamado
	 */
	private static ChamadoUnidadeOrganizacional duplicarChamadoUnidade(Chamado novoChamado, ChamadoUnidadeOrganizacional u) {
		final ChamadoUnidadeOrganizacional chamadoUnidade = new ChamadoUnidadeOrganizacional();
		chamadoUnidade.setChamado(novoChamado);
		chamadoUnidade.setUnidadeOrganizacional(u.getUnidadeOrganizacional());
		chamadoUnidade.setHabilitado(true);
		return chamadoUnidade;
	}

}
