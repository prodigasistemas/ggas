
package br.com.ggas.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;

/**
 * Classe responsável por tratar números e converter para String
 *
 */
public class NumeroUtil {

	public static final Long LONG_ZERO = Long.valueOf(0L);
	
	public static final Integer INTEGER_ZERO = Integer.valueOf(0);

	public static final BigDecimal BIG_DECIMAL_CEM = BigDecimal.valueOf(100);

	/**
	 * verifica se a entrada e maior que zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true se valor > 0, false caso contrario
	 */
	public static boolean maiorQueZero(BigDecimal valor) {

		if (valor != null) {
			return valor.compareTo(BigDecimal.ZERO) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * verifica se a entrada e maior que zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true se valor > 0, false caso contrario
	 */
	public static boolean maiorQueZero(Long valor) {

		if (valor != null) {
			return valor.compareTo(LONG_ZERO) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * verifica se a entrada não é nula e maior que zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true nao for mulo e valor > 0, false caso contrario
	 */
	public static boolean naoNulo(BigDecimal valor) {

		if (valor != null) {
			return maiorQueZero(valor);
		}
		return false;
	}

	/**
	 * verifica se a entrada não é nula e maior que zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true nao for mulo e valor > 0, false caso contrario
	 */
	public static boolean naoNulo(Long valor) {

		if (valor != null) {
			return maiorQueZero(valor);
		}
		return false;
	}

	/**
	 * verifica se a entrada é nula ou igual a zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true nao for mulo e valor > 0, false caso contrario
	 */
	public static boolean nulo(BigDecimal valor) {

		return !naoNulo(valor);
	}

	/**
	 * Nulo.
	 *
	 * @param valor the valor
	 * @return true, if successful
	 */
	public static boolean nulo(Long valor) {

		return !naoNulo(valor);
	}

	/**
	 * verifica se a entrada e menor que zero
	 * 
	 * @param valor valor a ser verificado
	 * @return true se valor < 0, false caso contrario
	 */
	public static boolean menorQueZero(BigDecimal valor) {

		if (valor != null) {
			return valor.compareTo(BigDecimal.ZERO) < 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Verifica se valor1 e menor que valor2
	 * 
	 * @param valor1 primeiro operando do menor que
	 * @param valor2 segundo operando do menor que
	 * @return valor1 < valor2
	 */
	public static boolean menorQue(BigDecimal valor1, BigDecimal valor2) {

		if (valor1 != null && valor2 != null) {
			return valor1.compareTo(valor2) < 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Verifica se valor1 e maior que valor2
	 * 
	 * @param valor1 primeiro operando do maior que
	 * @param valor2 segundo operando do maior que
	 * @return valor1 > valor2
	 */
	public static boolean maiorQue(BigDecimal valor1, BigDecimal valor2) {

		if (valor1 != null && valor2 != null) {
			return valor1.compareTo(valor2) > 0;
		}
		throw new IllegalArgumentException(Constantes.OPERANDO_NULO);
	}

	/**
	 * Método responsável por converter um string
	 * para um valor decimal.
	 *
	 * @param rotulo
	 *            O rótulo do campo
	 * @param valor
	 *            O valor
	 * @param locale
	 *            O Locale da aplicação
	 * @return Uma valor
	 * @throws GGASException
	 *             Caso ocorra algum erro de
	 *             conversão ou formato
	 */
	public static String converterCampoValorDecimalParaString(BigDecimal valor) {

		return converterCampoValorDecimalParaString(valor, null);
	}

	/**
	 * Método responsável por converter um valor decimal para String
	 * 
	 * @param valor - {@link BigDecimal}
	 * @param numeroDecimais - {@link Integer}
	 * @return valor convertido - {@link String}
	 */
	public static String converterCampoValorDecimalParaString(BigDecimal valor, Integer numeroDecimais) {

		if (valor != null) {
			DecimalFormat decimalFormat = obterDecimalFormatLocalizado(Constantes.LOCALE_PADRAO, numeroDecimais);
			return decimalFormat.format(valor);
		}

		return StringUtils.EMPTY;

	}

	/**
	 * Metodo que soma zero ou mais parcelas do tipo BigDecimal,
	 * caso a parcela seja nula ela será considerada elemento neutro.
	 * 
	 * @param parcelas as parcelas a serem somadas
	 * @return a soma das parcelas
	 */
	public static BigDecimal soma(BigDecimal... parcelas) {

		BigDecimal soma = BigDecimal.ZERO;
		for (BigDecimal parcela : parcelas) {
			if (parcela != null) {
				soma = soma.add(parcela);
			}
		}
		return soma;
	}

	/**
	 * metodo que obtem um Locale para Formatacao
	 * de Decimais baseado em Localidade e numero
	 * de
	 * Digitos decimais
	 *
	 * @param locale
	 * @param numeroDecimais
	 * @return
	 */
	private static DecimalFormat obterDecimalFormatLocalizado(Locale locale, Integer numeroDecimais) {

		DecimalFormat decimalFormat = new DecimalFormat(Constantes.FORMATO_VALOR_NUMERO, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);
		decimalFormat.setDecimalSeparatorAlwaysShown(true);
		if (numeroDecimais == null) {
			decimalFormat.setMinimumFractionDigits(Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
		} else {
			decimalFormat.setMinimumFractionDigits(numeroDecimais);
			decimalFormat.setMaximumFractionDigits(numeroDecimais);
		}

		return decimalFormat;
	}

	/**
	 * Metodo que retorna o maior dentre os argumentos.
	 * Obs.: valores nulos serao tratados como zero.
	 * 
	 * @param valores valores de entrada para se obter o maior
	 * @return o maior valor
	 */
	public static BigDecimal maior(BigDecimal... valores) {

		BigDecimal maximo = BigDecimal.ZERO;
		for (BigDecimal valor : valores) {
			if (valor != null && NumeroUtil.maiorQue(valor, maximo)) {
				maximo = valor;
			}
		}
		return maximo;
	}

	/**
	 * Método responsável por converter um string no formato #.###,### para um big decimal.
	 * 
	 * @param strValor O valor
	 * @return O valor Big Decimal
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de conversão ou formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(String strValor)
					throws FormatoInvalidoException {

		return converterCampoStringParaValorBigDecimal(null, strValor);
	}

	/**
	 * Método responsável por converter um string no formato #.###,### para um big decimal.
	 * 
	 * @param rotuloO rotulo do campo
	 * @param strValor O valor
	 * @return O valor Big Decimal
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de conversão ou formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(
					String rotulo, String strValor) throws FormatoInvalidoException {

		if (strValor != null) {
			return converterCampoStringParaValorBigDecimal(
							rotulo, strValor, Constantes.FORMATO_VALOR_NUMERO, 
							Constantes.LOCALE_PADRAO);
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Método responsável por converter um string para um big decimal.
	 * 
	 * @param rotulo
	 *            O rotulo do campo
	 * @param strValor
	 *            O valor
	 * @param formato
	 *            O formato
	 * @param locale
	 *            O Locale da aplicação
	 * @return Uma valor
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de
	 *             conversão ou
	 *             formato
	 */
	public static BigDecimal converterCampoStringParaValorBigDecimal(
					String rotulo, String strValor, String formato, Locale locale)
									throws FormatoInvalidoException {

		BigDecimal valor = null;
		DecimalFormat decimalFormat = new DecimalFormat(
						formato, new DecimalFormatSymbols(locale));
		decimalFormat.setParseBigDecimal(true);

		try {
			valor = (BigDecimal) decimalFormat.parse(strValor);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS, rotulo);
		}

		return valor;
	}

	/**
	 * Converter string formato ingles para valor big decimal.
	 *
	 * @param strValor the str valor
	 * @return the big decimal
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	public static BigDecimal converterStringFormatoInglesParaValorBigDecimal(String strValor) throws FormatoInvalidoException {

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);
		try {
			return (BigDecimal) decimalFormat.parse(strValor);
		} catch (ParseException e) {
			throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS);
		}
	}

	/**
	 * Método responsável por converter um valor
	 * BigDecimal real para um valor BigDecimal em
	 * percentual .
	 *
	 * @param valor
	 *            O valor a ser transformado
	 * @return Um valor BigDecimal em percentual
	 */
	public static BigDecimal converterValorAbsolutoParaPercentual(
					BigDecimal valor) {

		if (valor != null) {
			return valor.multiply(BIG_DECIMAL_CEM);
		}

		return null;
	}
	
	/**
	 * Método responsável por converter um valor
	 * percentual para um valor absoluto.
	 *
	 * @param valor the big decimal
	 * @return the big decimal
	 */
	public static BigDecimal converterPercentualParaValorAbsoluto(
					BigDecimal valor) {

		if (valor != null) {
			return valor.divide(BIG_DECIMAL_CEM);
		}

		return null;
	}

	/**
	 * Converter percentual para big decimal string.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String converterPercentualParaBigDecimalString(BigDecimal valor) {

		return converterCampoValorDecimalParaString(converterPercentualParaValorAbsoluto(valor));
	}

	/**
	 * Extrair fator de multiplicacao de percentual.
	 *
	 * @param valor the valor
	 * @return the big decimal
	 */
	public static BigDecimal calcularFatorMultiplicacaoDePercentual(BigDecimal valor) {

		return valor.divide(BIG_DECIMAL_CEM).add(BigDecimal.ONE);
	}

	/**
	 * Adicionar percentual.
	 *
	 * @param valor the valor
	 * @param percentual the percentual
	 * @return the big decimal
	 */
	public static BigDecimal adicionarPercentual(BigDecimal valor, BigDecimal percentual) {

		return valor.multiply(calcularFatorMultiplicacaoDePercentual(percentual));
	}
	
	/**
	 * Equals.
	 *
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @return true, if successful
	 */
	public static boolean iguais(Long arg1, Long arg2) {
		
		return arg1.equals(arg2);
	}
	
	/**
	 * Verifica se arg1 e igual a pelo menos um entre args.
	 *
	 * @param arg1 the arg1
	 * @param args the args
	 * @return true, if successful
	 */
	public static boolean igualAlgum(Long arg1, Long... args) {
		
		for (int i = 0; i < args.length; i++) {			
			if (arg1.equals(args[i])) {
				return true;
			}
		}
		
		return false;
		
	}
	
	/**
	 * Converte uma {@link String} em um {@link Long}. Caso ocorra um {@link NumberFormatException} na conversão, o valor retornado é o
	 * valor do argumento {@code padrao}.
	 * 
	 * @param s - string a ser convertida
	 * @param padrao - valor a ser retornado caso ocorra um {@link NumberFormatException}
	 * @return um objeto {@link Long} representando o argumento string, ou o argumento {@code padrao}
	 */
	public static Long stringParaLong(String s, Long padrao) {
		Long resultado = null;
		try {
			resultado = Long.valueOf(s);
		} catch (NumberFormatException e) {
			resultado = padrao;
		}
		return resultado;
	}

}
