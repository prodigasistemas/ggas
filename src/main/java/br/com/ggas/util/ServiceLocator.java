/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

/**
 * 
 * 
 */

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipeTurno;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.acao.negocio.ControladorAcao;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.dadocensitario.ControladorSetorCensitario;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.operacional.ControladorRamal;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.veiculo.ControladorVeiculo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.negativacao.ControladorNegativacao;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public final class ServiceLocator implements ApplicationContextAware {

	public static final String BEAN_ID_SERVICE_LOCATOR = "serviceLocator";

	private static ServiceLocator instancia = new ServiceLocator();

	private ApplicationContext applicationContext;

	/**
	 * Construtor padrão.
	 */
	private ServiceLocator() {

		super();
	}

	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {

		return applicationContext;
	}

	/**
	 * Obter a instância do service locator
	 * 
	 * @return
	 */
	public static ServiceLocator getInstancia() {

		return instancia;
	}

	/**
	 * Obter o controlador de negócio.
	 * 
	 * @param id
	 *            O id do controlador
	 * @return Um controlador de negócio
	 */
	public ControladorNegocio getControladorNegocio(String id) {

		return (ControladorNegocio) getBeanPorID(id);
	}

	/**
	 * Obtêm o bean do contexto do spring.
	 * 
	 * @param identificador
	 *            O ID configurado no contexo do
	 *            spring
	 * @return O Objeto
	 */
	public Object getBeanPorID(String identificador) {
		return applicationContext.getBean(identificador);
	}

	/**
	 * Obtém um bean do contexto de IC do Spring a partir de sua classe do
	 * @param classe tipo da classe que será injetada
	 * @param <T> tipo da classe que será retornada pelo contexto
	 * @return retorna o bean do contexto de inversão de controle do spring
	 */
	public <T> T getBean(Class<T> classe) {
		return applicationContext.getBean(classe);
	}

	/**
	 * Obtêm o bean do contexto do spring.
	 * 
	 * @param id
	 *            O ID do bean
	 * @return A classe do bean
	 */
	public Class<?> getClassPorID(String id) {

		return applicationContext.getType(id);
	}

	/**
	 * Seta o contexto da aplicação
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

		this.applicationContext = applicationContext;
	}

	/**
	 * Gets the bean por i de sete chave primaria.
	 * 
	 * @param identificador
	 *            the identificador
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the bean por i de sete chave primaria
	 */
	public EntidadeNegocio getBeanPorIDeSeteChavePrimaria(String identificador, Long chavePrimaria) {

		EntidadeNegocio entidadeNegocio = (EntidadeNegocio) applicationContext.getBean(identificador);
		if(chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
		}
		return entidadeNegocio;
	}

	public ControladorCobranca getControladorCobranca() {

		return (ControladorCobranca) ServiceLocator.getInstancia().getControladorNegocio(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
	}

	public ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
	}

	public ControladorCliente getControladorCliente() {

		return (ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
	}

	public ControladorSegmento getControladorSegmento() {

		return (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
	}

	public ControladorEndereco getControladorEndereco() {

		return (ControladorEndereco) ServiceLocator.getInstancia().getControladorNegocio(ControladorEndereco.BEAN_ID_CONTROLADOR_ENDERECO);
	}

	public ControladorEmpresa getControladorEmpresa() {

		return (ControladorEmpresa) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
	}

	public ControladorContrato getControladorContrato() {

		return (ControladorContrato) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
	}

	public ControladorModeloContrato getControladorModeloContrato() {

		return (ControladorModeloContrato) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorModeloContrato.BEAN_ID_CONTROLADOR_MODELO_CONTRATO);
	}

	public ControladorCreditoDebito getControladorCreditoDebito() {

		return (ControladorCreditoDebito) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);
	}

	public ControladorArrecadacao getControladorArrecadacao() {

		return (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
	}

	public ControladorImovel getControladorImovel() {

		return (ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
	}

	public ControladorAnormalidade getControladorAnormalidade() {

		return (ControladorAnormalidade) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
	}

	public ControladorRota getControladorRota() {

		return (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
	}

	public ControladorUsuario getControladorUsuario() {

		return (ControladorUsuario) ServiceLocator.getInstancia().getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
	}

	public ControladorHistoricoConsumo getControladorHistoricoConsumo() {

		return (ControladorHistoricoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
	}

	public ControladorHistoricoMedicao getControladorHistoricoMedicao() {

		return (ControladorHistoricoMedicao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
	}

	public ControladorPontoConsumo getControladorPontoConsumo() {

		return (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
	}

	public ControladorMedidor getControladorMedidor() {

		return (ControladorMedidor) ServiceLocator.getInstancia().getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
	}

	public ControladorParametroSistema getControladorParametroSistema() {

		return (ControladorParametroSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
	}

	public ControladorQuadra getControladorQuadra() {

		return (ControladorQuadra) ServiceLocator.getInstancia().getControladorNegocio(ControladorQuadra.BEAN_ID_CONTROLADOR_QUADRA);
	}

	public ControladorTemperaturaPressaoMedicao getControladorTemperaturaPressaoMedicao() {

		return (ControladorTemperaturaPressaoMedicao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorTemperaturaPressaoMedicao.BEAN_ID_CONTROLADOR_TEMPERATURA_PRESSAO_MEDICAO);
	}

	public ControladorTributo getControladorTributo() {

		return (ControladorTributo) ServiceLocator.getInstancia().getControladorNegocio(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);

	}

	public ControladorUnidade getControladorUnidade() {

		return (ControladorUnidade) ServiceLocator.getInstancia().getControladorNegocio(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
	}

	public ControladorUnidadeOrganizacional getControladorUnidadeOrganizacional() {

		return (ControladorUnidadeOrganizacional) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorUnidadeOrganizacional.BEAN_ID_CONTROLADOR_UNIDADE_ORGANIZACIONAL);
	}

	public ControladorFaixaPressaoFornecimento getControladorFaixaPressaoFornecimento() {

		return (ControladorFaixaPressaoFornecimento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFaixaPressaoFornecimento.BEAN_ID_CONTROLADOR_FAIXA_PRESSAO_FORNECIMENTO);
	}

	public ControladorLocalidade getControladorLocalidade() {

		return (ControladorLocalidade) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorLocalidade.BEAN_ID_CONTROLADOR_LOCALIDADE);
	}

	public ControladorSetorComercial getControladorSetorComercial() {

		return (ControladorSetorComercial) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSetorComercial.BEAN_ID_CONTROLADOR_SETOR_COMERCIAL);
	}

	public ControladorRubrica getControladorRubrica() {

		return (ControladorRubrica) ServiceLocator.getInstancia().getControladorNegocio(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);
	}

	public ControladorFatura getControladorFatura() {

		return (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
	}

	public ControladorRamal getControladorRamal() {

		return (ControladorRamal) ServiceLocator.getInstancia().getControladorNegocio(ControladorRamal.BEAN_ID_CONTROLADOR_RAMAL);
	}

	public ControladorVazaoCorretor getControladorVazaoCorretor() {

		return (ControladorVazaoCorretor) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
	}

	public ControladorMunicipio getControladorMunicipio() {

		return (ControladorMunicipio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorMunicipio.BEAN_ID_CONTROLADOR_MUNICIPIO);
	}

	public ControladorFeriado getControladorFeriado() {

		return (ControladorFeriado) ServiceLocator.getInstancia().getControladorNegocio(ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);
	}

	public ControladorSetorCensitario getControladorSetorCensitario() {

		return (ControladorSetorCensitario) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSetorCensitario.BEAN_ID_CONTROLADOR_SETOR_CENSITARIO);
	}

	public ControladorRede getControladorRede() {

		return (ControladorRede) ServiceLocator.getInstancia().getControladorNegocio(ControladorRede.BEAN_ID_CONTROLADOR_REDE);
	}

	public ControladorTronco getControladorTronco() {

		return (ControladorTronco) ServiceLocator.getInstancia().getControladorNegocio(ControladorTronco.BEAN_ID_CONTROLADOR_TRONCO);
	}

	public ControladorCityGateMedicao getControladorCityGateMedicao() {

		return (ControladorCityGateMedicao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCityGateMedicao.BEAN_ID_CONTROLADOR_CITY_GATE_MEDICAO);
	}

	public ControladorClienteImovel getControladorClienteImovel() {

		return (ControladorClienteImovel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorClienteImovel.BEAN_ID_CONTROLADOR_CLIENTE_IMOVEL);
	}

	public ControladorCronogramaFaturamento getControladorCronogramaFaturamento() {

		return (ControladorCronogramaFaturamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
	}

	public ControladorRamoAtividade getControladorRamoAtividade() {

		return (ControladorRamoAtividade) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRamoAtividade.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE);
	}

	public ControladorDocumentoLayout getControladorDocumentoLayout() {

		return (ControladorDocumentoLayout) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorDocumentoLayout.BEAN_ID_CONTROLADOR_DOCUMENTO_LAYOUT);
	}

	public ControladorPrecoGas getControladorPrecoGas() {

		return (ControladorPrecoGas) ServiceLocator.getInstancia().getControladorNegocio(ControladorPrecoGas.BEAN_ID_CONTROLADOR_PRECO_GAS);
	}

	public ControladorRamoAtividadeSubstituicaoTributaria getControladorRamoAtividadeSubstituicaoTributaria() {

		return (ControladorRamoAtividadeSubstituicaoTributaria) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRamoAtividadeSubstituicaoTributaria.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE_SUBS_TRIB);
	}

	public ControladorIntegracao getControladorIntegracao() {

		return (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
	}

	public ControladorParcelamento getControladorParcelamento() {

		return (ControladorParcelamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);
	}

	public ControladorDocumentoCobranca getControladorDocumentoCobranca() {

		return (ControladorDocumentoCobranca) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
	}

	public ControladorConstanteSistema getControladorConstanteSistema() {

		return (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}

	public ControladorLeituraMovimento getControladorLeituraMovimento() {

		return (ControladorLeituraMovimento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
	}

	public ControladorPerfilParcelamento getControladorPerfilParcelamento() {

		return (ControladorPerfilParcelamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPerfilParcelamento.BEAN_ID_CONTROLADOR_PERFIL_PARCELAMENTO);
	}

	public ControladorCalculoFornecimentoGas getControladorCalculoFornecimentoGas() {

		return (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);
	}

	public ControladorServicoTipo getControladorServicoTipo() {

		return (ControladorServicoTipo) ServiceLocator.getInstancia().getBeanPorID("controladorServicoTipo");
	}

	public ControladorTabelaAuxiliar getControladorTabelaAuxiliar() {

		return (ControladorTabelaAuxiliar) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR);
	}

	public ControladorFuncionario getControladorFuncionario() {

		return (ControladorFuncionario) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
	}

	public ControladorChamadoTipo getControladorChamadoTipo() {

		return (ControladorChamadoTipo) ServiceLocator.getInstancia().getBeanPorID("controladorChamadoTipo");
	}

	public ControladorChamadoAssunto getControladorChamadoAssunto() {

		return (ControladorChamadoAssunto) ServiceLocator.getInstancia().getBeanPorID("controladorChamadoAssunto");
	}

	public ControladorChamado getControladorChamado() {

		return (ControladorChamado) ServiceLocator.getInstancia().getBeanPorID("controladorChamado");
	}

	public ControladorEquipe getControladorEquipe() {

		return (ControladorEquipe) ServiceLocator.getInstancia().getBeanPorID("controladorEquipe");
	}

	public ControladorAcao getControladorAcao() {

		return (ControladorAcao) ServiceLocator.getInstancia().getBeanPorID("controladorAcao");
	}

	public ControladorTipoDocumento getControladorTipoDocumento() {

		return (ControladorTipoDocumento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorTipoDocumento.BEAN_ID_CONTROLADOR_TIPO_DOCUMENTO);
	}

	public ControladorModulo getControladorModulo() {

		return (ControladorModulo) ServiceLocator.getInstancia().getControladorNegocio(ControladorModulo.BEAN_ID_CONTROLADOR_MODULO);
	}

	public ControladorServicoAutorizacao getControladorServicoAutorizacao() {

		return (ControladorServicoAutorizacao) ServiceLocator.getInstancia().getBeanPorID("controladorServicoAutorizacao");
	}

	public ControladorAcaoComando getControladorAcaoComando() {

		return (ControladorAcaoComando) ServiceLocator.getInstancia().getBeanPorID("controladorAcaoComando");
	}

	public ControladorQuestionario getControladorQuestionario() {

		return (ControladorQuestionario) ServiceLocator.getInstancia().getBeanPorID("controladorQuestionario");
	}

	public ControladorTransportadora getControladorTransportadora() {

		return (ControladorTransportadora) ServiceLocator.getInstancia().getBeanPorID("controladorTransportadora");
	}

	public ControladorArrecadadorConvenio getControladorArrecadadorConvenio() {

		return (ControladorArrecadadorConvenio) ServiceLocator.getInstancia().getBeanPorID("controladorArrecadadorConvenio");

	}

	public ControladorDocumentoImpressaoLayout getControladorDocumentoImpressaoLayout() {

		return (ControladorDocumentoImpressaoLayout) ServiceLocator.getInstancia().getBeanPorID("controladorDocumentoImpressaoLayout");
	}

	public ControladorProgramacao getControladorProgramacao() {

		return (ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID("controladorProgramacao");
	}

	public ControladorTarifa getControladorTarifa() {

		return (ControladorTarifa) ServiceLocator.getInstancia().getControladorNegocio(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
	}

	public ControladorApuracaoPenalidade getControladorApuracaoPenalidade() {

		return (ControladorApuracaoPenalidade) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);
	}

	public ControladorContabil getControladorContabil() {

		return (ControladorContabil) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);
	}
	
	/**
	 * Cria nova instância do ControladorProcesso.
	 * 
	 * @return ControladorProcesso
	 */
	public ControladorProcesso getControladorProcesso() {
		return (ControladorProcesso) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
	}
	
	/**
	 * Cria nova instância do ControladorProcessoDocumento.
	 * 
	 * @return ControladorProcessoDocumento
	 */
	public ControladorProcessoDocumento getControladorProcessoDocumento() {
		return (ControladorProcessoDocumento) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorProcessoDocumento.BEAN_ID_CONTROLADOR_PROCESSO_DOCUMENTO);
	}
	
	/**
	 * Cria nova instância do ControladorProcessoDocumento.
	 * 
	 * @return ControladorProcessoDocumento
	 */
	public ControladorGrupoFaturamento getControladorGrupoFaturamento() {

		return (ControladorGrupoFaturamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorGrupoFaturamento.BEAN_ID_CONTROLADOR_GRUPO_FARURAMENTO);
	}
	
	/**
	 * Cria nova instância do ControladorNegativacao
	 * @return ControladorNegativacao
	 */
	public ControladorNegativacao getControladorNegativacao() {

		return (ControladorNegativacao) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorNegativacao.BEAN_ID_CONTROLADOR_NEGATIVACAO);
	}
	
	/**
	 * Cria nova instância do ControladorComunicacao
	 * @return ControladorComunicacao
	 */
	public ControladorComunicacao getControladorComunicacao() {

		return (ControladorComunicacao) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorComunicacao.BEAN_ID_CONTROLADOR_COMUNICACAO);
	}
	
	
	/**
	 * Cria nova instância do ControladorVeiculo
	 * @return ControladorVeiculo
	 */
	public ControladorVeiculo getControladorVeiculo() {

		return (ControladorVeiculo) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorVeiculo.BEAN_ID_CONTROLADOR_VEICULO);
	}
	
	
	/**
	 * Cria nova instância do ControladorEquipeTurno
	 * @return ControladorVeiculo
	 */
	public ControladorEquipeTurno getControladorEquipeTurno() {

		return (ControladorEquipeTurno) ServiceLocator.getInstancia().getBeanPorID("controladorEquipeTurno");
	}
}
