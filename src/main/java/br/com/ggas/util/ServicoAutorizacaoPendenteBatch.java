package br.com.ggas.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;

public class ServicoAutorizacaoPendenteBatch {
	
	public static final String BEAN_ID_SERVICO_AUTORIZACAO_PENDENTE = "servicoAutorizacaoPendenteBatch";
	
	private static final Logger LOG = Logger.getLogger(ServicoAutorizacaoPendenteBatch.class);
	
	private final ServiceLocator serviceLocator = ServiceLocator.getInstancia();
	
	private ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	private ControladorParametroSistema controladorParametroSistema;
	
	public ServicoAutorizacaoPendenteBatch() {
		controladorServicoAutorizacao = serviceLocator.getControladorServicoAutorizacao();
		controladorParametroSistema = (ControladorParametroSistema) serviceLocator
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
	}
	
	public void alertaServicoAutorizacaoPendente() throws NegocioException {
		
		LOG.info("Iniciando rotina de envio de emails para as Autorizações de Serviços Pendentes...");
		
		String periodicidade =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_NM_DIAS_EMAIL_AUTORIZACAO_SERVICO_PENDENTE);
		
		if (periodicidade != null && !"0".equals(periodicidade)) {

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());

			if (calendar.get(Calendar.DAY_OF_WEEK) == Integer.valueOf(periodicidade)) {
				
				try {
					enviaEmailServicoAutorizacaoPendente();
				} catch (GGASException e) {
					throw new NegocioException(e);
				}
				
			}
		}
		
		LOG.info("Finalizando rotina de envio de emails para as Autorizações de Serviços Pendentes atrasados.");

		
	}

	private void enviaEmailServicoAutorizacaoPendente() throws GGASException{
		JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		ParametroSistema servidorEmailConfigurado =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);
		ParametroSistema emailRemetentePadrao =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
		
		if (servidorEmailConfigurado != null
				&& BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor())) {
			Collection<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao
					.listarServicoAutorizacaoPendente();

			if (listaServicoAutorizacao != null && !listaServicoAutorizacao.isEmpty()) {
				enviarEmail(mailUtil, listaServicoAutorizacao, emailRemetentePadrao.getValor());
			}
		}
	}

	private void enviarEmail(JavaMailUtil mailUtil, Collection<ServicoAutorizacao> listaServicoAutorizacao,
			String emailRemetentePadrao) throws GGASException {
		Map<UnidadeOrganizacional, Collection<ServicoAutorizacao>> mapaFuncionarioAS = listaServicoAutorizacao.stream()
				.filter(p -> p.getEquipe() != null)
				.collect(Collectors.groupingBy(p -> p.getEquipe().getUnidadeOrganizacional(), LinkedHashMap::new,
						Collectors.toCollection(HashSet::new)));
		
		String assuntoEmail = "Lista de Autorização de Serviço Pendente";
		
		for(Entry<UnidadeOrganizacional, Collection<ServicoAutorizacao>> entry : mapaFuncionarioAS.entrySet()) {
			String corpoEmail = this.prepararCorpoEmail(entry.getValue());
			
			mailUtil.enviar(emailRemetentePadrao, entry.getKey().getEmailContato(), assuntoEmail, corpoEmail, true, false);
			
		}
		
		
		String corpoEmailTI = this.prepararCorpoEmail(listaServicoAutorizacao);
		mailUtil.enviar(emailRemetentePadrao, "ti@algas.com.br", assuntoEmail, corpoEmailTI, true, false);

		
		
		
	}

	private String prepararCorpoEmail(Collection<ServicoAutorizacao> listaServicoAutorizacao) throws NegocioException {
		
		StringBuilder email = new StringBuilder();
		
		if (Util.isNullOrEmpty(listaServicoAutorizacao)) {
			throw new NegocioException("Nenhum chamado atrasado foi encontrado.");
		}

		email.append("<h3>Abaixo segue a lista de servico autorização em aberto sob sua responsabilidade ou interesse.</h3>");
		email.append("<table border='1' cellpadding='5px' cellspacing='1px'><td><strong>Número Autorização Serviço</strong></td>");
		email.append("<td><strong>Responsável</strong></td><td><strong>Equipe</strong></td><td><strong>Tipo de Serviço</strong></td><td><strong>Número do Protocolo</strong></td><td><strong>Imóvel</strong></td>");
		email.append("<td><strong>Cliente</strong></td><td><strong>Data Geração</strong></td>");
		email.append("<td><strong>Data de Previsão</strong></td><td><strong>Prioridade</strong></td>");
		
		String abreTD = "<td align='center'>";
		String fechaTD = "</td>";

		for (ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
			email.append("<tr>");
			
			email.append(abreTD);
			email.append(servicoAutorizacao.getChavePrimaria());
			email.append(fechaTD);
			
			email.append(abreTD);
			if(servicoAutorizacao.getExecutante() != null) {
				email.append(servicoAutorizacao.getExecutante().getNome());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if(servicoAutorizacao.getEquipe() != null) {
				email.append(servicoAutorizacao.getEquipe().getNome());
			}
			email.append(fechaTD);

			email.append(abreTD);
			email.append(servicoAutorizacao.getServicoTipo().getDescricao());
			email.append(fechaTD);

			email.append(abreTD);
			if (servicoAutorizacao.getChamado() != null) {
				email.append(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo());
			} else if (servicoAutorizacao.getNumeroProtocolo() != null) {
				email.append(servicoAutorizacao.getNumeroProtocolo());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (servicoAutorizacao.getImovel() != null) {
				email.append(servicoAutorizacao.getImovel().getNome());
			}
			email.append(fechaTD);

			email.append(abreTD);
			if (servicoAutorizacao.getCliente() != null) {
				email.append(servicoAutorizacao.getCliente().getNome());
			}
			email.append(fechaTD);

			email.append(abreTD);
			email.append(DataUtil.converterDataParaString(servicoAutorizacao.getDataGeracao()));
			email.append(fechaTD);

			email.append(abreTD);
			email.append(DataUtil.converterDataParaString(servicoAutorizacao.getDataPrevisaoEncerramento()));
			email.append(fechaTD);

			email.append(abreTD);
			email.append(servicoAutorizacao.getServicoTipoPrioridade().getDescricao());
			email.append(fechaTD);

			email.append("</tr>");
		}

		email.append("</table>");

		return email.toString();
	}
	
	
	

}
