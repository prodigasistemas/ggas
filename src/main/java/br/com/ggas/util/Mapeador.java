package br.com.ggas.util;

/**
 * 
 * @author voliveira
 *
 * @param <T, V>
 */
public interface Mapeador<K, T> {

	/**
	 * Metodo que define o objeto por qual a classe sera mapeada.
	 * 
	 * @param elemento
	 * @return o objeto por qual a classe será mapeada
	 */
	K getChaveMapeadora(T elemento);
	
}
