/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.web.contrato.contrato.ContratoVO;
import br.com.ggas.web.contrato.modelocontrato.ModeloContratoVO;

public abstract class GGASBaseTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5636778291196123478L;

	private static final Logger LOG = Logger.getLogger(Object.class);

	private static final String CONTRATO_VO = "contratoVO";

	private static final String MODELO_CONTRATO_VO = "modeloContratoVO";

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.BodyTagSupport#doEndTag()
	 */
	@Override
	public abstract int doEndTag() throws JspException;

	/**
	 * Obter valor atributo.
	 * 
	 * @param atributo the atributo	-	{@link String}
	 * @return true, if successful	-	{@link Boolean}
	 */
	protected boolean obterValorAtributoForm(String atributo) {

		Boolean retorno = false;

		if (pageContext.getRequest().getAttribute(CONTRATO_VO) != null && StringUtils.capitalize(CONTRATO_VO)
				.equals(pageContext.getRequest().getAttribute(CONTRATO_VO).getClass().getSimpleName())) {
			retorno = getField(atributo, pageContext.getRequest().getAttribute(CONTRATO_VO), CONTRATO_VO);
		} else if (pageContext.getRequest().getAttribute(MODELO_CONTRATO_VO) != null && StringUtils.capitalize(MODELO_CONTRATO_VO)
				.equals(pageContext.getRequest().getAttribute(MODELO_CONTRATO_VO).getClass().getSimpleName())) {
			retorno = getField(atributo, pageContext.getRequest().getAttribute(MODELO_CONTRATO_VO), MODELO_CONTRATO_VO);
		}
		
		return retorno;

	}

	/**
	 * Obter valor atributo request.
	 * 
	 * @param atributo the atributo
	 * @return the boolean
	 */
	protected Boolean obterValorAtributoRequest(String atributo) {

		return (Boolean) pageContext.getRequest().getAttribute(atributo);
	}

	protected String getContextPath() {

		return ((HttpServletRequest) pageContext.getRequest()).getContextPath();
	}

	/**
	 * Verifica se atributo existe no objeto retornando true ou false.
	 * 
	 * @param fieldName			-	{@link Field}
	 * @param obj				-	{@link Object}
	 * @param nomeClasseVO		-	{@link String}
	 * @return valorAtributo	-	{@link Boolean}
	 */
	private Boolean getField(String fieldName, Object obj, String nomeClasseVO) {

		Field field = null;
		Boolean valorAtributo = null;

		try {

			if (CONTRATO_VO.equals(nomeClasseVO)) {
				field = ContratoVO.class.getDeclaredField(fieldName);
			} else {
				field = ModeloContratoVO.class.getDeclaredField(fieldName);
			}

			field.setAccessible(Boolean.TRUE);
			valorAtributo = (Boolean) field.get(obj);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return valorAtributo != null ? valorAtributo : false;
	}
	
}
