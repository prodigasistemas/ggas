/*
 * This file is part of PorExtenso. PorExtenso is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. PorExtenso is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received
 * a copy of the GNU Lesser General Public License along with PorExtenso. If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Copyright 2008, Marcelo Criscuolo.
 */

package br.com.ggas.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author Marcelo Criscuolo - criscuolo (dot) marcelo (at) gmail (dot) com
 *
 */
public class CurrencyWriter {

	private static final BigInteger THOUSAND = new BigInteger("1000");
	private static final BigInteger HUNDRED = new BigInteger("100");
	private static final String CENTO = "cento";
	private static final String CEM = "cem";

	private static final int UM = 1;
	private static final int DOIS = 2;
	private static final int TREIS = 3;
	private static final int QUATRO = 4;
	private static final int CINCO = 5;
	private static final int SEIS = 6;
	private static final int SETE = 7;
	private static final int OITO = 8;
	private static final int NOVE = 9;
	private static final int DEZ = 10;
	private static final int ONZE = 11;
	private static final int DOZE = 12;
	private static final int TREZE = 13;
	private static final int QUATORZE = 14;
	private static final int QUINZE = 15;
	private static final int DEZESSEIS = 16;
	private static final int DEZESSETE = 17;
	private static final int DEZOITO = 18;
	private static final int DEZENOVE = 19;
	private static final int VINTE = 20;
	private static final int TRINTA = 30;
	private static final int QUARENTA = 40;
	private static final int CINQUENTA = 50;
	private static final int SESSENTA = 60;
	private static final int SETENTA = 70;
	private static final int OITENTA = 80;
	private static final int NOVENTA = 90;
	private static final int NUMERO_CEM = 100;
	private static final int DUZENTOS = 200;
	private static final int TREZENTOS = 300;
	private static final int QUATROCENTOS = 400;
	private static final int QUINHENTOS = 500;
	private static final int SEICENTOS = 600;
	private static final int SETECENTOS = 700;
	private static final int OITOCENTOS = 800;
	private static final int NOVECENTOS = 900;

	/**
	 * 
	 * Nomes das grandezas numéricas no plural. O mapa a chave do mapa é o expoente
	 * de dez do número e o valor é o seu nome no plural. Por exemplo: para chave 3
	 * (10^3) o valor é "mil", para a chave 6 (10^6) o valor é "milhões", e assim
	 * por diante. Os nomes foram obtidos de um artigo publicado na seção Dois Mais
	 * Dois na revista SuperInteressante nº. 15, de dezembro de 1988 (Editora Abril,
	 * São Paulo/SP), disponível em
	 * http://www.novomilenio.inf.br/idioma/19881200.htm.
	 * 
	 */
	private final Map<Integer, String> grandezasPlural = new HashMap<Integer, String>();
	private final Map<Integer, String> grandezasSingular = new HashMap<Integer, String>();

	/**
	 * Nomes dos números.
	 */
	private final Map<Integer, String> nomes = new HashMap<Integer, String>();

	private static final String MOEDA_SINGULAR = "real";
	private static final String MOEDA_PLURAL = "reais";

	private static final String FRACAO_SINGULAR = "centavo";
	private static final String FRACAO_PLURAL = "centavos";

	private static final String PARTICULA_ADITIVA = "e";
	private static final String PARTICULA_DESCRITIVA = "de";

	private Boolean moeda = true;

	/**
	 * 
	 * O conversor reconhece números até a ordem dos setilhões, portanto, o maior
	 * valor suportado atualmente é o representado abaixo.
	 * 
	 */
	private static final BigDecimal MAX_SUPPORTED_VALUE = new BigDecimal("999999999999999999999999999.99");
	private static CurrencyWriter instance = null;

	private CurrencyWriter() {
		preencherGrandezasPlural();
		preencherGrandezasSingular();
		preencherNomes();
	}

	/**
	 * @return currencyWriter
	 */
	public static CurrencyWriter getInstance() {
		if (instance == null) {
			instance = new CurrencyWriter();
		}

		return instance;
	}

	/**
	 * @param amount
	 * @return porextenso
	 */
	public String write(final BigDecimal amount) {
		if (null == amount) {
			throw new IllegalArgumentException();
		}

		BigDecimal value = amount.setScale(DOIS, BigDecimal.ROUND_HALF_EVEN);

		if (value.compareTo(BigDecimal.ZERO) <= 0) {
			return "";
		}

		if (MAX_SUPPORTED_VALUE.compareTo(value) < 0) {
			throw new IllegalArgumentException("Valor acima do limite suportado.");
		}

		Stack<Integer> decomposed = decompose(value);

		/*
		 * 
		 * Se o número estiver, digamos, na casa dos milhões, a pilha deverá conter 4
		 * elementos sendo os dois últimos os das centenas e dos centavos,
		 * respectivamente. Assim, o expoente de dez que representa a grandeza no topo
		 * da pilha é o número de (elementos - 2) * 3
		 * 
		 */
		int expoente = 3 * (decomposed.size() - DOIS);

		StringBuilder sb = new StringBuilder();
		int lastNonZeroExponent = -1;

		while (!decomposed.empty()) {
			int valor = decomposed.pop();

			if (valor > 0) {
				sb.append(" ").append(PARTICULA_ADITIVA).append(" ");
				sb.append(comporNomeGrupos(valor));
				String nomeGrandeza = obterNomeGrandeza(expoente, valor);
				if (nomeGrandeza.length() > 0) {
					sb.append(" ");
				}
				sb.append(nomeGrandeza);

				lastNonZeroExponent = expoente;
			}

			switch (expoente) {
			case 0:
				BigInteger parteInteira = value.toBigInteger();

				if (BigInteger.ONE.equals(parteInteira)) {
					sb.append(" ");
					if (this.moeda) {
						sb.append(MOEDA_SINGULAR);
					}
				} else if (parteInteira.compareTo(BigInteger.ZERO) > 0) {
					if (lastNonZeroExponent >= SEIS) {
						sb.append(" ").append(PARTICULA_DESCRITIVA);
					}
					sb.append(" ");
					if (this.moeda) {
						sb.append(MOEDA_PLURAL);
					}
				}
				break;

			case -3:
				if (1 == valor) {
					sb.append(" ").append(FRACAO_SINGULAR);
				} else if (valor > 1) {
					sb.append(" ").append(FRACAO_PLURAL);
				}
				break;

			default:
				break;
			}

			expoente -= 3;
		}

		return sb.substring(3);
	}

	/**
	 * @param valor
	 * @return nomeGrupos
	 */
	private StringBuilder comporNomeGrupos(int valor) {
		StringBuilder nome = new StringBuilder();

		int centenas = valor - (valor % 100);
		int unidades = valor % 10;
		int dezenas = (valor - centenas) - unidades;
		int duasCasas = dezenas + unidades;

		if (centenas > 0) {
			nome.append(" ").append(PARTICULA_ADITIVA).append(" ");

			if (100 == centenas) {
				if (duasCasas > 0) {
					nome.append(CENTO);
				} else {
					nome.append(CEM);
				}
			} else {
				nome.append(nomes.get(centenas));
			}
		}

		if (duasCasas > 0) {
			nome.append(" ").append(PARTICULA_ADITIVA).append(" ");
			if (duasCasas < VINTE) {
				nome.append(nomes.get(duasCasas));
			} else {
				if (dezenas > 0) {
					nome.append(nomes.get(dezenas));
				}

				if (unidades > 0) {
					nome.append(" ").append(PARTICULA_ADITIVA).append(" ");
					nome.append(nomes.get(unidades));
				}
			}
		}

		return nome.delete(0, TREIS);
	}

	/**
	 * @param exponent
	 * @param value
	 * @return nomeGrandeza
	 */
	private String obterNomeGrandeza(int exponent, int value) {
		if (exponent < 3) {
			return "";
		}

		if (1 == value) {
			return grandezasSingular.get(exponent);
		} else {
			return grandezasPlural.get(exponent);
		}
	}

	/**
	 * @param value
	 * @return decompose
	 */
	private Stack<Integer> decompose(BigDecimal value) {
		BigInteger intermediate = value.multiply(new BigDecimal(NUMERO_CEM)).toBigInteger();
		Stack<Integer> decomposed = new Stack<Integer>();

		BigInteger[] result = intermediate.divideAndRemainder(HUNDRED);
		intermediate = result[0];
		decomposed.add(result[1].intValue());

		while (intermediate.compareTo(BigInteger.ZERO) > 0) {
			result = intermediate.divideAndRemainder(THOUSAND);
			intermediate = result[0];
			decomposed.add(result[1].intValue());
		}

		/*
		 * Se o valor for apenas em centavos, adicionar zero para a casa dos reais
		 * inteiros
		 * 
		 */
		if (decomposed.size() == 1) {
			decomposed.add(0);
		}

		return decomposed;
	}

	/**
	 * plural
	 */
	private void preencherGrandezasPlural() {
		grandezasPlural.put(TREIS, "mil");
		grandezasPlural.put(SEIS, "milhões");
		grandezasPlural.put(NOVE, "bilhões");
		grandezasPlural.put(DOZE, "trilhões");
		grandezasPlural.put(QUINZE, "quatrilhões");
		grandezasPlural.put(DEZOITO, "quintilhões");
		grandezasPlural.put(21, "sextilhões");
		grandezasPlural.put(24, "setilhões");
	}

	/**
	 * Singular
	 */
	private void preencherGrandezasSingular() {
		grandezasSingular.put(TREIS, "mil");
		grandezasSingular.put(SEIS, "milhão");
		grandezasSingular.put(NOVE, "bilhão");
		grandezasSingular.put(DOZE, "trilhão");
		grandezasSingular.put(QUINZE, "quatrilhão");
		grandezasSingular.put(DEZOITO, "quintilhão");
		grandezasSingular.put(21, "sextilhão");
		grandezasSingular.put(24, "setilhão");
	}

	/**
	 * nomes
	 */
	private void preencherNomes() {
		nomes.put(UM, "um");
		nomes.put(DOIS, "dois");
		nomes.put(TREIS, "três");
		nomes.put(QUATRO, "quatro");
		nomes.put(CINCO, "cinco");
		nomes.put(SEIS, "seis");
		nomes.put(SETE, "sete");
		nomes.put(OITO, "oito");
		nomes.put(NOVE, "nove");
		nomes.put(DEZ, "dez");
		nomes.put(ONZE, "onze");
		nomes.put(DOZE, "doze");
		nomes.put(TREZE, "treze");
		nomes.put(QUATORZE, "quatorze");
		nomes.put(QUINZE, "quinze");
		nomes.put(DEZESSEIS, "dezesseis");
		nomes.put(DEZESSETE, "dezessete");
		nomes.put(DEZOITO, "dezoito");
		nomes.put(DEZENOVE, "dezenove");
		nomes.put(VINTE, "vinte");
		nomes.put(TRINTA, "trinta");
		nomes.put(QUARENTA, "quarenta");
		nomes.put(CINQUENTA, "cinquenta");
		nomes.put(SESSENTA, "sessenta");
		nomes.put(SETENTA, "setenta");
		nomes.put(OITENTA, "oitenta");
		nomes.put(NOVENTA, "noventa");
		nomes.put(DUZENTOS, "duzentos");
		nomes.put(TREZENTOS, "trezentos");
		nomes.put(QUATROCENTOS, "quatrocentos");
		nomes.put(QUINHENTOS, "quinhentos");
		nomes.put(SEICENTOS, "seiscentos");
		nomes.put(SETECENTOS, "setecentos");
		nomes.put(OITOCENTOS, "oitocentos");
		nomes.put(NOVECENTOS, "novecentos");
	}

	/**
	 * @return moeda
	 */
	public Boolean getMoeda() {
		return moeda;
	}

	/**
	 * @param moeda
	 */
	public void setMoeda(Boolean moeda) {
		this.moeda = moeda;
	}
}
