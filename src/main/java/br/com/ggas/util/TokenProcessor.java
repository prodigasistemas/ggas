package br.com.ggas.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.geral.apresentacao.GenericAction;

/**
 * Classe responsável por manipular todas as funcionalidades relacionadas ao
 * token que assegura uma transação única.
 * 
 */
public class TokenProcessor {
	/**
	 * A instância singleton desta classe.
	 */
	private static TokenProcessor instance = new TokenProcessor();

	/**
	 * O registro de data e hora mais recente que foi utilizado para gerar um valor
	 * de token.
	 */
	private long previousTimestamp;

	protected static final Logger LOG = Logger.getLogger(GenericAction.class);

	/**
	 * Construtor privado da classe. Utilize TokenProcessor.getInstance() para obter
	 * uma instância.
	 */
	private TokenProcessor() {
		super();
	}

	/**
	 * Retorna uma instância única desta classe
	 */
	public static TokenProcessor getInstance() {
		return instance;
	}

	/**
	 * Retorna <code>true</code> se houver um token de transação armazenado na
	 * sessão atual do usuário e o valor enviado como parâmetro de solicitação com
	 * esta ação corresponda a ele. Retorna <code>false</code> sob qualquer uma das
	 * seguintes condições:
	 *
	 * <ul>
	 *
	 * <li>Nenhuma sessão associada a esta requisição</li>
	 * <li>Nenhum token de transação salvo na sessão</li>
	 *
	 * <li>Nenhum token de transação incluído como parâmetro da requisição</li>
	 *
	 * <li>O valor do token de transação incluído não corresponde ao token de
	 * transação na sessão do usuário.</li>
	 *
	 * </ul>
	 *
	 * @param request a requisição que está sendo processada
	 * @return boolean - {@link boolean}
	 */
	public synchronized boolean isTokenValid(HttpServletRequest request) {
		return this.isTokenValid(request, false);
	}

	/**
	 * Retorna <code>true</code> se houver um token de transação armazenado na
	 * sessão atual do usuário e o valor enviado como parâmetro de solicitação com
	 * esta ação corresponda a ele. Retorna <code>false</code> sob qualquer uma das
	 * seguintes condições:
	 *
	 * <ul>
	 *
	 * <li>Nenhuma sessão associada a esta requisição</li>
	 * <li>Nenhum token de transação salvo na sessão</li>
	 *
	 * <li>Nenhum token de transação incluído como parâmetro da requisição</li>
	 *
	 * <li>O valor do token de transação incluído não corresponde ao token de
	 * transação na sessão do usuário.</li>
	 *
	 * </ul>
	 *
	 * @param request a requisição que está sendo processada
	 * @param reset   indica se o token deve ser redefinido após a validação
	 * @return boolean - {@link boolean}
	 */
	public synchronized boolean isTokenValid(HttpServletRequest request, boolean reset) {

		HttpSession session = request.getSession(false);

		Boolean isValid = Boolean.FALSE;

		if (session != null) {

			String saved = (String) session.getAttribute(Constantes.TRANSACTION_SESSION_TOKEN);

			if (StringUtils.isNotBlank(saved)) {

				if (reset) {
					this.resetToken(request);
				}

				String token = request.getParameter(Constantes.TRANSACTION_REQUEST_TOKEN);

				isValid = saved.equals(token);

			}
		}

		return isValid;
	}

	/**
	 * Redefine o token de transação salvo na sessão do usuário. Isso indica que a
	 * verificação de token transacional não será necessária na próxima requisição
	 * realizada.
	 *
	 * @param request a requisição que está sendo processada
	 */
	public synchronized void resetToken(HttpServletRequest request) {
		HttpSession session = request.getSession(false);

		if (session != null) {
			session.removeAttribute(Constantes.TRANSACTION_SESSION_TOKEN);
		}
	}

	/**
	 * Salva um novo token de transação na sessão atual do usuário, criando uma nova
	 * sessão, se necessário.
	 *
	 * @param request a requisição que está sendo processada
	 **/
	public synchronized void saveToken(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String token = generateToken(request);

		if (token != null) {
			session.setAttribute(Constantes.TRANSACTION_SESSION_TOKEN, token);
		}
	}

	/**
	 * Gere um novo token de transação, que será utilizado para impor uma única
	 * requisição para uma transação específica.
	 *
	 *
	 * @param request a requisição que está sendo processada
	 * @return String - {@link String}
	 */
	public synchronized String generateToken(HttpServletRequest request) {
		HttpSession session = request.getSession();

		return generateToken(session.getId());
	}

	/**
	 * Gere um novo token de transação, que será utilizado para impor uma única
	 * requisição para uma transação específica.
	 *
	 * @param id um identiicador único que representa um token
	 * @return String - {@link String}
	 */
	public synchronized String generateToken(String id) {
		String token = null;

		try {
			long current = System.currentTimeMillis();

			if (current == previousTimestamp) {
				current++;
			}

			previousTimestamp = current;

			byte[] now = new Long(current).toString().getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");

			md.update(id.getBytes());
			md.update(now);

			token = toHex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			LOG.info(e.getMessage(), e);
		}
		return token;
	}

	private String toHex(byte[] buffer) {
		StringBuffer sb = new StringBuffer(buffer.length * 2);

		for (int i = 0; i < buffer.length; i++) {
			sb.append(Character.forDigit((buffer[i] & 0xf0) >> 4, 16));
			sb.append(Character.forDigit(buffer[i] & 0x0f, 16));
		}

		return sb.toString();
	}
}
