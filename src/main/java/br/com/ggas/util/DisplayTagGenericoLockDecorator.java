/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import br.com.ggas.web.contrato.contrato.ContratoAction;
import org.apache.commons.lang.StringUtils;
import org.displaytag.model.TableModel;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * @author bribeiro
 */
public abstract class DisplayTagGenericoLockDecorator extends DisplayTagGenericoDecorator {

	private String contextPath;

	/**
	 * @return the contextPath
	 */
	public String getContextPath() {

		return contextPath;
	}

	/**
	 * @param contextPath
	 *            the contextPath to set
	 */
	public void setContextPath(String contextPath) {

		this.contextPath = contextPath;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.displaytag.decorator.Decorator#init
	 * (javax.servlet.jsp.PageContext,
	 * java.lang.Object,
	 * org.displaytag.model.TableModel)
	 */
	@Override
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {

		super.init(pageContext, decorated, tableModel);
		this.contextPath = ((HttpServletRequest) pageContext.getRequest()).getContextPath();
	}

	/**
	 * Verifica se o item do grid representado por
	 * esse decoretor está sendo usado ou não
	 * pelo usuário logado ou por outro usuário. A
	 * partir disso retorna uma String com a
	 * forma de exibição adequada cujo nome para
	 * acesso no JSP é "chavePrimariaComLock".
	 * 
	 * @return chavePrimariaComLock
	 */
	public String getChavePrimariaComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		EntidadeNegocio entidadeNegocio = (EntidadeNegocio) getCurrentRowObject();
		montarCampoComLock(entidadeNegocio.getChavePrimaria(), entidadeNegocio.getChavePrimaria() + "", stringBuilder, true);
		return stringBuilder.toString();
	}

	/**
	 * Montar campo com lock.
	 * 
	 * @param chavePrimariaLock
	 *            the chave primaria lock
	 * @param valorColuna
	 *            the valor coluna
	 * @param stringBuilder
	 *            the string builder
	 * @param mostrarImagem
	 *            the mostrar imagem
	 */
	protected void montarCampoComLock(long chavePrimariaLock, String valorColuna, StringBuilder stringBuilder, boolean mostrarImagem) {

		boolean lock = false;
		boolean mostrarUnlock = false;
		String nomeUsuarioLock = StringUtils.EMPTY;

		Usuario usuarioLogado = (Usuario) getPageContext().getSession().getAttribute("usuarioLogado");
		Usuario usuarioLock = GGASApplicationContext.getInstancia().obterUsuarioLockOperacaoEntidade(getOperacaoLock(), chavePrimariaLock);

		if(usuarioLock != null) {
			lock = true;
			nomeUsuarioLock = usuarioLock.getLogin();
		}

		if(usuarioLogado != null && usuarioLogado.equals(usuarioLock)) {
			mostrarUnlock = true;
			lock = false;
		}

		String mensagemLock = GGASApplicationContext.getInstancia().obterMensagemLock(ContratoAction.LOCK_INTEGRACAO_CONTRATO, chavePrimariaLock);
		if (!"".contentEquals(mensagemLock)) {
			usuarioLock = GGASApplicationContext.getInstancia().obterUsuarioLockOperacaoEntidade(ContratoAction.LOCK_INTEGRACAO_CONTRATO,
					chavePrimariaLock);
			if (usuarioLogado != null && usuarioLogado.equals(usuarioLock)) {
				mostrarUnlock = true;
				lock = false;
			}
		} else {
			mensagemLock = GGASApplicationContext.getInstancia().obterMensagemLock(getOperacaoLock(), chavePrimariaLock);
		}

		if(lock) {
			adicionarHtmlLock(stringBuilder, nomeUsuarioLock, valorColuna, mostrarImagem);
		} else {
			adicionarHtmlUnlock(stringBuilder, valorColuna, chavePrimariaLock, mostrarUnlock, mostrarImagem, mensagemLock);
		}
	}

	/**
	 * Adicionar html lock.
	 * 
	 * @param stringBuilder
	 *            the string builder
	 * @param nomeUsuarioLock
	 *            the nome usuario lock
	 * @param valorColuna
	 *            the valor coluna
	 * @param mostrarImagem
	 *            the mostrar imagem
	 */
	public void adicionarHtmlLock(StringBuilder stringBuilder, String nomeUsuarioLock, String valorColuna, boolean mostrarImagem) {

		if(mostrarImagem) {
			stringBuilder.append("<img  alt=\"").append(getMensagemRegistroEmUso()).append(" por: ");
			stringBuilder.append(nomeUsuarioLock);
			stringBuilder.append("\" title=\"").append(getMensagemRegistroEmUso()).append(" por: ");
			stringBuilder.append(nomeUsuarioLock);
			stringBuilder.append("\" src=\"");
			stringBuilder.append(this.contextPath);
			stringBuilder.append("/imagens/Lock16.png");
			stringBuilder.append("\" border=\"0\"/>");
		}
	}

	/**
	 * Adicionar html unlock.
	 * 
	 * @param stringBuilder
	 *            the string builder
	 * @param valorColuna
	 *            the valor coluna
	 * @param chavePrimariaLock
	 *            the chave primaria lock
	 * @param mostrarUnlock
	 *            the mostrar unlock
	 * @param mensagemlock
	 * 			  the mensagem lock
	 * @param mostrarImagem
	 *            the mostrar imagem
	 */
	public void adicionarHtmlUnlock(StringBuilder stringBuilder, String valorColuna, long chavePrimariaLock, boolean mostrarUnlock,
					boolean mostrarImagem, String mensagemlock) {

		adicionarHtmlUnlockField(stringBuilder, valorColuna, chavePrimariaLock);
		if(mostrarUnlock && mostrarImagem) {
			adicionarHtmlUnlockAlerta(stringBuilder, mensagemlock);
		}
	}

	/**
	 * Adicionar html unlock field.
	 * 
	 * @param stringBuilder
	 *            the string builder
	 * @param valorColuna
	 *            the valor coluna
	 * @param chavePrimariaLock
	 *            the chave primaria lock
	 */
	public void adicionarHtmlUnlockField(StringBuilder stringBuilder, String valorColuna, long chavePrimariaLock) {

		stringBuilder.append("<input type=\"checkbox\" name=\"chavesPrimarias\" value=\"");
		stringBuilder.append(valorColuna);
		stringBuilder.append("\"");
		stringBuilder.append("/>");
	}

	/**
	 * Adicionar html unlock alerta.
	 * 
	 * @param stringBuilder - {@link StringBuilder}
	 * @param mensagemLock
	 *            the string builder
	 */
	public void adicionarHtmlUnlockAlerta(StringBuilder stringBuilder, String mensagemLock) {

		String icone = "Lock16.png";
		if ("".equals(mensagemLock)) {
			icone = "icon-alert16.gif";
			mensagemLock = getMensagemFormaLiberacaoRegistro();
		}
		stringBuilder.append("<img  alt=\"").append(mensagemLock).append("");
		stringBuilder.append("\" title=\"").append(mensagemLock).append("");
		stringBuilder.append("\" src=\"");
		stringBuilder.append(this.contextPath);
		stringBuilder.append("/imagens/").append(icone).append("");
		stringBuilder.append("\" border=\"0\"/>");

	}

	/**
	 * Retorna a String que será usada como chave
	 * da operação de lock no mapa
	 * mantido no contexto da aplicação.
	 * 
	 * @return operacaoLock
	 */
	public abstract String getOperacaoLock();

	/**
	 * Retorna a mensagem exibida para um registro
	 * locado por outro usuário que não o usuário
	 * logado.
	 * 
	 * @return mensagemRegistroEmUso
	 */
	public String getMensagemRegistroEmUso() {

		return "Registro em alteração";
	}

	/**
	 * Retorna a mensagem exibida para um registro
	 * locado pelo usuário logado.
	 * 
	 * @return mensagemFormaLiberacaoRegistro
	 */
	public String getMensagemFormaLiberacaoRegistro() {

		return "Atenção você bloqueou esse registro, para liberá-lo selecione o mesmo clique em Alterar e depois clique em Cancelar.";
	}

}
