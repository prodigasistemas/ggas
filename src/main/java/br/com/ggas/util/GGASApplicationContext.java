/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;

/**
 * Classe responsável por criar um contexto da
 * aplicação.
 * 
 * @author gmatos
 */
public final class GGASApplicationContext {

	private static GGASApplicationContext instancia = new GGASApplicationContext();

	private Map<String, Usuario> mapaSessaoSidUsuario = new HashMap<String, Usuario>();

	private Map<String, Map<Long, Usuario>> mapaLockOperacaoEntidadeUsuario = new HashMap<String, Map<Long, Usuario>>();

	private Map<String, Map<Long, String>> mapaLockMensagemEntidadeUsuario = new HashMap<String, Map<Long, String>>();

	/**
	 * Instantiates a new GGAS application context.
	 */
	private GGASApplicationContext() {

		super();
	}

	/**
	 * Método responsável por obter uma instancia.
	 *
	 * @return Uma instancia do objeto.
	 */
	public static GGASApplicationContext getInstancia() {

		return instancia;
	}


	/**
	 * Método responsável por registrar um usuário
	 * conectado do sistema.
	 *
	 * @param sid
	 *            O ID da sessão
	 * @param usuario
	 *            O usuário que será registrado
	 */
	public synchronized void registarUsuarioConectado(String sid, Usuario usuario) {

		this.mapaSessaoSidUsuario.put(sid, usuario);
	}

	/**
	 * Método responsável por desregistrar um
	 * usuário conectado do sistema.
	 *
	 * @param sid
	 *            O ID da sessão
	 */
	public synchronized void desregistrarUsuarioConectado(String sid) {

		this.removerLocksEntidadeUsuario(this.mapaSessaoSidUsuario.get(sid));
		this.mapaSessaoSidUsuario.remove(sid);
	}

	/**
	 * Registrar lock operacao entidade usuario.
	 *
	 * @param operacao
	 *            the operacao
	 * @param chavePrimariaEntidade
	 *            the chave primaria entidade
	 * @param usuario
	 *            the usuario
	 */
	public synchronized void registrarLockOperacaoEntidadeUsuario(String operacao, Long chavePrimariaEntidade, Usuario usuario) {

		if(!this.mapaLockOperacaoEntidadeUsuario.containsKey(operacao)) {
			this.mapaLockOperacaoEntidadeUsuario.put(operacao, new HashMap<Long, Usuario>());
			this.mapaLockMensagemEntidadeUsuario.put(operacao, new HashMap<Long, String>());
		}

		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		Map<Long, String> mapaLockMensagem = this.mapaLockMensagemEntidadeUsuario.get(operacao);

		if(mapaEntidadeUsuario != null && !mapaEntidadeUsuario.containsKey(chavePrimariaEntidade)
		// &&
		// !mapaEntidadeUsuario.containsValue(usuario)
		) {
			mapaEntidadeUsuario.put(chavePrimariaEntidade, usuario);
			mapaLockMensagem.put(chavePrimariaEntidade, "");
			this.mapaLockOperacaoEntidadeUsuario.put(operacao, mapaEntidadeUsuario);
			this.mapaLockMensagemEntidadeUsuario.put(operacao, mapaLockMensagem);
		}
	}

	/**
	 * Registrar lock operacao entidade usuario.
	 *
	 * @param operacao
	 *            the operacao
	 * @param chavePrimariaEntidade
	 *            the chave primaria entidade
	 * @param usuario
	 *            the usuario
	 * @param mensagemLock
	 *            the mensagem Lock
	 */
	public synchronized void registrarLockOperacaoEntidadeUsuario(String operacao, Long chavePrimariaEntidade, Usuario usuario,
			String mensagemLock) {

		if (!this.mapaLockOperacaoEntidadeUsuario.containsKey(operacao)) {
			this.mapaLockOperacaoEntidadeUsuario.put(operacao, new HashMap<Long, Usuario>());
			this.mapaLockMensagemEntidadeUsuario.put(operacao, new HashMap<Long, String>());
		}

		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		Map<Long, String> mapaMensagemLock = this.mapaLockMensagemEntidadeUsuario.get(operacao);

		if (mapaEntidadeUsuario != null && !mapaEntidadeUsuario.containsKey(chavePrimariaEntidade)) {
			mapaEntidadeUsuario.put(chavePrimariaEntidade, usuario);
			mapaMensagemLock.put(chavePrimariaEntidade, mensagemLock);
			this.mapaLockOperacaoEntidadeUsuario.put(operacao, mapaEntidadeUsuario);
			this.mapaLockMensagemEntidadeUsuario.put(operacao, mapaMensagemLock);
		}
	}

	/**
	 * Remove o regitro de lock da entidade cuja
	 * chave primária foi informada para o usuário
	 * e a operação dados.
	 *
	 * @param operacao
	 *            Operação para a qual o lock foi
	 *            necessário.
	 * @param chavePrimariaEntidade
	 *            Identificador da entidade
	 *            bloqueada.
	 * @param usuario
	 *            Usuário que possui o lock por
	 *            ter executado a operação para a
	 *            entidade informada.
	 */
	public synchronized void desregistrarLockOperacaoEntidadeUsuario(String operacao, Long chavePrimariaEntidade, Usuario usuario) {

		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		Map<Long, String> mapaMensagemLock = this.mapaLockMensagemEntidadeUsuario.get(operacao);

		if (mapaMensagemLock != null && mapaMensagemLock.containsKey(chavePrimariaEntidade)) {
			mapaMensagemLock.remove(chavePrimariaEntidade);
			this.mapaLockMensagemEntidadeUsuario.put(operacao, mapaMensagemLock);
		}

		if (mapaEntidadeUsuario != null && mapaEntidadeUsuario.containsKey(chavePrimariaEntidade) && usuario != null
						&& usuario.equals(mapaEntidadeUsuario.get(chavePrimariaEntidade))) {
			mapaEntidadeUsuario.remove(chavePrimariaEntidade);
			this.mapaLockOperacaoEntidadeUsuario.put(operacao, mapaEntidadeUsuario);
		}
	}

	/**
	 * Desregistrar lock operacao entidade usuario.
	 *
	 * @param operacao
	 *            the operacao
	 * @param usuario
	 *            the usuario
	 */
	public synchronized void desregistrarLockOperacaoEntidadeUsuario(String operacao, Usuario usuario) {

		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		Map<Long, String> mapaMensagemLock = this.mapaLockMensagemEntidadeUsuario.get(operacao);
		if(mapaEntidadeUsuario != null) {
			mapaMensagemLock.keySet().removeAll(mapaEntidadeUsuario.keySet());
			this.mapaLockMensagemEntidadeUsuario.put(operacao, mapaMensagemLock);
			while(mapaEntidadeUsuario.values().remove(usuario)){
				this.mapaLockOperacaoEntidadeUsuario.put(operacao, mapaEntidadeUsuario);
			}
		}
	}

	/**
	 * Existe lock operacao entidade usuario.
	 *
	 * @param operacao
	 *            the operacao
	 * @param chavePrimariaEntidade
	 *            the chave primaria entidade
	 * @param usuario
	 *            the usuario
	 * @return true, if successful
	 */
	public boolean existeLockOperacaoEntidadeUsuario(String operacao, Long chavePrimariaEntidade, Usuario usuario) {

		boolean retorno = false;
		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		if(mapaEntidadeUsuario != null) {
			Usuario usuarioLock = mapaEntidadeUsuario.get(chavePrimariaEntidade);
			if(usuarioLock != null && !usuarioLock.equals(usuario)) {
				retorno = true;
			}
		}
		return retorno;
	}

	/**
	 * Obter usuario lock operacao entidade.
	 *
	 * @param operacao
	 *            the operacao
	 * @param chavePrimariaEntidade
	 *            the chave primaria entidade
	 * @return the usuario
	 */
	public Usuario obterUsuarioLockOperacaoEntidade(String operacao, Long chavePrimariaEntidade) {

		Map<Long, Usuario> mapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.get(operacao);
		Usuario usuarioLock = null;
		if(mapaEntidadeUsuario != null) {
			usuarioLock = mapaEntidadeUsuario.get(chavePrimariaEntidade);
		}
		return usuarioLock;
	}

	/**
	 * Remover locks entidade usuario.
	 *
	 * @param usuario
	 *            the usuario
	 */
	private void removerLocksEntidadeUsuario(Usuario usuario) {

		Collection<Map<Long, Usuario>> colMapaEntidadeUsuario = this.mapaLockOperacaoEntidadeUsuario.values();
		for (Map<Long, Usuario> mapaEntidadeUsuario : colMapaEntidadeUsuario) {
			while(mapaEntidadeUsuario.values().remove(usuario));

		}
	}

	/**
	 * obter a mesagem de bloqueio.
	 * @param operacao - {@link String}
	 * @param chavePrimaria - {@link Long}
	 * @return String - {@link String}
	 */
	public String obterMensagemLock(String operacao, Long chavePrimaria) {

		String msg = "";
		Map<Long, String> mapaMensagemLock = this.mapaLockMensagemEntidadeUsuario.get(operacao);
		if (mapaMensagemLock != null && mapaMensagemLock.containsKey(chavePrimaria)) {
			msg = this.mapaLockMensagemEntidadeUsuario.get(operacao).get(chavePrimaria);
		}
		return msg;
	}

}
