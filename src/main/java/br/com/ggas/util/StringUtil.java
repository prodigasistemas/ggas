/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * Classe de utilitários para formatação de campos de texto
 * de preenchimento de texto a direita e a esquerda
 * 
 * @author ProdigaSistemas
 *
 */
public class StringUtil {
	
	private StringUtil() {
		
	}
	
	/**
	 * Método para estabelecer um formatação mediante preenchimento a esquerda
	 * 
	 * @param text texto para formatação
	 * @param tamanho tamanho do texto 
	 * @param preenchimento valor a ser preenchido
	 * @return String formatada
	 */
	public static String preencherEsquerda(String text, int tamanho, String preenchimento) {
		return preencher(tamanho - text.length(), preenchimento).concat(text);		
	}

	/**
	 * Método para estabelecer um formatação mediante preenchimento a direita
	 * 
	 * @param text texto para formatação
	 * @param tamanho tamanho do texto 
	 * @param preenchimento valor a ser preenchido
	 * @return String formatada
	 */
	public static String preencherDireita(String text, int tamanho, String preenchimento) {
		return text.concat(preencher(tamanho - text.length(), preenchimento));		
	}
	
	private static String preencher(int tamanho, String preenchimento) {
		// define valor padrao para o preenchimento caso nao seja definido
		if(StringUtils.isBlank(preenchimento)) {
			preenchimento = " ";
		}
		
		String newText = "";
		for (int i = 0; i < tamanho; i++) {
			newText = newText.concat(preenchimento);
		}
		
		return newText;
	}
	
	/**
	 * Capitalize
	 * @param texto - {@link String}
	 * @return String 
	 */
	public static String capitalize(String texto) {
		StringBuilder sb = new StringBuilder();
		texto = texto.substring(0, 1).concat(texto.substring(1).toLowerCase());
		if(!texto.contains(" ")){
			sb.append(texto);
		}
		while (texto.contains(" ")) {
			String[] f = texto.split(" ");
			for (int i = 0; i < f.length; i++) {
				texto = String.valueOf(f[i]);
				texto = texto.substring(0, 1).toUpperCase().concat(texto.substring(1));
				sb = sb.append(texto ).append(" ");
			}
		}
		return sb.toString();
	}

	/**
	 * Retirar Mascara
	 * @param cpfCnpj - {@link String}
	 * @return String 
	 */
	public static String retirarMascara(String cpfCnpj) {
		if (cpfCnpj != null) {
			cpfCnpj = cpfCnpj.replace(".", "");
			cpfCnpj = cpfCnpj.replace("-", "");
			cpfCnpj = cpfCnpj.replace("/", "");
		}
		
		return cpfCnpj;
	}
}
