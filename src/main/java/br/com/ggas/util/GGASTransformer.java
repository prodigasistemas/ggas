
package br.com.ggas.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import org.apache.commons.lang.StringUtils;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.transform.BasicTransformerAdapter;

import br.com.ggas.geral.exception.NegocioException;

/**
 * @author edmilson.santana
 *
 */
public class GGASTransformer extends BasicTransformerAdapter {

	private static final int LIMITE_QTD_CAMPOS = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, ClassMetadata> metadados;

	private static final String SEPARADOR_ALIAS = "_";

	private static final String QTD_CAMPOS_SEM_METADADOS_COM_MAPEAMENTO =
			"Caso a chave de mapeamento seja informado, mas os  metadados da classe não,"
					+ " selecione apenas dois campos, um para chave e outro para o valor.";

	private static final String QTD_CAMPOS_SEM_METADADOS_SEM_MAPEAMENTO =
			"Caso os metadados da classe e a chave de mapeamento não sejam informados, seleciona apenas um campo.";

	private static final String MENSAGEM_ALIAS_CHAVE_NAO_INFORMADO =
			"Informe o alias do campo que será utilizada como chave do mapeamento.";

	private static final String MENSAGEM_ALIAS_NAO_IDENTIFICADO =
			"Um alias associado a um campo selecionado não foi informado ou não está no formato adequado.";

	private Class<?> rootClass;

	private final String chaveMapeamento;

	/**
	 * Construtor
	 * 
	 * @param rootClass
	 * @param metadados
	 */
	public GGASTransformer(Class<?> rootClass, Map<String, ClassMetadata> metadados) {
		this(rootClass, metadados, "");

	}

	/**
	 * Construtor para realizar apenas a seleção do objeto consultado, sem contruir o objeto com seus atributos.
	 * 
	 * 
	 * @param rootClass - {@link Class}
	 * @param chaveMapeamento - {@link String}
	 */
	public GGASTransformer(Class<?> rootClass, String chaveMapeamento) {
		this(rootClass, null, chaveMapeamento);
	}

	/**
	 * Construtor
	 * 
	 * @param rootClass
	 */
	public GGASTransformer(Class<?> rootClass) {
		this(rootClass, null, null);
	}

	/**
	 * Construtor
	 * 
	 * @param rootClass
	 * @param metadados
	 * @param chaveMapeamento
	 */
	public GGASTransformer(Class<?> rootClass, Map<String, ClassMetadata> metadados, String chaveMapeamento) {
		this.metadados = metadados;
		this.rootClass = rootClass;
		this.chaveMapeamento = chaveMapeamento;
	}

	/**
	 * 
	 * 
	 */
	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {

		return this.mapearObjeto(tuple, aliases);
	}

	@Override
	public List transformList(List list) {

		return this.agruparObjetos(list);
	}

	/**
	 * @param list
	 * @return list
	 */
	private List agruparObjetos(List list) {

		if (StringUtils.isNotBlank(chaveMapeamento)) {

			Map<Object, Collection<Object>> mapa = new HashMap<>();

			for (Object objeto : list) {
				this.adicionarObjetosEmMapa(mapa, (Map<Object, Object>) objeto);
			}

			list.clear();
			list.add(mapa);
		}
		return list;
	}

	/**
	 * 
	 * Método utilizado para montar mapas e agrupar coleções
	 * 
	 * @param mapa
	 * @param objeto
	 * 
	 */
	private void adicionarObjetosEmMapa(Map<Object, Collection<Object>> mapa, Map<Object, Object> objeto) {
		Collection<Object> objetos = null;

		for (Entry<Object, Object> entrada : objeto.entrySet()) {

			if (!mapa.containsKey(entrada.getKey())) {
				objetos = new ArrayList<>();
				mapa.put(entrada.getKey(), objetos);
			} else {
				objetos = mapa.get(entrada.getKey());
			}

			objetos.add(entrada.getValue());
		}
	}

	/**
	 * @param tuple
	 * @param aliases
	 * @return
	 */
	private Object mapearObjeto(Object[] tuple, String[] aliases) {

		Object objeto = null;

		try {

			if (possuiMetadados()) {
				objeto = this.novaInstancia(rootClass);
			} else if (!possuiChaveMapeamento() && !possuiMetadados() && tuple.length > 1) {
				throw new IllegalStateException(QTD_CAMPOS_SEM_METADADOS_SEM_MAPEAMENTO);
			} else if (possuiChaveMapeamento() && !possuiMetadados() && tuple.length != LIMITE_QTD_CAMPOS) {
				throw new IllegalStateException(QTD_CAMPOS_SEM_METADADOS_COM_MAPEAMENTO);
			}

			objeto = this.mapearObjeto(objeto, tuple, aliases);
		} catch (Exception e) {
			throw new IllegalStateException(e.getMessage(), e);
		}

		return objeto;
	}

	/**
	 * @param objeto
	 * @param tuple
	 * @param aliases
	 * @return
	 * @throws Exception
	 */
	private Object mapearObjeto(Object objeto, Object[] tuple, String[] aliases) throws Exception {

		Map<Object, Object> mapa = new HashMap<>();

		Object valorMapeamento = null;
		for (int index = 0; index < tuple.length; index++) {
			Object valor = tuple[index];
			String alias = aliases[index];

			if (objeto != null && !this.campoPossuiAliasChaveMapeamento(alias)) {
				this.adicionarCampos(objeto, valor, alias);
			} else if (objeto == null && !alias.equals(this.chaveMapeamento)) {
				objeto = valor; // NOSONAR { é necessário manter a referência do objeto }
			}

			if (this.campoPossuiAliasChaveMapeamento(alias)) {
				valorMapeamento = valor;
			}
		}

		if (this.possuiChaveMapeamento() && valorMapeamento != null) {
			mapa.put(valorMapeamento, objeto);
			objeto = mapa; // NOSONAR { é necessário manter a referência do objeto }
		} else if (this.possuiChaveMapeamento()) {
			throw new IllegalStateException(MENSAGEM_ALIAS_CHAVE_NAO_INFORMADO);
		}

		return objeto;
	}

	/**
	 * @param objeto
	 * @param valor
	 * @param alias
	 * @throws Exception
	 */
	private void adicionarCampos(Object objeto, Object valor, String alias) throws Exception {
		
		Queue<String> campos = new LinkedList<>(Arrays.asList(alias.split(SEPARADOR_ALIAS)));
		if (!campos.isEmpty()) {
			this.adicionarValorCampo(objeto, valor, campos);
		} else {
			throw new IllegalStateException(MENSAGEM_ALIAS_NAO_IDENTIFICADO);
		}
	}

	/**
	 * Adiciona ao objeto passado por parâmetro o objeto valor
	 * 
	 * @param objeto - {@link Object}
	 * @param valor  - {@link Object}
	 * @param campos - {@link Queue}
	 * @throws Exception - {@link Exception}
	 */
	private void adicionarValorCampo(Object objeto, Object valor, Queue<String> campos) throws Exception {

		Class<?> classe = objeto.getClass();
		String nomeCampo = campos.poll();
		Field campo = Util.getCampo(nomeCampo, classe);

		if (campo == null) {
			throw new IllegalStateException(MENSAGEM_ALIAS_NAO_IDENTIFICADO);
		}
		if (valor != null) {
			if (!campos.isEmpty()) {
				Class<?> classeCampo = this.getClasseCampo(classe, nomeCampo);

				Object objetoCampo = campo.get(objeto);
				if (objetoCampo == null) {
					objetoCampo = this.novaInstancia(classeCampo);
					campo.set(objeto, objetoCampo);
				}

				this.adicionarValorCampo(objetoCampo, valor, campos);
			} else {
				campo.set(objeto, valor);
			}
		}
	}

	/**
	 * Cria uma novo objeto da classe passada por parâmetro.
	 * 
	 * @param classe
	 * @return Object - {@link Object}
	 * @throws Exception - {@link Exception}
	 */
	private Object novaInstancia(Class<?> classe) throws NegocioException {
		Object objeto = null;
		try {
			Constructor<?> constructor = classe.getDeclaredConstructor();
			constructor.setAccessible(Boolean.TRUE);
			objeto = constructor.newInstance();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}

		return objeto;
	}

	/**
	 * Obtém dos metadados da classe informada, a classe do campo de nome informado no parâmetro.
	 * 
	 * @param classe - {@link Class}
	 * @param nomeCampo - {@link String}
	 * @return classe do campo - {@link Class}
	 */
	private Class<?> getClasseCampo(Class<?> classe, String nomeCampo) {
		Class<?> classeCampo = null;
		if (this.possuiMetadados()) {
			ClassMetadata metadadosClasse = metadados.get(classe.getName());
			if (metadadosClasse != null) {
				classeCampo = metadadosClasse.getPropertyType(nomeCampo).getReturnedClass();
			}
		}
		return classeCampo;
	}

	/**
	 * @return
	 */
	private Boolean possuiChaveMapeamento() {
		return StringUtils.isNotBlank(this.chaveMapeamento);
	}

	/**
	 * @param alias
	 * @return
	 */
	private Boolean campoPossuiAliasChaveMapeamento(String alias) {
		return this.possuiChaveMapeamento() && this.chaveMapeamento.equals(alias);
	}

	private Boolean possuiMetadados() {
		return metadados != null;
	}

}
