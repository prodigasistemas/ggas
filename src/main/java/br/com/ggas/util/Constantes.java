/*
I Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.contabil.LancamentoContabilSintetico;

/**
 * The Class Constantes.
 *
 * @author fabiana
 */
public final class Constantes {

	/**
	 * Bloco de arquivos de configuração Neste bloco constarão os arquivos de
	 * configuração do sistema.
	 */

	public static final String ARQUIVO_MENSAGENS = "mensagens";

	/** The Constant ARQUIVO_PROPRIEDADES. */
	public static final String ARQUIVO_PROPRIEDADES = "constantes.properties";

	/** The Constant MENSAGEM_PROPRIEDADES. */
	public static final String MENSAGEM_PROPRIEDADES = "mensagens.properties";

	/** The Constant ATRIBUTO_USUARIO_LOGADO. */
	public static final String ATRIBUTO_USUARIO_LOGADO = "usuarioLogado";

	/** The Constant ERRO_ARRECADADOR_EM_USO. */
	public static final String ERRO_ARRECADADOR_EM_USO = "ERRO_ARRECADADOR_EM_USO";
	
	/** The Constant ERRO_AGENTE_NEGATIVADOR_EM_USO. */
	public static final String ERRO_AGENTE_NEGATIVADOR_EM_USO = "ERRO_AGENTE_NEGATIVADOR_EM_USO";
	
	/** The Constant ERRO_COMANDO_NEGATIVACAO. */
	public static final String ERRO_COMANDO_NEGATIVACAO = "ERRO_COMANDO_NEGATIVACAO";

	/**
	 * ambiente de homologação NFE (usado para emitir nota fiscal no ambiente de
	 * homologação).
	 */

	public static final String ERRO_GENERICO_DE_SISTEMAS = "ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA";

	/** The Constant HOMOLOGACAO. */
	public static final String HOMOLOGACAO = "2";

	/** The Constant ATRIBUTO_OPERACAO. */
	public static final String ATRIBUTO_OPERACAO = "operacao";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_CONSULTAR = "ERRO_NEGOCIO_SEM_PERMISSAO_CONSULTAR";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_EXECUTAR = "ERRO_NEGOCIO_SEM_PERMISSAO_EXECUTAR";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_REEXECUTAR = "ERRO_NEGOCIO_SEM_PERMISSAO_REEXECUTAR";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_AGENDAR = "ERRO_NEGOCIO_SEM_PERMISSAO_AGENDAR";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_CANCELAR = "ERRO_NEGOCIO_SEM_PERMISSAO_CANCELAR";
	public static final String ERRO_NEGOCIO_SEM_PERMISSAO_EXCLUIR = "ERRO_NEGOCIO_SEM_PERMISSAO_EXCLUIR";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Constantes.class);

	/**
	 * Bloco para as constantes que estão declaradas no arquivo
	 * constantes.properties
	 */

	public static String SERVIDOR_HELP;//NOSONAR constante definida em properties.

	/**
	 * Bloco para as constantes que estão declaradas no arquivo
	 * constantes.properties
	 */

	public static String PATH_GGAS_JAR;//NOSONAR constante definida em properties.

	public static String GERADOR_MATRICULA_SQL_NEXT_ID;//NOSONAR constante definida em properties.

	/** The quantidade casas valor decimal. */
	public static Integer QUANTIDADE_CASAS_VALOR_DECIMAL;//NOSONAR constante definida em properties.

	/** The gerar log faturamento. */
	public static String GERAR_LOG_FATURAMENTO;//NOSONAR constante definida em properties.

	/** The locale padrao. */
	public static Locale LOCALE_PADRAO;//NOSONAR constante definida em properties.

	/** The lingua padrao. */
	public static String LINGUA_PADRAO;//NOSONAR constante definida em properties.

	/** The pais padrao. */
	public static String PAIS_PADRAO;//NOSONAR constante definida em properties.

	/** The hash criptografia. */
	public static String HASH_CRIPTOGRAFIA;//NOSONAR constante definida em properties.

	/** The altura maxima imagem. */
	public static int ALTURA_MAXIMA_IMAGEM;//NOSONAR constante definida em properties.

	/** The largura maxima imagem. */
	public static int LARGURA_MAXIMA_IMAGEM;//NOSONAR constante definida em properties.

	/** The permitir registro leitura com anormalidade existente. */
	public static boolean PERMITIR_REGISTRO_LEITURA_COM_ANORMALIDADE_EXISTENTE;//NOSONAR constante definida em properties.

	/** The url logomarca ggas. */
	public static String URL_LOGOMARCA_GGAS;//NOSONAR constante definida em properties.

	/** The url logomarca banco. */
	public static String URL_LOGOMARCA_BANCO;//NOSONAR constante definida em properties.
	
	/** The api google maps **/
	public static String CHAVE_API_GOOGLE_MAPS;
	
	/** The url imagens ggas **/
	public static String URL_IMAGENS_GGAS;

	/** Outras constantes ainda não definidas. */

	public static final String FORMATO_DATA_DDMMYY = "ddMMyy";

	/** The Constant FORMATO_DATA_US_SEM_BARRA. */
	public static final String FORMATO_DATA_US_SEM_BARRA = "yyyyMMdd";

	/** The Constant FORMATO_DATA_DIA_MES_BR_SEM_BARRA. */
	public static final String FORMATO_DATA_DIA_MES_BR_SEM_BARRA = "ddMM";

	/** The Constant FORMATO_DATA_DIA_MES_ANO_BR_SEM_BARRA. */
	public static final String FORMATO_DATA_DIA_MES_ANO_BR_SEM_BARRA = "ddMMyyyy";

	/** The Constant FORMATO_DATA_BR. */
	public static final String FORMATO_DATA_BR = "dd/MM/yyyy";

	/** The Constant FORMATO_DATA_US. */
	public static final String FORMATO_DATA_US = "yyyy/MM/dd";

	/** The Constant FORMATO_DATA_HORA_BR. */
	public static final String FORMATO_DATA_HORA_BR = "dd/MM/yyyy HH:mm:ss";

	/** The Constant FORMATO_DATA_HORA_SEM_SEGUNDOS_BR. */
	public static final String FORMATO_DATA_HORA_SEM_SEGUNDOS_BR = "dd/MM/yyyy HH:mm";

	/** The Constant FORMATO_DATA_HORA_US. */
	public static final String FORMATO_DATA_HORA_US = "yyyy-MM-dd HH:mm:ss";

	/** The Constant FORMATO_DATA_HORA_US_COMPLETA. */
	public static final String FORMATO_DATA_HORA_US_COMPLETA = "yyyy-MM-dd HH:mm:ss.S";

	/** The Constant FORMATO_DATA_BR_MES_ANO. */
	public static final String FORMATO_DATA_BR_MES_ANO = "MM/yyyy";

	/** The Constant FORMATO_DATA_ANO_MES. */
	public static final String FORMATO_DATA_ANO_MES = "yyyyMM";

	/** The Constant FORMATO_DATA_ANO. */
	public static final String FORMATO_DATA_ANO = "yyyy";

	/** The Constant FORMATO_DATA_DIA. */
	public static final String FORMATO_DATA_DIA = "dd";

	/** The Constant FORMATO_DATA_BR_MES_EXT_ANO. */
	public static final String FORMATO_DATA_BR_MES_EXT_ANO = "MMM/yyyy";

	/** The Constant FORMATO_HORA. */
	public static final String FORMATO_HORA = "HH:mm";

	/** The Constant FORMATO_VALOR_BR. */
	public static final String FORMATO_VALOR_BR = "#,###.##";

	/** The Constant FORMATO_VALOR_MONETARIO_BR. */
	public static final String FORMATO_VALOR_MONETARIO_BR = "#,##0.00";

	/** The Constant FORMATO_VALOR_FATOR_K. */
	public static final String FORMATO_VALOR_FATOR_K = "#,###.####";

	/** The Constant FORMATO_VALOR_METROS_CUBICOS. */
	public static final String FORMATO_VALOR_METROS_CUBICOS = "#,##0.0000";

	/** The Constant FORMATO_VALOR_FATOR_Z. */
	public static final String FORMATO_VALOR_FATOR_Z = "##.########";

	/** The Constant FORMATO_VALOR_NUMERO. */
	public static final String FORMATO_VALOR_NUMERO = "#,###.####";

	public static final String FORMATO_VALOR_NUMERO_SEM_SEPARADOR = "####.####";

	/** The Constant FORMATO_VALOR_NUMERO_INTEIRO. */
	public static final String FORMATO_VALOR_NUMERO_INTEIRO = "#,###";

	/** The Constant FORMATO_VALOR_4_DECIMAIS. */
	public static final String FORMATO_VALOR_4_DECIMAIS = "##.####";

	/** The Constant FORMATO_VALOR_2_DECIMAIS. */
	public static final String FORMATO_VALOR_2_DECIMAIS = "##.##";

	/** The Constant FORMATO_VALOR_6_DECIMAIS. */
	public static final String FORMATO_VALOR_6_DECIMAIS = "##.######";

	/** The Constant STRING_TABULACAO. */
	public static final String STRING_TABULACAO = "\t";

	/** The Constant STRING_VIRGULA_ESPACO. */
	public static final String STRING_VIRGULA_ESPACO = ", ";

	/** The Constant STRING_DOIS_PONTOS_ESPACO. */
	public static final String STRING_DOIS_PONTOS_ESPACO = ": ";

	/** The Constant STRING_DOIS_PONTOS_TABULACAO. */
	public static final String STRING_DOIS_PONTOS_TABULACAO = ":\t";

	/** The Constant STRING_QUEBRA_LINHA_TABULACAO. */
	public static final String STRING_QUEBRA_LINHA_TABULACAO = "\r\n\t";

	/** The Constant STRING_HIFEN_ESPACO. */
	public static final String STRING_HIFEN_ESPACO = " - ";

	public static final String STRING_HIFEN = "-";

	/** The Constant STRING_VIRGULA. */
	public static final String STRING_VIRGULA = ",";

	/** The Constant STRING_PONTO. */
	public static final String STRING_PONTO = ".";

	/** The Constant STRING_IGUAL. */
	public static final String STRING_IGUAL = "=";

	/** The Constant RETORNO_PESQUISAR_CONTRATO. */
	public static final String RETORNO_PESQUISAR_CONTRATO = "pesquisa";

	/** The Constant EMITIR_FATURAS. */
	public static final String EMITIR_FATURAS = "EMITIR FATURAS";

	/**
	 * The Constant
	 * ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO.
	 */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO =
			"ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_DATA_INICIO_VIGENCIA. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_DATA_INICIO_VIGENCIA = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_DATA_INICIO_VIGENCIA";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_INDICE_CORRECAO_MONETARIA. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_INDICE_CORRECAO_MONETARIA =
			"ERRO_CONTRATO_ADEQUACAO_PARCERIA_INDICE_CORRECAO_MONETARIA";

	/** SUCESSO_ENTIDADE_TRAMITADA. */
	public static final String SUCESSO_ENTIDADE_TRAMITADA = "SUCESSO_ENTIDADE_TRAMITADA";

	/** SUCESSO_ENTIDADE_REATIVADO. */
	public static final String SUCESSO_ENTIDADE_REATIVADO = "SUCESSO_ENTIDADE_REATIVADO";

	/** SUCESSO_ENTIDADE_REABERTO. */
	public static final String SUCESSO_ENTIDADE_REABERTO = "SUCESSO_ENTIDADE_REABERTO";

	/** SUCESSO_ENTIDADE_REITERADO. */
	public static final String SUCESSO_ENTIDADE_REITERADO = "SUCESSO_ENTIDADE_REITERADO";

	/** ARRECADADOR_SEM_CONTRATO. */
	public static final String ARRECADADOR_SEM_CONTRATO = "ARRECADADOR_SEM_CONTRATO";

	/** SUCESSO_REITERADA. */
	public static final String SUCESSO_ENTIDADE_REITERADA = "SUCESSO_ENTIDADE_REITERADA";

	/** SUCESSO_ENCERRADA. */
	public static final String SUCESSO_ENTIDADE_ENCERRADA = "SUCESSO_ENTIDADE_ENCERRADA";

	/** SUCESSO_CAPTURADA. */
	public static final String SUCESSO_ENTIDADE_CAPTURADA = "SUCESSO_ENTIDADE_CAPTURADA";

	/** SUCESSO_ENTIDADE_EXECUTADA. */
	public static final String SUCESSO_ENTIDADE_EXECUTADA = "SUCESSO_ENTIDADE_EXECUTADA";

	/** SUCESSO_ENTIDADE_PENDENTE_ATUALIZACAO. */
	public static final String SUCESSO_ENTIDADE_PENDENTE_ATUALIZACAO = "SUCESSO_ENTIDADE_PENDENTE_ATUALIZACAO";

	/** SUCESSO_CAPTURADA. */
	public static final String SUCESSO_ENTIDADE_REMANEJADA = "SUCESSO_ENTIDADE_REMANEJADA";

	/** DELIMITADOR_ARQUIVO_IMPORTACA0. */
	public static final String DELIMITADOR_ARQUIVO_IMPORTACA0 = ";";

	/**
	 * Padrão para validações de data no formato DD/MM/YYYY.
	 */
	public static final String EXPRESSAO_REGULAR_DATA_DD_MM_YYYY = "\\d\\d/\\d\\d/\\d\\d\\d\\d";

	/**
	 * Padrão para validações de data no formato DD/MM.
	 */
	public static final String EXPRESSAO_REGULAR_DATA_DD_MM = "\\d\\d/\\d\\d";

	/**
	 * Padrão para validações de hora no formato HH:MM:SS.
	 */
	public static final String EXPRESSAO_REGULAR_HORA_HH_MM_SS = "\\d\\d:\\d\\d:\\d\\d";

	/**
	 * Padrão para validações de hora no formato HH:MM.
	 */
	public static final String EXPRESSAO_REGULAR_HORA_HH_MM = "\\d\\d:\\d\\d";

	/**
	 * Padrão para validações de data no formato DD/MM/YYYY HH:MM:SS.
	 */
	public static final String EXPRESSAO_REGULAR_DATA_HORA_DD_MM_YYYY_HH_MM_SS = "\\d\\d/\\d\\d/\\d\\d\\d\\d\\s\\d\\d:\\d\\d:\\d\\d";

	/** The Constant EXPRESSAO_REGULAR_EMAIL. */
	public static final String EXPRESSAO_REGULAR_EMAIL = "^[a-zA-Z0-9\\._-]+@[a-zA-Z0-9\\._-]+\\.([a-zA-Z]{2,4})$";

	/** SEPARADOR_EMAIL. */
	public static final String SEPARADOR_EMAIL = ";";

	/** The Constant EXPRESSAO_REGULAR_URL. */
	public static final String EXPRESSAO_REGULAR_URL = "^http://[^@!$%#*\\+\\(\\)\\s]*";

	/** The Constant EXPRESSAO_REGULAR_CARACTERES_VALIDOS. */
	public static final String EXPRESSAO_REGULAR_CARACTERES_VALIDOS = "[A-Za-zá-úÁ-Ú0-9\\u0020\\u002d]*";

	/** The Constant EXPRESSAO_REGULAR_NUMEROS. */
	public static final String EXPRESSAO_REGULAR_NUMEROS = "[0-9]*";

	/** The Constant EXPRESSAO_REGULAR_HORA. */
	public static final String EXPRESSAO_REGULAR_HORA = "[0-9\\u003a]*";

	/** The Constant EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES. */
	public static final String EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES = "[A-Za-zá-úÁ-Ú0-9\\u0020\\u002f\\u002d"
			+ "\\u002b\\u003f\\u003a\\u002e\\u0027\\u0028\\u0029\\u002c\\u0022]*";

	/** Codigos Numéricos usados na Expressao Regular na constante. */
	public static final String EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES_ALFA_SEM_ACENTO = "[A-Za-z0-9\\-]*";

	/** Codigos Numéricos usados na Expressao Regular na constante. */
	public static final String EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS = "[0-9\\u002e\\u002c]*";
	
	/** The Constant EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME. */
	public static final String EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME = "[A-Za-z0-9\\s\\-\\u0020\\u002d]*";

	/** Constante com código do parâmetro que indica se o faturamento executa o preparar e processar dados em paralelo. **/
	public static final String INDICADOR_FATURAMENTO_PARALELO = "INDICADOR_FATURAMENTO_PARALELO";
	
	/** Constante com código do parâmetro que indica se o faturamento executa a contabilização dos lançamentos contábeis. **/
	public static final String INDICADOR_CONTABILIZAR_LANCAMENTO_CONTABIL = "INDICADOR_CONTABILIZAR_LANCAMENTO_CONTABIL";
	
	/** Constante com código do parâmetro que indica se o ambiente e o de producao. **/
	public static final String INDICADOR_AMBIENTE_PRODUCAO = "INDICADOR_AMBIENTE_PRODUCAO";

	/**
	 * The Constant
	 * EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME_SETOR_COMERCIAL.
	 */
	public static final String EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME_SETOR_COMERCIAL = "[A-Za-z0-9\\@\\.\\s\\-\\u0020\\u002d]*";

	/** The Constant MASCARA_CNPJ. */
	public static final String MASCARA_CNPJ = "##.###.###/####-##";

	/** The Constant MASCARA_CPF. */
	public static final String MASCARA_CPF = "###.###.###-##";

	/** The Constant CPF. */
	public static final String CPF = "CPF";

	/** The Constant RG. */
	public static final String RG = "CLIENTE_RG";

	/** The Constant CNPJ. */
	public static final String CNPJ = "CLIENTE_CNPJ";

	/** The Constant INSCRICAO_ESTADUAL. */
	public static final String INSCRICAO_ESTADUAL = "CLIENTE_INSCRICAO_ESTADUAL";

	/** The Constant INSCRICAO_MUNICIPAL. */
	public static final String INSCRICAO_MUNICIPAL = "CLIENTE_INSCRICAO_MUNICIPAL";

	/** The Constant INSCRICAO_RURAL. */
	public static final String INSCRICAO_RURAL = "CLIENTE_INSCRICAO_RURAL";

	/** The Constant EXTENSAO_ARQUIVO_PDF. */
	public static final String EXTENSAO_ARQUIVO_PDF = ".pdf";

	/**
	 * Constantes referente a mensagem de erro de negócio que ocorre quando uma periodicidade é duplicada.
	 */
	public static final String ERRO_NEGOCIO_PERIODICIDADE_DUPLICIDADE = "ERRO_NEGOCIO_PERIODICIDADE_DUPLICIDADE";
	
	/**
	 * Chave do token de transação única armazenado na sessão
	 */
	public static final String TRANSACTION_SESSION_TOKEN = "TRANSACTION_SESSION_TOKEN";
	
	/**
	 * Chave do token de transação única armazenado na requisição
	 */
	public static final String TRANSACTION_REQUEST_TOKEN = "TRANSACTION_REQUEST_TOKEN";
	
	/**
	 * The Constant SEM_NUMERO.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String SEM_NUMERO = "S/N";

	/** The Constant FORMATO_ARQUIVO_XLS. */
	public static final String FORMATO_ARQUIVO_XLS = "application/vnd.ms-excel";

	/** The Constant FORMATO_ARQUIVO_XLSX. */
	public static final String FORMATO_ARQUIVO_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	/** The Constant PLANILHA_GGAS. */
	public static final String PLANILHA_GGAS = "GGAS";

	/** The Constant EXPRESSAO_REGULAR_VIRGULA. */
	public static final String EXPRESSAO_REGULAR_VIRGULA = "\\,";

	/** The Constant EXPRESSAO_REGULAR_PONTO. */
	public static final String EXPRESSAO_REGULAR_PONTO = "\\.";

	/**
	 * The Constant ID_PRODUTO_ARRECADACAO.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String ID_PRODUTO_ARRECADACAO = "8";

	/**
	 * The Constant ID_SEGMENTO_GAS.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String ID_SEGMENTO_GAS = "3";

	/**
	 * The Constant ID_VALOR_REAL.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String ID_VALOR_REAL = "6";

	/** The Constant DIRETORIO_COMPLEMENTAR_ERROS. */
	public static final String DIRETORIO_COMPLEMENTAR_ERROS = File.separator + "erros" + File.separator;

	/** The Constant DIRETORIO_COMPLEMENTAR_PROCESSADOS. */
	public static final String DIRETORIO_COMPLEMENTAR_PROCESSADOS = File.separator +  "processados" + File.separator;

	/** The Constant QUANTIDADE_CLIENTES_RELATORIO_LOTE. */
	public static final String QUANTIDADE_CLIENTES_RELATORIO_LOTE = "QUANTIDADE_CLIENTES_RELATORIO_LOTE";

	/** The Constant NUMERO_DIAS_REVISAO_FATURA. */
	public static final String NUMERO_DIAS_REVISAO_FATURA = "NUMERO_DIAS_REVISAO_FATURA";

	/** The Constant DATA_BASE_FATOR_VENCIMENTO. */
	public static final String DATA_BASE_FATOR_VENCIMENTO = "07/10/1997";

	/** The url logomarca empresa. */
	public static String URL_LOGOMARCA_EMPRESA; //NOSONAR constante definida em properties.

	/** The Constant CODIGO_RECURSO_AUTORIZACAO. */
	public static final Long CODIGO_RECURSO_AUTORIZACAO = 477L;

	public static final String VAZIO = "";

	public static final int ANO_1960 = 1960;

	public static final String HEADER_PARAM = "attachment";

	public static final String MEDIA_TYPE = "application/pdf";

	/** The Constant PAGINACAO_PADRAO */
	public static final int PAGINACAO_PADRAO = 15;

	public static final String NOTA_DEBITO_CREDITO_NAO_CONCILIADA = "NOTA_DEBITO_CREDITO_NAO_CONCILIADA";

	public static final String SEPARADOR_DECIMAL_REGEX = "[,\\.]";

	/**
	 * Booleano que informa se o sistema está executando em abiente de produção ou
	 * desenvolvimento, por padrão sempre é produção (true)
	 *
	 * squid:S1444 Não pode ser final, pois o valor desta variável é carregado no
	 * método iniciarPropriedades. Deve ser pública para ser acessada por uma classe
	 * externa
	 */
	@SuppressWarnings({ "squid:S1444", "findbugs:MS_PKGPROTECT" })
	public static Boolean IS_AMBIENTE_PRODUCAO = true;

	/**
	 * String que informa um host de uma agência virtual.
	 *
	 * squid:S1444 Não pode ser final, pois o valor desta variável é carregado no
	 * método iniciarPropriedades. Deve ser pública para ser acessada por uma classe
	 * externa
	 */
	@SuppressWarnings({ "squid:S1444", "findbugs:MS_PKGPROTECT" })
	public static String HOST_AGENCIA_VIRTUAL;

	/**
	 * Booleano que informa se o sistema executará os processos em batch em paralelo.
	 *
	 * squid:S1444 Não pode ser final, pois o valor desta variável é carregado no
	 * método iniciarPropriedades. Deve ser pública para ser acessada por uma classe
	 * externa
	 */
	@SuppressWarnings({ "squid:S1444", "findbugs:MS_PKGPROTECT" })
	public static Boolean IS_PARALLEL_BATCH = false;

	/**
	 * Lista de nomes abreviados de clientes. Criada temporariamente para
	 * identificação do cliente associado à empresa principal para definir a forma
	 * de obter a QDC no cálculo da multa recisória do contrato.
	 */
	public static enum NomeClienteAbreviado {

		/** The compagas. */
		// FIXME: URGENTE - Criar parâmetro para definição da regra do cálculo da
		// rescisão contratual
		@Deprecated
		COMPAGAS("COMPAGAS"),
		/** The algas. */
		@Deprecated
		ALGAS("ALGAS");

		/** The valor. */
		private final String valor;

		/**
		 * Instantiates a new nome cliente abreviado.
		 *
		 * @param valor the valor
		 */
		NomeClienteAbreviado(String valor) {

			this.valor = valor;
		}

		/**
		 * Gets the valor.
		 *
		 * @return the valor
		 */
		public String getValor() {

			return valor;
		}
	}

	/**
	 * The Enum TipoUnidadeTipo.
	 */
	public static enum TipoUnidadeTipo {

		/** The c. */
		// FIXME: Transformar em entidade conteudo
		@Deprecated
		C("C"),
		/** The t. */
		@Deprecated
		T("T"),
		/** The l. */
		@Deprecated
		L("L"),
		/** The g. */
		@Deprecated
		G("G");

		/** The valor. */
		private final String valor;

		/**
		 * Instantiates a new tipo unidade tipo.
		 *
		 * @param valor the valor
		 */
		TipoUnidadeTipo(String valor) {

			this.valor = valor;
		}

		/**
		 * Gets the valor.
		 *
		 * @return the valor
		 */
		public String getValor() {

			return valor;
		}
	}

	/**
	 * The Enum TipoLeitura.
	 */
	public static enum TipoLeitura {

		/** The eletrocorretor. */
		// FIXME: URGENTE - Substituir por constante
		@Deprecated
		ELETROCORRETOR("1"),
		/** The planilha. */
		@Deprecated
		PLANILHA("2"),
		/** The handheld. */
		@Deprecated
		HANDHELD("3");

		/** The valor. */
		private final String valor;

		/**
		 * Instantiates a new tipo leitura.
		 *
		 * @param valor the valor
		 */
		TipoLeitura(String valor) {

			this.valor = valor;
		}

		/**
		 * Gets the valor.
		 *
		 * @return the valor
		 */
		public String getValor() {

			return valor;
		}
	}

	/**
	 * The Enum TipoUnidade.
	 */
	public static enum TipoUnidade {

		/** The celsius. */
		// FIXME: URGENTE - Substituir por constante
		@Deprecated
		CELSIUS("31"),
		/** The kelvin. */
		@Deprecated
		KELVIN("32"),
		/** The fahrenheit. */
		@Deprecated
		FAHRENHEIT("33"),
		/** The RÉAMUR. */
		@Deprecated
		REAMUR("34");

		/** The valor. */
		private final String valor;

		/**
		 * Instantiates a new tipo unidade.
		 *
		 * @param valor the valor
		 */
		TipoUnidade(String valor) {

			this.valor = valor;
		}

		/**
		 * Gets the valor.
		 *
		 * @return the valor
		 */
		public String getValor() {

			return valor;
		}
	}

	/**
	 * The Enum Numero Ciclo.
	 */
	public static enum Ciclo {
		/** Ciclo 1 */
		UM(1),

		/** Ciclo 2 */
		DOIS(2),

		/** Ciclo 3 */
		TRES(3),

		/** Ciclo 4 */
		QUATRO(4),

		/** Ciclo 5 */
		CINCO(5),
		
		/** Ciclo 6 */
		SEIS(6);

		/** The valor. */
		private final Integer valor;

		/**
		 * Instantiates a new tipo integracao.
		 *
		 * @param valor the valor
		 */
		Ciclo(Integer valor) {

			this.valor = valor;
		}

		/**
		 * Gets the valor.
		 *
		 * @return the valor
		 */
		public Integer getValor() {

			return valor;
		}
	}

	/** The Constant ERRO_DESCRICAO. */
	public static final String ERRO_DESCRICAO = "ERRO_DESCRICAO";

	/** The Constant SIM. */
	public static final String SIM = "Sim";

	/** The Constant NAO. */
	public static final String NAO = "Não";

	/** The Constant MOVEL. */
	public static final String MOVEL = "Móvel";

	/** The Constant ESTATICO. */
	public static final String ESTATICO = "Estático";

	/** The Constant FALSE. */
	public static final String FALSE = "false";

	/** The Constant TRUE. */
	public static final String TRUE = "true";

	/** The Constant AREA_CONSTRUIDA_FAIXA_INICIO. */
	public static final String AREA_CONSTRUIDA_FAIXA_INICIO = "0";

	/** The Constant AREA_CONSTRUIDA_FAIXA_FINAL. */
	public static final Integer AREA_CONSTRUIDA_FAIXA_FINAL = 99999;

	/** The Constant TAMANHO_MAXIMO_PERCENTUAL. */
	public static final String TAMANHO_MAXIMO_PERCENTUAL = "100";

	/** The Constant ENTIDADE_CLASSE. */
	public static final String ENTIDADE_CLASSE = "br.com.ggas.geral.impl.EntidadeClasseImpl";

	/** The Constant ENTIDADE_CONTEUDO. */
	public static final String ENTIDADE_CONTEUDO = "br.com.ggas.geral.impl.EntidadeConteudoImpl";

	/** The Constant UNIDADE_CLASSE. */
	public static final String UNIDADE_CLASSE = "br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl";

	/** The Constant UNIDADE. */
	public static final String UNIDADE = "br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl";

	/** The Constant CORRETOR_MARCA. */
	public static final String CORRETOR_MARCA = "br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl";

	/** The Constant CORRETOR_MODELO. */
	public static final String CORRETOR_MODELO = "br.com.ggas.medicao.vazaocorretor.impl.ModeloCorretorImpl";

	/** The Constant MEDIDOR_MARCA. */
	public static final String MEDIDOR_MARCA = "br.com.ggas.medicao.medidor.impl.MarcaMedidorImpl";

	/** The Constant MEDIDOR_MODELO. */
	public static final String MEDIDOR_MODELO = "br.com.ggas.medicao.medidor.impl.ModeloMedidorImpl";

	/** The Constant ATIVIDADE_ECONOMICA. */
	public static final String ATIVIDADE_ECONOMICA = "br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl";

	/** The Constant MICRORREGIAO. */
	public static final String MICRORREGIAO = "br.com.ggas.cadastro.geografico.impl.MicrorregiaoImpl";

	/** The Constant UNIDADE_TIPO. */
	public static final String UNIDADE_TIPO = "br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl";

	/** The Constant TRIBUTO. */
	public static final String TRIBUTO = "br.com.ggas.faturamento.tributo.impl.TributoImpl";

	/** The Constant TRIBUTO_ALIQUOTA. */
	public static final String TRIBUTO_ALIQUOTA = "br.com.ggas.faturamento.tributo.impl.TributoAliquotaImpl";

	/** The Constant TARIFA_VIGENCIA_TRIBUTO. */
	public static final String TARIFA_VIGENCIA_TRIBUTO = "br.com.ggas.faturamento.tarifa.impl.TarifaVigenciaTributoImpl";

	/** The Constant PONTO_CONSUMO_TRIBUTO. */
	public static final String PONTO_CONSUMO_TRIBUTO = "br.com.ggas.cadastro.imovel.impl.PontoConsumoTributoAliquotaImpl";

	/** The Constant RUBRICA_TRIBUTO. */
	public static final String RUBRICA_TRIBUTO = "br.com.ggas.faturamento.rubrica.impl.RubricaTributoImpl";

	/** The Constant PERFIL_QUADRA. */
	public static final String PERFIL_QUADRA = "br.com.ggas.cadastro.localidade.impl.PerfilQuadraImpl";

	/** The Constant FERIADO. */
	public static final String FERIADO = "br.com.ggas.geral.feriado.impl.FeriadoImpl";
	
	public static final String INSTALACAO_MEDIDOR = "br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl";

	/** The Constant UNIDADE_FEDERACAO. */
	public static final String UNIDADE_FEDERACAO = "br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl";

	/** The Constant MUNICIPIO. */
	public static final String MUNICIPIO = "br.com.ggas.cadastro.geografico.impl.MunicipioImpl";

	/** The Constant TIPO_SEGMENTO. */
	public static final String TIPO_SEGMENTO = "br.com.ggas.cadastro.imovel.impl.TipoSegmentoImpl";

	/** The Constant TIPO_CLIENTE. */
	public static final String TIPO_CLIENTE = "br.com.ggas.cadastro.cliente.impl.TipoClienteImpl";

	/** The Constant TRONCO. */
	public static final String TRONCO = "br.com.ggas.cadastro.operacional.impl.TroncoImpl";

	/** The Constant CLIENTE_SITUACAO. */
	public static final String CLIENTE_SITUACAO = "br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl";

	/** The Constant SITUACAO_IMOVEL. */
	public static final String SITUACAO_IMOVEL = "br.com.ggas.cadastro.imovel.impl.SituacaoImovelImpl";

	/** The Constant CITY_GATE. */
	public static final String CITY_GATE = "br.com.ggas.cadastro.operacional.impl.CityGateImpl";

	/** The Constant PAVIMENTO_CALCADA. */
	public static final String PAVIMENTO_CALCADA = "br.com.ggas.cadastro.imovel.impl.PavimentoCalcadaImpl";

	/** The Constant PAVIMENTO_RUA. */
	public static final String PAVIMENTO_RUA = "br.com.ggas.cadastro.imovel.impl.PavimentoRuaImpl";

	/** The Constant PADRAO_CONSTRUCAO. */
	public static final String PADRAO_CONSTRUCAO = "br.com.ggas.cadastro.imovel.impl.PadraoConstrucaoImpl";

	/** The Constant TIPO_BOTIJAO. */
	public static final String TIPO_BOTIJAO = "br.com.ggas.cadastro.imovel.impl.TipoBotijaoImpl";

	/** The Constant PERFIL_IMOVEL. */
	public static final String PERFIL_IMOVEL = "br.com.ggas.cadastro.imovel.impl.PerfilImovelImpl";

	/** The Constant AREA_CONSTRUIDA. */
	public static final String AREA_CONSTRUIDA = "br.com.ggas.cadastro.imovel.impl.AreaConstruidaFaixaImpl";

	/** The Constant CHAMADO_PROTOCOLO. */
	public static final String CHAMADO_PROTOCOLO = "br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo";

	/** The Constant CHAMADO_HISTORICO. */
	public static final String CHAMADO_HISTORICO = "br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico";
	
	/** The Constant REDE_MATERIAL. */
	public static final String REDE_MATERIAL = "br.com.ggas.cadastro.operacional.impl.RedeMaterialImpl";

	/** The Constant REDE_DIAMETRO. */
	public static final String REDE_DIAMETRO = "br.com.ggas.cadastro.operacional.impl.RedeDiametroImpl";

	/** The Constant PROFISSAO. */
	public static final String PROFISSAO = "br.com.ggas.cadastro.cliente.impl.ProfissaoImpl";

	/** The Constant TIPO_CONTATO. */
	public static final String TIPO_CONTATO = "br.com.ggas.cadastro.cliente.impl.TipoContatoImpl";

	/** The Constant AREA_TIPO. */
	public static final String AREA_TIPO = "br.com.ggas.cadastro.localidade.impl.AreaTipoImpl";

	/** The Constant ZONA_BLOQUEIO. */
	public static final String ZONA_BLOQUEIO = "br.com.ggas.cadastro.operacional.impl.ZonaBloqueioImpl";

	/** The Constant SETOR_CENSITARIO. */
	public static final String SETOR_CENSITARIO = "br.com.ggas.cadastro.dadocensitario.impl.SetorCensitarioImpl";

	/** The Constant ZEIS. */
	public static final String ZEIS = "br.com.ggas.cadastro.localidade.impl.ZeisImpl";

	/** The Constant MEIO_SOLICITACAO. */
	public static final String MEIO_SOLICITACAO = "br.com.ggas.atendimento.registroatendimento.impl.MeioSolicitacaoImpl";

	/** The Constant UNIDADE_NEGOCIO. */
	public static final String UNIDADE_NEGOCIO = "br.com.ggas.cadastro.localidade.impl.UnidadeNegocioImpl";

	/** The Constant GRUPO_FATURAMENTO. */
	public static final String GRUPO_FATURAMENTO = "br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl";

	/** The Constant TIPO_RELACIONAMETO_CLIENTE_IMOVEL. */
	public static final String TIPO_RELACIONAMETO_CLIENTE_IMOVEL = "br.com.ggas.cadastro.imovel.impl.TipoRelacionamentoClienteImovelImpl";

	/** The Constant TIPO_ENDERECO. */
	public static final String TIPO_ENDERECO = "br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl";

	/** The Constant TIPO_FONE. */
	public static final String TIPO_FONE = "br.com.ggas.cadastro.cliente.impl.TipoFoneImpl";

	/** The Constant PERIODICIDADE. */
	public static final String PERIODICIDADE = "br.com.ggas.medicao.rota.impl.PeriodicidadeImpl";

	/** The Constant UNIDADE_ORGANIZACIONAL. */
	public static final String UNIDADE_ORGANIZACIONAL = "br.com.ggas.cadastro.unidade.impl.UnidadeOrganizacionalImpl";

	/** The Constant HISTORICO_CONSUMO. */
	public static final String HISTORICO_CONSUMO = "br.com.ggas.medicao.consumo.impl.HistoricoConsumoImpl";

	/** The Constant CREDITO_DEBITO_NEGOCIADO. */
	public static final String CREDITO_DEBITO_NEGOCIADO = "br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoNegociadoImpl";

	/** The Constant CREDITO_DEBITO_A_REALIAR. */
	public static final String CREDITO_DEBITO_A_REALIAR = "br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoARealizarImpl";

	/** The Constant SUPERVISORIO_MEDICAO_DIARIA. */
	public static final String SUPERVISORIO_MEDICAO_DIARIA = "br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl";

	/** The Constant INICIANDO_TRATAMENTO_MEDICOES_HORARIAS. */
	public static final String INICIANDO_TRATAMENTO_MEDICOES_HORARIAS = "Iniciando o tratamento das medições horárias \n";

	/** The Constant CONCLUINDO_TRATAMENTO_MEDICOES_HORARIAS. */
	public static final String CONCLUINDO_TRATAMENTO_MEDICOES_HORARIAS = "Concluindo o tratamento das medições horárias";

	/** The Constant INICIANDO_TRATAMENTO_MEDICOES_DIARIAS. */
	public static final String INICIANDO_TRATAMENTO_MEDICOES_DIARIAS = "Iniciando o tratamento das medições diárias";

	/** The Constant CONCLUINDO_TRATAMENTO_MEDICOES_DIARIAS. */
	public static final String CONCLUINDO_TRATAMENTO_MEDICOES_DIARIAS = "Concluindo o tratamento das medições diárias";

	/** The Constant REGISTRO_MEDICOES_HORARIA_PROCESSADOS. */
	public static final String REGISTRO_MEDICOES_HORARIA_PROCESSADOS = "Registros de medição horária processados: ";

	/** The Constant REGISTRO_MEDICOES_DIARIA_PROCESSADOS. */
	public static final String REGISTRO_MEDICOES_DIARIA_PROCESSADOS = "Registros de medição diária  processados: ";

	/** The Constant ANORMALIDADES_REGISTRADAS. */
	public static final String ANORMALIDADES_REGISTRADAS = "Anormalidades registradas: \n";

	/** The Constant PONTOS_CONSUMO_SEM_ROTA. */
	public static final String PONTOS_CONSUMO_SEM_ROTA = "Ponto(s) de consumo sem rota: \n";

	/**
	 * The Constant SUPERVISORIO_TIPO_MEDICAO_HORARIA.
	 *
	 */
	public static final String SUPERVISORIO_TIPO_MEDICAO_HORARIA = "H";

	/**
	 * The Constant SUPERVISORIO_TIPO_MEDICAO_DIARIA.
	 *
	 */
	public static final String SUPERVISORIO_TIPO_MEDICAO_DIARIA = "D";

	public static final String SUPERVISORIO_TIPO_MEDICAO_INCLUSAO_MANUAL = "M";

	public static final String SUPERVISORIO_TIPO_MEDICAO_INCLUSAO_AUTOMATICA = "A";

	/** The Constant NUMERO_DIAS_INCREMENTAR_DATA. */
	public static final int NUMERO_DIAS_INCREMENTAR_DATA = 1;

	/** The Constant DATA_INICIAL. */
	public static final String DATA_INICIAL = "dataInicial";

	/** The Constant DATA_FINAL. */
	public static final String DATA_FINAL = "dataFinal";

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_SUBSTITUICAO_CORRETOR_VAZAO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_SUBSTITUICAO_CORRETOR_VAZAO = 5L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_VIRADA_CORRETOR_VAZAO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_VIRADA_CORRETOR_VAZAO = 7L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_POSSIVEL_VIRADA_SEM_CORRETOR_VAZAO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_POSSIVEL_VIRADA_SEM_CORRETOR_VAZAO = 12L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MAIOR_24.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MAIOR_24 = 2L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MENOR_24.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MENOR_24 = 1L;
	
	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_CONSUMO_NEGATIVO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_CONSUMO_NEGATIVO = 28L;

	
	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_VIRADA_MEDIDOR.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_VIRADA_MEDIDOR = 29L;
	
	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PRIMEIRO_DIA_MEDICAO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PRIMEIRO_DIA_MEDICAO = 6L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_HORA_FRACIONADA.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_HORA_FRACIONADA = 3L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PC_SEM_CONTRATO_ATIVO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_SEM_CONTRATO_ATIVO = 10L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PC_NAO_IDENTIFICADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_NAO_IDENTIFICADO = 11L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PC_SEM_CORRETOR_VAZAO_INSTALADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_SEM_CORRETOR_VAZAO_INSTALADO = 9L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_REGISTRO_DIARIO_NAO_ENCONTRADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_REGISTRO_DIARIO_NAO_ENCONTRADO = 13L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_REGISTRO_HORA_FRACIONADA.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_REGISTRO_HORA_FRACIONADA = 3L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_VOLUME_DIVERGE.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_VOLUME_DIVERGE = 4L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_LEITURA_ANTERIOR_NAO_ENCONTRADA.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_LEITURA_ANTERIOR_NAO_ENCONTRADA = 14L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PC_INATIVO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_INATIVO = 16L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE = 17L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_REGISTRO_DUPLICADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_REGISTRO_DUPLICADO = 18L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_NAO_E_POSSIVEL_CALCULAR_O_FATOR.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_NAO_E_POSSIVEL_CALCULAR_O_FATOR = 19L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_CRONOGRAMA_FATURAMENTO_NAO_ENCONTRADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_CRONOGRAMA_FATURAMENTO_NAO_ENCONTRADO = 20L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_ENDERECO_REMOTO_NAO_LOCALIZADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_ENDERECO_REMOTO_NAO_LOCALIZADO = 21L;

	/**
	 * The Constant
	 * ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_MEDIDOR_VIRTUAL_PARA_MEDIDOR_INDEPENDENTE.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_MEDIDOR_VIRTUAL_PARA_MEDIDOR_INDEPENDENTE = 22L;

	/**
	 * The Constant
	 * ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_PONTO_CONSUMO_PARA_MEDIDOR_VIRTUAL.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_PONTO_CONSUMO_PARA_MEDIDOR_VIRTUAL = 23L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_MEDIDOR_SEM_CORRETOR_VAZAO_INSTALADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_MEDIDOR_SEM_CORRETOR_VAZAO_INSTALADO = 24L;

	/**
	 * The Constant
	 * ANORMALIDADE_SUPERVISORIO_PC_GRUPOS_FATURAMENTO_DIFERENTES_PARA_MI.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_GRUPOS_FATURAMENTO_DIFERENTES_PARA_MI = 25L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_PC_HORA_INICIAL_LEITURA_DIFERENTES.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_PC_HORA_INICIAL_LEITURA_DIFERENTES = 26L;

	/**
	 * The Constant ANORMALIDADE_SUPERVISORIO_MEDIDOR_NAO_INSTALADO.
	 *
	 */
	public static final Long ANORMALIDADE_SUPERVISORIO_MEDIDOR_NAO_INSTALADO = 27L;

	/** The Constant CONSUMO. */
	public static final String CONSUMO = "consumo";

	/** The Constant CONSUMO_COM_CORRECAO. */
	public static final String CONSUMO_COM_CORRECAO = "consumoComCorrecao";

	/** The Constant CONSUMO_SEM_CORRECAO. */
	public static final String CONSUMO_SEM_CORRECAO = "consumoSemCorrecao";

	/** The Constant SUBSTITUICAO_CORRETOR_VAZAO. */
	public static final String SUBSTITUICAO_CORRETOR_VAZAO = "isSubstituicaoCorretorVazao";

	/** The Constant LEITURA. */
	public static final String LEITURA = "leitura";

	/** The Constant LEITURA_COM_CORRECAO. */
	public static final String LEITURA_COM_CORRECAO = "leituraComCorrecao";

	/** The Constant SUPERVISORIO_MEDICAO_ANORMALIDADE_SUBSTITUICAO. */
	public static final String SUPERVISORIO_MEDICAO_ANORMALIDADE_SUBSTITUICAO = "supervisorioMedicaoAnormalidadeSubistituicao";

	/** The Constant SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA. */
	public static final String SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA = "supervisorioMedicaoAnormalidadeVirada";

	/** The Constant PULA_LINHA. */
	public static final String PULA_LINHA = "\n";

	/** The Constant STRING_ESPACO. */
	public static final String STRING_ESPACO = " ";

	/** The Constant STRING_BARRA. */
	public static final String STRING_BARRA = "/";

	/** The Constant ERRO_NEGOCIO_SEM_DADOS. */
	public static final String ERRO_NEGOCIO_SEM_DADOS = "Não há dados para serem processados.";

	/** The Constant NUMERO_HORAS_NO_DIA. */
	public static final int NUMERO_HORAS_NO_DIA = 24;

	/** The Constant HORA_INICIAL_CONTRATO. */
	public static final String HORA_INICIAL_CONTRATO = "dataHoraContrato";

	/** The Constant POSSUI_CONTRATO. */
	public static final String POSSUI_CONTRATO = "possuiContrato";

	/** The Constant PERIODICIDADE_CONTRATO. */
	public static final String PERIODICIDADE_CONTRATO = "periodicidade";

	/** The Constant QTD_MAXIMO_DIGITOS_CORRETOR_VAZAO_COMBO_PARAMETRO_SISTEMA. */
	public static final Integer QTD_MAXIMO_DIGITOS_CORRETOR_VAZAO_COMBO_PARAMETRO_SISTEMA = 9;

	/** The Constant POSSUI_LEITURA_ANTERIOR. */
	public static final String POSSUI_LEITURA_ANTERIOR = "possuiLeituraAnterior";

	/** The Constant SUPERVISORIO_MEDICAO_DIARIA_ANTERIOR. */
	public static final String SUPERVISORIO_MEDICAO_DIARIA_ANTERIOR = "supervisorioMedicaoDiariaAnterior";

	/** The Constant ESCALA_FATOR_PTZ. */
	public static final int ESCALA_FATOR_PTZ = 4;

	/**
	 * The Constant ATIVIDADE_SISTEMA_REGISTRAR_LEITURA.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final int ATIVIDADE_SISTEMA_REGISTRAR_LEITURA = 3;

	/** The Constant ERRO_DADOS_INVALIDOS_SUPERVISORIO. */
	public static final String ERRO_DADOS_INVALIDOS_SUPERVISORIO = "Dados inválidos";

	/** The Constant ANORMALIDADE_IMPEDE_PROCESSAMENTO. */
	public static final Boolean ANORMALIDADE_IMPEDE_PROCESSAMENTO = Boolean.TRUE;

	/** The Constant INDICADOR_PROCESSADO. */
	public static final Boolean INDICADOR_PROCESSADO = Boolean.TRUE;

	/** The Constant INDICADOR_INTEGRADO. */
	public static final Boolean INDICADOR_INTEGRADO = Boolean.TRUE;

	/** The Constant INDICADOR_CONSOLIDADO. */
	public static final Boolean INDICADOR_CONSOLIDADO = Boolean.TRUE;

	/** The Constant INDICADOR_HABILITADO. */
	public static final Boolean INDICADOR_HABILITADO = Boolean.TRUE;

	/** The Constant PARTE_INTEIRA_FATOR_PTZ_Z. */
	public static final int PARTE_INTEIRA_FATOR_PTZ_Z = 2;

	/** The Constant CLASSE_INTEGRACAO_BEM_MOVIMENTACAO. */
	public static final String CLASSE_INTEGRACAO_BEM_MOVIMENTACAO = "IntegracaoBemMovimentacaoImpl";

	/** The Constant VAZAO_CORRETOR. */
	public static final String VAZAO_CORRETOR = "br.com.ggas.medicao.vazaocorretor.VazaoCorretor";

	/** The Constant MEDIDOR. */
	public static final String MEDIDOR = "br.com.ggas.medicao.medidor.Medidor";

	/** The Constant BEM. */
	public static final String BEM = "bem";

	/** The Constant LOG_ROTULO_EVENTO_COMERCIAL. */
	public static final String LOG_ROTULO_EVENTO_COMERCIAL = "LOG_ROTULO_EVENTO_COMERCIAL";

	/** The Constant LOG_ROTULO_SEGMENTO. */
	public static final String LOG_ROTULO_SEGMENTO = "LOG_ROTULO_SEGMENTO";

	/** The Constant LOG_ROTULO_ITEM_CONTABIL. */
	public static final String LOG_ROTULO_ITEM_CONTABIL = "LOG_ROTULO_ITEM_CONTABIL";

	/** The Constant LOG_ROTULO_TRIBUTO. */
	public static final String LOG_ROTULO_TRIBUTO = "LOG_ROTULO_TRIBUTO";

	/** The Constant LOG_REGISTROS_PROCESSSADOS. */
	public static final String LOG_REGISTROS_PROCESSSADOS = "LOG_REGISTROS_PROCESSSADOS";

	/** The Constant LOG_REGISTROS_INCLUIDOS. */
	public static final String LOG_REGISTROS_INCLUIDOS = "LOG_REGISTROS_INCLUIDOS";

	/** The Constant LOG_REGISTROS_ALTERADOS. */
	public static final String LOG_REGISTROS_ALTERADOS = "LOG_REGISTROS_ALTERADOS";

	/** The Constant LOG_REGISTROS_EXCLUIDOS. */
	public static final String LOG_REGISTROS_EXCLUIDOS = "LOG_REGISTROS_EXCLUIDOS";

	/** The Constant LOG_REGISTROS_INCONSISTENTES. */
	public static final String LOG_REGISTROS_INCONSISTENTES = "LOG_REGISTROS_INCONSISTENTES";

	/** The Constant LOG_INCONSISTENCIAS_REGISTRADAS. */
	public static final String LOG_INCONSISTENCIAS_REGISTRADAS = "LOG_INCONSISTENCIAS_REGISTRADAS";

	/** The Constant LOG_TOTAL_INCONSISTENCIAS. */
	public static final String LOG_TOTAL_INCONSISTENCIAS = "LOG_TOTAL_INCONSISTENCIAS";

	/** The Constant LOG_LISTA_EVENTOS_INCONSISTENCIAS. */
	public static final String LOG_LISTA_EVENTOS_INCONSISTENCIAS = "LOG_LISTA_EVENTOS_INCONSISTENCIAS";

	/** The Constant USUARIO_INVALIDO. */
	public static final String USUARIO_INVALIDO = "A(s) entidade(s) usuário não está(ão) cadastradas.";

	/** The Constant SITUACAO_MEDIDOR. */
	public static final String SITUACAO_MEDIDOR = "SituacaoMedidorImpl";

	/** The Constant OPERACAO_MEDIDOR. */
	public static final String OPERACAO_MEDIDOR = "OperacaoMedidorImpl";

	/** The Constant USUARIO. */
	public static final String USUARIO = "UsuarioImpl";

	/** The Constant CLASSE_SITUACAO_MEDIDOR. */
	public static final String CLASSE_SITUACAO_MEDIDOR = "br.com.ggas.medicao.medidor.SituacaoMedidor";

	/** The Constant CLASSE_OPERACAO_MEDIDOR. */
	public static final String CLASSE_OPERACAO_MEDIDOR = "br.com.ggas.medicao.medidor.OperacaoMedidor";

	/** The Constant CLASSE_USUARIO. */
	public static final String CLASSE_USUARIO = "br.com.ggas.controleacesso.Usuario";

	/** The Constant ANOFABRICACAO. */
	public static final String ANOFABRICACAO = "Ano de Fabricação";

	/** The Constant SITUACAOMEDIDOR. */
	public static final String SITUACAOMEDIDOR = "Situação do Medidor";

	/** The Constant LOCALARMAZENAGEM. */
	public static final String LOCALARMAZENAGEM = "Local de Armazenagem";

	/** The Constant PRESSAOMAXIMA. */
	public static final String PRESSAOMAXIMA = "Pressão Máxima de Trabalho";

	/** The Constant UNIDADEPRESSAOMAXIMA. */
	public static final String UNIDADEPRESSAOMAXIMA = "Unidade de Pressão Máxima de Trabalho";

	/** The Constant NUMERODIGITOS. */
	public static final String NUMERODIGITOS = "Número de Digitos";

	/** The Constant OPERACAOINVALIDA. */
	public static final String OPERACAOINVALIDA = "Operação inválida";

	/** The Constant TABELA_MEDIDOR_OPERACAO. */
	public static final String TABELA_MEDIDOR_OPERACAO = "MEDIDOR_OPERACAO";

	/** The Constant TABELA_MEDIDOR_SITUACAO. */
	public static final String TABELA_MEDIDOR_SITUACAO = "MEDIDOR_SITUACAO";

	/** The Constant PAPEL. */
	public static final String PAPEL = "br.com.ggas.controleacesso.impl.PapelImpl";

	/** The Constant OPERACAO. */
	public static final String OPERACAO = "br.com.ggas.controleacesso.impl.OperacaoImpl";

	/** The Constant TABELA_MEDIDOR_LOCAL_ARMAZENAGEM. */
	public static final String TABELA_MEDIDOR_LOCAL_ARMAZENAGEM = "MEDIDOR_LOCAL_ARMAZENAGEM";

	/**
	 * Bloco de constantes referente as mensagens do sistema Neste bloco constarão
	 * as constantes que identificam as mensagens de erro e sucesso do sistema.
	 */
	public static final String ERRO_EMAIL_NAO_ENVIADO = "ERRO_EMAIL_NAO_ENVIADO";

	/** The Constant ERRO_ENVIO_ARQUIVO_EMAIL. */
	public static final String ERRO_ENVIO_ARQUIVO_EMAIL = "ERRO_ENVIO_ARQUIVO_EMAIL";

	/** The Constant ERRO_ARQUIVO_NAO_SELECIONADO. */
	public static final String ERRO_ARQUIVO_NAO_SELECIONADO = "ERRO_ARQUIVO_NAO_SELECIONADO";

	/** The Constant ERRO_ARQUIVOS_EXTENSAO_INVALIDA. */
	public static final String ERRO_ARQUIVOS_EXTENSAO_INVALIDA = "ERRO_ARQUIVOS_EXTENSAO_INVALIDA";

	/** The Constant ERRO_DUPLA_SUBMISSAO. */
	public static final String ERRO_DUPLA_SUBMISSAO = "ERRO_DUPLA_SUBMISSAO";

	/** The Constant ERRO_ENTIDADE_VERSAO_DESATUALIZADA. */
	public static final String ERRO_ENTIDADE_VERSAO_DESATUALIZADA = "ERRO_ENTIDADE_VERSAO_DESATUALIZADA";

	/** The Constant ERRO_ENTIDADE_NAO_ENCONTRADA. */
	public static final String ERRO_ENTIDADE_NAO_ENCONTRADA = "ERRO_ENTIDADE_NAO_ENCONTRADA";

	/** The Constant ERRO_ENTIDADE_ADICIONADA. */
	public static final String ERRO_ENTIDADE_ADICIONADA = "ERRO_ENTIDADE_ADICIONADA";

	/** The Constant ERRO_MENOR_QUE_MINIMO_SELECIONAR_LISTA_ORDENADA. */
	public static final String ERRO_MENOR_QUE_MINIMO_SELECIONAR_LISTA_ORDENADA = "ERRO_MENOR_QUE_MINIMO_SELECIONAR_LISTA_ORDENADA";

	/** The Constant ERRO_MAIOR_QUE_MAXIMO_SELECIONAR_LISTA_ORDENADA. */
	public static final String ERRO_MAIOR_QUE_MAXIMO_SELECIONAR_LISTA_ORDENADA = "ERRO_MAIOR_QUE_MAXIMO_SELECIONAR_LISTA_ORDENADA";

	/** The Constant ERRO_MAXIMO_SELECIONAR_LISTA_ORDENADA. */
	public static final String ERRO_MAXIMO_SELECIONAR_LISTA_ORDENADA = "ERRO_MAXIMO_SELECIONAR_LISTA_ORDENADA";

	/** The Constant ERRO_NAO_EXISTEM_DADOS_GERACAO_RELATORIO. */
	public static final String ERRO_NAO_EXISTEM_DADOS_GERACAO_RELATORIO = "ERRO_NAO_EXISTEM_DADOS_GERACAO_RELATORIO";

	/** The Constant ERRO_NAO_EXISTEM_DADOS_FILTRO_INFORMADO. */
	public static final String ERRO_NAO_EXISTEM_DADOS_FILTRO_INFORMADO = "ERRO_NAO_EXISTEM_DADOS_FILTRO_INFORMADO";

	/** The Constant ERRO_SEM_ENTIDADE_ADICIONADA. */
	public static final String ERRO_SEM_ENTIDADE_ADICIONADA = "ERRO_SEM_ENTIDADE_ADICIONADA";

	/** The Constant ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO. */
	public static final String ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO = "ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO";

	/**The Constant ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO. */
	public static final String ERRO_FALHA_NA_GERACAO_DO_DANFE = "ERRO_FALHA_NA_GERACAO_DO_DANFE";

	/** The Constant ERRO_NEGOCIO_RELATORIO_SEM_DADOS. */
	public static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	/** The Constant ERRO_NEGOCIO_RELATORIO_CDL_SEM_LOGO. */
	public static final String ERRO_NEGOCIO_RELATORIO_CDL_SEM_LOGO = "ERRO_NEGOCIO_RELATORIO_CDL_SEM_LOGO";

	/** The Constant ERRO_NEGOCIO_RELATORIO_BANCO_SEM_LOGO. */
	public static final String ERRO_NEGOCIO_RELATORIO_BANCO_SEM_LOGO = "ERRO_NEGOCIO_RELATORIO_BANCO_SEM_LOGO";

	/** The Constant ERRO_NEGOCIO_ENTIDADE_CADASTRADA. */
	public static final String ERRO_NEGOCIO_ENTIDADE_CADASTRADA = "ERRO_NEGOCIO_ENTIDADE_CADASTRADA";

	/** The Constant ERRO_DADOS_INVALIDOS. */
	public static final String ERRO_DADOS_INVALIDOS = "ERRO_DADOS_INVALIDOS";

	/** The Constant ERRO_DADOS_FALTANTES. */
	public static final String ERRO_DADOS_FALTANTES = "ERRO_DADOS_FALTANTES";

	/** The Constant ERRO_NAO_PARTICIPA_ECARTAS. */
	public static final String ERRO_NAO_PARTICIPA_ECARTAS = "ERRO_NAO_PARTICIPA_ECARTAS";

	/** The Constant ERRO_CONTRATO_NAO_PARTICIPA_ECARTAS. */
	public static final String ERRO_CONTRATO_NAO_PARTICIPA_ECARTAS = "ERRO_CONTRATO_NAO_PARTICIPA_ECARTAS";

	/** The Constant ERRO_NENHUMA_FATURA_ENCONTRADA_ECARTAS. */
	public static final String ERRO_NENHUMA_FATURA_ENCONTRADA_ECARTAS = "ERRO_NENHUMA_FATURA_ENCONTRADA_ECARTAS";

	/** Constante ANO_MES_FATURAMENTO. */
	public static final String ANO_MES_FATURAMENTO = "ANO_MES_FATURAMENTO";

	/** The Constant ERRO_NEGOCIO_DADOS_INVALIDOS. */
	public static final String ERRO_NEGOCIO_DADOS_INVALIDOS = "ERRO_NEGOCIO_DADOS_INVALIDOS";

	/** The Constant ERRO_INTEGRIDADE_RELACIONAL. */
	public static final String ERRO_INTEGRIDADE_RELACIONAL = "ERRO_INTEGRIDADE_RELACIONAL";

	/** The Constant ERRO_TAMANHO_LIMITE. */
	public static final String ERRO_TAMANHO_LIMITE = "ERRO_TAMANHO_LIMITE";

	/** The Constant ERRO_FORMATO_INVALIDO. */
	public static final String ERRO_FORMATO_INVALIDO = "ERRO_FORMATO_INVALIDO";

	/** The Constant ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS. */
	public static final String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS";

	/** The Constant ERRO_ADICIONAR_AO_ZIP_ECARTAS. */
	public static final String ERRO_ADICIONAR_AO_ZIP_ECARTAS = "ERRO_ADICIONAR_AO_ZIP_ECARTAS";

	/** The Constant ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS. */
	public static final String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_ABA = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_ABA";

	/** The Constant ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS. */
	public static final String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS";

	/** The Constant ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA. */
	public static final String ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA = "ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA";

	/** The Constant ERRO_NEGOCIO_ENTIDADE_INVALIDA. */
	public static final String ERRO_NEGOCIO_ENTIDADE_INVALIDA = "ERRO_NEGOCIO_ENTIDADE_INVALIDA";

	/** The Constant ERRO_ACESSO_NEGADO. */
	public static final String ERRO_ACESSO_NEGADO = "ERRO_ACESSO_NEGADO";

	/** The Constant ERRO_HOST_DESCONHECIDO. */
	public static final String ERRO_HOST_DESCONHECIDO = "ERRO_HOST_DESCONHECIDO";

	/** The Constant SUCESSO_ENTIDADE_ATUALIZADA. */
	public static final String SUCESSO_ENTIDADE_ATUALIZADA = "SUCESSO_ENTIDADE_ATUALIZADA";

	/** The Constant SUCESSO_ENTIDADE_INCLUIDA. */
	public static final String SUCESSO_ENTIDADE_INCLUIDA = "SUCESSO_ENTIDADE_INCLUIDA";

	/** The Constant SUCESSO_ENTIDADE. */
	public static final String SUCESSO_ENTIDADE = "SUCESSO_ENTIDADE";

	/** The Constant SUCESSO_ENTIDADE_INCLUIDA_FEMININO. */
	public static final String SUCESSO_ENTIDADE_INCLUIDA_FEMININO = "SUCESSO_ENTIDADE_INCLUIDA_FEMININO";

	/** The Constant SUCESSO_ENTIDADE_TRANSFERIDA. */
	public static final String SUCESSO_ENTIDADE_TRANSFERIDA = "SUCESSO_ENTIDADE_TRANSFERIDA";

	/** The Constant SUCESSO_ENTIDADE_CONFIGURADA. */
	public static final String SUCESSO_ENTIDADE_CONFIGURADA = "SUCESSO_ENTIDADE_CONFIGURADA";

	/** The Constant SUCESSO_ENTIDADE_PRONTA_PARA_TRANSFERENCIA. */
	public static final String SUCESSO_ENTIDADE_PRONTA_PARA_TRANSFERENCIA = "SUCESSO_ENTIDADE_PRONTA_PARA_TRANSFERENCIA";

	/** The Constant SUCESSO_ENTIDADE_ALTERADA. */
	public static final String SUCESSO_ENTIDADE_ALTERADA = "SUCESSO_ENTIDADE_ALTERADA";

	/** The Constant SUCESSO_ENTIDADE_ALTERADA_FEMININO. */
	public static final String SUCESSO_ENTIDADE_ALTERADA_FEMININO = "SUCESSO_ENTIDADE_ALTERADA_FEMININO";

	/** The Constant SUCESSO_ENTIDADES_ALTERADAS. */
	public static final String SUCESSO_ENTIDADES_ALTERADAS = "SUCESSO_ENTIDADES_ALTERADAS";

	/** The Constant SUCESSO_SENHA_ENVIADA. */
	public static final String SUCESSO_SENHA_ENVIADA = "SUCESSO_SENHA_ENVIADA";

	/** The Constant SUCESSO_SENHA_ALTERADA. */
	public static final String SUCESSO_SENHA_ALTERADA = "SUCESSO_SENHA_ALTERADA";

	/** The Constant ERRO_NEGOCIO_LOGIN_INVALIDO. */
	public static final String ERRO_NEGOCIO_LOGIN_INVALIDO = "ERRO_NEGOCIO_LOGIN_INVALIDO";

	/** The Constant ERRO_NEGOCIO_SENHA_ATUAL_NOVA_IGUAIS. */
	public static final String ERRO_NEGOCIO_SENHA_ATUAL_NOVA_IGUAIS = "ERRO_NEGOCIO_SENHA_ATUAL_NOVA_IGUAIS";

	/** The Constant ERRO_NEGOCIO_SENHA_ATUAL_INVALIDA. */
	public static final String ERRO_NEGOCIO_SENHA_ATUAL_INVALIDA = "ERRO_NEGOCIO_SENHA_ATUAL_INVALIDA";

	/** The Constant ERRO_NEGOCIO_SENHA_NAO_CONTEM_MAIUSCULAS. */
	public static final String ERRO_NEGOCIO_SENHA_NAO_CONTEM_MAIUSCULAS = "ERRO_NEGOCIO_SENHA_NAO_CONTEM_MAIUSCULAS";

	/** The Constant ERRO_NEGOCIO_SENHA_NAO_CONTEM_MINUSCULAS. */
	public static final String ERRO_NEGOCIO_SENHA_NAO_CONTEM_MINUSCULAS = "ERRO_NEGOCIO_SENHA_NAO_CONTEM_MINUSCULAS";

	/** The Constant ERRO_NEGOCIO_SENHA_NAO_CONTEM_NUMERO. */
	public static final String ERRO_NEGOCIO_SENHA_NAO_CONTEM_NUMERO = "ERRO_NEGOCIO_SENHA_NAO_CONTEM_NUMERO";

	/** The Constant ERRO_NEGOCIO_SENHA_TAMANHO. */
	public static final String ERRO_NEGOCIO_SENHA_TAMANHO = "ERRO_NEGOCIO_SENHA_TAMANHO";

	/** The Constant ERRO_NEGOCIO_SENHA_JA_UTILIZADA. */
	public static final String ERRO_NEGOCIO_SENHA_JA_UTILIZADA = "ERRO_NEGOCIO_SENHA_JA_UTILIZADA";

	/** The Constant ERRO_NEGOCIO_SENHA_NOVA_CONFIRMACAO_DIFERENTES. */
	public static final String ERRO_NEGOCIO_SENHA_NOVA_CONFIRMACAO_DIFERENTES = "ERRO_NEGOCIO_SENHA_NOVA_CONFIRMACAO_DIFERENTES";

	/** The Constant SUCESSO_ENTIDADE_EXCLUIDA. */
	public static final String SUCESSO_ENTIDADE_EXCLUIDA = "SUCESSO_ENTIDADE_EXCLUIDA";

	/** The Constant SUCESSO_ENTIDADE_EXCLUIDO. */
	public static final String SUCESSO_ENTIDADE_EXCLUIDO = "SUCESSO_ENTIDADE_EXCLUIDO";

	/** The Constant ERRO_NEGOCIO_SELECAO_DE_CHAVES. */
	public static final String ERRO_NEGOCIO_SELECAO_DE_CHAVES = "ERRO_NEGOCIO_SELECAO_DE_CHAVES";

	/** The Constant ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES. */
	public static final String ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES = "ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES";

	/** The Constant ERRO_NEGOCIO_VALOR_ZERADO. */
	public static final String ERRO_NEGOCIO_VALOR_ZERADO = "ERRO_NEGOCIO_VALOR_ZERADO";

	/** The Constant ERRO_PARAMETRO_NEGOCIO. */
	public static final String ERRO_PARAMETRO_NEGOCIO = "ERRO_PARAMETRO_NEGOCIO";

	/** The Constant SUCESSO_IMPORTACAO. */
	public static final String SUCESSO_IMPORTACAO = "SUCESSO_IMPORTACAO";

	/** The Constant SUCESSO_IMPORTACAO_TODOS_REJEITADOS. */
	public static final String SUCESSO_IMPORTACAO_TODOS_REJEITADOS = "SUCESSO_IMPORTACAO_TODOS_REJEITADOS";

	/** The Constant FALHA_FORMATO_IMPORTACAO. */
	public static final String FALHA_FORMATO_IMPORTACAO = "FALHA_FORMATO_IMPORTACAO";

	/** The Constant ERRO_CEP_NAO_ENCONTRADO. */
	public static final String ERRO_CEP_NAO_ENCONTRADO = "ERRO_CEP_NAO_ENCONTRADO";

	/** The Constant ERRO_NEGOCIO_VALOR_ZERO. */
	public static final String ERRO_NEGOCIO_VALOR_ZERO = "ERRO_NEGOCIO_VALOR_ZERO";

	/** The Constant ERRO_NEGOCIO_CEP_INEXISTENTE. */
	public static final String ERRO_NEGOCIO_CEP_INEXISTENTE = "ERRO_NEGOCIO_CEP_INEXISTENTE";

	/** The Constant SUCESSO_IMOVEIS_EM_LOTE. */
	public static final String SUCESSO_IMOVEIS_EM_LOTE = "SUCESSO_IMOVEIS_EM_LOTE";

	/** The Constant SUCESSO_ENTIDADE_ESTORNADO. */
	public static final String SUCESSO_ENTIDADE_ESTORNADO = "SUCESSO_ENTIDADE_ESTORNADO";

	/** The Constant ERRO_NEGOCIO_CLIENTE_CNPJ_DUPLICADO. */
	public static final String ERRO_NEGOCIO_CLIENTE_CNPJ_DUPLICADO = "ERRO_NEGOCIO_CLIENTE_CNPJ_DUPLICADO";

	/** The Constant ERRO_NEGOCIO_CLIENTE_CPF_DUPLICADO. */
	public static final String ERRO_NEGOCIO_CLIENTE_CPF_DUPLICADO = "ERRO_NEGOCIO_CLIENTE_CPF_DUPLICADO";

	/** The Constant FALHA_FORMATO_NUMERO_SERIE. */
	public static final String FALHA_FORMATO_NUMERO_SERIE = "FALHA_FORMATO_NUMERO_SERIE";

	/** The Constant ERRO_NEGOCIO_MODELO_CONTRATO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_MODELO_CONTRATO_EXISTENTE = "ERRO_NEGOCIO_MODELO_CONTRATO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CONTRATO_VINCULADO. */
	public static final String ERRO_NEGOCIO_CONTRATO_VINCULADO = "ERRO_NEGOCIO_CONTRATO_VINCULADO";

	/** The Constant ERRO_NEGOCIO_MODELO_CONTRATO_VINCULADO. */
	public static final String ERRO_NEGOCIO_MODELO_CONTRATO_VINCULADO = "ERRO_NEGOCIO_MODELO_CONTRATO_VINCULADO";
	
	/** The Constant CONTRATO_ALTERACAO_CONTRATO_AGRUPADO_POR_VALOR_PARA_POR_VOLUME. */
	public static final String CONTRATO_ALTERACAO_CONTRATO_AGRUPADO_POR_VALOR_PARA_POR_VOLUME = "CONTRATO_ALTERACAO_CONTRATO_AGRUPADO_POR_VALOR_PARA_POR_VOLUME";

	/** The Constant CONTRATO_INCLUIDO_SUCESSO. */
	public static final String CONTRATO_INCLUIDO_SUCESSO = "CONTRATO_INCLUIDO_SUCESSO";

	/** The Constant CONTRATO_ADITADO_SUCESSO. */
	public static final String CONTRATO_ADITADO_SUCESSO = "CONTRATO_ADITADO_SUCESSO";

	/** The Constant CONTRATO_ALTERADO_SUCESSO. */
	public static final String CONTRATO_ALTERADO_SUCESSO = "CONTRATO_ALTERADO_SUCESSO";

	/**
	 * The Constant ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO_PLANILHA.
	 */
	public static final String ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO_PLANILHA = "ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_"
			+ "INVESTIMENTO_PLANILHA";

	/** The Constant ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL. */
	public static final String ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL = "ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_"
			+ "PERCENTUAL";

	/** The Constant ERRO_NEGOCIO_ESTOURO_VALOR_CAMPO_TAMANHO_MAXIMO. */
	public static final String ERRO_NEGOCIO_ESTOURO_VALOR_CAMPO_TAMANHO_MAXIMO = "ERRO_NEGOCIO_ESTOURO_VALOR_CAMPO_TAMANHO_MAXIMO";

	/** The Constant ERRO_NEGOCIO_CONTRATO_DATA_INICIO_VIGENCIA_TOP. */
	public static final String ERRO_NEGOCIO_CONTRATO_DATA_INICIO_VIGENCIA_TOP = "ERRO_NEGOCIO_CONTRATO_DATA_INICIO_VIGENCIA_TOP";

	/** The Constant ERRO_NEGOCIO_CONTRATO_DATA_FIM_VIGENCIA_TOP. */
	public static final String ERRO_NEGOCIO_CONTRATO_DATA_FIM_VIGENCIA_TOP = "ERRO_NEGOCIO_CONTRATO_DATA_FIM_VIGENCIA_TOP";

	/** The Constant SUCESSO_EFETUAR_PARCELAMENTO. */
	public static final String SUCESSO_EFETUAR_PARCELAMENTO = "SUCESSO_EFETUAR_PARCELAMENTO";

	/** The Constant ERRO_NEGOCIO_FATURA_NAO_SELECIONADA_RECEBIMENTO. */
	public static final String ERRO_NEGOCIO_FATURA_NAO_SELECIONADA_RECEBIMENTO = "ERRO_NEGOCIO_FATURA_NAO_SELECIONADA_RECEBIMENTO";

	/** The Constant SUCESSO_CREDITO_DEBITO_CANCELADO. */
	public static final String SUCESSO_CREDITO_DEBITO_CANCELADO = "SUCESSO_CREDITO_DEBITO_CANCELADO";

	/** The Constant ERRO_VALIDACAO_DADOS_AUDITORIA. */
	public static final String ERRO_VALIDACAO_DADOS_AUDITORIA = "ERRO_VALIDACAO_DADOS_AUDITORIA";

	/** The Constant SUCESSO_ENTIDADE_CANCELADA. */
	public static final String SUCESSO_ENTIDADE_CANCELADA = "SUCESSO_ENTIDADE_CANCELADA";

	/** The Constant SUCESSO_NOTAS_CONCILIADA. */
	public static final String SUCESSO_NOTAS_CONCILIADA = "SUCESSO_NOTAS_CONCILIADA";

	/** The Constant ERRO_PERIODICIDADE_PARCELAMENTO_INVALIDA. */
	public static final String ERRO_PERIODICIDADE_PARCELAMENTO_INVALIDA = "ERRO_PERIODICIDADE_PARCELAMENTO_INVALIDA";

	/** The Constant OPERACAO_CONCLUIDA_COM_SUCESSO. */
	public static final String OPERACAO_CONCLUIDA_COM_SUCESSO = "OPERACAO_CONCLUIDA_COM_SUCESSO";

	/** The Constant ERRO_NEGOCIO_DATA_INICIAL_OBRIGATORIO. */
	public static final String ERRO_NEGOCIO_DATA_INICIAL_OBRIGATORIO = "ERRO_NEGOCIO_DATA_INICIAL_OBRIGATORIO";

	/** The Constant ERRO_NEGOCIO_DATA_INICIAL_DATA_VALIDA. */
	public static final String ERRO_NEGOCIO_DATA_INICIAL_DATA_VALIDA = "ERRO_NEGOCIO_DATA_INICIAL_DATA_VALIDA";

	/** The Constant ERRO_NEGOCIO_DATA_FINAL_OBRIGATORIO. */
	public static final String ERRO_NEGOCIO_DATA_FINAL_OBRIGATORIO = "ERRO_NEGOCIO_DATA_FINAL_OBRIGATORIO";

	/** The Constant ERRO_NEGOCIO_DATA_FINAL_DATA_VALIDA. */
	public static final String ERRO_NEGOCIO_DATA_FINAL_DATA_VALIDA = "ERRO_NEGOCIO_DATA_FINAL_DATA_VALIDA";

	/** The Constant ERRO_DATA_INICIAL_MAIOR_DATA_FINAL. */
	public static final String ERRO_DATA_INICIAL_MAIOR_DATA_FINAL = "ERRO_DATA_INICIAL_MAIOR_DATA_FINAL";

	/** The Constant ERRO_SOBREPOSICAO_VIGENCIA_SUBS_TRIBUT_RAMO_ATIVIDADE. */
	public static final String ERRO_SOBREPOSICAO_VIGENCIA_SUBS_TRIBUT_RAMO_ATIVIDADE = "ERRO_SOBREPOSICAO_VIGENCIA_SUBS_TRIBUT_RAMO_ATIVIDADE";

	/** The Constant ERRO_DATA_EMISSAO_INICIAL_MAIOR_DATA_FINAL. */
	public static final String ERRO_DATA_EMISSAO_INICIAL_MAIOR_DATA_FINAL = "ERRO_DATA_EMISSAO_INICIAL_MAIOR_DATA_FINAL";

	/** The Constant ERRO_DATA_VENCIMENTO_INICIAL_MAIOR_DATA_FINAL. */
	public static final String ERRO_DATA_VENCIMENTO_INICIAL_MAIOR_DATA_FINAL = "ERRO_DATA_VENCIMENTO_INICIAL_MAIOR_DATA_FINAL";

	/** The Constant ERRO_DATA_EMISSAO_INICIAL_POSTERIOR_ATUAL. */
	public static final String ERRO_DATA_EMISSAO_INICIAL_POSTERIOR_ATUAL = "ERRO_DATA_EMISSAO_INICIAL_POSTERIOR_ATUAL";

	/** The Constant ERRO_INTERVALO_VALORES_INVALIDO. */
	public static final String ERRO_INTERVALO_VALORES_INVALIDO = "ERRO_INTERVALO_VALORES_INVALIDO";

	/** The Constant NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA. */
	public static final String NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA = "NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA";

	/** The Constant ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_FATURAMENTO. */
	public static final String ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_FATURAMENTO = "ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_"
			+ "FATURAMENTO";

	/** The Constant SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_CORRETOR_VAZAO. */
	public static final String SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_CORRETOR_VAZAO = "SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_"
			+ "CORRETOR_VAZAO";

	/** The Constant ERRO_NEGOCIO_ATIVIDADE_PREDECESSORA_NAO_REALIZADA. */
	public static final String ERRO_NEGOCIO_ATIVIDADE_PREDECESSORA_NAO_REALIZADA = "ERRO_NEGOCIO_ATIVIDADE_PREDECESSORA_NAO_REALIZADA";

	/** The Constant ERRO_NEGOCIO_TABELA_VAZIA. */
	public static final String ERRO_NEGOCIO_TABELA_VAZIA = "ERRO_NEGOCIO_TABELA_VAZIA";

	/** The Constant ERRO_NEGOCIO_VALIDACAO_CICLO. */
	public static final String ERRO_NEGOCIO_VALIDACAO_CICLO = "ERRO_NEGOCIO_VALIDACAO_CICLO";

	/** The Constant ERRO_NEGOCIO_PAPEL_NAO_SELECIONADO. */
	public static final String ERRO_NEGOCIO_PAPEL_NAO_SELECIONADO = "ERRO_NEGOCIO_PAPEL_NAO_SELECIONADO";

	/** The Constant ERRO_NEGOCIO_ALCADA_JA_EXISTENTE. */
	public static final String ERRO_NEGOCIO_ALCADA_JA_EXISTENTE = "ERRO_NEGOCIO_ALCADA_JA_EXISTENTE";

	/** The Constant SUCESSO_ANOMALIA_TRATADA. */
	public static final String SUCESSO_ANOMALIA_TRATADA = "SUCESSO_ANOMALIA_TRATADA";

	/** The Constant FALHA_ANOMALIA_TRATADA. */
	public static final String FALHA_ANOMALIA_TRATADA = "FALHA_ANOMALIA_TRATADA";

	/** The Constant MENSAGEM_FATURAMENTO_INICIO_VIGENCIA. */
	public static final String MENSAGEM_FATURAMENTO_INICIO_VIGENCIA = "MENSAGEM_FATURAMENTO_INICIO_VIGENCIA";

	/** The Constant MENSAGEM_FATURAMENTO_FIM_VIGENCIA. */
	public static final String MENSAGEM_FATURAMENTO_FIM_VIGENCIA = "MENSAGEM_FATURAMENTO_FIM_VIGENCIA";

	/** The Constant ERRO_NEGOCIO_ENDERECO_REMOTO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_ENDERECO_REMOTO_EXISTENTE = "ERRO_NEGOCIO_ENDERECO_REMOTO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CAMPO_SIGLA_MAXIMO_3. */
	public static final String ERRO_NEGOCIO_CAMPO_SIGLA_MAXIMO_3 = "ERRO_NEGOCIO_CAMPO_SIGLA_MAXIMO_3";

	/** The Constant ERRO_NEGOCIO_SISTEMA_INTEGRANTE_EXISTENTE. */
	public static final String ERRO_NEGOCIO_SISTEMA_INTEGRANTE_EXISTENTE = "ERRO_NEGOCIO_SISTEMA_INTEGRANTE_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_INTERACOES_ATIVAS. */
	public static final String ERRO_NEGOCIO_INTERACOES_ATIVAS = "ERRO_NEGOCIO_INTERACOES_ATIVAS";

	/** The Constant ERRO_NEGOCIO_INTERACOES_RELACIONADAS. */
	public static final String ERRO_NEGOCIO_INTERACOES_RELACIONADAS = "ERRO_NEGOCIO_INTERACOES_RELACIONADAS";

	/** The Constant ERRO_DATA_INFORMADA_MAIOR_ATUAL. */
	public static final String ERRO_DATA_INFORMADA_MAIOR_ATUAL = "ERRO_DATA_INFORMADA_MAIOR_ATUAL";

	/** The Constant ERRO_NEGOCIO_SERIE_NUMERO_DUPLICADO. */
	public static final String ERRO_NEGOCIO_SERIE_NUMERO_DUPLICADO = "ERRO_NEGOCIO_SERIE_NUMERO_DUPLICADO";

	/** The Constant ERRO_NEGOCIO_SERIE_VALORES_DUPLICADO. */
	public static final String ERRO_NEGOCIO_SERIE_VALORES_DUPLICADO = "ERRO_NEGOCIO_SERIE_VALORES_DUPLICADO";

	/** The Constant ERRO_NEGOCIO_SERIE_EM_USO. */
	public static final String ERRO_NEGOCIO_SERIE_EM_USO = "ERRO_NEGOCIO_SERIE_EM_USO";

	/** The Constant ERRO_NEGOCIO_SERIE_NUMERO_MES_ANO_INVALIDO. */
	public static final String ERRO_NEGOCIO_SERIE_NUMERO_MES_ANO_INVALIDO = "ERRO_NEGOCIO_SERIE_NUMERO_MES_ANO_INVALIDO";

	/** The Constant ERRO_NEGOCIO_CORRETOR_VAZAO_SEM_HISTORICO. */
	public static final String ERRO_NEGOCIO_CORRETOR_VAZAO_SEM_HISTORICO = "ERRO_NEGOCIO_CORRETOR_VAZAO_SEM_HISTORICO";

	/** The Constant ERRO_NEGOCIO_MEDIDOR_NAO_PODE_SER_REMOVIDO. */
	public static final String ERRO_NEGOCIO_MEDIDOR_NAO_PODE_SER_REMOVIDO = "ERRO_NEGOCIO_MEDIDOR_NAO_PODE_SER_REMOVIDO";

	/** The Constant ERRO_NEGOCIO_CORRETOR_NAO_PODE_SER_REMOVIDO. */
	public static final String ERRO_NEGOCIO_CORRETOR_NAO_PODE_SER_REMOVIDO = "ERRO_NEGOCIO_CORRETOR_NAO_PODE_SER_REMOVIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS = "ERRO_NEGOCIO_INTEGRACAO_CAMPOS_OBRIGATORIOS_"
			+ "NAO_PREENCHIDOS";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO = "ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_SITUACAO_MEDIDOR_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_SITUACAO_MEDIDOR_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_SITUACAO_MEDIDOR_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_ENTIDADES_NAO_CADASTRADAS. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_ENTIDADES_NAO_CADASTRADAS = "ERRO_NEGOCIO_INTEGRACAO_ENTIDADES_NAO_CADASTRADAS";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_ANO_FABRICAO_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_ANO_FABRICAO_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_ANO_FABRICAO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_DATA_AQUISICAO_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_DATA_AQUISICAO_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_DATA_AQUISICAO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_DATA_MAXIMA_INSTALACAO_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_DATA_MAXIMA_INSTALACAO_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_DATA_"
			+ "MAXIMA_INSTALACAO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_DATA_CALIBRACAO_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_DATA_CALIBRACAO_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_DATA_CALIBRACAO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_NUMERO_DIGITOS_MEDIDOR_INVALIDO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_NUMERO_DIGITOS_MEDIDOR_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_NUMERO_"
			+ "DIGITOS_MEDIDOR_INVALIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO = "ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_SITUACAO_CORRETOR_INVALIDA. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_SITUACAO_CORRETOR_INVALIDA = "ERRO_NEGOCIO_INTEGRACAO_SITUACAO_CORRETOR_INVALIDA";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_TIPO_MOSTRADOR_CORRETOR_INVALIDO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_TIPO_MOSTRADOR_CORRETOR_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_TIPO_MOSTRADOR_CORRETOR_"
			+ "INVALIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CORRECAO_PRESSAO_INVALIDO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CORRECAO_PRESSAO_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CORRECAO_"
			+ "PRESSAO_INVALIDO";

	/**
	 * The Constant ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CORRECAO_TEMPERATURA_INVALIDO.
	 */
	public static final String ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CORRECAO_TEMPERATURA_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_INDICADOR_"
			+ "CORRECAO_TEMPERATURA_INVALIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CONTROLE_VAZAO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CONTROLE_VAZAO = "ERRO_NEGOCIO_INTEGRACAO_INDICADOR_CONTROLE_VAZAO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_INDICADOR_LINEARIZACAO_FATORK. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_INDICADOR_LINEARIZACAO_FATORK = "ERRO_NEGOCIO_INTEGRACAO_INDICADOR_LINEARIZACAO_"
			+ "FATORK";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_NUMERO_DIGITOS_CORRETOR_INVALIDO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_NUMERO_DIGITOS_CORRETOR_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_NUMERO_DIGITOS_CORRETOR_"
			+ "INVALIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO = "ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_"
			+ "CADASTRADO";

	/** The Constant ERRO_INTEGRACAO_INEXISTENTE. */
	public static final String ERRO_INTEGRACAO_INEXISTENTE = "ERRO_INTEGRACAO_INEXISTENTE";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO = "ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_"
			+ "CADASTRADO";

	/** The Constant ERRO_NEGOCIO_OPERACAO_INVALIDA. */
	public static final String ERRO_NEGOCIO_OPERACAO_INVALIDA = "ERRO_NEGOCIO_OPERACAO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_BUSCAR_CONSTANTE. */
	public static final String ERRO_NEGOCIO_BUSCAR_CONSTANTE = "ERRO_NEGOCIO_BUSCAR_CONSTANTE";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_EXPORTAR_DADOS. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_EXPORTAR_DADOS = "ERRO_NEGOCIO_INTEGRACAO_EXPORTAR_DADOS";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_EXISTENTE. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_EXISTENTE = "ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_INFORMAR_EMAIL. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_INFORMAR_EMAIL = "ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_INFORMAR_EMAIL";

	/** The Constant QUANTIDADE_MINIMA_INFORMADA_NAO_PODE_SER_MENOR_QUE_2. */
	public static final String QUANTIDADE_MINIMA_INFORMADA_NAO_PODE_SER_MENOR_QUE_2 = "QUANTIDADE_MINIMA_INFORMADA_NAO_PODE_"
			+ "SER_MENOR_QUE_2";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO = "ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_"
			+ "PARAMETRO_INVALIDO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_DADOS_NAO_INFORMADO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_DADOS_NAO_INFORMADO = "ERRO_NEGOCIO_INTEGRACAO_FUNCAO_"
			+ "SISTEMA_DADOS_NAO_INFORMADO";

	/** The Constant ERRO_NEGOCIO_BEM_NAO_CADASTRADO. */
	public static final String ERRO_NEGOCIO_BEM_NAO_CADASTRADO = "ERRO_NEGOCIO_BEM_NAO_CADASTRADO";

	/** The Constant ERRO_FORMATO_INVALIDO_CAMPOS. */
	public static final String ERRO_FORMATO_INVALIDO_CAMPOS = "ERRO_FORMATO_INVALIDO_CAMPOS";

	/** The Constant ERRO_NEGOCIO_JA_EXISTE_REGISTRO. */
	public static final String ERRO_NEGOCIO_JA_EXISTE_REGISTRO = "ERRO_NEGOCIO_JA_EXISTE_REGISTRO";

	/** The Constant ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXISTENTE = "ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_EVENTO_COMERCIAL_NAO_ENCONTRADO. */
	public static final String ERRO_NEGOCIO_EVENTO_COMERCIAL_NAO_ENCONTRADO = "ERRO_NEGOCIO_EVENTO_COMERCIAL_NAO_ENCONTRADO";

	/** The Constant ERRO_NEGOCIO_CONTA_CONTABIL_NOME_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CONTA_CONTABIL_NOME_EXISTENTE = "ERRO_NEGOCIO_CONTA_CONTABIL_NOME_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CONTA_CONTABIL_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CONTA_CONTABIL_EXISTENTE = "ERRO_NEGOCIO_CONTA_CONTABIL_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_CADASTRADA. */
	public static final String ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_CADASTRADA = "ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_CADASTRADA";

	/** The Constant PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_MOVER_ARQUIVO. */
	public static final String PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_MOVER_ARQUIVO = "ERRO_SISTEMA_IMPOSSIVEL_MOVER_ARQUIVO";

	/** The Constant PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO. */
	public static final String PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO = "ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO";

	/** The Constant PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO_SIMULACAO. */
	public static final String PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO_SIMULACAO = "ERRO_SISTEMA_IMPOSSIVEL_"
			+ "CRIAR_DIRETORIO_SIMULACAO";

	/** The Constant PARAMETRO_UTILIZAR_MULTIPLOS_CICLOS. */
	public static final String PARAMETRO_UTILIZAR_MULTIPLOS_CICLOS = "UTILIZAR_MULTIPLOS_CICLOS";
	
	/** The Constant PARAMETRO_CRIAR_CRONOGRAMAS_ENCERRAR. */
	public static final String PARAMETRO_CRIAR_CRONOGRAMAS_ENCERRAR = "CRIAR_CRONOGRAMAS_ENCERRAR";

	/** The Constant ERRO_SOMA_DOS_COMPONENTES. */
	public static final String ERRO_SOMA_DOS_COMPONENTES = "ERRO_SOMA_DOS_COMPONENTES";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_NUMERO_CONTRATO. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_NUMERO_CONTRATO = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_NUMERO_CONTRATO";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CLIENTE. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CLIENTE = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CLIENTE";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_PRAZO_ESTABELECIDO. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PRAZO_ESTABELECIDO = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_PRAZO_ESTABELECIDO";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA";

	/** The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO. */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO =
			"ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO";

	/**
	 * The Constant ERRO_CONTRATO_ADEQUACAO_PARCERIA_SELECIONE_UM_PONTO_DE_CONSUMO.
	 */
	public static final String ERRO_CONTRATO_ADEQUACAO_PARCERIA_SELECIONE_UM_PONTO_DE_CONSUMO =
			"ERRO_CONTRATO_ADEQUACAO_PARCERIA_SELECIONE_UM_PONTO_DE_CONSUMO";

	/** The Constant ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_INCLUSIVA. */
	public static final String ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_INCLUSIVA = "ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_INCLUSIVA";

	/** The Constant ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_INCLUSIVA. */
	public static final String ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_INCLUSIVA = "ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_INCLUSIVA";

	/** The Constant ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_EXCLUSIVA. */
	public static final String ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_EXCLUSIVA = "ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_EXCLUSIVA";

	/** The Constant ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_EXCLUSIVA. */
	public static final String ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_EXCLUSIVA = "ERRO_NEGOCIO_DATA_POSTERIOR_HOJE_MSG_EXCLUSIVA";

	/**
	 * Bloco de constantes referente a Parametro do Sistema Neste bloco constarão as
	 * constantes que identificam um Parametro do sistema.
	 */

	public static final String PARAMETRO_EMAIL_SOLICITACAO_AGENCIA_VIRTUAL = "EMAIL_SOLICITACAO_AGENCIA_VIRTUAL";

	/** The Constant PARAMETRO_ASSINATURA_EMAIL. */
	public static final String PARAMETRO_ASSINATURA_EMAIL = "P_ASSINATURA_EMAIL";

	/** The Constant PARAMETRO_CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA. */
	public static final String PARAMETRO_CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA = "CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA";

	/** The Constant PARAMETRO_VENCIMENTO_DIA_NAO_UTIL. */
	public static final String PARAMETRO_VENCIMENTO_DIA_NAO_UTIL = "VENCIMENTO_DIA_NAO_UTIL";

	/** The Constant PARAMETRO_CODIGO_PADRAO_COMPOSICAO_CONSUMO. */
	public static final String PARAMETRO_CODIGO_PADRAO_COMPOSICAO_CONSUMO = "CODIGO_PADRAO_COMPOSICAO_CONSUMO";

	/** The Constant PARAMETRO_SEQUENCIA_CONTRATO. */
	public static final String PARAMETRO_SEQUENCIA_CONTRATO = "SEQUENCIA_CONTRATO";

	/** The Constant PARAMETRO_ANO_CONTRATO. */
	public static final String PARAMETRO_ANO_CONTRATO = "ANO_CONTRATO";

	/** The Constant PARAMETRO_REINICIO_ANUAL_SEQUENCIA_CONTRATO. */
	public static final String PARAMETRO_REINICIO_ANUAL_SEQUENCIA_CONTRATO = "REINICIO_ANUAL_SEQUENCIA_CONTRATO";

	/** The Constant PARAMETRO_DIAS_ANTECEDENCIA_RENOVACAO_CONTRATUAL. */
	public static final String PARAMETRO_DIAS_ANTECEDENCIA_RENOVACAO_CONTRATUAL = "DIAS_ANTECEDENCIA_RENOVACAO_CONTRATUAL";

	/** The Constant PARAMETRO_PERIODICIDADE_VALIDACAO_COMPROMISSO_TOP_QPNR. */
	public static final String PARAMETRO_PERIODICIDADE_VALIDACAO_COMPROMISSO_TOP_QPNR = "PERIODICIDADE_VALIDACAO_COMPROMISSO_TOP_QPNR";

	/** The Constant PARAMETRO_ANO_CONTRATUAL. */
	public static final String PARAMETRO_ANO_CONTRATUAL = "ANO_CONTRATUAL";

	/** The Constant PARAMETRO_TIPO_PERIODICIDADE_PENALIDADE. */
	public static final String PARAMETRO_TIPO_PERIODICIDADE_PENALIDADE = "TIPO_PERIODICIDADE_PENALIDADE";

	/** The Constant PARAMETRO_MUNICIPIO_IMPLANTADOS. */
	public static final String PARAMETRO_MUNICIPIO_IMPLANTADOS = "MUNICIPIO_IMPLANTADO";

	/** The Constant PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE. */
	public static final String PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE = "CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE";

	/** The Constant PARAMETRO_TIPO_DOCUMENTO_DEBITO_A_COBRAR. */
	public static final String PARAMETRO_TIPO_DOCUMENTO_DEBITO_A_COBRAR = "TIPO_DOCUMENTO_DEBITO_A_COBRAR";

	/** The Constant PARAMETRO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR. */
	public static final String PARAMETRO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR = "TIPO_DOCUMENTO_CREDITO_A_REALIZAR";

	/** The Constant PARAMETRO_LANCAMENTO_CONTABIL_MODULO_FATURAMENTO. */
	public static final String PARAMETRO_LANCAMENTO_CONTABIL_MODULO_FATURAMENTO = "LANCAMENTO_CONTABIL_MODULO_FATURAMENTO";

	/** The Constant PARAMETRO_LANCAMENTO_CONTABIL_MODULO_COBRANCA. */
	public static final String PARAMETRO_LANCAMENTO_CONTABIL_MODULO_COBRANCA = "LANCAMENTO_CONTABIL_MODULO_COBRANCA";

	/** The Constant PARAMETRO_LANCAMENTO_CONTABIL_MODULO_ARRECADACAO. */
	public static final String PARAMETRO_LANCAMENTO_CONTABIL_MODULO_ARRECADACAO = "LANCAMENTO_CONTABIL_MODULO_ARRECADACAO";

	/** The Constant PARAMETRO_LANCAMENTO_CONTABIL_MODULO_MEDICAO. */
	public static final String PARAMETRO_LANCAMENTO_CONTABIL_MODULO_MEDICAO = "LANCAMENTO_CONTABIL_MODULO_MEDICAO";

	/** The Constant PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL. */
	public static final String PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL = "CREDITO_DEBITO_SITUACAO_NORMAL";

	/** The Constant PARAMETRO_MENSAGEM_NOTA_DEBITO_MULTA_RESCISORIA. */
	public static final String PARAMETRO_MENSAGEM_NOTA_DEBITO_MULTA_RESCISORIA = "MENSAGEM_NOTA_DEBITO_MULTA_RESCISORIA";

	/** The Constant PARAMETRO_CREDITO_DEBITO_SITUACAO_PARCELADA. */
	public static final String PARAMETRO_CREDITO_DEBITO_SITUACAO_PARCELADA = "CREDITO_DEBITO_SITUACAO_PARCELADA";

	/** The Constant PARAMETRO_CREDITO_DEBITO_SITUACAO_CANCELADA. */
	public static final String PARAMETRO_CREDITO_DEBITO_SITUACAO_CANCELADA = "CREDITO_DEBITO_SITUACAO_CANCELADA";

	/** The Constant PARAMETRO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO. */
	public static final String PARAMETRO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO = "CREDITO_DEBITO_MOTIVO_CANCELAMENTO";

	/** The Constant PARAMETRO_CODIGO_BANCO. */
	public static final String PARAMETRO_CODIGO_BANCO = "CODIGO_BANCO";

	/** The Constant PARAMETRO_DATA_VENCIMENTO_COBRANCA_CONTRA_APRESENTACAO. */
	public static final String PARAMETRO_DATA_VENCIMENTO_COBRANCA_CONTRA_APRESENTACAO = "DATA_VENCIMENTO_COBRANCA_CONTRA_APRESENTACAO";

	/** The Constant PARAMETRO_QUANTIDADE_DIAS_VENCIMENTO_DOCUMENTO_COMPLEMENTAR. */
	public static final String PARAMETRO_QUANTIDADE_DIAS_VENCIMENTO_DOCUMENTO_COMPLEMENTAR = "QUANTIDADE_DIAS_VENCIMENTO_"
			+ "DOCUMENTO_COMPLEMENTAR";

	/** The Constant PARAMETRO_CREDITO_ORIGEM_DEVOLUCAO_TARIFA. */
	public static final String PARAMETRO_CREDITO_ORIGEM_DEVOLUCAO_TARIFA = "CREDITO_ORIGEM_DEVOLUCAO_TARIFA";

	/** The Constant PARAMETRO_CREDITO_ORIGEM_DEVOLUCAO_JUROS. */
	public static final String PARAMETRO_CREDITO_ORIGEM_DEVOLUCAO_JUROS = "CREDITO_ORIGEM_DEVOLUCAO_JUROS";

	/** The Constant PARAMETRO_CREDITO_ORIGEM_SERVICOS_INDIRETOS. */
	public static final String PARAMETRO_CREDITO_ORIGEM_SERVICOS_INDIRETOS = "CREDITO_ORIGEM_SERVICOS_INDIRETOS";

	/** The Constant PARAMETRO_CREDITO_ORIGEM_DESCONTOS_INCONDICIONAIS. */
	public static final String PARAMETRO_CREDITO_ORIGEM_DESCONTOS_INCONDICIONAIS = "CREDITO_ORIGEM_DESCONTOS_INCONDICIONAIS";

	/** The Constant PARAMETRO_CREDITO_ORIGEM_DESCONTOS_CONDICIONAIS. */
	public static final String PARAMETRO_CREDITO_ORIGEM_DESCONTOS_CONDICIONAIS = "CREDITO_ORIGEM_DESCONTOS_CONDICIONAIS";

	/** The Constant PARAMETRO_NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO. */
	public static final String PARAMETRO_NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO = "NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO";

	/** The Constant PARAMETRO_REFERENCIA_CONTABIL. */
	public static final String PARAMETRO_REFERENCIA_CONTABIL = "REFERENCIA_CONTABIL";

	/** The Constant PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_NOTA. */
	public static final String PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_NOTA = "TAMANHO_DESCRICAO_COMPLEMENTAR_NOTA";

	/** The Constant PARAMETRO_METODO_DE_REAJUSTE_CASCATA. */
	public static final String PARAMETRO_METODO_DE_REAJUSTE_CASCATA = "METODO_DE_REAJUSTE_CASCATA_PROPORCIONAL_COMPLEMENTAR";

	/** The Constant PARAMETRO_FILTRO_ITEM_FATURA_PRECO_GAS. */
	public static final String PARAMETRO_FILTRO_ITEM_FATURA_PRECO_GAS = "FILTRO_ITEM_FATURA_PRECO_GAS";

	/** The Constant PARAMETRO_QUANTIDADE_DE_FATURA_POR_LOTE. */
	public static final String PARAMETRO_QUANTIDADE_DE_FATURA_POR_LOTE = "QUANTIDADE_DE_FATURA_POR_LOTE";

	/** The Constant PARAMETRO_QTD_DIAS_CANCELAMENTO_NOTA_FISCAL. */
	public static final String PARAMETRO_QTD_DIAS_CANCELAMENTO_NOTA_FISCAL = "PARAMETRO_QTD_DIAS_CANCELAMENTO_NOTA_FISCAL";

	/** The Constant PARAMETRO_CODIGO_MOTIVO_INCLUSAO_FATURAR_NORMAL. */
	public static final String PARAMETRO_CODIGO_MOTIVO_INCLUSAO_FATURAR_NORMAL = "CODIGO_MOTIVO_INCLUSAO_FATURAR_NORMAL";

	/** The Constant PARAMETRO_TIPO_CONSUMO_MEDIA. */
	public static final String PARAMETRO_TIPO_CONSUMO_MEDIA = "TIPO_CONSUMO_MEDIA";

	/** The Constant PARAMETRO_TIPO_CONSUMO_INFORMADO. */
	public static final String PARAMETRO_TIPO_CONSUMO_INFORMADO = "TIPO_CONSUMO_INFORMADO";

	/** The Constant PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_MEDIA. */
	public static final String PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_MEDIA = "QUANTIDADE_MAXIMA_FATURAMENTO_MEDIA";

	/** The Constant PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_INFORMADO_CLIENTE. */
	public static final String PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_INFORMADO_CLIENTE = "QUANTIDADE_MAXIMA_"
			+ "FATURAMENTO_INFORMADO_CLIENTE";

	/** The Constant PARAMETRO_VALOR_MINIMO_GERACAO_DOCUMENTO_COBRANCA. */
	public static final String PARAMETRO_VALOR_MINIMO_GERACAO_DOCUMENTO_COBRANCA = "VALOR_MINIMO_GERACAO_DOCUMENTO_COBRANCA";

	/** The Constant PARAMETRO_NUMERO_DIAS_MINIMO_EMISSAO_VENCIMENTO. */
	public static final String PARAMETRO_NUMERO_DIAS_MINIMO_EMISSAO_VENCIMENTO = "NUMERO_DIAS_MINIMO_EMISSAO_VENCIMENTO";

	/** The Constant PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO. */
	public static final String PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO = "TIPO_ARREDONDAMENTO_CONSUMO";

	/** The Constant PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO. */
	public static final String PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO = "TIPO_ARREDONDAMENTO_CALCULO";

	/** The Constant PARAMETRO_QUANTIDADE_ESCALA_CALCULO. */
	public static final String PARAMETRO_QUANTIDADE_ESCALA_CALCULO = "QUANTIDADE_ESCALA_CALCULO";

	/** The Constant PARAMETRO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR. */
	public static final String PARAMETRO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR = "CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR";

	/** The Constant PARAMETRO_MENSAGEM_VALOR_MIM_DOCUMENTO_COBRANCA. */
	public static final String PARAMETRO_MENSAGEM_VALOR_MIM_DOCUMENTO_COBRANCA = "MENSAGEM_VALOR_MIM_DOCUMENTO_COBRANCA";

	/** The Constant PARAMETRO_INTERVALO_MAXIMO_COBRANCA_FATURA. */
	public static final String PARAMETRO_INTERVALO_MAXIMO_COBRANCA_FATURA = "PARAMETRO_INTERVALO_MAXIMO_COBRANCA_FATURA";

	/** The Constant PARAMETRO_MENSAGEM_DEBITO_AUTOMATICO. */
	public static final String PARAMETRO_MENSAGEM_DEBITO_AUTOMATICO = "MENSAGEM_DEBITO_AUTOMATICO";

	/** The Constant PARAMETRO_VOLUME_REF_TARIFA_MEDIA. */
	public static final String PARAMETRO_VOLUME_REF_TARIFA_MEDIA = "VOLUME_REF_TARIFA_MEDIA";

	/** The Constant PARAMETRO_TARIFA_RECUPERACAO_COMPROMISSO. */
	public static final String PARAMETRO_TARIFA_RECUPERACAO_COMPROMISSO = "TARIFA_RECUPERACAO_COMPROMISSO";

	/** The Constant PARAMETRO_SERVIDOR_SMTP_HOST. */
	public static final String PARAMETRO_SERVIDOR_SMTP_HOST = "SERVIDOR_SMTP_HOST";

	/** The Constant PARAMETRO_SERVIDOR_SMTP_PORT. */
	public static final String PARAMETRO_SERVIDOR_SMTP_PORT = "SERVIDOR_SMTP_PORT";

	/** The Constant PARAMETRO_SERVIDOR_SMTP_USER. */
	public static final String PARAMETRO_SERVIDOR_SMTP_USER = "SERVIDOR_SMTP_USER";

	/** The Constant PARAMETRO_SERVIDOR_SMTP_PASSWD. */
	public static final String PARAMETRO_SERVIDOR_SMTP_PASSWD = "SERVIDOR_SMTP_PASSWORD"; // NOSONAR Credentials should
																																												// not be hard-coded :: O
																																												// password é armazenado no
																																												// banco e obtido através da
																																												// constante
	/** The Constant PARAMETRO_SERVIDOR_AUTENTICACAO_AD_HOST. */
	public static final String PARAMETRO_SERVIDOR_AUTENTICACAO_AD_HOST = "SERVIDOR_AUTENTICACAO_AD_HOST";

	/** The Constant PARAMETRO_SERVIDOR_AUTENTICACAO_AD_PORTA. */
	public static final String PARAMETRO_SERVIDOR_AUTENTICACAO_AD_PORTA = "SERVIDOR_AUTENTICACAO_AD_PORTA";

	/** The Constant PARAMETRO_SERVIDOR_AUTENTICACAO_AD_DOMINIO. */
	public static final String PARAMETRO_SERVIDOR_AUTENTICACAO_AD_DOMINIO = "SERVIDOR_AUTENTICACAO_AD_DOMINIO";

	/** The Constant PARAMETRO_AUTENTICACAO_POR_AD_WINDOWS. */
	public static final String PARAMETRO_AUTENTICACAO_POR_AD_WINDOWS = "AUTENTICACAO_POR_AD_WINDOWS";


	/** The Constant PARAMETRO_EMAIL_REMETENTE_PADRAO. */
	public static final String PARAMETRO_EMAIL_REMETENTE_PADRAO = "EMAIL_REMETENTE_DEFAULT";

	/** The Constant PARAMETRO_SERVIDOR_SMTP_AUTH. */
	public static final String PARAMETRO_SERVIDOR_SMTP_AUTH = "SERVIDOR_SMTP_AUTH";

	/** The Constant PARAMETRO_CONCEDE_DESCONTO_INADIMPLENTE. */
	public static final String PARAMETRO_CONCEDE_DESCONTO_INADIMPLENTE = "CONCEDE_DESCONTO_INADIMPLENTE";

	/** The Constant PARAMETRO_EXPURGAR_PERIODO_TESTE_APURACAO. */
	public static final String PARAMETRO_EXPURGAR_PERIODO_TESTE_APURACAO = "EXPURGAR_PERIODO_TESTE_APURACAO";

	/**
	 * The Constant PARAMETRO_MODULO_MEDICAO_CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO.
	 */
	public static final String PARAMETRO_MODULO_MEDICAO_CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO = "CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO";

	/** The Constant PARAMETRO_MODULO_MEDICAO_EXIBE_COLUNAS_SUPERVISORIO. */
	public static final String PARAMETRO_MODULO_MEDICAO_EXIBE_COLUNAS_SUPERVISORIO = "EXIBE_COLUNAS_SUPERVISORIO";

	/**
	 * The Constant PARAMETRO_EXIGE_COMENTARIO_MEDICAO_DIARIA_HORARIA_SUPERVISORIO.
	 */
	public static final String PARAMETRO_EXIGE_COMENTARIO_MEDICAO_DIARIA_HORARIA_SUPERVISORIO = "EXIGE_COMENTARIO_MEDICAO_"
			+ "DIARIA_HORARIA_SUPERVISORIO";

	/** The Constant PARAMETRO_NUMERO_MAXIMO_DIGITOS_CORRETOR. */
	public static final String PARAMETRO_NUMERO_MAXIMO_DIGITOS_CORRETOR = "NUMERO_MAXIMO_DIGITOS_CORRETOR";

	/** The Constant PARAMETRO_MODULO_MEDICAO_TIPO_CONSUMO_INFORMADO. */
	public static final String PARAMETRO_MODULO_MEDICAO_TIPO_CONSUMO_INFORMADO = "TIPO_CONSUMO_INFORMADO";

	/** The Constant PARAMETRO_MODULO_MEDICAO_TIPO_CONSUMO_MEDIA. */
	public static final String PARAMETRO_MODULO_MEDICAO_TIPO_CONSUMO_MEDIA = "TIPO_CONSUMO_MEDIA";

	/** The Constant PARAMETRO_DIRETORIO_IMPORTAR_LEITURA_UPLOAD. */
	public static final String PARAMETRO_DIRETORIO_IMPORTAR_LEITURA_UPLOAD = "DIRETORIO_IMPORTAR_LEITURA_UPLOAD";
	
	/** The Constant PARAMETRO_DIRETORIO_REGISTRO_FOTO. */
	public static final String PARAMETRO_DIRETORIO_REGISTRO_FOTO = "DIRETORIO_REGISTRO_FOTO";
	
	/** The Constant PARAMETRO_DIRETORIO_FALHA_REGISTRO_FOTO. */
	public static final String PARAMETRO_DIRETORIO_FALHA_REGISTRO_FOTO = "DIRETORIO_FALHA_REGISTRO_FOTO";



	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR = "CODIGO_ENTIDADE_CLASSE_"
			+ "MOTIVO_REFATURAR";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_CREDITO_DEBITO_SITUACAO_NORMAL. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_CREDITO_DEBITO_SITUACAO_NORMAL = "CREDITO_DEBITO_SITUACAO_NORMAL";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO = "DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO";

	/** The Constant PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO. */
	public static final String PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO = "PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_FILTRO_ITEM_FATURA_PRECO_GAS. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_FILTRO_ITEM_FATURA_PRECO_GAS = "FILTRO_ITEM_FATURA_PRECO_GAS";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_METODO_DE_REAJUSTE_CASCATA_PROPORCIONAL_COMPLEMENTAR.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_METODO_DE_REAJUSTE_CASCATA_PROPORCIONAL_COMPLEMENTAR = "METODO_DE_REAJUSTE_"
			+ "CASCATA_PROPORCIONAL_COMPLEMENTAR";

	/** The Constant PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR. */
	public static final String PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR = "TAMANHO_DESCRICAO_COMPLEMENTAR_"
			+ "CRE_DEB_REALIZAR";

	/** The Constant PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA. */
	public static final String PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA = "TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA";

	/** The Constant PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_DESTACADO. */
	public static final String PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_DESTACADO = "TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_DESTACADO";

	/**
	 * The Constant PARAMETRO_MODULO_FATURAMENTO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR = "TIPO_DOCUMENTO_CREDITO_A_REALIZAR";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO = "PERCENTUAL_VALOR_"
			+ "FATURA_LIMITE_DESCONTO";

	/** The Constant PARAMETRO_TIPO_SUBSTITUTO. */
	public static final String PARAMETRO_TIPO_SUBSTITUTO = "TIPO_SUBSTITUTO";

	/** The Constant PARAMETRO_REGIME_TRIBUTARIO. */
	public static final String PARAMETRO_REGIME_TRIBUTARIO = "REGIME_TRIBUTARIO";

	/** The Constant PARAMETRO_CODIGO_INDICADOR_AMBIENTE_SISTEMA_NFE. */
	public static final String PARAMETRO_CODIGO_INDICADOR_AMBIENTE_SISTEMA_NFE = "CODIGO_INDICADOR_AMBIENTE_SISTEMA_NFE";

	/** The Constant PARAMETRO_NR_VERSAO_XML_NFE. */
	public static final String PARAMETRO_NR_VERSAO_XML_NFE = "NR_VERSAO_XML_NFE";

	/** The Constant PARAMETRO_USUARIO_GGAS. */
	public static final String PARAMETRO_USUARIO_GGAS = "USUARIO_GGAS";

	/** The Constant PARAMETRO_CODIGO_EMPRESA_FEBRABAN. */
	public static final String PARAMETRO_CODIGO_EMPRESA_FEBRABAN = "CODIGO_EMPRESA_FEBRABAN";

	/** The Constant PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO. */
	public static final String PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO = "REFERENCIA_INTEGRACAO_NOTA_TITULO";

	/** The Constant PARAMETRO_TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO. */
	public static final String PARAMETRO_TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO = "TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO";

	/** The Constant C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CLIENTE. */
	public static final String C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CLIENTE = "C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CLIENTE";

	/** The Constant C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO. */
	public static final String C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO = "C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO";


	/** The Constant ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO. */
	public static final String ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO = "ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO";

	/** The Constant ARRECADACAO_CODIGO_CONVENIO. */
	public static final String ARRECADACAO_CODIGO_CONVENIO = "ARRECADACAO_CODIGO_CONVENIO";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_ARRECADACAO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE.
	 */
	public static final String PARAMETRO_MODULO_ARRECADACAO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE = "CODIGO_SITUACAO_"
			+ "RECEBIMENTO_DOCTO_INEXISTENTE";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_ARRECADACAO. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_ARRECADACAO = "DIRETORIO_ARQUIVOS_ARRECADACAO";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_REMESSA. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_REMESSA = "DIRETORIO_ARQUIVOS_REMESSA";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_RETORNO. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_RETORNO = "DIRETORIO_ARQUIVOS_RETORNO";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_UPLOAD. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_UPLOAD = "DIRETORIO_ARQUIVOS_UPLOAD";

	/** The Constant PARAMETRO_GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR. */
	public static final String PARAMETRO_GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR = "GERA_DOCUMENTO_COBRANCA_POR_"
			+ "RECEBIMENTO_MENOR";

	/** The Constant PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR. */
	public static final String PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR = "PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR";

	/**
	 * The Constant PARAMETRO_VALOR_MIN_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR.
	 */
	public static final String PARAMETRO_VALOR_MIN_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR = "VALOR_MIN_DOCUMENTO_COBRANCA_"
			+ "POR_RECEBIMENTO_MENOR";

	/** The Constant PARAMETRO_GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE. */
	public static final String PARAMETRO_GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE = "GERA_CREDITO_POR_RECEBIMENTO_"
			+ "MAIOR_DUPLICIDADE";

	/**
	 * The Constant PARAMETRO_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_ENCERRAR_CONTRATO.
	 */
	public static final String PARAMETRO_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_ENCERRAR_CONTRATO = "MOTIVO_FIM_RELACIONAMENTO_"
			+ "CLIENTE_ENCERRAR_CONTRATO";

	/** The Constant PARAMETRO_NOME_EMPRESA. */
	public static final String PARAMETRO_NOME_EMPRESA = "NOME_EMPRESA";

	/** The Constant PARAMETRO_CODIGO_CARACTERE_NUMERO. */
	public static final String PARAMETRO_CODIGO_CARACTERE_NUMERO = "CODIGO_CARACTERE_NUMERO";

	/** The Constant PARAMETRO_CODIGO_CARACTERE_MINUSCULO. */
	public static final String PARAMETRO_CODIGO_CARACTERE_MINUSCULO = "CODIGO_CARACTERE_MINUSCULO";

	/** The Constant PARAMETRO_CODIGO_CARACTERE_MAIUSCULO. */
	public static final String PARAMETRO_CODIGO_CARACTERE_MAIUSCULO = "CODIGO_CARACTERE_MAIUSCULO";

	/** The Constant PARAMETRO_TAMANHO_MINIMO_SENHA. */
	public static final String PARAMETRO_TAMANHO_MINIMO_SENHA = "TAMANHO_MINIMO_SENHA";

	/** The Constant PARAMETRO_TAMANHO_MAXIMO_SENHA. */
	public static final String PARAMETRO_TAMANHO_MAXIMO_SENHA = "TAMANHO_MAXIMO_SENHA";

	/** The Constant PARAMETRO_MENSAGEM_SENHA_EXPIRADA. */
	public static final String PARAMETRO_MENSAGEM_SENHA_EXPIRADA = "MENSAGEM_SENHA_EXPIRADA";

	/** The Constant PARAMETRO_SERVIDOR_IP. */
	public static final String PARAMETRO_SERVIDOR_IP = "SERVIDOR_IP";

	/** The Constant PARAMETRO_DIRETORIO_RELATORIO_PDF. */
	public static final String PARAMETRO_DIRETORIO_RELATORIO_PDF = "DIRETORIO_RELATORIO_PDF";

	/** The Constant PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO. */
	public static final String PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO = "QUANTIDADE_ANOS_VIGENCIA_FERIADO";

	/** The Constant PARAMETRO_MODULO_COBRANCA_CREDITO_DEBITO_SITUACAO_PARCELADA. */
	public static final String PARAMETRO_MODULO_COBRANCA_CREDITO_DEBITO_SITUACAO_PARCELADA = "CREDITO_DEBITO_SITUACAO_PARCELADA";

	/** The Constant PARAMETRO_MODULO_COBRANCA_CREDITO_DEBITO_SITUACAO_PARCELADA. */
	public static final String PARAMETRO_OBRIGATORIO_RETORNO_PROTOCOLO = "OBRIGATORIO_RETORNO_PROTOCOLO";

	/**
	 * The Constant PARAMETRO_MODULO_COBRANCA_QUANTIDADE_CLIENTES_RELATORIO_LOTE.
	 */
	public static final String PARAMETRO_MODULO_COBRANCA_QUANTIDADE_CLIENTES_RELATORIO_LOTE = "QUANTIDADE_CLIENTES_RELATORIO_LOTE";

	/** The Constant PARAMETRO_VALIDAR_MASCARA_INSCRICAO_ESTADUAL. */
	public static final String PARAMETRO_VALIDAR_MASCARA_INSCRICAO_ESTADUAL = "VALIDAR_MASCARA_INSCRICAO_ESTADUAL";

	/** The Constant PARAMETRO_MASCARA_INSCRICAO_ESTADUAL. */
	public static final String PARAMETRO_MASCARA_INSCRICAO_ESTADUAL = "MASCARA_INSCRICAO_ESTADUAL";

	/**
	 * The Constant PARAMETRO_MODULO_CADASTRO_CODIGO_TIPO_ENDERECO_CORRESPONDENCIA.
	 */
	public static final String PARAMETRO_MODULO_CADASTRO_CODIGO_TIPO_ENDERECO_CORRESPONDENCIA = "CODIGO_TIPO_ENDERECO_CORRESPONDENCIA";

	/** The Constant PARAMETRO_SENHA_CODIGO_CARACTERE_NUMERO. */
	public static final String PARAMETRO_SENHA_CODIGO_CARACTERE_NUMERO = "CODIGO_CARACTERE_NUMERO";

	/** The Constant PARAMETRO_SENHA_CODIGO_CARACTERE_MINUSCULO. */
	public static final String PARAMETRO_SENHA_CODIGO_CARACTERE_MINUSCULO = "CODIGO_CARACTERE_MINUSCULO";

	/** The Constant PARAMETRO_SENHA_CODIGO_CARACTERE_MAIUSCULO. */
	public static final String PARAMETRO_SENHA_CODIGO_CARACTERE_MAIUSCULO = "CODIGO_CARACTERE_MAIUSCULO";

	/** The Constant PARAMETRO_SENHA_TAMANHO_MINIMO_SENHA. */
	public static final String PARAMETRO_SENHA_TAMANHO_MINIMO_SENHA = "TAMANHO_MINIMO_SENHA";

	/** The Constant PARAMETRO_SENHA_TAMANHO_MAXIMO_SENHA. */
	public static final String PARAMETRO_SENHA_TAMANHO_MAXIMO_SENHA = "TAMANHO_MAXIMO_SENHA";

	/** The Constant PARAMETRO_PERIODICIDADE_SENHA_RENOVACAO. */
	public static final String PARAMETRO_PERIODICIDADE_SENHA_RENOVACAO = "PERIODICIDADE_SENHA_RENOVACAO";

	/** The Constant PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_A_REALIZAR. */
	public static final String PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_A_REALIZAR = "CODIGO_TABELA_CREDITO_DEBITO_A_REALIZAR";

	/** The Constant PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_NEGOCIADO. */
	public static final String PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_NEGOCIADO = "CODIGO_TABELA_CREDITO_DEBITO_NEGOCIADO";

	/** The Constant PARAMETRO_CODIGO_TABELA_PARCELAMENTO. */
	public static final String PARAMETRO_CODIGO_TABELA_PARCELAMENTO = "CODIGO_TABELA_PARCELAMENTO";

	/** The Constant PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_DIARIA. */
	public static final String PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_DIARIA = "CODIGO_TABELA_SUPER_MEDICAO_DIARIA";

	/** The Constant PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_HORARIA. */
	public static final String PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_HORARIA = "CODIGO_TABELA_SUPER_MEDICAO_HORARIA";

	/** The Constant P_LOCAL_BEM_INSTALACAO. */
	public static final String P_LOCAL_BEM_INSTALACAO = "P_LOCAL_BEM_INSTALACAO";

	/** The Constant PARAMETRO_MASCARA_NUMERO_CONTA. */
	public static final String PARAMETRO_MASCARA_NUMERO_CONTA = "MASCARA_NUMERO_CONTA";

	/** The Constant PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA. */
	public static final String PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA = "TEMPERATURA_CONDICAO_REFERENCIA";

	/** The Constant PARAMETRO_PRESSAO_CONDICAO_REFERENCIA. */
	public static final String PARAMETRO_PRESSAO_CONDICAO_REFERENCIA = "PRESSAO_CONDICAO_REFERENCIA";

	/** The Constant PARAMETRO_PCS_REFERENCIA. */
	public static final String PARAMETRO_PCS_REFERENCIA = "PCS_REFERENCIA";

	/** The Constant PARAMETRO_ARREDONDAMENTO_FATOR_PCS. */
	public static final String PARAMETRO_ARREDONDAMENTO_FATOR_PCS = "ARREDONDAMENTO_FATOR_PCS";

	/** The Constant PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS. */
	public static final String PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS = "TIPO_INTEGRACAO_SUPERVISORIOS";

	/** The Constant PARAMETRO_TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIO. */
	public static final String PARAMETRO_TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIO = "TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIO";

	/** The Constant PARAMETRO_PERMITE_ALTERACAO_PCS_CITY_GATE_FATURADO. */
	public static final String PARAMETRO_PERMITE_ALTERACAO_PCS_CITY_GATE_FATURADO = "PERMITE_ALTERACAO_PCS_CITY_GATE_FATURADO";

	/** The Constant PARAMETRO_PERMITE_ALTERACAO_PCSZ_IMOVEL. */
	public static final String PARAMETRO_PERMITE_ALTERACAO_PCSZ_IMOVEL = "PERMITE_ALTERACAO_PCSZ_IMOVEL";

	/** Constante PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS. */
	public static final String PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS = "QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS";

	public static final String PARAMETRO_PRAZO_MIN_ENTREGA_FATURA_VENCIMENTO = "PARAMETRO_PRAZO_MIN_ENTREGA_FATURA_VENCIMENTO";

	/** The Constant PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS_ROTA. */
	public static final String PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS = "QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS";

	/** The Constant PARAMETRO_UNIDADE_FEDERACAO_IMPLANTACAO. */
	public static final String PARAMETRO_UNIDADE_FEDERACAO_IMPLANTACAO = "UNIDADE_FEDERACAO_IMPLANTACAO";

	/** The Constant PARAMETRO_GERA_CREDITO_VOLUME_AUTOMATICAMENTE. */
	public static final String PARAMETRO_GERA_CREDITO_VOLUME_AUTOMATICAMENTE = "GERA_CREDITO_VOLUME_AUTOMATICAMENTE";

	/** The Constant PARAMETRO_HORA_INICIO_DIA. */
	public static final String PARAMETRO_HORA_INICIO_DIA = "HORA_INICIO_DIA";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_FATURA. */
	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_FATURA = "DIRETORIO_ARQUIVOS_FATURA";

	/** The Constant PARAMETRO_TEXTO_EMAIL_FATURA. */
	public static final String PARAMETRO_TEXTO_EMAIL_FATURA = "TEXTO_EMAIL_FATURA";

	/** The Constant PARAMETRO_DIRETORIO_ARQUIVOS_RELATORIO_LEITURA_CONSUMO. */
	public static final String PARAMETRO_DIRETORIO_RELATORIO_LEITURA_CONSUMO = "DIRETORIO_RELATORIO_LEITURA_CONSUMO";

	/** The Constant PARAMETRO_TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIOS. */
	public static final String PARAMETRO_TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIOS = "TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIOS";

	/** The Constant PARAMETRO_CAIXA_ALTA. */
	public static final String PARAMETRO_CAIXA_ALTA = "CAIXA_ALTA";

	/** The Constant PARAMETRO_ACEITA_CARACTER_ESPECIAL. */
	public static final String PARAMETRO_ACEITA_CARACTER_ESPECIAL = "ACEITA_CARACTER_ESPECIAL";

	/** The Constant PARAMETRO_REFERENCIA_FATURAMENTO. */
	public static final String PARAMETRO_REFERENCIA_FATURAMENTO = "REFERENCIA_FATURAMENTO";

	/** The Constant PARAMETRO_EXIGE_COMENTARIO_ANALISE_EXCECAO. */
	public static final String PARAMETRO_EXIGE_COMENTARIO_ANALISE_EXCECAO = "EXIGE_COMENTARIO_ANALISE_EXCECAO";

	/** The Constant PARAMETRO_QUANTIDADE_ANOS_CALENDARIO. */
	public static final String PARAMETRO_QUANTIDADE_ANOS_CALENDARIO = "QUANTIDADE_ANOS_CALENDARIO";

	/** The Constant REMANEJADO_COM_SUCESSO. */
	public static final String REMANEJADO_COM_SUCESSO = "REMANEJADO_COM_SUCESSO";

	/** The Constant PARAMETRO_TAMANHO_MAXIMO_ANEXO. */
	public static final String PARAMETRO_TAMANHO_MAXIMO_ANEXO = "P_TAMANHO_MAXIMO_ANEXO";

	/** The Constant ENVIAR_EMAIL_AO_SALVAR_PROPOSTA. */
	public static final String ENVIAR_EMAIL_AO_SALVAR_PROPOSTA = "ENVIAR_EMAIL_AO_SALVAR_PROPOSTA";

	/** The Constant OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA. */
	public static final String OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA = "OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA";

	/** The Constant PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO. */
	public static final String PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO = "SERVIDOR_EMAIL_CONFIGURADO";

	/** The Constant PARAMETRO_CONTABILIZAR_DESCONTO_TARIFA. */
	public static final String PARAMETRO_CONTABILIZAR_DESCONTO_TARIFA = "CONTABILIZAR_DESCONTO_TARIFA";

	/** The Constant SUCESSO_APROVADA. */
	public static final String SUCESSO_APROVADA = "SUCESSO_APROVADA";

	/** The Constant SUCESSO_REJEITADA. */
	public static final String SUCESSO_REJEITADA = "SUCESSO_REJEITADA";
	
	/** The Constant DESATIVAR_PONTO_CONTRATO. */
	public static final String DESATIVAR_PONTO_CONTRATO = "DESATIVAR_PONTO_CONTRATO";


	/**
	 * Bloco de constantes referente a Constantes do Sistema Neste bloco constarão
	 * as constantes que identificam uma Constante do sistema.
	 */

	/**
	 * Indica a situação onde o sistema deverá baixar um recebimento a menor como
	 * despesa
	 */
	public static final String C_BAIXA_RECEBIMENTO_MENOR_DESPESA = "C_BAIXA_RECEBIMENTO_MENOR_DESPESA";

	/**
	 * Indica a situação onde o sistema deverá baixar um recebimento a menor e gerar
	 * um débito a cobrar com o valor pago a menor.
	 */
	public static final String C_BAIXA_RECEBIMENTO_MENOR_GERA_DEBITO = "C_BAIXA_RECEBIMENTO_MENOR_GERA_DEBITO";

	/**
	 * Indica a situação onde o sistema não deverá baixar um recebimento a menor.
	 */
	public static final String C_NAO_BAIXA_RECEBIMENTO_MENOR = "C_NAO_BAIXA_RECEBIMENTO_MENOR";

	/** Indica que o recebimento é maior que o valor do debito. */
	public static final String C_RECEBIMENTO_DUPLICIDADE_EXCESSO = "C_RECEBIMENTO_DUPLICIDADE_EXCESSO";

	/** Indica que o documeto paga não foi encontrado. */
	public static final String C_RECEBIMENTO_DOCUMENTO_INEXISTENTE = "C_RECEBIMENTO_DOCUMENTO_INEXISTENTE";

	/** Indica que o recebimento é menor que o valor do debito. */
	public static final String C_RECEBIMENTO_VALOR_NAO_CONFERE = "C_RECEBIMENTO_VALOR_NAO_CONFERE";

	/** Indica que o recebimento confere com o valor do débito pago. */
	public static final String C_RECEBIMENTO_CLASSIFICADO = "C_RECEBIMENTO_CLASSIFICADO";

	/** Indica que o recebimento foi estornado. */
	public static final String C_RECEBIMENTO_ESTORNADO = "C_RECEBIMENTO_ESTORNADO";

	/** Indica que o recebimento foi realizado por Baixa por Dação. */
	public static final String C_RECEBIMENTO_BAIXA_POR_DACAO = "C_RECEBIMENTO_BAIXA_POR_DACAO";

	/**
	 * Chave da propriedade MENSAGEM_RECEBIMENTO_CLASSIFICADO no arquivo
	 * mensagens.properties
	 */
	public static final String MENSAGEM_RECEBIMENTO_CLASSIFICADO = "MENSAGEM_RECEBIMENTO_CLASSIFICADO";
	/**
	 * Chave da propriedade MENSAGEM_RECEBIMENTO_VALOR_NAO_CONFERE no arquivo
	 * mensagens.properties
	 */
	public static final String MENSAGEM_RECEBIMENTO_VALOR_NAO_CONFERE = "MENSAGEM_RECEBIMENTO_VALOR_NAO_CONFERE";
	/**
	 * Chave da propriedade MENSAGEM_RECEBIMENTO_DUPLICIDADE_EXCESSO no arquivo
	 * mensagens.properties
	 */
	public static final String MENSAGEM_RECEBIMENTO_DUPLICIDADE_EXCESSO = "MENSAGEM_RECEBIMENTO_DUPLICIDADE_EXCESSO";
	/**
	 * Chave da propriedade MENSAGEM_RECEBIMENTO_DOCUMENTO_INEXISTENTE no arquivo
	 * mensagens.properties
	 */
	public static final String MENSAGEM_RECEBIMENTO_DOCUMENTO_INEXISTENTE = "MENSAGEM_RECEBIMENTO_DOCUMENTO_INEXISTENTE";

	/** Indica o convênio débito automático. */
	public static final String C_TIPO_CONVENIO_DEBITO_AUTOMATICO = "C_TIPO_CONVENIO_DEBITO_AUTOMATICO";

	/** Indica o convênio cobrança registrada. */
	public static final String C_TIPO_CONVENIO_COBRANCA_REGISTRADA = "C_TIPO_CONVENIO_COBRANCA_REGISTRADA";

	/** Indica o convênio cobrança registrada escritural. */
	public static final String C_TIPO_CONVENIO_COBRANCA_REGISTRADA_ESCRITURAL = "C_TIPO_CONVENIO_COBRANCA_REGISTRADA_ESCRITURAL";

	/** Indica o convênio cobrança não registrada. */
	public static final String C_TIPO_CONVENIO_NAO_COBRANCA_REGISTRADA = "C_TIPO_CONVENIO_NAO_COBRANCA_REGISTRADA";

	/** Indica a situação do recebimento Pago. */
	public static final String C_SITUACAO_RECEBIMENTO_PAGO = "C_SITUACAO_RECEBIMENTO_PAGO";
	
	/** Indica a situação do recebimento Prescrito. */
	public static final String C_SITUACAO_RECEBIMENTO_PRESCRITO = "C_SITUACAO_RECEBIMENTO_PRESCRITO";

	/** Indica a situação do recebimento Parcialmente Pago. */
	public static final String C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO = "C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO";

	/** Indica a situação do recebimento Pendente. */
	public static final String C_SITUACAO_RECEBIMENTO_PENDENTE = "C_SITUACAO_RECEBIMENTO_PENDENTE";

	/** Indica a situação de recebimento devolvido. */
	public static final String C_SITUACAO_RECEBIMENTO_DEVOLVIDO = "C_SITUACAO_RECEBIMENTO_DEVOLVIDO";

	/** Indica a situação de contrato Ativo. */
	public static final String C_CONTRATO_EM_NEGOCIACAO = "C_CONTRATO_EM_NEGOCIACAO";

	/** The Constant FAIXA_CONSUMO_VARIACAO_INICIO. */
	public static final String FAIXA_CONSUMO_VARIACAO_INICIO = "0";

	/** The Constant FAIXA_CONSUMO_VARIACAO_FINAL. */
	public static final Integer FAIXA_CONSUMO_VARIACAO_FINAL = 99999;

	/** Indica a classe de unidade comprimento. */
	public static final String C_UNIDADE_COMPRIMENTO = "C_UNIDADE_COMPRIMENTO";

	/** Indica a classe de unidade pressão. */
	public static final String C_UNIDADE_PRESSAO = "C_UNIDADE_PRESSAO";

	/** Indica a classe de unidade quantidade. */
	public static final String C_UNIDADE_QUANTIDADE = "C_UNIDADE_QUANTIDADE";

	/** Indica a classe de unidade temperatura. */
	public static final String C_UNIDADE_TEMPERATURA = "C_UNIDADE_TEMPERATURA";

	/** Indica a classe de unidade vazão. */
	public static final String C_UNIDADE_VAZAO = "C_UNIDADE_VAZAO";

	/** Indica a classe de unidade volume. */
	public static final String C_UNIDADE_VOLUME = "C_UNIDADE_VOLUME";

	/** Indica a situação de imóvel factível. */
	public static final String C_IMOVEL_FACTIVEL = "C_IMOVEL_FACTIVEL";

	/** Indica a situação de imóvel interligado. */
	public static final String C_IMOVEL_INTERLIGADO = "C_IMOVEL_INTERLIGADO";

	/** Indica a situação de imóvel potencial. */
	public static final String C_IMOVEL_POTENCIAL = "C_IMOVEL_POTENCIAL";

	/** Indica a situação de imóvel não liberado. */
	public static final String C_IMOVEL_NAO_LIBERADO = "C_IMOVEL_NAO_LIBERADO";

	/** Indica Imóvel em Prospecção. */
	public static final String C_IMOVEL_EM_PROSPECCAO = "C_IMOVEL_EM_PROSPECCAO";

	/** Indica a esfera do poder municipal. */
	public static final String C_ESFERA_PODER_MUNICIAL = "C_ESFERA_PODER_MUNICIAL";

	/** Indica a situação do ponto de consumo aguardando ativação. */
	public static final String C_PONTO_CONSUMO_AGUARDANDO_ATIVACAO = "C_PONTO_CONSUMO_AGUARDANDO_ATIVACAO";

	/** Indica a Nacionalidade Estrangeira. */
	public static final String C_NACIONALIDADE_ESTRANGEIRA = "C_NACIONALIDADE_ESTRANGEIRA";

	/** Indica a periodicidade de juros mensal. */
	public static final String C_JUROS_MENSAL = "C_JUROS_MENSAL";

	/** Indica a forma de cobrança Nota de débitos. */
	public static final String C_FORMA_COBRANCA_NOTA_DEBITO = "C_FORMA_COBRANCA_NOTA_DEBITO";

	/** Indica a forma de cobrança Boleto Bancário. */
	public static final String C_FORMA_COBRANCA_BOLETO_BANCARIO = "C_FORMA_COBRANCA_BOLETO_BANCARIO";

	/** Indica a forma de cobrança código de barras. */
	public static final String C_FORMA_COBRANCA_CODIGO_BARRAS = "C_FORMA_COBRANCA_CODIGO_BARRAS";

	/** Indica a forma de cobrança Fatura. */
	public static final String C_FORMA_COBRANCA_FATURA = "C_FORMA_COBRANCA_FATURA";

	/** Indica a Multa por atraso. */
	public static final String C_MULTA_ATRASO = "C_MULTA_ATRASO";

	/** Indica o Juros de mora. */
	public static final String C_JUROS_MORA = "C_JUROS_MORA";

	/** Indica a cobrança na fatura. */
	public static final String C_COBRANCA_FATURA = "C_COBRANCA_FATURA";

	/** Indica a cobrança na nota de débito no mes atual. */
	public static final String C_COBRANCA_NOTA_MES_ATUAL = "C_COBRANCA_NOTA_MES_ATUAL";

	/** Indica a cobrança na nota de débito no mes atual mais um mes. */
	public static final String C_COBRANCA_NOTA_MES_POSTERIOR = "C_COBRANCA_NOTA_MES_POSTERIOR";

	/** Indica o tipo de rubrica Produto. */
	public static final String C_TIPO_RUBRICA_PRODUTO = "C_TIPO_RUBRICA_PRODUTO";

	/** Indica o tipo de rubrica Parcelamento. */
	public static final String C_TIPO_RUBRICA_PARCELAMENTO = "C_TIPO_RUBRICA_PARCELAMENTO";

	/** Indica o tipo de rubrica Serviço. */
	public static final String C_TIPO_RUBRICA_SERVICO = "C_TIPO_RUBRICA_SERVICO";

	/** Indica a amortização Price. */
	public static final String C_AMORTIZACAO_PRICE = "C_AMORTIZACAO_PRICE";

	/** Indica a amortização SAC. */
	public static final String C_AMORTIZACAO_SAC = "C_AMORTIZACAO_SAC";

	/** Indica a situação de cobrança PENDENTE. */
	public static final String C_SITUACAO_COBRANCA_PENDENTE = "C_SITUACAO_COBRANCA_PENDENTE";

	/** Indica o tipo de documento Extrato de Débito. */
	public static final String C_TIPO_DOCUMENTO_EXTRATO_DEBITO = "C_TIPO_DOCUMENTO_EXTRATO_DEBITO";

	/** Indica o item de lançamento contábil Desconto. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_DESCONTO = "C_LANCAMENTO_ITEM_CONTABIL_DESCONTO";

	/** Indica o item de lançamento contábil Desconto sem lancamento contabil. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_DESCONTO_SEM_LANCAMENTO_CONTABIL = "C_LANCAMENTO_ITEM_CONTABIL_DESCONTO_"
			+ "SEM_LANCAMENTO_CONTABIL";

	/** IndiIndica o item de lançamento contábil Juros de Financiamento. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_JUROS_FINANCIAMENTO = "C_LANCAMENTO_ITEM_CONTABIL_JUROS_FINANCIAMENTO";

	/** Indica o item de lançamento contábil Juros de Mora. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_JUROS_MORA = "C_LANCAMENTO_ITEM_CONTABIL_JUROS_MORA";

	/** Indica o item de lançamento contábil Juros de Parcelamento. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO = "C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO";

	/** Indica o item de lançamento contábil Parcelamento. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_PARCELAMENTO = "C_LANCAMENTO_ITEM_CONTABIL_PARCELAMENTO";

	/** Indica o item de lançamento contábil Penalidade Ship or Pay. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_SHIP_OR_PAY = "C_LANCAMENTO_ITEM_CONTABIL_SHIP_OR_PAY";

	/** Indica o item de lançamento contábil Penalidade Take or Pay. */
	public static final String C_LANCAMENTO_ITEM_CONTABIL_TAKE_OR_PAY = "C_LANCAMENTO_ITEM_CONTABIL_TAKE_OR_PAY";

	/** indica o índice financeiro Dolar. */
	public static final String C_DOLAR = "C_DOLAR";

	/** Indica a penalidade Delivery or Pay. */
	public static final String C_PENALIDADE_DELIVERY_OR_PAY = "C_PENALIDADE_DELIVERY_OR_PAY";

	/** Indica a penalidade Gás Fora de Especificação. */
	public static final String C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO = "C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO";

	/** Indica a penalidade Retirada a Maior. */
	public static final String C_PENALIDADE_RETIRADA_A_MAIOR = "C_PENALIDADE_RETIRADA_A_MAIOR";

	/** Indica a penalidade Retirada a Menor. */
	public static final String C_PENALIDADE_RETIRADA_A_MENOR = "C_PENALIDADE_RETIRADA_A_MENOR";

	/** Indica a penalidade Ship or Pay. */
	public static final String C_PENALIDADE_SHIP_OR_PAY = "C_PENALIDADE_SHIP_OR_PAY";

	/** Indica a penalidade Take or Pay. */
	public static final String C_PENALIDADE_TAKE_OR_PAY = "C_PENALIDADE_TAKE_OR_PAY";

	/**
	 * Indica o volume de referência menor entre QDC e QDP para apuração de volumes
	 * de penalidade/compromisso contratual.
	 */
	public static final String C_CONSUMO_REFERENCIA_MENOR_QDC_QDP = "C_CONSUMO_REFERENCIA_MENOR_QDC_QDP";

	/**
	 * Indica o volume de referência QDC para apuração de volumes de
	 * penalidade/compromisso contratual.
	 */
	public static final String C_CONSUMO_REFERENCIA_QDC = "C_CONSUMO_REFERENCIA_QDC";

	/**
	 * Indica o volume de referência QDP para apuração de volumes de
	 * penalidade/compromisso contratual.
	 */
	public static final String C_CONSUMO_REFERENCIA_QDP = "C_CONSUMO_REFERENCIA_QDP";

	/** The Constant C_REFERENCIA_QF_MAIOR_QDP_QDR. */
	public static final String C_REFERENCIA_QF_MAIOR_QDP_QDR = "C_REFERENCIA_QF_MAIOR_QDP_QDR";

	/** The Constant C_REFERENCIA_QF_QDR. */
	public static final String C_REFERENCIA_QF_QDR = "C_REFERENCIA_QF_QDR";

	/** The Constant C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDC. */
	public static final String C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDC = "C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDC";

	/** The Constant C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDP_RET. */
	public static final String C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDP_RET = "C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDP_RET";

	/** The Constant C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDC. */
	public static final String C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDC = "C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDC";

	/** The Constant C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDP_RET. */
	public static final String C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDP_RET = "C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDP_RET";

	/** Indica a situação de contrato Ativo. */
	public static final String C_CONTRATO_ATIVO = "C_CONTRATO_ATIVO";

	/** Indica a situação de contrato Em Recuperação. */
	public static final String C_CONTRATO_EM_RECUPERACAO = "C_CONTRATO_EM_RECUPERACAO";

	/** The Constant C_TIPO_MODELO_CONTRATO. */
	public static final String C_TIPO_MODELO_CONTRATO = "C_TIPO_MODELO_CONTRATO";

	/** The Constant MODELO_CONTRATO_TIPO_MODELO. */
	public static final String MODELO_CONTRATO_TIPO_MODELO = "MODELO_CONTRATO_TIPO_MODELO";

	/** The Constant C_TIPO_MODELO_CONTRATO_COMPLEMENTAR. */
	public static final String C_TIPO_MODELO_CONTRATO_COMPLEMENTAR = "C_TIPO_MODELO_CONTRATO_COMPLEMENTAR";

	/** The Constant C_TIPO_MODELO_CONTRATO_PRINCIPAL. */
	public static final String C_TIPO_MODELO_CONTRATO_PRINCIPAL = "C_TIPO_MODELO_CONTRATO_PRINCIPAL";

	/** The Constant ERRO_CONTRATO_COMPLEMENTAR_INCLUSAO_COMPLEMENTAR. */
	public static final String ERRO_CONTRATO_COMPLEMENTAR_INCLUSAO_COMPLEMENTAR = "ERRO_CONTRATO_COMPLEMENTAR_INCLUSAO_COMPLEMENTAR";

	/** Indica a situação de contrato Encerrado. */

	public static final String C_CONTRATO_ENCERRADO = "C_CONTRATO_ENCERRADO";

	/**
	 * Indica o relacionamento cliente X imóvel onde o cliente é o responsável pelo
	 * contrato.
	 */
	public static final String C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO = "C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO";

	/** Indica a base de apuração de penalidade diária. */
	public static final String C_BASE_APURACAO_PENALIDADE_DIARIA = "C_BASE_APURACAO_PENALIDADE_DIARIA";

	/** Indica a base de apuração de penalidade mensal. */
	public static final String C_BASE_APURACAO_PENALIDADE_MENSAL = "C_BASE_APURACAO_PENALIDADE_MENSAL";

	/** Indica a entidade classe Periodicidade dos Juros. */
	public static final String C_ENTIDADE_CLASSE_PERIODICIDADE_JUROS = "C_ENTIDADE_CLASSE_PERIODICIDADE_JUROS";

	/** Indica o status de alçada autorizado. */
	public static final String C_ALCADA_AUTORIZADO = "C_ALCADA_AUTORIZADO";

	/** Indica o status de alçada não autorizado. */
	public static final String C_ALCADA_NAO_AUTORIZADO = "C_ALCADA_NAO_AUTORIZADO";

	/** Indica o status de alçada pendente. */
	public static final String C_ALCADA_PENDENTE = "C_ALCADA_PENDENTE";

	/** Indica a situação de medidor disponível. */
	public static final String C_MEDIDOR_DISPONIVEL = "C_MEDIDOR_DISPONIVEL";

	/** Indica a situação de medidor instalado. */
	public static final String C_MEDIDOR_INSTALADO = "C_MEDIDOR_INSTALADO";

	/** Indica a situação de medidor pronto para instalar. */
	public static final String C_MEDIDOR_PRONTO_INSTALAR = "C_MEDIDOR_PRONTO_INSTALAR";

	/** Indica a operação de bloqueio de medidor. */
	public static final String C_OPERACAO_BLOQUEIO = "C_OPERACAO_BLOQUEIO";

	/** Indica a operação de instalação de medidor. */
	public static final String C_OPERACAO_INSTALACAO = "C_OPERACAO_INSTALACAO";

	/** Indica a operação de mudança de pressão de medidor. */
	public static final String C_OPERACAO_MUDANCA_PRESSAO = "C_OPERACAO_MUDANCA_PRESSAO";

	/** Indica a operação de reativação de medidor. */
	public static final String C_OPERACAO_REATIVACAO = "C_OPERACAO_REATIVACAO";

	/** Indica a operação de retirada de medidor/corretor de vazão. */
	public static final String C_OPERACAO_RETIRADA = "C_OPERACAO_RETIRADA";

	/** Indica a operação de substituição de medidor/corretor de vazão. */
	public static final String C_OPERACAO_SUBSTITUICAO = "C_OPERACAO_SUBSTITUICAO";

	/** Indica o tipo de consumo ajustado. */
	public static final String C_TIPO_CONSUMO_AJUSTADO = "C_TIPO_CONSUMO_AJUSTADO";

	/** Indica o tipo de consumo estimado. */
	public static final String C_TIPO_CONSUMO_ESTIMADO = "C_TIPO_CONSUMO_ESTIMADO";

	/** Indica o tipo de consumo informado. */
	public static final String C_TIPO_CONSUMO_INFORMADO = "C_TIPO_CONSUMO_INFORMADO";

	/** Indica o tipo de consumo médio. */
	public static final String C_TIPO_CONSUMO_MEDIA = "C_TIPO_CONSUMO_MEDIA";

	/** Indica o tipo de consumo mínimo. */
	public static final String C_TIPO_CONSUMO_MINIMO = "C_TIPO_CONSUMO_MINIMO";

	/** Indica o tipo de consumo não apurado. */
	public static final String C_TIPO_CONSUMO_NAO_APURADO = "C_TIPO_CONSUMO_NAO_APURADO";

	/** Indica o tipo de consumo real. */
	public static final String C_TIPO_CONSUMO_REAL = "C_TIPO_CONSUMO_REAL";

	/** Indica o tipo de consumo real atual. */
	public static final String C_TIPO_CONSUMO_REAL_ATUAL = "C_TIPO_CONSUMO_REAL_ATUAL";

	/**
	 * Indica o tipo de consumo real composto pela soma dos volumes dos medidores
	 * atual e anterior.
	 */
	public static final String C_TIPO_CONSUMO_REAL_COMPOSTO = "C_TIPO_CONSUMO_REAL_COMPOSTO";

	/** Indica o tipo de consumo sem consumo. */
	public static final String C_TIPO_CONSUMO_SEM_CONSUMO = "C_TIPO_CONSUMO_SEM_CONSUMO";

	/** Indica a anormalidade consumo alto. */
	public static final String C_ANORMALIDADE_CONSUMO_ALTO = "C_ANORMALIDADE_CONSUMO_ALTO";

	/** Indica a anormalidade consumo baixo. */
	public static final String C_ANORMALIDADE_CONSUMO_BAIXO = "C_ANORMALIDADE_CONSUMO_BAIXO";

	/**
	 * The Constant PARAMETRO_MODULO_MEDICAO_CODIGO_ANORMALIDADE_CONSUMO_ZERO.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_MODULO_MEDICAO_CODIGO_ANORMALIDADE_CONSUMO_ZERO = "CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO";

	/** Indica a anormalidade ciclo de medição incompleto. */
	public static final String C_ANORMALIDADE_CONSUMO_CICLO_INCOMPLETO = "C_ANORMALIDADE_CONSUMO_CICLO_INCOMPLETO";

	/** Indica a anormalidade estouro consumo. */
	public static final String C_ANORMALIDADE_CONSUMO_ESTOURO = "C_ANORMALIDADE_CONSUMO_ESTOURO";

	/** Indica a anormalidade consumo fora de faixa. */
	public static final String C_ANORMALIDADE_CONSUMO_FORA_FAIXA = "C_ANORMALIDADE_CONSUMO_FORA_FAIXA";

	/** Indica a anormalidade medidor da composicao sem medicao. */
	public static final String C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO = "C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO";

	/** Indica a anormalidade consumo informado. */
	public static final String C_ANORMALIDADE_CONSUMO_INFORMADO = "C_ANORMALIDADE_CONSUMO_INFORMADO";

	/** Indica a anormalidade consumo leitura menor que anterior. */
	public static final String C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR = "C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR";

	/**
	 * Indica a anormalidade leitura atual maior que a quantidade de digitos do
	 * medidor.
	 */
	public static final String C_ANORMALIDADE_CONSUMO_LEITURA_MAIOR_QNT_DIGITOS_MEDIDOR =
			"C_ANORMALIDADE_CONSUMO_LEITURA_MAIOR_QNT_DIGITOS_MEDIDOR";

	/**
	 * Indica a anormalidade consumo leitura menor que anterior na substituicao do
	 * medidor.
	 */
	public static final String C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR_SUBSTITUICAO = "C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_"
			+ "QUE_ANTERIOR_SUBSTITUICAO";

	/** Indica a anormalidade leitura menor que projetada. */
	public static final String C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_PROJETADA = "C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_PROJETADA";

	/** Indica a anormalidade leitura não informada. */
	public static final String C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA = "C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA";

	/** Indica a anormalidade substituição de medidor. */
	public static final String C_ANORMALIDADE_CONSUMO_MEDIDOR_SUBSTITUIDO = "C_ANORMALIDADE_CONSUMO_MEDIDOR_SUBSTITUIDO";

	/** Indica a anormalidade ponto de consumo sem contrato ativo. */
	public static final String C_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO = "C_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_"
			+ "SEM_CONTRATO_ATIVO";

	/** The Constant C_ANORMALIDADE_PERIODO_LEITURA_MAIOR. */
	public static final String C_ANORMALIDADE_PERIODO_LEITURA_MAIOR = "C_ANORMALIDADE_PERIODO_LEITURA_MAIOR";
	
	/** The Constant C_ANORMALIDADE_PERIODO_LEITURA_MENOR. */
	public static final String C_ANORMALIDADE_PERIODO_LEITURA_MENOR = "C_ANORMALIDADE_PERIODO_LEITURA_MENOR";
	
	/** The Constant C_CONSUMO_APURADO_NEGATIVO. */
	public static final String C_ANORMALIDADE_CONSUMO_APURADO_NEGATIVO = "C_ANORMALIDADE_CONSUMO_APURADO_NEGATIVO";
	
	/** The Constant C_CONSUMO_APURADO_NULO. */
	public static final String C_ANORMALIDADE_CONSUMO_APURADO_NULO = "C_ANORMALIDADE_CONSUMO_APURADO_NULO";

	/** Indica a anormalidade substituição medidor não informado. */
	public static final String C_ANORMALIDADE_CONSUMO_SUBSTITUICAO_MEDIDOR_NAO_INFORMADO = "C_ANORMALIDADE_CONSUMO_SUBSTITUICAO_"
			+ "MEDIDOR_NAO_INFORMADO";

	/** Indica a anormalidade virada de medidor. */
	public static final String C_ANORMALIDADE_CONSUMO_VIRADA_MEDIDOR = "C_ANORMALIDADE_CONSUMO_VIRADA_MEDIDOR";

	/** The Constant ERRO_NEGOCIO_REMOVER_UNIDADE_ORGANIZACIONAL. */
	public static final String ERRO_NEGOCIO_REMOVER_UNIDADE_ORGANIZACIONAL = "ERRO_NEGOCIO_REMOVER_UNIDADE_ORGANIZACIONAL";

	/**
	 * Indica o percentual padrão de participação do City Gate no fornecimento do
	 * gás.
	 */
	public static final String C_PERCENTUAL_PARTICIPACAO_CITY_GATE = "C_PERCENTUAL_PARTICIPACAO_CITY_GATE";

	/** Indica o código do tipo de medição Diária. */
	public static final String C_MEDICAO_DIARIA = "C_MEDICAO_DIARIA";

	/** Indica o código do tipo de medição Periódica. */
	public static final String C_MEDICAO_PERIODICA = "C_MEDICAO_PERIODICA";

	/** Indica a Situação de Leitura Confirmada. */
	public static final String C_SITUACAO_LEITURA_CONFIRMADA = "C_SITUACAO_LEITURA_CONFIRMADA";

	/** Indica a Situação de Leitura Não Realizada. */
	public static final String C_SITUACAO_LEITURA_NAO_REALIZADA = "C_SITUACAO_LEITURA_NAO_REALIZADA";

	/** Indica a Situação de Leitura Realizada. */
	public static final String C_SITUACAO_LEITURA_REALIZADA = "C_SITUACAO_LEITURA_REALIZADA";

	/** Indica a situação do crédito/débito Cancelado. */
	public static final String C_CREDITO_DEBITO_CANCELADO = "C_CREDITO_DEBITO_CANCELADO";

	/** Indica a situação do crédito/débito Incluído. */
	public static final String C_CREDITO_DEBITO_INCLUIDO = "C_CREDITO_DEBITO_INCLUIDO";

	/** Indica a situação do crédito/débito Nota Fiscal Pendente. */
	public static final String C_CREDITO_DEBITO_NFE_PENDENTE = "C_CREDITO_DEBITO_NFE_PENDENTE";

	/** Indica a situação do crédito/débito Normal. */
	public static final String C_CREDITO_DEBITO_NORMAL = "C_CREDITO_DEBITO_NORMAL";

	/** Indica a situação do crédito/débito Parcelado. */
	public static final String C_CREDITO_DEBITO_PARCELADO = "C_CREDITO_DEBITO_PARCELADO";

	/** Indica a situação do crédito/débito Retificado. */
	public static final String C_CREDITO_DEBITO_RETIFICADO = "C_CREDITO_DEBITO_RETIFICADO";

	/** Indica a situação do crédito/débito Devolvido. */
	public static final String C_CREDITO_DEBITO_DEVOLVIDO = "C_CREDITO_DEBITO_DEVOLVIDO";

	/** Indica o status da Nfe Aguardando Envio. */
	public static final String C_NFE_AGUARDANDO_ENVIO = "C_NFE_AGUARDANDO_ENVIO";

	/** The Constant CLASSE_CHAMADO. */
	public static final String CLASSE_CHAMADO = "br.com.ggas.atendimento.chamado.dominio.Chamado";

	/** Indica o status da Nfe Aguardando Retorno. */
	public static final String C_NFE_AGUARDANDO_RETORNO = "C_NFE_AGUARDANDO_RETORNO";

	/** Indica o status da Nfe Autorizada. */
	public static final String C_NFE_AUTORIZADA = "C_NFE_AUTORIZADA";

	/** Indica o status da Nfe Cancelada. */
	public static final String C_NFE_CANCELADA = "C_NFE_CANCELADA";

	/** Indica o status da Nfe Contingência no envio para sefaz. */
	public static final String C_NFE_CONTINGENCIA_ENVIO_PARA_SEFAZ = "C_NFE_CONTINGENCIA_ENVIO_PARA_SEFAZ";

	/** Indica o status da Nfe Contingência no retorno do envio para a sefaz. */
	public static final String C_NFE_CONTINGENCIA_RETORNO_ENVIO_PARA_SEFAZ = "C_NFE_CONTINGENCIA_RETORNO_ENVIO_PARA_SEFAZ";

	/** IndiIndica o status da Nfe Denegada. */
	public static final String C_NFE_DENEGADA = "C_NFE_DENEGADA";

	/** Indica o status da Nfe Erro na busca do retorno da sefaz. */
	public static final String C_NFE_ERRO_BUSCA_RETORNO_SEFAZ = "C_NFE_ERRO_BUSCA_RETORNO_SEFAZ";

	/** Indica o status da Nfe Erro no Envio para a Sefaz. */
	public static final String C_NFE_ERRO_ENVIO_PARA_SEFAZ = "C_NFE_ERRO_ENVIO_PARA_SEFAZ";

	/** Indica o status da Nfe Erro 4. */
	public static final String C_NFE_ERRO_GERAL = "C_NFE_ERRO_GERAL";

	/** Indica o status da Nfe Erro na montagem do XML. */
	public static final String C_NFE_ERRO_MONTAGEM_XML = "C_NFE_ERRO_MONTAGEM_XML";

	/** Indica o status da Nfe Erro de validação. */
	public static final String C_NFE_ERRO_VALIDACAO = "C_NFE_ERRO_VALIDACAO";

	/** Indica o status da Nfe Não Processada. */
	public static final String C_NFE_NAO_PROCESSADA = "C_NFE_NAO_PROCESSADA";

	/** Indica o status da Nfe Rejeitada. */
	public static final String C_NFE_REJEITADA = "C_NFE_REJEITADA";

	/** Indica alíquota com redução de base de cálculo. */
	public static final String C_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS = "C_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS";

	/**
	 * Indica alíquota com operação Tributável (base de cálculo = valor da operação
	 * (alíquota diferenciada)).
	 */
	public static final String C_ALIQUOTA_DIFERENCIADA_PIS_COFINS = "C_ALIQUOTA_DIFERENCIADA_PIS_COFINS";

	/** Indica alíquota Isenta (ICMS). */
	public static final String C_ALIQUOTA_ISENTA_ICMS = "C_ALIQUOTA_ISENTA_ICMS";

	/** Indica alíquota com operação Isenta da Contribuição (PIS/COFINS). */
	public static final String C_ALIQUOTA_ISENTO_PIS_COFINS = "C_ALIQUOTA_ISENTO_PIS_COFINS";

	/** Indica alíquota não tributada (ICMS). */
	public static final String C_ALIQUOTA_NAO_TRIBUTADA_ICMS = "C_ALIQUOTA_NAO_TRIBUTADA_ICMS";

	/**
	 * Indica alíquota com operação Tributável (base de cálculo = valor da operação
	 * (alíquota normal (cumulativo/não cumulativo))).
	 */
	public static final String C_ALIQUOTA_NORMAL_PIS_COFINS = "C_ALIQUOTA_NORMAL_PIS_COFINS";

	/** Indica alíquota comoperação Sem Incidência da Contribuição (PIS/COFINS). */
	public static final String C_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS = "C_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS";

	/**
	 * Indica alíquota tributada e com cobrança do ICMS por substituição tributária.
	 */
	public static final String C_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_POR_SUBST_TRIBUTARIA = "C_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_"
			+ "POR_SUBST_TRIBUTARIA";

	/** Indica alíquota tributada integralmente. */
	public static final String C_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS = "C_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS";

	/** Indica o tipo de faturamento produto. */
	public static final String C_FATURAMENTO_PRODUTO = "C_FATURAMENTO_PRODUTO";

	/** Indica o tipo de faturamento serviço. */
	public static final String C_FATURAMENTO_SERVICO = "C_FATURAMENTO_SERVICO";

	/** Indica o tipo de operação entrada. */
	public static final String C_TIPO_OPERACAO_ENTRADA = "C_TIPO_OPERACAO_ENTRADA";

	/** Indica o tipo de operação saída. */
	public static final String C_TIPO_OPERACAO_SAIDA = "C_TIPO_OPERACAO_SAIDA";

	/** Indica a situação do crédito/débito Cancelada Por Refaturamento. */
	public static final String C_CREDITO_DEBITO_CANCELADO_POR_REFATURAMENTO = "C_CREDITO_DEBITO_CANCELADO_POR_REFATURAMENTO";

	/** Código do credito de origem por compensacao de volume. */
	public static final String C_CREDITO_ORIGEM_COMPENSACAO_DE_VOLUME = "C_CREDITO_ORIGEM_COMPENSACAO_DE_VOLUME";

	/** Indica o código da origem do crédito como sendo pagamento em duplicidade. */
	public static final String C_CREDITO_ORIGEM_DOCUMENTOS_PAGOS_DUPLICIDADE = "C_CREDITO_ORIGEM_DOCUMENTOS_PAGOS_DUPLICIDADE";

	/**
	 * Indica faturamento agrupado pelo valor calculado individualmente por ponto de
	 * consumo.
	 */
	public static final String C_AGRUPAMENTO_POR_VALOR = "C_AGRUPAMENTO_POR_VALOR";

	/** Indica faturamento agrupado pela soma dos volumes dos pontos de consumo. */
	public static final String C_AGRUPAMENTO_POR_VOLUME = "C_AGRUPAMENTO_POR_VOLUME";

	/** Indica o item da fatura Gás. */
	public static final String C_GAS = "C_GAS";

	/** Indica o item da fatura Margem de distribuição. */
	public static final String C_MARGEM_DISTRIBUICAO = "C_MARGEM_DISTRIBUICAO";

	/** The Constant C_LEITURA_MOVIMENTO_EMITIR. */
	public static final String C_LEITURA_MOVIMENTO_EMITIR = "C_LEITURA_MOVIMENTO_EMITIR";

	/** Indica o item da fatura Transporte. */
	public static final String C_TRANSPORTE = "C_TRANSPORTE";

	/** Indica a opção de vencimento dia fixo. */
	public static final String C_VENCIMENTO_DIA_FIXO = "C_VENCIMENTO_DIA_FIXO";

	/** Indica a opção de vencimento dia fixo Extendido. */
	public static final String C_VENCIMENTO_DIA_FIXO_ESTENDIDO = "C_VENCIMENTO_DIA_FIXO_ESTENDIDO";

	/** Indica a opcao para calcular a data de vencimento apos a leitura. */
	public static final String C_VENCIMENTO_APOS_LEITURA = "C_VENCIMENTO_APOS_LEITURA";

	/** Indica a opcao para calcular a data de vencimento apos o faturamento. */
	public static final String C_VENCIMENTO_APOS_FATURAMENTO = "C_VENCIMENTO_APOS_FATURAMENTO";

	/** Indica a periodicidade de cobrança do débito no Faturamento. */
	public static final String C_COBRANCA_A_CADA_FATURAMENTO = "C_COBRANCA_A_CADA_FATURAMENTO";

	/** Indica a periodicidade de cobrança do débito mensal. */
	public static final String C_COBRANCA_MENSAL = "C_COBRANCA_MENSAL";

	/** Indica o tipo de documento Fatura. */
	public static final String C_TIPO_DOCUMENTO_FATURA = "C_TIPO_DOCUMENTO_FATURA";

	/** Indica o tipo de documento Crédito. */
	public static final String C_TIPO_DOCUMENTO_NOTA_CREDITO = "C_TIPO_DOCUMENTO_NOTA_CREDITO";

	/** The Constant C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE. */
	public static final String C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE = "C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE";

	/** The Constant C_STATUS_PONTO_CONSUMO_BLOQUEADO. */
	public static final String C_STATUS_PONTO_CONSUMO_BLOQUEADO = "C_STATUS_PONTO_CONSUMO_BLOQUEADO";

	/** The Constant C_STATUS_PONTO_CONSUMO_BLOQUEADO_TECNICAMENTE. */
	public static final String C_STATUS_PONTO_CONSUMO_BLOQUEADO_TECNICAMENTE = "C_STATUS_PONTO_CONSUMO_BLOQUEADO_TECNICAMENTE";

	/** The Constant C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO. */
	public static final String C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO = "C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO";

	/** The Constant C_STATUS_PONTO_CONSUMO_SUPRIMIDO_PARCIAL. */
	public static final String C_STATUS_PONTO_CONSUMO_SUPRIMIDO_PARCIAL = "C_STATUS_PONTO_CONSUMO_SUPRIMIDO_PARCIAL";

	/** The Constant C_STATUS_PONTO_CONSUMO_SUPRIMIDO_TOTAL. */
	public static final String C_STATUS_PONTO_CONSUMO_SUPRIMIDO_TOTAL = "C_STATUS_PONTO_CONSUMO_SUPRIMIDO_TOTAL";

	/** The Constant C_STATUS_PONTO_CONSUMO_SUSPENSO. */
	public static final String C_STATUS_PONTO_CONSUMO_SUSPENSO = "C_STATUS_PONTO_CONSUMO_SUSPENSO";

	/** The Constant C_STATUS_PONTO_CONSUMO_ATIVO. */
	public static final String C_STATUS_PONTO_CONSUMO_ATIVO = "C_STATUS_PONTO_CONSUMO_ATIVO";

	/** The Constant C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO. */
	public static final String C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO = "C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO";

	/** Indica o tipo de documento Débito. */
	public static final String C_TIPO_DOCUMENTO_NOTA_DEBITO = "C_TIPO_DOCUMENTO_NOTA_DEBITO";

	/** Indica o tipo de documento Débito Penalidade. */
	public static final String C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE = "C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE";

	/** Indica a entidade classe Item da Fatura. */
	public static final String C_ENTIDADE_CLASSE_ITEM_FATURA = "C_ENTIDADE_CLASSE_ITEM_FATURA";

	/** Indica a entidade classe motivo de inclusão. */
	public static final String C_ENTIDADE_CLASSE_MOTIVO_INCLUSAO = "C_ENTIDADE_CLASSE_MOTIVO_INCLUSAO";

	/** Indica o tipo percentual. */
	public static final String C_PERCENTUAL = "C_PERCENTUAL";

	/** Indica o tipo valor. */
	public static final String C_VALOR = "C_VALOR";

	/** indica o tributo COFINS. */
	public static final String C_COFINS = "C_COFINS";

	/** indica o tributo ICMS. */
	public static final String C_ICMS = "C_ICMS";

	/** Indica o tributo PIS. */
	public static final String C_PIS = "C_PIS";

	/** The Constant C_IPI. */
	public static final String C_IPI = "C_IPI";

	/** Indica o tipo do item Débito. */
	public static final String C_DEBITO = "C_DEBITO";

	/** Indica o tipo do item Crédito. */
	public static final String C_CREDITO = "C_CREDITO";

	/** Indica o tipo do item Débito Penalidade. */

	public static final String C_DEBITO_PENALIDADE = "C_DEBITO_PENALIDADE";

	/** Indica o motivo de movimentação de entrada do medidor. */
	public static final String C_MOTIVO_MOVIMENTACAO_MEDIDOR_ENTRADA = "C_MOTIVO_MOVIMENTACAO_MEDIDOR_ENTRADA";

	/** Indica o motivo de movimentação de transferência do medidor. */
	public static final String C_MOTIVO_MOVIMENTACAO_MEDIDOR_TRANSFERENCIA = "C_MOTIVO_MOVIMENTACAO_MEDIDOR_TRANSFERENCIA";

	/** Indica o motivo de devolução. */
	public static final String C_MOTIVO_DEVOLUCAO = "C_MOTIVO_DEVOLUCAO";

	/** The Constant C_INTEGRACAO_SITUACAO_NAO_PROCESSADO. */
	public static final String C_INTEGRACAO_SITUACAO_NAO_PROCESSADO = "C_INTEGRACAO_SITUACAO_NAO_PROCESSADO";

	/** The Constant C_INTEGRACAO_SITUACAO_PROCESSADO. */
	public static final String C_INTEGRACAO_SITUACAO_PROCESSADO = "C_INTEGRACAO_SITUACAO_PROCESSADO";

	/** The Constant C_INTEGRACAO_SISTEMA_GGAS. */
	public static final String C_INTEGRACAO_SISTEMA_GGAS = "C_INTEGRACAO_SISTEMA_GGAS";

	/** The Constant C_INTEGRACAO_SITUACAO_ERRO. */
	public static final String C_INTEGRACAO_SITUACAO_ERRO = "C_INTEGRACAO_SITUACAO_ERRO";

	/** The Constant C_INTEGRACAO_CADASTRO_BENS. */
	public static final String C_INTEGRACAO_CADASTRO_BENS = "C_INTEGRACAO_CADASTRO_BENS";

	/** The Constant C_INTEGRACAO_MOVIMENTACAO_BENS. */
	public static final String C_INTEGRACAO_MOVIMENTACAO_BENS = "C_INTEGRACAO_MOVIMENTACAO_BENS";

	/** The Constant C_INTEGRACAO_CADASTRO_CLIENTES. */
	public static final String C_INTEGRACAO_CADASTRO_CLIENTES = "C_INTEGRACAO_CADASTRO_CLIENTES";

	/** The Constant C_INTEGRACAO_CONTRATOS. */
	public static final String C_INTEGRACAO_CONTRATOS = "C_INTEGRACAO_CONTRATOS";

	/** The Constant C_ERRO_NEGOCIO_BUSCAR_VALOR_METODO. */
	public static final String C_ERRO_NEGOCIO_BUSCAR_VALOR_METODO = "C_ERRO_NEGOCIO_BUSCAR_VALOR_METODO";

	/** The Constant C_CLIENTE_SITUACAO_INAPTO_POR_DENEGACAO. */
	public static final String C_CLIENTE_SITUACAO_INAPTO_POR_DENEGACAO = "C_CLIENTE_SITUACAO_INAPTO_POR_DENEGACAO";

	/** The Constant C_INTEGRACAO_OPERACAO_INCLUSAO. */
	public static final String C_INTEGRACAO_OPERACAO_INCLUSAO = "C_INTEGRACAO_OPERACAO_INCLUSAO";

	/** The Constant C_OPERACAO_APROVACAO_CONTRATO. */
	public static final String C_OPERACAO_APROVACAO_CONTRATO = "C_OPERACAO_APROVACAO_CONTRATO";

	/** The Constant C_INTEGRACAO_OPERACAO_ALTERACAO. */
	public static final String C_INTEGRACAO_OPERACAO_ALTERACAO = "C_INTEGRACAO_OPERACAO_ALTERACAO";

	/** The Constant C_INTEGRACAO_OPERACAO_EXCLUSAO. */
	public static final String C_INTEGRACAO_OPERACAO_EXCLUSAO = "C_INTEGRACAO_OPERACAO_EXCLUSAO";

	/** The Constant C_INTEGRACAO_OPERACAO_CANCELAMENTO. */
	public static final String C_INTEGRACAO_OPERACAO_CANCELAMENTO = "C_INTEGRACAO_OPERACAO_CANCELAMENTO";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_NORMAL. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_NORMAL = "C_INTEGRACAO_NOTA_FISCAL_NORMAL";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_DEVOLUCAO. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_DEVOLUCAO = "C_INTEGRACAO_NOTA_FISCAL_DEVOLUCAO";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_ENTRADA. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_ENTRADA = "C_INTEGRACAO_NOTA_FISCAL_ENTRADA";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_SAIDA. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_SAIDA = "C_INTEGRACAO_NOTA_FISCAL_SAIDA";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_PRODUTO. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_PRODUTO = "C_INTEGRACAO_NOTA_FISCAL_PRODUTO";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_SERVICO. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_SERVICO = "C_INTEGRACAO_NOTA_FISCAL_SERVICO";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL. */
	public static final String C_INTEGRACAO_NOTA_FISCAL = "C_INTEGRACAO_NOTA_FISCAL";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_TERCEIROS. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_TERCEIROS = "C_INTEGRACAO_NOTA_FISCAL_TERCEIROS";

	/** The Constant C_INTEGRACAO_NOTA_FISCAL_PROPRIO. */
	public static final String C_INTEGRACAO_NOTA_FISCAL_PROPRIO = "C_INTEGRACAO_NOTA_FISCAL_PROPRIO";

	/** The Constant C_INTEGRACAO_PROC_TITULO_BAIXA. */
	public static final String C_INTEGRACAO_PROC_TITULO_BAIXA = "C_INTEGRACAO_PROC_TITULO_BAIXA";

	/** The Constant C_CODIGO_TABELA_FATURAMENTO. */
	public static final String C_CODIGO_TABELA_FATURAMENTO = "C_CODIGO_TABELA_FATURAMENTO";

	/** The Constant C_CODIGO_TABELA_RECEBIMENTO. */
	public static final String C_CODIGO_TABELA_RECEBIMENTO = "C_CODIGO_TABELA_RECEBIMENTO";

	/** The Constant C_PROV_DEV_DUV_MOTIVO_BAIXA_CANCELAMENTO. */
	public static final String C_PROV_DEV_DUV_MOTIVO_BAIXA_CANCELAMENTO = "C_PROV_DEV_DUV_MOTIVO_BAIXA_CANCELAMENTO";

	/** The Constant C_PROV_DEV_DUV_MOTIVO_BAIXA_PARCELAMENTO. */
	public static final String C_PROV_DEV_DUV_MOTIVO_BAIXA_PARCELAMENTO = "C_PROV_DEV_DUV_MOTIVO_BAIXA_PARCELAMENTO";

	/** The Constant C_PROV_DEV_DUV_MOTIVO_BAIXA_RETIFICACAO. */
	public static final String C_PROV_DEV_DUV_MOTIVO_BAIXA_RETIFICACAO = "C_PROV_DEV_DUV_MOTIVO_BAIXA_RETIFICACAO";

	/** The Constant C_PROV_DEV_DUV_MOTIVO_BAIXA_CONCILIACAO. */
	public static final String C_PROV_DEV_DUV_MOTIVO_BAIXA_CONCILIACAO = "C_PROV_DEV_DUV_MOTIVO_BAIXA_CONCILIACAO";

	/** The Constant C_PROV_DEV_DUV_MOTIVO_BAIXA_RECEBIMENTO. */
	public static final String C_PROV_DEV_DUV_MOTIVO_BAIXA_RECEBIMENTO = "C_PROV_DEV_DUV_MOTIVO_BAIXA_RECEBIMENTO";

	/** The Constant C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO. */
	public static final String C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO = "C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO";

	/** The Constant C_SIM. */
	public static final String C_SIM = "C_SIM";

	/** The Constant C_NAO. */
	public static final String C_NAO = "C_NAO";

	/** The Constant C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO. */
	public static final String C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO = "C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO";

	/** The Constant C_ATIVIDADE_SISTEMA_FATURAR_GRUPO. */
	public static final String C_ATIVIDADE_SISTEMA_FATURAR_GRUPO = "C_ATIVIDADE_SISTEMA_FATURAR_GRUPO";

	/** The Constant C_ATIVIDADE_SISTEMA_EMITIR_FATURA. */
	public static final String C_ATIVIDADE_SISTEMA_EMITIR_FATURA = "C_ATIVIDADE_SISTEMA_EMITIR_FATURA";

	/** The Constant C_ATIVIDADE_SISTEMA_ENCERRAR_CICLO_FATURAMENTO. */
	public static final String C_ATIVIDADE_SISTEMA_ENCERRAR_CICLO_FATURAMENTO = "C_ATIVIDADE_SISTEMA_ENCERRAR_CICLO_FATURAMENTO";

	/** The Constant C_ATIVIDADE_SISTEMA_ENCERRAR_REFERENCIA_CONTABIL. */
	public static final String C_ATIVIDADE_SISTEMA_ENCERRAR_REFERENCIA_CONTABIL = "C_ATIVIDADE_SISTEMA_ENCERRAR_REFERENCIA_CONTABIL";

	/** The Constant C_REFERENCIA_QF_PARADA_PROGRAMADA. */
	public static final String C_REFERENCIA_QF_PARADA_PROGRAMADA = "C_REFERENCIA_QF_PARADA_PROGRAMADA";

	/**
	 * Revisar no código em geral a utilização das constantes abaixo.
	 */
	public static final String TABELA_CLIENTE_SITUACAO = "C_TABELA_CLIENTE_SITUACAO";

	/** The Constant TABELA_CLIENTE_TIPO. */
	public static final String TABELA_CLIENTE_TIPO = "C_TABELA_CLIENTE_TIPO";

	/** The Constant TABELA_ORGAO_EXPEDIDOR_RG. */
	public static final String TABELA_ORGAO_EXPEDIDOR_RG = "C_TABELA_ORGAO_EXPEDIDOR_RG";

	/** The Constant TABELA_UNIDADE_FEDERACAO. */
	public static final String TABELA_UNIDADE_FEDERACAO = "C_TABELA_UNIDADE_FEDERACAO";

	/** The Constant TABELA_NACIONALIDADE. */
	public static final String TABELA_NACIONALIDADE = "C_TABELA_NACIONALIDADE";

	/** The Constant TABELA_PROFISSAO. */
	public static final String TABELA_PROFISSAO = "C_TABELA_PROFISSAO";

	/** The Constant TABELA_PESSOA_SEXO. */
	public static final String TABELA_PESSOA_SEXO = "C_TABELA_PESSOA_SEXO";

	/** The Constant TABELA_RENDA_FAMILIAR_FAIXA. */
	public static final String TABELA_RENDA_FAMILIAR_FAIXA = "C_TABELA_RENDA_FAMILIAR_FAIXA";

	/** The Constant TABELA_ATIVIDADE_ECONOMICA. */
	public static final String TABELA_ATIVIDADE_ECONOMICA = "C_TABELA_ATIVIDADE_ECONOMICA";

	/** The Constant TABELA_ENDERECO_TIPO. */
	public static final String TABELA_ENDERECO_TIPO = "C_TABELA_ENDERECO_TIPO";

	/** The Constant C_INTEGRACAO_TITULOS. */
	public static final String C_INTEGRACAO_TITULOS = "C_INTEGRACAO_TITULOS";

	/** The Constant TABELA_FONE_TIPO. */
	public static final String TABELA_FONE_TIPO = "C_TABELA_FONE_TIPO";

	/** The Constant TABELA_CONTATO_TIPO. */
	public static final String TABELA_CONTATO_TIPO = "C_TABELA_CONTATO_TIPO";

	/** The Constant C_ENTIDADE_CLASSE_CLASSE_PRESSAO_FORNECIMENTO. */
	public static final String C_ENTIDADE_CLASSE_CLASSE_PRESSAO_FORNECIMENTO = "C_ENTIDADE_CLASSE_CLASSE_PRESSAO_FORNECIMENTO";

	/** The Constant C_FORMA_EMISSAO_NFE_CONTINGENCIA. */
	public static final String C_FORMA_EMISSAO_NFE_CONTINGENCIA = "C_FORMA_EMISSAO_NFE_CONTINGENCIA";

	/** The Constant C_FORMA_EMISSAO_NFE_NORMAL. */
	public static final String C_FORMA_EMISSAO_NFE_NORMAL = "C_FORMA_EMISSAO_NFE_NORMAL";

	/** The Constant C_INTEGRACAO_LANCAMENTOS_CONTABEIS. */
	public static final String C_INTEGRACAO_LANCAMENTOS_CONTABEIS = "C_INTEGRACAO_LANCAMENTOS_CONTABEIS";

	/** The Constant C_INTEGRACAO_TITULO_BAIXA_PARCIAL. */
	public static final String C_INTEGRACAO_TITULO_BAIXA_PARCIAL = "C_INTEGRACAO_TITULO_BAIXA_PARCIAL";

	/** The Constant C_INTEGRACAO_TITULO_BAIXA_TOTAL. */
	public static final String C_INTEGRACAO_TITULO_BAIXA_TOTAL = "C_INTEGRACAO_TITULO_BAIXA_TOTAL";

	/** The Constant C_INTEGRACAO_TITULO_BAIXA_NAO_IDENTIFICADA. */
	public static final String C_INTEGRACAO_TITULO_BAIXA_NAO_IDENTIFICADA = "C_INTEGRACAO_TITULO_BAIXA_NAO_IDENTIFICADA";

	/** The Constant C_TIPO_LEITURA_ELETROCORRETOR. */
	public static final String C_TIPO_LEITURA_ELETROCORRETOR = "C_TIPO_LEITURA_ELETROCORRETOR";

	/** The Constant C_TIPO_LEITURA_PLANILHA. */
	public static final String C_TIPO_LEITURA_PLANILHA = "C_TIPO_LEITURA_PLANILHA";

	/** The Constant C_TIPO_MODALIDADE_USO. */
	public static final String C_TIPO_MODALIDADE_USO = "C_TIPO_MODALIDADE_USO";
	
	/** The Constant C_URL_IMAGEM_LOGO_ALGAS. */
	public static final String C_URL_IMAGEM_LOGO_ALGAS = "C_URL_IMAGEM_LOGO_ALGAS";
	
	/** The Constant C_URL_IMAGEM_INFORMACAO_PUBLICA. */
	public static final String C_URL_IMAGEM_INFORMACAO_PUBLICA = "C_URL_IMAGEM_INFORMACAO_PUBLICA";
	
	/** The Constant ERRO_CLIENTE_INTEGRACAO_INEXISTENTE. */
	public static final String ERRO_CLIENTE_INTEGRACAO_INEXISTENTE = "ERRO_CLIENTE_INTEGRACAO_INEXISTENTE";

	/** The Constant ERRO_CONTRATO_INTEGRACAO_INEXISTENTE. */
	public static final String ERRO_CONTRATO_INTEGRACAO_INEXISTENTE = "ERRO_CONTRATO_INTEGRACAO_INEXISTENTE";

	/** The Constant ERRO_JA_EXISTEM_REGISTROS_INTEGRADOS. */
	public static final String ERRO_JA_EXISTEM_REGISTROS_INTEGRADOS = "ERRO_JA_EXISTEM_REGISTROS_INTEGRADOS";

	/** The Constant ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA. */
	public static final String ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA = "ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA";

	/** The Constant ERRO_INTEGRACAO_NOTA_FISCAL_NAO_INTEGRADA. */
	public static final String ERRO_INTEGRACAO_NOTA_FISCAL_NAO_INTEGRADA = "ERRO_INTEGRACAO_NOTA_FISCAL_NAO_INTEGRADA";

	/** The Constant ERRO_INTEGRACAO_TITULO_PAGAR_INEXISTENTE. */
	public static final String ERRO_INTEGRACAO_TITULO_PAGAR_INEXISTENTE = "ERRO_INTEGRACAO_TITULO_PAGAR_INEXISTENTE";

	/** The Constant ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE. */
	public static final String ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE = "ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE";

	/** The Constant ERRO_LIMITE_FAVORITOS. */
	public static final String ERRO_LIMITE_FAVORITOS = "Limite de Favoritos Alcançado.";

	/** The Constant ERRO_LIMITE_FAVORITOS_DUPLICADO. */
	public static final String ERRO_LIMITE_FAVORITOS_DUPLICADO = "Já existe um favorito para este item de menu.";

	/** The Constant ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE = "ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_QUANTIDADE_MENOR_MASCARA. */
	public static final String ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_QUANTIDADE_MENOR_MASCARA = "ERRO_NEGOCIO_NUMERO_QUANTIDADE_MENOR_MASCARA";

	/** ERRO_NEGOCIO_DURACAO_ULTRAPASSANDO_DATA_INICIO. */
	public static final String ERRO_NEGOCIO_DURACAO_ULTRAPASSANDO_DATA_INICIO = "ERRO_NEGOCIO_DURACAO_ULTRAPASSANDO_DATA_INICIO";

	/** The Constant ERRO_NEGOCIO_HISTORICO_CONSUMO_PTZ. */
	public static final String ERRO_NEGOCIO_HISTORICO_CONSUMO_PTZ = "ERRO_NEGOCIO_HISTORICO_CONSUMO_PTZ";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_CREDITO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_CREDITO = "ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_"
			+ "CONTABIL_CONTA_CREDITO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_DEBITO. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_DEBITO = "ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_"
			+ "CONTABIL_CONTA_DEBITO";

	/** The Constant ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_VALOR. */
	public static final String ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_VALOR = "ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_VALOR";

	/** ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_GRUPO_FATURAMENTO. */
	public static final String ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_GRUPO_FATURAMENTO = "ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_"
			+ "ANO_GRUPO_FATURAMENTO";

	/** The Constant ERRO_NEGOCIO_RELACIONAMENTO_PRINCIPAL_EXISTENTE. */
	public static final String ERRO_NEGOCIO_RELACIONAMENTO_PRINCIPAL_EXISTENTE = "ERRO_NEGOCIO_RELACIONAMENTO_PRINCIPAL_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_REMOVER_CONTA_CONTABIL. */
	public static final String ERRO_NEGOCIO_REMOVER_CONTA_CONTABIL = "ERRO_NEGOCIO_REMOVER_CONTA_CONTABIL";

	/** The Constant ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_EXISTE. */
	public static final String ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_EXISTE = "ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_EXISTE";

	/** The Constant ERRO_SEGMENTO_CONTA_CONTABIL_INFORMADA. */
	public static final String ERRO_SEGMENTO_CONTA_CONTABIL_INFORMADA = "ERRO_SEGMENTO_CONTA_CONTABIL_INFORMADA";

	/** The Constant ERRO_PONTO_CONSUMO_JA_EXISTE. */
	public static final String ERRO_PONTO_CONSUMO_JA_EXISTE = "ERRO_PONTO_CONSUMO_JA_EXISTE";

	/** The Constant ERRO_REMOVER_SEGMENTO_JA_UTILIZADO. */
	public static final String ERRO_REMOVER_SEGMENTO_JA_UTILIZADO = "ERRO_REMOVER_SEGMENTO_JA_UTILIZADO";

	/** The Constant ERRO_TABELA_NAO_ENCONTRADA. */
	public static final String ERRO_TABELA_NAO_ENCONTRADA = "ERRO_TABELA_NAO_ENCONTRADA";

	/** The Constant P_LIMITE_INTEGRACAO_CONTATO_CLIENTE. */
	public static final String P_LIMITE_INTEGRACAO_CONTATO_CLIENTE = "P_LIMITE_INTEGRACAO_CONTATO_CLIENTE";

	/** The Constant P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE. */
	public static final String P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE = "P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE";

	/** The Constant P_LIMITE_INTEGRACAO_TELEFONE_CLIENTE. */
	public static final String P_LIMITE_INTEGRACAO_TELEFONE_CLIENTE = "P_LIMITE_INTEGRACAO_TELEFONE_CLIENTE";

	/** The Constant ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXCEDENTE. */
	public static final String ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXCEDENTE = "ERRO_NEGOCIO_EVENTO_COMERCIAL_LANCAMENTO_EXCEDENTE";

	/**
	 * The Constant PARAMETRO_CODIGO_STATUS_PENDENTE.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_CODIGO_STATUS_PENDENTE = "CODIGO_STATUS_PENDENTE";

	/** The Constant CODIGO_TIPO_OPERACAO_SAIDA. */
	public static final String CODIGO_TIPO_OPERACAO_SAIDA = "CODIGO_TIPO_OPERACAO_SAIDA";

	/** The Constant CODIGO_TIPO_OPERACAO_ENTRADA. */
	public static final String CODIGO_TIPO_OPERACAO_ENTRADA = "CODIGO_TIPO_OPERACAO_ENTRADA";

	/** The Constant TIPO_CREDITO. */
	public static final String TIPO_CREDITO = "TIPO_CREDITO";

	/**
	 * The Constant PARAMETRO_CODIGO_PERIODICIDADE_COBRANCA_MENSAL.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_CODIGO_PERIODICIDADE_COBRANCA_MENSAL = "CODIGO_PERIODICIDADE_COBRANCA_MENSAL";

	/**
	 * The Constant PARAMETRO_TRIBUTO_ICMS.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_TRIBUTO_ICMS = "TRIBUTO_ICMS";

	/**
	 * The Constant PARAMETRO_TRIBUTO_PIS.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_TRIBUTO_PIS = "TRIBUTO_PIS";

	/**
	 * The Constant PARAMETRO_TRIBUTO_COFINS.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_TRIBUTO_COFINS = "TRIBUTO_COFINS";

	/**
	 * The Constant PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PENDENTE.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PENDENTE = "CODIGO_SITUACAO_PAGAMENTO_PENDENTE";

	/**
	 * The Constant PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PARCIALMENTE_PAGO.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PARCIALMENTE_PAGO = "CODIGO_SITUACAO_PAGAMENTO_PARCIALMENTE_PAGO";

	/** The Constant CAMPO_MODALIDADE_SELECIONADO. */
	public static final String CAMPO_MODALIDADE_SELECIONADO = "O campo modalidade precisa ser selecionado";

	/** The Constant CAMPO_MODALIDADE_OBRIGATORIO. */
	public static final String CAMPO_MODALIDADE_OBRIGATORIO = "O campo modalidade precisa ser Obrigatório";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DESCRICAO. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DESCRICAO = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DESCRICAO";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_VOLUME_REFERENCIA. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_VOLUME_REFERENCIA = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_VOLUME_REFERENCIA";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DATA_VENCIMENTO. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DATA_VENCIMENTO = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DATA_VENCIMENTO";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_RETIRADA_MENOR. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_RETIRADA_MENOR = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_RETIRADA_MENOR";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_QDC_CONTRATO. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_QDC_CONTRATO = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_QDC_CONTRATO";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_TAKE_OR_PAY. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_TAKE_OR_PAY = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_TAKE_OR_PAY";

	/** The Constant ERRO_MODELO_ATRIBUTO_OBRIGATORIO_PERCENTUAL_QNR. */
	public static final String ERRO_MODELO_ATRIBUTO_OBRIGATORIO_PERCENTUAL_QNR = "ERRO_MODELO_ATRIBUTO_OBRIGATORIO_PERCENTUAL_QNR";
	
	/** The Constant ERRO_NAO_POSSIVEL_MIGRAR_CONTRATO_SITUACAO_ENCERRADO_RESCINDIDO */
	public static final String ERRO_NAO_POSSIVEL_MIGRAR_CONTRATO_SITUACAO_ENCERRADO_RESCINDIDO = "ERRO_NAO_POSSIVEL_MIGRAR_CONTRATO_SITUACAO_ENCERRADO_RESCINDIDO";
	
	/** The Constant CODIGO_SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO. */
	public static final String CODIGO_SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO = "CODIGO_SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO";

	/** The Constant DIRETORIO_ARQUIVOS_FATURA. */
	public static final String DIRETORIO_ARQUIVOS_FATURA = "DIRETORIO_ARQUIVOS_FATURA";

	/** The Constant LANCAMENTO_CONTABIL_NOME_CONTA_CREDITO. */
	public static final String LANCAMENTO_CONTABIL_NOME_CONTA_CREDITO = "Nome da Conta Crédito";

	/** The Constant LANCAMENTO_CONTABIL_CONTA_CREDITO. */
	public static final String LANCAMENTO_CONTABIL_CONTA_CREDITO = "Conta Crédito";

	/** The Constant LANCAMENTO_CONTABIL_NOME_CONTA_DEBITO. */
	public static final String LANCAMENTO_CONTABIL_NOME_CONTA_DEBITO = "Nome da Conta Débito";

	/** The Constant LANCAMENTO_CONTABIL_CONTA_DEBITO. */
	public static final String LANCAMENTO_CONTABIL_CONTA_DEBITO = "Conta Débito";

	/** The Constant LANCAMENTO_CONTABIL_VALOR. */
	public static final String LANCAMENTO_CONTABIL_VALOR = "Valor do Lançamento Contábil";

	/** The Constant PARAMETRO_MODULO_CONTABILIDADE_MASCARA_NUMERO_CONTA. */
	public static final String PARAMETRO_MODULO_CONTABILIDADE_MASCARA_NUMERO_CONTA = "MASCARA_NUMERO_CONTA";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NAO_TRIBUTADA_ICMS. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NAO_TRIBUTADA_ICMS = "ALIQUOTA_NAO_TRIBUTADA_ICMS";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTA_ICMS. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTA_ICMS = "ALIQUOTA_ISENTA_ICMS";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS = "ALIQUOTA_COM_REDUCAO_"
			+ "BASE_CALCULO_ICMS";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_POR_SUBST_TRIBUTARIA.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_POR_SUBST_TRIBUTARIA = "ALIQUOTA_TRIBUTADA_"
			+ "COBRANCA_ICMS_POR_SUBST_TRIBUTARIA";

	/**
	 * The Constant
	 * PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS = "ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS";

	/**
	 * The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_DIFERENCIADA_PIS_COFINS.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_DIFERENCIADA_PIS_COFINS = "ALIQUOTA_DIFERENCIADA_PIS_COFINS";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTO_PIS_COFINS. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTO_PIS_COFINS = "ALIQUOTA_ISENTO_PIS_COFINS";

	/** The Constant ERRO_CODIGO_LEGADO_EM_USO. */
	public static final String ERRO_CODIGO_LEGADO_EM_USO = "ERRO_CODIGO_LEGADO_EM_USO";

	/** The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NORMAL_PIS_COFINS. */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NORMAL_PIS_COFINS = "ALIQUOTA_NORMAL_PIS_COFINS";

	/**
	 * The Constant PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS.
	 */
	public static final String PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS = "ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS";

	/** The Constant PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1. */
	public static final String PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1 = "SUPERVISORIO_FATORZ_VALOR_FIXO_1";

	/**
	 * The Constant PARAMETRO_TIPO_DOCUMENTO_FATURA.
	 *
	 * @deprecated
	 */
	@Deprecated
	public static final String PARAMETRO_TIPO_DOCUMENTO_FATURA = "TIPO_DOCUMENTO_FATURA";

	/** The Constant IMOVEL. */
	public static final String IMOVEL = "br.com.ggas.cadastro.imovel.impl.ImovelImpl";

	/** PARAMETRO_EXIGE_MATRICULA_FUNCIONARIO. */
	public static final String PARAMETRO_EXIGE_MATRICULA_FUNCIONARIO = "EXIGE_MATRICULA_FUNCIONARIO";

	/** The Constant C_VENCIMENTO_DIA_UTIL. */
	public static final String C_VENCIMENTO_DIA_UTIL = "C_VENCIMENTO_DIA_UTIL";

	/** The Constant C_VENCIMENTO_DIAS_CORRIDOS. */
	public static final String C_VENCIMENTO_DIAS_CORRIDOS = "C_VENCIMENTO_DIAS_CORRIDOS";

	/** ERRO_NEGOCIO_MATERIAL. */
	public static final String ERRO_NEGOCIO_CODIGO_MATERIAL_EXISTENTE = "ERRO_NEGOCIO_CODIGO_MATERIAL_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_MATERIAL_EXISTENTE. */
	public static final String ERRO_NEGOCIO_MATERIAL_EXISTENTE = "ERRO_NEGOCIO_MATERIAL_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_MATERIAL_RELACIONADO. */
	public static final String ERRO_NEGOCIO_MATERIAL_RELACIONADO = "ERRO_NEGOCIO_MATERIAL_RELACIONADO";

	/** The Constant DESCRICAO_MATERIAL. */
	public static final String DESCRICAO_MATERIAL = "Descrição do Material";

	/** The Constant UNIDADE_MEDIDA. */
	public static final String UNIDADE_MEDIDA = "Unidade de Medida";

	/** ERRO_NEGOCIO_EQUIPE. */
	public static final String ERRO_NEGOCIO_EQUIPE_COMPONENTE_RESPONSAVEL = "ERRO_NEGOCIO_EQUIPE_COMPONENTE_RESPONSAVEL";

	/** The Constant ERRO_NEGOCIO_EQUIPE_EXISTENTE. */
	public static final String ERRO_NEGOCIO_EQUIPE_EXISTENTE = "ERRO_NEGOCIO_EQUIPE_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_EQUIPE_RELACIONADA. */
	public static final String ERRO_NEGOCIO_EQUIPE_RELACIONADA = "ERRO_NEGOCIO_EQUIPE_RELACIONADA";

	/** The Constant ERRO_NEGOCIO_EQUIPE_RESPONSAVEL. */
	public static final String ERRO_NEGOCIO_EQUIPE_RESPONSAVEL = "ERRO_NEGOCIO_EQUIPE_RESPONSAVEL";

	/** The Constant ERRO_NEGOCIO_EQUIPE_SEM_COMPONENTES. */
	public static final String ERRO_NEGOCIO_EQUIPE_SEM_COMPONENTES = "ERRO_NEGOCIO_EQUIPE_SEM_COMPONENTES";

	/** The Constant DESCRICAO_EQUIPE. */
	public static final String DESCRICAO_EQUIPE = "Descrição da Equipe";

	/** The Constant ERRO_NEGOCIO_EQUIPE_INEXISTENTE. */
	public static final String ERRO_NEGOCIO_EQUIPE_INEXISTENTE = "ERRO_NEGOCIO_EQUIPE_INEXISTENTE";

	/** The Constant CARGA_TRABALHO_DIARIA. */
	public static final String CARGA_TRABALHO_DIARIA = "Carga do Trabalho Diária";

	/** The Constant LIMITE_CARGA_TRABALHO_DIARIA. */
	public static final String LIMITE_CARGA_TRABALHO_DIARIA = "LIMITE_CARGA_TRABALHO_DIARIA";

	/** The Constant ATRIBUTO_UNIDADE_ORGANIZACIONAL. */
	public static final String ATRIBUTO_UNIDADE_ORGANIZACIONAL = "Unidade Organizacional";

	/** The Constant ERRO_NEGOCIO_SERVICO_TIPO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_EXISTENTE = "ERRO_NEGOCIO_SERVICO_TIPO_EXISTENTE";

	/** The Constant SERVICO_TIPO. */
	public static final String SERVICO_TIPO = "SERVICO_TIPO";

	/** The Constant TIPO_SERVICO. */
	public static final String TIPO_SERVICO = "Tipo de Serviço";

	/** The Constant DESCRICAO. */
	public static final String DESCRICAO = "Descrição";

	/** The Constant PRIORIDADE. */
	public static final String PRIORIDADE = "Prioridade";

	/** The Constant DOCUMENTO_LAYOUT. */
	public static final String DOCUMENTO_LAYOUT = "Layout do documento";

	/** The Constant EQUIPAMENTO_ESPECIAL. */
	public static final String EQUIPAMENTO_ESPECIAL = "Indicador de equipamento especial";

	/** The Constant SERVICO_TIPO_PRIORIDADE. */
	public static final String SERVICO_TIPO_PRIORIDADE = "servicoTipoPrioridade";

	/** The Constant PRIORIDADE_TIPO_SERVICO. */
	public static final String PRIORIDADE_TIPO_SERVICO = "Prioridade do Tipo de Serviço";

	/** The Constant DOCUMENTO_IMPRESSOA_LAYOUT. */
	public static final String DOCUMENTO_IMPRESSOA_LAYOUT = "documentoImpressaoLayout";

	/** The Constant LISTA_EQUIPAMENTOS. */
	public static final String LISTA_EQUIPAMENTOS = "servicoTipoPrioridade";

	/** The Constant LISTA_MATERIAIS. */
	public static final String LISTA_MATERIAIS = "servicoTipoPrioridade";

	/** The Constant LISTA_AGENDAMENTOS. */
	public static final String LISTA_AGENDAMENTOS = "servicoTipoPrioridade";

	/** The Constant ERRO_NEGOCIO_REMOVER_FUNCIONARIO. */
	public static final String ERRO_NEGOCIO_REMOVER_FUNCIONARIO = "ERRO_NEGOCIO_REMOVER_FUNCIONARIO";

	/** The Constant ATUALIZACAO_CADASTRAL_IMEDIATA. */
	public static final String ATUALIZACAO_CADASTRAL_IMEDIATA = "I";

	/** The Constant ATUALIZACAO_CADASTRAL_POSTERIOR. */
	public static final String ATUALIZACAO_CADASTRAL_POSTERIOR = "P";

	/** The Constant ATUALIZACAO_CADASTRAL_NAO. */
	public static final String ATUALIZACAO_CADASTRAL_NAO = "N";

	/** The Constant INDICADOR_ATUALIZACAO_CADASTRAL. */
	public static final String INDICADOR_ATUALIZACAO_CADASTRAL = "Exige Atualização Cadastral";

	/** The Constant TELA_ATUALIZACAO. */
	public static final String TELA_ATUALIZACAO = "Tela de Atualziação";

	/** The Constant TIPO_ASSOCIACAO_PONTO_CONSUMO. */
	public static final String TIPO_ASSOCIACAO_PONTO_CONSUMO = "Tipo de Associação";

	/** The Constant TIPO_OPERACAO_MEDIDOR. */
	public static final String TIPO_OPERACAO_MEDIDOR = "Ação";

	/** The Constant C_ENT_CLASSE_TELA_ATUALIZACAO. */
	public static final String C_ENT_CLASSE_TELA_ATUALIZACAO = "C_ENT_CLASSE_TELA_ATUALIZACAO";

	/** The Constant C_ATUALIZACAO_TELA_IMOVEL. */
	public static final String C_ATUALIZACAO_TELA_IMOVEL = "C_ATUALIZACAO_TELA_IMOVEL";

	/** The Constant C_ATUALIZACAO_TELA_CLIENTE. */
	public static final String C_ATUALIZACAO_TELA_CLIENTE = "C_ATUALIZACAO_TELA_CLIENTE";

	/** The Constant C_ATUALIZACAO_TELA_CONTRATO. */
	public static final String C_ATUALIZACAO_TELA_CONTRATO = "C_ATUALIZACAO_TELA_CONTRATO";

	/** The Constant C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO. */
	public static final String C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO = "C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO";

	/** The Constant ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO. */
	public static final String ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO = "ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO";

	/** The Constant C_STATUS_SERV_AUT_PENDENTE. */
	public static final String C_STATUS_SERV_AUT_PENDENTE = "C_STATUS_SERV_AUT_PENDENTE";

	/** The Constant C_STATUS_SERV_AUT_ENCERRADO. */
	public static final String C_STATUS_SERV_AUT_ENCERRADO = "C_STATUS_SERV_AUT_ENCERRADO";

	/** The Constant C_STATUS_SERV_AUT_EXECUTADO. */
	public static final String C_STATUS_SERV_AUT_EXECUTADO = "C_STATUS_SERV_AUT_EXECUTADO";

	/** The Constant C_STATUS_SERV_AUT_CANCELADO. */
	public static final String C_STATUS_SERV_AUT_CANCELADO = "C_STATUS_SERV_AUT_CANCELADO";

	/** The Constant C_STATUS_SERV_AUT_ABERTO. */
	public static final String C_STATUS_SERV_AUT_ABERTO = "C_STATUS_SERV_AUT_ABERTO";

	/** The Constant ERRO_NEGOCIO_SERVICO_TIPO_TIPO_ASSOCIACAO_ACAO. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_TIPO_ASSOCIACAO_ACAO = "ERRO_NEGOCIO_SERVICO_TIPO_TIPO_ASSOCIACAO_ACAO";

	/** The Constant ERRO_NEGOCIO_SERVICO_TIPO_INDICADOR_VALOR. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_INDICADOR_VALOR = "ERRO_NEGOCIO_SERVICO_TIPO_INDICADOR_VALOR";

	/** ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_EXISTENTE = "ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_EXISTENTE";

	/** The Constant SERVICO_TIPO_DESCRICAO. */
	public static final String SERVICO_TIPO_DESCRICAO = "Descrição";

	/** The Constant SERVICO_TIPO_QUANTIDADE_HORAS_MAXIMA. */
	public static final String SERVICO_TIPO_QUANTIDADE_HORAS_MAXIMA = "Quantidade máxima de horas para a execução do serviço";

	/** The Constant SERVICO_TIPO_QUANTIDADE_HORAS_MINIMA. */
	public static final String SERVICO_TIPO_QUANTIDADE_HORAS_MINIMA = "SERVICO_TIPO_QUANTIDADE_HORAS_MINIMA";

	/** The Constant SERVICO_TIPO_INDICADOR_DIAS_CORRIDO. */
	public static final String SERVICO_TIPO_INDICADOR_DIAS_CORRIDO = "Indicador dias corrido e util";

	/** The Constant ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_RELACIONADO. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_RELACIONADO = "ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_RELACIONADO";

	/** The Constant ERRO_NEGOCIO_SERVICO_TIPO_RELACIONADA. */
	public static final String ERRO_NEGOCIO_SERVICO_TIPO_RELACIONADA = "ERRO_NEGOCIO_SERVICO_TIPO_RELACIONADA";

	/** CHAMADO_ASSUNTO. */
	public static final String CAMPO_PROTOCOLO = "Número do Protocolo";

	/** The Constant CAMPO_DESCRICAO. */
	public static final String CAMPO_DESCRICAO = "Descrição";

	/** The Constant CHAMADO_ASSUNTO. */
	public static final String CHAMADO_ASSUNTO = "CHAMADO_ASSUNTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE = "ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ASSUNTO_DESCRICAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_ASSUNTO_DESCRICAO = "ERRO_NEGOCIO_CHAMADO_ASSUNTO_DESCRICAO";

	/** The Constant CAMPOS_OBRIGATORIOS. */
	public static final String CAMPOS_OBRIGATORIOS = "CAMPOS_OBRIGATORIOS";

	/** The Constant ERRO_NEGOCIO_EQUIPE_COMPONENTE_EXISTENTE. */
	public static final String ERRO_NEGOCIO_EQUIPE_COMPONENTE_EXISTENTE = "ERRO_NEGOCIO_EQUIPE_COMPONENTE_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CHAMADO_SERVICO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CHAMADO_SERVICO_EXISTENTE = "ERRO_NEGOCIO_CHAMADO_SERVICO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_CONSULTA_PARAMETRO. */
	public static final String ERRO_NEGOCIO_CONSULTA_PARAMETRO = "ERRO_NEGOCIO_CONSULTA_PARAMETRO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ASSUNTO_VAZIO. */
	public static final String ERRO_NEGOCIO_CHAMADO_ASSUNTO_VAZIO = "ERRO_NEGOCIO_CHAMADO_ASSUNTO_VAZIO";

	/** The Constant QUANTIDADE_MINIMA_DE_HORAS. */
	public static final String QUANTIDADE_MINIMA_DE_HORAS = "QUANTIDADE_MINIMA_DE_HORAS";

	/** The Constant ERRO_NEGOCIO_SERVICO_MATERIAL_EXISTENTE. */
	public static final String ERRO_NEGOCIO_SERVICO_MATERIAL_EXISTENTE = "ERRO_NEGOCIO_SERVICO_MATERIAL_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_SERVICO_TURNO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_SERVICO_TURNO_EXISTENTE = "ERRO_NEGOCIO_SERVICO_TURNO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_SERVICO_EQUIPAMENTO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_SERVICO_EQUIPAMENTO_EXISTENTE = "ERRO_NEGOCIO_SERVICO_EQUIPAMENTO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_REMOVER_FUNCIONARIO. */
	public static final String ERRO_NEGOCIO_REMOVER_EQUIPAMENTO = "ERRO_NEGOCIO_REMOVER_EQUIPAMENTO";

	/** The Constant QUANTIDADE_MINIMA_DE_EQUIPAMENTO. */
	public static final String QUANTIDADE_MINIMA_DE_EQUIPAMENTO = "QUANTIDADE_MINIMA_DE_EQUIPAMENTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ASSUNTO_RELACIONADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_ASSUNTO_RELACIONADO = "ERRO_NEGOCIO_CHAMADO_ASSUNTO_RELACIONADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_RESPONSAVEL. */
	public static final String ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_RESPONSAVEL = "ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_RESPONSAVEL";

	/** The Constant ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO. */
	public static final String ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO = "ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO";

	/** Funcionário. */

	public static final String FUNCIONARIO_ERRO_MATRICULA = "FUNCIONARIO_ERRO_MATRICULA";

	/** VALIDAR DADOS CHAMADO. */
	public static final String CAMPO_CHAMADO_ASSUNTO = "Assunto do Chamado";

	/** The Constant CAMPO_UNIDADE_ORGANIZACIONAL. */
	public static final String CAMPO_UNIDADE_ORGANIZACIONAL = "Unidade Organizacional";

	/** The Constant CAMPO_CANAL_ATENDIMENTO. */
	public static final String CAMPO_CANAL_ATENDIMENTO = "Canal de Atendimento";

	/** The Constant CAMPO_CHAMADO_TIPO. */
	public static final String CAMPO_CHAMADO_TIPO = "Tipo de Chamado";

	/** The Constant C_STATUS_CHAMADO_RASCUNHO. */
	public static final String C_STATUS_CHAMADO_RASCUNHO = "C_STATUS_CHAMADO_RASCUNHO";

	/** The Constant C_STATUS_CHAMADO_ABERTO. */
	public static final String C_STATUS_CHAMADO_ABERTO = "C_STATUS_CHAMADO_ABERTO";

	/** The Constant C_STATUS_CHAMADO_EM_ANDAMENTO. */
	public static final String C_STATUS_CHAMADO_EM_ANDAMENTO = "C_STATUS_CHAMADO_EM_ANDAMENTO";

	/** The Constant C_STATUS_CHAMADO_FINALIZADO. */
	public static final String C_STATUS_CHAMADO_FINALIZADO = "C_STATUS_CHAMADO_FINALIZADO";
	
	/** The Constant C_STATUS_CHAMADO_ABERTO. */
	public static final String C_STATUS_CHAMADO_PENDENTE = "C_STATUS_CHAMADO_PENDENTE";

	/** The Constant C_OPERACAO_CHAMADO_INCLUIDO. */
	public static final String C_OPERACAO_CHAMADO_INCLUIDO = "C_OPERACAO_CHAMADO_INCLUIDO";

	/** The Constant C_OPERACAO_CHAMADO_ALTERADO. */
	public static final String C_OPERACAO_CHAMADO_ALTERADO = "C_OPERACAO_CHAMADO_ALTERADO";

	/** The Constant C_OPERACAO_CHAMADO_TRAMITADO. */
	public static final String C_OPERACAO_CHAMADO_TRAMITADO = "C_OPERACAO_CHAMADO_TRAMITADO";

	/** The Constant C_OPERACAO_CHAMADO_REITERADO. */
	public static final String C_OPERACAO_CHAMADO_REITERADO = "C_OPERACAO_CHAMADO_REITERADO";

	/** The Constant C_OPERACAO_CHAMADO_REATIVADO. */
	public static final String C_OPERACAO_CHAMADO_REATIVADO = "C_OPERACAO_CHAMADO_REATIVADO";

	/** The Constant C_OPERACAO_CHAMADO_REABERTO. */
	public static final String C_OPERACAO_CHAMADO_REABERTO = "C_OPERACAO_CHAMADO_REABERTO";

	/** The Constant C_OPERACAO_CHAMADO_CAPTURADO. */
	public static final String C_OPERACAO_CHAMADO_CAPTURADO = "C_OPERACAO_CHAMADO_CAPTURADO";

	/** The Constant C_OPERACAO_CHAMADO_ENCERRADO. */
	public static final String C_OPERACAO_CHAMADO_ENCERRADO = "C_OPERACAO_CHAMADO_ENCERRADO";

	/** The Constant C_OPERACAO_CHAMADO_ANEXO. */
	public static final String C_OPERACAO_CHAMADO_ANEXO = "C_OPERACAO_CHAMADO_ANEXO";

	/** The Constant C_OPERACAO_SERV_AUT_REMANEJADO. */
	public static final String C_OPERACAO_SERV_AUT_REMANEJADO = "C_OPERACAO_SERV_AUT_REMANEJADO";

	/** The Constant C_OPERACAO_SERV_AUT_EXECUTADO. */
	public static final String C_OPERACAO_SERV_AUT_EXECUTADO = "C_OPERACAO_SERV_AUT_EXECUTADO";

	/** The Constant C_OPERACAO_SERV_AUT_ALTERADO. */
	public static final String C_OPERACAO_SERV_AUT_ALTERADO = "C_OPERACAO_SERV_AUT_ALTERADO";

	/** The Constant C_OPERACAO_SERV_AUT_ENCERRAMENTO. */
	public static final String C_OPERACAO_SERV_AUT_ENCERRAMENTO = "C_OPERACAO_SERV_AUT_ENCERRAMENTO";
	
	/** The Constant C_OPERACAO_GERAR_SERV_AUT_LOTE. */
	public static final String C_OPERACAO_GERAR_SERV_AUT_LOTE = "C_OPERACAO_GERAR_SERV_AUT_LOTE";
	
	/** The Constant C_OPERACAO_EMITIR_NOTIFICACAO_NEGATIVACAO. */
	public static final String C_OPERACAO_EMITIR_NOTIFICACAO_NEGATIVACAO = "C_OPERACAO_EMITIR_NOTIFICACAO_NEGATIVACAO";

	/** The Constant C_ENT_CLASSE_MOT_ENCERRAMENTO. */
	public static final String C_ENT_CLASSE_MOT_ENCERRAMENTO = "C_ENT_CLASSE_MOT_ENCERRAMENTO";

	/** The Constant C_ENT_CLASSE_MOT_REITERACAO. */
	public static final String C_ENT_CLASSE_MOT_REITERACAO = "C_ENT_CLASSE_MOT_REITERACAO";

	/** The Constant C_ENT_CLASSE_MOT_REABERTURA. */
	public static final String C_ENT_CLASSE_MOT_REABERTURA = "C_ENT_CLASSE_MOT_REABERTURA";

	/** The Constant C_ENT_CLASSE_MOT_REATIVACAO. */
	public static final String C_ENT_CLASSE_MOT_REATIVACAO = "C_ENT_CLASSE_MOT_REATIVACAO";

	/** The Constant C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO. */
	public static final String C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO = "C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO";

	/** The Constant C_ENT_CLASSE_STATUS_CHAMADO. */
	public static final String C_ENT_CLASSE_STATUS_CHAMADO = "C_ENT_CLASSE_STATUS_CHAMADO";

	/** The Constant C_ENT_CLASSE_STATUS_CHAMADO. */
	public static final String C_ENT_CONTEUDO_MOTIVO_AS_EXECUTADAS = "C_ENT_CONTEUDO_MOTIVO_AS_EXECUTADAS";

	/** The Constant P_NUMERO_PROTOCOLO_AUTOMATICO. */
	public static final String P_NUMERO_PROTOCOLO_AUTOMATICO = "P_NUMERO_PROTOCOLO_AUTOMATICO";

	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL = "EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO = "EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL =
			"EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL =
			"EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO = "EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL = "EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO = "EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO = "EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA
			= "EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS = "EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO = "EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA = "EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA";
	public static final String EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO = "EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO";

	/** The Constant CHAMADO. */
	public static final String CHAMADO = "CHAMADO";

	/** The Constant CHAMADO_CLIENTE_OBRIGATORIO. */
	public static final String CHAMADO_CLIENTE_OBRIGATORIO = "Cliente";

	/**
	 * The Constant CHAMADO_IMOVEL_OBRIGATORIO.
	 */
	public static final String CHAMADO_IMOVEL_OBRIGATORIO = "Imóvel";

	/**
	 * Constante da integração de cliente
	 **/
	public static final String C_INTEGRACAO_CADASTRO_CLIENTE = "CLIEN";

	/**
	 * Constante da integração de contrato
	 **/
	public static final String C_INTEGRACAO_CONTRATO = "CONTR";

	/** The Constant CHAMADO_IMOVEL_OBRIGATORIO. */
	public static final String CHAMADO_VAZAMENTO_CONFIRMADO_OBRIGATORIO = "Vazamento confirmado";

	/** The Constant CHAMADO_RG_SOLICITANTE_OBRIGATORIO. */
	public static final String CHAMADO_RG_SOLICITANTE_OBRIGATORIO = "RG do solicitante";

	/** The Constant CHAMADO_CPFCNPJ_SOLICITANTE_OBRIGATORIO. */
	public static final String CHAMADO_CPFCNPJ_SOLICITANTE_OBRIGATORIO = "CPF/CNPJ do solicitante";

	/** The Constant CHAMADO_NOME_SOLICITANTE_OBRIGATORIO. */
	public static final String CHAMADO_NOME_SOLICITANTE_OBRIGATORIO = "Nome do solicitante";

	/** The Constant CHAMADO_TELEFONE_SOLICITANTE_OBRIGATORIO. */
	public static final String CHAMADO_TELEFONE_SOLICITANTE_OBRIGATORIO = "Telefone do solicitante";

	/** The Constant CHAMADO_EMAIL_SOLICITANTE_OBRIGATORIO. */
	public static final String CHAMADO_EMAIL_SOLICITANTE_OBRIGATORIO = "E-mail do solicitante";

	/** The Constant ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO = "ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO = "ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_TRAMITACAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_TRAMITACAO = "ERRO_NEGOCIO_CHAMADO_TRAMITACAO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_RASCUNHO. */
	public static final String ERRO_NEGOCIO_CHAMADO_RASCUNHO = "ERRO_NEGOCIO_CHAMADO_RASCUNHO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_FINALIZADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_FINALIZADO = "ERRO_NEGOCIO_CHAMADO_FINALIZADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_REABERTO. */
	public static final String ERRO_NEGOCIO_CHAMADO_REABERTO = "ERRO_NEGOCIO_CHAMADO_REABERTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_REATIVADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_REATIVADO = "ERRO_NEGOCIO_CHAMADO_REATIVADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ALTERACAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_ALTERACAO = "ERRO_NEGOCIO_CHAMADO_ALTERACAO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_MOTIVO. */
	public static final String ERRO_NEGOCIO_CHAMADO_MOTIVO = "ERRO_NEGOCIO_CHAMADO_MOTIVO";
	
	/** The Constant ERRO_NEGOCIO_CHAMADO_DATA_RESOLUCAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_DATA_RESOLUCAO = "ERRO_NEGOCIO_CHAMADO_DATA_RESOLUCAO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_CAPTURADO. */
	public static final String ERRO_NEGOCIO_CHAMADO_CAPTURADO = "ERRO_NEGOCIO_CHAMADO_CAPTURADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ABERTO. */
	public static final String ERRO_NEGOCIO_CHAMADO_ABERTO = "ERRO_NEGOCIO_CHAMADO_ABERTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO. */
	public static final String ERRO_NEGOCIO_CHAMADO = "ERRO_NEGOCIO_CHAMADO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_STATUS. */
	public static final String ERRO_NEGOCIO_CHAMADO_STATUS = "ERRO_NEGOCIO_CHAMADO_STATUS";

	/** The Constant CHAMADO_STATUS_RASCUNHO. */
	public static final String CHAMADO_STATUS_RASCUNHO = "Rascunho";

	/** The Constant CHAMADO_STATUS_FINALIZADO. */
	public static final String CHAMADO_STATUS_FINALIZADO = "Finalizado";

	/** The Constant CHAMADO_STATUS_EMANDAMENTO. */
	public static final String CHAMADO_STATUS_EM_ANDAMENTO = "Em andamento";

	/** The Constant CHAMADO_STATUS_ABERTO. */
	public static final String CHAMADO_STATUS_ABERTO = "Aberto";

	/** The Constant ERRO_NEGOCIO_CHAMADO_PROTOCOLO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CHAMADO_PROTOCOLO_EXISTENTE = "ERRO_NEGOCIO_CHAMADO_PROTOCOLO_EXISTENTE";

	/** The Constant CHAMADO_MOTIVO_REABERTURA. */
	public static final String CHAMADO_MOTIVO_REABERTURA = "Motivo da Reabertura";

	/** The Constant CHAMADO_MOTIVO_REATIVACAO. */
	public static final String CHAMADO_MOTIVO_REATIVACAO = "Motivo da Reativação";

	/** The Constant CHAMADO_MOTIVO_ENCERRAMENTO. */
	public static final String CHAMADO_MOTIVO_ENCERRAMENTO = "Motivo do Encerramento";

	/** The Constant CHAMADO_OPERACAO_REATIVACAO. */
	public static final String CHAMADO_OPERACAO_REATIVACAO = "CHAMADO_OPERACAO_REATIVACAO";

	/** The Constant CHAMADO_OPERACAO_REABERTURA. */
	public static final String CHAMADO_OPERACAO_REABERTURA = "CHAMADO_OPERACAO_REABERTURA";
	
	/** The Constant CHAMADO_DATA_RESOLUCAO. */
	public static final String CHAMADO_DATA_RESOLUCAO = "Data de Resolução";

	/** The Constant ERRO_NEGOCIO_USUARIO_NAO_AUTORIZADO. */
	public static final String ERRO_NEGOCIO_USUARIO_NAO_AUTORIZADO = "ERRO_NEGOCIO_USUARIO_NAO_AUTORIZADO";

	/** The Constant ERRO_NEGOCIO_UNOR_NAO_AUTORIZADO. */
	public static final String ERRO_NEGOCIO_UNOR_NAO_AUTORIZADO = "ERRO_NEGOCIO_UNOR_NAO_AUTORIZADO";

	/** The Constant ERRO_NEGOCIO_UNOR_NAO_POSSUI_UNIDADE_ORGANIZACIONAL. */
	public static final String ERRO_NEGOCIO_UNOR_NAO_POSSUI_UNIDADE_ORGANIZACIONAL = "ERRO_NEGOCIO_UNOR_NAO_POSSUI_UNIDADE_ORGANIZACIONAL";

	/** The Constant CHAMADO_OPERACAO_TRAMITACAO. */
	public static final String CHAMADO_OPERACAO_TRAMITACAO = "CHAMADO_OPERACAO_TRAMITACAO";

	/** The Constant CHAMADO_OPERACAO_REITERACAO. */
	public static final String CHAMADO_OPERACAO_REITERACAO = "CHAMADO_OPERACAO_REITERACAO";

	/** The Constant CHAMADO_OPERACAO_CAPTURA. */
	public static final String CHAMADO_OPERACAO_CAPTURA = "CHAMADO_OPERACAO_CAPTURA";

	/** The Constant CHAMADO_OPERACAO_ENCERRAMENTO. */
	public static final String CHAMADO_OPERACAO_ENCERRAMENTO = "CHAMADO_OPERACAO_ENCERRAMENTO";

	/** The Constant CHAMADO_OPERACAO_ALTERACAO. */
	public static final String CHAMADO_OPERACAO_ALTERACAO = "CHAMADO_OPERACAO_ALTERACAO";

	/** The Constant CHAMADO_OPERACAO_RESPONDER_QUESTIONARIO. */
	public static final String CHAMADO_OPERACAO_RESPONDER_QUESTIONARIO = "CHAMADO_OPERACAO_RESPONDER_QUESTIONARIO";

	/** The Constant CHAMADO_GERACAO_SERVICO_AUTORIZACAO. */
	public static final String CHAMADO_GERACAO_SERVICO_AUTORIZACAO = "CHAMADO_GERACAO_SERVICO_AUTORIZACAO";

	/**
	 * The Constant ERRO_PREVISAO_ENCERRAMENTO_CHAMADO_PASSADO
	 */
	public static final String ERRO_PREVISAO_ENCERRAMENTO_CHAMADO_PASSADO = "ERRO_PREVISAO_ENCERRAMENTO_CHAMADO_PASSADO";

	/**
	 * The Constant ERRO_SEGMENTO_DIFERENTE_PONTO_CONSUMO_CHAMADO_TIPO
	 */
	public static final String ERRO_SEGMENTO_DIFERENTE_PONTO_CONSUMO_CHAMADO_TIPO = "ERRO_SEGMENTO_DIFERENTE_PONTO_CONSUMO_CHAMADO_TIPO";

	/** The Constant SERVICO_AUTORIZACAO_CONTRATO_OBRIGATORIO. */
	public static final String SERVICO_AUTORIZACAO_CONTRATO_OBRIGATORIO = "Contrato";

	/** The Constant SERVICO_AUTORIZACAO_PONTO_CONSUMO_OBRIGATORIO. */
	public static final String SERVICO_AUTORIZACAO_PONTO_CONSUMO_OBRIGATORIO = "Ponto de Consumo";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_RUBRICA_DEBITO. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_RUBRICA_DEBITO = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_RUBRICA_DEBITO";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_INDICADOR_COBRANCA. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_INDICADOR_COBRANCA = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_INDICADOR_COBRANCA";

	/** The Constant SUCESSO_GERACAO_SERVICO_AUTORIZACAO. */
	public static final String SUCESSO_GERACAO_SERVICO_AUTORIZACAO = "SUCESSO_GERACAO_SERVICO_AUTORIZACAO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_GERACAO_SERVICO_AUTORIZACAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_GERACAO_SERVICO_AUTORIZACAO = "ERRO_NEGOCIO_CHAMADO_GERACAO_SERVICO_AUTORIZACAO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_GERACAO_SEM_SERVICO. */
	public static final String ERRO_NEGOCIO_CHAMADO_GERACAO_SEM_SERVICO = "ERRO_NEGOCIO_CHAMADO_GERACAO_SEM_SERVICO";

	/** The Constant ERRO_NEGOCIO_EMAIL_INVALIDO. */
	public static final String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	/** The Constant CANAL_ATENDIMENTO. */
	public static final String CANAL_ATENDIMENTO = "br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl";

	/** The Constant ERRO_NEGOCIO_REMOVER_CANAL_ATENDIMENTO. */
	public static final String ERRO_NEGOCIO_REMOVER_CANAL_ATENDIMENTO = "ERRO_NEGOCIO_REMOVER_CANAL_ATENDIMENTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_ALTERACAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_ALTERACAO = "ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_ALTERACAO";

	/**The Constant ERRO_NEGOCIO_CHAMADO_UNIDADE_ORGANIZACIONAL_TRAMITACAO. */
	public static final String ERRO_NEGOCIO_CHAMADO_UNIDADE_ORGANIZACIONAL_TRAMITACAO =
			"ERRO_NEGOCIO_CHAMADO_UNIDADE_ORGANIZACIONAL_TRAMITACAO";

	/** The Constant C_ENT_CLASSE_TURNO. */
	public static final String C_ENT_CLASSE_TURNO = "C_ENT_CLASSE_TURNO";

	/** The Constant C_ENT_CLASSE_EQUIPAMENTO. */
	public static final String C_ENT_CLASSE_EQUIPAMENTO = "C_ENT_CLASSE_EQUIPAMENTO";

	/** The Constant C_CODIGO_ATIVIDADE_REALIZAR_LEITURA. */
	public static final String C_CODIGO_ATIVIDADE_REALIZAR_LEITURA = "C_CODIGO_ATIVIDADE_REALIZAR_LEITURA";

	/** The Constant C_CODIGO_ATIVIDADE_REGISTRAR_LEITURA. */
	public static final String C_CODIGO_ATIVIDADE_REGISTRAR_LEITURA = "C_CODIGO_ATIVIDADE_REGISTRAR_LEITURA";

	/** The Constant TIPO_DE_DOCUMENTO. */
	public static final String TIPO_DE_DOCUMENTO = "Tipo de documento";

	/** The Constant CONTROLE_DE_ENTREGA_DE_DOCUMENTO. */
	public static final String CONTROLE_DE_ENTREGA_DE_DOCUMENTO = "Controle de entrega de documento";

	/** The Constant C_CONTROLE_DOCUMENTO_SITUACAO_ENTREGA. */
	public static final String C_CONTROLE_DOCUMENTO_SITUACAO_ENTREGA = "C_CONTROLE_DOCUMENTO_SITUACAO_ENTREGA";

	/** The Constant C_CONTROLE_DOCUMENTO_MOTIVO_NAO_ENTREGA. */
	public static final String C_CONTROLE_DOCUMENTO_MOTIVO_NAO_ENTREGA = "C_CONTROLE_DOCUMENTO_MOTIVO_NAO_ENTREGA";

	/** The Constant QUANTIDADE_DIAS_PERIODICIDADE. */
	public static final int QUANTIDADE_DIAS_PERIODICIDADE = 14;

	/** The Constant QUANTIDADE_CICLOS_PERIODICIDADE. */
	public static final int QUANTIDADE_CICLOS_PERIODICIDADE = 3;

	/** The Constant ULTIMO_DIA_FEVEREIRO. */
	public static final int ULTIMO_DIA_FEVEREIRO = 28;

	/** The Constant QUANTIDADE_DIAS_PERIODICIDADE_SEMANAL. */
	public static final int QUANTIDADE_DIAS_PERIODICIDADE_SEMANAL = 7;

	/** The Constant QUANTIDADE_CICLOS_PERIODICIDADE_SEMANAL. */
	public static final int QUANTIDADE_CICLOS_PERIODICIDADE_SEMANAL = 5;

	/** The Constant ERRO_NEGOCIO_TIPO_DOCUMENTO_EM_USO. */
	public static final String ERRO_NEGOCIO_TIPO_DOCUMENTO_EM_USO = "ERRO_NEGOCIO_TIPO_DOCUMENTO_EM_USO";

	/** The Constant SUCESSO_ARRECADADOR_CONVENIO_PADRAO. */
	public static final String SUCESSO_ARRECADADOR_CONVENIO_PADRAO = "SUCESSO_ARRECADADOR_CONVENIO_PADRAO";

	/**
	 * The Constant
	 * ERRO_NEGOCIO_CONTRATO_ARRECADADOR_NUMERO_CONTRATO_EXISTENTE_ARRECADADOR.
	 */
	public static final String ERRO_NEGOCIO_CONTRATO_ARRECADADOR_NUMERO_CONTRATO_EXISTENTE_ARRECADADOR =
			"ERRO_NEGOCIO_CONTRATO_ARRECADADOR_NUMERO_CONTRATO_EXISTENTE_ARRECADADOR";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_PADRAO_INATIVO. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_PADRAO_INATIVO = "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_PADRAO_INATIVO";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_INATIVO_CONTRATO_ATIVO. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_INATIVO_CONTRATO_ATIVO
			= "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_INATIVO_CONTRATO_ATIVO";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE = "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE_CONTRATO. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE_CONTRATO = "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE_CONTRATO";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_BOLETO_SEM_CARTEIRA. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_BOLETO_SEM_CARTEIRA = "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_BOLETO_SEM_CARTEIRA";

	/** The Constant ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA. */
	public static final String ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA = "ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA";

	/** The Constant ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_GERADO. */
	public static final String ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_GERADO = "ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_GERADO";

	/** The Constant ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_INICIO. */
	public static final String ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_INICIO = "ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_INICIO";

	/** The Constant ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_FIM. */
	public static final String ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_FIM = "ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_FIM";

	/** The Constant ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_NOSSO_NUMERO. */
	public static final String ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_NOSSO_NUMERO = "ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_NOSSO_NUMERO";

	/** The Constant ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_INICIAL. */
	public static final String ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_INICIAL =
			"ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_INICIAL";

	/** The Constant ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_FIM. */
	public static final String ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_FIM = "ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_FIM";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_LEIAUTE_TIPO_CONVENIO. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_LEIAUTE_TIPO_CONVENIO =
			"ERRO_NEGOCIO_ARRECADADOR_CONVENIO_LEIAUTE_TIPO_CONVENIO";

	/** The Constant ERRO_NEGOCIO_ARRECADADOR_CONVENIO_NAO_CADASTRADO. */
	public static final String ERRO_NEGOCIO_ARRECADADOR_CONVENIO_NAO_CADASTRADO = "ERRO_NEGOCIO_ARRECADADOR_CONVENIO_NAO_CADASTRADO";

	/** The Constant SELECIONE_UM_PONTO_CONSUMO. */
	public static final String SELECIONE_UM_PONTO_CONSUMO = "SELECIONE_UM_PONTO_CONSUMO";

	/** The Constant ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_PONTOS_ASSOCIADOS. */
	public static final String ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_PONTOS_ASSOCIADOS = "ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_PONTOS_ASSOCIADOS";

	/** The Constant ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_FATURAS_ASSOCIADOS. */
	public static final String ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_FATURAS_ASSOCIADOS = "ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_FATURAS_ASSOCIADOS";

	/** The Constant ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_LEITURA_MOVIMENTO_ASSOCIADOS. */
	public static final String ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_LEITURA_MOVIMENTO_ASSOCIADOS = "ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_"
			+ "LEITURA_MOVIMENTO_ASSOCIADOS";

	/** The Constant ERRO_NEGOCIO_TIPO_DOCUMENTO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_TIPO_DOCUMENTO_EXISTENTE = "ERRO_NEGOCIO_TIPO_DOCUMENTO_EXISTENTE";

	/** The Constant ERRO_SELECIONAR_FILTRO. */
	public static final String ERRO_SELECIONAR_FILTRO = "ERRO_SELECIONAR_FILTRO";

	/** The Constant ERRO_NUMERO_DE_ROTAS_EXCEDIDO. */
	public static final String ERRO_NUMERO_DE_ROTAS_EXCEDIDO = "ERRO_NUMERO_DE_ROTAS_EXCEDIDO";

	/** The Constant ERRO_RA_TARIFA_DIVERGE_RA_PC. */
	public static final String ERRO_RA_TARIFA_DIVERGE_RA_PC = "ERRO_RA_TARIFA_DIVERGE_RA_PC";

	/** The Constant ACAO. */
	public static final String ACAO = "ACAO";

	/** The Constant C_ENT_CLASSE_OPCOES_DIAS. */
	public static final String C_ENT_CLASSE_OPCOES_DIAS = "C_ENT_CLASSE_OPCOES_DIAS";

	/** The Constant CAMPO_OPERACAO. */
	public static final String CAMPO_OPERACAO = "Operação";

	/** The Constant ERRO_NEGOCIO_ACAO_RELACIONADO. */
	public static final String ERRO_NEGOCIO_ACAO_RELACIONADO = "ERRO_NEGOCIO_ACAO_RELACIONADO";

	/** The Constant SERVICO_AUTORIZACAO. */
	public static final String SERVICO_AUTORIZACAO = "SERVICO_AUTORIZACAO";

	/** The Constant DATA_PREVISAO_ENCERRAMENTO. */
	public static final String DATA_PREVISAO_ENCERRAMENTO = "Data de previsão do encerramento";

	/** The Constant ERRO_DATA_INVALIDADE. */
	public static final String ERRO_DATA_INVALIDADE = "ERRO_DATA_INVALIDADE";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR_SEM_ALTERACAO. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR_SEM_ALTERACAO = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR_"
			+ "SEM_ALTERACAO";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATAS. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATAS = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATAS";

	/** The Constant ERRO_NEGOCIO_CHAMADO_DATAS. */
	public static final String ERRO_NEGOCIO_CHAMADO_DATAS = "ERRO_NEGOCIO_CHAMADO_DATAS";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_EXECUTAR_DATAS. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_EXECUTAR_DATAS = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_EXECUTAR_DATAS";

	/** The Constant EQUIPE. */
	public static final String EQUIPE = "Equipe";

	/** The Constant CLIENTE. */
	public static final String CLIENTE = "Cliente";

	/** The Constant ERRO_SERVICO_AUTORIZACAO_DUPLICADO. */
	public static final String ERRO_SERVICO_AUTORIZACAO_DUPLICADO = "ERRO_SERVICO_AUTORIZACAO_DUPLICADO";

	/** The Constant ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO. */
	public static final String ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO = "ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO";

	public static final String ERRO_SERVICO_TIPO_RESTRITO = "ERRO_SERVICO_TIPO_RESTRITO";

	public static final String ERRO_SERVICO_NUMERO_EXECUCOES_ESTOURADO = "ERRO_SERVICO_NUMERO_EXECUCOES_ESTOURADO";

	/** The Constant C_ENT_CLASSE_STATUS_SERV_AUT. */
	public static final String C_ENT_CLASSE_STATUS_SERV_AUT = "C_ENT_CLASSE_STATUS_SERV_AUT";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_SEM_MOTIVO. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_SEM_MOTIVO = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_"
			+ "ENCERRAMENTO_SEM_MOTIVO";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO";

	/**
	 * The Constant
	 * ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_TIPO_SERVICO_INDICADOR_IMEDIATO.
	 */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_TIPO_SERVICO_INDICADOR_IMEDIATO = "ERRO_NEGOCIO_"
			+ "SERVICO_AUTORIZACAO_ENCERRAMENTO_TIPO_SERVICO_INDICADOR_IMEDIATO";

	/** The Constant ERRO_SELECIONE_ALGUMA_FATURA. */
	public static final String ERRO_SELECIONE_ALGUMA_FATURA = "ERRO_SELECIONE_ALGUMA_FATURA";

	/** The Constant ERRO_NAO_EXISTE_ESTA_POSICAO. */
	public static final String ERRO_NAO_EXISTE_ESTA_POSICAO = "ERRO_NAO_EXISTE_ESTA_POSICAO";

	/** The Constant DIGITE_A_POSICAO_PARA_MOVER. */
	public static final String DIGITE_A_POSICAO_PARA_MOVER = "DIGITE_A_POSICAO_PARA_MOVER";

	/** The Constant ESCOLHA_UM_PONTO_PARA_MOVER. */
	public static final String ESCOLHA_UM_PONTO_PARA_MOVER = "ESCOLHA_UM_PONTO_PARA_MOVER";

	/** The Constant ERRO_SELECIONE_ALGUM_SEGMENTO. */
	public static final String ERRO_SELECIONE_ALGUM_SEGMENTO = "ERRO_SELECIONE_ALGUM_SEGMENTO";

	/** The Constant ERRO_NEGOCIO_MES_ANO_NAO_INFORMADO. */
	public static final String ERRO_NEGOCIO_MES_ANO_NAO_INFORMADO = "ERRO_NEGOCIO_MES_ANO_NAO_INFORMADO";

	/** The Constant ERRO_NEGOCIO_MES_ANO_NAO_VALIDO. */
	public static final String ERRO_NEGOCIO_MES_ANO_NAO_VALIDO = "ERRO_NEGOCIO_MES_ANO_NAO_VALIDO";

	/** The Constant IMOVEL_INTERLIGADO. */
	public static final String IMOVEL_INTERLIGADO = "Interligado";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_IMOVEL_INTERLIGADO. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_IMOVEL_INTERLIGADO = "ERRO_NAO_E_POSSIVEL_REMOVER_IMOVEL_INTERLIGADO";

	/** The Constant ERRO_NEGOCIO_ACAOCOMANDO_RELACIONADO. */
	public static final String ERRO_NEGOCIO_ACAOCOMANDO_RELACIONADO = "ERRO_NEGOCIO_ACAOCOMANDO_RELACIONADO";

	/** The Constant ERRO_INFORME_DATA_FINAL. */
	public static final String ERRO_INFORME_DATA_FINAL = "ERRO_INFORME_DATA_FINAL";

	/** The Constant ERRO_INFORME_DATA_INICIAL. */
	public static final String ERRO_INFORME_DATA_INICIAL = "ERRO_INFORME_DATA_INICIAL";

	/** The Constant ERRO_INFORME_PERIODO_ANO_MES. */
	public static final String ERRO_INFORME_PERIODO_ANO_MES = "ERRO_INFORME_PERIODO_ANO_MES";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_LOCALIDADE_EM_USO. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_LOCALIDADE_EM_USO = "ERRO_NAO_E_POSSIVEL_REMOVER_LOCALIDADE_EM_USO";

	/** The Constant ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATA_FINAL_MENOR. */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATA_FINAL_MENOR = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATA_FINAL_MENOR";

	/** The Constant DATA_GERACAO. */
	public static final String DATA_GERACAO = "DATA_GERACAO";

	/** The Constant DATA_ENCERRAMENTO. */
	public static final String DATA_ENCERRAMENTO = "DATA_ENCERRAMENTO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_ENCERRAR_SEAU_ABERTA. */
	public static final String ERRO_NEGOCIO_CHAMADO_ENCERRAR_SEAU_ABERTA = "ERRO_NEGOCIO_CHAMADO_ENCERRAR_SEAU_ABERTA";

	/** The Constant PARAMETRO_EMISSAO_SERIE_ELETRONICA. */
	public static final String PARAMETRO_EMISSAO_SERIE_ELETRONICA = "EMISSAO_SERIE_ELETRONICA";

	/** The Constant ERRO_NAO_EXISTE_SERIE_COMPATIVEL_A_DATA_REFERENCIA_FATURA. */
	public static final String ERRO_NAO_EXISTE_SERIE_COMPATIVEL_A_DATA_REFERENCIA_FATURA = "ERRO_NAO_EXISTE_SERIE_COMPATIVEL_A_"
			+ "DATA_REFERENCIA_FATURA";

	/** The Constant ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO. */
	public static final String ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO = "ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_"
			+ "CODIGO_PONTO_CONSUMO";

	/** The Constant ERRO_NAO_REMOVER_USUARIO. */
	public static final String ERRO_NAO_REMOVER_USUARIO = "ERRO_NAO_REMOVER_USUARIO";

	/**
	 * The Constant
	 * ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_MOTIVO_CANCELAMENTO.
	 */
	public static final String ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_MOTIVO_CANCELAMENTO = "ERRO_NEGOCIO_SERVICO_AUTORIZACAO_"
			+ "ENCERRAMENTO_MOTIVO_CANCELAMENTO";

	/** The Constant C_INTEGRACAO_TITULO_NEGOCIACAO. */
	public static final String C_INTEGRACAO_TITULO_NEGOCIACAO = "C_INTEGRACAO_TITULO_NEGOCIACAO";

	/** The Constant C_FATURA_CANCELA_POR_PARCELAMENTO. */
	public static final String C_FATURA_CANCELA_POR_PARCELAMENTO = "C_FATURA_CANCELA_POR_PARCELAMENTO";

	/** The Constant ERRO_SELECIONE_UM_IMOVEL_OU_CLIENTE. */
	public static final String ERRO_SELECIONE_UM_IMOVEL_OU_CLIENTE = "ERRO_SELECIONE_UM_IMOVEL_OU_CLIENTE";

	/** The Constant ERRO_NEGOCIO_EXCLUIR_AGENDAMENTO_PENDENTE. */
	public static final String ERRO_NEGOCIO_EXCLUIR_AGENDAMENTO_PENDENTE = "ERRO_NEGOCIO_EXCLUIR_AGENDAMENTO_PENDENTE";

	/** The Constant ERRO_SELECIONE_UM_PONTO_PARA_SER_ORDENADO. */
	public static final String ERRO_SELECIONE_UM_PONTO_PARA_SER_ORDENADO = "ERRO_SELECIONE_UM_PONTO_PARA_SER_ORDENADO";

	/** The Constant C_MEDIDOR_OPERACAO_MUDANCA_PRESSAO. */
	public static final String C_MEDIDOR_OPERACAO_MUDANCA_PRESSAO = "C_MEDIDOR_OPERACAO_MUDANCA_PRESSAO";

	/** The Constant C_MEDIDOR_OPERACAO_SUPRESSAO. */
	public static final String C_MEDIDOR_OPERACAO_SUPRESSAO = "C_MEDIDOR_OPERACAO_SUPRESSAO";

	/** The Constant C_MEDIDOR_OPERACAO_CALIBRACAO. */
	public static final String C_MEDIDOR_OPERACAO_CALIBRACAO = "C_MEDIDOR_OPERACAO_CALIBRACAO";

	/** The Constant C_MEDIDOR_OPERACAO_SUBSTITUICAO. */
	public static final String C_MEDIDOR_OPERACAO_SUBSTITUICAO = "C_MEDIDOR_OPERACAO_SUBSTITUICAO";

	/** The Constant C_MEDIDOR_OPERACAO_INSTALACAO. */
	public static final String C_MEDIDOR_OPERACAO_INSTALACAO = "C_MEDIDOR_OPERACAO_INSTALACAO";

	/** The Constant C_MEDIDOR_OPERACAO_ATIVACAO. */
	public static final String C_MEDIDOR_OPERACAO_ATIVACAO = "C_MEDIDOR_OPERACAO_ATIVACAO";

	/** The Constant C_MEDIDOR_OPERACAO_RETIRADA. */
	public static final String C_MEDIDOR_OPERACAO_RETIRADA = "C_MEDIDOR_OPERACAO_RETIRADA";

	/** The Constant C_MEDIDOR_OPERACAO_BLOQUEIO. */
	public static final String C_MEDIDOR_OPERACAO_BLOQUEIO = "C_MEDIDOR_OPERACAO_BLOQUEIO";

	/** The Constant C_MEDIDOR_OPERACAO_REATIVACAO. */
	public static final String C_MEDIDOR_OPERACAO_REATIVACAO = "C_MEDIDOR_OPERACAO_REATIVACAO";

	/** The Constant C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO. */
	public static final String C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO = "C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO";
	
	/** The Constant C_MEDIDOR_OPERACAO_BLOQUEIO_REATIVACAO. */
	public static final String C_MEDIDOR_OPERACAO_BLOQUEIO_REATIVACAO = "C_MEDIDOR_OPERACAO_BLOQUEIO_REATIVACAO";

	/** The Constant C_STATUS_SERV_AUT_EXECUCAO. */
	public static final String C_STATUS_SERV_AUT_EXECUCAO = "C_STATUS_SERV_AUT_EXECUCAO";

	/** Questionario /Pergunta. */

	public static final String QUESTIONARIO = "Questionario";

	/** The Constant PERGUNTA. */
	public static final String PERGUNTA = "Pergunta";

	/** The Constant CAMPO_NOTA_MINIMA. */
	public static final String CAMPO_NOTA_MINIMA = "Nota Minima";

	/** The Constant CAMPO_NOTA_MAXIMA. */
	public static final String CAMPO_NOTA_MAXIMA = "Nota Maxima";

	/** The Constant CAMPO_NOME_PERGUNTA. */
	public static final String CAMPO_NOME_PERGUNTA = "Nome da Pergunta";

	/** The Constant CAMPO_OBJETIVA. */
	public static final String CAMPO_OBJETIVA = "Ojetiva";

	/** The Constant RESPOSTA. */
	public static final String RESPOSTA = "Resposta";

	/** The Constant ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA. */
	public static final String ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA = "ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA";

	/** The Constant ERRO_NEGOCIO_CAMPO_NOTA_MINIMA_NOTA_MAXIMA. */
	public static final String ERRO_NEGOCIO_CAMPO_NOTA_MINIMA_NOTA_MAXIMA = "ERRO_NEGOCIO_CAMPO_NOTA_MINIMA_NOTA_MAXIMA";

	/** The Constant ERRO_NEGOCIO_CAMPO_NOTA_NEGATIVO. */
	public static final String ERRO_NEGOCIO_CAMPO_NOTA_NEGATIVO = "ERRO_NEGOCIO_CAMPO_NOTA_NEGATIVO";

	/** The Constant ERRO_NEGOCIO_QUESTIONARIO_TIPOSERVICO_SEGMENTO. */
	public static final String ERRO_NEGOCIO_QUESTIONARIO_TIPOSERVICO_SEGMENTO = "ERRO_NEGOCIO_QUESTIONARIO_TIPOSERVICO_SEGMENTO";

	/** The Constant ERRO_NEGOCIO_QUESTIONARIO_NAO_EXISTE. */
	public static final String ERRO_NEGOCIO_QUESTIONARIO_NAO_EXISTE = "ERRO_NEGOCIO_QUESTIONARIO_NAO_EXISTE";

	/** The Constant ERRO_NEGOCIO_REMOCAO_QUESTIONARIO. */
	public static final String ERRO_NEGOCIO_REMOCAO_QUESTIONARIO = "ERRO_NEGOCIO_REMOCAO_QUESTIONARIO";

	/** The Constant ERRO_NEGOCIO_REMOCAO_PERGUNTA. */
	public static final String ERRO_NEGOCIO_REMOCAO_PERGUNTA = "ERRO_NEGOCIO_REMOCAO_PERGUNTA";

	/** The Constant ERRO_NEGOCIO_QUESTIONARIO_RESPONDIDO. */
	public static final String ERRO_NEGOCIO_QUESTIONARIO_RESPONDIDO = "ERRO_NEGOCIO_QUESTIONARIO_RESPONDIDO";

	/** The Constant C_CHAMADO_TIPO_PESQUISA. */
	public static final String C_CHAMADO_TIPO_PESQUISA = "C_CHAMADO_TIPO_PESQUISA";

	/** The Constant C_CANAL_ATENDIMENTO_PESQUISA. */
	public static final String C_CANAL_ATENDIMENTO_PESQUISA = "C_CANAL_ATENDIMENTO_PESQUISA";

	/** The Constant ERRO_NEGOCIO_CHAMADO_NAO_TIPO_PESQUISA. */
	public static final String ERRO_NEGOCIO_CHAMADO_NAO_TIPO_PESQUISA = "ERRO_NEGOCIO_CHAMADO_NAO_TIPO_PESQUISA";

	/** The Constant C_TIPO_DOCUMENTO_NOTIFICACAO_CORTE. */
	public static final String C_TIPO_DOCUMENTO_NOTIFICACAO_CORTE = "C_TIPO_DOCUMENTO_NOTIFICACAO_CORTE";

	/** The Constant C_TIPO_DOCUMENTO_AVISO_CORTE. */
	public static final String C_TIPO_DOCUMENTO_AVISO_CORTE = "C_TIPO_DOCUMENTO_AVISO_CORTE";
	
	/** The Constant C_TIPO_DOCUMENTO_AVISO_CORTE. */
	public static final String C_TIPO_DOCUMENTO_PROTOCOLO = "PROTOCOLO";

	/** The Constant CAMPO_SEGMENTO. */
	public static final String CAMPO_SEGMENTO = "Segmento";

	/** The Constant CAMPO_NOME. */
	public static final String CAMPO_NOME = "Nome";

	/** The Constant ERRO_NEGOCIO_CAMPOS_RESPOSTAS_OBRIGATORIOS. */
	public static final String ERRO_NEGOCIO_CAMPOS_RESPOSTAS_OBRIGATORIOS = "ERRO_NEGOCIO_CAMPOS_RESPOSTAS_OBRIGATORIOS";

	/** Agência Virtual. */

	public static final String C_USUARIO_AGENCIA_VIRTUAL = "C_USUARIO_AGENCIA_VIRTUAL";

	/** The Constant C_CANAL_ATENDIMENTO_AGENCIA_VIRTUAL. */
	public static final String C_CANAL_ATENDIMENTO_AGENCIA_VIRTUAL = "C_CANAL_ATENDIMENTO_AGENCIA_VIRTUAL";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO = "C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_ALTERAR_VENCIMENTO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_ALTERAR_VENCIMENTO = "C_CHAMADO_ASSUNTO_AGV_ALTERAR_VENCIMENTO";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_SOLICITAR_INTERRUPCAO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_SOLICITAR_INTERRUPCAO = "C_CHAMADO_ASSUNTO_AGV_SOLICITAR_INTERRUPCAO";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_SOLICITAR_RELIGACAO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_SOLICITAR_RELIGACAO = "C_CHAMADO_ASSUNTO_AGV_SOLICITAR_RELIGACAO";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_SOLICITAR_LIGACAO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_SOLICITAR_LIGACAO = "C_CHAMADO_ASSUNTO_AGV_SOLICITAR_LIGACAO";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_FALE_CONOSCO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_FALE_CONOSCO = "C_CHAMADO_ASSUNTO_AGV_FALE_CONOSCO";

	/** The Constant C_ABA_ANEXO_IDENTIFICACAO. */
	public static final String C_ABA_ANEXO_IDENTIFICACAO = "C_ABA_ANEXO_IDENTIFICACAO";

	/** The Constant C_ABA_ANEXO_ENDERECO. */
	public static final String C_ABA_ANEXO_ENDERECO = "C_ABA_ANEXO_ENDERECO";

	/** The Constant C_TIPO_ANEXO_CPF. */
	public static final String C_TIPO_ANEXO_CPF = "C_TIPO_ANEXO_CPF";

	/** The Constant C_TIPO_ANEXO_RG. */
	public static final String C_TIPO_ANEXO_RG = "C_TIPO_ANEXO_RG";

	/** The Constant C_TIPO_ANEXO_CNPJ. */
	public static final String C_TIPO_ANEXO_CNPJ = "C_TIPO_ANEXO_CNPJ";

	/** The Constant C_TIPO_ANEXO_COMPROVANTE_RESIDENCIA. */
	public static final String C_TIPO_ANEXO_COMPROVANTE_RESIDENCIA = "C_TIPO_ANEXO_COMPROVANTE_RESIDENCIA";

	/** The Constant C_TIPO_ANEXO_OUTROS. */
	public static final String C_TIPO_ANEXO_OUTROS = "C_TIPO_ANEXO_OUTROS";

	/** The Constant C_TIPO_ACAO_INCLUSAO. */
	public static final String C_TIPO_ACAO_INCLUSAO = "C_TIPO_ACAO_INCLUSAO";

	/** The Constant C_TIPO_ACAO_ALTERACAO. */
	public static final String C_TIPO_ACAO_ALTERACAO = "C_TIPO_ACAO_ALTERACAO";

	/** The Constant C_TIPO_ACAO_EXCLUSAO. */
	public static final String C_TIPO_ACAO_EXCLUSAO = "C_TIPO_ACAO_EXCLUSAO";

	/** The Constant C_ENT_CLASSE_TIPO_ANEXO. */
	public static final String C_ENT_CLASSE_TIPO_ANEXO = "C_ENT_CLASSE_TIPO_ANEXO";

	/** The Constant C_ENTIDADE_CLASSE_TIPO_FALE_CONOSCO. */
	public static final String C_ENTIDADE_CLASSE_TIPO_FALE_CONOSCO = "C_ENTIDADE_CLASSE_TIPO_FALE_CONOSCO";

	/** The Constant C_CHAMADO_TIPO_AGV_ALTERAR_VENCIMENTO. */
	public static final String C_CHAMADO_TIPO_AGV_ALTERAR_VENCIMENTO = "C_CHAMADO_TIPO_AGV_ALTERAR_VENCIMENTO";

	/** The Constant C_MOTIVO_ENCERRAMENTO_CHAMADO. */
	public static final String C_MOTIVO_ENCERRAMENTO_CHAMADO = "C_MOTIVO_ENCERRAMENTO_CHAMADO";

	/** The Constant C_ENT_CLASSE_TIPO_FALE_CONOSCO. */
	public static final String C_ENT_CLASSE_TIPO_FALE_CONOSCO = "C_ENT_CLASSE_TIPO_FALE_CONOSCO";

	/** The Constant C_OPERACAO_SISTEMA_GERAR_PESQ_SATISF. */
	public static final String C_OPERACAO_SISTEMA_GERAR_PESQ_SATISF = "C_OPERACAO_SISTEMA_GERAR_PESQ_SATISF";

	/** The Constant CAMPO_MODULO. */
	public static final String CAMPO_MODULO = "Módulo";

	/** The Constant C_CHAMADO_ASSUNTO_PESQUISA. */
	public static final String C_CHAMADO_ASSUNTO_PESQUISA = "C_CHAMADO_ASSUNTO_PESQUISA";

	/** The Constant C_OPERACAO_CHAM_TENT_CONTATO. */
	public static final String C_OPERACAO_CHAM_TENT_CONTATO = "C_OPERACAO_CHAM_TENT_CONTATO";

	/** The Constant ERRO_NEGOCIO_REGISTRAR_TENTATIVA_CONTATO. */
	public static final String ERRO_NEGOCIO_REGISTRAR_TENTATIVA_CONTATO = "ERRO_NEGOCIO_REGISTRAR_TENTATIVA_CONTATO";

	/** The Constant SUCESSO_REGISTRO_TENTATIVA_CONTATO. */
	public static final String SUCESSO_REGISTRO_TENTATIVA_CONTATO = "SUCESSO_REGISTRO_TENTATIVA_CONTATO";

	/** The Constant C_DIAS_POSSIVEIS_VENCIMENTO. */
	public static final String C_DIAS_POSSIVEIS_VENCIMENTO = "C_DIAS_POSSIVEIS_VENCIMENTO";

	/** The Constant ERRO_NEGOCIO_NAO_HA_SERVICO_TIPO_AGENDAMENTO. */
	public static final String ERRO_NEGOCIO_NAO_HA_SERVICO_TIPO_AGENDAMENTO = "ERRO_NEGOCIO_NAO_HA_SERVICO_TIPO_AGENDAMENTO";

	/** The Constant CONTA_CONTABIL_NUMERO. */
	public static final String CONTA_CONTABIL_NUMERO = "Número de Conta Contábil";

	/** The Constant FORMATO_HORA_INVALIDO. */
	public static final String FORMATO_HORA_INVALIDO = "FORMATO_HORA_INVALIDO";

	/** The Constant C_SEGMENTO_FATURA_AVULSO. */
	public static final String C_SEGMENTO_FATURA_AVULSO = "C_SEGMENTO_FATURA_AVULSO";

	/** The Constant ERRO_SEGMENTO_NAO_PERMITIDO_FATURA_AVULSO. */
	public static final String ERRO_SEGMENTO_NAO_PERMITIDO_FATURA_AVULSO = "ERRO_SEGMENTO_NAO_PERMITIDO_FATURA_AVULSO";

	/** The Constant ERRO_PONTO_CONSUMO_NAO_POSSUI_CONTRATO. */
	public static final String ERRO_PONTO_CONSUMO_NAO_POSSUI_CONTRATO = "ERRO_PONTO_CONSUMO_NAO_POSSUI_CONTRATO";

	/** The Constant ERRO_CONTRATO_NAO_POSSUI_ITEM_FATURAMENTO. */
	public static final String ERRO_CONTRATO_NAO_POSSUI_ITEM_FATURAMENTO = "ERRO_CONTRATO_NAO_POSSUI_ITEM_FATURAMENTO";

	/** The Constant ERRO_NAO_E_POSSIVEL_INCLUIR_FATURA_AVULSO_CORRETOR_VAZAO. */
	public static final String ERRO_NAO_E_POSSIVEL_INCLUIR_FATURA_AVULSO_CORRETOR_VAZAO = "ERRO_NAO_E_POSSIVEL_INCLUIR_FATURA_"
			+ "AVULSO_CORRETOR_VAZAO";

	/** The Constant C_CODIGO_LOCAL_ARMAZENAGEM_CLIENTE. */
	public static final String C_CODIGO_LOCAL_ARMAZENAGEM_CLIENTE = "C_CODIGO_LOCAL_ARMAZENAGEM_CLIENTE";

	/** The Constant ERRO_NEGOCIO_REGISTROS_NAO_PROCESSADOS. */
	public static final String ERRO_NEGOCIO_REGISTROS_NAO_PROCESSADOS = "ERRO_NEGOCIO_REGISTROS_NAO_PROCESSADOS";
	
	/** The Constant ERRO_NEGOCIO_REGISTROS_INCOMPLETOS. */
	public static final String ERRO_NEGOCIO_REGISTROS_INCOMPLETOS = "ERRO_NEGOCIO_REGISTROS_INCOMPLETOS";

	/** The Constant ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO. */
	public static final String ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO = "ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO";

	/** The Constant ERRO_ARQUIVO_EXTENSAO_INVALIDA. */
	public static final String ERRO_ARQUIVO_EXTENSAO_INVALIDA = "ERRO_ARQUIVO_EXTENSAO_INVALIDA";

	/** The Constant C_CHAMADO_ASSUNTO_AGV_PROGRAMACAO_CONSUMO. */
	public static final String C_CHAMADO_ASSUNTO_AGV_PROGRAMACAO_CONSUMO = "C_CHAMADO_ASSUNTO_AGV_PROGRAMACAO_CONSUMO";

	/** Carteira Cobrança. */

	public static final String C_ENT_CLASSE_CODIGO_CARTEIRA_COBRANCA = "C_ENT_CLASSE_CODIGO_CARTEIRA_COBRANCA";

	/** The Constant C_ENT_CLASSE_TIPO_CARTEIRA_COBRANCA. */
	public static final String C_ENT_CLASSE_TIPO_CARTEIRA_COBRANCA = "C_ENT_CLASSE_TIPO_CARTEIRA_COBRANCA";

	/** The Constant ERRO_NEGOCIO_CARTEIRA_COBRANCA_RELACIONADA. */
	public static final String ERRO_NEGOCIO_CARTEIRA_COBRANCA_RELACIONADA = "ERRO_NEGOCIO_CARTEIRA_COBRANCA_RELACIONADA";

	/** The Constant ERRO_NEGOCIO_CARTEIRA_COBRANCA_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CARTEIRA_COBRANCA_EXISTENTE = "ERRO_NEGOCIO_CARTEIRA_COBRANCA_EXISTENTE";

	/** Agência Bancária. */
	public static final String ERRO_NEGOCIO_CONTA_CORRENTE_EXISTENTE = "ERRO_NEGOCIO_CONTA_CORRENTE_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_AGENCIA_RELACIONADA. */
	public static final String ERRO_NEGOCIO_AGENCIA_RELACIONADA = "ERRO_NEGOCIO_AGENCIA_RELACIONADA";
	
	/** The Constant ERRO_NEGOCIO_AGENCIA_RELACIONADA. */
	public static final String ERRO_ROTA_NAO_INFORMADA = "ERRO_ROTA_NAO_INFORMADA";

	/** The Constant ERRO_NEGOCIO_AGENCIA_BANCARIA_EXISTENTE. */
	public static final String ERRO_NEGOCIO_AGENCIA_BANCARIA_EXISTENTE = "ERRO_NEGOCIO_AGENCIA_BANCARIA_EXISTENTE";

	/** Transportadora. */

	public static final String TRANSPORTADORA = "TRANSPORTADORA";

	/** The Constant ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA_TRANSPORTADORA. */
	public static final String ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA_TRANSPORTADORA = "ERRO_NEGOCIO_AOMENOS_UM_REGISTO_"
			+ "NA_LISTA_TRANSPORTADORA";

	/** The Constant ERRO_NEGOCIO_MOTORISTA_RESPONDIDO. */
	public static final String ERRO_NEGOCIO_MOTORISTA_RESPONDIDO = "ERRO_NEGOCIO_MOTORISTA_RESPONDIDO";

	/** The Constant ERRO_TRANSPORTADORA_INFORME_VEICULO_MOTORISTA. */
	public static final String ERRO_TRANSPORTADORA_INFORME_VEICULO_MOTORISTA = "ERRO_TRANSPORTADORA_INFORME_VEICULO_MOTORISTA";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_TRANSPORTADORA. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_TRANSPORTADORA = "ERRO_NAO_E_POSSIVEL_REMOVER_TRANSPORTADORA";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_VEICULO. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_VEICULO = "ERRO_NAO_E_POSSIVEL_REMOVER_VEICULO";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_MOTORISTA. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_MOTORISTA = "ERRO_NAO_E_POSSIVEL_REMOVER_MOTORISTA";

	/** The Constant ERRO_JA_EXISTE_TRANSPORTADORA. */
	public static final String ERRO_JA_EXISTE_TRANSPORTADORA = "ERRO_JA_EXISTE_TRANSPORTADORA";

	/** The Constant ERRO_NEGOCIO_CONTA_BANCARIA_RELACIONADA. */
	public static final String ERRO_NEGOCIO_CONTA_BANCARIA_RELACIONADA = "ERRO_NEGOCIO_CONTA_BANCARIA_RELACIONADA";

	/** The Constant C_PERIODICIDADE_PENALIDADE_MENSAL. */
	public static final String C_PERIODICIDADE_PENALIDADE_MENSAL = "C_PERIODICIDADE_PENALIDADE_MENSAL";

	/** The Constant C_PERIODICIDADE_PENALIDADE_TRIMESTRAL. */
	public static final String C_PERIODICIDADE_PENALIDADE_TRIMESTRAL = "C_PERIODICIDADE_PENALIDADE_TRIMESTRAL";

	/** The Constant C_PERIODICIDADE_PENALIDADE_ANUAL. */
	public static final String C_PERIODICIDADE_PENALIDADE_ANUAL = "C_PERIODICIDADE_PENALIDADE_ANUAL";

	/** The Constant ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO. */
	public static final String ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO = "ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO";

	/**
	 * The Constant ERRO_NAO_POSSIVEL_REMOVER_QUADRAFACE_ASSOCIADA_PONTO_CONSUMO.
	 */
	public static final String ERRO_NAO_POSSIVEL_REMOVER_QUADRAFACE_ASSOCIADA_PONTO_CONSUMO = "ERRO_NAO_POSSIVEL_REMOVER_QUADRAFACE_"
			+ "ASSOCIADA_PONTO_CONSUMO";

	/** The Constant ERRO_NEGOCIO_FATURA_SERIE_NAO_CADASTRADA_COM_FILTROS. */
	public static final String ERRO_NEGOCIO_FATURA_SERIE_NAO_CADASTRADA_COM_FILTROS = "ERRO_NEGOCIO_FATURA_SERIE_NAO_CADASTRADA_"
			+ "COM_FILTROS";

	/** The Constant ERRO_NUMERO_EMPENHO_CLIENTE_NAO_PUBLICO. */
	public static final String ERRO_NUMERO_EMPENHO_CLIENTE_NAO_PUBLICO = "ERRO_NUMERO_EMPENHO_CLIENTE_NAO_PUBLICO";

	/**
	 * The Constant
	 * PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO.
	 */
	public static final String PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO = "QUANTIDADE_CASAS_DECIMAIS_"
			+ "APRESENTACAO_CONSUMO_APURADO";

	/** The Constant ERRO_CLIENTE_NAO_INFORMADO. */
	public static final String ERRO_CLIENTE_NAO_INFORMADO = "ERRO_CLIENTE_NAO_INFORMADO";

	/** The Constant ERRO_CRONOGRAMA_DE_FATURAMENTO_NAO_ENCONTRADO. */
	public static final String ERRO_CRONOGRAMA_DE_FATURAMENTO_NAO_ENCONTRADO = "ERRO_CRONOGRAMA_DE_FATURAMENTO_NAO_ENCONTRADO";

	/** The Constant ERRO_CRONOGRAMA_NENHUMA_DATA_ATIVIDADE_INFORMADA. */
	public static final String ERRO_CRONOGRAMA_NENHUMA_DATA_ATIVIDADE_INFORMADA = "ERRO_CRONOGRAMA_NENHUMA_DATA_ATIVIDADE_INFORMADA";

	/** The Constant PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA. */
	public static final String PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA = "ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA";

	/** The Constant PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA. */
	public static final String PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA = "EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA";

	/** The Constant PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL. */
	public static final String PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL = "PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL";

	/** The Constant PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO. */
	public static final String PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO = "COBRANCA_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO";

	/** The Constant PARAMETRO_PARTICIPA_CONVENIO_ECARTAS. */
	public static final String PARAMETRO_PARTICIPA_CONVENIO_ECARTAS = "PARTICIPA_CONVENIO_ECARTAS";

	/** The Constant PARAMETRO_NUMERO_CONTRATO_ECARTAS. */
	public static final String PARAMETRO_NUMERO_CONTRATO_ECARTAS = "NUMERO_CONTRATO_ECARTAS";

	/** The Constant PARAMETRO_NUMERO_CARTAO_POSTAL_ECARTAS. */
	public static final String PARAMETRO_NUMERO_CARTAO_POSTAL_ECARTAS = "NUMERO_CARTAO_POSTAL_ECARTAS";

	/** The Constant PARAMETRO_QUANTIDADE_FATURA_POR_LOTE_ECARTAS. */
	public static final String PARAMETRO_QUANTIDADE_FATURA_POR_LOTE_ECARTAS = "QUANTIDADE_FATURA_POR_LOTE_ECARTAS";

	/** The Constant ERRO_ATIVIDADE_SISTEMA_EMAIL_REMETENTE. */
	public static final String ERRO_ATIVIDADE_SISTEMA_EMAIL_REMETENTE = "ERRO_ATIVIDADE_SISTEMA_EMAIL_REMETENTE";

	/** The Constant C_IMOVEL_MODALIDADE_MEDICAO_INDIVIDUAL. */
	public static final String C_IMOVEL_MODALIDADE_MEDICAO_INDIVIDUAL = "C_IMOVEL_MODALIDADE_MEDICAO_INDIVIDUAL";

	/** The Constant C_IMOVEL_MODALIDADE_MEDICAO_COLETIVA. */
	public static final String C_IMOVEL_MODALIDADE_MEDICAO_COLETIVA = "C_IMOVEL_MODALIDADE_MEDICAO_COLETIVA";

	/** The Constant ERRO_NEGOCIO_ANO_MES_INVALIDO. */
	public static final String ERRO_NEGOCIO_ANO_MES_INVALIDO = "ERRO_NEGOCIO_ANO_MES_INVALIDO";

	/** The Constant ERRO_NEGOCIO_DATA_LEITURA_OBRIGAROTIA. */
	public static final String ERRO_NEGOCIO_DATA_LEITURA_OBRIGAROTIA = "ERRO_NEGOCIO_DATA_LEITURA_OBRIGAROTIA";

	/** The Constant ERRO_CAMPO_OBRIGATORIO_MIGRACAO_CONTRATO. */
	public static final String ERRO_CAMPO_OBRIGATORIO_MIGRACAO_CONTRATO = "ERRO_CAMPO_OBRIGATORIO_MIGRACAO_CONTRATO";

	/** The Constant SUCESSO_ENTIDADE_MIGRACAO_CONTRATO. */
	public static final String SUCESSO_ENTIDADE_MIGRACAO_CONTRATO = "SUCESSO_ENTIDADE_MIGRACAO_CONTRATO";

	/** The Constant ALERTA_ENTIDADE_MIGRACAO_CONTRATO. */
	public static final String ALERTA_ENTIDADE_MIGRACAO_CONTRATO = "ALERTA_ENTIDADE_MIGRACAO_CONTRATO";

	/** The Constant ERRO_MEDIDA_MAXIMA_MENOR. */
	public static final String ERRO_MEDIDA_MAXIMA_MENOR = "ERRO_MEDIDA_MAXIMA_MENOR";

	// Constantes de layout de impressão de documentos

	/** The Constant AVISO_CORTE. */
	public static final String AVISO_CORTE = "AVISO_CORTE";

	/** The Constant NOTIFICACAO_CORTE. */
	public static final String NOTIFICACAO_CORTE = "NOTIFICACAO_CORTE";
	
	/** The Constant NOTIFICACAO_NEGATIVACAO. */
	public static final String NOTIFICACAO_NEGATIVACAO = "NOTIFICACAO_NEGATIVACAO";

	/** The Constant RELATORIO_NOTA_FISCAL_FATURA. */
	public static final String RELATORIO_NOTA_FISCAL_FATURA = "RELATORIO_NOTA_FISCAL_FATURA";

	/** The Constant RELATORIO_VERSO_NOTA_FISCAL_FATURA. */
	public static final String RELATORIO_VERSO_NOTA_FISCAL_FATURA = "RELATORIO_VERSO_NOTA_FISCAL_FATURA";

	/** The Constant RELATORIO_COMPLEMENTO_NOTA_FISCAL_FATURA. */
	public static final String RELATORIO_COMPLEMENTO_NOTA_FISCAL_FATURA = "RELATORIO_COMPLEMENTO_NOTA_FISCAL_FATURA";
	
	/** The Constant RELATORIO_PROTOCOLO. */
	public static final String RELATORIO_PROTOCOLO = "RELATORIO_PROTOCOLO";

	/** The Constant ACESSO_AGENCIA_VIRTUAL. */
	// parametros para agencia virtual
	public static final String ACESSO_AGENCIA_VIRTUAL = "ACESSO_AGENCIA_VIRTUAL";

	/** The Constant TAMANHO_MAXIMO_SENHA_AGENCIA. */
	public static final String TAMANHO_MAXIMO_SENHA_AGENCIA = "TAMANHO_MAXIMO_SENHA_AGENCIA";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_COMERCIAL = "SEGMENTO_COMERCIAL";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_VEICULAR = "SEGMENTO_VEICULAR";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_INDUSTRIAL = "SEGMENTO_INDUSTRIAL";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_RESIDENCIAL = "SEGMENTO_RESIDENCIAL";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_PUBLICO = "SEGMENTO_PUBLICO";

	public static final String PARAMETRO_MODULO_GERAL_SEGMENTO_COGERACAO = "SEGMENTO_COGERACAO";

	/** The Constant TAMANHO_MINIMO_SENHA_AGENCIA. */
	public static final String TAMANHO_MINIMO_SENHA_AGENCIA = "TAMANHO_MINIMO_SENHA_AGENCIA";

	/** The Constant CODIGO_CARACTERE_MAIUSCULO_AGENCIA. */
	public static final String CODIGO_CARACTERE_MAIUSCULO_AGENCIA = "CODIGO_CARACTERE_MAIUSCULO_AGENCIA";

	/** The Constant CODIGO_CARACTERE_MINUSCULO_AGENCIA. */
	public static final String CODIGO_CARACTERE_MINUSCULO_AGENCIA = "CODIGO_CARACTERE_MINUSCULO_AGENCIA";

	/** The Constant CODIGO_CARACTERE_NUMERO. */
	public static final String CODIGO_CARACTERE_NUMERO = "CODIGO_CARACTERE_NUMERO";

	/** The Constant C_ENTIDADE_CLASSE_TIPO_MANIFESTACAO_CHAMADO. */
	public static final String C_ENTIDADE_CLASSE_TIPO_MANIFESTACAO_CHAMADO = "C_ENTIDADE_CLASSE_TIPO_MANIFESTACAO_CHAMADO";

	/** The Constant P_CHAMADO_MANIFESTACAO_OBRIGATORIO. */
	public static final String P_CHAMADO_MANIFESTACAO_OBRIGATORIO = "P_CHAMADO_MANIFESTACAO_OBRIGATORIO";
	
	/** The Constant P_PASSAPORTE_CLIENTE. */
	public static final String P_PASSAPORTE_CLIENTE = "P_PASSAPORTE_CLIENTE";

	/** The Constant NEGATIVACAO_MANIFESTACAO_OBRIGATORIO. */
	public static final String NEGATIVACAO_OBRIGATORIO = "NEGATIVACAO_OBRIGATORIO";
	
	/** The Constant TELEFONE_OBRIGATORIO_SOLICITACAO_GAS. */
	public static final String TELEFONE_OBRIGATORIO_SOLICITACAO_GAS = "TELEFONE_OBRIGATORIO_SOLICITACAO_GAS";

	public static final String DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO = "DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO";

	public static final String DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO";

	public static final String DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO";

	public static final String DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO";

	public static final String DATA_LIMITE_RELIGACAO_GAS = "DATA_LIMITE_RELIGACAO_GAS";
	
	public static final String NUMERO_DIAS_CHECAGEM_CORTE = "NUMERO_DIAS_CHECAGEM_CORTE";
	
	public static final String PRAZO_MINIMO_CRITICIDADE = "PRAZO_MINIMO_CRITICIDADE";
	
	public static final String PRAZO_MAXIMO_CRITICIDADE = "PRAZO_MAXIMO_CRITICIDADE";
	
	public static final String VALOR_MINIMO_CRITICIDADE = "VALOR_MINIMO_CRITICIDADE";
	
	public static final String VALOR_MEDIO_CRITICIDADE = "VALOR_MEDIO_CRITICIDADE";
	
	public static final String VALOR_MAXIMO_CRITICIDADE = "VALOR_MAXIMO_CRITICIDADE";

	/** The Constant CREDENCIAL_SMS. */
	// parametros para envio de sms
	public static final String CREDENCIAL_SMS = "CREDENCIAL_SMS";

	/** The Constant PROJETO_SMS. */
	public static final String PROJETO_SMS = "PROJETO_SMS";

	/** The Constant USUARIO_AUXILIAR_SMS. */
	public static final String USUARIO_AUXILIAR_SMS = "USUARIO_AUXILIAR_SMS";

	/** The Constant MENSAGEN_SMS. */
	public static final String MENSAGEN_SMS = "MENSAGEN_SMS";

	/** The Constant PROXY_HOST_SMS. */
	public static final String PROXY_HOST_SMS = "PROXY_HOST_SMS";

	/** The Constant PROXY_PORTA_SMS. */
	public static final String PROXY_PORTA_SMS = "PROXY_PORTA_SMS";

	/** The Constant ENVIA_SMS. */
	public static final String ENVIA_SMS = "ENVIA_SMS";

	// dias faturmanto estendido

	/** The Constant QTD_DIAS_FIXO_ESTENDIDO. */
	public static final String QTD_DIAS_FIXO_ESTENDIDO = "QTD_DIAS_FIXO_ESTENDIDO";
	
	/** The Constant QTD_DIAS_FIXO_ESTENDIDO. */
	public static final String PARAMETRO_QUANTIDADE_MESES_PRESCRICAO = "P_QUANTIDADE_MESES_PRESCRICAO";
	
	/** The Constrant CALCULAR_COLETOR_TARIFA_DIARIA. */
	public static final String PARAMETRO_CALCULAR_COLETOR_TARIFA_DIARIA = "P_CALCULAR_COLETOR_TARIFA_DIARIA";
	
	/** The Constrant PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO. */
	public static final String PARAMETRO_PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO = "P_PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO";
	
	/** The Constrant QUANTIDADE_REFERENCIAS_CALCULO_MEDIA. */
	public static final String PARAMETRO_QUANTIDADE_REFERENCIAS_CALCULO_MEDIA = "P_QUANTIDADE_REFERENCIAS_CALCULO_MEDIA";
	
	/** The Constant C_SERVICO_TIPO_HABILITACAO. */
	public static final String C_SERVICO_TIPO_HABILITACAO = "C_SERVICO_TIPO_HABILITACAO";

	/** The Constant ERRO_PREVISAO_CADASTRADA. */
	public static final String ERRO_PREVISAO_CADASTRADA = "ERRO_PREVISAO_CADASTRADA";

	/** The Constant PREVISAO_CAPTACAO. */
	public static final String PREVISAO_CAPTACAO = "PREVISAO_CAPTACAO";

	/** The Constant ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO. */
	public static final String ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO = "ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO";

	/** The Constant C_OPERACAO_SISTEMA_IMPORTAR_LEITURA_UPLOAD. */
	public static final String C_OPERACAO_SISTEMA_IMPORTAR_LEITURA_UPLOAD = "C_OPERACAO_SISTEMA_IMPORTAR_LEITURA_UPLOAD";

	/** The Constant PARAMETRO_FATURA_VALOR_ZERO. */
	public static final String PARAMETRO_FATURA_VALOR_ZERO = "P_FATURA_VALOR_ZERO";

	/** The Constant PARAMETRO_FATURA_VALOR_ZERO. */
	public static final String PARAMETRO_FATURA_CONSUMO_ZERO = "P_FATURA_CONSUMO_ZERO";
	
	/** The Constant PARAMETRO_MEDIR_ROTA_FISCALIZACAO. */
	public static final String PARAMETRO_MEDIR_ROTA_FISCALIZACAO = "P_PERMITE_MEDIR_ROTA_FISCALIZACAO";

	/** The Constant SITUACAO_PAGAMENTO_FATURA_QUITADO. */
	public static final String SITUACAO_PAGAMENTO_FATURA_QUITADO = "SITUACAO_PAGAMENTO_FATURA_QUITADO";

	/**
	 * The Constant SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO.
	 */
	public static final String SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO = "SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO";

	/**
	 * The Constant SITUACAO_PAGAMENTO_FATURA_PENDENTE.
	 */
	public static final String SITUACAO_PAGAMENTO_FATURA_PENDENTE = "SITUACAO_PAGAMENTO_FATURA_PENDENTE";

	/** Constante ERRO_NEGOCIO_REMOVER_FATURAS. */
	public static final String ERRO_NEGOCIO_REMOVER_FATURAS = "ERRO_NEGOCIO_REMOVER_FATURAS";

	/** Constante ERRO_NEGOCIO_CANCELAR_FATURAS. */
	public static final String ERRO_NEGOCIO_CANCELAR_FATURAS = "ERRO_NEGOCIO_CANCELAR_FATURAS";

	/** Constante ERRO_NEGOCIO_REMOVER_FATURAS_CRONOGRAMA_ENCERRADO. */
	public static final String ERRO_NEGOCIO_REMOVER_FATURAS_CRONOGRAMA_ENCERRADO = "ERRO_NEGOCIO_REMOVER_FATURAS_CRONOGRAMA_ENCERRADO";

	/** Constante ERRO_NEGOCIO_CANCELAR_FATURAS_CRONOGRAMA_ENCERRADO. */
	public static final String ERRO_NEGOCIO_CANCELAR_FATURAS_CRONOGRAMA_ENCERRADO = "ERRO_NEGOCIO_CANCELAR_FATURAS_CRONOGRAMA_ENCERRADO";

	/** The Constant ERRO_EMAIL_PRINCIPAL_OBRIGATORIO. */
	public static final String ERRO_EMAIL_PRINCIPAL_OBRIGATORIO = "ERRO_EMAIL_PRINCIPAL_OBRIGATORIO";

	/** The Constant HORA_MAXIMA_PROGRAMAR_SOLICITACAO_CONSUMO. */
	public static final String HORA_MAXIMA_PROGRAMAR_SOLICITACAO_CONSUMO = "HORA_MAXIMA_PROGRAMAR_SOLICITACAO_CONSUMO";

	/** The Constant ERRO_DESFAZER_GERAR_DADOS_LEITURA. */
	public static final String ERRO_DESFAZER_GERAR_DADOS_LEITURA = "ERRO_DESFAZER_GERAR_DADOS_LEITURA";

	/** The Constant ERRO_DESFAZER_REGISTRAR_LEITURA. */
	public static final String ERRO_DESFAZER_REGISTRAR_LEITURA = "ERRO_DESFAZER_REGISTRAR_LEITURA";

	/** The Constant ERRO_DESFAZER_CONSISTIR_LEITURA. */
	public static final String ERRO_DESFAZER_CONSISTIR_LEITURA = "ERRO_DESFAZER_CONSISTIR_LEITURA";

	/** The Constant ERRO_DESFAZER_DADOS_SUPERVISORIO. */
	public static final String ERRO_DESFAZER_DADOS_SUPERVISORIO = "ERRO_DESFAZER_DADOS_SUPERVISORIO";

	/** The Constant ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_DADOS. */
	public static final String ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_DADOS = "ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_DADOS";

	/** The Constant ERRO_DESFAZER_CONSOLIDAR_SUPERVISORIO. */
	public static final String ERRO_DESFAZER_CONSOLIDAR_SUPERVISORIO = "ERRO_DESFAZER_CONSOLIDAR_SUPERVISORIO";

	/** The Constant ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_PONTO_CONSUMO. */
	public static final String ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_PONTO_CONSUMO = "ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_PONTO_CONSUMO";

	/** The Constant MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO. */
	public static final String MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO = "MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO";

	/** The Constant QUANTIDADE_MESES_INCLUSAO_PROGRAMACAO_CONSUMO. */
	public static final String QUANTIDADE_MESES_INCLUSAO_PROGRAMACAO_CONSUMO = "QUANTIDADE_MESES_INCLUSAO_PROGRAMACAO_CONSUMO";

	/** The Constant DIA_LIMITE_PROGRAMACAO_CONSUMO. */
	public static final String DIA_LIMITE_PROGRAMACAO_CONSUMO = "DIA_LIMITE_PROGRAMACAO_CONSUMO";

	/** The Constant HORA_MAXIMA_PROGRAMACAO_CONSUMO_DIARIA. */
	public static final String HORA_MAXIMA_PROGRAMACAO_CONSUMO_DIARIA = "HORA_MAXIMA_PROGRAMACAO_CONSUMO_DIARIA";

	/** The Constant ERRO_SELECIONE_PONTO_DOAR. */
	public static final String ERRO_SELECIONE_PONTO_DOAR = "ERRO_SELECIONE_PONTO_DOAR";

	/** The Constant ERRO_SELECIONE_PONTO_RECEBER. */
	public static final String ERRO_SELECIONE_PONTO_RECEBER = "ERRO_SELECIONE_PONTO_RECEBER";

	/** The Constant ERRO_VALOR_QNR_MAIOR_SALDO. */
	public static final String ERRO_VALOR_QNR_MAIOR_SALDO = "ERRO_VALOR_QNR_MAIOR_SALDO";

	/** The Constant ERRO_VALOR_QPNR_MAIOR_SALDO. */
	public static final String ERRO_VALOR_QPNR_MAIOR_SALDO = "ERRO_VALOR_QPNR_MAIOR_SALDO";

	/** The Constant ERRO_CONTRATO_NAO_ENCERRADO. */
	public static final String ERRO_CONTRATO_NAO_ENCERRADO = "ERRO_CONTRATO_NAO_ENCERRADO";

	/** The Constant ERRO_NAO_POSSIVEL_MIGRAR_ZERADO. */
	public static final String ERRO_NAO_POSSIVEL_MIGRAR_ZERADO = "ERRO_NAO_POSSIVEL_MIGRAR_ZERADO";

	/** The Constant ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE. */
	public static final String ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE = "ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_"
			+ "CONTRATO_SEM_MODALIDADE";

	/**
	 * The Constant ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE_TOP.
	 */
	public static final String ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE_TOP = "ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_"
			+ "CONTRATO_SEM_MODALIDADE_TOP";

	/** The Constant ERRO_NAO_POSSIVEL_MIGRAR_NEGATIVO. */
	public static final String ERRO_NAO_POSSIVEL_MIGRAR_NEGATIVO = "ERRO_NAO_POSSIVEL_MIGRAR_NEGATIVO";

	/** The Constant ERRO_PONTO_CONSUMO_NAO_POSSUI_MODALIDADE. */
	public static final String ERRO_PONTO_CONSUMO_NAO_POSSUI_MODALIDADE = "ERRO_PONTO_CONSUMO_NAO_POSSUI_MODALIDADE";

	/** The Constant ERRO_SELECIONE_UMA_MODALIDADE. */
	public static final String ERRO_SELECIONE_UMA_MODALIDADE = "ERRO_SELECIONE_UMA_MODALIDADE";

	/** The Constant ERRO_VOLUME_FORNECIMENTO_MAIOR_SOMAQDC. */
	public static final String ERRO_VOLUME_FORNECIMENTO_MAIOR_SOMAQDC = "ERRO_VOLUME_FORNECIMENTO_MAIOR_SOMAQDC";

	/** The Constant C_TABELA_PRECOS_VIGENTE_ULTIDO_DIA_PERIODO_PENALIDADE. */
	public static final String C_TABELA_PRECOS_VIGENTE_ULTIDO_DIA_PERIODO_PENALIDADE = "C_TABELA_PRECOS_VIGENTE_ULTIDO_DIA_"
			+ "PERIODO_PENALIDADE";

	/** The Constant C_TABELA_PRECOS_VIGENTE_MOMENTO_COBRANCA. */
	public static final String C_TABELA_PRECOS_VIGENTE_MOMENTO_COBRANCA = "C_TABELA_PRECOS_VIGENTE_MOMENTO_COBRANCA";

	/** The Constant C_TARIFA_MEDIA_PERIODO. */
	public static final String C_TARIFA_MEDIA_PERIODO = "C_TARIFA_MEDIA_PERIODO";

	/** The Constant ERRO_VOLUMERECUPERAVEL_MAIOR_QPNR. */
	public static final String ERRO_VOLUMERECUPERAVEL_MAIOR_QPNR = "ERRO_VOLUMERECUPERAVEL_MAIOR_QPNR";

	/** The Constant ERRO_RUBRICA_OBRIGATORIA. */
	public static final String ERRO_RUBRICA_OBRIGATORIA = "ERRO_RUBRICA_OBRIGATORIA";

	/** The Constant ERRO_RUBRICA_NAO_POSSUI_ITEM_FATURA. */
	public static final String ERRO_RUBRICA_NAO_POSSUI_ITEM_FATURA = "ERRO_RUBRICA_NAO_POSSUI_ITEM_FATURA";

	/** The Constant C_APLICACAO_QDC_CASCATA_TARIFARIA. */
	public static final String C_APLICACAO_QDC_CASCATA_TARIFARIA = "C_APLICACAO_QDC_CASCATA_TARIFARIA";

	/** The Constant C_APLICACAO_VOLUME_PENALIDADE_CASCATA_TARIFARIA. */
	public static final String C_APLICACAO_VOLUME_PENALIDADE_CASCATA_TARIFARIA = "C_APLICACAO_VOLUME_PENALIDADE_CASCATA_TARIFARIA";

	/** The Constant C_TIPO_GRUPO_ECONOMICO. */
	public static final String C_TIPO_GRUPO_ECONOMICO = "C_TIPO_GRUPO_ECONOMICO";

	/** The Constant AGRUPAR_VOLUMES_IMPRESSAO_FATURA_MAIS_TARIFA_CICLO. */
	public static final String AGRUPAR_VOLUMES_IMPRESSAO_FATURA_MAIS_TARIFA_CICLO = "AGRUPAR_VOLUMES_IMPRESSAO_FATURA_MAIS_TARIFA_CICLO";

	/** The Constant C_RUBRICA_CONSUMO_GAS. */
	public static final String C_RUBRICA_CONSUMO_GAS = "C_RUBRICA_CONSUMO_GAS";

	/** The Constant C_RUBRICA_CREDITO_GAS_AJUSTE_PCS. */
	public static final String C_RUBRICA_CREDITO_GAS_AJUSTE_PCS = "C_RUBRICA_CREDITO_GAS_AJUSTE_PCS";

	/** The Constant C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS. */
	public static final String C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS = "C_RUBRICA_CREDITO_MARGEM_AJUSTE_PCS";

	/** The Constant C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS. */
	public static final String C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS = "C_RUBRICA_CREDITO_TRANSPORTE_AJUSTE_PCS";

	/** The Constant C_RUBRICA_DEBITO_GAS_AJUSTE_PCS. */
	public static final String C_RUBRICA_DEBITO_GAS_AJUSTE_PCS = "C_RUBRICA_DEBITO_GAS_AJUSTE_PCS";

	/** The Constant C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS. */
	public static final String C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS = "C_RUBRICA_DEBITO_MARGEM_AJUSTE_PCS";

	/** The Constant C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS. */
	public static final String C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS = "C_RUBRICA_DEBITO_TRANSPORTE_AJUSTE_PCS";

	/** The Constant C_RUBRICA_MULTA_ATRASO. */
	public static final String C_RUBRICA_MULTA_ATRASO = "C_RUBRICA_MULTA_ATRASO";

	/** The Constant C_RUBRICA_CREDITO_PAGTO_MAIOR. */
	public static final String C_RUBRICA_CREDITO_PAGTO_MAIOR = "C_RUBRICA_CREDITO_PAGTO_MAIOR";

	/** The Constant C_RUBRICA_DEBITO_PAGTO_MENOR. */
	public static final String C_RUBRICA_DEBITO_PAGTO_MENOR = "C_RUBRICA_DEBITO_PAGTO_MENOR";

	/** The Constant C_RUBRICA_DESPESA. */
	public static final String C_RUBRICA_DESPESA = "C_RUBRICA_DESPESA";

	/** The Constant C_RUBRICA_JUROS_MORA. */
	public static final String C_RUBRICA_JUROS_MORA = "C_RUBRICA_JUROS_MORA";

	/** The Constant C_RUBRICA_CREDITO_VOLUME. */
	public static final String C_RUBRICA_CREDITO_VOLUME = "C_RUBRICA_CREDITO_VOLUME";

	/** The Constant C_RUBRICA_DESCONTO_ICMS_PRODESIN. */
	public static final String C_RUBRICA_DESCONTO_ICMS_PRODESIN = "C_RUBRICA_DESCONTO_ICMS_PRODESIN";

	/** The Constant C_RUBRICA_PARCELAMENTO_DEBITO. */
	public static final String C_RUBRICA_PARCELAMENTO_DEBITO = "C_RUBRICA_PARCELAMENTO_DEBITO";

	/** The Constant C_RUBRICA_MULTA_RECISORIA. */
	public static final String C_RUBRICA_MULTA_RECISORIA = "C_RUBRICA_MULTA_RECISORIA";

	/** The Constant C_RUBRICA_ICMS_SUBSTITUTO. */
	public static final String C_RUBRICA_ICMS_SUBSTITUTO = "C_RUBRICA_ICMS_SUBSTITUTO";

	/** The Constant C_EVENTO_COMERCIAL_DESPESA. */
	public static final String C_EVENTO_COMERCIAL_DESPESA = "C_EVENTO_COMERCIAL_DESPESA";

	/** The Constant ALTERA_VENCIMENTO_FATURA_AUTOMATICO. */
	public static final String ALTERA_VENCIMENTO_FATURA_AUTOMATICO = "ALTERA_VENCIMENTO_FATURA_AUTOMATICO";

	/** The Constant ALTERA_VENCIMENTO_FATURA_DEBITO_ATRASO. */
	public static final String ALTERA_VENCIMENTO_FATURA_DEBITO_ATRASO = "ALTERA_VENCIMENTO_FATURA_DEBITO_ATRAZO";

	/** The Constant PRAZO_ALTERA_VENCIMENTO_FATURA_AUTOMATICO. */
	public static final String PRAZO_ALTERA_VENCIMENTO_FATURA_AUTOMATICO = "PRAZO_ALTERA_VENCIMENTO_FATURA_AUTOMATICO";

	/** The Constant ERRO_MENSAGEM_FATURA_SEM_DOCUMENTO_COBRANCA. */
	public static final String ERRO_MENSAGEM_FATURA_SEM_DOCUMENTO_COBRANCA = "ERRO_MENSAGEM_FATURA_SEM_DOCUMENTO_COBRANCA";

	/** Constantes para CROMATOGRAFIA. */

	public static final String SOMATORIO_DOS_COMPONENTES = "Soma dos Componentes";

	/** Constante DATA_CROMATOGRAFIA. */
	public static final String DATA_CROMATOGRAFIA = "Data da Cromatografia";

	/** Constante PCS_CROMATOGRAFIA. */
	public static final String PCS_CROMATOGRAFIA = "PCS da Cromatografia";

	/** Constante FORMA_APURACAO_VOLUME_PARADA_PROGRAMADA. */
	public static final String FORMA_APURACAO_VOLUME_PARADA_PROGRAMADA = "Forma de Apuração do Volume de Parada Programada";

	/** Constante FORMA_APURACAO_VOLUME_FALHA_FORNECIMENTO. */
	public static final String FORMA_APURACAO_VOLUME_FALHA_FORNECIMENTO = "Forma de Apuração do Volume de Falha de Fornecimento";

	/** Constante FORMA_APURACAO_VOLUME_CASO_FORTUITO. */
	public static final String FORMA_APURACAO_VOLUME_CASO_FORTUITO = "Forma de Apuração do Volume de Caso Fortuito ou Força Maior";

	/** The Constant FATOR_COMPRESSIBILIDADE_CROMATOGRAFIA. */
	public static final String FATOR_COMPRESSIBILIDADE_CROMATOGRAFIA = "Fator de Compressibilidade da Cromatografia";

	/** The Constant PRESSAO_CROMATOGRAFIA. */
	public static final String PRESSAO_CROMATOGRAFIA = "Pressão da Cromatografia";

	/** The Constant TEMPERATURA_CROMATOGRAFIA. */
	public static final String TEMPERATURA_CROMATOGRAFIA = "Temperatura da Cromatografia";

	/** The Constant CITY_GATE_CROMATOGRAFIA. */
	public static final String CITY_GATE_CROMATOGRAFIA = "City Gate";

	/** The Constant SEPARADOR_CITY_GATE. */
	public static final String SEPARADOR_CITY_GATE = ", ";

	/** The Constant ROTULO_SEM_CITY_GATE. */
	public static final String ROTULO_SEM_CITY_GATE = "Sem City Gate";

	/** The Constant LIMITE_APROVACAO_PROG_CONSUMO_MENSAL. */
	public static final String LIMITE_APROVACAO_PROG_CONSUMO_MENSAL = "LIMITE_APROVACAO_PROG_CONSUMO_MENSAL";

	/** The Constant MENSAGEM_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO. */
	public static final String MENSAGEM_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO = "MENSAGEM_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO";

	/** The Constant C_SERVICO_TIPO_CORTE. */
	public static final String C_SERVICO_TIPO_CORTE = "C_SERVICO_TIPO_CORTE";

	/** The Constant C_SERVICO_TIPO_RELIGACAO. */
	public static final String C_SERVICO_TIPO_RELIGACAO = "C_SERVICO_TIPO_RELIGACAO";

	/** **********************************. */

	public static final String SEGMENTO = "Segmento";

	/** Constante para rótulo segmento */
	public static final String SEGMENTO_ROTULO = "SEGMENTO_ROTULO";

	/** The Constant ANO_REFERENCIA. */
	public static final String ANO_REFERENCIA = "Ano Referência";

	/** The Constant ERRO_NAO_E_POSSIVEL_REMOVER_RAMO_ATIVIDADE. */
	public static final String ERRO_NAO_E_POSSIVEL_REMOVER_RAMO_ATIVIDADE = "ERRO_NAO_E_POSSIVEL_REMOVER_RAMO_ATIVIDADE";

	/** The Constant TIPO_APURACAO. */
	public static final String TIPO_APURACAO = "Cálculo Cobrança e Recuperação";

	/** The Constant PRECO_COBRANCA. */
	public static final String PRECO_COBRANCA = "Tabela de Preços para Cobrança";

	/** The Constant PARADA_PROGRAMADA. */
	public static final String PARADA_PROGRAMADA = "Apuração Vol Parada Programada";

	/** The Constant PARADA_NAO_PROGRAMADA. */
	public static final String PARADA_NAO_PROGRAMADA = "Apuração Vol Parada Não Progra";

	/** The Constant PARADA_CASO_FORTUITO. */
	public static final String PARADA_CASO_FORTUITO = "Apuração Vol Caso Fortuito";

	/** The Constant PARADA_FALHA_FORNECIMENTO. */
	public static final String PARADA_FALHA_FORNECIMENTO = "Apuração Vol de Falha Fornec";

	/** The Constant TIPO_AGRUPAMENTO. */
	public static final String TIPO_AGRUPAMENTO = "Tipo de agrupamento contrato";

	/** The Constant NOTA_DEBITO_CREDITO_PRORROGADA_COM_SUCESSO. */
	public static final String NOTA_DEBITO_CREDITO_PRORROGADA_COM_SUCESSO = "NOTA_DEBITO_CREDITO_PRORROGADA_COM_SUCESSO";

	/**
	 * The Constant NOTA_DEBITO_CREDITO_DATA_PRORROGACAO_MAIOR_IGUAL_DATA_CORRENTE.
	 */
	public static final String NOTA_DEBITO_CREDITO_DATA_PRORROGACAO_MAIOR_IGUAL_DATA_CORRENTE
			= "NOTA_DEBITO_CREDITO_DATA_PRORROGACAO_MAIOR_IGUAL_DATA_CORRENTE";

	/** The Constant NOTA_DEBITO_CREDITO_SITUACAO_PAGAMENTO_QUITADA. */
	public static final String NOTA_DEBITO_CREDITO_SITUACAO_PAGAMENTO_QUITADA = "NOTA_DEBITO_CREDITO_SiTUACAO_PAGAMENTO_QUITADA";

	/** The Constant NOTA_DEBITO_CREDITO_QUITADO. */
	public static final String NOTA_DEBITO_CREDITO_QUITADO = "Quitado";

	/** The Constant ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE. */
	public static final String ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE = "ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE";
	/** The Constant ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE_BA. */
	public static final String ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE_BA = "ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE_BA";

	/** The Constant ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA. */
	public static final String ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA = "ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA";

	/** The Constant C_CABECALHO_ARQUIVO_TXT_IMPORTACAO_DADOS_MOVEIS_ALGAS. */
	public static final String C_CABECALHO_ARQUIVO_TXT_IMPORTACAO_DADOS_MOVEIS_ALGAS = "C_CABECALHO_ARQUIVO_TXT_IMPORTACAO_DADOS_MOVEIS_ALGAS";

	/** The Constant FUNCIONARIO_OBRIGATORIO. */
	/*
	 * ERRO NEGÓCIO AGENTE
	 */
	public static final String FUNCIONARIO_OBRIGATORIO = "Funcionário";

	/** The Constant ERRO_AGENTE_ESTA_SENDO_USANDO. */
	public static final String ERRO_AGENTE_ESTA_SENDO_USANDO = "ERRO_AGENTE_ESTA_SENDO_USANDO";

	/** The Constant ERRO_FUNCIONARIO_VINCULADO_OUTRO_AGENTE. */
	public static final String ERRO_FUNCIONARIO_VINCULADO_OUTRO_AGENTE = "ERRO_FUNCIONARIO_VINCULADO_OUTRO_AGENTE";

	/** The Constant ERRO_MUDANCA_TITULARIDADE_PREENCHIMENTO. */
	public static final String ERRO_MUDANCA_TITULARIDADE_PREENCHIMENTO = "ERRO_MUDANCA_TITULARIDADE_PREENCHIMENTO";

	/** Indica o Status do Imóvel. */
	public static final String C_IMOVEL_STATUS_PROSPECTADO = "C_IMOVEL_STATUS_PROSPECTADO";

	/** The Constant C_IMOVEL_STATUS_EM_PROSPECCAO. */
	public static final String C_IMOVEL_STATUS_EM_PROSPECCAO = "C_IMOVEL_STATUS_EM_PROSPECCAO";

	/** The Constant C_IMOVEL_STATUS. */
	public static final String C_IMOVEL_STATUS = "C_IMOVEL_STATUS";

	/** Levantamento de Mercado. */

	public static final String C_FORNECEDORES_OUTROS_COMBUSTIVEIS = "C_FORNECEDORES_OUTROS_COMBUSTIVEIS";

	/** The Constant C_APARELHOS_MAIOR_INDICE_CONSUMO. */
	public static final String C_APARELHOS_MAIOR_INDICE_CONSUMO = "C_APARELHOS_MAIOR_INDICE_CONSUMO";

	/** The Constant C_TIPOS_FONTES_ENERGETICAS. */
	public static final String C_TIPOS_FONTES_ENERGETICAS = "C_TIPOS_FONTES_ENERGETICAS";

	/** The Constant C_ESTADO_REDE_GLP. */
	public static final String C_ESTADO_REDE_GLP = "C_ESTADO_REDE_GLP";

	/** The Constant ERRO_NEGOCIO_APARELHO_EXISTENTE. */
	public static final String ERRO_NEGOCIO_APARELHO_EXISTENTE = "ERRO_NEGOCIO_APARELHO_EXISTENTE";

	/** The Constant ERRO_NEGOCIO_TIPO_COMBUSTIVEL_EXISTENTE. */
	public static final String ERRO_NEGOCIO_TIPO_COMBUSTIVEL_EXISTENTE = "ERRO_NEGOCIO_TIPO_COMBUSTIVEL_EXISTENTE";

	/** ERRO NEGÓCIO LEVANTAMENTO DE MERCADO. */

	public static final String ERRO_IMOVEL = "Imovel";

	/** The Constant ERRO_TIPO_COMBUSTIVEL. */
	public static final String ERRO_TIPO_COMBUSTIVEL = "Tipo Combustível";

	/** The Constant ERRO_REDE_MATERIAL. */
	public static final String ERRO_REDE_MATERIAL = "Material de Rede";

	/** The Constant ERRO_SEGMENTO. */
	public static final String ERRO_SEGMENTO = "Segmento";

	/** The Constant ERRO_TIPO_APARELHO. */
	public static final String ERRO_TIPO_APARELHO = "Aparelho";

	/** The Constant ERRO_AGENTE. */
	public static final String ERRO_AGENTE = "Agente";

	/** The Constant ERRO_REDE_ESTADO. */
	public static final String ERRO_REDE_ESTADO = "Estado da Rede";

	/** The Constant ERRO_FORNECEDOR. */
	public static final String ERRO_FORNECEDOR = "Fornecedor";

	/** The Constant ERRO_VIGENCIA_INICIO. */
	public static final String ERRO_VIGENCIA_INICIO = "Data Vigência Início";

	/** The Constant ERRO_VIGENCIA_FIM. */
	public static final String ERRO_VIGENCIA_FIM = "Data Vigência Final";

	/** The Constant ERRO_CONSUMO. */
	public static final String ERRO_CONSUMO = "Consumo";

	/** The Constant ERRO_PRECO. */
	public static final String ERRO_PRECO = "Preço";

	/** The Constant ERRO_PERIODICIDADE. */
	public static final String ERRO_PERIODICIDADE = "Periodicidade";

	/** The Constant ERRO_VENTILACAO. */
	public static final String ERRO_VENTILACAO = "Ventilação";

	/** The Constant ERRO_GERADOR. */
	public static final String ERRO_GERADOR = "Gerador";

	/** The Constant ERRO_UNIDADES_CONSUMIDORAS. */
	public static final String ERRO_UNIDADES_CONSUMIDORAS = "Un. Consumistas";

	/** The Constant ERRO_QUANTIDADE_UNIDADES_CONSUMIDORAS. */
	public static final String ERRO_QUANTIDADE_UNIDADES_CONSUMIDORAS = "Quantidade";

	/** The Constant ERRO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EXCLUIDO. */
	public static final String ERRO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EXCLUIDO = "ERRO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EXCLUIDO";

	/** The Constant ERRO_MODALIDADE_USO_OBRIGATORIO. */
	public static final String ERRO_MODALIDADE_USO_OBRIGATORIO = "Modalidade de Uso";

	/** The Constant ERRO_TIPO_COMBUSTIVEL_OBRIGATORIO. */
	public static final String ERRO_TIPO_COMBUSTIVEL_OBRIGATORIO = "Tipo Combustível";

	/** ERRO NEGÓCIO AGENDA VISITA AGENTE - Constantes. */
	public static final String ERRO_AGENDA_LM_DATA_VISITA = "Data Visita";

	/** The Constant ERRO_AGENDA_LM_HORA_VISITA. */
	public static final String ERRO_AGENDA_LM_HORA_VISITA = "Hora Visita";

	/** The Constant ERRO_AGENDA_LM_HORA_VISITA_INVALIDA. */
	public static final String ERRO_AGENDA_LM_HORA_VISITA_INVALIDA = "Hora Visita Inválida";

	/** The Constant DATA_MENOR_QUE_DATA_ATUAL. */
	public static final String DATA_MENOR_QUE_DATA_ATUAL = "DATA_MENOR_QUE_DATA_ATUAL";

	/** The Constant VIGENCIA_INICIO_MAIOR_VIGENCIA_FIM. */
	public static final String VIGENCIA_INICIO_MAIOR_VIGENCIA_FIM = "VIGENCIA_INICIO_MAIOR_VIGENCIA_FIM";

	/** Validação Hora. */

	public static final String ERRO_HORA_INVALIDA = "Hora Inválida";

	/** Validação Email Contato Imóvel - Módulo Proposta. */

	public static final String ERRO_EMAIL_CONTATO_IMOVEL = "ERRO_EMAIL_CONTATO_IMOVEL";

	/** The Constant SUCESSO_ENVIO_EMAIL. */
	public static final String SUCESSO_ENVIO_EMAIL = "SUCESSO_ENVIO_EMAIL";

	/** The Constant CLIENTE_ERRO_EMAIL. */
	public static final String CLIENTE_ERRO_EMAIL = "CLIENTE_ERRO_EMAIL";

	/** The Constant ERRO_CONTATO_IMOVEL_OBRIGATORIO. */
	public static final String ERRO_CONTATO_IMOVEL_OBRIGATORIO = "ERRO_CONTATO_IMOVEL_OBRIGATORIO";

	/** The Constant ERRO_CONECTAR_SERVIDOR_EMAIL. */
	public static final String ERRO_CONECTAR_SERVIDOR_EMAIL = "ERRO_CONECTAR_SERVIDOR_EMAIL";

	/** ERRO NEGÓCIO LevantamentoMercadoTipoCombustivel. */

	public static final String TIPO_COMBUSTIVEL = "Tipo de Combustível";

	/** The Constant PRECO. */
	public static final String PRECO = "Preço";

	/** The Constant FORNECEDOR. */
	public static final String FORNECEDOR = "Fornecedor";

	/** The Constant MODALIDA_USO. */
	public static final String MODALIDA_USO = "Modalidade de Uso";

	/** The Constant ESTADO_REDE. */
	public static final String ESTADO_REDE = "Estado da Rede";

	/** The Constant CENTRAL. */
	public static final String CENTRAL = "Central";

	/** The Constant MATERIAL_REDE. */
	public static final String MATERIAL_REDE = "Material de Rede";

	/** The Constant VENTILACAO. */
	public static final String VENTILACAO = "Ventilação";

	/** The Constant APARELHO. */
	public static final String APARELHO = "Aparelho";

	/** Retorna a Chave Primaria da Entidade Conteudo Base Apuração Peródica. */
	public static final String C_TARIFA_BASE_APURACAO_PERIODICA = "C_TARIFA_BASE_APURACAO_PERIODICA";

	/** Retorna a Chave Primaria da Entidade Conteudo Base Apuração Diária. */
	public static final String C_TARIFA_BASE_APURACAO_DIARIA = "C_TARIFA_BASE_APURACAO_DIARIA";

	/**
	 * Proposta = Mensagem Erro. Proposta Não Pôde Ser Rejeitada.
	 */
	public static final String ERRO_NEGOCIO_PROPOSTA_NAO_REJEITADA = "ERRO_NEGOCIO_PROPOSTA_NAO_REJEITADA";

	/**
	 * Proposta = Situação da Proposta Não Pode Ser Alterada.
	 */
	public static final String ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_ALTERADA = "ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_ALTERADA";

	/** Mensagem Data inválida Prorrogação Nota de Débito. */
	public static final String ERRO_DATA_PRORROGACAO_NOTA_DEBITO_INVALIDA = "ERRO_DATA_PRORROGACAO_NOTA_DEBITO_INVALIDA";

	/** Não existe Penalidade Apurada que posso ser Recuperada. */
	public static final String ERRO_NOTA_DEBITO_CREDITO_NAO_EXISTE_PENALIDADE_APURADA =
			"ERRO_NOTA_DEBITO_CREDITO_NAO_EXISTE_PENALIDADE_APURADA";

	/** The Constant ERRO_PERCENTUAL_PENALIDADE_NAO_ENCONTRADO. */
	public static final String ERRO_PERCENTUAL_PENALIDADE_NAO_ENCONTRADO = "ERRO_PERCENTUAL_PENALIDADE_NAO_ENCONTRADO";

	/**
	 * Documento Impressão Layout Para Nota de Débito.
	 */
	public static final String C_RELATORIO_NOTA_DEBITO_BOLETO_ATIVO = "C_RELATORIO_NOTA_DEBITO_BOLETO_ATIVO";

	/**
	 * Documento Impressão Layout Para Nota de Crédito.
	 */
	public static final String C_RELATORIO_NOTA_CREDITO = "C_RELATORIO_NOTA_CREDITO";
	
	/**
	 * Documento Impressão Layout Para Nota de Crédito.
	 */
	public static final String C_RELATORIO_NOTA_CREDITO_REALIZAR = "C_RELATORIO_NOTA_CREDITO_REALIZAR";

	/** Documento Impressão Layout Para Nota de Débito Boleto, Não está Definido. */
	public static final String ERRO_DOCUMENTO_IMPRESSAO_LAYOU_NOTA_DEBITO_NAO_DEFINIDO =
			"ERRO_DOCUMENTO_IMPRESSAO_LAYOU_NOTA_DEBITO_NAO_DEFINIDO";

	/** Erro negócio levantamento de mercado. */

	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_DATAS_VIGENCIA_OBRIGATORIAS =
			"ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_DATAS_VIGENCIA_OBRIGATORIAS";

	/** Parâmetro sistema guarda último número proposta. */

	public static final String SEQUENCIA_NUMERO_PROPOSTA = "SEQUENCIA_NUMERO_PROPOSTA";

	/** The Constant C_ENTIDADE_CLASSE_CORRIGI_PT. */
	public static final String C_ENTIDADE_CLASSE_CORRIGI_PT = "C_ENTIDADE_CLASSE_CORRIGI_PT";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR = "C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO = "C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO";

	/**
	 * The Constant C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA.
	 */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA =
			"C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO = "C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO";

	/** The Constant C_ENTIDADE_CLASSE_CORRIGI_Z. */
	public static final String C_ENTIDADE_CLASSE_CORRIGI_Z = "C_ENTIDADE_CLASSE_CORRIGI_Z";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR = "C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA = "C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA";

	/** The Constant C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO. */
	public static final String C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO = "C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO";

	/** The Constant C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE. */
	public static final String C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE = "C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE";

	/** The Constant C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE. */
	public static final String C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE = "C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE";

	/** The Constant C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE. */
	public static final String C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE
			= "C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE";

	/**
	 * The Constant C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE.
	 */
	public static final String C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE
			= "C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE";

	/** The Constant ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PRESSAO_TEMPERATURA. */
	public static final String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PRESSAO_TEMPERATURA = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PRESSAO_TEMPERATURA";

	/** The Constant ERRO_NEGOCIO_NAO_E_POSSIVEL_GERAR_PROPOSTA. */
	public static final String ERRO_NEGOCIO_NAO_E_POSSIVEL_GERAR_PROPOSTA = "ERRO_NEGOCIO_NAO_E_POSSIVEL_GERAR_PROPOSTA";

	/** The Constant ERRO_NEGOCIO_HORARIO_ANTERIOR_ATUAL. */
	public static final String ERRO_NEGOCIO_HORARIO_ANTERIOR_ATUAL = "ERRO_NEGOCIO_HORARIO_ANTERIOR_ATUAL";

	/** The Constant C_TIPOS_MOTIVO_COMPLEMENTO_FATURA. */
	public static final String C_TIPOS_MOTIVO_COMPLEMENTO_FATURA = "C_TIPOS_MOTIVO_COMPLEMENTO_FATURA";

	/** The Constant C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA. */
	public static final String C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA = "C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA";

	/** The Constant C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL. */
	public static final String C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL = "C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL";

	/** The Constant C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR. */
	public static final String C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR = "C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR";

	/** The Constant C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO. */
	public static final String C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO = "C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO";

	/** The Constant C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL. */
	public static final String C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL = "C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL";

	/** The Constant C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR. */
	public static final String C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR = "C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR";

	/** The Constant ERRO_FATURA_COMPLEMENTAR_MOTIVO_NAO_INFORMADO. */
	public static final String ERRO_FATURA_COMPLEMENTAR_MOTIVO_NAO_INFORMADO = "ERRO_FATURA_COMPLEMENTAR_MOTIVO_NAO_INFORMADO";

	/** The Constant ERRO_NEGOCIO_DATA_AGENDAMENTO_INVALIDA. */
	public static final String ERRO_NEGOCIO_DATA_AGENDAMENTO_INVALIDA = "ERRO_NEGOCIO_DATA_AGENDAMENTO_INVALIDA";

	/** The Constant ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_POSSUI_PROPOSTA. */
	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_POSSUI_PROPOSTA = "ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_POSSUI_PROPOSTA";

	/** The Constant ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_POSSUI_PROPOSTA. */
	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_POSSUI_PROPOSTA = "ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_POSSUI_PROPOSTA";

	/** The Constant ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_PROPOSTA_NAO_APROVADA. */
	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_PROPOSTA_NAO_APROVADA =
			"ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_PROPOSTA_NAO_APROVADA";

	/** The Constant ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO. */
	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO =
			"ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO";

	/** Constates referente aos status de levantamento de mercado. */
	public static final String C_LEVANTAMENTO_MERCADO_EM_ANALISE = "C_LEVANTAMENTO_MERCADO_EM_ANALISE";

	/** The Constant C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA. */
	public static final String C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA = "C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA";

	/** The Constant C_LEVANTAMENTO_MERCADO_ENCERRADO. */
	public static final String C_LEVANTAMENTO_MERCADO_ENCERRADO = "C_LEVANTAMENTO_MERCADO_ENCERRADO";

	/** The Constant C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO. */
	public static final String C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO = "C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO";

	/** The Constant ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA. */
	public static final String ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA = "ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA";

	/**
	 * Mensagem exibida ao executar o fluxo de escalonamento de leiturista - TELA
	 * CRONOGRAMA.
	 */

	public static final String ESCALONAMENTO_REALIZADO_COM_SUCESSO = "ESCALONAMENTO_REALIZADO_COM_SUCESSO";

	/**
	 * Mensagem apresentada quando não houver nenhum leiturita cadastradado para
	 * realizar o escalonamento automático.
	 */

	public static final String NAO_EXISTEM_LEITURISTA_CADASTRADOS = "NAO_EXISTEM_LEITURISTA_CADASTRADOS";

	/** The Constant CAMPOS_NAO_PREENCHIDOS. */
	public static final String CAMPOS_NAO_PREENCHIDOS = "CAMPOS_NAO_PREENCHIDOS";

	/** The Constant CRONOGRAMA_JA_ESCALONADO. */
	public static final String CRONOGRAMA_JA_ESCALONADO = "CRONOGRAMA_JA_ESCALONADO";

	/**
	 * Constante de sistema para situação associação medidor independente.
	 */
	public static final String C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO
			= "C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO";

	/**
	 * Constante de sistema para situação associação medidor independente.
	 */
	public static final String C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO = "C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO";

	/**
	 * Constante de sistema para situação associação medidor independente.
	 */
	public static final String C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO = "C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO";

	/** Erro registro inativo. */
	public static final String ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_INATIVO = "ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_INATIVO";

	/** Mensagem de excecao para metodos nulo seguros. */
	public static final String OPERANDO_NULO = "Um dos valores esta nulo e a comparacao nao pode ser realizada.";

	/**
	 * Constante de sistema para status do levantamento de mercado.
	 */
	public static final String C_STATUS_LEVANTAMENTO_MERCADO = "C_STATUS_LEVANTAMENTO_MERCADO";

	/**
	 * Constante de sistema para Perfil "virtual" do medidor.
	 */
	public static final String C_MODO_USO_MEDIDOR_VIRTUAL = "C_MODO_USO_MEDIDOR_VIRTUAL";

	/**
	 * Constante de sistema para Perfil "normal" do medidor.
	 */
	public static final String C_MODO_USO_MEDIDOR_NORMAL = "C_MODO_USO_MEDIDOR_NORMAL";

	/**
	 * Constante de sistema para Perfil "independente" do medidor.
	 */
	public static final String C_MODO_USO_MEDIDOR_INDEPENDENTE = "C_MODO_USO_MEDIDOR_INDEPENDENTE";

	/** Constante de sistema para motivo de complemento de fatura complementar. */
	public static final String C_MOTIVO_COMPLEMENTO_ICMS = "C_MOTIVO_COMPLEMENTO_ICMS";

	/** Constante de sistema para motivo de complemento de fatura complementar. */
	public static final String C_MOTIVO_COMPLEMENTO_ICMS_ST = "C_MOTIVO_COMPLEMENTO_ICMS_ST";

	/**
	 * The Constant ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_NAO_VINCULADO_A_TARIFA.
	 */
	public static final String ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_NAO_VINCULADO_A_TARIFA
			= "ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_NAO_VINCULADO_A_TARIFA";

	/** The Constant ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_ST. */
	public static final String ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_ST = "ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_ST";

	/** The Constant LOG_ERRO_NEGOCIO_SEM_DADOS. */
	public static final String LOG_ERRO_NEGOCIO_SEM_DADOS = "LOG_ERRO_NEGOCIO_SEM_DADOS";

	/** The Constant LOG_ERRO_INTEGRACAO_LANCAMENTOS_CONTABEIS_NAO_LOCALIZADO. */
	public static final String LOG_ERRO_INTEGRACAO_LANCAMENTOS_CONTABEIS_NAO_LOCALIZADO =
			"LOG_ERRO_INTEGRACAO_LANCAMENTOS_CONTABEIS_NAO_LOCALIZADO";

	/** The Constant LOG_INICIO_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS. */
	public static final String LOG_INICIO_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS = "LOG_INICIO_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS";

	/** The Constant LOG_FIM_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS. */
	public static final String LOG_FIM_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS = "LOG_FIM_VALIDACAO_LANCAMENTOS_CONTABEIS_SINTETICOS";

	/** The Constant C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO. */
	public static final String C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO = "C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO";

	/** The Constant C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL. */
	public static final String C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL = "C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL";

	/** The Constant ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_CADASTRO. */
	public static final String ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_CADASTRO = "ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_CADASTRO";

	/** The Constant ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_VENCIMENTO. */
	public static final String ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_VENCIMENTO = "ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_VENCIMENTO";

	/** The Constant ERRO_NEGOCIO_CROMATOGRAFIA_EXISTENTE. */
	public static final String ERRO_NEGOCIO_CROMATOGRAFIA_EXISTENTE = "ERRO_NEGOCIO_CROMATOGRAFIA_EXISTENTE";

	/** The Constant C_PROPOSTA_SITUACAO_APROVADA. */
	public static final String C_PROPOSTA_SITUACAO_APROVADA = "C_PROPOSTA_SITUACAO_APROVADA";

	/** The Constant C_PROPOSTA_SITUACAO_REJEITADA. */
	public static final String C_PROPOSTA_SITUACAO_REJEITADA = "C_PROPOSTA_SITUACAO_REJEITADA";

	/** The Constant ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO_NOTA_FISCAL. */
	public static final String ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO_NOTA_FISCAL = "ERRO_PROCESSO_INTEGRACAO_"
			+ "AGUARDANDO_RETORNO_NOTA_FISCAL";

	/** The Constant LAT_GRAU. */
	public static final String LAT_GRAU = "Lat Grau";

	/** The Constant LONG_GRAU. */
	public static final String LONG_GRAU = "Long Grau";

	/** The Constant TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA. */
	public static final String TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA = "TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA";

	/** The Constant VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO. */
	public static final String VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO =
			"VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO";

	/** The Constant P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA. */
	public static final String P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA = "P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA";

	/** The Constant C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO. */
	public static final String C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO =
			"C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO";

	/** The Constant C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA. */
	public static final String C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA = "C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA";

	/** The Constant C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO. */
	public static final String C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO = "C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO";

	/** The Constant C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO. */
	public static final String C_ANORMALIDADE_CLIENTE_SEM_CONTRATO = "C_ANORMALIDADE_CLIENTE_SEM_CONTRATO";
	
	/** The Constant C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO. */
	public static final String C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO =
			"C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO";

	/** The Constant C_SERV_AUT_MOTIVO_ENCERRAMENTO. */
	public static final String C_SERV_AUT_MOTIVO_ENCERRAMENTO = "C_SERV_AUT_MOTIVO_ENCERRAMENTO";

	/** The Constant C_SERV_AUT_MOTIVO_CANCELAMENTO. */
	public static final String C_SERV_AUT_MOTIVO_CANCELAMENTO = "C_SERV_AUT_MOTIVO_CANCELAMENTO";

	/** The Constant C_PCEQ_POTENCIA_FIXA_ALTA. */
	public static final String C_PCEQ_POTENCIA_FIXA_ALTA = "C_PCEQ_POTENCIA_FIXA_ALTA";

	/** The Constant C_PCEQ_POTENCIA_FIXA_MEDIA. */
	public static final String C_PCEQ_POTENCIA_FIXA_MEDIA = "C_PCEQ_POTENCIA_FIXA_MEDIA";

	/** The Constant C_PCEQ_POTENCIA_FIXA_BAIXA. */
	public static final String C_PCEQ_POTENCIA_FIXA_BAIXA = "C_PCEQ_POTENCIA_FIXA_BAIXA";

	public static final String ATIVO = "1";

	public static final String INATIVO = "0";

	public static final String ERRO_INTEGRACAO_NAO_PROCESSADA = "ERRO_INTEGRACAO_NAO_PROCESSADA";

	public static final String ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO = "ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO";

	public static final String ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO = "ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO";

	public static final String ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_IMOVEL = "ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_IMOVEL";

	public static final String ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_CLIENTE = "ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_CLIENTE";

	public static final String ERRO_NEGOCIO_UNIDADE_CONSUMIDORA_EXISTENTE = "ERRO_NEGOCIO_UNIDADE_CONSUMIDORA_EXISTENTE";

	/** The Constant PARAMETRO_PDD_VALOR_LIMITE_ANUAL. */
	public static final String PARAMETRO_PDD_VALOR_LIMITE_ANUAL = "PARAMETRO_PDD_VALOR_LIMITE_ANUAL";

	/** The Constant PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL. */
	public static final String PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL = "PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL";

	/** A Constante CAMPO_ACAO. */
	public static final String CAMPO_ACAO = "Ação";
	
	/** A Constante CAMPO_ACAO_COMANDO. */
	public static final String CAMPO_ACAO_COMANDO = "Ação Comando";
	
	/** A Constante CAMPO_PONTO_CONSUMO. */
	public static final String CAMPO_PONTO_CONSUMO = "Ponto de Consumo";

	/** A Constante ATIVIDADE. */
	public static final String ATIVIDADE = "Atividade";

	/** A constante ERRO_NEGOCIO_ANO_FABRICACAO_INICIO_POSTERIOR */
	public static final String ERRO_NEGOCIO_ANO_FABRICACAO_INICIO_POSTERIOR = "ERRO_NEGOCIO_ANO_FABRICACAO_INICIO_POSTERIOR";

	/** A constante ERRO_NEGOCIO_DATA_POSTERIOR */
	public static final String ERRO_NEGOCIO_DATA_POSTERIOR = "ERRO_NEGOCIO_DATA_POSTERIOR";

	/** A constante RELATORIO_BOLETO */
	public static final String RELATORIO_BOLETO = "RELATORIO_BOLETO";

	/** A constante DIVISOR_MODULO10 */
	public static final int DIVISOR_MODULO10 = 10;

	/** A constante DIVISOR_MODULO11 */
	public static final int DIVISOR_MODULO11 = 11;

	/** A constante QTD_CARACTER_ANOMES */
	public static final int QTD_CARACTER_ANOMES = 6;

	/** A contante ERRO_INESPERADO */
	public static final String ERRO_INESPERADO = "ERRO_INESPERADO";

	/** A contante ALERTA_NAO_HA_AUTORIZACOES_DE_SERVICO_PARA_SEREM_AGENDADAS */
	public static final String ALERTA_NAO_HA_AUTORIZACOES_DE_SERVICO_PARA_SEREM_AGENDADAS
			= "ALERTA_NAO_HA_AUTORIZACOES_DE_SERVICO_PARA_SEREM_AGENDADAS";

	/** A Constante ERRO_NEGOCIO_JA_EXISTE_AGENDAMENTO_PARA_O_TIPO_SERVICO. */
	public static final String ERRO_NEGOCIO_JA_EXISTE_AGENDAMENTO_PARA_O_TIPO_SERVICO =
			"ERRO_NEGOCIO_JA_EXISTE_AGENDAMENTO_PARA_O_TIPO_SERVICO";

	/** A Constante ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE. */
	public static final String ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE = "ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE";

	/** A Constante ERRO_NEGOCIO_CONSTANTE_CORRIGE_PT_MAL_CONFIGURADA. */
	public static final String ERRO_NEGOCIO_CONSTANTE_CORRIGE_PT_MAL_CONFIGURADA = "ERRO_NEGOCIO_CONSTANTE_CORRIGE_PT_MAL_CONFIGURADA";

	/**
	 * Constante de sistema para erro de negócio onde ocorre mais de um
	 * {@link LancamentoContabilSintetico} inserido com atributos idênticos
	 */
	public static final String ERRO_NEGOCIO_MAIS_DE_UM_LANCAMENTO = "ERRO_NEGOCIO_MAIS_DE_UM_LANCAMENTO";

	/**
	 * Constante para erro que acontece quando o campo que define o diretório aonde
	 * o relatório será salvo está preenchido incorretamente.
	 */
	public static final String ERRO_NEGOCIO_DIRETORIO_MAL_CONFIGURADO = "ERRO_NEGOCIO_DIRETORIO_MAL_CONFIGURADO";

	public static final String NOME_FORM = "nomeForm";

	/**
	 * Constante para parametro PARAMETRO_UNIDADE_PRESSAO_MEDICAO.
	 */
	public static final String PARAMETRO_UNIDADE_PRESSAO_MEDICAO = "PARAMETRO_UNIDADE_PRESSAO_MEDICAO";

	/**
	 * Constante para parametro PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO.
	 */
	public static final String PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO = "PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO";

	/** A Constante SEQUENCIAL_NSA. */
	public static final String SEQUENCIAL_NSA = "SEQUENCIAL_NSA";

	/** A Constante CODIGO_CONVENIO. */
	public static final String CODIGO_CONVENIO = "CODIGO_CONVENIO";

	/** A Constante NOME_EMPRESA. */
	public static final String NOME_EMPRESA = "NOME_EMPRESA";

	/** A Constante CODIGO_BANCO. */
	public static final String CODIGO_BANCO = "CODIGO_BANCO";

	/** A Constante CODIGO_REMESSA. */
	public static final String CODIGO_REMESSA = "CODIGO_REMESSA";

	/** A Constante VERSAO_LAYOUT_ARQUIVO. */
	public static final String VERSAO_LAYOUT_ARQUIVO = "VERSAO_LAYOUT_ARQUIVO";

	/** A Constante IDENT_CLIENTE_EMPRESA. */
	public static final String IDENT_CLIENTE_EMPRESA = "IDENT_CLIENTE_EMPRESA";

	/** A Constante AGENCIA_DEBITO. */
	public static final String AGENCIA_DEBITO = "AGENCIA_DEBITO";

	/** A Constante IDENT_CLIENTE_BANCO. */
	public static final String IDENT_CLIENTE_BANCO = "IDENT_CLIENTE_BANCO";

	/** A Constante DATA_OPCAO. */
	public static final String DATA_OPCAO = "DATA_OPCAO";

	/** A Constante CODIGO_MOVIMENTO. */
	public static final String CODIGO_MOVIMENTO = "CODIGO_MOVIMENTO";

	/** A Constante ID_CREDITADA. */
	public static final String ID_CREDITADA = "ID_CREDITADA";

	/** A Constante DATA_CREDITO. */
	public static final String DATA_CREDITO = "DATA_CREDITO";

	/** A Constante CODIGO_BARRAS. */
	public static final String CODIGO_BARRAS = "CODIGO_BARRAS";

	/** A Constante NUMERO_AUT_CODIGO_TRANSMISSAO. */
	public static final String NUMERO_AUT_CODIGO_TRANSMISSAO = "NUMERO_AUT_CODIGO_TRANSMISSAO";

	/** A Constante VALOR_RECEBIMENTO. */
	public static final String VALOR_RECEBIMENTO = "VALOR_RECEBIMENTO";

	/** A Constante DATA_RECEBIMENTO. */
	public static final String DATA_RECEBIMENTO = "DATA_RECEBIMENTO";

	/** A Constante CODIGO_RETORNO_OCORRENCIA. */
	public static final String NUMERO_DOCUMENTO = "NUMERO_DOCUMENTO";

	/** A Constante CODIGO_RETORNO_OCORRENCIA. */
	public static final String CODIGO_RETORNO_OCORRENCIA = "CODIGO_RETORNO_OCORRENCIA";

	/** A Constante NOSSO_NUMERO. */
	public static final String NOSSO_NUMERO = "NOSSO_NUMERO";

	/** A Constante JUROS_MORA_MULTA. */
	public static final String JUROS_MORA_MULTA = "JUROS_MORA_MULTA";

	/** A Constante DESCONTOS. */
	public static final String DESCONTOS = "DESCONTOS";

	/** A Constante VALOR_ABATIMENTO. */
	public static final String VALOR_ABATIMENTO = "VALOR_ABATIMENTO";

	/** A Constante OUTROS_CREDITOS. */
	public static final String OUTROS_CREDITOS = "OUTROS_CREDITOS";

	/** A Constante IOF. */
	public static final String IOF = "IOF";

	/** A Constante TARIFA_COBRANCA. */
	public static final String TARIFA_COBRANCA = "TARIFA_COBRANCA";

	/** A Constante DATA_VENCIMENTO. */
	public static final String DATA_VENCIMENTO = "DATA_VENCIMENTO";

	/** A Constante TOTAL_REGISTROS. */
	public static final String TOTAL_REGISTROS = "TOTAL_REGISTROS";

	/** A Constante VALOR_TOTAL_REGISTROS. */
	public static final String VALOR_TOTAL_REGISTROS = "VALOR_TOTAL_REGISTROS";

	/** A Constante VALOR_DEBITO. */
	public static final String VALOR_DEBITO = "VALOR_DEBITO";

	/** A Constante USO_EMPRESA. */
	public static final String USO_EMPRESA = "USO_EMPRESA";

	/** A Constante NOME_PAGADOR. */
	public static final String NOME_PAGADOR = "NOME_PAGADOR";

	/** A Constante PRIMEIRO_CODIGO_ERRO. */
	public static final String PRIMEIRO_CODIGO_ERRO = "PRIMEIRO_CODIGO_ERRO";

	/** A Constante SEGUNDO_CODIGO_ERRO. */
	public static final String SEGUNDO_CODIGO_ERRO = "SEGUNDO_CODIGO_ERRO";

	/** A Constante TERCEIRO_CODIGO_ERRO. */
	public static final String TERCEIRO_CODIGO_ERRO = "TERCEIRO_CODIGO_ERRO";

	/**
	 * Constante para o erro ERRO_NEGOCIO_BANCO_CODIGO_BARRAS_NAO_IMPLEMENTADO.
	 */
	public static final String ERRO_NEGOCIO_BANCO_CODIGO_BARRAS_NAO_IMPLEMENTADO = "ERRO_NEGOCIO_BANCO_CODIGO_BARRAS_NAO_IMPLEMENTADO";

	/** A constante CHAMADO_INCLUIDO_SUCESSO */
	public static final String CHAMADO_INCLUIDO_SUCESSO = "CHAMADO_INCLUIDO_SUCESSO";

	/**
	 * Constante de chamados em lote incluido com sucesso
	 */
	public static final String CHAMADOS_EM_LOTE_INCLUIDO_SUCESSO = "CHAMADOS_EM_LOTE_INCLUIDO_SUCESSO";

	/**
	 * Constante para o erro
	 * ERRO_NEGOCIO_DATA_INSTALACAO_MEDIDOR_MAIOR_DATA_LEITURA.
	 */
	public static final String ERRO_NEGOCIO_DATA_INSTALACAO_MEDIDOR_MAIOR_DATA_LEITURA =
			"ERRO_NEGOCIO_DATA_INSTALACAO_MEDIDOR_MAIOR_DATA_LEITURA";

	/**
	 * Constante para o erro ERRO_NEGOCIO_DATA_ATIVACAO_MEDIDOR_MAIOR_DATA_LEITURA.
	 */
	public static final String ERRO_NEGOCIO_DATA_ATIVACAO_MEDIDOR_MAIOR_DATA_LEITURA = "ERRO_NEGOCIO_DATA_ATIVACAO_MEDIDOR_MAIOR_DATA_LEITURA";

	/**
	 * Constante para o erro
	 * ERRO_NEGOCIO_DATA_INSTALACAO_CORRETOR_MAIOR_DATA_LEITURA.
	 */
	public static final String ERRO_NEGOCIO_DATA_INSTALACAO_CORRETOR_MAIOR_DATA_LEITURA =
			"ERRO_NEGOCIO_DATA_INSTALACAO_CORRETOR_MAIOR_DATA_LEITURA";

	/**
	 * Constante para o erro ERRO_NEGOCIO_PONTO_CONSUMO_NAO_SELECIONADO.
	 */
	public static final String ERRO_NEGOCIO_PONTO_CONSUMO_NAO_SELECIONADO = "ERRO_NEGOCIO_PONTO_CONSUMO_NAO_SELECIONADO";

	/**
	 * Constante ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO_EM_DIRETORIO
	 */
	public static final String ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO_EM_DIRETORIO = "ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO_EM_DIRETORIO";

	/**
	 * Constante C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA
	 */
	public static final String C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA = "C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA";

	/**
	 * Constante C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA
	 */
	public static final String C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA = "C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA";

	/** Constante ALERTA_INSERIDO_SEM_ENVIAR_EMAIL */
	public static final String ALERTA_INSERIDO_SEM_ENVIAR_EMAIL = "ALERTA_INSERIDO_SEM_ENVIAR_EMAIL";

	/** Constante ALERTA_INSERIDO_ERRO_ENVIO_EMAIL */
	public static final String ALERTA_INSERIDO_ERRO_ENVIO_EMAIL = "ALERTA_INSERIDO_ERRO_ENVIO_EMAIL";

	/**
	 * Constante para o erro ERRO_NEGOCIO_CHAMADO_SEM_QUESTIONARIO.
	 */
	public static final String ERRO_NEGOCIO_CHAMADO_SEM_QUESTIONARIO = "ERRO_NEGOCIO_CHAMADO_SEM_QUESTIONARIO";

	/**
	 * Constante para o erro ERRO_NEGOCIO_CHAMADO_JA_RESPONDIDO.
	 */
	public static final String ERRO_NEGOCIO_CHAMADO_JA_RESPONDIDO = "ERRO_NEGOCIO_CHAMADO_JA_RESPONDIDO";

	/**
	 * Constante para o erro PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS.
	 */
	public static final String PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS = "PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS";
	
	/**
	 * Constante para o erro PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS.
	 */
	public static final String PARAMETRO_NM_DIAS_EMAIL_AUTORIZACAO_SERVICO_PENDENTE = "PARAMETRO_NM_DIAS_EMAIL_AUTORIZACAO_SERVICO_PENDENTE";

	/**
	 * Constante C_SERVICO_PRESTADO_CONSTRUTORA
	 */
	public static final String C_SERVICO_PRESTADO_CONSTRUTORA = "C_SERVICO_PRESTADO_CONSTRUTORA";

	/**
	 * Constante C_SERVICO_PRESTADO_MEDICAO
	 */
	public static final String C_SERVICO_PRESTADO_MEDICAO = "C_SERVICO_PRESTADO_MEDICAO";

	/**
	 * Constante C_INTEGRACAO_TIPO_BEM_MEDIDOR
	 */
	public static final String C_INTEGRACAO_TIPO_BEM_MEDIDOR = "C_INTEGRACAO_TIPO_BEM_MEDIDOR";

	/**
	 * Constante C_INTEGRACAO_TIPO_BEM_CORRETOR
	 */
	public static final String C_INTEGRACAO_TIPO_BEM_CORRETOR = "C_INTEGRACAO_TIPO_BEM_CORRETOR";

	/**
	 * Constante C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL
	 */
	public static final String C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL = "C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL";

	/**
	 * Constante C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ANTERIOR_MAIS_ATUAL
	 */
	public static final String C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ANTERIOR_MAIS_ATUAL = "C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ANTERIOR_MAIS_ATUAL";

	/**
	 * Constante C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL_AJUSTADO
	 */
	public static final String C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL_AJUSTADO = "C_CONSUMO_COMPOSICAO_TIPO_MEDIDOR_ATUAL_AJUSTADO";

	/**
	 * Constante ERRO_FORMULARIO_OBRIGATORIO
	 */
	public static final String ERRO_FORMULARIO_OBRIGATORIO = "ERRO_FORMULARIO_OBRIGATORIO";

	/**
	 * Constante C_CONTRATO_AGUARDANDO_APROVACAO
	 */
	public static final String C_CONTRATO_AGUARDANDO_APROVACAO = "C_CONTRATO_AGUARDANDO_APROVACAO";

	/**
	 * Constante ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE
	 */
	public static final String ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE = "ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE";

	/**
	 * Constante PERGUNTA_QUESTIONARIO_SATISFACAO
	 */
	public static final String PARAMETRO_PERGUNTA_QUESTIONARIO_SATISFACAO = "P_PERGUNTA_QUESTIONARIO_SATISFACAO";

	/**
	 * Constatnte ERRO_CEP_NAO_CADASTRADO
	 */
	public static final String ERRO_CEP_NAO_CADASTRADO = "CEP não cadastrado no sistema.";

	/**
	 * Constante MENSAGEM_PRESSAO_FORNECIMENTO_VINCULADA
	 */
	public static final String MENSAGEM_PRESSAO_FORNECIMENTO_VINCULADA = "MENSAGEM_PRESSAO_FORNECIMENTO_VINCULADA";

	/**
	 * Constante SEGMENTO_COMERCIAL_DESCRICAO
	 */
	public static final String SEGMENTO_COMERCIAL_DESCRICAO = "COMERCIAL";

	/**
	 * Constante TARIFA_EXISTENTE
	 */
	public static final String TARIFA_EXISTENTE = "TARIFA_EXISTENTE";

	/**
	 * Constante TTIPO_CONSUMO_FATURAMENTO
	 */
	public static final String TIPO_CONSUMO_FATURAMENTO = "C_TIPO_CONSUMO_FATURAMENTO";

	/**
	 * Constante REFERENCIA
	 */
	public static final String REFERENCIA = "referencia";

	/**
	 * Constante CICLO
	 */
	public static final String CICLO = "ciclo";

	public static final String ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO = "ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO";


	/**
	 * Constante PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA
	 */
	public static final String PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA = "PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA";

	/**
	 * Constante PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA
	 */
	public static final String PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA = "PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA";

	/**
	 * Constante usada para recuperar o usuario Admin padrao do sistema
	 */
	public static final String USUARIO_AUDITORIA = "admin";

	/**
	 * Constante ERRO_IMOVEL_EM_CICLO_DE_LEITURA
	 */
	public static final String ERRO_IMOVEL_EM_CICLO_DE_LEITURA = "ERRO_IMOVEL_EM_CICLO_DE_LEITURA";

	/**
	 * Constante SUCESSO_PLANILHA_CITY_GATE_IMPORTADA
	 */
	public static final String SUCESSO_PLANILHA_CITY_GATE_IMPORTADA = "SUCESSO_PLANILHA_CITY_GATE_IMPORTADA";

	/**
	 * Constante ERRO_PLANILHA_CITY_GATE_IMPORTADA
	 */
	public static final String ERRO_PLANILHA_CITY_GATE_IMPORTADA = "ERRO_PLANILHA_CITY_GATE_IMPORTADA";

	/**
	 * Constante CITY_GATE_ARQUIVO_SELECAO_INVALIDA
	 */
	public static final String CITY_GATE_ARQUIVO_SELECAO_INVALIDA = "CITY_GATE_ARQUIVO_SELECAO_INVALIDA";

	/**
	 * Constante CITY_GATE_ARQUIVO_SELECAO_INVALIDA
	 */
	public static final String CITY_GATE_PLANILHA_NAO_EXISTE = "CITY_GATE_PLANILHA_NAO_EXISTE";

	/**
	 * Constante ARQUIVO_GERADO_NULO
	 */
	public static final String ARQUIVO_GERADO_NULO = "ARQUIVO_GERADO_NULO";

	/**
	 * Constante Número da carteira do banco Daycoval
	 */
	public static final String CARTEIRA_BANCO_DAYCOVAL = "109";
	
	/** The Constant C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_MODELO_CONTRATO. */
	public static final String C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_MODELO_CONTRATO = "C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_MODELO_CONTRATO";
	
	/** The Constant C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_EMAIL_AS. */
	public static final String C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_EMAIL_AS = "C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_EMAIL_AS";

	/**
	 * Constantes Validações Tela
	 */
	public static final String CITY_GATE_IMPORTACAO_ERRO_DIAS_INCOMPLETOS = "CITY_GATE_IMPORTACAO_ERRO_DIAS_INCOMPLETOS";
	public static final String CITY_GATE_IMPORTACAO_ERRO_DIA_FINAL_MAIOR = "CITY_GATE_IMPORTACAO_ERRO_DIA_FINAL_MAIOR";
	public static final String CITY_GATE_IMPORTACAO_ERRO_DIA_FINAL_NAO_EXISTE = "CITY_GATE_IMPORTACAO_ERRO_DIA_FINAL_NAO_EXISTE";

	public static final String CITY_GATE_IMPORTACAO_ERRO_CELL_PERIODO = "CITY_GATE_IMPORTACAO_ERRO_CELL_PERIODO";
	public static final String CITY_GATE_IMPORTACAO_ERRO_CELL_DIA = "CITY_GATE_IMPORTACAO_ERRO_CELL_DIA";

	public static final String PLANILHA_MEDICAO_VOLUME = "Volume";
	public static final Integer PLANILHA_MEDICAO_LINHA_INICIO_DIAS = 11;
	public static final Integer PLANILHA_MEDICAO_COLUNA_DIA = 0;
	public static final Integer PLANILHA_MEDICAO_COLUNA_VOLUME = 1;
	public static final Integer PLANILHA_MEDICAO_COLUNA_PCS = 3;
	public static final Integer PLANILHA_MEDICAO_LINHA_PERIODO = 6;
	public static final Integer PLANILHA_MEDICAO_COLUNA_PERIODO = 1;

	public static final String MOTIVO_SUCESSO_ENCERRAMENTO_AS = "COM SUCESSO";

	/**
	 * Constante para ocorrencia de envio de Cancelamento de Titulo do Santander
	 */
	public static final String C_OCORRENCIA_ENVIO_ENTRADA_TITULO_SANTANDER = "C_OCORRENCIA_ENVIO_ENTRADA_TITULO_SANTANDER";

	/**
	 * Constante para ocorrencia de envio de Cancelamento de Titulo do Santander
	 */
	public static final String C_OCORRENCIA_ENVIO_CANCELAMENTO_TITULO_SANTANDER = "C_OCORRENCIA_ENVIO_CANCELAMENTO_TITULO_SANTANDER";

	/**
	 * Constante para ocorrencia de envio de Cancelamento de Titulo do Santander
	 */
	public static final String C_OCORRENCIA_ENVIO_ALTERACAO_VENCIMENTO_TITULO_SANTANDER =
			"C_OCORRENCIA_ENVIO_ALTERACAO_VENCIMENTO_TITULO_SANTANDER";

	public static final String PARAMETRO_DIRETORIO_ARQUIVOS_SEFAZ = "PARAMETRO_DIRETORIO_ARQUIVOS_SEFAZ";

	public static final int QUANTIDADE_MAXIMA_MEDICOES_HORARIAS = 24;

	public static final int QUANTIDADE_MESES_NO_ANO = 12;
	/**
	 * Constante para identificar tipo pessoa FISICA
	 */
	public static final int PESSOA_FISICA = 1;

	/**
	 * Constante para identificar tipo pessoa JURIDICA
	 */
	public static final int PESSOA_JURIDICA = 2;

	/**
	 * Constante para identificar a quantidade minima de dias para ser considerado MENSAL
	 */
	public static final int QUANTIDADE_MINIMA_DE_DIAS_PARA_SER_CONSIDERADO_MENSAL = 25;

	/**
	 * Constante EERRO_CONTRATO_ENCERRADO_RESCINDIDO
	 */
	public static final String ERRO_CONTRATO_ENCERRADO_RESCINDIDO = "ERRO_CONTRATO_ENCERRADO_RESCINDIDO";
	
	/**
	 * Constante ERRO_NEGOCIO_CONTRATO_CAMPO_DATA_RECISAO_OBRIGATORIO
	 */
	public static final String ERRO_NEGOCIO_CONTRATO_CAMPO_DATA_RECISAO_OBRIGATORIO = "ERRO_NEGOCIO_CONTRATO_CAMPO_DATA_RECISAO_OBRIGATORIO";
	
	/** 
	 * Indica a situação de contrato Rescindido.  
	 */
	public static final String C_CONTRATO_RESCINDIDO = "C_CONTRATO_RESCINDIDO";

	/**
	 * Constante para identificar a chave habilitada do filtro
	 */
	public static final String HABILITADO = "habilitado";

	/**
	 * Constante para identificar o comando de cobranca NOTIFICACAO DE CORTE
	 */
	public static final String COMANDO_COBRANCA_NOTIFICACAO_CORTE = "notificacaoCorte";

	/**
	 * Constante para identificar o comando de cobranca NOTIFICACAO DE CORTE
	 */
	public static final String COMANDO_COBRANCA_AVISO_CORTE = "avisoCorte";

	/**
	 * Constante para identificar a mensagem de NOTIFICACAO DE CORTE
	 */
	public static final String MENSAGEM_NOTIFICACAO_CORTE = "MENSAGEM_NOTIFICACAO_CORTE";
	
	public static final String SUCESSO_ENTIDADE_HABILITADO = "SUCESSO_ENTIDADE_HABILITADO";
	
	public static final String SUCESSO_ENTIDADE_DESABILITADO = "SUCESSO_ENTIDADE_DESABILITADO";
	
	public static final String LATITUDE_EMPRESA = "LATITUDE_EMPRESA";
	
	public static final String LONGITUDE_EMPRESA = "LONGITUDE_EMPRESA";
		
	public static final String DESCRICAO_ACAO_COMANDO_AS_LOTE = "GERAR AUTORIZACAO DE SERVICO DE CORTE EM LOTE";
	
	public static final int LIMITE_CAMPO = 2;
	
	public static final String PARAMETRO_VALIDA_CONTRATO = "PARAMETRO_VALIDA_CONTRATO";
	
	public static final String PARAMETRO_REGISTRA_HISTORICO_MEDICAO = "PARAMETRO_REGISTRA_HISTORICO_MEDICAO";
	
	public static final String SUCESSO_INCLUSAO_NEGATIVACAO = "SUCESSO_INCLUSAO_NEGATIVACAO";
	
	public static final String SUCESSO_CONFIRMACAO_NEGATIVACAO = "SUCESSO_CONFIRMACAO_NEGATIVACAO";
	
	public static final String SUCESSO_EXCLUSAO_NEGATIVACAO = "SUCESSO_EXCLUSAO_NEGATIVACAO";
	
	public static final String SUCESSO_CANCELAMENTO_NEGATIVACAO = "SUCESSO_CANCELAMENTO_NEGATIVACAO";
	
	public static final String ERRO_NEGOCIO_AGENTE_MESMO_PERIODO = "ERRO_NEGOCIO_AGENTE_MESMO_PERIODO";
	
	public static final String ERRO_NEGOCIO_AGENTE_DATAS_FIM_MENOR = "ERRO_NEGOCIO_AGENTE_DATAS_FIM_MENOR";
	
	public static final String ERRO_NEGOCIO_COMANDO_PERIODO_VENCIMENTO = "ERRO_NEGOCIO_COMANDO_PERIODO_VENCIMENTO";
	
	public static final String ERRO_NOME_COMANDO_INVALIDO = "ERRO_NOME_COMANDO_INVALIDO";
	
	public static final String PARAMETRO_CANCELA_FATURA_APOS_PRAZO = "PARAMETRO_CANCELA_FATURA_APOS_PRAZO";
	
	public static final String SUCESSO_DESFAZER_CRONOGRAMA = "SUCESSO_DESFAZER_CRONOGRAMA";
	
	public static final String C_SERVICO_RIF_REATIVACAO = "C_SERVICO_RIF_REATIVACAO";
	
	public static final String PARAMETRO_CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS = "PARAMETRO_CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS";
	
	public static final String PARAMETRO_CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO = "PARAMETRO_CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO";
	
	public static final String C_ENT_CLASSE_CILINDRO_AS = "C_ENT_CLASSE_CILINDRO_AS";
	
	public static final String C_ENT_CLASSE_SOLUCAO_AS = "C_ENT_CLASSE_SOLUCAO_AS";
	
	public static final String C_ENT_CLASSE_CAUSA_AS = "C_ENT_CLASSE_CAUSA_AS";
	
	public static final String C_ENT_CLASSE_LOCAL_AS = "C_ENT_CLASSE_LOCAL_AS";
	
	public static final String C_ENT_CLASSE_OCORRENCIA_AS = "C_ENT_CLASSE_OCORRENCIA_AS";
	
	public static final String C_ENT_CLASSE_STATUS_AS = "C_ENT_CLASSE_STATUS_AS";
	
	public static final String C_ENT_CLASSE_ANORM_AS  = "C_ENT_CLASSE_ANORM_AS";
	
	public static final String C_ENT_CLASSE_TIPO_INST_AS = "C_ENT_CLASSE_TIPO_INST_AS";
	
	public static final String ERRO_MULTIPLOS_ENDERECOS = "Não é possível incluir chamado com múltiplos endereços";
	
	public static final String ERRO_NUMERO_ENDERECO_CHAMADO = "Não é possível incluir um endereço sem um número no chamado";

	public static final String SERVICO_TIPO_PROTOCOLO = "SERVICO_TIPO_PROTOCOLO";
	
	public static final String PARAMETRO_NOME_CERTIFICADO_DIGITAL = "PARAMETRO_NOME_CERTIFICADO_DIGITAL";

	private Constantes() {
	}

	/**
	 * Inicializar propriedades.
	 */
	private static void inicializarPropriedades() {

		Properties propriedades = new Properties();
		try {
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES);
			propriedades.load(stream);
			HASH_CRIPTOGRAFIA = (String) propriedades.get("HASH_CRIPTOGRAFIA");
			ALTURA_MAXIMA_IMAGEM = Integer.parseInt((String) propriedades.get("ALTURA_MAXIMA_IMAGEM"));
			LARGURA_MAXIMA_IMAGEM = Integer.parseInt((String) propriedades.get("LARGURA_MAXIMA_IMAGEM"));
			PERMITIR_REGISTRO_LEITURA_COM_ANORMALIDADE_EXISTENTE = Boolean
					.valueOf((String) propriedades.get("PERMITIR_REGISTRO_LEITURA_COM_ANORMALIDADE_EXISTENTE"));
			LINGUA_PADRAO = (String) propriedades.get("LINGUA_PADRAO");
			PAIS_PADRAO = (String) propriedades.get("PAIS_PADRAO");
			LOCALE_PADRAO = new Locale(LINGUA_PADRAO.trim(), PAIS_PADRAO.trim());

			QUANTIDADE_CASAS_VALOR_DECIMAL = Integer.parseInt((String) propriedades.get("QUANTIDADE_CASAS_VALOR_DECIMAL"));

			GERAR_LOG_FATURAMENTO = (String) propriedades.get("GERAR_LOG_FATURAMENTO");

			Path relPath = Paths.get(System.getProperty("user.dir")); //NOSONAR

			String isAmbienteProducao = propriedades.getProperty("IS_AMBIENTE_PRODUCAO", "true");

			IS_PARALLEL_BATCH = Boolean.parseBoolean(propriedades.getProperty("IS_PARALLEL_BATCH", FALSE));

			URL_LOGOMARCA_GGAS = substituirValorDePropriedade(propriedades, "URL_LOGOMARCA_GGAS", relPath);

			URL_LOGOMARCA_BANCO = substituirValorDePropriedade(propriedades, "URL_LOGOMARCA_BANCO", relPath);

			URL_LOGOMARCA_EMPRESA = substituirValorDePropriedade(propriedades, "URL_LOGOMARCA_EMPRESA", relPath);

			GERADOR_MATRICULA_SQL_NEXT_ID = (String) propriedades.get("GERADOR_MATRICULA_SQL_NEXT_ID");

			IS_AMBIENTE_PRODUCAO = Boolean.parseBoolean(isAmbienteProducao);
			SERVIDOR_HELP = propriedades.getProperty("SERVIDOR_HELP");

			HOST_AGENCIA_VIRTUAL = propriedades.getProperty("HOST_AGENCIA_VIRTUAL");

			PATH_GGAS_JAR = propriedades.getProperty("PATH_GGAS_JAR");
			
			CHAVE_API_GOOGLE_MAPS = propriedades.getProperty("CHAVE_API_GOOGLE_MAPS");
			
			URL_IMAGENS_GGAS = substituirValorDePropriedade(propriedades, "URL_IMAGENS_GGAS", relPath);
			

			stream.close();
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}

	private static String substituirValorDePropriedade(Properties propriedades, String chavePropriedade, Path relPath) {
		String valorPropriedade = String.valueOf(propriedades.getProperty(chavePropriedade));

		if (StringUtils.isNotBlank(valorPropriedade) && valorPropriedade.contains("{0}")) {
			valorPropriedade = valorPropriedade.replace("{0}",
					relPath.subpath(relPath.getNameCount() - 1, relPath.getNameCount()).toString());
		}

		return valorPropriedade;
	}

	static {
		inicializarPropriedades();
	}



}
