/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import br.com.ggas.geral.exception.NegocioException;

//TODO: [jgfm] Refactory 
public class DiaOrdinal {

	private Integer unidade;

	private Integer dezena;

	private static final String ERRO_NEGOCIO_DIA_NULO = "ERRO_NEGOCIO_DIA_NULO";

	private static final String ERRO_NEGOCIO_DIA_ZERO = "ERRO_NEGOCIO_DIA_ZERO";

	private static final String[] ARRAY_UNIDADE = {"primeiro", "segundo", "terceiro", "quarto", 
	                                               "quinto", "sexto", "sétimo", "oitavo", "nono", "décimo"};

	private static final String[] ARRAY_DEZENA = {"décimo", "vigésimo", "trigésimo"};

	/**
	 * Construtor.
	 * 
	 * @param numeroOrdinal
	 *            valor para colocar em ordinal
	 *            por extenso
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public DiaOrdinal(Integer numeroOrdinal) throws NegocioException {

		defineValores(numeroOrdinal);
	}

	/**
	 * Define valores.
	 * 
	 * @param numeroOrdinal
	 *            the numero ordinal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void defineValores(Integer numeroOrdinal) throws NegocioException {

		String numeroOrdinalString = "";

		if (numeroOrdinal != null) {
			numeroOrdinalString = String.valueOf(numeroOrdinal);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_DIA_NULO, true);
		}

		if (numeroOrdinal < 10 && numeroOrdinal > 0) {
			String unidadeString = numeroOrdinalString.substring(0, 1);
			this.unidade = Integer.valueOf(unidadeString);

		} else if (numeroOrdinal <= 31 && numeroOrdinal > 0) {
			String dezenaString = numeroOrdinalString.substring(0, 1);
			String unidadeString = numeroOrdinalString.substring(1, 2);
			this.dezena = Integer.valueOf(dezenaString);
			this.unidade = Integer.valueOf(unidadeString);

		} else {
			throw new NegocioException(ERRO_NEGOCIO_DIA_ZERO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder buf = new StringBuilder();

		if (this.dezena != null) {
			buf.append(this.ARRAY_DEZENA[this.dezena - 1]);
		}

		if (this.unidade != 0) {
			buf.append(" ");
			buf.append(this.ARRAY_UNIDADE[this.unidade - 1]);
		}

		return buf.toString();
	}

}
