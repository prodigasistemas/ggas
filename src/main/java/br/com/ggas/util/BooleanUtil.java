
package br.com.ggas.util;

import org.apache.commons.lang.StringUtils;

public class BooleanUtil {

	/**
	 * Converte as strings "0" e "1" para false e true respectivamente.
	 * 
	 * @param valor
	 *            "0" ou "1"
	 * @return "0" para false, "1" para true
	 */
	public static boolean converterStringCharParaBooleano(String valor) {

		return converterStringCharParaBooleano(valor, false);
	}
	
	/**
	 * Converte as strings "0" e "1" para false e true respectivamente.
	 * Caso isStringUmParaVerdade seja true, "1" retorna true e qualquer outra string
	 * retorna false.
	 *
	 * @param valor the valor
	 * @param isStringUmParaVerdade the is string um para verdade
	 * @return true, if successful
	 */
	public static boolean converterStringCharParaBooleano(String valor, 
					boolean isStringUmParaVerdade) {

		if(StringUtils.equals(valor, "1") || StringUtils.equals(valor, "true")) {
			return true;
		}
		
		if (!isStringUmParaVerdade) {			
			if(StringUtils.equals(valor, "0") || StringUtils.equals(valor, "false")) {
				return false;
			}
			throw new IllegalArgumentException(valor + " nao e char booleano. So \"0\", \"false\", \"1\" ou \"true\" sao permitidos.");
		}
		
		return false;
	}

	/**
	 * Converter string char para string booleano.
	 * 
	 * @param valor
	 *            the valor
	 * @return the string
	 */
	public static String converterStringCharParaStringBooleano(String valor) {

		return Boolean.toString(converterStringCharParaBooleano(valor));
	}

}
