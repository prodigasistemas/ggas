/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;

/**
 * 
 *
 */
@SuppressWarnings("unchecked")
public class TrilhaAuditoriaUtil {

	private static final String ID_SESSION_FACTORY = "sessionFactory";

	private static Map<String, List<String>> MAP_TRILHA_AUDITORIA;
	
	private static Map<String, Collection<Coluna>> mapaColunas = new HashMap<>();

	

	//
	// by
	// gmatos
	// on
	// 15/10/09
	// 10:22

	/**
	 * Gets the trilha auditoria.
	 * 
	 * @param nomeInterface
	 *            the nome interface
	 * @return the trilha auditoria
	 */
	public static List<String> getTrilhaAuditoria(String nomeInterface) {

		List<String> trilhaAuditoria = null;

		verificarTabelaColunaAuditavel(nomeInterface);

		if(MAP_TRILHA_AUDITORIA != null && !MAP_TRILHA_AUDITORIA.isEmpty()) {
			trilhaAuditoria = MAP_TRILHA_AUDITORIA.get(nomeInterface);
		}

		return trilhaAuditoria;
	}

	/**
	 * Verificar tabela coluna auditavel.
	 * 
	 * @param nomeInterface
	 *            the nome interface
	 */
	private static void verificarTabelaColunaAuditavel(String nomeInterface) {

		MAP_TRILHA_AUDITORIA = new HashMap<String, List<String>>();

		SessionFactory sessionFactory = (SessionFactory) ServiceLocator.getInstancia().getBeanPorID(ID_SESSION_FACTORY);
		Session session = null;

		try {
			if (!mapaColunas.containsKey(nomeInterface)) {

				StringBuilder hql = new StringBuilder();
				hql.append(" select colunaTabela ");
				hql.append(" from ");
				hql.append(ServiceLocator.getInstancia().getClassPorID(Coluna.BEAN_ID_COLUNA_TABELA).getSimpleName());
				hql.append(" colunaTabela ");
				hql.append(" where ");
				hql.append(" colunaTabela.habilitado = true ");
				hql.append(" and colunaTabela.auditavel = true");
				hql.append(" and colunaTabela.nomePropriedade is not null ");
				hql.append(" and colunaTabela.tabela.auditavel = true ");
				hql.append(" and colunaTabela.tabela.habilitado = true ");
				hql.append(" and colunaTabela.tabela.nomeClasse = :nomeClasse ");

				session = (Session) sessionFactory.openSession();
				Query query = session.createQuery(hql.toString());
				query.setString("nomeClasse", nomeInterface);

				mapaColunas.put(nomeInterface, query.list());
			}

			for (Coluna coluna : mapaColunas.get(nomeInterface)) {
				List<String> listaColunas = new ArrayList<String>();

				if(MAP_TRILHA_AUDITORIA.containsKey(coluna.getTabela().getNomeClasse())) {
					listaColunas = MAP_TRILHA_AUDITORIA.get(coluna.getTabela().getNomeClasse());
					listaColunas.add(coluna.getNomePropriedade());
					MAP_TRILHA_AUDITORIA.put(coluna.getTabela().getNomeClasse(), listaColunas);
				} else {
					listaColunas.add(coluna.getNomePropriedade());
					MAP_TRILHA_AUDITORIA.put(coluna.getTabela().getNomeClasse(), listaColunas);
				}
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Atualizar mapa trilha auditoria.
	 * 
	 * @param tabela
	 *            the tabela
	 */
	public static void atualizarMapaTrilhaAuditoria(Tabela tabela) {

		if(MAP_TRILHA_AUDITORIA == null) {
			MAP_TRILHA_AUDITORIA = new HashMap<String, List<String>>();
		}

		if(tabela.getAuditavel()) {
			MAP_TRILHA_AUDITORIA.put(tabela.getNomeClasse(), montarListaColunasAuditaveis(tabela));
		} else {
			MAP_TRILHA_AUDITORIA.remove(tabela.getNomeClasse());
		}
	}

	/**
	 * Montar lista colunas auditaveis.
	 * 
	 * @param tabela
	 *            the tabela
	 * @return the array list
	 */
	private static List<String> montarListaColunasAuditaveis(Tabela tabela) {

		List<String> listaColunasAuditaveis = new ArrayList<String>();

		if(tabela != null && tabela.getListaColunaTabela() != null) {
			for (Coluna coluna : tabela.getListaColunaTabela()) {
				if(coluna.getAuditavel()) {
					listaColunasAuditaveis.add(coluna.getNomePropriedade());
				}
			}
		}

		return listaColunasAuditaveis;
	}
	
	public static void limparCache(){
		mapaColunas = new HashMap<>();
	}


}
