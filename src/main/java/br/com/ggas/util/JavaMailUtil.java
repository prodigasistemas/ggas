/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import java.util.Properties;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;

public class JavaMailUtil extends JavaMailSenderImpl {

	public static final String BEAN_ID_JAVA_MAIL_UTIL = "javaMailUtil";

	/**
	 * Método responsável por enviar emails baseando-se nas configurações definidas
	 * nos parametros do sistema.
	 *
	 * @param remetente     the remetente
	 * @param destinatario  the destinatario
	 * @param subject       the subject
	 * @param conteudoEmail the conteudo email
	 * @throws GGASException the GGAS exception
	 */
	public void enviar(String remetente, String destinatario, String subject, String conteudoEmail)
			throws GGASException {

		enviar(remetente, destinatario, subject, conteudoEmail, null, null, false, false);
	}

	/**
	 * Método responsável por enviar emails html baseando-se nas configurações
	 * definidas nos parametros do sistema.
	 *
	 * @param assinatura    assinatura do email
	 * @param remetente     remetente do email
	 * @param destinatario  destinatario do email
	 * @param subject       assunto do email
	 * @param conteudoEmail conteudo do email
	 * @param isContentHtml se o conteudo é html
	 * @throws GGASException the GGAS exception
	 */
	public void enviar(String remetente, String destinatario, String subject, String conteudoEmail,
			Boolean isContentHtml, Boolean assinatura) throws GGASException {

		enviar(remetente, destinatario, subject, conteudoEmail, null, null, isContentHtml, assinatura);
	}

	/**
	 * Método responsável por enviar emails com anexo baseando-se nas configurações
	 * definidas nos parametros do sistema.
	 *
	 * @param assinatura         assinatura do email
	 * @param remetente          remetente do email
	 * @param destinatario       destinatario do email
	 * @param subject            assunto do email
	 * @param conteudoEmail      conteudo do email
	 * @param attachmentFilename anexo do email
	 * @param dataSource         source
	 * @param isContenteHtml     se o conteudo é html
	 * @throws GGASException     the GGAS exception
	 */
	public void enviar(String remetente, String destinatario, String subject, String conteudoEmail,
			String attachmentFilename, DataSource dataSource, Boolean isContenteHtml, Boolean assinatura)
			throws GGASException {
		try {
			this.send(montarMimeMessage(remetente, destinatario, subject, conteudoEmail, attachmentFilename, dataSource,
					isContenteHtml, assinatura));
		} catch (MailException e) {
			Log.error(e.getStackTrace(), e);
			throw new NegocioException(Constantes.ERRO_CONECTAR_SERVIDOR_EMAIL, true);
		}
	}
	
	/**
	 * Método responsável por enviar emails com anexo baseando-se nas configurações
	 * definidas nos parametros do sistema.
	 *
	 * @param remetente          the remetente
	 * @param destinatario       the destinatario
	 * @param subject            the subject
	 * @param conteudoEmail      the conteudo email
	 * @param attachmentFilename the attachment filename
	 * @param dataSource         the data source
	 * @throws GGASException the GGAS exception
	 */
	public void enviar(String remetente, String destinatario, String subject, String conteudoEmail,
			String attachmentFilename, DataSource dataSource) throws GGASException {

		enviar(remetente, destinatario, subject, conteudoEmail, attachmentFilename, dataSource, false, true);
	}

	/**
	 * Metodo que retorna um MimeMessage para o envio de email com o JavaMailSender.
	 *
	 * @param remetente          the remetente
	 * @param destinatario       the destinatario
	 * @param subject            the subject
	 * @param conteudoEmail      the conteudo email
	 * @param attachmentFilename the attachment filename
	 * @param dataSource         the data source
	 * @param isContentHtml      the isContentHtml
	 * @return the mime message
	 * @throws GGASException the GGAS exception
	 */
	private MimeMessage montarMimeMessage(String remetente, String destinatario, String subject, String conteudoEmail,
			String attachmentFilename, DataSource dataSource, Boolean isContentHtml, Boolean assinaturaEmail)
			throws GGASException {

		MimeMessage message = this.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			// configurações de SMTP
			this.setHost(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_HOST).getValor());
			this.setPort(Integer.parseInt(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PORT).getValor()));

			String autenticacao = obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_AUTH).getValor();
			if (!"0".equals(autenticacao)) {
				this.setUsername(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_USER).getValor());
				this.setPassword(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PASSWD).getValor());
			}

			// configurações do helper
			helper = new MimeMessageHelper(message, true);

			// verifica se há um remetente
			// especifico para a mensagem.
			if (remetente != null && remetente.trim().length() > 0) {
				helper.setFrom(remetente);
			} else {
				// caso contrario, pesquisa o
				// remetente default
				helper.setFrom(obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO).getValor());
			}

			// destinatario
			String[] destinatarios = destinatario.split(";");
			helper.setTo(destinatarios);

			helper.setSubject(subject);

			String assinatura = obterParametroPorCodigo(Constantes.PARAMETRO_ASSINATURA_EMAIL).getValor();
			String email = conteudoEmail;

			if ((StringUtils.isNotBlank(assinatura)) && (assinaturaEmail)) {
				if (isContentHtml) {
					email = conteudoEmail.concat("<br />").concat(assinatura.replaceAll("(\r\n|\n)", "<br />"));
				} else {
					email = conteudoEmail.concat("\r\n").concat(assinatura);
				}
			}

			helper.setText(email, isContentHtml);

			if (attachmentFilename != null) {
				helper.addAttachment(attachmentFilename, dataSource);
			}
		} catch (MessagingException e) {
			throw new GGASException(e.getMessage(), e);
		}
		return message;

	}

	/**
	 * Metodo que retorna o parametro de sistema pelo seu respectivo codigo.
	 *
	 * @param codigo the codigo
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	private ParametroSistema obterParametroPorCodigo(String codigo) throws NegocioException {

		return ((ControladorParametroSistema) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA))
						.obterParametroPorCodigo(codigo);
	}

	/**
	 * Metodo que retorna o Controlador Empresa pelo seu respectivo codigo.
	 *
	 * @return the parametro sistema
	 */
	private Empresa obterobterLogoEmpresaPrincipal() {

		return ((ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA)).obterLogoEmpresaPrincipal();
	}

	/**
	 * Metodo que retorna um MimeMessage para o envio de email com o JavaMailSender.
	 *
	 * @param remetente     the remetente
	 * @param destinatario  the destinatario
	 * @param subject       the subject
	 * @param conteudoEmail the conteudo email
	 * @param isContentHtml the isContentHtml
	 * @return the mime message
	 * @throws GGASException the GGAS exception
	 */

	private MimeMessage montarMimeMessageChamado(String remetente, String destinatario, String subject,
			String conteudoEmail, Boolean isContentHtml) throws GGASException {

		MimeMessage message = this.createMimeMessage();
		MimeMessageHelper helper = null;
		try {
			// configurações de SMTP
			this.setHost(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_HOST).getValor());
			this.setPort(Integer.parseInt(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PORT).getValor()));

			// verifica se o servidor requer
			// autenticação de segurança.
			String autenticacao = obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_AUTH).getValor();
			if (!"0".equals(autenticacao)) {
				this.setUsername(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_USER).getValor());
				this.setPassword(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PASSWD).getValor());
			}

			// configurações do helper
			helper = new MimeMessageHelper(message, true);

			// verifica se há um remetente
			// especifico para a mensagem.
			if (remetente != null && remetente.trim().length() > 0) {
				helper.setFrom(remetente);
			} else {
				// caso contrario, pesquisa o
				// remetente default
				helper.setFrom(obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO).getValor());
			}

			// destinatario
			String[] destinatarios = destinatario.split(";");
			helper.setTo(destinatarios);

			helper.setSubject(subject);

			String assinatura = obterParametroPorCodigo(Constantes.PARAMETRO_ASSINATURA_EMAIL).getValor();
			String email = conteudoEmail;

			if (StringUtils.isNotBlank(assinatura)) {
				assinatura = Util.converterISOtoUTF8(assinatura);
				if (isContentHtml) {

					Empresa empresa = obterobterLogoEmpresaPrincipal();
					String caminhoImagem = Constantes.URL_LOGOMARCA_EMPRESA
							+ String.valueOf(empresa.getChavePrimaria());
					email = conteudoEmail.concat("<br />").concat(assinatura.replaceAll("(\r\n|\n)", "<br />"));
					String[] emailSeparado = email.split("-");
					String cabecalho = "<!DOCTYPE html><html><head><meta charset=\"iso-8859-1\"></head>"
							+ "<table border=1 cellspacing=3 cellpadding=0 "
							+ "width=\"100%\" style='width:100.0%;mso-cellspacing:2.2pt;mso-yfti-tbllook:1184;"
							+ "mso-padding-alt:0cm 0cm 0cm 0cm'>"
							+ "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>"
							+ "<td style='padding:.75pt .75pt .75pt .75pt'>" + "<div style=\"margin-left:5px;\">"
							+ "<p><img border=0 src=\"" + caminhoImagem + "\" height=\"190\" width=\"214\" ></p>"
							+ "</div>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#0066CC; text-align: left; "
							+ "padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"white\" size=\"4\">Protocolo de Atendimento</font>" + "</th>" + "</tr>"
							+ "</table>" + "<br/>" + "<div style=\"width: 100%; margin-left: 25px; border-color:gray; "
							+ "padding:2pt 3.75pt 2pt 3.75pt\">"
							+ "<font color=\"black\" size=\"4\">Prezado cliente,<br/>"
							+ "<br/>Seguem abaixo os dados relativos ao atendimento obtido atrav&eacute;s "
							+ "do nosso SAC:</font></br>" + "</div>" + "<br/>";

					String[] dados = emailSeparado[0].split(";");

					String corpo = "<div style=\"width: 90%; margin-left: 25px; border-color:gray; padding:2pt 3.75pt 2pt 3.75pt\">"
							+ "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left; border-radius: 10px;"
							+ " padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>CLIENTE:</b></font> "
							+ Util.converterISOtoUTF8(dados[0]) + "</th>" + "</tr>" + "</table>"
							+ "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left; border-radius: 10px;"
							+ " padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>PROTOCOLO:</b></font> " + dados[1] + "</th>"
							+ "</tr>" + "</table>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left;  border-radius: 10px;"
							+ " padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>DATA E HORA ATENDIMENTO:</b></font> " + dados[2]
							+ "</th>" + "</tr>" + "</table>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left; border-radius: 10px; "
							+ "padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>PREVIS&Atilde;O DE ENCERRAMENTO:</b></font> "
							+ dados[3] + "</th>" + "</tr>" + "</table>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left; border-radius: 10px; "
							+ "padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>ASSUNTO:</b></font> "
							+ Util.converterISOtoUTF8(dados[4]) + "</th>" + "</tr>" + "</table>"
							+ "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#F0F0F0; font-weight:normal; text-align: left; border-radius: 10px;"
							+ " padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"black\" size=\"4\"><b>STATUS:</b></font> " + dados[5] + "</th>" + "</tr>"
							+ "</table>" + "</div>" + "<br/>" + "</td></tr></table></html>";

					message.setContent(cabecalho + corpo + assinatura, "text/html");
				} else {
					email = conteudoEmail.concat("\r\n").concat(assinatura);
				}
			}
			helper.setText(email, isContentHtml);
		} catch (MessagingException e) {
			throw new GGASException(e.getMessage(), e);
		}

		return message;
	}
	
	/**
	 * Método responsável por enviar emails com anexo baseando-se nas configurações
	 * definidas nos parametros do sistema.
	 *
	 * @param remetente      remetente do email
	 * @param destinatario   destinário do email
	 * @param subject        assunto do email
	 * @param conteudoEmail  conteudo do email
	 * @param isContenteHtml se o conteudo do email é html ou nao
	 * @throws GGASException falha lançada caso ocorra erro ao se conectar com o
	 *                       servidor de email
	 */
	public void enviarChamado(String remetente, String destinatario, String subject, String conteudoEmail,
			Boolean isContenteHtml) throws GGASException {

		try {
			this.send(montarMimeMessageChamado(remetente, destinatario, subject, conteudoEmail, isContenteHtml));
		} catch (MailException e) {
			Log.error(e.getStackTrace(), e);
			throw new NegocioException(Constantes.ERRO_CONECTAR_SERVIDOR_EMAIL, true);
		}
	}

	/**
	 * Método responsável por enviar email de agendamento de serviços baseando-se
	 * nas configurações definidas nos parametros do sistema.
	 *
	 * @param destinatario    destinário do email
	 * @param subject         assunto do email
	 * @param conteudoEmail   conteudo do email
	 * @param isVeioDoChamado se veio do chamado
	 * @param isContenteHtml  se o conteudo do email é html ou nao
	 * @throws GGASException falha lançada caso ocorra erro ao se conectar com o
	 *                       servidor de email
	 */
	public void enviarAgendamentoServico(String destinatario, String subject, String conteudoEmail,
			Boolean isVeioDoChamado, Boolean isContenteHtml) throws GGASException {

		try {
			this.send(montarMimeMessageAgendamentoServico(destinatario, subject, conteudoEmail, isVeioDoChamado,
					isContenteHtml));
		} catch (MailException e) {
			Log.error(e.getStackTrace(), e);
			throw new NegocioException(Constantes.ERRO_CONECTAR_SERVIDOR_EMAIL, true);
		}
	}

	/**
	 * Metodo que retorna um MimeMessage para o envio de email com o JavaMailSender.
	 *
	 * @param destinatario    the destinatario
	 * @param subject         the subject
	 * @param conteudoEmail   the conteudo email
	 * @param isVeioDoChamado the isVeioDoChamado
	 * @param isContentHtml   the isContentHtml
	 * @return the mime message
	 * @throws GGASException the GGAS exception
	 */

	private MimeMessage montarMimeMessageAgendamentoServico(String destinatario, String subject, String conteudoEmail,
			Boolean isVeioDoChamado, Boolean isContentHtml) throws GGASException {

		MimeMessage message = this.createMimeMessage();
		MimeMessageHelper helper = null;
		String cabecalho = "";

		try {
			// configurações de SMTP
			this.setHost(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_HOST).getValor());
			this.setPort(Integer.parseInt(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PORT).getValor()));

			// verifica se o servidor requer
			// autenticação de segurança.
			String autenticacao = obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_AUTH).getValor();
			if (!"0".equals(autenticacao)) {
				this.setUsername(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_USER).getValor());
				this.setPassword(obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_SMTP_PASSWD).getValor());
			}

			// configurações do helper
			helper = new MimeMessageHelper(message, true);

			// remetente
			helper.setFrom(obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO).getValor());

			// destinatario
			String[] destinatarios = destinatario.split(";");
			helper.setTo(destinatarios);

			helper.setSubject(subject);

			String email = conteudoEmail;

			if (isContentHtml) {
				Empresa empresa = obterobterLogoEmpresaPrincipal();
				String caminhoImagem = Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria());
				String[] emailSeparado = email.split("-");
				if (isVeioDoChamado) {
					cabecalho = "<!DOCTYPE html><html><head><meta charset=\"iso-8859-1\">" + "</head>"
							+ "<table border=1 cellspacing=3 cellpadding=0 "
							+ "style='width:100.0%;mso-cellspacing:2.2pt;mso-yfti-tbllook:1184;"
							+ "mso-padding-alt:0cm 0cm 0cm 0cm'>"
							+ "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>"
							+ "<td style='padding:auto'>" + "<div style=\"margin-left:5px;\">"
							+ "<p><img border=0 src=\"" + caminhoImagem + "\" height=\"190\" width=\"214\" ></p>"
							+ "</div>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#0066CC; text-align: left; "
							+ "padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"white\" size=\"4\">Confirmação de Agendamento</font>" + "</th>" + "</tr>"
							+ "</table>" + "<br/>" + "<div style=\"width: 100%; border-color:gray; "
							+ "padding:0 20px\">" + "<font color=\"black\" size=\"4\">Prezado cliente,<br/>"
							+ "<br/>Seja bem vindo à Companhia de Gás da Bahia - Bahiagás</br>"
							+ "Informamos que o número do protocolo referente às informações obtidas através "
							+ "do nosso SAC em </b></font> " + emailSeparado[0] + "é </b></font>" + emailSeparado[1]
							+ "</br>"
							+ "</br> Seguem abaixo os dados do seu agendamento. Lembramos que o acesso do técnico ocorrerá somente "
							+ "a presença de um responsável, maior de 18 anos." + "</div>" + "<br/>";
				} else {
					cabecalho = "<!DOCTYPE html><html><head><meta charset=\"iso-8859-1\">" + "</head>"
							+ "<table border=1 cellspacing=3 cellpadding=0 "
							+ "width=\"100%\" style='width:100.0%;mso-cellspacing:2.2pt;mso-yfti-tbllook:1184;"
							+ "mso-padding-alt:0cm 0cm 0cm 0cm'>"
							+ "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>"
							+ "<td style='padding: auto'>" + "<div style=\"margin-left:5px;\">"
							+ "<p><img border=0 src=\"" + caminhoImagem + "\" height=\"190\" width=\"214\" ></p>"
							+ "</div>" + "<table style=\"width:100%\">" + "<tr>"
							+ "<th style=\"background:#0066CC; text-align: left; "
							+ "padding:7.5pt 3.75pt 7.5pt 22.5pt \">"
							+ "<font color=\"white\" size=\"4\">Confirmação de Agendamento</font>" + "</th>" + "</tr>"
							+ "</table>" + "<br/>" + "<div style=\"width: 100%; border-color:gray; "
							+ "padding:0 20px\">" + "<font color=\"black\" size=\"4\">Prezado cliente,<br/>"
							+ "<br/>Seja bem vindo à Companhia de Gás da Bahia - Bahiagás.<br/>"
							+ "<br/> Seguem abaixo os dados do seu agendamento. Lembramos que o acesso do técnico ocorrerá somente "
							+ "a presença de um responsável, maior de 18 anos." + "</div>" + "<br/>";
				}
				String[] dados = emailSeparado[0].split(";");

				String corpo = "<div style=\"width: 100%; border-color:gray; padding:5px 20px\">"
						+ "<table style=\"width:100%;\">" + "<tr>"
						+ "<th style=\"background:#000000; font-weight:normal; text-align: center; border: 10px; "
						+ "padding:auto\">" + "<font color=\"white\" size=\"4\"><b>CLIENTE:</b></font> "
						+ Util.converterISOtoUTF8(dados[2]) + "</th>" + "</tr>" + "</table>"
						+ "<table style=\"width:100%\">" + popularTabelaAgendamentoServico(dados) + "</table>"
						+ popularCorpoEmailAgendamentoServico(dados) + "</table>" + "<br/>" + "</td></tr></table>"
						+ "<table style=\"margin-right: 40px; margin-left: 40px; width: 95%;\"> "
						+ popularRodape(caminhoImagem) + "/table>  " + "</html>";

				message.setContent(cabecalho + corpo, "text/html");
			} else {
				email = conteudoEmail;
			}
			helper.setText(email, isContentHtml);
		} catch (MessagingException e) {
			throw new GGASException(e.getMessage(), e);
		}

		return message;
	}

	/**
	 * Metodo que retorna um rodape para um email de agendamento de servico.
	 *
	 * @param caminho da imagem
	 * @return um rodape
	 */

	private String popularRodape(String caminhoImagem) {
		return "<tr>" + "<td style=\"width: 1%;\"><img border=0 src=\"" + caminhoImagem
				+ "\" height=\"80\" width=\"120\"></td> " + "<td style=\"width: 99%;\">Atendimento Bahiagás</br> "
				+ "SAC: 0800 071 9111<br/> " + "Emergência: 0800 284 1111<br/> " + "Website: www.bahiagas.com.br</td> "
				+ "<tr> "
				+ "<td  colspan=\"2\" style=\"width: 100%; background-color: #87CEFA;\">Bahiagás - Companhia de Gás da Bahia.<br/> "
				+ "Av. Prof. Magalhães Neto, nº 1838. Ed. Civil Business, Pituba, Salvador/BA.</td> " + "</tr> "
				+ "<tr> " + "<td colspan=\"2\" style=\"width: 100%; text-align: center;\">"
				+ "<br/><br/><br/>\"O conteúdo deste documento é de propriedade da Companhia de Gás da Bahia - Bahiagás e contém "
				+ "informações confidenciais, de uso exclusivo de seus proprietários e pessoas autorizadas. O acesso às informações "
				+ "contidas neste documento é proibido a terceiros. Cabe ao destinatário cuidar quanto ao tratamento adequado. Sem a "
				+ "devida autorização, a divulgação, a reprodução, a distribuição ou qualquer outra ação em desconformidade com as normas "
				+ "internas da empresa são proibidas e passíveis de sanção disciplinar, cível e criminal. Caso não seja uma das pessoas autorizadas "
				+ "a acessar as informações aqui contidas, destrua-a imediatamente.\" " + "</tr> ";
	}

	/**
	 * Metodo que retorna um corpo para um email de agendamento de servico.
	 *
	 * @param dados
	 * @return um corpo para um email de agendamento de servico
	 */
	private String popularCorpoEmailAgendamentoServico(String[] dados) {
		return "</div>"
				+ "<table style=\"width: 97.2%; margin: 20px 20px 20px 20px; padding: 5px 60px; background-color: #F0F8FF;\"> "
				+ "<th> " + "<tr style=\"text-align: justify;\"><font color=\"black\" size=\"2\"> "
				+ "<p>Para a sua segurança e comodidade, recomendamos que leia com atenção abaixo, de modo a garantir a realização do serviço.</p> "
				+ "<p><b>1. Verifique se os aparelhos a gás (fogão, forno aquecedor, etc.) são próprios para uso do gás natural.</b></p> "
				+ "<p>Estes aparelhos podem ser adiquiridos diretamente nas lojas ou pela internet. Caso não possuam esta característica, "
				+ "os mesmos deverão ser CONVERTIDOS PARA GÁS NATURAL "
				+ "antes de serem utilizados. A Bahiagás recomenda que a conversão seja realizada pela Assitência Técnica Autorizada do próprio fabricante,"
				+ " de modo a preservar a originalidade e manter a garantia do aparelho. "
				+ "Para maiores informações, consulte o manual do produto.</p> "
				+ "<p><b>2. Verifique se o mangote flexível (mangueira) que está aclopado ao aparelho "
				+ "possui a especificação técnica recomendada por uma das seguintes normas:</b></p> "
				+ "<p>a. NBR 13419: mangueira flexível de borracha;<br/> "
				+ "b. NBR 14177: tubo flexível metálico;<br/> "
				+ "c. NBR 14955: tubo flexível de borracha para uso em instalações de GLP/GN.</p> "
				+ "<p>NOTA: Para aparelhos de cocção (fogão, cooktop e forno) a mangueria deve possuir"
				+ " comprimento entre 1,20m e 1,25m e conexões de 1/2\"(meia polegada), sendo um lado tipo<br/>"
				+ "\"fêmea\" giratório e o outro tipo \"macho\" fixo ou rotativo. "
				+ "No caso de aquecedores de água a gás as dimensões deverão ser especificadas pelas assistência técnica do aparelho. "
				+ "<b>3. Certifique-se de que o ponto de saída de gás está desobstruído e em local de fácil acesso,"
				+ " possibilitando a abertura e fechamento da válvula, conforme recomendação "
				+ "da NBR 15.526.</b></p>"
				+ "<p><b>4. No ponto de sáida de gás, utilize válvula tipo esfera angular (90\") para gás,"
				+ " diâmetro nominal: ½\", extremidades: ½\" BSP macho x ½\" BSP macho passagem plena ou "
				+ "reduzida, material: latão forjado ou fundido, classe de pressão: 5 kgf/cm² ou superior.</b></p>"
				+ "<p>A Bahiagás recomenda a todos os seus clientes que utilizem de forma segura o gás natural,"
				+ " por isso as instalações internas devem atender a todas as normas brasileiras "
				+ " vigentes para utilização de gás. Se no decurso da inspeção o técnico da Bahiagás identificar"
				+ " qualquer inconformidade que impeça a utilização do gás em condição segura, a sua "
				+ "liberação não será realizada. Nesta situação, o cliente deverá providenciar"
				+ " as correções necessárias e reagendar o serviço através do SAC.</p> "
				+ "<p><b>ATENÇÃO: Os técnicos da Bahiagás ou seus terceiros não estão autorizados a realizar"
				+ " venda de materiais ou prestar qualquer serviço, além do especificado na ordem da<br/> "
				+ "PROGRAMAÇÃO DIÁRIA.</b></p> "
				+ "<p>Em caso de dúvidas o nosso Serviço de Atendimento ao Cliente permanece "
				+ "à sua inteira disposição através do telefone <b>0800 071 9111</b> ou do link "
				+ "<a href=http://clienteonline.bahiagas.com.br/seab/contato/faleconosco.seam>www.bahiagas.com.br/faleconosco.</a></p>"
				+ "<p>Será sempre um prazer atendê-lo.</p> " + "</font>" + "</tr>" + "</th>";
	}

	/**
	 * Metodo que retorna uma tabela para um email de agendamento de servico.
	 *
	 * @param dados
	 * @return uma tabela para um email de agendamento de servico.
	 */
	private String popularTabelaAgendamentoServico(String[] dados) {
		return "<tr>" + "<th style=\"background:#B0C4DE; font-weight:normal; width: 12%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>DATA</b></font> " + "</th>"
				+ "<th style=\"background:#B0C4DE; font-weight:normal; width: 10%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>TURNO</b></font> " + "</th>"
				+ "<th style=\"background:#B0C4DE; font-weight:normal;  width: 39%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>OCORRÊNCIA</b></font> " + "</th>"
				+ "<th style=\"background:#B0C4DE; font-weight:normal;  width: 39%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>OBSERVAÇÃO</b></font> " + "</th>" + "</tr>"
				+ "<tr>" + "<th style=\"background:#F0F8FF; font-weight:normal; width: 12%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>" + dados[0] + "</b></font></th> "
				+ "<th style=\"background:#F0F8FF; font-weight:normal; width: 10%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>" + dados[3] + "</b></font></th> "
				+ "<th style=\"background:#F0F8FF; font-weight:normal;  width: 39%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>" + dados[5] + "</b></font></th> "
				+ "<th style=\"background:#F0F8FF; font-weight:normal;  width: 39%; text-align: left; "
				+ "padding:auto\">" + "<font color=\"black\" size=\"4\"><b>" + dados[4] + "</b></font></th> " + "</tr>";
	}
}
