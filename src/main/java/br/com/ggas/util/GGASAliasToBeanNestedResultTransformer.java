/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.property.PropertyAccessor;
import org.hibernate.property.PropertyAccessorFactory;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.transform.BasicTransformerAdapter;
import org.hibernate.transform.ResultTransformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementaçã ode Trasnformer Adapter que permite construir objetos que estão inseridos como propriedade dentro de outros
 * objetos
 * @author jose.victor@logiquesistemas.com.br
 */
public class GGASAliasToBeanNestedResultTransformer extends BasicTransformerAdapter {

	private static final Logger LOG = Logger.getLogger(GGASAliasToBeanNestedResultTransformer.class);

	private final Class<?> resultClass;
	private final Map<Class, Class> mapEntities;

	/**
	 * Construtor que informa a classe de resultado e mapear as transformações das entidades
	 * @param resultClass classe resultante da transformação
	 * @param mapEntities mapa de entidades que deverão ser transformadas. Use para definir implementações de interfaces
	 */
	public GGASAliasToBeanNestedResultTransformer(Class<?> resultClass, Map<Class, Class> mapEntities) {
		this.resultClass = resultClass;
		this.mapEntities = mapEntities;
	}

	/**
	 * Construtor padrão, sem definição de mapeamentos
	 * @param resultClass classe resultante
	 */
	public GGASAliasToBeanNestedResultTransformer(Class<?> resultClass) {
		this(resultClass, new HashMap<>());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {

		Map<Class<?>, List<?>> subclassToAlias = new HashMap<>();
		List<String> nestedAliases = identificarAliasAninhados(tuple, aliases, subclassToAlias);

		Object[] novaTupla = new Object[aliases.length - nestedAliases.size()];
		String[] novosAlias = new String[aliases.length - nestedAliases.size()];
		substituirAliasAninhados(tuple, aliases, nestedAliases, novaTupla, novosAlias);

		ResultTransformer rootTransformer = new AliasToBeanResultTransformer(resultClass);
		Object root = rootTransformer.transformTuple(novaTupla, novosAlias);
		construirObjetosAninhados(subclassToAlias, root);

		return root;
	}

	/**
	 * Popula os objetos aninhados dentro do objeto root
	 * @param subclassToAlias mapa que contém a lista de subclasses aninhadas e seus respectivos alias
	 * @param root objeto root
	 */
	private void construirObjetosAninhados(Map<Class<?>, List<?>> subclassToAlias, Object root) {
		final int ENTIDADE = 2;
		for (Map.Entry<Class<?>, List<?>> entry : subclassToAlias.entrySet()) {
			final Class subclassMapped = this.mapEntities.getOrDefault(entry.getKey(), entry.getKey());
			ResultTransformer subclassTransformer = new AliasToBeanResultTransformer(subclassMapped);
			Object subObject = subclassTransformer.transformTuple(
					((List<Object>) entry.getValue().get(0)).toArray(),
					((List<Object>) entry.getValue().get(1)).toArray(new String[0])
			);

			PropertyAccessor accessor = PropertyAccessorFactory.getPropertyAccessor("property");
			accessor.getSetter(resultClass, (String) subclassToAlias.get(entry.getKey()).get(ENTIDADE)).set(root, subObject, null);

		}
	}

	/**
	 * Substtui os alias nos arrays de tuplas
	 * @param tuple tupla
	 * @param aliases alias
	 * @param nestedAliases alias aninhados
	 * @param newTuple nova tupla
	 * @param newAliases novos alias
	 */
	private void substituirAliasAninhados(Object[] tuple, String[] aliases, List<String> nestedAliases, Object[] newTuple,
			String[] newAliases) {
		int i = 0;
		for (int j = 0; j < aliases.length; j++) {
			if (!nestedAliases.contains(aliases[j])) {
				newTuple[i] = tuple[j];
				newAliases[i] = aliases[j];
				++i;
			}
		}
	}

	/**
	 * Identifica os alias que estão aninhados isto é, que possui separação por '.'
	 * @param tuple tupla
	 * @param aliases alias
	 * @param subclassToAlias mapa com as informações de mapeamento de classes e suas subclasses
	 * @return retorna a lista de string de alias encadeados
	 */
	private List<String> identificarAliasAninhados(Object[] tuple, String[] aliases, Map<Class<?>, List<?>> subclassToAlias) {
		List<String> aliasEncadeados = new ArrayList<>();

		try {
			for (int i = 0; i < aliases.length; i++) {
				identificarAlias(tuple[i], aliases[i], subclassToAlias, aliasEncadeados);
			}
		} catch (NoSuchFieldException e) {
			LOG.error("Não foi possvíel isntanciar a classe", e);
			throw new HibernateException( "Não foi possível instanciar a classe: " + resultClass.getName() );
		}

		return aliasEncadeados;
	}

	/**
	 * Identifica o alinhas que possui separação por '.'
	 * @param e objeto
	 * @param alias alias
	 * @param subclassToAlias subclasse do alias
	 * @param aliasEncadeados alias encadeados
	 * @throws NoSuchFieldException exceção lançada quando o campo não é identificado
	 */
	private void identificarAlias(Object e, String alias, Map<Class<?>, List<?>> subclassToAlias, List<String> aliasEncadeados)
			throws NoSuchFieldException {

		if (alias.contains(".")) {
			aliasEncadeados.add(alias);

			String[] sp = alias.split("\\.");
			String nomeCampo = sp[0];
			String nomeAlias = sp[1];

			Class<?> subclass = resultClass.getDeclaredField(nomeCampo).getType();

			if (!subclassToAlias.containsKey(subclass)) {
				List<Object> list = new ArrayList<>();
				list.add(new ArrayList<>());
				list.add(new ArrayList<String>());
				list.add(nomeCampo);
				subclassToAlias.put(subclass, list);
			}
			((List<Object>)subclassToAlias.get(subclass).get(0)).add(e);
			((List<String>)subclassToAlias.get(subclass).get(1)).add(nomeAlias);
		}
	}

}
