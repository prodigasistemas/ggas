package br.com.ggas.web.cobranca.extratodebito;

/**
 * 
 * @author pedro
 *	Classe responsável pela transferência de dados entre as telas de Extrato Débito
 */
public class ExtratoDebitoVO {

	private Boolean indicadorPesquisaGeral;
	private Long[] chavesCreditoDebito = new Long[0]; 
	private String indicadorPesquisa;
	private String numeroContrato;
	private String dataVencimento;
	private Long idPontoConsumo;
	private Long[] chavesFatura = new Long[0];
	private String anoGeracao;
	private Long idCliente;
	private Long idImovel;
	
	public Boolean getIndicadorPesquisaGeral() {
		return indicadorPesquisaGeral;
	}
	public void setIndicadorPesquisaGeral(Boolean indicadorPesquisaGeral) {
		this.indicadorPesquisaGeral = indicadorPesquisaGeral;
	}
	public Long[] getChavesCreditoDebito() {
		Long[] chavesCredDeb = new Long[0];
		if(this.chavesCreditoDebito.length > 0) {
			chavesCredDeb = this.chavesCreditoDebito.clone();
		}
		return chavesCredDeb;
	}
	public void setChavesCreditoDebito(Long[] chavesCreditoDebito) {
		if(chavesCreditoDebito != null) {
			this.chavesCreditoDebito = chavesCreditoDebito.clone();
		}
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public Long[] getChavesFatura() {
		Long[] chavesFat = new Long[0];
		if(this.chavesFatura.length > 0) {
			chavesFat = chavesFatura.clone();
		}
		return chavesFat;
	}
	public void setChavesFatura(Long[] chavesFatura) {
		if(chavesFatura != null) {
			this.chavesFatura = chavesFatura.clone();
		}
	}
	public String getAnoGeracao() {
		return anoGeracao;
	}
	public void setAnoGeracao(String anoGeracao) {
		this.anoGeracao = anoGeracao;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	
}
