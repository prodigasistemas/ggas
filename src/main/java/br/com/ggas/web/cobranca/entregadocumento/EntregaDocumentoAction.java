/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 04/12/2013 10:03:51
 * @author wcosta
 */

package br.com.ggas.web.cobranca.entregadocumento;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.impl.EntregaDocumentoImpl;
import br.com.ggas.cobranca.entregadocumento.impl.EntregaDocumentoVO;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelas telas relacionadas
 * as entregas de documentos. 
 *
 */
@Controller
public class EntregaDocumentoAction extends GenericAction {

	private static final String DATA_DA_SITUACAO = "Data da situação";

	private static final String IMOVEL = "imovel";

	private static final String TELA_EXIBIR_ALTERAR_ENTREGA_DOCUMENTO = "exibirAlterarEntregaDocumento";

	private static final String TELA_EXIBIR_PESQUISAR_ENTREGA_DOCUMENTO = "exibirPesquisarEntregaDocumento";

	private static final String TELA_EXIBIR_INCLUIR_ENTREGA_DOCUMENTO = "exibirIncluirEntregaDocumento";

	private static final String FORWARD_PESQUISAR_ENTREGA_DOCUMENTO = "forward:pesquisarEntregaDocumento";

	private static final String ENTREGA_DOCUMENTO = "entregaDocumento";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String HABILITADO = "habilitado";

	private static final String SITUACAO_ENTREGA = "situacaoEntrega";

	private static final String DATA_SITUACAO = "dataSituacao";

	private static final String DATA_VENCIMENTO = "dataVencimento";

	private static final String MOTIVO_NAO_ENTREGA = "motivoNaoEntrega";

	private static final String ID_CLIENTE = "idCliente";

	private static final String TIPO_DOCUMENTO = "tipoDocumento";

	private static final String ROTULO_DATA_SITUACAO = "Data da Situação";

	private String retorno;
	
	private static final String MSG_RETORNAR_PROTOCOLO = "Operação realizada com sucesso!";
	
	private static final String MSG_CANCELAMENTO_PROTOCOLO = "Protocolo cancelado com sucesso.";

	private static final String MSG_ITEM_COM_AS = "Não é possível cancelar pois essa Comunicação possui Autorização de Serviço.";
	
	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorArrecadacao")
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	@Autowired
	@Qualifier("controladorEntregaDocumento")
	private ControladorEntregaDocumento controladorEntregaDocumento;

	@Autowired
	@Qualifier("controladorAvisoCorte")
	private ControladorAvisoCorte controladorAvisoCorte;

	@Autowired
	@Qualifier("controladorCobranca")
	private ControladorCobranca controladorCobranca;
	
	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorServicoTipo controladorServicoTipo;
	
	@Autowired
	private ControladorComunicacao controladorComunicacao;
	

	/**
	 * 
	 * @param model
	 * @return exibirPesquisarEntregaDocumento
	 * @throws GGASException
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISAR_ENTREGA_DOCUMENTO)
	public String exibirPesquisarEntregaDocumento(Model model) throws GGASException {
		carregarCombos(model);
		return TELA_EXIBIR_PESQUISAR_ENTREGA_DOCUMENTO;
	}
	
	/**
	 * @param request - {@link Request}
	 * @param model - {@link Model}
	 * @return exibirPesquisaControleProtocolo
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaControleProtocolo")
	public String exibirPesquisaControleProtocolo(HttpServletRequest request, Model model) throws GGASException {
		model.addAttribute("listaGruposFaturamento", controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute("listaTipoProtocolo",
				new ArrayList<String>(Arrays.asList("Aviso de Corte","Negativação", "Comunicação")));
		model.addAttribute("listaServicoTipo", controladorServicoTipo.consultarServicoTipoComunicacao());
		
		if(!model.containsAttribute("isComunicacao")) {
			request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
		}
		
		
		carregarCombos(model);
		return "exibirPesquisaControleProtocolo";
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param results
	 * @param dataSituacao
	 * @param dataVencimento
	 * @param habilitado
	 * @param model
	 * @param imovel
	 * @param request
	 * @return exibirPesquisarEntregaDocumento
	 * @throws GGASException
	 * @{@link RETURN} exibirPesquisarEntregaDocumento
	 */
	@RequestMapping("pesquisarEntregaDocumento")
	public String pesquisarEntregaDocumento(@ModelAttribute("EntregaDocumentoImpl") EntregaDocumentoImpl entregaDocumento,
			BindingResult results, @RequestParam(value = "dataSituacao", required = false) String dataSituacao,
			@RequestParam(value = "dataVencimento", required = false) String dataVencimento,
			@RequestParam(value = "habilitado", required = false) String habilitado,
			@RequestParam(value = IMOVEL, required = false) Long imovel, HttpServletRequest request, Model model)
			throws GGASException {

		Map<String, Object> filtro = prepararFiltro(entregaDocumento, dataVencimento, habilitado);
		criarColecaoPaginada(request, filtro, controladorEntregaDocumento.listarEntregaDocumento(filtro));
		model.addAttribute(IMOVEL, imovel);
		model.addAttribute("habilitado", habilitado);
		model.addAttribute("dataVencimento", dataVencimento);
		model.addAttribute("dataSituacao", dataSituacao);
		model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
		
		Collection<EntregaDocumento> listaEntregaDocumento = controladorEntregaDocumento.listarEntregaDocumento(filtro);
		
		for(EntregaDocumento entregaDocAux : listaEntregaDocumento) {
			if(entregaDocAux.getAvisoCorte() != null) {
				DocumentoFiscal df = controladorFatura
						.obterDocumentoFiscalPorFaturaMaisRecente(entregaDocAux.getAvisoCorte().getFatura().getChavePrimaria());						
				if (df != null) {
					entregaDocAux.getAvisoCorte().setNumeroTitulo(df.getNumero());
				} else {
					entregaDocAux.getAvisoCorte().setNumeroTitulo(
							entregaDocAux.getAvisoCorte().getFatura().getChavePrimaria());
				}
			}
		}
		
		model.addAttribute("listaEntregaDocumento", listaEntregaDocumento);
		adicionarFiltroPaginacao(request, filtro);
		carregarCombos(model);

		return TELA_EXIBIR_PESQUISAR_ENTREGA_DOCUMENTO;
	}
	
	/**
	 * 
	 * @param entregaDocumentoVO - {@link EntregaDocumentoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link Request}
	 * @param model - {@link Model}
	 * @return pesquisarRetornoProtocolo
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarRetornoProtocolo")
	public String pesquisarRetornoProtocolo(EntregaDocumentoVO entregaDocumentoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(entregaDocumentoVO);
		model.addAttribute("entregaDocumentoVO", entregaDocumentoVO);
		Boolean isComunicacao = Boolean.FALSE;
		request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
		
		if("Comunicação".equals(entregaDocumentoVO.getTipoProtocolo())) {
			isComunicacao = Boolean.TRUE;
			FiltroAcompanhamentoLoteDTO filtroComunicacao = montarFiltroComunicacao(entregaDocumentoVO);
			
			Collection<LoteComunicacao> loteComunicacao = controladorComunicacao.consultarLoteComunicacao(filtroComunicacao);
			
			model.addAttribute("listaLoteAcompanhamento", loteComunicacao);
			request.getSession().setAttribute("listaLoteAcompanhamentoReq", loteComunicacao);
		} else {
			
			Collection<EntregaDocumento> listaEntregaDocumento = controladorEntregaDocumento.listarProtocolos(filtro);
			
			for(EntregaDocumento entregaDocAux : listaEntregaDocumento) {
				if(entregaDocAux.getAvisoCorte() != null || entregaDocAux.getFaturaClienteNegativacao() != null) {
					
					Fatura fatura = null;
					if(entregaDocAux.getAvisoCorte() != null) {
						fatura = entregaDocAux.getAvisoCorte().getFatura();
					} else {
						fatura = entregaDocAux.getFaturaClienteNegativacao().getFatura();
					}
					
					entregaDocAux.setFatura(fatura);
					
					DocumentoFiscal df = controladorFatura
							.obterDocumentoFiscalPorFaturaMaisRecente(fatura.getChavePrimaria());						
					if (df != null) {
						entregaDocAux.setNumeroTitulo(String.valueOf(df.getNumero()));
					} else {
						entregaDocAux.setNumeroTitulo(String.valueOf(fatura.getChavePrimaria()));
					}
				}
			}
			
			model.addAttribute("listaEntregaDocumento", listaEntregaDocumento);
			request.getSession().setAttribute("listaEntregaDocumentoReq", listaEntregaDocumento);
		}
		
		

		
		model.addAttribute("isComunicacao", isComunicacao);



		return exibirPesquisaControleProtocolo(request, model);
	}
	
	
	@RequestMapping("exibirProtocolosLoteComunicacao")
	public String exibirProtocolosLoteComunicacao(@RequestParam("idLoteComunicacao") Long idLoteComunicacao, EntregaDocumentoVO entregaDocumentoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		
		Map<String, Object> filtro = prepararFiltro(entregaDocumentoVO);
		filtro.put("idLoteComunicacao", idLoteComunicacao);
		model.addAttribute("entregaDocumentoVO", entregaDocumentoVO);
		
		Collection<EntregaDocumento> listaEntregaDocumento = controladorEntregaDocumento.listarProtocolos(filtro);
		
		Collection<LoteComunicacao> loteComunicacao  = (Collection<LoteComunicacao>) request.getSession().getAttribute("listaLoteAcompanhamentoReq");
		
	    request.getSession().setAttribute("listaEntregaDocumentoReq", listaEntregaDocumento);
		model.addAttribute("listaEntregaDocumento", listaEntregaDocumento);
		model.addAttribute("isComunicacao", Boolean.TRUE);
		model.addAttribute("listaLoteAcompanhamento", loteComunicacao);
		model.addAttribute("idLoteComunicacao", idLoteComunicacao);
		model.addAttribute("isComunicacao", Boolean.TRUE);

		
		return exibirPesquisaControleProtocolo(request, model);
	}

	private FiltroAcompanhamentoLoteDTO montarFiltroComunicacao(EntregaDocumentoVO entregaDocumentoVO) {
		FiltroAcompanhamentoLoteDTO filtro = new FiltroAcompanhamentoLoteDTO();
		filtro.setDescricaoPontoConsumo(entregaDocumentoVO.getDescricaoPontoConsumo());
		filtro.setDataGeracaoInicial(entregaDocumentoVO.getDataInicioEmissao());
		filtro.setDataGeracaoFinal(entregaDocumentoVO.getDataFimEmissao());
		filtro.setProtocoloRetornado(entregaDocumentoVO.getIndicadorRetornado());
		filtro.setIdGrupoFaturamento(entregaDocumentoVO.getIdGrupoFaturamento());
		filtro.setServicoTipo(entregaDocumentoVO.getServicoTipo());
		filtro.setLotesCartasCanceladas(entregaDocumentoVO.getLotesCartasCanceladas());
		
		return filtro;
	}

	private Map<String, Object> prepararFiltro(EntregaDocumentoVO entregaDocumentoVO) throws GGASException {
		Map<String, Object> filtro = new HashMap<String, Object>();
		Date fimRetorno = null;
		Date fimVencimento = null;

		Boolean indicadorRetornado = entregaDocumentoVO.getIndicadorRetornado();
		if (indicadorRetornado != null )  {
			filtro.put("indicadorRetornado", indicadorRetornado);
		} 
		
		String dataInicioEmissao = entregaDocumentoVO.getDataInicioEmissao();
		if (!StringUtils.isEmpty(dataInicioEmissao)) {
			Date inicioRetorno = Util.converterCampoStringParaData("Data Emissao Inicial", dataInicioEmissao, Constantes.FORMATO_DATA_BR);
			filtro.put("dataInicioEmissao", inicioRetorno);

			Date dataAtual = Calendar.getInstance().getTime();
			
			String dataFimEmissao = entregaDocumentoVO.getDataFimEmissao();
			if (DataUtil.gerarDataHmsZerados(inicioRetorno).compareTo(DataUtil.gerarDataHmsZerados(dataAtual)) <= 0
					&& dataFimEmissao == null || StringUtils.isEmpty(dataFimEmissao)) {
				fimRetorno = dataAtual;
				filtro.put("dataFimEmissao", fimRetorno);
			}
		}
		
		if (fimRetorno == null && !StringUtils.isEmpty(entregaDocumentoVO.getDataFimEmissao())) {

			Date dataFim = Util.converterCampoStringParaData("Data Emissao Final", entregaDocumentoVO.getDataFimEmissao(), Constantes.FORMATO_DATA_BR);	
		
			filtro.put("dataFimEmissao", Util.adicionarDiasData(dataFim, 1));
		}
		
		String dataInicioVencimento = entregaDocumentoVO.getDataInicioVencimento();
		if (!StringUtils.isEmpty(dataInicioVencimento)) {
			Date inicioVencimento = Util.converterCampoStringParaData("Data Vencimento Inicial", dataInicioVencimento, Constantes.FORMATO_DATA_BR);
			filtro.put("dataInicioVencimento", inicioVencimento);

			Date dataAtual = Calendar.getInstance().getTime();
			
			String dataFimVencimento = entregaDocumentoVO.getDataFimVencimento();
			if (DataUtil.gerarDataHmsZerados(inicioVencimento).compareTo(DataUtil.gerarDataHmsZerados(dataAtual)) <= 0
					&& dataFimVencimento == null || StringUtils.isEmpty(dataFimVencimento)) {
				fimVencimento = dataAtual;
				filtro.put("dataFimVencimento", fimVencimento);
			}
		}
		
		if (fimVencimento == null && !StringUtils.isEmpty(entregaDocumentoVO.getDataFimVencimento())) {
			Date dataFim = Util.converterCampoStringParaData("Data Vencimento Final", entregaDocumentoVO.getDataFimVencimento(), Constantes.FORMATO_DATA_BR);		
			filtro.put("dataFimVencimento", Util.adicionarDiasData(dataFim, 1));
		}
		
		Long idGrupoFaturamento = entregaDocumentoVO.getIdGrupoFaturamento();
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			filtro.put("idGrupoFaturamento", idGrupoFaturamento);
		}
		
		String tipoProtocolo = entregaDocumentoVO.getTipoProtocolo();
		if(tipoProtocolo != null && !"".equals(tipoProtocolo)) {
			filtro.put("tipoProtocolo", tipoProtocolo);
		}
		
		String descricaoPontoConsumo = entregaDocumentoVO.getDescricaoPontoConsumo();
		if (descricaoPontoConsumo != null && !descricaoPontoConsumo.isEmpty()) {
			filtro.put("descricaoPontoConsumo", descricaoPontoConsumo);
		}
		
		ServicoTipo servicoTipo = entregaDocumentoVO.getServicoTipo();
		if (servicoTipo != null) {
			filtro.put("chaveServicoTipo", servicoTipo.getChavePrimaria());
		}

		return filtro;
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param model
	 * @return exibirIncluirEntregaDocumento
	 * @throws GGASException
	 */
	@RequestMapping(TELA_EXIBIR_INCLUIR_ENTREGA_DOCUMENTO)
	public String exibirIncluirEntregaDocumento(EntregaDocumentoImpl entregaDocumento, Model model) throws GGASException {

		carregarListaDocumentoCobranca(entregaDocumento, model);
		carregarCombos(model);

		return TELA_EXIBIR_INCLUIR_ENTREGA_DOCUMENTO;
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param results
	 * @param dataSituacao
	 * @param dataVencimento
	 * @param documentos
	 * @param request
	 * @param model
	 * @param imovel
	 * @return pesquisarEntregaDocumento
	 * @throws GGASException
	 */
	@RequestMapping("incluirEntregaDocumento")
	public String incluirEntregaDocumento(@ModelAttribute("EntregaDocumentoImpl") EntregaDocumentoImpl entregaDocumento,
			BindingResult results, @RequestParam(value = "dataSituacao", required = false) String dataSituacao,
			@RequestParam(value = "dataVencimento", required = false) String dataVencimento,
			@RequestParam(value = "documentos", required = false) Long[] documentos,
			@RequestParam(value = IMOVEL, required = false) Long imovel, HttpServletRequest request, Model model) throws GGASException {
		EntregaDocumentoImpl entregaDocumentoAux = new EntregaDocumentoImpl();		
		carregarCombos(model);
		retorno = TELA_EXIBIR_INCLUIR_ENTREGA_DOCUMENTO;
		if (!"".equals(dataSituacao)) {
			entregaDocumento
					.setDataSituacao(Util.converterCampoStringParaData(DATA_DA_SITUACAO, dataSituacao, Constantes.FORMATO_DATA_BR));
		}
		model.addAttribute(IMOVEL, imovel);
		model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
		Map<String, Object> erros = entregaDocumento.validarDados();
		
		if ("PROTOCOLO".equals(entregaDocumento.getTipoDocumento().getDescricao())
				&& (documentos == null || documentos.length == 0)) {
			StringBuilder erroAviso = new StringBuilder();
			if (erros.get("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS") == null) {
				erroAviso.append("Aviso de Corte");
				erros.put("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS", erroAviso.toString());
			} else {
				erroAviso.append(", Aviso de Corte");
				erros.put("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS",
						erros.get("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS") + erroAviso.toString());
			}
		}
			
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
			carregarListaDocumentoCobranca(entregaDocumento, model);
		} else {
			entregaDocumentoAux.setCliente(entregaDocumento.getCliente());
			entregaDocumentoAux.setHabilitado(Boolean.TRUE);
			entregaDocumentoAux.setDataSituacao(entregaDocumento.getDataSituacao());
			entregaDocumentoAux.setTipoDocumento(entregaDocumento.getTipoDocumento());
			
			Collection<EntregaDocumento> listaEntregaDocumento = new HashSet<>();
			
			if (documentos != null && documentos.length > 0) {
				for (Long chaveDocumento : documentos) {
					definirDocumento(entregaDocumento, chaveDocumento, model);
					listaEntregaDocumento.add((EntregaDocumento)SerializationUtils.clone(entregaDocumento));
				}
			} else {
				listaEntregaDocumento.add(entregaDocumento);
			}
			try {
				
				for(EntregaDocumento insereEntregaDocumento : listaEntregaDocumento) {
					insereEntregaDocumento.setDadosAuditoria(getDadosAuditoria(request));
					insereEntregaDocumento.setHabilitado(true);
					insereEntregaDocumento
							.setDataSituacao(Util.converterCampoStringParaData(DATA_DA_SITUACAO, dataSituacao, Constantes.FORMATO_DATA_BR));
					insereEntregaDocumento.setDataEmissao(Calendar.getInstance().getTime());
					if("Retornado".equals(insereEntregaDocumento.getSituacaoEntrega().getDescricao())) {
						insereEntregaDocumento.setIndicadorRetornado(true);
					} else {
						insereEntregaDocumento.setIndicadorRetornado(false);
					}
					
					controladorEntregaDocumento.inserir(insereEntregaDocumento);
				}
							
				model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Constantes.CONTROLE_DE_ENTREGA_DE_DOCUMENTO);
				retorno = pesquisarEntregaDocumento(entregaDocumentoAux, results, dataSituacao, dataVencimento,
						String.valueOf(entregaDocumentoAux.isHabilitado()), imovel, request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return exibirDetalharEntregaDocumento
	 * @throws Exception
	 */
	@RequestMapping("exibirDetalharEntregaDocumento")
	public String exibirDetalharEntregaDocumento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws NegocioException {

		model.addAttribute(ENTREGA_DOCUMENTO, controladorEntregaDocumento.obterEntregaDocumento(chavePrimaria));
		return "exibirDetalharEntregaDocumento";
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return exibirAlterarEntregaDocumento
	 * @throws Exception
	 */
	@RequestMapping(TELA_EXIBIR_ALTERAR_ENTREGA_DOCUMENTO)
	public String exibirAlterarEntregaDocumento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws GGASException {

		model.addAttribute(ENTREGA_DOCUMENTO, controladorEntregaDocumento.obterEntregaDocumento(chavePrimaria));
		carregarCombos(model);
		return TELA_EXIBIR_ALTERAR_ENTREGA_DOCUMENTO;
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param results
	 * @param dataSituacao
	 * @param request
	 * @param chavePrimaria
	 * @param dataVencimento
	 * @param model
	 * @return FORWARD_PESQUISAR_ENTREGA_DOCUMENTO
	 * @throws Exception
	 */
	@RequestMapping("alterarEntregaDocumento")
	public String alterarEntregaDocumento(@ModelAttribute("EntregaDocumentoImpl") EntregaDocumentoImpl entregaDocumento,
			@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = "dataVencimento", required = false) String dataVencimento,
			@RequestParam("dataSituacao") String dataSituacao, HttpServletRequest request, Model model) throws GGASException {

		retorno = TELA_EXIBIR_ALTERAR_ENTREGA_DOCUMENTO;
		
		if (!"".equals(dataSituacao) && !dataSituacao.contains("_")) {
			entregaDocumento
					.setDataSituacao(Util.converterCampoStringParaData(DATA_DA_SITUACAO, dataSituacao, Constantes.FORMATO_DATA_BR));
		}
		
		if("Retornado".equals(entregaDocumento.getSituacaoEntrega().getDescricao())) {
			entregaDocumento.setIndicadorRetornado(true);
		} else {
			entregaDocumento.setIndicadorRetornado(false);
		}
		
		model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
		Map<String, Object> erros = entregaDocumento.validarDados();
		carregarCombos(model);
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorEntregaDocumento.atualizar(entregaDocumento);
				model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Constantes.CONTROLE_DE_ENTREGA_DE_DOCUMENTO);
				retorno = pesquisarEntregaDocumento(entregaDocumento, null, dataSituacao, dataVencimento,
						String.valueOf(entregaDocumento.isHabilitado()), chavePrimaria, request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param model
	 * @return exibirAlterarEntregaDocumentoEmLote
	 * @throws Exception
	 */
	@RequestMapping("exibirAlterarEntregaDocumentoEmLote")
	public String exibirAlterarEntregaDocumentoEmLote(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, Model model)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		model.addAttribute("listaEntregaDocumento", controladorEntregaDocumento.listarEntregaDocumento(filtro));
		carregarCombos(model);

		return "exibirAlterarEntregaDocumentoEmLote";
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param situacaoEntrega
	 * @param motivoNaoEntrega
	 * @param dataSituacao
	 * @param model
	 * @return pesquisarEntregaDocumento
	 * @throws Exception
	 */
	@RequestMapping("alterarEntregaDocumentoEmLote")
	public String alterarEntregaDocumentoEmLote(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam("situacaoEntrega") Long situacaoEntrega, @RequestParam("motivoNaoEntrega") Long motivoNaoEntrega,
			@RequestParam("dataSituacao") String dataSituacao, Model model) throws GGASException {

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {

			for (Long chavePrimaria : chavesPrimarias) {

				EntregaDocumento entregaDocumento = controladorEntregaDocumento.obterEntregaDocumento(chavePrimaria);
				
				if (!StringUtils.isEmpty(dataSituacao)) {
					entregaDocumento.setDataSituacao(
							Util.converterCampoStringParaData(ROTULO_DATA_SITUACAO, dataSituacao, Constantes.FORMATO_DATA_BR));
				}

				if (situacaoEntrega != null && situacaoEntrega.longValue() > 0) {
					entregaDocumento.setSituacaoEntrega(controladorEntidadeConteudo.obterEntidadeConteudo(situacaoEntrega));
				}

				if (motivoNaoEntrega != null && motivoNaoEntrega.longValue() > 0) {
					entregaDocumento.setMotivoNaoEntrega(controladorEntidadeConteudo.obterEntidadeConteudo(motivoNaoEntrega));
				}
				
				if("Retornado".equals(entregaDocumento.getSituacaoEntrega().getDescricao())) {
					entregaDocumento.setIndicadorRetornado(true);
				} else {
					entregaDocumento.setIndicadorRetornado(false);
				}

				controladorEntregaDocumento.atualizar(entregaDocumento);
			}

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Constantes.CONTROLE_DE_ENTREGA_DE_DOCUMENTO);
		}

		return FORWARD_PESQUISAR_ENTREGA_DOCUMENTO;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return pesquisarEntregaDocumento
	 * @throws Exception
	 */
	@RequestMapping("removerEntregaDocumento")
	public String removerEntregaDocumento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws NegocioException {

		validarSelecaoUmOuMais(chavesPrimarias);

		try {
			controladorEntregaDocumento.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, Constantes.CONTROLE_DE_ENTREGA_DE_DOCUMENTO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(e));
		}

		return FORWARD_PESQUISAR_ENTREGA_DOCUMENTO;
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param results
	 * @param model
	 * @param imovel
	 * @return exibirIncluirEntregaDocumento
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarDocumentoCobranca")
	public String pesquisarDocumentoCobranca(@ModelAttribute("EntregaDocumentoImpl") EntregaDocumentoImpl entregaDocumento,
			BindingResult results, @RequestParam(IMOVEL) Long imovel, Model model) throws GGASException {

		carregarListaDocumentoCobranca(entregaDocumento, model);
		carregarCombos(model);
		carregarDados(entregaDocumento, model);
		model.addAttribute(IMOVEL, imovel);
		model.addAttribute(ENTREGA_DOCUMENTO, entregaDocumento);
		return TELA_EXIBIR_INCLUIR_ENTREGA_DOCUMENTO;
	}

	/**
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCombos(Model model) throws GGASException {

		model.addAttribute("listaSituacaoEntrega", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTROLE_DOCUMENTO_SITUACAO_ENTREGA))));
		model.addAttribute("listaMotivoNaoEntrega", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTROLE_DOCUMENTO_MOTIVO_NAO_ENTREGA))));
		model.addAttribute("listaTipoDocumento", controladorArrecadacao.listarTipoDocumentoExistente());
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarDados(EntregaDocumentoImpl entregaDocumento, Model model) throws NegocioException {

		if (entregaDocumento.getCliente() != null) {
			model.addAttribute("cliente", entregaDocumento.getCliente().getChavePrimaria());
		} else {
			if (entregaDocumento.getAvisoCorte() != null
					&& entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria() > 0) {
				ClienteImovel clienteImovel = controladorImovel
						.obterClienteImovelPrincipal(entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria());
				model.addAttribute("cliente", clienteImovel.getCliente().getChavePrimaria());
			}
		}
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param model
	 * @throws GGASException
	 */
	private void carregarListaDocumentoCobranca(EntregaDocumentoImpl entregaDocumento, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (entregaDocumento.getCliente() != null && entregaDocumento.getCliente().getChavePrimaria() > 0) {
			filtro.put(ID_CLIENTE, entregaDocumento.getCliente().getChavePrimaria());
		} else {
			if (entregaDocumento.getAvisoCorte() != null
					&& entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria() > 0) {
				ClienteImovel clienteImovel = controladorImovel
						.obterClienteImovelPrincipal(entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria());
				if (clienteImovel != null && clienteImovel.getCliente() != null) {
					filtro.put(ID_CLIENTE, clienteImovel.getCliente().getChavePrimaria());
				}
			}
		}
		
		if(entregaDocumento.getImovel() != null) {
			filtro.put("idImovel", entregaDocumento.getImovel().getChavePrimaria());
		}

		if (entregaDocumento.getTipoDocumento() != null) {
			filtro.put(TIPO_DOCUMENTO, entregaDocumento.getTipoDocumento().getChavePrimaria());
		}

		filtro.put(ENTREGA_DOCUMENTO, Boolean.TRUE);
		identificarTipoDocumento(entregaDocumento, model, filtro);
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param model
	 * @param filtro
	 * @throws GGASException 
	 */
	private void identificarTipoDocumento(EntregaDocumentoImpl entregaDocumento, Model model,
			Map<String, Object> filtro) throws GGASException {

		if ((filtro.containsKey(ID_CLIENTE) || filtro.containsKey("idImovel")) && filtro.containsKey(TIPO_DOCUMENTO)) {
			if (entregaDocumento.getTipoDocumento() != null) {
				if (controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTIFICACAO_CORTE)
						.equals(String.valueOf(entregaDocumento.getTipoDocumento().getChavePrimaria()))) {
					filtro.put("tipo", "notificacaoCorte");
					model.addAttribute("listaAvisoCorte",
							controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtro));
				} else if (controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_AVISO_CORTE)
						.equals(String.valueOf(entregaDocumento.getTipoDocumento().getChavePrimaria()))) {
					filtro.put("tipo", "avisoCorte");
					model.addAttribute("listaAvisoCorte",
							controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtro));
				} else if (Constantes.C_TIPO_DOCUMENTO_PROTOCOLO
						.equals(entregaDocumento.getTipoDocumento().getDescricao())) {
					filtro.put("tipo", "avisoCorte");
					filtro.put("entregaDocumento", Boolean.TRUE);
					
					if(entregaDocumento.getImovel() != null) {
						PontoConsumo pontoConsumoAux = controladorImovel
								.carregarPontoConsumoPorChaveImovel(entregaDocumento.getImovel().getChavePrimaria(), Boolean.TRUE).iterator().next();
						
						filtro.put("idPontoConsumo", pontoConsumoAux.getChavePrimaria());
					}

					Collection<AvisoCorte> listaAvisoCorte = controladorAvisoCorte.listar(filtro);		
																		
					for(AvisoCorte avisoCorteAux : listaAvisoCorte) {
						DocumentoFiscal df = controladorFatura
								.obterDocumentoFiscalPorFaturaMaisRecente(avisoCorteAux.getFatura().getChavePrimaria());						
						if (df != null) {
							avisoCorteAux.setNumeroTitulo(df.getNumero());
						} else {
							avisoCorteAux.setNumeroTitulo(
									avisoCorteAux.getFatura().getChavePrimaria());
						}
					}
					
					model.addAttribute("listaAvisoCorte", listaAvisoCorte);
				}
			} else {

				Collection<DocumentoCobrancaItem> listaDocumentoIten = new ArrayList<DocumentoCobrancaItem>();

				for (DocumentoCobranca documentoCobranca : controladorCobranca.listarDocumentoCobranca(filtro)) {

					for (DocumentoCobrancaItem documentoCobrancaItem : documentoCobranca.getItens()) {

						listaDocumentoIten.add(documentoCobrancaItem);
						break;
					}
				}

				model.addAttribute("listaDocumentoCobranca", listaDocumentoIten);
			}
		}
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param chaveDocumento
	 * @throws NegocioException
	 */
	private void definirDocumento(EntregaDocumento entregaDocumento, Long chaveDocumento, Model model)
			throws NegocioException {

		if (chaveDocumento != null && chaveDocumento.longValue() > 0) {
			if (Long.valueOf(entregaDocumento.getTipoDocumento().getChavePrimaria()) != null && (Long
					.valueOf(entregaDocumento.getTipoDocumento().getChavePrimaria())
					.equals(Long.parseLong(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTIFICACAO_CORTE)))
					|| Long.valueOf(entregaDocumento.getTipoDocumento().getChavePrimaria())
							.equals(Long.valueOf(controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_AVISO_CORTE)))
					|| Constantes.C_TIPO_DOCUMENTO_PROTOCOLO
							.equals(entregaDocumento.getTipoDocumento().getDescricao()))) {
				entregaDocumento.setAvisoCorte((AvisoCorte) controladorAvisoCorte.obter(chaveDocumento));
				model.addAttribute("documento", entregaDocumento.getAvisoCorte());
			} else {
				entregaDocumento.setDocumentoCobranca(controladorCobranca.obterDocumentoCobranca(chaveDocumento));
				model.addAttribute("documento", entregaDocumento.getDocumentoCobranca());
			}
		}
	}

	/**
	 * 
	 * @param entregaDocumento
	 * @param dataSituacao
	 * @param dataVencimento
	 * @param habilitado
	 * @return filtro
	 * @throws GGASException
	 */
	private Map<String, Object> prepararFiltro(EntregaDocumento entregaDocumento, String dataVencimento, String habilitado)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (entregaDocumento.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, entregaDocumento.getChavePrimaria());
		}

		if (entregaDocumento.getDataSituacao() != null) {
			filtro.put(DATA_SITUACAO, Util.converterDataParaStringSemHora(entregaDocumento.getDataSituacao(), Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(dataVencimento)) {
			filtro.put(DATA_VENCIMENTO, dataVencimento);
		}

		if (entregaDocumento.getSituacaoEntrega() != null) {
			filtro.put(SITUACAO_ENTREGA, entregaDocumento.getSituacaoEntrega().getChavePrimaria());
		}

		if (entregaDocumento.getMotivoNaoEntrega() != null) {
			filtro.put(MOTIVO_NAO_ENTREGA, entregaDocumento.getMotivoNaoEntrega().getChavePrimaria());
		}

		if (entregaDocumento.getTipoDocumento() != null) {
			filtro.put(TIPO_DOCUMENTO, entregaDocumento.getTipoDocumento().getChavePrimaria());
		}

		if (entregaDocumento.getCliente() != null && entregaDocumento.getCliente().getChavePrimaria() > 0) {
			filtro.put(ID_CLIENTE, entregaDocumento.getCliente().getChavePrimaria());
		} else {
			if (entregaDocumento.getAvisoCorte() != null
					&& entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria() > 0) {
				ClienteImovel clienteImovel = controladorImovel
						.obterClienteImovelPrincipal(entregaDocumento.getAvisoCorte().getPontoConsumo().getImovel().getChavePrimaria());
				if (clienteImovel != null && clienteImovel.getCliente() != null) {
					filtro.put(ID_CLIENTE, clienteImovel.getCliente().getChavePrimaria());
				}
			} else {
				if (entregaDocumento.getImovel() != null && entregaDocumento.getImovel().getChavePrimaria() > 0) {
					filtro.put("idImovel", entregaDocumento.getImovel().getChavePrimaria());
				}
			}
		}
		
		

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.valueOf(habilitado));
		}

		return filtro;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("salvarDataRetornoProtocolos")
	public String salvarDataRetornoProtocolos(@RequestParam(required = false, value="idLoteComunicacao") Long idLoteComunicacao, EntregaDocumentoVO entregaDocumentoVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] idsProtocolos, HttpServletRequest request,
			Model model) throws GGASException {
		Collection<EntregaDocumento> colecaoProtocolos = (Collection<EntregaDocumento>) request.getSession()
				.getAttribute("listaEntregaDocumentoReq");
		
		Funcionario funcionario = super.obterUsuario(request).getFuncionario();
		Boolean isMesmaMensagem = Boolean.valueOf(request.getParameter("isMesmaMensagem"));
		Boolean isSucessoData = Boolean.TRUE;
		
		if ((entregaDocumentoVO.getDataRetorno() != null && !"".equals(entregaDocumentoVO.getDataRetorno()))
				|| (entregaDocumentoVO.getDescricaoObservacaoRetorno() != null
						&& !"".equals(entregaDocumentoVO.getDescricaoObservacaoRetorno()))) {
			isSucessoData = controladorEntregaDocumento.popularDataMensagemRetornoProtocolo(idsProtocolos, colecaoProtocolos,
					entregaDocumentoVO.getDataRetorno(), entregaDocumentoVO.getDescricaoObservacaoRetorno(),
					funcionario, isMesmaMensagem);
		}
		
		if(isSucessoData) {
			mensagemSucesso(model, MSG_RETORNAR_PROTOCOLO);
		} else {
			mensagemErro(model, "Data digitada anterior da emissao!");
		}
		
		if(idLoteComunicacao != null && idLoteComunicacao > 0) {
			model.addAttribute("isComunicacao", Boolean.TRUE);
			FiltroAcompanhamentoLoteDTO filtroComunicacao = montarFiltroComunicacao(entregaDocumentoVO);
			Collection<LoteComunicacao> loteComunicacao = controladorComunicacao.consultarLoteComunicacao(filtroComunicacao);
			request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
			request.getSession().setAttribute("listaLoteAcompanhamentoReq", loteComunicacao);
			return exibirProtocolosLoteComunicacao(idLoteComunicacao, entregaDocumentoVO, result, request, model);
		} else {
			return pesquisarRetornoProtocolo(entregaDocumentoVO, result, request, model);
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping("cancelarProtocolos")
	public String cancelarProtocolos(
			@RequestParam(required = false, value = "idLoteComunicacao") Long idLoteComunicacao,
			EntregaDocumentoVO entregaDocumentoVO, BindingResult result, @RequestParam(CHAVE_PRIMARIA) Long idProtocolo,
			HttpServletRequest request, Model model) throws GGASException {

		if (idLoteComunicacao != null && idLoteComunicacao > 0) {

			Collection<LoteComunicacao> colecaoLoteComunicacao = (Collection<LoteComunicacao>) request.getSession()
					.getAttribute("listaLoteAcompanhamentoReq");

			LoteComunicacao loteComunicacao = colecaoLoteComunicacao.stream()
					.filter(p -> p.getChavePrimaria() == idLoteComunicacao).findFirst().get();

			ItemLoteComunicacao itemLote = loteComunicacao.getItens().stream()
					.filter(p -> p.getDocumentoNaoEntregue() != null
							&& p.getDocumentoNaoEntregue().getChavePrimaria() == idProtocolo)
					.findFirst().orElse(null);

			if (itemLote != null && itemLote.getServicoAutorizacao() == null) {
				controladorComunicacao.removerProtocolo(itemLote);
				mensagemSucesso(model, MSG_CANCELAMENTO_PROTOCOLO);
			} else {
				mensagemErro(model, MSG_ITEM_COM_AS);
			}

		}

		return pesquisarRetornoProtocolo(entregaDocumentoVO, result, request, model);	
	}
}
