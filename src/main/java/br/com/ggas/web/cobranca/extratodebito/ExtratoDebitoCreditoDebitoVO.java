/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.extratodebito;

import java.io.Serializable;

public class ExtratoDebitoCreditoDebitoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9046104164225011487L;

	private long idCreditoDebito;

	private String tipoDocumento;

	private String descricaoRubrica;

	private String inicioCobranca;

	private String parcelasCobradas;

	private String totalParcelas;

	private String valorAVista;

	private String valorTotal;

	private String indicadorAntecipacao;

	private long idPontoConsumo;

	private long idTipoDocumento;

	private boolean indicadorPontoConsumo;

	/**
	 * @return the idCreditoDebito
	 */
	public long getIdCreditoDebito() {

		return idCreditoDebito;
	}

	/**
	 * @param idCreditoDebito
	 *            the idCreditoDebito to set
	 */
	public void setIdCreditoDebito(long idCreditoDebito) {

		this.idCreditoDebito = idCreditoDebito;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {

		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the descricaoRubrica
	 */
	public String getDescricaoRubrica() {

		return descricaoRubrica;
	}

	/**
	 * @param descricaoRubrica
	 *            the descricaoRubrica to set
	 */
	public void setDescricaoRubrica(String descricaoRubrica) {

		this.descricaoRubrica = descricaoRubrica;
	}

	/**
	 * @return the inicioCobranca
	 */
	public String getInicioCobranca() {

		return inicioCobranca;
	}

	/**
	 * @param inicioCobranca
	 *            the inicioCobranca to set
	 */
	public void setInicioCobranca(String inicioCobranca) {

		this.inicioCobranca = inicioCobranca;
	}

	/**
	 * @return the parcelasCobradas
	 */
	public String getParcelasCobradas() {

		return parcelasCobradas;
	}

	/**
	 * @param parcelasCobradas
	 *            the parcelasCobradas to set
	 */
	public void setParcelasCobradas(String parcelasCobradas) {

		this.parcelasCobradas = parcelasCobradas;
	}

	/**
	 * @return the totalParcelas
	 */
	public String getTotalParcelas() {

		return totalParcelas;
	}

	/**
	 * @param totalParcelas
	 *            the totalParcelas to set
	 */
	public void setTotalParcelas(String totalParcelas) {

		this.totalParcelas = totalParcelas;
	}

	/**
	 * @return the valorAVista
	 */
	public String getValorAVista() {

		return valorAVista;
	}

	/**
	 * @param valorAVista
	 *            the valorAVista to set
	 */
	public void setValorAVista(String valorAVista) {

		this.valorAVista = valorAVista;
	}

	/**
	 * @return the valorTotal
	 */
	public String getValorTotal() {

		return valorTotal;
	}

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	public void setValorTotal(String valorTotal) {

		this.valorTotal = valorTotal;
	}

	/**
	 * @return the indicadorAntecipacao
	 */
	public String getIndicadorAntecipacao() {

		return indicadorAntecipacao;
	}

	/**
	 * @param indicadorAntecipacao
	 *            the indicadorAntecipacao to set
	 */
	public void setIndicadorAntecipacao(String indicadorAntecipacao) {

		this.indicadorAntecipacao = indicadorAntecipacao;
	}

	/**
	 * @return the idPontoConsumo
	 */
	public long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the idTipoDocumento
	 */
	public long getIdTipoDocumento() {

		return idTipoDocumento;
	}

	/**
	 * @param idTipoDocumento
	 *            the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(long idTipoDocumento) {

		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * @param indicadorPontoConsumo
	 *            the indicadorPontoConsumo to set
	 */
	public void setIndicadorPontoConsumo(boolean indicadorPontoConsumo) {

		this.indicadorPontoConsumo = indicadorPontoConsumo;
	}

	/**
	 * @return indicadorPontoConsumo
	 */
	public boolean getIndicadorPontoConsumo() {

		return indicadorPontoConsumo;
	}
}
