/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.cobranca.parcelamento.decorator;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.util.DisplayTagGenericoLockDecorator;

/**
 * @author bribeiro
 */
public class ContratoPontoConsumoResultadoPesquisaDecorator extends DisplayTagGenericoLockDecorator {

	public static final String LOCK_PARCELAMENTO_CONTRATO_PONTO_CONSUMO = "LOCK_PARCELAMENTO_CONTRATO_PONTO_CONSUMO";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.
	 * DisplayTagGenericoLockDecorator
	 * #getOperacaoLock()
	 */
	@Override
	public String getOperacaoLock() {

		return LOCK_PARCELAMENTO_CONTRATO_PONTO_CONSUMO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.
	 * DisplayTagGenericoLockDecorator
	 * #getMensagemRegistroEmUso()
	 */
	@Override
	public String getMensagemRegistroEmUso() {

		return "Ponto de consumo em processo de parcelamento";
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.
	 * DisplayTagGenericoLockDecorator
	 * #getMensagemFormaLiberacaoRegistro()
	 */
	@Override
	public String getMensagemFormaLiberacaoRegistro() {

		return "Atenção você bloqueou esse ponto de consumo para parcelamento, para liberá-lo selecione o mesmo e depois clique em Cancelar.";
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.DisplayTagGenericoDecorator#addRowClass()
	 */
	@Override
	public String addRowClass() {

		String rowClass = null;
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();
		if(contratoPontoConsumo != null && contratoPontoConsumo.getPontoConsumo() != null
						&& !contratoPontoConsumo.getPontoConsumo().isHabilitado()) {
			rowClass = "linhaInativa";
		}
		
		if (rowClass != null) {
			return rowClass;
		} else {
			return "";
		}
	
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.DisplayTagGenericoLockDecorator#adicionarHtmlLock(java.lang.StringBuilder, java.lang.String, java.lang.String,
	 * boolean)
	 */
	@Override
	public void adicionarHtmlLock(StringBuilder stringBuilder, String nomeUsuarioLock, String valorColuna, boolean mostrarImagem) {

		if(mostrarImagem) {
			stringBuilder.append("<img  alt=\"").append(getMensagemRegistroEmUso()).append(" por: ");
			stringBuilder.append(nomeUsuarioLock);
			stringBuilder.append("\" title=\"").append(getMensagemRegistroEmUso()).append(" por: ");
			stringBuilder.append(nomeUsuarioLock);
			stringBuilder.append("\" src=\"");
			stringBuilder.append(getContextPath());
			stringBuilder.append("/imagens/Lock16.png");
			stringBuilder.append("\" border=\"0\"/>");
		}
		stringBuilder.append("<span>").append(valorColuna).append("</span>");
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.DisplayTagGenericoLockDecorator#adicionarHtmlUnlockField(java.lang.StringBuilder, java.lang.String, long)
	 */
	@Override
	public void adicionarHtmlUnlockField(StringBuilder stringBuilder, String valorColuna, long chavePrimariaLock) {

		stringBuilder.append("<a href='javascript:exibirExtratoDebitosPontoConsumo(").append(chavePrimariaLock).append(",")
				.append(this.getClienteAssintaturaChavePrimariaComLock()).append(");'><span class=\"linkInvisivel\"></span>");
		stringBuilder.append(valorColuna);
		stringBuilder.append("</a>");
	}

	// Campos
	public String getContratoNumeroFormatadoComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();
		montarCampoComLock(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), contratoPontoConsumo.getContrato()
						.getNumeroFormatado() + "", stringBuilder, true);
		return stringBuilder.toString();
	}

	public String getImovelPontoConsumoComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();

		String valorColuna = contratoPontoConsumo.getPontoConsumo().getImovel().getNome() + " - "
						+ contratoPontoConsumo.getPontoConsumo().getDescricao();

		montarCampoComLock(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), valorColuna, stringBuilder, false);
		return stringBuilder.toString();
	}

	public String getPontoConsumoHabilitadoComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();

		String valorColuna;
		
		if (contratoPontoConsumo.getPontoConsumo().isHabilitado()) {
			valorColuna = "Ativo";
		} else {
			valorColuna = "Inativo";
		}

		montarCampoComLock(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), valorColuna, stringBuilder, false);
		return stringBuilder.toString();
	}

	private String getClienteAssintaturaChavePrimariaComLock() {

		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();

		return String.valueOf(contratoPontoConsumo.getContrato().getClienteAssinatura().getChavePrimaria());
	}

	public String getSegmentoDescricaoComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();

		String valorColuna = contratoPontoConsumo.getPontoConsumo().getSegmento().getDescricao();

		montarCampoComLock(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), valorColuna, stringBuilder, false);
		return stringBuilder.toString();
	}

	public String getRamoAtividadeDescricaoComLock() {

		StringBuilder stringBuilder = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) getCurrentRowObject();

		String valorColuna = contratoPontoConsumo.getPontoConsumo().getRamoAtividade().getDescricao();

		montarCampoComLock(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), valorColuna, stringBuilder, false);
		return stringBuilder.toString();
	}

}
