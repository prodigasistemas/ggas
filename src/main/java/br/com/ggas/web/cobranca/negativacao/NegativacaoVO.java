package br.com.ggas.web.cobranca.negativacao;

import java.io.Serializable;

public class NegativacaoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2975755793920981830L;

	private Long idNegativador;
	private Long idCliente;
	private Long idGrupoFaturamento;
	private Long [] rotasSelecionadas;
	private Long idSegmento;
	private Long idSituacaoPontoConsumo;
	private Long idSituacaoContrato;
	private String tituloCriterio;
	private boolean simular;
	private boolean contratoSituacao;
	private boolean considerarContasAnalise;
	private String dataPrevistaExecucao;
	private Long quantidadeMaximaInclusao;
	private String peridoReferenciaDebitoInicio;
	private String peridoReferenciaDebitoFim;
	private String periodoVencimentoInicio;
	private String periodoVencimentoFim;
	private Long valorDebitoInicio;
	private Long valorDebitoFim;

	public Long getIdNegativador() {
		return idNegativador;
	}

	public void setIdNegativador(Long idNegativador) {
		this.idNegativador = idNegativador;
	}

	public String getTituloCriterio() {
		return tituloCriterio;
	}

	public void setTituloCriterio(String tituloCriterio) {
		this.tituloCriterio = tituloCriterio;
	}

	public String getDataPrevistaExecucao() {
		return dataPrevistaExecucao;
	}

	public void setDataPrevistaExecucao(String dataPrevistaExecucao) {
		this.dataPrevistaExecucao = dataPrevistaExecucao;
	}

	public boolean isSimular() {
		return simular;
	}

	public void setSimular(boolean simular) {
		this.simular = simular;
	}

	public String getPeridoReferenciaDebitoInicio() {
		return peridoReferenciaDebitoInicio;
	}

	public void setPeridoReferenciaDebitoInicio(String peridoReferenciaDebitoInicio) {
		this.peridoReferenciaDebitoInicio = peridoReferenciaDebitoInicio;
	}

	public Long getQuantidadeMaximaInclusao() {
		return quantidadeMaximaInclusao;
	}

	public void setQuantidadeMaximaInclusao(Long quantidadeMaximaInclusao) {
		this.quantidadeMaximaInclusao = quantidadeMaximaInclusao;
	}

	public String getPeridoReferenciaDebitoFim() {
		return peridoReferenciaDebitoFim;
	}

	public void setPeridoReferenciaDebitoFim(String peridoReferenciaDebitoFim) {
		this.peridoReferenciaDebitoFim = peridoReferenciaDebitoFim;
	}

	public String getPeriodoVencimentoInicio() {
		return periodoVencimentoInicio;
	}

	public void setPeriodoVencimentoInicio(String periodoVencimentoInicio) {
		this.periodoVencimentoInicio = periodoVencimentoInicio;
	}

	public String getPeriodoVencimentoFim() {
		return periodoVencimentoFim;
	}

	public void setPeriodoVencimentoFim(String periodoVencimentoFim) {
		this.periodoVencimentoFim = periodoVencimentoFim;
	}

	public Long getValorDebitoInicio() {
		return valorDebitoInicio;
	}

	public void setValorDebitoInicio(Long valorDebitoInicio) {
		this.valorDebitoInicio = valorDebitoInicio;
	}

	public Long getValorDebitoFim() {
		return valorDebitoFim;
	}

	public void setValorDebitoFim(Long valorDebitoFim) {
		this.valorDebitoFim = valorDebitoFim;
	}

	public boolean isConsiderarContasAnalise() {
		return considerarContasAnalise;
	}

	public void setConsiderarContasAnalise(boolean considerarContasAnalise) {
		this.considerarContasAnalise = considerarContasAnalise;
	}

	public boolean isContratoSituacao() {
		return contratoSituacao;
	}

	public void setContratoSituacao(boolean contratoSituacao) {
		this.contratoSituacao = contratoSituacao;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	
	public Long [] getRotasSelecionadas() {
		return rotasSelecionadas;
	}
	public void setRotasSelecionadas(Long [] rotasSelecionadas) {
		this.rotasSelecionadas = rotasSelecionadas;
	}

	public Long getIdSegmento() {
		return idSegmento;
	}

	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	public Long getIdsituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}

	public void setIdsituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}

	public Long getIdSituacaoContrato() {
		return idSituacaoContrato;
	}

	public void setIdSituacaoContrato(Long idSituacaoContrato) {
		this.idSituacaoContrato = idSituacaoContrato;
	}

}
