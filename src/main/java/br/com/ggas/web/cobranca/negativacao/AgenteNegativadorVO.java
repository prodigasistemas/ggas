package br.com.ggas.web.cobranca.negativacao;

public class AgenteNegativadorVO {
	
	private String nomeCliente;

	private String numeroContrato;
	
	private Boolean indicadorPrioritario;

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public Boolean getIndicadorPrioritario() {
		return indicadorPrioritario;
	}

	public void setIndicadorPrioritario(Boolean indicadorPrioritario) {
		this.indicadorPrioritario = indicadorPrioritario;
	}
}
