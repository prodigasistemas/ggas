/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 10:03:51
 @author wcosta
 */

package br.com.ggas.web.cobranca.avisocorte;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.batch.impl.ProcessoVO;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.AvisoCorteEmissao;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao;
import br.com.ggas.cobranca.avisocorte.impl.AvisoCorteNotificacaoVO;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import br.com.ggas.web.batch.ProcessoAction;

/**
 * Classe responsável pelas telas relacionadas as emissões de notificação de
 * corte
 *
 */
@Controller
public class AvisoCorteEmissaoAction extends GenericAction {

	private static final String ID_FATURA = "idFatura";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_AVISO_CORTE = "listaAvisoCorte";

	private static final String HABILITADO = "habilitado";

	private static final String ID_CLIENTE = "idCliente";
	
	private static final String ID_IMOVEL = "idImovel";

	private static final String INDICADOR_EXIBIR_EM_TELA = "indicadorExibirEmTela";

	private static final String TIPO = "tipo";
	
	private static final String LISTA_SEGMENTO = "listaSegmento";
	
	private static final String MSG_SUCESSO_PROCESSO_INSERIDO = "Processo para gerar Autorizaçoes de Serviço em Lote inserido com sucesso";
	
	private static final String MSG_SALVAR_PRIORIZACAO = "Priorização de Corte salvo com sucesso";
	
	private static final String LISTA_GRUPOS_FATURAMENTO = "listaGruposFaturamento";
	
	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	@Autowired
	private ControladorAvisoCorteEmissao controladorAvisoCorteEmissao;

	@Autowired
	private ControladorAvisoCorte controladorAvisoCorte;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorCobranca controladorCobranca;
	
	@Autowired
	private ControladorAcaoComando controladorAcaoComando;
	
	@Autowired
	private ControladorAcesso controladorAcesso;
		
	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	@Autowired
	private ProcessoAction processoAction;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();

	/**
	 * Método responsável por exibir tela de pesquisa emissao notificacao corte.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException 
	 */
	@RequestMapping("exibirPesquisarEmissaoNotificacaoCorte")
	public String exibirPesquisarEmissaoNotificacaoCorte(HttpServletRequest request, Model model) throws NegocioException {

		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		
		return "exibirPesquisarEmissaoNotificacaoCorte";

	}
	
	/**
	 * Método responsável por exibir tela de priorizacao ou liberacao de faturas.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("priorizarLiberarFaturas")
	public String priorizarLiberarFaturas(HttpServletRequest request, Model model) {

		return "priorizarLiberarFaturas";

	}

	/**
	 * Método responsável por exibir tela de priorizacao ou liberacao de faturas.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("exibirRoteirizarFaturas")
	public String exibirRoteirizarFaturas(HttpServletRequest request, Model model) throws NegocioException {
		request.getSession().setAttribute("listaAvisoCorteRoteirizados", null);
		Collection<AvisoCorte> listaPriorizada = controladorAvisoCorte.consultarAvisosCortePriorizados();
		Collection<AvisoCorte> retirarListaComAS = new HashSet<>();
		
		for(AvisoCorte avisoCorte : listaPriorizada) {
			if(controladorAvisoCorteEmissao.validarSeTemServicoAutorizacaoPorFatura(avisoCorte.getFatura())){
				retirarListaComAS.add(avisoCorte);
			}
		}
		
		listaPriorizada.removeAll(retirarListaComAS);
		Collection<AvisoCorte> listaRoteirizada = new HashSet<>();
		if (!listaPriorizada.isEmpty()) {
			listaRoteirizada = controladorServicoAutorizacao.roteirizacaoCorte(listaPriorizada);
			for (AvisoCorte avisoAux : listaRoteirizada) {
				if (avisoAux.getFatura().getHistoricoMedicao() == null) {
					Collection<Fatura> colecaoFaturasFilhas = controladorFatura
							.consultarFaturaPorFaturaAgrupamento(avisoAux.getFatura().getChavePrimaria());
					if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
						avisoAux.getFatura().getPontoConsumo().setDescricao("");
						for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
							String descricao = faturaAgrupada.getPontoConsumo().getDescricao();
							String descricaoAgrupada = avisoAux.getFatura().getPontoConsumo().getDescricao();

							if ("".equals(descricaoAgrupada)) {
								avisoAux.getFatura().getPontoConsumo().setDescricao(descricao);
							} else {
								avisoAux.getFatura().getPontoConsumo()
										.setDescricao(descricaoAgrupada.concat(Constantes.STRING_ESPACO)
												.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO)
												.concat(descricao));
							}
						}
					}
				}
			}
			request.getSession().setAttribute("listaAvisoCorteRoteirizados", listaRoteirizada);
		}

		model.addAttribute("listaAvisoCorte", listaRoteirizada);
		return "exibirRoteirizarFaturas";

	}

	/**
	 * Método responsável por exibir tela de pesquisa emissao notificacao aviso
	 * corte.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisarEmissaoNotificacaoAvisoCorte")
	public String exibirPesquisarEmissaoNotificacaoAvisoCorte(HttpServletRequest request, Model model) {

		request.getSession().setAttribute(TIPO, null);
		return "exibirPesquisarEmissaoNotificacaoAvisoCorte";

	}

	/**
	 * Método responsável por pesquisar emissao notificacao aviso corte.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarEmissaoNotificacaoAvisoCorte")
	public String pesquisarEmissaoNotificacaoAvisoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		try {
			prepararFiltro(filtro, avisoCorteNotificacaoVO);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		Collection<AvisoCorte> listaAvisoCorte = controladorAvisoCorte
				.consultarAvisoCorteAcompanhamentoCobranca(filtro);

		atualizarDiasAtraso(listaAvisoCorte);

		model.addAttribute(LISTA_AVISO_CORTE, listaAvisoCorte);

		request.getSession().setAttribute(TIPO, avisoCorteNotificacaoVO.getTipo());

		return "exibirPesquisarEmissaoNotificacaoAvisoCorte";

	}

	/**
	 * Método responsável por atualizar dias atraso.
	 * 
	 * @param listaAvisoCorte {@link Collection}
	 */
	private void atualizarDiasAtraso(Collection<AvisoCorte> listaAvisoCorte) {

		Date dataAtual = Calendar.getInstance().getTime();
		for (AvisoCorte aviso : listaAvisoCorte) {
			Integer diasAtraso = Util.intervaloDatas(aviso.getFatura().getDataVencimento(), dataAtual);
			aviso.setDiasAtraso(diasAtraso);
		}
	}

	/**
	 * Método responsável por pesquisar uma entidade emissao notificacao corte.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarEmissaoNotificacaoCorte")
	public String pesquisarEmissaoNotificacaoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute("avisoCorteNotificacaoVO", avisoCorteNotificacaoVO);
		prepararFiltro(filtro, avisoCorteNotificacaoVO);

		filtro.put(Constantes.COMANDO_COBRANCA_NOTIFICACAO_CORTE, Boolean.TRUE);
		filtro.put(Constantes.COMANDO_COBRANCA_AVISO_CORTE, Boolean.FALSE);
		Collection<Fatura> listaFatura = controladorAvisoCorteEmissao.consultarFaturasValidasNotificacaoCorte(filtro,
				null);

		Iterator<Fatura> iteratorFaturas = listaFatura.iterator();

		Set<Fatura> faturasAgrupadas = new HashSet<Fatura>();

		Map<String, Object> filtroAvisoCorte = new HashMap<String, Object>();
		while (iteratorFaturas.hasNext()) {
			Fatura fatura = iteratorFaturas.next();
			Fatura faturaAgrupada = fatura.getFaturaAgrupada();
			if (faturaAgrupada != null) {
				filtroAvisoCorte.put(ID_FATURA, faturaAgrupada.getChavePrimaria());
				if (controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtroAvisoCorte).isEmpty()) {
					if (faturasAgrupadas.contains(faturaAgrupada)) {
						String descricaoAgrupada = faturaAgrupada.getPontoConsumo().getDescricao();
						String descricao = fatura.getPontoConsumo().getDescricao();
						faturaAgrupada.getPontoConsumo().setDescricao(descricaoAgrupada.concat(Constantes.STRING_ESPACO)
								.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO).concat(descricao));
					} else {
						PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
						pontoConsumo.setDescricao(fatura.getDescricaoPontoConsumo());
						faturaAgrupada.setPontoConsumo(pontoConsumo);
						faturasAgrupadas.add(faturaAgrupada);
					}
				}
				iteratorFaturas.remove();
			}
			if(faturasAgrupadas.contains(fatura)) {
				iteratorFaturas.remove();
			}
		}
		listaFatura.addAll(faturasAgrupadas);
		Collection<Fatura> faturas = controladorFatura.atualizarDiasAtraso(listaFatura,
				avisoCorteNotificacaoVO.getDiasAtraso());

		model.addAttribute("listaFatura", faturas);

		return exibirPesquisarEmissaoNotificacaoCorte(request, model);

	}

	/**
	 * Método responsável pela emissão da 2via do aviso de corte.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param chavePrimaria           {@link Long}
	 * @param request                 {@link HttpServletRequest}
	 * @param response                {@link HttpServletResponse}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws IOException   {@link IOException}
	 */
	@RequestMapping("emitir2viaAvisoCorte")
	public String emitir2viaAvisoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, HttpServletResponse response,
			Model model) throws IOException, GGASException {

		if ("notificacaoCorte".equals(avisoCorteNotificacaoVO.getTipo())) {
			byte[] relatorioNotificacaoCorte = controladorAvisoCorte.emitir2ViaNotificacaoCorte(chavePrimaria);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(relatorioNotificacaoCorte.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorioNotificacaoCorte.pdf");
			servletOutputStream.write(relatorioNotificacaoCorte, 0, relatorioNotificacaoCorte.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} else {
			byte[] relatorioAvisoCorte = controladorAvisoCorte.emitir2ViaAvisoCorte(chavePrimaria);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(relatorioAvisoCorte.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorioAvisoCorte.pdf");
			servletOutputStream.write(relatorioAvisoCorte, 0, relatorioAvisoCorte.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		return pesquisarEmissaoNotificacaoAvisoCorte(avisoCorteNotificacaoVO, result, request, model);

	}

	/**
	 * Método responsável por habilitar emissao notificacao corte.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param idsFatura               {@link Long}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("habilitarEmissaoNotificacaoCorte")
	public String habilitarEmissaoNotificacaoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] idsFatura, HttpServletRequest request,
			Model model) throws GGASException {

		Collection<Fatura> listaFatura = controladorCobranca.consultarFaturas(idsFatura);

		try {
			if (listaFatura != null && !listaFatura.isEmpty()) {
				for (Fatura fatura : listaFatura) {
					// caso seja desabilitado inclui uma entidade AvisoCorteEmissao
					if (!avisoCorteNotificacaoVO.getHabilitar()) {
						
						Collection<Fatura> colecaoFaturasFilhas = controladorFatura
								.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
						
						if(colecaoFaturasFilhas !=null && !colecaoFaturasFilhas.isEmpty()) {
							for(Fatura faturaAgrupada : colecaoFaturasFilhas) {
								AvisoCorteEmissao ace = (AvisoCorteEmissao) controladorAvisoCorteEmissao.criar();
								ace.setFatura(faturaAgrupada);
								ace.setIndicadorEmissao(Boolean.FALSE);

								if (fatura.getPontoConsumo() != null) {
									ace.setPontoConsumo(faturaAgrupada.getPontoConsumo());
								}

								controladorAvisoCorteEmissao.inserir(ace);
							}
						}
						
						AvisoCorteEmissao ace = (AvisoCorteEmissao) controladorAvisoCorteEmissao.criar();
						ace.setFatura(fatura);
						ace.setIndicadorEmissao(Boolean.FALSE);

						if (fatura.getPontoConsumo() != null) {
							ace.setPontoConsumo(fatura.getPontoConsumo());
						}

						controladorAvisoCorteEmissao.inserir(ace);
						// caso seja habilitado
					} else if (avisoCorteNotificacaoVO.getHabilitar()) {

						Collection<Fatura> colecaoFaturasFilhas = controladorFatura
								.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
						
						if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
							for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
								Map<String, Object> filtroAvisoCorteEmissao = new HashMap<String, Object>();
								filtroAvisoCorteEmissao.put(ID_FATURA, faturaAgrupada.getChavePrimaria());
								Collection<AvisoCorteEmissao> listaAvisoCorteEmissao = controladorAvisoCorteEmissao
										.consultar(filtroAvisoCorteEmissao);

								Iterator<AvisoCorteEmissao> iterator = listaAvisoCorteEmissao.iterator();

								while (iterator.hasNext()) {
									AvisoCorteEmissao ace = iterator.next();
									controladorAvisoCorteEmissao.remover(ace);
								}
							}
						}
						
						Map<String, Object> filtroAvisoCorteEmissao = new HashMap<String, Object>();
						filtroAvisoCorteEmissao.put(ID_FATURA, fatura.getChavePrimaria());
						Collection<AvisoCorteEmissao> listaAvisoCorteEmissao = controladorAvisoCorteEmissao
								.consultar(filtroAvisoCorteEmissao);

						Iterator<AvisoCorteEmissao> iterator = listaAvisoCorteEmissao.iterator();

						while (iterator.hasNext()) {
							AvisoCorteEmissao ace = iterator.next();
							controladorAvisoCorteEmissao.remover(ace);
						}

					}

				}
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return pesquisarEmissaoNotificacaoCorte(avisoCorteNotificacaoVO, result, request, model);

	}

	/**
	 * Método responsável por preparar filtro para consulta.
	 * 
	 * @param filtro                  {@link Map}
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, AvisoCorteNotificacaoVO avisoCorteNotificacaoVO)
			throws GGASException {

		String tipo = avisoCorteNotificacaoVO.getTipo();
		if (avisoCorteNotificacaoVO.getDiasAtraso() == null) {
			avisoCorteNotificacaoVO.setDiasAtraso(0);
		}

		if (avisoCorteNotificacaoVO.getChavePrimaria() != null) {
			filtro.put(CHAVE_PRIMARIA, avisoCorteNotificacaoVO.getChavePrimaria());
		}

		if (tipo != null) {
			filtro.put(TIPO, tipo);
		}

		Long idCliente = avisoCorteNotificacaoVO.getIdCliente();
		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		} else {
			Long idImovel = avisoCorteNotificacaoVO.getIdImovel();
			if (idImovel != null && idImovel > 0) {
				filtro.put(ID_IMOVEL, idImovel);
			}
		}

		Long idFatura = avisoCorteNotificacaoVO.getIdFatura();
		if (idFatura != null && idFatura > 0) {
			filtro.put(ID_FATURA, idFatura);
		}

		if (avisoCorteNotificacaoVO.getHabilitado() != null) {
			filtro.put(HABILITADO, String.valueOf(avisoCorteNotificacaoVO.getHabilitado()));
		} else {
			filtro.put(HABILITADO, "");
		}
		
		Long idGrupoFaturamento = avisoCorteNotificacaoVO.getIdGrupoFaturamento();
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
		}

		filtro.put(INDICADOR_EXIBIR_EM_TELA, "true");
	}

	/**
	 * Método responsável por exibir tela de pesquisa emissao aviso corte.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException - {@link GGasException}
	 */
	@RequestMapping("exibirPesquisarEmissaoAvisoCorte")
	public String exibirPesquisarEmissaoAvisoCorte(HttpServletRequest request, Model model) throws GGASException {
		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());

		carregarCombos(model);
		
		return "exibirPesquisarEmissaoAvisoCorte";
	}

	/**
	 * Método responsável por pesquisar as notificacoes que na tenham aviso gerado
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarEmissaoAvisoCorte")
	public String pesquisarEmissaoAvisoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		AcaoComando acao = null;
		Integer diasVencimento = 0;
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute("avisoCorteNotificacaoVO", avisoCorteNotificacaoVO);
		prepararFiltro(filtro, avisoCorteNotificacaoVO);

		filtro.put(Constantes.COMANDO_COBRANCA_NOTIFICACAO_CORTE, Boolean.FALSE);
		filtro.put(Constantes.COMANDO_COBRANCA_AVISO_CORTE, Boolean.TRUE);
		
		if(avisoCorteNotificacaoVO.getHabilitado() != null) {
			filtro.put(Fatura.INDICADOR_AVISO_CORTE_FATURA, avisoCorteNotificacaoVO.getHabilitado());
			filtro.remove("habilitado");
		}
		
		if(avisoCorteNotificacaoVO.getIdComando() != -1) {
			acao = controladorAcaoComando.obterAcaoComando(avisoCorteNotificacaoVO.getIdComando(), "acao",
					"grupoFaturamento", "localidade", "setorComercial", "municipio", "marcaMedidor", "modeloMedidor",
					"marcaCorretor", "modeloCorretor", "situacaoConsumo", "segmento", "acao.servicoTipo.servicoTipoPrioridade",
					"acao.servicoTipo.listaEquipamentoEspecial", "acao.servicoTipo.listaMateriais",
					"acao.servicoTipo.indicadorGeraLote", "acao.servicoTipoPesquisa.indicadorPesquisaSatisfacao",
					"acao.chamadoAssuntoPesquisa.indicadorGeraServicoAutorizacao", "acao.chamadoAssuntoPesquisa");
			
			diasVencimento = acao.getAcao().getNumeroDiasVencimento();
		}
		Collection<Fatura> listaFatura = controladorAvisoCorteEmissao.consultarFaturasValidasAvisoCorte(filtro, null);

		Iterator<Fatura> iteratorFaturas = listaFatura.iterator();

		Set<Fatura> faturasAgrupadas = new HashSet<Fatura>();

		Map<String, Object> filtroAvisoCorte = new HashMap<String, Object>();
		while (iteratorFaturas.hasNext()) {
			Fatura fatura = iteratorFaturas.next();
			Fatura faturaAgrupada = fatura.getFaturaAgrupada();
			if (faturaAgrupada != null) {
				filtroAvisoCorte.put(ID_FATURA, faturaAgrupada.getChavePrimaria());
				if (listaFatura.contains(faturaAgrupada) || faturasAgrupadas.contains(faturaAgrupada)) {
					if (controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtroAvisoCorte).size() <= 1) {
						if (faturasAgrupadas.contains(faturaAgrupada)) {
							String descricaoAgrupada = faturaAgrupada.getPontoConsumo().getDescricao();
							String descricao = fatura.getPontoConsumo().getDescricao();
							faturaAgrupada.getPontoConsumo()
									.setDescricao(descricaoAgrupada.concat(Constantes.STRING_ESPACO)
											.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO)
											.concat(descricao));
						} else {
							PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
							pontoConsumo.setDescricao(fatura.getDescricaoPontoConsumo());
							faturaAgrupada.setPontoConsumo(pontoConsumo);
							faturasAgrupadas.add(faturaAgrupada);
						}
					}
				}
				iteratorFaturas.remove();
			}
			if (faturasAgrupadas.contains(fatura)) {
				iteratorFaturas.remove();
			}
		}
		listaFatura.addAll(faturasAgrupadas);
		
		if(acao != null) {
			Collection<Fatura> faturasVencidasAcao = new HashSet<>();
			Date dataAtual = new Date();
			
			for(Fatura faturaVencida : listaFatura) {
				Integer diasVencido = DataUtil.diferencaDiasEntreDatas(faturaVencida.getDataVencimento(), dataAtual);
				if(diasVencimento > diasVencido) {
					faturasVencidasAcao.add(faturaVencida);
				}
			}
			
			listaFatura.removeAll(faturasVencidasAcao);
		}
		Collection<Fatura> faturas = controladorFatura.atualizarDiasAtraso(listaFatura,
				avisoCorteNotificacaoVO.getDiasAtraso());

		model.addAttribute("listaFatura", faturas);
		carregarCombos(model);

		return exibirPesquisarEmissaoAvisoCorte(request, model);
	}
	
	
	/**
	 * Carregar combos.
	 *
	 * @param model
	 *            the model
	 * @throws GGASException
	 */
	private void carregarCombos(Model model) throws GGASException {
		Map<String, Object> filtro = new HashMap<>();

		filtro.put(HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_SEGMENTO, fachada.consultarSegmento(filtro));

	}

	/**
	 * Habilitar ou desabilita faturas a receberem o aviso de corte
	 * @param avisoCorteNotificacaoVO - {@link AvisoCorteNotificacaoVO}
	 * @param result - {@link BindingResult}
	 * @param idsFatura - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - @{ling GGASException}
	 */
	@RequestMapping("habilitarOuDesabilitarAvisoCorteFatura")
	public String habilitarOuDesabilitarAvisoCorteFatura(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] idsFatura, HttpServletRequest request,
			Model model) throws GGASException {
		String constanteHabilitarDesabilitar;
		Collection<Fatura> listaFatura = controladorCobranca.consultarFaturas(idsFatura);

		try {
			if (!CollectionUtils.isEmpty(listaFatura)) {
				for (Fatura fatura : listaFatura) {
					fatura.setIndicadorExibirAvisoCorte(avisoCorteNotificacaoVO.getHabilitar());
					controladorFatura.atualizar(fatura);

					Collection<Fatura> colecaoFaturasFilhas = controladorFatura
							.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());

					if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
						for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
							faturaAgrupada.setIndicadorExibirAvisoCorte(avisoCorteNotificacaoVO.getHabilitar());
							controladorFatura.atualizar(faturaAgrupada);
						}
					}
				}
				if(avisoCorteNotificacaoVO.getHabilitar()) {
					constanteHabilitarDesabilitar = Constantes.SUCESSO_ENTIDADE_HABILITADO;
				} else {
					constanteHabilitarDesabilitar = Constantes.SUCESSO_ENTIDADE_DESABILITADO;
				}
				super.mensagemSucesso(model, constanteHabilitarDesabilitar,
						super.obterMensagem(Constantes.AVISO_CORTE));
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}
		return pesquisarEmissaoAvisoCorte(avisoCorteNotificacaoVO, result, request, model);
	}
	
	
	/**
	 * Carregar comandos.
	 * 
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the model and view
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("carregarComandoAvisoCorte")
	public ModelAndView carregarComandoAvisoCorte() throws NegocioException {
		Operacao operacao = controladorAcesso.obterOperacaoSistemaPelaDescricao("Processar Emissão Aviso de Corte");
		ModelAndView model = new ModelAndView("comboComandos");
		
		if(operacao != null) {
			model.addObject("comandos", controladorAcaoComando.consultarComandos(operacao.getChavePrimaria()));
		}

		return model;
	}
	
	
	/**
	 * Pesquisa Priorizacao Corte
	 * @param avisoCorteNotificacaoVO - {@link AvisoCorteNotificacaoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {link Model}
	 * @return string - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPriorizacaoCorte")
	public String pesquisarPriorizacaoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException{
					
		Collection<AvisoCorte> listaAvisoCorte = controladorAvisoCorte.obterListaAvisoCortePriorizar();
		listaAvisoCorte = controladorServicoAutorizacao.priorizacaoCorte(listaAvisoCorte);
		
		Collection<AvisoCorte> listaRetorno = new HashSet<>();
		
		for (AvisoCorte avisoAux : listaAvisoCorte) {
			if (avisoAux.getFatura().getHistoricoMedicao() == null) {
				Collection<Fatura> colecaoFaturasFilhas = controladorFatura
						.consultarFaturaPorFaturaAgrupamento(avisoAux.getFatura().getChavePrimaria());
				if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
					avisoAux.getFatura().getPontoConsumo().setDescricao("");
					for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
						String descricao = faturaAgrupada.getPontoConsumo().getDescricao();
						String descricaoAgrupada = avisoAux.getFatura().getPontoConsumo().getDescricao();

						if ("".equals(descricaoAgrupada)) {
							avisoAux.getFatura().getPontoConsumo().setDescricao(descricao);
						} else {
							avisoAux.getFatura().getPontoConsumo()
									.setDescricao(descricaoAgrupada.concat(Constantes.STRING_ESPACO)
											.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO)
											.concat(descricao));
						}
					}
				}
			}
		}
		
		if (avisoCorteNotificacaoVO.getHabilitado() == null) {
			model.addAttribute("listaAvisoCorte", listaAvisoCorte);
		} else {
			Boolean isPriorizado = avisoCorteNotificacaoVO.getHabilitado();
			for (AvisoCorte avisoAux : listaAvisoCorte) {
				if (isPriorizado && avisoAux.getIndicadorFaturaPriorizada()) {
					listaRetorno.add(avisoAux);
				} else if (!isPriorizado && (!avisoAux.getIndicadorFaturaPriorizada()
						|| avisoAux.getIndicadorFaturaPriorizada() == null)) {
					listaRetorno.add(avisoAux);
				}
			}

			model.addAttribute("listaAvisoCorte", listaRetorno);
		}

		request.getSession().setAttribute("listaAvisoCorteReq", listaAvisoCorte);
		request.getSession().setAttribute("listaAvisoCorteRoteirizados", null);

		return "priorizarLiberarFaturas";
	}
	
	/**
	 * Priorizacao Corte
	 * @param avisoCorteNotificacaoVO - {@link AvisoCorteNotificacaoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {link Model}
	 * @return string - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("priorizarAvisoDeCorte")
	public String priorizarAvisoDeCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException{
		
		Collection<AvisoCorte> listaAvisoCorte = (Collection<AvisoCorte>) request.getSession().getAttribute("listaAvisoCorteReq");
						
		model.addAttribute("listaAvisoCorte", listaAvisoCorte);
		request.getSession().setAttribute("listaAvisoCorteRoteirizados", null);
		return "priorizarLiberarFaturas";
	}
	
	
	/**
	 * Método responsável por remover faturas priorizacao corte.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param idsFatura               {@link Long}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerPriorizacaoCorte")
	public String removerPriorizacaoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] idsFatura, HttpServletRequest request,
			Model model) throws GGASException {
		
		Collection<AvisoCorte> listaAvisoCorte =  (Collection<AvisoCorte>) request.getSession().getAttribute("listaAvisoCorteReq");
		
		listaAvisoCorte = controladorAvisoCorte.removerAvisosDeCorte(idsFatura,listaAvisoCorte);
		
		model.addAttribute("listaAvisoCorte", listaAvisoCorte);
		request.getSession().setAttribute("listaAvisoCorteReq", listaAvisoCorte);
		request.getSession().setAttribute("listaAvisoCorteRoteirizados", null);
		
		return "priorizarLiberarFaturas";
	}
	
	/**
	 * Método responsável por roteirizar as faturas.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param numeroFaturas               {@link Integer}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @param idsFatura 			  {@link Long}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("concluirPriorizacaoCorte")
	public String concluirPriorizacaoCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] idsFatura, HttpServletRequest request,
			Model model) throws GGASException {
		Collection<AvisoCorte> listaPriorizada = (Collection<AvisoCorte>) request.getSession()
				.getAttribute("listaAvisoCorteReq");
		
		controladorAvisoCorte.salvarListaPriorizada(idsFatura, listaPriorizada);
		mensagemSucesso(model, MSG_SALVAR_PRIORIZACAO);
		model.addAttribute("listaAvisoCorte", listaPriorizada);

		return "priorizarLiberarFaturas";
	}
	
	/**
	 * Método responsável por roteirizar as faturas.
	 * 
	 * @param avisoCorteNotificacaoVO {@link AvisoCorteNotificacaoVO}
	 * @param result                  {@link BindingResult}
	 * @param numeroFaturas               {@link Integer}
	 * @param request                 {@link HttpServletRequest}
	 * @param model                   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("roteirizarCorte")
	public String roteirizarCorte(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO,
			BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		request.getSession().setAttribute("listaAvisoCorteRoteirizados", null);
		Collection<AvisoCorte> listaPriorizada = controladorAvisoCorte.consultarAvisosCortePriorizados();
		Collection<AvisoCorte> retirarListaComAS = new HashSet<>();
		
		for(AvisoCorte avisoCorte : listaPriorizada) {
			if(controladorAvisoCorteEmissao.validarSeTemServicoAutorizacaoPorFatura(avisoCorte.getFatura())){
				retirarListaComAS.add(avisoCorte);
			}
		}
		
		listaPriorizada.removeAll(retirarListaComAS);
		
		Collection<AvisoCorte> listaRoteirizada = new HashSet<>();
		if (!listaPriorizada.isEmpty()) {
			listaRoteirizada = controladorServicoAutorizacao.roteirizacaoCorte(listaPriorizada);
			listaRoteirizada =  controladorAvisoCorte
					.selecionarNumeroAvisoCorte(listaRoteirizada, avisoCorteNotificacaoVO.getNumeroPontos());
			
			for (AvisoCorte avisoAux : listaRoteirizada) {
				if (avisoAux.getFatura().getHistoricoMedicao() == null) {
					Collection<Fatura> colecaoFaturasFilhas = controladorFatura
							.consultarFaturaPorFaturaAgrupamento(avisoAux.getFatura().getChavePrimaria());
					if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
						avisoAux.getFatura().getPontoConsumo().setDescricao("");
						for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
							String descricao = faturaAgrupada.getPontoConsumo().getDescricao();
							String descricaoAgrupada = avisoAux.getFatura().getPontoConsumo().getDescricao();

							if ("".equals(descricaoAgrupada)) {
								avisoAux.getFatura().getPontoConsumo().setDescricao(descricao);
							} else {
								avisoAux.getFatura().getPontoConsumo()
										.setDescricao(descricaoAgrupada.concat(Constantes.STRING_ESPACO)
												.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO)
												.concat(descricao));
							}
						}
					}
				}
			}
			
			request.getSession().setAttribute("listaAvisoCorteRoteirizados", listaRoteirizada);
		}

		model.addAttribute("listaAvisoCorte", listaRoteirizada);
		return "exibirRoteirizarFaturas";
	}
	
	/**
	 * Método responsável por exibir tela de roteirizacao.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRoteirizacaoCorte")
	public String exibirRoteirizacaoCorte(HttpServletRequest request, Model model) throws GGASException {
		return "exibirRoteirizacaoCorte";
	}
	
	/**
	 * Método responsável para emitir as Autorizacoes de Servico via processo batch 
	 * 
	 * @param avisoCorteNotificacaoVO - {@link AvisoCorteNotificacaoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String {@link String}
	 * @throws GGASException the GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("emitirAutorizacaoServicoLote")
	public String emitirAutorizacaoServicoLote(AvisoCorteNotificacaoVO avisoCorteNotificacaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		Processo processo = null;
		ProcessoVO processoVo = new ProcessoVO();
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_OPERACAO_GERAR_SERV_AUT_LOTE);
		Operacao operacao = controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor()));
		try {
			Collection<AvisoCorte> listaRoteirizada = (Collection<AvisoCorte>) request.getSession().getAttribute("listaAvisoCorteRoteirizados");
			processo = controladorServicoAutorizacao.inserirProcessoEmLoteParaCorte(listaRoteirizada,
					obterUsuario(request), getDadosAuditoria(request));
		    processoVo.setModulo(operacao.getModulo().getChavePrimaria());
		    processo.setSituacao(-1);
			mensagemSucesso(model, MSG_SUCESSO_PROCESSO_INSERIDO);
			request.getSession().removeAttribute("listaAvisoCorteReq");
			request.getSession().removeAttribute("listaAvisoCorteRoteirizados");
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, request, e);
		}
		
		return processoAction.pesquisarProcesso((ProcessoImpl) processo, result, processoVo, result, request, model);
		
	}
}
