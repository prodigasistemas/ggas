/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe PerfilParcelamentoAction representa uma PerfilParcelamentoAction no sistema.
 *
 * @since 22/02/2010
 * 
 */

package br.com.ggas.web.cobranca.perfilparcelamento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSegmento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSituacaoPontoConsumo;
import br.com.ggas.cobranca.perfilparcelamento.impl.ParcelamentoPerfilImpl;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas aos perfis de parcelamento
 *
 */
@Controller
public class PerfilParcelamentoAction extends GenericAction {

	private static final String PERFIL_PARCELAMENTO = "perfilParcelamento";

	private static final String VIGENCIA_FINAL = "vigenciaFinal";

	private static final String VIGENCIA_INICIAL = "vigenciaInicial";
	
	private static final String VIGENCIA_FINAL_HIDDEN = "vigenciaFinalHidden";

	private static final String VIGENCIA_INICIAL_HIDDEN = "vigenciaInicialHidden";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String DESCRICAO_AMORTIZACAO = "descricaoAmortizacao";

	private static final String LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO = "listaParcPerfilSituacaoPontoConsumo";

	private static final String LISTA_PARC_PERFIL_SEGMENTO = "listaParcPerfilSegmento";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String LISTA_PERFIL = "listaPerfil";

	private static final String HABILITADO = "habilitado";

	private static final String MAXIMO_PARCELAS = "maximoParcelas";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String DESCRICAO = "descricao";

	private static final String LISTA_AMORTIZACAO = "listaAmortizacao";

	private static final String LISTA_SITUACOES_ASSOCIADOS = "listaSituacoesAssociados";

	private static final String LISTA_SITUACOES = "listaSituacoes";

	private static final String ID_SITUACOES_ASSOCIADAS = "idSituacoesAssociadas";

	private static final String LISTA_SEGMENTOS_ASSOCIADOS = "listaSegmentosAssociados";

	private static final String LISTA_SEGMENTOS = "listaSegmentos";

	private static final String ID_SEGMENTOS_ASSOCIADOS = "idSegmentosAssociados";

	private static final String ATUALIZAR_SOMENTE_VIGENCIA = "atualizarSomenteVigencia";

	private static final String ID_SITUACOES_ASSOCIADAS_HIDDEN = "idSituacoesAssociadasHidden";

	private static final String ID_SEGMENTOS_ASSOCIADOS_HIDDEN = "idSegmentosAssociadosHidden";

	private static final String LISTA_INDICE_FINANCEIRO = "listaIndiceFinanceiro";

	private static final String DESCRICAO_INDICE_FINANCEIRO = "descricaoIndiceFinanceiro";

	@Autowired
	private ControladorPerfilParcelamento controladorPerfilParcelamento;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorParcelamento controladorParcelamento;

	/**
	 * Método responsável por exibir a tela de pesquisa de Perfil de Parcelamento.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaPerfilParcelamento")
	public String exibirPesquisaPerfilParcelamento(HttpServletRequest request, Model model) throws NegocioException {

		model.addAttribute(LISTA_SEGMENTOS, controladorSegmento.listarSegmento());
		request.getSession().removeAttribute(ID_SEGMENTO);
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaPerfilParcelamento";
	}

	/**
	 * Método responsável por pesquisar os Perfis de Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param vigenciaInicial
	 *            - {@link String}
	 * @param vigenciaFinal
	 *            - {@link String}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarPerfilParcelamento")
	public String pesquisarPerfilParcelamento(ParcelamentoPerfilImpl parcelamentoPerfil, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado,
			@RequestParam(value = VIGENCIA_INICIAL, required = false) String vigenciaInicial,
			@RequestParam(value = VIGENCIA_FINAL, required = false) String vigenciaFinal, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute(PERFIL_PARCELAMENTO, parcelamentoPerfil);
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, parcelamentoPerfil, request, vigenciaInicial, vigenciaFinal, habilitado, model);
		model.addAttribute(LISTA_PERFIL, controladorPerfilParcelamento.consultarPerfilParcelamento(filtro));

		return exibirPesquisaPerfilParcelamento(request, model);
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Perfil de Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param idSegmentosAssociados
	 *            - {@link Long}
	 * @param idSituacoesAssociadas
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoPerfilParcelamento")
	public String exibirInclusaoPerfilParcelamento(
			@RequestParam(value = ID_SEGMENTOS_ASSOCIADOS, required = false) Long[] idSegmentosAssociados,
			@RequestParam(value = ID_SITUACOES_ASSOCIADAS, required = false) Long[] idSituacoesAssociadas,
			HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);

		this.carregarCampos(model);
		this.tratarCamposSelect(idSegmentosAssociados, idSituacoesAssociadas, model);

		return "exibirInclusaoPerfilParcelamento";
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Perfil de Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param idSegmentosAssociados
	 *            - {@link Long}
	 * @param idSituacoesAssociadas
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirPerfilParcelamento")
	public String incluirPerfilParcelamento(ParcelamentoPerfilImpl parcelamentoPerfil, BindingResult result,
			@RequestParam(value = ID_SEGMENTOS_ASSOCIADOS, required = false) Long[] idSegmentosAssociados,
			@RequestParam(value = ID_SITUACOES_ASSOCIADAS, required = false) Long[] idSituacoesAssociadas,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "forward:exibirInclusaoPerfilParcelamento";
		model.addAttribute(PERFIL_PARCELAMENTO, parcelamentoPerfil);
		String vigenciaInicial = null;
		String vigenciaFinal = null;
		String maximoParcelas = null;

		if (request.getParameter(VIGENCIA_INICIAL) != null) {
			vigenciaInicial = (String) request.getParameter(VIGENCIA_INICIAL);
			model.addAttribute(VIGENCIA_INICIAL, vigenciaInicial);
		}

		if (request.getParameter(VIGENCIA_FINAL) != null) {
			vigenciaFinal = (String) request.getParameter(VIGENCIA_FINAL);
			model.addAttribute(VIGENCIA_FINAL, vigenciaFinal);
		}

		if (request.getParameter(MAXIMO_PARCELAS) != null) {
			maximoParcelas = (String) request.getParameter(MAXIMO_PARCELAS);
			model.addAttribute(MAXIMO_PARCELAS, maximoParcelas);
		}

		this.setarCamposSelectNoPerfil(idSegmentosAssociados, idSituacoesAssociadas, request, parcelamentoPerfil,
				model);

		try {
			validarToken(request);
			if (maximoParcelas != null && !StringUtils.isEmpty(maximoParcelas)) {
				parcelamentoPerfil.setMaximoParcelas(Util.converterCampoStringParaValorInteger(maximoParcelas));
			}
			Map<String, Object> erros = parcelamentoPerfil.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
			} else {
				parcelamentoPerfil.setHabilitado(true);
				parcelamentoPerfil.setVigenciaInicial(Util.converterCampoStringParaData(
						ParcelamentoPerfil.VIGENCIA_INICIAL, vigenciaInicial, Constantes.FORMATO_DATA_BR));

				if (!"".equals(vigenciaFinal)) {
					parcelamentoPerfil.setVigenciaFinal(Util.converterCampoStringParaData(
							ParcelamentoPerfil.VIGENCIA_FINAL, vigenciaFinal, Constantes.FORMATO_DATA_BR));
				}

				controladorPerfilParcelamento.inserir(parcelamentoPerfil);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						ParcelamentoPerfil.PARCELAMENTO_PERFIL);
				view = pesquisarPerfilParcelamento(parcelamentoPerfil, result, true, vigenciaInicial, vigenciaFinal,
						request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Tratar campos select.
	 * 
	 * @param idSegmentosAssociados
	 *            - {@link Long}
	 * @param idSituacoesAssociadas
	 *            - {@link Long}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void tratarCamposSelect(Long[] idSegmentosAssociados, Long[] idSituacoesAssociadas, Model model)
			throws GGASException {

		if (idSegmentosAssociados != null && idSegmentosAssociados.length > 0) {
			Collection<Segmento> listaSegmentosAssociados = new ArrayList<Segmento>();
			for (Long chaveSegmento : idSegmentosAssociados) {
				Segmento segmento = (Segmento) controladorSegmento.obter(chaveSegmento);
				listaSegmentosAssociados.add(segmento);
			}

			Collection<Segmento> listaSegmentosDisp = controladorSegmento.listarSegmento();
			listaSegmentosDisp.removeAll(listaSegmentosAssociados);
			model.addAttribute(LISTA_SEGMENTOS, listaSegmentosDisp);
			model.addAttribute(LISTA_SEGMENTOS_ASSOCIADOS, listaSegmentosAssociados);
		}

		if (idSituacoesAssociadas != null && idSituacoesAssociadas.length > 0) {
			Collection<SituacaoConsumo> listaSituacoesAssociados = new ArrayList<SituacaoConsumo>();
			for (Long chaveSituacao : idSituacoesAssociadas) {
				SituacaoConsumo situacao = controladorImovel.obterSituacaoConsumo(chaveSituacao);
				listaSituacoesAssociados.add(situacao);
			}

			Collection<SituacaoConsumo> listaSituacoesDisp = controladorPontoConsumo.listarSituacoesPontoConsumo();
			listaSituacoesDisp.removeAll(listaSituacoesAssociados);
			model.addAttribute(LISTA_SITUACOES, listaSituacoesDisp);
			model.addAttribute(LISTA_SITUACOES_ASSOCIADOS, listaSituacoesAssociados);
		}
	}

	/**
	 * Setar campos select no perfil.
	 * 
	 * @param idSegmentosAssociadosTmp
	 *            - {@link Long}
	 * @param idSituacoesAssociadasTmp
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param perfil
	 *            - {@link ParcelamentoPerfil}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void setarCamposSelectNoPerfil(Long[] idSegmentosAssociadosTmp, Long[] idSituacoesAssociadasTmp,
			HttpServletRequest request, ParcelamentoPerfil perfil, Model model) throws GGASException {

		Collection<ParcelamentoPerfilSegmento> listaParcPerfilSegmento = new ArrayList<ParcelamentoPerfilSegmento>();
		String segmentosAssociadosHiddenString = null;
		String situacoesAssociadasHiddenString = null;
		Long[] idSegmentosAssociados = idSegmentosAssociadosTmp;
		Long[] idSituacoesAssociadas = idSituacoesAssociadasTmp;

		if (request.getAttribute(ID_SEGMENTOS_ASSOCIADOS_HIDDEN) != null) {
			segmentosAssociadosHiddenString = (String) request.getAttribute(ID_SEGMENTOS_ASSOCIADOS_HIDDEN);
		}
		if (request.getAttribute(ID_SITUACOES_ASSOCIADAS_HIDDEN) != null) {
			situacoesAssociadasHiddenString = (String) request.getAttribute(ID_SITUACOES_ASSOCIADAS_HIDDEN);
		}

		if (idSegmentosAssociados != null && idSegmentosAssociados.length == 0
				&& !StringUtils.isEmpty(segmentosAssociadosHiddenString)) {
			String[] segmentosAssociadosHidden = segmentosAssociadosHiddenString.split(",");
			idSegmentosAssociados = Util.arrayStringParaArrayLong(segmentosAssociadosHidden);

		}

		if (idSegmentosAssociados != null && idSegmentosAssociados.length > 0) {
			for (Long idSegmento : idSegmentosAssociados) {
				Segmento segmento = (Segmento) controladorSegmento.obter(idSegmento);

				ParcelamentoPerfilSegmento segmentoPerfil = controladorPerfilParcelamento
						.obterSegmentoPerfilParcelamento(perfil.getChavePrimaria(), segmento.getChavePrimaria());

				if (segmentoPerfil != null) {
					listaParcPerfilSegmento.add(segmentoPerfil);
				} else {
					ParcelamentoPerfilSegmento perfilSegmento = (ParcelamentoPerfilSegmento) controladorPerfilParcelamento
							.criarParcelamentoPerfilSegmento();
					perfilSegmento.setSegmento(segmento);
					perfilSegmento.setHabilitado(Boolean.TRUE);

					listaParcPerfilSegmento.add(perfilSegmento);
				}
			}
			perfil.getListaParcPerfilSegmento().clear();
			perfil.getListaParcPerfilSegmento().addAll(listaParcPerfilSegmento);
		} else {
			perfil.getListaParcPerfilSegmento().clear();
		}

		Collection<ParcelamentoPerfilSituacaoPontoConsumo> listaParcPerfilSituacaoPC = new ArrayList<ParcelamentoPerfilSituacaoPontoConsumo>();

		if (idSituacoesAssociadas != null && idSituacoesAssociadas.length == 0
				&& !StringUtils.isEmpty(situacoesAssociadasHiddenString)) {
			String[] situacoesAssociadasHidden = situacoesAssociadasHiddenString.split(",");
			idSituacoesAssociadas = Util.arrayStringParaArrayLong(situacoesAssociadasHidden);

		}

		if (idSituacoesAssociadas != null && idSituacoesAssociadas.length > 0) {
			for (Long idSituacao : idSituacoesAssociadas) {
				ParcelamentoPerfilSituacaoPontoConsumo perfilSituacao = (ParcelamentoPerfilSituacaoPontoConsumo) controladorPerfilParcelamento
						.criarParcelamentoPerfilSituacaoPontoConsumo();

				ParcelamentoPerfilSituacaoPontoConsumo parcelamentoPerfilSituacao = controladorPerfilParcelamento
						.obterSituacaoConsumoPerfilParcelamento(perfil.getChavePrimaria(), idSituacao);

				if (parcelamentoPerfilSituacao == null) {
					SituacaoConsumo situacao = controladorImovel.obterSituacaoConsumo(idSituacao);

					perfilSituacao.setSituacaoConsumo(situacao);
					perfilSituacao.setHabilitado(Boolean.TRUE);

					listaParcPerfilSituacaoPC.add(perfilSituacao);
				} else {
					listaParcPerfilSituacaoPC.add(parcelamentoPerfilSituacao);
				}
			}
			perfil.getListaParcPerfilSituacaoPontoConsumo().clear();
			perfil.getListaParcPerfilSituacaoPontoConsumo().addAll(listaParcPerfilSituacaoPC);
		} else {
			perfil.getListaParcPerfilSituacaoPontoConsumo().clear();
		}

		model.addAttribute(ID_SITUACOES_ASSOCIADAS, idSituacoesAssociadas);
		model.addAttribute(ID_SEGMENTOS_ASSOCIADOS, idSegmentosAssociados);
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_INDICE_FINANCEIRO, controladorTarifa.listaIndiceFinanceiro());
		model.addAttribute(LISTA_AMORTIZACAO, controladorEntidadeConteudo.obterListaAmortizacao());
		model.addAttribute(LISTA_SEGMENTOS, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_SITUACOES, controladorPontoConsumo.listarSituacoesPontoConsumo());
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro
	 *            - {@link Map}
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, ParcelamentoPerfilImpl parcelamentoPerfil,
			HttpServletRequest request, String vigenciaInicial, String vigenciaFinal, Boolean habilitado, Model model)
			throws GGASException {

		Long idSegmento = 0L;

		if (request.getParameter(ID_SEGMENTO) != null) {
			idSegmento = Long.parseLong(request.getParameter(ID_SEGMENTO));
		} else if (request.getSession().getAttribute(ID_SEGMENTO) != null) {
			idSegmento = (Long) request.getSession().getAttribute(ID_SEGMENTO);
		}

		if (parcelamentoPerfil != null) {

			if (!StringUtils.isEmpty(parcelamentoPerfil.getDescricao())) {
				filtro.put(DESCRICAO, parcelamentoPerfil.getDescricao().trim());
			}

			if (!StringUtils.isEmpty(parcelamentoPerfil.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, parcelamentoPerfil.getDescricaoAbreviada().trim());
			}
		}

		if (idSegmento != null && idSegmento > 0) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		if (!StringUtils.isEmpty(vigenciaInicial) && Util.isDataValida(vigenciaInicial, Constantes.FORMATO_DATA_BR)) {
			filtro.put(VIGENCIA_INICIAL, Util.converterCampoStringParaData(ParcelamentoPerfil.VIGENCIA_INICIAL,
					vigenciaInicial, Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(vigenciaFinal) && Util.isDataValida(vigenciaFinal, Constantes.FORMATO_DATA_BR)) {
			filtro.put(VIGENCIA_FINAL, Util.converterCampoStringParaData(ParcelamentoPerfil.VIGENCIA_FINAL,
					vigenciaFinal, Constantes.FORMATO_DATA_BR));
		}

		model.addAttribute(VIGENCIA_INICIAL, vigenciaInicial);
		model.addAttribute(VIGENCIA_FINAL, vigenciaFinal);
		model.addAttribute(ID_SEGMENTO, idSegmento);
		model.addAttribute(HABILITADO, habilitado);
	}

	/**
	 * Método responsável por exibir a tela de alteração de Perfil de Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResul}
	 * @param idSegmentosAssociadosTmp
	 *            - {@link Long}
	 * @param idSituacoesAssociadasTmp
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoPerfilParcelamento")
	public String exibirAlteracaoPerfilParcelamento(ParcelamentoPerfilImpl parcelamentoPerfil, BindingResult result,
			@RequestParam(value = ID_SEGMENTOS_ASSOCIADOS, required = false) Long[] idSegmentosAssociadosTmp,
			@RequestParam(value = ID_SITUACOES_ASSOCIADAS, required = false) Long[] idSituacoesAssociadasTmp,
			HttpServletRequest request, Model model) throws GGASException {

		if(retornaPesquisa(request, PERFIL_PARCELAMENTO) == null) {
			salvarPesquisa(request, parcelamentoPerfil, PERFIL_PARCELAMENTO);
		}
		Long idSegmento = 0L;
		Long[] idSegmentosAssociados = idSegmentosAssociadosTmp;
		Long[] idSituacoesAssociadas = idSituacoesAssociadasTmp;
		String vigenciaInicial = "";
		String vigenciaFinal = "";

		if (request.getParameter(ID_SEGMENTO) != null) {
			idSegmento = Long.parseLong(request.getParameter(ID_SEGMENTO));
		}
		if (request.getParameter(VIGENCIA_INICIAL_HIDDEN) != null) {
			vigenciaInicial = (String) request.getParameter(VIGENCIA_INICIAL_HIDDEN);
			model.addAttribute(VIGENCIA_INICIAL_HIDDEN, vigenciaInicial);
		}

		if (request.getParameter(VIGENCIA_FINAL_HIDDEN) != null) {
			vigenciaFinal = (String) request.getParameter(VIGENCIA_FINAL_HIDDEN);
			model.addAttribute(VIGENCIA_FINAL_HIDDEN, vigenciaFinal);
		}

		if(retornaPesquisa(request, ID_SEGMENTO) == null) {
			salvarPesquisa(request, idSegmento, ID_SEGMENTO);
		}

		this.carregarCampos(model);
		if (!super.isPostBack(request)) {
			ParcelamentoPerfil perfil = (ParcelamentoPerfil) controladorPerfilParcelamento.obter(
					parcelamentoPerfil.getChavePrimaria(), LISTA_PARC_PERFIL_SEGMENTO,
					LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO);
			model.addAttribute(VIGENCIA_INICIAL, Util
					.converterDataParaStringSemHoraSemBarra(perfil.getVigenciaInicial(), Constantes.FORMATO_DATA_BR));

			if (perfil.getVigenciaFinal() != null) {
				model.addAttribute(VIGENCIA_FINAL, Util
						.converterDataParaStringSemHoraSemBarra(perfil.getVigenciaFinal(), Constantes.FORMATO_DATA_BR));
			}

			Collection<Parcelamento> colecaoParcelamento = controladorParcelamento
					.consultarParcelamentosVinculadosPerfilParcelamento(parcelamentoPerfil.getChavePrimaria());

			if (colecaoParcelamento != null && !colecaoParcelamento.isEmpty()) {
				if (perfil.getVigenciaFinal() == null) {
					model.addAttribute(ATUALIZAR_SOMENTE_VIGENCIA, Boolean.TRUE);
				} else {
					controladorPerfilParcelamento.validarPerfilParcelamentoVigenciaFinal(perfil.getVigenciaFinal());
				}
			}
			model.addAttribute(PERFIL_PARCELAMENTO, perfil);
			popularCamposSelectNoForm(model, perfil);
			idSegmentosAssociados = (Long[]) model.asMap().get(ID_SEGMENTOS_ASSOCIADOS);
			idSituacoesAssociadas = (Long[]) model.asMap().get(ID_SITUACOES_ASSOCIADAS);

		}
		tratarCamposSelect(idSegmentosAssociados, idSituacoesAssociadas, model);
		saveToken(request);
		return "exibirAlteracaoPerfilParcelamento";
	}

	/**
	 * Popular campos select no form.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param perfil
	 *            - {@link ParcelamentoPerfil}
	 */
	private void popularCamposSelectNoForm(Model model, ParcelamentoPerfil perfil) {

		if (perfil.getListaParcPerfilSegmento() != null && !perfil.getListaParcPerfilSegmento().isEmpty()) {
			Collection<Segmento> listaPerfilSegmento = new ArrayList<Segmento>();
			for (ParcelamentoPerfilSegmento perfilSegmento : perfil.getListaParcPerfilSegmento()) {
				listaPerfilSegmento.add(perfilSegmento.getSegmento());
			}
			model.addAttribute(ID_SEGMENTOS_ASSOCIADOS, Util.collectionParaArrayChavesPrimarias(listaPerfilSegmento));
			model.addAttribute(ID_SEGMENTOS_ASSOCIADOS_HIDDEN,
					Util.arrayParaString(Util.collectionParaArrayChavesPrimarias(listaPerfilSegmento), ","));
		}

		if (perfil.getListaParcPerfilSituacaoPontoConsumo() != null
				&& !perfil.getListaParcPerfilSituacaoPontoConsumo().isEmpty()) {
			Collection<SituacaoConsumo> listaPerfilSituacao = new ArrayList<SituacaoConsumo>();
			for (ParcelamentoPerfilSituacaoPontoConsumo perfilSituacao : perfil
					.getListaParcPerfilSituacaoPontoConsumo()) {
				listaPerfilSituacao.add(perfilSituacao.getSituacaoConsumo());
			}

			model.addAttribute(ID_SITUACOES_ASSOCIADAS, Util.collectionParaArrayChavesPrimarias(listaPerfilSituacao));

			model.addAttribute(ID_SITUACOES_ASSOCIADAS_HIDDEN,
					Util.arrayParaString(Util.collectionParaArrayChavesPrimarias(listaPerfilSituacao), ","));
		}
	}

	/**
	 * Método responsável por alterar Perfil Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param idSegmentosAssociados
	 *            - {@link Long}
	 * @param idSituacoesAssociadas
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarPerfilParcelamento")
	public String alterarPerfilParcelamento(ParcelamentoPerfilImpl parcelamentoPerfil, BindingResult result,
			@RequestParam(value = ID_SEGMENTOS_ASSOCIADOS, required = false) Long[] idSegmentosAssociados,
			@RequestParam(value = ID_SITUACOES_ASSOCIADAS, required = false) Long[] idSituacoesAssociadas,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "forward:exibirAlteracaoPerfilParcelamento";
		model.addAttribute(PERFIL_PARCELAMENTO, parcelamentoPerfil);
		String vigenciaInicial = null;
		String vigenciaFinal = null;
		String maximoParcelas = null;

		if (request.getParameter(VIGENCIA_INICIAL) != null) {
			vigenciaInicial = (String) request.getParameter(VIGENCIA_INICIAL);
			model.addAttribute(VIGENCIA_INICIAL, vigenciaInicial);
		}

		if (request.getParameter(VIGENCIA_FINAL) != null) {
			vigenciaFinal = (String) request.getParameter(VIGENCIA_FINAL);
			model.addAttribute(VIGENCIA_FINAL, vigenciaFinal);
		}

		if (request.getParameter(MAXIMO_PARCELAS) != null) {
			maximoParcelas = (String) request.getParameter(MAXIMO_PARCELAS);
			model.addAttribute(MAXIMO_PARCELAS, maximoParcelas);
		}

		this.setarCamposSelectNoPerfil(idSegmentosAssociados, idSituacoesAssociadas, request, parcelamentoPerfil,
				model);
		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);

			if (maximoParcelas != null && !StringUtils.isEmpty(maximoParcelas)) {
				parcelamentoPerfil.setMaximoParcelas(Util.converterCampoStringParaValorInteger(maximoParcelas));
			}
			Map<String, Object> erros = parcelamentoPerfil.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
			} else {
				controladorPerfilParcelamento.validarDataVigenciaInicialPerfilParcelamento(
						Util.converterCampoStringParaData(ParcelamentoPerfil.VIGENCIA_INICIAL, vigenciaInicial,
								Constantes.FORMATO_DATA_BR),
						parcelamentoPerfil.getVigenciaInicial());
				parcelamentoPerfil.setVigenciaInicial(Util.converterCampoStringParaData(
						ParcelamentoPerfil.VIGENCIA_INICIAL, vigenciaInicial, Constantes.FORMATO_DATA_BR));

				if (!"".equals(vigenciaFinal)) {
					parcelamentoPerfil.setVigenciaFinal(Util.converterCampoStringParaData(
							ParcelamentoPerfil.VIGENCIA_FINAL, vigenciaFinal, Constantes.FORMATO_DATA_BR));
				}

				parcelamentoPerfil.getListaParcPerfilSegmento().iterator().next();

				controladorPerfilParcelamento.atualizar(parcelamentoPerfil);

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						ParcelamentoPerfil.PARCELAMENTO_PERFIL);
				view = pesquisarPerfilParcelamento(parcelamentoPerfil, result, parcelamentoPerfil.isHabilitado(),
						vigenciaInicial, vigenciaFinal, request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por excluir o Perfil de Parcelamento.
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("excluirPerfilParcelamento")
	public String excluirPerfilParcelamento(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			getDadosAuditoria(request);
			controladorPerfilParcelamento.removerPerfisParcelamento(chavesPrimarias);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request,
					ParcelamentoPerfil.PARCELAMENTO_PERFIL);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarPerfilParcelamento(null, null, true, null, null, request, model);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento Perfil de Parcelamento.
	 * 
	 * @param parcelamentoPerfil
	 *            - {@link ParcelamentoPerfilImpl}
	 * @param result
	 *            - {@link BindingResul}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoPerfilParcelamento")
	public String exibirDetalhamentoPerfilParcelamento(ParcelamentoPerfilImpl parcelamentoPerfil, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		salvarPesquisa(request, parcelamentoPerfil, PERFIL_PARCELAMENTO);
		Long idSegmento = 0L;
		Long[] idSegmentosAssociados = null;
		Long[] idSituacoesAssociadas = null;
		String vigenciaInicial = "";
		String vigenciaFinal = "";

		if (request.getParameter(ID_SEGMENTO) != null) {
			idSegmento = Long.parseLong(request.getParameter(ID_SEGMENTO));
		}
		if (request.getParameter(VIGENCIA_INICIAL) != null) {
			vigenciaInicial = (String) request.getParameter(VIGENCIA_INICIAL);
			model.addAttribute(VIGENCIA_INICIAL, vigenciaInicial);
		}

		if (request.getParameter(VIGENCIA_FINAL) != null) {
			vigenciaFinal = (String) request.getParameter(VIGENCIA_FINAL);
			model.addAttribute(VIGENCIA_FINAL, vigenciaFinal);
		}

		salvarPesquisa(request, idSegmento, ID_SEGMENTO);
		if (!super.isPostBack(request)) {
			ParcelamentoPerfil perfil = (ParcelamentoPerfil) controladorPerfilParcelamento.obter(
					parcelamentoPerfil.getChavePrimaria(), LISTA_PARC_PERFIL_SEGMENTO,
					LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO, "tipoTabela", "indiceFinanceiro");
			model.addAttribute(PERFIL_PARCELAMENTO, perfil);
			if (perfil.getTipoTabela() != null) {
				model.addAttribute(DESCRICAO_AMORTIZACAO, perfil.getTipoTabela().getDescricao());
			}
			if (perfil.getIndiceFinanceiro() != null) {
				model.addAttribute(DESCRICAO_INDICE_FINANCEIRO, perfil.getIndiceFinanceiro().getDescricao());
			}
			popularCamposSelectNoForm(model, perfil);
			idSegmentosAssociados = (Long[]) model.asMap().get(ID_SEGMENTOS_ASSOCIADOS);
			idSituacoesAssociadas = (Long[]) model.asMap().get(ID_SITUACOES_ASSOCIADAS);
		}
		carregarCampos(model);
		tratarCamposSelect(idSegmentosAssociados, idSituacoesAssociadas, model);

		return "exibirDetalhamentoPerfilParcelamento";
	}

	/**
	 * Responsável por recuperar um filtro anterior
	 * 
	 * @param parcelamentoPerfilTmp
	 *            - {@link ParcelamentoPerfil}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param vigenciaInicialHidden
	 *            - {@link String}
	 * @param vigenciaFinalHidden
	 *            - {@link String}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("voltarPerfilParcelamento")
	public String voltar(ParcelamentoPerfilImpl parcelamentoPerfilTmp, BindingResult result, HttpServletRequest request,
			@RequestParam("vigenciaInicialHidden") String vigenciaInicialHidden,
			@RequestParam("vigenciaFinalHidden") String vigenciaFinalHidden, Model model) throws GGASException {
		ParcelamentoPerfilImpl parcelamentoPerfil = parcelamentoPerfilTmp;
		String view = "forward:pesquisarPerfilParcelamento";
		if (parcelamentoPerfilTmp != null) {
			parcelamentoPerfil = (ParcelamentoPerfilImpl) retornaPesquisa(request, PERFIL_PARCELAMENTO);
			view = pesquisarPerfilParcelamento(parcelamentoPerfil, result, parcelamentoPerfil.isHabilitado(),
					vigenciaInicialHidden, vigenciaFinalHidden, request, model);
		}
		request.getSession().removeAttribute(PERFIL_PARCELAMENTO);
		return view;
	}

}
