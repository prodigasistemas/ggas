/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.extratodebito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ggas.cadastro.funcionario.Funcionario;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.base.Strings;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas: Declaração de Quitação Anual e Extrato de
 * Débitos e seus respectivos métodos.
 *
 */
@Controller
public class ExtratoDebitoAction extends GenericAction {

	private static final String EXTRATO_DEBITO_VO = "extratoDebitoVO";

	private static final String ID_IMOVEL = "idImovel";

	private static final String VALOR_DEBITOS_SIMULADOS = "valorDebitosSimulados";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String ANO_GERACAO = "anoGeracao";

	private static final String ROTULO_DATA_VENCIMENTO = "Data Vencimento";

	private static final String LISTA_CREDITO_DEBITO = "listaCreditoDebito";

	private static final String LISTA_FATURA = "listaFatura";

	private static final String ID_CLIENTE = "idCliente";

	private static final String INDICADOR_PESQUISA_GERAL = "indicadorPesquisaGeral";

	private static final String INDICADOR_PESQUISA_CONTRATO = "indicadorPesquisaContrato";

	private static final String INDICADOR_PESQUISA_CLIENTE = "indicadorPesquisaCliente";

	private static final String INDICADOR_PESQUISA_IMOVEL = "indicadorPesquisaImovel";

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorProcesso controladorProcesso;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Método responsável por gerar o extrato de quitação anual.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws IOException 
	 */
	@RequestMapping("gerarExtratoQuitacaoAnual")
	public String gerarExtratoQuitacaoAnual(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

		Boolean indicadorPesquisaGeral = false;
		model.addAttribute(EXTRATO_DEBITO_VO, extratoDebitoVO);
		try {
			if (extratoDebitoVO.getIndicadorPesquisa().equals(INDICADOR_PESQUISA_GERAL)) {
				indicadorPesquisaGeral = true;
			}

			controladorCobranca.validarFiltroDeclaracaoQuitacaoAnual(extratoDebitoVO.getAnoGeracao(),
					extratoDebitoVO.getIdCliente(), extratoDebitoVO.getIdPontoConsumo(),
					extratoDebitoVO.getNumeroContrato(), indicadorPesquisaGeral);

			byte[] relatorioDeclaracaoQuitacaoAnual = null;
			if (extratoDebitoVO.getIndicadorPesquisa() != null) {
				if (extratoDebitoVO.getIndicadorPesquisa().equals(INDICADOR_PESQUISA_GERAL)) {
					this.processarDeclaracaoQuitacaoAnualDebito(extratoDebitoVO, result, request, model);
				} else if (extratoDebitoVO.getIndicadorPesquisa().equals(INDICADOR_PESQUISA_CONTRATO)) {
					relatorioDeclaracaoQuitacaoAnual = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(
							extratoDebitoVO.getAnoGeracao(), null, null, extratoDebitoVO.getNumeroContrato());
				} else if (extratoDebitoVO.getIndicadorPesquisa().equals(INDICADOR_PESQUISA_CLIENTE)
						|| extratoDebitoVO.getIndicadorPesquisa().equals(INDICADOR_PESQUISA_IMOVEL)) {
					relatorioDeclaracaoQuitacaoAnual = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(
							extratoDebitoVO.getAnoGeracao(), extratoDebitoVO.getIdCliente(),
							extratoDebitoVO.getIdPontoConsumo(), null);

				}
			}

			// não faz download se entrar no batch.
			if (relatorioDeclaracaoQuitacaoAnual != null) {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(relatorioDeclaracaoQuitacaoAnual.length);
				response.addHeader("Content-Disposition", "attachment; filename=relatorioExtratoDebito.pdf");
				servletOutputStream.write(relatorioDeclaracaoQuitacaoAnual, 0, relatorioDeclaracaoQuitacaoAnual.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} else {
				// exibir mensagem de processo
				// encaminhado para batch
				// ou mensagem de débitos em aberto
				// caso haja algum e não gere a
				// declaração de quitação anual.
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaExtratoQuitacaoAnual(request, model);
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de Extrato de Débitos.
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("exibirPesquisaExtratoDebitos")
	public String exibirPesquisaExtratoDebitos(Model model) {

		return "exibirPesquisaExtratoDebitos";
	}

	/**
	 * Método responsável por pesquisar os Extratos de Débitos.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("pesquisarPontosConsumoExtratoDebito")
	public String pesquisarPontosConsumoExtratoDebito(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, Model model) {

		model.addAttribute(EXTRATO_DEBITO_VO, extratoDebitoVO);
		try {
			pesquisarPontos(extratoDebitoVO, result, model);
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return exibirPesquisaExtratoDebitos(model);
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo de um imóvel.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("pesquisarPontosConsumoAnual")
	public String pesquisarPontosConsumoAnual(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, Model model) {

		model.addAttribute(EXTRATO_DEBITO_VO, extratoDebitoVO);
		try {
			pesquisarPontos(extratoDebitoVO, result, model);
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return exibirPesquisaExtratoQuitacaoAnual(request, model);
	}

	/**
	 * Pesquisar pontos.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void pesquisarPontos(ExtratoDebitoVO extratoDebitoVO, BindingResult result, Model model)
			throws NegocioException {

		Collection<ContratoPontoConsumo> listaContratoPontoConsumos = null;
		if (extratoDebitoVO.getIdImovel() != null && extratoDebitoVO.getIdImovel() > 0) {
			listaContratoPontoConsumos = controladorContrato
					.consultarContratoClientePontoConsumoPorImovel(extratoDebitoVO.getIdImovel());
		}
		if ((extratoDebitoVO.getIdCliente() != null) && (extratoDebitoVO.getIdCliente() > 0)) {
			listaContratoPontoConsumos = controladorContrato
					.consultarContratoPontoConsumoPorCliente(extratoDebitoVO.getIdCliente());
		}

		model.addAttribute("listaContratoPontoConsumos", listaContratoPontoConsumos);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de extrato de débitos do
	 * cliente.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirExtratoDebitos")
	public String exibirExtratoDebitos(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirExtratoDebitos";
		try {
			String valor = request.getParameter(VALOR_DEBITOS_SIMULADOS);
			controladorCobranca.validarExibirExtratoDebito(extratoDebitoVO.getIdCliente(),
					extratoDebitoVO.getIdPontoConsumo(), extratoDebitoVO.getIdImovel());

			Long idPagamentoPendente = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
			Long idPagamentoParcial = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));

			Long[] arrayPagamento = { idPagamentoPendente, idPagamentoParcial };

			Map<String, Object> filtroFatura = montaFiltroListafatura(extratoDebitoVO.getIdCliente(),
					extratoDebitoVO.getIdImovel(), extratoDebitoVO.getIdPontoConsumo(), arrayPagamento);
			Collection<Fatura> listaFatura = controladorFatura.consultarFatura(filtroFatura);

			Map<String, Object> filtro = montafiltroListaDebitoCredito(extratoDebitoVO.getIdCliente(),
					extratoDebitoVO.getIdImovel(), extratoDebitoVO.getIdPontoConsumo(), arrayPagamento);
			Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorCreditoDebito
					.consultarCreditosDebitos(filtro);

			if ((extratoDebitoVO.getIdPontoConsumo() != null) && (extratoDebitoVO.getIdPontoConsumo() > 0)) {

				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
						.obter(extratoDebitoVO.getIdPontoConsumo(), "cep", "imovel", "quadraFace");
				model.addAttribute("pontoConsumo", pontoConsumo);
			}

			if ((extratoDebitoVO.getIdImovel() != null) && (extratoDebitoVO.getIdImovel() > 0)) {
				Imovel imovel = (Imovel) controladorImovel.obter(extratoDebitoVO.getIdImovel());
				model.addAttribute("imovel", imovel);
			}

			if (Strings.isNullOrEmpty(extratoDebitoVO.getDataVencimento())) {
				extratoDebitoVO.setDataVencimento(
						Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));
			}

			if (extratoDebitoVO.getIdCliente() != null && extratoDebitoVO.getIdCliente() > 0) {
				Cliente cliente = controladorCliente.obterCliente(extratoDebitoVO.getIdCliente());
				model.addAttribute("nomeCompletoCliente", cliente.getNome());
				model.addAttribute(ID_CLIENTE, cliente.getChavePrimaria());
			} else if (listaFatura != null && !listaFatura.isEmpty()) {
				Cliente cliente = listaFatura.iterator().next().getCliente();
				model.addAttribute("nomeCompletoCliente", cliente.getNome());
				model.addAttribute(ID_CLIENTE, cliente.getChavePrimaria());
			}

			String idTipoNotaCredito = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO);
			String idTipoCredito = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR);
			model.addAttribute(VALOR_DEBITOS_SIMULADOS, valor);
			model.addAttribute("tipoCredito", idTipoCredito);
			model.addAttribute("tipoNotaCredito", idTipoNotaCredito);

			List<ExtratoDebitoFaturaVO> listaExtratoDebitoFaturaVO = new ArrayList<ExtratoDebitoFaturaVO>();

			TransformadorExtratoDebitoFaturaVO transformador = new TransformadorExtratoDebitoFaturaVO(
					Util.converterCampoStringParaData(ROTULO_DATA_VENCIMENTO, extratoDebitoVO.getDataVencimento(),
							Constantes.FORMATO_DATA_BR));

			if (listaFatura != null && !listaFatura.isEmpty()) {
				for (Fatura fatura : listaFatura) {
					listaExtratoDebitoFaturaVO.add(transformador.criarExtratoDebitoFaturaVO(fatura));
				}
			}

			request.getSession().setAttribute(LISTA_FATURA, listaExtratoDebitoFaturaVO);
			model.addAttribute(LISTA_FATURA, listaExtratoDebitoFaturaVO);
			model.addAttribute(LISTA_CREDITO_DEBITO, this.popularExtratoDebitoCreditoDebitoVO(listaCreditoDebito));
			model.addAttribute(EXTRATO_DEBITO_VO, extratoDebitoVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "exibirPesquisaExtratoDebitos";
		}

		return view;
	}

	/**
	 * 
	 * @param idCliente
	 *            {@link Long}
	 * @param idImovel
	 *            {@link Long}
	 * @param idPontoConsumo
	 *            {@link Long}
	 * @param arrayPagamento
	 *            {@link Long[]}
	 * @return Map {@link Map}
	 */
	private Map<String, Object> montafiltroListaDebitoCredito(Long idCliente, Long idImovel, Long idPontoConsumo,
			Long[] arrayPagamento) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("arrayPagamento", arrayPagamento);

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}
		if ((idImovel != null) && (idImovel > 0)) {
			filtro.put(ID_IMOVEL, idImovel);
		}
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			filtro.put(CHAVES_PONTO_CONSUMO, new Long[] { idPontoConsumo });
		}

		// Restrição de tarifa autorizada (Alçada)
		String statusAutorizado = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (statusAutorizado != null) {
			filtro.put("status", Long.valueOf(statusAutorizado));
		}

		return filtro;
	}

	private Map<String, Object> montaFiltroListafatura(Long idCliente, Long idImovel, Long idPontoConsumo,
			Long[] arrayPagamento) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("situacaoPagamento", arrayPagamento);
		filtro.put("situacaoValida", true);
		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			filtro.put(CHAVES_PONTO_CONSUMO, new Long[] { idPontoConsumo });
		} else if ((idImovel != null) && (idImovel > 0)) {
			Collection<PontoConsumo> listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(idImovel);
			filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
		}
		return filtro;
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de extrato de débitos anual.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaExtratoQuitacaoAnual")
	public String exibirPesquisaExtratoQuitacaoAnual(HttpServletRequest request, Model model){

		Collection<String> listaAnos = Util.listarAnos("20");
		((List<String>) listaAnos).remove(0);
		model.addAttribute("listaAnos", listaAnos);

		return "exibirPesquisaExtratoQuitacaoAnual";
	}

	/**
	 * Método responsável por gerar o extrato de débitos.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@RequestMapping("gerarExtratoDebitos")
	public String gerarExtratoDebitos(ExtratoDebitoVO extratoDebitoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException, IOException {

		Long idCliente = null;
		model.addAttribute("extratoDebitoVO", extratoDebitoVO);
		try {
			if ((extratoDebitoVO.getIdCliente() == null) || (extratoDebitoVO.getIdCliente() <= 0)) {
				Cliente cliente = controladorCliente.obterClientePorPontoConsumo(extratoDebitoVO.getIdPontoConsumo());
				idCliente = cliente.getChavePrimaria();
			} else {
				idCliente = extratoDebitoVO.getIdCliente();
			}

			DocumentoCobranca documentoCobranca = (DocumentoCobranca) controladorCobranca.criarDocumentoCobranca();
			if (!StringUtils.isEmpty(extratoDebitoVO.getDataVencimento())) {
				documentoCobranca.setDataVencimento(Util.converterCampoStringParaData("Data de Vencimento",
						extratoDebitoVO.getDataVencimento(), Constantes.FORMATO_DATA_BR));
			} else {
				documentoCobranca.setDataVencimento(Calendar.getInstance().getTime());
			}
			documentoCobranca.setDadosAuditoria(getDadosAuditoria(request));

			byte[] relatorioExtratoDebito = controladorCobranca.gerarExtratoDebito(extratoDebitoVO.getChavesFatura(),
					extratoDebitoVO.getChavesCreditoDebito(), idCliente, documentoCobranca);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(relatorioExtratoDebito.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorioCobranca.pdf");
			servletOutputStream.write(relatorioExtratoDebito, 0, relatorioExtratoDebito.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirExtratoDebitos";
	}

	/**
	 * Calcular valor notas debitos.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("calcularValorNotasDebitos")
	public String calcularValorNotasDebitos(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute("extratoDebitoVO", extratoDebitoVO);
		try {

			Date data = Util.converterCampoStringParaData(ROTULO_DATA_VENCIMENTO, extratoDebitoVO.getDataVencimento(),
					Constantes.FORMATO_DATA_BR);

			BigDecimal valor = controladorCobranca.calcularValorTotalDebitos(extratoDebitoVO.getChavesFatura(),
					extratoDebitoVO.getChavesCreditoDebito(), data);
			if (valor != null && !(valor.compareTo(BigDecimal.ZERO) == 0)) {

				String valorCalculado = Util.converterCampoValorDecimalParaString("Valor Calculado", valor,
						Constantes.LOCALE_PADRAO, 2);

				model.addAttribute(VALOR_DEBITOS_SIMULADOS, valorCalculado);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirExtratoDebitos(extratoDebitoVO, result, request, model);
	}

	/**
	 * Popular extrato debito credito debito vo.
	 * 
	 * @param listaCreditoDebito 
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private Collection<ExtratoDebitoCreditoDebitoVO> popularExtratoDebitoCreditoDebitoVO(
			Collection<CreditoDebitoARealizar> listaCreditoDebito) throws GGASException {

		List<ExtratoDebitoCreditoDebitoVO> listaCreditoDebitoVO = new LinkedList<ExtratoDebitoCreditoDebitoVO>();

		for (CreditoDebitoARealizar creditoDebito : listaCreditoDebito) {
			ExtratoDebitoCreditoDebitoVO extratoDebito = new ExtratoDebitoCreditoDebitoVO();

			extratoDebito.setIdCreditoDebito(creditoDebito.getChavePrimaria());

			Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
					.obterCDDetalhamentoPeloCDARealizar(creditoDebito.getChavePrimaria());
			// TODO verificar se pode pegar o
			// primeiro desta lista.
			if ((listaCDDetalhamento != null) && (!listaCDDetalhamento.isEmpty())) {
				CreditoDebitoDetalhamento creditoDebitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento)
						.get(0);
				extratoDebito.setTipoDocumento(
						creditoDebitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito().getDescricao());
				extratoDebito.setIdTipoDocumento(creditoDebitoDetalhamento.getLancamentoItemContabil()
						.getTipoCreditoDebito().getChavePrimaria());
			}
			if (creditoDebito.getCreditoDebitoNegociado().getRubrica() != null) {
				extratoDebito
						.setDescricaoRubrica(creditoDebito.getCreditoDebitoNegociado().getRubrica().getDescricao());
			}
			if (creditoDebito.getCreditoDebitoNegociado().getDataInicioCobranca() != null) {
				extratoDebito.setInicioCobranca(Util.converterDataParaStringSemHora(
						creditoDebito.getCreditoDebitoNegociado().getDataInicioCobranca(), Constantes.FORMATO_DATA_BR));
			}
			extratoDebito.setParcelasCobradas(
					String.valueOf(creditoDebito.getCreditoDebitoNegociado().getNumeroPrestacaoCobrada()));
			extratoDebito.setTotalParcelas(String.valueOf(creditoDebito.getNumeroPrestacao()));
			extratoDebito.setIndicadorAntecipacao(String.valueOf(creditoDebito.isIndicadorAntecipacao()));
			if (creditoDebito.getCreditoDebitoNegociado().getPontoConsumo() != null) {
				extratoDebito.setIdPontoConsumo(
						creditoDebito.getCreditoDebitoNegociado().getPontoConsumo().getChavePrimaria());
				extratoDebito.setIndicadorPontoConsumo(Boolean.TRUE);
			} else {
				extratoDebito.setIndicadorPontoConsumo(Boolean.FALSE);
			}
			BigDecimal valor = creditoDebito.getValor();
			if (creditoDebito.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
				valor = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebito);
			}
			extratoDebito.setValorTotal(Util.converterCampoCurrencyParaString(valor, Constantes.LOCALE_PADRAO));
			listaCreditoDebitoVO.add(extratoDebito);
		}

		return listaCreditoDebitoVO;
	}

	/**
	 * Método responsável por realizar o processamento das declarações de quitação
	 * anual de débitos.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("processarDeclaracaoQuitacaoAnualDebito")
	public String processarDeclaracaoQuitacaoAnualDebito(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, Model model) {

		Map<String, String> listaParametro = new HashMap<String, String>();
		try {
			listaParametro.put(ANO_GERACAO, extratoDebitoVO.getAnoGeracao());

			Operacao operacao = super.obterOperacao(request);

			Processo processo = (Processo) controladorProcesso.criar();
			processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
			processo.setHabilitado(true);
			processo.setOperacao(operacao);
			processo.setDescricao(operacao.getDescricao());
			processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
			processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
			processo.setDiaNaoUtil(true);
			processo.setUsuario(super.obterUsuario(request));
			if (processo.getUsuario() != null && processo.getUsuario().getFuncionario() != null) {
				Funcionario funcionario = processo.getUsuario().getFuncionario();
				processo.setUsuarioExecucao(funcionario.getNome());
				if (funcionario.getUnidadeOrganizacional() != null) {
					processo.setUnidadeOrganizacionalExecucao(
							funcionario.getUnidadeOrganizacional().getDescricao());
				}
			}
			processo.setParametros(listaParametro);
			processo.setDadosAuditoria(getDadosAuditoria(request));

			controladorProcesso.inserir(processo);

			mensagemSucesso(model, ControladorCobranca.SUCESSO_GERAR_DECLARACAO_QUITACAO_ANUAL);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaExtratoQuitacaoAnual(request, model);
	}

	/**
	 * Exibir logo marca banco.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return String {@link String}
	 * @throws NegocioException 
	 *             {@link Exception}
	 * @throws IOException 
	 */
	@RequestMapping("exibirLogoMarcaBanco")
	public String exibirLogoMarcaBanco(HttpServletRequest request, HttpServletResponse response) throws NegocioException, IOException {

		Long idBanco = Long.valueOf(request.getParameter("idBanco"));
		Banco banco = controladorCobranca.obterBanco(idBanco);

		byte[] logo = banco.getLogoBanco();

		if (logo != null) {

			try {

				ByteArrayInputStream in = new ByteArrayInputStream(logo);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				response.reset();
				response.setContentType("image/jpeg");

				while ((index = in.read(bytearray)) != -1) {
					response.getOutputStream().write(bytearray, 0, index);
				}
				response.flushBuffer();
				in.close();

			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			} finally {
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
		}

		return null;

	}
}
