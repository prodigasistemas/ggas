/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.extratodebito;

import java.util.Date;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.GGASException;

/**
 * Esta classe pode transformar uma {@link Fatura} em um
 * {@link ExtratoDebitoFaturaVO}. O proósito original desta classe é trabalhar
 * no nível de coleções, por exemplo:
 * 
 * <pre>
 * {@code
 * Collections2.transform(listaFatura,
 * 	new TransformadorExtratoDebitoFaturaVO(pagamento));}
 * </pre>
 */
public class TransformadorExtratoDebitoFaturaVO{

	private Date pagamento;

	/**
	 * Cria um novo transformador com a data de pagamento que será usada para
	 * preencher o saldo corrigido do {@link ExtratoDebitoFaturaVO}
	 *
	 * @param pagamento
	 *            a data de pagamento para o cálculo da correção de saldo
	 */
	public TransformadorExtratoDebitoFaturaVO(Date pagamento) {
		if (pagamento != null) {
			this.pagamento = (Date) pagamento.clone();
		} else {
			this.pagamento = null;
		}
	}

	public ExtratoDebitoFaturaVO criarExtratoDebitoFaturaVO(Fatura fatura) throws GGASException {
		ConstrutorExtratoDebitoFaturaVO construtor = new ConstrutorExtratoDebitoFaturaVO(fatura);
		
		construtor.preencherIdFatura().preencherCicloReferencia().preencherValor().preencherSaldo()
				.preencherDataEmissao().preencherDataVencimento().preencherTipoDocumento()
				.preencherIdTipoDocumento().preencherSituacaoPagamento().preencherIndicadorPontoConsumo()
				.preencherSaldoCorrigido(pagamento);
		
		if(fatura.getPontoConsumo()!=null){
			construtor.preencherIdPontoConsumo();
		}
		
		return construtor.construir();
	}
}
