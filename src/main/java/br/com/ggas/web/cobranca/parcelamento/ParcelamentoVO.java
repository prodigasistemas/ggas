package br.com.ggas.web.cobranca.parcelamento;

import java.io.Serializable;

/**
 * Classe responsável pela representação dos atributos necessários para 
 * realização das operações referentes ao Parcelamento.
 * @author esantana
 *
 */
public class ParcelamentoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7766584948908197291L;

	private Long chavePrimaria;
	
	private String descricaoPerfilParcelamento;
	
	private String dataInicial;
	
	private String dataFinal;
	
	private String dataParcelamentoInicial;
	
	private String dataParcelamentoFinal;
	
	private String chavesPrimarias;
	
	private Long status;
	
	private String isAutorizacao;
	
	private String indicadorPesquisa;
	
	private Boolean bloquearCobrancaFatura;
	
	private String cobrancaEmConta = "true";
	
	private Long[] chavesFatura;
	
	private Long[] chavesCreditoDebito;
	
	private String valorDebitos;
	
	private String valorCreditos;
	
	private Long idIndiceFinanceiro;
	
	private Long idPerfilParcelamento;
	
	private String valorTotalDebito;
	
	private String valorDebitoCredito;
	
	private String valorCorrigidoDebitoCredito;
	
	private String percentualDesconto;
	
	private String valorDesconto;
	
	private String valorTotalLiquido;

	private String numeroParcelas;
	
	private String vencimentoInicial = "true";
	
	private String dataPrimeiroVencimento;
	
	private String indicadorNotaPromissoria = "false";
	
	private String indicadorConfissaoDivida = "false";
	
	private String periodicidadeParcelas;
	
	private Long idCliente;
	
	private String nomeCompletoCliente;
	
	private String documentoFormatado;
	
	private String emailCliente;
	
	private String enderecoFormatadoCliente;
	
	private Long idImovel;
	
	private String nomeFantasiaImovel;
	
	private String matriculaImovel;
	
	private String numeroImovel;
	
	private String cidadeImovel;
	
	private Boolean condominio;
	
	private Long idPontoConsumo;
	
	private String descricaoPontoConsumo;
	
	private String enderecoPontoConsumo;
	
	private String complementoPontoConsumo;
	
	private String cepPontoConsumo;
	
	private String emailFiador;
	
	private String enderecoFiador;
	
	private String documentoFiador;
	
	private String nomeFiador;
	
	private Boolean hasPermissaoAutorizar;
	
	private Integer versao;
	
	
	public String getEmailFiador() {
		return emailFiador;
	}

	public void setEmailFiador(String emailFiador) {
		this.emailFiador = emailFiador;
	}

	public String getEnderecoFiador() {
		return enderecoFiador;
	}

	public void setEnderecoFiador(String enderecoFiador) {
		this.enderecoFiador = enderecoFiador;
	}

	public String getDocumentoFiador() {
		return documentoFiador;
	}

	public void setDocumentoFiador(String documentoFiador) {
		this.documentoFiador = documentoFiador;
	}

	public String getNomeFiador() {
		return nomeFiador;
	}

	public void setNomeFiador(String nomeFiador) {
		this.nomeFiador = nomeFiador;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getCidadeImovel() {
		return cidadeImovel;
	}

	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}

	public Boolean getCondominio() {
		return condominio;
	}

	public void setCondominio(Boolean condominio) {
		this.condominio = condominio;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getEnderecoPontoConsumo() {
		return enderecoPontoConsumo;
	}

	public void setEnderecoPontoConsumo(String enderecoPontoConsumo) {
		this.enderecoPontoConsumo = enderecoPontoConsumo;
	}

	public String getComplementoPontoConsumo() {
		return complementoPontoConsumo;
	}

	public void setComplementoPontoConsumo(String complementoPontoConsumo) {
		this.complementoPontoConsumo = complementoPontoConsumo;
	}

	public String getCepPontoConsumo() {
		return cepPontoConsumo;
	}

	public void setCepPontoConsumo(String cepPontoConsumo) {
		this.cepPontoConsumo = cepPontoConsumo;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}

	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente;
	}

	public String getDocumentoFormatado() {
		return documentoFormatado;
	}

	public void setDocumentoFormatado(String documentoFormatado) {
		this.documentoFormatado = documentoFormatado;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getEnderecoFormatadoCliente() {
		return enderecoFormatadoCliente;
	}

	public void setEnderecoFormatadoCliente(String enderecoFormatadoCliente) {
		this.enderecoFormatadoCliente = enderecoFormatadoCliente;
	}

	public String getValorDebitos() {
		return valorDebitos;
	}

	public void setValorDebitos(String valorDebitos) {
		this.valorDebitos = valorDebitos;
	}

	public String getValorCreditos() {
		return valorCreditos;
	}

	public void setValorCreditos(String valorCreditos) {
		this.valorCreditos = valorCreditos;
	}

	public Long getIdIndiceFinanceiro() {
		return idIndiceFinanceiro;
	}

	public void setIdIndiceFinanceiro(Long idIndiceFinanceiro) {
		this.idIndiceFinanceiro = idIndiceFinanceiro;
	}

	public Long getIdPerfilParcelamento() {
		return idPerfilParcelamento;
	}

	public void setIdPerfilParcelamento(Long idPerfilParcelamento) {
		this.idPerfilParcelamento = idPerfilParcelamento;
	}

	public String getValorTotalDebito() {
		return valorTotalDebito;
	}

	public void setValorTotalDebito(String valorTotalDebito) {
		this.valorTotalDebito = valorTotalDebito;
	}

	public String getValorDebitoCredito() {
		return valorDebitoCredito;
	}

	public void setValorDebitoCredito(String valorDebitoCredito) {
		this.valorDebitoCredito = valorDebitoCredito;
	}

	public String getValorCorrigidoDebitoCredito() {
		return valorCorrigidoDebitoCredito;
	}

	public void setValorCorrigidoDebitoCredito(String valorCorrigidoDebitoCredito) {
		this.valorCorrigidoDebitoCredito = valorCorrigidoDebitoCredito;
	}

	public String getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(String percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public String getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(String valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getValorTotalLiquido() {
		return valorTotalLiquido;
	}

	public void setValorTotalLiquido(String valorTotalLiquido) {
		this.valorTotalLiquido = valorTotalLiquido;
	}

	public String getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(String numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

	public String getVencimentoInicial() {
		return vencimentoInicial;
	}

	public void setVencimentoInicial(String vencimentoInicial) {
		this.vencimentoInicial = vencimentoInicial;
	}

	public String getDataPrimeiroVencimento() {
		return dataPrimeiroVencimento;
	}

	public void setDataPrimeiroVencimento(String dataPrimeiroVencimento) {
		this.dataPrimeiroVencimento = dataPrimeiroVencimento;
	}

	public String getIndicadorNotaPromissoria() {
		return indicadorNotaPromissoria;
	}

	public void setIndicadorNotaPromissoria(String indicadorNotaPromissoria) {
		this.indicadorNotaPromissoria = indicadorNotaPromissoria;
	}

	public String getIndicadorConfissaoDivida() {
		return indicadorConfissaoDivida;
	}

	public void setIndicadorConfissaoDivida(String indicadorConfissaoDivida) {
		this.indicadorConfissaoDivida = indicadorConfissaoDivida;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getDataParcelamentoInicial() {
		return dataParcelamentoInicial;
	}

	public void setDataParcelamentoInicial(String dataParcelamentoInicial) {
		this.dataParcelamentoInicial = dataParcelamentoInicial;
	}

	public String getDataParcelamentoFinal() {
		return dataParcelamentoFinal;
	}

	public void setDataParcelamentoFinal(String dataParcelamentoFinal) {
		this.dataParcelamentoFinal = dataParcelamentoFinal;
	}

	public String getChavesPrimarias() {
		return chavesPrimarias;
	}

	public void setChavesPrimarias(String chavesPrimarias) {
		this.chavesPrimarias = chavesPrimarias;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getIsAutorizacao() {
		return isAutorizacao;
	}

	public void setIsAutorizacao(String isAutorizacao) {
		this.isAutorizacao = isAutorizacao;
	}

	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

	public Boolean getBloquearCobrancaFatura() {
		return bloquearCobrancaFatura;
	}

	public void setBloquearCobrancaFatura(Boolean bloquearCobrancaFatura) {
		this.bloquearCobrancaFatura = bloquearCobrancaFatura;
	}

	public String getCobrancaEmConta() {
		return cobrancaEmConta;
	}

	public void setCobrancaEmConta(String cobrancaEmConta) {
		this.cobrancaEmConta = cobrancaEmConta;
	}

	public Long[] getChavesFatura() {
		Long[] retorno = null;
		if(this.chavesFatura != null) {
			retorno = this.chavesFatura.clone();
		}
		return retorno;
	}

	public void setChavesFatura(Long[] chavesFatura) {
		if(chavesFatura != null) {
			this.chavesFatura = chavesFatura.clone();
		}
	}

	public Long[] getChavesCreditoDebito() {
		Long[] retorno = null;
		if(this.chavesCreditoDebito != null) {
			retorno = this.chavesCreditoDebito.clone();
		}
		return retorno;
	}

	public void setChavesCreditoDebito(Long[] chavesCreditoDebito) {
		if(chavesCreditoDebito != null) {
			this.chavesCreditoDebito = chavesCreditoDebito.clone();
		}
	}

	public String getPeriodicidadeParcelas() {
		return periodicidadeParcelas;
	}

	public void setPeriodicidadeParcelas(String periodicidadeParcelas) {
		this.periodicidadeParcelas = periodicidadeParcelas;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public String getDescricaoPerfilParcelamento() {
		return descricaoPerfilParcelamento;
	}

	public void setDescricaoPerfilParcelamento(String descricaoPerfilParcelamento) {
		this.descricaoPerfilParcelamento = descricaoPerfilParcelamento;
	}

	public Boolean getHasPermissaoAutorizar() {
		return hasPermissaoAutorizar;
	}

	public void setHasPermissaoAutorizar(Boolean hasPermissaoAutorizar) {
		this.hasPermissaoAutorizar = hasPermissaoAutorizar;
	}

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	
	
	
	
}
