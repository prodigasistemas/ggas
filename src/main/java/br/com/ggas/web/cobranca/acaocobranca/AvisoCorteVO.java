/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/12/2013 17:04:58
 @author wcosta
 */

package br.com.ggas.web.cobranca.acaocobranca;

import java.io.Serializable;
import java.util.List;

public class AvisoCorteVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4832397688374301238L;

	private String nome;

	private String endereco;

	private String bairro;

	private String cidade;

	private String codigo;

	private String codigoUnico;

	private String pontoEntrega;

	private String cep;

	private String codigoContrato;

	private String tipoResidencia;

	private String data;

	private String municipioUf;

	private List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturaVencida;

	private String cepCidadeEstado;
	
	private String enderecoPontoConsumoRelatorioCorteSergas;

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getPontoEntrega() {

		return pontoEntrega;
	}

	public void setPontoEntrega(String pontoEntrega) {

		this.pontoEntrega = pontoEntrega;
	}

	public List<SubRelatorioFaturasVencidasCobrancaVO> getColecaoFaturaVencida() {

		return colecaoFaturaVencida;
	}

	public void setColecaoFaturaVencida(List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturaVencida) {

		this.colecaoFaturaVencida = colecaoFaturaVencida;
	}

	public String getCodigoUnico() {

		return codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {

		this.codigoUnico = codigoUnico;
	}

	public String getCodigoContrato() {

		return codigoContrato;
	}

	public void setCodigoContrato(String codigoContrato) {

		this.codigoContrato = codigoContrato;
	}

	public String getTipoResidencia() {

		return tipoResidencia;
	}

	public void setTipoResidencia(String tipoResidencia) {

		this.tipoResidencia = tipoResidencia;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	public String getMunicipioUf() {

		return municipioUf;
	}

	public void setMunicipioUf(String municipioUf) {

		this.municipioUf = municipioUf;
	}

	public String getCepCidadeEstado() {

		return cepCidadeEstado;
	}

	public void setCepCidadeEstado(String cepCidadeEstado) {

		this.cepCidadeEstado = cepCidadeEstado;
	}

	public String getEnderecoPontoConsumoRelatorioCorteSergas() {
		return enderecoPontoConsumoRelatorioCorteSergas;
	}

	public void setEnderecoPontoConsumoRelatorioCorteSergas(String enderecoPontoConsumoRelatorioCorteSergas) {
		this.enderecoPontoConsumoRelatorioCorteSergas = enderecoPontoConsumoRelatorioCorteSergas;
	}

}
