package br.com.ggas.web.cobranca.acaocobranca;

/**
 * Classe responsável pela representação dos dados de uma ação de cobrança.
 * 
 * @author orube
 *
 */
public class AcaoCobrancaVO {

	private Long chavePrimaria;

	private Long[] chavesPrimarias;

	private Integer versao;

	private String descricao;

	private Long tipoNumeroDiasValidade;

	private Long tipoNumeroDiasVencimento;

	private String numeroDiasValidadeAcao;

	private String numeroDiasVencimento;

	private String gerarOrdemServico;

	private String consideraDebitosVencer;

	private String bloquearEmissaoDocumentosPagaveis;

	private String descricaoTipoDocumento;

	private Long tipoDocumentoGerado;

	private String descricaoTipoNumeroDiasValidade;

	private String descricaoTipoNumeroDiasVencimento;

	private Long acaoPredecessora;

	private String descricaoAcao;

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getTipoNumeroDiasValidade() {
		return tipoNumeroDiasValidade;
	}

	public void setTipoNumeroDiasValidade(Long tipoNumeroDiasValidade) {
		this.tipoNumeroDiasValidade = tipoNumeroDiasValidade;
	}

	public Long getTipoNumeroDiasVencimento() {
		return tipoNumeroDiasVencimento;
	}

	public void setTipoNumeroDiasVencimento(Long tipoNumeroDiasVencimento) {
		this.tipoNumeroDiasVencimento = tipoNumeroDiasVencimento;
	}

	public String getNumeroDiasValidadeAcao() {
		return numeroDiasValidadeAcao;
	}

	public void setNumeroDiasValidadeAcao(String numeroDiasValidadeAcao) {
		this.numeroDiasValidadeAcao = numeroDiasValidadeAcao;
	}

	public String getNumeroDiasVencimento() {
		return numeroDiasVencimento;
	}

	public void setNumeroDiasVencimento(String numeroDiasVencimento) {
		this.numeroDiasVencimento = numeroDiasVencimento;
	}

	public String getGerarOrdemServico() {
		return gerarOrdemServico;
	}

	public void setGerarOrdemServico(String gerarOrdemServico) {
		this.gerarOrdemServico = gerarOrdemServico;
	}

	public String getConsideraDebitosVencer() {
		return consideraDebitosVencer;
	}

	public void setConsideraDebitosVencer(String consideraDebitosVencer) {
		this.consideraDebitosVencer = consideraDebitosVencer;
	}

	public String getBloquearEmissaoDocumentosPagaveis() {
		return bloquearEmissaoDocumentosPagaveis;
	}

	public void setBloquearEmissaoDocumentosPagaveis(String bloquearEmissaoDocumentosPagaveis) {
		this.bloquearEmissaoDocumentosPagaveis = bloquearEmissaoDocumentosPagaveis;
	}

	public String getDescricaoTipoDocumento() {
		return descricaoTipoDocumento;
	}

	public void setDescricaoTipoDocumento(String descricaoTipoDocumento) {
		this.descricaoTipoDocumento = descricaoTipoDocumento;
	}

	public Long getTipoDocumentoGerado() {
		return tipoDocumentoGerado;
	}

	public void setTipoDocumentoGerado(Long tipoDocumentoGerado) {
		this.tipoDocumentoGerado = tipoDocumentoGerado;
	}

	public String getDescricaoTipoNumeroDiasValidade() {
		return descricaoTipoNumeroDiasValidade;
	}

	public void setDescricaoTipoNumeroDiasValidade(String descricaoTipoNumeroDiasValidade) {
		this.descricaoTipoNumeroDiasValidade = descricaoTipoNumeroDiasValidade;
	}

	public String getDescricaoTipoNumeroDiasVencimento() {
		return descricaoTipoNumeroDiasVencimento;
	}

	public void setDescricaoTipoNumeroDiasVencimento(String descricaoTipoNumeroDiasVencimento) {
		this.descricaoTipoNumeroDiasVencimento = descricaoTipoNumeroDiasVencimento;
	}

	public Long getAcaoPredecessora() {
		return acaoPredecessora;
	}

	public void setAcaoPredecessora(Long acaoPredecessora) {
		this.acaoPredecessora = acaoPredecessora;
	}

	public String getDescricaoAcao() {
		return descricaoAcao;
	}

	public void setDescricaoAcao(String descricaoAcao) {
		this.descricaoAcao = descricaoAcao;
	}

	public Long[] getChavesPrimarias() {
		Long[] chaves = null;
		if (this.chavesPrimarias != null) {
			chaves = this.chavesPrimarias.clone();
		}
		return chaves;
	}

	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

}
