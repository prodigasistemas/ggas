/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.extratodebito;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;

/**
 * Esta classe é capaz de construir em etapas um {@link ExtratoDebitoFaturaVO}.
 * A maioria dos métodos de preenchimento usa dados extraídos da fatura passada
 * como argumento no construtor. As seguintes informações da fatura são úteis a
 * esta classe:
 * <ul>
 * <li>chave primária</li>
 * <li>ciclo de referência</li>
 * <li>valor total</li>
 * <li>saldo conciliado</li>
 * <li>data de emissão</li>
 * <li>data de vencimento</li>
 * <li>descrição do tipo de documento</li>
 * <li>chave primária do tipo de documento</li>
 * <li>descrição da situação de pagamento</li>
 * <li>chave primária do ponto de consumo</li>
 * </ul>
 * Não é obrigatório que a fatura em questão possua todas as informações, porém
 * nenhuma garantia é oferecida caso algum método de preenchimento seja invocado
 * com a informação faltando.
 */
public class ConstrutorExtratoDebitoFaturaVO {

	private Fachada fachada;
	private Fatura fatura;

	private static final String ROTULO_SITUACAO_DE_PAGAMENTO = "Situação de Pagamento";

	private Long idFatura;
	private String cicloReferencia;
	private String valor;
	private String saldo;
	private String dataEmissao;
	private String dataVencimento;
	private String tipoDocumento;
	private long idTipoDocumento;
	private String situacaoPagamento;
	private long idPontoConsumo;
	private Boolean indicadorPontoConsumo;
	private String saldoCorrigido;

	/**
	 * Constrói uma instância desta classe passando uma fatura contendo
	 * informações necessárias.
	 *
	 * @param fatura
	 *            a fatura contendo dados
	 */
	public ConstrutorExtratoDebitoFaturaVO(Fatura fatura) {
		this.fatura = fatura;
		fachada = Fachada.getInstancia();
	}

	/**
	 * Preenche a chave primária da fatura.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherIdFatura() {
		idFatura = fatura.getChavePrimaria();
		return this;
	}

	/**
	 * Preenche o ciclo de referência formatado.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherCicloReferencia() {
		cicloReferencia = fatura.getCicloReferenciaFormatado();
		return this;
	}

	/**
	 * Preenche o valor total da fatura.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherValor() {
		valor = Util.converterCampoCurrencyParaString(fatura.getValorTotal(), Constantes.LOCALE_PADRAO);
		return this;
	}

	/**
	 * Preenche o saldo.
	 *
	 * @return esta instância
	 * @throws GGASException
	 *             em caso de erro na consulta pelo recebimento
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherSaldo() throws GGASException {
		BigDecimal valorSaldo = fatura.getValorSaldoConciliado();
		BigDecimal valorSubtraido = subtrairRecebimentos();
		if(valorSubtraido!=null){
			valorSaldo = valorSubtraido;
		}
		saldo = Util.converterCampoCurrencyParaString(valorSaldo, Constantes.LOCALE_PADRAO);
		return this;
	}

	/**
	 * Preenche a data de emissão.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherDataEmissao() {
		dataEmissao = Util.converterDataParaStringSemHora(fatura.getDataEmissao(), Constantes.FORMATO_DATA_BR);
		return this;
	}

	/**
	 * Preenche a data de vencimento.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherDataVencimento() {
		dataVencimento = Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR);
		return this;
	}

	/**
	 * Preenche a descrição do tipo de documento da fatura.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherTipoDocumento() {
		tipoDocumento = fatura.getTipoDocumento().getDescricao();
		return this;
	}

	/**
	 * Preenche a chave primária do tipo de documento da fatura.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherIdTipoDocumento() {
		idTipoDocumento = fatura.getTipoDocumento().getChavePrimaria();
		return this;
	}

	/**
	 * Preenche a situação de pagamento.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherSituacaoPagamento() {
		situacaoPagamento = fatura.getSituacaoPagamento().getDescricao();
		return this;
	}

	/**
	 * Preenche a chave primária do ponto de consumo.
	 *
	 * @return esta instância
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherIdPontoConsumo() {
		idPontoConsumo = fatura.getPontoConsumo().getChavePrimaria();
		return this;
	}

	/**
	 * Preenche o indicador de existência de ponto de consumo.
	 *
	 * @return esta instância
	 * @throws NegocioException
	 *             em caso de erro na consulta pelos pontos de consumo
	 *             associados a fatura
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherIndicadorPontoConsumo() throws NegocioException {
		Long[] chavesPontoConsumo = fachada.obterChavesPontosConsumoFatura(fatura.getChavePrimaria());
		indicadorPontoConsumo = Boolean.valueOf(chavesPontoConsumo != null && chavesPontoConsumo.length > 0);
		return this;
	}

	/**
	 * Preenche o saldo corrigido da fatura.
	 *
	 * @param pagamento
	 *            a data de pagamento para correção do saldo
	 * @return esta instância
	 * @throws GGASException
	 *             em caso de erro ao corrigir o saldo da fatura
	 */
	public ConstrutorExtratoDebitoFaturaVO preencherSaldoCorrigido(Date pagamento) throws GGASException {
		BigDecimal saldoFatura = fachada.calcularSaldoFatura(fatura, pagamento, true, true).getSecond();
		saldoCorrigido = Util.converterCampoCurrencyParaString(saldoFatura, Constantes.LOCALE_PADRAO);
		return this;
	}

	/**
	 * Gera um novo {@link ExtratoDebitoFaturaVO} consolidando todos os campos
	 * preenchidos usando este objeto.
	 *
	 * @return esta instância
	 */
	public ExtratoDebitoFaturaVO construir() {
		ExtratoDebitoFaturaVO instancia = new ExtratoDebitoFaturaVO();
		instancia.setIdFatura(idFatura);
		instancia.setCicloReferencia(cicloReferencia);
		instancia.setValor(valor);
		instancia.setSaldo(saldo);
		instancia.setDataEmissao(dataEmissao);
		instancia.setDataVencimento(dataVencimento);
		instancia.setTipoDocumento(tipoDocumento);
		instancia.setIdTipoDocumento(idTipoDocumento);
		instancia.setSituacaoPagamento(situacaoPagamento);
		instancia.setIdPontoConsumo(idPontoConsumo);
		instancia.setIndicadorPontoConsumo(indicadorPontoConsumo);
		instancia.setSaldoCorrigido(saldoCorrigido);
		return instancia;
	}

	private BigDecimal subtrairRecebimentos() throws GGASException {
		String codigoSituacaoPagamentoParcialmentePago = fachada
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
		Long idFaturaParcialmentePaga = Util.converterCampoStringParaValorLong(ROTULO_SITUACAO_DE_PAGAMENTO,
				codigoSituacaoPagamentoParcialmentePago);
		if ((idFaturaParcialmentePaga != null) && (fatura.getSituacaoPagamento() != null)
				&& (idFaturaParcialmentePaga == fatura.getSituacaoPagamento().getChavePrimaria())) {
			BigDecimal valorRecebimento = fachada.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
			if (valorRecebimento != null) {
				return fatura.getValorSaldoConciliado().subtract(valorRecebimento);
			}
		}
		return null;
	}

}
