/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 10:03:51
 @author wcosta
 */

package br.com.ggas.web.cobranca.negativacao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.batch.impl.ProcessoVO;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.impl.EntregaDocumentoVO;
import br.com.ggas.cobranca.negativacao.AgenteNegativador;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoCliente;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoRota;
import br.com.ggas.cobranca.negativacao.ControladorNegativacao;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.cobranca.negativacao.dominio.ClienteNegativacaoVO;
import br.com.ggas.cobranca.negativacao.dominio.ComandoNegativacaoVO;
import br.com.ggas.cobranca.negativacao.impl.ComandoNegativacaoImpl;
import br.com.ggas.cobranca.negativacao.impl.ComandoNegativacaoRotaImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.BigDecimalUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.batch.ProcessoAction;

/**
 * Classe responsável pelas telas relacionadas a negativação
 *
 */
@Controller
public class NegativacaoAction extends GenericAction {

	private static final String INDEX_LISTA = "indexLista";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String CHAVE_PRIMARIA = "chavePrimariaAtualizar";

	@Autowired
	private ControladorCliente controladorCliente;

	private static final String LISTA_GRUPOS_FATURAMENTO = "listaGruposFaturamento";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";

	private static final String MSG_SUCESSO_PROCESSO_INSERIDO = "Processo para Notificação da Negativação iniciado com sucesso";

	private static final String MSG_SUCESSO_SIMULACAO = "Simulação realizada com sucesso";

	private static final String COMANDO_NEGATIVACAO = "Comando de Negativação";

	private static final String COMANDO_NEGATIVACAO_CLIENTE = "Comando de Negativação do Cliente";

	private static final String FATURA_CLIENTE_FORM = "faturaClienteForm";

	private static final String MSG_SUCESSO_ALTERAR_COMANDO = "Nome do Comando Alterado com Sucesso";

	private static final String MSG_ERRO_ALTERAR_COMANDO = "Novo nome do comando vazio";

	private static final String MSG_ERRO_NOME_COMANDO = "Nome do Comando já está sendo utilizado";

	private static final String MSG_ERRO_DATA_PREENCHIDA = "Data não preenchida";

	private static final String MSG_ERRO_COMANDO_EXECUTADO = "Só é possível alterar o Nome de comandos ja executados";

	/**
	 * Controlador negativação
	 */
	@Autowired
	private ControladorNegativacao controladorNegativacao;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorModeloContrato controladorModeloContrato;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorAcesso controladorAcesso;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ProcessoAction processoAction;

	/**
	 * Controlador parametros sistema
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * Método responsável por exibir tela de inclusao de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 */
	@RequestMapping("exibirInclusaoComandoNegativacao")
	public String exibirInclusaoComandoNegativacao(ComandoNegativacaoImpl comandoNegativacaoImpl,
			HttpServletRequest request, Model model) throws NegocioException {

		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addAttribute("listaSituacaoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
		model.addAttribute("listarAgenteNegativador", controladorNegativacao.listarAgenteNegativador());
		model.addAttribute("comandoNegativacao", comandoNegativacaoImpl);
		model.addAttribute("dataVencimentoInicial", comandoNegativacaoImpl.getDataVencimentoInicial() == null ? ""
				: DataUtil.converterDataParaString(comandoNegativacaoImpl.getDataVencimentoInicial()));
		model.addAttribute("dataVencimentoFinal", comandoNegativacaoImpl.getDataVencimentoFinal() == null ? ""
				: DataUtil.converterDataParaString(comandoNegativacaoImpl.getDataVencimentoFinal()));
		model.addAttribute("dataPrevistaExecucao", comandoNegativacaoImpl.getDataPrevistaExecucao() == null ? ""
				: DataUtil.converterDataParaString(comandoNegativacaoImpl.getDataPrevistaExecucao()));

		return "exibirInclusaoComandoNegativacao";
	}

	/**
	 * Método responsável por exibir tela de pesquisa de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 */
	@RequestMapping("exibirPesquisaComandoNegativacao")
	public String exibirPesquisaComandoNegativacao(ComandoNegativacaoVO comandoNegativacaoVO,
			HttpServletRequest request, Model model) throws NegocioException {

		request.getSession().removeAttribute("listaCliente");
		request.getSession().setAttribute("isPermiteCaracteresEspeciais", "1");

		return "exibirPesquisaComandoNegativacao";
	}

	/**
	 * Método responsável por exibir tela de detalhamento de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 */
	@RequestMapping("exibirDetalhamentoComandoNegativacao")
	public String exibirDetalhamentoComandoNegativacao(ComandoNegativacaoImpl comandoNegativacaoImpl,
			@RequestParam(CHAVE_PRIMARIA) Long chaveComando, HttpServletRequest request, Model model)
			throws NegocioException {

		ComandoNegativacao comando = controladorNegativacao.obterComandoNegativacao(chaveComando);
		
		Collection<ComandoNegativacaoRota> listaComandoRota = controladorNegativacao
				.obterComandoNegativacaoRota(comando.getChavePrimaria());
		if (listaComandoRota != null && !listaComandoRota.isEmpty()) {
			StringBuilder chavesRotas = new StringBuilder();
			for (ComandoNegativacaoRota comandoRota : listaComandoRota) {
				chavesRotas.append(comandoRota.getRota().getChavePrimaria());
				chavesRotas.append(",");
			}
			model.addAttribute("chavesRotasSelecionadas", chavesRotas.toString());
		}

		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addAttribute("listaSituacaoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
		model.addAttribute("listarAgenteNegativador", controladorNegativacao.listarAgenteNegativador());
		model.addAttribute("comandoNegativacao", comando);
		model.addAttribute("dataVencimentoInicial", comando.getDataVencimentoInicial() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataVencimentoInicial()));
		model.addAttribute("dataVencimentoFinal", comando.getDataVencimentoFinal() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataVencimentoFinal()));
		model.addAttribute("dataPrevistaExecucao", comando.getDataPrevistaExecucao() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataPrevistaExecucao()));

		if (comando.getIndicadorComandoCliente()) {
			request.getSession().setAttribute("listaCliente",
					controladorNegativacao.popularListaClienteVOComandoCliente(chaveComando));
		}

		request.getSession().setAttribute("isPermiteCaracteresEspeciais", "1");

		return "exibirDetalhamentoComandoNegativacao";
	}

	/**
	 * Método responsável por exibir tela de alteração de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 * @throws ParseException
	 */
	@RequestMapping("exibirAlterarComandoNegativacao")
	public String exibirAlterarComandoNegativacao(ComandoNegativacaoVO comandoNegativacaoVO, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chaveComando, HttpServletRequest request, Model model)
			throws NegocioException, ParseException {

		Boolean comandoExecutado = controladorNegativacao.verificaSeComandoFoiExecutado(chaveComando);
		Boolean erroNomeComando = (Boolean) request.getAttribute("erroNome");
		if (!comandoExecutado) {
			if (erroNomeComando == null) {
				mensagemAlerta(model, MSG_ERRO_COMANDO_EXECUTADO);
			}
			model.addAttribute("atualizarSomenteNome", 1);
		}
		
		ComandoNegativacao comando = (ComandoNegativacao) request.getAttribute("comandoAlterar");
		if (comando == null) {
			comando = controladorNegativacao.obterComandoNegativacao(chaveComando);
		}
		
		Collection<ComandoNegativacaoRota> listaComandoRota = controladorNegativacao
				.obterComandoNegativacaoRota(comando.getChavePrimaria());
		if (listaComandoRota != null && !listaComandoRota.isEmpty()) {
			StringBuilder chavesRotas = new StringBuilder();
			for (ComandoNegativacaoRota comandoRota : listaComandoRota) {
				chavesRotas.append(comandoRota.getRota().getChavePrimaria());
				chavesRotas.append(",");
			}
			model.addAttribute("chavesRotasSelecionadas", chavesRotas.toString());
		}

		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addAttribute("listaSituacaoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
		model.addAttribute("listarAgenteNegativador", controladorNegativacao.listarAgenteNegativador());
		model.addAttribute("comandoNegativacao", comando);
		model.addAttribute("dataVencimentoInicial", comando.getDataVencimentoInicial() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataVencimentoInicial()));
		model.addAttribute("dataVencimentoFinal", comando.getDataVencimentoFinal() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataVencimentoFinal()));
		model.addAttribute("dataPrevistaExecucao", comando.getDataPrevistaExecucao() == null ? ""
				: DataUtil.converterDataParaString(comando.getDataPrevistaExecucao()));

		request.getSession().setAttribute("isPermiteCaracteresEspeciais", "1");

		if (comando.getIndicadorComandoCliente()) {
			request.getSession().setAttribute("listaCliente",
					controladorNegativacao.popularListaClienteVOComandoCliente(chaveComando));
			model.addAttribute("comandoCliente", "1");
		}
		return "exibirAlterarComandoNegativacao";
	}

	/**
	 * Método responsável por pesquisar de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 * @throws ParseException
	 */
	@RequestMapping("pesquisarComandoNegativacao")
	public String pesquisarComandoNegativacao(ComandoNegativacaoVO comandoNegativacaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws NegocioException, ParseException {

		Collection<ComandoNegativacao> listaComando = controladorNegativacao.listarComandos(comandoNegativacaoVO);
		request.getSession().removeAttribute("listaCliente");
		model.addAttribute("listaComandoNegativacao", listaComando);

		return exibirPesquisaComandoNegativacao(comandoNegativacaoVO, request, model);
	}

	/**
	 * Método responsável por exibir tela de inclusao de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("executarComandoNegativacao")
	public String executarComandoNegativacao(ComandoNegativacaoVO comandoNegativacaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		Processo processo = null;
		ProcessoVO processoVo = new ProcessoVO();
		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_OPERACAO_EMITIR_NOTIFICACAO_NEGATIVACAO);
		Operacao operacao = controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor()));

		ComandoNegativacao comando = controladorNegativacao
				.obterComandoNegativacao(Long.valueOf(comandoNegativacaoVO.getChaveComando()));

		processo = controladorNegativacao.inserirProcesso(obterUsuario(request), getDadosAuditoria(request), comando);
		processoVo.setModulo(operacao.getModulo().getChavePrimaria());
		processo.setSituacao(-1);
		mensagemSucesso(model, MSG_SUCESSO_PROCESSO_INSERIDO);

		return processoAction.pesquisarProcesso((ProcessoImpl) processo, result, processoVo, result, request, model);

	}

	@RequestMapping("incluirClienteGrid")
	public ModelAndView incluirClienteGrid(HttpServletRequest request, Model model)
			throws NumberFormatException, NegocioException {
		ModelAndView gridCliente = new ModelAndView("gridClientes");

		Collection<ClienteVO> listaCliente = (Collection<ClienteVO>) request.getSession().getAttribute("listaCliente");

		if (listaCliente == null || listaCliente.isEmpty()) {
			listaCliente = new ArrayList<>();
		}

		Cliente cliente = (Cliente) controladorCliente.obter(Long.valueOf(request.getParameter("idCliente")));

		boolean contemCliente = listaCliente.stream()
				.filter(o -> o.getCliente().getChavePrimaria() == cliente.getChavePrimaria()).findFirst().isPresent();

		if (cliente != null && !contemCliente) {
			Integer quantidadeFaturasVencidas = 0;
			BigDecimal valorTotalFaturasVencidas = BigDecimal.ZERO;

			ClienteVO clienteVO = new ClienteVO();
			clienteVO.setCliente(cliente);

			Collection<Fatura> faturasVencidas = controladorFatura
					.consultarFaturasVencidasCliente(cliente.getChavePrimaria());

			if (faturasVencidas != null && !faturasVencidas.isEmpty()) {

				for (Fatura fatura : faturasVencidas) {
					quantidadeFaturasVencidas = quantidadeFaturasVencidas + 1;
					valorTotalFaturasVencidas = valorTotalFaturasVencidas.add(fatura.getValorTotal());
				}
			}

			clienteVO.setQuantidadeFaturasVencidas(quantidadeFaturasVencidas);
			clienteVO.setValorTotalFaturasVencidas(valorTotalFaturasVencidas);

			listaCliente.add(clienteVO);

		}

		request.getSession().setAttribute("listaCliente", listaCliente);

		gridCliente.addObject("listaCliente", listaCliente);

		return gridCliente;
	}

	@RequestMapping("removerClientesGrid")
	public ModelAndView removerClientesGrid(HttpServletRequest request, Model model) {
		ModelAndView gridCliente = new ModelAndView("gridClientes");

		Collection<ClienteVO> listaCliente = (Collection<ClienteVO>) request.getSession().getAttribute("listaCliente");

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		if (listaCliente != null && indexLista != null) {
			ClienteVO cliente = ((ArrayList<ClienteVO>) listaCliente).get(indexLista.intValue());
			listaCliente.remove(cliente);
		}

		request.getSession().setAttribute("listaCliente", listaCliente);

		gridCliente.addObject("listaCliente", listaCliente);

		return gridCliente;
	}

	/**
	 * Incluir chamado.
	 *
	 * @param chamado the negativação
	 * @param result  the result
	 * @param session the session
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("incluirComandoNegativacao")
	public String incluirComandoNegativacao(
			@RequestParam(value = "rotasSelecionadas", required = false) Long[] rotasSelecionadas,
			@ModelAttribute("ComandoNegativacaoImpl") ComandoNegativacaoImpl comandoNegativacao, BindingResult result,
			HttpSession session, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> erros = comandoNegativacao.validarDados();
		Collection<ComandoNegativacao> listaComando = new HashSet<>();

		String tipoCliente = request.getParameter("tipoComando");

		if ("cliente".equals(tipoCliente)) {
			comandoNegativacao.setIndicadorComandoCliente(true);
		}
		try {

			popularComandoNegativacao(request, comandoNegativacao);

			model.addAttribute("valorInicialDebitos", request.getParameter("valorInicialDebitos"));
			model.addAttribute("valorFinalDebitos", request.getParameter("valorFinalDebitos"));
			
			StringBuilder chavesRotas = new StringBuilder();
			
			if(rotasSelecionadas != null && rotasSelecionadas.length > 0) {
				for (Long rotaSelecionada : rotasSelecionadas) {
					chavesRotas.append(rotaSelecionada);
					chavesRotas.append(",");
				}
				model.addAttribute("chavesRotasSelecionadas", chavesRotas.toString());
			}


			try {
				if (!erros.isEmpty()) {
					mensagemErro(model, new NegocioException(erros));
				}

				if (controladorNegativacao.obterComandoNegativacaoPeloNomeEChave(comandoNegativacao.getNomeComando(),
						comandoNegativacao.getChavePrimaria()) != null) {
					throw new NegocioException(MSG_ERRO_NOME_COMANDO);
				}
				// Inserir e Lançar mensagem de sucesso
				long chavePrimariaComando = controladorNegativacao.inserir(comandoNegativacao);
				ComandoNegativacao comandoNegativacaoInserido = controladorNegativacao
						.obterComandoNegativacao(chavePrimariaComando);

				if (comandoNegativacao.getGrupoFaturamento() != null) {
					if (rotasSelecionadas != null && rotasSelecionadas.length > 0) {
						for (Long rotaSelecionada : rotasSelecionadas) {
							ComandoNegativacaoRota comandoNegativacaoRota = (ComandoNegativacaoRota) controladorNegativacao
									.criarComandoNegativacaoRota();
							comandoNegativacaoRota.setComandoNegativacao(comandoNegativacaoInserido);
							comandoNegativacaoRota.setRota((Rota) controladorRota.obter(rotaSelecionada));

							controladorNegativacao.inserir(comandoNegativacaoRota);
						}
					}

				}

				if ("cliente".equals(tipoCliente)) {
					Collection<ClienteVO> listaCliente = (Collection<ClienteVO>) request.getSession()
							.getAttribute("listaCliente");

					if (listaCliente != null && !listaCliente.isEmpty()) {

						for (ClienteVO clienteVO : listaCliente) {

							Cliente cliente = controladorCliente
									.obterCliente(clienteVO.getCliente().getChavePrimaria());

							ComandoNegativacaoCliente comandoNegativacaoCliente = (ComandoNegativacaoCliente) controladorNegativacao
									.criarComandoNegativacaoCliente();

							comandoNegativacaoCliente.setCliente(cliente);
							comandoNegativacaoCliente.setComando(comandoNegativacaoInserido);
							controladorNegativacao.inserir(comandoNegativacaoCliente);
						}

						request.getSession().removeAttribute("listaCliente");
						mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, COMANDO_NEGATIVACAO_CLIENTE);
					}

				} else {
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, COMANDO_NEGATIVACAO);
				}

				listaComando.add(comandoNegativacaoInserido);
				model.addAttribute("listaComandoNegativacao", listaComando);
			} catch (NegocioException e) {
				// Lançar mensagem de erro
				mensagemErroParametrizado(model, e);
				return exibirInclusaoComandoNegativacao(comandoNegativacao, request, model);
			}
			// }
		} catch (NegocioException e) {
			model.addAttribute("comandoNegativacao", comandoNegativacao);
			// Lançar mensagem de erro
			mensagemErroParametrizado(model, e);
			return exibirInclusaoComandoNegativacao(comandoNegativacao, request, model);
		}

		ComandoNegativacaoVO comandoVO = new ComandoNegativacaoVO();
		comandoVO.setExecutado(false);

		return exibirPesquisaComandoNegativacao(comandoVO, request, model);

	}

	/**
	 * Incluir chamado.
	 *
	 * @param chamado the negativação
	 * @param result  the result
	 * @param session the session
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 * @throws ParseException 
	 */
	@RequestMapping("simularComandoNegativacao")
	public String simularComandoNegativacao(
			@RequestParam(value = "rotasSelecionadas", required = false) Long[] rotasSelecionadas,
			@ModelAttribute("ComandoNegativacaoImpl") ComandoNegativacaoImpl comandoNegativacao, BindingResult result,
			HttpSession session, HttpServletRequest request, Model model) throws GGASException, ParseException {

		String telaAlterar = (String) request.getParameter("telaAlterar");
		String telaDetalhar = (String) request.getParameter("telaDetalhar");
		
		try {
			popularComandoNegativacao(request, comandoNegativacao);

			if (comandoNegativacao.getGrupoFaturamento() != null) {
				if (rotasSelecionadas != null && rotasSelecionadas.length > 0) {
					comandoNegativacao.setChavesRotas(rotasSelecionadas);
					model.addAttribute("rotasSelecionadas", rotasSelecionadas);
				}
			}

			Collection<Fatura> listaSimulada = controladorNegativacao.obterFaturasComando(comandoNegativacao);

			model.addAttribute("listaFaturaSimulada", listaSimulada);
			model.addAttribute("qtdClientesSimulacao", obterQuantidadeClientesSimulacao(listaSimulada));
			model.addAttribute("valorTotalFaturasSimulacao", obterValorTotalFaturasSimulacao(listaSimulada));
			model.addAttribute("valorInicialDebitos", request.getParameter("valorInicialDebitos"));
			model.addAttribute("valorFinalDebitos", request.getParameter("valorFinalDebitos"));

			mensagemSucesso(model, MSG_SUCESSO_SIMULACAO);

		} catch (NegocioException e) {
			model.addAttribute("comandoNegativacao", comandoNegativacao);
			// Lançar mensagem de erro
			mensagemErroParametrizado(model, e);
			
			if("1".equals(telaAlterar)) {
				return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result,
						comandoNegativacao.getChavePrimaria(), request, model);
			} else {
				return exibirInclusaoComandoNegativacao(comandoNegativacao, request, model);
			}
		}

		if (telaAlterar != null && "1".equals(telaAlterar)) {
			request.setAttribute("comandoAlterar", comandoNegativacao);

			return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result,
					comandoNegativacao.getChavePrimaria(), request, model);
		} else if (telaDetalhar != null && "2".equals(telaDetalhar)) {
			return exibirDetalhamentoComandoNegativacao(comandoNegativacao, comandoNegativacao.getChavePrimaria(),
					request, model);
		} else {
			return exibirInclusaoComandoNegativacao(comandoNegativacao, request, model);
		}
	}

	/**
	 * Alterar arrecadador.
	 * 
	 * @param arrecadador
	 * @param result
	 * @param chavePrimaria
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return pesquisarArrecadador
	 * @throws ParseException
	 * @throws NegocioException
	 */
	@RequestMapping("alterarComandoNegativacao")
	public String alterarComandoNegativacao(
			@RequestParam(value = "rotasSelecionadas", required = false) Long[] rotasSelecionadas,
			@ModelAttribute("ComandoNegativacaoImpl") ComandoNegativacaoImpl comandoNegativacao, BindingResult result,
			HttpServletRequest request, Model model) throws NegocioException, ParseException {

		model.addAttribute("comandoNegativacao", comandoNegativacao);
		String tipoCliente = request.getParameter("comandoCliente");
		popularComandoNegativacao(request, comandoNegativacao);
		
		if ("1".equals(tipoCliente)) {
			comandoNegativacao.setIndicadorComandoCliente(true);
		}
		
		request.setAttribute("comandoAlterar", comandoNegativacao);
		if (controladorNegativacao.obterComandoNegativacaoPeloNomeEChave(comandoNegativacao.getNomeComando(),
				comandoNegativacao.getChavePrimaria()) != null) {
			mensagemErro(model, MSG_ERRO_NOME_COMANDO);
			return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result,
					comandoNegativacao.getChavePrimaria(), request, model);
		} else {
			Map<String, Object> erros = comandoNegativacao.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, new NegocioException(erros));
				return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result,
						comandoNegativacao.getChavePrimaria(), request, model);
			} else {
				try {
					

					comandoNegativacao.setDadosAuditoria(getDadosAuditoria(request));
					comandoNegativacao.setHabilitado(true);
					controladorNegativacao.atualizar(comandoNegativacao);

					Collection<ComandoNegativacaoRota> listaComandoRota = controladorNegativacao
							.obterComandoNegativacaoRota(comandoNegativacao.getChavePrimaria());
					if (listaComandoRota != null && !listaComandoRota.isEmpty()) {
						Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(listaComandoRota);
						controladorNegativacao.remover(chavesPrimarias, ComandoNegativacaoRota.class.getName(),
								getDadosAuditoria(request));
					}
					
					if (comandoNegativacao.getGrupoFaturamento() != null) {
						if (rotasSelecionadas != null && rotasSelecionadas.length > 0) {


							for (Long rotaSelecionada : rotasSelecionadas) {
								ComandoNegativacaoRota comandoNegativacaoRota = (ComandoNegativacaoRota) controladorNegativacao
										.criarComandoNegativacaoRota();
								comandoNegativacaoRota.setComandoNegativacao(comandoNegativacao);
								comandoNegativacaoRota.setRota((Rota) controladorRota.obter(rotaSelecionada));

								controladorNegativacao.inserir(comandoNegativacaoRota);
							}
						}

					}

					if ("1".equals(tipoCliente)) {
						Collection<ClienteVO> listaCliente = (Collection<ClienteVO>) request.getSession()
								.getAttribute("listaCliente");

						Collection<ComandoNegativacaoCliente> listaComandoNegativacao = controladorNegativacao
								.obterComandoCliente(comandoNegativacao.getChavePrimaria());
						Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(listaComandoNegativacao);
						controladorNegativacao.remover(chavesPrimarias, ComandoNegativacaoCliente.class.getName(),
								getDadosAuditoria(request));

						if (listaCliente != null && !listaCliente.isEmpty()) {

							for (ClienteVO clienteVO : listaCliente) {

								Cliente cliente = controladorCliente
										.obterCliente(clienteVO.getCliente().getChavePrimaria());

								ComandoNegativacaoCliente comandoNegativacaoCliente = (ComandoNegativacaoCliente) controladorNegativacao
										.criarComandoNegativacaoCliente();

								comandoNegativacaoCliente.setCliente(cliente);
								comandoNegativacaoCliente.setComando(comandoNegativacao);
								controladorNegativacao.inserir(comandoNegativacaoCliente);
							}

							request.getSession().removeAttribute("listaCliente");
							mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, COMANDO_NEGATIVACAO_CLIENTE);
						}

					}

					model.addAttribute(CHAVE_PRIMARIA, comandoNegativacao.getChavePrimaria());
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, COMANDO_NEGATIVACAO);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					mensagemErro(model, new NegocioException(erros));
					return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result,
							comandoNegativacao.getChavePrimaria(), request, model);
				}
			}

			ComandoNegativacaoVO comandoNegativacaoVO = new ComandoNegativacaoVO();
			comandoNegativacaoVO.setChaveComando(String.valueOf(comandoNegativacao.getChavePrimaria()));

			return pesquisarComandoNegativacao(comandoNegativacaoVO, result, request, model);
		}
	}

	private Integer obterQuantidadeClientesSimulacao(Collection<Fatura> listaSimulada) {
		Map<Cliente, Collection<Fatura>> mapaClienteFatura = new HashMap<Cliente, Collection<Fatura>>();
		for (Fatura fatura : listaSimulada) {

			if (mapaClienteFatura.containsKey(fatura.getCliente())) {
				mapaClienteFatura.get(fatura.getCliente()).add(fatura);
			} else {
				Collection<Fatura> novaListaFatura = new ArrayList<Fatura>();
				novaListaFatura.add(fatura);
				mapaClienteFatura.put(fatura.getCliente(), novaListaFatura);
			}
		}

		return mapaClienteFatura.keySet().size();
	}

	private BigDecimal obterValorTotalFaturasSimulacao(Collection<Fatura> listaFaturas) {
		BigDecimal valorTotal = new BigDecimal(0);
		for (Fatura fatura : listaFaturas) {
			valorTotal = valorTotal.add(fatura.getValorTotal());
		}

		return valorTotal;
	}

	private void popularComandoNegativacao(HttpServletRequest request, ComandoNegativacaoImpl comandoNegativacao)
			throws NumberFormatException, NegocioException {
		String referenciaInicioDebitos = request.getParameter("referenciaInicioDebitos");
		String referenciaFimDebitos = request.getParameter("referenciaFimDebitos");

		if (!referenciaInicioDebitos.isEmpty() && !referenciaFimDebitos.isEmpty()) {
			comandoNegativacao.setReferenciaInicialDebitos(Integer.valueOf(referenciaInicioDebitos.replace("/", "")));
			comandoNegativacao.setReferenciaFinalDebitos(Integer.valueOf(referenciaFimDebitos.replace("/", "")));
		}

		String valorInicialDebitos = request.getParameter("valorInicialDebitos");
		String valorFinalDebitos = request.getParameter("valorFinalDebitos");

		if (!valorInicialDebitos.isEmpty() && !valorFinalDebitos.isEmpty()) {
			valorInicialDebitos = valorInicialDebitos.replace(".", "");
			valorFinalDebitos = valorFinalDebitos.replace(".", "");

			comandoNegativacao.setValorInicialDebitos(new BigDecimal(valorInicialDebitos.replace(",", ".")));
			comandoNegativacao.setValorFinalDebitos(new BigDecimal(valorFinalDebitos.replace(",", ".")));
		}

		String situacaoContrato = request.getParameter("situacaoContrato");

		if (!situacaoContrato.isEmpty() && Long.parseLong(situacaoContrato) > 0) {
			comandoNegativacao
					.setSituacaoContrato(controladorContrato.obterSituacaoContrato(Long.parseLong(situacaoContrato)));
		}

		String situacaoConsumo = request.getParameter("situacaoConsumo");

		if (!situacaoConsumo.isEmpty() && Long.parseLong(situacaoConsumo) > 0) {
			comandoNegativacao
					.setSituacaoConsumo(controladorImovel.obterSituacaoConsumo(Long.parseLong(situacaoConsumo)));
		}

		String agenteNegativador = request.getParameter("agenteNegativador");
		if (!agenteNegativador.isEmpty() && Long.parseLong(agenteNegativador) > 0) {
			comandoNegativacao.setAgenteNegativador((AgenteNegativador) controladorNegativacao
					.obterAgenteNegativador(Long.parseLong(agenteNegativador)));
		}

		String dataPrevistaExecucao = request.getParameter("dataPrevistaExecucao");
		if (!dataPrevistaExecucao.isEmpty()) {
			comandoNegativacao.setDataPrevistaExecucao(DataUtil.converterParaData(dataPrevistaExecucao));
		}

		String dataVencimentoInicial = request.getParameter("dataVencimentoInicial");
		if (dataVencimentoInicial != null && !dataVencimentoInicial.isEmpty()) {
			comandoNegativacao.setDataVencimentoInicial(DataUtil.converterParaData(dataVencimentoInicial));
		}

		String dataVencimentoFinal = request.getParameter("dataVencimentoFinal");
		if (dataVencimentoFinal != null && !dataVencimentoFinal.isEmpty()) {
			comandoNegativacao.setDataVencimentoFinal(DataUtil.converterParaData(dataVencimentoFinal));
		}

		if (((dataVencimentoInicial != null && !dataVencimentoFinal.isEmpty())
				&& (dataVencimentoFinal != null && !dataVencimentoInicial.isEmpty()))
				&& (comandoNegativacao.getDataVencimentoInicial().after(comandoNegativacao.getDataVencimentoFinal()))) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_COMANDO_PERIODO_VENCIMENTO, true);

		}

	}

	/**
	 * Método responsável por exibir tela de pesquisa de clientes da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 */
	@RequestMapping("exibirPesquisaControleNegativacao")
	public String exibirPesquisaControleNegativacao(ClienteNegativacaoVO vo, HttpServletRequest request, Model model)
			throws NegocioException {

		request.getSession().setAttribute("isPermiteCaracteresEspeciais", "1");

		return "exibirPesquisaControleNegativacao";
	}

	/**
	 * Método responsável por pesquisar de comando da negativação.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 * @throws ParseException
	 */
	@RequestMapping("pesquisarClientesParaNegativacao")
	public String pesquisarClientesParaNegativacao(ClienteNegativacaoVO clienteNegativacaoVO, BindingResult result,
			@RequestParam(value = "situacaoFaturas", required = false) String situacaoFaturas,
			@RequestParam(value = "situacaoNegativacao", required = false) String situacaoNegativacao,
			HttpServletRequest request, Model model) throws NegocioException, ParseException {

		prepararFiltro(situacaoFaturas, situacaoNegativacao, clienteNegativacaoVO);

		Collection<FaturaClienteNegativacao> listaFaturasNegativacao = controladorNegativacao
				.obterListaClientesNegativacao(clienteNegativacaoVO);
		model.addAttribute("listaFaturasNegativacao", listaFaturasNegativacao);
		model.addAttribute(FATURA_CLIENTE_FORM, clienteNegativacaoVO);

		return exibirPesquisaControleNegativacao(clienteNegativacaoVO, request, model);
	}

	private void prepararFiltro(String situacaoFaturas, String situacaoNegativacao,
			ClienteNegativacaoVO clienteNegativacaoVO) {
		if (!StringUtils.isEmpty(situacaoFaturas)) {
			clienteNegativacaoVO.setSituacaoFaturas(Integer.parseInt(situacaoFaturas));
		}

		if (!StringUtils.isEmpty(situacaoNegativacao)) {
			clienteNegativacaoVO.setSituacaoNegativacao(Integer.parseInt(situacaoNegativacao));
		}
	}

	/**
	 * Método responsável por incluir os clientes na negativacao
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 * @throws ParseException
	 */
	@RequestMapping("incluirClientesNegativacao")
	public String incluirClientesNegativacao(ClienteNegativacaoVO clienteNegativacaoVO, BindingResult result,
			@RequestParam(CHAVES_PRIMARIAS) Long[] idsClientesNegativacao, HttpServletRequest request, Model model)
			throws NegocioException, ParseException {

		String retorno = null;
		String dataInclusao = request.getParameter("dataIncluir");

		if (dataInclusao != null && !dataInclusao.isEmpty()) {
			try {
				Date dataInclusaoInserir = Util.converterCampoStringParaData("Data Inclusão", dataInclusao,
						Constantes.FORMATO_DATA_BR);
				controladorNegativacao.incluirClientesNegativacao(idsClientesNegativacao, dataInclusaoInserir);

				String replicar = request.getParameter("replicar");
				if ("true".equals(replicar)) {
					controladorNegativacao.confirmarInclusaoClientesNegativacao(idsClientesNegativacao,
							getDadosAuditoria(request), dataInclusaoInserir);
				}

				model.addAttribute(FATURA_CLIENTE_FORM, clienteNegativacaoVO);
				mensagemSucesso(model, Constantes.SUCESSO_INCLUSAO_NEGATIVACAO);
				// retorno = exibirPesquisaControleNegativacao(clienteNegativacaoVO, request,
				// model);
				retorno = pesquisarClientesParaNegativacao(clienteNegativacaoVO, result,
						String.valueOf(clienteNegativacaoVO.getSituacaoFaturas()),
						String.valueOf(clienteNegativacaoVO.getSituacaoNegativacao()), request, model);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				retorno = "forward:ipesquisarClientesParaNegativacao";
			}
		} else {
			mensagemErro(model, MSG_ERRO_DATA_PREENCHIDA);
			retorno = "forward:ipesquisarClientesParaNegativacao";
		}
		return retorno;
	}

	/**
	 * Método responsável por confirmar a inclusao dos clientes na negativacao
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws ParseException
	 * @throws GGASException
	 */
	@RequestMapping("confirmarClientesNegativacao")
	public String confirmarClientesNegativacao(ClienteNegativacaoVO clienteNegativacaoVO, BindingResult result,
			@RequestParam(CHAVES_PRIMARIAS) Long[] idsClientesNegativacao, HttpServletRequest request, Model model)
			throws ParseException, GGASException {

		String dataConfirmar = request.getParameter("dataConfirmar");

		if (dataConfirmar != null && !dataConfirmar.isEmpty()) {
			controladorNegativacao.confirmarInclusaoClientesNegativacao(idsClientesNegativacao,
					getDadosAuditoria(request),
					Util.converterCampoStringParaData("Data Confirmar", dataConfirmar, Constantes.FORMATO_DATA_BR));
			mensagemSucesso(model, Constantes.SUCESSO_CONFIRMACAO_NEGATIVACAO);
		} else {
			mensagemErro(model, MSG_ERRO_DATA_PREENCHIDA);
		}
		return exibirPesquisaControleNegativacao(clienteNegativacaoVO, request, model);
	}

	/**
	 * Método responsável por excluir os clientes na negativacao
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws ParseException
	 * @throws GGASException
	 */
	@RequestMapping("excluirClientesNegativacao")
	public String excluirClientesNegativacao(ClienteNegativacaoVO clienteNegativacaoVO, BindingResult result,
			@RequestParam(CHAVES_PRIMARIAS) Long[] idsClientesNegativacao, HttpServletRequest request, Model model)
			throws ParseException, GGASException {

		String dataExclusao = request.getParameter("dataExclusao");

		if (dataExclusao != null && !dataExclusao.isEmpty()) {
			controladorNegativacao.excluirClientesNegativacao(idsClientesNegativacao, getDadosAuditoria(request),
					Util.converterCampoStringParaData("Data Exclusão", dataExclusao, Constantes.FORMATO_DATA_BR));
			mensagemSucesso(model, Constantes.SUCESSO_EXCLUSAO_NEGATIVACAO);
		} else {
			mensagemErro(model, MSG_ERRO_DATA_PREENCHIDA);
		}
		return exibirPesquisaControleNegativacao(clienteNegativacaoVO, request, model);
	}

	/**
	 * Método responsável por cancelar clientes na negativacao
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model   {@link Model}
	 * @return String {@link String}
	 * @throws ParseException
	 * @throws GGASException
	 */
	@RequestMapping("cancelarClientesNegativacao")
	public String cancelarClientesNegativacao(ClienteNegativacaoVO clienteNegativacaoVO, BindingResult result,
			@RequestParam(CHAVES_PRIMARIAS) Long[] idsClientesNegativacao, HttpServletRequest request, Model model)
			throws ParseException, GGASException {

		String dataCancelamento = request.getParameter("dataCancelamento");

		if (dataCancelamento != null && !dataCancelamento.isEmpty()) {
			controladorNegativacao.cancelarClientesNegativacao(idsClientesNegativacao, getDadosAuditoria(request), Util
					.converterCampoStringParaData("Data Cancelamanto", dataCancelamento, Constantes.FORMATO_DATA_BR));
			mensagemSucesso(model, Constantes.SUCESSO_CANCELAMENTO_NEGATIVACAO);
		} else {
			mensagemErro(model, MSG_ERRO_DATA_PREENCHIDA);
		}
		return exibirPesquisaControleNegativacao(clienteNegativacaoVO, request, model);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("alterarNomeComando")
	public String alterarNomeComando(ComandoNegativacaoImpl comandoNegativacao, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chaveComando, HttpServletRequest request, Model model)
			throws GGASException, ParseException {

		ComandoNegativacao comando = controladorNegativacao.obterComandoNegativacao(chaveComando);

		if (controladorNegativacao.obterComandoNegativacaoPeloNomeEChave(comandoNegativacao.getNomeComando(),
				comandoNegativacao.getChavePrimaria()) != null) {
			mensagemErro(model, MSG_ERRO_NOME_COMANDO);
			request.setAttribute("erroNome", true);
			return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result, chaveComando, request, model);
		} else {
			if (comandoNegativacao.getNomeComando() != null && !comandoNegativacao.getNomeComando().isEmpty()) {
				comando.setNomeComando(comandoNegativacao.getNomeComando());
				controladorNegativacao.atualizar(comando, ComandoNegativacao.class);
				mensagemSucesso(model, MSG_SUCESSO_ALTERAR_COMANDO);

				ComandoNegativacaoVO comandoNegativacaoVO = new ComandoNegativacaoVO();
				comandoNegativacaoVO.setChaveComando(String.valueOf(chaveComando));
				return pesquisarComandoNegativacao(comandoNegativacaoVO, result, request, model);
			} else {
				mensagemErro(model, MSG_ERRO_ALTERAR_COMANDO);
				request.setAttribute("erroNome", true);
				return exibirAlterarComandoNegativacao(new ComandoNegativacaoVO(), result, chaveComando, request,
						model);
			}
		}

	}
	
	@RequestMapping("removerComandoNegativacao")
	public String removerComandoNegativacao(ComandoNegativacaoVO comandoNegativacaoVO, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws NegocioException, ParseException {

		try {

			controladorNegativacao.removerComandoNegativacao(chavesPrimarias, getDadosAuditoria(request));
			
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, ComandoNegativacao.COMANDO_NEGATIVACAO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_COMANDO_NEGATIVACAO, e));
		}

		return pesquisarComandoNegativacao(comandoNegativacaoVO, result, request, model);
	}

}
