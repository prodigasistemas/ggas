/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/12/2013 18:56:30
 @author wcosta
 */

package br.com.ggas.web.cobranca.acaocobranca;

import java.io.Serializable;
import java.math.BigDecimal;

public class SubRelatorioFaturasVencidasCobrancaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5715196914581461016L;

	private String titulo;

	private String deposito;

	private String emissao;

	private String vencimento;

	private String atraso;

	private BigDecimal valor;

	private BigDecimal saldo;

	private BigDecimal juros;

	private BigDecimal valorPagar;

	private String descricaoPontoConsumo;
	
	private String nomeRota;

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getTitulo() {

		return titulo;
	}

	public void setTitulo(String titulo) {

		this.titulo = titulo;
	}

	public String getDeposito() {

		return deposito;
	}

	public void setDeposito(String deposito) {

		this.deposito = deposito;
	}

	public String getEmissao() {

		return emissao;
	}

	public void setEmissao(String emissao) {

		this.emissao = emissao;
	}

	public String getVencimento() {

		return vencimento;
	}

	public void setVencimento(String vencimento) {

		this.vencimento = vencimento;
	}

	public String getAtraso() {

		return atraso;
	}

	public void setAtraso(String atraso) {

		this.atraso = atraso;
	}

	public BigDecimal getValor() {

		return valor;
	}

	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	public BigDecimal getSaldo() {

		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	public BigDecimal getJuros() {

		return juros;
	}

	public void setJuros(BigDecimal juros) {

		this.juros = juros;
	}

	public BigDecimal getValorPagar() {

		return valorPagar;
	}

	public void setValorPagar(BigDecimal valorPagar) {

		this.valorPagar = valorPagar;
	}

	public String getNomeRota() {
		return nomeRota;
	}

	public void setNomeRota(String nomeRota) {
		this.nomeRota = nomeRota;
	}

}
