package br.com.ggas.web.cobranca.acaocobranca;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVOCobranca;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ImovelPopupVO;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.web.cadastro.cliente.ClientePopupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável pelas operações referentes ao acompanhamento de cobrança.
 * 
 * @author esantana
 *
 */
@Controller
public class AcompanhamentoCobrancaAction extends GenericAction {

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	@Autowired
	private ControladorAvisoCorte controladorAvisoCorte;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Exibir acompanhamento cobranca.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAcompanhamentoCobranca")
	public String exibirAcompanhamentoCobranca() {

		return "exibirAcompanhamentoCobranca";
	}

	/**
	 * Pesquisar aviso corte.
	 * 
	 * @param model          - {@link Model}
	 * @param request        - {@link HttpServletRequest}
	 * @param idPontoConsumo - {@link Long}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO  - {@link ImovelPopupVO}
	 * @param bindingResult  - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarAvisoCorte")
	public String pesquisarAvisoCorte(Model model, HttpServletRequest request,
			@RequestParam(value = "idPontoConsumo", required = false) Long idPontoConsumo,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		Map<String, Object> filtros = popularFiltroPesquisaAvisoCorte(clientePopupVO.getIdCliente(),
				imovelPopupVO.getIdImovel(), idPontoConsumo, true);

		List<AvisoCorte> avisosCorte = new ArrayList<AvisoCorte>();

		try {

			avisosCorte.addAll(controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtros));

			Collection<ServicoAutorizacaoVOCobranca> listaServicoAutorizacaoVO = controladorAvisoCorte
					.criarListaServicoAutorizacaoVOCobranca(avisosCorte, idPontoConsumo);

			model.addAttribute("listaServicoAutorizacaoVO", listaServicoAutorizacaoVO);

			model.addAttribute("listaPontoConsumo",
					this.buscarListaPontosDeConsumo(filtros, clientePopupVO, imovelPopupVO));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPesquisaAcaoCobranca(model, request, clientePopupVO, imovelPopupVO, idPontoConsumo);
	}

	/**
	 * Pesquisar ponto consumo acompanhamento cobranca.
	 * 
	 * @param model          - {@link Model}
	 * @param request        - {@link HttpServletRequest}
	 * @param idPontoConsumo - {@link Long}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO  - {@link ImovelPopupVO}
	 * @param bindingResult  - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarPontoConsumoAcompanhamentoCobranca")
	public String pesquisarPontoConsumoAcompanhamentoCobranca(Model model, HttpServletRequest request,
			@RequestParam(value = "idPontoConsumo", required = false) Long idPontoConsumo,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		Map<String, Object> filtros = popularFiltroPesquisaAvisoCorte(clientePopupVO.getIdCliente(),
				imovelPopupVO.getIdImovel(), idPontoConsumo, true);

		try {
			model.addAttribute("listaPontoConsumo",
					this.buscarListaPontosDeConsumo(filtros, clientePopupVO, imovelPopupVO));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return this.exibirPesquisaAcaoCobranca(model, request, clientePopupVO, imovelPopupVO, idPontoConsumo);
	}

	private List<PontoConsumo> buscarListaPontosDeConsumo(Map<String, Object> filtros, ClientePopupVO clientePopupVO,
			ImovelPopupVO imovelPopupVO) throws GGASException {

		Long idImovel = imovelPopupVO.getIdImovel();
		Long idCliente = clientePopupVO.getIdCliente();
		List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		if (idImovel != null && idImovel > 0) {
			listaPontoConsumo.addAll(controladorImovel.listarPontoConsumoPorChaveImovel(idImovel));

		} else if (idCliente != null && idCliente > 0) {
			listaPontoConsumo.addAll(controladorPontoConsumo.listarPontosConsumoPorChaveCliente(idCliente));

		} else {
			listaPontoConsumo.addAll(controladorPontoConsumo.listarPontosConsumo(filtros));
		}

		return listaPontoConsumo;
	}

	private Map<String, Object> popularFiltroPesquisaAvisoCorte(Long idCliente, Long idImovel, Long idPontoConsumo,
			boolean isPesquisaPorPontoConsumo) {

		Map<String, Object> filtros = new HashMap<String, Object>();

		if (idCliente != null && idCliente > 0) {
			filtros.put(ID_CLIENTE, idCliente);
		}
		if (idImovel != null && idImovel > 0) {
			filtros.put(ID_IMOVEL, idImovel);
		}

		if (isPesquisaPorPontoConsumo && idPontoConsumo != null && idPontoConsumo > 0) {
			filtros.put(ID_PONTO_CONSUMO, idPontoConsumo);
		}

		filtros.put("contratoAtivo", Boolean.TRUE);

		return filtros;
	}

	/**
	 * Método responsável por exibir a página de Pesquisa de Açoes de Cobrança.
	 * 
	 * @param model          - {@link Model}
	 * @param request        - {@link HttpServletRequest}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO  - {@link ImovelPopupVO}
	 * @param idPontoConsumo - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaAcaoCobranca")
	public String exibirPesquisaAcaoCobranca(Model model, HttpServletRequest request, ClientePopupVO clientePopupVO,
			ImovelPopupVO imovelPopupVO,
			@RequestParam(value = "idPontoConsumo", required = false) Long idPontoConsumo) {

		try {
			this.carregarCampos(request);
			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("clientePopupVO", clientePopupVO);
		model.addAttribute("imovelPopupVO", imovelPopupVO);
		model.addAttribute(ID_PONTO_CONSUMO, idPontoConsumo);

		return "exibirAcompanhamentoCobranca";
	}

	private void carregarCampos(HttpServletRequest request) throws GGASException {

		request.setAttribute("listaTipoDocumento", controladorArrecadacao.listarTipoDocumento());
		request.setAttribute("listaTipoNumeroDias", controladorEntidadeConteudo.obterListaOpcaoDias());
	}

	/**
	 * Pesquisar aviso corte por imovel cliente.
	 * 
	 * @param model          - {@link Model}
	 * @param request        - {@link HttpServletRequest}
	 * @param idPontoConsumo - {@link Long}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO  - {@link ImovelPopupVO}
	 * @param bindingResult  - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarAvisoCortePorImovelCliente")
	public String pesquisarAvisoCortePorImovelCliente(Model model, HttpServletRequest request,
			@RequestParam(value = "idPontoConsumo", required = false) Long idPontoConsumo,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {
		try {
			Map<String, Object> filtros = popularFiltroPesquisaAvisoCorte(clientePopupVO.getIdCliente(),
					imovelPopupVO.getIdImovel(), idPontoConsumo, false);

			Collection<AvisoCorte> avisosCorte = controladorAvisoCorte
					.consultarAvisoCorteAcompanhamentoCobranca(filtros);

			Collection<ServicoAutorizacaoVOCobranca> listaServicoAutorizacaoVO = controladorAvisoCorte
					.criarListaServicoAutorizacaoVOCobranca(avisosCorte, null);

			request.setAttribute("listaServicoAutorizacaoVO2", listaServicoAutorizacaoVO);
		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPesquisaAcaoCobranca(model, request, clientePopupVO, imovelPopupVO, idPontoConsumo);
	}
}
