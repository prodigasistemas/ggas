package br.com.ggas.web.cobranca.negativacao;

import java.io.Serializable;
import java.util.List;

import br.com.ggas.web.cobranca.acaocobranca.SubRelatorioFaturasVencidasCobrancaVO;

public class NotificacaoNegativacaoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5286312847473616404L;
	
	private String nome;

	private String endereco;

	private String bairro;

	private String cidade;

	private String codigo;

	private String codigoUnico;

	private String pontoEntrega;

	private String cep;

	private String codigoContrato;

	private String tipoResidencia;

	private String data;

	private String municipioUf;
	
	private Long numeroRota;

	private List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturaVencida;

	private String cepCidadeEstado;
	
	private String enderecoPontoConsumoRelatorioCorteSergas;

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getCodigo() {

		return codigo;
	}

	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getPontoEntrega() {

		return pontoEntrega;
	}

	public void setPontoEntrega(String pontoEntrega) {

		this.pontoEntrega = pontoEntrega;
	}

	public List<SubRelatorioFaturasVencidasCobrancaVO> getColecaoFaturaVencida() {

		return colecaoFaturaVencida;
	}

	public void setColecaoFaturaVencida(List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturaVencida) {

		this.colecaoFaturaVencida = colecaoFaturaVencida;
	}

	public String getCodigoUnico() {

		return codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {

		this.codigoUnico = codigoUnico;
	}

	public String getCodigoContrato() {

		return codigoContrato;
	}

	public void setCodigoContrato(String codigoContrato) {

		this.codigoContrato = codigoContrato;
	}

	public String getTipoResidencia() {

		return tipoResidencia;
	}

	public void setTipoResidencia(String tipoResidencia) {

		this.tipoResidencia = tipoResidencia;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	public String getMunicipioUf() {

		return municipioUf;
	}

	public void setMunicipioUf(String municipioUf) {

		this.municipioUf = municipioUf;
	}

	public String getCepCidadeEstado() {

		return cepCidadeEstado;
	}

	public void setCepCidadeEstado(String cepCidadeEstado) {

		this.cepCidadeEstado = cepCidadeEstado;
	}

	public String getEnderecoPontoConsumoRelatorioCorteSergas() {
		return enderecoPontoConsumoRelatorioCorteSergas;
	}

	public void setEnderecoPontoConsumoRelatorioCorteSergas(String enderecoPontoConsumoRelatorioCorteSergas) {
		this.enderecoPontoConsumoRelatorioCorteSergas = enderecoPontoConsumoRelatorioCorteSergas;
	}

	public Long getNumeroRota() {
		return numeroRota;
	}

	public void setNumeroRota(Long numeroRota) {
		this.numeroRota = numeroRota;
	}

}
