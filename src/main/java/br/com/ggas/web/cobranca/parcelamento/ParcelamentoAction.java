package br.com.ggas.web.cobranca.parcelamento;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.parcelamento.ParcelamentoItem;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Util;

/**
 * 
 * Classe responsável pelas operações referentes ao Parcelamento.
 *
 */
@Controller
public class ParcelamentoAction extends GenericAction {

	private static final String HEADER_PARAM = "attachment";

	private static final String MEDIA_TYPE = "application/pdf";

	private static final String PARCELAMENTO_VO = "parcelamentoVO";

	private static final String STATUS = "status";

	private static final String DATA_FINAL = "dataFinal";

	private static final String DATA_INICIAL = "dataInicial";

	private static final String TIPO_NOTA_CREDITO = "tipoNotaCredito";

	private static final String TIPO_CREDITO = "tipoCredito";

	private static final String STATUS_AUTORIZADO = "statusAutorizado";

	private static final String STATUS_NAO_AUTORIZADO = "statusNaoAutorizado";

	private static final String STATUS_PENDENTE = "statusPendente";

	private static final String LISTA_CONTRATO_PONTO_CONSUMOS = "listaContratoPontoConsumos";

	private static final String LISTA_CREDITO_DEBITO = "listaCreditoDebito";

	private static final String ID_CLIENTE = "idCliente";

	private static final String LISTA_FATURA = "listaFatura";

	private static final String LISTA_PERFIL_PARCELAMENTO = "listaPerfilParcelamento";

	private static final String DADOS_GERAIS_SESSAO = "dadosGeraisSessao";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ID_PERFIL_PARCELAMENTO = "idPerfilParcelamento";

	private static final String VALOR_TOTAL_DEBITO = "valorTotalDebito";

	private static final String VALOR_DEBITO_CREDITO = "valorDebitoCredito";

	private static final String VALOR_DESCONTO = "valorDesconto";

	private static final String PERCENTUAL_DESCONTO = "percentualDesconto";

	private static final String VALOR_TOTAL_LIQUIDO = "valorTotalLiquido";

	private static final String NUMERO_PARCELAS = "numeroParcelas";

	private static final String COBRANCA_EM_CONTA = "cobrancaEmConta";

	private static final String VALOR_NAO = "false";

	private static final String PERIODICIDADE_PARCELAS = "periodicidadeParcelas";

	private static final String VENCIMENTO_INICIAL = "vencimentoInicial";

	private static final String INDICADOR_NOTA_PROMISSORIA = "indicadorNotaPromissoria";

	private static final String INDICADOR_CONFISSAO_DIVIDA = "indicadorConfissaoDivida";

	private static final String DADOS_GERAIS = "dadosGerais";

	private static final String RELATORIO_EXIBICAO = "relatorioExibicao";

	private static final String VALOR_SIM = "true";

	private static final String PARCELAMENTO_PERFIL = "parcelamentoPerfil";

	private static final String LISTA_PARCELAMENTO_ITEM = "listaParcelamentoItem";

	private static final String SITUACAO_PAGAMENTO = "situacaoPagamento";

	private static final String TIPO_DOCUMENTO = "tipoDocumento";

	private static final String CREDITO_DEBITO_NEGOCIADO = "creditoDebitoNegociado";

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorParcelamento controladorParcelamento;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorAlcada controladorAlcada;

	/**
	 * Método responsável por exibir a tela de pesquisa de Parcelamento de Débitos.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaParcelamentoDebitos")
	public String exibirPesquisaParcelamentoDebitos(Model model) throws GGASException {

		model.addAttribute("intervaloAnosData", intervaloAnosData());

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
		model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));
		model.addAttribute(STATUS_PENDENTE, Long.valueOf(statusPendente));

		return "exibirPesquisaParcelamentoDebitos";
	}

	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Método responsável por pesquisar os Parcelamentos de Débitos.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("pesquisarParcelamentoDebitos")
	public String pesquisarParcelamentoDebitos(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO,
			BindingResult bindingResult) {

		String view = "forward:/exibirPesquisaParcelamentoDebitos";

		model.addAttribute(PARCELAMENTO_VO, parcelamentoVO);

		try {
			Collection<Parcelamento> listaParcelamento = null;
			Date objDataInicial = null;
			Date objDataFinal = null;

			Map<String, Object> filtro = this.prepararFiltro(parcelamentoVO);

			String dataInicial = (String) filtro.get(DATA_INICIAL);
			String dataFinal = (String) filtro.get(DATA_FINAL);
			Long status = (Long) filtro.get(STATUS);
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);

			controladorParcelamento.validarFiltroPesquisaParcelamentoDebitos(dataInicial, dataFinal);

			if (!StringUtils.isEmpty(dataInicial) && !StringUtils.isEmpty(dataFinal)) {
				objDataInicial = Util.converterCampoStringParaData("Data Inicial", dataInicial, Constantes.FORMATO_DATA_BR);
				objDataFinal = Util.converterCampoStringParaData("Data Final", dataFinal, Constantes.FORMATO_DATA_BR);
			}

			listaParcelamento = controladorParcelamento.consultarParcelamentoCliente(parcelamentoVO.getIdCliente(),
					parcelamentoVO.getIdImovel(), objDataInicial, objDataFinal, status, chavesPrimarias);

			model.addAttribute("listaParcelamento", listaParcelamento);
			view = this.exibirPesquisaParcelamentoDebitos(model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private Map<String, Object> prepararFiltro(ParcelamentoVO parcelamentoVO) {

		Map<String, Object> filtro = new HashMap<>();

		Long status = null;
		String dataInicial = null;
		String dataFinal = null;

		if (!StringUtils.isEmpty(parcelamentoVO.getIsAutorizacao()) && Boolean.valueOf(parcelamentoVO.getIsAutorizacao())) {

			dataInicial = parcelamentoVO.getDataInicial();
			if (!StringUtils.isEmpty(dataInicial)) {
				parcelamentoVO.setDataParcelamentoInicial(dataInicial);
			}

			dataFinal = parcelamentoVO.getDataFinal();
			if (!StringUtils.isEmpty(dataFinal)) {
				parcelamentoVO.setDataParcelamentoFinal(dataFinal);
			}

			String listaChavesPrimarias = parcelamentoVO.getChavesPrimarias();
			if (!StringUtils.isEmpty(listaChavesPrimarias)) {
				Long[] chavesPrimarias = Util.arrayStringParaArrayLong(listaChavesPrimarias.split(","));
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			}
		} else {
			dataInicial = parcelamentoVO.getDataParcelamentoInicial();

			dataFinal = parcelamentoVO.getDataParcelamentoFinal();

			status = parcelamentoVO.getStatus();
		}

		filtro.put(DATA_INICIAL, dataInicial);
		filtro.put(DATA_FINAL, dataFinal);
		filtro.put(STATUS, status);

		return filtro;
	}

	/**
	 * Método responsável por exibir a tela de pesquisa para efetuar o Parcelamento de Débitos.
	 * 
	 * @param model - {@link Model}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirPesquisaEfetuarParcelamentoDebitos")
	public String exibirPesquisaEfetuarParcelamentoDebitos(Model model, ParcelamentoVO parcelamentoVO,
			BindingResult bindingResult) {

		model.addAttribute(PARCELAMENTO_VO, parcelamentoVO);
		
		return "exibirPesquisaEfetuarParcelamentoDebitos";
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo do imóvel.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("pesquisarPontosConsumoParcelamento")
	public String pesquisarPontosConsumo(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO,
			BindingResult bindingResult) {

		try {
			this.pesquisarPontos(parcelamentoVO.getIdImovel(), parcelamentoVO.getIdCliente(), parcelamentoVO, model);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPesquisaEfetuarParcelamentoDebitos(model, parcelamentoVO, bindingResult);
	}

	private void pesquisarPontos(Long idImovel, Long idCliente, ParcelamentoVO parcelamentoVO, Model model) throws GGASException {

		controladorParcelamento.validarFiltroPesquisaPontosConsumo(idImovel, idCliente);
		Collection<ContratoPontoConsumo> listaContratoPontoConsumos = null;

		if ((idImovel != null) && (idImovel > 0)) {
			listaContratoPontoConsumos = controladorContrato.consultarContratoClientePontoConsumoPorImovel(idImovel);
			parcelamentoVO.setIndicadorPesquisa("indicadorPesquisaImovel");
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaContratoPontoConsumos = controladorContrato.consultarContratoPontoConsumoPorCliente(idCliente);
			parcelamentoVO.setIndicadorPesquisa("indicadorPesquisaCliente");
		}

		model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de débitos do cliente.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param aposValidacao - {@link Boolean}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDebitosParaParcelamento")
	public String exibirDebitosParaParcelamento(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO,
			BindingResult bindingResult, @RequestParam(value = "aposValidacao", required = false) Boolean aposValidacao) {

		String view = "exibirDebitosParaParcelamento";

		try {
			montarDadosParaExibicao(model, parcelamentoVO);
			
			if (!(aposValidacao != null && aposValidacao)) {
				Long idCliente = parcelamentoVO.getIdCliente();
				this.limparFormFiador(parcelamentoVO);
				parcelamentoVO.setIdCliente(idCliente);
			}

			model.addAttribute(PARCELAMENTO_VO, parcelamentoVO);

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/pesquisarPontosConsumoParcelamento";
		} catch (Exception e) {
			throw new InfraestruturaException(e);
		}

		return view;
	}

	private void limparFormFiador(ParcelamentoVO parcelamentoVO) {

		parcelamentoVO.setIdCliente(Long.valueOf(0));
		parcelamentoVO.setNomeCompletoCliente(StringUtils.EMPTY);
		parcelamentoVO.setDocumentoFormatado(StringUtils.EMPTY);
		parcelamentoVO.setDocumentoFormatado(StringUtils.EMPTY);
		parcelamentoVO.setEmailCliente(StringUtils.EMPTY);
		parcelamentoVO.setEnderecoFormatadoCliente(StringUtils.EMPTY);
	}

	private void montarDadosParaExibicao(Model model, ParcelamentoVO parcelamentoVO)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, GGASException {

		Long idCliente = parcelamentoVO.getIdCliente();
		Long idPontoConsumo = parcelamentoVO.getIdPontoConsumo();

		DadosParcelamento dadosParcelamento = controladorParcelamento.obterDadosExibicaoEfetuarParcelamento(idCliente, idPontoConsumo);

		if (dadosParcelamento.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo = dadosParcelamento.getPontoConsumo();
			parcelamentoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			parcelamentoVO.setEnderecoPontoConsumo(pontoConsumo.getEnderecoFormatado());
			parcelamentoVO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
			if (pontoConsumo.getCep() != null) {
				parcelamentoVO.setCepPontoConsumo(pontoConsumo.getCep().getCep());
			}
		}

		if (dadosParcelamento.isBloquearCobrancaFatura()) {
			parcelamentoVO.setBloquearCobrancaFatura(dadosParcelamento.isBloquearCobrancaFatura());
			parcelamentoVO.setCobrancaEmConta(Boolean.FALSE.toString());
		}

		model.addAttribute(LISTA_FATURA, dadosParcelamento.getListaFatura());
		model.addAttribute(LISTA_PERFIL_PARCELAMENTO, dadosParcelamento.getListaPerfil());

		Map<String, Object> filtro = new HashMap<String, Object>();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			Long[] chavesPontoConsumo = new Long[] { idPontoConsumo };
			filtro.put("chavesPontoConsumo", chavesPontoConsumo);
		}

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (statusAutorizado != null) {
			filtro.put(STATUS, Long.valueOf(statusAutorizado));
		}

		Collection<Map<String, Object>> listaExibicaoCreditoDebito =
				controladorCreditoDebito.consultarPesquisaCreditoDebitoParcelamento(filtro, true);
		model.addAttribute(LISTA_CREDITO_DEBITO, listaExibicaoCreditoDebito);

		this.adicionarTipoCredito(model);
	}

	private void adicionarTipoCredito(Model model) throws GGASException {

		String tipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);
		String tipoNotaCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO);

		model.addAttribute(TIPO_CREDITO, Long.parseLong(tipoCredito));
		model.addAttribute(TIPO_NOTA_CREDITO, Long.parseLong(tipoNotaCredito));
	}

	/**
	 * Método responsável por gerar as parcelas.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	@RequestMapping("gerarParcelas")
	public String gerarParcelas(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO, BindingResult bindingResult) 
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{

		String view = "forward:/exibirDebitosParaParcelamento?aposValidacao=true";

		try {
			request.getSession().removeAttribute(DADOS_GERAIS_SESSAO);
			Map<String, Object> dados = this.prepararDados(parcelamentoVO);

			DadosGeraisParcelas dadosGerais =
					controladorParcelamento.gerarParcelas(dados, parcelamentoVO.getChavesFatura(), parcelamentoVO.getChavesCreditoDebito());
			request.getSession().setAttribute(DADOS_GERAIS_SESSAO, dadosGerais);
			request.setAttribute(DADOS_GERAIS, dadosGerais);

			this.montarDadosParaExibicao(model, parcelamentoVO);

			model.addAttribute(PARCELAMENTO_VO, parcelamentoVO);

			view = "exibirDebitosParaParcelamento";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private Map<String, Object> prepararDados(ParcelamentoVO parcelamentoVO) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();

		if (parcelamentoVO.getIdCliente() != null && parcelamentoVO.getIdCliente() > 0) {
			dados.put(ID_CLIENTE, parcelamentoVO.getIdCliente());
		}

		if (parcelamentoVO.getIdPontoConsumo() != null && parcelamentoVO.getIdPontoConsumo() > 0) {
			dados.put(ID_PONTO_CONSUMO, parcelamentoVO.getIdPontoConsumo());
		}

		if (parcelamentoVO.getIdPerfilParcelamento() != null && parcelamentoVO.getIdPerfilParcelamento() > 0) {
			dados.put(ID_PERFIL_PARCELAMENTO, parcelamentoVO.getIdPerfilParcelamento());
		}

		if (!StringUtils.isEmpty(parcelamentoVO.getValorTotalDebito())) {
			dados.put(VALOR_TOTAL_DEBITO, NumeroUtil.converterCampoStringParaValorBigDecimal(parcelamentoVO.getValorTotalDebito()));
		}

		if (!StringUtils.isEmpty(parcelamentoVO.getValorDebitoCredito())) {
			dados.put(VALOR_DEBITO_CREDITO, NumeroUtil.converterCampoStringParaValorBigDecimal(parcelamentoVO.getValorDebitoCredito()));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getValorDesconto())) {
			dados.put(VALOR_DESCONTO, Util.converterCampoStringParaValorBigDecimal("", parcelamentoVO.getValorDesconto(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getPercentualDesconto())) {
			dados.put(PERCENTUAL_DESCONTO, Util.converterCampoStringParaValorBigDecimal("", parcelamentoVO.getPercentualDesconto(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getValorTotalLiquido())) {
			dados.put(VALOR_TOTAL_LIQUIDO, Util.converterCampoStringParaValorBigDecimal("",
					parcelamentoVO.getValorTotalLiquido().replace(".", ","), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getNumeroParcelas())) {
			dados.put(NUMERO_PARCELAS, Util.converterCampoStringParaValorInteger("Número de Parcelas", parcelamentoVO.getNumeroParcelas()));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getCobrancaEmConta())) {
			dados.put(COBRANCA_EM_CONTA, Boolean.valueOf(parcelamentoVO.getCobrancaEmConta()));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getVencimentoInicial())) {
			dados.put("indicadorVencimentoInicial", Boolean.valueOf(parcelamentoVO.getVencimentoInicial()));

			Calendar dataVencimento = Calendar.getInstance();

			if (parcelamentoVO.getDataPrimeiroVencimento() != null && !parcelamentoVO.getDataPrimeiroVencimento().isEmpty()) {
				Date data =
						Util.converterCampoStringParaData("Data", parcelamentoVO.getDataPrimeiroVencimento(), Constantes.FORMATO_DATA_BR);
				dataVencimento.setTime(data);
			}
			if (VALOR_NAO.equalsIgnoreCase(parcelamentoVO.getVencimentoInicial())
					&& VALOR_NAO.equalsIgnoreCase(parcelamentoVO.getCobrancaEmConta())) {
				dataVencimento.add(Calendar.MONTH, 1);
			}

			DateTime vencimentoInicial = new DateTime(dataVencimento.getTime());

			dados.put(VENCIMENTO_INICIAL, vencimentoInicial.toDate());
		}

		dados.put(PERIODICIDADE_PARCELAS, 30);
		if (!StringUtils.isEmpty(parcelamentoVO.getIndicadorNotaPromissoria())) {
			dados.put(INDICADOR_NOTA_PROMISSORIA, Boolean.valueOf(parcelamentoVO.getIndicadorNotaPromissoria()));
		}
		if (!StringUtils.isEmpty(parcelamentoVO.getIndicadorConfissaoDivida())) {
			dados.put(INDICADOR_CONFISSAO_DIVIDA, Boolean.valueOf(parcelamentoVO.getIndicadorConfissaoDivida()));
		}

		return dados;
	}

	/**
	 * Método responsável por salvar o parcelamento.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("salvarParcelamento")
	public String salvarParcelamento(HttpServletRequest request, Model model, ParcelamentoVO parcelamentoVO, BindingResult bindingResult) {

		String view = null;
		try {
			validarToken(request);
			view = this.salvar(request, model, parcelamentoVO, bindingResult);
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirPesquisaEfetuarParcelamentoDebitos";
		}

		return view;
	}
	
	private String salvar(HttpServletRequest request, Model model, ParcelamentoVO parcelamentoVO, BindingResult bindingResult) {
		
		String view = null;
		
		try {

			DadosGeraisParcelas dadosGerais = (DadosGeraisParcelas) request.getSession().getAttribute(DADOS_GERAIS_SESSAO);
			dadosGerais.setDadosAuditoria(getDadosAuditoria(request));

			String indicadorNotaPromissoria = parcelamentoVO.getIndicadorNotaPromissoria();
			String indicadorConfissaoDivida = parcelamentoVO.getIndicadorConfissaoDivida();

			String cobrancaEmConta = parcelamentoVO.getCobrancaEmConta();
			String paramTpCobranca = null;
			if (!StringUtils.isEmpty(cobrancaEmConta) && cobrancaEmConta.equals(Boolean.TRUE.toString())) {
				paramTpCobranca = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_FATURA);
			} else {
				String vencimentoInicial = parcelamentoVO.getVencimentoInicial();
				if (!StringUtils.isEmpty(vencimentoInicial) && vencimentoInicial.equals(Boolean.TRUE.toString())) {
					paramTpCobranca = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_NOTA_MES_ATUAL);
				} else {
					paramTpCobranca =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_NOTA_MES_POSTERIOR);
				}
			}

			EntidadeConteudo tipoCobranca = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramTpCobranca));
			dadosGerais.setTipoCobranca(tipoCobranca);

			byte[] relatoriosExibicao = controladorParcelamento.salvarParcelamento(dadosGerais,
					Boolean.parseBoolean(indicadorNotaPromissoria), Boolean.parseBoolean(indicadorConfissaoDivida));

			request.getSession().removeAttribute(DADOS_GERAIS_SESSAO);
			this.limparFormCondicoesNegociacao(parcelamentoVO);

			super.mensagemSucesso(model, Constantes.SUCESSO_EFETUAR_PARCELAMENTO, super.obterMensagem(Parcelamento.ROTULO_PARCELAMENTO));

			if (relatoriosExibicao != null) {
				model.addAttribute("gerarRelatorioConfissaoDividaNotaPromissoria", true);
				request.getSession().setAttribute(RELATORIO_EXIBICAO, relatoriosExibicao);
			}

			parcelamentoVO.setDataParcelamentoInicial(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));
			parcelamentoVO.setDataParcelamentoFinal(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));
			view = this.pesquisarParcelamentoDebitos(model, request, parcelamentoVO, bindingResult);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirDebitosParaParcelamento?aposValidacao=true";
		}
		
		return view;
	}

	private void limparFormCondicoesNegociacao(ParcelamentoVO parcelamentoVO) {

		parcelamentoVO.setIdPerfilParcelamento(Long.valueOf(0));
		parcelamentoVO.setValorTotalDebito(StringUtils.EMPTY);
		parcelamentoVO.setValorTotalLiquido(StringUtils.EMPTY);
		parcelamentoVO.setNumeroParcelas(StringUtils.EMPTY);
		parcelamentoVO.setCobrancaEmConta(VALOR_SIM);
		parcelamentoVO.setVencimentoInicial(StringUtils.EMPTY);
		parcelamentoVO.setPeriodicidadeParcelas(StringUtils.EMPTY);
		parcelamentoVO.setPercentualDesconto(StringUtils.EMPTY);
		parcelamentoVO.setValorDesconto(StringUtils.EMPTY);
		parcelamentoVO.setIndicadorNotaPromissoria(VALOR_NAO);
		parcelamentoVO.setIndicadorConfissaoDivida(VALOR_NAO);
		this.limparFormFiador(parcelamentoVO);
	}

	/**
	 * Método responsável por exibir o pdf do relatório de Notas Promissorias.
	 * 
	 * @param model - {@link Model}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return relatório - {@link ResponseEntity}
	 */
	@RequestMapping("imprimirNotasPromissorias")
	public ResponseEntity<byte[]> imprimirNotasPromissorias(Model model, ParcelamentoVO parcelamentoVO, BindingResult bindingResult)
			throws GGASException {

		return this.gerarRelatorioNotasPromissorias(model, parcelamentoVO);
	}

	private ResponseEntity<byte[]> gerarRelatorioNotasPromissorias(Model model, ParcelamentoVO parcelamentoVO) throws GGASException {

		Parcelamento parcelamento = (Parcelamento) controladorParcelamento.obter(parcelamentoVO.getChavePrimaria(), PARCELAMENTO_PERFIL,
				LISTA_PARCELAMENTO_ITEM);

		Collection<ParcelamentoItem> listaParcelamentoItem = null;
		if (parcelamento != null) {
			popularForm(parcelamentoVO, parcelamento);
			listaParcelamentoItem = parcelamento.getListaParcelamentoItem();
		}

		Collection<DadosParcelas> listaDadosParcelas = new ArrayList<DadosParcelas>();

		model.addAttribute(LISTA_PARCELAMENTO_ITEM, listaParcelamentoItem);

		Collection<Fatura> listaFatura = new ArrayList<Fatura>();
		Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<CreditoDebitoARealizar>();
		if (listaParcelamentoItem != null) {
			for (ParcelamentoItem parcelamentoItem : listaParcelamentoItem) {
				if (parcelamentoItem.getFaturaGeral() != null) {
					FaturaGeral faturaGeral = controladorArrecadacao.obterFaturaGeral(parcelamentoItem.getFaturaGeral().getChavePrimaria());
					Fatura fatura = (Fatura) controladorCobranca.obterFatura(faturaGeral.getFaturaAtual().getChavePrimaria(),
							SITUACAO_PAGAMENTO, TIPO_DOCUMENTO);
					if (fatura.getSituacaoPagamento() != null) {
						fatura.setSituacaoPagamento(
								controladorEntidadeConteudo.obterEntidadeConteudo(fatura.getSituacaoPagamento().getChavePrimaria()));
					}
					if (fatura.getTipoDocumento() != null) {
						fatura.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(fatura.getTipoDocumento().getChavePrimaria()));
					}
					listaFatura.add(fatura);
				}
				popularListaCreditoDebito(listaCreditoDebito, parcelamentoItem);
			}
		}
		model.addAttribute(LISTA_FATURA, listaFatura);
		model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebito);

		Collection<Fatura> listaParcelasFatura = null;
		if (parcelamento != null) {
			listaParcelasFatura = controladorFatura.consultarFaturasPorParcelamento(parcelamento.getChavePrimaria());
		}
		
		if (listaParcelasFatura != null && !listaParcelasFatura.isEmpty()) {
			popularListaDadosParcelasComDadosDeFatura(listaDadosParcelas, listaParcelasFatura);
		} else {
			popularListaDadosParcelasComDadosDeCreditoDebito(parcelamento, listaDadosParcelas);
		}

		byte[] relatorioNotasPromissorias =
				controladorParcelamento.gerarRelatorioNotaPromissoria(parcelamentoVO.getChavePrimaria(), listaDadosParcelas);

		ResponseEntity<byte[]> responseEntity = null;

		if (relatorioNotasPromissorias != null) {
			MediaType contentType = MediaType.parseMediaType(MEDIA_TYPE);
			String filename = "relatorioNotasPromissorias.pdf";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(contentType);
			headers.setContentDispositionFormData(HEADER_PARAM, filename);

			responseEntity = new ResponseEntity<byte[]>(relatorioNotasPromissorias, headers, HttpStatus.OK);
		}

		return responseEntity;
	}

	private void popularForm(ParcelamentoVO parcelamentoVO, Parcelamento parcelamento) throws GGASException {

		parcelamentoVO.setChavePrimaria(parcelamento.getChavePrimaria());
		parcelamentoVO.setDescricaoPerfilParcelamento(parcelamento.getParcelamentoPerfil().getDescricao());
		BigDecimal valorDesconto = parcelamento.getValorDesconto();
		String valorTotalDebito = parcelamento.getValorParcelado();

		if (parcelamento.getCliente() != null) {
			Cliente cliente = (Cliente) controladorCliente.obter(parcelamento.getCliente().getChavePrimaria(), "enderecos");
			parcelamentoVO.setIdCliente(cliente.getChavePrimaria());
			parcelamentoVO.setNomeCompletoCliente(cliente.getNome());
			parcelamentoVO.setDocumentoFormatado(cliente.getNumeroDocumentoFormatado());
			if (cliente.getEnderecoPrincipal() != null) {
				parcelamentoVO.setEnderecoFormatadoCliente(cliente.getEnderecoPrincipal().getEnderecoFormatado());
			}
			parcelamentoVO.setEmailCliente(cliente.getEmailPrincipal());
		}
		if (parcelamento.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo = parcelamento.getPontoConsumo();
			parcelamentoVO.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
		}
		if (valorTotalDebito != null) {
			parcelamentoVO.setValorTotalDebito(valorTotalDebito);
		}
		if (parcelamento.getDataVencimentoInicial() != null) {
			parcelamentoVO.setVencimentoInicial(
					Util.converterDataParaStringSemHora(parcelamento.getDataVencimentoInicial(), Constantes.FORMATO_DATA_BR));
		}
		if (parcelamento.getNumeroDiasPeriodicidade() != null) {
			parcelamentoVO.setPeriodicidadeParcelas(String.valueOf(parcelamento.getNumeroDiasPeriodicidade()));
		}
		if (valorDesconto != null) {
			parcelamentoVO.setValorDesconto(Util.converterCampoCurrencyParaString(valorDesconto, Constantes.LOCALE_PADRAO));
		}

		if (parcelamento.getValorLiquido() != null) {
			parcelamentoVO.setValorTotalLiquido(parcelamento.getValorLiquido());
		}

		if (parcelamento.getNumeroPrestacoes() != null) {
			parcelamentoVO.setNumeroParcelas(String.valueOf(parcelamento.getNumeroPrestacoes()));
		}

		parcelamentoVO.setIndicadorNotaPromissoria(String.valueOf(parcelamento.isEmissaoNotasPromissorias()));
		parcelamentoVO.setIndicadorConfissaoDivida(String.valueOf(parcelamento.isEmissaoTermoParcelamento()));

		if (parcelamento.getFiador() != null) {
			Cliente fiador = (Cliente) controladorCliente.obter(parcelamento.getFiador().getChavePrimaria(), "enderecos");
			parcelamentoVO.setNomeFiador(fiador.getNome());
			parcelamentoVO.setDocumentoFiador(fiador.getNumeroDocumentoFormatado());
			if (fiador.getEnderecoPrincipal() != null) {
				parcelamentoVO.setEnderecoFiador(fiador.getEnderecoPrincipal().getEnderecoFormatado());
			}
			parcelamentoVO.setEmailFiador(fiador.getEmailPrincipal());
		}
	}

	/**
	 * Método responsável por exibir o pdf do relatório de Notas Promissorias.
	 * 
	 * @param model - {@link Model}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return relatório - {@link ResponseEntity}
	 */
	@RequestMapping("imprimirConfissaoDividas")
	public ResponseEntity<byte[]> imprimirConfissaoDividas(Model model, ParcelamentoVO parcelamentoVO, BindingResult bindingResult)
			throws GGASException {

		return this.gerarRelatorioConfissaoDividas(model, parcelamentoVO);
	}

	public ResponseEntity<byte[]> gerarRelatorioConfissaoDividas(Model model, ParcelamentoVO parcelamentoVO) throws GGASException {

		Long chavePrimaria = parcelamentoVO.getChavePrimaria();
		Parcelamento parcelamento =
				(Parcelamento) controladorParcelamento.obter(chavePrimaria, PARCELAMENTO_PERFIL, LISTA_PARCELAMENTO_ITEM);

		Collection<ParcelamentoItem> listaParcelamentoItem = null;
		if (parcelamento != null) {
			popularForm(parcelamentoVO, parcelamento);
			listaParcelamentoItem = parcelamento.getListaParcelamentoItem();
		}

		Collection<DadosParcelas> listaDadosParcelas = new ArrayList<DadosParcelas>();

		model.addAttribute(LISTA_PARCELAMENTO_ITEM, listaParcelamentoItem);

		Collection<Fatura> listaFatura = new ArrayList<Fatura>();
		Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<CreditoDebitoARealizar>();

		if (listaParcelamentoItem != null) {
			for (ParcelamentoItem parcelamentoItem : listaParcelamentoItem) {
				popularListaFatura(listaFatura, parcelamentoItem);
				popularListaCreditoDebito(listaCreditoDebito, parcelamentoItem);
			}
		}

		model.addAttribute(LISTA_FATURA, listaFatura);
		model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebito);

		Collection<Fatura> listaParcelasFatura = null;
		if (parcelamento != null) {
			listaParcelasFatura = controladorFatura.consultarFaturasPorParcelamento(parcelamento.getChavePrimaria());
		}

		if (listaParcelasFatura != null && !listaParcelasFatura.isEmpty()) {
			popularListaDadosParcelasComDadosDeFatura(listaDadosParcelas, listaParcelasFatura);
		} else {
			popularListaDadosParcelasComDadosDeCreditoDebito(parcelamento, listaDadosParcelas);
		}

		byte[] relatorioConfissaoDividas =
				controladorParcelamento.gerarRelatorioConfissaoDividas(chavePrimaria, listaDadosParcelas, listaFatura);
		ResponseEntity<byte[]> responseEntity = null;

		if (relatorioConfissaoDividas != null) {

			MediaType contentType = MediaType.parseMediaType(MEDIA_TYPE);
			String filename = "relatorioConfissaoDividas.pdf";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(contentType);
			headers.setContentDispositionFormData(HEADER_PARAM, filename);

			responseEntity = new ResponseEntity<byte[]>(relatorioConfissaoDividas, headers, HttpStatus.OK);
		}

		return responseEntity;
	}

	private void popularListaDadosParcelasComDadosDeCreditoDebito(Parcelamento parcelamento, Collection<DadosParcelas> listaDadosParcelas)
			throws NegocioException {
		DadosParcelas dadosParcelas = null;
		Collection<CreditoDebitoARealizar> listaParcelasCreditoDebito =
				controladorCreditoDebito.consultarCreditoDebitoARealizarPorParcelamento(parcelamento.getChavePrimaria());
		Integer ultimoNumeroParcela = 0;
		for (CreditoDebitoARealizar creditoDebitoARealizar : listaParcelasCreditoDebito) {
			dadosParcelas = (DadosParcelas) controladorParcelamento.criarDadosParcelas();
			if (!ultimoNumeroParcela.equals(creditoDebitoARealizar.getNumeroPrestacao())) {
				ultimoNumeroParcela = creditoDebitoARealizar.getNumeroPrestacao();
				dadosParcelas.setNumeroParcela(creditoDebitoARealizar.getNumeroPrestacao());
				dadosParcelas.setTotal(creditoDebitoARealizar.getValor());
				listaDadosParcelas.add(dadosParcelas);
			}
		}
	}

	private void popularListaDadosParcelasComDadosDeFatura(Collection<DadosParcelas> listaDadosParcelas,
			Collection<Fatura> listaParcelasFatura) throws NegocioException {
		DadosParcelas dadosParcelas = null;
		int contador = 1;
		for (Fatura fatura : listaParcelasFatura) {
			dadosParcelas = (DadosParcelas) controladorParcelamento.criarDadosParcelas();
			dadosParcelas.setNumeroParcela(contador);

			BigDecimal valorSaldoFatura = controladorFatura.calcularSaldoFatura(fatura);
			if (valorSaldoFatura != null) {
				dadosParcelas.setTotal(valorSaldoFatura);
			}
			if (fatura.getDataVencimento() != null) {
				dadosParcelas.setDataVencimento(fatura.getDataVencimento());
			}
			listaDadosParcelas.add(dadosParcelas);
			contador++;
		}
	}

	private void popularListaFatura(Collection<Fatura> listaFatura, ParcelamentoItem parcelamentoItem) throws NegocioException {
		if (parcelamentoItem.getFaturaGeral() != null) {
			FaturaGeral faturaGeral = controladorArrecadacao.obterFaturaGeral(parcelamentoItem.getFaturaGeral().getChavePrimaria());
			Fatura fatura = (Fatura) controladorCobranca.obterFatura(faturaGeral.getFaturaAtual().getChavePrimaria(), SITUACAO_PAGAMENTO,
					TIPO_DOCUMENTO);
			if (fatura.getSituacaoPagamento() != null) {
				fatura.setSituacaoPagamento(
						controladorEntidadeConteudo.obterEntidadeConteudo(fatura.getSituacaoPagamento().getChavePrimaria()));
			}
			if (fatura.getTipoDocumento() != null) {
				fatura.setTipoDocumento(controladorCobranca.obterTipoDocumento(fatura.getTipoDocumento().getChavePrimaria()));
			}
			listaFatura.add(fatura);
		}
	}

	private void popularListaCreditoDebito(Collection<CreditoDebitoARealizar> listaCreditoDebito, ParcelamentoItem parcelamentoItem)
			throws NegocioException {
		if (parcelamentoItem.getCreditoDebitoARealizar() != null) {
			CreditoDebitoARealizar creditoDebitoARealizar = (CreditoDebitoARealizar) controladorCreditoDebito
					.obter(parcelamentoItem.getCreditoDebitoARealizar().getChavePrimaria(), CREDITO_DEBITO_NEGOCIADO);
			if (creditoDebitoARealizar.getCreditoDebitoNegociado().getRubrica() != null) {
				creditoDebitoARealizar.getCreditoDebitoNegociado().setRubrica((Rubrica) controladorRubrica
						.obter(creditoDebitoARealizar.getCreditoDebitoNegociado().getRubrica().getChavePrimaria()));
			}
			if (creditoDebitoARealizar.getCreditoDebitoSituacao() != null) {
				creditoDebitoARealizar.setCreditoDebitoSituacao(controladorCobranca
						.obterCreditoDebitoSituacao(creditoDebitoARealizar.getCreditoDebitoSituacao().getChavePrimaria()));
			}
			if (creditoDebitoARealizar.getSituacaoPagamento() != null) {
				creditoDebitoARealizar.setSituacaoPagamento(controladorEntidadeConteudo
						.obterEntidadeConteudo(creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria()));
			}
			listaCreditoDebito.add(creditoDebitoARealizar);
		}
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de um Parcelamento.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoParcelamentoDebitos")
	public String exibirDetalhamentoParcelamentoDebitos(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO) {

		String view = "exibirDetalhamentoParcelamentoDebitos";
		try {
			this.detalharParcelamentoDebitos(request, parcelamentoVO);
			String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
			String statusNaoAutorizado =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);

			model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
			model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));
			model.addAttribute(PARCELAMENTO_VO, parcelamentoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/pesquisarParcelamentoDebitos";
		}

		return view;
	}

	private void detalharParcelamentoDebitos(HttpServletRequest request, ParcelamentoVO parcelamentoVO) throws GGASException {

		Long chavePrimaria = parcelamentoVO.getChavePrimaria();

		Parcelamento parcelamento = (Parcelamento) controladorParcelamento.obter(chavePrimaria, PARCELAMENTO_PERFIL,
				LISTA_PARCELAMENTO_ITEM, "ultimoUsuarioAlteracao", STATUS);

		// Verificando permissão para autorização
		// de pendência pela alçada do usuário
		// logado
		Usuario usuarioLogado = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		parcelamentoVO.setHasPermissaoAutorizar(controladorAlcada.possuiAlcadaAutorizacaoParcelamento(usuarioLogado, parcelamento));

		Collection<ParcelamentoItem> listaParcelamentoItem = null;
		if (parcelamento != null) {
			popularForm(parcelamentoVO, parcelamento);
			listaParcelamentoItem = parcelamento.getListaParcelamentoItem();
		}

		request.setAttribute(LISTA_PARCELAMENTO_ITEM, listaParcelamentoItem);

		Collection<Fatura> listaFatura = new ArrayList<Fatura>();
		Collection<Long> colecaoChavesCreditoDebitoNegociado = new ArrayList<Long>();
		if (listaParcelamentoItem != null) {
			for (ParcelamentoItem parcelamentoItem : listaParcelamentoItem) {
				if (parcelamentoItem.getFaturaGeral() != null) {
					FaturaGeral faturaGeral = controladorArrecadacao.obterFaturaGeral(parcelamentoItem.getFaturaGeral().getChavePrimaria());
					Fatura fatura = (Fatura) controladorCobranca.obterFatura(faturaGeral.getFaturaAtual().getChavePrimaria(),
							SITUACAO_PAGAMENTO, TIPO_DOCUMENTO);
					if (fatura.getSituacaoPagamento() != null) {
						fatura.setSituacaoPagamento(
								controladorEntidadeConteudo.obterEntidadeConteudo(fatura.getSituacaoPagamento().getChavePrimaria()));
					}
					if (fatura.getTipoDocumento() != null) {
						fatura.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(fatura.getTipoDocumento().getChavePrimaria()));
					}

					fatura.setValorConciliado(controladorFatura.calcularSaldoFatura(fatura));

					listaFatura.add(fatura);
				}

				if (parcelamentoItem.getCreditoDebitoARealizar() != null) {
					CreditoDebitoARealizar creditoDebitoARealizar = (CreditoDebitoARealizar) controladorCobranca
							.obter(parcelamentoItem.getCreditoDebitoARealizar().getChavePrimaria(), CREDITO_DEBITO_NEGOCIADO);

					if (!colecaoChavesCreditoDebitoNegociado
							.contains(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria())) {
						colecaoChavesCreditoDebitoNegociado.add(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria());
					}
				}
			}
		}
		request.setAttribute(LISTA_FATURA, listaFatura);

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long[] chavesCreditoDebitoNegociado = new Long[colecaoChavesCreditoDebitoNegociado.size()];
		int iCont = 0;
		for (Long chaveCreditoDebitoNegociado : colecaoChavesCreditoDebitoNegociado) {
			chavesCreditoDebitoNegociado[iCont] = chaveCreditoDebitoNegociado;
			iCont++;
		}

		if (chavesCreditoDebitoNegociado.length > 0) {
			filtro.put("chavesPrimarias", chavesCreditoDebitoNegociado);

			// Restrição de tarifa autorizada
			// (Alçada)
			String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
			if (statusAutorizado != null) {
				filtro.put(STATUS, Long.valueOf(statusAutorizado));
			}

			Collection<Map<String, Object>> listaExibicaoCreditoDebito =
					controladorCreditoDebito.consultarPesquisaCreditoDebitoParcelamento(filtro, false);
			request.setAttribute(LISTA_CREDITO_DEBITO, listaExibicaoCreditoDebito);
		}

		// Recupera as Parcelas do Parcelamento
		Collection<ParcelaVO> listaParcelas = null;
		if (parcelamento != null) {
			listaParcelas = controladorParcelamento.consultarParcelamentosNotaDebito(parcelamento.getChavePrimaria());
		}

		if (listaParcelas != null && !listaParcelas.isEmpty()) {
			parcelamentoVO.setCobrancaEmConta(VALOR_NAO);
			request.setAttribute("mostrarColunaDataVencimento", true);
		} else {
			listaParcelas = controladorParcelamento.consultarParcelamentosCreditoARealizar(parcelamento.getChavePrimaria());
			parcelamentoVO.setCobrancaEmConta(VALOR_SIM);
			request.setAttribute("mostrarColunaDataVencimento", false);
		}
		request.setAttribute(LISTA_PARCELAMENTO_ITEM, listaParcelas);
	}

	/**
	 * Método responsável por autorizar um Parcelamento.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("autorizarParcelamento")
	public String autorizarParcelamento(HttpServletRequest request, Model model, ParcelamentoVO parcelamentoVO) {

		try {

			Long chavePrimaria = parcelamentoVO.getChavePrimaria();
			Long autorizado = parcelamentoVO.getStatus();

			if (autorizado != null) {

				EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(autorizado);

				if (status != null) {
					controladorParcelamento.autorizarParcelamento(chavePrimaria, status, getDadosAuditoria(request));
					super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA,
							super.obterMensagem(Parcelamento.ROTULO_PARCELAMENTO));
				}
			}

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirDetalhamentoParcelamentoDebitos(model, request, parcelamentoVO);
	}

	/**
	 * Responsável em gerar o relatório de confissão de dívida e/ou nota promissória após salvar as parcelas.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("gerarRelatorioConfissaoDividaNotaPromissoria")
	public ResponseEntity<byte[]> gerarRelatorioConfissaoDividaNotaPromissoria(HttpServletRequest request) {

		byte[] relatorioExibicao = (byte[]) request.getSession().getAttribute(RELATORIO_EXIBICAO);

		ResponseEntity<byte[]> responseEntity = null;
		if (relatorioExibicao != null) {
			MediaType contentType = MediaType.parseMediaType(MEDIA_TYPE);
			String filename = "relatorio.pdf";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(contentType);
			headers.setContentDispositionFormData(HEADER_PARAM, filename);

			responseEntity = new ResponseEntity<byte[]>(relatorioExibicao, headers, HttpStatus.OK);
		}

		request.removeAttribute("gerarRelatorioConfissaoDividaNotaPromissoria");
		request.getSession().removeAttribute(RELATORIO_EXIBICAO);

		return responseEntity;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de débitos do cliente.
	 * 
	 * @param model - Model
	 * @param request - {@link HttpServletRequest}
	 * @param parcelamentoVO - {@link ParcelamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("cancelarDebitosParaParcelamento")
	public String cancelarDebitosParaParcelamento(Model model, HttpServletRequest request, ParcelamentoVO parcelamentoVO,
			BindingResult bindingResult) {

		this.limparFormCondicoesNegociacao(parcelamentoVO);
		request.getSession().removeAttribute(DADOS_GERAIS_SESSAO);

		return this.exibirDebitosParaParcelamento(model, request, parcelamentoVO, bindingResult, Boolean.FALSE);
	}

}
