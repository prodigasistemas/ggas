/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.extratodebito;

import java.io.Serializable;

public class ExtratoDebitoFaturaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4546706690107286502L;

	private long idFatura;

	private String cicloReferencia;

	private String valor;

	private String dataEmissao;

	private String dataVencimento;

	private String tipoDocumento;

	private String situacaoPagamento;

	private long idPontoConsumo;

	private long idTipoDocumento;

	private String saldo;
	
	private String saldoCorrigido;

	private boolean indicadorPontoConsumo;

	/**
	 * @return the idFatura
	 */
	public long getIdFatura() {

		return idFatura;
	}

	/**
	 * @param idFatura
	 *            the idFatura to set
	 */
	public void setIdFatura(long idFatura) {

		this.idFatura = idFatura;
	}

	/**
	 * @return the cicloReferencia
	 */
	public String getCicloReferencia() {

		return cicloReferencia;
	}

	/**
	 * @param cicloReferencia
	 *            the cicloReferencia to set
	 */
	public void setCicloReferencia(String cicloReferencia) {

		this.cicloReferencia = cicloReferencia;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor) {

		this.valor = valor;
	}

	/**
	 * @return the dataEmissao
	 */
	public String getDataEmissao() {

		return dataEmissao;
	}

	/**
	 * @param dataEmissao
	 *            the dataEmissao to set
	 */
	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the dataVencimento
	 */
	public String getDataVencimento() {

		return dataVencimento;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	public void setDataVencimento(String dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {

		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the situacaoPagamento
	 */
	public String getSituacaoPagamento() {

		return situacaoPagamento;
	}

	/**
	 * @param situacaoPagamento
	 *            the situacaoPagamento to set
	 */
	public void setSituacaoPagamento(String situacaoPagamento) {

		this.situacaoPagamento = situacaoPagamento;
	}

	/**
	 * @return the idPontoConsumo
	 */
	public long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the idTipoDocumento
	 */
	public long getIdTipoDocumento() {

		return idTipoDocumento;
	}

	/**
	 * @param idTipoDocumento
	 *            the idTipoDocumento to set
	 */
	public void setIdTipoDocumento(long idTipoDocumento) {

		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {

		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {

		this.saldo = saldo;
	}

	/**
	 * @param indicadorPontoConsumo
	 *            the indicadorPontoConsumo to set
	 */
	public void setIndicadorPontoConsumo(boolean indicadorPontoConsumo) {

		this.indicadorPontoConsumo = indicadorPontoConsumo;
	}

	/**
	 * @return indicadorPontoConsumo
	 */
	public boolean getIndicadorPontoConsumo() {

		return indicadorPontoConsumo;
	}

	public String getSaldoCorrigido() {
		return saldoCorrigido;
	}

	public void setSaldoCorrigido(String saldoCorrigido) {
		this.saldoCorrigido = saldoCorrigido;
	}
	
}
