package br.com.ggas.web.cobranca.negativacao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cobranca.negativacao.AgenteNegativador;
import br.com.ggas.cobranca.negativacao.ControladorNegativacao;
import br.com.ggas.cobranca.negativacao.impl.AgenteNegativadorImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

@Controller
public class AgenteNegativadorAction extends GenericAction {

	private static final String AGENTE_NEGATIVADOR = "Agente Negativador";
	/**
	 * Controlador parametros sistema
	 */
	@Autowired
	@Qualifier("controladorCliente")
	ControladorCliente controladorCliente;

	/**
	 * Controlador negativação
	 */
	@Autowired
	private ControladorNegativacao controladorNegativacao;

	@RequestMapping("exibirPesquisaAgenteNegativador")
	public String exibirPesquisaAgenteNegativador(AgenteNegativadorVO agenteNegativadorVO, HttpServletRequest request,
			Model model) {

		return "exibirPesquisaAgenteNegativador";
	}

	@RequestMapping("exibirIncluirAgenteNegativador")
	public String exibirIncluirAgenteNegativador(HttpServletRequest request, Model model) {

		return "exibirIncluirAgenteNegativador";
	}

	@RequestMapping("exibirAlterarAgenteNegativador")
	public String exibirAlterarAgenteNegativador(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		AgenteNegativador agenteNegativador = controladorNegativacao.obterAgenteNegativador(chavePrimaria);

		model.addAttribute("agenteNegativador", agenteNegativador);

		return "exibirAlterarAgenteNegativador";
	}

	@RequestMapping("incluirAgenteNegativador")
	public String incluirAgenteNegativador(
			@ModelAttribute("AgenteNegativadorImpl") AgenteNegativadorImpl agenteNegativadorImpl,
			HttpServletRequest request, Model model) throws ConcorrenciaException {

		try {
			if (agenteNegativadorImpl.isIndicadorPrioritario()) {
				AgenteNegativador agenteNegativador = controladorNegativacao.obterAgenteNegativadorPrioritario();
				if (agenteNegativador != null) {
					agenteNegativador.setIndicadorPrioritario(false);

					controladorNegativacao.atualizar(agenteNegativador);
				}
			}

			popularAgenteNegativador(request, agenteNegativadorImpl);

			Map<String, Object> erros = agenteNegativadorImpl.validarDados();
			if (erros != null && !erros.isEmpty()) {
				throw new NegocioException(erros);
			}
			
			if(controladorNegativacao.verificarCadastroAgenteNegativador(agenteNegativadorImpl)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_AGENTE_MESMO_PERIODO, true);
			}
			
			long chaveAgenteInserido = controladorNegativacao.inserir(agenteNegativadorImpl);
			Collection<AgenteNegativador> listaAgenteNegativador = new HashSet<>();
			listaAgenteNegativador.add(controladorNegativacao.obterAgenteNegativador(chaveAgenteInserido));
			model.addAttribute("listaAgenteNegativador", listaAgenteNegativador);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, AGENTE_NEGATIVADOR);
		} catch (NegocioException e) {
			if (agenteNegativadorImpl.getCliente() != null) {
				model.addAttribute("cliente", agenteNegativadorImpl.getCliente());
			}
			model.addAttribute("agenteNegativador", agenteNegativadorImpl);
			// Lançar mensagem de erro
			mensagemErroParametrizado(model, e);
			return exibirIncluirAgenteNegativador(request, model);
		}

		AgenteNegativadorVO agenteNegativadorVO = new AgenteNegativadorVO();
		agenteNegativadorVO.setNomeCliente(agenteNegativadorImpl.getCliente().getNome());

		return exibirPesquisaAgenteNegativador(agenteNegativadorVO, request, model);
	}

	@RequestMapping("pesquisarAgenteNegativador")
	public String pesquisarAgenteNegativador(AgenteNegativadorVO agenteNegativadorVO, BindingResult result,
			HttpServletRequest request, Model model) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, agenteNegativadorVO);

		Collection<AgenteNegativador> listaAgenteNegativador = controladorNegativacao.consultaAgenteNegativador(filtro);
		model.addAttribute("listaAgenteNegativador", listaAgenteNegativador);
		request.getSession().setAttribute("listaAgenteNegativadorReq", listaAgenteNegativador);

		return exibirPesquisaAgenteNegativador(agenteNegativadorVO, request, model);
	}

	@RequestMapping("alterarAgenteNegativador")
	public String alterarAgenteNegativador(
			@ModelAttribute("AgenteNegativadorImpl") AgenteNegativadorImpl agenteNegativador, BindingResult result,
			@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
			throws ConcorrenciaException, NegocioException {

		try {

			if (agenteNegativador.isIndicadorPrioritario()) {
				AgenteNegativador agenteNegativadorPrioritario = controladorNegativacao
						.obterAgenteNegativadorPrioritario();

				if (agenteNegativadorPrioritario != null) {
					agenteNegativadorPrioritario.setIndicadorPrioritario(false);

					controladorNegativacao.atualizar(agenteNegativadorPrioritario);
				}
			}
			popularAgenteNegativador(request, agenteNegativador);

			Map<String, Object> erros = agenteNegativador.validarDados();
			if (erros != null && !erros.isEmpty()) {
				throw new NegocioException(erros);
			}
			
			if(controladorNegativacao.verificarCadastroAgenteNegativador(agenteNegativador)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_AGENTE_MESMO_PERIODO, true);
			}

			agenteNegativador.setHabilitado(true);
			agenteNegativador.setDadosAuditoria(getDadosAuditoria(request));
			controladorNegativacao.atualizar(agenteNegativador);
			model.addAttribute("chavePrimaria", agenteNegativador.getChavePrimaria());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, AGENTE_NEGATIVADOR);
		} catch (NegocioException e) {
			if (agenteNegativador.getCliente() != null) {
				model.addAttribute("cliente", agenteNegativador.getCliente());
			}
			model.addAttribute("agenteNegativador", agenteNegativador);
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
			return exibirAlterarAgenteNegativador(agenteNegativador.getChavePrimaria(), request, model);
		}

		Collection<AgenteNegativador> listaAgenteNegativador = new HashSet<>();
		listaAgenteNegativador.add(agenteNegativador);
		model.addAttribute("listaAgenteNegativador", listaAgenteNegativador);


		AgenteNegativadorVO agenteNegativadorVO = new AgenteNegativadorVO();
		agenteNegativadorVO.setNomeCliente(agenteNegativador.getCliente().getNome());
		return exibirPesquisaAgenteNegativador(agenteNegativadorVO, request, model);
	}

	@RequestMapping("removerAgenteNegativador")
	public String removerAgenteNegativador(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) {
		Collection<AgenteNegativador> listaAgenteNegativador = (Collection<AgenteNegativador>) request.getSession()
				.getAttribute("listaAgenteNegativadorReq");

		try {

			controladorNegativacao.remover(chavesPrimarias, getDadosAuditoria(request));

			for (Long chavePrimariaRemovida : chavesPrimarias) {
				listaAgenteNegativador.removeIf(e -> e.getChavePrimaria() == chavePrimariaRemovida);
			}

			model.addAttribute("listaAgenteNegativador", listaAgenteNegativador);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, AgenteNegativador.AGENTE_NEGATIVADOR);
		} catch (Exception e) {
			model.addAttribute("listaAgenteNegativador", listaAgenteNegativador);
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_AGENTE_NEGATIVADOR_EM_USO, e));
		}

		return exibirPesquisaAgenteNegativador(new AgenteNegativadorVO(), request, model);
	}

	private void popularAgenteNegativador(HttpServletRequest request, AgenteNegativadorImpl agenteNegativador)
			throws NumberFormatException, NegocioException {

		String valorContrato = request.getParameter("valorContratoForm");
		if (!valorContrato.isEmpty()) {
			valorContrato = valorContrato.replace(".", "");
			agenteNegativador.setValorContrato(new BigDecimal(valorContrato.replace(",", ".")));
		}

		String valorTarifaInclusaoForm = request.getParameter("valorTarifaInclusaoForm");
		if (!valorTarifaInclusaoForm.isEmpty()) {
			valorTarifaInclusaoForm = valorTarifaInclusaoForm.replace(".", "");
			agenteNegativador.setValorTarifaInclusao(new BigDecimal(valorTarifaInclusaoForm.replace(",", ".")));
		}

		String chaveCliente = request.getParameter("chaveCliente");
		if (!chaveCliente.isEmpty()) {
			agenteNegativador.setCliente(controladorCliente.obterCliente(Long.valueOf(chaveCliente)));
		}

		String dataFimContrato = request.getParameter("dataFimContratoForm");
		if (!dataFimContrato.isEmpty()) {
			agenteNegativador.setDataFimContrato(DataUtil.converterParaData(dataFimContrato));
		}

		String dataInicioContrato = request.getParameter("dataInicioContratoForm");
		if (!dataInicioContrato.isEmpty()) {
			agenteNegativador.setDataInicioContrato(DataUtil.converterParaData(dataInicioContrato));
		}
        
		if((!dataFimContrato.isEmpty() && !dataInicioContrato.isEmpty()) && 
				(agenteNegativador.getDataInicioContrato().after(agenteNegativador.getDataFimContrato()))) {
			
			throw new NegocioException(Constantes.ERRO_NEGOCIO_AGENTE_DATAS_FIM_MENOR, true);
			
		}
		
	}

	@RequestMapping("carregarClienteAgenteNegativador")
	public ModelAndView carregarClienteAgenteNegativador(
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria, HttpServletResponse response,
			HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridDivClientesAgente");
		Cliente cliente = null;
		try {
			if (chavePrimaria != null) {
				cliente = (Cliente) controladorCliente.obter(chavePrimaria);

			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView("ajaxErro");
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}
		model.addObject("cliente", cliente);

		return model;
	}

	private void prepararFiltro(Map<String, Object> filtro, AgenteNegativadorVO agenteNegativadorVO) {
		if (agenteNegativadorVO.getNomeCliente() != null && !agenteNegativadorVO.getNomeCliente().isEmpty()) {
			filtro.put("nomeCliente", agenteNegativadorVO.getNomeCliente());
		}

		if (agenteNegativadorVO.getNumeroContrato() != null && !agenteNegativadorVO.getNumeroContrato().isEmpty()) {
			filtro.put("numeroContrato", agenteNegativadorVO.getNumeroContrato());
		}

		if (agenteNegativadorVO.getIndicadorPrioritario() != null) {
			filtro.put("indicadorPrioritario", agenteNegativadorVO.getIndicadorPrioritario());
		}
	}
}
