/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cobranca.acaocobranca;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cobranca.acao.AcaoCobranca;
import br.com.ggas.cobranca.acao.ControladorAcaoCobranca;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * @author edmilson.santana
 *
 * Classe responsável pelos métodos que gerenciam as telas relacionadas a Acao Cobranca
 */
@Controller
public class AcaoCobrancaAction extends GenericAction {

	private static final String LISTA_ACAO_COBRANCA = "listaAcaoCobranca";

	private static final String DESCRICAO = "descricao";

	private static final String TIPO_DOCUMENTO = "tipoDocumentoGerado";

	private static final String NUMERO_DIAS_VALIDADE_ACAO = "numeroDiasValidadeAcao";

	private static final String NUMERO_DIAS_VENCIMENTO = "numeroDiasVencimento";

	private static final String GERAR_ORDEM_SERVICO = "gerarOrdemServico";

	private static final String CONSIDERA_DEBITOS_VENCER = "consideraDebitosVencer";

	private static final String BLOQUEAR_EMISSAO_DOCUMENTOS_PAGAVEIS = "bloquearEmissaoDocumentosPagaveis";

	@Autowired
	private ControladorAcaoCobranca controladorAcaoCobranca;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Método responsável por exibir a página de Pesquisa de Açoes de Cobrança.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model   - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAcaoCobranca")
	public String exibirPesquisaAcaoCobranca(HttpServletRequest request, Model model) {

		try {
			this.carregarCampos(model);
			saveToken(request);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaAcaoCobranca";
	}

	/**
	 * Método responsável por exibir a página de inclusão de Acões de Cobrança.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model -  {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirInclusaoAcaoCobranca")
	public String exibirInclusaoAcaoCobranca(HttpServletRequest request, Model model) {

		String view = "exibirInclusaoAcaoCobranca";
		try {
			this.carregarCampos(model);

			model.addAttribute(LISTA_ACAO_COBRANCA, controladorAcaoCobranca.listarAcaoCobranca());

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a página de detalhamento de uma Ação de
	 * Cobrança.
	 * 
	 * @param model         - {@link Model}
	 * @param chavePrimaria - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoAcaoCobranca")
	public String exibirDetalhamentoAcaoCobranca(Model model, @RequestParam("chavePrimaria") Long chavePrimaria) {

		try {
			AcaoCobranca acaoCobranca = controladorAcaoCobranca.obterAcaoCobranca(chavePrimaria);

			if (acaoCobranca != null) {
				model.addAttribute("acaoCobrancaVO", popularForm(acaoCobranca));
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirDetalhamentoAcaoCobranca";
	}

	/**
	 * Método responsável por pesquisar Açoes de Cobrança.
	 * 
	 * @param request        - {@link HttpServletRequest}
	 * @param model          - {@link Model}
	 * @param acaoCobrancaVO - {@link AcaoCobrancaVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarAcaoCobranca")
	public String pesquisarAcaoCobranca(HttpServletRequest request, Model model, AcaoCobrancaVO acaoCobrancaVO) {

		model.addAttribute("acaoCobrancaVO", acaoCobrancaVO);

		try {
			acaoCobrancaVO.setChavesPrimarias(null);

			Map<String, Object> filtro = new HashMap<String, Object>();

			prepararFiltro(filtro, acaoCobrancaVO);

			Collection<AcaoCobranca> listaAcaoCobranca = controladorAcaoCobranca.consultarAcoesCobranca(filtro);

			model.addAttribute(LISTA_ACAO_COBRANCA, listaAcaoCobranca);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return this.exibirPesquisaAcaoCobranca(request, model);
	}

	/**
	 * Incluir acao cobranca.
	 * 
	 * @param request        - {@link HttpServletRequest}
	 * @param model          - {@link Model}
	 * @param acaoCobrancaVO - {@link AcaoCobrancaVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirAcaoCobranca")
	public String incluirAcaoCobranca(HttpServletRequest request, Model model, AcaoCobrancaVO acaoCobrancaVO) {

		String view = null;

		model.addAttribute("acaoCobrancaVO", acaoCobrancaVO);

		try {
			AcaoCobranca acaoCobranca = (AcaoCobranca) controladorAcaoCobranca.criar();

			popularAcaoCobranca(acaoCobranca, acaoCobrancaVO);

			acaoCobranca.setDadosAuditoria(getDadosAuditoria(request));
			acaoCobranca.setHabilitado(true);

			Long idAcaoCobranca = controladorAcaoCobranca.inserir(acaoCobranca);
			acaoCobrancaVO.setChavePrimaria(idAcaoCobranca);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA,
					super.obterMensagem(AcaoCobranca.ACAO_COBRANCA));
			view = this.pesquisarAcaoCobranca(request, model, acaoCobrancaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = this.exibirInclusaoAcaoCobranca(request, model);
		}
		return view;
	}

	/**
	 * Método responsável por exibir uma tela de alteração de Ação de Cobrança.
	 * 
	 * @param request        - {@link HttpServletRequest}
	 * @param model          - {@link Model}
	 * @param acaoCobrancaVO - {@link AcaoCobrancaVO}
	 * @return view - {@link String}
	 * @throws Exception - {@link Exception}
	 */
	@RequestMapping("exibirAlteracaoAcaoCobranca")
	public String exibirAlteracaoAcaoCobranca(HttpServletRequest request, Model model, AcaoCobrancaVO acaoCobrancaVO)
			throws Exception {

		String view = "exibirAlteracaoAcaoCobranca";

		model.addAttribute("acaoCobrancaVO", acaoCobrancaVO);

		try {
			Long chavePrimaria = acaoCobrancaVO.getChavePrimaria();
			AcaoCobranca acaoCobranca = controladorAcaoCobranca.obterAcaoCobranca(chavePrimaria);

			if (!super.isPostBack(request)) {
				model.addAttribute("acaoCobrancaVO", popularForm(acaoCobranca));
			}

			model.addAttribute(LISTA_ACAO_COBRANCA, controladorAcaoCobranca.listarAcaoCobranca());

			carregarCampos(model);

			saveToken(request);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
			view = "exibirPesquisaAcaoCobranca";
		}

		return view;
	}

	/**
	 * Método responsável por alterar uma Ação de Cobrança.
	 * 
	 * @param request        - {@link HttpServletRequest}
	 * @param model          - {@link Model}
	 * @param acaoCobrancaVO - {@link AcaoCobrancaVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarAcaoCobranca")
	public String alterarAcaoCobranca(HttpServletRequest request, Model model, AcaoCobrancaVO acaoCobrancaVO) {

		String view = "forward:/exibirAlteracaoAcaoCobranca";
		try {

			validarToken(request);

			Long chavePrimaria = acaoCobrancaVO.getChavePrimaria();

			AcaoCobranca acaoCobranca = controladorAcaoCobranca.obterAcaoCobranca(chavePrimaria);

			popularAcaoCobranca(acaoCobranca, acaoCobrancaVO);

			acaoCobranca.setDadosAuditoria(getDadosAuditoria(request));
			acaoCobranca.setHabilitado(true);

			controladorAcaoCobranca.atualizar(acaoCobranca);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA,
					super.obterMensagem(AcaoCobranca.ACAO_COBRANCA));

			view = this.pesquisarAcaoCobranca(request, model, acaoCobrancaVO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;

	}

	/**
	 * Método responsável por excluir uma ou mais Ações de Cobrança.
	 * 
	 * @param request        - {@link HttpServletRequest}
	 * @param model          - {@link Model}
	 * @param acaoCobrancaVO - {@link AcaoCobrancaVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("excluirAcaoCobranca")
	public String excluirAcaoCobranca(HttpServletRequest request, Model model, AcaoCobrancaVO acaoCobrancaVO) {

		try {
			validarToken(request);

			Long[] chavesPrimarias = acaoCobrancaVO.getChavesPrimarias();

			controladorAcaoCobranca.remover(chavesPrimarias, getDadosAuditoria(request));

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA,
					super.obterMensagem(AcaoCobranca.ACOES_COBRANCA));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return this.pesquisarAcaoCobranca(request, model, acaoCobrancaVO);
	}

	private void prepararFiltro(Map<String, Object> filtro, AcaoCobrancaVO acaoCobrancaVO) {

		String descricao = acaoCobrancaVO.getDescricao();
		if (!StringUtils.isEmpty(descricao)) {
			filtro.put(DESCRICAO, descricao);
		}

		Long codigoTipoDocumentoGerado = acaoCobrancaVO.getTipoDocumentoGerado();
		if ((codigoTipoDocumentoGerado != null) && (codigoTipoDocumentoGerado > 0)) {
			filtro.put(TIPO_DOCUMENTO, codigoTipoDocumentoGerado);
		}

		String numeroDiasValidadeAcao = acaoCobrancaVO.getNumeroDiasValidadeAcao();
		if (!StringUtils.isEmpty(numeroDiasValidadeAcao)) {
			filtro.put(NUMERO_DIAS_VALIDADE_ACAO, Integer.valueOf(numeroDiasValidadeAcao));
		}

		String numeroDiasVencimento = acaoCobrancaVO.getNumeroDiasVencimento();
		if (!StringUtils.isEmpty(numeroDiasVencimento)) {
			filtro.put(NUMERO_DIAS_VENCIMENTO, Integer.valueOf(numeroDiasVencimento));
		}

		String gerarOrdemServico = acaoCobrancaVO.getGerarOrdemServico();
		if (!StringUtils.isEmpty(gerarOrdemServico)) {
			filtro.put(GERAR_ORDEM_SERVICO, Boolean.parseBoolean(gerarOrdemServico));
		} else {
			filtro.put(GERAR_ORDEM_SERVICO, false);
		}

		String consideraDebitosVencer = acaoCobrancaVO.getConsideraDebitosVencer();
		if (!StringUtils.isEmpty(consideraDebitosVencer)) {
			filtro.put(CONSIDERA_DEBITOS_VENCER, Boolean.parseBoolean(consideraDebitosVencer));
		} else {
			filtro.put(CONSIDERA_DEBITOS_VENCER, false);
		}

		String bloquearEmissaoDocumentosPagaveis = acaoCobrancaVO.getBloquearEmissaoDocumentosPagaveis();
		if (!StringUtils.isEmpty(bloquearEmissaoDocumentosPagaveis)) {
			filtro.put(BLOQUEAR_EMISSAO_DOCUMENTOS_PAGAVEIS, Boolean.parseBoolean(bloquearEmissaoDocumentosPagaveis));
		} else {
			filtro.put(BLOQUEAR_EMISSAO_DOCUMENTOS_PAGAVEIS, false);
		}
	}

	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute("listaTipoDocumento", controladorArrecadacao.listarTipoDocumento());
		model.addAttribute("listaTipoNumeroDias", controladorEntidadeConteudo.obterListaOpcaoDias());
	}

	private void popularAcaoCobranca(AcaoCobranca acaoCobranca, AcaoCobrancaVO acaoCobrancaVO) throws GGASException {

		Integer versao = acaoCobrancaVO.getVersao();
		Long codigoTipoDocumentoGerado = acaoCobrancaVO.getTipoDocumentoGerado();
		Long acaoPredecessora = acaoCobrancaVO.getAcaoPredecessora();
		String descricaoAcao = acaoCobrancaVO.getDescricao();
		String numeroDiasValidadeAcao = acaoCobrancaVO.getNumeroDiasValidadeAcao();
		String numeroDiasVencimento = acaoCobrancaVO.getNumeroDiasVencimento();
		String gerarOrdemServico = acaoCobrancaVO.getGerarOrdemServico();
		String consideraDebitosVencer = acaoCobrancaVO.getConsideraDebitosVencer();
		String bloquearEmissaoDocumentosPagaveis = acaoCobrancaVO.getBloquearEmissaoDocumentosPagaveis();
		Long tipoNumeroDiasVencimento = acaoCobrancaVO.getTipoNumeroDiasVencimento();
		Long tipoNumeroDiasValidade = acaoCobrancaVO.getTipoNumeroDiasValidade();

		if (versao != null && versao > 0) {
			acaoCobranca.setVersao(versao);
		}

		if ((acaoPredecessora != null) && acaoPredecessora > 0) {
			acaoCobranca.setAcaoPrecedente(controladorAcaoCobranca.obterAcaoCobranca(acaoPredecessora));
		} else {
			acaoCobranca.setAcaoPrecedente(null);
		}

		if ((codigoTipoDocumentoGerado != null) && (codigoTipoDocumentoGerado > 0)) {
			acaoCobranca.setDocumento(controladorArrecadacao.obterTipoDocumento(codigoTipoDocumentoGerado));
		} else {
			acaoCobranca.setDocumento(null);
		}

		if (!StringUtils.isEmpty(descricaoAcao)) {
			acaoCobranca.setDescricao(descricaoAcao.trim());
		} else {
			acaoCobranca.setDescricao(null);
		}

		if (!StringUtils.isEmpty(numeroDiasValidadeAcao)) {
			acaoCobranca.setNumeroDiasValidade(Util.converterCampoStringParaValorInteger(numeroDiasValidadeAcao));
		} else {
			acaoCobranca.setNumeroDiasValidade(null);
		}

		if (tipoNumeroDiasValidade != null && tipoNumeroDiasValidade > 0) {
			acaoCobranca.setTipoNumeroDiasValidade(tipoNumeroDiasValidade);
		} else {
			acaoCobranca.setTipoNumeroDiasValidade(null);
		}

		if (!StringUtils.isEmpty(numeroDiasVencimento)) {
			acaoCobranca.setNumeroDiasVencimento(Util.converterCampoStringParaValorInteger(numeroDiasVencimento));
		} else {
			acaoCobranca.setNumeroDiasVencimento(null);
		}

		if (tipoNumeroDiasVencimento != null && tipoNumeroDiasVencimento > 0) {
			acaoCobranca.setTipoNumeroDiasVencimento(tipoNumeroDiasVencimento);
		} else {
			acaoCobranca.setTipoNumeroDiasVencimento(null);
		}

		if (gerarOrdemServico != null) {
			acaoCobranca.setGerarOrdemServico(Boolean.parseBoolean(gerarOrdemServico));
		} else {
			acaoCobranca.setGerarOrdemServico(null);
		}

		if (consideraDebitosVencer != null) {
			acaoCobranca.setConsiderarDebitosAVencer(Boolean.parseBoolean(consideraDebitosVencer));
		} else {
			acaoCobranca.setConsiderarDebitosAVencer(null);
		}

		if (bloquearEmissaoDocumentosPagaveis != null) {
			acaoCobranca.setBloquearDocumentosPagaveis(Boolean.parseBoolean(bloquearEmissaoDocumentosPagaveis));
		} else {
			acaoCobranca.setBloquearDocumentosPagaveis(null);
		}
	}

	private AcaoCobrancaVO popularForm(AcaoCobranca acaoCobranca) throws GGASException {

		AcaoCobrancaVO acaoCobrancaVO = new AcaoCobrancaVO();

		acaoCobrancaVO.setChavePrimaria(acaoCobranca.getChavePrimaria());
		acaoCobrancaVO.setDescricao(acaoCobranca.getDescricao());

		if (acaoCobranca.getTipoNumeroDiasValidade() != null) {
			acaoCobrancaVO.setTipoNumeroDiasValidade(acaoCobranca.getTipoNumeroDiasValidade());
		}
		if (acaoCobranca.getTipoNumeroDiasVencimento() != null) {
			acaoCobrancaVO.setTipoNumeroDiasVencimento(acaoCobranca.getTipoNumeroDiasVencimento());
		}

		if (acaoCobranca.getNumeroDiasValidade() != null) {
			acaoCobrancaVO.setNumeroDiasValidadeAcao(String.valueOf(acaoCobranca.getNumeroDiasValidade()));
		} else {
			acaoCobrancaVO.setNumeroDiasValidadeAcao("");
		}

		if (acaoCobranca.getNumeroDiasVencimento() != null) {
			acaoCobrancaVO.setNumeroDiasVencimento(String.valueOf(acaoCobranca.getNumeroDiasVencimento()));
		} else {
			acaoCobrancaVO.setNumeroDiasVencimento("");
		}

		acaoCobrancaVO.setGerarOrdemServico(String.valueOf(acaoCobranca.isGerarOrdemServico()));
		acaoCobrancaVO.setConsideraDebitosVencer(String.valueOf(acaoCobranca.isConsiderarDebitosAVencer()));
		acaoCobrancaVO
				.setBloquearEmissaoDocumentosPagaveis(String.valueOf(acaoCobranca.isBloquearDocumentosPagaveis()));

		TipoDocumento tipoDocumento = controladorArrecadacao
				.obterTipoDocumento(acaoCobranca.getDocumento().getChavePrimaria());
		acaoCobrancaVO.setDescricaoTipoDocumento(tipoDocumento.getDescricao());

		acaoCobrancaVO.setTipoDocumentoGerado(acaoCobranca.getDocumento().getChavePrimaria());

		if (acaoCobranca.getTipoNumeroDiasValidade() != null) {
			EntidadeConteudo entidadeNrDiasValidade = controladorEntidadeConteudo
					.obter(acaoCobranca.getTipoNumeroDiasValidade());
			if (entidadeNrDiasValidade != null) {
				acaoCobrancaVO.setDescricaoTipoNumeroDiasValidade(entidadeNrDiasValidade.getDescricao());
			}
		}

		if (acaoCobranca.getTipoNumeroDiasVencimento() != null) {
			EntidadeConteudo entidadeNrDiasVencimento = controladorEntidadeConteudo
					.obter(acaoCobranca.getTipoNumeroDiasVencimento());
			if (entidadeNrDiasVencimento != null) {
				acaoCobrancaVO.setDescricaoTipoNumeroDiasVencimento(entidadeNrDiasVencimento.getDescricao());
			}
		}

		if (acaoCobranca.getAcaoPrecedente() != null) {
			acaoCobrancaVO.setAcaoPredecessora(acaoCobranca.getAcaoPrecedente().getChavePrimaria());
			acaoCobrancaVO.setDescricaoAcao(acaoCobranca.getAcaoPrecedente().getDescricao());
		}

		return acaoCobrancaVO;
	}

}
