
package br.com.ggas.web;

import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import com.google.common.base.Function;

/**
 * Classe reponsável por extrair o {@link ServicoTipo} do 
 * {@link ChamadoAssuntoServicoTipo}. A operação desta classe
 * está sujeita a {@link NullPointerException} caso um valor nulo
 * seja passado como argumento.
 */
public class ColetorServicoTipo implements Function<ChamadoAssuntoServicoTipo, ServicoTipo> {

	@Override
	public ServicoTipo apply(ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo) {

		return chamadoAssuntoServicoTipo.getServicoTipo();
	}

}
