/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.exportacaodados;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Exportação de Dados.
 * 
 * @author bruno silva
 * 
 * @see ExportacaoDadosAction, ExportarDadosDispositivosMoveisAction
 * 
 */
public class ExportacaoDadosVO {

	private String nome;
	private String sql;
	private String descricao;
	private String descricaoModulo;
	private String paramCodigo;
	private String paramNome;
	private String paramMascara;
	private String separador;
	
	private String colunas;
	private String colunasSelecionadas;

	private Boolean paramIsChaveEntidade;
	private Boolean paramIsPermiteMultiplaSelecao;
	private Boolean paramIsAlterado;

	private Integer paramQtdMaxCaracteres;
	private Integer paramOrdemExibicao;
	private Integer indexLista = 1;

	private Long chavePrimaria;
	private Long idModulo;
	private Long paramIdTipo;
	private Long paramIdEntidadeReferenciada;
	private Long paramIdClasseAssociada;
	private Long[] chavesPrimarias;

	/**
	 * @return nome - {@link String}
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome - {@link String}
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return sql - {@link String}
	 */
	public String getSql() {
		return sql;
	}

	/**
	 * @param sql - {@link String}
	 */
	public void setSql(String sql) {
		this.sql = sql;
	}

	/**
	 * @return descricao - {@link String}
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao - {@link String}
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return descricaoModulo - {@link String}
	 */
	public String getDescricaoModulo() {
		return descricaoModulo;
	}

	/**
	 * @param descricaoModulo - {@link String}
	 */
	public void setDescricaoModulo(String descricaoModulo) {
		this.descricaoModulo = descricaoModulo;
	}

	/**
	 * @return paramCodigo - {@link String}
	 */
	public String getParamCodigo() {
		return paramCodigo;
	}

	/**
	 * @param paramCodigo - {@link String}
	 */
	public void setParamCodigo(String paramCodigo) {
		this.paramCodigo = paramCodigo;
	}

	/**
	 * @return paramNome - {@link String}
	 */
	public String getParamNome() {
		return paramNome;
	}

	/**
	 * @param paramNome - {@link String}
	 */
	public void setParamNome(String paramNome) {
		this.paramNome = paramNome;
	}

	/**
	 * @return paramMascara - {@link String}
	 */
	public String getParamMascara() {
		return paramMascara;
	}

	/**
	 * @param paramMascara - {@link String}
	 */
	public void setParamMascara(String paramMascara) {
		this.paramMascara = paramMascara;
	}

	/**
	 * @return separador - {@link String}
	 */
	public String getSeparador() {
		return separador;
	}

	/**
	 * @param separador - {@link String}
	 */
	public void setSeparador(String separador) {
		this.separador = separador;
	}
	
	/**
	 * @return colunas - {@link String}
	 */
	public String getColunas() {
		return colunas;
	}

	/**
	 * @param colunas - {@link String}
	 */
	public void setColunas(String colunas) {
		this.colunas = colunas;
	}

	/**
	 * @return colunasSelecionadas - {@link String}
	 */
	public String getColunasSelecionadas() {
		return colunasSelecionadas;
	}

	/**
	 * @param colunasSelecionadas - {@link String}
	 */
	public void setColunasSelecionadas(String colunasSelecionadas) {
		this.colunasSelecionadas = colunasSelecionadas;
	}

	/**
	 * @return paramIsChaveEntidade - {@link Boolean}
	 */
	public Boolean getParamIsChaveEntidade() {
		return paramIsChaveEntidade;
	}

	/**
	 * @param paramIsChaveEntidade - {@link Boolean}
	 */
	public void setParamIsChaveEntidade(Boolean paramIsChaveEntidade) {
		this.paramIsChaveEntidade = paramIsChaveEntidade;
	}

	/**
	 * @return paramIsPermiteMultiplaSelecao - {@link Boolean}
	 */
	public Boolean getParamIsPermiteMultiplaSelecao() {
		return paramIsPermiteMultiplaSelecao;
	}

	/**
	 * @param paramIsPermiteMultiplaSelecao - {@link Boolean}
	 */
	public void setParamIsPermiteMultiplaSelecao(Boolean paramIsPermiteMultiplaSelecao) {
		this.paramIsPermiteMultiplaSelecao = paramIsPermiteMultiplaSelecao;
	}

	/**
	 * @return paramIsAlterado - {@link Boolean}
	 */
	public Boolean getParamIsAlterado() {
		return paramIsAlterado;
	}

	/**
	 * @param paramIsAlterado - {@link Boolean}
	 */
	public void setParamIsAlterado(Boolean paramIsAlterado) {
		this.paramIsAlterado = paramIsAlterado;
	}

	/**
	 * @return paramQtdMaxCaracteres - {@link Integer}
	 */
	public Integer getParamQtdMaxCaracteres() {
		return paramQtdMaxCaracteres;
	}

	/**
	 * @param paramQtdMaxCaracteres - {@link Integer}
	 */
	public void setParamQtdMaxCaracteres(Integer paramQtdMaxCaracteres) {
		this.paramQtdMaxCaracteres = paramQtdMaxCaracteres;
	}

	/**
	 * @return paramOrdemExibicao - {@link Integer}
	 */
	public Integer getParamOrdemExibicao() {
		return paramOrdemExibicao;
	}

	/**
	 * @param paramOrdemExibicao - {@link Integer}
	 */
	public void setParamOrdemExibicao(Integer paramOrdemExibicao) {
		this.paramOrdemExibicao = paramOrdemExibicao;
	}

	/**
	 * @return indexLista - {@link Integer}
	 */
	public Integer getIndexLista() {
		return indexLista;
	}

	/**
	 * @param indexLista - {@link Integer}
	 */
	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}

	/**
	 * @return chavePrimaria - {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	/**
	 * @param indexLista - {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return idModulo - {@link Long}
	 */
	public Long getIdModulo() {
		return idModulo;
	}

	/**
	 * @param idModulo - {@link Long}
	 */
	public void setIdModulo(Long idModulo) {
		this.idModulo = idModulo;
	}

	/**
	 * @return paramIdTipo - {@link Long}
	 */
	public Long getParamIdTipo() {
		return paramIdTipo;
	}

	/**
	 * @param paramIdTipo - {@link Long}
	 */
	public void setParamIdTipo(Long paramIdTipo) {
		this.paramIdTipo = paramIdTipo;
	}

	/**
	 * @return paramIdEntidadeReferenciada - {@link Long}
	 */
	public Long getParamIdEntidadeReferenciada() {
		return paramIdEntidadeReferenciada;
	}

	/**
	 * @param paramIdEntidadeReferenciada - {@link Long}
	 */
	public void setParamIdEntidadeReferenciada(Long paramIdEntidadeReferenciada) {
		this.paramIdEntidadeReferenciada = paramIdEntidadeReferenciada;
	}

	/**
	 * @return paramIdClasseAssociada - {@link Long}
	 */
	public Long getParamIdClasseAssociada() {
		return paramIdClasseAssociada;
	}

	/**
	 * @param paramIdClasseAssociada - {@link Long}
	 */
	public void setParamIdClasseAssociada(Long paramIdClasseAssociada) {
		this.paramIdClasseAssociada = paramIdClasseAssociada;
	}

	/**
	 * @return chavesPrimariasTmp - {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] chavesPrimariasTmp = null;
		if (chavesPrimarias != null) {
			chavesPrimariasTmp = chavesPrimarias.clone();
		}
		return chavesPrimariasTmp;
	}

	/**
	 * @param chavesPrimarias - {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

}
