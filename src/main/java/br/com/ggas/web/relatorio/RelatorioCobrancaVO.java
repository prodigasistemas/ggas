package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes as funcionalidades do módulo Relatório no sub-menu Cobrança.
 * 
 * @author bruno silva
 * 
 * @see RelatorioDeInadimplentesAction, RelatorioPrazoMinimoAntecedencia
 * 
 */
public class RelatorioCobrancaVO {
	
	private String idTipoGrafico;
	private String formatoImpressao;
	private String dataBase;
	private String diasVencido = "todos";
	private String tipoRelatorio;
	private String indicadorPesquisa;
	private String mesAno;
	
	
	private Boolean faturaRevisao = false;
	private Boolean exibirFiltros;
	private Boolean exibirGrafico;
	private Boolean pesquisaPontoConsumo;
	
	private Integer diasAtraso;
	
	private Long idCliente;
	private Long idImovel;
	private Long idSituacaoPontoConsumo;
	private Long[] idsPontoConsumo;
	private Long[] idsSegmentosDisponiveis;
	private Long[] idsSegmentosAssociados;
	
	
	/**
	 * @return idTipoGrafico - {@link String}
	 */
	public String getIdTipoGrafico() {
		return idTipoGrafico;
	}
	/**
	 * @param idTipoGrafico - {@link String}
	 */
	public void setIdTipoGrafico(String idTipoGrafico) {
		this.idTipoGrafico = idTipoGrafico;
	}
	/**
	 * @return formatoImpressao - {@link String}
	 */
	public String getFormatoImpressao() {
		return formatoImpressao;
	}
	/**
	 * @param formatoImpressao - {@link String}
	 */
	public void setFormatoImpressao(String formatoImpressao) {
		
		if(("").equals(formatoImpressao)) {
			formatoImpressao = "PDF";
		}
		
		this.formatoImpressao = formatoImpressao;
	}
	/**
	 * @return dataBase - {@link String}
	 */
	public String getDataBase() {
		return dataBase;
	}
	/**
	 * @param dataBase - {@link String}
	 */
	public void setDataBase(String dataBase) {
		this.dataBase = dataBase;
	}
	/**
	 * @return diasVencido - {@link String}
	 */
	public String getDiasVencido() {
		return diasVencido;
	}
	/**
	 * @param diasVencido - {@link String}
	 */
	public void setDiasVencido(String diasVencido) {
		this.diasVencido = diasVencido;
	}
	/**
	 * @return tipoRelatorio - {@link String}
	 */
	public String getTipoRelatorio() {
		return tipoRelatorio;
	}
	/**
	 * @param tipoRelatorio - {@link String}
	 */
	public void setTipoRelatorio(String tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}
	/**
	 * @return indicadorPesquisa - {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa - {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	/**
	 * @return mesAno - {@link String}
	 */
	public String getMesAno() {
		return mesAno;
	}
	/**
	 * @param mesAno - {@link String}
	 */
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}
	/**
	 * @return faturaRevisao - {@link Boolean}
	 */
	public Boolean getFaturaRevisao() {
		return faturaRevisao;
	}
	/**
	 * @param faturaRevisao - {@link Boolean}
	 */
	public void setFaturaRevisao(Boolean faturaRevisao) {
		this.faturaRevisao = faturaRevisao;
	}
	/**
	 * @return exibirFiltros - {@link Boolean}
	 */
	public Boolean getExibirFiltros() {
		return exibirFiltros;
	}
	/**
	 * @param exibirFiltros - {@link Boolean}
	 */
	public void setExibirFiltros(Boolean exibirFiltros) {
		this.exibirFiltros = exibirFiltros;
	}
	/**
	 * @return exibirGrafico - {@link Boolean}
	 */
	public Boolean getExibirGrafico() {
		return exibirGrafico;
	}
	/**
	 * @param exibirGrafico - {@link Boolean}
	 */
	public void setExibirGrafico(Boolean exibirGrafico) {
		this.exibirGrafico = exibirGrafico;
	}
	/**
	 * @return pesquisaPontoConsumo - {@link Boolean}
	 */
	public Boolean getPesquisaPontoConsumo() {
		return pesquisaPontoConsumo;
	}
	/**
	 * @param pesquisaPontoConsumo - {@link Boolean}
	 */
	public void setPesquisaPontoConsumo(Boolean pesquisaPontoConsumo) {
		this.pesquisaPontoConsumo = pesquisaPontoConsumo;
	}
	/**
	 * @return diasAtraso - {@link Integer}
	 */
	public Integer getDiasAtraso() {
		return diasAtraso;
	}
	/**
	 * @param diasAtraso - {@link Integer}
	 */
	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idSituacaoPontoConsumo - {@link Long}
	 */
	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}
	/**
	 * @param idSituacaoPontoConsumo - {@link Long}
	 */
	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}
	
	/**
	 * @return idsPontoConsumo {@link Long}
	 */
	public Long[] getIdsPontoConsumo() {
		return validarClonar(idsPontoConsumo);
	}
	/**
	 * @param idsPontoConsumo {@link Long}
	 */
	public void setIdsPontoConsumo(Long[] idsPontoConsumo) {
		this.idsPontoConsumo = validarClonar(idsPontoConsumo);
	}
	
	/**
	 * @return idsSegmentosDisponiveis {@link Long}
	 */
	public Long[] getIdsSegmentosDisponiveis() {
		return validarClonar(idsSegmentosDisponiveis);
	}
	/**
	 * @param idsSegmentosDisponiveis {@link Long}
	 */
	public void setIdsSegmentosDisponiveis(Long[] idsSegmentosDisponiveis) {
		this.idsSegmentosDisponiveis = validarClonar(idsSegmentosDisponiveis);
	}
	
	/**
	 * @return idsSegmentosAssociados {@link Long}
	 */
	public Long[] getIdsSegmentosAssociados() {
		return validarClonar(idsSegmentosAssociados);
	}
	/**
	 * @param idsSegmentosDisponiveis {@link Long}
	 */
	public void setIdsSegmentosAssociados(Long[] idsSegmentosAssociados) {
		this.idsSegmentosAssociados = validarClonar(idsSegmentosAssociados);
	}
	
	/**
	 * Método responsável em verificar se uma variável é nula, retornando uma cópia da mesma.
	 * 
	 * @param tmp - {@link Long}
	 * @return tmp variável temporária. - {@link Long}
	 */
	private Long[] validarClonar(Long[] tmp) {
		if (tmp != null) {
			return tmp.clone();
		} else {
			return null;
		}
	}

}
