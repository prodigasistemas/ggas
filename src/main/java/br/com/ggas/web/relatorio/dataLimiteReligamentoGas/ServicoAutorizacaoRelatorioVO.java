package br.com.ggas.web.relatorio.dataLimiteReligamentoGas;

import java.util.Date;

public class ServicoAutorizacaoRelatorioVO {

	private String nomeCliente;
	private Date dataGerada;
	private Date dataEncerramento;
	
	public ServicoAutorizacaoRelatorioVO(String nomeCliente, Date dataGerada, Date dataEncerramento) {
		super();
		this.nomeCliente = nomeCliente;
		this.dataGerada = dataGerada;
		this.dataEncerramento = dataEncerramento;
	}

	public ServicoAutorizacaoRelatorioVO() {
		super();
	}

	
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}

	
	public Date getDataGerada() {
		Date data = null;
		if (this.dataGerada != null) {
			data = (Date) dataGerada.clone();
		}
		return data;
	}

	
	public void setDataGerada(Date dataGerada) {
	
		this.dataGerada = dataGerada;
	}

	public Date getDataEncerramento() {
		Date data = null;
		if(dataEncerramento != null) {
			data = (Date) dataEncerramento.clone();
		}
		return data;
	}
	
	public void setDataEncerramento(Date dataEncerramento) {
	
		this.dataEncerramento = dataEncerramento;
	}
}
