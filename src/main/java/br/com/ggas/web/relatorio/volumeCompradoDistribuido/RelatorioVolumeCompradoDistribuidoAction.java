/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.volumeCompradoDistribuido;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioVolumeCompradoDistribuido;
import br.com.ggas.relatorio.comum.SelecionarListaOrdenadaUtil;
import br.com.ggas.relatorio.comum.SelecionarListaOrdenadaVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.ListaUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.util.Util;

/**
 * The Class RelatorioVolumeCompradoDistribuidoAction.
 */
@Controller
public class RelatorioVolumeCompradoDistribuidoAction extends GenericAction {
	
	private static final String ACTION_EXIBIR_RELATORIO_VOLUME_COMPRADO_DISTRIBUIDO = "exibirRelatorioVolumeCompradoDistribuido";

	private static final String NOME_METODO_RECARREGAR_GRID = "recarregarGridListaOrdenadaSegmentos";
	
	private static final String NOME_PARAMETRO_CPS_ADICIONADAS = "cpsSegmentosAdicionados";
	
	private static final int NUMERO_MINIMO_ENTIDADE = 1;

	private static final int NUMERO_MAXIMO_ENTIDADE = 5;

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
	public static final String CPS_ADICIONADAS = "cpAdicionadas";

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioVolumeCompradoDistribuido controladorRelatorioVolumeCompradoDistribuido;

	/**
	 * Exibir relatorio volume comprado distribuido.
	 *
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping(ACTION_EXIBIR_RELATORIO_VOLUME_COMPRADO_DISTRIBUIDO)
	public ModelAndView exibirRelatorioVolumeCompradoDistribuido() throws NegocioException {

		return montarExibirRelatorioVolumeCompradoDistribuido(null, null, null);
		
	}

	/**
	 * Recarregar grid lista ordenada segmentos.
	 *
	 * @param cpsSegmentosAdicionados the cps segmentos adicionados
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping(NOME_METODO_RECARREGAR_GRID)
	public ModelAndView recarregarGridListaOrdenadaSegmentos(
					@RequestParam(value = NOME_PARAMETRO_CPS_ADICIONADAS, required = false) Long[] cpsSegmentosAdicionados) 
									throws NegocioException {
		
		return SelecionarListaOrdenadaUtil.montarGridListaOrdenadaSegmentos(
						controladorSegmento, cpsSegmentosAdicionados);

	}
	
	/**
	 * Gerar relatorio volume comprado distribuido.
	 *
	 * @param strFormatoImpressao the str formato impressao
	 * @param dataEmissaoInicial the data emissao inicial
	 * @param dataEmissaoFinal the data emissao final
	 * @param exibirFiltros the exibir filtros
	 * @param cpAdicionadas the cp adicionadas
	 * @param response the response
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioVolumeCompradoDistribuido")
	public ModelAndView gerarRelatorioVolumeCompradoDistribuido(
					@RequestParam(value = "formatoImpressao", required = false) String strFormatoImpressao,
					@RequestParam(value = "dataEmissaoInicial", required = false) String dataEmissaoInicial,
					@RequestParam(value = "dataEmissaoFinal", required = false) String dataEmissaoFinal,
					@RequestParam(value = "exibirFiltros", required = false) String exibirFiltros,
					@RequestParam(value = "cpAdicionados", required = false) Long[] cpAdicionadas,
					HttpServletResponse response) throws GGASException {
		
		List<String> msgErros = validarEntradas(
						dataEmissaoInicial, dataEmissaoFinal, cpAdicionadas);
	
		if (msgErros.isEmpty()) {
			montarArquivoRelatorio(strFormatoImpressao, dataEmissaoInicial, 
							dataEmissaoFinal, exibirFiltros, cpAdicionadas, response);
		}
		
		ModelAndView model = 
						montarExibirRelatorioVolumeCompradoDistribuido(
										msgErros, dataEmissaoInicial, dataEmissaoFinal);
		SelecionarListaOrdenadaUtil.montarGridListaOrdenadaSegmentos(model, 
						controladorSegmento, cpAdicionadas);
		
		return model;
		
	}

	/**
	 * Montar arquivo relatorio.
	 *
	 * @param strFormatoImpressao the str formato impressao
	 * @param dataEmissaoInicial the data emissao inicial
	 * @param dataEmissaoFinal the data emissao final
	 * @param exibirFiltros the exibir filtros
	 * @param cpAdicionadas the cp adicionadas
	 * @param response the response
	 * @throws GGASException the GGAS exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void montarArquivoRelatorio(String strFormatoImpressao, 
					String dataEmissaoInicial, String dataEmissaoFinal,
					String exibirFiltros, Long[] cpAdicionadas, 
					HttpServletResponse response) 
									throws GGASException {

		FormatoImpressao formatoImpressao = 
						Util.obterFormatoImpressao(strFormatoImpressao);

		Map<String, Object> filtro = this.prepararFiltro(
						dataEmissaoInicial, dataEmissaoFinal, 
						exibirFiltros, cpAdicionadas);
		
		byte[] relatorio = controladorRelatorioVolumeCompradoDistribuido
						.gerarRelatorio(filtro, formatoImpressao);
		
		ServletOutputStream servletOutputStream;
		try {
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition",
							"attachment; filename=relatorioVolumeCompradoDistribuido" + 
											FormatoImpressao.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getStackTrace(), e);
			throw new GGASException(e.getMessage());
		}
	}

	/**
	 * Montar exibir relatorio volume comprado distribuido.
	 *
	 * @param msgErros the msg erros
	 * @param dataEmissaoInicial the data emissao inicial
	 * @param dataEmissaoFinal the data emissao final
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	private ModelAndView montarExibirRelatorioVolumeCompradoDistribuido(
					List<String> msgErros, String dataEmissaoInicial, 
					String dataEmissaoFinal) 
									throws NegocioException {

		ModelAndView model = new ModelAndView(ACTION_EXIBIR_RELATORIO_VOLUME_COMPRADO_DISTRIBUIDO);

		Collection<Segmento> listaSegmentos = controladorSegmento.listarSegmento();
		
		SelecionarListaOrdenadaVO vo = new SelecionarListaOrdenadaVO();
		vo.setListaSegmentos(listaSegmentos);
		vo.setNomeMetodoRecarregarGrid(NOME_METODO_RECARREGAR_GRID);
		vo.setNomeParametroCpsAdicionadas(NOME_PARAMETRO_CPS_ADICIONADAS);
		vo.setNumeroMinimoEntidade(NUMERO_MINIMO_ENTIDADE);
		vo.setNumeroMaximoEntidade(NUMERO_MAXIMO_ENTIDADE);
		vo.setDataEmissaoInicial(dataEmissaoInicial);
		vo.setDataEmissaoFinal(dataEmissaoFinal);
		
		SelecionarListaOrdenadaUtil.montarSelecionarListaOrdenada(model, vo);
		
		if (CollectionUtils.isNotEmpty(msgErros)) {
			mensagemErro(model, msgErros);
		}
		
		return model;
	}

	/**
	 * Preparar filtro.
	 * @param exibirFiltro 
	 * @param dataEmissaoFinal 
	 * @param dataEmissaoInicial 
	 * @param cpAdicionados 
	 * 
	 * @param dynaForm
	 *            the dyna form
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, Object> prepararFiltro(String dataEmissaoInicial, 
					String dataEmissaoFinal, String exibirFiltros, Long[] cpAdicionadas) 
									throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(DATA_EMISSAO_INICIAL, DataUtil.gerarDataHmsZerados(DataUtil.parse(dataEmissaoInicial)));
		filtro.put(DATA_EMISSAO_FINAL, DataUtil.gerarDataHmsZerados(DataUtil.parse(dataEmissaoFinal)));
		if (StringUtils.isEmpty(exibirFiltros)) {
			filtro.put(EXIBIR_FILTROS, Boolean.FALSE);
		} else {
			filtro.put(EXIBIR_FILTROS, Boolean.parseBoolean(exibirFiltros));
		}
		filtro.put(CPS_ADICIONADAS, cpAdicionadas);
		
		
		return filtro;
	}
	
	/**
	 * Validar entradas.
	 *
	 * @param dataEmissaoInicial the data emissao inicial
	 * @param dataEmissaoFinal the data emissao final
	 * @param cpAdicionados the cp adicionados
	 * @return the list
	 */
	private List<String> validarEntradas(
					String dataEmissaoInicial, String dataEmissaoFinal, 
					Long[] cpAdicionados) {
		
		Pair<Date, String> validarDataInicial = DataUtil.validarData(dataEmissaoInicial, 
						Constantes.ERRO_NEGOCIO_DATA_INICIAL_OBRIGATORIO, 
						Constantes.ERRO_NEGOCIO_DATA_INICIAL_DATA_VALIDA);
		Date dataInicial = validarDataInicial.getFirst();
		
		Pair<Date, String> validarDataFinal = DataUtil.validarData(dataEmissaoFinal, 
						Constantes.ERRO_NEGOCIO_DATA_FINAL_OBRIGATORIO, 
						Constantes.ERRO_NEGOCIO_DATA_FINAL_DATA_VALIDA);
		Date dataFinal = validarDataInicial.getFirst();
		
		
		String validarIntervalo = DataUtil.validarIntervalo(dataInicial, dataFinal);
		
		String validarQuantidadeSegmentos = ListaUtil.validarLimites(
						cpAdicionados, NUMERO_MINIMO_ENTIDADE, NUMERO_MAXIMO_ENTIDADE, 
						Constantes.ERRO_MENOR_QUE_MINIMO_SELECIONAR_LISTA_ORDENADA, 
						Constantes.ERRO_MAIOR_QUE_MAXIMO_SELECIONAR_LISTA_ORDENADA,
						Segmento.SEGMENTOS_ROTULO);
		
		return MensagemUtil.montarListaErros(validarDataInicial.getSecond(), 
						validarDataFinal.getSecond(), validarIntervalo, 
						validarQuantidadeSegmentos);
		
	}

}
