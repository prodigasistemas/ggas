/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturamento;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * @author vpessoa
 */
@Controller
public class RelatorioAcompanhamentoClientesAction extends GenericAction {

	private static final String LISTA_FISCAL = "listaFiscal";

	private static final String LISTA_VENDEDOR = "listaVendedor";

	private static final String LISTA_SITUACAO_PROPOSTA = "listaSituacaoProposta";

	private static final Logger LOG = Logger.getLogger(RelatorioAcompanhamentoClientesAction.class);

	@Autowired
	@Qualifier("controladorProposta")
	private ControladorProposta controladorProposta;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	/**
	 * Exibir pesquisa relatorio acompanhamento clientes.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("exibirPesquisaRelatorioAcompanhamentoClientes")
	public ModelAndView exibirPesquisaRelatorioAcompanhamentoClientes() throws NegocioException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioAcompanhamentoClientes");

		model.addObject(LISTA_SITUACAO_PROPOSTA, controladorProposta.listarSituacaoProposta());
		model.addObject(LISTA_VENDEDOR, controladorFuncionario.listarFuncionarioFiscalVendedor(false, true));
		model.addObject(LISTA_FISCAL, controladorFuncionario.listarFuncionarioFiscalVendedor(true, false));

		return model;
	}

	/**
	 * Gerar relatorio acompanhamento clientes.
	 * 
	 * @param relatorioAcompanhamentoClientesVO
	 *            the relatorio acompanhamento clientes vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioAcompanhamentoClientes")
	public ModelAndView gerarRelatorioAcompanhamentoClientes(
					@ModelAttribute("RelatorioAcompanhamentoClientesVO") PesquisaRelatorioAcompanhamentoClientesVO relatorioAcompanhamentoClientesVO,
					@RequestParam("tipoRelatorio") String tipoRelatorio, BindingResult result, HttpServletRequest request,
					HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioAcompanhamentoClientes");
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			relatorio = controladorProposta.gerarRelatorioAcompanhamentoClientes(
							relatorioAcompanhamentoClientesVO, formatoImpressao);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=RelatorioAcompanhamentoClientes_" + diaMesAno + "_" + hora
							+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			model.addObject(LISTA_SITUACAO_PROPOSTA, controladorProposta.listarSituacaoProposta());
			model.addObject(LISTA_VENDEDOR, controladorFuncionario.listarVendedores());
			model.addObject(LISTA_FISCAL, controladorFuncionario.listarFiscais());
			model.addObject("relatorioAcompanhamentoClientesVO", relatorioAcompanhamentoClientesVO);
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (NegocioException e) {
			model.addObject(LISTA_SITUACAO_PROPOSTA, controladorProposta.listarSituacaoProposta());
			model.addObject(LISTA_VENDEDOR, controladorFuncionario.listarVendedores());
			model.addObject(LISTA_FISCAL, controladorFuncionario.listarFiscais());
			model.addObject("relatorioAcompanhamentoClientesVO", relatorioAcompanhamentoClientesVO);
			mensagemErroParametrizado(model, e);
			LOG.error(e.getMessage(), e);
		}
		return model;
	}

}
