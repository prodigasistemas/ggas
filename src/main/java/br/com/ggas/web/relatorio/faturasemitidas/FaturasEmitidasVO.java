/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturasemitidas;

import java.util.Date;

public class FaturasEmitidasVO {

	private Date data;

	private Long qtdFaturas;

	private Long qtdFaturasCanceladas;

	private Long qtdTotalFaturas;

	private Long qtdTotalFaturasCanceladas;

	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	public void setData(Date data) {

		this.data = data;
	}

	public Long getQtdFaturas() {

		return qtdFaturas;
	}

	public void setQtdFaturas(Long qtdFaturas) {

		this.qtdFaturas = qtdFaturas;
	}

	public Long getQtdFaturasCanceladas() {

		return qtdFaturasCanceladas;
	}

	public void setQtdFaturasCanceladas(Long qtdFaturasCanceladas) {

		this.qtdFaturasCanceladas = qtdFaturasCanceladas;
	}

	public Long getQtdTotalFaturas() {

		return qtdTotalFaturas;
	}

	public void setQtdTotalFaturas(Long qtdTotalFaturas) {

		this.qtdTotalFaturas = qtdTotalFaturas;
	}

	public Long getQtdTotalFaturasCanceladas() {

		return qtdTotalFaturasCanceladas;
	}

	public void setQtdTotalFaturasCanceladas(Long qtdTotalFaturasCanceladas) {

		this.qtdTotalFaturasCanceladas = qtdTotalFaturasCanceladas;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		if (data == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + data.hashCode();
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(this == obj) {
			return true;
		}
		if(obj == null){ 
			return false;
		}
		if(getClass() != obj.getClass()){ 
			return false;
		}
		FaturasEmitidasVO other = (FaturasEmitidasVO) obj;
		if(data == null) {
			if(other.data != null){ 
				return false;
			}
		}
		else if(!data.equals(other.data)){ 
			return false;
		}
		return true;
	}

}
