
package br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioCobrancaRecuperacaoPenalidades;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioContratoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Cobrança e Recuperação de Penalidades.
 */
@Controller
public class RelatorioCobrancaRecuperacaoPenalidadesAction extends GenericAction {

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	public static final String PARAM_DATA_EMISSAO_INICIAL = "dataInicial";

	public static final String PARAM_DATA_EMISSAO_FINAL = "dataFinal";

	public static final String TIPO_RELATORIO = "tipoExibicao";

	private static final String RELATORIO = "relatorio";
	
	public static final String FORMATO_IMPRESSAO = "formatoImpressao";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String EXIBIR_RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES = "exibirRelatorioCobrancaRecuperacaoPenalidades";

	@Autowired
	private ControladorRelatorioCobrancaRecuperacaoPenalidades controladorRelatorioCobrancaRecuperacaoPenalidades;
	
	/**
	 * Exibir relatorio cobranca recuperacao penalidades.
	 * 
	 * @return exibirRelatorioCobrancaRecuperacaoPenalidades - {@link String}
	 */
	@RequestMapping(EXIBIR_RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES)
	public String exibirRelatorioCobrancaRecuperacaoPenalidades() {

		return EXIBIR_RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES;
	}

	/**
	 * Gerar relatorio cobranca recuperacao penalidades.
	 * 
	 * @param relatorioContratoVO - {@link RelatorioContratoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirRelatorioCobrancaRecuperacaoPenalidades - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping("gerarRelatorioCobrancaRecuperacaoPenalidades")
	public String gerarRelatorioCobrancaRecuperacaoPenalidades(RelatorioContratoVO relatorioContratoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String formato = relatorioContratoVO.getFormatoImpressao();
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(formato);

		try {
			Map<String, Object> filtro = this.prepararFiltro(relatorioContratoVO);
			filtro.put(FORMATO_IMPRESSAO, formatoImpressao);

			byte[] relatorio = controladorRelatorioCobrancaRecuperacaoPenalidades.gerarRelatorioCobrancaRecuperacaoPenalidades(filtro);

			if (relatorio != null) {
				Date dataAtual = Calendar.getInstance().getTime();
				int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
				int minutes = Calendar.getInstance().get(Calendar.MINUTE);
				int seconds = Calendar.getInstance().get(Calendar.SECOND);

				String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual);

				ServletOutputStream servletOutputStream = response.getOutputStream();
				request.getSession().setAttribute(RELATORIO, relatorio);

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition", "attachment; filename=" + EXIBIR_RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES + "_"
						+ data + "_" + hours + minutes + seconds + "." + formato.toLowerCase());
				response.getOutputStream().write(relatorio);
				servletOutputStream.flush();
				servletOutputStream.close();
			}

		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().removeAttribute(RELATORIO);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return EXIBIR_RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES;
	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioContratoVO - {@link RelatorioContratoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link}
	 */
	private Map<String, Object> prepararFiltro(RelatorioContratoVO relatorioContratoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long idCliente = relatorioContratoVO.getIdCliente();
		Long idImovel = relatorioContratoVO.getIdImovel();
		String dataInicial = relatorioContratoVO.getDataInicial();
		String dataFinal = relatorioContratoVO.getDataFinal();
		String tipoRelatorio = relatorioContratoVO.getTipo();

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (idImovel != null && idImovel > 0) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		if (StringUtils.isNotEmpty(dataInicial)) {
			Date dataInicialAux = Util.converterCampoStringParaData("Período de Emissão", dataInicial, Constantes.FORMATO_DATA_BR);
			filtro.put("dataEmissaoInicial", dataInicialAux);
		}

		if (StringUtils.isNotEmpty(dataFinal)) {
			Date dataFinalAux = Util.converterCampoStringParaData("Período de Emissão", dataFinal, Constantes.FORMATO_DATA_BR);
			filtro.put("dataEmissaoFinal", dataFinalAux);
		}

		if (StringUtils.isNotEmpty(tipoRelatorio)) {
			filtro.put(TIPO_RELATORIO, tipoRelatorio);
		}

		filtro.put(EXIBIR_FILTROS, relatorioContratoVO.isExibirFiltros());

		return filtro;
	}

}
