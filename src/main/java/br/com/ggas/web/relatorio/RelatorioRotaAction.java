/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.relatorio.ControladorRelatorioRota;
import br.com.ggas.util.FormatoImpressao;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Clientes por Rotas.
 */
@Controller
public class RelatorioRotaAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioRotaAction.class);

	private static final String LISTA_TIPO_LEITURA = "listaTipoLeitura";

	private static final String LISTA_EMPRESA = "listaEmpresa";

	private static final String LISTA_LEITURISTA = "listaLeiturista";

	private static final String LISTA_PERIODICIDADE = "listaPeriodicidade";

	private static final String LISTA_SETOR_COMERCIAL = "listaSetorComercial";

	private static final String LISTA_TIPO_ROTA = "listaTipoRota";

	private static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	private static final String ID_PERIODICIDADE = "idPeriodicidade";

	private static final String ID_TIPO_LEITURA = "idTipoLeitura";

	private static final String NUMERO_ROTA = "numeroRota";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_EMPRESA = "idEmpresa";

	private static final String ID_LEITURISTA = "idLeiturista";

	private static final String EXIBIR_FILTRO = "exibirFiltros";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String RELATORIO_ROTA = "relatorioRota";

	private static final String HABILITADO = "habilitado";

	protected static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorEmpresa controladorEmpresa;
	
	@Autowired
	private ControladorSetorComercial controladorSetorComercial;
	
	@Autowired
	private ControladorRelatorioRota controladorRelatorioRota;
	
	/**
	 * Método responsavel por exibir a tela de
	 * pesquisa do relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioRota - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioRota")
	public String exibirPesquisaRelatorioRota(RelatorioCadastroVO relatorioCadastroVO, Model model) throws GGASException {

		model.addAttribute("relatorioCadastroVO", relatorioCadastroVO);
		configurarPesquisaRelatorioRota(model);

		return "exibirPesquisaRelatorioRota";
	}
	
	/**
	 * Método responsável por exibir o relatório
	 * de analise de demanda.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioRota")
	public String exibirRelatorioRota(HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		byte[] relatorioAnaliseDemanda = (byte[]) request.getSession().getAttribute(RELATORIO_ROTA);
		FormatoImpressao formatoImpressao = (FormatoImpressao) request.getSession().getAttribute(FORMATO_IMPRESSAO);

		request.getSession().removeAttribute(RELATORIO_ROTA);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		if (relatorioAnaliseDemanda != null) {
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			try {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorioAnaliseDemanda.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioRota" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorioAnaliseDemanda, 0, relatorioAnaliseDemanda.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e);
				throw new GGASException(e);
			}

		}

		return null;
	}

	/**
	 * Método responsavel por gerar o relatório.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioRota - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioRota")
	public String gerarRelatorioRota(RelatorioCadastroVO relatorioCadastroVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		Map<String, Object> parametros = obterParametrosRelatorio(relatorioCadastroVO);
		try {
			byte[] relatorio = controladorRelatorioRota.gerarRelatorio(parametros);
			if (relatorio != null) {
				request.getSession().setAttribute(RELATORIO_ROTA, relatorio);
				request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf((String) parametros.get(FORMATO_IMPRESSAO)));
				model.addAttribute("exibirRelatorio", Boolean.TRUE);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaRelatorioRota(relatorioCadastroVO, model);
	}

	/**
	 * Configurar pesquisa relatorio rota.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarPesquisaRelatorioRota(Model model) throws GGASException {

		model.addAttribute(LISTA_TIPO_LEITURA, controladorRota.listarTiposLeitura());
		model.addAttribute(LISTA_EMPRESA, controladorEmpresa.listarEmpresasMedicao());
		model.addAttribute(LISTA_LEITURISTA, controladorRota.listarLeituristas());
		model.addAttribute(LISTA_PERIODICIDADE, controladorRota.listarPeriodicidades());
		model.addAttribute(LISTA_SETOR_COMERCIAL, controladorSetorComercial.listarSetoresComerciais());
		model.addAttribute(LISTA_TIPO_ROTA, controladorRota.listarTiposRotas());
		model.addAttribute(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());

	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @return mapa - {@link Map}
	 */
	private Map<String, Object> obterParametrosRelatorio(RelatorioCadastroVO relatorioCadastroVO) {

		String numeroRota = relatorioCadastroVO.getNumeroRota();
		String formatoImpressao = relatorioCadastroVO.getFormatoImpressao();
		Boolean exibirFiltros = relatorioCadastroVO.getExibirFiltros();
		Boolean habilitado = relatorioCadastroVO.getHabilitado();
		Long idPeriodicidade = relatorioCadastroVO.getIdPeriodicidade();
		Long idTipoLeitura = relatorioCadastroVO.getIdTipoLeitura();

		Map<String, Object> mapa = new HashMap<String, Object>();

		if (StringUtils.isNotEmpty(numeroRota)) {
			mapa.put(NUMERO_ROTA, numeroRota);
		}
		if (StringUtils.isNotEmpty(formatoImpressao)) {
			mapa.put(FORMATO_IMPRESSAO, formatoImpressao);
		}
		
		mapa.put(EXIBIR_FILTRO, exibirFiltros);
		
		if (habilitado != null) {
			mapa.put(HABILITADO, habilitado);
		}
		
		if (idPeriodicidade != null && idPeriodicidade > 0) {
			mapa.put(ID_PERIODICIDADE, idPeriodicidade);
		}
		if (idTipoLeitura != null && idTipoLeitura > 0) {
			mapa.put(ID_TIPO_LEITURA, idTipoLeitura);
		}
		
		obeterParametroRelatorioDois(relatorioCadastroVO, mapa);

		return mapa;
	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param idGrupoFaturamento
	 * @param idSetorComercial
	 * @param idEmpresa
	 * @param idLeiturista
	 * @param mapa
	 */
	private void obeterParametroRelatorioDois(RelatorioCadastroVO relatorioCadastroVO, Map<String, Object> mapa) {

		Long idGrupoFaturamento = relatorioCadastroVO.getIdGrupoFaturamento();
		Long idSetorComercial = relatorioCadastroVO.getIdSetorComercial();
		Long idEmpresa = relatorioCadastroVO.getIdEmpresa();
		Long idLeiturista = relatorioCadastroVO.getIdLeiturista();
		
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			mapa.put(ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
		}
		if (idSetorComercial != null && idSetorComercial > 0) {
			mapa.put(ID_SETOR_COMERCIAL, idSetorComercial);
		}
		if (idEmpresa != null && idEmpresa > 0) {
			mapa.put(ID_EMPRESA, idEmpresa);
		}
		if (idLeiturista != null && idLeiturista > 0) {
			mapa.put(ID_LEITURISTA, idLeiturista);
		}

	}

}
