package br.com.ggas.web.relatorio.medidores;

import java.math.BigDecimal;
import java.util.Date;

public class MedidoresVO {
	
	private String numeroSerie;
	private String descricaoModelo;
	private String descricaoTipoMedidor;
	private String descricaoMarcaMedidor;
	private Integer anoFabricacao;
	private String descricaoSituacaoMedidor;
	private String descricaoLocalArmazenagem;
	private Date dataAquisicao;
	
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getDescricaoModelo() {
		return descricaoModelo;
	}
	public void setDescricaoModelo(String descricaoModelo) {
		this.descricaoModelo = descricaoModelo;
	}
	public String getDescricaoTipoMedidor() {
		return descricaoTipoMedidor;
	}
	public void setDescricaoTipoMedidor(String descricaoTipoMedidor) {
		this.descricaoTipoMedidor = descricaoTipoMedidor;
	}
	public String getDescricaoMarcaMedidor() {
		return descricaoMarcaMedidor;
	}
	public void setDescricaoMarcaMedidor(String descricaoMarcaMedidor) {
		this.descricaoMarcaMedidor = descricaoMarcaMedidor;
	}
	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	public String getDescricaoSituacaoMedidor() {
		return descricaoSituacaoMedidor;
	}
	public void setDescricaoSituacaoMedidor(String descricaoSituacaoMedidor) {
		this.descricaoSituacaoMedidor = descricaoSituacaoMedidor;
	}
	public String getDescricaoLocalArmazenagem() {
		return descricaoLocalArmazenagem;
	}
	public void setDescricaoLocalArmazenagem(String descricaoLocalArmazenagem) {
		this.descricaoLocalArmazenagem = descricaoLocalArmazenagem;
	}
	public Date getDataAquisicao() {
		return dataAquisicao;
	}
	public void setDataAquisicao(Date dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}
	

}
