package br.com.ggas.web.relatorio.medidores;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.util.ServiceLocator;

@Repository
public class RepositorioMedidores extends RepositorioGenerico {
	
	@Autowired
	public RepositorioMedidores(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
	}
	
	public Class<?> getClasseEntidadeHistoricoMedidor(){
		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	
	@SuppressWarnings("unchecked")
	public Collection<MedidoresVO> listarMedidoresPorData(Date dataInicial, Date dataFinal, String numeroSerie, 
			String marcaMedidor, String tipoMedidor, String modoUso, String modelo, String localArmazenagem
			, String habilitado, String anoFabricacao){
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT medidor.numeroSerie as numeroSerie, medidor.anoFabricacao as anoFabricacao," // 
				+ " modelo.descricao as descricaoModelo, tipoMedidor.descricao as descricaoTipoMedidor, "
				+ " marcaMedidor.descricao as descricaoMarcaMedidor, situacaoMedidor.descricao as descricaoSituacaoMedidor, "
				+ " localArmazenagem.descricao as descricaoLocalArmazenagem, "
				+ " medidor.dataAquisicao as dataAquisicao ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" medidor ");
		hql.append(" INNER JOIN medidor.modelo modelo ");
		hql.append(" INNER JOIN medidor.tipoMedidor tipoMedidor ");
		hql.append(" INNER JOIN medidor.marcaMedidor marcaMedidor ");
		hql.append(" INNER JOIN medidor.situacaoMedidor situacaoMedidor ");
		hql.append(" INNER JOIN medidor.localArmazenagem localArmazenagem ");
		hql.append(" INNER JOIN medidor.modoUso modoUso ");
		hql.append(" WHERE 1=1 ");
		
		if(!numeroSerie.isEmpty()) {
			hql.append(" AND ");
			hql.append(" medidor.numeroSerie = :numeroSerie ");
		}
		if(dataInicial != null) {
			hql.append(" AND ");
			hql.append(" medidor.dataAquisicao >= :dataInicial ");
		}
		if(dataFinal != null ) {
			hql.append(" AND ");
			hql.append(" medidor.dataAquisicao <= :dataFinal ");
		}
		if(!marcaMedidor.equals("-1")) {
			hql.append(" AND ");
			hql.append(" marcaMedidor.chavePrimaria = :marcaMedidor ");
		}
		
		if(!tipoMedidor.equals("-1")) {
			hql.append(" AND ");
			hql.append(" tipoMedidor.chavePrimaria = :tipoMedidor ");
		}
		
		if(!modoUso.equals("null")) {
			hql.append(" AND ");
			hql.append(" modoUso.chavePrimaria = :modoUso ");
		}
		
		if(!modelo.equals("-1")) {
			hql.append(" AND ");
			hql.append(" modelo.chavePrimaria = :modelo ");
		}
		
		if(!localArmazenagem.equals("-1")) {
			hql.append(" AND ");
			hql.append(" localArmazenagem.chavePrimaria = :localArmazenagem ");	
		}
		if(habilitado != null) {
			if(Boolean.parseBoolean(habilitado)) {
				hql.append(" AND ");
				hql.append(" medidor.habilitado = true ");
			}else {
				hql.append(" AND ");
				hql.append(" medidor.habilitado = false ");
			}
		}
		if(!anoFabricacao.isEmpty()) {
			hql.append(" AND ");
			hql.append(" medidor.anoFabricacao = :anoFabricacao ");
		}
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(MedidoresVO.class));
		
		if(!numeroSerie.isEmpty())
			query.setParameter("numeroSerie", numeroSerie);
		if(dataInicial != null)
			query.setDate("dataInicial", dataInicial);
		if(dataFinal != null)
			query.setDate("dataFinal", dataFinal);
		if(!marcaMedidor.equals("-1"))
			query.setLong("marcaMedidor", Long.parseLong(marcaMedidor));
		if(!tipoMedidor.equals("-1"))
			query.setLong("tipoMedidor", Long.parseLong(tipoMedidor));
		if(!modoUso.equals("null"))
			query.setLong("modoUso", Long.parseLong(modoUso));
		if(!modelo.equals("-1"))
			query.setLong("modelo", Long.parseLong(modelo));
		if(!localArmazenagem.equals("-1"))
			query.setLong("localArmazenagem", Long.parseLong(localArmazenagem));
		if(!anoFabricacao.isEmpty())
			query.setLong("anoFabricacao", Long.parseLong(anoFabricacao));
		
		return query.list();
	}
	
	public Collection<SubstituicaoMedidorVO> listarSubstituicaoMedidores(Date dataInicial, Date dataFinal, String operacao, List<Long> equipe, List<Long> servico){
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT pontoConsumo.POCN_DS as pontoConsumo, tipoMedidor.METI_DS as medidorTipo, ");
		hql.append(" marcaMedidor.MEMA_DS as medidorFabricante, medidor.MEDI_NR_SERIE as numeroSerie, ");
		hql.append(" modelo.MEMD_DS as medidorModelo, medidor.MEDI_NR_TOMBO as medidorBP, ");
		hql.append(" moh.MEOH_MD_LEITURA as leituraFinal, motivoOperacaoMedidor.MEMP_DS as motivoOperacao, ");
		hql.append(" moh.MEOH_DT_REALIZADA as dataOperacao ");
		hql.append(" FROM ");
		hql.append(" MEDIDOR_OPERACAO_HISTORICO ");
		hql.append(" moh ");
		hql.append(" INNER JOIN PONTO_CONSUMO pontoConsumo ON moh.POCN_CD = pontoConsumo.POCN_CD ");
		hql.append(" INNER JOIN MEDIDOR medidor ON moh.MEDI_CD = medidor.MEDI_CD ");
		hql.append(" INNER JOIN MEDIDOR_TIPO tipoMedidor ON medidor.METI_CD = tipoMedidor.METI_CD ");
		hql.append(" INNER JOIN MEDIDOR_MARCA marcaMedidor ON medidor.MEMA_CD = marcaMedidor.MEMA_CD ");
		hql.append(" INNER JOIN MEDIDOR_MODELO modelo ON medidor.MEMD_CD = modelo.MEMD_CD ");
		hql.append(" INNER JOIN MEDIDOR_MOTIVO_OPERACAO motivoOperacaoMedidor ON moh.MEMP_CD = motivoOperacaoMedidor.MEMP_CD ");
		if(equipe != null || servico != null) {
			//Operações de substituição e/ou retirada com envolvimento de AS
			hql.append(" INNER JOIN SERVICO_AUTORIZACAO servicoAutorizacao ON pontoConsumo.POCN_CD = servicoAutorizacao.POCN_CD ");
			hql.append(" INNER JOIN SERVICO_TIPO servicoTipo ON servicoAutorizacao.SRTI_CD = servicoTipo.SRTI_CD ");
			hql.append(" INNER JOIN EQUIPE equipe ON servicoAutorizacao.EQUI_CD = equipe.EQUI_CD ");
		}
		
		hql.append(" WHERE (moh.MEOH_DT_REALIZADA >= :dataInicial AND moh.MEOH_DT_REALIZADA <= :dataFinal) ");
		
		if(operacao != null && !operacao.isEmpty()) {
			hql.append(" AND moh.MEOH_DS = :operacao ");
		}else {
			hql.append(" AND (moh.MEOH_DS = 'Substituição' OR moh.MEOH_DS = 'Retirada') ");
		}
		
		if(equipe != null) {
			if(equipe.size() > 0) {
				if(equipe.get(0) != -1 || equipe.size() > 1) {
					hql.append(" AND equipe.EQUI_CD IN (:equipe) ");
				}
			}
		}
		
		if(servico != null) {
			if(servico.size() > 0) {
				if(servico.get(0) != -1 || servico.size() > 1) {
					hql.append(" AND servicoTipo.SRTI_CD IN (:servico) ");
				}
			}
			
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString())
		.addScalar("pontoConsumo",StringType.INSTANCE)
		.addScalar("medidorTipo", StringType.INSTANCE)
		.addScalar("medidorFabricante", StringType.INSTANCE)
		.addScalar("numeroSerie", StringType.INSTANCE)
		.addScalar("medidorModelo", StringType.INSTANCE)
		.addScalar("medidorBP", StringType.INSTANCE)
		.addScalar("medidorBP", StringType.INSTANCE)
		.addScalar("leituraFinal", BigDecimalType.INSTANCE)
		.addScalar("motivoOperacao",StringType.INSTANCE)
		.addScalar("dataOperacao",DateType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(SubstituicaoMedidorVO.class));
		
		if(dataInicial != null) {
			query.setParameter("dataInicial", dataInicial);
		}
		if(dataFinal != null) {
			query.setParameter("dataFinal", dataFinal);
		}
		if(operacao != null && !operacao.isEmpty()) {
			query.setParameter("operacao", operacao);
		}
		
		if(equipe != null) {
			if(equipe.size() > 0) {
				if(equipe.get(0) != -1 || equipe.size() > 1) {
					query.setParameterList("equipe", equipe);
				}
			}
		}
		
		if(servico != null) {
			if(servico.size() > 0) {
				if(servico.get(0) != -1 || servico.size() > 1) {
					query.setParameterList("servico", servico);
				}
			}
		}
		
		return query.list();
	}
	
}
