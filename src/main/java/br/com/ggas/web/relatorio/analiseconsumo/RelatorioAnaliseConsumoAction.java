/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.analiseconsumo;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseConsumo;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuMedicaoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Análise de Consumo.
 */
@Controller
public class RelatorioAnaliseConsumoAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioAnaliseConsumoAction.class);

	private static final String DATA_INICIAL = "dataEmissaoInicial";

	private static final String DATA_FINAL = "dataEmissaoFinal";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ERRO_PERIODO_NAO_INFORMADO = "ERRO_PERIODO_NAO_INFORMADO";

	private static final String ERRO_DATA_INICIAL_MAIOR_DATA_FINAL = "ERRO_DATA_INICIAL_MAIOR_DATA_FINAL";

	public static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorRelatorioAnaliseConsumo controladorRelatorioAnaliseConsumo;

	/**
	 * Método responsável por exibir a tela de
	 * filtro para exibição do relatório de
	 * análise de consumo.
	 * 
	 * @return exibirRelatorioAnaliseConsumo - {@link String}
	 */
	@RequestMapping("exibirRelatorioAnaliseConsumo")
	public String exibirRelatorioAnaliseConsumo() {

		return "exibirRelatorioAnaliseConsumo";
	}

	/**
	 * Gerar relatorio analise consumo.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String} Quando uma Exception for lançada
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioAnaliseConsumo")
	public String gerarRelatorioAnaliseConsumo(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao());

		try {

			Map<String, Object> filtro = this.prepararFiltro(relatorioSubMenuMedicaoVO);

			this.validarDataInicialFinal(relatorioSubMenuMedicaoVO);

			byte[] relatorio = controladorRelatorioAnaliseConsumo.gerarRelatorio(filtro, formatoImpressao);

			if (relatorio != null) {

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				ServletOutputStream servletOutputStream;
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioAnaliseConsumo" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}

		} catch (IOException e) {
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "exibirRelatorioAnaliseConsumo";
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String dataEmissaoInicial = relatorioSubMenuMedicaoVO.getDataEmissaoInicial();
		String dataEmissaoFinal = relatorioSubMenuMedicaoVO.getDataEmissaoFinal();
		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuMedicaoVO.isExibirFiltros());

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (StringUtils.isNotEmpty(dataEmissaoInicial)) {
			filtro.put(DATA_INICIAL, Util.converterMesAnoEmAnoMes(dataEmissaoInicial));
		}

		if (StringUtils.isNotEmpty(dataEmissaoFinal)) {
			filtro.put(DATA_FINAL, Util.converterMesAnoEmAnoMes(dataEmissaoFinal));
		}

		return filtro;
	}

	/**
	 * Validar data inicial final.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarDataInicialFinal(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) throws GGASException {

		String dataEmissaoInicial = relatorioSubMenuMedicaoVO.getDataEmissaoInicial();
		String dataEmissaoFinal = relatorioSubMenuMedicaoVO.getDataEmissaoFinal();

		try {

			if (dataEmissaoInicial.isEmpty() || dataEmissaoFinal.isEmpty()) {
				throw new NegocioException(ERRO_PERIODO_NAO_INFORMADO, true);
			} else {
				DateFormat formatter = new SimpleDateFormat("MM/yyyy");

				Date dateInicial = formatter.parse(dataEmissaoInicial);
				Date dateFinal = formatter.parse(dataEmissaoFinal);

				if (dateInicial.compareTo(dateFinal) > 0) {
					throw new NegocioException(ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}

			}

		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(Constantes.ERRO_DATA_INVALIDADE, true);
		}
	}

}
