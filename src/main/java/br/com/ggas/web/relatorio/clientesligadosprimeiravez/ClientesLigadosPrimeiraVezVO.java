package br.com.ggas.web.relatorio.clientesligadosprimeiravez;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientesLigadosPrimeiraVezVO {

	private String codigoUnico;
	private String nomeCliente;
	private String descricaoPontoConsumo;
	private String tipoLogradouro;
	private String logradouro;
	private String bairro;
	private String dddNumeroTelefone;
	private String numeroTelefone;
	private String email;
	private String situacaoPontoConsumo;
	private String segmento;
	private String cep;
	private String numeroImovel;
	private Date dataAtivacao;
	private String dataFormatada;
	private String medidorModelo;
	private String numeroSerieMedidor;
	private String pressaoEntrega;
	
	public String getCodigoUnico() {
		return codigoUnico;
	}
	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public String getTipoLogradouro() {
		return tipoLogradouro;
	}
	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getDddNumeroTelefone() {
		return dddNumeroTelefone;
	}
	public void setDddNumeroTelefone(String dddNumeroTelefone) {
		this.dddNumeroTelefone = dddNumeroTelefone;
	}
	public String getNumeroTelefone() {
		return numeroTelefone;
	}
	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSituacaoPontoConsumo() {
		return situacaoPontoConsumo;
	}
	public void setSituacaoPontoConsumo(String situacaoPontoConsumo) {
		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}
	public String getSegmento() {
		return segmento;
	}
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getNumeroImovel() {
		return numeroImovel;
	}
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	public Date getDataAtivacao() {
		return dataAtivacao;
	}
	public void setDataAtivacao(Date dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}
	public String getDataFormatada() {
		return dataFormatada;
	}
	public void setDataFormatada(Date dataAtivacao) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.dataFormatada = sdf.format(dataAtivacao);
	}
	public String getMedidorModelo() {
		return medidorModelo;
	}
	public void setMedidorModelo(String medidorModelo) {
		this.medidorModelo = medidorModelo;
	}
	public String getNumeroSerieMedidor() {
		return numeroSerieMedidor;
	}
	public void setNumeroSerieMedidor(String numeroSerieMedidor) {
		this.numeroSerieMedidor = numeroSerieMedidor;
	}
	public String getPressaoEntrega() {
		return pressaoEntrega;
	}
	public void setPressaoEntrega(String pressaoEntrega) {
		this.pressaoEntrega = pressaoEntrega;
	}
	
	
}
