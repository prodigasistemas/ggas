/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/09/2014 15:31:19
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

/**
 * Classe referentes as chamadas da classe PesquisaRelatorioAcompanhamentoClientesVO
 * 
 * @author vpessoa
 */
public class PesquisaRelatorioAcompanhamentoClientesVO {

	private Long statusProposta;

	private Long vendedor;

	private Long fiscal;

	private Integer volumeInicial;

	private Integer volumeFinal;

	private String dataEntregaPropostaInicial;

	private String dataEntregaPropostaFinal;

	private String dataAssinaturaContratoInicial;

	private String dataAssinaturaContratoFinal;

	private String dataEmissaoAutorizacaoServicoInicial;

	private String dataEmissaoAutorizacaoServicoFinal;

	private Double valorOrcamentoInicial;

	private Double valorOrcamentoFinal;

	private Long idCliente;

	public Long getIdCliente() {

		return idCliente;
	}

	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

	public Long getStatusProposta() {

		return statusProposta;
	}

	public void setStatusProposta(Long statusProposta) {

		this.statusProposta = statusProposta;
	}

	public Long getVendedor() {

		return vendedor;
	}

	public void setVendedor(Long vendedor) {

		this.vendedor = vendedor;
	}

	public Long getFiscal() {

		return fiscal;
	}

	public void setFiscal(Long fiscal) {

		this.fiscal = fiscal;
	}

	public Integer getVolumeInicial() {

		return volumeInicial;
	}

	public void setVolumeInicial(Integer volumeInicial) {

		this.volumeInicial = volumeInicial;
	}

	public Integer getVolumeFinal() {

		return volumeFinal;
	}

	public void setVolumeFinal(Integer volumeFinal) {

		this.volumeFinal = volumeFinal;
	}

	public String getDataEntregaPropostaInicial() {

		return dataEntregaPropostaInicial;
	}

	public void setDataEntregaPropostaInicial(String dataEntregaPropostaInicial) {

		this.dataEntregaPropostaInicial = dataEntregaPropostaInicial;
	}

	public String getDataEntregaPropostaFinal() {

		return dataEntregaPropostaFinal;
	}

	public void setDataEntregaPropostaFinal(String dataEntregaPropostaFinal) {

		this.dataEntregaPropostaFinal = dataEntregaPropostaFinal;
	}

	public String getDataAssinaturaContratoInicial() {

		return dataAssinaturaContratoInicial;
	}

	public void setDataAssinaturaContratoInicial(String dataAssinaturaContratoInicial) {

		this.dataAssinaturaContratoInicial = dataAssinaturaContratoInicial;
	}

	public String getDataAssinaturaContratoFinal() {

		return dataAssinaturaContratoFinal;
	}

	public void setDataAssinaturaContratoFinal(String dataAssinaturaContratoFinal) {

		this.dataAssinaturaContratoFinal = dataAssinaturaContratoFinal;
	}

	public String getDataEmissaoAutorizacaoServicoInicial() {

		return dataEmissaoAutorizacaoServicoInicial;
	}

	public void setDataEmissaoAutorizacaoServicoInicial(String dataEmissaoAutorizacaoServicoInicial) {

		this.dataEmissaoAutorizacaoServicoInicial = dataEmissaoAutorizacaoServicoInicial;
	}

	public String getDataEmissaoAutorizacaoServicoFinal() {

		return dataEmissaoAutorizacaoServicoFinal;
	}

	public void setDataEmissaoAutorizacaoServicoFinal(String dataEmissaoAutorizacaoServicoFinal) {

		this.dataEmissaoAutorizacaoServicoFinal = dataEmissaoAutorizacaoServicoFinal;
	}

	public Double getValorOrcamentoInicial() {

		return valorOrcamentoInicial;
	}

	public void setValorOrcamentoInicial(Double valorOrcamentoInicial) {

		this.valorOrcamentoInicial = valorOrcamentoInicial;
	}

	public Double getValorOrcamentoFinal() {

		return valorOrcamentoFinal;
	}

	public void setValorOrcamentoFinal(Double valorOrcamentoFinal) {

		this.valorOrcamentoFinal = valorOrcamentoFinal;
	}

	/**
	 * Possui parametros.
	 * 
	 * @return the boolean
	 */
	public Boolean possuiParametros() {

		if(this.getFiscal() != null || this.getVendedor() != null
						|| (this.getDataAssinaturaContratoInicial() != null && this.getDataAssinaturaContratoFinal() != null)
						|| (this.getDataAssinaturaContratoFinal() != null && this.getDataAssinaturaContratoInicial() != null)
						|| (this.getDataEmissaoAutorizacaoServicoInicial() != null && this.getDataEmissaoAutorizacaoServicoFinal() != null)
						|| this.getIdCliente() != null) {
			return true;
		}
		return false;
	}

}
