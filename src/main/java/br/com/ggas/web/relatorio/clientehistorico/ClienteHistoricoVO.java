/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.clientehistorico;

import java.util.Collection;
/**
 * Classe responsável pela transferência de dados relacionados
 * ao histórico do ponto de consumo vinculado a um cliente
 *
 */
public class ClienteHistoricoVO {

	private String nomeCliente;

	private Collection<PontoConsumoHistoricoVO> listaPontoConsumoHistoricoVO;

	/**
	 * Instantiates a new cliente historico vo.
	 */
	public ClienteHistoricoVO() {

	}

	/**
	 * Instantiates a new cliente historico vo.
	 * 
	 * @param nomeCliente
	 *            the nome cliente
	 * @param listaPontoConsumoHistoricoVO
	 *            the lista ponto consumo historico vo
	 */
	public ClienteHistoricoVO(String nomeCliente, Collection<PontoConsumoHistoricoVO> listaPontoConsumoHistoricoVO) {

		this.nomeCliente = nomeCliente;
		this.listaPontoConsumoHistoricoVO = listaPontoConsumoHistoricoVO;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public Collection<PontoConsumoHistoricoVO> getListaPontoConsumoHistoricoVO() {

		return listaPontoConsumoHistoricoVO;
	}

	public void setListaPontoConsumoHistoricoVO(Collection<PontoConsumoHistoricoVO> listaPontoConsumoHistoricoVO) {

		this.listaPontoConsumoHistoricoVO = listaPontoConsumoHistoricoVO;
	}

}
