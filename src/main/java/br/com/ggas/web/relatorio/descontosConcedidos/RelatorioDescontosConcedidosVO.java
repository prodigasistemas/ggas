/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.descontosConcedidos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RelatorioDescontosConcedidosVO implements Serializable {

	private static final long serialVersionUID = 4230327478511820371L;

	private String pontoConsumo;

	private String segmento;

	private String notaFiscal;

	private Date emissao;

	private Date vencimento;

	private BigDecimal volume;

	private BigDecimal desconto;

	private String descricao;

	private String nomeCliente;

	private BigDecimal somaCredito;

	public BigDecimal getSomaCredito() {

		return somaCredito;
	}

	public void setSomaCredito(BigDecimal somaCredito) {

		this.somaCredito = somaCredito;
	}

	public String getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getNotaFiscal() {

		return notaFiscal;
	}

	public void setNotaFiscal(String notaFiscal) {

		this.notaFiscal = notaFiscal;
	}

	public Date getEmissao() {
		Date data = null;
		if (this.emissao != null) {
			data = (Date) emissao.clone();
		}
		return data;
	}

	public void setEmissao(Date emissao) {

		this.emissao = emissao;
	}

	public Date getVencimento() {
		Date data = null;
		if (this.vencimento != null) {
			data = (Date) vencimento.clone();
		}
		return data;
	}

	public void setVencimento(Date vencimento) {

		this.vencimento = vencimento;
	}

	public BigDecimal getVolume() {

		return volume;
	}

	public void setVolume(BigDecimal volume) {

		this.volume = volume;
	}

	public BigDecimal getDesconto() {

		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {

		this.desconto = desconto;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

}
