/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.quantidadesContratadas;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioQuantidadesContratadas;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioContratoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Quantidades Contratadas.
 */
@Controller
public class RelatorioQuantidadesContratadasAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioQuantidadesContratadasAction.class);

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";

	private static final String DATA_ASSINATURA = "dataAssinatura";

	private static final String DATA_VENCIMENTO = "dataVencimento";

	private static final String ID_CLIENTE = "idCliente";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String ID_SEGMENTO = "idSegmento";

	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	private static final String ERRO_PERIODO_NAO_INFORMADO = "ERRO_PERIODO_NAO_INFORMADO";

	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioQuantidadesContratadas controladorRelatorioQuantidadesContratadas;

	/**
	 * Exibir pesquisa relatorio quantidades contratadas.
	 * 
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioQuantidadesContratadas - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioQuantidadesContratadas")
	public String exibirPesquisaRelatorioQuantidadesContratadas(Model model) throws GGASException {

		popularFiltrosRelatorioQuantidadesContratada(model);

		model.addAttribute(LISTA_TIPO_GRAFICO, RelatorioUtil.TipoGrafico.listar());

		return "exibirPesquisaRelatorioQuantidadesContratadas";
	}
	
	/**
	 * Gerar relatorio quantidades contratadas.
	 * 
	 * @param relatorioContratoVO - {@link RelatorioContratoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioQuantidadesContratadas - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioQuantidadesContratadas")
	public String gerarRelatorioQuantidadesContratadas(RelatorioContratoVO relatorioContratoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		saveToken(request);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioContratoVO.getFormatoImpressao());

		try {

			Map<String, Object> filtro = this.prepararFiltro(relatorioContratoVO);

			this.validarFiltro(relatorioContratoVO, request);

			byte[] relatorioQuantidadesContratadas = controladorRelatorioQuantidadesContratadas.gerarRelatorio(filtro, formatoImpressao);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioQuantidadesContratadas.length);
			response.addHeader("Content-Disposition",
					"attachment; filename=relatorioQuantidadesContratadas" + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorioQuantidadesContratadas, 0, relatorioQuantidadesContratadas.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute("relatorioContratoVO", relatorioContratoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);

		return exibirPesquisaRelatorioQuantidadesContratadas(model);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioContratoVO - {@link RelatorioContratoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link}
	 */
	private Map<String, Object> prepararFiltro(RelatorioContratoVO relatorioContratoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long idCliente = relatorioContratoVO.getIdCliente();
		String dataInicial = relatorioContratoVO.getDataInicial();
		String dataFinal = relatorioContratoVO.getDataFinal();
		String dataVencimento = relatorioContratoVO.getDataVencimento();
		String dataAssinatura = relatorioContratoVO.getDataAssinatura();
		String numeroContrato = relatorioContratoVO.getNumeroContrato();
		Long idSegmento = relatorioContratoVO.getIdSegmento();

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}
		
		if (idSegmento != null && idSegmento > 0) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		if (StringUtils.isNotEmpty(dataInicial)) {
			Date dataInicialAux = Util.converterCampoStringParaData("Data Inicial", dataInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_INICIAL, dataInicialAux);
		}

		if (StringUtils.isNotEmpty(dataFinal)) {
			Date dataFinalAux = Util.converterCampoStringParaData("Data Final", dataFinal, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_FINAL, dataFinalAux);
		}

		if (StringUtils.isNotEmpty(dataVencimento)) {
			Date dataVencimentoAux = Util.converterCampoStringParaData("Data Vencimento", dataVencimento, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_VENCIMENTO, dataVencimentoAux);
		}

		if (StringUtils.isNotEmpty(dataAssinatura)) {
			Date dataAssinaturaAux = Util.converterCampoStringParaData("Data Assinatura", dataAssinatura, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_ASSINATURA, dataAssinaturaAux);
		}

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTRATO, numeroContrato);
		}

		filtro.put(EXIBIR_FILTROS, relatorioContratoVO.isExibirFiltros());

		return filtro;
	}

	/**
	 * Popular filtros relatorio quantidades contratada.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	public void popularFiltrosRelatorioQuantidadesContratada(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute("listaSegmento", listaSegmento);
	}

	/**
	 * Validar filtro.
	 * 
	 * @param relatorioContratoVO - {@link RelatorioContratoVO}
	 * @param request - {@link HttpServletRequest}
	 * @throws GGASException - {@link GGASException}
	 */
	public void validarFiltro(RelatorioContratoVO relatorioContratoVO, HttpServletRequest request) throws GGASException {

		String dataInicial = relatorioContratoVO.getDataInicial();
		String dataFinal = relatorioContratoVO.getDataFinal();

		if (StringUtils.isEmpty(dataInicial) || StringUtils.isEmpty(dataFinal)) {
			throw new NegocioException(ERRO_PERIODO_NAO_INFORMADO, true);
		}

	}

}
