/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.resumoVolume;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioResumoVolume;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuMedicaoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Resumo de Volume.
 */
@Controller
public class RelatorioResumoVolumeAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioResumoVolumeAction.class);

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ANO = "ano";

	private static final String MES = "mes";

	private static final String ID_RAMO_ATIVIDADE = "idRamoAtividade";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;
	
	@Autowired
	private ControladorRelatorioResumoVolume controladorRelatorioResumoVolume;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	/**
	 * Método responsável por exibir o relatorio
	 * resumo volume.
	 * 
	 * @param model - {@link Model}
	 * @return exibirRelatorioResumoVolume - {@link}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioResumoVolume")
	public String exibirRelatorioResumoVolume(Model model) throws GGASException {

		try {
			popularFiltrosRelatorioResumoVolume(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirRelatorioResumoVolume";
	}

	/**
	 * Pesquisar ponto consumo relatorio resumo volume.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return exibirRelatorioResumoVolume - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatorioResumoVolume")
	public String pesquisarPontoConsumoRelatorioResumoVolume(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, Model model) throws GGASException {

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		try {

			if ((idImovel != null) && (idImovel > 0)) {
				listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
			} else if ((idCliente != null) && (idCliente > 0)) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
			}

			popularListaRamoAtividade(relatorioSubMenuMedicaoVO, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute("listaPontoConsumo", listaPontoConsumo);
		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return exibirRelatorioResumoVolume(model);
	}
	
	/**
	 * Método responsável por gerar relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioResumoVolume")
	public String gerarRelatorioResumoVolume(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		try {

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			Map<String, Object> filtro = prepararFiltro(relatorioSubMenuMedicaoVO);

			controladorRelatorioResumoVolume.validarFiltroRelatorioResumoVolume(filtro);

			byte[] relatorioResumoVolume = controladorRelatorioResumoVolume.gerarRelatorioResumoVolume(filtro);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			FormatoImpressao formatoImpressao = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioResumoVolume.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorioResumoVolume"
					+ this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));
			servletOutputStream.write(relatorioResumoVolume, 0, relatorioResumoVolume.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioResumoVolume(model);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}
		
	/**
	 * Método responsável por preencher lista de Ramo de Atividade de acordo com segmento
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param model - {@link Model}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void popularListaRamoAtividade(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Model model) throws NegocioException {

		Long idSegmento = relatorioSubMenuMedicaoVO.getIdSegmento();

		if (idSegmento != null && idSegmento > 0) {
			Collection<RamoAtividade> listaRamoAtividade = controladorRamoAtividade.listarRamoAtividadePorSegmento(idSegmento);
			model.addAttribute("listaRamoAtividade", listaRamoAtividade);
		}
	}

	/**
	 * Popular filtros relatorio resumo volume.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	public void popularFiltrosRelatorioResumoVolume(Model model) throws GGASException {

		Collection<String> listaAnos = Util.listarAnos("20");
		model.addAttribute("listaAnos", listaAnos);

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute("listaSegmento", listaSegmento);

		model.addAttribute("listaMeses", Mes.MESES_ANO);
	}

	/**
	 * Método responsável por preparar os filtros
	 * do relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String ano = relatorioSubMenuMedicaoVO.getAno();
		String mes = relatorioSubMenuMedicaoVO.getMes();
		String tipoExibicao = relatorioSubMenuMedicaoVO.getTipoExibicao();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuMedicaoVO.isExibirFiltros());
		filtro.put(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao()));

		if (StringUtils.isNotEmpty(ano)) {
			filtro.put(ANO, ano);
		}

		if (StringUtils.isNotEmpty(mes)) {
			filtro.put(MES, mes);
		}

		if (StringUtils.isNotEmpty(tipoExibicao)) {
			filtro.put(TIPO_EXIBICAO, tipoExibicao);
		}

		prepararFiltroDois(relatorioSubMenuMedicaoVO, filtro);

		return filtro;

	}

	/**
	 * Método responsável por preparar os filtros
	 * do relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @return filtro - {@link Map}
	 */
	private void prepararFiltroDois(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Map<String, Object> filtro) {

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idSegmento = relatorioSubMenuMedicaoVO.getIdSegmento();
		Long ramoAtividade = relatorioSubMenuMedicaoVO.getIdRamoAtividade();
		Long[] chavesPontoConsumo = relatorioSubMenuMedicaoVO.getChavesPontoConsumo();

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if ((idSegmento != null) && (idSegmento > 0)) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		if (ramoAtividade != null && ramoAtividade > 0) {
			filtro.put(ID_RAMO_ATIVIDADE, ramoAtividade);
		}

		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			filtro.put(CHAVES_PONTO_CONSUMO, chavesPontoConsumo);
		}

	}

}
