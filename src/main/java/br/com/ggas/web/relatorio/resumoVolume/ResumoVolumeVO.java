/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.resumoVolume;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * 
 * Classe responsável pela representação da entidade ResumoVolume
 *
 */
public class ResumoVolumeVO {
	
	private static final Logger LOG = Logger.getLogger(ResumoVolumeVO.class);

	private BigDecimal volumeNoMes;

	private BigDecimal volumeNoMesPorDia;

	private BigDecimal volumeAteMes;

	private BigDecimal volumeAteMesPorDia;

	private BigDecimal totalVolumeNoMes;

	private BigDecimal totalVolumeNoMesPorDia;

	private BigDecimal totalVolumeAteMes;

	private BigDecimal totalVolumeAteMesPorDia;

	private BigDecimal quantidadePontoConsumo;

	private BigDecimal totalPontoConsumo;

	private String nomeCliente;

	private String descricaoSegmento;

	private String descricaoPontoConsumo;

	private String referenciaCiclo;

	public BigDecimal getVolumeNoMes() {

		return volumeNoMes;
	}

	public String getVolumeNoMesFormatado() {

		String retorno = " - ";
		if(volumeNoMes != null) {
			retorno = Util.converterCampoValorDecimalParaString("", volumeNoMes, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public void setVolumeNoMes(BigDecimal volumeNoMes) {

		this.volumeNoMes = volumeNoMes;
	}

	public BigDecimal getVolumeNoMesPorDia() {

		return volumeNoMesPorDia;
	}

	public String getVolumeNoMesPorDiaFormatado() {

		String retorno = " - ";
		if(volumeNoMesPorDia != null) {
			retorno = Util.converterCampoValorDecimalParaString("", volumeNoMesPorDia, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public void setVolumeNoMesPorDia(BigDecimal volumeNoMesPorDia) {

		this.volumeNoMesPorDia = volumeNoMesPorDia;
	}

	public BigDecimal getVolumeAteMes() {

		return volumeAteMes;
	}

	public String getVolumeAteMesFormatado() {

		String retorno = " - ";
		if(volumeAteMes != null) {
			retorno = Util.converterCampoValorDecimalParaString("", volumeAteMes, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public void setVolumeAteMes(BigDecimal volumeAteMes) {

		this.volumeAteMes = volumeAteMes;
	}

	public BigDecimal getVolumeAteMesPorDia() {

		return volumeAteMesPorDia;
	}

	public String getVolumeAteMesPorDiaFormatado() {

		String retorno = " - ";
		if(volumeAteMesPorDia != null) {
			retorno = Util.converterCampoValorDecimalParaString("", volumeAteMesPorDia, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public void setVolumeAteMesPorDia(BigDecimal volumeAteMesPorDia) {

		this.volumeAteMesPorDia = volumeAteMesPorDia;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public BigDecimal getQuantidadePontoConsumo() {

		return quantidadePontoConsumo;
	}

	public BigDecimal getTotalPontoConsumo() {

		return totalPontoConsumo;
	}

	public BigDecimal getTotalVolumeNoMes() {

		return totalVolumeNoMes;
	}

	public void setTotalVolumeNoMes(BigDecimal totalVolumeNoMes) {

		this.totalVolumeNoMes = totalVolumeNoMes;
	}

	public BigDecimal getTotalVolumeNoMesPorDia() {

		return totalVolumeNoMesPorDia;
	}

	public void setTotalVolumeNoMesPorDia(BigDecimal totalVolumeNoMesPorDia) {

		this.totalVolumeNoMesPorDia = totalVolumeNoMesPorDia;
	}

	public BigDecimal getTotalVolumeAteMes() {

		return totalVolumeAteMes;
	}

	public void setTotalVolumeAteMes(BigDecimal totalVolumeAteMes) {

		this.totalVolumeAteMes = totalVolumeAteMes;
	}

	public BigDecimal getTotalVolumeAteMesPorDia() {

		return totalVolumeAteMesPorDia;
	}

	public void setTotalVolumeAteMesPorDia(BigDecimal totalVolumeAteMesPorDia) {

		this.totalVolumeAteMesPorDia = totalVolumeAteMesPorDia;
	}

	public void setQuantidadePontoConsumo(BigDecimal quantidadePontoConsumo) {

		this.quantidadePontoConsumo = quantidadePontoConsumo;
	}

	public void setTotalPontoConsumo(BigDecimal totalPontoConsumo) {

		this.totalPontoConsumo = totalPontoConsumo;
	}

	public String getReferenciaCiclo() {

		return referenciaCiclo;
	}

	public void setReferenciaCiclo(String referenciaCiclo) {

		this.referenciaCiclo = referenciaCiclo;
	}
}
