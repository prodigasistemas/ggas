/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.clientehistorico;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela transferência de dados relacionados ao histórico de fatura
 *
 */
public class FaturaHistoricoVO {

	private String numeroTitulo;

	private String serieTitulo;

	private String tipoTitulo;

	private Date dataEmissao;

	private Date datasVencimento;

	private Date dataPagamento;

	private BigDecimal valorDocumento;

	private BigDecimal valorCredito;

	private BigDecimal valorPago;

	private BigDecimal valorAcumulado;

	private Integer qtdDiasAtrasado;

	private BigDecimal saldo;

	/**
	 * Instantiates a new fatura historico vo.
	 */
	public FaturaHistoricoVO() {

	}

	/**
	 * Instantiates a new fatura historico vo.
	 * 
	 * @param numeroTitulo
	 *            the numero titulo
	 * @param serieTitulo
	 *            the serie titulo
	 * @param tipoTitulo
	 *            the tipo titulo
	 * @param dataEmissao
	 *            the data emissao
	 * @param dataVencimento
	 *            the data vencimento
	 * @param dataPagamento
	 *            the data pagamento
	 * @param valorDocumento
	 *            the valor documento
	 * @param valorCredito
	 *            the valor credito
	 * @param valorPago
	 *            the valor pago
	 * @param valorAcumulado
	 *            the valor acumulado
	 * @param qtdDiasAtrasado
	 *            the qtd dias atrasado
	 * @param saldo
	 *            the saldo
	 */
	public FaturaHistoricoVO(String numeroTitulo, String serieTitulo, String tipoTitulo, Date dataEmissao, Date dataVencimento,
								Date dataPagamento, BigDecimal valorDocumento, BigDecimal valorCredito, BigDecimal valorPago,
								BigDecimal valorAcumulado, Integer qtdDiasAtrasado, BigDecimal saldo) {

		super();
		this.numeroTitulo = numeroTitulo;
		this.serieTitulo = serieTitulo;
		this.tipoTitulo = tipoTitulo;
		this.dataEmissao = dataEmissao;
		this.datasVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
		this.valorDocumento = valorDocumento;
		this.valorCredito = valorCredito;
		this.valorPago = valorPago;
		this.valorAcumulado = valorAcumulado;
		this.qtdDiasAtrasado = qtdDiasAtrasado;
		this.saldo = saldo;
	}

	public String getNumeroTitulo() {

		return numeroTitulo;
	}

	public void setNumeroTitulo(String numeroTitulo) {

		this.numeroTitulo = numeroTitulo;
	}

	public String getSerieTitulo() {

		return serieTitulo;
	}

	public void setSerieTitulo(String serieTitulo) {

		this.serieTitulo = serieTitulo;
	}

	public String getTipoTitulo() {

		return tipoTitulo;
	}

	public void setTipoTitulo(String tipoTitulo) {

		this.tipoTitulo = tipoTitulo;
	}

	public Date getDataEmissao() {
		Date data = null;
		if(this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {
		if(dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.datasVencimento != null) {
			data = (Date) datasVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {
		if(dataVencimento != null) {
			this.datasVencimento = (Date) dataVencimento.clone();
		} else {
			this.datasVencimento = null;
		}
	}

	public Date getDataPagamento() {
		Date data = null;
		if (this.dataPagamento != null) {
			data = (Date) dataPagamento.clone();
		}
		return data;
	}

	public void setDataPagamento(Date dataPagamento) {
		if(dataPagamento != null) {
			this.dataPagamento = (Date) dataPagamento.clone();
		} else {
			this.dataPagamento = null;
		}
	}

	public BigDecimal getValorDocumento() {

		return valorDocumento;
	}

	public void setValorDocumento(BigDecimal valorDocumento) {

		this.valorDocumento = valorDocumento;
	}

	public BigDecimal getValorCredito() {

		return valorCredito;
	}

	public void setValorCredito(BigDecimal valorCredito) {

		this.valorCredito = valorCredito;
	}

	public BigDecimal getValorPago() {

		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {

		this.valorPago = valorPago;
	}

	public BigDecimal getValorAcumulado() {

		return valorAcumulado;
	}

	public void setValorAcumulado(BigDecimal valorAcumulado) {

		this.valorAcumulado = valorAcumulado;
	}

	public Integer getQtdDiasAtrasado() {

		return qtdDiasAtrasado;
	}

	public void setQtdDiasAtrasado(Integer qtdDiasAtrasado) {

		this.qtdDiasAtrasado = qtdDiasAtrasado;
	}

	public BigDecimal getSaldo() {

		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	/**
	 * Sets the atributos fatura documento fiscal.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param documentoFiscal
	 *            the documento fiscal
	 */
	public void setAtributosFaturaDocumentoFiscal(Fatura fatura, DocumentoFiscal documentoFiscal) {

		Date dataVencimento = fatura.getDataVencimento();
		String tipoDocumento = fatura.getTipoDocumento().getDescricao();
		Date dataEmissao = fatura.getDataEmissao();
		BigDecimal valorDocumento = fatura.getValorTotal();

		this.setNumeroTitulo(documentoFiscal.getNumero().toString());
		this.setSerieTitulo(documentoFiscal.getDescricaoSerie());

		if(dataVencimento != null) {
			this.setDataVencimento(dataVencimento);
		}

		if(tipoDocumento != null && !tipoDocumento.isEmpty()) {
			this.setTipoTitulo(tipoDocumento);
		}

		if(dataEmissao != null) {
			this.setDataEmissao(dataEmissao);
		}

		if(valorDocumento != null) {
			if(documentoFiscal.isTipoOperacaoEntrada()) {
				this.setValorDocumento(valorDocumento.negate());
			} else {
				this.setValorDocumento(valorDocumento);
			}
		}

	}

	/**
	 * Sets the atributos fatura recebimento.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param recebimento
	 *            the recebimento
	 * @param debitosACobrarVO
	 *            the debitos a cobrar vo
	 */
	public void setAtributosFaturaRecebimento(Fatura fatura, Recebimento recebimento, DebitosACobrar debitosACobrarVO) {

		Date dataPagamento = recebimento.getDataRecebimento();
		BigDecimal valorPago = recebimento.getValorRecebimento();

		this.setDataPagamento(dataPagamento);
		this.setValorPago(valorPago);

		if(recebimento.getValorCredito() != null) {
			this.setValorCredito(recebimento.getValorCobranca());
		}

		if(fatura.getDataVencimento().before(dataPagamento)) {
			Integer diasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), dataPagamento);
			if(diasAtraso > 0) {
				this.setQtdDiasAtrasado(diasAtraso);
			}
		}

		if(debitosACobrarVO != null) {
			if(debitosACobrarVO.getJurosMora() != null) {
				BigDecimal saldo = fatura.getValorTotal().add(debitosACobrarVO.getJurosMora()).subtract(fatura.getValorTotal());
				this.setSaldo(saldo);
			}
			if(debitosACobrarVO.getMulta() != null) {
				BigDecimal saldo = fatura.getValorTotal().add(debitosACobrarVO.getMulta()).subtract(fatura.getValorTotal());
				this.setSaldo(saldo);
			}
		}

	}

}
