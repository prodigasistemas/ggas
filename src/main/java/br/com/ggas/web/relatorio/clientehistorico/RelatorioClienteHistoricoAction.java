/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.clientehistorico;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioHistoricoCliente;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioCadastroVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Histórico do Cliente.
 */
@Controller
public class RelatorioClienteHistoricoAction extends GenericAction {

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ID_CLIENTE = "idCliente";
	
	private static final String CLIENTE = "cliente";

	private static final String CODIGO_PONTO_CONSUMO = "codigoPontoConsumo";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String SITUACAO = "situacao";

	private static final String RELATORIO_HISTORICO_CLIENTE = "relatorioHistoricoCliente";

	private static final String ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO = 
					"ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO";

	private static final String ERRO_NEGOCIO_CLIENTE = "ERRO_NEGOCIO_CLIENTE";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String FILTRO_CLIENTE = "filtroCliente";

	private static final String FILTRO_CODIGO_PONTO_CONSUMO = "filtroCodigoPontoConsumo";

	private static final String FILTRO_DATA_EMISSAO_INICIAL = "filtroDataEmissaoInicial";

	private static final String FILTRO_DATA_EMISSAO_FINAL = "filtroDataEmissaoFinal";

	private static final String FILTRO_DATA_VENCIMENTO_INICIAL = "filtroDataVencimentoInicial";

	private static final String FILTRO_DATA_VENCIMENTO_FINAL = "filtroDataVencimentoFinal";

	private static final String FILTRO_SITUACAO = "filtroSituacao";
	
	public static final String EXIBIR_FILTROS = "exibirFiltros";
	
	protected static final String FORMATO_IMPRESSAO = "formatoImpressao";
	
	protected static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorRelatorioHistoricoCliente controladorRelatorioHistoricoCliente;
	
	@Autowired
	private ControladorCliente controladorCliente;

	/**
	 * Exibir pesquisa relatorio historico cliente.
	 * 
	 * @return exibirPesquisaRelatorioHistoricoCliente - {@link String}
	 */
	@RequestMapping("exibirPesquisaRelatorioHistoricoCliente")
	public String exibirPesquisaRelatorioHistoricoCliente() {

		return "exibirPesquisaRelatorioHistoricoCliente";
	}

	/**
	 * Pesquisar ponto consumo historico cliente.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioHistoricoCliente - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoHistoricoCliente")
	public String pesquisarPontoConsumoHistoricoCliente(RelatorioCadastroVO relatorioCadastroVO, BindingResult esult,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		Long idCliente = relatorioCadastroVO.getIdCliente();

		try {

			validarForm(relatorioCadastroVO, model);

			popularFiltroRelatorio(relatorioCadastroVO);

			if (idCliente != null && idCliente > 0) {
				Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorContratoAtivoECliente(idCliente);
				model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
			} else {
				throw new NegocioException(ERRO_NEGOCIO_CLIENTE, true);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("relatorioCadastroVO", relatorioCadastroVO);
		model.addAttribute(CLIENTE, clienteImovelPesquisaPopupVO);

		return exibirPesquisaRelatorioHistoricoCliente();
	}

	/**
	 * Gerar relatorio historico cliente.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioHistoricoCliente - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioHistoricoCliente")
	public String gerarRelatorioHistoricoCliente(RelatorioCadastroVO relatorioCadastroVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String exibirGridPontoConsumo = request.getParameter("exibirGridPontoConsumo");

		try {

			validarForm(relatorioCadastroVO, model);

			Boolean exibirFiltros = relatorioCadastroVO.getExibirFiltros();
			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioCadastroVO.getFormatoImpressao());

			Map<String, Object> filtro = popularFiltroRelatorio(relatorioCadastroVO);

			filtro.put(EXIBIR_FILTROS, exibirFiltros);

			if (StringUtils.isNotEmpty(exibirGridPontoConsumo)) {
				filtro.remove(CHAVES_PRIMARIAS);
			}

			filtro.put(FORMATO_IMPRESSAO, formatoImpressao);
			byte[] relatorio = controladorRelatorioHistoricoCliente.gerarRelatorio(filtro);

			if (relatorio != null) {

				request.setAttribute(EXIBIR_MENSAGEM_TELA, false);
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						Util.montarRelatorioContentDisposition(RELATORIO_HISTORICO_CLIENTE, formatoImpressao));
				response.getOutputStream().write(relatorio);
			}

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("relatorioCadastroVO", relatorioCadastroVO);
		model.addAttribute(CLIENTE, clienteImovelPesquisaPopupVO);

		return exibirPesquisaRelatorioHistoricoCliente();
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @return filtroRelatorio - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> popularFiltroRelatorio(RelatorioCadastroVO relatorioCadastroVO) throws GGASException {
	
		Long idCliente = relatorioCadastroVO.getIdCliente();
		Long codigoPontoConsumo = relatorioCadastroVO.getCodigoPontoConsumo();
		Long[] chavesPrimarias = relatorioCadastroVO.getChavesPrimarias();

		Map<String, Object> filtroRelatorio = new HashMap<String, Object>();

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			filtroRelatorio.put(CHAVES_PRIMARIAS, chavesPrimarias);
		}
		if (idCliente != null && idCliente > 0) {
			filtroRelatorio.put(ID_CLIENTE, idCliente);
			Cliente cliente = controladorCliente.obterCliente(idCliente);
			String filtroCliente = cliente.getNome();
			filtroRelatorio.put(FILTRO_CLIENTE, filtroCliente);
		}
		if (codigoPontoConsumo != null && codigoPontoConsumo > 0) {
			filtroRelatorio.put(CODIGO_PONTO_CONSUMO, codigoPontoConsumo);
			PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(codigoPontoConsumo);
			if (pontoConsumo != null) {
				String filtroCodigoPontoConsumo = pontoConsumo.getCodigoPontoConsumo();
				filtroRelatorio.put(FILTRO_CODIGO_PONTO_CONSUMO, filtroCodigoPontoConsumo);
			} else {
				throw new NegocioException(ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO, true);
			}
		}
		
		popularFiltroRelatorioCampoData(relatorioCadastroVO, filtroRelatorio);
		popularFiltroRelatorioDois(relatorioCadastroVO, filtroRelatorio);

		return filtroRelatorio;
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param filtroRelatorio - {@link Map}
	 */
	private void popularFiltroRelatorioDois(RelatorioCadastroVO relatorioCadastroVO, Map<String, Object> filtroRelatorio) {

		String situacao = relatorioCadastroVO.getSituacao();
		Boolean filtros = relatorioCadastroVO.getExibirFiltros();
		String formato = relatorioCadastroVO.getFormatoImpressao();

		if (situacao != null && !situacao.isEmpty()) {
			filtroRelatorio.put(SITUACAO, situacao);
			filtroRelatorio.put(FILTRO_SITUACAO, situacao);
		}
		if (filtros != null) {
			filtroRelatorio.put(EXIBIR_FILTROS, filtros);
		}
		if (StringUtils.isNotEmpty(formato)) {
			filtroRelatorio.put(FORMATO_IMPRESSAO, formato);
		}
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param filtroRelatorio - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularFiltroRelatorioCampoData(RelatorioCadastroVO relatorioCadastroVO, Map<String, Object> filtroRelatorio)
			throws GGASException {

		String emissaoInicial = relatorioCadastroVO.getDataEmissaoInicial();
		String emissaoFinal = relatorioCadastroVO.getDataEmissaoFinal();
		String vencimentoInicial = relatorioCadastroVO.getDataVencimentoInicial();
		String vencimentoFinal = relatorioCadastroVO.getDataVencimentoFinal();

		if (emissaoInicial != null && !emissaoInicial.isEmpty()) {
			Date dataEmissaoInicial = Util.converterCampoStringParaData("Data Emissão Inicial", emissaoInicial, Constantes.FORMATO_DATA_BR);
			filtroRelatorio.put(DATA_EMISSAO_INICIAL, dataEmissaoInicial);
			filtroRelatorio.put(FILTRO_DATA_EMISSAO_INICIAL, emissaoInicial);
		}

		if (emissaoFinal != null && !emissaoFinal.isEmpty()) {
			Date dataEmissaoFinal = Util.converterCampoStringParaData("Data Emissão Final", emissaoFinal, Constantes.FORMATO_DATA_BR);
			filtroRelatorio.put(DATA_EMISSAO_FINAL, dataEmissaoFinal);
			filtroRelatorio.put(FILTRO_DATA_EMISSAO_FINAL, emissaoFinal);
		}
		if (vencimentoInicial != null && !vencimentoInicial.isEmpty()) {
			Date dataVencimentoInicial =
					Util.converterCampoStringParaData("Data Vencimento Inicial", vencimentoInicial, Constantes.FORMATO_DATA_BR);
			filtroRelatorio.put(DATA_VENCIMENTO_INICIAL, dataVencimentoInicial);
			filtroRelatorio.put(FILTRO_DATA_VENCIMENTO_INICIAL, dataVencimentoInicial);
		}
		if (vencimentoFinal != null && !vencimentoFinal.isEmpty()) {
			Date dataVencimentoFinal =
					Util.converterCampoStringParaData("Data Vencimento Final", vencimentoFinal, Constantes.FORMATO_DATA_BR);
			filtroRelatorio.put(DATA_VENCIMENTO_FINAL, dataVencimentoFinal);
			filtroRelatorio.put(FILTRO_DATA_VENCIMENTO_FINAL, dataVencimentoFinal);
		}
	}

	/**
	 * Validar form.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarForm(RelatorioCadastroVO relatorioCadastroVO, Model model) throws GGASException {

		Map<String, Object> erros = new HashMap<String, Object>();
		String delimitador = "";
		StringBuilder campos = new StringBuilder();
		Long idCliente = relatorioCadastroVO.getIdCliente();

		Date dataAtual = Calendar.getInstance().getTime();
		Date dataEmissaoInicial = null;
		Date dataEmissaoFinal = null;
		Date dataVencimentoInicial = null;
		Date dataVencimentoFinal = null;

		if (idCliente == null || idCliente <= 0) {
			campos.append("Cliente");
			campos.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (campos.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, campos.toString());
			throw new NegocioException(erros);
		}

		delimitador = validarPeriodoEmissao(relatorioCadastroVO, dataAtual, dataEmissaoInicial, dataEmissaoFinal, campos, delimitador);
		validarPeriodoVencimento(relatorioCadastroVO, dataVencimentoInicial, dataVencimentoFinal, campos, delimitador);

	}
	
	/**
	 * Valida os campos Período de Emissão, se a Data Inicial e a Data Final foram preenchidos corretamente.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param dataAtual - {@link Date}
	 * @param dataEmissaoInicial - {@link Date}
	 * @param dataEmissaoFinal - {@link Date}
	 * @param campos - {@link StringBuilder}
	 * @param delimitador - {@link delimitador}
	 * @return delimitador - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	public String validarPeriodoEmissao(RelatorioCadastroVO relatorioCadastroVO, Date dataAtual, Date dataEmissaoInicial,
			Date dataEmissaoFinal, StringBuilder campos, String delimitador) throws GGASException {

		if (relatorioCadastroVO.getDataEmissaoInicial().isEmpty()) {
			campos.append(delimitador).append("Período de Emissão Inicial");
			delimitador = ", ";
		} else {
			dataEmissaoInicial = Util.converterCampoStringParaData("Período de Emissão Inicial",
					relatorioCadastroVO.getDataEmissaoInicial(), Constantes.FORMATO_DATA_BR);
		}

		if (relatorioCadastroVO.getDataEmissaoFinal().isEmpty()) {
			campos.append(delimitador).append("Período de Emissão Final");
			delimitador = ", ";
		} else {
			dataEmissaoFinal = Util.converterCampoStringParaData("Período de Emissão Final", relatorioCadastroVO.getDataEmissaoFinal(),
					Constantes.FORMATO_DATA_BR);
		}

		if ((dataEmissaoInicial != null) && dataEmissaoInicial.after(dataAtual)) {
			throw new NegocioException(Constantes.ERRO_DATA_EMISSAO_INICIAL_POSTERIOR_ATUAL, true);
		}

		if ((dataEmissaoInicial != null && dataEmissaoFinal != null) && dataEmissaoInicial.after(dataEmissaoFinal)) {
			throw new NegocioException(Constantes.ERRO_DATA_EMISSAO_INICIAL_MAIOR_DATA_FINAL, true);
		}

		return delimitador;

	}
	
	/**
	 * Valida os campos Períodos de Vencimento, se a Data Inicial e a Data Final foram preenchidos corretamente.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param dataVencimentoInicial - {@link Date}
	 * @param dataVencimentoFinal - {@link Date}
	 * @param campos - {@link StringBuilder}
	 * @param delimitador - {@link delimitador}
	 * @throws GGASException - {@link GGASException}
	 */
	public void validarPeriodoVencimento(RelatorioCadastroVO relatorioCadastroVO, Date dataVencimentoInicial, Date dataVencimentoFinal,
			StringBuilder campos, String delimitador) throws GGASException {

		if (relatorioCadastroVO.getDataVencimentoInicial().isEmpty()) {
			campos.append(delimitador).append("Período de Vencimento Inicial");
			delimitador = ", ";
		} else {
			dataVencimentoInicial = Util.converterCampoStringParaData("Período de Vencimento Inicial",
					relatorioCadastroVO.getDataVencimentoInicial(), Constantes.FORMATO_DATA_BR);
		}

		if (relatorioCadastroVO.getDataVencimentoFinal().isEmpty()) {
			campos.append(delimitador).append("Período de Vencimento Final");
			delimitador = ", ";
		} else {
			dataVencimentoFinal = Util.converterCampoStringParaData("Período de Vencimento Final",
					relatorioCadastroVO.getDataVencimentoFinal(), Constantes.FORMATO_DATA_BR);
		}

		if ((dataVencimentoInicial != null && dataVencimentoFinal != null) && dataVencimentoInicial.after(dataVencimentoFinal)) {
			throw new NegocioException(Constantes.ERRO_DATA_VENCIMENTO_INICIAL_MAIOR_DATA_FINAL, true);
		}

	}

}
