/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.quantidadesContratadas;

import java.math.BigDecimal;
import java.util.Date;
/**
 * 
 * Classe responsável pela representação da entidade QuantidadesContratadas
 *
 */
public class QuantidadesContratadasVO {

	private Date dataAssinatura;

	private Date dataRecisao;

	private Date dataVencimento;

	private String descricaoPontoConsumo;

	private String descricaoModalidade;

	private String descricaoSegmento;

	private BigDecimal qdc;

	// by gmatos on
	// 11/05/11 10:54

	private BigDecimal top;

	// by gmatos on
	// 11/05/11 10:54

	private String numeroContrato;

	private Date dataVigenciaQDT;

	private String nomeCliente;

	public Date getDataAssinatura() {
		Date data = null;
		if (this.dataAssinatura != null) {
			data = (Date) dataAssinatura.clone();
		}
		return data;
	}

	public void setDataAssinatura(Date dataAssinatura) {

		this.dataAssinatura = dataAssinatura;
	}

	public Date getDataRecisao() {
		Date data = null;
		if (this.dataRecisao != null) {
			data = (Date) dataRecisao.clone();
		}
		return data;
	}

	public void setDataRecisao(Date dataRecisao) {

		this.dataRecisao = dataRecisao;
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getDescricaoModalidade() {

		return descricaoModalidade;
	}

	public void setDescricaoModalidade(String descricaoModalidade) {

		this.descricaoModalidade = descricaoModalidade;
	}

	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	public BigDecimal getQDC() {

		return qdc;
	}

	public void setQDC(BigDecimal qDC) {

		qdc = qDC;
	}

	public BigDecimal getTOP() {

		return top;
	}

	public void setTOP(BigDecimal tOP) {

		top = tOP;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public Date getDataVigenciaQDT() {
		Date data = null;
		if (this.dataVigenciaQDT != null) {
			data = (Date) dataVigenciaQDT.clone();
		}
		return data;
	}

	public void setDataVigenciaQDT(Date dataVigenciaQDT) {

		this.dataVigenciaQDT = dataVigenciaQDT;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

}
