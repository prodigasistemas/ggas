/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.analiseconsumo;

import java.math.BigDecimal;

public class RelatorioAnaliseConsumoDadosResumoVO {

	private String tabelaPreco;

	private String faixa;

	private String produto;

	private BigDecimal volume;

	private BigDecimal preco;

	private BigDecimal total;

	/**
	 * @return the tabelaPreco
	 */
	public String getTabelaPreco() {

		return tabelaPreco;
	}

	/**
	 * @param tabelaPreco
	 *            the tabelaPreco to set
	 */
	public void setTabelaPreco(String tabelaPreco) {

		this.tabelaPreco = tabelaPreco;
	}

	/**
	 * @return the faixa
	 */
	public String getFaixa() {

		return faixa;
	}

	/**
	 * @param faixa
	 *            the faixa to set
	 */
	public void setFaixa(String faixa) {

		this.faixa = faixa;
	}

	/**
	 * @return the produto
	 */
	public String getProduto() {

		return produto;
	}

	/**
	 * @param produto
	 *            the produto to set
	 */
	public void setProduto(String produto) {

		this.produto = produto;
	}

	/**
	 * @return the volume
	 */
	public BigDecimal getVolume() {

		return volume;
	}

	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(BigDecimal volume) {

		this.volume = volume;
	}

	/**
	 * @return the preco
	 */
	public BigDecimal getPreco() {

		return preco;
	}

	/**
	 * @param preco
	 *            the preco to set
	 */
	public void setPreco(BigDecimal preco) {

		this.preco = preco;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {

		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(BigDecimal total) {

		this.total = total;
	}

}
