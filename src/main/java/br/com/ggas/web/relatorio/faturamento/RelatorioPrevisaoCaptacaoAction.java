/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturamento;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * Classe referentes as chamadas do  RelatorioPrevisaoCaptacaoWrapper 
 * 
 * @author vpessoa
 */
@Controller
public class RelatorioPrevisaoCaptacaoAction extends GenericAction {

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Exibir pesquisa relatorio acompanhamento clientes.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirPesquisaRelatorioPrevisaoCaptacao")
	public ModelAndView exibirPesquisaRelatorioAcompanhamentoClientes() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioPrevisaoCaptacao");

		carregarCombos(model);

		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCombos(ModelAndView model) throws NegocioException {

		List<String> listaAnoReferencia = new ArrayList<String>();

		Calendar calendario = GregorianCalendar.getInstance();
		int anoAtual = calendario.get(Calendar.YEAR);
		for (int anoInicicio = anoAtual - 3; anoInicicio <= anoAtual + 3; anoInicicio++) {
			listaAnoReferencia.add(String.valueOf(anoInicicio));
		}
		model.addObject("listaAnoReferencia", listaAnoReferencia);
		model.addObject("listaSegmento", controladorSegmento.listarSegmento());
	}

	/**
	 * Gerar relatorio previsao captacao.
	 * 
	 * @param relatorioPrevisaoCaptacaoVO
	 *            the relatorio previsao captacao vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioPrevisaoCaptacao")
	public ModelAndView gerarRelatorioPrevisaoCaptacao(
					@ModelAttribute("RelatorioPrevisaoCaptacaoVO") PesquisaRelatorioPrevisaoCaptacaoVO relatorioPrevisaoCaptacaoVO,
					@RequestParam("tipoRelatorio") String tipoRelatorio, BindingResult result, HttpServletRequest request,
					HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioPrevisaoCaptacao");

		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			relatorio = controladorPontoConsumo.gerarRelatorioPrevisaoCaptacao(relatorioPrevisaoCaptacaoVO, formatoImpressao);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(
							"Content-Disposition",
							"attachment; filename=RelatórioPrevisaoCaptacao_" + diaMesAno + "_" + hora
											+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch(NegocioException e) {
			carregarCombos(model);
			mensagemErroParametrizado(model, e);
		}
		return model;
	}
}
