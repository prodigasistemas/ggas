/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 27/10/2014 11:23:54
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author vpessoa
 */
public class RelatorioComercialMensalVO {

	private String segmento;

	private String situacao;

	private String razaoSocial;

	private String nomeFantasia;

	private String nomePontoConsumo;

	private BigDecimal tarifa;

	private String codTarifa;

	private BigDecimal volumeContratado;

	private Date dataAssinaturaContrato;

	private Date dataInicioConsumo;

	private Date dataDesligamento;

	private String atividadeEconomica;

	private String bairro;

	private String municipio;

	private String zonaBloqueio;

	private BigDecimal totalAno;

	private BigDecimal totalAnoAnterior;

	private BigDecimal consumoJaneiro;

	private BigDecimal consumoFevereiro;

	private BigDecimal consumoMarco;

	private BigDecimal consumoAbril;

	private BigDecimal consumoMaio;

	private BigDecimal consumoJunho;

	private BigDecimal consumoJulho;

	private BigDecimal consumoAgosto;

	private BigDecimal consumoSetembro;

	private BigDecimal consumoOutubro;

	private BigDecimal consumoNovembro;

	private BigDecimal consumoDezembro;

	private Integer numeroUDA;

	private Integer udaJaneiro;

	private Integer udaFevereiro;

	private Integer udaMarco;

	private Integer udaAbril;

	private Integer udaMaio;

	private Integer udaJunho;

	private Integer udaJulho;

	private Integer udaAgosto;

	private Integer udaSetembro;

	private Integer udaOutubro;

	private Integer udaNovembro;

	private Integer udaDezembro;

	private BigDecimal valorTotal;

	private String imovel;

	private String modalidade;

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getSituacao() {

		return situacao;
	}

	public void setSituacao(String situacao) {

		this.situacao = situacao;
	}

	public String getRazaoSocial() {

		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {

		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getNomePontoConsumo() {

		return nomePontoConsumo;
	}

	public void setNomePontoConsumo(String nomePontoConsumo) {

		this.nomePontoConsumo = nomePontoConsumo;
	}

	public BigDecimal getTarifa() {

		return tarifa;
	}

	public void setTarifa(BigDecimal tarifa) {

		this.tarifa = tarifa;
	}

	public String getCodTarifa() {

		return codTarifa;
	}

	public void setCodTarifa(String codTarifa) {

		this.codTarifa = codTarifa;
	}

	public BigDecimal getVolumeContratado() {

		return volumeContratado;
	}

	public void setVolumeContratado(BigDecimal volumeContratado) {

		this.volumeContratado = volumeContratado;
	}

	public Date getDataAssinaturaContrato() {
		Date data = null;
		if (this.dataAssinaturaContrato != null) {
			data = (Date) dataAssinaturaContrato.clone();
		}
		return data;
	}

	public void setDataAssinaturaContrato(Date dataAssinaturaContrato) {

		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	public Date getDataInicioConsumo() {
		Date data = null;
		if (this.dataInicioConsumo != null) {
			data = (Date) dataInicioConsumo.clone();
		}
		return data;
	}

	public void setDataInicioConsumo(Date dataInicioConsumo) {

		this.dataInicioConsumo = dataInicioConsumo;
	}

	public Date getDataDesligamento() {
		Date data = null;
		if (this.dataDesligamento != null) {
			data = (Date) dataDesligamento.clone();
		}
		return data;
	}

	public void setDataDesligamento(Date dataDesligamento) {

		this.dataDesligamento = dataDesligamento;
	}

	public String getAtividadeEconomica() {

		return atividadeEconomica;
	}

	public void setAtividadeEconomica(String atividadeEconomica) {

		this.atividadeEconomica = atividadeEconomica;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getMunicipio() {

		return municipio;
	}

	public void setMunicipio(String municipio) {

		this.municipio = municipio;
	}

	public String getZonaBloqueio() {

		return zonaBloqueio;
	}

	public void setZonaBloqueio(String zonaBloqueio) {

		this.zonaBloqueio = zonaBloqueio;
	}

	public BigDecimal getTotalAno() {

		return totalAno;
	}

	public void setTotalAno(BigDecimal totalAno) {

		this.totalAno = totalAno;
	}

	public BigDecimal getConsumoJaneiro() {

		return consumoJaneiro;
	}

	public void setConsumoJaneiro(BigDecimal consumoJaneiro) {

		this.consumoJaneiro = consumoJaneiro;
	}

	public BigDecimal getConsumoFevereiro() {

		return consumoFevereiro;
	}

	public void setConsumoFevereiro(BigDecimal consumoFevereiro) {

		this.consumoFevereiro = consumoFevereiro;
	}

	public BigDecimal getConsumoMarco() {

		return consumoMarco;
	}

	public void setConsumoMarco(BigDecimal consumoMarco) {

		this.consumoMarco = consumoMarco;
	}

	public BigDecimal getConsumoAbril() {

		return consumoAbril;
	}

	public void setConsumoAbril(BigDecimal consumoAbril) {

		this.consumoAbril = consumoAbril;
	}

	public BigDecimal getConsumoMaio() {

		return consumoMaio;
	}

	public void setConsumoMaio(BigDecimal consumoMaio) {

		this.consumoMaio = consumoMaio;
	}

	public BigDecimal getConsumoJunho() {

		return consumoJunho;
	}

	public void setConsumoJunho(BigDecimal consumoJunho) {

		this.consumoJunho = consumoJunho;
	}

	public BigDecimal getConsumoJulho() {

		return consumoJulho;
	}

	public void setConsumoJulho(BigDecimal consumoJulho) {

		this.consumoJulho = consumoJulho;
	}

	public BigDecimal getConsumoAgosto() {

		return consumoAgosto;
	}

	public void setConsumoAgosto(BigDecimal consumoAgosto) {

		this.consumoAgosto = consumoAgosto;
	}

	public BigDecimal getConsumoSetembro() {

		return consumoSetembro;
	}

	public void setConsumoSetembro(BigDecimal consumoSetembro) {

		this.consumoSetembro = consumoSetembro;
	}

	public BigDecimal getConsumoOutubro() {

		return consumoOutubro;
	}

	public void setConsumoOutubro(BigDecimal consumoOutubro) {

		this.consumoOutubro = consumoOutubro;
	}

	public BigDecimal getConsumoNovembro() {

		return consumoNovembro;
	}

	public void setConsumoNovembro(BigDecimal consumoNovembro) {

		this.consumoNovembro = consumoNovembro;
	}

	public BigDecimal getConsumoDezembro() {

		return consumoDezembro;
	}

	public void setConsumoDezembro(BigDecimal consumoDezembro) {

		this.consumoDezembro = consumoDezembro;
	}

	public BigDecimal getTotalAnoAnterior() {

		return totalAnoAnterior;
	}

	public void setTotalAnoAnterior(BigDecimal totalAnoAnterior) {

		this.totalAnoAnterior = totalAnoAnterior;
	}

	public Integer getNumeroUDA() {

		return numeroUDA;
	}

	public void setNumeroUDA(Integer numeroUDA) {

		this.numeroUDA = numeroUDA;
	}

	public Integer getUdaJaneiro() {

		return udaJaneiro;
	}

	public void setUdaJaneiro(Integer udaJaneiro) {

		this.udaJaneiro = udaJaneiro;
	}

	public Integer getUdaFevereiro() {

		return udaFevereiro;
	}

	public void setUdaFevereiro(Integer udaFevereiro) {

		this.udaFevereiro = udaFevereiro;
	}

	public Integer getUdaMarco() {

		return udaMarco;
	}

	public void setUdaMarco(Integer udaMarco) {

		this.udaMarco = udaMarco;
	}

	public Integer getUdaAbril() {

		return udaAbril;
	}

	public void setUdaAbril(Integer udaAbril) {

		this.udaAbril = udaAbril;
	}

	public Integer getUdaMaio() {

		return udaMaio;
	}

	public void setUdaMaio(Integer udaMaio) {

		this.udaMaio = udaMaio;
	}

	public Integer getUdaJunho() {

		return udaJunho;
	}

	public void setUdaJunho(Integer udaJunho) {

		this.udaJunho = udaJunho;
	}

	public Integer getUdaJulho() {

		return udaJulho;
	}

	public void setUdaJulho(Integer udaJulho) {

		this.udaJulho = udaJulho;
	}

	public Integer getUdaAgosto() {

		return udaAgosto;
	}

	public void setUdaAgosto(Integer udaAgosto) {

		this.udaAgosto = udaAgosto;
	}

	public Integer getUdaSetembro() {

		return udaSetembro;
	}

	public void setUdaSetembro(Integer udaSetembro) {

		this.udaSetembro = udaSetembro;
	}

	public Integer getUdaOutubro() {

		return udaOutubro;
	}

	public void setUdaOutubro(Integer udaOutubro) {

		this.udaOutubro = udaOutubro;
	}

	public Integer getUdaNovembro() {

		return udaNovembro;
	}

	public void setUdaNovembro(Integer udaNovembro) {

		this.udaNovembro = udaNovembro;
	}

	public Integer getUdaDezembro() {

		return udaDezembro;
	}

	public void setUdaDezembro(Integer udaDezembro) {

		this.udaDezembro = udaDezembro;
	}

	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	public String getImovel() {

		return imovel;
	}

	public void setImovel(String imovel) {

		this.imovel = imovel;
	}

	public String getModalidade() {

		return modalidade;
	}

	public void setModalidade(String modalidade) {

		this.modalidade = modalidade;
	}

}
