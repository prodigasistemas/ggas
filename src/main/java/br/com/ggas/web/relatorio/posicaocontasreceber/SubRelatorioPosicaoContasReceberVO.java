/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/12/2013 17:29:02
 @author ccavalcanti
 */

package br.com.ggas.web.relatorio.posicaocontasreceber;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SubRelatorioPosicaoContasReceberVO implements Serializable {

	private static final long serialVersionUID = -2131067729112754272L;

	private String descricaoPontoConsumo;

	private String numeroTitulo;

	private String dataEmissao;

	private String qtdDiasAtraso;

	private Double qtdDiasAtrasoDouble;

	private String qtdTitulos;

	private BigDecimal valorHistorico;

	private BigDecimal valorCorrigido;

	private BigDecimal valorPagamento;

	private Date dataPagamento;

	private String dataVencimento;

	private String totalQtdDiasAtraso;

	private BigDecimal totalValorHistorico;

	private BigDecimal totalValorCorrigido;

	private String totalqtdTitulo;

	private String tipoDocumento;

	/**
	 * Instantiates a new sub relatorio posicao contas receber vo.
	 * 
	 * @param qtdDiasAtrasoDouble
	 *            the qtd dias atraso double
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param dataPagamento
	 *            the data pagamento
	 */
	public SubRelatorioPosicaoContasReceberVO(Double qtdDiasAtrasoDouble, BigDecimal valorPagamento, Date dataPagamento) {

		this.qtdDiasAtrasoDouble = qtdDiasAtrasoDouble;
		this.valorPagamento = valorPagamento;
		this.dataPagamento = dataPagamento;
	}

	/**
	 * Instantiates a new sub relatorio posicao contas receber vo.
	 */
	public SubRelatorioPosicaoContasReceberVO() {

		super();
	}

	/**
	 * Instantiates a new sub relatorio posicao contas receber vo.
	 * 
	 * @param nomeClienteOuImovel
	 *            the nome cliente ou imovel
	 * @param numeroTitulo
	 *            the numero titulo
	 * @param dataEmissao
	 *            the data emissao
	 * @param qtdDiasAtraso
	 *            the qtd dias atraso
	 * @param qtdDiasAtrasoDouble
	 *            the qtd dias atraso double
	 * @param qtdTitulos
	 *            the qtd titulos
	 * @param valorHistorico
	 *            the valor historico
	 * @param valorCorrigido
	 *            the valor corrigido
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param dataPagamento
	 *            the data pagamento
	 * @param dataVencimento
	 *            the data vencimento
	 * @param totalQtdDiasAtraso
	 *            the total qtd dias atraso
	 * @param totalValorHistorico
	 *            the total valor historico
	 * @param totalValorCorrigido
	 *            the total valor corrigido
	 * @param totalqtdTitulo
	 *            the totalqtd titulo
	 * @param tipoDocumento
	 *            the tipo documento
	 */
	public SubRelatorioPosicaoContasReceberVO(String nomeClienteOuImovel, String numeroTitulo, String dataEmissao, String qtdDiasAtraso,
												Double qtdDiasAtrasoDouble, String qtdTitulos, BigDecimal valorHistorico,
												BigDecimal valorCorrigido, BigDecimal valorPagamento, Date dataPagamento,
												String dataVencimento, String totalQtdDiasAtraso, BigDecimal totalValorHistorico,
												BigDecimal totalValorCorrigido, String totalqtdTitulo, String tipoDocumento) {

		super();
		this.descricaoPontoConsumo = nomeClienteOuImovel;
		this.numeroTitulo = numeroTitulo;
		this.dataEmissao = dataEmissao;
		this.qtdDiasAtraso = qtdDiasAtraso;
		this.qtdDiasAtrasoDouble = qtdDiasAtrasoDouble;
		this.qtdTitulos = qtdTitulos;
		this.valorHistorico = valorHistorico;
		this.valorCorrigido = valorCorrigido;
		this.valorPagamento = valorPagamento;
		this.dataPagamento = dataPagamento;
		this.dataVencimento = dataVencimento;
		this.totalQtdDiasAtraso = totalQtdDiasAtraso;
		this.totalValorHistorico = totalValorHistorico;
		this.totalValorCorrigido = totalValorCorrigido;
		this.totalqtdTitulo = totalqtdTitulo;
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroTitulo() {

		return numeroTitulo;
	}

	public void setNumeroTitulo(String numeroTitulo) {

		this.numeroTitulo = numeroTitulo;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getQtdDiasAtraso() {

		return qtdDiasAtraso;
	}

	public void setQtdDiasAtraso(String qtdDiasAtraso) {

		this.qtdDiasAtraso = qtdDiasAtraso;
	}

	public String getQtdTitulos() {

		return qtdTitulos;
	}

	public void setQtdTitulos(String qtdTitulos) {

		this.qtdTitulos = qtdTitulos;
	}

	public BigDecimal getValorCorrigido() {

		return valorCorrigido;
	}

	public void setValorCorrigido(BigDecimal valorCorrigido) {

		this.valorCorrigido = valorCorrigido;
	}

	public Double getQtdDiasAtrasoDouble() {

		return qtdDiasAtrasoDouble;
	}

	public void setQtdDiasAtrasoDouble(Double qtdDiasAtrasoDouble) {

		this.qtdDiasAtrasoDouble = qtdDiasAtrasoDouble;
	}

	public BigDecimal getValorPagamento() {

		return valorPagamento;
	}

	public void setValorPagamento(BigDecimal valorPagamento) {

		this.valorPagamento = valorPagamento;
	}

	public Date getDataPagamento() {
		Date data = null;
		if (this.dataPagamento != null) {
			data = (Date) dataPagamento.clone();
		}
		return data;
	}

	public void setDataPagamento(Date dataPagamento) {

		this.dataPagamento = dataPagamento;
	}

	public String getTotalQtdDiasAtraso() {

		return totalQtdDiasAtraso;
	}

	public void setTotalQtdDiasAtraso(String totalQtdDiasAtraso) {

		this.totalQtdDiasAtraso = totalQtdDiasAtraso;
	}

	public BigDecimal getTotalValorDebito() {

		return totalValorHistorico;
	}

	public BigDecimal getTotalValorCorrigido() {

		return totalValorCorrigido;
	}

	public void setTotalValorCorrigido(BigDecimal totalValorCorrigido) {

		this.totalValorCorrigido = totalValorCorrigido;
	}

	public String getTotalqtdTitulo() {

		return totalqtdTitulo;
	}

	public void setTotalqtdTitulo(String totalqtdTitulo) {

		this.totalqtdTitulo = totalqtdTitulo;
	}

	public BigDecimal getValorHistorico() {

		return valorHistorico;
	}

	public void setValorHistorico(BigDecimal valorHistorico) {

		this.valorHistorico = valorHistorico;
	}

	public String getDataVencimento() {

		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public BigDecimal getTotalValorHistorico() {

		return totalValorHistorico;
	}

	public void setTotalValorHistorico(BigDecimal totalValorHistorico) {

		this.totalValorHistorico = totalValorHistorico;
	}

	public String getTipoDocumento() {

		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

}
