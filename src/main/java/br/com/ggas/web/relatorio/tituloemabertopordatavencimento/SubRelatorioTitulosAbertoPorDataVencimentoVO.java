/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/12/2013 17:29:02
 @author ccavalcanti
 */

package br.com.ggas.web.relatorio.tituloemabertopordatavencimento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SubRelatorioTitulosAbertoPorDataVencimentoVO implements Serializable {

	private static final long serialVersionUID = -2131067729112754272L;

	private String nomeCliente;

	private String numeroContrato;

	private String numeroTitulo;

	private String numeroNotaFiscal;

	private String situacao;

	private String tipoDocumento;

	private String dataEmissao;

	private String qtdDiasAtraso;

	private Double qtdDiasAtrasoDouble;

	private String qtdTitulos;

	private BigDecimal valorDebito;

	private BigDecimal valorMultaJuros;

	private BigDecimal valorCorrigido;

	private BigDecimal valorPagamento;

	private Date dataPagamento;

	private String totalQtdDiasAtraso;

	private BigDecimal totalValorDebito;

	private BigDecimal totalValorMultaJuros;

	private BigDecimal totalValorCorrigido;

	private String totalqtdTitulo;

	/**
	 * Instantiates a new sub relatorio titulos aberto por data vencimento vo.
	 * 
	 * @param qtdDiasAtrasoDouble
	 *            the qtd dias atraso double
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param dataPagamento
	 *            the data pagamento
	 */
	public SubRelatorioTitulosAbertoPorDataVencimentoVO(Double qtdDiasAtrasoDouble, BigDecimal valorPagamento, Date dataPagamento) {

		this.qtdDiasAtrasoDouble = qtdDiasAtrasoDouble;
		this.valorPagamento = valorPagamento;
		this.dataPagamento = dataPagamento;
	}

	/**
	 * Instantiates a new sub relatorio titulos aberto por data vencimento vo.
	 */
	public SubRelatorioTitulosAbertoPorDataVencimentoVO() {

		super();
	}

	/**
	 * Instantiates a new sub relatorio titulos aberto por data vencimento vo.
	 * 
	 * @param nomeCliente
	 *            the nome cliente
	 * @param numeroContrato
	 *            the numero contrato
	 * @param numeroTitulo
	 *            the numero titulo
	 * @param numeroNotaFiscal
	 *            the numero nota fiscal
	 * @param situacao
	 *            the situacao
	 * @param tipoDocumento
	 *            the tipo documento
	 * @param dataEmissao
	 *            the data emissao
	 * @param qtdDiasAtraso
	 *            the qtd dias atraso
	 * @param qtdTitulos
	 *            the qtd titulos
	 * @param valorDebito
	 *            the valor debito
	 * @param valorMultaJuros
	 *            the valor multa juros
	 * @param valorCorrigido
	 *            the valor corrigido
	 */
	public SubRelatorioTitulosAbertoPorDataVencimentoVO(String nomeCliente, String numeroContrato, String numeroTitulo,
														String numeroNotaFiscal, String situacao, String tipoDocumento, String dataEmissao,
														String qtdDiasAtraso, String qtdTitulos, BigDecimal valorDebito,
														BigDecimal valorMultaJuros, BigDecimal valorCorrigido) {

		super();
		this.nomeCliente = nomeCliente;
		this.numeroContrato = numeroContrato;
		this.numeroTitulo = numeroTitulo;
		this.numeroNotaFiscal = numeroNotaFiscal;
		this.situacao = situacao;
		this.tipoDocumento = tipoDocumento;
		this.dataEmissao = dataEmissao;
		this.qtdDiasAtraso = qtdDiasAtraso;
		this.qtdTitulos = qtdTitulos;
		this.valorDebito = valorDebito;
		this.valorMultaJuros = valorMultaJuros;
		this.valorCorrigido = valorCorrigido;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getNumeroTitulo() {

		return numeroTitulo;
	}

	public void setNumeroTitulo(String numeroTitulo) {

		this.numeroTitulo = numeroTitulo;
	}

	public String getNumeroNotaFiscal() {

		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {

		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	public String getSituacao() {

		return situacao;
	}

	public void setSituacao(String situacao) {

		this.situacao = situacao;
	}

	public String getTipoDocumento() {

		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getQtdDiasAtraso() {

		return qtdDiasAtraso;
	}

	public void setQtdDiasAtraso(String qtdDiasAtraso) {

		this.qtdDiasAtraso = qtdDiasAtraso;
	}

	public String getQtdTitulos() {

		return qtdTitulos;
	}

	public void setQtdTitulos(String qtdTitulos) {

		this.qtdTitulos = qtdTitulos;
	}

	public BigDecimal getValorDebito() {

		return valorDebito;
	}

	public void setValorDebito(BigDecimal valorDebito) {

		this.valorDebito = valorDebito;
	}

	public BigDecimal getValorMultaJuros() {

		return valorMultaJuros;
	}

	public void setValorMultaJuros(BigDecimal valorMultaJuros) {

		this.valorMultaJuros = valorMultaJuros;
	}

	public BigDecimal getValorCorrigido() {

		return valorCorrigido;
	}

	public void setValorCorrigido(BigDecimal valorCorrigido) {

		this.valorCorrigido = valorCorrigido;
	}

	public Double getQtdDiasAtrasoDouble() {

		return qtdDiasAtrasoDouble;
	}

	public void setQtdDiasAtrasoDouble(Double qtdDiasAtrasoDouble) {

		this.qtdDiasAtrasoDouble = qtdDiasAtrasoDouble;
	}

	public BigDecimal getValorPagamento() {

		return valorPagamento;
	}

	public void setValorPagamento(BigDecimal valorPagamento) {

		this.valorPagamento = valorPagamento;
	}

	public Date getDataPagamento() {
		Date data = null;
		if (this.dataPagamento != null) {
			data = (Date) dataPagamento.clone();
		}
		return data;
	}

	public void setDataPagamento(Date dataPagamento) {

		this.dataPagamento = dataPagamento;
	}

	public String getTotalQtdDiasAtraso() {

		return totalQtdDiasAtraso;
	}

	public void setTotalQtdDiasAtraso(String totalQtdDiasAtraso) {

		this.totalQtdDiasAtraso = totalQtdDiasAtraso;
	}

	public BigDecimal getTotalValorDebito() {

		return totalValorDebito;
	}

	public void setTotalValorDebito(BigDecimal totalValorDebito) {

		this.totalValorDebito = totalValorDebito;
	}

	public BigDecimal getTotalValorMultaJuros() {

		return totalValorMultaJuros;
	}

	public void setTotalValorMultaJuros(BigDecimal totalValorMultaJuros) {

		this.totalValorMultaJuros = totalValorMultaJuros;
	}

	public BigDecimal getTotalValorCorrigido() {

		return totalValorCorrigido;
	}

	public void setTotalValorCorrigido(BigDecimal totalValorCorrigido) {

		this.totalValorCorrigido = totalValorCorrigido;
	}

	public String getTotalqtdTitulo() {

		return totalqtdTitulo;
	}

	public void setTotalqtdTitulo(String totalqtdTitulo) {

		this.totalqtdTitulo = totalqtdTitulo;
	}

}
