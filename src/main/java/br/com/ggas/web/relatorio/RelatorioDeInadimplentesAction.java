/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioDeInadimplentes;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes e seus respectivos metodos relacionados as regras de
 * negócio e de modelo realizando alterações nas informações das telas referentes a funcionalidade Relatório de Inadimplentes.
 */
@Controller
public class RelatorioDeInadimplentesAction extends GenericAction {

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_SITUACAO_PONTO_CONSUMO = "listaSituacaoPontoConsumo";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String RELATORIO_DE_INADIMPLENTES = "relatorioDeInadimplentes";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String IDS_PONTO_CONSUMO = "idsPontoConsumo";

	private static final String IDS_SEGMENTOS = "idsSegmentos";

	private static final String DATA_BASE = "dataBase";

	private static final String TIPO_EXIBICAO = "tipoRelatorio";

	private static final String EXIBIR_FILTRO = "exibirFiltros";

	private static final String EXIBIR_GRAFICO = "exibirGrafico";

	private static final String ID_TIPO_GRAFICO = "idTipoGrafico";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_RELATORIO = "exibirRelatorio";

	private static final String DIAS_VENCIDO = "diasVencido";

	private static final String ID_SITUACAO_PONTO_CONSUMO = "idSituacaoPontoConsumo";

	private static final String DIAS_ATRASO = "diasAtraso";

	private static final String FATURA_REVISAO = "faturaRevisao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorRelatorioDeInadimplentes controladorRelatorioDeInadimplentes;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * Método reponsável por exibir a tela de pesquisa do relatório.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioDeInadimplentes - {@link String}
	 * @throws GGASException - {@link GGASException}
	 * @throws ParseException - {@link ParseException}
	 */
	@RequestMapping("exibirPesquisaRelatorioDeInadimplentes")
	public String exibirPesquisaRelatorioDeInadimplentes(RelatorioCobrancaVO relatorioCobrancaVO,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException, ParseException {

		try {
			configurarDadosPesquisaRelatorioDeInadimplentes(relatorioCobrancaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("relatorioCobrancaVO", relatorioCobrancaVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return "exibirPesquisaRelatorioDeInadimplentes";

	}

	/**
	 * Método responsável por exibir o relatorio de inadimplentes.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws IOException - {@link IOException}
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping("exibirRelatorioDeInadimplentes")
	public String exibirRelatorioDeInadimplentes(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

		byte[] relatorioDeInadimplentes = (byte[]) request.getSession().getAttribute(RELATORIO_DE_INADIMPLENTES);
		FormatoImpressao formatoImpressao = (FormatoImpressao) request.getSession().getAttribute(FORMATO_IMPRESSAO);

		request.getSession().removeAttribute(RELATORIO_DE_INADIMPLENTES);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		if (relatorioDeInadimplentes != null) {
			Date dataAtual = Calendar.getInstance().getTime();

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual);
			int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			int minutes = Calendar.getInstance().get(Calendar.MINUTE);
			int seconds = Calendar.getInstance().get(Calendar.SECOND);
			Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			Calendar.getInstance().get(Calendar.MINUTE);
			Calendar.getInstance().get(Calendar.SECOND);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioDeInadimplentes.length);
			response.addHeader("Content-Disposition", "attachment; filename=" + "relatorioDeInadimplentes" + "_" + data + "_" + hours
					+ minutes + seconds + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorioDeInadimplentes, 0, relatorioDeInadimplentes.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		return null;
	}

	/**
	 * Método responsável por gerar o relatorio.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioDeInadimplentes - {@link String}
	 * @throws GGASException - {@link GGASException}
	 * @throws ParseException - {@link ParseException}
	 */
	@RequestMapping("gerarRelatorioDeInadimplentes")
	public String gerarRelatorioDeInadimplentes(RelatorioCobrancaVO relatorioCobrancaVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException, ParseException {

		try {

			Map<String, Object> parametros = this.obterParametrosRelatorio(relatorioCobrancaVO);
			byte[] relatorio = controladorRelatorioDeInadimplentes.gerarRelatorio(parametros);
			if (relatorio != null) {

				request.getSession().setAttribute(RELATORIO_DE_INADIMPLENTES, relatorio);
				request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf((String) parametros.get(FORMATO_IMPRESSAO)));
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute("relatorioCobrancaVO", relatorioCobrancaVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return exibirPesquisaRelatorioDeInadimplentes(relatorioCobrancaVO, clienteImovelPesquisaPopupVO, bindingResult, request, model);

	}

	/**
	 * Configurar dados pesquisa relatorio de inadimplentes.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 * @throws ParseException - {@link ParseException}
	 */
	private void configurarDadosPesquisaRelatorioDeInadimplentes(RelatorioCobrancaVO relatorioCobrancaVO, HttpServletRequest request,
			Model model) throws GGASException, ParseException {

		model.addAttribute(INTERVALO_ANOS_DATA, obterIntervaloAnosData());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_SITUACAO_PONTO_CONSUMO, controladorPontoConsumo.listarSituacoesPontoConsumo());
		configurarDadosPontoConsumo(relatorioCobrancaVO, request, model);

	}

	/**
	 * Configurar dados ponto consumo.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @throws ParseException - {@link ParseException}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarDadosPontoConsumo(RelatorioCobrancaVO relatorioCobrancaVO, HttpServletRequest request, Model model)
			throws ParseException, GGASException {

		Boolean pesquisaPontoConsumo = relatorioCobrancaVO.getPesquisaPontoConsumo();

		if (pesquisaPontoConsumo != null && pesquisaPontoConsumo) {

			Map<String, Object> filtro = this.obterParametrosRelatorio(relatorioCobrancaVO);

			super.adicionarFiltroPaginacao(request, filtro);

			List<PontoConsumo> listaPontoConsumo = controladorRelatorioDeInadimplentes.listarPontosConsumoRelatorioInadimplentes(filtro);

			model.addAttribute(LISTA_PONTO_CONSUMO, super.criarColecaoPaginada(request, filtro, listaPontoConsumo));

		}

	}

	/**
	 * Obter intervalo anos data.
	 * 
	 * @return the string - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	private String obterIntervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		return String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();
	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @return mapa - {@link Map}
	 * @throws ParseException - {@link ParseException}
	 * @throws GGASException
	 */
	private Map<String, Object> obterParametrosRelatorio(RelatorioCobrancaVO relatorioCobrancaVO) throws ParseException, GGASException {

		Long idCliente = relatorioCobrancaVO.getIdCliente();
		Long idImovel = relatorioCobrancaVO.getIdImovel();
		Long[] idsSegmentos = relatorioCobrancaVO.getIdsSegmentosAssociados();
		Long[] idsPontoConsumo = relatorioCobrancaVO.getIdsPontoConsumo();

		Map<String, Object> mapa = new HashMap<String, Object>();

		if (idCliente != null && idCliente > 0) {
			mapa.put(ID_CLIENTE, idCliente);
		}

		if (idImovel != null && idImovel > 0) {
			mapa.put(ID_IMOVEL, idImovel);
		}

		if (idsSegmentos != null && idsSegmentos.length > 0) {
			mapa.put(IDS_SEGMENTOS, idsSegmentos);
		}

		if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
			mapa.put(IDS_PONTO_CONSUMO, idsPontoConsumo);
		}

		obterParametrosRelatorioDois(relatorioCobrancaVO, mapa);
		obterParametrosRelatorioTres(relatorioCobrancaVO, mapa);

		return mapa;
	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param mapa - {@link Map}
	 */
	private void obterParametrosRelatorioDois(RelatorioCobrancaVO relatorioCobrancaVO, Map<String, Object> mapa) {

		Boolean faturaRevisao = relatorioCobrancaVO.getFaturaRevisao();
		Boolean exibirFiltro = relatorioCobrancaVO.getExibirFiltros();
		Boolean exibirGrafico = relatorioCobrancaVO.getExibirGrafico();
		Integer diasAtraso = relatorioCobrancaVO.getDiasAtraso();
		Long idSituacaoPontoConsumo = relatorioCobrancaVO.getIdSituacaoPontoConsumo();

		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > 0) {
			mapa.put(ID_SITUACAO_PONTO_CONSUMO, idSituacaoPontoConsumo);
		}

		if (exibirFiltro != null) {
			mapa.put(EXIBIR_FILTRO, exibirFiltro);
		}

		if (exibirGrafico != null) {
			mapa.put(EXIBIR_GRAFICO, exibirGrafico);
		}

		if (diasAtraso != null && diasAtraso > 0) {
			mapa.put(DIAS_ATRASO, diasAtraso);
		}

		if (faturaRevisao != null) {
			mapa.put(FATURA_REVISAO, faturaRevisao);
		}
	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param mapa - {@link Map}
	 * @throws ParseException - {@link ParseException}
	 * @throws GGASException
	 */
	private void obterParametrosRelatorioTres(RelatorioCobrancaVO relatorioCobrancaVO, Map<String, Object> mapa)
			throws ParseException, GGASException {

		String dataBase = relatorioCobrancaVO.getDataBase();
		String diasVencido = relatorioCobrancaVO.getDiasVencido();
		String tipoExibicao = relatorioCobrancaVO.getTipoRelatorio();
		String idTipoGrafico = relatorioCobrancaVO.getIdTipoGrafico();
		String formatoImpressao = relatorioCobrancaVO.getFormatoImpressao();

		if (StringUtils.isNotEmpty(dataBase)) {

			Util.converterCampoStringParaData("Data de Referência", dataBase, Constantes.FORMATO_DATA_BR);
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(dataBase));
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);

			mapa.put(DATA_BASE, cal.getTime());
		}

		if (StringUtils.isNotEmpty(diasVencido)) {
			mapa.put(DIAS_VENCIDO, diasVencido);
		}

		if (StringUtils.isNotEmpty(tipoExibicao)) {
			mapa.put(TIPO_EXIBICAO, tipoExibicao);
		}

		if (StringUtils.isNotEmpty(idTipoGrafico)) {
			mapa.put(ID_TIPO_GRAFICO, idTipoGrafico);
		}

		if (StringUtils.isNotEmpty(formatoImpressao)) {
			mapa.put(FORMATO_IMPRESSAO, formatoImpressao);
		}
	}

}
