
package br.com.ggas.web.relatorio.titulosatraso;

import java.math.BigDecimal;
import java.util.Date;
/**
 * 
 * Classe responsável pela representação da entidade TitulosAtrasadoDetalhados
 *
 */
public class TitulosAtrasadoDetalhadosV0 {

	private String tipoDocumento;

	private Long numeroDocumento;

	private Date dataEmissao;

	private Date dataVencimento;

	private Integer diasAtraso;

	private String nomeFantasiaPontoConsumo;

	private Integer quantidadeDocumentos;

	private BigDecimal valorHistorico;

	private BigDecimal valorCorrigido;

	public Integer getQuantidadeDocumentos() {

		return quantidadeDocumentos;
	}

	public void setQuantidadeDocumentos(Integer quantidadeDocumentos) {

		this.quantidadeDocumentos = quantidadeDocumentos;
	}

	public BigDecimal getValorCorrigido() {

		return valorCorrigido;
	}

	public void setValorCorrigido(BigDecimal valorCorrigido) {

		this.valorCorrigido = valorCorrigido;
	}

	public BigDecimal getValorHistorico() {

		return valorHistorico;
	}

	public void setValorHistorico(BigDecimal valorHistorico) {

		this.valorHistorico = valorHistorico;
	}

	public String getTipoDocumento() {

		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	public Long getNumeroDocumento() {

		return numeroDocumento;
	}

	public void setNumeroDocumento(Long numeroDocumento) {

		this.numeroDocumento = numeroDocumento;
	}

	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public Integer getDiasAtraso() {

		return diasAtraso;
	}

	public void setDiasAtraso(Integer diasAtraso) {

		this.diasAtraso = diasAtraso;
	}

	public String getNomeFantasiaPontoConsumo() {

		return nomeFantasiaPontoConsumo;
	}

	public void setNomeFantasiaPontoConsumo(String nomeFantasiaPontoConsumo) {

		this.nomeFantasiaPontoConsumo = nomeFantasiaPontoConsumo;
	}

}
