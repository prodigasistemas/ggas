/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.perfilconsumocliente;

import java.math.BigDecimal;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class PerfilConsumoVO {

	private String nomeCliente;

	private String descricaoSegmento;

	private String descricaoPontoConsumo;

	private String descricaoImovel;

	private String dataAssinaturaContrato;

	private String dataInicioConsumo;

	private String dataEncerramento;

	private String zonaBloqueio;

	private String modalidadeMedicao;

	private String descricaoRamoAtividade;

	private String modalidadeUso;

	private Long idCliente;

	private Long idPontoConsumo;

	private Long idContrato;

	private String volumeJaneiro;

	private String volumeFevereiro;

	private String volumeMarco;

	private String volumeAbril;

	private String volumeMaio;

	private String volumeJunho;

	private String volumeJulho;

	private String volumeAgosto;

	private String volumeSetembro;

	private String volumeOutubro;

	private String volumeNovembro;

	private String volumeDezembro;
	
	private static final int JANEIRO = 1;
	
	private static final int FEVEREIRO = 2;	
	
	private static final int MARÇO = 3;
	
	private static final int ABRIL = 4;
	
	private static final int MAIO = 5;
	
	private static final int JUNHO = 6;
	
	private static final int JULHO = 7;
	
	private static final int AGOSTO = 8;
	
	private static final int SETEMBRO = 9;
	
	private static final int OUTUBRO = 10;
	
	private static final int NOVEMBRO = 11;
	
	private static final int DEZEMBRO = 12;
	
	private static final int VARIAVEL_INICIAL_PARA_SELECIONAR_MES = 4;

	private static final int VARIAVEL_FINAL_PARA_SELECIONAR_MES = 6;
	
	
	/**
	 * @return the idPontoConsumo
	 */
	public Long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the idContrato
	 */
	public Long getIdContrato() {

		return idContrato;
	}

	/**
	 * @param idContrato
	 *            the idContrato to set
	 */
	public void setIdContrato(Long idContrato) {

		this.idContrato = idContrato;
	}

	/**
	 * @return the volumeJaneiro
	 */
	public String getVolumeJaneiro() {

		return volumeJaneiro;
	}

	/**
	 * @param volumeJaneiro
	 *            the volumeJaneiro to set
	 */
	public void setVolumeJaneiro(String volumeJaneiro) {

		this.volumeJaneiro = volumeJaneiro;
	}

	/**
	 * @return the volumeFevereiro
	 */
	public String getVolumeFevereiro() {

		return volumeFevereiro;
	}

	/**
	 * @param volumeFevereiro
	 *            the volumeFevereiro to set
	 */
	public void setVolumeFevereiro(String volumeFevereiro) {

		this.volumeFevereiro = volumeFevereiro;
	}

	/**
	 * @return the volumeMarco
	 */
	public String getVolumeMarco() {

		return volumeMarco;
	}

	/**
	 * @param volumeMarco
	 *            the volumeMarco to set
	 */
	public void setVolumeMarco(String volumeMarco) {

		this.volumeMarco = volumeMarco;
	}

	/**
	 * @return the volumeAbril
	 */
	public String getVolumeAbril() {

		return volumeAbril;
	}

	/**
	 * @param volumeAbril
	 *            the volumeAbril to set
	 */
	public void setVolumeAbril(String volumeAbril) {

		this.volumeAbril = volumeAbril;
	}

	/**
	 * @return the volumeMaio
	 */
	public String getVolumeMaio() {

		return volumeMaio;
	}

	/**
	 * @param volumeMaio
	 *            the volumeMaio to set
	 */
	public void setVolumeMaio(String volumeMaio) {

		this.volumeMaio = volumeMaio;
	}

	/**
	 * @return the volumeJunho
	 */
	public String getVolumeJunho() {

		return volumeJunho;
	}

	/**
	 * @param volumeJunho
	 *            the volumeJunho to set
	 */
	public void setVolumeJunho(String volumeJunho) {

		this.volumeJunho = volumeJunho;
	}

	/**
	 * @return the volumeJulho
	 */
	public String getVolumeJulho() {

		return volumeJulho;
	}

	/**
	 * @param volumeJulho
	 *            the volumeJulho to set
	 */
	public void setVolumeJulho(String volumeJulho) {

		this.volumeJulho = volumeJulho;
	}

	/**
	 * @return the volumeAgosto
	 */
	public String getVolumeAgosto() {

		return volumeAgosto;
	}

	/**
	 * @param volumeAgosto
	 *            the volumeAgosto to set
	 */
	public void setVolumeAgosto(String volumeAgosto) {

		this.volumeAgosto = volumeAgosto;
	}

	/**
	 * @return the volumeSetembro
	 */
	public String getVolumeSetembro() {

		return volumeSetembro;
	}

	/**
	 * @param volumeSetembro
	 *            the volumeSetembro to set
	 */
	public void setVolumeSetembro(String volumeSetembro) {

		this.volumeSetembro = volumeSetembro;
	}

	/**
	 * @return the volumeOutubro
	 */
	public String getVolumeOutubro() {

		return volumeOutubro;
	}

	/**
	 * @param volumeOutubro
	 *            the volumeOutubro to set
	 */
	public void setVolumeOutubro(String volumeOutubro) {

		this.volumeOutubro = volumeOutubro;
	}

	/**
	 * @return the volumeNovembro
	 */
	public String getVolumeNovembro() {

		return volumeNovembro;
	}

	/**
	 * @param volumeNovembro
	 *            the volumeNovembro to set
	 */
	public void setVolumeNovembro(String volumeNovembro) {

		this.volumeNovembro = volumeNovembro;
	}

	/**
	 * @return the volumeDezembro
	 */
	public String getVolumeDezembro() {

		return volumeDezembro;
	}

	/**
	 * @param volumeDezembro
	 *            the volumeDezembro to set
	 */
	public void setVolumeDezembro(String volumeDezembro) {

		this.volumeDezembro = volumeDezembro;
	}

	/**
	 * @return the nomeCliente
	 */
	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 *            the nomeCliente to set
	 */
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return the descricaoSegmento
	 */
	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	/**
	 * @param descricaoSegmento
	 *            the descricaoSegmento to set
	 */
	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	/**
	 * @return the descricaoPontoConsumo
	 */
	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	/**
	 * @param descricaoPontoConsumo
	 *            the descricaoPontoConsumo to set
	 */
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/**
	 * @return the descricaoImovel
	 */
	public String getDescricaoImovel() {

		return descricaoImovel;
	}

	/**
	 * @param descricaoImovel
	 *            the descricaoImovel to set
	 */
	public void setDescricaoImovel(String descricaoImovel) {

		this.descricaoImovel = descricaoImovel;
	}

	/**
	 * @return the dataAssinaturaContrato
	 */
	public String getDataAssinaturaContrato() {

		return dataAssinaturaContrato;
	}

	/**
	 * @param dataAssinaturaContrato
	 *            the dataAssinaturaContrato to
	 *            set
	 */
	public void setDataAssinaturaContrato(String dataAssinaturaContrato) {

		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	/**
	 * @return the dataInicioConsumo
	 */
	public String getDataInicioConsumo() {

		return dataInicioConsumo;
	}

	/**
	 * @param dataInicioConsumo
	 *            the dataInicioConsumo to set
	 */
	public void setDataInicioConsumo(String dataInicioConsumo) {

		this.dataInicioConsumo = dataInicioConsumo;
	}

	/**
	 * @return the dataEncerramento
	 */
	public String getDataEncerramento() {

		return dataEncerramento;
	}

	/**
	 * @param dataEncerramento
	 *            the dataEncerramento to set
	 */
	public void setDataEncerramento(String dataEncerramento) {

		this.dataEncerramento = dataEncerramento;
	}

	/**
	 * @return the zonaBloqueio
	 */
	public String getZonaBloqueio() {

		return zonaBloqueio;
	}

	/**
	 * @param zonaBloqueio
	 *            the zonaBloqueio to set
	 */
	public void setZonaBloqueio(String zonaBloqueio) {

		this.zonaBloqueio = zonaBloqueio;
	}

	/**
	 * @return the modalidadeMedicao
	 */
	public String getModalidadeMedicao() {

		return modalidadeMedicao;
	}

	/**
	 * @param modalidadeMedicao
	 *            the modalidadeMedicao to set
	 */
	public void setModalidadeMedicao(String modalidadeMedicao) {

		this.modalidadeMedicao = modalidadeMedicao;
	}

	/**
	 * @return the descricaoRamoAtividade
	 */
	public String getDescricaoRamoAtividade() {

		return descricaoRamoAtividade;
	}

	/**
	 * @param descricaoRamoAtividade
	 *            the descricaoRamoAtividade to
	 *            set
	 */
	public void setDescricaoRamoAtividade(String descricaoRamoAtividade) {

		this.descricaoRamoAtividade = descricaoRamoAtividade;
	}

	/**
	 * @return the modalidadeUso
	 */
	public String getModalidadeUso() {

		return modalidadeUso;
	}

	/**
	 * @param modalidadeUso
	 *            the modalidadeUso to set
	 */
	public void setModalidadeUso(String modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	/**
	 * @return the idCliente
	 */
	public Long getIdCliente() {

		return idCliente;
	}

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

	/**
	 * Método responsável por setar os volumes de
	 * cada mês do ano.
	 * 
	 * @param volume
	 *            Volume
	 * @param anoMes
	 *            Ano e Mes.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void setarVolume(BigDecimal volume, Integer anoMes) throws GGASException {

		int mes = Integer.parseInt(anoMes.toString().substring(VARIAVEL_INICIAL_PARA_SELECIONAR_MES, VARIAVEL_FINAL_PARA_SELECIONAR_MES));
		String volumeFaturado = Util.converterCampoValorDecimalParaString("", volume, Constantes.LOCALE_PADRAO);

		switch(mes) {
			case JANEIRO :
				setVolumeJaneiro(volumeFaturado);
				break;
			case FEVEREIRO :
				setVolumeFevereiro(volumeFaturado);
				break;
			case MARÇO :
				setVolumeMarco(volumeFaturado);
				break;
			case ABRIL :
				setVolumeAbril(volumeFaturado);
				break;
			case MAIO :
				setVolumeMaio(volumeFaturado);
				break;
			case JUNHO :
				setVolumeJunho(volumeFaturado);
				break;
			case JULHO :
				setVolumeJulho(volumeFaturado);
				break;
			case AGOSTO :
				setVolumeAgosto(volumeFaturado);
				break;
			case SETEMBRO :
				setVolumeSetembro(volumeFaturado);
				break;
			case OUTUBRO :
				setVolumeOutubro(volumeFaturado);
				break;
			case NOVEMBRO :
				setVolumeNovembro(volumeFaturado);
				break;
			case DEZEMBRO :
				setVolumeDezembro(volumeFaturado);
				break;
			default :
				break;
		}

	}

}
