/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.descontosConcedidos;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioDescontosConcedidosFaturaVO;

/**
 * Classe referentes as chamadas da classe RelatorioDescontosConcedidosFaturaAction
 * 
 * @author vpessoa
 */
@Controller
public class RelatorioDescontosConcedidosFaturaAction extends GenericAction {
	
	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	private static final Logger LOG = Logger.getLogger(RelatorioDescontosConcedidosFaturaAction.class);

	protected Fachada fachada = Fachada.getInstancia();

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	/**
	 * Exibir pesquisa relatorio descontos concedidos fatura.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("exibirPesquisaRelatorioDescontosConcedidosFatura")
	public ModelAndView exibirPesquisaRelatorioDescontosConcedidosFatura() throws NegocioException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioDescontosConcedidosFatura");

		try {
			model.addObject(LISTA_GRUPO_FATURAMENTO, fachada.listarGruposFaturamentoRotas());
			model.addObject(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		} catch(GGASException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return model;
	}

	/**
	 * Gerar relatorio descontos concedidos fatura.
	 * 
	 * @param pesquisaRelatorioDescontosConcedidosFaturaVO
	 *            the pesquisa relatorio descontos concedidos fatura vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioDescontosConcedidosFatura")
	public ModelAndView gerarRelatorioDescontosConcedidosFatura(
		@ModelAttribute("PesquisaRelatorioDescontosConcedidosFaturaVO") 
		PesquisaRelatorioDescontosConcedidosFaturaVO pesquisaRelatorioDescontosConcedidosFaturaVO,
		@RequestParam("tipoRelatorio") String tipoRelatorio, BindingResult result, HttpServletRequest request,
		HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioDescontosConcedidosFatura");
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			relatorio = controladorFatura.gerarRelatorioDescontosConcedidosFatura(pesquisaRelatorioDescontosConcedidosFaturaVO);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(
							"Content-Disposition",
							"attachment; filename=RelatorioDescontosConcedidos_" + diaMesAno + "_" + hora
											+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch(IOException e) {
			LOG.error(e.getMessage(), e);
			model.addObject(LISTA_GRUPO_FATURAMENTO, fachada.listarGruposFaturamentoRotas());
			model.addObject(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
			model.addObject("pesquisaRelatorioDescontosConcedidos", pesquisaRelatorioDescontosConcedidosFaturaVO);
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		} catch (NegocioException e) {
			model.addObject(LISTA_GRUPO_FATURAMENTO, fachada.listarGruposFaturamentoRotas());
			model.addObject(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
			model.addObject("pesquisaRelatorioDescontosConcedidos", pesquisaRelatorioDescontosConcedidosFaturaVO);
			Collection<PontoConsumo> listaPontoConsumo = null;
			Long idImovel = pesquisaRelatorioDescontosConcedidosFaturaVO.getIdImovel();
			Long idCliente = pesquisaRelatorioDescontosConcedidosFaturaVO.getIdCliente();
			if ((idImovel != null) && (idImovel > 0)) {
				listaPontoConsumo = fachada.carregarPontoConsumoPorChaveImovel(idImovel);
			} else if ((idCliente != null) && (idCliente > 0)) {
				listaPontoConsumo = fachada.listarPontoConsumoPorCliente(idCliente);
			}
			model.addObject("listaPontoConsumo", listaPontoConsumo);
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Pesquisar pontos consumo descontos.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @param idCliente
	 *            the id cliente
	 * @param numeroContrato
	 *            the numero contrato
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarPontosConsumoDescontos")
	public ModelAndView pesquisarPontosConsumoDescontos(@RequestParam("idImovel") Long idImovel, @RequestParam("idCliente") Long idCliente,
					@RequestParam("numeroContrato") String numeroContrato) throws GGASException {

		ModelAndView model = new ModelAndView("exibirGridPontosConsumo");
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = fachada.carregarPontoConsumoPorChaveImovel(idImovel);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = fachada.listarPontoConsumoPorCliente(idCliente);
		} else if (numeroContrato != null && !numeroContrato.isEmpty()) {
			listaPontoConsumo = fachada.listarPontoConsumoPorNumeroContrato(numeroContrato);
		}
		model.addObject("listaPontoConsumo", listaPontoConsumo);
		return model;
	}

}
