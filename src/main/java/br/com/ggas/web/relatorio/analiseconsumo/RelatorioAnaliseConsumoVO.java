/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.analiseconsumo;

import java.util.Collection;

public class RelatorioAnaliseConsumoVO {

	private String nomeCliente;

	private String nomeFantasia;

	private String cpfCnpj;

	private String numeroDocFiscal;

	private String dataInicial;

	private String dataFinal;

	private Integer qtdDias;

	private String imovelPontoConsumo;

	private Collection<DadosRelatorioAnaliseConsumoVO> listaDados;

	private Collection<RelatorioAnaliseConsumoDadosResumoVO> listaDadosResumo;

	private String tipoConsumo;

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getCpfCnpj() {

		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {

		this.cpfCnpj = cpfCnpj;
	}

	public Collection<DadosRelatorioAnaliseConsumoVO> getListaDados() {

		return listaDados;
	}

	public void setListaDados(Collection<DadosRelatorioAnaliseConsumoVO> listaDados) {

		this.listaDados = listaDados;
	}

	public String getNumeroDocFiscal() {

		return numeroDocFiscal;
	}

	public void setNumeroDocFiscal(String numeroDocFiscal) {

		this.numeroDocFiscal = numeroDocFiscal;
	}

	public String getDataInicial() {

		return dataInicial;
	}

	public void setDataInicial(String dataInicial) {

		this.dataInicial = dataInicial;
	}

	public String getDataFinal() {

		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {

		this.dataFinal = dataFinal;
	}

	public Integer getQtdDias() {

		return qtdDias;
	}

	public void setQtdDias(Integer qtdDias) {

		this.qtdDias = qtdDias;
	}

	public String getTipoConsumo() {

		return tipoConsumo;
	}

	public void setTipoConsumo(String tipoConsumo) {

		this.tipoConsumo = tipoConsumo;
	}

	public Collection<RelatorioAnaliseConsumoDadosResumoVO> getListaDadosResumo() {

		return listaDadosResumo;
	}

	public void setListaDadosResumo(Collection<RelatorioAnaliseConsumoDadosResumoVO> listaDadosResumo) {

		this.listaDadosResumo = listaDadosResumo;
	}

	public String getImovelPontoConsumo() {

		return imovelPontoConsumo;
	}

	public void setImovelPontoConsumo(String imovelPontoConsumo) {

		this.imovelPontoConsumo = imovelPontoConsumo;
	}

}
