/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.tabelastarifariascadastradas;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.impl.TarifaVO;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Tarifas Cadastradas.
 */
@Controller
public class RelatorioTabelasTarifariasCadastradasAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioTabelasTarifariasCadastradasAction.class);

	private static final String DATA_INICIAL = "dataInicialTarifas";

	private static final String DATA_FINAL = "dataFinalTarifas";

	private static final String PERIODO = "periodo";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String DESCRICAO_SEGMENTO_FILTRO = "descricaoSegmentoFiltro";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String RELATORIO_TARIFAS_CADASTRADAS = "relatorioTarifasCadastradas.jasper";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorTarifa controladorTarifa;
	
	@Autowired
	private ControladorEmpresa controladorEmpresa;
	
	/**
	 * Exibir relatorio tabelas tarifas cadastradas.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirRelatorioTabelasTarifasCadastradas - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioTabelasTarifasCadastradas")
	public String exibirRelatorioTabelasTarifasCadastradas(HttpServletRequest request, Model model) throws GGASException {

		try {
			Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
			model.addAttribute("listaSegmento", listaSegmento);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirRelatorioTabelasTarifasCadastradas";
	}

	/**
	 * Gerar relatorio tabelas tarifarias cadastradas.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioTabelasTarifariasCadastradas")
	public String gerarRelatorioTabelasTarifariasCadastradas(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		String dataInicial = relatorioSubMenuFaturamentoVO.getDataInicial();
		String dataFinal = relatorioSubMenuFaturamentoVO.getDataFinal();
		Boolean exibirFiltros = relatorioSubMenuFaturamentoVO.isExibirFiltros();
		Long idSegmento = relatorioSubMenuFaturamentoVO.getIdSegmento();

		String formato = relatorioSubMenuFaturamentoVO.getFormatoImpressao();
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(formato);

		Map<String, Object> filtro = new HashMap<String, Object>();
		Map<String, Object> parametros = new HashMap<String, Object>();

		try {

			if (StringUtils.isNotEmpty(dataInicial)) {
				filtro.put(DATA_INICIAL, Util.converterCampoStringParaData("Data Inicial", dataInicial, Constantes.FORMATO_DATA_BR));
				parametros.put(DATA_INICIAL, dataInicial);
			}

			if (StringUtils.isNotEmpty(dataFinal)) {
				filtro.put(DATA_FINAL, Util.converterCampoStringParaData("Data Final", dataFinal, Constantes.FORMATO_DATA_BR));
				parametros.put(DATA_FINAL, dataFinal);
			}

			if (idSegmento != null && idSegmento > 0) {
				filtro.put(ID_SEGMENTO, idSegmento);
				parametros.put(ID_SEGMENTO, idSegmento);
			}

			Collection<TarifaVO> tarifaVo = controladorTarifa.listarTarifaVigenciaVO(filtro);

			Empresa principal = controladorEmpresa.obterEmpresaPrincipal();
			if (principal.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(principal.getChavePrimaria()));
			}

			parametros.put(EXIBIR_FILTROS, exibirFiltros);

			this.setarDadosFiltro(parametros);

			byte[] relatorioTarifasCadastradas =
					RelatorioUtil.gerarRelatorio(tarifaVo, parametros, RELATORIO_TARIFAS_CADASTRADAS, formatoImpressao);

			ServletOutputStream servletOutputStream;

			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioTarifasCadastradas.length);
			response.addHeader("Content-Disposition",
					"attachment; filename=relatorioTarifa" + this.obterFormatoRelatorio(formatoImpressao));

			request.setAttribute(EXIBIR_MENSAGEM_TELA, false);

			servletOutputStream.write(relatorioTarifasCadastradas, 0, relatorioTarifasCadastradas.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioTabelasTarifasCadastradas(request, model);
		}

		return view;
	}

	/**
	 * Setar dados filtro.
	 * 
	 * @param parametros - {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void setarDadosFiltro(Map<String, Object> parametros) throws GGASException {

		if (parametros.containsKey(EXIBIR_FILTROS)) {

			Boolean exibirFiltros = (Boolean) parametros.get(EXIBIR_FILTROS);

			if (exibirFiltros) {

				String dataInicial = (String) parametros.get(DATA_INICIAL);
				String dataFinal = (String) parametros.get(DATA_FINAL);
				Long idSegmento = (Long) parametros.get(ID_SEGMENTO);

				if (dataInicial != null && dataFinal != null) {

					parametros.put(PERIODO, dataInicial + " à " + dataFinal);

				}

				if (idSegmento != null) {

					Segmento segmento = controladorSegmento.obterSegmento(idSegmento);
					parametros.put(DESCRICAO_SEGMENTO_FILTRO, segmento.getDescricao());

				}

			}

		}

	}

}
