package br.com.ggas.web.relatorio.pontoConsumoPorData;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Classe responsável pela transferência de dados relacionados ao ponto de consumo
 *
 */
public class PontoConsumoPorDataVO {
	
	private String nomeCliente;
	private String descricaoPontoConsumo;
	private String segmento;
	private String ramoAtividade;
	private String cep;
	private String numero;
	private String rota;
	private Date dataAtivacao;
	private String dataFormatada;
	private String tarifa;

	public PontoConsumoPorDataVO(){

	}

	public PontoConsumoPorDataVO(String nomeCliente, String descricaoPontoConsumo, String segmento, 
					String ramoAtividade, String cep, String numero, String rota, Date dataAtivacao){
		this.nomeCliente = nomeCliente;
		this.descricaoPontoConsumo = descricaoPontoConsumo;
		this.segmento = segmento;
		this.ramoAtividade = ramoAtividade;
		this.cep = cep;
		this.numero = numero;
		this.rota = rota;
		this.dataAtivacao = dataAtivacao;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.dataFormatada = sdf.format(dataAtivacao);
	}
	
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}

	public String getDescricaoPontoConsumo() {
	
		return descricaoPontoConsumo;
	}

	
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
	
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	
	public String getSegmento() {
	
		return segmento;
	}

	
	public void setSegmento(String segmento) {
	
		this.segmento = segmento;
	}

	
	public String getRamoAtividade() {
	
		return ramoAtividade;
	}

	
	public void setRamoAtividade(String ramoAtividade) {
	
		this.ramoAtividade = ramoAtividade;
	}

	
	public String getCep() {
	
		return cep;
	}

	
	public void setCep(String cep) {
	
		this.cep = cep;
	}

	
	public String getNumero() {
	
		return numero;
	}

	
	public void setNumero(String numero) {
	
		this.numero = numero;
	}

	
	public String getRota() {
	
		return rota;
	}

	
	public void setRota(String rota) {
	
		this.rota = rota;
	}

	public Date getDataAtivacao() {
		Date data = null;
		if (this.dataAtivacao != null) {
			data = (Date) dataAtivacao.clone();
		}
		return data;
	}

	public void setDataAtivacao(Date dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.dataFormatada = sdf.format(dataAtivacao);
	}

	public String getDataFormatada() {
		return dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
	
	
	
	

}
