/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturamento;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.fatura.impl.OrdenacaoDataEmissaoFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.relatorio.ControladorRelatorioFaturamento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Faturamento.
 */
@Controller
public class RelatorioFaturamentoAction extends GenericAction {

	private static final String ID_CLIENTE = "idCliente";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String ID_CONTRATO = "idContrato";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String NUMERO_NOTA_FISCAL_INICIAL = "numeroNotaFiscalInicial";

	private static final String NUMERO_NOTA_FISCAL_FINAL = "numeroNotaFiscalFinal";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String ORDENACAO_RELATORIO_FATURAMENTO = "ordenacaoRelatorioFaturamento";

	private static final String SITUACAO_VALIDA = "situacaoValida";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupo";
	
	private static final String ID_ROTA = "idRota";
	
	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";
		
	private static final String NUMERO_CICLO = "numeroCiclo";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired 
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorRelatorioFaturamento controladorRelatorioFaturamento;
	
	/**
	 * Método responsável por exibir a tela de
	 * inclusão de contrato passo 1.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return exibirRelatorioFaturamento - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioFaturamento")
	public String exibirRelatorioFaturamento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result, Model model)
			throws GGASException {

		try {
			this.carregarDados(relatorioSubMenuFaturamentoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirRelatorioFaturamento";
	}

	/**
	 * Pesquisar ponto consumo relatorio faturamento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirRelatorioFaturamento - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatorioFaturamento")
	public String pesquisarPontoConsumoRelatorioFaturamento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO,
			BindingResult result, ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			pesquisarPontosConsumo(relatorioSubMenuFaturamentoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return exibirRelatorioFaturamento(relatorioSubMenuFaturamentoVO, result, model);
	}

	/**
	 * Pesquisar pontos consumo.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void pesquisarPontosConsumo(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Model model) throws GGASException {

		Long idImovel = relatorioSubMenuFaturamentoVO.getIdImovel();
		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}

	/**
	 * Carregar dados.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Model model) throws GGASException {

		Long idGrupo = relatorioSubMenuFaturamentoVO.getIdGrupo();

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<Segmento> listaSegmento = controladorSegmento.consultarSegmento(filtro);

		model.addAttribute("listaSegmento", listaSegmento);
		model.addAttribute("listaGrupoFaturamento", controladorRota.listarGruposFaturamentoRotas());

		model.addAttribute("listaCiclo", new ArrayList<Integer>(Arrays.asList(Constantes.Ciclo.UM.getValor(),
				Constantes.Ciclo.DOIS.getValor(), Constantes.Ciclo.TRES.getValor(), Constantes.Ciclo.QUATRO.getValor())));

		if (idGrupo != null) {
			model.addAttribute("listaRota", controladorRota.listarRotasPorGrupoFaturamento(idGrupo));
		}

	}

	/**
	 * Gerar relatorio faturamento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioFaturamento")
	public String gerarRelatorioFaturamento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, Model model, HttpServletRequest request,
			HttpServletResponse response) throws GGASException{

		String view = null;

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuFaturamentoVO.getFormatoImpressao());

		try {

			Map<String, Object> filtro = this.prepararFiltro(relatorioSubMenuFaturamentoVO);

			boolean exibirFiltros = relatorioSubMenuFaturamentoVO.isExibirFiltros();

			byte[] relatorio = controladorRelatorioFaturamento.gerarRelatorio(filtro, formatoImpressao, exibirFiltros);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition",
					"attachment; filename=relatorioFaturamento" + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioFaturamento(relatorioSubMenuFaturamentoVO, bindingResult, model);
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String dataEmissaoInicial = relatorioSubMenuFaturamentoVO.getDataEmissaoInicial();
		String dataEmissaoFinal = relatorioSubMenuFaturamentoVO.getDataEmissaoFinal();
		String dataVencimentoInicial = relatorioSubMenuFaturamentoVO.getDataVencimentoInicial();
		String dataVencimentoFinal = relatorioSubMenuFaturamentoVO.getDataVencimentoFinal();
		String anoMesReferencia = relatorioSubMenuFaturamentoVO.getAnoMesReferencia();
		String numeroNotaFiscalInicial = relatorioSubMenuFaturamentoVO.getNumeroNotaFiscalInicial();
		String numeroNotaFiscalFinal = relatorioSubMenuFaturamentoVO.getNumeroNotaFiscalFinal();
		Integer numeroCiclo = relatorioSubMenuFaturamentoVO.getNumeroCiclo();

		if (StringUtils.isNotEmpty(numeroNotaFiscalInicial)) {
			filtro.put(NUMERO_NOTA_FISCAL_INICIAL,
					Util.converterCampoStringParaValorLong("Número da nota fiscal inicial", numeroNotaFiscalInicial));
		}

		if (StringUtils.isNotEmpty(numeroNotaFiscalFinal)) {
			filtro.put(NUMERO_NOTA_FISCAL_FINAL,
					Util.converterCampoStringParaValorLong("Número da nota fiscal Final", numeroNotaFiscalFinal));
		}

		if (StringUtils.isNotEmpty(dataEmissaoInicial)) {
			DateTime dateTime = Util.zerarHorario(
					new DateTime(Util.converterCampoStringParaData(DATA_EMISSAO_INICIAL, dataEmissaoInicial, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_EMISSAO_INICIAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataEmissaoFinal)) {
			DateTime dateTime = Util.ultimoHorario(
					new DateTime(Util.converterCampoStringParaData(DATA_EMISSAO_FINAL, dataEmissaoFinal, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_EMISSAO_FINAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataVencimentoInicial)) {
			DateTime dateTime = Util.zerarHorario(new DateTime(
					Util.converterCampoStringParaData(DATA_VENCIMENTO_INICIAL, dataVencimentoInicial, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_INICIAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataVencimentoFinal)) {
			DateTime dateTime = Util.ultimoHorario(new DateTime(
					Util.converterCampoStringParaData(DATA_VENCIMENTO_FINAL, dataVencimentoFinal, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_FINAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(anoMesReferencia)) {
			filtro.put(ANO_MES_REFERENCIA, Integer.parseInt(anoMesReferencia.replace("/", "").replaceAll("_", "")));
		}
		
		if (numeroCiclo > 0) {
			filtro.put(NUMERO_CICLO, numeroCiclo);
		}
		
		filtro.put(SITUACAO_VALIDA, true);
		
		filtro.put(ORDENACAO_RELATORIO_FATURAMENTO, "");
		
		prepararFiltroDois(relatorioSubMenuFaturamentoVO, filtro);
		
		prepararFiltroTres(relatorioSubMenuFaturamentoVO, filtro);


		OrdenacaoDataEmissaoFatura.setOrdenacao(filtro, OrdenacaoDataEmissaoFatura.ASCENDENTE);

		return filtro;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private void prepararFiltroDois(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Map<String, Object> filtro)
			throws GGASException {

		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();
		Long idImovel = relatorioSubMenuFaturamentoVO.getIdImovel();
		Long idPontoConsumo = relatorioSubMenuFaturamentoVO.getIdPontoConsumo();

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			filtro.put(CHAVES_PONTO_CONSUMO, new Long[] { idPontoConsumo });
		}

		if (idImovel != null && idImovel > 0) {
			Collection<PontoConsumo> listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(idImovel);
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
			}
		}
	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param filtro - {@link Map}
	 */
	private void prepararFiltroTres(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Map<String, Object> filtro) {

		Long idContrato = relatorioSubMenuFaturamentoVO.getIdContrato();
		Long idSegmento = relatorioSubMenuFaturamentoVO.getIdSegmento();
		Long idGrupo = relatorioSubMenuFaturamentoVO.getIdGrupo();
		Long idRota = relatorioSubMenuFaturamentoVO.getIdRota();

		if (idContrato != null && idContrato > 0) {
			filtro.put(ID_CONTRATO, idContrato);
		}

		if (idSegmento != null && idSegmento > 0) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		if (idGrupo != null && idGrupo > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, idGrupo);
		}

		if (idRota != null && idRota > 0) {
			filtro.put(ID_ROTA, idRota);
		}
	}

}
