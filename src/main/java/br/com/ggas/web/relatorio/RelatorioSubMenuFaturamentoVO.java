package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes a funcionalidade Relatorio Sub-Menu Faturamento.
 * 
 * @author bruno silva
 * 
 * @see	-	RelatorioTitulosAtrasoAction, RelatorioDescontosConcedidosAction, RelatorioFaturamentoAction
 * 			RelatorioFaturasEmitidasAction, RelatorioTabelasTarifariasCadastradasAction, RelatorioVolumesFaturadosSegmentoAction
 * 			RelatorioPosicaoContasReceberAction, RelatorioTitulosAbertoPorDataVencimentoAction
 */
public class RelatorioSubMenuFaturamentoVO {

	private String dataReferencia;
	private String formatoImpressao;
	private String exibirCliente = "false";
	private String dataInicial;
	private String dataFinal;
	private String dataEmissao;
	private String dataEmissaoInicial;
	private String dataEmissaoFinal;
	private String dataVencimentoInicial;
	private String dataVencimentoFinal;
	private String tipoExibicao = "analitico";
	private String numeroNota;
	private String numeroContrato;
	private String idTipoGrafico;
	private String indicadorPesquisa;
	private String anoMesReferencia;
	private String situacaoDebito;
	
	private boolean exibirFiltros;
	private boolean exibirValoresNulos = true;
	private boolean exibirTambemContratosInativos = true;
	private Boolean exibirGrafico;
	
	private Integer diasAtraso;
	private Integer numeroCiclo;
	
	private Long idImovel;
	private Long idCliente;
	private Long idSegmento;
	private Long idPontoConsumo;
	private Long idContrato;
	private String numeroNotaFiscalInicial;
	private String numeroNotaFiscalFinal;
	private Long idGrupo;
	private Long idRota;
	private Long[] idsSegmentos;
	private Long[] rubricasAssociadas;
	private Long[] idsPontoConsumo;
	
	/**
	 * @return dataReferencia - {@link String}
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}
	
	/**
	 * @param dataReferencia - {@link String}
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	
	/**
	 * @return formatoImpressao - {@link String}
	 */
	public String getFormatoImpressao() {
		return formatoImpressao;
	}

	/**
	 * @param formatoImpressao - {@link String}
	 */
	public void setFormatoImpressao(String formatoImpressao) {
		
		if(("").equals(formatoImpressao)) {
			formatoImpressao = "PDF";
		}
		
		this.formatoImpressao = formatoImpressao;
	}

	/**
	 * @return exibirCliente - {@link String}
	 */
	public String getExibirCliente() {
		return exibirCliente;
	}

	/**
	 * @param exibirCliente - {@link String}
	 */
	public void setExibirCliente(String exibirCliente) {
		this.exibirCliente = exibirCliente;
	}
	
	/**
	 * @return dataInicial - {@link String}
	 */
	public String getDataInicial() {
		return dataInicial;
	}

	/**
	 * @param dataInicial - {@link String}
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}

	/**
	 * @return dataFinal - {@link String}
	 */
	public String getDataFinal() {
		return dataFinal;
	}

	/**
	 * @param dataFinal - {@link String}
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}

	/**
	 * @return dataEmissao - {@link String}
	 */
	public String getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * @param dataEmissao - {@link String}
	 */
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	
	/**
	 * @return dataEmissaoInicial - {@link String}
	 */
	public String getDataEmissaoInicial() {
		return dataEmissaoInicial;
	}

	/**
	 * @param dataEmissaoInicial - {@link String}
	 */
	public void setDataEmissaoInicial(String dataEmissaoInicial) {
		this.dataEmissaoInicial = dataEmissaoInicial;
	}

	/**
	 * @return dataEmissaoFinal - {@link String}
	 */
	public String getDataEmissaoFinal() {
		return dataEmissaoFinal;
	}

	/**
	 * @param dataEmissaoFinal - {@link String}
	 */
	public void setDataEmissaoFinal(String dataEmissaoFinal) {
		this.dataEmissaoFinal = dataEmissaoFinal;
	}

	/**
	 * @return dataVencimentoInicial - {@link String}
	 */
	public String getDataVencimentoInicial() {
		return dataVencimentoInicial;
	}

	/**
	 * @param dataVencimentoInicial - {@link String}
	 */
	public void setDataVencimentoInicial(String dataVencimentoInicial) {
		this.dataVencimentoInicial = dataVencimentoInicial;
	}

	/**
	 * @return dataVencimentoFinal - {@link String}
	 */
	public String getDataVencimentoFinal() {
		return dataVencimentoFinal;
	}

	/**
	 * @param dataVencimentoFinal - {@link String}
	 */
	public void setDataVencimentoFinal(String dataVencimentoFinal) {
		this.dataVencimentoFinal = dataVencimentoFinal;
	}
	
	/**
	 * @return tipoExibicao - {@link String}
	 */
	public String getTipoExibicao() {
		return tipoExibicao;
	}

	/**
	 * @param tipoExibicao - {@link String}
	 */
	public void setTipoExibicao(String tipoExibicao) {
		this.tipoExibicao = tipoExibicao;
	}

	/**
	 * @return numeroNota - {@link String}
	 */
	public String getNumeroNota() {
		return numeroNota;
	}

	/**
	 * @param numeroNota - {@link String}
	 */
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	/**
	 * @return numeroContrato - {@link String}
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * @param numeroContrato - {@link String}
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return idTipoGrafico - {@link String}
	 */
	public String getIdTipoGrafico() {
		return idTipoGrafico;
	}

	/**
	 * @param idTipoGrafico - {@link String}
	 */
	public void setIdTipoGrafico(String idTipoGrafico) {
		this.idTipoGrafico = idTipoGrafico;
	}
	
	/**
	 * @return indicadorPesquisa - {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa - {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	
	/**
	 * @return anoMesReferencia - {@link String}
	 */
	public String getAnoMesReferencia() {
		return anoMesReferencia;
	}

	/**
	 * @param anoMesReferencia - {@link String}
	 */
	public void setAnoMesReferencia(String anoMesReferencia) {
		this.anoMesReferencia = anoMesReferencia;
	}
	
	/**
	 * @return numeroNotaFiscalInicial - {@link String}
	 */
	public String getNumeroNotaFiscalInicial() {
		return numeroNotaFiscalInicial;
	}

	/**
	 * @param numeroNotaFiscalInicial - {@link String}
	 */
	public void setNumeroNotaFiscalInicial(String numeroNotaFiscalInicial) {
		this.numeroNotaFiscalInicial = numeroNotaFiscalInicial;
	}

	/**
	 * @return numeroNotaFiscalFinal - {@link String}
	 */
	public String getNumeroNotaFiscalFinal() {
		return numeroNotaFiscalFinal;
	}

	/**
	 * @param numeroNotaFiscalFinal - {@link String}
	 */
	public void setNumeroNotaFiscalFinal(String numeroNotaFiscalFinal) {
		this.numeroNotaFiscalFinal = numeroNotaFiscalFinal;
	}
	
	/**
	 * @return situacaoDebito - {@link String}
	 */
	public String getSituacaoDebito() {
		return situacaoDebito;
	}

	/**
	 * @param situacaoDebito - {@link String}
	 */
	public void setSituacaoDebito(String situacaoDebito) {
		this.situacaoDebito = situacaoDebito;
	}

	/**
	 * @return exibirFiltros - {@link Boolean}
	 */
	public boolean isExibirFiltros() {
		return exibirFiltros;
	}

	/**
	 * @param exibirFiltros - {@link Boolean}
	 */
	public void setExibirFiltros(boolean exibirFiltros) {
		this.exibirFiltros = exibirFiltros;
	}
	
	/**
	 * @return exibirValoresNulos - {@link Boolean}
	 */
	public boolean isExibirValoresNulos() {
		return exibirValoresNulos;
	}

	/**
	 * @param exibirValoresNulos - {@link Boolean}
	 */
	public void setExibirValoresNulos(boolean exibirValoresNulos) {
		this.exibirValoresNulos = exibirValoresNulos;
	}
	
	/**
	 * @return exibirTambemContratosInativos - {@link Boolean}
	 */
	public boolean isExibirTambemContratosInativos() {
		return exibirTambemContratosInativos;
	}

	/**
	 * @param exibirTambemContratosInativos - {@link Boolean}
	 */
	public void setExibirTambemContratosInativos(boolean exibirTambemContratosInativos) {
		this.exibirTambemContratosInativos = exibirTambemContratosInativos;
	}
	
	/**
	 * @return exibirGrafico - {@link Boolean}
	 */
	public Boolean getExibirGrafico() {
		return exibirGrafico;
	}

	/**
	 * @param exibirGrafico - {@link Boolean}
	 */
	public void setExibirGrafico(Boolean exibirGrafico) {
		this.exibirGrafico = exibirGrafico;
	}

	/**
	 * @return diasAtraso - {@link Integer}
	 */
	public Integer getDiasAtraso() {
		return diasAtraso;
	}

	/**
	 * @param diasAtraso - {@link Integer}
	 */
	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	
	/**
	 * @return numeroCiclo - {@link Integer}
	 */
	public Integer getNumeroCiclo() {
		return numeroCiclo;
	}

	/**
	 * @param numeroCiclo - {@link Integer}
	 */
	public void setNumeroCiclo(Integer numeroCiclo) {
		this.numeroCiclo = numeroCiclo;
	}

	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}

	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return idSegmento - {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}

	/**
	 * @param idSegmento - {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	/**
	 * @return idPontoConsumo - {@link Long}
	 */
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo - {@link Long}
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	
	/**
	 * @return idContrato - {@link Long}
	 */
	public Long getIdContrato() {
		return idContrato;
	}

	/**
	 * @param idContrato - {@link Long}
	 */
	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	/**
	 * @return idGrupo - {@link Long}
	 */
	public Long getIdGrupo() {
		return idGrupo;
	}

	/**
	 * @param idGrupo - {@link Long}
	 */
	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	/**
	 * @return idRota - {@link Long}
	 */
	public Long getIdRota() {
		return idRota;
	}

	/**
	 * @param idRota - {@link Long}
	 */
	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}
	
	/**
	 * @return idsSegmentosTmp - {@link Long}
	 */
	public Long[] getIdsSegmentos() {
		Long[] idsSegmentosTmp = null;
		if (idsSegmentos != null) {
			idsSegmentosTmp = idsSegmentos.clone();
		}
		return idsSegmentosTmp;
	}
	/**
	 * @param idsSegmentos - {@link Long}
	 */
	public void setIdsSegmentos(Long[] idsSegmentos) {
		if (idsSegmentos != null) {
			this.idsSegmentos = idsSegmentos.clone();
		} else {
			this.idsSegmentos = null;
		}
	}
	
	/**
	 * @return rubricasAssociadasTmp - {@link Long}
	 */
	public Long[] getRubricasAssociadas() {
		Long[] rubricasAssociadasTmp = null;
		if (rubricasAssociadas != null) {
			rubricasAssociadasTmp = rubricasAssociadas.clone();
		}
		return rubricasAssociadasTmp;
	}
	/**
	 * @param rubricasAssociadas - {@link Long}
	 */
	public void setRubricasAssociadas(Long[] rubricasAssociadas) {
		if (rubricasAssociadas != null) {
			this.rubricasAssociadas = rubricasAssociadas.clone();
		} else {
			this.rubricasAssociadas = null;
		}
	}
	
	/**
	 * @return idsPontoConsumoTmp - {@link Long}
	 */
	public Long[] getIdsPontoConsumo() {
		Long[] idsPontoConsumoTmp = null;
		if (idsPontoConsumo != null) {
			idsPontoConsumoTmp = idsPontoConsumo.clone();
		}
		return idsPontoConsumoTmp;
	}
	/**
	 * @param idsPontoConsumo - {@link Long}
	 */
	public void setIdsPontoConsumo(Long[] idsPontoConsumo) {
		if (idsPontoConsumo != null) {
			this.idsPontoConsumo = idsPontoConsumo.clone();
		} else {
			this.idsPontoConsumo = null;
		}
	}
	
}
