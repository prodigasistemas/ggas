package br.com.ggas.web.relatorio.pontoConsumoPorData;

public class PontoConsumoPorDataTotalizadorVO {
	private String descricaoTotalizador;
	private Long quantidadeTotalizador;
	
	public String getDescricaoTotalizador() {
		return descricaoTotalizador;
	}
	public void setDescricaoTotalizador(String descricaoTotalizador) {
		this.descricaoTotalizador = descricaoTotalizador;
	}
	public Long getQuantidadeTotalizador() {
		return quantidadeTotalizador;
	}
	public void setQuantidadeTotalizador(Long quantidadeTotalizador) {
		this.quantidadeTotalizador = quantidadeTotalizador;
	}
}
