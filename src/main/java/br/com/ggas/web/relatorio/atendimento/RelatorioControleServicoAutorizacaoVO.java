/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe referentes as chamadas da classe RelatorioControleServicoAutorizacaoVO
 * 
 @since 13/10/2014 09:54:35
 @author vpessoa
 */

package br.com.ggas.web.relatorio.atendimento;

import java.math.BigDecimal;

/**
 * @author vpessoa
 */
public class RelatorioControleServicoAutorizacaoVO {

	private String contrato;

	private String empresaContratada;

	private String cliente;

	private String segmento;

	private String numeroServicoAutorizacao;

	private String dataEmissao;

	private BigDecimal valorTotal;

	public String getContrato() {

		return contrato;
	}

	public void setContrato(String contrato) {

		this.contrato = contrato;
	}

	public String getEmpresaContratada() {

		return empresaContratada;
	}

	public void setEmpresaContratada(String empresaContratada) {

		this.empresaContratada = empresaContratada;
	}

	public String getCliente() {

		return cliente;
	}

	public void setCliente(String cliente) {

		this.cliente = cliente;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getNumeroServicoAutorizacao() {

		return numeroServicoAutorizacao;
	}

	public void setNumeroServicoAutorizacao(String numeroServicoAutorizacao) {

		this.numeroServicoAutorizacao = numeroServicoAutorizacao;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

}
