package br.com.ggas.web.relatorio.dataLimiteReligamentoGas;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.ServiceLocator;

@Repository
public class RepositorioDataLimiteReligamentoGas extends RepositorioGenerico{

	@Autowired
	public RepositorioDataLimiteReligamentoGas(SessionFactory sessionFactory){
		setSessionFactory(sessionFactory);
	}
	
	@Override
	public EntidadeNegocio criar() {

		return new ServicoAutorizacao();
	}

	@Override
	public Class<ServicoAutorizacao> getClasseEntidade() {

		return ServicoAutorizacao.class;
	}

	/**
	 * Listar Pontos de Consumo por data.
	 * 
	 * @param dataInicio {@link Date}
	 * @param dataFinal {@link Date}
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoAutorizacaoRelatorioVO> listarDatasLimiteParaReligamentoGas(Date dataInicio, Date dataFinal) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente.nome as nomeCliente, " + "servicoAutorizacao.dataGeracao as dataGerada, "
				+ "servicoAutorizacao.dataEncerramento as dataEncerramento ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servicoAutorizacao ");
		hql.append(" inner join servicoAutorizacao.cliente cliente ");

		if (dataInicio != null) {
			hql.append(" where ");
			hql.append(" servicoAutorizacao.dataGeracao >= :inicio ");
		}

		if (dataFinal != null) {
			if (dataInicio == null) {
				hql.append(" where ");
				hql.append(" iservicoAutorizacao.dataGeracao <= :fim ");
			} else {
				hql.append(" And servicoAutorizacao.dataGeracao <= :fim ");
			}
		}

		hql.append(" order by cliente.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(ServicoAutorizacaoRelatorioVO.class));
		if (dataInicio != null) {
			query.setDate("inicio", dataInicio);
		}
		if (dataFinal != null) {
			query.setDate("fim", dataFinal);
		}

		return query.list();
	}
	
	/**
	 * Listar Pontos de Consumo por data.
	 * 
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public List<String> buscarValorParamentro(String param){
		
		ServiceLocator.getInstancia().getControladorParametroSistema();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select parametroSistema.valor as valor ");
		hql.append(" from ");
		hql.append(" ParametroSistemaImpl ");
		hql.append(" parametroSistema ");
		hql.append(" Where ");
		hql.append(" parametroSistema.codigo ");
		hql.append(" LIKE ");
		hql.append(" :param ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("param", "%"+param+"%");
		
		return query.list();
	}
}

