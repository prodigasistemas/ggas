/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioVolumesFaturados;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Volumes Faturados.
 */
@Controller
public class RelatorioVolumesFaturadosAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioVolumesFaturadosAction.class);

	public static final String PARAM_DATA_INICIAL = "dataInicial";

	public static final String PARAM_DATA_FINAL = "dataFinal";

	public static final String PARAM_PONTOS_CONSUMO = "pontosConsumo";

	public static final String PARAM_SEGMENTO = "descricaoSegmento";

	public static final String PARAM_CLIENTE = "nomeCliente";

	public static final String PARAM_IMOVEL = "nomeImovel";

	public static final String IMOVEL = "idImovel";

	public static final String CLIENTE = "idCliente";

	public static final String SEGMENTO = "idSegmento";

	public static final String PONTOS_CONSUMO = "chavesPontoConsumo";

	public static final String DATA_INICIAL = "dataInicial";

	public static final String DATA_FINAL = "dataFinal";

	public static final String TIPO_EXIBICAO = "tipoExibicao";

	public static final String EXIBIR_FILTROS = "exibirFiltros";

	public static final String EXIBIR_GRAFICO = "exibirGrafico";

	public static final String TIPO_GRAFICO = "idTipoGrafico";

	public static final String FORMATO_IMPRESSAO = "formatoImpressao";

	public static final String RELATORIO = "relatorio";

	public static final String POSSUI_RELATORIO = "possuiRelatorio";

	public static final String LISTA_SEGMENTO = "listaSegmento";

	public static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioVolumesFaturados controladorRelatorioVolumesFaturados;

	/**
	 * Exibir relatorio volumes faturados.
	 * 
	 * @param model - {@link Model}
	 * @return exibirRelatorioVolumesFaturados - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioVolumesFaturados")
	public String exibirRelatorioVolumesFaturados(Model model) throws GGASException {

		try {
			this.carregarDados(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirRelatorioVolumesFaturados";
	}
	
	/**
	 * Gerar relatorio volumes faturados.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioVolumesFaturados")
	public String gerarRelatorioVolumesFaturados(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		LOG.info("gerando Relatorio de Variação de Volumes baseado no mapa de filtros.");

		try {

			Map<String, Object> filtro = prepararFiltroRelatorio(relatorioSubMenuMedicaoVO);

			byte[] relatorio = controladorRelatorioVolumesFaturados.gerarRelatorioVolumesFaturados(filtro);

			if (relatorio != null) {

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				ServletOutputStream servletOutputStream = response.getOutputStream();
				FormatoImpressao formatoImpressao = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition", "attachment; filename=relatorioVolumesFaturados"
						+ this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioVolumesFaturados(model);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Pesquisar ponto consumo relatorio volumes faturados.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirRelatorioVolumesFaturados - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatorioVolumesFaturados")
	public String pesquisarPontoConsumoRelatorioVolumesFaturados(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		try {
			if ((idImovel != null) && (idImovel > 0)) {
				listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
			} else if (idCliente != null && idCliente > 0) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return exibirRelatorioVolumesFaturados(model);
	}
	
	/**
	 * Carregar dados.
	 * 

	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute(LISTA_SEGMENTO, listaSegmento);
	}
	
	/**
	 * Método responsável por preparar os filtros
	 * do relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltroRelatorio(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long cliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long imovel = relatorioSubMenuMedicaoVO.getIdImovel();
		Long[] pontosConsumo = relatorioSubMenuMedicaoVO.getChavesPontoConsumo();
		Long segmento = relatorioSubMenuMedicaoVO.getIdSegmento();
		String dataInicial = relatorioSubMenuMedicaoVO.getDataInicial();
		String dataFinal = relatorioSubMenuMedicaoVO.getDataFinal();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuMedicaoVO.isExibirFiltros());
		filtro.put(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao()));

		if (StringUtils.isNotEmpty(dataInicial)) {
			filtro.put("dataEmissaoInicial", Util.converterCampoStringParaData("Data Inicial", dataInicial, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataFinal)) {
			filtro.put("dataEmissaoFinal", Util.converterCampoStringParaData("Data Final", dataFinal, Constantes.FORMATO_DATA_BR));
		}
		
		if (segmento != null) {
			filtro.put(SEGMENTO, segmento);
		}
		
		if (pontosConsumo != null && pontosConsumo.length > 0) {
			filtro.put(PONTOS_CONSUMO, pontosConsumo);
		} else if (imovel != null && imovel > 0) {
			filtro.put(IMOVEL, imovel);
		} else if (cliente != null && cliente > 0) {
			filtro.put(CLIENTE, cliente);
		}

		return filtro;
	}

}
