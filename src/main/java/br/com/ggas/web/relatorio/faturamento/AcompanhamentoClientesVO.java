/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe referentes as chamadas da classe AcompanhamentoClientesVO
 * 
 @since 13/10/2014 21:02:42
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author vpessoa
 */
public class AcompanhamentoClientesVO {

	private String numeroContrato;

	private String nomeCliente;

	private String situacaoProposta;

	private BigDecimal volumeMensal;

	private String vendedor;

	private String fiscal;

	private Date dataEntregaProposta;

	private Date dataAssinaturaContrato;

	private String executor;

	private Date dataEmissaoAES;

	private Date dataSolicitacaoRamal;

	private Date dataEnvioMateriais;

	private Date dataLiberacao;

	private Date dataDesligamento;

	private Date dataDistrato;

	private BigDecimal participacaoCliente;

	private BigDecimal valorTotal;

	private BigDecimal valorMaoObra;

	private BigDecimal valorMaterial;

	private BigDecimal percentualTIR;

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getSituacaoProposta() {

		return situacaoProposta;
	}

	public void setSituacaoProposta(String situacaoProposta) {

		this.situacaoProposta = situacaoProposta;
	}

	public BigDecimal getVolumeMensal() {

		return volumeMensal;
	}

	public void setVolumeMensal(BigDecimal volumeMensal) {

		this.volumeMensal = volumeMensal;
	}

	public String getVendedor() {

		return vendedor;
	}

	public void setVendedor(String vendedor) {

		this.vendedor = vendedor;
	}

	public String getFiscal() {

		return fiscal;
	}

	public void setFiscal(String fiscal) {

		this.fiscal = fiscal;
	}

	public Date getDataEntregaProposta() {
		Date data = null;
		if (this.dataEntregaProposta != null) {
			data = (Date) dataEntregaProposta.clone();
		}
		return data;
	}

	public void setDataEntregaProposta(Date dataEntregaProposta) {

		this.dataEntregaProposta = dataEntregaProposta;
	}

	public Date getDataAssinaturaContrato() {
		Date data = null;
		if (this.dataAssinaturaContrato != null) {
			data = (Date) dataAssinaturaContrato.clone();
		}
		return data;
	}

	public void setDataAssinaturaContrato(Date dataAssinaturaContrato) {

		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	public String getExecutor() {

		return executor;
	}

	public void setExecutor(String executor) {

		this.executor = executor;
	}

	public Date getDataEmissaoAES() {
		Date data = null;
		if (this.dataEmissaoAES != null) {
			data = (Date) dataEmissaoAES.clone();
		}
		return data;
	}

	public void setDataEmissaoAES(Date dataEmissaoAES) {

		this.dataEmissaoAES = dataEmissaoAES;
	}

	public Date getDataSolicitacaoRamal() {
		Date data = null;
		if (this.dataSolicitacaoRamal != null) {
			data = (Date) dataSolicitacaoRamal.clone();
		}
		return data;
	}

	public void setDataSolicitacaoRamal(Date dataSolicitacaoRamal) {

		this.dataSolicitacaoRamal = dataSolicitacaoRamal;
	}

	public Date getDataEnvioMateriais() {
		Date data = null;
		if (this.dataEnvioMateriais != null) {
			data = (Date) dataEnvioMateriais.clone();
		}
		return data;
	}

	public void setDataEnvioMateriais(Date dataEnvioMateriais) {

		this.dataEnvioMateriais = dataEnvioMateriais;
	}

	public Date getDataLiberacao() {
		Date data = null;
		if (this.dataLiberacao != null) {
			data = (Date) dataLiberacao.clone();
		}
		return data;
	}

	public void setDataLiberacao(Date dataLiberacao) {

		this.dataLiberacao = dataLiberacao;
	}

	public Date getDataDesligamento() {
		Date data = null;
		if (this.dataDesligamento != null) {
			data = (Date) dataDesligamento.clone();
		}
		return data;
	}

	public void setDataDesligamento(Date dataDesligamento) {

		this.dataDesligamento = dataDesligamento;
	}

	public Date getDataDistrato() {
		Date data = null;
		if (this.dataDistrato != null) {
			data = (Date) dataDistrato.clone();
		}
		return data;
	}

	public void setDataDistrato(Date dataDistrato) {

		this.dataDistrato = dataDistrato;
	}

	public BigDecimal getParticipacaoCliente() {

		return participacaoCliente;
	}

	public void setParticipacaoCliente(BigDecimal participacaoCliente) {

		this.participacaoCliente = participacaoCliente;
	}

	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorMaoObra() {

		return valorMaoObra;
	}

	public void setValorMaoObra(BigDecimal valorMaoObra) {

		this.valorMaoObra = valorMaoObra;
	}

	public BigDecimal getValorMaterial() {

		return valorMaterial;
	}

	public void setValorMaterial(BigDecimal valorMaterial) {

		this.valorMaterial = valorMaterial;
	}

	public BigDecimal getPercentualTIR() {

		return percentualTIR;
	}

	public void setPercentualTIR(BigDecimal percentualTIR) {

		this.percentualTIR = percentualTIR;
	}

}
