package br.com.ggas.web.relatorio.clientesligadosprimeiravez;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.pontoConsumoPorData.PontoConsumoPorDataVO;

@Repository
public class RepositorioClientesLigadosPrimeiraVez extends RepositorioGenerico{

	@Autowired
	public RepositorioClientesLigadosPrimeiraVez (SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	public Collection<ClientesLigadosPrimeiraVezVO> obterHistoricoOperacaoMedidorClientesLigadosPrimeiraVez(Date dataInicio, Date dataFinal){
		
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT "
		        + "pontoConsumo.POCN_CD_LEGADO codigoUnico, "
		        + "cliente.CLIE_NM nomeCliente, " 
		        + "pontoConsumo.POCN_DS descricaoPontoConsumo, "
		        + "cep.CEP_DS_TIPO_LOGRADOURO tipoLogradouro, "
		        + "cep.CEP_NM_LOGRADOURO logradouro, "
		        + "cep.CEP_NM_BAIRRO bairro, "
		        + "telefones.numerosTelefone, "
		        + "cliente.CLIE_DS_EMAIL_PRINCIPAL email, " 
		        + "pontoConsumoSituacao.POCS_DS situacaoPontoConsumo, "
		        + "segmento.SEGM_DS segmento, " 
		        + "cep.CEP_NR cep, "
		        + "pontoConsumo.POCN_NR_IMOVEL numeroImovel, " 
		        + "instalacaoMedidor.MEIN_DT_ATIVACAO dataAtivacao, "
		        + "modelo.MEMD_DS medidorModelo, " 
		        + "medidor.MEDI_NR_SERIE numeroSerieMedidor, " 
		        + "contratoPontoConsumo.COPC_MD_PRESSAO pressaoEntrega");
		hql.append(" FROM ");
		hql.append(" PONTO_CONSUMO pontoConsumo ");
		hql.append(" INNER JOIN IMOVEL imovel ON imovel.IMOV_CD = pontoConsumo.IMOV_CD ");
		hql.append(" INNER JOIN CLIENTE_IMOVEL clienteImovel ON clienteImovel.IMOV_CD = imovel.IMOV_CD ");
		hql.append(" INNER JOIN CLIENTE cliente ON cliente.CLIE_CD = clienteImovel.CLIE_CD ");
		hql.append(" INNER JOIN MEDIDOR_INSTALACAO instalacaoMedidor ON instalacaoMedidor.MEIN_CD = pontoConsumo.MEIN_CD ");
		hql.append(" INNER JOIN MEDIDOR medidor ON medidor.MEDI_CD = instalacaoMedidor.MEDI_CD ");
		hql.append(" INNER JOIN SEGMENTO segmento ON segmento.SEGM_CD = pontoConsumo.SEGM_CD ");
		hql.append(" INNER JOIN CEP cep ON cep.CEP_CD = pontoConsumo.CEP_CD ");
		hql.append(" INNER JOIN PONTO_CONSUMO_SITUACAO pontoConsumoSituacao ON pontoConsumoSituacao.POCS_CD = pontoConsumo.POCS_CD ");
		hql.append(" INNER JOIN MEDIDOR_MODELO modelo ON modelo.MEMD_CD = medidor.MEMD_CD ");
		hql.append(" INNER JOIN CONTRATO_PONTO_CONSUMO contratoPontoConsumo ON contratoPontoConsumo.POCN_CD = pontoConsumo.POCN_CD ");

		hql.append(" LEFT JOIN (");
		hql.append("   SELECT cliente.CLIE_CD, LISTAGG(clienteFone.CLFO_CD_DDD || clienteFone.CLFO_NR, ', ') WITHIN GROUP (ORDER BY clienteFone.CLFO_CD_DDD) as numerosTelefone ");
		hql.append("   FROM CLIENTE_FONE clienteFone ");
		hql.append("   INNER JOIN CLIENTE cliente ON clienteFone.CLIE_CD = cliente.CLIE_CD ");
		hql.append("   GROUP BY cliente.CLIE_CD ");
		hql.append(" ) telefones ON telefones.CLIE_CD = cliente.CLIE_CD ");

		hql.append(" WHERE instalacaoMedidor.MEIN_DT_ATIVACAO BETWEEN :dataInicio AND :dataFinal ");
		hql.append(" AND pontoConsumo.POCN_IN_USO = 1 ");
		hql.append(" GROUP BY " + 
		        "pontoConsumo.POCN_CD_LEGADO, " +
		        "cliente.CLIE_NM, " + 
		        "pontoConsumo.POCN_DS, " + 
		        "cep.CEP_DS_TIPO_LOGRADOURO, " + 
		        "cep.CEP_NM_LOGRADOURO, " + 
		        "cep.CEP_NM_BAIRRO, " +
		        "telefones.numerosTelefone, " + 
		        "cliente.CLIE_DS_EMAIL_PRINCIPAL, " + 
		        "pontoConsumoSituacao.POCS_DS, " + 
		        "segmento.SEGM_DS, " + 
		        "cep.CEP_NR, " + 
		        "pontoConsumo.POCN_NR_IMOVEL, " + 
		        "instalacaoMedidor.MEIN_DT_ATIVACAO, " + 
		        "modelo.MEMD_DS, " + 
		        "medidor.MEDI_NR_SERIE, " + 
		        "contratoPontoConsumo.COPC_MD_PRESSAO");
		hql.append(" ORDER BY cliente.CLIE_NM ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		query.setParameter("dataInicio", dataInicio);
		query.setParameter("dataFinal", dataFinal);
		
		Collection<Object> resultado = query.list();
		Collection<ClientesLigadosPrimeiraVezVO> clientesLigadosPrimeiraVez = new ArrayList<ClientesLigadosPrimeiraVezVO>();
		for (Object retorno : resultado) {
			
			ClientesLigadosPrimeiraVezVO clientesLigadosPrimeiraVezVO = new ClientesLigadosPrimeiraVezVO();
			
			Object[] linha = (Object[]) retorno;
			
			String codigoUnico = String.valueOf(linha[0]);
			String nomeCliente = String.valueOf(linha[1]);
			String descricaoPontoConsumo = String.valueOf(linha[2]);
			String tipoLogradouro = String.valueOf(linha[3]);
			String logradouro = String.valueOf(linha[4]);
//			String dddNumeroTelefone = String.valueOf(linha[5]);
//			String numeroTelefone = String.valueOf(linha[6]);
			String bairro = String.valueOf(linha[5]);
			String numeroTelefone = String.valueOf(linha[6]);
			String email = String.valueOf(linha[7]);
			String situacaoPontoConsumo = String.valueOf(linha[8]);
			String segmento = String.valueOf(linha[9]);
			String cep = String.valueOf(linha[10]);
			String numeroImovel = String.valueOf(linha[11]);
			Date dataAtivacao = (Date) linha[12];
			String medidorModelo = String.valueOf(linha[13]);
			String numeroSerieMedidor = String.valueOf(linha[14]);
			String pressaoEntrega = String.valueOf(linha[15]);
			
			clientesLigadosPrimeiraVezVO.setCodigoUnico(codigoUnico);
			clientesLigadosPrimeiraVezVO.setNomeCliente(nomeCliente);
			clientesLigadosPrimeiraVezVO.setDescricaoPontoConsumo(descricaoPontoConsumo);
			clientesLigadosPrimeiraVezVO.setTipoLogradouro(tipoLogradouro);
			clientesLigadosPrimeiraVezVO.setLogradouro(logradouro);
			clientesLigadosPrimeiraVezVO.setBairro(bairro);
//			clientesLigadosPrimeiraVezVO.setDddNumeroTelefone(dddNumeroTelefone);
			clientesLigadosPrimeiraVezVO.setNumeroTelefone(numeroTelefone);
			clientesLigadosPrimeiraVezVO.setEmail(email);
			clientesLigadosPrimeiraVezVO.setSituacaoPontoConsumo(situacaoPontoConsumo);
			clientesLigadosPrimeiraVezVO.setSegmento(segmento);
			clientesLigadosPrimeiraVezVO.setCep(cep);
			clientesLigadosPrimeiraVezVO.setNumeroImovel(numeroImovel);
			clientesLigadosPrimeiraVezVO.setDataAtivacao(dataAtivacao);
			clientesLigadosPrimeiraVezVO.setMedidorModelo(medidorModelo);
			clientesLigadosPrimeiraVezVO.setNumeroSerieMedidor(numeroSerieMedidor);
			clientesLigadosPrimeiraVezVO.setPressaoEntrega(pressaoEntrega);
			clientesLigadosPrimeiraVezVO.setDataFormatada(dataAtivacao);
			
			clientesLigadosPrimeiraVez.add(clientesLigadosPrimeiraVezVO);
		}

		return clientesLigadosPrimeiraVez;
	}
	
	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadeClienteTelefone(){
		return ServiceLocator.getInstancia().getClassPorID(ClienteFone.BEAN_ID_CLIENTE_FONE);
	}

}
