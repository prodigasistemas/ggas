package br.com.ggas.web.relatorio.prazoParaEntregaFatura;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoEntregaFatura;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

@Controller
@SessionAttributes({ "FaturaImpl", "FaturaVO" })
public class RelatorioPrazoMinimoEntregaFaturaAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioPrazoMinimoEntregaFaturaAction.class);

	@Autowired
	private ControladorRelatorioPrazoMinimoEntregaFatura controladorRelatorioPrazoMinimoEntregaFatura;

	/**
	 * Método responsavel por exibir à tela.
	 * 
	 * @param mapping the mapping
	 * @param form the form
	 * @param request the request
	 * @param response the response
	 * @return the action forward
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirPesquisaPrazoMinimoEntregaFatura")
	public ModelAndView exibirTela() {

		return new ModelAndView("exibirPesquisaPrazoMinimoEntregaFatura");
	}

	/**
	 * 
	 * @param dataIncial {@link String}
	 * @param dataFinal {@link String}
	 * @param response {@link HttpServletResponse}
	 * @param tipoRelatorio {@link String}
	 * @param exibirFiltros {@link String}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 * @throws ParseException  {@link ParseException}
	 */
	
	@RequestMapping("gerarRelatorioPrazoMinimoEntregaFatura")
	public ModelAndView gerarRelatorioDataLimiteReligamentoGas(@RequestParam("dataInicio") String dataIncial,
					@RequestParam("dataFinal") String dataFinal, HttpServletResponse response, @RequestParam("tipoRelatorio") String tipoRelatorio,
					@RequestParam("exibirFiltros") String exibirFiltros)
					throws GGASException, ParseException {


		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaPrazoMinimoEntregaFatura");
		
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());
			
			DateFormat formatador = new SimpleDateFormat("dd/MM/yy");
			Date inicio = null;
			
			if("".equals(dataIncial)){
				inicio = null;
			}else{
				inicio = formatador.parse(dataIncial);
			}
			Date fim = null;
			
			if("".equals(dataFinal)){
				fim = null;
			}else{
				fim = formatador.parse(dataFinal);
			}

			relatorio = controladorRelatorioPrazoMinimoEntregaFatura.gerarRelatorio(inicio, fim, exibirFiltros, tipoRelatorio);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=RelatórioDataLimiteReligamentoGas_" + diaMesAno + "_" + hora
							+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemAlerta(model, e.getMessage() );
		} catch (NegocioException e) {
			LOG.debug(e.getStackTrace(), e);
			mensagemAlerta(model, e.getMessage() );
		}
		return model;
	}
}
