/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.clienteunidadeconsumidora;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.impl.ModalidadeMedicaoImovelImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioClienteUnidadeConsumidora;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioCadastroVO;

@Controller
public class RelatorioClienteUnidadeConsumidoraAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioClienteUnidadeConsumidoraAction.class);

	private static final String ID_IMOVEL = "idImovel";
	
	private static final String ID_MODALIDADE_MEDICAO = "idModalidadeMedicao";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	public static final String FORMATO_IMPRESSAO = "formatoImpressao";
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
	/**
	 * Classe controladora responsável por gerenciar os eventos e acionar as classes
	 * e seus respectivos metodos relacionados as regras de negócio e de modelo
	 * realizando alterações nas informações das telas referentes a funcionalidade
	 * Número de Clientes e Número de Unidades Consumidoras.
	 */
	@Autowired
	private ControladorRelatorioClienteUnidadeConsumidora controladorRelatorioClienteUnidadeConsumidora;

	/**
	 * Exibir relatorio cliente unidade consumidora.
	 * 
	 * @param model - {@link Model}
	 * @return exibirRelatorioClienteUnidadeConsumidora - {@link String}
	 */
	@RequestMapping("exibirRelatorioClienteUnidadeConsumidora")
	public String exibirRelatorioClienteUnidadeConsumidora(Model model) {

		model.addAttribute("listaModalidadeMedicao", ModalidadeMedicaoImovelImpl.listar());

		return "exibirRelatorioClienteUnidadeConsumidora";
	}

	/**
	 * Gerar relatorio cliente unidade consumidora.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirRelatorioClienteUnidadeConsumidora - {@link String}
	 * @throws IOException - {@link IOException}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioClienteUnidadeConsumidora")
	public String gerarRelatorioClienteUnidadeConsumidora(RelatorioCadastroVO relatorioCadastroVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws IOException, GGASException {

		Map<String, Object> filtro = popularFiltroRelatorio(relatorioCadastroVO);
		String view = null;

		try {

			byte[] relatorio = controladorRelatorioClienteUnidadeConsumidora.consultar(filtro);

			if (relatorio != null) {
				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				FormatoImpressao formatoImpressao = FormatoImpressao.valueOf((String) filtro.get(FORMATO_IMPRESSAO));

				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioDeNumeroClientesNumeroUC" + this.obterFormatoRelatorio(formatoImpressao));
				response.getOutputStream().write(relatorio);
			}

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = exibirRelatorioClienteUnidadeConsumidora(model);
		}

		model.addAttribute("relatorioCadastroVO", relatorioCadastroVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioCadastroVO - {@link RelatorioCadastroVO}
	 * @return filtroRelatorio - {@link Map}
	 */
	private Map<String, Object> popularFiltroRelatorio(RelatorioCadastroVO relatorioCadastroVO) {

		Map<String, Object> filtroRelatorio = new HashMap<String, Object>();

		String formatoImpressao = relatorioCadastroVO.getFormatoImpressao();
		Boolean exibirFiltro = relatorioCadastroVO.getExibirFiltros();
		Long idImovel = relatorioCadastroVO.getIdImovel();
		Long idModalidadeMedicao = relatorioCadastroVO.getIdModalidadeMedicao();

		if (StringUtils.isNotEmpty(formatoImpressao)) {
			filtroRelatorio.put(FORMATO_IMPRESSAO, formatoImpressao);
		}
		
		filtroRelatorio.put(EXIBIR_FILTROS, exibirFiltro);
		
		if (idImovel != null && idImovel > 0) {
			filtroRelatorio.put(ID_IMOVEL, idImovel);
		}
		if (idModalidadeMedicao != null && idModalidadeMedicao > 0) {
			filtroRelatorio.put(ID_MODALIDADE_MEDICAO, idModalidadeMedicao.intValue());
		}

		return filtroRelatorio;
	}

}
