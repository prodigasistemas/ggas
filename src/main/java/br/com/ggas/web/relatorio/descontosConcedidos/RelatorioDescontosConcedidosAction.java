/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.descontosConcedidos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioDescontosConcedidos;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil.TipoGrafico;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Descontos Concedidos.
 */
@Controller
public class RelatorioDescontosConcedidosAction extends GenericAction {

	private static final String IMOVEL_PESQUISA_POPUP = "imovel";

	private static final String CLIENTE_PESQUISA_POPUP = "cliente";

	private static final String RELATORIO_FATURAMENTO_VO = "relatorioFaturamentoVO";

	public static final String DATA_INICIAL = "dataInicial";

	public static final String DATA_FINAL = "dataFinal";

	public static final String DATA_EMISSAO = "dataEmissao";

	public static final String IMOVEL = "idImovel";

	public static final String SEGMENTO = "idSegmento";

	public static final String CLIENTE = "idCliente";

	public static final String TIPO_EXIBICAO = "tipoExibicao";

	public static final String EXIBIR_FILTROS = "exibirFiltros";

	public static final String EXIBIR_GRAFICO = "exibirGrafico";

	public static final String TIPO_GRAFICO = "idTipoGrafico";

	public static final String FORMATO_IMPRESSAO = "formatoImpressao";

	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	public static final String RELATORIO = "relatorio";

	public static final String POSSUI_RELATORIO = "possuiRelatorio";

	public static final String LISTA_RUBRICAS = "listaRubricas";

	public static final String LISTA_RUBRICAS_ASSOCIADOS = "listaRubricasAssociadas";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String RUBRICAS_ASSOCIADAS = "rubricasAssociadas";

	private static final String NUMERO_NOTA = "numeroNota";

	private static final String NUMERO_CONTATO = "numeroContrato";

	private static final String ERRO_PERIODO_NAO_INFORMADO = "ERRO_PERIODO_NAO_INFORMADO";

	protected static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioDescontosConcedidos controladorRelatorioDescontosConcedidos;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	/**
	 * Exibir relatorio descontos concedidos.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return exibirRelatorioDescontosConcedidos - {@link String}
	 * @throws GGASException - {@link Model}
	 */
	@RequestMapping("exibirRelatorioDescontosConcedidos")
	public String exibirRelatorioDescontosConcedidos(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, BindingResult result, Model model)
			throws GGASException {

		try {
			this.carregarDados(relatorioFaturamentoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirRelatorioDescontosConcedidos";
	}
	
	/**
	 * Pesquisar ponto consumo.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirRelatorioDescontosConcedidos - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatoriosConcedidos")
	public String pesquisarPontoConsumo(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			pesquisarPontosConsumo(relatorioFaturamentoVO, clienteImovelPesquisaPopupVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(RELATORIO_FATURAMENTO_VO, relatorioFaturamentoVO);
		model.addAttribute(CLIENTE_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);
		model.addAttribute(IMOVEL_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);

		return exibirRelatorioDescontosConcedidos(relatorioFaturamentoVO, bindingResult, model);
	}
	
	/**
	 * Carregar dados.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute("listaSegmento", listaSegmento);

		model.addAttribute(LISTA_TIPO_GRAFICO, TipoGrafico.listar());

		ControladorRubrica controladorRubrica =
				(ControladorRubrica) ServiceLocator.getInstancia().getControladorNegocio(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);

		Long[] idrubricasAssociadas = relatorioFaturamentoVO.getRubricasAssociadas();
		Collection<Rubrica> listaRubricasAssociadas = new ArrayList<Rubrica>();

		if (idrubricasAssociadas != null && idrubricasAssociadas.length > 0) {
			filtro.put(ControladorRubrica.CHAVES_PRIMARIAS, idrubricasAssociadas);
			listaRubricasAssociadas = controladorRubrica.consultarRubrica(filtro);
		}

		model.addAttribute(LISTA_RUBRICAS_ASSOCIADOS, listaRubricasAssociadas);

		Collection<Rubrica> listaRubricas = controladorRubrica.listarRubricas();
		listaRubricas.removeAll(listaRubricasAssociadas);
		model.addAttribute(LISTA_RUBRICAS, listaRubricas);

	}

	/**
	 * Gerar relatorio descontos concedidos.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioDescontosConcedidos")
	public String gerarRelatorioDescontosConcedidos(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		try {

			Map<String, Object> filtro = this.prepararFiltro(relatorioFaturamentoVO);
			this.validarFiltro(relatorioFaturamentoVO);

			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioFaturamentoVO.getFormatoImpressao());

			byte[] relatorio = controladorRelatorioDescontosConcedidos.gerarRelatorio(filtro, formatoImpressao);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));

			if (relatorio != null) {
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioDescontosConcedidos" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);
				model.addAttribute(POSSUI_RELATORIO, Boolean.TRUE);
				request.getSession().setAttribute(RELATORIO, relatorio);
			}

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioDescontosConcedidos(relatorioFaturamentoVO, bindingResult, model);
		}

		model.addAttribute(RELATORIO_FATURAMENTO_VO, relatorioFaturamentoVO);
		model.addAttribute(CLIENTE_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);
		model.addAttribute(IMOVEL_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);

		return view;
	}

	/**
	 * Pesquisar pontos consumo.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void pesquisarPontosConsumo(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, Model model) throws GGASException {

		Long idImovel = relatorioFaturamentoVO.getIdImovel();
		Long idCliente = relatorioFaturamentoVO.getIdCliente();
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}

		model.addAttribute(RELATORIO_FATURAMENTO_VO, relatorioFaturamentoVO);
		model.addAttribute(CLIENTE_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);
		model.addAttribute(IMOVEL_PESQUISA_POPUP, clienteImovelPesquisaPopupVO);
		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}


	/**
	 * Validar filtro.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @throws GGASException - {@link GGASException}
	 */
	public void validarFiltro(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO) throws GGASException {

		String dataInicial = relatorioFaturamentoVO.getDataInicial();
		String dataFinal = relatorioFaturamentoVO.getDataFinal();

		if (StringUtils.isEmpty(dataInicial) || StringUtils.isEmpty(dataFinal)) {
			throw new NegocioException(ERRO_PERIODO_NAO_INFORMADO, true);
		}

	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String dataInicial = relatorioFaturamentoVO.getDataInicial();
		String dataFinal = relatorioFaturamentoVO.getDataFinal();
		String dataEmissao = relatorioFaturamentoVO.getDataEmissao();
		String tipoExibicao = relatorioFaturamentoVO.getTipoExibicao();
		String tipoGrafico = relatorioFaturamentoVO.getIdTipoGrafico();
		String numeroNota = relatorioFaturamentoVO.getNumeroNota();
		String numeroContrato = relatorioFaturamentoVO.getNumeroContrato();
		Boolean exibirFiltros = relatorioFaturamentoVO.isExibirFiltros();
		Long idImovel = relatorioFaturamentoVO.getIdImovel();

		filtro.put(EXIBIR_FILTROS, exibirFiltros);

		if (StringUtils.isNotEmpty(dataInicial)) {
			filtro.put(DATA_INICIAL, Util.converterCampoStringParaData("Data Inicial", dataInicial, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataFinal)) {
			filtro.put(DATA_FINAL, Util.converterCampoStringParaData("Data Final", dataFinal, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataEmissao)) {
			filtro.put(DATA_FINAL, Util.converterCampoStringParaData("Data Emissão", dataEmissao, Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isNotEmpty(tipoExibicao)) {
			filtro.put(TIPO_EXIBICAO, tipoExibicao);
		}

		if (StringUtils.isNotEmpty(tipoGrafico)) {
			filtro.put(TIPO_GRAFICO, tipoGrafico);
		}

		if (StringUtils.isNotEmpty(numeroNota)) {
			filtro.put(NUMERO_NOTA, numeroNota);
		}

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTATO, numeroContrato);
		}

		if (idImovel != null && idImovel > 0) {
			filtro.put(IMOVEL, idImovel);
		}

		prepararFiltroDois(relatorioFaturamentoVO, filtro);

		return filtro;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param filtro - {@link Map}
	 */
	private void prepararFiltroDois(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, Map<String, Object> filtro) {

		Boolean exibirGrafico = relatorioFaturamentoVO.getExibirGrafico();
		Long idCliente = relatorioFaturamentoVO.getIdCliente();
		Long idSegmento = relatorioFaturamentoVO.getIdSegmento();
		Long idPontoConsumo = relatorioFaturamentoVO.getIdPontoConsumo();
		Long[] idRubricasAssociadas = relatorioFaturamentoVO.getRubricasAssociadas();

		if (idCliente != null && idCliente > 0) {
			filtro.put(CLIENTE, idCliente);
		}

		if (idSegmento != null && idSegmento > 0) {
			filtro.put(SEGMENTO, idSegmento);
		}

		if (exibirGrafico != null) {
			filtro.put(EXIBIR_GRAFICO, exibirGrafico);
		}

		if (idRubricasAssociadas != null && idRubricasAssociadas.length > 0) {
			filtro.put(RUBRICAS_ASSOCIADAS, idRubricasAssociadas);
		}

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			filtro.put(ID_PONTO_CONSUMO, idPontoConsumo);
		}
	}
	
}
