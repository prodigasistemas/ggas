
package br.com.ggas.web.relatorio.titulosatraso;

import java.util.List;
/**
 * 
 * Classe responsável pela representação da entidade TitulosAtrasado
 *
 */
public class TitulosAtrasadoV0 {

	private Long idFantasiaImovel;

	private String nomeFantasiaImovel;

	private String nomeCliente;

	private List<TitulosAtrasadoDetalhadosV0> titulosAtrasados;

	private Integer quantidadeDocumentos;

	public List<TitulosAtrasadoDetalhadosV0> getTitulosAtrasados() {

		return titulosAtrasados;
	}

	public void setTitulosAtrasados(List<TitulosAtrasadoDetalhadosV0> titulosAtrasados) {

		this.titulosAtrasados = titulosAtrasados;
	}

	public Long getIdFantasiaImovel() {

		return idFantasiaImovel;
	}

	public void setIdFantasiaImovel(Long idFantasiaImovel) {

		this.idFantasiaImovel = idFantasiaImovel;
	}

	public Integer getQuantidadeDocumentos() {

		return quantidadeDocumentos;
	}

	public void setQuantidadeDocumentos(Integer quantidadeDocumentos) {

		this.quantidadeDocumentos = quantidadeDocumentos;
	}

	public String getNomeFantasiaImovel() {

		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {

		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		
		if (idFantasiaImovel == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + idFantasiaImovel.hashCode();
		}

		if (nomeCliente == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + nomeCliente.hashCode();
		}
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		TitulosAtrasadoV0 other = (TitulosAtrasadoV0) obj;
		if(idFantasiaImovel == null) {
			if(other.idFantasiaImovel != null) {
				return false;
			}
		} else if(!idFantasiaImovel.equals(other.idFantasiaImovel)) {
			return false;
		}
		if(nomeCliente == null) {
			if(other.nomeCliente != null) {
				return false;
			}
		} else if(!nomeCliente.equals(other.nomeCliente)) {
			return false;
		}
		return true;
	}

}
