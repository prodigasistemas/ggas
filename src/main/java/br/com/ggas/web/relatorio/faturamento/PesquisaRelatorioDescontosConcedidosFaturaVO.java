/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/10/2014 08:21:46
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

import java.util.List;

import br.com.ggas.cadastro.imovel.Segmento;

/**
 * @author vpessoa
 */
public class PesquisaRelatorioDescontosConcedidosFaturaVO {

	private Long idCliente;

	private Long idImovel;

	private Long numeroFaturaInicial;

	private Long numeroFaturaFinal;

	private Long numeroNFInicial;

	private Long numeroNFFinal;

	private Long numeroContrato;

	private String dataEmissaoInicial;

	private String dataEmissaoFinal;

	private String vencimentoInicial;

	private String vencimentoFinal;

	private Long idPontoConsumo;

	private List<Segmento> segmentos;

	private String indicadorPesquisa;

	private String nomeCliente;

	private String documentoCliente;

	private String emailCliente;

	private String enderecoCliente;

	private String nomeFantasiaImovel;

	private String matriculaImovel;

	private String numeroImovel;

	private String cidadeImovel;

	private String condominio;

	private Long idGrupoFaturamento;

	public Long getIdCliente() {

		return idCliente;
	}

	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

	public Long getIdImovel() {

		return idImovel;
	}

	public void setIdImovel(Long idImovel) {

		this.idImovel = idImovel;
	}

	public Long getNumeroFaturaInicial() {

		return numeroFaturaInicial;
	}

	public void setNumeroFaturaInicial(Long numeroFaturaInicial) {

		this.numeroFaturaInicial = numeroFaturaInicial;
	}

	public Long getNumeroFaturaFinal() {

		return numeroFaturaFinal;
	}

	public void setNumeroFaturaFinal(Long numeroFaturaFinal) {

		this.numeroFaturaFinal = numeroFaturaFinal;
	}

	public Long getNumeroNFInicial() {

		return numeroNFInicial;
	}

	public void setNumeroNFInicial(Long numeroNFInicial) {

		this.numeroNFInicial = numeroNFInicial;
	}

	public Long getNumeroNFFinal() {

		return numeroNFFinal;
	}

	public void setNumeroNFFinal(Long numeroNFFinal) {

		this.numeroNFFinal = numeroNFFinal;
	}

	public Long getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(Long numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getDataEmissaoInicial() {

		return dataEmissaoInicial;
	}

	public void setDataEmissaoInicial(String dataEmissaoInicial) {

		this.dataEmissaoInicial = dataEmissaoInicial;
	}

	public String getDataEmissaoFinal() {

		return dataEmissaoFinal;
	}

	public void setDataEmissaoFinal(String dataEmissaoFinal) {

		this.dataEmissaoFinal = dataEmissaoFinal;
	}

	public String getVencimentoInicial() {

		return vencimentoInicial;
	}

	public void setVencimentoInicial(String vencimentoInicial) {

		this.vencimentoInicial = vencimentoInicial;
	}

	public String getVencimentoFinal() {

		return vencimentoFinal;
	}

	public void setVencimentoFinal(String vencimentoFinal) {

		this.vencimentoFinal = vencimentoFinal;
	}

	public Long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	public List<Segmento> getSegmentos() {

		return segmentos;
	}

	public void setSegmentos(List<Segmento> segmentos) {

		this.segmentos = segmentos;
	}

	public String getIndicadorPesquisa() {

		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {

		this.indicadorPesquisa = indicadorPesquisa;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getDocumentoCliente() {

		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {

		this.documentoCliente = documentoCliente;
	}

	public String getEmailCliente() {

		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {

		this.emailCliente = emailCliente;
	}

	public String getEnderecoCliente() {

		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente) {

		this.enderecoCliente = enderecoCliente;
	}

	public String getNomeFantasiaImovel() {

		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {

		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getMatriculaImovel() {

		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getCidadeImovel() {

		return cidadeImovel;
	}

	public void setCidadeImovel(String cidadeImovel) {

		this.cidadeImovel = cidadeImovel;
	}

	public String getCondominio() {

		return condominio;
	}

	public void setCondominio(String condominio) {

		this.condominio = condominio;
	}

	public Long getIdGrupoFaturamento() {

		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {

		this.idGrupoFaturamento = idGrupoFaturamento;
	}

}
