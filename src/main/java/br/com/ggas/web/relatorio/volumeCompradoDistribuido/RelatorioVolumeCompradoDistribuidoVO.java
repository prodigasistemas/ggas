/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.volumeCompradoDistribuido;

import java.math.BigDecimal;
import java.util.Date;

public class RelatorioVolumeCompradoDistribuidoVO {

	private Date data;

	private Double volumeDistribuidora;

	private Double volumeSupridora;

	private Double comprado;

	private Double m3;

	private Double percent;

	private BigDecimal comercialMedido;

	private BigDecimal comercialFaturado;

	private BigDecimal veicularMedido;

	private BigDecimal veicularFaturado;

	private BigDecimal industrialMedido;

	private BigDecimal industrialFaturado;

	private BigDecimal residencialMedido;

	private BigDecimal residencialFaturado;

	private BigDecimal publicoMedido;

	private BigDecimal publicoFaturado;

	private BigDecimal cogeracaoMedido;

	private BigDecimal cogeracaoFaturado;

	private BigDecimal totalMedido;

	private BigDecimal totalFaturado;

	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	public void setData(Date data) {

		this.data = data;
	}

	public BigDecimal getComercialMedido() {

		return comercialMedido;
	}

	public void setComercialMedido(BigDecimal comercialMedido) {

		this.comercialMedido = comercialMedido;
	}

	public BigDecimal getComercialFaturado() {

		return comercialFaturado;
	}

	public void setComercialFaturado(BigDecimal comercialFaturado) {

		this.comercialFaturado = comercialFaturado;
	}

	public BigDecimal getVeicularMedido() {

		return veicularMedido;
	}

	public void setVeicularMedido(BigDecimal veicularMedido) {

		this.veicularMedido = veicularMedido;
	}

	public BigDecimal getVeicularFaturado() {

		return veicularFaturado;
	}

	public void setVeicularFaturado(BigDecimal veicularFaturado) {

		this.veicularFaturado = veicularFaturado;
	}

	public BigDecimal getIndustrialMedido() {

		return industrialMedido;
	}

	public void setIndustrialMedido(BigDecimal industrialMedido) {

		this.industrialMedido = industrialMedido;
	}

	public BigDecimal getIndustrialFaturado() {

		return industrialFaturado;
	}

	public void setIndustrialFaturado(BigDecimal industrialFaturado) {

		this.industrialFaturado = industrialFaturado;
	}

	public BigDecimal getResidencialMedido() {

		return residencialMedido;
	}

	public void setResidencialMedido(BigDecimal residencialMedido) {

		this.residencialMedido = residencialMedido;
	}

	public BigDecimal getResidencialFaturado() {

		return residencialFaturado;
	}

	public void setResidencialFaturado(BigDecimal residencialFaturado) {

		this.residencialFaturado = residencialFaturado;
	}

	public BigDecimal getPublicoMedido() {

		return publicoMedido;
	}

	public void setPublicoMedido(BigDecimal publicoMedido) {

		this.publicoMedido = publicoMedido;
	}

	public BigDecimal getPublicoFaturado() {

		return publicoFaturado;
	}

	public void setPublicoFaturado(BigDecimal publicoFaturado) {

		this.publicoFaturado = publicoFaturado;
	}

	public BigDecimal getCogeracaoMedido() {

		return cogeracaoMedido;
	}

	public void setCogeracaoMedido(BigDecimal cogeracaoMedido) {

		this.cogeracaoMedido = cogeracaoMedido;
	}

	public BigDecimal getCogeracaoFaturado() {

		return cogeracaoFaturado;
	}

	public void setCogeracaoFaturado(BigDecimal cogeracaoFaturado) {

		this.cogeracaoFaturado = cogeracaoFaturado;
	}

	public BigDecimal getTotalMedido() {

		return totalMedido;
	}

	public void setTotalMedido(BigDecimal totalMedido) {

		this.totalMedido = totalMedido;
	}

	public BigDecimal getTotalFaturado() {

		return totalFaturado;
	}

	public void setTotalFaturado(BigDecimal totalFaturado) {

		this.totalFaturado = totalFaturado;
	}

	public void setVolumeDistribuidora(Double volumeDistribuidora) {

		this.volumeDistribuidora = volumeDistribuidora;
	}

	public Double getVolumeDistribuidora() {

		return volumeDistribuidora;
	}

	public void setVolumeSupridora(Double volumeSupridora) {

		this.volumeSupridora = volumeSupridora;
	}

	public Double getVolumeSupridora() {

		return volumeSupridora;
	}

	public void setComprado(Double comprado) {

		this.comprado = comprado;
	}

	public Double getComprado() {

		return comprado;
	}

	public void setM3(Double m3) {

		this.m3 = m3;
	}

	public Double getM3() {

		return m3;
	}

	public void setPercent(Double percent) {

		this.percent = percent;
	}

	public Double getPercent() {

		return percent;
	}
}
