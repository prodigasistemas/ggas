/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 29/08/2014 15:28:55
 @author rfilho
 */

package br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SubRelatorioCobrancaRecuperacaoPenalidadesTopVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -362168792830631543L;

	private Date dataInicio;

	private Date dataFim;

	private BigDecimal consumoContratado;

	private BigDecimal consumoRealizado;

	private BigDecimal consumoMinimo;

	private BigDecimal consumoMaximo;

	private BigDecimal takeOrPay;

	private BigDecimal volTopPercentualCobranca;

	private BigDecimal penalidadeTop;

	private BigDecimal volMaior;

	private BigDecimal volMaiorPercentualCobranca;

	private BigDecimal penalidadeMaiorPcm;

	private String cobrado;

	private String quitado;

	private BigDecimal tarifaMedia;

	private String notaDebito;

	private String emissaoND;

	private String vencimentoND;

	public Date getDataInicio() {
		Date data = null;
		if (this.dataInicio != null) {
			data = (Date) dataInicio.clone();
		}
		return data;
	}

	public void setDataInicio(Date dataInicio) {

		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		Date data = null;
		if (this.dataFim != null) {
			data = (Date) dataFim.clone();
		}
		return data;
	}

	public void setDataFim(Date dataFim) {

		this.dataFim = dataFim;
	}

	public BigDecimal getConsumoContratado() {

		return consumoContratado;
	}

	public void setConsumoContratado(BigDecimal consumoContratado) {

		this.consumoContratado = consumoContratado;
	}

	public BigDecimal getConsumoRealizado() {

		return consumoRealizado;
	}

	public void setConsumoRealizado(BigDecimal consumoRealizado) {

		this.consumoRealizado = consumoRealizado;
	}

	public BigDecimal getConsumoMinimo() {

		return consumoMinimo;
	}

	public void setConsumoMinimo(BigDecimal consumoMinimo) {

		this.consumoMinimo = consumoMinimo;
	}

	public BigDecimal getConsumoMaximo() {

		return consumoMaximo;
	}

	public void setConsumoMaximo(BigDecimal consumoMaximo) {

		this.consumoMaximo = consumoMaximo;
	}

	public BigDecimal getTakeOrPay() {

		return takeOrPay;
	}

	public void setTakeOrPay(BigDecimal takeOrPay) {

		this.takeOrPay = takeOrPay;
	}

	public BigDecimal getVolTopPercentualCobranca() {

		return volTopPercentualCobranca;
	}

	public void setVolTopPercentualCobranca(BigDecimal volTopPercentualCobranca) {

		this.volTopPercentualCobranca = volTopPercentualCobranca;
	}

	public BigDecimal getPenalidadeTop() {

		return penalidadeTop;
	}

	public void setPenalidadeTop(BigDecimal penalidadeTop) {

		this.penalidadeTop = penalidadeTop;
	}

	public BigDecimal getVolMaior() {

		return volMaior;
	}

	public void setVolMaior(BigDecimal volMaior) {

		this.volMaior = volMaior;
	}

	public BigDecimal getVolMaiorPercentualCobranca() {

		return volMaiorPercentualCobranca;
	}

	public void setVolMaiorPercentualCobranca(BigDecimal volMaiorPercentualCobranca) {

		this.volMaiorPercentualCobranca = volMaiorPercentualCobranca;
	}

	public BigDecimal getPenalidadeMaiorPcm() {

		return penalidadeMaiorPcm;
	}

	public void setPenalidadeMaiorPcm(BigDecimal penalidadeMaiorPcm) {

		this.penalidadeMaiorPcm = penalidadeMaiorPcm;
	}

	public String getCobrado() {

		return cobrado;
	}

	public void setCobrado(String cobrado) {

		this.cobrado = cobrado;
	}

	public BigDecimal getTarifaMedia() {

		return tarifaMedia;
	}

	public void setTarifaMedia(BigDecimal tarifaMedia) {

		this.tarifaMedia = tarifaMedia;
	}

	public String getNotaDebito() {

		return notaDebito;
	}

	public void setNotaDebito(String notaDebito) {

		this.notaDebito = notaDebito;
	}

	public String getEmissaoND() {

		return emissaoND;
	}

	public void setEmissaoND(String emissaoND) {

		this.emissaoND = emissaoND;
	}

	public String getQuitado() {

		return quitado;
	}

	public void setQuitado(String quitado) {

		this.quitado = quitado;
	}

	public String getVencimentoND() {

		return vencimentoND;
	}

	public void setVencimentoND(String vencimentoND) {

		this.vencimentoND = vencimentoND;
	}

}
