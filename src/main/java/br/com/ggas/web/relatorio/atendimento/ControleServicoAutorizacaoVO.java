/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe referentes as chamadas da classe ControleServicoAutorizacaoVO
 * 
 @since 14/10/2014 09:33:43
 @author vpessoa
 */

package br.com.ggas.web.relatorio.atendimento;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Classe referentes as chamadas da classe ControleServicoAutorizacaoVO
 * 
 * @author vpessoa
 */
public class ControleServicoAutorizacaoVO {

	private String numeroContrato;

	private String empresaContratada;

	private String nomeCliente;

	private String segmento;

	private String numeroServicoAutorizacao;

	private Date dataEmissao;

	private BigDecimal valor;

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getEmpresaContratada() {

		return empresaContratada;
	}

	public void setEmpresaContratada(String empresaContratada) {

		this.empresaContratada = empresaContratada;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getNumeroServicoAutorizacao() {

		return numeroServicoAutorizacao;
	}

	public void setNumeroServicoAutorizacao(String numeroServicoAutorizacao) {

		this.numeroServicoAutorizacao = numeroServicoAutorizacao;
	}

	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public BigDecimal getValor() {

		return valor;
	}

	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

}
