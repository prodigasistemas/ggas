/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.prazominimoantecedencia;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoAntecedencia;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioCobrancaVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referente as funcionalidades
 * Prazo Mínimo de Antecedência para Envio de Notificação de Corte e
 * Prazo Mínimo de Antecedência para Envio do Aviso de Corte.
 */
@Controller
public class RelatorioPrazoMinimoAntecedencia extends GenericAction {

	private static final String MES_ANO = "mesAno";

	private static final String RELATORIO = "relatorio";

	private static final String EXIBIR_FILTRO = "exibirFiltros";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_RELATORIO = "exibirRelatorio";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorRelatorioPrazoMinimoAntecedencia controladorRelatorioPrazoMinimoAntecedencia;
	
	/**
	 * Exibir pesquisa relatorio prazo minimo notificacao corte.
	 * 
	 * @return exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte - {@link String}
	 */
	@RequestMapping("exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte")
	public String exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte(){
		
		return "exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte";
		
	}

	/**
	 * Gerar relatorio prazo minimo notificacao corte.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioPrazoMinimoNotificacaoCorte")
	public String gerarRelatorioPrazoMinimoNotificacaoCorte(RelatorioCobrancaVO relatorioCobrancaVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltroRelatorio(relatorioCobrancaVO);

		try {
			validarMesAno((String) filtro.get(MES_ANO));
			
			filtro.put("nomeResponsavel", getDadosAuditoria(request).getUsuario().getFuncionario().getNome());

			byte[] relatorio = controladorRelatorioPrazoMinimoAntecedencia.gerarRelatorioNotificacaoCorte(filtro);

			if (relatorio != null) {

				request.getSession().setAttribute(RELATORIO, relatorio);
				request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf((String) filtro.get(FORMATO_IMPRESSAO)));
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);

			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaRelatorioPrazoMinimoNotificacaoCorte();

	}

	/**
	 * Exibir relatorio prazo minimo notificacao corte.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioPrazoMinimoNotificacaoCorte")
	public String exibirRelatorioPrazoMinimoNotificacaoCorte(HttpServletRequest request, HttpServletResponse response, Model model)
			throws GGASException {

		byte[] relatorio = (byte[]) request.getSession().getAttribute(RELATORIO);
		FormatoImpressao formatoImpressao = (FormatoImpressao) request.getSession().getAttribute(FORMATO_IMPRESSAO);

		request.getSession().removeAttribute(RELATORIO);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		if (relatorio != null) {
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			try {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioPrazoMinimoNotificacaoCorte" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e);
				throw new GGASException(e);
			}

		}

		return null;

	}
	
	/**
	 * Exibir pesquisa relatorio prazo minimo aviso corte.
	 * 
	 * @return exibirPesquisaRelatorioPrazoMinimoAvisoCorte - {@link String}
	 */
	@RequestMapping("exibirPesquisaRelatorioPrazoMinimoAvisoCorte")
	public String exibirPesquisaRelatorioPrazoMinimoAvisoCorte() {

		return "exibirPesquisaRelatorioPrazoMinimoAvisoCorte";

	}

	/**
	 * Gerar relatorio prazo minimo aviso corte.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioPrazoMinimoAvisoCorte - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioPrazoMinimoAvisoCorte")
	public String gerarRelatorioPrazoMinimoAvisoCorte(RelatorioCobrancaVO relatorioCobrancaVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltroRelatorio(relatorioCobrancaVO);

		try {
			validarMesAno((String) filtro.get(MES_ANO));

			filtro.put("nomeResponsavel", getDadosAuditoria(request).getUsuario().getFuncionario().getNome());

			byte[] relatorio = controladorRelatorioPrazoMinimoAntecedencia.gerarRelatorioAvisoCorte(filtro);

			if (relatorio != null) {

				request.getSession().setAttribute(RELATORIO, relatorio);
				request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf((String) filtro.get(FORMATO_IMPRESSAO)));
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);

			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaRelatorioPrazoMinimoAvisoCorte();

	}

	/**
	 * Validar mes ano.
	 * 
	 * @param mesAno - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void validarMesAno(String mesAno) throws NegocioException {

		if (mesAno == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MES_ANO_NAO_INFORMADO, true);
		} else {
			String mesAnoValidar = mesAno.replace("/", "").replace("_", "");
			if ("".equalsIgnoreCase(mesAnoValidar)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_MES_ANO_NAO_INFORMADO, true);
			}
			if (!Util.validarMesAno(mesAnoValidar)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_MES_ANO_NAO_VALIDO, mesAno.replace("_", ""));
			}
		}

	}

	/**
	 * Exibir relatorio prazo minimo aviso corte.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioPrazoMinimoAvisoCorte")
	public String exibirRelatorioPrazoMinimoAvisoCorte(HttpServletRequest request, HttpServletResponse response, Model model)
			throws GGASException {

		byte[] relatorio = (byte[]) request.getSession().getAttribute(RELATORIO);
		FormatoImpressao formatoImpressao = (FormatoImpressao) request.getSession().getAttribute(FORMATO_IMPRESSAO);

		request.getSession().removeAttribute(RELATORIO);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		if (relatorio != null) {
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			try {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioPrazoMinimoAvisoCorte" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		}

		return null;

	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioCobrancaVO - {@link RelatorioCobrancaVO}
	 * @return mapa - {@link Map}
	 */
	private Map<String, Object> popularFiltroRelatorio(RelatorioCobrancaVO relatorioCobrancaVO) {

		Map<String, Object> mapa = new HashMap<String, Object>();
		
		String formatoImpressao = relatorioCobrancaVO.getFormatoImpressao();
		Boolean exibirFiltro = relatorioCobrancaVO.getExibirFiltros();
		String mesAno = relatorioCobrancaVO.getMesAno();
		
		mapa.put(FORMATO_IMPRESSAO, formatoImpressao);

		if (exibirFiltro != null) {
			mapa.put(EXIBIR_FILTRO, exibirFiltro);
		}

		if (mesAno != null) {
			mapa.put(MES_ANO, mesAno);
		}

		return mapa;

	}

}
