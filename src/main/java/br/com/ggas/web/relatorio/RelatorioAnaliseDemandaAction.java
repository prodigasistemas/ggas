/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseDemanda;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil.TipoGrafico;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Análise de Demanda.
 */
@Controller
public class RelatorioAnaliseDemandaAction extends GenericAction {

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_RAMO_ATIVIDADE = "listaRamoAtividade";

	private static final String LISTA_DIAS_SEMANA = "listaDiasSemana";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String IDS_SEGMENTOS = "idsSegmentos";

	private static final String IDS_RAMOS_ATIVIDADES = "idsRamosAtividades";

	private static final String IDS_PONTO_CONSUMO = "idsPontoConsumo";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String EXIBIR_FILTRO = "exibirFiltros";

	private static final String EXIBIR_GRAFICO = "exibirGrafico";

	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	private static final String ID_TIPO_GRAFICO = "idTipoGrafico";

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";

	private static final String CONSUMO_PROGRAMADO = "consumoProgramado";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";


	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorRelatorioAnaliseDemanda controladorRelatorioAnaliseDemanda;
	
	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	/**
	 * Método responsavel por exibir a tela de
	 * pesquisa do relatorio.
	 *
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioAnaliseDemanda - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioAnaliseDemanda")
	public String exibirPesquisaRelatorioAnaliseDemanda(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			this.configurarDadosPesquisaRelatorioAnaliseDemanda(relatorioSubMenuMedicaoVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return "exibirPesquisaRelatorioAnaliseDemanda";

	}
	
	/**
	 * Método responsável por exibir o relatório
	 * de analise de demanda.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String} Quando uma Exception for lançada
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioAnaliseDemanda")
	public String gerarRelatorioAnaliseDemanda(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao());

		try {

			Map<String, Object> parametros = this.obterParametrosRelatorio(relatorioSubMenuMedicaoVO);

			byte[] relatorioAnaliseDemanda = controladorRelatorioAnaliseDemanda.gerarRelatorio(parametros);

			if (relatorioAnaliseDemanda != null) {

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				ServletOutputStream servletOutputStream;
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorioAnaliseDemanda.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioAnaliseDemanda" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorioAnaliseDemanda, 0, relatorioAnaliseDemanda.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}

		} catch (IOException e) {
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaRelatorioAnaliseDemanda(relatorioSubMenuMedicaoVO, result, clienteImovelPesquisaPopupVO, bindingResult,
					request, model);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Configurar dados pesquisa relatorio analise demanda.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarDadosPesquisaRelatorioAnaliseDemanda(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(INTERVALO_ANOS_DATA, this.obterIntervaloAnosData());
		model.addAttribute(LISTA_DIAS_SEMANA, this.obterDiasSemana());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_TIPO_GRAFICO, TipoGrafico.listar());

		this.configurarDadosPontoConsumo(relatorioSubMenuMedicaoVO, request, model);
		this.configurarDadosRamoAtividades(relatorioSubMenuMedicaoVO, model);

	}

	/**
	 * Configurar dados ramo atividades.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarDadosRamoAtividades(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Model model) throws GGASException {

		Long[] idsSegmentosAssociados = relatorioSubMenuMedicaoVO.getIdsSegmentosAssociados();

		if (idsSegmentosAssociados != null && idsSegmentosAssociados.length > 0) {
			model.addAttribute(LISTA_RAMO_ATIVIDADE, controladorRamoAtividade.consultarRamoAtividadePorSegmentos(idsSegmentosAssociados));
		}

	}

	/**
	 * Configurar dados ponto consumo.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarDadosPontoConsumo(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, HttpServletRequest request, Model model)
			throws GGASException {

		if (relatorioSubMenuMedicaoVO.getPesquisarPontosConsumo()) {

			Map<String, Object> filtro = this.obterParametrosRelatorio(relatorioSubMenuMedicaoVO);

			super.adicionarFiltroPaginacao(request, filtro);

			List<PontoConsumo> listaPontoConsumo = controladorRelatorioAnaliseDemanda.listarPontosConsumoRelatorioAnaliseDemanda(filtro);

			model.addAttribute(LISTA_PONTO_CONSUMO, super.criarColecaoPaginada(request, filtro, listaPontoConsumo));

		}

	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @return mapa - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> obterParametrosRelatorio(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) throws GGASException {

		Map<String, Object> mapa = new HashMap<String, Object>();

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();

		String tipoExibicao = relatorioSubMenuMedicaoVO.getTipoExibicao();
		String idTipoGrafico = relatorioSubMenuMedicaoVO.getIdTipoGrafico();

		mapa.put(CONSUMO_PROGRAMADO, relatorioSubMenuMedicaoVO.isConsumoProgramado());
		mapa.put(EXIBIR_FILTRO, relatorioSubMenuMedicaoVO.isExibirFiltros());
		mapa.put(EXIBIR_GRAFICO, relatorioSubMenuMedicaoVO.isExibirGrafico());
		mapa.put(FORMATO_IMPRESSAO, relatorioSubMenuMedicaoVO.getFormatoImpressao());

		if (StringUtils.isNotEmpty(tipoExibicao)) {
			mapa.put(TIPO_EXIBICAO, tipoExibicao);
		}

		if (StringUtils.isNotEmpty(idTipoGrafico)) {
			mapa.put(ID_TIPO_GRAFICO, idTipoGrafico);
		}

		if (idCliente != null && idCliente > 0) {
			mapa.put(ID_CLIENTE, idCliente);
		}

		if (idImovel != null && idImovel > 0) {
			mapa.put(ID_IMOVEL, idImovel);
		}

		obterParametrosRelatorioDois(relatorioSubMenuMedicaoVO, mapa);

		validarDataInicialFinal(relatorioSubMenuMedicaoVO, mapa);

		return mapa;
	}

	/**
	 * Obter parametros relatorio.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param mapa - {@link Map}
	 */
	private void obterParametrosRelatorioDois(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Map<String, Object> mapa) {

		Long[] idsSegmentos = relatorioSubMenuMedicaoVO.getIdsSegmentosAssociados();
		Long[] idsRamosAtividades = relatorioSubMenuMedicaoVO.getIdsRamosAtividadesAssociados();
		Long[] idsPontoConsumo = relatorioSubMenuMedicaoVO.getIdsPontoConsumo();

		if (idsSegmentos != null && idsSegmentos.length > 0) {
			mapa.put(IDS_SEGMENTOS, idsSegmentos);
		}

		if (idsRamosAtividades != null && idsRamosAtividades.length > 0) {
			mapa.put(IDS_RAMOS_ATIVIDADES, idsRamosAtividades);
		}

		if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
			mapa.put(IDS_PONTO_CONSUMO, idsPontoConsumo);
		}
	}

	/**
	 * Obter intervalo anos data.
	 * 
	 * @return o intervalo - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	private String obterIntervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		return String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();
	}

	/**
	 * Obter dias semana.
	 * 
	 * @return mapaDiasSemana - {@link Map}
	 */
	private Map<Integer, String> obterDiasSemana() {

		Map<Integer, String> mapaDiasSemana = new LinkedHashMap<Integer, String>();
		mapaDiasSemana.put(Calendar.SUNDAY, "Domingo");
		mapaDiasSemana.put(Calendar.SECOND, "Segunda");
		mapaDiasSemana.put(Calendar.TUESDAY, "Terça");
		mapaDiasSemana.put(Calendar.WEDNESDAY, "Quarta");
		mapaDiasSemana.put(Calendar.THURSDAY, "Quinta");
		mapaDiasSemana.put(Calendar.FRIDAY, "Sexta");
		mapaDiasSemana.put(Calendar.SATURDAY, "Sábado");

		return mapaDiasSemana;
	}
	
	/**
	 * Validar data inicial final.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarDataInicialFinal(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Map<String, Object> mapa)
			throws GGASException {

		String dataInicial = relatorioSubMenuMedicaoVO.getDataInicial();
		String dataFinal = relatorioSubMenuMedicaoVO.getDataFinal();
		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		try {

			if (StringUtils.isNotEmpty(dataInicial)) {
				mapa.put(DATA_INICIAL, df.parse(dataInicial));
			}

			if (StringUtils.isNotEmpty(dataFinal)) {
				mapa.put(DATA_FINAL, df.parse(dataFinal));
			}

		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(Constantes.ERRO_DATA_INVALIDADE, true);
		}
	}

}
