package br.com.ggas.web.relatorio.prazoParaEntregaFatura;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.ServiceLocator;

@Repository
public class RepositorioPrazoMinimoEntregaFatura extends RepositorioGenerico{

	@Autowired
	public RepositorioPrazoMinimoEntregaFatura(SessionFactory sessionFactory){
		setSessionFactory(sessionFactory);
	}
	@Override
	public EntidadeNegocio criar() {

		return new FaturaImpl();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return FaturaImpl.class;
	}

	public Collection<FaturaVO> listaFaturasPrazoDeVencimento(Date dataInicio, Date dataFinal){
		
		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente.nome as nomeCliente, "
						+ "fatura.dataEmissao as dataFatura, "
						+ "fatura.dataVencimento as dataVencimentoFatura ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" fatura ");
		hql.append(" inner join fatura.cliente cliente ");

		if(dataInicio != null){
			hql.append(" where ");
			hql.append(" fatura.dataEmissao >= :inicio ");
		}
		if(dataFinal != null){
			if(dataInicio == null){
				hql.append(" where ");
				hql.append(" fatura.dataEmissao <= :fim ");
			}else{
				hql.append(" And fatura.dataEmissao <= :fim ");
			}
		}
		hql.append(" order by cliente.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(FaturaVO.class));
		if(dataInicio != null){
			query.setDate("inicio", dataInicio );
		}
		if(dataFinal != null){
			query.setDate("fim", dataFinal );
		}

		@SuppressWarnings("unchecked")
		Collection<FaturaVO> listaFaturaVO = query.list();

		return listaFaturaVO;
	}
	
	/**
	 * Buscar Parametro do Sistama.
	 * 
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public List<String> buscarValorParamentro(String param){
		
		ServiceLocator.getInstancia().getControladorParametroSistema();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select parametroSistema.valor as valor ");
		hql.append(" from ");
		hql.append(" ParametroSistemaImpl ");
		hql.append(" parametroSistema ");
		hql.append(" Where ");
		hql.append(" parametroSistema.codigo ");
		hql.append(" LIKE ");
		hql.append(" :param ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("param", "%"+param+"%");
		
		return query.list();
	}
}
