package br.com.ggas.web.relatorio.medidores;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioMedidores;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioCadastroVO;
import br.com.ggas.web.relatorio.RelatorioRotaAction;

@Controller
@SessionAttributes({"medidores", "medidoresVO"})
public class RelatorioMedidoresAction extends GenericAction {
	
	private static final Logger LOG = Logger.getLogger(RelatorioRotaAction.class);
	
	private static final String LISTA_FAIXA_TEMPERATURA_TRABALHO = "listaFaixaTemperaturaTrabalho";

	private static final String LISTA_LOCAL_ARMAZENAGEM = "listaLocalArmazenagem";

	private static final String LISTA_CAPACIDADE_MEDIDOR = "listaCapacidadeMedidor";

	private static final String LISTA_DIAMETRO_MEDIDOR = "listaDiametroMedidor";

	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";

	private static final String LISTA_MARCA_MEDIDOR = "listaMarcaMedidor";

	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";

	private static final String LISTA_FATOR_K = "listaFatorK";

	private static final String LISTA_TIPO_MEDIDOR = "listaTipoMedidor";
	
	private static final String LISTA_UNIDADE_PRESSAO = "listaUnidadePressao";
	
	public static final String LISTA_TIPO_SERVICO = "listaTipoServico";
	
	private static final String REQUEST_SEM_CADASTRAMENTO_EM_LOTE = "comCadastroEmLote";

	private static final String REQUEST_COM_CADASTRAMENTO_EM_LOTE = "semCadastroEmLote";
	
	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";
	
	private static final String EXISTE_CADASTRO_BENS = "existeCadastroBens";
	
	private static final String VIEW_RELATORIO_SUBSTITUICAO_MEDIDORES = "exibirPesquisaRelatorioSubstituicaoMedidores";
	
	public static final String EXIBIR_FILTROS = "exibirFiltros";
	
	@Autowired
	private ControladorRelatorioMedidores controladorRelatorioMedidores;
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorUnidade controladorUnidade;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorIntegracao controladorIntegracao;
	
	@Autowired
	private ControladorEquipe controladorEquipe;
	
	@Autowired
	private ControladorServicoTipo controladorServicoTipo;
	
	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();
	
	@RequestMapping("exibirPesquisaRelatorioMedidores")
	public String exibirPesquisaRelatorioMedidores(Model model) throws GGASException {
//		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioMedidores");
		carregarCampos(model);
		carregarCamposPesquisa(model);
		return "exibirPesquisaRelatorioMedidores";
	}
	
	
	@RequestMapping("exibirPesquisaRelatorioSubstituicaoMedidor")
	public String exibirPesquisaRelatorioSubstituicaoMedidor(Model model) throws NegocioException {
		model.addAttribute("listaEquipe",controladorEquipe.listarEquipe());
		model.addAttribute(LISTA_TIPO_SERVICO, controladorServicoTipo.listarServicosTipoAtivosParaNovoMedidor());
		return VIEW_RELATORIO_SUBSTITUICAO_MEDIDORES;
	}
	
	
	@RequestMapping("gerarRelatorioSubstituicaoMedidor")
	public ModelAndView gerarRelatorioSubstituicaoMedidor(@RequestParam("dataInicio") String dataInicio,
			@RequestParam("dataFinal") String dataFinal,
			@RequestParam("tipoRelatorio") String tipoRelatorio,
			@RequestParam("exibirFiltros") String exibirFiltros,
			@RequestParam(value = "operacao", required = false) String operacao,
			@RequestParam(value = "equipe", required = false) List<Long> equipe,
			@RequestParam(value = "servico", required = false) List<Long> servico,
			@RequestParam(value = "tipoRelacaoRelatorio", required = true) String tipoRelacaoRelatorio,
			HttpServletResponse response) throws GGASException, ParseException {
		ModelAndView model = new ModelAndView("forward:/"+VIEW_RELATORIO_SUBSTITUICAO_MEDIDORES);
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		
		byte[] relatorio = null;
		try {

			Map<String, Object> filtros = popularFiltroRelatorio(operacao,equipe,servico,tipoRelacaoRelatorio);
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			DateFormat formatador = new SimpleDateFormat("dd/MM/yy");
			Date inicio = null;

			if ("".equals(dataInicio)) {
				inicio = null;
			} else {
				inicio = formatador.parse(dataInicio);
			}
			Date fim = null;

			if ("".equals(dataFinal)) {
				fim = null;
			} else {
				fim = formatador.parse(dataFinal);
			}
			
			relatorio = controladorRelatorioMedidores.gerarRelatorio(inicio, fim, exibirFiltros, tipoRelatorio, operacao, equipe, servico, tipoRelacaoRelatorio,filtros);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=RelatórioSubstituiçãoMedidores_"
					+ diaMesAno + "_" + hora + Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemAlerta(model, e.getMessage());
		} 
		catch (NegocioException e) {
			model = new ModelAndView(VIEW_RELATORIO_SUBSTITUICAO_MEDIDORES);
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model,e);
		}
		return model;
	}
	
	/**
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		model.addAttribute(LISTA_FATOR_K, controladorMedidor.listarFatorK());
		model.addAttribute(LISTA_SITUACAO_MEDIDOR, controladorMedidor.listarSituacaoMedidorCadastro());
		model.addAttribute(LISTA_DIAMETRO_MEDIDOR, controladorMedidor.listarDiametroMedidor());
		model.addAttribute(LISTA_CAPACIDADE_MEDIDOR, controladorMedidor.listarCapacidadeMedidor());
		model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
		model.addAttribute(LISTA_FAIXA_TEMPERATURA_TRABALHO, controladorMedidor.listarFaixaTemperaturaTrabalho());
		model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());

		model.addAttribute(REQUEST_SEM_CADASTRAMENTO_EM_LOTE, Medidor.SEM_LOTE);
		model.addAttribute(REQUEST_COM_CADASTRAMENTO_EM_LOTE, Medidor.COM_LOTE);

		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));

		model.addAttribute("listaAnosFabricacao", Util.listarAnos((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO)));
		model.addAttribute("intervaloAnosData", intervaloAnosData());

	}
	
	/**
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCamposPesquisa(Model model) throws GGASException {

		Collection<IntegracaoSistemaFuncao> listaIsfCadastroBens = controladorIntegracao
				.listarIntegracaoSistemaFuncaoCadastroBens();

		model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		model.addAttribute(EXISTE_CADASTRO_BENS, CollectionUtils.isNotEmpty(listaIsfCadastroBens));
	}

	
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String intervaloAnosData = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		intervaloAnosData = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return intervaloAnosData;
	}
	
	@RequestMapping("gerarRelatorioMedidores")
	public ModelAndView gerarRelatorioMedidores(@RequestParam(value = "dataInicio", required = false) String dataIncial,
			@RequestParam(value = "dataFinal", required = false) String dataFinal,
			@RequestParam(value="numeroSerie",required = false) String numeroSerie, 
			@RequestParam(value="marcaMedidor",required = false) String marcaMedidor, 
			@RequestParam(value="tipoMedidor",required = false) String tipoMedidor, 
			@RequestParam(value="modoUso",required = false) String modoUso, 
			@RequestParam(value="modelo",required = false) String modelo, 
			@RequestParam(value="localArmazenagem",required = false) String localArmazenagem, 
			@RequestParam(value="habilitado",required = false) String habilitado,
			@RequestParam(value="anoFabricacao",required = false) String anoFabricacao,
			HttpServletResponse response,
			@RequestParam("tipoRelatorio") String tipoRelatorio, @RequestParam("exibirFiltros") String exibirFiltros)
			throws GGASException, ParseException {

		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioMedidores");

		byte[] relatorio = null;
		try {
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			DateFormat formatador = new SimpleDateFormat("dd/MM/yy");
			Date inicio = null;

			if ("".equals(dataIncial)) {
				inicio = null;
			} else {
				inicio = formatador.parse(dataIncial);
			}
			Date fim = null;

			if ("".equals(dataFinal)) {
				fim = null;
			} else {
				fim = formatador.parse(dataFinal);
			}

			relatorio = controladorRelatorioMedidores.gerarRelatorio(inicio, fim, exibirFiltros, tipoRelatorio, numeroSerie,
					marcaMedidor, tipoMedidor, modoUso, modelo, localArmazenagem, habilitado, anoFabricacao);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=RelatórioMedidores_"
					+ diaMesAno + "_" + hora + Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemAlerta(model, e.getMessage());
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemAdvertencia(model, e.getChaveErro());
		}
		
		return model;
	}
	
	/**
	 * Popular filtro relatorio.
	 * 
	 * @param operacao
	 * @param equipe
	 * @param servico
	 * @param tipoRelacaoRelatorio 
	 * @return filtroRelatorio - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> popularFiltroRelatorio(String operacao, List<Long> equipe,List<Long> servico, 
			String tipoRelacaoRelatorio) throws GGASException {
	

		Map<String, Object> filtroRelatorio = new HashMap<String, Object>();

		if (operacao != null && !operacao.isEmpty()) {
			filtroRelatorio.put("filtroOperacao", operacao);
		}
		
		if (equipe != null) {
			StringBuilder nomeEquipes = new StringBuilder();
			int controle = 0;
			if(equipe.size() > 0) {
				for (Long chavePrimaria : equipe) {
					if(controle < 3) {
						if(chavePrimaria > 0) {
							nomeEquipes.append(controladorEquipe.obterEquipe(chavePrimaria).getNome()+" ");
							controle++;
						}
					}
				}
				filtroRelatorio.put("filtroEquipe",nomeEquipes);
			}

		}
		
		if (servico != null) {
			StringBuilder nomeServicos = new StringBuilder();
			int controle = 0;
			if(servico.size() > 0) {
				for (Long chavePrimaria : servico) {
					if(controle < 3) {
						if(chavePrimaria > 0) {
							nomeServicos.append(controladorServicoTipo.consultarServicoTipo(chavePrimaria).getDescricao()+" ");
							controle++;
						}
					}

				}
				filtroRelatorio.put("filtroServico",nomeServicos);
			}
		}
		
		if (tipoRelacaoRelatorio != null) {
			if(tipoRelacaoRelatorio.equals("relacao")) {
				filtroRelatorio.put("filtroTipoRelacaoRelatorio", "Relação");
			}else {
				filtroRelatorio.put("filtroTipoRelacaoRelatorio", "Listagem");
			}
		}
		
		return filtroRelatorio;
	}

}
