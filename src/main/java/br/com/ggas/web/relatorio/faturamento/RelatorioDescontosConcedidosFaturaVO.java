/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe referentes as chamadas do
 * 
 @since 27/10/2014 18:45:56
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author vpessoa
 */
public class RelatorioDescontosConcedidosFaturaVO {

	private String cliente;

	private String pontoConsumo;

	private String segmento;

	private String notaFiscal;

	private Date dataEmissao;

	private Date dataVencimento;

	private BigDecimal volume;

	private BigDecimal valorConsumoGas;

	private BigDecimal desconto;

	private BigDecimal valorTotal;

	public String getCliente() {

		return cliente;
	}

	public void setCliente(String cliente) {

		this.cliente = cliente;
	}

	public String getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getNotaFiscal() {

		return notaFiscal;
	}

	public void setNotaFiscal(String notaFiscal) {

		this.notaFiscal = notaFiscal;
	}

	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public BigDecimal getVolume() {

		return volume;
	}

	public void setVolume(BigDecimal volume) {

		this.volume = volume;
	}

	public BigDecimal getValorConsumoGas() {

		return valorConsumoGas;
	}

	public void setValorConsumoGas(BigDecimal valorConsumoGas) {

		this.valorConsumoGas = valorConsumoGas;
	}

	public BigDecimal getDesconto() {

		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {

		this.desconto = desconto;
	}

	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

}
