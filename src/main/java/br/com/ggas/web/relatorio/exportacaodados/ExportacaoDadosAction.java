/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.exportacaodados;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.controleacesso.ChaveOperacaoSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.relatorio.exportacaodados.ControladorConsulta;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.leitura.ExportarDadosDispositivosMoveisAction;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Exportação de Dados.
 */
@Controller
public class ExportacaoDadosAction extends GenericAction {

	private static final String EXPORTACAO_DADOS_VO = "exportacaoDadosVO";

	private static final String CONSULTA = "consulta";

	private static final String EXIBIR_RELATORIO = "exibirRelatorio";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	private static final String NOME_CONSULTA = "nomeConsulta";

	private static final String MODULOS = "modulos";

	private static final Logger LOG = Logger.getLogger(ExportarDadosDispositivosMoveisAction.class);

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String NOME = "nome";

	private static final String ID_MODULO = "idModulo";

	private static final String ID_OPERACAO = "idOperacao";

	private static final String LISTA_CONSULTA_PARAMETRO = "listaConsultaParametro";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String PARAMETRO_ALTERADO = "parametroAlterado";

	private static final String RELATORIO_EXPORTACAO = "relatorioExportacao";

	@Autowired
	private ControladorModulo controladorModulo;

	@Autowired
	private ControladorConsulta controladorConsulta;

	@Autowired
	private ControladorAcesso controladorAcesso;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorAuditoria controladorAuditoria;

	/**
	 * Método responsável por exibir a tela de pesquisa consulta.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return exibirPesquisaConsultaExportacao - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaConsultaExportacao")
	public String exibirPesquisaConsultaExportacao(Model model, HttpServletRequest request) throws GGASException {

		try {
			model.addAttribute(MODULOS, controladorModulo.consultarTodosModulos());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaConsultaExportacao";
	}

	/**
	 * Método responsável por pesquisar consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaConsultaExportacao - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarConsultaExportacao")
	public String pesquisarConsultaExportacao(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);

		request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, null);

		model.addAttribute(CHAVES_PRIMARIAS, null);

		try {

			Map<String, Object> filtro = obterFiltroForm(exportacaoDadosVO);
			model.addAttribute(MODULOS, controladorModulo.consultarModulos(null));
			Collection<Consulta> listaConsulta = controladorConsulta.consultarConsulta(filtro);
			model.addAttribute("listaConsulta", listaConsulta);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaConsultaExportacao(model, request);
	}

	/**
	 * Metodo responsavel por exibir a tela de detalhamento de uma consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoConsultaExportacao")
	public String exibirDetalhamentoConsultaExportacao(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoConsultaExportacao";
		Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();

		try {

			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);
			model.addAttribute(CONSULTA, consulta);
			Collection<ConsultaParametro> listaParametros = new ArrayList<ConsultaParametro>();
			listaParametros.addAll(consulta.getListaParametro());
			request.setAttribute(LISTA_CONSULTA_PARAMETRO, listaParametros);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		saveToken(request);

		return view;
	}

	/**
	 * Método responsável por exibir a tela de inserir consulta.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return exibirInclusaoConsultaExportacao - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoConsultaExportacao")
	public String exibirInclusaoConsultaExportacao(Model model, HttpServletRequest request) throws GGASException {

		carregarListasComboNoRequest(model);
		model.addAttribute(LISTA_CONSULTA_PARAMETRO, request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO));

		saveToken(request);

		return "exibirInclusaoConsultaExportacao";
	}

	/**
	 * Método responsável por inserir uma consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("incluirConsultaExportacao")
	@SuppressWarnings("unchecked")
	public String inserirConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;

		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);
			Consulta consulta = (Consulta) controladorConsulta.criar();
			popularConsulta(consulta, exportacaoDadosVO);
			Long chaveOperacao = controladorAcesso.obterChaveOperacaoSistema(ChaveOperacaoSistema.EXPORTAR_DADOS_PARA_ARQUIVO);
			consulta.setOperacao(controladorModulo.buscarOperacaoPorChave(chaveOperacao));
			consulta.setDadosAuditoria(getDadosAuditoria(request));
			consulta.setHabilitado(true);
			Collection<ConsultaParametro> listaConsultaParametro =
					(Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

			setarListaParametroConsultaInserir(consulta, listaConsultaParametro);

			controladorConsulta.inserir(consulta);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Consulta.CONSULTA_CAMPO_STRING);

			view = pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
			
			exportacaoDadosVO.setNome(null);
			exportacaoDadosVO.setIdModulo(null);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirInclusaoConsultaExportacao(model, request);
		}

		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);
		
		return view;
	}

	/**
	 * Metodo responsavel por exibir a tela de atualizacao de uma consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoConsultaExportacao")
	public String exibirAlteracaoConsultaExportacao(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();
		Boolean isAcaoCopiar = Boolean.valueOf(request.getParameter("isAcaoCopiar"));

		String view = "exibirAlteracaoConsultaExportacao";

		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);

		try {

			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);

			carregarCampos(request, consulta, model);
			model.addAttribute(CONSULTA, consulta);

			if (isAcaoCopiar) {
				popularVO(exportacaoDadosVO, consulta);
				view = exibirInclusaoConsultaExportacao(model, request);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		saveToken(request);

		return view;
	}

	/**
	 * Método responsável por atualizar uma Gerencia Regional.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarConsultaExportacao")
	@SuppressWarnings("unchecked")
	public String alterarConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;

		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);

			Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();
			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);
			popularConsulta(consulta, exportacaoDadosVO);
			consulta.setDadosAuditoria(getDadosAuditoria(request));

			Collection<ConsultaParametro> listaParametros =
					(Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

			controladorConsulta.setarListaParametroConsulta(consulta, listaParametros);
			controladorConsulta.atualizar(consulta);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Consulta.CONSULTA_CAMPO_STRING);

			view = pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por adicionar parametros na consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param isFluxoInserirConsulta - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarParametroConsulta")
	public String adicionarParametroConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			@RequestParam(value = "fluxo", required = false) Boolean isFluxoInserirConsulta, Model model) throws GGASException {

		Integer indexLista = exportacaoDadosVO.getIndexLista();
		String view = null;

		List<ConsultaParametro> listaConsultaParametro =
				(List<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

		try {
			validarToken(request);

			if (indexLista == null || indexLista < 0) {
				ConsultaParametro consultaParametro = (ConsultaParametro) controladorConsulta.criarConsultaParametro();
				consultaParametro.setDadosAuditoria(getDadosAuditoria(request));
				this.popularConsultaParametro(consultaParametro, exportacaoDadosVO, listaConsultaParametro);
				if (indexLista != null) {
					validarConsultaParametro(consultaParametro, listaConsultaParametro, indexLista);
				}
				if (listaConsultaParametro == null) {
					listaConsultaParametro = new ArrayList<ConsultaParametro>();
				}
				listaConsultaParametro.add(consultaParametro);

			} else {
				request.setAttribute(PARAMETRO_ALTERADO, true);
				this.alterarParametroConsulta(indexLista, exportacaoDadosVO, listaConsultaParametro);
			}

			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, listaConsultaParametro);
		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);

		if (isFluxoInserirConsulta) {
			view = exibirInclusaoConsultaExportacao(model, request);
		} else {
			view = exibirAlteracaoConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		return view;
	}

	/**
	 * Alterar parametro consulta.
	 * 
	 * @param indexLista - {@link Integer}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param listaConsultaParametro - {@link Lista}
	 * @throws GGASException - {@link GGASException}
	 */
	private void alterarParametroConsulta(Integer indexLista, ExportacaoDadosVO exportacaoDadosVO,
			List<ConsultaParametro> listaConsultaParametro) throws GGASException {

		if (listaConsultaParametro != null) {
			ConsultaParametro consultaParametro = (ConsultaParametro) controladorConsulta.criarConsultaParametro();
			this.popularConsultaParametro(consultaParametro, exportacaoDadosVO, listaConsultaParametro);
			validarConsultaParametro(consultaParametro, listaConsultaParametro, indexLista);
			ConsultaParametro consultaParametroExistente = null;
			for (int i = 0; i < listaConsultaParametro.size(); i++) {
				consultaParametroExistente = listaConsultaParametro.get(i);
				if (i == indexLista && consultaParametroExistente != null) {
					this.popularConsultaParametro(consultaParametroExistente, exportacaoDadosVO, listaConsultaParametro);
				}
			}
		}

	}

	/**
	 * Método responsável por remover parametros na consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param isFluxoInserirConsulta - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerParametroConsulta")
	public String removerParametroConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			@RequestParam(value = "fluxo", required = false) Boolean isFluxoInserirConsulta, Model model) throws GGASException {

		Integer indexLista = exportacaoDadosVO.getIndexLista();
		String view = null;

		Collection<ConsultaParametro> listaConsultaParametro =
				(Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

		try {

			validarToken(request);

			if (listaConsultaParametro != null && indexLista != null) {
				((List) listaConsultaParametro).remove(indexLista.intValue());
			} else {
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO_EXCLUIR_SELECIONADO);
			}

			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, listaConsultaParametro);

		if (isFluxoInserirConsulta) {
			view = exibirInclusaoConsultaExportacao(model, request);
		} else {
			view = exibirAlteracaoConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por remover um ou mais Gerencia Regional.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return pesquisarConsultaExportacao - {@link String}
	 * @throws GGASException -  - {@link GGASException}
	 */
	@RequestMapping("excluirConsultaExportacao")
	public String removerConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			validarToken(request);
			Long[] chavesPrimarias = exportacaoDadosVO.getChavesPrimarias();
			controladorConsulta.validarRemoverConsulta(chavesPrimarias);
			controladorConsulta.remover(chavesPrimarias, getDadosAuditoria(request));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, Consulta.CONSULTA_CAMPO_STRING);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
	}
	
	/**
	 * Método responsável por pesquisar consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirExportacaoDados - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirExportacaoDados")
	public String exibirExportacaoDados(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirExportacaoDados";
		
		try {
			Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();
			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);

			List<ConsultaParametro> lista = null;
			if (consulta.getListaParametro() != null) {
				lista = Util.ordenarColecaoPorAtributo(consulta.getListaParametro(), "ordem", true);
			}
			model.addAttribute(CONSULTA, consulta);
			request.setAttribute(LISTA_CONSULTA_PARAMETRO, lista);
			Map<Long, Collection<EntidadeNegocio>> listas = new HashMap<Long, Collection<EntidadeNegocio>>();
			if (lista != null) {
				atribuirListasEntidadesReferenciadas(listas, lista);
			}
			request.setAttribute("listas", listas);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarConsultaExportacao(exportacaoDadosVO, bindingResult, request, model);
		}

		saveToken(request);

		return view;

	}
	
	/**
	 * Exibir exportacao dados relatorio.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirExportacaoDadosRelatorio")
	public String exibirExportacaoDadosRelatorio(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		try {
			String nome = (String) request.getSession().getAttribute(NOME_CONSULTA);
			byte[] arquivoExportacao = (byte[]) request.getSession().getAttribute(RELATORIO_EXPORTACAO);

			request.getSession().removeAttribute(RELATORIO_EXPORTACAO);
			request.getSession().removeAttribute(NOME_CONSULTA);

			if (arquivoExportacao != null) {
				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);
				exibirDownload(nome, arquivoExportacao, response);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirExportacaoDados(exportacaoDadosVO, bindingResult, request, model);
	}
	
	/**
	 * Método responsável por inserir uma
	 * consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO} 
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return exibirExportacaoDadosRelatorio - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exportarDados")
	public String exportarDados(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();

		try {

			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);
			Map<String, Object[]> mapDadosParametro = obterMapExportacaoPopulado(consulta, request);
			String separador = exportacaoDadosVO.getSeparador();
			mapDadosParametro.put("exportarDados", null);
			byte[] arquivo = controladorConsulta.obterDadosExecucaoConsulta(consulta, mapDadosParametro, separador);
			if (arquivo != null) {
				request.getSession().setAttribute(RELATORIO_EXPORTACAO, arquivo);
				request.getSession().setAttribute(NOME_CONSULTA, consulta.getNome());
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirExportacaoDados(exportacaoDadosVO, bindingResult, request, model);
	}
	
	/**
	 * Exibir download.
	 *
	 * @param nomeConsulta - {@link String}
	 * @param arquivo - {@link Byte}
	 * @param response - {@link HttpServletResponse}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void exibirDownload(String nomeConsulta, byte[] arquivo, HttpServletResponse response) throws NegocioException {

		response.setContentType("application/text");
		response.setContentLength(arquivo.length);
		response.addHeader("Content-Disposition",
				"attachment; filename=" + Util.removerCaracteresEspeciais(nomeConsulta.replace(" ", "_").toUpperCase()) + ".TXT");

		try {
			response.getOutputStream().write(arquivo);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e.getMessage());
		}
	}

	/**
	 * Carregar listas combo no request.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarListasComboNoRequest(Model model) throws GGASException {

		model.addAttribute(MODULOS, controladorModulo.consultarTodosModulos());
		model.addAttribute("tipos", controladorEntidadeConteudo.listarTipoParametro());
		model.addAttribute("mascaras_a", controladorConsulta.listarMascaraParametro("221"));
		model.addAttribute("mascaras_n", controladorConsulta.listarMascaraParametro("220"));
		model.addAttribute("mascaras_d", controladorConsulta.listarMascaraParametro("222"));
		model.addAttribute("entidadesReferenciadas", controladorConsulta.listarEntidadeReferenciada());
		model.addAttribute("entidadesClasses", controladorEntidadeConteudo.listarEntidadeClasse());
		model.addAttribute("classeUnidades", controladorEntidadeConteudo.listarClasseUnidade());
	}

	/**
	 * Popular consulta.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularConsulta(Consulta consulta, ExportacaoDadosVO exportacaoDadosVO) throws GGASException {

		String nome = exportacaoDadosVO.getNome();
		Long idModulo = exportacaoDadosVO.getIdModulo();
		String sql = exportacaoDadosVO.getSql();
		String descricao = exportacaoDadosVO.getDescricao();

		if (StringUtils.isEmpty(nome)) {
			consulta.setNome(null);
		} else {
			consulta.setNome(nome);
		}

		if (idModulo != null && idModulo > 0) {
			consulta.setModulo((Modulo) controladorModulo.obter(idModulo));
		} else {
			consulta.setModulo(null);
		}

		if (StringUtils.isEmpty(sql)) {
			consulta.setSql(null);
		} else {
			consulta.setSql(sql);
		}

		if (StringUtils.isEmpty(descricao)) {
			consulta.setDescricao(null);
		} else {
			consulta.setDescricao(descricao);
		}

	}

	/**
	 * Setar lista parametro consulta inserir.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param listaConsultaParametro - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	public void setarListaParametroConsultaInserir(Consulta consulta, Collection<ConsultaParametro> listaConsultaParametro) {

		consulta.getListaParametro().clear();
		if (listaConsultaParametro != null) {

			for (ConsultaParametro consultaParametro : listaConsultaParametro) {

				ConsultaParametro cp = (ConsultaParametro) controladorConsulta.criarConsultaParametro();
				cp.setConsulta(consulta);
				cp.setUltimaAlteracao(Calendar.getInstance().getTime());
				cp.setCodigo(consultaParametro.getCodigo());
				cp.setNomeCampo(consultaParametro.getNomeCampo());
				cp.setEntidadeConteudo(consultaParametro.getEntidadeConteudo());
				cp.setNumeroMaximoCaracteres(consultaParametro.getNumeroMaximoCaracteres());
				cp.setMascara(consultaParametro.getMascara());
				cp.setOrdem(consultaParametro.getOrdem());
				cp.setFk(consultaParametro.getFk());
				cp.setMultivalorado(consultaParametro.getMultivalorado());
				cp.setTabela(consultaParametro.getTabela());
				cp.setChaveEntidadeComplementar(consultaParametro.getChaveEntidadeComplementar());

				consulta.getListaParametro().add(cp);

			}
		}
	}

	/**
	 * Carregar campos.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param consulta {@link Consulta}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void carregarCampos(HttpServletRequest request, Consulta consulta, Model model) throws GGASException {

		carregarListasComboNoRequest(model);

		Collection<ConsultaParametro> listaParametros = new ArrayList<ConsultaParametro>();
		if (!isPostBack(request)) {
			if (consulta != null) {
				listaParametros.addAll(consulta.getListaParametro());
			}
			request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, listaParametros);
		} else {
			listaParametros = (Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);
		}
		request.setAttribute(LISTA_CONSULTA_PARAMETRO, listaParametros);
	}

	/**
	 * Validar consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param listaConsultaParametro - {@link Collection}
	 * @param indice - {@link Integer}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarConsultaParametro(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
			int indice) throws GGASException {

		controladorConsulta.validarConsultaParametro(consultaParametro, listaConsultaParametro, indice);
	}

	/**
	 * Popula ExportacaoDadosVO quando realizada ação de copiar.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param consulta - {@link Consulta}
	 */
	private void popularVO(ExportacaoDadosVO exportacaoDadosVO, Consulta consulta) {
		exportacaoDadosVO.setChavePrimaria(consulta.getChavePrimaria());
		exportacaoDadosVO.setNome(consulta.getNome());
		exportacaoDadosVO.setIdModulo(consulta.getModulo().getChavePrimaria());
		exportacaoDadosVO.setSql(consulta.getSql());
		exportacaoDadosVO.setDescricao(consulta.getDescricao());
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> obterFiltroForm(ExportacaoDadosVO exportacaoDadosVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String nome = exportacaoDadosVO.getNome();
		if (StringUtils.isNotEmpty(nome)) {
			filtro.put(NOME, nome);
		}

		Long idModulo = exportacaoDadosVO.getIdModulo();
		if (idModulo != null && idModulo > 0) {
			filtro.put(ID_MODULO, idModulo);
		}

		Long chaveOperacao = controladorAcesso.obterChaveOperacaoSistema(ChaveOperacaoSistema.EXPORTAR_DADOS_PARA_ARQUIVO);
		if (chaveOperacao != null && chaveOperacao > 0) {
			filtro.put(ID_OPERACAO, chaveOperacao);
		}

		return filtro;
	}

	/**
	 * Popular consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param listaConsultaParametro - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularConsultaParametro(ConsultaParametro consultaParametro, ExportacaoDadosVO exportacaoDadosVO,
			Collection<ConsultaParametro> listaConsultaParametro) throws GGASException {

		String paramCodigo = exportacaoDadosVO.getParamCodigo();
		String paramNome = exportacaoDadosVO.getParamNome();
		Long paramIdTipo = exportacaoDadosVO.getParamIdTipo();
		Integer paramQtdMaxCaracteres = exportacaoDadosVO.getParamQtdMaxCaracteres();
		String paramMascara = exportacaoDadosVO.getParamMascara();

		if (StringUtils.isEmpty(paramCodigo)) {
			consultaParametro.setCodigo(null);
		} else {
			consultaParametro.setCodigo(paramCodigo);
		}

		if (StringUtils.isEmpty(paramNome)) {
			consultaParametro.setNomeCampo(null);
		} else {
			consultaParametro.setNomeCampo(paramNome);
		}

		if (paramIdTipo != null && paramIdTipo > 0) {
			consultaParametro.setEntidadeConteudo(controladorEntidadeConteudo.obterEntidadeConteudo(paramIdTipo));
		} else {
			consultaParametro.setEntidadeConteudo(null);
		}

		if (paramQtdMaxCaracteres != null && paramQtdMaxCaracteres > 0) {
			consultaParametro.setNumeroMaximoCaracteres(paramQtdMaxCaracteres);
		} else {
			consultaParametro.setNumeroMaximoCaracteres(null);
		}

		if (StringUtils.isEmpty(paramMascara) || "-1".equals(paramMascara)) {
			consultaParametro.setMascara(null);
		} else {
			consultaParametro.setMascara(paramMascara);
		}

		popularConsultaParametroDois(consultaParametro, listaConsultaParametro, exportacaoDadosVO);
	}

	/**
	 * Popular consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param listaConsultaParametro - {@link Collection}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void popularConsultaParametroDois(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
			ExportacaoDadosVO exportacaoDadosVO) throws NegocioException {

		Integer paramOrdemExibicao = exportacaoDadosVO.getParamOrdemExibicao();
		Boolean paramIsEntidadeChave = exportacaoDadosVO.getParamIsChaveEntidade();
		Boolean paramIsPermiteMultiplaSelecao = exportacaoDadosVO.getParamIsPermiteMultiplaSelecao();
		Long paramEntidadeReferenciada = exportacaoDadosVO.getParamIdEntidadeReferenciada();
		Long paramClasseChaveEntidade = exportacaoDadosVO.getParamIdClasseAssociada();

		if (paramOrdemExibicao != null && paramOrdemExibicao > 0) {
			consultaParametro.setOrdem(paramOrdemExibicao);
		} else {
			if (listaConsultaParametro == null) {
				consultaParametro.setOrdem(1);
			} else {
				consultaParametro.setOrdem(null);
			}
		}

		if (paramIsEntidadeChave != null) {
			consultaParametro.setFk(paramIsEntidadeChave);
		} else {
			consultaParametro.setFk(false);
		}

		if (paramIsPermiteMultiplaSelecao != null) {
			consultaParametro.setMultivalorado(paramIsPermiteMultiplaSelecao);
		} else {
			consultaParametro.setMultivalorado(false);
		}

		if (paramEntidadeReferenciada != null && paramEntidadeReferenciada > 0) {
			consultaParametro.setTabela(controladorAuditoria.obterTabelaSelecionada(paramEntidadeReferenciada));
		} else {
			consultaParametro.setTabela(null);
		}

		if (paramClasseChaveEntidade != null && paramClasseChaveEntidade > 0) {
			consultaParametro.setChaveEntidadeComplementar(paramClasseChaveEntidade);
		} else {
			consultaParametro.setChaveEntidadeComplementar(null);
		}
	}
	
	/**
	 * Atribuir listas entidades referenciadas.
	 * 
	 * @param listas - {@link Collection}
	 * @param parametros - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void atribuirListasEntidadesReferenciadas(Map<Long, Collection<EntidadeNegocio>> listas,
			Collection<ConsultaParametro> parametros) throws NegocioException {

		for (ConsultaParametro consultaParametro : parametros) {
			if (consultaParametro.getFk() && consultaParametro.getTabela() != null) {
				listas.put(consultaParametro.getChavePrimaria(),
						controladorConsulta.listarRegistrosDaEntidadeReferenciada(consultaParametro.getTabela()));
			}
		}
	}
	
	/**
	 *
	 * @param consulta - {@link Consulta}
	 * @param request - {@link HttpServletRequest}
	 * @return mapCamposExecucaoConsulta - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object[]> obterMapExportacaoPopulado(Consulta consulta, HttpServletRequest request) throws GGASException {

		Map<String, Object[]> mapCamposExecucaoConsulta = new HashMap<String, Object[]>();
		List<ConsultaParametro> lista = Util.ordenarColecaoPorAtributo(consulta.getListaParametro(), "ordem", true);
		for (ConsultaParametro consultaParametro : lista) {
			Object[] valor = request.getParameterValues(consultaParametro.getChavePrimaria() + "");

			if (valor == null) {
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
			}

			// removendo mascaras
			for (int i = 0; i < valor.length; i++) {
				if (!"DATA".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
					controladorConsulta.tratarParametrosData(consultaParametro, valor, i);
				}
			}

			if (valor.length == 0 && !"BOLEANO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				// campo booleano pode ser null
				// (false)
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
			}

			if ("BOLEANO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				tratandoParametrosBooleano(mapCamposExecucaoConsulta, consultaParametro);
			} else if ("DATA".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				Date[] params = new Date[valor.length];
				for (int i = 0; i < valor.length; i++) {
					params[i] = Util.converterCampoStringParaData(consultaParametro.getNomeCampo(), valor[i].toString(),
							Constantes.FORMATO_DATA_BR);
				}
				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			} else if ("NUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				tratandoParametrosNumerico(mapCamposExecucaoConsulta, consultaParametro, valor);
			} else if ("ALFANUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				String[] params = new String[valor.length];
				for (int i = 0; i < valor.length; i++) {
					params[i] = valor[i].toString();
				}
				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			}
		}
		return mapCamposExecucaoConsulta;
	}

	private void tratandoParametrosBooleano(Map<String, Object[]> mapCamposExecucaoConsulta, ConsultaParametro consultaParametro) {
		Object[] valor;
		valor = new Boolean[1];
		valor[0] = null;

		Boolean[] params = new Boolean[valor.length];
		for (int i = 0; i < valor.length; i++) {
			if (valor[i] != null && "on".equals(valor[i].toString())) {
				params[i] = Boolean.TRUE;
			} else {
				params[i] = Boolean.FALSE;
			}
		}
		mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
	}

	private void tratandoParametrosNumerico(Map<String, Object[]> mapCamposExecucaoConsulta, ConsultaParametro consultaParametro,
			Object[] valor) {
		Object[] params = new Object[valor.length];

		if (valor[0].toString().contains(".")) {
			for (int i = 0; i < valor.length; i++) {
				params[i] = new Double(valor[i].toString().trim());
			}
		} else {
			for (int i = 0; i < valor.length; i++) {
				params[i] = Long.valueOf(valor[i].toString().trim());
			}
		}

		mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
	}

}
