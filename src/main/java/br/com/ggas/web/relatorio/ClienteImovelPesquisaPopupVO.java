package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes aos
 * Popups de pesquisas de Cliente e Imovel que são apresentados em algumas telas.
 *
 * @author bruno silva
 * 
 * @see RelatorioDeInadimplentesAction, RelatorioClienteUnidadeConsumidoraAction, RelatorioClienteHistoricoAction, 
 * 
 */
public class ClienteImovelPesquisaPopupVO {

	private Long idCliente;
	private String nomeCompletoCliente;
	private String nomeCliente;
	private String documentoFormatado;
	private String documentoCliente;
	private String enderecoFormatadoCliente;
	private String enderecoCliente;
	private String emailCliente;
	
	private Long idImovel;
	private String nomeFantasiaImovel;
	private String matriculaImovel;
	private String numeroImovel;
	private String cidadeImovel;
	private Boolean condominio;
	
	
	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return nomeCompletoCliente - {@link String}
	 */
	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}
	/**
	 * @param nomeCompletoCliente - {@link String}
	 */
	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente;
	}
	/**
	 * @return nomeCliente - {@link String}
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}
	/**
	 * @param nomeCliente - {@link String}
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	/**
	 * @return documentoFormatado - {@link String}
	 */
	public String getDocumentoFormatado() {
		return documentoFormatado;
	}
	/**
	 * @param documentoFormatado - {@link String}
	 */
	public void setDocumentoFormatado(String documentoFormatado) {
		this.documentoFormatado = documentoFormatado;
	}
	/**
	 * @return documentoCliente - {@link String}
	 */
	public String getDocumentoCliente() {
		return documentoCliente;
	}
	/**
	 * @param documentoCliente - {@link String}
	 */
	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}
	/**
	 * @return enderecoCliente - {@link String}
	 */
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	/**
	 * @param enderecoCliente - {@link String}
	 */
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	/**
	 * @return enderecoFormatadoCliente - {@link String}
	 */
	public String getEnderecoFormatadoCliente() {
		return enderecoFormatadoCliente;
	}
	/**
	 * @param enderecoFormatadoCliente - {@link String}
	 */
	public void setEnderecoFormatadoCliente(String enderecoFormatadoCliente) {
		this.enderecoFormatadoCliente = enderecoFormatadoCliente;
	}
	/**
	 * @return emailCliente - {@link String}
	 */
	public String getEmailCliente() {
		return emailCliente;
	}
	/**
	 * @param emailCliente - {@link String}
	 */
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return nomeFantasiaImovel - {@link String}
	 */
	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}
	/**
	 * @param nomeFantasiaImovel - {@link String}
	 */
	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}
	/**
	 * @return matriculaImovel - {@link String}
	 */
	public String getMatriculaImovel() {
		return matriculaImovel;
	}
	/**
	 * @param matriculaImovel - {@link String}
	 */
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}
	/**
	 * @return numeroImovel - {@link String}
	 */
	public String getNumeroImovel() {
		return numeroImovel;
	}
	/**
	 * @param numeroImovel - {@link String}
	 */
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	/**
	 * @return cidadeImovel - {@link String}
	 */
	public String getCidadeImovel() {
		return cidadeImovel;
	}
	/**
	 * @param cidadeImovel - {@link String}
	 */
	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}
	/**
	 * @return condominio - {@link Boolean}
	 */
	public Boolean getCondominio() {
		return condominio;
	}
	/**
	 * @param condominio - {@link Boolean}
	 */
	public void setCondominio(Boolean condominio) {
		this.condominio = condominio;
	}
	
}
