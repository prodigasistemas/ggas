package br.com.ggas.web.relatorio.medidores;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.util.Util;

public class SubstituicaoMedidorVO {
	
	private String pontoConsumo;
	private String medidorTipo;
	private String medidorFabricante;
	private String numeroSerie;
	private String medidorModelo;
	private String medidorBP;
	private BigDecimal leituraFinal;
	private String motivoOperacao;
	private Date dataOperacao;
	private String dataOperacaoFormatada;
	
	public String getPontoConsumo() {
		return pontoConsumo;
	}
	public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}
	public String getMedidorTipo() {
		return medidorTipo;
	}
	public void setMedidorTipo(String medidorTipo) {
		this.medidorTipo = medidorTipo;
	}
	public String getMedidorFabricante() {
		return medidorFabricante;
	}
	public void setMedidorFabricante(String medidorFabricante) {
		this.medidorFabricante = medidorFabricante;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getMedidorModelo() {
		return medidorModelo;
	}
	public void setMedidorModelo(String medidorModelo) {
		this.medidorModelo = medidorModelo;
	}
	public String getMedidorBP() {
		return medidorBP;
	}
	public void setMedidorBP(String medidorBP) {
		this.medidorBP = medidorBP;
	}
	public BigDecimal getLeituraFinal() {
		return leituraFinal;
	}
	public void setLeituraFinal(BigDecimal leituraFinal) {
		this.leituraFinal = leituraFinal;
	}
	public String getMotivoOperacao() {
		return motivoOperacao;
	}
	public void setMotivoOperacao(String motivoOperacao) {
		this.motivoOperacao = motivoOperacao;
	}
	public Date getDataOperacao() {
		return dataOperacao;
	}
	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	public String getDataOperacaoFormatada() {
		String dataFormatada = "";
		if(dataOperacao != null) {
			dataFormatada = Util.converterDataParaString(dataOperacao);
		}
		return dataFormatada;
	}
}
