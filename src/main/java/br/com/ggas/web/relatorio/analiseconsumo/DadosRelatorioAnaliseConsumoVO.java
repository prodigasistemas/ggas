/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.analiseconsumo;

import java.math.BigDecimal;
import java.util.Date;

public class DadosRelatorioAnaliseConsumoVO {

	private Date data;

	private BigDecimal volumeMedido;

	private BigDecimal pcs;

	private BigDecimal fatorCorrecao;

	private BigDecimal volumeCorrigido;

	private BigDecimal volumeFaturado;

	private String descricaoRubrica;

	private Long numeroDocFiscal;

	private Boolean exibeData;

	private BigDecimal totalVolumeFaturado;

	/**
	 * @return the data
	 */
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Date data) {

		this.data = data;
	}

	/**
	 * @return the volumeMedido
	 */
	public BigDecimal getVolumeMedido() {

		return volumeMedido;
	}

	/**
	 * @param volumeMedido
	 *            the volumeMedido to set
	 */
	public void setVolumeMedido(BigDecimal volumeMedido) {

		this.volumeMedido = volumeMedido;
	}

	/**
	 * @return the pcs
	 */
	public BigDecimal getPcs() {

		return pcs;
	}

	/**
	 * @param pcs
	 *            the pcs to set
	 */
	public void setPcs(BigDecimal pcs) {

		this.pcs = pcs;
	}

	/**
	 * @return the fatorCorrecao
	 */
	public BigDecimal getFatorCorrecao() {

		return fatorCorrecao;
	}

	/**
	 * @param fatorCorrecao
	 *            the fatorCorrecao to set
	 */
	public void setFatorCorrecao(BigDecimal fatorCorrecao) {

		this.fatorCorrecao = fatorCorrecao;
	}

	/**
	 * @return the volumeCorrigido
	 */
	public BigDecimal getVolumeCorrigido() {

		return volumeCorrigido;
	}

	/**
	 * @param volumeCorrigido
	 *            the volumeCorrigido to set
	 */
	public void setVolumeCorrigido(BigDecimal volumeCorrigido) {

		this.volumeCorrigido = volumeCorrigido;
	}

	/**
	 * @return the volumeFaturado
	 */
	public BigDecimal getVolumeFaturado() {

		return volumeFaturado;
	}

	/**
	 * @param volumeFaturado
	 *            the volumeFaturado to set
	 */
	public void setVolumeFaturado(BigDecimal volumeFaturado) {

		this.volumeFaturado = volumeFaturado;
	}

	public String getDescricaoRubrica() {

		return descricaoRubrica;
	}

	public void setDescricaoRubrica(String descricaoRubrica) {

		this.descricaoRubrica = descricaoRubrica;
	}

	public Long getNumeroDocFiscal() {

		return numeroDocFiscal;
	}

	public void setNumeroDocFiscal(Long numeroDocFiscal) {

		this.numeroDocFiscal = numeroDocFiscal;
	}

	public Boolean getExibeData() {

		return exibeData;
	}

	public void setExibeData(Boolean exibeData) {

		this.exibeData = exibeData;
	}

	/**
	 * @return the totalVolFaturado
	 */
	public BigDecimal getTotalVolumeFaturado() {

		return totalVolumeFaturado;
	}

	/**
	 * @param totalVolFaturado
	 *            the totalVolumeFAturado to set
	 */
	public void setTotalVolumeFaturado(BigDecimal totalVolumeFaturado) {

		this.totalVolumeFaturado = totalVolumeFaturado;
	}

}
