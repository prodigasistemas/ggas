/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 29/08/2014 15:24:25
 @author rfilho
 */

package br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class RelatorioCobrancaRecuperacaoPenalidadesVO implements Serializable {

	private static final long serialVersionUID = -6156416006633455660L;

	private String nomeClientePontoConsumo;

	private Collection<SubRelatorioCobrancaRecuperacaoPenalidadesVO> colecaoVOs;

	private Collection<SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO> colecaoPeriodicidadeVOs;

	private BigDecimal saldoVolumeRecuperavelAnterior;

	private BigDecimal saldoValorRecuperavelAnterior;

	private BigDecimal saldoTotalVolumeTopPercenCobrancaCliente;

	private BigDecimal saldoPenalidadeTop;

	private BigDecimal saldoVolAMaior;

	private BigDecimal saldoTotalMaiorPercenCobranca;

	private BigDecimal saldoPenalidadeAMaior;

	private String segmento;

	/**
	 * Instantiates a new relatorio cobranca recuperacao penalidades vo.
	 */
	public RelatorioCobrancaRecuperacaoPenalidadesVO() {

		this.colecaoVOs = new ArrayList<SubRelatorioCobrancaRecuperacaoPenalidadesVO>();
		this.colecaoPeriodicidadeVOs = new ArrayList<SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO>();
	}

	public Collection<SubRelatorioCobrancaRecuperacaoPenalidadesVO> getColecaoVOs() {

		return colecaoVOs;
	}

	public void setColecaoVOs(Collection<SubRelatorioCobrancaRecuperacaoPenalidadesVO> colecaoVOs) {

		this.colecaoVOs = colecaoVOs;
	}

	public String getNomeClientePontoConsumo() {

		return nomeClientePontoConsumo;
	}

	public void setNomeClientePontoConsumo(String nomeClientePontoConsumo) {

		this.nomeClientePontoConsumo = nomeClientePontoConsumo;
	}

	public BigDecimal getSaldoVolumeRecuperavelAnterior() {

		return saldoVolumeRecuperavelAnterior;
	}

	public void setSaldoVolumeRecuperavelAnterior(BigDecimal saldoVolumeRecuperavelAnterior) {

		this.saldoVolumeRecuperavelAnterior = saldoVolumeRecuperavelAnterior;
	}

	public BigDecimal getSaldoValorRecuperavelAnterior() {

		return saldoValorRecuperavelAnterior;
	}

	public void setSaldoValorRecuperavelAnterior(BigDecimal saldoValorRecuperavelAnterior) {

		this.saldoValorRecuperavelAnterior = saldoValorRecuperavelAnterior;
	}

	public Collection<SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO> getColecaoPeriodicidadeVOs() {

		return colecaoPeriodicidadeVOs;
	}

	public void setColecaoPeriodicidadeVOs(Collection<SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO> colecaoPeriodicidadeVOs) {

		this.colecaoPeriodicidadeVOs = colecaoPeriodicidadeVOs;
	}

	public BigDecimal getSaldoTotalVolumeTopPercenCobrancaCliente() {

		return saldoTotalVolumeTopPercenCobrancaCliente;
	}

	public void setSaldoTotalVolumeTopPercenCobrancaCliente(BigDecimal saldoTotalVolumeTopPercenCobrancaCliente) {

		this.saldoTotalVolumeTopPercenCobrancaCliente = saldoTotalVolumeTopPercenCobrancaCliente;
	}

	public BigDecimal getSaldoPenalidadeTop() {

		return saldoPenalidadeTop;
	}

	public void setSaldoPenalidadeTop(BigDecimal saldoPenalidadeTop) {

		this.saldoPenalidadeTop = saldoPenalidadeTop;
	}

	public BigDecimal getSaldoVolAMaior() {

		return saldoVolAMaior;
	}

	public void setSaldoVolAMaior(BigDecimal saldoVolAMaior) {

		this.saldoVolAMaior = saldoVolAMaior;
	}

	public BigDecimal getSaldoTotalMaiorPercenCobranca() {

		return saldoTotalMaiorPercenCobranca;
	}

	public void setSaldoTotalMaiorPercenCobranca(BigDecimal saldoTotalMaiorPercenCobranca) {

		this.saldoTotalMaiorPercenCobranca = saldoTotalMaiorPercenCobranca;
	}

	public BigDecimal getSaldoPenalidadeAMaior() {

		return saldoPenalidadeAMaior;
	}

	public void setSaldoPenalidadeAMaior(BigDecimal saldoPenalidadeAMaior) {

		this.saldoPenalidadeAMaior = saldoPenalidadeAMaior;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

}
