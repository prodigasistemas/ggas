package br.com.ggas.web.relatorio.dataLimiteReligamentoGas;


public class ParamentroRelatorio {

	private String nomeCliente;
	private String dataGerada;
	private String dataEncerramento;
	private String dataLimite;
	private String status;
	
	public ParamentroRelatorio(String nomeCliente, String dataGerada, String dataEncerramento, String dataLimite, String status) {
		super();
		this.nomeCliente = nomeCliente;
		this.dataGerada = dataGerada;
		this.dataEncerramento = dataEncerramento;
		this.dataLimite = dataLimite;
		this.status = status;
	}

	public ParamentroRelatorio() {
		super();
	}

	
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}

	
	public String getDataGerada() {
	
		return dataGerada;
	}

	
	public void setDataGerada(String dataGerada) {
	
		this.dataGerada = dataGerada;
	}

	
	public String getDataEncerramento() {
	
		return dataEncerramento;
	}

	
	public void setDataEncerramento(String dataEncerramento) {
	
		this.dataEncerramento = dataEncerramento;
	}

	
	public String getDataLimite() {
	
		return dataLimite;
	}

	
	public void setDataLimite(String dataLimite) {
	
		this.dataLimite = dataLimite;
	}

	
	public String getStatus() {
	
		return status;
	}

	
	public void setStatus(String status) {
	
		this.status = status;
	}
	
}
