
package br.com.ggas.web.relatorio.titulosatraso;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioTitulosAtraso;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Títulos em Atraso.
 */
@Controller
public class RelatorioTitulosAtrasoAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioTitulosAtrasoAction.class);

	private static final String DATA_REFERENCIA = "dataReferencia";

	private static final String DIAS_ATRASO = "diasAtraso";

	private static final String RELATORIO_TITULOS_ATRASO = "relatorioTitulosAtraso";

	public static final String IDS_SEGMENTOS = "idsSegmentos";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String EXIBIR_CLIENTE = "exibirCliente";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	private static final String ARRAY_PAGAMENTO = "arrayPagamento";
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioTitulosAtraso controladorRelatorioTitulosAtraso;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Exibir relatorio titulos atraso.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @return exibirRelatorioTitulosAtraso - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioTitulosAtraso")
	public String exibirRelatorioTitulosAtraso(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, Model model) throws GGASException {

		model.addAttribute("relatorioFaturamentoVO", relatorioFaturamentoVO);
		carregarDados(model);

		return "exibirRelatorioTitulosAtraso";
	}

	/**
	 * Carregar dados.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute(LISTA_SEGMENTO, listaSegmento);
	}

	/**
	 * Gerar relatorio titulos atraso.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioTitulosAtraso")
	public String gerarRelatorioTitulosAtraso(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO, BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		try {

			Map<String, Object> filtro = popularFiltroRelatorio(relatorioFaturamentoVO);
			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioFaturamentoVO.getFormatoImpressao());
			byte[] relatorio = controladorRelatorioTitulosAtraso.consultar(filtro);

			Date dataAtual = Calendar.getInstance().getTime();

			int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			int minutes = Calendar.getInstance().get(Calendar.MINUTE);
			int seconds = Calendar.getInstance().get(Calendar.SECOND);

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual);
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=" + RELATORIO_TITULOS_ATRASO + "_" + data + "_" + hours
					+ minutes + seconds + "." + relatorioFaturamentoVO.getFormatoImpressao().toLowerCase());
			response.getOutputStream().write(relatorio);

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioTitulosAtraso(relatorioFaturamentoVO, model);
		}

		model.addAttribute("relatorioFaturamentoVO", relatorioFaturamentoVO);

		return view;
	}
	
	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtroRelatorio - {@link Map}
	 */
	private Map<String, Object> popularFiltroRelatorio(RelatorioSubMenuFaturamentoVO relatorioFaturamentoVO){

		Map<String, Object> filtroRelatorio = new HashMap<String, Object>();

		String dataReferencia = relatorioFaturamentoVO.getDataReferencia();
		String formato = relatorioFaturamentoVO.getFormatoImpressao();
		String exibirCliente = relatorioFaturamentoVO.getExibirCliente();
		boolean filtros = relatorioFaturamentoVO.isExibirFiltros();
		Integer diasAtraso = relatorioFaturamentoVO.getDiasAtraso();
		Long[] idsSegmentos = relatorioFaturamentoVO.getIdsSegmentos();

		filtroRelatorio.put(DATA_REFERENCIA, dataReferencia);
		filtroRelatorio.put(DIAS_ATRASO, diasAtraso);
		filtroRelatorio.put(EXIBIR_CLIENTE, exibirCliente);
		filtroRelatorio.put(EXIBIR_FILTROS, filtros);
		filtroRelatorio.put(FORMATO_IMPRESSAO, formato);
		filtroRelatorio.put(ARRAY_PAGAMENTO, obterSituacaoPagamentoPendente());

		if (idsSegmentos != null && idsSegmentos.length > 0) {
			filtroRelatorio.put(IDS_SEGMENTOS, idsSegmentos);
		}

		return filtroRelatorio;
	}
	
	private Long[] obterSituacaoPagamentoPendente() {
		Long codigoSituacaoPagamentoPendente = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		
		Long[] arrayPagamento = {codigoSituacaoPagamentoPendente};
		return arrayPagamento; 
	}

}
