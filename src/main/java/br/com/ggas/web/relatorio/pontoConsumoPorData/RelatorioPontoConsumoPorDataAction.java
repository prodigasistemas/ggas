package br.com.ggas.web.relatorio.pontoConsumoPorData;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorData;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioRotaAction;

@Controller
@SessionAttributes({"pontoConsumo", "pontoConsumoPorDataVO"})
public class RelatorioPontoConsumoPorDataAction extends GenericAction{

	private static final Logger LOG = Logger.getLogger(RelatorioRotaAction.class);
	private static final String LISTA_SEGMENTO = "listaSegmento";

	
	@Autowired
	private ControladorRelatorioPontosDeConsumoPorData controladorPontoConsumoPorData;
	
	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();
	
	/**
	 * Método responsavel por exibir à tela.
	 * 
	 * @param mapping the mapping
	 * @param form the form
	 * @param request the request
	 * @param response the response
	 * @return the action forward
	 * @throws NegocioException 
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirPesquisaRelatorioPontoConsumoPorData")
	public ModelAndView exibirTela() throws NegocioException {
		
		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioPontoConsumoPorData");
		
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		
		return model;
	}

	/**
	 * Método responsavel por gerar o relatório.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException {@link ParseException}
	 */
	
	@RequestMapping("gerarRelatorioPontoConsumoPorData")
	public ModelAndView gerarRelatorioPontoConsumoPorData(@RequestParam("dataInicio") String dataIncial,
			@RequestParam("dataFinal") String dataFinal, HttpServletResponse response,
			@RequestParam("tipoRelatorio") String tipoRelatorio, @RequestParam("exibirFiltros") String exibirFiltros,
			@RequestParam("segmento") Segmento segmento, @RequestParam("isAtivoPrimeiraVez") Boolean isAtivoPrimeiraVez)
			throws GGASException, ParseException {

		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioPontoConsumoPorData");

		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			DateFormat formatador = new SimpleDateFormat("dd/MM/yy");
			Date inicio = null;

			if ("".equals(dataIncial)) {
				inicio = null;
			} else {
				inicio = formatador.parse(dataIncial);
			}
			Date fim = null;

			if ("".equals(dataFinal)) {
				fim = null;
			} else {
				fim = formatador.parse(dataFinal);
			}

			relatorio = controladorPontoConsumoPorData.gerarRelatorio(inicio, fim, exibirFiltros, tipoRelatorio,
					segmento, isAtivoPrimeiraVez);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=RelatórioDePontoConsumoAtivoPorData_"
					+ diaMesAno + "_" + hora + Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemAlerta(model, e.getMessage());
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemAdvertencia(model, e.getChaveErro());
		}
		
		return model;
	}
}
