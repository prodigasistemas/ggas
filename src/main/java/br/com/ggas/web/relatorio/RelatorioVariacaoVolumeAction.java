/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioVariacaoVolume;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil.TipoGrafico;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Movimentos Mensais de Consumo por Cliente.
 */
@Controller
public class RelatorioVariacaoVolumeAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioVariacaoVolumeAction.class);

	private static final String ERRO_PERIODO_NAO_INFORMADO = "ERRO_PERIODO_NAO_INFORMADO";

	public static final String PARAM_REFERENCIA = "anoMesReferencia";

	public static final String PARAM_PONTOS_CONSUMO = "pontosConsumo";

	public static final String PARAM_SEGMENTO = "descricaoSegmento";

	public static final String REFERENCIA = "referencia";

	public static final String IMOVEL = "idImovel";

	public static final String CLIENTE = "idCliente";

	public static final String SEGMENTO = "idSegmento";

	public static final String PONTOS_CONSUMO = "chavesPontoConsumo";

	public static final String TIPO_EXIBICAO = "tipoExibicao";

	public static final String EXIBIR_FILTROS = "exibirFiltros";

	public static final String EXIBIR_GRAFICO = "exibirGrafico";

	public static final String TIPO_GRAFICO = "idTipoGrafico";

	public static final String FORMATO_IMPRESSAO = "formatoImpressao";

	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	public static final String RELATORIO = "relatorio";

	public static final String POSSUI_RELATORIO = "possuiRelatorio";

	public static final String LISTA_SEGMENTO = "listaSegmento";

	public static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioVariacaoVolume controladorRelatorioVariacaoVolume;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	/**
	 * Exibir relatorio variacao volume.
	 * 
	 * @param model - Model
	 * @return exibirPesquisaRelatorioVariacaoVolume - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioVariacaoVolume")
	public String exibirRelatorioVariacaoVolume(Model model) throws GGASException {

		try {
			this.carregarDados(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaRelatorioVariacaoVolume";
	}

	/**
	 * Método responsável por exibir o relatório
	 * de analise de demanda.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String} Quando uma Exception for lançada
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioVariacaoVolume")
	public String gerarRelatorioVariacaoVolume(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		LOG.info("gerando Relatorio de Variação de Volumes baseado no mapa de filtros.");

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao());

		try {

			Map<String, Object> filtro = prepararFiltroRelatorio(relatorioSubMenuMedicaoVO);

			byte[] relatorio = controladorRelatorioVariacaoVolume.gerarRelatorioVariacaoVolume(filtro);

			if (relatorio != null) {

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorio." + StringUtils.lowerCase(relatorioSubMenuMedicaoVO.getFormatoImpressao()));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}

		} catch (IOException e) {
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioVariacaoVolume(model);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Pesquisar pontos de consumo.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return exibirRelatorioVariacaoVolume - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatorioVariacaoVolume")
	public String pesquisarPontoConsumo(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, Model model) throws GGASException {

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		try {
			if ((idImovel != null) && (idImovel > 0)) {
				listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
			} else if ((idCliente != null) && (idCliente > 0)) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return exibirRelatorioVariacaoVolume(model);

	}
	
	/**
	 * Carregar dados.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();

		model.addAttribute(LISTA_SEGMENTO, listaSegmento);
		model.addAttribute(LISTA_TIPO_GRAFICO, TipoGrafico.listar());

	}
	
	/**
	 * Método responsável por validar e preparar os parametros que serão usados na geração do relatório.
	 * 
	 * @param relatorioSubMenuMedicaoVO
	 * @return
	 * @throws NegocioException
	 */
	private Map<String, Object> prepararFiltroRelatorio(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String referencia = relatorioSubMenuMedicaoVO.getReferencia();
		Long[] pontosConsumo = relatorioSubMenuMedicaoVO.getChavesPontoConsumo();
		Long segmento = relatorioSubMenuMedicaoVO.getIdSegmento();
		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuMedicaoVO.isExibirFiltros());
		filtro.put(FORMATO_IMPRESSAO, relatorioSubMenuMedicaoVO.getFormatoImpressao());

		if (pontosConsumo != null && pontosConsumo.length > 0) {
			filtro.put(PONTOS_CONSUMO, pontosConsumo);
		} else {
			Long imovel = relatorioSubMenuMedicaoVO.getIdImovel();
			if (imovel != null && (imovel > 0)) {
				filtro.put(IMOVEL, imovel);
			}
		}

		if (segmento != null && segmento > 0) {
			filtro.put(SEGMENTO, segmento);
		}

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(CLIENTE, idCliente);
		}

		validarConverterMesAnoReferencia(filtro, referencia);

		return filtro;
	}

	/**
	 * Método responsável por validar os dados preenchidos no campo "Mês/Ano de Referência" antes de tentar converter.
	 * 
	 * @param filtro - {@link Map}
	 * @param referencia - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void validarConverterMesAnoReferencia(Map<String, Object> filtro, String referencia) throws NegocioException {
		
		if (StringUtils.isNotEmpty(referencia)) {
			filtro.put(REFERENCIA, Util.converterMesAnoEmAnoMes(referencia));
		} else {
			throw new NegocioException(ERRO_PERIODO_NAO_INFORMADO, true);
		}
		
	}

}
