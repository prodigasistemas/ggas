/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe referentes as chamadas do RelatorioPrevisaoCaptacaoVO
 * 
 @since 20/10/2014 08:12:22
 @author vpessoa
 */

package br.com.ggas.web.relatorio.faturamento;

/**
 * Classe referentes as chamadas do RelatorioPrevisaoCaptacaoVO
 * 
 * @author vpessoa
 */
public class RelatorioPrevisaoCaptacaoVO {

	private String segmento;

	private Long previsaoJaneiro;

	private Long previsaoFevereiro;

	private Long previsaoMarco;

	private Long previsaoAbril;

	private Long previsaoMaio;

	private Long previsaoJunho;

	private Long previsaoJulho;

	private Long previsaoAgosto;

	private Long previsaoSetembro;

	private Long previsaoOutubro;

	private Long previsaoNovembro;

	private Long previsaoDezembro;

	private Long captacaoJaneiro;

	private Long captacaoFevereiro;

	private Long captacaoMarco;

	private Long captacaoAbril;

	private Long captacaoMaio;

	private Long captacaoJunho;

	private Long captacaoJulho;

	private Long captacaoAgosto;

	private Long captacaoSetembro;

	private Long captacaoOutubro;

	private Long captacaoNovembro;

	private Long captacaoDezembro;

	public Long getPrevisaoJaneiro() {

		return previsaoJaneiro;
	}

	public void setPrevisaoJaneiro(Long previsaoJaneiro) {

		this.previsaoJaneiro = previsaoJaneiro;
	}

	public Long getPrevisaoFevereiro() {

		return previsaoFevereiro;
	}

	public void setPrevisaoFevereiro(Long previsaoFevereiro) {

		this.previsaoFevereiro = previsaoFevereiro;
	}

	public Long getPrevisaoMarco() {

		return previsaoMarco;
	}

	public void setPrevisaoMarco(Long previsaoMarco) {

		this.previsaoMarco = previsaoMarco;
	}

	public Long getPrevisaoAbril() {

		return previsaoAbril;
	}

	public void setPrevisaoAbril(Long previsaoAbril) {

		this.previsaoAbril = previsaoAbril;
	}

	public Long getPrevisaoMaio() {

		return previsaoMaio;
	}

	public void setPrevisaoMaio(Long previsaoMaio) {

		this.previsaoMaio = previsaoMaio;
	}

	public Long getPrevisaoJunho() {

		return previsaoJunho;
	}

	public void setPrevisaoJunho(Long previsaoJunho) {

		this.previsaoJunho = previsaoJunho;
	}

	public Long getPrevisaoJulho() {

		return previsaoJulho;
	}

	public void setPrevisaoJulho(Long previsaoJulho) {

		this.previsaoJulho = previsaoJulho;
	}

	public Long getPrevisaoAgosto() {

		return previsaoAgosto;
	}

	public void setPrevisaoAgosto(Long previsaoAgosto) {

		this.previsaoAgosto = previsaoAgosto;
	}

	public Long getPrevisaoSetembro() {

		return previsaoSetembro;
	}

	public void setPrevisaoSetembro(Long previsaoSetembro) {

		this.previsaoSetembro = previsaoSetembro;
	}

	public Long getPrevisaoOutubro() {

		return previsaoOutubro;
	}

	public void setPrevisaoOutubro(Long previsaoOutubro) {

		this.previsaoOutubro = previsaoOutubro;
	}

	public Long getPrevisaoNovembro() {

		return previsaoNovembro;
	}

	public void setPrevisaoNovembro(Long previsaoNovembro) {

		this.previsaoNovembro = previsaoNovembro;
	}

	public Long getPrevisaoDezembro() {

		return previsaoDezembro;
	}

	public void setPrevisaoDezembro(Long previsaoDezembro) {

		this.previsaoDezembro = previsaoDezembro;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public Long getCaptacaoJaneiro() {

		return captacaoJaneiro;
	}

	public void setCaptacaoJaneiro(Long captacaoJaneiro) {

		this.captacaoJaneiro = captacaoJaneiro;
	}

	public Long getCaptacaoFevereiro() {

		return captacaoFevereiro;
	}

	public void setCaptacaoFevereiro(Long captacaoFevereiro) {

		this.captacaoFevereiro = captacaoFevereiro;
	}

	public Long getCaptacaoMarco() {

		return captacaoMarco;
	}

	public void setCaptacaoMarco(Long captacaoMarco) {

		this.captacaoMarco = captacaoMarco;
	}

	public Long getCaptacaoAbril() {

		return captacaoAbril;
	}

	public void setCaptacaoAbril(Long captacaoAbril) {

		this.captacaoAbril = captacaoAbril;
	}

	public Long getCaptacaoMaio() {

		return captacaoMaio;
	}

	public void setCaptacaoMaio(Long captacaoMaio) {

		this.captacaoMaio = captacaoMaio;
	}

	public Long getCaptacaoJunho() {

		return captacaoJunho;
	}

	public void setCaptacaoJunho(Long captacaoJunho) {

		this.captacaoJunho = captacaoJunho;
	}

	public Long getCaptacaoJulho() {

		return captacaoJulho;
	}

	public void setCaptacaoJulho(Long captacaoJulho) {

		this.captacaoJulho = captacaoJulho;
	}

	public Long getCaptacaoAgosto() {

		return captacaoAgosto;
	}

	public void setCaptacaoAgosto(Long captacaoAgosto) {

		this.captacaoAgosto = captacaoAgosto;
	}

	public Long getCaptacaoSetembro() {

		return captacaoSetembro;
	}

	public void setCaptacaoSetembro(Long captacaoSetembro) {

		this.captacaoSetembro = captacaoSetembro;
	}

	public Long getCaptacaoOutubro() {

		return captacaoOutubro;
	}

	public void setCaptacaoOutubro(Long captacaoOutubro) {

		this.captacaoOutubro = captacaoOutubro;
	}

	public Long getCaptacaoNovembro() {

		return captacaoNovembro;
	}

	public void setCaptacaoNovembro(Long captacaoNovembro) {

		this.captacaoNovembro = captacaoNovembro;
	}

	public Long getCaptacaoDezembro() {

		return captacaoDezembro;
	}

	public void setCaptacaoDezembro(Long captacaoDezembro) {

		this.captacaoDezembro = captacaoDezembro;
	}

}
