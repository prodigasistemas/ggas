
package br.com.ggas.web.relatorio.volumesfaturadossegmento;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.relatorio.ControladorRelatorioVolumesFaturadosSegmento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Volumes Faturados por Segmento.
 */
@Controller
public class RelatorioVolumesFaturadosSegmentoAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioVolumesFaturadosSegmentoAction.class);

	public static final String PARAM_DATA_EMISSAO_INICIAL = "dataInicial";

	public static final String PARAM_DATA_EMISSAO_FINAL = "dataFinal";

	public static final String PARAM_ANO_MES_NUMERO_CICLO = "anoMesReferenciaNumeroCiclo";

	public static final String IDS_SEGMENTOS = "idsSegmentos";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_CICLO = "listaCiclo";

	private static final String RELATORIO = "relatorio";

	private static final String RELATORIO_VOLUMES_FATURADOS_SEGMENTO = "relatorioVolumesFaturadosSegmentos";

	public static final String TIPO_EXIBICAO_ANALITICO = "analitico";

	public static final String TIPO_EXIBICAO_SINTETICO = "sintetico";

	public static final String TIPO_RELATORIO = "tipoExibicao";

	public static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	public static final String NUMERO_CICLO = "numeroCiclo";

	public static final String EXIBIR_VALORES_NULOS = "exibirValoresNulos";

	public static final String EXIBIR_FILTROS = "exibirFiltros";
	
	public static final String FORMATO_IMPRESSAO = "formatoImpressao";
	
	public static final String CONTRATOS_ATIVOS = "contratosAtivos";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorRelatorioVolumesFaturadosSegmento controladorRelatorioVolumesFaturadosSegmento;
	
	/**
	 * Exibir relatorio volumes faturados segmento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @return exibirRelatorioVolumesFaturadosSegmento - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioVolumesFaturadosSegmento")
	public String exibirRelatorioVolumesFaturadosSegmento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Model model)
			throws GGASException {

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);

		try {
			this.carregarDados(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirRelatorioVolumesFaturadosSegmento";
	}
	
	/**
	 * Gerar relatorio volumes faturados segmento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioVolumesFaturadosSegmento")
	public String gerarRelatorioVolumesFaturadosSegmento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		try {
			Map<String, Object> filtro = prepararFiltro(relatorioSubMenuFaturamentoVO, request, model);
			byte[] relatorio = controladorRelatorioVolumesFaturadosSegmento.consultar(filtro);
			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuFaturamentoVO.getFormatoImpressao());

			montarHtmlResponse(request, response, relatorio, formatoImpressao);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioVolumesFaturadosSegmento(relatorioSubMenuFaturamentoVO, model);
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);

		return view;
	}
	
	/**
	 * Montar html response.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param relatorio - {@link Byte}
	 * @param formatoImpressao - {@link FormatoImpressao}
	 * @throws GGASException - {@link GGASException}
	 */
	private void montarHtmlResponse(HttpServletRequest request, HttpServletResponse response, byte[] relatorio,
			FormatoImpressao formatoImpressao) throws GGASException {

		if (relatorio != null) {
			try {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				request.getSession().setAttribute(RELATORIO, relatorio);
				request.setAttribute(EXIBIR_MENSAGEM_TELA, false);
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						Util.montarRelatorioContentDisposition(RELATORIO_VOLUMES_FATURADOS_SEGMENTO, formatoImpressao.name()));
				response.getOutputStream().write(relatorio);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		}

		request.getSession().removeAttribute(RELATORIO);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);
	}
	
	/**
	 * Carregar dados.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarDados(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		Collection<Integer> listarCiclos = controladorRota.listarCiclos();

		model.addAttribute(LISTA_SEGMENTO, listaSegmento);
		model.addAttribute(LISTA_CICLO, listarCiclos);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, HttpServletRequest request,
			Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		String tipoRelatorio = relatorioSubMenuFaturamentoVO.getTipoExibicao();
		String dataInicial = relatorioSubMenuFaturamentoVO.getDataInicial();
		String dataFinal = relatorioSubMenuFaturamentoVO.getDataFinal();
		String anoMesReferenciaString = relatorioSubMenuFaturamentoVO.getAnoMesReferencia();
		Integer numeroCiclo = relatorioSubMenuFaturamentoVO.getNumeroCiclo();
		Long[] idsSegmentos = relatorioSubMenuFaturamentoVO.getIdsSegmentos();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuFaturamentoVO.isExibirFiltros());
		filtro.put(EXIBIR_VALORES_NULOS, relatorioSubMenuFaturamentoVO.isExibirValoresNulos());
		filtro.put(CONTRATOS_ATIVOS, relatorioSubMenuFaturamentoVO.isExibirTambemContratosInativos());
		filtro.put(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(relatorioSubMenuFaturamentoVO.getFormatoImpressao()));

		// Tipo de Exibição Relatório
		if (StringUtils.isNotEmpty(tipoRelatorio)) {
			filtro.put(TIPO_RELATORIO, tipoRelatorio);
		}

		// Lista de segmentos selecionados
		if (ArrayUtils.isNotEmpty(idsSegmentos)) {
			model.addAttribute(IDS_SEGMENTOS, idsSegmentos);
			request.getSession().setAttribute(IDS_SEGMENTOS, idsSegmentos);
			filtro.put(IDS_SEGMENTOS, idsSegmentos);
		}

		// Período de emissão
		if (StringUtils.isNotEmpty(dataInicial)) {
			filtro.put("dataEmissaoInicial",
					Util.converterCampoStringParaData("Período de Emissão Inicial", dataInicial, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataFinal)) {
			filtro.put("dataEmissaoFinal",
					Util.converterCampoStringParaData("Período de Emissão Final", dataFinal, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(anoMesReferenciaString)) {
			anoMesReferenciaString = anoMesReferenciaString.replaceAll("/", "").replaceAll("-", "");
			filtro.put(ANO_MES_REFERENCIA, Util.converterCampoStringParaValorInteger("Ano / Mês de referência", anoMesReferenciaString));
		}

		if ((numeroCiclo != null) && (numeroCiclo > 0)) {
			filtro.put(NUMERO_CICLO, numeroCiclo);
		}

		return filtro;

	}
	
}
