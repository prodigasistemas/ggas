package br.com.ggas.web.relatorio.pontoConsumoPorData;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.util.ServiceLocator;

@Repository
public class RepositorioPontoConsumoPorData extends RepositorioGenerico {

	@Autowired
	public RepositorioPontoConsumoPorData(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadeMedidorInstalacao() {
		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}
	
	public Class<?> getClasseEntidadeContratoPontoConsumoItemFaturamento() {
		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}
	
	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {
		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	/**
	 * Listar Pontos de Consumo por data.
	 * @param dataInicio {@link Date}
	 * @param dataFinal {@link Date}
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<PontoConsumoPorDataVO> listarPontosConsumoAtivoPorData(Date dataInicio, Date dataFinal, Segmento segmento, Boolean isAtivoPrimeiraVez) {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente.nome as nomeCliente, " + "pontoConsumo.descricao as descricaoPontoConsumo, "
				+ "segmento.descricao as segmento, " + "ramoAtividade.descricao as ramoAtividade, " + "cep.cep as cep, "
				+ "pontoConsumo.numeroImovel as numero, " + "rota.numeroRota as rota," + "instalacaoMedidor.dataAtivacao as dataAtivacao," + "contratoPontoConsumoItemFaturamento.tarifa.descricao as tarifa");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" contratoPontoConsumoItemFaturamento ");
		hql.append(" inner join contratoPontoConsumoItemFaturamento.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel imovel ");
		hql.append(" inner join imovel.listaClienteImovel clienteImovel ");
		hql.append(" inner join clienteImovel.cliente cliente ");
		hql.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join instalacaoMedidor.medidor medidor ");
		hql.append(" inner join pontoConsumo.segmento segmento ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" inner join pontoConsumo.ramoAtividade ramoAtividade ");
		hql.append(" inner join pontoConsumo.cep cep ");
		
		hql.append(" where 1 = 1 ");

		if (dataInicio != null) {
			hql.append(" and instalacaoMedidor.dataAtivacao >= :inicio ");
		}
		
		if (dataFinal != null) {
				hql.append(" and instalacaoMedidor.dataAtivacao <= :fim ");
		}
		
		hql.append(" and pontoConsumo.habilitado = true ");
		
		
		if(segmento != null && segmento.getChavePrimaria() > 0) {
			hql.append(" and pontoConsumo.segmento.chavePrimaria = :segmento ");
		}
		
		if(isAtivoPrimeiraVez!= null) {
			hql.append(" and (select count(*) ");
			hql.append(" from ");
			hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
			hql.append(" historicoMedidor ");
			hql.append(" where ");
			hql.append(" pontoConsumo.chavePrimaria = historicoMedidor.pontoConsumo.chavePrimaria ");
			hql.append(" and historicoMedidor.operacaoMedidor.chavePrimaria in (:operacaoAtivacao, :operacaoReativacao) )");

			
			if(isAtivoPrimeiraVez) {
				hql.append(" = 1 ");
			} else {
				hql.append(" > 1 ");
			}

		}

		hql.append(" order by pontoConsumo.descricao, contratoPontoConsumoItemFaturamento.ultimaAlteracao desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(PontoConsumoPorDataVO.class));
		if (dataInicio != null) {
			query.setDate("inicio", dataInicio);
		}
		if (dataFinal != null) {
			query.setDate("fim", dataFinal);
		}
		
		if (segmento != null && segmento.getChavePrimaria() > 0) {
			query.setLong("segmento", segmento.getChavePrimaria());
		}
		
		if(isAtivoPrimeiraVez!= null) {
			query.setLong("operacaoAtivacao", OperacaoMedidor.CODIGO_ATIVACAO);
			query.setLong("operacaoReativacao", OperacaoMedidor.CODIGO_REATIVACAO);

		}
		
		Collection<PontoConsumoPorDataVO> lista = query.list();
		
		Set<String> set = new HashSet<>(lista.size());

		lista.removeIf(p -> !set.add(p.getDescricaoPontoConsumo()));
		
		return lista;
	}

}
