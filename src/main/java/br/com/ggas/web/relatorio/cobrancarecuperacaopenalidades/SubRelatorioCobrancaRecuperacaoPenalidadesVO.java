/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 29/08/2014 15:28:55
 @author rfilho
 */

package br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SubRelatorioCobrancaRecuperacaoPenalidadesVO implements Serializable {

	private static final long serialVersionUID = -3459326862812304584L;

	private Date dataInicio;

	private Date dataFim;

	private BigDecimal volumeCobrado;

	private BigDecimal valorCobrado;

	private BigDecimal volumeRecuperavel;

	private BigDecimal valorRecuperavel;

	private BigDecimal volumeRecuperado;

	private BigDecimal valorRecuperado;

	private String historicoCobrancaRecuperacao;

	private String numeroDocumento;

	private String dataEmissao;

	private String dataVencimento;

	private String quitado;

	private BigDecimal saldoRecuperavel;

	private BigDecimal saldoARecuperar;

	public Date getDataInicio() {
		Date data = null;
		if (this.dataInicio != null) {
			data = (Date) dataInicio.clone();
		}
		return data;
	}

	public void setDataInicio(Date dataInicio) {

		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		Date data = null;
		if (this.dataFim != null) {
			data = (Date) dataFim.clone();
		}
		return data;
	}

	public void setDataFim(Date dataFim) {

		this.dataFim = dataFim;
	}

	public BigDecimal getVolumeCobrado() {

		return volumeCobrado;
	}

	public void setVolumeCobrado(BigDecimal volumeCobrado) {

		this.volumeCobrado = volumeCobrado;
	}

	public BigDecimal getValorCobrado() {

		return valorCobrado;
	}

	public void setValorCobrado(BigDecimal valorCobrado) {

		this.valorCobrado = valorCobrado;
	}

	public BigDecimal getVolumeRecuperavel() {

		return volumeRecuperavel;
	}

	public void setVolumeRecuperavel(BigDecimal volumeRecuperavel) {

		this.volumeRecuperavel = volumeRecuperavel;
	}

	public BigDecimal getValorRecuperavel() {

		return valorRecuperavel;
	}

	public void setValorRecuperavel(BigDecimal valorRecuperavel) {

		this.valorRecuperavel = valorRecuperavel;
	}

	public BigDecimal getVolumeRecuperado() {

		return volumeRecuperado;
	}

	public void setVolumeRecuperado(BigDecimal volumeRecuperado) {

		this.volumeRecuperado = volumeRecuperado;
	}

	public BigDecimal getValorRecuperado() {

		return valorRecuperado;
	}

	public void setValorRecuperado(BigDecimal valorRecuperado) {

		this.valorRecuperado = valorRecuperado;
	}

	public String getHistoricoCobrancaRecuperacao() {

		return historicoCobrancaRecuperacao;
	}

	public void setHistoricoCobrancaRecuperacao(String historicoCobrancaRecuperacao) {

		this.historicoCobrancaRecuperacao = historicoCobrancaRecuperacao;
	}

	public String getNumeroDocumento() {

		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {

		this.numeroDocumento = numeroDocumento;
	}

	public String getDataEmissao() {

		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getDataVencimento() {

		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public String getQuitado() {

		return quitado;
	}

	public void setQuitado(String quitado) {

		this.quitado = quitado;
	}

	public BigDecimal getSaldoRecuperavel() {

		return saldoRecuperavel;
	}

	public void setSaldoRecuperavel(BigDecimal saldoRecuperavel) {

		this.saldoRecuperavel = saldoRecuperavel;
	}

	public BigDecimal getSaldoARecuperar() {

		return saldoARecuperar;
	}

	public void setSaldoARecuperar(BigDecimal saldoARecuperar) {

		this.saldoARecuperar = saldoARecuperar;
	}

}
