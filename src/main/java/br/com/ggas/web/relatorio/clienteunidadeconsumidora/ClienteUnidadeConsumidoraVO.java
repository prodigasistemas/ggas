/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.clienteunidadeconsumidora;

import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;

public class ClienteUnidadeConsumidoraVO {

	private String nomeImovel;

	private String endereco;

	private String bairro;

	private String cidade;

	private String modalidadeMedicao;

	private Integer numeroUCIndividual;

	private Integer numeroPontoConsumo;

	private Integer numeroUCImovel;

	/**
	 * Instantiates a new cliente unidade consumidora vo.
	 */
	public ClienteUnidadeConsumidoraVO() {

	}

	/**
	 * Instantiates a new cliente unidade consumidora vo.
	 * 
	 * @param nomeImovel
	 *            the nome imovel
	 * @param endereco
	 *            the endereco
	 * @param bairro
	 *            the bairro
	 * @param cidade
	 *            the cidade
	 * @param numeroUCIndividual
	 *            the numero uc individual
	 * @param numeroPontoConsumo
	 *            the numero ponto consumo
	 * @param numeroUCImovel
	 *            the numero uc imovel
	 * @param modalidadeMedicao
	 *            the modalidade medicao
	 */
	public ClienteUnidadeConsumidoraVO(String nomeImovel, String endereco, String bairro, String cidade, Number numeroUCIndividual,
										Number numeroPontoConsumo, Number numeroUCImovel, String modalidadeMedicao) {

		this.nomeImovel = nomeImovel;
		this.endereco = endereco;
		this.bairro = bairro;
		this.cidade = cidade;
		this.numeroUCIndividual = numeroUCIndividual.intValue();
		this.numeroPontoConsumo = numeroPontoConsumo.intValue();
		this.numeroUCImovel = numeroUCImovel.intValue();
		this.modalidadeMedicao = modalidadeMedicao;
	}

	public String getNomeImovel() {

		return nomeImovel;
	}

	public void setNomeImovel(String nomeImovel) {

		this.nomeImovel = nomeImovel;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getModalidadeMedicao() {

		return modalidadeMedicao;
	}

	public void setModalidadeMedicao(String modalidadeMedicao) {

		this.modalidadeMedicao = modalidadeMedicao;
	}

	public Integer getNumeroUCIndividual() {

		return numeroUCIndividual;
	}

	public void setNumeroUCIndividual(Integer numeroUCIndividual) {

		this.numeroUCIndividual = numeroUCIndividual;
	}

	public Integer getNumeroPontoConsumo() {

		return numeroPontoConsumo;
	}

	public void setNumeroPontoConsumo(Integer numeroPontoConsumo) {

		this.numeroPontoConsumo = numeroPontoConsumo;
	}

	public Integer getNumeroUCImovel() {

		return numeroUCImovel;
	}

	public void setNumeroUCImovel(Integer numeroUCImovel) {

		this.numeroUCImovel = numeroUCImovel;
	}

	public Integer getNumeroUC() {

		Integer numeroUC = 0;

		if (ModalidadeMedicaoImovel.DESCRICAO_INDIVIDUAL.equals(modalidadeMedicao)) {

			if (numeroUCIndividual != null) {
				numeroUC = numeroUCIndividual;
			} else {
				numeroUC = 0;
			}
			
		} else if(ModalidadeMedicaoImovel.DESCRICAO_COLETIVA.equals(modalidadeMedicao)) {
			
			if (numeroUCImovel != null) {
				numeroUC = numeroUCImovel;
			} else {
				numeroUC = 0;
			}
			

			if(numeroPontoConsumo != null && numeroPontoConsumo > 0) {
				numeroUC = numeroUC + 1;
			}
		}

		return numeroUC;
	}

}
