package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes as funcionalidades do módulo Relatório no sub-menu Cadastro.
 * 
 * @author bruno silva
 * 
 * @see RelatorioRotaAction, RelatorioClienteUnidadeConsumidoraAction, RelatorioClienteHistoricoAction
 */
public class RelatorioCadastroVO {

	private String numeroRota;
	private String formatoImpressao;
	private String dataEmissaoInicial;
	private String dataEmissaoFinal;
	private String dataVencimentoInicial;
	private String dataVencimentoFinal;
	private String situacao;

	private Boolean habilitado = true;
	private boolean exibirFiltros;
	
	private Long idPeriodicidade;
	private Long idTipoRota;
	private Long idTipoLeitura;
	private Long idGrupoFaturamento;
	private Long idSetorComercial;
	private Long idEmpresa;
	private Long idLeiturista;
	private Long idImovel;
	private Long idCliente;
	private Long idModalidadeMedicao;
	private Long codigoPontoConsumo;
	private Long chavePrimaria;
	private Long[] chavesPrimarias;
	
	
	/**
	 * @return numeroRota - {@link String}
	 */
	public String getNumeroRota() {
		return numeroRota;
	}
	/**
	 * @param numeroRota - {@link String}
	 */
	public void setNumeroRota(String numeroRota) {
		this.numeroRota = numeroRota;
	}
	/**
	 * @return formatoImpressao - {@link String}
	 */
	public String getFormatoImpressao() {
		return formatoImpressao;
	}
	/**
	 * @param formatoImpressao - {@link String}
	 */
	public void setFormatoImpressao(String formatoImpressao) {
		
		if(("").equals(formatoImpressao)) {
			formatoImpressao = "PDF";
		}
		
		this.formatoImpressao = formatoImpressao;
	}
	/**
	 * @return dataEmissaoInicial - {@link String}
	 */
	public String getDataEmissaoInicial() {
		return dataEmissaoInicial;
	}
	/**
	 * @param dataEmissaoInicial - {@link String}
	 */
	public void setDataEmissaoInicial(String dataEmissaoInicial) {
		this.dataEmissaoInicial = dataEmissaoInicial;
	}
	/**
	 * @return dataEmissaoFinal - {@link String}
	 */
	public String getDataEmissaoFinal() {
		return dataEmissaoFinal;
	}
	/**
	 * @param dataEmissaoFinal - {@link String}
	 */
	public void setDataEmissaoFinal(String dataEmissaoFinal) {
		this.dataEmissaoFinal = dataEmissaoFinal;
	}
	/**
	 * @return dataVencimentoInicial - {@link String}
	 */
	public String getDataVencimentoInicial() {
		return dataVencimentoInicial;
	}
	/**
	 * @param dataVencimentoInicial - {@link String}
	 */
	public void setDataVencimentoInicial(String dataVencimentoInicial) {
		this.dataVencimentoInicial = dataVencimentoInicial;
	}
	/**
	 * @return dataVencimentoFinal - {@link String}
	 */
	public String getDataVencimentoFinal() {
		return dataVencimentoFinal;
	}
	/**
	 * @param dataVencimentoFinal - {@link String}
	 */
	public void setDataVencimentoFinal(String dataVencimentoFinal) {
		this.dataVencimentoFinal = dataVencimentoFinal;
	}
	/**
	 * @return situacao - {@link String}
	 */
	public String getSituacao() {
		return situacao;
	}
	/**
	 * @param situacao - {@link String}
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	
	/**
	 * @return habilitado - {@link Boolean}
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}
	/**
	 * @param habilitado - {@link Boolean}
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * @return exibirFiltros - {@link Boolean}
	 */
	public Boolean getExibirFiltros() {
		return exibirFiltros;
	}
	/**
	 * @param exibirFiltros - {@link Boolean}
	 */
	public void setExibirFiltros(Boolean exibirFiltros) {
		this.exibirFiltros = exibirFiltros;
	}
	

	/**
	 * @return idPeriodicidade - {@link Long}
	 */
	public Long getIdPeriodicidade() {
		return idPeriodicidade;
	}
	/**
	 * @param idPeriodicidade - {@link Long}
	 */
	public void setIdPeriodicidade(Long idPeriodicidade) {
		this.idPeriodicidade = idPeriodicidade;
	}
	/**
	 * @return idTipoRota - {@link Long}
	 */
	public Long getIdTipoRota() {
		return idTipoRota;
	}
	/**
	 * @param idTipoRota - {@link Long}
	 */
	public void setIdTipoRota(Long idTipoRota) {
		this.idTipoRota = idTipoRota;
	}
	/**
	 * @return idTipoLeitura - {@link Long}
	 */
	public Long getIdTipoLeitura() {
		return idTipoLeitura;
	}
	/**
	 * @param idTipoLeitura - {@link Long}
	 */
	public void setIdTipoLeitura(Long idTipoLeitura) {
		this.idTipoLeitura = idTipoLeitura;
	}
	/**
	 * @return idGrupoFaturamento - {@link Long}
	 */
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	/**
	 * @param idGrupoFaturamento - {@link Long}
	 */
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	/**
	 * @return idSetorComercial - {@link Long}
	 */
	public Long getIdSetorComercial() {
		return idSetorComercial;
	}
	/**
	 * @param idSetorComercial - {@link Long}
	 */
	public void setIdSetorComercial(Long idSetorComercial) {
		this.idSetorComercial = idSetorComercial;
	}
	/**
	 * @return idEmpresa - {@link Long}
	 */
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	/**
	 * @param idEmpresa - {@link Long}
	 */
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	/**
	 * @return idLeiturista - {@link Long}
	 */
	public Long getIdLeiturista() {
		return idLeiturista;
	}
	/**
	 * @param idLeiturista - {@link Long}
	 */
	public void setIdLeiturista(Long idLeiturista) {
		this.idLeiturista = idLeiturista;
	}
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return idModalidadeMedicao - {@link Long}
	 */
	public Long getIdModalidadeMedicao() {
		return idModalidadeMedicao;
	}
	/**
	 * @param idModalidadeMedicao - {@link Long}
	 */
	public void setIdModalidadeMedicao(Long idModalidadeMedicao) {
		this.idModalidadeMedicao = idModalidadeMedicao;
	}
	/**
	 * @return codigoPontoConsumo - {@link Long}
	 */
	public Long getCodigoPontoConsumo() {
		return codigoPontoConsumo;
	}
	/**
	 * @param idModalidadeMedicao - {@link Long}
	 */
	public void setCodigoPontoConsumo(Long codigoPontoConsumo) {
		this.codigoPontoConsumo = codigoPontoConsumo;
	}
	/**
	 * @return chavePrimaria - {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria - {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	/**
	 * @return chavesPrimariasTmp - {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] chavesPrimariasTmp = null;
		if (chavesPrimarias != null) {
			chavesPrimariasTmp = chavesPrimarias.clone();
		}
		return chavesPrimariasTmp;
	}
	/**
	 * @param chavesPrimarias - {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}
	

}
