package br.com.ggas.web.relatorio.prazoMaximoDeReligacaoPorCorteIndevido;

import java.util.Date;

public class ServicoAutorizacaoRelatorioVO {

	private String nomeCliente;
	private Date dataGerada;
	private Date dataEncerramento;
	
	public ServicoAutorizacaoRelatorioVO(String nomeCliente, Date dataGerada, Date dataEncerramento) {
		super();
		this.nomeCliente = nomeCliente;
		this.dataGerada = dataGerada;
		this.dataEncerramento = dataEncerramento;
	}

	public ServicoAutorizacaoRelatorioVO() {
		super();
	}

	
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}

	
	public Date getDataGerada() {
	
		return dataGerada;
	}

	
	public void setDataGerada(Date dataGerada) {
	
		this.dataGerada = dataGerada;
	}

	
	public Date getDataEncerramento() {
	
		return dataEncerramento;
	}

	
	public void setDataEncerramento(Date dataEncerramento) {
	
		this.dataEncerramento = dataEncerramento;
	}
}

