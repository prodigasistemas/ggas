/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.perfilconsumocliente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioPerfilConsumo;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil.TipoGrafico;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuMedicaoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Perfil de Consumo por Cliente.
 */
@Controller
public class RelatorioPerfilConsumoClienteAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioPerfilConsumoClienteAction.class);

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String ID_MODALIDADE_MEDICAO = "idModalidadeMedicao";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String DATA_ASSINATURA = "dataAssinatura";

	private static final String DATA_INICIO = "dataInicio";

	private static final String DATA_ENCERRAMENTO = "dataEncerramento";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";
	
	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorRelatorioPerfilConsumo controladorRelatorioPerfilConsumo;

	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	/**
	 * Exibir relatorio perfil consumo cliente.
	 * 
	 * @param model - {@link Model}
	 * @return exibirRelatorioPerfilConsumoCliente - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioPerfilConsumoCliente")
	public String exibirRelatorioPerfilConsumoCliente(Model model) throws GGASException {

		try {
			popularFiltrosRelatorioPerfilConsumoCliente(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(LISTA_TIPO_GRAFICO, TipoGrafico.listar());

		return "exibirRelatorioPerfilConsumoCliente";
	}

	/**
	 * Pesquisar ponto consumo relatorio perfil consumo cliente.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return exibirRelatorioPerfilConsumoCliente - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoRelatorioPerfilConsumoCliente")
	public String pesquisarPontoConsumoRelatorioPerfilConsumoCliente(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO,
			BindingResult result, ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, Model model)
			throws GGASException {

		try {
			this.pesquisarPontosConsumo(relatorioSubMenuMedicaoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);
		model.addAttribute(LISTA_TIPO_GRAFICO, TipoGrafico.listar());

		return exibirRelatorioPerfilConsumoCliente(model);
	}

	/**
	 * Pesquisar pontos consumo.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void pesquisarPontosConsumo(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Model model) throws GGASException {

		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();
		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}

	/**
	 * Popular filtros relatorio perfil consumo cliente.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularFiltrosRelatorioPerfilConsumoCliente(Model model) throws GGASException {

		Collection<String> listaAnos = Util.listarAnos("20");

		((List<String>) listaAnos).remove(0);
		model.addAttribute("listaAnos", listaAnos);

		model.addAttribute("listaModalidadeMedicao", controladorImovel.listarModalidadeMedicaoImovel());
		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		model.addAttribute("listaSegmento", listaSegmento);
	}

	/**
	 * Gerar relatorio perfil consumo cliente.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @return view - {@link String} Quando uma Exception for lançada
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioPerfilConsumoCliente")
	public String gerarRelatorioPerfilConsumoCliente(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, Model model, HttpServletRequest request,
			HttpServletResponse response) throws GGASException {

		String view = null;

		Map<String, Object> filtro = new HashMap<String, Object>();
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuMedicaoVO.getFormatoImpressao());

		try {
			
			this.prepararFiltro(relatorioSubMenuMedicaoVO, filtro);

			byte[] relatorioPerfilConsumo = controladorRelatorioPerfilConsumo.gerarRelatorio(filtro, formatoImpressao);
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioPerfilConsumo.length);
			response.addHeader("Content-Disposition",
					"attachment; filename=relatorioPerfilConsumo" + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorioPerfilConsumo, 0, relatorioPerfilConsumo.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			mensagemErroParametrizado(model, request, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirRelatorioPerfilConsumoCliente(model);
		}

		model.addAttribute("relatorioSubMenuMedicaoVO", relatorioSubMenuMedicaoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private void prepararFiltro(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Map<String, Object> filtro) throws GGASException {

		String numeroContrato = relatorioSubMenuMedicaoVO.getNumeroContrato();
		String dataAssinatura = relatorioSubMenuMedicaoVO.getDataAssinatura();
		String dataInicio = relatorioSubMenuMedicaoVO.getDataInicio();
		String dataEncerramento = relatorioSubMenuMedicaoVO.getDataEncerramento();
		String anoSelecionado = relatorioSubMenuMedicaoVO.getAno();
		String dataEmissaoInicial = "01/01/";
		String dataEmissaoFinal = "31/12/";
		Long idSegmento = relatorioSubMenuMedicaoVO.getIdSegmento();

		filtro.put(EXIBIR_FILTROS, relatorioSubMenuMedicaoVO.isExibirFiltros());

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTRATO, numeroContrato);
		}

		if (StringUtils.isNotEmpty(dataAssinatura)) {
			filtro.put(DATA_ASSINATURA, dataAssinatura);
		}

		if (StringUtils.isNotEmpty(dataInicio)) {
			filtro.put(DATA_INICIO, dataInicio);
		}

		if (StringUtils.isNotEmpty(dataEncerramento)) {
			filtro.put(DATA_ENCERRAMENTO, dataEncerramento);
		}

		/*
		 * os campos de períodos são setados no filtro como "dataEmissaoInicial" e "dataEmissaoFinal" pois serão usados ao consultar as
		 * faturas. o periodo exibido será o do ano inteiro selecionado.
		 */

		if (!("-1").equals(anoSelecionado)) {
			dataEmissaoInicial += anoSelecionado;
			dataEmissaoFinal += anoSelecionado;
			filtro.put("anoSelecionado", anoSelecionado);
		}

		if (StringUtils.isNotEmpty(dataEmissaoInicial)) {
			filtro.put("dataEmissaoInicial",
					Util.converterCampoStringParaData("dataEmissaoInicial", dataEmissaoInicial, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataEmissaoFinal)) {
			filtro.put("dataEmissaoFinal",
					Util.converterCampoStringParaData("dataEmissaoFinal", dataEmissaoFinal, Constantes.FORMATO_DATA_BR));
		}
		
		if ((idSegmento != null) && (idSegmento > 0)) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		prepararFiltroDois(relatorioSubMenuMedicaoVO, filtro);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuMedicaoVO - {@link RelatorioSubMenuMedicaoVO}
	 * @param filtro - {@link Map}
	 */
	private void prepararFiltroDois(RelatorioSubMenuMedicaoVO relatorioSubMenuMedicaoVO, Map<String, Object> filtro) {

		Long idCliente = relatorioSubMenuMedicaoVO.getIdCliente();
		Long idImovel = relatorioSubMenuMedicaoVO.getIdImovel();
		Long[] idsPontoConsumo = relatorioSubMenuMedicaoVO.getChavesPontoConsumo();
		Long idModalidadeMedicao = relatorioSubMenuMedicaoVO.getIdModalidadeMedicao();

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if ((idImovel != null) && (idImovel > 0)) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
			filtro.put(CHAVES_PONTO_CONSUMO, idsPontoConsumo);
		}

		if ((idModalidadeMedicao != null) && (idModalidadeMedicao > 0)) {
			filtro.put(ID_MODALIDADE_MEDICAO, idModalidadeMedicao);
		}
	}

}
