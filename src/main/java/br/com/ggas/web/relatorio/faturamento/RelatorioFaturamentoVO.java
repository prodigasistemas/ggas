/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturamento;

import java.math.BigDecimal;
import java.util.Date;
/**
 * Classe responsável pela representação da entidade RelatorioFaturamento.
 *
 */
public class RelatorioFaturamentoVO {

	private String codigoCliente;

	private String contrato;

	private String razaoSocial;

	private String nomeFantasia;

	private String segmento;

	private String numeroNF;

	private Date dataEmissao;

	private Date dataVencimento;

	private BigDecimal valorIcms;

	private BigDecimal volumeFaturado;

	private Integer qtdDias;

	private BigDecimal m3Dias;

	private BigDecimal tarifaUnitaria;

	private BigDecimal valorFaturado;

	private String pontoConsumo;

	public String getCodigoCliente() {

		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {

		this.codigoCliente = codigoCliente;
	}

	public String getContrato() {

		return contrato;
	}

	public void setContrato(String contrato) {

		this.contrato = contrato;
	}

	public String getRazaoSocial() {

		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {

		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getNumeroNF() {

		return numeroNF;
	}

	public void setNumeroNF(String numeroNF) {

		this.numeroNF = numeroNF;
	}

	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public BigDecimal getValorIcms() {

		return valorIcms;
	}

	public void setValorIcms(BigDecimal valorIcms) {

		this.valorIcms = valorIcms;
	}

	public BigDecimal getVolumeFaturado() {

		return volumeFaturado;
	}

	public void setVolumeFaturado(BigDecimal volumeFaturado) {

		this.volumeFaturado = volumeFaturado;
	}

	public Integer getQtdDias() {

		return qtdDias;
	}

	public void setQtdDias(Integer qtdDias) {

		this.qtdDias = qtdDias;
	}

	public BigDecimal getM3Dias() {

		return m3Dias;
	}

	public void setM3Dias(BigDecimal m3Dias) {

		this.m3Dias = m3Dias;
	}

	public BigDecimal getTarifaUnitaria() {

		return tarifaUnitaria;
	}

	public void setTarifaUnitaria(BigDecimal tarifaUnitaria) {

		this.tarifaUnitaria = tarifaUnitaria;
	}

	public BigDecimal getValorFaturado() {

		return valorFaturado;
	}

	public void setValorFaturado(BigDecimal valorFaturado) {

		this.valorFaturado = valorFaturado;
	}

	public String getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}
}
