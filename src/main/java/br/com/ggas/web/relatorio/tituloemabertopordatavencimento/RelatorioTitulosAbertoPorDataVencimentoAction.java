/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 02/01/2014 11:27:36
 @author ccavalcanti
 */

package br.com.ggas.web.relatorio.tituloemabertopordatavencimento;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioTituloAbertoPorDataVencimento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * TTítulos em Aberto por Data Vencimento.
 */
@Controller
public class RelatorioTitulosAbertoPorDataVencimentoAction extends GenericAction {

	private static final String RELATORIO_SUB_MENU_FATURAMENTO_VO = "relatorioSubMenuFaturamentoVO";

	private static final String EXIBIR_RELATORIO_TITULOS_EM_ABERTO_POR_DATA_VENCIMENTO = "exibirRelatorioTitulosEmAbertoPorDataVencimento";

	private static final Logger LOG = Logger.getLogger(RelatorioTitulosAbertoPorDataVencimentoAction.class);

	private static final String ID_CLIENTE = "idCliente";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String IDS_PONTO_CONSUMO = "idsPontoConsumo";

	private static final String SITUACAO_DEBITO = "situacaoDebito";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorRelatorioTituloAbertoPorDataVencimento controladorRelatorioTitulosAbertoPorDataVencimento;

	/**
	 * Exibir relatorio titulos em aberto por data vencimento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param model - {@link Model}
	 * @return exibirRelatorioTitulosEmAbertoPorDataVencimento - {@link String}
	 */
	@RequestMapping(EXIBIR_RELATORIO_TITULOS_EM_ABERTO_POR_DATA_VENCIMENTO)
	public String exibirRelatorioTitulosEmAbertoPorDataVencimento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO,
			Model model) {

		model.addAttribute(RELATORIO_SUB_MENU_FATURAMENTO_VO, relatorioSubMenuFaturamentoVO);

		return EXIBIR_RELATORIO_TITULOS_EM_ABERTO_POR_DATA_VENCIMENTO;
	}

	/**
	 * Pesquisar ponto consumo.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResults - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirRelatorioTitulosEmAbertoPorDataVencimento - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumo")
	public String pesquisarPontoConsumo(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResults, HttpServletRequest request,
			Model model) throws GGASException {

		Long idImovel = relatorioSubMenuFaturamentoVO.getIdImovel();
		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();
		Long[] idsPontoConsumo = relatorioSubMenuFaturamentoVO.getIdsPontoConsumo();

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		try {

			if ((idImovel != null) && (idImovel > 0)) {
				listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
				relatorioSubMenuFaturamentoVO.setIndicadorPesquisa("indicadorPesquisaImovel");
			} else if ((idCliente != null) && (idCliente > 0)) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
				relatorioSubMenuFaturamentoVO.setIndicadorPesquisa("indicadorPesquisaCliente");
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.getSession().setAttribute(IDS_PONTO_CONSUMO, idsPontoConsumo);

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		model.addAttribute(RELATORIO_SUB_MENU_FATURAMENTO_VO, relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return EXIBIR_RELATORIO_TITULOS_EM_ABERTO_POR_DATA_VENCIMENTO;
	}

	/**
	 * Gerar relatorio titulos em aberto por data vencimento.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResults - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping("gerarRelatorioTitulosEmAbertoPorDataVencimento")
	public String gerarRelatorioTitulosEmAbertoPorDataVencimento(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO,
			BindingResult result, ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResults,
			HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		String view = null;
		Long idImovel = relatorioSubMenuFaturamentoVO.getIdImovel();
		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();

		try {

			if ((idCliente == null || idCliente <= 0) && (idImovel == null || idImovel <= 0)) {
				throw new NegocioException(Constantes.ERRO_SELECIONE_UM_IMOVEL_OU_CLIENTE, true);
			}

			request.getAttribute(LISTA_PONTO_CONSUMO);

			Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);

			if (listaPontoConsumo != null) {
				request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
				model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
			}

			Map<String, Object> filtro = this.prepararFiltro(relatorioSubMenuFaturamentoVO);

			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuFaturamentoVO.getFormatoImpressao());

			byte[] relatorio = controladorRelatorioTitulosAbertoPorDataVencimento.gerarRelatorioTituloAbertoPorDataVencimento(filtro,
					formatoImpressao);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream;

			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);

			Date dataAtual = Calendar.getInstance().getTime();
			int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			int minutes = Calendar.getInstance().get(Calendar.MINUTE);
			int seconds = Calendar.getInstance().get(Calendar.SECOND);

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + "_";
			response.addHeader("Content-Disposition", "attachment; filename=relatorioTitulosEmAbertoPorDataVencimento" + "_" + data + hours
					+ minutes + seconds + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = EXIBIR_RELATORIO_TITULOS_EM_ABERTO_POR_DATA_VENCIMENTO;
		}

		model.addAttribute(RELATORIO_SUB_MENU_FATURAMENTO_VO, relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);
		model.addAttribute("imovel", clienteImovelPesquisaPopupVO);

		return view;

	}

	/**
	 * Preparar filtro.
	 *
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String tipoExibicao = relatorioSubMenuFaturamentoVO.getTipoExibicao();
		String tipoFormatoImpressao = relatorioSubMenuFaturamentoVO.getFormatoImpressao();
		String situacaoDebito = relatorioSubMenuFaturamentoVO.getSituacaoDebito();
		String dataEmissaoInicial = relatorioSubMenuFaturamentoVO.getDataEmissaoInicial();
		String dataEmissaoFinal = relatorioSubMenuFaturamentoVO.getDataEmissaoFinal();
		String dataVencimentoInicial = relatorioSubMenuFaturamentoVO.getDataVencimentoInicial();
		String dataVencimentoFinal = relatorioSubMenuFaturamentoVO.getDataVencimentoFinal();

		Boolean exibirFiltros = relatorioSubMenuFaturamentoVO.isExibirFiltros();

		if(exibirFiltros) {
			filtro.put(EXIBIR_FILTROS, exibirFiltros);
		}
		
		filtro.put(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(tipoFormatoImpressao));
		filtro.put(SITUACAO_DEBITO, situacaoDebito);
		filtro.put(TIPO_EXIBICAO, tipoExibicao);

		if (StringUtils.isNotEmpty(dataEmissaoInicial)) {
			DateTime dateTime = Util.zerarHorario(new DateTime(
					Util.converterCampoStringParaData("Período de Emissão Inicial", dataEmissaoInicial, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_EMISSAO_INICIAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataEmissaoFinal)) {
			DateTime dateTime = Util.ultimoHorario(new DateTime(
					Util.converterCampoStringParaData("Período de Emissão Final", dataEmissaoFinal, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_EMISSAO_FINAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataVencimentoInicial)) {
			DateTime dateTime = Util.zerarHorario(new DateTime(
					Util.converterCampoStringParaData("Data Vencimento Inicial", dataVencimentoInicial, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_INICIAL, dateTime.toDate());
		}

		if (StringUtils.isNotEmpty(dataVencimentoFinal)) {
			DateTime dateTime = Util.ultimoHorario(new DateTime(
					Util.converterCampoStringParaData("Data Vencimento Final", dataVencimentoFinal, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_FINAL, dateTime.toDate());
		}

		prepararFiltroImovelPontoConsumo(relatorioSubMenuFaturamentoVO, filtro);

		return filtro;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private void prepararFiltroImovelPontoConsumo(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Map<String, Object> filtro)
			throws GGASException {

		Long idImovel = relatorioSubMenuFaturamentoVO.getIdImovel();
		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();
		Long[] chavesPontoConsumo = relatorioSubMenuFaturamentoVO.getIdsPontoConsumo();

		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			filtro.put(CHAVES_PONTO_CONSUMO, chavesPontoConsumo);
		} else if (idImovel != null && idImovel > 0) {
			Collection<PontoConsumo> listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(idImovel);
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
			}
		}

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

	}

}
