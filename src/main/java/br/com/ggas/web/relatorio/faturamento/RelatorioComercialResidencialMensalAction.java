/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturamento;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * @author vpessoa
 */
@Controller
public class RelatorioComercialResidencialMensalAction extends GenericAction {
	
	private static final String RELATORIO_COMERCIAL_MENSAL_VO = "relatorioComercialMensalVO";

	private static final Logger LOG = Logger.getLogger(RelatorioComercialResidencialMensalAction.class);

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Exibir pesquisa relatorio comercial mensal.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("exibirPesquisaRelatorioComercialMensal")
	public ModelAndView exibirPesquisaRelatorioComercialMensal() throws NegocioException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioComercialMensal");

		carregarCombosComercial(model);

		return model;
	}

	/**
	 * Carregar combos comercial.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCombosComercial(ModelAndView model) throws NegocioException {

		List<String> listaAnoReferencia = new ArrayList<String>();
		Calendar calendar = Calendar.getInstance();
		for (int i = 2010; i <= calendar.get(Calendar.YEAR); i++) {
			listaAnoReferencia.add(String.valueOf(i));
		}
		model.addObject("listaAnoReferencia", listaAnoReferencia);
		model.addObject("listaSegmento", controladorSegmento.listarSegmento());
	}

	/**
	 * Exibir pesquisa relatorio residencial.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("exibirPesquisaRelatorioResidencialMensal")
	public ModelAndView exibirPesquisaRelatorioResidencial() throws NegocioException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioResidencialMensal");

		carregarCombosResidencial(model);

		return model;
	}

	/**
	 * Carregar combos residencial.
	 * 
	 * @param model
	 *            the model
	 */
	private void carregarCombosResidencial(ModelAndView model) {

		List<String> listaAnoReferencia = new ArrayList<String>();
		listaAnoReferencia.add("2014");
		listaAnoReferencia.add("2013");
		model.addObject("listaAnoReferencia", listaAnoReferencia);
		model.addObject("listaModalidadeMedicao", controladorImovel.listarModalidadeMedicaoImovel());
	}

	/**
	 * Gerar relatorio comercial mensal.
	 * 
	 * @param relatorioComercialMensalVO
	 *            the relatorio comercial mensal vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioComercialMensal")
	public ModelAndView gerarRelatorioComercialMensal(
					@ModelAttribute("RelatorioComercialMensalVO") PesquisaRelatorioComercialMensalVO relatorioComercialMensalVO,
					@RequestParam("tipoRelatorio") String tipoRelatorio, BindingResult result, HttpServletRequest request,
					HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioComercialMensal");
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			relatorio = controladorPontoConsumo.gerarRelatorioComercialMensal(relatorioComercialMensalVO, formatoImpressao);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(
							"Content-Disposition",
							"attachment; filename=RelatorioComercialMensal_" + diaMesAno + "_" + hora
											+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			carregarCombosComercial(model);
			model.addObject(RELATORIO_COMERCIAL_MENSAL_VO, relatorioComercialMensalVO);
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch(NegocioException e) {
			carregarCombosComercial(model);
			model.addObject(RELATORIO_COMERCIAL_MENSAL_VO, relatorioComercialMensalVO);
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Gerar relatorio residencial mensal.
	 * 
	 * @param relatorioComercialMensalVO
	 *            the relatorio comercial mensal vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("gerarRelatorioResidencialMensal")
	public ModelAndView gerarRelatorioResidencialMensal(
					@ModelAttribute("RelatorioComercialMensalVO") PesquisaRelatorioComercialMensalVO relatorioComercialMensalVO,
					@RequestParam("tipoRelatorio") String tipoRelatorio, BindingResult result, HttpServletRequest request,
					HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioResidencialMensal");
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());

			relatorio = controladorPontoConsumo.gerarRelatorioResidencialMensal(relatorioComercialMensalVO, formatoImpressao);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(
							"Content-Disposition",
							"attachment; filename=RelatorioResidencialMensal_" + diaMesAno + "_" + hora
											+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			carregarCombosResidencial(model);
			LOG.error(e.getMessage(), e);
		} catch(NegocioException e) {
			carregarCombosResidencial(model);
			mensagemErroParametrizado(model, e);
			LOG.error(e.getMessage(), e);
		}
		model.addObject(RELATORIO_COMERCIAL_MENSAL_VO, relatorioComercialMensalVO);
		return model;
	}

}
