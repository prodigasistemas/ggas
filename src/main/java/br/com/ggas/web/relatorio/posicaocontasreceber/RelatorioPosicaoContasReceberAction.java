/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.posicaocontasreceber;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioDeInadimplentes;
import br.com.ggas.relatorio.ControladorRelatorioPosicaoContasReceber;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.ClienteImovelPesquisaPopupVO;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Posição de Contas a Receber por Período.
 */
@Controller
public class RelatorioPosicaoContasReceberAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioPosicaoContasReceberAction.class);

	private static final String ID_CLIENTE = "idCliente";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String DATA_REFERENCIA = "dataReferencia";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String QUANTIDADES_DIAS_ATRASO = "diasAtraso";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String IDS_SEGMENTO = "idsSegmentos";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String IDS_PONTO_CONSUMO = "idsPontoConsumo";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRelatorioPosicaoContasReceber controladorRelatorioPosicaoContasReceber;
	
	@Autowired
	private ControladorRelatorioDeInadimplentes controladorRelatorioDeInadimplentes;
	
	/**
	 * Exibir pesquisa relatorio posicao contas receber.
	 * 
	 * @param model - {@link Model}
	 * @return exibirPesquisaRelatorioPosicaoContasReceber - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioPosicaoContasReceber")
	public String exibirPesquisaRelatorioPosicaoContasReceber(Model model) throws GGASException {

		try {
			Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
			model.addAttribute(LISTA_SEGMENTO, listaSegmento);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaRelatorioPosicaoContasReceber";
	}
	
	/**
	 * Exibir pesquisa pontos consumo.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaPontosConsumo - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaPontosConsumo")
	public String exibirPesquisaPontosConsumo(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
			model.addAttribute(LISTA_SEGMENTO, listaSegmento);
			this.configurarDadosPesquisaRelatorio(relatorioSubMenuFaturamentoVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);

		return "exibirPesquisaPontosConsumo";
	}

	/**
	 * Configurar dados pesquisa relatorio.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void configurarDadosPesquisaRelatorio(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, HttpServletRequest request,
			Model model) throws GGASException {

		String indicadorPesquisa = relatorioSubMenuFaturamentoVO.getIndicadorPesquisa();

		if (("indicadorPesquisaCliente").equals(indicadorPesquisa)) {

			Map<String, Object> filtro = this.popularFiltroRelatorio(relatorioSubMenuFaturamentoVO);

			super.adicionarFiltroPaginacao(request, filtro);

			List<PontoConsumo> listaPontoConsumo = controladorRelatorioDeInadimplentes.listarPontosConsumoRelatorioInadimplentes(filtro);

			model.addAttribute(LISTA_PONTO_CONSUMO, super.criarColecaoPaginada(request, filtro, listaPontoConsumo));

		}

	}

	/**
	 * Gerar relatorio posicao contas receber.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param clienteImovelPesquisaPopupVO - {@link ClienteImovelPesquisaPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping("gerarRelatorioPosicaoContasReceber")
	public String gerarRelatorioPosicaoContasReceber(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result,
			ClienteImovelPesquisaPopupVO clienteImovelPesquisaPopupVO, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		try {

			Map<String, Object> filtro = this.popularFiltroRelatorio(relatorioSubMenuFaturamentoVO);

			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(relatorioSubMenuFaturamentoVO.getFormatoImpressao());

			byte[] relatorio = controladorRelatorioPosicaoContasReceber.gerarPosicaoContasReceber(filtro, formatoImpressao);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);

			Date dataAtual = Calendar.getInstance().getTime();
			int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			int minutes = Calendar.getInstance().get(Calendar.MINUTE);
			int seconds = Calendar.getInstance().get(Calendar.SECOND);

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + "_";
			response.addHeader("Content-Disposition", "attachment; filename=relatorioPosicaoContasReceber" + "_" + data + hours + minutes
					+ seconds + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaRelatorioPosicaoContasReceber(model);
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);
		model.addAttribute("cliente", clienteImovelPesquisaPopupVO);

		return view;
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> popularFiltroRelatorio(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String tipoExibicao = relatorioSubMenuFaturamentoVO.getTipoExibicao();
		String tipoFormatoImpressao = relatorioSubMenuFaturamentoVO.getFormatoImpressao();
		String dataReferencia = relatorioSubMenuFaturamentoVO.getDataReferencia();
		String dataVencimentoInicial = relatorioSubMenuFaturamentoVO.getDataVencimentoInicial();
		String dataVencimentoFinal = relatorioSubMenuFaturamentoVO.getDataVencimentoFinal();

		Boolean exibirFiltros = relatorioSubMenuFaturamentoVO.isExibirFiltros();

		Long idSegmento = relatorioSubMenuFaturamentoVO.getIdSegmento();
		Long[] idsSegmento = relatorioSubMenuFaturamentoVO.getIdsSegmentos();

		filtro.put(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(tipoFormatoImpressao));
		
		if(exibirFiltros) {
			filtro.put(EXIBIR_FILTROS, exibirFiltros);
		}

		if (StringUtils.isNotEmpty(tipoExibicao)) {
			filtro.put(TIPO_EXIBICAO, tipoExibicao);
		}
		if (StringUtils.isNotEmpty(dataReferencia)) {
			DateTime dateTime = Util.zerarHorario(
					new DateTime(Util.converterCampoStringParaData("Data de Referência", dataReferencia, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_REFERENCIA, dateTime.toDate());
		}
		if (StringUtils.isNotEmpty(dataVencimentoInicial)) {
			DateTime dateTime = Util.zerarHorario(new DateTime(
					Util.converterCampoStringParaData("Data Vencimento Inicial", dataVencimentoInicial, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_INICIAL, dateTime.toDate());
		}
		if (StringUtils.isNotEmpty(dataVencimentoFinal)) {
			DateTime dateTime = Util.ultimoHorario(new DateTime(
					Util.converterCampoStringParaData("Data Vencimento Final", dataVencimentoFinal, Constantes.FORMATO_DATA_BR)));
			filtro.put(DATA_VENCIMENTO_FINAL, dateTime.toDate());
		}

		if (idSegmento != null && idSegmento > 0) {
			filtro.put(ID_SEGMENTO, idSegmento);
		}

		if (idsSegmento != null && idsSegmento.length > 0) {
			filtro.put(IDS_SEGMENTO, idsSegmento);
		}

		popularFiltroRelatorioDois(relatorioSubMenuFaturamentoVO, filtro);

		return filtro;
	}

	/**
	 * Popular filtro relatorio.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @return filtro - {@link Map}
	 */
	private void popularFiltroRelatorioDois(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, Map<String, Object> filtro) {

		Long idCliente = relatorioSubMenuFaturamentoVO.getIdCliente();
		Long[] idsPontoConsumo = relatorioSubMenuFaturamentoVO.getIdsPontoConsumo();
		Integer diasAtraso = relatorioSubMenuFaturamentoVO.getDiasAtraso();

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
			filtro.put(IDS_PONTO_CONSUMO, idsPontoConsumo);
		}

		if (diasAtraso != null && diasAtraso > 0) {
			filtro.put(QUANTIDADES_DIAS_ATRASO, diasAtraso);
		}

	}

}
