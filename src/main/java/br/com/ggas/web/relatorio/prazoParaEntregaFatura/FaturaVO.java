package br.com.ggas.web.relatorio.prazoParaEntregaFatura;

import java.util.Date;
/**
 * 
 * Classe responsável pela representação da entidade Fatura
 *
 */
public class FaturaVO {

	private String nomeCliente;
	private Date dataFatura;
	private Date dataVencimentoFatura;
	
	public FaturaVO(String nomeCliente, Date dataFatura, Date dataVencimentoFatura){
		super();
		this.nomeCliente = nomeCliente;
		this.dataFatura = dataFatura;
		this.dataVencimentoFatura = dataVencimentoFatura;
	}
	public FaturaVO(){
		super();
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Date getDataFatura() {
		Date data = null;
		if (this.dataFatura != null) {
			data = (Date) dataFatura.clone();
		}
		return data;
	}
	
	public void setDataFatura(Date dataFatura) {
		this.dataFatura = dataFatura;
	}

	public Date getDataVencimentoFatura() {
		Date data = null;
		if (dataVencimentoFatura != null) {
			data = (Date) dataVencimentoFatura.clone();
		}
		return data;
	}
	
	public void setDataVencimentoFatura(Date dataVencimentoFatura) {
		this.dataVencimentoFatura = dataVencimentoFatura;
	}
	
}
