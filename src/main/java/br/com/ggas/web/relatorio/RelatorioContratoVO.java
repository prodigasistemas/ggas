package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes as funcionalidades do módulo Relatório no sub-menu Contrato.
 * 
 * @author bruno silva
 * 
 * @see RelatorioQuantidadesContratadasAction, RelatorioCobrancaRecuperacaoPenalidadesAction
 */
public class RelatorioContratoVO {
	
	private String indicadorPesquisa;
	private String dataInicial;
	private String dataFinal;
	private String dataVencimento;
	private String dataAssinatura;
	private String numeroContrato;
	private String formatoImpressao;
	private String tipo;
	
	private boolean exibirFiltros;
	
	private Long idCliente;
	private Long idImovel;
	private Long idSegmento;
	
	/**
	 * @return indicadorPesquisa - {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa - {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	/**
	 * @return dataInicial - {@link String}
	 */
	public String getDataInicial() {
		return dataInicial;
	}
	/**
	 * @param dataInicial - {@link String}
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	/**
	 * @return dataFinal - {@link String}
	 */
	public String getDataFinal() {
		return dataFinal;
	}
	/**
	 * @param dataFinal - {@link String}
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	/**
	 * @return dataVencimento - {@link String}
	 */
	public String getDataVencimento() {
		return dataVencimento;
	}
	/**
	 * @param dataVencimento - {@link String}
	 */
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	/**
	 * @return dataAssinatura - {@link String}
	 */
	public String getDataAssinatura() {
		return dataAssinatura;
	}
	/**
	 * @param dataAssinatura - {@link String}
	 */
	public void setDataAssinatura(String dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}
	/**
	 * @return numeroContrato - {@link String}
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato - {@link String}
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return formatoImpressao - {@link String}
	 */
	public String getFormatoImpressao() {
		return formatoImpressao;
	}
	/**
	 * @param formatoImpressao - {@link String}
	 */
	public void setFormatoImpressao(String formatoImpressao) {
		
		if(("").equals(formatoImpressao)) {
			formatoImpressao = "PDF";
		}
		
		this.formatoImpressao = formatoImpressao;
	}
	/**
	 * @return tipo - {@link String}
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo - {@link String}
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return exibirFiltros - {@link Boolean}
	 */
	public boolean isExibirFiltros() {
		return exibirFiltros;
	}
	/**
	 * @param exibirFiltros - {@link Boolean}
	 */
	public void setExibirFiltros(boolean exibirFiltros) {
		this.exibirFiltros = exibirFiltros;
	}
	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idSegmento - {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}
	/**
	 * @param idSegmento - {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}
	
}
