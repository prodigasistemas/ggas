package br.com.ggas.web.relatorio.prazoParaEntregaFatura;

public class ParamentroRelatorio {

	private String nomeCliente;
	private String dataFatura;
	private String dataVencimentoFatura;
	private String dataLimite;
	private String status;
	
	public ParamentroRelatorio(String nomeCliente, String dataFatura, String dataVencimentoFatura, String dataLimite,
			String status) {
		super();
		this.nomeCliente = nomeCliente;
		this.dataFatura = dataFatura;
		this.dataVencimentoFatura = dataVencimentoFatura;
		this.dataLimite = dataLimite;
		this.status = status;
	}
	
	public ParamentroRelatorio() {
		super();
	}

	public String getNomeCliente() {
		return nomeCliente;
	}
	
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	public String getDataFatura() {
		return dataFatura;
	}
	
	public void setDataFatura(String dataFatura) {
		this.dataFatura = dataFatura;
	}
	
	public String getDataVencimentoFatura() {
		return dataVencimentoFatura;
	}
	
	public void setDataVencimentoFatura(String dataVencimentoFatura) {
		this.dataVencimentoFatura = dataVencimentoFatura;
	}
	
	public String getDataLimite() {
		return dataLimite;
	}
	
	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
}
