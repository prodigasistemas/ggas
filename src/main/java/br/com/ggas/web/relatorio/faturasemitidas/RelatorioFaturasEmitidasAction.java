/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.relatorio.faturasemitidas;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioFaturasEmitidas;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.web.relatorio.RelatorioSubMenuFaturamentoVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Faturas Emitidas.
 */
@Controller
public class RelatorioFaturasEmitidasAction extends GenericAction {

	private static final String EXIBIR_RELATORIO_FATURAS_EMITIDAS = "exibirRelatorioFaturasEmitidas";

	private static final Logger LOG = Logger.getLogger(RelatorioFaturasEmitidasAction.class);

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";
	
	private static final String ID_TIPO_DOCUMENTO = "idTipoDocumento";
	
	public static final String EXIBIR_FILTROS = "exibirFiltros";
	
	public static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	@Autowired
	private ControladorRelatorioFaturasEmitidas controladorRelatorioFaturasEmitidas;

	/**
	 * Exibir relatorio faturas emitidas.
	 * 
	 * @return exibirRelatorioFaturasEmitidas - {@link String}
	 */
	@RequestMapping(EXIBIR_RELATORIO_FATURAS_EMITIDAS)
	public String exibirRelatorioFaturasEmitidas() {

		return EXIBIR_RELATORIO_FATURAS_EMITIDAS;
	}

	/**
	 * Gerar relatorio faturas emitidas.
	 * 
	 * @param relatorioSubMenuFaturamentoVO - {@link RelatorioSubMenuFaturamentoVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioFaturasEmitidas")
	public String gerarRelatorioFaturasEmitidas(RelatorioSubMenuFaturamentoVO relatorioSubMenuFaturamentoVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		String view = null;

		String dataInicial = relatorioSubMenuFaturamentoVO.getDataInicial();
		String dataFinal = relatorioSubMenuFaturamentoVO.getDataFinal();
		String formato = relatorioSubMenuFaturamentoVO.getFormatoImpressao();
		Boolean filtros = relatorioSubMenuFaturamentoVO.isExibirFiltros();
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(formato);

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(DATA_INICIAL, dataInicial);
		filtro.put(DATA_FINAL, dataFinal);
		filtro.put(ID_TIPO_DOCUMENTO, TipoDocumento.TIPO_FATURA);
		filtro.put(EXIBIR_FILTROS, filtros);
		filtro.put(FORMATO_IMPRESSAO, formato);

		try {
			byte[] relatorio = controladorRelatorioFaturasEmitidas.consultar(filtro);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorio." + formato.toLowerCase());
			response.getOutputStream().write(relatorio);

		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = EXIBIR_RELATORIO_FATURAS_EMITIDAS;
		}

		model.addAttribute("relatorioSubMenuFaturamentoVO", relatorioSubMenuFaturamentoVO);

		return view;
	}

}
