package br.com.ggas.web.relatorio;

/**
 * Classe responsável pela representação de valores referentes as funcionalidades do módulo Relatório no sub-menu Medição.
 * 
 * @author bruno silva
 * 
 * @see - RelatorioPerfilConsumoClienteAction, RelatorioAnaliseConsumoAction, RelatorioAnaliseDemandaAction,
 * 		  RelatorioVariacaoVolumeAction, RelatorioResumoVolumeAction, RelatorioVolumesFaturadosAction
 */
public class RelatorioSubMenuMedicaoVO {

	private String numeroContrato;
	private String dataAssinatura;
	private String dataInicio;
	private String dataEncerramento;
	private String mes;
	private String ano;
	private String formatoImpressao;
	private String indicadorPesquisa;
	private String dataEmissaoInicial;
	private String dataEmissaoFinal;
	private String dataInicial;
	private String dataFinal;
	private String tipoExibicao = "analitico";
	private String idTipoGrafico;
	private String referencia;
	
	private boolean exibirFiltros = true;
	private boolean exibirGrafico = true;
	private boolean consumoProgramado = true;
	private Boolean pesquisarPontosConsumo = false;
	
	private Long idImovel;
	private Long idCliente;
	private Long idSegmento;
	private Long idModalidadeMedicao;
	private Long idRamoAtividade;
	private Long[] chavesPontoConsumo;
	private Long[] idsSegmentosAssociados;
	private Long[] idsRamosAtividadesAssociados;
	private Long[] idsPontoConsumo;
	private Long[] idsSegmentosDisponiveis;
	private Long[] idsRamosAtividadesDisponiveis;
	
	/**
	 * @return numeroContrato - {@link String}
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato - {@link String}
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return dataAssinatura - {@link String}
	 */
	public String getDataAssinatura() {
		return dataAssinatura;
	}
	/**
	 * @param dataAssinatura - {@link String}
	 */
	public void setDataAssinatura(String dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}
	/**
	 * @return dataInicio - {@link String}
	 */
	public String getDataInicio() {
		return dataInicio;
	}
	/**
	 * @param dataInicio - {@link String}
	 */
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	/**
	 * @return dataEncerramento - {@link String}
	 */
	public String getDataEncerramento() {
		return dataEncerramento;
	}
	/**
	 * @param dataEncerramento - {@link String}
	 */
	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}
	/**
	 * @return mes - {@link String}
	 */
	public String getMes() {
		return mes;
	}
	/**
	 * @param mes - {@link String}
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}
	/**
	 * @return ano - {@link String}
	 */
	public String getAno() {
		return ano;
	}
	/**
	 * @param ano - {@link String}
	 */
	public void setAno(String ano) {
		this.ano = ano;
	}
	/**
	 * @return formatoImpressao - {@link String}
	 */
	public String getFormatoImpressao() {
		return formatoImpressao;
	}
	/**
	 * @param formatoImpressao - {@link String}
	 */
	public void setFormatoImpressao(String formatoImpressao) {
		
		if(("").equals(formatoImpressao)) {
			formatoImpressao = "PDF";
		}
		
		this.formatoImpressao = formatoImpressao;
	}
	/**
	 * @return indicadorPesquisa - {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa - {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	/**
	 * @return dataEmissaoInicial - {@link String}
	 */
	public String getDataEmissaoInicial() {
		return dataEmissaoInicial;
	}
	/**
	 * @param dataEmissaoInicial - {@link String}
	 */
	public void setDataEmissaoInicial(String dataEmissaoInicial) {
		this.dataEmissaoInicial = dataEmissaoInicial;
	}
	/**
	 * @return dataEmissaoFinal - {@link String}
	 */
	public String getDataEmissaoFinal() {
		return dataEmissaoFinal;
	}
	/**
	 * @param dataEmissaoFinal - {@link String}
	 */
	public void setDataEmissaoFinal(String dataEmissaoFinal) {
		this.dataEmissaoFinal = dataEmissaoFinal;
	}
	/**
	 * @return dataInicial - {@link String}
	 */
	public String getDataInicial() {
		return dataInicial;
	}
	/**
	 * @param dataInicial - {@link String}
	 */
	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}
	/**
	 * @return dataFinal - {@link String}
	 */
	public String getDataFinal() {
		return dataFinal;
	}
	/**
	 * @param dataFinal - {@link String}
	 */
	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}
	/**
	 * @return tipoExibicao - {@link String}
	 */
	public String getTipoExibicao() {
		return tipoExibicao;
	}
	/**
	 * @param tipoExibicao - {@link String}
	 */
	public void setTipoExibicao(String tipoExibicao) {
		this.tipoExibicao = tipoExibicao;
	}
	/**
	 * @return idTipoGrafico - {@link String}
	 */
	public String getIdTipoGrafico() {
		return idTipoGrafico;
	}
	/**
	 * @param idTipoGrafico - {@link String}
	 */
	public void setIdTipoGrafico(String idTipoGrafico) {
		this.idTipoGrafico = idTipoGrafico;
	}
	/**
	 * @return referencia - {@link String}
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia - {@link String}
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	
	/**
	 * @return exibirFiltros - {@link Boolean}
	 */
	public boolean isExibirFiltros() {
		return exibirFiltros;
	}
	/**
	 * @param exibirFiltros - {@link Boolean}
	 */
	public void setExibirFiltros(boolean exibirFiltros) {
		this.exibirFiltros = exibirFiltros;
	}
	/**
	 * @return exibirGrafico - {@link Boolean}
	 */
	public boolean isExibirGrafico() {
		return exibirGrafico;
	}
	/**
	 * @param exibirGrafico - {@link Boolean}
	 */
	public void setExibirGrafico(boolean exibirGrafico) {
		this.exibirGrafico = exibirGrafico;
	}
	/**
	 * @return consumoProgramado - {@link Boolean}
	 */
	public boolean isConsumoProgramado() {
		return consumoProgramado;
	}
	/**
	 * @param consumoProgramado - {@link Boolean}
	 */
	public void setConsumoProgramado(boolean consumoProgramado) {
		this.consumoProgramado = consumoProgramado;
	}
	/**
	 * @return pesquisarPontosConsumo - {@link Boolean}
	 */
	public Boolean getPesquisarPontosConsumo() {
		return pesquisarPontosConsumo;
	}
	/**
	 * @param pesquisarPontosConsumo - {@link Boolean}
	 */
	public void setPesquisarPontosConsumo(Boolean pesquisarPontosConsumo) {
		this.pesquisarPontosConsumo = pesquisarPontosConsumo;
	}

	
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idCliente - {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente - {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return idSegmento - {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}
	/**
	 * @param idSegmento - {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}
	/**
	 * @return idModalidadeMedicao - {@link Long}
	 */
	public Long getIdModalidadeMedicao() {
		return idModalidadeMedicao;
	}
	/**
	 * @param idModalidadeMedicao - {@link Long}
	 */
	public void setIdModalidadeMedicao(Long idModalidadeMedicao) {
		this.idModalidadeMedicao = idModalidadeMedicao;
	}
	/**
	 * @return idRamoAtividade - {@link Long}
	 */
	public Long getIdRamoAtividade() {
		return idRamoAtividade;
	}
	/**
	 * @param idRamoAtividade - {@link Long}
	 */
	public void setIdRamoAtividade(Long idRamoAtividade) {
		this.idRamoAtividade = idRamoAtividade;
	}
	
	/**
	 * @return chavesPontoConsumo {@link Long}
	 */
	public Long[] getChavesPontoConsumo() {
		return validarClonarSubMenuMedicao(chavesPontoConsumo);
	}
	/**
	 * @param chavesPontoConsumo {@link Long}
	 */
	public void setChavesPontoConsumo(Long[] chavesPontoConsumo) {
		this.chavesPontoConsumo = validarClonarSubMenuMedicao(chavesPontoConsumo);
	}
	
	/**
	 * @return idsSegmentosAssociados {@link Long}
	 */
	public Long[] getIdsSegmentosAssociados() {
		return validarClonarSubMenuMedicao(idsSegmentosAssociados);
	}
	/**
	 * @param idsSegmentosAssociados {@link Long}
	 */
	public void setIdsSegmentosAssociados(Long[] idsSegmentosAssociados) {
		this.idsSegmentosAssociados = validarClonarSubMenuMedicao(idsSegmentosAssociados);
	}
	
	/**
	 * @return idsRamosAtividadesAssociados {@link Long}
	 */
	public Long[] getIdsRamosAtividadesAssociados() {
		return validarClonarSubMenuMedicao(idsRamosAtividadesAssociados);
	}
	/**
	 * @param idsRamosAtividadesAssociados {@link Long}
	 */
	public void setIdsRamosAtividadesAssociados(Long[] idsRamosAtividadesAssociados) {
		this.idsRamosAtividadesAssociados = validarClonarSubMenuMedicao(idsRamosAtividadesAssociados);
	}
	
	/**
	 * @return idsPontoConsumo {@link Long}
	 */
	public Long[] getIdsPontoConsumo() {
		return validarClonarSubMenuMedicao(idsPontoConsumo);
	}
	/**
	 * @param idsPontoConsumo {@link Long}
	 */
	public void setIdsPontoConsumo(Long[] idsPontoConsumo) {
		this.idsPontoConsumo = validarClonarSubMenuMedicao(idsPontoConsumo);
	}
	/**
	 * @return idsSegmentosDisponiveis {@link Long}
	 */
	public Long[] getIdsSegmentosDisponiveis() {
		return validarClonarSubMenuMedicao(idsSegmentosDisponiveis);
	}
	/**
	 * @param idsSegmentosDisponiveis {@link Long}
	 */
	public void setIdsSegmentosDisponiveis(Long[] idsSegmentosDisponiveis) {
		this.idsSegmentosDisponiveis = validarClonarSubMenuMedicao(idsSegmentosDisponiveis);
	}
	/**
	 * @return idsRamosAtividadesDisponiveis {@link Long}
	 */
	public Long[] getIdsRamosAtividadesDisponiveis() {
		return validarClonarSubMenuMedicao(idsRamosAtividadesDisponiveis);
	}
	/**
	 * @param idsRamosAtividadesDisponiveis {@link Long}
	 */
	public void setIdsRamosAtividadesDisponiveis(Long[] idsRamosAtividadesDisponiveis) {
		this.idsRamosAtividadesDisponiveis = validarClonarSubMenuMedicao(idsRamosAtividadesDisponiveis);
	}
	
	/**
	 * Método responsável em verificar se uma variável é nula, retornando uma cópia da mesma.
	 * 
	 * @param tmp - {@link Long}
	 * @return tmp variável temporária. - {@link Long}
	 */
	private Long[] validarClonarSubMenuMedicao(Long[] tmp) {
		if (tmp != null) {
			return tmp.clone();
		} else {
			return new Long[0];
		}
	}
	
}
