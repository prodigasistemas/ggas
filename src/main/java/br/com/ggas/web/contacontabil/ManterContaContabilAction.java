/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contacontabil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.ControladorContaContabil;
import br.com.ggas.contabil.impl.ContaContabilImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Controller MVC para a entidade ManterContaContabil
 * 
 * @author Orube
 */
@Controller
public class ManterContaContabilAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(ManterContaContabilAction.class);

	private static final String HABILITADO = "habilitado";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME = "nomeConta";

	private static final String NUMERO_CONTA = "numeroConta";

	private static final String MASCARA_NUMERO_CONTA = "mascaraNumeroConta";

	private String retorno;

	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier("controladorContaContabil")
	private ControladorContaContabil controladorContaContabil;

	/**
	 * 
	 * @param model
	 * @return String exibirPesquisaContaContabil
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaContaContabil")
	public String exibirPesquisaContaContabil(Model model) throws GGASException {

		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));

		return "exibirPesquisaContaContabil";
	}

	/**
	 * 
	 * @param contaContabil
	 * @param result
	 * @param habilitado
	 * @param request 
	 * @param model
	 * @return String exibirPesquisaContaContabil
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarContaContabil")
	public String pesquisarContaContabil(@ModelAttribute("ContaContabilImpl") ContaContabilImpl contaContabil, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) String habilitado, HttpServletRequest request, Model model)
			throws GGASException {
		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, contaContabil, habilitado);
		if(contaContabil.getNumeroConta() != null){
			contaContabil.setNumeroConta(Util.removerCaracteresEspeciais(contaContabil.getNumeroConta()));			
		}

		String mascara = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA);
		Collection<ContaContabil> listaContaContabil = controladorContaContabil.listarContaContabil(filtro);

		for (ContaContabil itemContaContabil : listaContaContabil) {
			itemContaContabil.setNumeroConta(Util.formatarMascaraTruncada(mascara, itemContaContabil.getNumeroConta()));
		}
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("contaContabil", contaContabil);
		model.addAttribute("listaContaContabil", listaContaContabil);

		return "exibirPesquisaContaContabil";
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return String exibirAlterarContaContabil
	 * @throws NegocioException
	 */
	@RequestMapping("exibirAlterarContaContabil")
	public String exibirAlterarContaContabil(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws NegocioException {

		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));
		model.addAttribute("contaContabil", controladorContaContabil.obterContaContabil(chavePrimaria));

		return "exibirAlterarContaContabil";
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param model
	 * @param request
	 * @return String
	 * @throws GGASException 
	 * @throws Exception
	 */
	@RequestMapping("/removerContaContabil")
	public String removerContaContabil(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, Model model, HttpServletRequest request)
			throws GGASException {

		retorno = "exibirPesquisaContaContabil";
		try {
			controladorContaContabil.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, ContaContabil.CONTA_CONTABIL);
			retorno = pesquisarContaContabil(new ContaContabilImpl(), null, null, request, model);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_REMOVER_CONTA_CONTABIL, true));
		}

		return retorno;
	}

	/**
	 * 
	 * @param contaContabil
	 * @param model
	 * @return String exibirInclusaoContaContabil
	 * @throws Exception
	 */
	@RequestMapping("exibirInclusaoContaContabil")
	public String exibirInclusaoContaContabil(@ModelAttribute("ContaContabilImpl") ContaContabilImpl contaContabil, Model model)
			throws NegocioException {

		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));

		return "exibirInclusaoContaContabil";
	}

	/**
	 * 
	 * @param contaContabil
	 * @param result
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("salvarContaContabil")
	public String salvarContaContabil(@ModelAttribute("ContaContabilImpl") ContaContabilImpl contaContabil, BindingResult result,
			Model model) throws GGASException {
		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));
		retorno = "exibirInclusaoContaContabil";
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, contaContabil, String.valueOf(contaContabil.isHabilitado()));
		
		contaContabil.setNumeroConta(Util.removerCaracteresEspeciais(contaContabil.getNumeroConta().replace("_", "")));
		model.addAttribute("contaContabil", contaContabil);

		Map<String, Object> erros = contaContabil.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				validarNumeroContaContabil(filtro);
				if (validarNaoExiste(filtro)) {
					controladorContaContabil.inserirContaContabil(contaContabil);
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA_FEMININO, ContaContabil.CONTA_CONTABIL);
					retorno = pesquisarContaContabil(contaContabil, result, String.valueOf(contaContabil.isHabilitado()), null, model);
				} else {
					mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE, true));
				}
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_QUANTIDADE_MENOR_MASCARA, e));
			}
		}
		return retorno;
	}

	/**
	 * 
	 * @param contaContabil
	 * @param result
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("salvarAlteracaoContaContabil")
	public String salvarAlteracaoContaContabil(@ModelAttribute("ContaContabilImpl") ContaContabilImpl contaContabil, BindingResult result,
			Model model) throws GGASException {
		model.addAttribute(MASCARA_NUMERO_CONTA,
				controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA));

		retorno = "exibirAlterarContaContabil";
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, contaContabil, String.valueOf(contaContabil.isHabilitado()));

		contaContabil.setNumeroConta(Util.removerCaracteresEspeciais(contaContabil.getNumeroConta().replace("_", "")));
		model.addAttribute("contaContabil", contaContabil);

		Map<String, Object> erros = contaContabil.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				validarNumeroContaContabil(filtro);
				if (validarNaoExiste(filtro)) {
					controladorContaContabil.atualizarContaContabil(contaContabil);
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA_FEMININO, ContaContabil.CONTA_CONTABIL);
					retorno = pesquisarContaContabil(contaContabil, result, String.valueOf(contaContabil.isHabilitado()), null, model);
				} else {
					mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_EXISTENTE, true));
				}
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_QUANTIDADE_MENOR_MASCARA, e));
			}
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return String exibirDetalharContaContabil
	 * @throws NegocioException 
	 */
	@RequestMapping("exibirDetalharContaContabil")
	public String exibirDetalharContaContabil(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws NegocioException  {

		ContaContabil contaContabil = controladorContaContabil.obterContaContabil(chavePrimaria);
		contaContabil.setNumeroConta(Util.formatarMascaraTruncada(
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA),
				contaContabil.getNumeroConta()));

		model.addAttribute("contaContabil", contaContabil);

		return "exibirDetalharContaContabil";
	}

	/**
	 * 
	 * @param filtro
	 * @param contaContabil
	 * @param habilitado
	 */
	private void prepararFiltro(Map<String, Object> filtro, ContaContabilImpl contaContabil, String habilitado) {

		if (contaContabil.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, contaContabil.getChavePrimaria());
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}

		if (contaContabil.getNomeConta() != null &&  !contaContabil.getNomeConta().isEmpty()) {
			filtro.put(NOME, contaContabil.getNomeConta());
		}

		if (contaContabil.getNumeroConta() != null && !contaContabil.getNumeroConta().isEmpty()) {
			filtro.put(NUMERO_CONTA, Util.removerCaracteresEspeciais(contaContabil.getNumeroConta()).replace("_", ""));
		} else {
			filtro.put(NUMERO_CONTA, null);
		}

	}

	/**
	 * 
	 * @param filtro
	 * @param model
	 * @throws GGASException
	 */
	private void validarNumeroContaContabil(Map<String, Object> filtro) throws GGASException {

		String mascara = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA);
		int x;
		int tamanhoString = 0;
		List<Integer> tamanhosDisponiveis = new ArrayList<Integer>();
		for (x = 0; x < mascara.length(); x++) {
			if (mascara.charAt(x) == '.' || mascara.charAt(x) == '-') {
				String substring = mascara.substring(0, x + 1);
				substring = substring.replace(".", "");
				substring = substring.replace("-", "");
				tamanhoString = substring.length();
				tamanhosDisponiveis.add(tamanhoString);
			}
		}
		tamanhosDisponiveis.add(mascara.replace(".", "").replace("-", "").length());
		if (filtro.get(NUMERO_CONTA) != null && !mascara.isEmpty()
				&& !tamanhosDisponiveis.contains(((String) filtro.get(NUMERO_CONTA)).replace("_", "").trim().length())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CONTABIL_NUMERO_QUANTIDADE_MENOR_MASCARA);
		}
	}

	/**
	 * 
	 * @param filtro
	 * @return Boolean
	 */
	private Boolean validarNaoExiste(Map<String, Object> filtro) {

		ContaContabil contaContabil = controladorContaContabil.obterContaContabil(filtro);

		if (contaContabil == null) {
			return true;
		} else {
			if (filtro.get(CHAVE_PRIMARIA) == null) {
				return false;
			} else {
				return String.valueOf(contaContabil.getChavePrimaria()).trim().equals(filtro.get(CHAVE_PRIMARIA).toString().trim());
			}
		}
	}
}
