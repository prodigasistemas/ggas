/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */


package br.com.ggas.web.contrato.contrato;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadeVO;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Penalidades.
 */
@Controller
public class PenalidadeAction extends GenericAction {
	
	private static final String IMOVEL = "imovel";

	private static final String CLIENTE = "cliente";

	private static final String PENALIDADE_VO = "penalidadeVO";

	private static final String APURACAO = "apuracao";

	private static final String CONTRATO = "Contrato";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	private static final String LISTA_PENALIDADES = "listaPenalidades";

	private static final String LISTA_PONTO_CONSUMO_VO = "listaPontoConsumoVO";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String LISTA_APURACAO_QTDA_PENALIDADE_PERIODICIDADE = "listaApuracaoQtdaPenalidadePeriodicidade";

	private static final String LISTA_CONTRATO_MODALIDADE = "listaContratoModalidade";

	private static final String LISTA_QTDA_CONTRATADA_VO = "listaQtdaContratadaVO";

	private static final String LISTA_QTDA_RETIRADA_PERIODO_VO = "listaQtdaRetiradaPeriodoVO";

	private static final String LISTA_QTDA_NAO_RETIRADA_VO_PARADA_PROGRAMADA = "listaQtdaNaoRetiradaVOParadaProgramada";

	private static final String LISTA_QTDA_NAO_RETIRADA_VO_PARADA_PROGRAMADA_CDL = "listaQtdaNaoRetiradaVOParadaProgramadaCdl";

	private static final String LISTA_QTDA_NAO_RETIRADA_VO_FALHA_FORNECIMENTO = "listaQtdaNaoRetiradaVOFalhaFornecimento";

	private static final String LISTA_QTDA_NAO_RETIRADA_VO_CASO_FORTUITO_FORCA_MAIOR = "listaQtdaNaoRetiradaVOCasoFortuitoForcaMaior";

	private static final String RELATORIO_NOTAS_DEBITOS_CREDITOS = "relatorioNotasDebitosCreditos";

	private static final String EXIBIR_RELATORIO_NOTAS_DEBITOS_CREDITOS = "exibirRelatorioNotasDebitosCreditos";

	private static final String FILTRO_PENALIDADES = "filtroPenalidades";

	private static final String EXIBIR_DADOS_FILTRO = "exibirDadosFiltro";

	private static final String MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO = "MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String PESQUISA_PENALIDADE = "penalidade";

	private static final String PESQUISA_PONTO_CONSUMO = "pontoConsumo";

	private static final String ID_PENALIDADES_ASSOCIADAS = "idPenalidadesAssociadas";

	private static final String ID_PENALIDADES_DISPONIVEIS = "idPenalidadesDisponiveis";

	private static final String DATA_APURACAO_INICIO = "dataApuracaoInicio";

	private static final String DATA_APURACAO_FIM = "dataApuracaoFim";

	private static final String COBRADA = "cobrada";

	private static final String ID_APURACAO_QTDA_PENALIDADE_PERIODICIDADE = "idApuracaoQtdaPenalidadePeriodicidade";

	private static final String DESCRICAO_PENALIDADE = "descricaoPenalidade";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String ID_CONTRATO = "idContrato";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String DATA_ASSINATURA_CONTRATO = "dataAssinaturaContrato";

	private static final String DATA_VENCIMENTO_OBRIGACOES_CONTRATUAIS_CONTRATO = "dataVencimentoObrigacoesContratuaisContrato";

	private static final String MODALIDADE_CONTRATO = "modalidadeContrato";

	private static final String PERIODICIDADE_CONTRATO = "periodicidadeContrato";

	private static final String CONSUMO_REFERENCIA_INFERIOR_CONTRATO = "consumoReferenciaInferiorContrato";

	private static final String COMPROMISSO_RETIRADA_CONTRATO = "compromissoRetiradaContrato";

	private static final String RECUPERACAO_AUTOMATICA_CONTRATO = "recuperacaoAutomaticaContrato";

	private static final String PERIODO_APURACAO = "periodoApuracao";

	private static final String COMPROMISSO_RETIRADA = "compromissoRetirada";

	private static final String QTDA_RETIRADA_PERIODO = "qtdaRetiradaPeriodo";

	private static final String PARADA_PROGRAMADA = "paradaProgramada";

	private static final String PARADA_NAO_PROGRAMADA_CDL = "paradaNaoProgramadaCdl";

	private static final String FALHA_FORNECIMENTO = "falhaFornecimento";

	private static final String CASO_FORTUITO_FORCA_MAIOR = "casoFortuitoForcaMaior";

	private static final String QTDA_RECUPERADA = "qtdaRecuperada";

	private static final String QPNR = "qpnr";

	private static final String QTDA_NAO_RETIRADA_PERIODO = "qtdaNaoRetiradaPeriodo";

	private static final String PERCENTUAL_APLICADO_SOBRE_TARIFA = "percentualAplicadoSobreTarifa";

	private static final String CONSUMO_REF_SUPERIOR_INFERIOR = "consumoRefSuperiorInferior";

	private static final String FAIXA_PENALIDADE_POR_RETIRADA_MAIOR_MENOR = "faixaPenalidadePorRetiradaMaiorMenor";

	private static final String PERCENTUAL_POR_RETIRADA_MAIOR_MENOR = "percentualPorRetiradaMaiorMenor";

	private static final String QNR = "qnr";

	private static final String VALOR_MEDIO_TARIFA = "valorMedioTarifa";

	private static final String VALOR_CALCULADO = "valorCalculado";

	private static final String VALOR_FINAL = "valorFinal";

	private static final String DESCONTO = "desconto";

	private static final String DESCONTO_VALOR = "descontoValor";

	private static final String OBSERVACOES = "observacoes";

	private static final String HABILITAR_CAMPOS_DESCONTO = "habilitarCamposDesconto";

	private static final String BLOQUEAR_CAMPOS = "bloquearCampos";

	private static final String CODIGO_PENALIDADE_TAKE_OR_PAY = "codigoPenalidadeTakeOrPay";

	private static final String CODIGO_PENALIDADE_SHIP_OR_PAY = "codigoPenalidadeShipOrPay";

	private static final String CODIGO_PENALIDADE_DELIVERY_OR_PAY = "codigoPenalidadeDeliveryOrPay";

	private static final String CODIGO_PENALIDADE_RETIRADA_MAIOR = "codigoPenalidadeRetiradaMaior";

	private static final String CODIGO_PENALIDADE_RETIRADA_MENOR = "codigoPenalidadeRetiradaMenor";

	private static final String CODIGO_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO = "codigoPenalidadeGasForaDeEspecificacao";

	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorFatura controladorFatura;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de penalidades.
	 * 
	 * @param model {@link Model}
	 * @return exibirPesquisaPenalidades {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaPenalidades")
	public String exibirPesquisaPenalidades(Model model) throws GGASException {

		if(!model.asMap().containsKey(PENALIDADE_VO)) {
			model.addAttribute("indicadorPenalidadeCobrada", Boolean.FALSE);
		}
		configurarTelaPesquisaPenalidades(model);

		return "exibirPesquisaPenalidades";
	}

	/**
	 * Método responsável por pesquisar as
	 * penalidades.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param tipoPesquisa {@link String}
	 * @param model {@link Model}
	 * @return exibirPesquisaPenalidades {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarPenalidades")
	public String pesquisarPenalidades(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		String tipoPesquisa = penalidadeVO.getTipoPesquisa();
		
		try {

			if (penalidadeVO.getIdCliente() != null && penalidadeVO.getIdCliente() > 0) {
				model.addAttribute(CLIENTE, controladorCliente.obter(penalidadeVO.getIdCliente()));
			}
			
			if (penalidadeVO.getIdImovel() != null && penalidadeVO.getIdImovel() > 0) {
				model.addAttribute(IMOVEL, controladorImovel.obter(penalidadeVO.getIdImovel()));
			}
			
			if (StringUtils.isNotEmpty(tipoPesquisa)) {

				Map<String, Object> filtro = obterFiltroPesquisaPenalidades(penalidadeVO, request, model);

				if (PESQUISA_PENALIDADE.equals(tipoPesquisa)) {

					
					adicionarFiltroPaginacao(request, filtro);

					Collection<ApuracaoQuantidadeVO> listaApuracaoQuantidadeVO = controladorApuracaoPenalidade
							.montarListaApuracaoQuantidadeVO(
									controladorApuracaoPenalidade.listarApuracaoQuantidadeVO(filtro));

					model.addAttribute(LISTA_APURACAO_QTDA_PENALIDADE_PERIODICIDADE,
							criarColecaoPaginada(request, filtro, listaApuracaoQuantidadeVO));

				} else if (PESQUISA_PONTO_CONSUMO.equals(tipoPesquisa)) {
					

					adicionarFiltroPaginacao(request, filtro);
					Collection<PontoConsumoVO> listaPontoConsumoVO = controladorApuracaoPenalidade
							.pesquisarPontoConsumoPenalidade(filtro);

					model.addAttribute(LISTA_PONTO_CONSUMO_VO,
							criarColecaoPaginada(request, filtro, listaPontoConsumoVO));

				}

			}
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		configurarTelaPesquisaPenalidades(model);
		model.addAttribute(PENALIDADE_VO, penalidadeVO);

		return exibirPesquisaPenalidades(model);
	}
	
	/**
	 * Método responsável por exibir a tela de
	 * alterar penalidade.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirAlterarPenalidade {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirAlterarPenalidade")
	public String exibirAlterarPenalidade(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		saveToken(request);

		String tela = "exibirAlterarPenalidade";

		try {
			configurarTelaAlterarPenalidade(penalidadeVO, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarPenalidades(penalidadeVO, bindingResult, request, model);
		}

		model.addAttribute(PENALIDADE_VO, penalidadeVO);

		return tela;
	}
	
	
	/**
	 * Método responsavel por recalcular os dados
	 * da penalidade.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirAlterarPenalidade {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("recalcularPenalidade")
	public String recalcularPenalidade(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes = obterApuracaoQuantidadePenalidadePeriodicidades(penalidadeVO);

			if (apuracoes != null && !apuracoes.isEmpty()) {

				controladorApuracaoPenalidade.processarPenalidades(apuracoes, this.getDadosAuditoria(request));

			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		mensagemSucesso(model, MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);

		model.addAttribute(PENALIDADE_VO, penalidadeVO);

		return exibirAlterarPenalidade(penalidadeVO, bindingResult, request, model);
	}
	
	/**
	 * Método responsável por exibir o relatorio
	 * de notas de debitos e creditos.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return responseEntity {@link ResponseEntity}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirRelatorioNotasDebitosCreditos")
	public ResponseEntity<byte[]> exibirRelatorioNotasDebitosCreditos(HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");

		filtro.put(FORMATO_IMPRESSAO, formatoImpressao);

		ResponseEntity<byte[]> responseEntity = null;

		byte[] relatorioNotaDebitoCredito = (byte[]) request.getSession()
				.getAttribute(RELATORIO_NOTAS_DEBITOS_CREDITOS);
		request.getSession().removeAttribute(RELATORIO_NOTAS_DEBITOS_CREDITOS);

		if (relatorioNotaDebitoCredito != null) {

			FormatoImpressao formatoImpressao2 = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);

			MediaType contentType = MediaType.parseMediaType(this.obterContentTypeRelatorio(formatoImpressao2));

			String filename = String.format("relatorioNotasDebitosCreditos%s",
					this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));

			HttpHeaders headers = new HttpHeaders();

			headers.setContentType(contentType);
			headers.setContentDispositionFormData("attachment", filename);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			responseEntity = new ResponseEntity<byte[]>(relatorioNotaDebitoCredito, headers, HttpStatus.OK);
		}

		return responseEntity;
	}

	/**
	 * Método responsável por aplicar a penalidade.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirAlterarPenalidade {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("aplicarPenalidade")
	public String aplicarPenalidade(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = null;
		
		try {

			validarToken(request);

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades = this
					.obterApuracaoQuantidadePenalidadePeriodicidades(penalidadeVO);

			// Se o tipo de cobrança for no momento da cobrança
			controladorApuracaoPenalidade.aplicarTarifaMediaMomentoCobranca(listaPenalidades, true);

			byte[] relatorio = controladorApuracaoPenalidade.aplicarPenalidade(listaPenalidades,
					this.getDadosAuditoria(request));

			if (relatorio != null) {
				request.getSession().setAttribute(RELATORIO_NOTAS_DEBITOS_CREDITOS, relatorio);
				model.addAttribute(EXIBIR_RELATORIO_NOTAS_DEBITOS_CREDITOS, true);
			}

			mensagemSucesso(model, MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
			
			tela = pesquisarPenalidades(penalidadeVO, bindingResult, request, model);

		} catch (GGASException e) {
			LOG.info(e.getMessage());
			mensagemErroParametrizado(model, request, e);
			tela = exibirAlterarPenalidade(penalidadeVO, bindingResult, request, model);
		}

		return tela;
	}

	/**
	 * Salvar penalidade.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("salvarPenalidade")
	public String salvarPenalidade(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		try {

			validarToken(request);

			Long idApuracaoPenalidade = penalidadeVO.getIdApuracaoQtdaPenalidadePeriodicidade();
			Long idModalidade = penalidadeVO.getIdModalidade();

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> lista =
					controladorApuracaoPenalidade.atualizarPenalidadeModalidade(idModalidade, idApuracaoPenalidade);

			ContratoModalidade modalidade = controladorContrato.obterContratoModalidade(idModalidade);

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : lista) {
				if (apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade() == null) {
					apuracaoQuantidadePenalidadePeriodicidade.setContratoModalidade(modalidade);
					controladorApuracaoPenalidade
							.atualizarApuracaoQuantidadePenalidadePeriodicidade(apuracaoQuantidadePenalidadePeriodicidade);
				}
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirAlterarPenalidade(penalidadeVO, bindingResult, request, model);
	}

	/**
	 * Configurar tela pesquisa penalidades.
	 * 
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarTelaPesquisaPenalidades(Model model) throws GGASException {

		model.addAttribute(LISTA_PENALIDADES, controladorApuracaoPenalidade.listarPenalidades());
		model.addAttribute(INTERVALO_ANOS_DATA, obterIntervaloAnosData());

	}

	/**
	 * Configurar tela alterar penalidade.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarTelaAlterarPenalidade(PenalidadeVO penalidadeVO, HttpServletRequest request, Model model) throws GGASException {

		Long idContrato = penalidadeVO.getIdContrato();
		Long idPenalidade = penalidadeVO.getIdPenalidade();
		Long idPontoConsumo = penalidadeVO.getIdPontoConsumo();
		Long idModalidade = penalidadeVO.getIdModalidade();
		Integer periodoApuracao = Integer.valueOf(request.getParameter(PERIODO_APURACAO));

		if (idModalidade != null && idModalidade <= 0) {
			idModalidade = null;
		}

		ApuracaoQuantidadePenalidadePeriodicidade apuracao = controladorApuracaoPenalidade
				.obterApuracaoQuantidadePenalidadePeriodicidade(idContrato, idPenalidade, idPontoConsumo, periodoApuracao, idModalidade);

		configurarDadosCliente(apuracao, model);
		configurarDadosPontoConsumo(apuracao, model);
		configurarDadosContrato(apuracao, penalidadeVO, model);
		configurarDadosAlterarPenalidade(apuracao, penalidadeVO, model);

	}

	/**
	 * Configurar dados penalidade take or pay.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeTakeOrPay(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());

		this.configurarDadosPenalidade(apuracao, model);
		this.configurarDadosQuantidade(apuracao, model);
		this.configurarDadosQuantidadesNaoRetiradas(apuracao, model);
		this.configurarDadosResultadoApuracao(apuracao, model);

	}

	/**
	 * Configurar dados penalidade ship or pay.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeShipOrPay(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());

		this.configurarDadosPenalidade(apuracao, model);
		this.configurarDadosQuantidade(apuracao, model);
		this.configurarDadosQuantidadesNaoRetiradas(apuracao, model);
		this.configurarDadosResultadoApuracao(apuracao, model);

	}

	/**
	 * Configurar dados penalidade delivery or pay.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeDeliveryOrPay(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model)
			throws GGASException {

		this.configurarDadosPenalidade(apuracao, model);

	}

	/**
	 * Configurar dados penalidade retirada maior.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeRetiradaMaior(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		this.configurarDadosPenalidade(apuracao, model);

	}

	/**
	 * Configurar dados penalidade retirada menor.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeRetiradaMenor(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model)
			throws GGASException {

		this.configurarDadosPenalidade(apuracao, model);

	}

	/**
	 * Configurar dados penalidade gas fora de especificacao.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidadeGasForaDeEspecificacao(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model)
			throws GGASException {

		this.configurarDadosPenalidade(apuracao, model);

	}

	/**
	 * Configurar dados cliente.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosCliente(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		Contrato contrato = obterContratoAtual(apuracao);

		Cliente cliente = contrato.getClienteAssinatura();

		if (cliente != null) {

			model.addAttribute(CLIENTE, controladorCliente.obter(cliente.getChavePrimaria()));

		}

	}

	/**
	 * Configurar dados ponto consumo.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPontoConsumo(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		Contrato contrato = this.obterContratoAtual(apuracao);

		Collection<PontoConsumo> collPontoConsumo = controladorPontoConsumo.listaPontoConsumoPorContrato(contrato.getChavePrimaria());

		if (collPontoConsumo != null) {
			for (PontoConsumo pontoConsumo : collPontoConsumo) {
				listaPontoConsumo.add(pontoConsumo);
			}
		}

		Collections.sort(listaPontoConsumo, new Comparator<PontoConsumo>() {

			@Override
			public int compare(PontoConsumo pontoConsumo1, PontoConsumo pontoConsumo2) {

				return pontoConsumo1.getImovel().getNome().compareTo(pontoConsumo2.getImovel().getNome());
			}
		});

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);

	}

	/**
	 * Configurar dados contrato.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param model {@link Model}
	 */
	private void configurarDadosContrato(ApuracaoQuantidadePenalidadePeriodicidade apuracao, PenalidadeVO penalidadeVO, Model model) {

		Contrato contrato = obterContratoAtual(apuracao);

		if (contrato != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

			penalidadeVO.setIdContrato(apuracao.getContratoAtual().getChavePrimaria());
			model.addAttribute(NUMERO_CONTRATO, contrato.getNumeroFormatado());

			if (contrato.getDataAssinatura() != null) {
				model.addAttribute(DATA_ASSINATURA_CONTRATO, df.format(contrato.getDataAssinatura()));
			} else {
				model.addAttribute(DATA_ASSINATURA_CONTRATO, "");
			}
			if (contrato.getDataVencimentoObrigacoes() != null) {
				model.addAttribute(DATA_VENCIMENTO_OBRIGACOES_CONTRATUAIS_CONTRATO, df.format(contrato.getDataVencimentoObrigacoes()));
			} else {
				model.addAttribute(DATA_VENCIMENTO_OBRIGACOES_CONTRATUAIS_CONTRATO, "");
			}
		}

	}

	/**
	 * Configurar dados filtro pesquisa penalidades.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param filtro {@link Map}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosFiltroPesquisaPenalidades(PenalidadeVO penalidadeVO, Map<String, Object> filtro,
			Model model) throws GGASException {

		if (filtro.get(ID_CLIENTE) != null) {

			model.addAttribute(CLIENTE, controladorCliente.obterCliente((Long) filtro.get(ID_CLIENTE), "enderecos"));

			penalidadeVO.setIndicadorPesquisa("indicadorPesquisaCliente");

		} else if (filtro.get(ID_IMOVEL) != null) {

			model.addAttribute(IMOVEL,
					controladorImovel.obter((Long) filtro.get(ID_IMOVEL), "quadraFace.endereco.cep"));

			penalidadeVO.setIndicadorPesquisa("indicadorPesquisaImovel");

		}

		if (filtro.get(ID_PENALIDADES_ASSOCIADAS) != null) {
			penalidadeVO.setIdPenalidadesAssociadas((Long[]) filtro.get(ID_PENALIDADES_ASSOCIADAS));
		}

		if (filtro.get(ID_PENALIDADES_DISPONIVEIS) != null) {
			penalidadeVO.setIdPenalidadesDisponiveis((Long[]) filtro.get(ID_PENALIDADES_DISPONIVEIS));
		}

		if (filtro.get(NUMERO_CONTRATO) != null) {
			penalidadeVO.setNumeroContrato(String.valueOf(filtro.get(NUMERO_CONTRATO)));
		}

		if (filtro.get(DATA_APURACAO_INICIO) != null && filtro.get(DATA_APURACAO_FIM) != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			penalidadeVO.setDataApuracaoInicio(df.format(filtro.get(DATA_APURACAO_INICIO)));
			penalidadeVO.setDataApuracaoFim(df.format(filtro.get(DATA_APURACAO_FIM)));
		}

		if (filtro.get(COBRADA) != null) {
			Boolean cobrada = (Boolean) filtro.get(COBRADA);
			penalidadeVO.setCobrada(cobrada);
		} else {
			penalidadeVO.setCobrada(null);
		}

	}

	/**
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @return contrato {@link Contrato}
	 */
	private Contrato obterContratoAtual(ApuracaoQuantidadePenalidadePeriodicidade apuracao) {

		Contrato contrato = apuracao.getContratoAtual();
		if (contrato == null) {
			contrato = apuracao.getContrato();
		}

		return contrato;
	}

	/**
	 * Configurar dados penalidade.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosPenalidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		Contrato contrato = obterContratoAtual(apuracao);

		Map<String, Object> params = new HashMap<String, Object>();

		Long codigoPenalidadeRetiradaMaior = (Long) model.asMap().get(CODIGO_PENALIDADE_RETIRADA_MAIOR);

		ContratoPenalidade contratoPenalidadeRetiradaMaior = null;
		ContratoPenalidade contratoPenalidadeRetiradaMenor = null;

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMaior = null;
		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMenor = null;

		EntidadeConteudo consumoReferenciaMenor = null;
		Penalidade penalidadeMenor = null;
		BigDecimal percentualValorMenor = null;

		EntidadeConteudo consumoReferenciaMaior = null;
		Penalidade penalidadeMaior = null;
		BigDecimal percentualValorMaior = null;

		if (codigoPenalidadeRetiradaMaior != null) {

			params.put(ID_CONTRATO, contrato.getChavePrimaria());
			params.put("dataInicioVigencia", apuracao.getDataInicioApuracao());
			params.put("dataFimVigencia", apuracao.getDataFimApuracao());
			params.put("idPenalidadeRetiradaMaiorMenor", codigoPenalidadeRetiradaMaior);

			contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

			if (contratoPenalidadeRetiradaMaior != null) {

				consumoReferenciaMaior = contratoPenalidadeRetiradaMaior.getConsumoReferencia();
				penalidadeMaior = contratoPenalidadeRetiradaMaior.getPenalidade();
				percentualValorMaior = contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor();
			}

			if (apuracao.getContratoModalidade() != null) {
				ContratoPontoConsumoModalidade cpcm =
						controladorContrato.obterContratoPontoConsumoModalidade(apuracao.getPontoConsumo().getChavePrimaria(),
								apuracao.getContratoAtual().getChavePrimaria(), apuracao.getContratoModalidade().getChavePrimaria());

				params.put("idContratoModalidade", cpcm.getChavePrimaria());
				contratoPontoConsumoPenalidadeMaior = controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);
			}
			if (contratoPontoConsumoPenalidadeMaior != null) {

				consumoReferenciaMaior = contratoPontoConsumoPenalidadeMaior.getConsumoReferencia();
				penalidadeMaior = contratoPontoConsumoPenalidadeMaior.getPenalidade();
				percentualValorMaior = contratoPontoConsumoPenalidadeMaior.getValorPercentualRetMaiorMenor();

			}

		}

		Long codigoPenalidadeRetiradaMenor = (Long) model.asMap().get(CODIGO_PENALIDADE_RETIRADA_MENOR);

		if (codigoPenalidadeRetiradaMenor != null) {
			params.put(ID_CONTRATO, contrato.getChavePrimaria());
			params.put("dataInicioVigencia", apuracao.getDataInicioApuracao());
			params.put("dataFimVigencia", apuracao.getDataFimApuracao());
			params.put("idPenalidadeRetiradaMaiorMenor", codigoPenalidadeRetiradaMenor);
			contratoPenalidadeRetiradaMenor = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

			if (contratoPenalidadeRetiradaMenor != null) {

				consumoReferenciaMenor = contratoPenalidadeRetiradaMenor.getConsumoReferencia();
				penalidadeMenor = contratoPenalidadeRetiradaMenor.getPenalidade();
				percentualValorMenor = contratoPenalidadeRetiradaMenor.getValorPercentualRetMaiorMenor();
			}

			if (apuracao.getContratoModalidade() != null) {
				ContratoPontoConsumoModalidade cpcm =
						controladorContrato.obterContratoPontoConsumoModalidade(apuracao.getPontoConsumo().getChavePrimaria(),
								apuracao.getContratoAtual().getChavePrimaria(), apuracao.getContratoModalidade().getChavePrimaria());

				params.put("idContratoModalidade", cpcm.getChavePrimaria());
				contratoPontoConsumoPenalidadeMenor = controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

			}
			if (contratoPontoConsumoPenalidadeMenor != null) {

				consumoReferenciaMenor = contratoPontoConsumoPenalidadeMenor.getConsumoReferencia();
				penalidadeMenor = contratoPontoConsumoPenalidadeMenor.getPenalidade();
				percentualValorMenor = contratoPontoConsumoPenalidadeMenor.getValorPercentualRetMaiorMenor();

			}

		}

		long idPontoConsumo = 0;
		long idPrimeiroPontoConsumo = 0;

		if (apuracao.getPontoConsumo() != null) {
			idPontoConsumo = apuracao.getPontoConsumo().getChavePrimaria();
			idPrimeiroPontoConsumo = idPontoConsumo;
		} else {
			Collection<ContratoPontoConsumo> listaContratoPontoConsumo = contrato.getListaContratoPontoConsumo();
			ContratoPontoConsumo primeiroContratoPontoConsumo = listaContratoPontoConsumo.iterator().next();
			PontoConsumo primeiroPontoConsumo = primeiroContratoPontoConsumo.getPontoConsumo();
			idPrimeiroPontoConsumo = primeiroPontoConsumo.getChavePrimaria();

		}

		if ((model.asMap().containsKey(CODIGO_PENALIDADE_TAKE_OR_PAY)) || (model.asMap().containsKey(CODIGO_PENALIDADE_SHIP_OR_PAY))) {

			model.addAttribute(ID_APURACAO_QTDA_PENALIDADE_PERIODICIDADE, apuracao.getChavePrimaria());
			model.addAttribute(APURACAO, apuracao);

			if (apuracao.getPeriodoApuracao() != null) {
				model.addAttribute(PERIODICIDADE_CONTRATO, apuracao.getPeriodoApuracaoFormatado());
			} else {
				model.addAttribute(PERIODICIDADE_CONTRATO, "");
			}
			if (apuracao.getContratoModalidade() != null) {

				model.addAttribute(MODALIDADE_CONTRATO, apuracao.getContratoModalidade().getDescricao());
				model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());

				ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = controladorContrato.obterContratoPontoConsumoModalidade(
						idPrimeiroPontoConsumo, contrato.getChavePrimaria(), apuracao.getContratoModalidade().getChavePrimaria());

				if (contratoPontoConsumoModalidade != null) {

					if (contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica()) {
						model.addAttribute(RECUPERACAO_AUTOMATICA_CONTRATO, "Sim");
					} else {
						model.addAttribute(RECUPERACAO_AUTOMATICA_CONTRATO, "Não");
					}
					if (apuracao.getPenalidade() != null && apuracao.getPeriodicidadePenalidade() != null) {

						ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
								controladorContrato.obterContratoPontoConsumoPenalidade(contratoPontoConsumoModalidade.getChavePrimaria(),
										apuracao.getPenalidade().getChavePrimaria(),
										apuracao.getPeriodicidadePenalidade().getChavePrimaria(), apuracao.getDataInicioApuracao(),
										apuracao.getDataFimApuracao());

						if (contratoPontoConsumoPenalidade != null) {

							if (contratoPontoConsumoPenalidade.getConsumoReferencia() != null) {
								model.addAttribute(CONSUMO_REFERENCIA_INFERIOR_CONTRATO,
										contratoPontoConsumoPenalidade.getConsumoReferencia().getDescricao());
							} else {
								model.addAttribute(CONSUMO_REFERENCIA_INFERIOR_CONTRATO, "");
							}

							BigDecimal percenMargemVariacao = contratoPontoConsumoPenalidade.getPercentualMargemVariacao();

							if (percenMargemVariacao == null) {
								percenMargemVariacao = BigDecimal.ZERO;
							}

							BigDecimal percentualMargemVariacao = percenMargemVariacao.multiply(BigDecimal.valueOf(Double.valueOf("100")));

							contratoPontoConsumoPenalidade.setPercentualMargemVariacao(percentualMargemVariacao);
							model.addAttribute(COMPROMISSO_RETIRADA_CONTRATO,
									this.converterValorBigDecimalParaString(percenMargemVariacao));

						} else {

							verificarConsumoReferencia(apuracao, model, contrato);
						}
					}
				}

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades =
						new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();
				listaPenalidades.add(apuracao);
				controladorApuracaoPenalidade.aplicarTarifaMediaMomentoCobranca(listaPenalidades, false);

			} else {

				verificarConsumoReferencia(apuracao, model, contrato);
			}

		} else if ((model.asMap().containsKey(CODIGO_PENALIDADE_DELIVERY_OR_PAY))
				|| (model.asMap().containsKey(CODIGO_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO))) {

			Long codigo = null;
			if (model.asMap().containsKey(CODIGO_PENALIDADE_DELIVERY_OR_PAY)) {
				codigo = (Long) model.asMap().get(CODIGO_PENALIDADE_DELIVERY_OR_PAY);
			} else {
				codigo = (Long) model.asMap().get(CODIGO_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO);
			}

			if (apuracao.getPeriodoApuracao() != null) {
				model.addAttribute(PERIODICIDADE_CONTRATO, apuracao.getPeriodoApuracaoFormatado());
			} else {
				model.addAttribute(PERIODICIDADE_CONTRATO, "");
			}
			model.addAttribute(PERCENTUAL_APLICADO_SOBRE_TARIFA,
					this.converterValorBigDecimalParaString(contrato.getPercentualTarifaDoP()));
			model.addAttribute(LISTA_APURACAO_QTDA_PENALIDADE_PERIODICIDADE,
					controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(apuracao.getPeriodoApuracao(),
							idPontoConsumo, codigo, apuracao.getContratoAtual()));

		} else if (model.asMap().containsKey(CODIGO_PENALIDADE_RETIRADA_MAIOR)) {

			if (consumoReferenciaMaior != null) {
				model.addAttribute(CONSUMO_REF_SUPERIOR_INFERIOR, consumoReferenciaMaior.getDescricao());
			}

			if (penalidadeMaior != null) {
				model.addAttribute(FAIXA_PENALIDADE_POR_RETIRADA_MAIOR_MENOR, penalidadeMaior.getDescricao());
			}

			BigDecimal percentual = percentualValorMaior;
			if (percentual != null) {
				percentual = percentual.multiply(BigDecimal.valueOf(Double.valueOf("100")));
			}

			model.addAttribute(PERCENTUAL_POR_RETIRADA_MAIOR_MENOR, this.converterValorBigDecimalParaString(percentual));

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades =
					controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(apuracao.getPeriodoApuracao(),
							idPontoConsumo, (Long) model.asMap().get(CODIGO_PENALIDADE_RETIRADA_MAIOR), apuracao.getContratoAtual());

			controladorApuracaoPenalidade.aplicarTarifaMediaMomentoCobranca(listaPenalidades, false);
			model.addAttribute(BLOQUEAR_CAMPOS, false);
			model.addAttribute(LISTA_APURACAO_QTDA_PENALIDADE_PERIODICIDADE, listaPenalidades);
			model.addAttribute(APURACAO, apuracao);

		} else if (model.asMap().containsKey(CODIGO_PENALIDADE_RETIRADA_MENOR)) {
			if (consumoReferenciaMenor != null) {
				model.addAttribute(CONSUMO_REF_SUPERIOR_INFERIOR, consumoReferenciaMenor.getDescricao());
			}

			if (penalidadeMenor != null) {
				model.addAttribute(FAIXA_PENALIDADE_POR_RETIRADA_MAIOR_MENOR, penalidadeMenor.getDescricao());
			}

			BigDecimal percentual = percentualValorMenor;
			if (percentual != null) {
				percentual = percentual.multiply(BigDecimal.valueOf(Double.valueOf("100")));
			}
			model.addAttribute(PERCENTUAL_POR_RETIRADA_MAIOR_MENOR, this.converterValorBigDecimalParaString(percentual));

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades =
					controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(apuracao.getPeriodoApuracao(),
							idPontoConsumo, (Long) model.asMap().get(CODIGO_PENALIDADE_RETIRADA_MENOR), apuracao.getContratoAtual());
			controladorApuracaoPenalidade.aplicarTarifaMediaMomentoCobranca(listaPenalidades, false);
			model.addAttribute(BLOQUEAR_CAMPOS, false);
			model.addAttribute(LISTA_APURACAO_QTDA_PENALIDADE_PERIODICIDADE, listaPenalidades);
			model.addAttribute(APURACAO, apuracao);
		}

	}

	/**Verifica se o consumo referencia de um contrato penalidade é nulo
	 * 
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @param contrato {@link Contrato}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void verificarConsumoReferencia(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model, Contrato contrato)
			throws NegocioException {
		
		ContratoPenalidade contratoPenalidade = controladorContrato.obterContratoPenalidade(contrato.getChavePrimaria(),
				apuracao.getPenalidade().getChavePrimaria(), apuracao.getPeriodicidadePenalidade().getChavePrimaria(),
				apuracao.getDataInicioApuracao(), apuracao.getDataFimApuracao());

		if (contratoPenalidade != null) {

			if (contratoPenalidade.getConsumoReferencia() != null) {
				model.addAttribute(CONSUMO_REFERENCIA_INFERIOR_CONTRATO, contratoPenalidade.getConsumoReferencia().getDescricao());
			} else {
				model.addAttribute(CONSUMO_REFERENCIA_INFERIOR_CONTRATO, "");
			}

			BigDecimal percentualMargemVariacao =
					contratoPenalidade.getPercentualMargemVariacao().multiply(BigDecimal.valueOf(Double.valueOf("100")));

			contratoPenalidade.setPercentualMargemVariacao(percentualMargemVariacao);
			model.addAttribute(COMPROMISSO_RETIRADA_CONTRATO,
					this.converterValorBigDecimalParaString(contratoPenalidade.getPercentualMargemVariacao()));

		}

	}

	/**
	 * Configurar dados quantidade.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosQuantidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) throws GGASException {

		if (apuracao.getQtdaDiariaContratada() != null) {
			model.addAttribute(COMPROMISSO_RETIRADA, this.converterValorBigDecimalParaString(apuracao.getQtdaDiariaContratada()));
		} else {
			model.addAttribute(COMPROMISSO_RETIRADA, this.converterValorBigDecimalParaString(apuracao.getQtdaDiariaProgramada()));
		}

		model.addAttribute(QTDA_RETIRADA_PERIODO, this.converterValorBigDecimalParaString(apuracao.getQtdaDiariaRetirada()));

		model.addAttribute(LISTA_QTDA_CONTRATADA_VO, controladorApuracaoPenalidade.obterQuantidadeContratrada(apuracao));
		model.addAttribute(LISTA_QTDA_RETIRADA_PERIODO_VO, controladorApuracaoPenalidade.obterQuantidadeRetiradaPeriodo(apuracao));

	}

	/**
	 * Configurar dados quantidades nao retiradas.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosQuantidadesNaoRetiradas(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model)
			throws GGASException {

		Long codigoPenalidadeTakeOrPay =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));

		Long idPontoConsumo = apuracao.getPontoConsumo().getChavePrimaria();
		Contrato contratoAtual = apuracao.getContratoAtual();
		Integer periodoApuracao = apuracao.getPeriodoApuracao();

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> lista =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(periodoApuracao, idPontoConsumo,
						codigoPenalidadeTakeOrPay, contratoAtual);
		BigDecimal qnr = BigDecimal.ZERO;
		BigDecimal qpnr = BigDecimal.ZERO;
		if (lista != null) {
			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : lista) {

				BigDecimal valorQnr = null;

				if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada() != null) {
					valorQnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();
				} else {
					valorQnr = BigDecimal.ZERO;
				}
				qnr = qnr.add(valorQnr);

				BigDecimal valorQpnr = null;

				if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada() != null) {
					valorQpnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
				} else {
					valorQpnr = BigDecimal.ZERO;
				}
				qpnr = qpnr.add(valorQpnr);
			}
		}

		model.addAttribute(PARADA_PROGRAMADA, this.converterValorBigDecimalParaString(apuracao.getQtdaNaoRetiradaPorParadaProgramada()));
		model.addAttribute(PARADA_NAO_PROGRAMADA_CDL,
				this.converterValorBigDecimalParaString(apuracao.getQtdaNaoRetiradaPorParadaNaoProgramada()));
		model.addAttribute(FALHA_FORNECIMENTO, this.converterValorBigDecimalParaString(apuracao.getQtdaNaoRetiradaPorFalhaFornecimento()));
		model.addAttribute(CASO_FORTUITO_FORCA_MAIOR,
				this.converterValorBigDecimalParaString(apuracao.getQtdaNaoRetiradaPorCasoFortuito()));
		model.addAttribute(QTDA_RECUPERADA, this.converterValorBigDecimalParaString(apuracao.getQtdaRecuperada()));
		model.addAttribute(QPNR, this.converterValorBigDecimalParaString(qpnr));
		model.addAttribute(QTDA_NAO_RETIRADA_PERIODO, this.converterValorBigDecimalParaString(qnr));

		model.addAttribute(LISTA_QTDA_NAO_RETIRADA_VO_PARADA_PROGRAMADA,
				controladorApuracaoPenalidade.obterQuantidadeNaoRetirada(apuracao, EntidadeConteudo.CHAVE_PARADA_PROGRAMADA));
		model.addAttribute(LISTA_QTDA_NAO_RETIRADA_VO_PARADA_PROGRAMADA_CDL,
				controladorApuracaoPenalidade.obterQuantidadeNaoRetirada(apuracao, EntidadeConteudo.CHAVE_PARADA_NAO_PROGRAMA));
		model.addAttribute(LISTA_QTDA_NAO_RETIRADA_VO_FALHA_FORNECIMENTO,
				controladorApuracaoPenalidade.obterQuantidadeNaoRetirada(apuracao, EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO));
		model.addAttribute(LISTA_QTDA_NAO_RETIRADA_VO_CASO_FORTUITO_FORCA_MAIOR,
				controladorApuracaoPenalidade.obterQuantidadeNaoRetirada(apuracao, EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO));

	}

	/**
	 * Configurar dados resultado apuracao.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param model {@link Model}
	 */
	private void configurarDadosResultadoApuracao(ApuracaoQuantidadePenalidadePeriodicidade apuracao, Model model) {

		String valorCalculado = this.converterValorBigDecimalParaString(apuracao.getValorCalculado());

		Boolean habilitarCamposDesconto = Boolean.FALSE;
		model.addAttribute(QNR, model.asMap().get(QTDA_NAO_RETIRADA_PERIODO));
		model.addAttribute(VALOR_MEDIO_TARIFA, this.converterValorBigDecimalParaString(apuracao.getValorTarifaMedia()));

		model.addAttribute(VALOR_CALCULADO, valorCalculado);

		if (apuracao.getValorCobrado() != null) {

			model.addAttribute(VALOR_FINAL, this.converterValorBigDecimalParaString(apuracao.getValorCobrado()));
			model.addAttribute(DESCONTO, this.converterValorBigDecimalParaString(apuracao.getPercentualDescontoAplicado()));
			model.addAttribute(DESCONTO_VALOR, this.converterValorBigDecimalParaString(apuracao.getValorDescontoAplicado()));
			model.addAttribute(OBSERVACOES, apuracao.getObservacoes());

		} else {

			model.addAttribute(VALOR_FINAL, valorCalculado);

		}

		if (apuracao.getValorCalculado() != null && apuracao.getValorCalculado().compareTo(BigDecimal.ZERO) > 0) {
			habilitarCamposDesconto = Boolean.TRUE;
		}

		model.addAttribute(HABILITAR_CAMPOS_DESCONTO, habilitarCamposDesconto);

	}

	/**
	 * Obter valor big decimal string.
	 * 
	 * @param valorFormatado {@link String}
	 * @return um valor formatado {@link BigDecimal}
	 * @throws GGASException {@link GGASException} 
	 */
	private BigDecimal obterValorBigDecimalString(String valorFormatado) throws GGASException {

		if (valorFormatado != null && StringUtils.isNotEmpty(valorFormatado)) {
			return Util.converterCampoStringParaValorBigDecimal("Valor", valorFormatado, Constantes.FORMATO_VALOR_BR,
					Constantes.LOCALE_PADRAO);
		} else {
			return null;
		}

	}

	/**
	 * Converter valor big decimal para string.
	 * 
	 * @param valor {@link BigDecimal}
	 * @return um valor formatado {@link String}
	 */
	private String converterValorBigDecimalParaString(BigDecimal valor) {

		if (valor != null) {
			return String.valueOf(valor);
		} else {
			return "";
		}

	}

	/**
	 * Obter intervalo anos data.
	 * 
	 * @return uma data {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private String obterIntervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		return String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();
	}

	/**
	 * Obter apuracao quantidade penalidade periodicidades.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param idsApuracaoQtdaPenalidadePeriodicidade {@link idsApuracaoQtdaPenalidadePeriodicidade}
	 * @param desc {@link String}
	 * @param descValor {@link String}
	 * @param obs {@link String}
	 * @return lista {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> obterApuracaoQuantidadePenalidadePeriodicidades(PenalidadeVO penalidadeVO)
			throws GGASException {

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> lista = new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();

		if (penalidadeVO.getIdsApuracaoQtdaPenalidadePeriodicidade() != null
				&& penalidadeVO.getIdsApuracaoQtdaPenalidadePeriodicidade().length > 0) {

			for (int i = 0; i < penalidadeVO.getIdsApuracaoQtdaPenalidadePeriodicidade().length; i++) {

				ApuracaoQuantidadePenalidadePeriodicidade apuracao = controladorApuracaoPenalidade
						.obterApuracaoQuantidadePenalidadePeriodicidade(penalidadeVO.getIdsApuracaoQtdaPenalidadePeriodicidade()[i]);
				apuracao.setPercentualDescontoAplicado(this.obterValorBigDecimalString(penalidadeVO.getDesc()[i]));
				apuracao.setValorDescontoAplicado(this.obterValorBigDecimalString(penalidadeVO.getDescValor()[i]));
				apuracao.setObservacoes(penalidadeVO.getObs()[i]);

				lista.add(apuracao);
			}

		} else {

			String desconto = penalidadeVO.getDesconto();
			String descontoValor = penalidadeVO.getDescontoValor();
			String observacoes = penalidadeVO.getObservacoes();
			Long idApuracaoQtdaPenalidadePeriodicidade = penalidadeVO.getIdApuracaoQtdaPenalidadePeriodicidade();

			ApuracaoQuantidadePenalidadePeriodicidade apuracao =
					controladorApuracaoPenalidade.obterApuracaoQuantidadePenalidadePeriodicidade(idApuracaoQtdaPenalidadePeriodicidade);

			if (desconto != null && !desconto.isEmpty()) {
				apuracao.setPercentualDescontoAplicado(this.obterValorBigDecimalString(desconto));
			}
			if (descontoValor != null && !descontoValor.isEmpty()) {
				apuracao.setValorDescontoAplicado(this.obterValorBigDecimalString(descontoValor));
			}
			apuracao.setObservacoes(observacoes);

			lista.add(apuracao);
		}

		return lista;
	}

	/**
	 * Apurar.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @return String {@link String}
	 * @throws GGASException {@link String}
	 */
	@RequestMapping("apurar")
	public String apurar(PenalidadeVO penalidadeVO, BindingResult bindingResult) throws GGASException {

		Long contrato = Long.parseLong((String) penalidadeVO.getNumeroContrato());
		Long pontoConsumo = penalidadeVO.getIdPontoConsumo();

		Date periodoIncial = Util.converterCampoStringParaData(DATA_APURACAO_INICIO, penalidadeVO.getDataApuracaoInicio(), "dd/MM/yyyy");
		Date periodoFinal = Util.converterCampoStringParaData(DATA_APURACAO_FIM, penalidadeVO.getDataApuracaoFim(), "dd/MM/yyyy");

		controladorApuracaoPenalidade.apuracaoAdapter(contrato, pontoConsumo, periodoIncial, periodoFinal);

		return null;
	}

	/**
	 * Tratar penalidades.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("tratarPenalidades")
	public String tratarPenalidades(PenalidadeVO penalidadeVO, BindingResult bindingResult, HttpServletRequest request)
			throws GGASException {

		// TODO método temporário apenas para
		// teste. REMOVER!!!

		Long idContrato = penalidadeVO.getIdContrato();
		Long idPontoConsumo = penalidadeVO.getIdPontoConsumo();
		Date periodoIncial = new Date();
		Date periodoFinal = new Date();

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		Contrato contrato = (Contrato) controladorContrato.obter(idContrato);
		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(idPontoConsumo);

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		listaPontoConsumo.add(pontoConsumo);

		controladorApuracaoPenalidade.tratarPenalidades(contrato, listaPontoConsumo, periodoIncial, periodoFinal, dadosAuditoria, null);

		return null;
	}
	
	/**
	 * Configurar dados alterar penalidade.
	 * 
	 * @param apuracao {@link ApuracaoQuantidadePenalidadePeriodicidade}
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarDadosAlterarPenalidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao, PenalidadeVO penalidadeVO,
			Model model) throws GGASException {

		Boolean bloquearCampos = Boolean.FALSE;

		if (apuracao.getValorCalculado() == null || (apuracao.getFatura() != null
				&& (!controladorFatura.verificarFaturaSituacaoPendente(apuracao.getFatura().getChavePrimaria())))) {
			bloquearCampos = Boolean.TRUE;
		}

		model.addAttribute(BLOQUEAR_CAMPOS, bloquearCampos);

		if (apuracao.getPenalidade() != null) {

			penalidadeVO.setIdPenalidade(apuracao.getPenalidade().getChavePrimaria());
			model.addAttribute(DESCRICAO_PENALIDADE, apuracao.getPenalidade().getDescricao());

			Long codigoPenalidadeTakeOrPay =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));
			Long codigoPenalidadeShipOrPay =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY));
			Long codigoPenalidadeDeliveryOrPay =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY));
			Long codigoPenalidadeRetiradaMaior =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));
			Long codigoPenalidadeRetiradaMenor =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));
			Long codigoPenalidadeGasForaDeEspecificacao = Long.valueOf(
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO));

			if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeTakeOrPay) {
				model.addAttribute(CODIGO_PENALIDADE_TAKE_OR_PAY, codigoPenalidadeTakeOrPay);
				this.configurarDadosPenalidadeTakeOrPay(apuracao, model);
			} else if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeShipOrPay) {
				model.addAttribute(CODIGO_PENALIDADE_SHIP_OR_PAY, codigoPenalidadeShipOrPay);
				this.configurarDadosPenalidadeShipOrPay(apuracao, model);
			} else if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeDeliveryOrPay) {
				model.addAttribute(CODIGO_PENALIDADE_DELIVERY_OR_PAY, codigoPenalidadeDeliveryOrPay);
				this.configurarDadosPenalidadeDeliveryOrPay(apuracao, model);
			} else if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMaior) {
				model.addAttribute(CODIGO_PENALIDADE_RETIRADA_MAIOR, codigoPenalidadeRetiradaMaior);
				this.configurarDadosPenalidadeRetiradaMaior(apuracao, model);
			} else if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMenor) {
				model.addAttribute(CODIGO_PENALIDADE_RETIRADA_MENOR, codigoPenalidadeRetiradaMenor);
				this.configurarDadosPenalidadeRetiradaMenor(apuracao, model);
			} else if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeGasForaDeEspecificacao) {
				model.addAttribute(CODIGO_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO, codigoPenalidadeGasForaDeEspecificacao);
				this.configurarDadosPenalidadeGasForaDeEspecificacao(apuracao, model);
			}

		}

	}

	/**
	 * Verifica se a Data Inicial e a Data Final do campo Data de apuração foram preenchidos corretamente.
	 * 
	 * @param filtro
	 * @param penalidadeVO
	 * @throws GGASException
	 */
	private void verificaDataInicioDataFim(Map<String, Object> filtro, PenalidadeVO penalidadeVO) throws GGASException {

		Boolean campoDataInicioVazio = false;
		Boolean campoDataFimVazio = false;
		Date dataInicio = null;
		Date dataFim = null;

		if (penalidadeVO.getDataApuracaoInicio() != null && StringUtils.isNotEmpty(penalidadeVO.getDataApuracaoInicio())) {
			campoDataInicioVazio = true;
			dataInicio =
					Util.converterCampoStringParaData("Data de Apuração", penalidadeVO.getDataApuracaoInicio(), Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_APURACAO_INICIO, dataInicio);
		}

		if (penalidadeVO.getDataApuracaoFim() != null && StringUtils.isNotEmpty(penalidadeVO.getDataApuracaoFim())) {
			campoDataFimVazio = true;
			dataFim = Util.converterCampoStringParaData("Data de Apuração", penalidadeVO.getDataApuracaoFim(), Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_APURACAO_FIM, dataFim);
		}

		if (!campoDataInicioVazio && campoDataFimVazio) {
			throw new GGASException(Constantes.ERRO_NEGOCIO_DATA_INICIAL_OBRIGATORIO, true);
		}

		if ((campoDataInicioVazio && campoDataFimVazio) && (dataFim.before(dataInicio))) {
			throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
		}

	}

	/**
	 * Obter filtro pesquisa penalidades.
	 * 
	 * @param penalidadeVO {@link PenalidadeVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> obterFiltroPesquisaPenalidades(PenalidadeVO penalidadeVO, HttpServletRequest request, Model model)
			throws GGASException {

		Boolean exibirDadosFiltroSessao = Boolean.valueOf(request.getParameter(EXIBIR_DADOS_FILTRO));

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (exibirDadosFiltroSessao != null && exibirDadosFiltroSessao) {

			filtro = (Map<String, Object>) request.getSession().getAttribute(FILTRO_PENALIDADES);
			configurarDadosFiltroPesquisaPenalidades(penalidadeVO, filtro, model);

		} else {

			String numeroContrato = penalidadeVO.getNumeroContrato();
			Boolean cobrada = penalidadeVO.getCobrada();

			Long idCliente = penalidadeVO.getIdCliente();
			Long idImovel = penalidadeVO.getIdImovel();
			Long idContrato = penalidadeVO.getIdContrato();
			Long idPontoConsumo = penalidadeVO.getIdPontoConsumo();
			Long[] idPenalidadesAssociadas = penalidadeVO.getIdPenalidadesAssociadas();
			Long[] idPenalidadesDisponiveis = penalidadeVO.getIdPenalidadesDisponiveis();

			if (idCliente != null && idCliente > 0) {
				filtro.put(ID_CLIENTE, idCliente);
			}

			if (idImovel != null && idImovel > 0) {
				filtro.put(ID_IMOVEL, idImovel);
			}

			if (idPontoConsumo != null && idPontoConsumo > 0) {
				filtro.put(ID_PONTO_CONSUMO, idPontoConsumo);
			}

			if (idContrato != null && idContrato > 0) {
				filtro.put(ID_CONTRATO, idContrato);
			}

			if (idPenalidadesAssociadas != null && idPenalidadesAssociadas.length != 0) {
				filtro.put(ID_PENALIDADES_ASSOCIADAS, idPenalidadesAssociadas);
			}

			verificaDataInicioDataFim(filtro, penalidadeVO);

			obterFiltroPesquisaPenalidadesDois(filtro, numeroContrato, cobrada, idPenalidadesDisponiveis);

			request.getSession().setAttribute(FILTRO_PENALIDADES, filtro);
		}

		return filtro;
	}

	/**
	 * Obter filtro pesquisa penalidades.
	 * 
	 * @param filtro {@link Map}
	 * @param numeroContrato {@link String}
	 * @param cobrada {@link Boolean}
	 * @param idPenalidadesDisponiveis {@link Long}
	 * @throws GGASException {@link GGASException}
	 */
	public void obterFiltroPesquisaPenalidadesDois(Map<String, Object> filtro, String numeroContrato, Boolean cobrada,
			Long[] idPenalidadesDisponiveis) throws GGASException {

		if (idPenalidadesDisponiveis != null && idPenalidadesDisponiveis.length != 0) {
			filtro.put(ID_PENALIDADES_DISPONIVEIS, idPenalidadesDisponiveis);
		}

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTRATO, Util.converterCampoStringParaValorInteger(CONTRATO, numeroContrato));
		}

		if (cobrada != null) {
			filtro.put(COBRADA, cobrada);
		}

	}

}
