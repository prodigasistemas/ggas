/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/12/2014 15:03:26
 @author rfilho
 */

package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.util.Util;

/**
 * PenalidadesRetiradaMaiorMenorVO
 *
 */
public class PenalidadesRetiradaMaiorMenorVO implements Serializable {

	private static final Logger LOG = Logger.getLogger(PenalidadesRetiradaMaiorMenorVO.class);

	private static final long serialVersionUID = -825902918071993992L;

	private Contrato contrato;

	private Penalidade penalidadeRetiradaMaiorMenor;

	private EntidadeConteudo entidadePeriodicidadeRetiradaMaiorMenor;

	private EntidadeConteudo entidadeBaseApuracaoRetiradaMaiorMenor;

	private Date dataInicioVigencia;

	private Date dataFimVigencia;

	private EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor;

	private EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor;

	private EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor;

	private BigDecimal valorPercentualCobRetMaiorMenor;

	private BigDecimal valorPercentualCobIntRetMaiorMenor;

	private BigDecimal valorPercentualRetMaiorMenor;

	private Boolean indicadorImposto;

	/**
	 * @return penalidadeRetiradaMaiorMenor
	 */
	public Penalidade getPenalidadeRetiradaMaiorMenor() {

		return penalidadeRetiradaMaiorMenor;
	}

	/**
	 * @param penalidadeRetiradaMaiorMenor
	 */
	public void setPenalidadeRetiradaMaiorMenor(Penalidade penalidadeRetiradaMaiorMenor) {

		this.penalidadeRetiradaMaiorMenor = penalidadeRetiradaMaiorMenor;
	}

	/**
	 * @return entidadePeriodicidadeRetiradaMaiorMenor
	 */
	public EntidadeConteudo getEntidadePeriodicidadeRetiradaMaiorMenor() {

		return entidadePeriodicidadeRetiradaMaiorMenor;
	}

	/**
	 * @param entidadePeriodicidadeRetiradaMaiorMenor
	 */
	public void setEntidadePeriodicidadeRetiradaMaiorMenor(EntidadeConteudo entidadePeriodicidadeRetiradaMaiorMenor) {

		this.entidadePeriodicidadeRetiradaMaiorMenor = entidadePeriodicidadeRetiradaMaiorMenor;
	}

	/**
	 * @return entidadeBaseApuracaoRetiradaMaiorMenor
	 */
	public EntidadeConteudo getEntidadeBaseApuracaoRetiradaMaiorMenor() {

		return entidadeBaseApuracaoRetiradaMaiorMenor;
	}

	/**
	 * @param entidadeBaseApuracaoRetiradaMaiorMenor
	 */
	public void setEntidadeBaseApuracaoRetiradaMaiorMenor(EntidadeConteudo entidadeBaseApuracaoRetiradaMaiorMenor) {

		this.entidadeBaseApuracaoRetiradaMaiorMenor = entidadeBaseApuracaoRetiradaMaiorMenor;
	}

	/**
	 * @return dataInicioVigencia
	 */
	public Date getDataInicioVigencia() {
		Date data = null;
		if (this.dataInicioVigencia != null) {
			data = (Date) dataInicioVigencia.clone();
		}
		return data;
	}

	/**
	 * @param dataInicioVigencia
	 */
	public void setDataInicioVigencia(Date dataInicioVigencia) {
		if(dataInicioVigencia != null) {
			this.dataInicioVigencia = (Date) dataInicioVigencia.clone();
		} else {
			this.dataInicioVigencia = null;
		}
	}

	/**
	 * @return dataFimVigencia
	 */
	public Date getDataFimVigencia() {
		Date data = null;
		if (this.dataFimVigencia != null) {
			data = (Date) dataFimVigencia.clone();
		}
		return data;
	}

	/**
	 * @param dataFimVigencia
	 */
	public void setDataFimVigencia(Date dataFimVigencia) {
		if(dataFimVigencia != null) {
			this.dataFimVigencia = (Date) dataFimVigencia.clone();
		} else {
			this.dataFimVigencia = null;
		}
	}

	/**
	 * @return entidadePrecoCobrancaRetiradaMaiorMenor
	 */
	public EntidadeConteudo getEntidadePrecoCobrancaRetiradaMaiorMenor() {

		return entidadePrecoCobrancaRetiradaMaiorMenor;
	}

	/**
	 * @param entidadePrecoCobrancaRetiradaMaiorMenor
	 */
	public void setEntidadePrecoCobrancaRetiradaMaiorMenor(EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor) {

		this.entidadePrecoCobrancaRetiradaMaiorMenor = entidadePrecoCobrancaRetiradaMaiorMenor;
	}

	/**
	 * @return entidadeTipoApuracaoRetirMaiorMenor
	 */
	public EntidadeConteudo getEntidadeTipoApuracaoRetirMaiorMenor() {

		return entidadeTipoApuracaoRetirMaiorMenor;
	}

	/**
	 * @param entidadeTipoApuracaoRetirMaiorMenor
	 */
	public void setEntidadeTipoApuracaoRetirMaiorMenor(EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor) {

		this.entidadeTipoApuracaoRetirMaiorMenor = entidadeTipoApuracaoRetirMaiorMenor;
	}

	/**
	 * @return entidadeConsumoReferenciaRetMaiorMenor
	 */
	public EntidadeConteudo getEntidadeConsumoReferenciaRetMaiorMenor() {

		return entidadeConsumoReferenciaRetMaiorMenor;
	}

	/**
	 * @param entidadeConsumoReferenciaRetMaiorMenor
	 */
	public void setEntidadeConsumoReferenciaRetMaiorMenor(EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor) {

		this.entidadeConsumoReferenciaRetMaiorMenor = entidadeConsumoReferenciaRetMaiorMenor;
	}

	/**
	 * @return valorPercentualCobRetMaiorMenor
	 */
	public BigDecimal getValorPercentualCobRetMaiorMenor() {

		return valorPercentualCobRetMaiorMenor;
	}

	/**
	 * @param valorPercentualCobRetMaiorMenor
	 */
	public void setValorPercentualCobRetMaiorMenor(BigDecimal valorPercentualCobRetMaiorMenor) {

		this.valorPercentualCobRetMaiorMenor = valorPercentualCobRetMaiorMenor;
	}

	/**
	 * @return valorPercentualCobIntRetMaiorMenor
	 */
	public BigDecimal getValorPercentualCobIntRetMaiorMenor() {

		return valorPercentualCobIntRetMaiorMenor;
	}

	/**
	 * @param valorPercentualCobIntRetMaiorMenor
	 */
	public void setValorPercentualCobIntRetMaiorMenor(BigDecimal valorPercentualCobIntRetMaiorMenor) {

		this.valorPercentualCobIntRetMaiorMenor = valorPercentualCobIntRetMaiorMenor;
	}

	/**
	 * @return valorPercentualRetMaiorMenor
	 */
	public BigDecimal getValorPercentualRetMaiorMenor() {

		return valorPercentualRetMaiorMenor;
	}

	/**
	 * @param valorPercentualRetMaiorMenor
	 */
	public void setValorPercentualRetMaiorMenor(BigDecimal valorPercentualRetMaiorMenor) {

		this.valorPercentualRetMaiorMenor = valorPercentualRetMaiorMenor;
	}

	/**
	 * @return indicadorImposto
	 */
	public Boolean getIndicadorImposto() {

		return indicadorImposto;
	}

	/**
	 * @param indicadorImposto
	 */
	public void setIndicadorImposto(Boolean indicadorImposto) {

		this.indicadorImposto = indicadorImposto;
	}

	/**
	 * @return contrato
	 */
	public Contrato getContrato() {
		return contrato;
	}

	/**
	 * @param contrato
	 */
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof PenalidadesRetiradaMaiorMenorVO) {
			PenalidadesRetiradaMaiorMenorVO comparado = (PenalidadesRetiradaMaiorMenorVO) obj;
			try {
				if (this.getPenalidadeRetiradaMaiorMenor().getDescricao().equals(comparado.getPenalidadeRetiradaMaiorMenor().getDescricao())
						&& this.getEntidadePeriodicidadeRetiradaMaiorMenor().getDescricao()
								.equals(comparado.getEntidadePeriodicidadeRetiradaMaiorMenor().getDescricao())
						&& this.getEntidadeBaseApuracaoRetiradaMaiorMenor().getDescricao()
								.equals(comparado.getEntidadeBaseApuracaoRetiradaMaiorMenor().getDescricao())) {
					if (Util.isDataDentroIntervalo(this.getDataInicioVigencia(), comparado.getDataInicioVigencia(),
							comparado.getDataFimVigencia())
							|| Util.isDataDentroIntervalo(this.getDataFimVigencia(), comparado.getDataInicioVigencia(),
									comparado.getDataFimVigencia())) {
						return true;
					}
				}
			} catch (Exception e) {
				LOG.error("Objetos diferentes", e);
				return false;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if ((this.penalidadeRetiradaMaiorMenor != null && this.penalidadeRetiradaMaiorMenor.getChavePrimaria() > 0)
				&& (this.entidadePeriodicidadeRetiradaMaiorMenor != null
						&& this.entidadePeriodicidadeRetiradaMaiorMenor.getChavePrimaria() > 0)
				&& (this.entidadeBaseApuracaoRetiradaMaiorMenor != null
						&& this.entidadeBaseApuracaoRetiradaMaiorMenor.getChavePrimaria() > 0)) {
			return new HashCodeBuilder(7, 11).append(penalidadeRetiradaMaiorMenor.getChavePrimaria())
					.append(entidadePeriodicidadeRetiradaMaiorMenor.getChavePrimaria())
					.append(entidadeBaseApuracaoRetiradaMaiorMenor.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

}
