/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.io.Serializable;
import java.util.Date;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class ApuracaoQuantidadePenalidadePeriVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1894183794959890201L;

	private Date dataInicioApuracao;

	private Date dataFimApuracao;

	private Integer periodoApuracao;

	private String valorQDC;

	private String valorQDS;

	private String valorQDP;

	private String valorQNR;

	private String valorQPNR;

	private String valorQDCM;

	private String valorCalculado;

	public Date getDataInicioApuracao() {
		Date data = null;
		if (this.dataInicioApuracao != null) {
			data = (Date) dataInicioApuracao.clone();
		}
		return data;
	}

	public void setDataInicioApuracao(Date dataInicioApuracao) {

		this.dataInicioApuracao = dataInicioApuracao;
	}

	public Integer getPeriodoApuracao() {

		return periodoApuracao;
	}

	public void setPeriodoApuracao(Integer periodoApuracao) {

		this.periodoApuracao = periodoApuracao;
	}

	public Date getDataFimApuracao() {
		Date data = null;
		if (this.dataFimApuracao != null) {
			data = (Date) dataFimApuracao.clone();
		}
		return data;
	}

	public void setDataFimApuracao(Date dataFimApuracao) {

		this.dataFimApuracao = dataFimApuracao;
	}

	public String getValorQDC() {

		return valorQDC;
	}

	public void setValorQDC(String valorQDC) {

		this.valorQDC = valorQDC;
	}

	public String getValorQDS() {

		return valorQDS;
	}

	public void setValorQDS(String valorQDS) {

		this.valorQDS = valorQDS;
	}

	public String getValorQDP() {

		return valorQDP;
	}

	public void setValorQDP(String valorQDP) {

		this.valorQDP = valorQDP;
	}

	public String getValorQNR() {

		return valorQNR;
	}

	public void setValorQNR(String valorQNR) {

		this.valorQNR = valorQNR;
	}

	public String getValorQPNR() {

		return valorQPNR;
	}

	public void setValorQPNR(String valorQPNR) {

		this.valorQPNR = valorQPNR;
	}

	public String getValorQDCM() {

		return valorQDCM;
	}

	public void setValorQDCM(String valorQDCM) {

		this.valorQDCM = valorQDCM;
	}

	public String getValorCalculado() {

		return valorCalculado;
	}

	public void setValorCalculado(String valorCalculado) {

		this.valorCalculado = valorCalculado;
	}

	/**
	 * Método responsável por retornar a data de
	 * solicitacao no formato dd/MM/yyyy
	 * 
	 * @return
	 */
	public String getDataFormatada() {

		return Util.converterDataParaStringSemHora(this.getDataInicioApuracao(), Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getPeriodoApuracaoFormatado()
	 */
	public String getPeriodoApuracaoFormatado() {

		if(periodoApuracao != null) {

			String periodo = String.valueOf(periodoApuracao);

			if(periodo.length() == 6) {
				return periodo.substring(4, 6) + "/" + periodo.substring(0, 4);
			} else {
				return periodo;
			}

		} else {
			return "";
		}

	}
}
