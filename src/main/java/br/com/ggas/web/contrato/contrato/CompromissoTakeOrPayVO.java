/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.util.Util;

/**
 * CompromissoTakeOrPayVO
 *
 */
public class CompromissoTakeOrPayVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -2907827988020224758L;

	private Contrato contrato;

	private EntidadeConteudo periodicidadeTakeOrPay;

	private EntidadeConteudo consumoReferenciaTakeOrPay;

	private BigDecimal margemVariacaoTakeOrPay;

	private EntidadeConteudo referenciaQFParadaProgramada;

	private Date dataInicioVigencia;

	private Date dataFimVigencia;

	private BigDecimal percentualNaoRecuperavel;

	private Boolean consideraParadaProgramada;

	private Boolean consideraFalhaFornecimento;

	private Boolean consideraCasoFortuito;

	private Boolean recuperavel;

	private EntidadeConteudo tipoApuracao;

	private EntidadeConteudo precoCobranca;

	private EntidadeConteudo apuracaoParadaProgramada;

	private EntidadeConteudo apuracaoCasoFortuito;

	private EntidadeConteudo apuracaoFalhaFornecimento;

	private EntidadeConteudo tipoAgrupamentoContrato;

	/**
	 * @return the periodicidadeTakeOrPay
	 */
	public EntidadeConteudo getPeriodicidadeTakeOrPay() {

		return periodicidadeTakeOrPay;
	}

	/**
	 * @param periodicidadeTakeOrPay
	 *            the periodicidadeTakeOrPay to
	 *            set
	 */
	public void setPeriodicidadeTakeOrPay(EntidadeConteudo periodicidadeTakeOrPay) {

		this.periodicidadeTakeOrPay = periodicidadeTakeOrPay;
	}

	/**
	 * @return the consumoReferenciaTakeOrPay
	 */
	public EntidadeConteudo getConsumoReferenciaTakeOrPay() {

		return consumoReferenciaTakeOrPay;
	}

	/**
	 * @param consumoReferenciaTakeOrPay
	 *            the consumoReferenciaTakeOrPay
	 *            to set
	 */
	public void setConsumoReferenciaTakeOrPay(EntidadeConteudo consumoReferenciaTakeOrPay) {

		this.consumoReferenciaTakeOrPay = consumoReferenciaTakeOrPay;
	}

	/**
	 * @return the margemVariacaoTakeOrPay
	 */
	public BigDecimal getMargemVariacaoTakeOrPay() {

		return margemVariacaoTakeOrPay;
	}

	/**
	 * @param margemVariacaoTakeOrPay
	 *            the margemVariacaoTakeOrPay to
	 *            set
	 */
	public void setMargemVariacaoTakeOrPay(BigDecimal margemVariacaoTakeOrPay) {

		this.margemVariacaoTakeOrPay = margemVariacaoTakeOrPay;
	}

	/**
	 * @return referenciaQFParadaProgramada
	 */
	public EntidadeConteudo getReferenciaQFParadaProgramada() {

		return referenciaQFParadaProgramada;
	}

	/**
	 * @param referenciaQFParadaProgramada
	 */
	public void setReferenciaQFParadaProgramada(EntidadeConteudo referenciaQFParadaProgramada) {

		this.referenciaQFParadaProgramada = referenciaQFParadaProgramada;
	}

	/**
	 * @return dataInicioVigencia
	 */
	public Date getDataInicioVigencia() {
		Date data = null;
		if(this.dataInicioVigencia != null) {
			data = (Date) dataInicioVigencia.clone();
		}
		return data;
	}

	/**
	 * @param dataInicioVigencia
	 */
	public void setDataInicioVigencia(Date dataInicioVigencia) {
		if (dataInicioVigencia != null) {
			this.dataInicioVigencia = (Date) dataInicioVigencia.clone();
		} else {
			this.dataInicioVigencia = null;
		}
	}

	/**
	 * @return dataFimVigencia
	 */
	public Date getDataFimVigencia() {
		Date data = null;
		if (this.dataFimVigencia != null) {
			data = (Date) dataFimVigencia.clone();
		}
		return data;
	}

	/**
	 * @param dataFimVigencia
	 */
	public void setDataFimVigencia(Date dataFimVigencia) {
		if(dataFimVigencia != null) {
			this.dataFimVigencia = (Date) dataFimVigencia.clone();
		} else {
			this.dataFimVigencia = null;
		}
	}

	/**
	 * @return percentualNaoRecuperavel
	 */
	public BigDecimal getPercentualNaoRecuperavel() {

		return percentualNaoRecuperavel;
	}

	/**
	 * @param percentualNaoRecuperavel
	 */
	public void setPercentualNaoRecuperavel(BigDecimal percentualNaoRecuperavel) {

		this.percentualNaoRecuperavel = percentualNaoRecuperavel;
	}

	/**
	 * @return consideraParadaProgramada
	 */
	public Boolean getConsideraParadaProgramada() {

		return consideraParadaProgramada;
	}

	/**
	 * @param consideraParadaProgramada
	 */
	public void setConsideraParadaProgramada(Boolean consideraParadaProgramada) {

		this.consideraParadaProgramada = consideraParadaProgramada;
	}

	/**
	 * @return consideraFalhaFornecimento
	 */
	public Boolean getConsideraFalhaFornecimento() {

		return consideraFalhaFornecimento;
	}

	/**
	 * @param consideraFalhaFornecimento
	 */
	public void setConsideraFalhaFornecimento(Boolean consideraFalhaFornecimento) {

		this.consideraFalhaFornecimento = consideraFalhaFornecimento;
	}

	/**
	 * @return consideraCasoFortuito
	 */
	public Boolean getConsideraCasoFortuito() {

		return consideraCasoFortuito;
	}

	/**
	 * @param consideraCasoFortuito
	 */
	public void setConsideraCasoFortuito(Boolean consideraCasoFortuito) {

		this.consideraCasoFortuito = consideraCasoFortuito;
	}

	/**
	 * @return recuperavel
	 */
	public Boolean getRecuperavel() {

		return recuperavel;
	}

	/**
	 * @param recuperavel
	 */
	public void setRecuperavel(Boolean recuperavel) {

		this.recuperavel = recuperavel;
	}

	/**
	 * @return tipoApuracao
	 */
	public EntidadeConteudo getTipoApuracao() {

		return tipoApuracao;
	}

	/**
	 * @param tipoApuracao
	 */
	public void setTipoApuracao(EntidadeConteudo tipoApuracao) {

		this.tipoApuracao = tipoApuracao;
	}

	/**
	 * @return precoCobranca
	 */
	public EntidadeConteudo getPrecoCobranca() {

		return precoCobranca;
	}

	/**
	 * @param precoCobranca
	 */
	public void setPrecoCobranca(EntidadeConteudo precoCobranca) {

		this.precoCobranca = precoCobranca;
	}

	/**
	 * @return apuracaoParadaProgramada
	 */
	public EntidadeConteudo getApuracaoParadaProgramada() {

		return apuracaoParadaProgramada;
	}

	/**
	 * @param apuracaoParadaProgramada
	 */
	public void setApuracaoParadaProgramada(EntidadeConteudo apuracaoParadaProgramada) {

		this.apuracaoParadaProgramada = apuracaoParadaProgramada;
	}

	/**
	 * @return apuracaoCasoFortuito
	 */
	public EntidadeConteudo getApuracaoCasoFortuito() {

		return apuracaoCasoFortuito;
	}

	/**
	 * @param apuracaoCasoFortuito
	 */
	public void setApuracaoCasoFortuito(EntidadeConteudo apuracaoCasoFortuito) {

		this.apuracaoCasoFortuito = apuracaoCasoFortuito;
	}

	/**
	 * @return apuracaoFalhaFornecimento
	 */
	public EntidadeConteudo getApuracaoFalhaFornecimento() {

		return apuracaoFalhaFornecimento;
	}

	/**
	 * @param apuracaoFalhaFornecimento
	 */
	public void setApuracaoFalhaFornecimento(EntidadeConteudo apuracaoFalhaFornecimento) {

		this.apuracaoFalhaFornecimento = apuracaoFalhaFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof CompromissoTakeOrPayVO)) {
			return false;
		}
		if((this.periodicidadeTakeOrPay != null && this.periodicidadeTakeOrPay.getChavePrimaria() > 0)
						&& (consumoReferenciaTakeOrPay != null && consumoReferenciaTakeOrPay.getChavePrimaria() > 0)) {
			CompromissoTakeOrPayVO that = (CompromissoTakeOrPayVO) obj;
			boolean igual =
					new EqualsBuilder().append(periodicidadeTakeOrPay.getChavePrimaria(), that.periodicidadeTakeOrPay.getChavePrimaria())
							.append(consumoReferenciaTakeOrPay.getChavePrimaria(), that.consumoReferenciaTakeOrPay.getChavePrimaria())
							.isEquals();

			if ((Util.isDataDentroIntervalo(this.getDataInicioVigencia(), that.getDataInicioVigencia(), that.getDataFimVigencia())
					|| Util.isDataDentroIntervalo(this.getDataFimVigencia(), that.getDataInicioVigencia(), that.getDataFimVigencia()))
					&& igual) {
				return true;
			}

			return false;
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if((this.periodicidadeTakeOrPay != null && this.periodicidadeTakeOrPay.getChavePrimaria() > 0)
						&& (consumoReferenciaTakeOrPay != null && consumoReferenciaTakeOrPay.getChavePrimaria() > 0)) {
			return new HashCodeBuilder(3, 5).append(periodicidadeTakeOrPay.getChavePrimaria())
							.append(consumoReferenciaTakeOrPay.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/**
	 * @return tipoAgrupamentoContrato
	 */
	public EntidadeConteudo getTipoAgrupamentoContrato() {

		return tipoAgrupamentoContrato;
	}

	/**
	 * @param tipoAgrupamentoContrato
	 */
	public void setTipoAgrupamentoContrato(EntidadeConteudo tipoAgrupamentoContrato) {

		this.tipoAgrupamentoContrato = tipoAgrupamentoContrato;
	}

	/**
	 * @return contrato
	 */
	public Contrato getContrato() {

		return contrato;
	}

	/**
	 * @param contrato
	 */
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

}
