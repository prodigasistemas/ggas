/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.io.Serializable;

import br.com.ggas.cadastro.imovel.PontoConsumo;

public class ParadaProgramadaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5033450027107604895L;

	private String ano;

	private String mes;

	private String dia;

	private String comentario;

	private String nomeSolicitante;

	private String dataParada;

	private String menorPressao;

	private String unidadeMenorPressao;

	private PontoConsumo pontoConsumo;

	private String dataAviso;

	private String volumeFornecimento;

	/**
	 * @return the comentario
	 */
	public String getComentario() {

		return comentario;
	}

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	/**
	 * @return the ano
	 */
	public String getAno() {

		return ano;
	}

	/**
	 * @param ano
	 *            the ano to set
	 */
	public void setAno(String ano) {

		this.ano = ano;
	}

	/**
	 * @return the mes
	 */
	public String getMes() {

		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(String mes) {

		this.mes = mes;
	}

	/**
	 * @return the dia
	 */
	public String getDia() {

		return dia;
	}

	/**
	 * @param dia
	 *            the dia to set
	 */
	public void setDia(String dia) {

		this.dia = dia;
	}

	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	/**
	 * @return the dataParada
	 */
	public String getDataParada() {

		return dataParada;
	}

	/**
	 * @param dataParada
	 *            the dataParada to set
	 */
	public void setDataParada(String dataParada) {

		this.dataParada = dataParada;
	}

	public String getMenorPressao() {

		return menorPressao;
	}

	public void setMenorPressao(String menorPressao) {

		this.menorPressao = menorPressao;
	}

	public String getUnidadeMenorPressao() {

		return unidadeMenorPressao;
	}

	public void setUnidadeMenorPressao(String unidadeMenorPressao) {

		this.unidadeMenorPressao = unidadeMenorPressao;
	}

	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public String getDataAviso() {

		return dataAviso;
	}

	public void setDataAviso(String dataAviso) {

		this.dataAviso = dataAviso;
	}

	public String getVolumeFornecimento() {

		return volumeFornecimento;
	}

	public void setVolumeFornecimento(String volumeFornecimento) {

		this.volumeFornecimento = volumeFornecimento;
	}

}
