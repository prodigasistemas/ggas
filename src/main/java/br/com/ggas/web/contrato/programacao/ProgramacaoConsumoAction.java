/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas as telas de Solicitação de Consumo
 *
 */
@Controller
public class ProgramacaoConsumoAction extends GenericAction {

	private static final int FIM_INDICE_MES = 5;
	private static final int INICIO_INDICE_MES = 3;
	private static final int FIM_INDICE_ANO = 10;
	private static final int INICIO_INDICE_ANO = 6;
	private static final String PROGRAMACAO_CONSUMO_VO = "programacaoConsumoVO";
	private static final String DATA_SOLICITACAO = "dataSolicitacao";
	private static final String ID_CITY_GATE = "idCityGate";
	private static final String NUMERO_CONTRATO = "numeroContrato";
	private static final String ID_CLIENTE = "idCliente";
	private static final String OPCAO = "opcao";
	private static final String DATA_ATUAL = "dataAtual";
	protected static final String FORWARD_SUCESSO = "sucesso";

	@Autowired
	private ControladorProgramacao controladorProgramacao;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorQuadra controladorQuadra;

	@Autowired
	private ControladorTronco controladorTronco;

	@Autowired
	private ControladorCliente controladorCliente;

	/**
	 * Exibir pesquisa programacao consumo.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaProgramacaoConsumo")
	public String exibirPesquisaProgramacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) {

		request.getSession().removeAttribute(PROGRAMACAO_CONSUMO_VO);
		if (programacaoConsumoVO.getDataAtual() == null || programacaoConsumoVO.getDataAtual().isEmpty()) {
			programacaoConsumoVO.setDataAtual(Util.converterDataParaString(Calendar.getInstance().getTime()));
		}
		if (programacaoConsumoVO.getOpcao() == null || programacaoConsumoVO.getOpcao().isEmpty()) {
			programacaoConsumoVO.setOpcao("todos");
		}

		model.addAttribute("listaCityGate", controladorTronco.listarCityGateExistente());

		return "exibirPesquisaProgramacaoConsumo";
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo do cliente agrupados por imóvel.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("pesquisarImoveisProgramacaoConsumo")
	public String pesquisarImoveisProgramacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) throws NegocioException {

		try {
			if ((programacaoConsumoVO.getIdCliente() != null) && (programacaoConsumoVO.getIdCliente() > 0)) {

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(ID_CLIENTE, programacaoConsumoVO.getIdCliente());

				Collection<Object[]> listaImoveisPonto = new ArrayList<Object[]>();

				Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);

				for (Imovel imovel : listaImoveis) {
					Collection<PontoConsumo> listaPontosConsumo = this.consultarPontosConsumo(imovel);
					Collection<PontoConsumoProgramacaoConsumoVO> listaPontosConsumoVO = this.montarListaPontosVO(listaPontosConsumo);

					listaImoveisPonto.add(new Object[] { imovel.getNome(), listaPontosConsumoVO });
				}

				model.addAttribute("listaImoveisPonto", listaImoveisPonto);
			}
		} catch (Exception e) {
			super.mensagemErro(model, e.getMessage());
			LOG.info(e.getMessage());
		}

		return exibirPesquisaProgramacaoConsumo(programacaoConsumoVO, result, request, model);
	}

	/**
	 * Método responsável por pesquisar os pontos do consumo do cliente agrupados por contrato.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("pesquisarContratosProgramacaoConsumo")
	public String pesquisarContratosProgramacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) throws Exception {

		model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
		try {
			Map<String, Object> filtro = obterFiltroForm(programacaoConsumoVO);
			Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
					controladorContrato.consultarContratoPontoConsumoProgramacaoConsumo(filtro);
			if (programacaoConsumoVO.getIdCliente() != null) {
				model.addAttribute("cliente", controladorCliente.obter(programacaoConsumoVO.getIdCliente()));
			}

			Map<Object, Collection<PontoConsumo>> mapContratoPontoConsumo = new HashMap<Object, Collection<PontoConsumo>>();

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
				Contrato contrato = contratoPontoConsumo.getContrato();
				Collection<PontoConsumo> colecaoPontosContrato = null;
				Integer numeroAditivo = contrato.getNumeroAditivo();
				String numeroAditivoString = String.valueOf(numeroAditivo);
				if (numeroAditivo < 10) {
					numeroAditivoString = "0".concat(numeroAditivoString);
				}
				Integer numeroContrato = (contrato.getAnoContrato() * 100000) + contrato.getNumero();
				String numeroString = String.valueOf(numeroContrato) + /* "-" + */numeroAditivoString;
				Long numero = Long.valueOf(numeroString);
				if (!mapContratoPontoConsumo.containsKey(numero)) {
					colecaoPontosContrato = new ArrayList<PontoConsumo>();
					mapContratoPontoConsumo.put(numero, colecaoPontosContrato);
				} else {
					colecaoPontosContrato = mapContratoPontoConsumo.get(numero);
				}
				colecaoPontosContrato.add(contratoPontoConsumo.getPontoConsumo());

			}

			if (mapContratoPontoConsumo.isEmpty()) {
				mapContratoPontoConsumo.put(Long.valueOf("0"), new ArrayList<PontoConsumo>());
			}

			request.setAttribute("listaContratoPonto", mapContratoPontoConsumo);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return exibirPesquisaProgramacaoConsumo(programacaoConsumoVO, result, request, model);
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @return Map {@link Map}
	 * @throws Exception {@link Exception}
	 */
	private Map<String, Object> obterFiltroForm(ProgramacaoConsumoVO programacaoConsumoVO) throws Exception {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if ((programacaoConsumoVO.getIdCliente() != null) && (programacaoConsumoVO.getIdCliente() > 0)) {
			filtro.put(ID_CLIENTE, programacaoConsumoVO.getIdCliente());
		}

		if (!StringUtils.isEmpty(programacaoConsumoVO.getNumeroContrato())) {
			filtro.put(NUMERO_CONTRATO, programacaoConsumoVO.getNumeroContrato());
		}

		if (!StringUtils.isEmpty(programacaoConsumoVO.getDataAtual())) {
			filtro.put(DATA_ATUAL, programacaoConsumoVO.getDataAtual());
		}

		if (!StringUtils.isEmpty(programacaoConsumoVO.getOpcao())) {
			filtro.put(OPCAO, programacaoConsumoVO.getOpcao());
		}

		if ((programacaoConsumoVO.getIdCityGate() != null) && (programacaoConsumoVO.getIdCityGate() > 0)) {
			filtro.put(ID_CITY_GATE, programacaoConsumoVO.getIdCityGate());
		}

		return filtro;
	}

	/**
	 * Método responsável por exibir a página de programação de consumo.
	 * 
	 * @throws Exception
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("exibirProgramacaoConsumo")
	public String exibirProgramacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result, HttpServletRequest request,
			Model model) throws Exception {

		DateTime dataHoje = new DateTime();
		ProgramacaoConsumoVO programacaoConsumoVOTmp = new ProgramacaoConsumoVO();

		salvandoPesquisa(request, programacaoConsumoVOTmp);

		super.salvarPesquisa(request, programacaoConsumoVOTmp, PROGRAMACAO_CONSUMO_VO);

		if (programacaoConsumoVO.getAno() == null && (programacaoConsumoVO.getAno() == null || programacaoConsumoVO.getAno() < 1)) {
			programacaoConsumoVO.setAno(dataHoje.getYear());
		}
		if (programacaoConsumoVO.getMes() == null && (programacaoConsumoVO.getMes() == null || programacaoConsumoVO.getMes() < 1)) {
			programacaoConsumoVO.setMes(dataHoje.getMonthOfYear());
		}

		// Guardar a informação do ano que foi exibido no form.
		// Usar essa informação no manter e não o valor do combo.
		programacaoConsumoVO.setAnoExibido(programacaoConsumoVO.getAno());
		programacaoConsumoVO.setMesExibido(programacaoConsumoVO.getMes());

		verificarMesAno(programacaoConsumoVO.getMesExibido(), programacaoConsumoVO.getAnoExibido(), model);

		request.setAttribute("listaAnos", listarAnos());
		request.setAttribute("listaMeses", Mes.MESES_ANO);

		ContratoPontoConsumo contratoPonto = null;
		if (programacaoConsumoVO.getIdPontoConsumo() != null && programacaoConsumoVO.getIdPontoConsumo() > 0) {
			contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(programacaoConsumoVO.getIdPontoConsumo());
		}

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidadeExibir = null;
		if (contratoPonto != null) {
			listaContratoPontoConsumoModalidadeExibir =
					controladorContrato.listarContratoPontoConsumoModalidadePorContratoPontoConsumo(contratoPonto.getChavePrimaria());
		}

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<ContratoPontoConsumoModalidade>();

		if (programacaoConsumoVO.getChaveModalidade() == null) {
			ContratoPontoConsumoModalidade consumoModalidade = Util.primeiroElemento(listaContratoPontoConsumoModalidadeExibir);
			programacaoConsumoVO.setChaveModalidade(consumoModalidade.getChavePrimaria());
		}

		model.addAttribute("chaveModalidadeExibir", programacaoConsumoVO.getChaveModalidade());
		ContratoPontoConsumoModalidade contratoPontoModalidade =
				controladorContrato.obterContratoPontoConsumoModalidade(programacaoConsumoVO.getChaveModalidade());
		listaContratoPontoConsumoModalidade.add(contratoPontoModalidade);

		Collection<SolicitacaoConsumoPontoConsumoVO> listaSolicitacaoConsumoVO;
		if (!isPostBack(request) || !model.containsAttribute("postBack")) {

			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo =
					controladorProgramacao.listarSolicitacaoConsumoPontoConsumo(contratoPonto, listaContratoPontoConsumoModalidade,
							programacaoConsumoVO.getAno(), programacaoConsumoVO.getMes());
			listaSolicitacaoConsumoVO = converterListaSolicitacaoConsumo(listaSolicitacaoConsumo);
		} else {
			// Caso venha de uma excecao, manter os dados que foram digitados no form.
			listaSolicitacaoConsumoVO = converterSolicitacaoConsumo(programacaoConsumoVO, request);
		}

		boolean indicadorRetiradaAutomatica = false;
		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {
			Boolean retiradaAutomatica = contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica();
			if ((retiradaAutomatica != null) && (retiradaAutomatica)) {
				indicadorRetiradaAutomatica = true;
				break;
			}

		}

		String mesAux = String.valueOf(programacaoConsumoVO.getMes());
		if (mesAux.length() == 1) {
			mesAux = "0" + mesAux;
		}

		BigDecimal saldoARecuperar = null;

		if (contratoPonto != null) {
			saldoARecuperar = controladorProgramacao.obterSaldoARecuperar(contratoPonto.getContrato(),
					new Long[] { programacaoConsumoVO.getIdPontoConsumo() }, Long.valueOf(1),
					Integer.valueOf(programacaoConsumoVO.getAno() + mesAux), new HashSet<ApuracaoQuantidadePenalidadePeriodicidade>());
			programacaoConsumoVO.setNumeroContrato(contratoPonto.getContrato().getNumeroFormatado());
		}

		model.addAttribute("indicadorRetiradaAutomatica", indicadorRetiradaAutomatica);
		if (listaContratoPontoConsumoModalidadeExibir != null) {
			request.setAttribute("listaContratoPontoConsumoModalidade", listaContratoPontoConsumoModalidadeExibir);
		}

		model.addAttribute("listaSolicitacaoConsumoVO", listaSolicitacaoConsumoVO);

		if (saldoARecuperar != null) {
			programacaoConsumoVO.setSaldoARecuperarAux(saldoARecuperar);
		} else {
			programacaoConsumoVO.setSaldoARecuperarAux(null);
		}

		if (programacaoConsumoVO.getIdPontoConsumo() != null) {
			programacaoConsumoVO.setDescricaoPontoConsumo(
					((PontoConsumo) controladorPontoConsumo.obter(programacaoConsumoVO.getIdPontoConsumo())).getDescricao());
			programacaoConsumoVO
					.setNomeImovel(((PontoConsumo) controladorPontoConsumo.obter(programacaoConsumoVO.getIdPontoConsumo(), "imovel"))
							.getImovel().getNome());
		}

		model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
		saveToken(request);

		return "exibirProgramacaoConsumo";
	}

	/**
	 * Método responsável por salvar filtro da pesquisa anterior
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param programacaoConsumoVOTmp {@link ProgramacaoConsumoVO}
	 */
	private void salvandoPesquisa(HttpServletRequest request, ProgramacaoConsumoVO programacaoConsumoVOTmp) {

		if (request.getParameter("idCliente") != null && !request.getParameter("idCliente").isEmpty()) {
			Long idCliente = Long.valueOf(request.getParameter("idCliente"));
			programacaoConsumoVOTmp.setIdCliente(idCliente);
		}
		if (request.getParameter("idCityGate") != null && !request.getParameter("idCityGate").isEmpty()) {
			Long idCityGate = Long.valueOf(request.getParameter("idCityGate"));
			programacaoConsumoVOTmp.setIdCityGate(idCityGate);
		}
		String indicadorPesquisa = request.getParameter("indicadorPesquisa");
		String dataAtual = request.getParameter("dataAtual");
		String opcao = request.getParameter("opcao");
		String numeroContrato = request.getParameter("numeroContrato");

		programacaoConsumoVOTmp.setDataAtual(dataAtual);
		programacaoConsumoVOTmp.setOpcao(opcao);
		programacaoConsumoVOTmp.setNumeroContrato(numeroContrato);
		programacaoConsumoVOTmp.setIndicadorPesquisa(indicadorPesquisa);
	}

	/**
	 * Verificar mes ano.
	 * 
	 * 
	 * @param mes {@link Integer}
	 * @param ano {@link Integer}
	 * @param model {@link Model}
	 */
	private void verificarMesAno(Integer mes, Integer ano, Model model) {

		Calendar calendar = Calendar.getInstance();
		Integer anoCorrente = calendar.get(Calendar.YEAR);
		Integer mesCorrente = calendar.get(Calendar.MONTH) + 1;

		if ((ano != null && mes != null) && (ano <= anoCorrente) && (mes < mesCorrente)) {

			model.addAttribute("anoMesAnterior", true);
		}
	}

	/**
	 * Método responsável por realizar a manutenção dos valores de consumo.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("manterProgramacaoConsumo")
	public String manterProgramacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result, HttpServletRequest request,
			Model model) throws Exception {

		try {
			getDadosAuditoria(request);

			// Obtém dados do form e persiste
			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao = new ArrayList<SolicitacaoConsumoPontoConsumo>();
			popularListaSolicitacaoConsumoPontoConsumo(programacaoConsumoVO, request, listaSolicitacao);
			model.addAttribute("listaSolicitacaoConsumoVO", listaSolicitacao);

			// Usar 'anoExibido' e não 'ano'.
			// Usar
			// 'anoExibido'
			// e
			// não
			// 'ano'.
			// Usar
			// 'mesExibido'
			// e
			// não
			// 'mes'.

			if (programacaoConsumoVO.getAnoExibido() == null || programacaoConsumoVO.getAnoExibido() < 1) {
				programacaoConsumoVO.setAnoExibido(null);
			}

			if (programacaoConsumoVO.getMesExibido() == null || programacaoConsumoVO.getMesExibido() < 1) {
				programacaoConsumoVO.setMesExibido(null);
			}

			controladorProgramacao.atualizarListaSolicitacaoConsumoPontoConsumo(listaSolicitacao, programacaoConsumoVO.getIdPontoConsumo(),
					programacaoConsumoVO.getAnoExibido(), programacaoConsumoVO.getMesExibido(), programacaoConsumoVO.getChaveModalidade());

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					SolicitacaoConsumoPontoConsumo.SOLICITACAO_CONSUMO_PONTO_CONSUMO);
			model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			model.addAttribute("postBack", true);
		}

		return exibirProgramacaoConsumo(programacaoConsumoVO, result, request, model);
	}

	/**
	 * Método responsável por recuperar os valores do form e popular uma lista com essas informações.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param request {@link HttpServletRequest}
	 * @param listaSolicitacao {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularListaSolicitacaoConsumoPontoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, HttpServletRequest request,
			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao) throws GGASException {

		if (programacaoConsumoVO.getSaldoARecuperarAux() == null) {
			programacaoConsumoVO.setSaldoARecuperarAux(new BigDecimal(0));
		}
		controladorProgramacao.validarValorTotalQPNR(programacaoConsumoVO.getValorQPNR(), programacaoConsumoVO.getSaldoARecuperarAux());

		PontoConsumo pontoConsumo = null;
		ContratoPontoConsumoModalidade contratoPontoModalidade = null;

		if (programacaoConsumoVO.getIdPontoConsumo() != null && programacaoConsumoVO.getIdPontoConsumo() > 0) {
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(programacaoConsumoVO.getIdPontoConsumo());
		}

		for (int i = 0; i < programacaoConsumoVO.getDataSolicitacao().length; i++) {

			SolicitacaoConsumoPontoConsumo solicitacao =
					(SolicitacaoConsumoPontoConsumo) controladorProgramacao.criarSolicitacaoConsumoPontoConsumo();
			if (pontoConsumo != null) {
				solicitacao.setPontoConsumo(pontoConsumo);
			} else {
				solicitacao.setPontoConsumo((PontoConsumo) controladorPontoConsumo.obter(programacaoConsumoVO.getIdPontoConsumoLista()[i]));
			}

			solicitacao.setContratoPontoConsumoModalidade(contratoPontoModalidade);
			if (programacaoConsumoVO.getIdModalidade() != null && programacaoConsumoVO.getIdModalidade().length > 0) {
				solicitacao.setContratoPontoConsumoModalidade(
						controladorContrato.obterContratoPontoConsumoModalidade(programacaoConsumoVO.getIdModalidade()[i]));
			}
			if (!StringUtils.isEmpty(programacaoConsumoVO.getDataSolicitacao()[i])) {
				solicitacao.setDataSolicitacao(Util.converterCampoStringParaData("Data", programacaoConsumoVO.getDataSolicitacao()[i],
						Constantes.FORMATO_DATA_BR));
			}

			if (!StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i])) {
				solicitacao.setValorQDC(Util.converterCampoStringParaValorBigDecimal("Valor QDC", programacaoConsumoVO.getValorQDC()[i],
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			}

			if ((!StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i]) && !"0".equals(programacaoConsumoVO.getValorQDC()[i]))
					&& !StringUtils.isEmpty(programacaoConsumoVO.getValorQDS()[i])) {

				solicitacao.setValorQDS(Util.converterCampoStringParaValorBigDecimal("Valor QDS", programacaoConsumoVO.getValorQDS()[i],
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));

			} else if (!StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i])) {

				solicitacao.setValorQDP(Util.converterCampoStringParaValorBigDecimal("Valor QDP", programacaoConsumoVO.getValorQDC()[i],
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));

			} else if (StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i]) || "0".equals(programacaoConsumoVO.getValorQDC()[i])) {

				solicitacao.setValorQDC(BigDecimal.ZERO);
				solicitacao.setValorQDS(BigDecimal.ZERO);
				solicitacao.setValorQDP(BigDecimal.ZERO);

			}
			if (!StringUtils.isEmpty(programacaoConsumoVO.getValorQPNR()[i])) {

				solicitacao.setValorQPNR(Util.converterCampoStringParaValorBigDecimal("Valor QPNR", programacaoConsumoVO.getValorQPNR()[i],
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));

			}
			if ((!StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i]) && !"0".equals(programacaoConsumoVO.getValorQDC()[i]))
					&& !StringUtils.isEmpty(programacaoConsumoVO.getValorQDP()[i])) {

				solicitacao.setValorQDP(Util.converterCampoStringParaValorBigDecimal("Valor QDP", programacaoConsumoVO.getValorQDP()[i],
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			}

			Boolean valorIndicadorAceite = obterValorCheckbox(request, "indicadorAceite", programacaoConsumoVO.getDataSolicitacao()[i]);
			if (valorIndicadorAceite != null) {
				solicitacao.setIndicadorAceite(valorIndicadorAceite);
			} else {
				if (solicitacao.getValorQDP() != null && solicitacao.getValorQDP().compareTo(BigDecimal.ZERO) > 0) {
					solicitacao.setIndicadorAceite(Boolean.FALSE);
					solicitacao.setDescricaoMotivoNaoAceite(programacaoConsumoVO.getDescricaoMotivoNaoAceite()[i]);
				}
			}

			String valorIndTipoProgConsumo =
					obterValorInput(request, "indicadorTipoProgramacaoConsumo", programacaoConsumoVO.getDataSolicitacao()[i]);
			if (valorIndTipoProgConsumo != null) {
				solicitacao.setIndicadorTipoProgramacaoConsumo(valorIndTipoProgConsumo);
			}

			solicitacao.setHabilitado(true);
			solicitacao.setDataOperacao(Calendar.getInstance().getTime());
			solicitacao.setUsuario(obterUsuario(request));
			solicitacao.setIndicadorSolicitante(true);

			listaSolicitacao.add(solicitacao);
		}
	}

	/**
	 * Obter valor checkbox.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param campo {@link String}
	 * @param dataSolicitacao {@link String}
	 * @return Boolean {@link Boolean}
	 */
	private Boolean obterValorCheckbox(HttpServletRequest request, String campo, String dataSolicitacao) {

		String campoCheck = null;
		String campoValor = null;
		Boolean retorno = null;

		campoCheck = campo + dataSolicitacao;
		campoValor = request.getParameter(campoCheck);

		if (campoValor != null) {
			retorno = Boolean.valueOf(campoValor);
		}

		return retorno;
	}

	/**
	 * Obter valor do input.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param campo {@link String}
	 * @param dataSolicitacao {@link String}
	 * @return String {@link String}
	 */
	private String obterValorInput(HttpServletRequest request, String campo, String dataSolicitacao) {

		String campoCheck = null;
		String campoValor = null;

		campoCheck = campo + dataSolicitacao;
		campoValor = request.getParameter(campoCheck);

		return campoValor;
	}

	/**
	 * Converter solicitacao consumo.
	 * 
	 * @param listaSolicitacaoConsumo {@link Collection}
	 * @return Collection {@link Collection}
	 * @throws Exception {@link Exception}
	 */
	private Collection<SolicitacaoConsumoPontoConsumoVO> converterListaSolicitacaoConsumo(
			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo) throws Exception {

		Collection<SolicitacaoConsumoPontoConsumoVO> listaSolicitacaoConsumoVO = new ArrayList<SolicitacaoConsumoPontoConsumoVO>();

		if (listaSolicitacaoConsumo != null && !listaSolicitacaoConsumo.isEmpty()) {
			for (SolicitacaoConsumoPontoConsumo solicitacao : listaSolicitacaoConsumo) {
				SolicitacaoConsumoPontoConsumoVO solicitacaoVO = converterSolicitacaoConsumo(solicitacao);
				listaSolicitacaoConsumoVO.add(solicitacaoVO);
			}
		}

		return listaSolicitacaoConsumoVO;
	}

	/**
	 * 
	 * @param solicitacao {@link SolicitacaoConsumoPontoConsumo}
	 * @return SolicitacaoConsumoPontoConsumoVO {@link SolicitacaoConsumoPontoConsumoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private SolicitacaoConsumoPontoConsumoVO converterSolicitacaoConsumo(SolicitacaoConsumoPontoConsumo solicitacao) {

		SolicitacaoConsumoPontoConsumoVO solicitacaoVO = new SolicitacaoConsumoPontoConsumoVO();

		solicitacaoVO.setIdPontoConsumo(solicitacao.getPontoConsumo().getChavePrimaria());
		solicitacaoVO.setDataSolicitacao(solicitacao.getDataSolicitacao());

		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = solicitacao.getContratoPontoConsumoModalidade();
		if (contratoPontoConsumoModalidade != null) {
			solicitacaoVO.setIdModalidade(contratoPontoConsumoModalidade.getChavePrimaria());
			solicitacaoVO.setDescricaoModalidade(contratoPontoConsumoModalidade.getDescricaoContratoModalidade());
		}

		if (solicitacao.getValorQDC() != null) {
			solicitacaoVO.setValorQDC(Util.converterCampoValorParaString(solicitacao.getValorQDC(), Constantes.FORMATO_VALOR_NUMERO_INTEIRO,
					Constantes.LOCALE_PADRAO));
		}
		if (solicitacao.getValorQDS() != null) {
			solicitacaoVO.setValorQDS(Util.converterCampoValorParaString(solicitacao.getValorQDS(), Constantes.FORMATO_VALOR_NUMERO_INTEIRO,
					Constantes.LOCALE_PADRAO));
		} else {
			solicitacaoVO.setValorQDS(
					Util.converterCampoValorParaString(BigDecimal.ZERO, Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		if (solicitacao.getValorQPNR() != null) {
			solicitacaoVO.setValorQPNR(Util.converterCampoValorParaString(solicitacao.getValorQPNR(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		if (solicitacao.getValorQDP() != null) {
			solicitacaoVO.setValorQDP(Util.converterCampoValorParaString(solicitacao.getValorQDP(), Constantes.FORMATO_VALOR_NUMERO_INTEIRO,
					Constantes.LOCALE_PADRAO));
		}

		if (solicitacao.getIndicadorAceite() != null) {
			if ((solicitacao.getValorQDS() == null) && (solicitacao.getValorQPNR() == null)) {
				solicitacaoVO.setIndicadorAceite(Boolean.FALSE);
			} else {
				solicitacaoVO.setIndicadorAceite(solicitacao.getIndicadorAceite());
			}
		}
		if (solicitacao.getDescricaoMotivoNaoAceite() != null) {
			solicitacaoVO.setDescricaoMotivoNaoAceite(solicitacao.getDescricaoMotivoNaoAceite());
		}

		solicitacaoVO.setIndicadorTipoProgramacaoConsumo(solicitacao.getIndicadorTipoProgramacaoConsumo());

		if (solicitacao.getPontoConsumo() != null) {
			ContratoPontoConsumo contratoPonto = controladorContrato.obterContratoAtivoPontoConsumo(solicitacao.getPontoConsumo());
			solicitacaoVO.setNomeCliente(contratoPonto.getContrato().getClienteAssinatura().getNome());
			solicitacaoVO.setDescricaoPontoConsumo(solicitacao.getPontoConsumo().getDescricao());
		}
		return solicitacaoVO;
	}

	/**
	 * Converter solicitacao consumo.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param request {@link HttpServletRequest}
	 * @return Collection {@link Controller}
	 * @throws GGASException {@link GGASException}
	 */
	private Collection<SolicitacaoConsumoPontoConsumoVO> converterSolicitacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO,
			HttpServletRequest request) throws GGASException {

		Collection<SolicitacaoConsumoPontoConsumoVO> listaSolicitacaoConsumoVO = new ArrayList<SolicitacaoConsumoPontoConsumoVO>();

		if (programacaoConsumoVO.getDataSolicitacao() != null && programacaoConsumoVO.getDataSolicitacao().length > 0) {

			for (int i = 0; i < programacaoConsumoVO.getDataSolicitacao().length; i++) {
				SolicitacaoConsumoPontoConsumoVO solicitacaoVO = new SolicitacaoConsumoPontoConsumoVO();
				if (!StringUtils.isEmpty(programacaoConsumoVO.getDataSolicitacao()[i])) {
					solicitacaoVO.setDataSolicitacao(Util.converterCampoStringParaData("Data", programacaoConsumoVO.getDataSolicitacao()[i],
							Constantes.FORMATO_DATA_BR));
				}
				if (programacaoConsumoVO.getValorQDC() != null && !StringUtils.isEmpty(programacaoConsumoVO.getValorQDC()[i])) {
					solicitacaoVO.setValorQDC(programacaoConsumoVO.getValorQDC()[i]);
				}
				if (programacaoConsumoVO.getValorQDS() != null && !StringUtils.isEmpty(programacaoConsumoVO.getValorQDS()[i])) {
					solicitacaoVO.setValorQDS(programacaoConsumoVO.getValorQDS()[i]);
				} else {
					solicitacaoVO.setValorQDS("0");
				}
				if (programacaoConsumoVO.getValorQPNR() != null && !StringUtils.isEmpty(programacaoConsumoVO.getValorQPNR()[i])) {
					solicitacaoVO.setValorQPNR(programacaoConsumoVO.getValorQPNR()[i]);
				}
				if (programacaoConsumoVO.getValorQDP() != null && !StringUtils.isEmpty(programacaoConsumoVO.getValorQDP()[i])) {
					solicitacaoVO.setValorQDP(programacaoConsumoVO.getValorQDP()[i]);
				}

				Boolean valorIndicadorAceite = obterValorCheckbox(request, "indicadorAceite", programacaoConsumoVO.getDataSolicitacao()[i]);
				if (valorIndicadorAceite == null) {
					if (((programacaoConsumoVO.getValorQDS()[i] == null) || (StringUtils.isEmpty(programacaoConsumoVO.getValorQDS()[i])))
							&& ((programacaoConsumoVO.getValorQPNR()[i] == null)
									|| (StringUtils.isEmpty(programacaoConsumoVO.getValorQPNR()[i])))) {
						solicitacaoVO.setIndicadorAceite(Boolean.FALSE);
					} else {
						String descricaoMotivoNaoAceite =
								request.getParameter("descricaoMotivo" + programacaoConsumoVO.getDataSolicitacao()[i]);
						solicitacaoVO.setDescricaoMotivoNaoAceite(descricaoMotivoNaoAceite);
					}
				} else {
					solicitacaoVO.setIndicadorAceite(valorIndicadorAceite);
				}
				listaSolicitacaoConsumoVO.add(solicitacaoVO);
			}
		}

		return listaSolicitacaoConsumoVO;
	}

	/**
	 * Consultar pontos consumo.
	 * 
	 * @param imovel {@link Imovel}
	 * @return {@link Collection}
	 * @throws Exception {@link Exception}
	 */
	private Collection<PontoConsumo> consultarPontosConsumo(Imovel imovel) throws Exception {

		Collection<PontoConsumo> pontosDeConsumo = new ArrayList<PontoConsumo>();

		Collection<PontoConsumo> pontosConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(imovel.getChavePrimaria());
		if ((pontosConsumo != null) && (!pontosConsumo.isEmpty())) {
			pontosDeConsumo.addAll(pontosConsumo);
		}
		if (imovel.getCondominio()) {
			Collection<Imovel> listaImoveisCondominio = controladorImovel.obterImoveisFilhosDoCondominio(imovel);

			if (listaImoveisCondominio != null && !listaImoveisCondominio.isEmpty()) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("chavesPrimariasImoveis", Util.collectionParaArrayChavesPrimarias(listaImoveisCondominio));
				pontosConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);
				if ((pontosConsumo != null) && (!pontosConsumo.isEmpty())) {
					pontosDeConsumo.addAll(pontosConsumo);
				}
			}
		}

		return pontosDeConsumo;
	}

	/**
	 * Montar lista pontos vo.
	 * 
	 * @param listaPontosConsumo {@link Collection}
	 * @return {@link Collection}
	 * @throws Exception {@link Exception}
	 */
	private Collection<PontoConsumoProgramacaoConsumoVO> montarListaPontosVO(Collection<PontoConsumo> listaPontosConsumo) throws Exception {

		Collection<PontoConsumoProgramacaoConsumoVO> pontosConsumoVO = new ArrayList<PontoConsumoProgramacaoConsumoVO>();

		PontoConsumoProgramacaoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			pontoVO = new PontoConsumoProgramacaoConsumoVO();

			PontoConsumo pontoConsumoCompleto =
					(PontoConsumo) controladorPontoConsumo.obter(pontoConsumo.getChavePrimaria(), "segmento", "ramoAtividade");

			pontoVO.setIdPontoConsumo(pontoConsumoCompleto.getChavePrimaria());
			pontoVO.setPontoConsumo(pontoConsumoCompleto.getDescricao());

			if (pontoConsumoCompleto.getSegmento() != null) {
				pontoVO.setSegmento(pontoConsumoCompleto.getSegmento().getDescricao());
			}

			if (pontoConsumoCompleto.getRamoAtividade() != null) {
				pontoVO.setRamoAtividade(pontoConsumoCompleto.getRamoAtividade().getDescricao());
			}
			pontosConsumoVO.add(pontoVO);
		}
		return pontosConsumoVO;
	}

	/**
	 * Listar anos.
	 * 
	 * @return {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	private Collection<String> listarAnos() throws GGASException {

		Collection<String> listaAnos = new ArrayList<String>();

		Integer anoAtual = new DateTime().getYear();
		Integer proximoAno = anoAtual + 1;

		String valorQuantidadeAnos =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		Integer quantidadeAnos = Integer.valueOf(valorQuantidadeAnos);
		for (int i = 0; i < quantidadeAnos; i++) {
			Integer ano = proximoAno - i;
			listaAnos.add(ano.toString());
		}

		return listaAnos;
	}

	/**
	 * Exibir comentario solicitacao consumo.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("exibirComentarioSolicitacaoConsumo")
	public String exibirComentarioSolicitacaoConsumo(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) throws Exception {

		Date data = Util.converterCampoStringParaData(DATA_SOLICITACAO, programacaoConsumoVO.getDataSolicitacaoComentario(),
				Constantes.FORMATO_DATA_BR);

		Collection<SolicitacaoConsumoPontoConsumo> listaComentarioSolicitacaoConsumo =
				controladorProgramacao.listarComentarioSolicitacaoConsumo(programacaoConsumoVO.getIdPontoConsumo(), data,
						programacaoConsumoVO.getChaveModalidade());

		model.addAttribute("listaComentarioSolicitacaoConsumo", listaComentarioSolicitacaoConsumo);
		model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);

		return "exibirComentarioSolicitacaoConsumo";
	}

	/**
	 * Pesquisar programacao consumo diaria intra diaria.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("pesquisarProgramacaoConsumoDiariaIntraDiaria")
	public String pesquisarProgramacaoConsumoDiariaIntraDiaria(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) throws Exception {

		try {
			Map<String, Object> filtro = obterFiltroForm(programacaoConsumoVO);

			Collection<CityGateSolicitacaoConsumoPontoConsumoVO> listaCityGateSolicitacaoConsumoVO =
					new ArrayList<CityGateSolicitacaoConsumoPontoConsumoVO>();

			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo =
					controladorProgramacao.listarSolicitacaoConsumoDiariasIntradiarias(filtro);

			for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacaoConsumo) {

				String descricaoListaCityGate = montarDescricaoCityGateSolicitacaoConsumoPontoConsumoVO(
						programacaoConsumoVO.getIdCityGate(), solicitacaoConsumoPontoConsumo);

				CityGateSolicitacaoConsumoPontoConsumoVO cityGateSolicitacaoConsumoPontoConsumoVO =
						CityGateSolicitacaoConsumoPontoConsumoVO.buscarObjeto(listaCityGateSolicitacaoConsumoVO, descricaoListaCityGate);

				if (cityGateSolicitacaoConsumoPontoConsumoVO == null) {
					cityGateSolicitacaoConsumoPontoConsumoVO = new CityGateSolicitacaoConsumoPontoConsumoVO();
					cityGateSolicitacaoConsumoPontoConsumoVO.setListaDescricaoCityGate(descricaoListaCityGate);
					listaCityGateSolicitacaoConsumoVO.add(cityGateSolicitacaoConsumoPontoConsumoVO);
				}

				BigDecimal valorQDS = solicitacaoConsumoPontoConsumo.getValorQDS();
				if (NumeroUtil.naoNulo(valorQDS)) {
					cityGateSolicitacaoConsumoPontoConsumoVO.adicionarTotalQds(valorQDS);
				}

				cityGateSolicitacaoConsumoPontoConsumoVO.getListaSolicitacaoConsumoVO()
						.add(converterSolicitacaoConsumo(solicitacaoConsumoPontoConsumo));

			}

			Collections.sort((List<CityGateSolicitacaoConsumoPontoConsumoVO>) listaCityGateSolicitacaoConsumoVO);
			model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
			request.setAttribute("listaCityGateSolicitacaoConsumoVO", listaCityGateSolicitacaoConsumoVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaProgramacaoConsumo(programacaoConsumoVO, result, request, model);
	}

	private String montarDescricaoCityGateSolicitacaoConsumoPontoConsumoVO(Long idCityGate,
			SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo) throws GGASException {

		long idQuadraFace = solicitacaoConsumoPontoConsumo.getPontoConsumo().getImovel().getQuadraFace().getChavePrimaria();

		Collection<CityGate> listaCityGate = controladorQuadra.listarCityGatePorQuadraFace(idQuadraFace);

		if (CollectionUtils.isNotEmpty(listaCityGate)) {

			return montarDescricaoListaCityGate(listaCityGate);
		} else {

			long chavePrimaria = solicitacaoConsumoPontoConsumo.getPontoConsumo().getChavePrimaria();

			Collection<PontoConsumoCityGate> listaPontoConsumoCityGate =
					controladorPontoConsumo.obterPontoConsumoCityGateOrdenadoPorCityGate(chavePrimaria, idCityGate);

			if (CollectionUtils.isNotEmpty(listaPontoConsumoCityGate)) {

				return montarDescricaoListaPontoConsumoCityGate(listaPontoConsumoCityGate);
			} else {

				return Constantes.ROTULO_SEM_CITY_GATE;
			}

		}

	}

	private String montarDescricaoListaCityGate(Collection<CityGate> listaCityGate) {

		String descricaoCityGate;
		StringBuilder listaDescricaoCityGate = new StringBuilder();
		if (CollectionUtils.isNotEmpty(listaCityGate)) {
			for (CityGate cityGate : listaCityGate) {
				concatenarDescricaoCityGate(listaDescricaoCityGate, cityGate);
			}
		}
		descricaoCityGate = listaDescricaoCityGate.toString();
		return descricaoCityGate;
	}

	private String montarDescricaoListaPontoConsumoCityGate(Collection<PontoConsumoCityGate> listaPontoConsumoCityGate) {

		String descricaoCityGate;
		StringBuilder listaDescricaoCityGate = new StringBuilder();
		if (CollectionUtils.isNotEmpty(listaPontoConsumoCityGate)) {
			for (PontoConsumoCityGate pontoConsumoCityGate : listaPontoConsumoCityGate) {
				concatenarDescricaoCityGate(listaDescricaoCityGate, pontoConsumoCityGate.getCityGate());
			}
		}
		descricaoCityGate = listaDescricaoCityGate.toString();
		return descricaoCityGate;
	}

	private void concatenarDescricaoCityGate(StringBuilder listaDescricaoCityGate, CityGate cityGate) {

		String descricaoCityGate = cityGate.getDescricao();
		if (listaDescricaoCityGate.length() == 0) {
			listaDescricaoCityGate.append(descricaoCityGate);
		} else {
			listaDescricaoCityGate.append(Constantes.SEPARADOR_CITY_GATE);
			listaDescricaoCityGate.append(descricaoCityGate);
		}
	}

	/**
	 * Manter programacao consumo diaria intradiaria.
	 * 
	 * @param programacaoConsumoVO {@link ProgramacaoConsumoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("manterProgramacaoConsumoDiariaIntradiaria")
	public String manterProgramacaoConsumoDiariaIntradiaria(ProgramacaoConsumoVO programacaoConsumoVO, BindingResult result,
			HttpServletRequest request, Model model) throws Exception {

		try {
			getDadosAuditoria(request);

			Long chaveModalidade = null;
			// Obtém dados do form e persiste
			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao = new ArrayList<SolicitacaoConsumoPontoConsumo>();
			popularListaSolicitacaoConsumoPontoConsumo(programacaoConsumoVO, request, listaSolicitacao);
			Long idPontoConsumo = null;
			Integer ano = Integer.valueOf(programacaoConsumoVO.getDataAtual().substring(INICIO_INDICE_ANO, FIM_INDICE_ANO));

			Integer mes = Integer.valueOf(programacaoConsumoVO.getDataAtual().substring(INICIO_INDICE_MES, FIM_INDICE_MES));

			if (ano == null || ano < 1) {
				ano = null;
			}

			if (mes == null || mes < 1) {
				mes = null;
			}
			for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacao) {

				List<SolicitacaoConsumoPontoConsumo> listaManipulacao = new ArrayList<SolicitacaoConsumoPontoConsumo>();
				listaManipulacao.add(solicitacaoConsumoPontoConsumo);
				idPontoConsumo = solicitacaoConsumoPontoConsumo.getPontoConsumo().getChavePrimaria();
				chaveModalidade = solicitacaoConsumoPontoConsumo.getContratoPontoConsumoModalidade().getChavePrimaria();

				controladorProgramacao.atualizarListaSolicitacaoConsumoPontoConsumo(listaManipulacao, idPontoConsumo, ano, mes,
						chaveModalidade);
			}
			model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					SolicitacaoConsumoPontoConsumo.SOLICITACAO_CONSUMO_PONTO_CONSUMO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirProgramacaoConsumo(programacaoConsumoVO, result, request, model);
	}

	/**
	 * Método responsável por recuperar dados de pesquisa
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("cancelarManterSolicitacaoConsumo")
	public String cancelarManterSolicitacaoConsumo(HttpServletRequest request, Model model) throws Exception {

		ProgramacaoConsumoVO programacaoConsumoVO = new ProgramacaoConsumoVO();
		if (super.retornaPesquisa(request, PROGRAMACAO_CONSUMO_VO) != null) {
			programacaoConsumoVO = (ProgramacaoConsumoVO) super.retornaPesquisa(request, PROGRAMACAO_CONSUMO_VO);
			request.getSession().removeAttribute(PROGRAMACAO_CONSUMO_VO);
			model.addAttribute(PROGRAMACAO_CONSUMO_VO, programacaoConsumoVO);
		}

		return pesquisarContratosProgramacaoConsumo(programacaoConsumoVO, null, request, model);
	}
}
