/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.geral.EntidadeConteudo;

/**
 * @author homologueggas
 */
public class ModalidadeConsumoFaturamentoVO implements Serializable {

	private static final long serialVersionUID = -4687898925335358970L;

	private ContratoModalidade modalidade;

	private Date dataVigenciaQDC;

	private Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC = 
					new HashSet<ContratoPontoConsumoModalidadeQDC>();

	private Date prazoRevizaoQDC;

	private Boolean qdsMaiorQDC;

	private BigDecimal variacaoSuperiorQDC;

	private Integer diasAntecSolicConsumo;

	private Integer numMesesSolicConsumo;

	private Boolean confirmacaoAutomaticaQDS;

	private Boolean indicadorProgramacaoConsumo;

	private Collection<CompromissoTakeOrPayVO> listaCompromissoTakeOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();

	private Collection<CompromissoTakeOrPayVO> listaCompromissoShipOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();

	private Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetiradaMaiorMenorVO = 
					new ArrayList<PenalidadesRetiradaMaiorMenorVO>();

	private Boolean recuperacaoAutoTakeOrPay;

	private BigDecimal percDuranteRetiradaQPNR;

	private BigDecimal percMinDuranteRetiradaQPNR;

	private BigDecimal percFimRetiradaQPNR;

	private Date dataInicioRetiradaQPNR;

	private Date dataFimRetiradaQPNR;

	private EntidadeConteudo consumoReferenciaShipOrPay;

	private BigDecimal margemVariacaoShipOrPay;

	private Integer tempoValidadeRetiradaQPNR;

	private Integer horaLimiteProgramacaoDiaria;

	private Integer horaLimiteProgIntradiaria;

	private Integer horaLimiteAceitacaoDiaria;

	private Integer horaLimiteAceitIntradiaria;

	private BigDecimal percentualQNR;

	/**
	 * @return the recuperacaoAutoTakeOrPay
	 */
	public Boolean getRecuperacaoAutoTakeOrPay() {

		return recuperacaoAutoTakeOrPay;
	}

	/**
	 * @param recuperacaoAutoTakeOrPay
	 *            the recuperacaoAutoTakeOrPay to
	 *            set
	 */
	public void setRecuperacaoAutoTakeOrPay(Boolean recuperacaoAutoTakeOrPay) {

		this.recuperacaoAutoTakeOrPay = recuperacaoAutoTakeOrPay;
	}

	/**
	 * @return the modalidade
	 */
	public ContratoModalidade getModalidade() {

		return modalidade;
	}

	/**
	 * @param modalidade
	 *            the modalidade to set
	 */
	public void setModalidade(ContratoModalidade modalidade) {

		this.modalidade = modalidade;
	}

	/**
	 * @return the dataVigenciaQDC
	 */
	public Date getDataVigenciaQDC() {
		Date data = null;
		if (this.dataVigenciaQDC != null) {
			data = (Date) dataVigenciaQDC.clone();
		}
		return data;
	}

	/**
	 * @param dataVigenciaQDC
	 *            the dataVigenciaQDC to set
	 */
	public void setDataVigenciaQDC(Date dataVigenciaQDC) {
		if (dataVigenciaQDC != null) {
			this.dataVigenciaQDC = (Date) dataVigenciaQDC.clone();
		} else {
			this.dataVigenciaQDC = null;
		}
	}

	/**
	 * @return the
	 *         listaContratoPontoConsumoModalidadeQDC
	 */
	public Collection<ContratoPontoConsumoModalidadeQDC> getListaContratoPontoConsumoModalidadeQDC() {

		return listaContratoPontoConsumoModalidadeQDC;
	}

	/**
	 * @param listaContratoPontoConsumoModalidadeQDC
	 *            the
	 *            listaContratoPontoConsumoModalidadeQDC
	 *            to set
	 */
	public void setListaContratoPontoConsumoModalidadeQDC(
					Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC) {

		this.listaContratoPontoConsumoModalidadeQDC = listaContratoPontoConsumoModalidadeQDC;
	}

	/**
	 * @return the prazoRevizaoQDC
	 */
	public Date getPrazoRevizaoQDC() {
		Date data = null;
		if(this.prazoRevizaoQDC != null) {
			data = (Date) prazoRevizaoQDC.clone();
		}
		return data;
	}

	/**
	 * @param prazoRevizaoQDC
	 *            the prazoRevizaoQDC to set
	 */
	public void setPrazoRevizaoQDC(Date prazoRevizaoQDC) {
		if (prazoRevizaoQDC != null) {
			this.prazoRevizaoQDC = (Date) prazoRevizaoQDC.clone();
		} else {
			this.prazoRevizaoQDC = null;
		}
	}

	/**
	 * @return the qdsMaiorQDC
	 */
	public Boolean getQdsMaiorQDC() {

		return qdsMaiorQDC;
	}

	/**
	 * @param qdsMaiorQDC
	 *            the qdsMaiorQDC to set
	 */
	public void setQdsMaiorQDC(Boolean qdsMaiorQDC) {

		this.qdsMaiorQDC = qdsMaiorQDC;
	}

	/**
	 * @return the diasAntecSolicConsumo
	 */
	public Integer getDiasAntecSolicConsumo() {

		return diasAntecSolicConsumo;
	}

	/**
	 * @param diasAntecSolicConsumo
	 *            the diasAntecSolicConsumo to set
	 */
	public void setDiasAntecSolicConsumo(Integer diasAntecSolicConsumo) {

		this.diasAntecSolicConsumo = diasAntecSolicConsumo;
	}

	/**
	 * @return the numMesesSolicConsumo
	 */
	public Integer getNumMesesSolicConsumo() {

		return numMesesSolicConsumo;
	}

	/**
	 * @param numMesesSolicConsumo
	 *            the numMesesSolicConsumo to set
	 */
	public void setNumMesesSolicConsumo(Integer numMesesSolicConsumo) {

		this.numMesesSolicConsumo = numMesesSolicConsumo;
	}

	/**
	 * @return the confirmacaoAutomaticaQDS
	 */
	public Boolean getConfirmacaoAutomaticaQDS() {

		return confirmacaoAutomaticaQDS;
	}

	/**
	 * @param confirmacaoAutomaticaQDS
	 *            the confirmacaoAutomaticaQDS to
	 *            set
	 */
	public void setConfirmacaoAutomaticaQDS(Boolean confirmacaoAutomaticaQDS) {

		this.confirmacaoAutomaticaQDS = confirmacaoAutomaticaQDS;
	}

	/**
	 * @return the indicadorProgramacaoConsumo
	 */
	public Boolean getIndicadorProgramacaoConsumo() {

		return indicadorProgramacaoConsumo;
	}

	/**
	 * @param indicadorProgramacaoConsumo
	 *            the indicadorProgramacaoConsumo to set
	 */
	public void setIndicadorProgramacaoConsumo(Boolean indicadorProgramacaoConsumo) {

		this.indicadorProgramacaoConsumo = indicadorProgramacaoConsumo;
	}

	/**
	 * @return the listaCompromissoTakeOrPayVO
	 */
	public Collection<CompromissoTakeOrPayVO> getListaCompromissoTakeOrPayVO() {

		return listaCompromissoTakeOrPayVO;
	}

	/**
	 * @param listaCompromissoTakeOrPayVO
	 *            the listaCompromissoTakeOrPayVO
	 *            to set
	 */
	public void setListaCompromissoTakeOrPayVO(Collection<CompromissoTakeOrPayVO> listaCompromissoTakeOrPayVO) {

		this.listaCompromissoTakeOrPayVO = listaCompromissoTakeOrPayVO;
	}

	/**
	 * @return the percDuranteRetiradaQPNR
	 */
	public BigDecimal getPercDuranteRetiradaQPNR() {

		return percDuranteRetiradaQPNR;
	}

	/**
	 * @param percDuranteRetiradaQPNR
	 *            the percDuranteRetiradaQPNR to
	 *            set
	 */
	public void setPercDuranteRetiradaQPNR(BigDecimal percDuranteRetiradaQPNR) {

		this.percDuranteRetiradaQPNR = percDuranteRetiradaQPNR;
	}

	/**
	 * @return the percFimRetiradaQPNR
	 */
	public BigDecimal getPercFimRetiradaQPNR() {

		return percFimRetiradaQPNR;
	}

	/**
	 * @param percFimRetiradaQPNR
	 *            the percFimRetiradaQPNR to set
	 */
	public void setPercFimRetiradaQPNR(BigDecimal percFimRetiradaQPNR) {

		this.percFimRetiradaQPNR = percFimRetiradaQPNR;
	}

	/**
	 * @return the dataInicioRetiradaQPNR
	 */
	public Date getDataInicioRetiradaQPNR() {
		Date data = null;
		if (this.dataInicioRetiradaQPNR != null) {
			data = (Date) dataInicioRetiradaQPNR.clone();
		}
		return data;
	}

	/**
	 * @param dataInicioRetiradaQPNR
	 *            the dataInicioRetiradaQPNR to set
	 */
	public void setDataInicioRetiradaQPNR(Date dataInicioRetiradaQPNR) {
		if (dataInicioRetiradaQPNR != null) {
			this.dataInicioRetiradaQPNR = (Date) dataInicioRetiradaQPNR.clone();
		} else {
			this.dataInicioRetiradaQPNR = null;
		}
	}

	/**
	 * @return the dataFimRetiradaQPNR
	 */
	public Date getDataFimRetiradaQPNR() {
		Date data = null;
		if (this.dataFimRetiradaQPNR != null) {
			data = (Date) dataFimRetiradaQPNR.clone();
		}
		return data;
	}

	/**
	 * @param dataFimRetiradaQPNR
	 *            the dataFimRetiradaQPNR to set
	 */
	public void setDataFimRetiradaQPNR(Date dataFimRetiradaQPNR) {
		if(dataFimRetiradaQPNR != null) {
			this.dataFimRetiradaQPNR = (Date) dataFimRetiradaQPNR.clone();
		} else {
			this.dataFimRetiradaQPNR = null;
		}
	}

	/**
	 * @return the consumoReferenciaShipOrPay
	 */
	public EntidadeConteudo getConsumoReferenciaShipOrPay() {

		return consumoReferenciaShipOrPay;
	}

	/**
	 * @param consumoReferenciaShipOrPay
	 *            the consumoReferenciaShipOrPay
	 *            to set
	 */
	public void setConsumoReferenciaShipOrPay(EntidadeConteudo consumoReferenciaShipOrPay) {

		this.consumoReferenciaShipOrPay = consumoReferenciaShipOrPay;
	}

	/**
	 * @return the margemVariacaoShipOrPay
	 */
	public BigDecimal getMargemVariacaoShipOrPay() {

		return margemVariacaoShipOrPay;
	}

	/**
	 * @param margemVariacaoShipOrPay
	 *            the margemVariacaoShipOrPay to
	 *            set
	 */
	public void setMargemVariacaoShipOrPay(BigDecimal margemVariacaoShipOrPay) {

		this.margemVariacaoShipOrPay = margemVariacaoShipOrPay;
	}

	/**
	 * @return the tempoValidadeRetiradaQPNR
	 */
	public Integer getTempoValidadeRetiradaQPNR() {

		return tempoValidadeRetiradaQPNR;
	}

	/**
	 * @param tempoValidadeRetiradaQPNR
	 *            the tempoValidadeRetiradaQPNR to
	 *            set
	 */
	public void setTempoValidadeRetiradaQPNR(Integer tempoValidadeRetiradaQPNR) {

		this.tempoValidadeRetiradaQPNR = tempoValidadeRetiradaQPNR;
	}

	/**
	 * @return the listaCompromissoShipOrPayVO
	 */
	public Collection<CompromissoTakeOrPayVO> getListaCompromissoShipOrPayVO() {

		return listaCompromissoShipOrPayVO;
	}

	/**
	 * @param listaCompromissoShipOrPayVO
	 *            the listaCompromissoShipOrPayVO
	 *            to set
	 */
	public void setListaCompromissoShipOrPayVO(Collection<CompromissoTakeOrPayVO> listaCompromissoShipOrPayVO) {

		this.listaCompromissoShipOrPayVO = listaCompromissoShipOrPayVO;
	}

	/**
	 * @return
	 */
	public BigDecimal getVariacaoSuperiorQDC() {

		return variacaoSuperiorQDC;
	}

	/**
	 * @param variacaoSuperiorQDC
	 */
	public void setVariacaoSuperiorQDC(BigDecimal variacaoSuperiorQDC) {

		this.variacaoSuperiorQDC = variacaoSuperiorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ModalidadeConsumoFaturamentoVO)) {
			return false;
		}
		if (this.modalidade.getChavePrimaria() > 0) {
			ModalidadeConsumoFaturamentoVO that = (ModalidadeConsumoFaturamentoVO) obj;
			return new EqualsBuilder().append(modalidade.getChavePrimaria(), that.modalidade.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if (modalidade.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(modalidade.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/**
	 * Obtem horaLimiteProgramacaoDiaria.
	 * 
	 * @return horaLimiteProgramacaoDiaria
	 */
	public Integer getHoraLimiteProgramacaoDiaria() {

		return horaLimiteProgramacaoDiaria;
	}

	/**
	 * Atribui valor a horaLimiteProgramacaoDiaria
	 * 
	 * @param horaLimiteProgramacaoDiaria
	 */
	public void setHoraLimiteProgramacaoDiaria(Integer horaLimiteProgramacaoDiaria) {

		this.horaLimiteProgramacaoDiaria = horaLimiteProgramacaoDiaria;
	}

	/**
	 * Obtem horaLimiteProgIntradiaria
	 * 
	 * @return horaLimiteProgIntradiaria
	 */
	public Integer getHoraLimiteProgramacaoIntraDiaria() {

		return horaLimiteProgIntradiaria;
	}

	/**
	 * Atribui valor a horaLimiteProgramacaoIntraDiaria
	 * 
	 * @param horaLimiteProgramacaoIntraDiaria
	 */
	public void setHoraLimiteProgramacaoIntraDiaria(Integer horaLimiteProgramacaoIntraDiaria) {

		this.horaLimiteProgIntradiaria = horaLimiteProgramacaoIntraDiaria;
	}

	/**
	 * Obtem horaLimiteAceitacaoDiaria
	 * 
	 * @return horaLimiteAceitacaoDiaria
	 */
	public Integer getHoraLimiteAceitacaoDiaria() {

		return horaLimiteAceitacaoDiaria;
	}

	/**
	 * Atribui valor a  horaLimiteAceitacaoDiaria
	 * 
	 * @param horaLimiteAceitacaoDiaria
	 */
	public void setHoraLimiteAceitacaoDiaria(Integer horaLimiteAceitacaoDiaria) {

		this.horaLimiteAceitacaoDiaria = horaLimiteAceitacaoDiaria;
	}

	public Integer getHoraLimiteAceitIntradiaria() {

		return horaLimiteAceitIntradiaria;
	}

	public void setHoraLimiteAceitIntradiaria(Integer horaLimiteAceitIntradiaria) {

		this.horaLimiteAceitIntradiaria = horaLimiteAceitIntradiaria;
	}

	public BigDecimal getPercentualQNR() {

		return percentualQNR;
	}

	public void setPercentualQNR(BigDecimal percentualQNR) {

		this.percentualQNR = percentualQNR;
	}

	public Collection<PenalidadesRetiradaMaiorMenorVO> getListaPenalidadesRetiradaMaiorMenorVO() {

		return listaPenalidadesRetiradaMaiorMenorVO;
	}

	public void setListaPenalidadesRetiradaMaiorMenorVO(Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetiradaMaiorMenorVO) {

		this.listaPenalidadesRetiradaMaiorMenorVO = listaPenalidadesRetiradaMaiorMenorVO;
	}

	public BigDecimal getPercMinDuranteRetiradaQPNR() {

		return percMinDuranteRetiradaQPNR;
	}

	public void setPercMinDuranteRetiradaQPNR(BigDecimal percMinDuranteRetiradaQPNR) {

		this.percMinDuranteRetiradaQPNR = percMinDuranteRetiradaQPNR;
	}

}
