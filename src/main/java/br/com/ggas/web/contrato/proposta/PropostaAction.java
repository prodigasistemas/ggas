/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.proposta;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.SituacaoProposta;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 */
@Controller
public class PropostaAction extends GenericAction {

	private static final String CLIENTE = "cliente";

	private static final String IMOVEL = "imovel";

	private static final int PORCENTAGEM = 100;

	private static final String PROPOSTA_VO = "propostaVO";

	private static final String ATTACHMENT_FILENAME = "attachment; filename=";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	private static final String VENDEDOR = "vendedor";

	private static final String UNIDADE_CONSUMO_MENSAL = "unidadeConsumoMensal";

	private static final String SEGMENTO = "segmento";

	private static final String FISCAL = "fiscal";

	private static final String UNIDADE_CONSUMO_ANUAL = "unidadeConsumoAnual";

	private static final String TARIFA = "tarifa";

	private static final String SITUACAO_PROPOSTA = "situacaoProposta";

	private static final String PERIODICIDADE_JUROS = "periodicidadeJuros";

	private static final String VALOR_MÉDIO_GN = "Valor Médio GN";

	private static final String CONTROLADOR_LEVANTAMENTO_MERCADO = "controladorLevantamentoMercado";

	private static final String PROPOSTA_LEVANTAMENTO_MERCADO = "propostaLevantamentoMercado";

	private static final String LISTA_TARIFAS = "listaTarifas";

	private static final String VALOR_MENSAL_TOTAL = "valorMensalTotal";

	private static final String FLUXO_LEVANTAMENTO_MERCADO = "fluxoLevantamentoMercado";

	private static final String CONSUMO_MEDIO_ANUAL_TOTAL = "consumoMedioAnualTotal";

	private static final String CONSUMO_MEDIO_MENSAL_TOTAL = "consumoMedioMensalTotal";

	private static final String ID_MODALIDADE_USO = "idModalidadeUso";

	private static final String LISTA_MODALIDADE_USO = "listaModalidadeUso";

	private static final String LISTA_TIPO_COMBUSTIVEL_LM = "listaTipoCombustivelLM";

	private static final String PDF = "PDF";

	private static final String ID_IMOVEL = "idImovel";

	private static final String PROPOSTA = "proposta";

	private static final String ID_CLIENTE = "idCliente";

	private static final String NUMERO_PROPOSTA = "numeroProposta";

	private static final String HABILITADO = "habilitado";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String PLANILHA_IMPORTACAO = "planilhaImportacao";

	private static final String ABA_IDENTIFICACAO = "0";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ENVIO_EMAIL_ATIVO = "1";

	private static final String CONTATO_IMOVEL_OBRIGATORIO = "1";

	private static final String SITUACAO_PROPOSTA_REJEITADA = "REJEITADA";

	private static final String SITUACAO_PROPOSTA_APROVADA = "APROVADA";

	/**
	 * CONSTANTES RELATORIO DA PROPOSTA
	 */
	private static final String RELATORIO_PROPOSTA = "relatorioProposta";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_RELATORIO = "exibirRelatorio";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	@Autowired
	private ControladorProposta controladorProposta;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorCliente controladorCliente;

	/**
	 * Método responsável por exibir a tela de inclusão de proposta.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoProposta")
	public String exibirInclusaoProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		if (propostaVO.getChavePrimariaLM() != null && propostaVO.getChavePrimariaLM() > 0) {
			popularFormLM(propostaVO, model);
		}
		if (Boolean.valueOf(request.getParameter("limpar"))) {
			request.getSession().removeAttribute(PLANILHA_IMPORTACAO);
		}
		this.carregarCampos(model, propostaVO.getChavePrimariaLM());

		saveToken(request);
		return "exibirInclusaoProposta";
	}

	/**
	 * Método responsável por incluir uma proposta.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param arquivoForm
	 *            {@link MultipartFile}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("incluirProposta")
	public String incluirProposta(PropostaVO propostaVO, BindingResult result,
			@RequestParam(value = "planilhaImportacao") MultipartFile arquivoForm, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		String view = null;
		Proposta proposta = (Proposta) controladorProposta.criar();
		model.addAttribute(PROPOSTA_VO, propostaVO);
		manterIndicadorPesquisa(propostaVO, model);

		try {

			validarToken(request);

			this.popularProposta(proposta, propostaVO, arquivoForm);
			if (proposta.getVersaoProposta() == null) {
				proposta.setVersaoProposta(1);
			}
			proposta.setDadosAuditoria(getDadosAuditoria(request));

			ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
					.getControladorParametroSistema();

			String valorParametro = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA)
					.getValor();
			if (valorParametro.equals(CONTATO_IMOVEL_OBRIGATORIO)) {
				this.validarContatoImovel(proposta.getImovel(), Boolean.FALSE);
			}

			controladorProposta.inserirNumeroAnoProposta(proposta);

			validarConsumosMedios(proposta, propostaVO);
			Long idProposta = controladorProposta.inserir(proposta);

			Proposta propostaComChavePrimaria = (Proposta) controladorProposta.obter(idProposta);
			this.vincularPropostaLevantamentoMercado(propostaVO, propostaComChavePrimaria);

			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put(PROPOSTA, proposta);
			byte[] relatorio = controladorProposta.gerarPropostaPDF(parametros);
			String valorParametroEnviarEmail = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.ENVIAR_EMAIL_AO_SALVAR_PROPOSTA).getValor();

			boolean temErro = validaEEnviaEmail(request, proposta, relatorio, valorParametroEnviarEmail, model);

			if (!temErro) {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Proposta.PROPOSTA_CAMPO_STRING);
				request.getSession().removeAttribute(PLANILHA_IMPORTACAO);
			}
			propostaVO.setChavePrimaria(proposta.getChavePrimaria());
			view = gerarPropostaPDF(propostaVO, result, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirInclusaoProposta(propostaVO, result, request, model);
		}

		return view;
	}

	/**
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param proposta
	 *            {@link Proposta}
	 * @param relatorio
	 *            {@link byte}
	 * @param valorParametroEnviarEmail
	 *            {@link String}
	 * @param model
	 *            {@link Model}
	 * @return boolean {@link boolean}
	 */
	private boolean validaEEnviaEmail(HttpServletRequest request, Proposta proposta, byte[] relatorio,
			String valorParametroEnviarEmail, Model model) {

		boolean temErro = false;
		if (valorParametroEnviarEmail.equalsIgnoreCase(ENVIO_EMAIL_ATIVO)) {
			try {
				this.validarContatoImovel(proposta.getImovel(), Boolean.TRUE);
			} catch (Exception e) {
				Log.error(e);
				mensagemErro(model, request, Constantes.ALERTA_INSERIDO_SEM_ENVIAR_EMAIL);
				temErro = true;
			}

			try {
				this.enviarEmail(proposta, request, relatorio, model);
			} catch (GGASException e) {
				Log.error(e);
				mensagemErro(model, request, Constantes.ALERTA_INSERIDO_ERRO_ENVIO_EMAIL);
				temErro = true;
			}
		}
		return temErro;
	}

	/**
	 * 
	 * 
	 * @param proposta
	 *            {@link Proposta}
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void validarConsumosMedios(Proposta proposta, PropostaVO propostaVO) throws NegocioException {

		LevantamentoMercado levMercado = null;
		if (propostaVO.getPropostaLevantamentoMercado() != null && propostaVO.getChavePrimariaLM() != null
				&& propostaVO.getChavePrimariaLM() > 0 && "true".equals(propostaVO.getPropostaLevantamentoMercado())) {
			ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
					.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
			levMercado = controladorLevantamentoMercado.obterLevantamentoMercado(propostaVO.getChavePrimariaLM());

		}

		controladorProposta.validarConsumosMedios(proposta, levMercado);
	}

	/**
	 * Carregar campos.
	 * 
	 * 
	 * @param model
	 *            {@link Model}
	 * @param idLevantamentoMercado
	 *            {@link Long}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void carregarCampos(Model model, Long idLevantamentoMercado) throws GGASException {

		model.addAttribute("listaSituacaoProposta", controladorProposta.listarSituacaoProposta());
		model.addAttribute("listaUnidadeConsumo", controladorUnidade.listarUnidadesVolume());
		String codigoPeriodicidadeJuros = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CLASSE_PERIODICIDADE_JUROS);
		model.addAttribute("listaPeriodicidadeJuros",
				controladorProposta.consultarPeriodicidadeJuros(Long.valueOf(codigoPeriodicidadeJuros)));
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaSituacaoProposta", controladorProposta.listarSituacoesIniciaisProposta());
		model.addAttribute("listaModalidadeMedicao", controladorImovel.listarModalidadeMedicaoImovel());
		model.addAttribute("listaVendedor", controladorFuncionario.listarVendedores());
		model.addAttribute("listaFiscal", controladorFuncionario.listarFiscais());
		model.addAttribute(LISTA_MODALIDADE_USO, controladorImovel.listarModalidadeUso());

		if (idLevantamentoMercado != null && idLevantamentoMercado > 0) {
			ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
					.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
			LevantamentoMercado levMercado = controladorLevantamentoMercado
					.obterLevantamentoMercado(idLevantamentoMercado);
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_SEGMENTO, levMercado.getSegmento().getChavePrimaria());
			filtro.put("verificarTarifaVigenciaAutorizada", Boolean.TRUE);
			model.addAttribute(LISTA_TARIFAS, controladorTarifa.consultarTarifas(filtro));
		} else {
			model.addAttribute(LISTA_TARIFAS, controladorTarifa.listarTarifas());
		}
	}

	/**
	 * Popular proposta.
	 * 
	 * @param proposta
	 *            {@link Proposta}
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param arquivoForm
	 *            {@link MultipartFile}
	 * @throws IOException
	 *             {@link IOException}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularProposta(Proposta proposta, PropostaVO propostaVO, MultipartFile arquivoForm)
			throws IOException, GGASException {

		if (propostaVO.getIdModalidadeUso() != null && propostaVO.getIdModalidadeUso() > 0) {
			proposta.setModalidadeUso(
					controladorEntidadeConteudo.obterEntidadeConteudo(propostaVO.getIdModalidadeUso()));
		}

		if (arquivoForm != null) {
			proposta.setTipoPlanilha(arquivoForm.getContentType());
			proposta.setPlanilha(arquivoForm.getBytes());
			proposta.setNomePlanilha(arquivoForm.getOriginalFilename());
		}

		if (propostaVO.getIndicadorNovaVersao() != null && propostaVO.getIndicadorNovaVersao()) {
			if (!StringUtils.isEmpty(propostaVO.getVersaoPropostaAnterior())) {
				Integer novaVersaoProposta = Util.converterCampoStringParaValorInteger(Proposta.VERSAO_PROPOSTA,
						propostaVO.getVersaoPropostaAnterior());
				novaVersaoProposta += 1;
				proposta.setVersaoProposta(novaVersaoProposta);
			}
		} else {
			if (propostaVO.getChavePrimaria() != null && propostaVO.getChavePrimaria() > 0) {
				proposta.setChavePrimaria(propostaVO.getChavePrimaria());
			}
			if (StringUtils.isEmpty(propostaVO.getVersaoProposta())) {
				proposta.setVersaoProposta(null);
			} else {
				proposta.setVersaoProposta(Util.converterCampoStringParaValorInteger(Proposta.VERSAO_PROPOSTA,
						propostaVO.getVersaoProposta()));
			}
		}

		if (propostaVO.getVersao() != null && Long.parseLong(propostaVO.getVersao()) > 0) {
			proposta.setVersao(Util.converterCampoStringParaValorInteger(propostaVO.getVersao()));
		}

		if (propostaVO.getHabilitado() != null) {
			proposta.setHabilitado(propostaVO.getHabilitado());
		}
		if (StringUtils.isEmpty(propostaVO.getNumeroProposta())) {
			proposta.setNumeroProposta(null);
		} else {
			proposta.setNumeroProposta(propostaVO.getNumeroProposta());
		}
		if (StringUtils.isEmpty(propostaVO.getDataEmissao())) {
			proposta.setDataEmissao(null);
		} else {
			proposta.setDataEmissao(Util.converterCampoStringParaData(Proposta.DATA_EMISSAO,
					propostaVO.getDataEmissao(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isEmpty(propostaVO.getDataVigencia())) {
			proposta.setDataVigencia(null);
		} else {
			proposta.setDataVigencia(Util.converterCampoStringParaData(Proposta.DATA_VIGENCIA,
					propostaVO.getDataVigencia(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isEmpty(propostaVO.getDataEntrega())) {
			proposta.setDataEntrega(null);
		} else {
			proposta.setDataEntrega(Util.converterCampoStringParaData(Proposta.DATA_ENTREGA,
					propostaVO.getDataEntrega(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isEmpty(propostaVO.getPercentualTIR())) {
			proposta.setPercentualTIR(null);
		} else {
			BigDecimal valor = Util.converterCampoStringParaValorBigDecimal("Percentual TIR",
					propostaVO.getPercentualTIR(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			proposta.setPercentualTIR(Util.converterBigDecimalParaPercentual(valor));
		}
		if (StringUtils.isEmpty(propostaVO.getValorMaterial())) {
			proposta.setValorMaterial(null);
		} else {
			proposta.setValorMaterial(Util.converterCampoStringParaValorBigDecimal("Valor Material",
					propostaVO.getValorMaterial().trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isEmpty(propostaVO.getValorMaoDeObra())) {
			proposta.setValorMaoDeObra(null);
		} else {
			proposta.setValorMaoDeObra(Util.converterCampoStringParaValorBigDecimal("Valor de Mão De Obra",
					propostaVO.getValorMaoDeObra(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isEmpty(propostaVO.getValorInvestimento())) {
			proposta.setValorInvestimento(null);
		} else {
			proposta.setValorInvestimento(Util.converterCampoStringParaValorBigDecimal("Valor Investimento",
					propostaVO.getValorInvestimento(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (propostaVO.getIndicadorParticipacao() != null) {
			proposta.setIndicadorParticipacao(propostaVO.getIndicadorParticipacao());
			if (propostaVO.getIndicadorParticipacao()) {
				if (StringUtils.isEmpty(propostaVO.getPercentualCliente())) {
					proposta.setPercentualCliente(null);
				} else {
					BigDecimal valor = Util.converterCampoStringParaValorBigDecimal("Percentual Cliente",
							propostaVO.getPercentualCliente(), Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);
					proposta.setPercentualCliente(Util.converterBigDecimalParaPercentual(valor));
				}
				if (StringUtils.isEmpty(propostaVO.getValorCliente())) {
					proposta.setValorCliente(null);
				} else {
					proposta.setValorCliente(Util.converterCampoStringParaValorBigDecimal("Valor Cliente",
							propostaVO.getValorCliente(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
				}
				if (StringUtils.isEmpty(propostaVO.getQuantidadeParcela())) {
					proposta.setQuantidadeParcela(null);
				} else {
					proposta.setQuantidadeParcela(Util.converterCampoStringParaValorInteger(Proposta.QUANTIDADE_PARCELA,
							propostaVO.getQuantidadeParcela()));
				}
				if (StringUtils.isEmpty(propostaVO.getValorParcela())) {
					proposta.setValorParcela(null);
				} else {
					proposta.setValorParcela(Util.converterCampoStringParaValorBigDecimal("Valor Parcela",
							propostaVO.getValorParcela(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
				}
				if (StringUtils.isEmpty(propostaVO.getPercentualJuros())) {
					proposta.setPercentualJuros(null);
				} else {
					BigDecimal valor = Util.converterCampoStringParaValorBigDecimal("Percentual Juros",
							propostaVO.getPercentualJuros(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
					proposta.setPercentualJuros(Util.converterBigDecimalParaPercentual(valor));
				}
			}
		}
		if (StringUtils.isEmpty(propostaVO.getConsumoMedioAnual())) {
			proposta.setConsumoMedioAnual(null);
		} else {
			proposta.setConsumoMedioAnual(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Anual",
					propostaVO.getConsumoMedioAnual(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isEmpty(propostaVO.getConsumoMedioMensal())) {
			proposta.setConsumoMedioMensal(null);
		} else {
			proposta.setConsumoMedioMensal(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Mensal",
					propostaVO.getConsumoMedioMensal(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getConsumoUnidadeConsumidora())) {
			proposta.setConsumoUnidadeConsumidora(null);
		} else {
			proposta.setConsumoUnidadeConsumidora(Util.converterCampoStringParaValorBigDecimal(
					"Consumo Unidade Consumidora", propostaVO.getConsumoUnidadeConsumidora(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isEmpty(propostaVO.getAnoMesReferenciaPreco())) {
			proposta.setAnoMesReferenciaPreco(null);
		} else {
			String anoMesSemBarra = Util.removerCaracteresEspeciais(propostaVO.getAnoMesReferenciaPreco());
			proposta.setAnoMesReferenciaPreco(
					Util.converterCampoStringParaValorInteger(Proposta.ANO_MES_REFERENCIA_PRECO, anoMesSemBarra));
		}

		if (StringUtils.isEmpty(propostaVO.getValorGastoMensal())) {
			proposta.setValorGastoMensal(null);
		} else {
			proposta.setValorGastoMensal(Util.converterCampoStringParaValorBigDecimal("Valor Gasto Mensal",
					propostaVO.getValorGastoMensal(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getEconomiaMensalGN())) {
			proposta.setEconomiaMensalGN(null);
		} else {
			proposta.setEconomiaMensalGN(Util.converterCampoStringParaValorBigDecimal("Economia Mensal GN",
					propostaVO.getEconomiaMensalGN(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getEconomiaAnualGN())) {
			proposta.setEconomiaAnualGN(null);
		} else {
			proposta.setEconomiaAnualGN(Util.converterCampoStringParaValorBigDecimal("Economia Anual GN",
					propostaVO.getEconomiaAnualGN(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isEmpty(propostaVO.getPercentualEconomia())) {
			proposta.setPercentualEconomia(null);
		} else {
			BigDecimal valor = Util.converterCampoStringParaValorBigDecimal("Percentual Economia",
					propostaVO.getPercentualEconomia(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			proposta.setPercentualEconomia(Util.converterBigDecimalParaPercentual(valor));
		}
		if ((propostaVO.getIdSituacaoProposta() == null) || (propostaVO.getIdSituacaoProposta() <= 0)) {
			proposta.setSituacaoProposta(null);
		} else {
			SituacaoProposta situacaoProposta = controladorProposta
					.obterSituacaoProposta(propostaVO.getIdSituacaoProposta());
			proposta.setSituacaoProposta(situacaoProposta);
		}

		if (propostaVO.getIdTarifa() != null && propostaVO.getIdTarifa() > -1) {
			Tarifa tarifa = controladorTarifa.obterTarifa(propostaVO.getIdTarifa());
			if (tarifa != null) {
				proposta.setTarifa(tarifa);
			}
		} else {
			proposta.setTarifa(null);
		}

		if ((propostaVO.getIdPeriodicidadeJuros() == null) || (propostaVO.getIdPeriodicidadeJuros() <= 0)) {
			proposta.setPeriodicidadeJuros(null);
		} else {
			EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo
					.obterEntidadeConteudo(propostaVO.getIdPeriodicidadeJuros());
			proposta.setPeriodicidadeJuros(entidadeConteudo);
		}

		if (propostaVO.getIdSegmento() == null || propostaVO.getIdSegmento() <= 0) {
			proposta.setSegmento(null);
		} else {
			Segmento segmento = controladorSegmento.obterSegmento(propostaVO.getIdSegmento());
			proposta.setSegmento(segmento);
		}

		if ((propostaVO.getIdImovel() == null) || (propostaVO.getIdImovel() <= 0)) {
			proposta.setImovel(null);
		} else {
			Imovel imovel = (Imovel) controladorImovel.obter(propostaVO.getIdImovel(), "redeInternaImovel",
					"listaClienteImovel", "unidadesConsumidoras");
			if (imovel.getCondominio()) {
				if (!StringUtils.isEmpty(propostaVO.getNumeroAptoLojas())) {
					proposta.setQuantidadeApartamentos(Integer.valueOf(propostaVO.getNumeroAptoLojas()));
				} else {
					proposta.setQuantidadeApartamentos(null);
				}
				if (propostaVO.getCodigoModalidadeMedicao() == null || propostaVO.getCodigoModalidadeMedicao() <= 0) {
					proposta.setIndicadorMedicao(false);
				} else {
					ModalidadeMedicaoImovel modalidadeMedicao = controladorImovel
							.obterModalidadeMedicaoImovel(propostaVO.getCodigoModalidadeMedicao());
					if (modalidadeMedicao != null) {
						proposta.setIndicadorMedicao(true);
						imovel.setModalidadeMedicaoImovel(modalidadeMedicao);
					}
				}
			}
			proposta.setImovel(imovel);
		}

		if ((propostaVO.getIdCliente() == null) || (propostaVO.getIdCliente() <= 0)) {
			proposta.setCliente(null);
		} else {
			Cliente cliente = controladorCliente.obterCliente(propostaVO.getIdCliente());
			proposta.setCliente(cliente);
		}

		if ((propostaVO.getIdUnidadeConsumoAnual() == null) || (propostaVO.getIdUnidadeConsumoAnual() <= 0)) {
			proposta.setUnidadeConsumoAnual(null);
		} else {
			Unidade unidadeConsumoAnual = (Unidade) controladorUnidade.obter(propostaVO.getIdUnidadeConsumoAnual());
			proposta.setUnidadeConsumoAnual(unidadeConsumoAnual);
		}
		if ((propostaVO.getIdUnidadeConsumoMensal() == null) || (propostaVO.getIdUnidadeConsumoMensal() <= 0)) {
			proposta.setUnidadeConsumoMensal(null);
		} else {
			Unidade unidadeConsumoMensal = (Unidade) controladorUnidade.obter(propostaVO.getIdUnidadeConsumoMensal());
			proposta.setUnidadeConsumoMensal(unidadeConsumoMensal);
		}
		if (StringUtils.isEmpty(propostaVO.getDataAssembleiaCondominio())) {
			proposta.setDataApresentacaoCondominio(null);
		} else {
			proposta.setDataApresentacaoCondominio(
					Util.converterCampoStringParaData(Proposta.DATA_APRESENTACAO_ASSEMBLEIA,
							propostaVO.getDataAssembleiaCondominio(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isEmpty(propostaVO.getDataEleicaoSindico())) {
			proposta.setDataEleicaoSindico(null);
		} else {
			proposta.setDataEleicaoSindico(Util.converterCampoStringParaData(Proposta.DATA_ELEICAO_SINDICO,
					propostaVO.getDataEleicaoSindico(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isEmpty(propostaVO.getComentarioAdicional())) {
			proposta.setComentario(propostaVO.getComentarioAdicional());
		} else {
			controladorProposta.validarTamanhoComentario(propostaVO.getComentarioAdicional());
			proposta.setComentario(propostaVO.getComentarioAdicional());
		}

		if (StringUtils.isEmpty(propostaVO.getConsumoMedioAnualGN())) {
			proposta.setConsumoMedioAnualGN(null);
		} else {
			proposta.setConsumoMedioAnualGN(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Anual GN",
					propostaVO.getConsumoMedioAnualGN(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getConsumoMedioMensalGN())) {
			proposta.setConsumoMedioMensalGN(null);
		} else {
			proposta.setConsumoMedioMensalGN(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Mensal GN",
					propostaVO.getConsumoMedioMensalGN(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getVolumeDiarioEstimadoGN())) {
			proposta.setVolumeDiarioEstimadoGN(null);
		} else {
			proposta.setVolumeDiarioEstimadoGN(Util.converterCampoStringParaValorBigDecimal("Volume Diário Estimado GN",
					propostaVO.getVolumeDiarioEstimadoGN().trim(), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getValorGastoMensalGN())) {
			proposta.setValorMensalGastoGN(null);
		} else {
			proposta.setValorMensalGastoGN(Util.converterCampoStringParaValorBigDecimal("Valor Gasto Mensal GN",
					propostaVO.getValorGastoMensalGN().trim(), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(propostaVO.getValorParticipacaoCDL())) {
			proposta.setValorParticipacaoCDL(null);
		} else {
			proposta.setValorParticipacaoCDL(Util.converterCampoStringParaValorBigDecimal("Valor Participação CDL",
					propostaVO.getValorParticipacaoCDL().trim(), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));
		}

		if (propostaVO.getIdFiscal() == null || propostaVO.getIdFiscal() <= 0) {
			proposta.setFiscal(null);
		} else {
			Funcionario fiscal = (Funcionario) controladorFuncionario.obter(propostaVO.getIdFiscal());
			proposta.setFiscal(fiscal);
		}

		if (propostaVO.getIdVendedor() == null || propostaVO.getIdVendedor() <= 0) {
			proposta.setVendedor(null);
		} else {
			Funcionario vendedor = (Funcionario) controladorFuncionario.obter(propostaVO.getIdVendedor());
			proposta.setVendedor(vendedor);
		}

		if (!StringUtils.isEmpty(propostaVO.getValorMedioGN())) {
			proposta.setValorMedioGN(Util.converterCampoStringParaValorBigDecimal(VALOR_MÉDIO_GN,
					propostaVO.getValorMedioGN().trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
	}

	/**
	 * Popular form.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param proposta
	 *            {@link}
	 * @param model
	 *            {@link Model}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 */
	private void popularForm(PropostaVO propostaVO, HttpServletRequest request, Proposta proposta, Model model)
			throws NegocioException, FormatoInvalidoException {

		if (!super.isPostBack(request)) {
			propostaVO.setChavePrimaria(proposta.getChavePrimaria());
			propostaVO.setVersao(String.valueOf(proposta.getVersao()));
			propostaVO.setHabilitado(proposta.isHabilitado());

			if (proposta.getNumeroProposta() != null) {
				propostaVO.setNumeroProposta(proposta.getNumeroProposta());
			}
			if (proposta.getNumeroCompletoProposta() != null) {
				propostaVO.setNumeroProposta(String.valueOf(proposta.getNumeroCompletoProposta()));
			}
			if (proposta.getVersaoProposta() != null) {
				propostaVO.setVersaoProposta(String.valueOf(proposta.getVersaoProposta()));
				propostaVO.setVersaoPropostaAnterior(String.valueOf(proposta.getVersaoProposta()));
			}
			if (proposta.getDataEmissao() != null) {
				propostaVO.setDataEmissao(
						Util.converterDataParaStringSemHora(proposta.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			}
			if (proposta.getDataVigencia() != null) {
				propostaVO.setDataVigencia(
						Util.converterDataParaStringSemHora(proposta.getDataVigencia(), Constantes.FORMATO_DATA_BR));
			}
			if (proposta.getDataEntrega() != null) {
				propostaVO.setDataEntrega(
						Util.converterDataParaStringSemHora(proposta.getDataEntrega(), Constantes.FORMATO_DATA_BR));
			}
			if (proposta.getPercentualTIR() != null) {
				propostaVO.setPercentualTIR(Util.converterCampoPercentualParaString("Percentual TIR",
						proposta.getPercentualTIR().multiply(new BigDecimal(100)), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorMaterial() != null) {
				propostaVO.setValorMaterial(
						Util.converterCampoCurrencyParaString(proposta.getValorMaterial(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorMaoDeObra() != null) {
				propostaVO.setValorMaoDeObra(
						Util.converterCampoCurrencyParaString(proposta.getValorMaoDeObra(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorInvestimento() != null) {
				propostaVO.setValorInvestimento(Util.converterCampoCurrencyParaString(proposta.getValorInvestimento(),
						Constantes.LOCALE_PADRAO));
			}

			if (proposta.getVendedor() != null) {
				propostaVO.setNomeVendedor(proposta.getVendedor().getNome());
			}

			if (proposta.getFiscal() != null) {
				propostaVO.setNomeFiscal(proposta.getFiscal().getNome());
			}

			if (proposta.getIndicadorMedicao() != null) {
				propostaVO.setIndicadorMedicao(proposta.getIndicadorMedicao());
			}

			if (proposta.isIndicadorParticipacao()) {
				propostaVO.setIndicadorParticipacao(proposta.isIndicadorParticipacao());
			} else {
				propostaVO.setIndicadorParticipacao(false);
			}

			if (proposta.getPercentualCliente() != null) {
				propostaVO.setPercentualCliente(Util.converterCampoPercentualParaString("Percentual CDL",
						proposta.getPercentualCliente().multiply(new BigDecimal(100)), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorCliente() != null) {
				propostaVO.setValorCliente(
						Util.converterCampoCurrencyParaString(proposta.getValorCliente(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorParcela() != null) {
				propostaVO.setValorParcela(
						Util.converterCampoCurrencyParaString(proposta.getValorParcela(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getPercentualJuros() != null) {
				propostaVO.setPercentualJuros(Util.converterCampoPercentualParaString("Percentual dos Juros",
						proposta.getPercentualJuros().multiply(new BigDecimal(100)), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getAnoMesReferenciaPreco() != null) {
				propostaVO.setAnoMesReferenciaPreco(
						Util.converterDataIntegerParaDataString(proposta.getAnoMesReferenciaPreco()));
			}
			if (proposta.getValorGastoMensal() != null) {
				propostaVO.setValorGastoMensal(Util.converterCampoCurrencyParaString(proposta.getValorGastoMensal(),
						Constantes.LOCALE_PADRAO));
			}
			if (proposta.getPercentualEconomia() != null) {
				propostaVO.setPercentualEconomia(Util.converterCampoPercentualParaString("Percentual Economia",
						proposta.getPercentualEconomia().multiply(new BigDecimal(PORCENTAGEM)),
						Constantes.LOCALE_PADRAO));
			}
			if (proposta.getEconomiaAnualGN() != null) {
				propostaVO.setEconomiaAnualGN(
						Util.converterCampoCurrencyParaString(proposta.getEconomiaAnualGN(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getEconomiaMensalGN() != null) {
				propostaVO.setEconomiaMensalGN(Util.converterCampoCurrencyParaString(proposta.getEconomiaMensalGN(),
						Constantes.LOCALE_PADRAO));
			}
			if (proposta.getSituacaoProposta() != null) {
				propostaVO.setIdSituacaoProposta(proposta.getSituacaoProposta().getChavePrimaria());
				if (proposta.getSituacaoProposta().getDescricao() != null
						&& !StringUtils.isEmpty(proposta.getSituacaoProposta().getDescricao())) {
					propostaVO.setDescricaoSituacaoProposta(proposta.getSituacaoProposta().getDescricao());
				}
			}
			if (proposta.getImovel() != null) {
				model.addAttribute(IMOVEL, proposta.getImovel());
				if (proposta.getImovel().getCondominio()) {
					model.addAttribute("condominio", Boolean.TRUE);
				}
			}

			if (proposta.getCliente() != null) {
				model.addAttribute(CLIENTE, proposta.getCliente());
			}

			if (proposta.getQuantidadeApartamentos() != null) {
				propostaVO.setNumeroAptoLojas(proposta.getQuantidadeApartamentos().toString());
			}

			if (proposta.getTarifa() != null) {
				propostaVO.setIdTarifa(proposta.getTarifa().getChavePrimaria());
			}
			if (proposta.getConsumoUnidadeConsumidora() != null
					&& proposta.getConsumoUnidadeConsumidora().intValue() > 0) {
				propostaVO.setConsumoUnidadeConsumidora(
						Util.converterCampoValorDecimalParaString("Consumo Unidade Consumidora",
								proposta.getConsumoUnidadeConsumidora(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getConsumoMedioMensal() != null && proposta.getConsumoMedioMensal().intValue() > 0) {
				propostaVO.setConsumoMedioMensal(Util.converterCampoValorDecimalParaString("Consumo Medio Mensal",
						proposta.getConsumoMedioMensal(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getConsumoMedioAnual() != null && proposta.getConsumoMedioAnual().intValue() > 0) {
				propostaVO.setConsumoMedioAnual(Util.converterCampoValorDecimalParaString("Consumo Medio Anual",
						proposta.getConsumoMedioAnual(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getUnidadeConsumoAnual() != null) {
				propostaVO.setIdUnidadeConsumoAnual(proposta.getUnidadeConsumoAnual().getChavePrimaria());
				propostaVO.setDescricaoUnidadeConsumoAnual(proposta.getUnidadeConsumoAnual().getDescricaoAbreviada());
			}
			if (proposta.getUnidadeConsumoMensal() != null) {
				propostaVO.setIdUnidadeConsumoMensal(proposta.getUnidadeConsumoMensal().getChavePrimaria());
				propostaVO.setDescricaoUnidadeConsumoMensal(proposta.getUnidadeConsumoMensal().getDescricaoAbreviada());
			}
			if (proposta.getDataApresentacaoCondominio() != null) {
				propostaVO.setDataAssembleiaCondominio(Util.converterDataParaStringSemHora(
						proposta.getDataApresentacaoCondominio(), Constantes.FORMATO_DATA_BR));
			}
			if (proposta.getDataEleicaoSindico() != null) {
				propostaVO.setDataEleicaoSindico(Util.converterDataParaStringSemHora(proposta.getDataEleicaoSindico(),
						Constantes.FORMATO_DATA_BR));
			}
			if (proposta.getComentario() != null) {
				propostaVO.setComentarioAdicional(proposta.getComentario());
			}
			if (proposta.getPeriodicidadeJuros() != null) {
				propostaVO.setIdPeriodicidadeJuros(proposta.getPeriodicidadeJuros().getChavePrimaria());
				if (proposta.getPeriodicidadeJuros().getDescricao() != null
						&& !StringUtils.isEmpty(proposta.getPeriodicidadeJuros().getDescricao())) {
					propostaVO.setPeriodicidadeJuros(proposta.getPeriodicidadeJuros().getDescricao());
				}
			}
			if (proposta.getSegmento() != null) {
				propostaVO.setIdSegmento(proposta.getSegmento().getChavePrimaria());
				propostaVO.setDescricaoSegmento(proposta.getSegmento().getDescricao());
			}

			if (proposta.getModalidadeUso() != null) {
				propostaVO.setIdModalidadeUso(proposta.getModalidadeUso().getChavePrimaria());
			}

			request.getSession().removeAttribute(PLANILHA_IMPORTACAO);

			if (proposta.getConsumoMedioAnualGN() != null && proposta.getConsumoMedioAnualGN().intValue() > 0) {
				propostaVO.setConsumoMedioAnualGN(Util.converterCampoValorDecimalParaString("Consumo Medio Anual",
						proposta.getConsumoMedioAnualGN(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getConsumoMedioMensalGN() != null && proposta.getConsumoMedioMensalGN().intValue() > 0) {
				propostaVO.setConsumoMedioMensalGN(Util.converterCampoValorDecimalParaString("Consumo Medio Mensal",
						proposta.getConsumoMedioMensalGN(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getVolumeDiarioEstimadoGN() != null && proposta.getVolumeDiarioEstimadoGN().intValue() > 0) {
				propostaVO.setVolumeDiarioEstimadoGN(Util.converterCampoValorDecimalParaString("Volume Diario Esimado",
						proposta.getVolumeDiarioEstimadoGN(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorMensalGastoGN() != null && proposta.getValorMensalGastoGN().intValue() > 0) {
				propostaVO.setValorGastoMensalGN(Util.converterCampoValorDecimalParaString("Valor Mensal Gasto",
						proposta.getValorMensalGastoGN(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorParticipacaoCDL() != null && proposta.getValorParticipacaoCDL().intValue() > 0) {
				propostaVO.setValorParticipacaoCDL(Util.converterCampoValorDecimalParaString("Participacao CDL",
						proposta.getValorParticipacaoCDL(), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getFiscal() != null) {
				propostaVO.setIdFiscal(proposta.getFiscal().getChavePrimaria());
				propostaVO.setNomeFiscal(proposta.getFiscal().getNome());
			}

			if (proposta.getVendedor() != null) {
				propostaVO.setIdVendedor(proposta.getVendedor().getChavePrimaria());
				propostaVO.setNomeVendedor(proposta.getVendedor().getNome());
			}

			if (proposta.getQuantidadeParcela() != null) {
				propostaVO.setQuantidadeParcela(String.valueOf(proposta.getQuantidadeParcela()));
			} else {
				propostaVO.setQuantidadeParcela(null);
			}
			if (proposta.getValorParcela() != null) {
				propostaVO.setValorParcela(
						Util.converterCampoCurrencyParaString(proposta.getValorParcela(), Constantes.LOCALE_PADRAO));
			}
			ControladorLevantamentoMercado controlMercado = (ControladorLevantamentoMercado) ServiceLocator
					.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);

			LevantamentoMercado levantamentoMercado = controlMercado
					.obterLevantamentoMercadoPorChaveProposta(proposta.getChavePrimaria());
			if (levantamentoMercado != null) {
				Collection<LevantamentoMercadoTipoCombustivel> lista = new ArrayList<LevantamentoMercadoTipoCombustivel>();

				if (!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
					for (LevantamentoMercadoTipoCombustivel lev : levantamentoMercado.getListaTipoCombustivel()) {
						lista.add(lev);
					}
					model.addAttribute(LISTA_TIPO_COMBUSTIVEL_LM, lista);
				}
			}

		}
		ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
				.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
		if (propostaVO.getChavePrimariaLM() != null && propostaVO.getChavePrimariaLM() > 0) {
			LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado
					.obterLevantamentoMercado(propostaVO.getChavePrimariaLM());
			model.addAttribute(FLUXO_LEVANTAMENTO_MERCADO, "true");
			if (!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
				List<LevantamentoMercadoTipoCombustivel> levaList = new ArrayList<LevantamentoMercadoTipoCombustivel>();
				for (LevantamentoMercadoTipoCombustivel levCombustivel : levantamentoMercado
						.getListaTipoCombustivel()) {
					levaList.add(levCombustivel);
				}
				model.addAttribute(LISTA_TIPO_COMBUSTIVEL_LM, levaList);
			}
		}

		if (proposta.getValorMedioGN() != null) {
			propostaVO.setValorMedioGN(Util.converterCampoValorDecimalParaString(VALOR_MÉDIO_GN,
					proposta.getValorMedioGN(), Constantes.LOCALE_PADRAO));
		}
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de proposta.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaProposta")
	public String exibirPesquisaProposta(HttpServletRequest request, Model model) {

		request.removeAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
		request.getSession().removeAttribute(PLANILHA_IMPORTACAO);
		if(request.getSession().getAttribute(PROPOSTA_VO) != null){
			request.getSession().removeAttribute(PROPOSTA_VO);
		}
		if (!model.containsAttribute(PROPOSTA_VO)) {
			PropostaVO propostaVO = new PropostaVO();
			propostaVO.setIndicadorPesquisa("indicadorPesquisaImovel");
			propostaVO.setHabilitado(true);
			model.addAttribute(PROPOSTA_VO, propostaVO);
		}

		return "exibirPesquisaProposta";
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Proposta.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("pesquisarProposta")
	public String pesquisarProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		this.prepararFiltro(filtro, propostaVO);
		Collection<Proposta> listaPropostas;
		
		try {
			manterIndicadorPesquisa(propostaVO, model);
			listaPropostas = controladorProposta.consultarPropostas(filtro);
			model.addAttribute("listaPropostas", listaPropostas);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return exibirPesquisaProposta(request, model);
	}

	/**
	 * Método responsável por manter os dados dos indicadores de pesquisa
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param model
	 *            {@link Model}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void manterIndicadorPesquisa(PropostaVO propostaVO, Model model) throws NegocioException {
		if (propostaVO.getIdCliente() != null) {
			model.addAttribute(CLIENTE, (Cliente) controladorCliente.obter(propostaVO.getIdCliente()));
		}
		if (propostaVO.getIdImovel() != null) {
			model.addAttribute(IMOVEL, (Imovel) controladorImovel.obter(propostaVO.getIdImovel()));
		}
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param propostaVO
	 *            {@link PropostaVO}
	 */
	private void prepararFiltro(Map<String, Object> filtro, PropostaVO propostaVO) {

		if ((propostaVO.getIdCliente() != null) && (propostaVO.getIdCliente() > 0)) {
			filtro.put(ID_CLIENTE, propostaVO.getIdCliente());
		}

		if ((propostaVO.getIdImovel() != null) && (propostaVO.getIdImovel() > 0)) {
			filtro.put(ID_IMOVEL, propostaVO.getIdImovel());
		}

		if (propostaVO.getHabilitado() != null) {
			filtro.put(HABILITADO, propostaVO.getHabilitado());
		}

		if (!StringUtils.isEmpty(propostaVO.getNumeroProposta())) {
			filtro.put(NUMERO_PROPOSTA, propostaVO.getNumeroProposta());
		}

		if (propostaVO.getChavePrimaria() != null && propostaVO.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, propostaVO.getChavePrimaria());
		}
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Proposta.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoProposta")
	public String exibirDetalhamentoProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirDetalhamentoProposta";
		super.salvarPesquisa(request, propostaVO, PROPOSTA_VO);
		
		Long chavePrimaria = propostaVO.getChavePrimaria();
		propostaVO = new PropostaVO();
		
		try {
			Proposta proposta = (Proposta) controladorProposta.obter(chavePrimaria, SITUACAO_PROPOSTA,
					TARIFA, UNIDADE_CONSUMO_ANUAL, UNIDADE_CONSUMO_MENSAL, PERIODICIDADE_JUROS, SEGMENTO, FISCAL,
					VENDEDOR);
			this.popularForm(propostaVO, request, proposta, model);
			model.addAttribute(PROPOSTA, proposta);
			this.carregarCampos(model, null);
			this.popularDadosDetalhamento(propostaVO, proposta, model);
			model.addAttribute(PROPOSTA_VO, propostaVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaProposta(request, model);
		}
		return view;
	}

	/**
	 * Método responsável por exibir a tela de alteração de Proposta.
	 * 
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoProposta")
	public String exibirAlteracaoProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirAlteracaoProposta";
		PropostaVO voProposta = new PropostaVO();
		voProposta.setIdCliente(propostaVO.getIdCliente());
		voProposta.setIdImovel(propostaVO.getIdImovel());
		voProposta.setNumeroProposta(propostaVO.getNumeroProposta());
		voProposta.setHabilitado(propostaVO.getHabilitado());
		super.salvarPesquisa(request, voProposta, PROPOSTA_VO);
		
		ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
				.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);

		Proposta proposta = null;
		try {
			if (propostaVO.getChavePrimariaLM() != null && propostaVO.getChavePrimariaLM() > 0) {
				proposta = (Proposta) controladorProposta.obter(
						controladorLevantamentoMercado.obterLevantamentoMercado(propostaVO.getChavePrimariaLM())
								.getProposta().getChavePrimaria(),
						SITUACAO_PROPOSTA, TARIFA, UNIDADE_CONSUMO_ANUAL, UNIDADE_CONSUMO_MENSAL, PERIODICIDADE_JUROS,
						SEGMENTO, FISCAL, VENDEDOR);

				validarRemocao(proposta);
				model.addAttribute(PROPOSTA_LEVANTAMENTO_MERCADO, "true");
				this.carregarCampos(model, propostaVO.getChavePrimariaLM());
			} else {
				proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria(), SITUACAO_PROPOSTA,
						TARIFA, UNIDADE_CONSUMO_ANUAL, UNIDADE_CONSUMO_MENSAL, PERIODICIDADE_JUROS, SEGMENTO, FISCAL,
						VENDEDOR);
				validarRemocao(proposta);
				this.carregarCampos(model, null);
			}
			this.popularForm(propostaVO, request, proposta, model);
			model.addAttribute(PROPOSTA_VO, propostaVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarProposta(propostaVO, result, request, model);
		}

		saveToken(request);
		return view;

	}

	/**
	 * Método responsável por alterar uma proposta.
	 * 
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param arquivoForm
	 *            {@link MultipartFile}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@RequestMapping("alterarProposta")
	public String alterarProposta(PropostaVO propostaVO, BindingResult result,
			@RequestParam(value = "planilhaImportacao") MultipartFile arquivoForm, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException, IOException {

		String view = "exibirAlteracaoProposta";

		try {
			Boolean indicadorNovaProposta = Boolean.valueOf(request.getParameter("indicadorNovaProposta"));
			this.carregarCampos(model, null);
			if (propostaVO.getIdCliente() != null) {
				model.addAttribute(CLIENTE, controladorCliente.obter(propostaVO.getIdCliente()));
			}
			if (propostaVO.getIdImovel() != null) {
				model.addAttribute(IMOVEL, controladorImovel.obter(propostaVO.getIdImovel()));
			}
			validarToken(request);
			if (indicadorNovaProposta) {
				Proposta novaProposta = (Proposta) controladorProposta.criar();
				popularProposta(novaProposta, propostaVO, arquivoForm);
				novaProposta.setDadosAuditoria(getDadosAuditoria(request));
				validarConsumosMedios(novaProposta, propostaVO);
				controladorProposta.inserir(novaProposta);
			} else {
				Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria(),
						SITUACAO_PROPOSTA, TARIFA, UNIDADE_CONSUMO_ANUAL, UNIDADE_CONSUMO_MENSAL, PERIODICIDADE_JUROS);
				popularProposta(proposta, propostaVO, arquivoForm);
				proposta.setDadosAuditoria(getDadosAuditoria(request));
				validarConsumosMedios(proposta, propostaVO);
				controladorProposta.atualizar(proposta);
			}
			propostaVO.setNumeroProposta(null);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Proposta.PROPOSTA_CAMPO_STRING);
			view = pesquisarProposta(propostaVO, result, request, model);
			request.getSession().removeAttribute(PLANILHA_IMPORTACAO);

			if (propostaVO.getPropostaLevantamentoMercado() != null
					&& "true".equals(propostaVO.getPropostaLevantamentoMercado())) {
				view = "redirect:exibirPesquisaLevantamentoMercado";
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por remover uma proposta.
	 * 
	 * @throws NegocioException
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("removerProposta")
	public String removerProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		try {
			controladorProposta.validarRemoverPropostas(propostaVO.getChavesPrimarias());
			controladorProposta.remover(propostaVO.getChavesPrimarias(), getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Proposta.PROPOSTA_CAMPO_STRING);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return pesquisarProposta(propostaVO, result, request, model);
	}

	/**
	 * Método responsável por importar dados de um arquivo excel.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param arquivoForm
	 *            {@link MultipartFile}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("importarDadosProposta")
	public String importarDadosProposta(PropostaVO propostaVO, BindingResult result,
			@RequestParam(value = "planilhaImportacao") MultipartFile arquivoForm, HttpServletRequest request,
			Model model) throws GGASException, IOException {

		String view = null;
		model.addAttribute(PROPOSTA_VO, propostaVO);
		manterIndicadorPesquisa(propostaVO, model);
		try {
			if (arquivoForm != null) {
				Map<String, Object> dadosImportados = controladorProposta.importarDadosPropostaExcel(
						arquivoForm.getBytes(), arquivoForm.getContentType(), arquivoForm.getOriginalFilename());
				model.addAllAttributes(dadosImportados);
				preencherComDadosPlanilha(propostaVO, dadosImportados);
				request.getSession().setAttribute(PLANILHA_IMPORTACAO, arquivoForm);
				mensagemSucesso(model, Constantes.SUCESSO_IMPORTACAO, request, Proposta.PROPOSTA_CAMPO_STRING);
			}

			this.carregarCampos(model, null);
			model.addAttribute(ATRIBUTO_ABA_ID, ABA_IDENTIFICACAO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if ("inclusao".equals(String.valueOf(request.getParameter("tela")))) {
			view = exibirInclusaoProposta(propostaVO, result, request, model);
		} else {
			view = exibirAlteracaoProposta(propostaVO, result, request, model);
		}

		return view;
	}

	private void preencherComDadosPlanilha(PropostaVO propostaVO, Map<String, Object> dadosImportados) {

		if (dadosImportados.containsKey(LISTA_TIPO_COMBUSTIVEL_LM)) {
			propostaVO.setNumeroProposta(dadosImportados.get(LISTA_TIPO_COMBUSTIVEL_LM).toString());
		}
		if (dadosImportados.containsKey("percentualTIR")) {
			propostaVO.setPercentualTIR(dadosImportados.get("percentualTIR").toString());
		}
		if (dadosImportados.containsKey("valorCliente")) {
			propostaVO.setValorCliente(dadosImportados.get("valorCliente").toString());
		}
		if (dadosImportados.containsKey("consumoMedioMensal")) {
			propostaVO.setConsumoMedioMensal(dadosImportados.get("consumoMedioMensal").toString());
		}
		if (dadosImportados.containsKey("percentualJuros")) {
			propostaVO.setPercentualJuros(dadosImportados.get("percentualJuros").toString());
		}
		if (dadosImportados.containsKey("valorParcela")) {
			propostaVO.setValorParcela(dadosImportados.get("valorParcela").toString());
		}
		if (dadosImportados.containsKey("valorInvestimento")) {
			propostaVO.setValorInvestimento(dadosImportados.get("valorInvestimento").toString());
		}
		if (dadosImportados.containsKey("economiaAnualGN")) {
			propostaVO.setEconomiaAnualGN(dadosImportados.get("economiaAnualGN").toString());
		}
		if (dadosImportados.containsKey("economiaMensalGN")) {
			propostaVO.setEconomiaMensalGN(dadosImportados.get("economiaMensalGN").toString());
		}
		if (dadosImportados.containsKey("percentualEconomia")) {
			propostaVO.setPercentualEconomia(dadosImportados.get("percentualEconomia").toString());
		}
		if (dadosImportados.containsKey("valorMaoDeObra")) {
			propostaVO.setValorMaoDeObra(dadosImportados.get("valorMaoDeObra").toString());
		}
		if (dadosImportados.containsKey("valorMaterial")) {
			propostaVO.setValorMaterial(dadosImportados.get("valorMaterial").toString());
		}
		if (dadosImportados.containsKey("dataEmissao")) {
			propostaVO.setDataEmissao(dadosImportados.get("dataEmissao").toString());
		}
		if (dadosImportados.containsKey("quantidadeParcela")) {
			propostaVO.setQuantidadeParcela(dadosImportados.get("quantidadeParcela").toString());
		}
		if (dadosImportados.containsKey("valorGastoMensalGN")) {
			propostaVO.setValorGastoMensalGN(dadosImportados.get("valorGastoMensalGN").toString());
		}
	}

	/**
	 * Método responsável por efetutar o download da planilha excel.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("efetuarDownloadPlanilha")
	public String efetuarDownloadPlanilha(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		try {
			if (propostaVO.getChavePrimaria() != null && propostaVO.getChavePrimaria() > 0) {
				Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria());
				if (proposta != null) {
					byte[] bytes = proposta.getPlanilha();
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType(proposta.getTipoPlanilha());
					response.setContentLength(bytes.length);
					response.addHeader(CONTENT_DISPOSITION,
							ATTACHMENT_FILENAME + proposta.getNomePlanilha().replaceAll(" ", "_"));
					servletOutputStream.write(bytes, 0, bytes.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				}
			} else {
				MultipartFile arquivoForm = (MultipartFile) request.getSession().getAttribute(PLANILHA_IMPORTACAO);
				if (arquivoForm != null) {
					byte[] bytes = arquivoForm.getBytes();
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType(arquivoForm.getContentType());
					response.setContentLength(bytes.length);
					response.addHeader(CONTENT_DISPOSITION,
							ATTACHMENT_FILENAME + arquivoForm.getOriginalFilename().replaceAll(" ", "_"));
					servletOutputStream.write(bytes, 0, bytes.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				}
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return null;
	}

	/**
	 * Popular form lm.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularFormLM(PropostaVO propostaVO, Model model) throws GGASException {

		ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
				.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
		LevantamentoMercado levMercado = controladorLevantamentoMercado
				.obterLevantamentoMercado(propostaVO.getChavePrimariaLM());
		long idModalidadeUso = 0;

		if (levMercado != null) {
			model.addAttribute(IMOVEL, levMercado.getImovel());
			model.addAttribute(PROPOSTA_LEVANTAMENTO_MERCADO, "true");
			model.addAttribute(FLUXO_LEVANTAMENTO_MERCADO, "true");
			if (!CollectionUtils.isEmpty(levMercado.getListaTipoCombustivel())) {
				List<LevantamentoMercadoTipoCombustivel> levaList = new ArrayList<LevantamentoMercadoTipoCombustivel>();
				for (LevantamentoMercadoTipoCombustivel levCombustivel : levMercado.getListaTipoCombustivel()) {
					if (levCombustivel.getTipoCombustivel() != null) {
						levaList.add(levCombustivel);
					}
					if (levCombustivel.getModalidadeUso() != null) {
						idModalidadeUso = levCombustivel.getModalidadeUso().getChavePrimaria();
					}
				}
				model.addAttribute(LISTA_TIPO_COMBUSTIVEL_LM, levaList);
			}

			model.addAttribute(ID_MODALIDADE_USO, idModalidadeUso);
			Map<String, BigDecimal> valores = controladorProposta.calcularValoresProposta(levMercado);
			if (valores.get(CONSUMO_MEDIO_ANUAL_TOTAL) != null) {
				propostaVO
						.setConsumoMedioAnual(Util.converterCampoValorParaString(valores.get(CONSUMO_MEDIO_ANUAL_TOTAL),
								Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
			}

			if (valores.get(CONSUMO_MEDIO_MENSAL_TOTAL) != null) {
				propostaVO.setConsumoMedioMensal(
						Util.converterCampoValorParaString(valores.get(CONSUMO_MEDIO_MENSAL_TOTAL),
								Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
			}

			if (valores.get(VALOR_MENSAL_TOTAL) != null) {
				propostaVO.setValorGastoMensal(Util.converterCampoValorParaString(valores.get(VALOR_MENSAL_TOTAL),
						Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO));
			}
		}
	}

	/**
	 * Método responsável gerar o PDF da proposta.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("gerarPropostaPDF")
	public String gerarPropostaPDF(PropostaVO propostaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(PROPOSTA_VO, propostaVO);
		try {
			Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria());
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put(PROPOSTA, proposta);
			byte[] relatorio = controladorProposta.gerarPropostaPDF(parametros);

			if (relatorio != null) {

				request.getSession().setAttribute(RELATORIO_PROPOSTA, relatorio);
				request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(PDF));
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return exibirPesquisaProposta(request, model);
	}

	/**
	 * Método responsável por exibir o relatório na tela.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("exibirRelatorioProposta")
	public String exibirRelatorioProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		byte[] relatorioProposta = (byte[]) request.getSession().getAttribute(RELATORIO_PROPOSTA);
		FormatoImpressao formatoImpressao = (FormatoImpressao) request.getSession().getAttribute(FORMATO_IMPRESSAO);

		request.getSession().removeAttribute(RELATORIO_PROPOSTA);
		request.getSession().removeAttribute(FORMATO_IMPRESSAO);

		if (relatorioProposta != null) {
			Date dataAtual = Calendar.getInstance().getTime();

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual);
			int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			int minutes = Calendar.getInstance().get(Calendar.MINUTE);
			int seconds = Calendar.getInstance().get(Calendar.SECOND);
			Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			Calendar.getInstance().get(Calendar.MINUTE);
			Calendar.getInstance().get(Calendar.SECOND);

			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorioProposta.length);
			response.addHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + "Proposta" + "_" + data + "_" + hours
					+ minutes + seconds + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorioProposta, 0, relatorioProposta.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.util.DispatchAction#obterFormatoRelatorio(br.com.ggas.util.
	 * FormatoImpressao)
	 */
	@Override
	protected String obterFormatoRelatorio(FormatoImpressao formatoImpressao) {

		String retorno = null;

		if (formatoImpressao.equals(FormatoImpressao.PDF)) {
			retorno = ".pdf";
		} else if (formatoImpressao.equals(FormatoImpressao.XLS)) {
			retorno = ".xls";
		} else if (formatoImpressao.equals(FormatoImpressao.RTF)) {
			retorno = ".rtf";
		}

		return retorno;
	}

	/**
	 * Método responsável por enviar a Proposta por Email.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("enviarPropostaPorEmail")
	public String enviarPropostaPorEmail(PropostaVO propostaVO, BindingResult result, HttpServletRequest request,
			Model model) throws NegocioException {

		try {
			Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria());
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put(PROPOSTA, proposta);
			byte[] relatorio = controladorProposta.gerarPropostaPDF(parametros);

			controladorProposta.enviarPropostaPorEmail(proposta, relatorio);

			mensagemSucesso(model, Constantes.SUCESSO_ENVIO_EMAIL);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarProposta(propostaVO, result, request, model);
	}

	/**
	 * Validar contato imovel.
	 * 
	 * @param imovel
	 *            {@link Imovel}
	 * @param validarEmail
	 *            {@link boolean}
	 * @throws Exception
	 *             {@link Exception}
	 */
	private void validarContatoImovel(Imovel imovel, boolean validarEmail) throws Exception {

		Imovel imovelSelecionado = (Imovel) controladorImovel.obter(imovel.getChavePrimaria());

		if (imovelSelecionado.getContatos() == null || imovelSelecionado.getContatos().isEmpty()) {
			throw new NegocioException(Constantes.ERRO_CONTATO_IMOVEL_OBRIGATORIO, true);
		} else {
			if (!imovelSelecionado.getContatos().isEmpty() && validarEmail) {
				ContatoImovel contato = null;
				for (ContatoImovel contatoImovel : imovelSelecionado.getContatos()) {
					if (contatoImovel.isPrincipal()) {
						contato = contatoImovel;
					}
				}
				if (contato != null && contato.getEmail() == null) {
					throw new NegocioException(Constantes.ERRO_EMAIL_CONTATO_IMOVEL, true);
				}
			}
		}
	}

	/**
	 * Vincular proposta levantamento mercado.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param proposta
	 *            {@link Proposta}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void vincularPropostaLevantamentoMercado(PropostaVO propostaVO, Proposta proposta) throws GGASException {

		if (propostaVO.getPropostaLevantamentoMercado() != null
				&& (propostaVO.getChavePrimariaLM() != null && propostaVO.getChavePrimariaLM() > 0)
				&& "true".equals(propostaVO.getPropostaLevantamentoMercado())) {

			ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
					.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
			LevantamentoMercado levMercado = controladorLevantamentoMercado
					.obterLevantamentoMercado(propostaVO.getChavePrimariaLM());
			levMercado.setProposta(proposta);
			atualizarStatusProposta(null, levMercado, Boolean.TRUE, Boolean.FALSE);
			controladorLevantamentoMercado.atualizarLevantamentoMercado(levMercado);
		}
	}

	/**
	 * Enviar email.
	 * 
	 * @param proposta
	 *            {@link Proposta}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param relatorio
	 *            {@link byte}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void enviarEmail(Proposta proposta, HttpServletRequest request, byte[] relatorio, Model model)
			throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put(PROPOSTA, proposta);

		if (relatorio != null) {
			request.getSession().setAttribute(RELATORIO_PROPOSTA, relatorio);
			request.getSession().setAttribute(FORMATO_IMPRESSAO, FormatoImpressao.valueOf(PDF));
			model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);
		}

		String valorParametro = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.ENVIAR_EMAIL_AO_SALVAR_PROPOSTA).getValor();
		if (valorParametro.equals(ENVIO_EMAIL_ATIVO)) {
			controladorProposta.enviarPropostaPorEmail(proposta, relatorio);
		}
	}

	/**
	 * Método responsável por alterar a situação da proposta para Rejeitada.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("rejeitarProposta")
	public String rejeitarProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria());
			String situacaoRejeitadaDescricao = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_PROPOSTA_SITUACAO_REJEITADA);
			SituacaoProposta situacaoProposta = controladorProposta
					.obterSituacaoPropostaPorDescricao(situacaoRejeitadaDescricao);
			verificarPropostaAlteracao(proposta);

			if (situacaoProposta != null) {
				proposta.setSituacaoProposta(situacaoProposta);
				controladorProposta.atualizar(proposta);
				mensagemSucesso(model, Constantes.SUCESSO_REJEITADA, Proposta.PROPOSTA_CAMPO_STRING);
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_PROPOSTA_NAO_REJEITADA, true);
			}

			atualizarStatusProposta(propostaVO.getChavePrimaria(), null, Boolean.FALSE, Boolean.TRUE);

		} catch (NegocioException | ConcorrenciaException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarProposta(propostaVO, result, request, model);
	}

	/**
	 * Método responsável por alterar a situação da proposta para APROVADA.
	 * 
	 * @param propostaVO
	 *            {@link PropostaVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * @throws ConcorrenciaException
	 *             {@link ConcorrenciaException}
	 */
	@RequestMapping("aprovarProposta")
	public String aprovarProposta(PropostaVO propostaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException, ConcorrenciaException {

		try {
			Proposta proposta = (Proposta) controladorProposta.obter(propostaVO.getChavePrimaria());
			if (proposta != null) {
				verificarPropostaAlteracao(proposta);
				String situacaoAprovadaDescricao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PROPOSTA_SITUACAO_APROVADA);
				SituacaoProposta situacaoProposta = controladorProposta
						.obterSituacaoPropostaPorDescricao(situacaoAprovadaDescricao);
				if (situacaoProposta != null) {
					proposta.setSituacaoProposta(situacaoProposta);
					if (propostaVO.getIdSituacaoImovel() != null && propostaVO.getIdSituacaoImovel() > 0) {
						SituacaoImovel situacaoImovel = controladorImovel
								.obterSituacaoImovel(propostaVO.getIdSituacaoImovel());
						if (situacaoImovel != null) {
							proposta.getImovel().setSituacaoImovel(situacaoImovel);
						}
					}
					controladorProposta.atualizar(proposta);
					mensagemSucesso(model, Constantes.SUCESSO_APROVADA, request, Proposta.PROPOSTA_CAMPO_STRING);
				}
			}
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return pesquisarProposta(propostaVO, result, request, model);
	}

	/**
	 * Popular dados detalhamento.
	 * 
	 * @param propostaVo
	 *            {@link PropostaVO}
	 * @param proposta
	 *            {@link Proposta}
	 * @param model
	 *            {@link Model}
	 */
	private void popularDadosDetalhamento(PropostaVO propostaVo, Proposta proposta, Model model) {

		if (proposta.getValorMensalGastoGN() != null) {
			propostaVo.setValorGastoMensalGN(Util.converterCampoValorDecimalParaString("Valor Mensal Gasto",
					proposta.getValorMensalGastoGN(), Constantes.LOCALE_PADRAO));
		}

		if (proposta.getTarifa() != null) {
			propostaVo.setDescricaoTarifa(proposta.getTarifa().getDescricaoAbreviada());
		}

		if (proposta.getValorMedioGN() != null) {
			propostaVo.setValorMedioGN(Util.converterCampoValorDecimalParaString(VALOR_MÉDIO_GN,
					proposta.getValorMedioGN(), Constantes.LOCALE_PADRAO));
		}

		if (proposta.getImovel() != null) {
			model.addAttribute(IMOVEL, proposta.getImovel());

			if (proposta.getQuantidadeApartamentos() != null) {
				propostaVo.setNumeroAptoLojas(proposta.getQuantidadeApartamentos().toString());
			}

		}

		if (proposta.getPercentualEconomia() != null) {
			propostaVo.setPercentualEconomia(proposta.getPercentualEconomia().multiply(new BigDecimal(100)).toString());
		}

	}

	/**
	 * Verificar proposta alteracao.
	 * 
	 * @param proposta
	 *            {@link Proposta}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void verificarPropostaAlteracao(Proposta proposta) throws NegocioException {

		if (proposta.getSituacaoProposta() != null
				&& (proposta.getSituacaoProposta().getDescricao().equalsIgnoreCase(SITUACAO_PROPOSTA_APROVADA)
						|| proposta.getSituacaoProposta().getDescricao().equalsIgnoreCase(SITUACAO_PROPOSTA_REJEITADA))
				|| !proposta.isHabilitado()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA, true);
		}
	}

	/**
	 * Atualizar status proposta.
	 * 
	 * @param chaveProposta
	 *            {@link Long}
	 * @param levantamentoMercado
	 *            {@link LevantamentoMercado}
	 * @param propostaEnviada
	 *            {@link boolean}
	 * @param propostaRejeitada
	 *            {@link boolean}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void atualizarStatusProposta(Long chaveProposta, LevantamentoMercado levantamentoMercado,
			boolean propostaEnviada, boolean propostaRejeitada) throws GGASException {

		if (propostaEnviada) {
			EntidadeConteudo status = controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_LEVANTAMENTO_MERCADO_PROPOSTA_ENVIADA).getValor()));
			levantamentoMercado.setStatus(status);
		}

		if (propostaRejeitada && chaveProposta != null) {
			ControladorLevantamentoMercado controladorLevantamentoMercado = (ControladorLevantamentoMercado) ServiceLocator
					.getInstancia().getBeanPorID(CONTROLADOR_LEVANTAMENTO_MERCADO);
			LevantamentoMercado levantamento = controladorLevantamentoMercado
					.obterLevantamentoMercadoPorChaveProposta(chaveProposta);
			if (levantamento != null) {
				EntidadeConteudo status = controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_LEVANTAMENTO_MERCADO_ENCERRADO).getValor()));
				levantamento.setStatus(status);
				controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamento);
			}
		}
	}

	/**
	 * Validar remocao.
	 * 
	 * @param proposta
	 *            {@link Proposta}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void validarRemocao(Proposta proposta) throws NegocioException {

		if (proposta.getSituacaoProposta() != null && proposta.getSituacaoProposta().getDescricao() != null
				&& SITUACAO_PROPOSTA_APROVADA.equalsIgnoreCase(proposta.getSituacaoProposta().getDescricao())
				|| SITUACAO_PROPOSTA_REJEITADA.equalsIgnoreCase(proposta.getSituacaoProposta().getDescricao())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA, true);
		}
	}
	
	@RequestMapping("cancelarAcaoProposta")
	public String cancelar(HttpServletRequest request, Model model) throws NegocioException {
		
		PropostaVO propostaVO = new PropostaVO();
		if(super.retornaPesquisa(request, PROPOSTA_VO) != null) {
			propostaVO = (PropostaVO) super.retornaPesquisa(request, PROPOSTA_VO);
			request.getSession().removeAttribute(PROPOSTA_VO);
			model.addAttribute(PROPOSTA_VO, propostaVO);
		}
		
		return pesquisarProposta(propostaVO, null, request, model);
	}
}
