package br.com.ggas.web.contrato.proposta;

import java.io.Serializable;

/**
 * Classe VO responsável pela transferência de dados entre os métodos
 * relacionados às telas de Proposta.
 *
 * @author pedro
 */
public class PropostaVO implements Serializable {

	private static final long serialVersionUID = 7489543199361915778L;
	private Integer codigoModalidadeMedicao;
	private Long idImovel;
	private Long idTarifa;
	private Long idFiscal;
	private Long idCliente;
	private Long idSegmento;
	private Long idVendedor;
	private Long chavePrimaria;
	private Long idModalidadeUso;
	private Long chavePrimariaLM;
	private Long idSituacaoImovel;
	private Long idSituacaoProposta;
	private Long idPeriodicidadeJuros;
	private Long idUnidadeConsumoAnual;
	private Long idUnidadeConsumoMensal;
	private Long[] chavesPrimarias;
	private String versao;
	private String dataEntrega;
	private String dataEmissao;
	private String dataVigencia;
	private String percentualTIR;
	private String valorMaterial;
	private String versaoProposta;
	private String numeroProposta;
	private String valorMaoDeObra;
	private String valorInvestimento;
	private String percentualCliente;
	private String quantidadeParcela;
	private String valorCliente;
	private String valorParcela;
	private String percentualJuros;
	private String valorGastoMensal;
	private String consumoMedioAnual;
	private String consumoMedioMensal;
	private String anoMesReferenciaPreco;
	private String consumoUnidadeConsumidora;
	private String nomeFiscal;
	private String nomeVendedor;
	private String valorMedioGN;
	private String nomePlanilha;
	private String descricaoTarifa;
	private String economiaAnualGN;
	private String economiaMensalGN;
	private String indicadorPesquisa;
	private String descricaoSegmento;
	private String percentualEconomia;
	private String dataEleicaoSindico;
	private String comentarioAdicional;
	private String consumoMedioAnualGN;
	private String consumoMedioMensalGN;
	private String volumeDiarioEstimadoGN;
	private String numeroAptoLojas;
	private String valorGastoMensalGN;
	private String valorParticipacaoCDL;
	private String versaoPropostaAnterior;
	private String dataAssembleiaCondominio;
	private String descricaoSituacaoProposta;
	private String propostaLevantamentoMercado;
	private String descricaoUnidadeConsumoAnual;
	private String descricaoUnidadeConsumoMensal;
	private String periodicidadeJuros;
	private Boolean habilitado;
	private Boolean indicadorMedicao;
	private Boolean indicadorNovaVersao;
	private Boolean indicadorParticipacao;

	public Integer getCodigoModalidadeMedicao() {
		return codigoModalidadeMedicao;
	}

	public void setCodigoModalidadeMedicao(Integer codigoModalidadeMedicao) {
		this.codigoModalidadeMedicao = codigoModalidadeMedicao;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public Long getIdTarifa() {
		return idTarifa;
	}

	public void setIdTarifa(Long idTarifa) {
		this.idTarifa = idTarifa;
	}

	public Long getIdFiscal() {
		return idFiscal;
	}

	public void setIdFiscal(Long idFiscal) {
		this.idFiscal = idFiscal;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdSegmento() {
		return idSegmento;
	}

	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	public Long getIdVendedor() {
		return idVendedor;
	}

	public void setIdVendedor(Long idVendedor) {
		this.idVendedor = idVendedor;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Long getIdModalidadeUso() {
		return idModalidadeUso;
	}

	public void setIdModalidadeUso(Long idModalidadeUso) {
		this.idModalidadeUso = idModalidadeUso;
	}

	public Long getChavePrimariaLM() {
		return chavePrimariaLM;
	}

	public void setChavePrimariaLM(Long chavePrimariaLM) {
		this.chavePrimariaLM = chavePrimariaLM;
	}

	public Long getIdSituacaoImovel() {
		return idSituacaoImovel;
	}

	public void setIdSituacaoImovel(Long idSituacaoImovel) {
		this.idSituacaoImovel = idSituacaoImovel;
	}

	public Long getIdSituacaoProposta() {
		return idSituacaoProposta;
	}

	public void setIdSituacaoProposta(Long idSituacaoProposta) {
		this.idSituacaoProposta = idSituacaoProposta;
	}

	public Long getIdPeriodicidadeJuros() {
		return idPeriodicidadeJuros;
	}

	public void setIdPeriodicidadeJuros(Long idPeriodicidadeJuros) {
		this.idPeriodicidadeJuros = idPeriodicidadeJuros;
	}

	public Long getIdUnidadeConsumoAnual() {
		return idUnidadeConsumoAnual;
	}

	public void setIdUnidadeConsumoAnual(Long idUnidadeConsumoAnual) {
		this.idUnidadeConsumoAnual = idUnidadeConsumoAnual;
	}

	public Long getIdUnidadeConsumoMensal() {
		return idUnidadeConsumoMensal;
	}

	public void setIdUnidadeConsumoMensal(Long idUnidadeConsumoMensal) {
		this.idUnidadeConsumoMensal = idUnidadeConsumoMensal;
	}

	public Long[] getChavesPrimarias() {
		Long[] chaves = null;
		if (this.chavesPrimarias != null) {
			chaves = chavesPrimarias.clone();
		}
		return chaves;
	}

	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(String dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getDataVigencia() {
		return dataVigencia;
	}

	public void setDataVigencia(String dataVigencia) {
		this.dataVigencia = dataVigencia;
	}

	public String getPercentualTIR() {
		return percentualTIR;
	}

	public void setPercentualTIR(String percentualTIR) {
		this.percentualTIR = percentualTIR;
	}

	public String getValorMaterial() {
		return valorMaterial;
	}

	public void setValorMaterial(String valorMaterial) {
		this.valorMaterial = valorMaterial;
	}

	public String getVersaoProposta() {
		return versaoProposta;
	}

	public void setVersaoProposta(String versaoProposta) {
		this.versaoProposta = versaoProposta;
	}

	public String getNumeroProposta() {
		return numeroProposta;
	}

	public void setNumeroProposta(String numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

	public String getValorMaoDeObra() {
		return valorMaoDeObra;
	}

	public void setValorMaoDeObra(String valorMaoDeObra) {
		this.valorMaoDeObra = valorMaoDeObra;
	}

	public String getValorInvestimento() {
		return valorInvestimento;
	}

	public void setValorInvestimento(String valorInvestimento) {
		this.valorInvestimento = valorInvestimento;
	}

	public String getPercentualCliente() {
		return percentualCliente;
	}

	public void setPercentualCliente(String percentualCliente) {
		this.percentualCliente = percentualCliente;
	}

	public String getQuantidadeParcela() {
		return quantidadeParcela;
	}

	public void setQuantidadeParcela(String quantidadeParcela) {
		this.quantidadeParcela = quantidadeParcela;
	}

	public String getValorCliente() {
		return valorCliente;
	}

	public void setValorCliente(String valorCliente) {
		this.valorCliente = valorCliente;
	}

	public String getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(String valorParcela) {
		this.valorParcela = valorParcela;
	}

	public String getPercentualJuros() {
		return percentualJuros;
	}

	public void setPercentualJuros(String percentualJuros) {
		this.percentualJuros = percentualJuros;
	}

	public String getValorGastoMensal() {
		return valorGastoMensal;
	}

	public void setValorGastoMensal(String valorGastoMensal) {
		this.valorGastoMensal = valorGastoMensal;
	}

	public String getConsumoMedioAnual() {
		return consumoMedioAnual;
	}

	public void setConsumoMedioAnual(String consumoMedioAnual) {
		this.consumoMedioAnual = consumoMedioAnual;
	}

	public String getConsumoMedioMensal() {
		return consumoMedioMensal;
	}

	public void setConsumoMedioMensal(String consumoMedioMensal) {
		this.consumoMedioMensal = consumoMedioMensal;
	}

	public String getAnoMesReferenciaPreco() {
		return anoMesReferenciaPreco;
	}

	public void setAnoMesReferenciaPreco(String anoMesReferenciaPreco) {
		this.anoMesReferenciaPreco = anoMesReferenciaPreco;
	}

	public String getConsumoUnidadeConsumidora() {
		return consumoUnidadeConsumidora;
	}

	public void setConsumoUnidadeConsumidora(String consumoUnidadeConsumidora) {
		this.consumoUnidadeConsumidora = consumoUnidadeConsumidora;
	}

	public String getNomeFiscal() {
		return nomeFiscal;
	}

	public void setNomeFiscal(String nomeFiscal) {
		this.nomeFiscal = nomeFiscal;
	}

	public String getNomeVendedor() {
		return nomeVendedor;
	}

	public void setNomeVendedor(String nomeVendedor) {
		this.nomeVendedor = nomeVendedor;
	}

	public String getValorMedioGN() {
		return valorMedioGN;
	}

	public void setValorMedioGN(String valorMedioGN) {
		this.valorMedioGN = valorMedioGN;
	}

	public String getNomePlanilha() {
		return nomePlanilha;
	}

	public void setNomePlanilha(String nomePlanilha) {
		this.nomePlanilha = nomePlanilha;
	}

	public String getDescricaoTarifa() {
		return descricaoTarifa;
	}

	public void setDescricaoTarifa(String descricaoTarifa) {
		this.descricaoTarifa = descricaoTarifa;
	}

	public String getEconomiaAnualGN() {
		return economiaAnualGN;
	}

	public void setEconomiaAnualGN(String economiaAnualGN) {
		this.economiaAnualGN = economiaAnualGN;
	}

	public String getEconomiaMensalGN() {
		return economiaMensalGN;
	}

	public void setEconomiaMensalGN(String economiaMensalGN) {
		this.economiaMensalGN = economiaMensalGN;
	}

	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

	public String getDescricaoSegmento() {
		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {
		this.descricaoSegmento = descricaoSegmento;
	}

	public String getPercentualEconomia() {
		return percentualEconomia;
	}

	public void setPercentualEconomia(String percentualEconomia) {
		this.percentualEconomia = percentualEconomia;
	}

	public String getDataEleicaoSindico() {
		return dataEleicaoSindico;
	}

	public void setDataEleicaoSindico(String dataEleicaoSindico) {
		this.dataEleicaoSindico = dataEleicaoSindico;
	}

	public String getComentarioAdicional() {
		return comentarioAdicional;
	}

	public void setComentarioAdicional(String comentarioAdicional) {
		this.comentarioAdicional = comentarioAdicional;
	}

	public String getConsumoMedioAnualGN() {
		return consumoMedioAnualGN;
	}

	public void setConsumoMedioAnualGN(String consumoMedioAnualGN) {
		this.consumoMedioAnualGN = consumoMedioAnualGN;
	}

	public String getConsumoMedioMensalGN() {
		return consumoMedioMensalGN;
	}

	public void setConsumoMedioMensalGN(String consumoMedioMensalGN) {
		this.consumoMedioMensalGN = consumoMedioMensalGN;
	}

	public String getVolumeDiarioEstimadoGN() {
		return volumeDiarioEstimadoGN;
	}

	public void setVolumeDiarioEstimadoGN(String volumeDiarioEstimadoGN) {
		this.volumeDiarioEstimadoGN = volumeDiarioEstimadoGN;
	}

	public String getNumeroAptoLojas() {
		return numeroAptoLojas;
	}

	public void setNumeroAptoLojas(String numeroAptoLojas) {
		this.numeroAptoLojas = numeroAptoLojas;
	}

	public String getValorGastoMensalGN() {
		return valorGastoMensalGN;
	}

	public void setValorGastoMensalGN(String valorGastoMensalGN) {
		this.valorGastoMensalGN = valorGastoMensalGN;
	}

	public String getValorParticipacaoCDL() {
		return valorParticipacaoCDL;
	}

	public void setValorParticipacaoCDL(String valorParticipacaoCDL) {
		this.valorParticipacaoCDL = valorParticipacaoCDL;
	}

	public String getVersaoPropostaAnterior() {
		return versaoPropostaAnterior;
	}

	public void setVersaoPropostaAnterior(String versaoPropostaAnterior) {
		this.versaoPropostaAnterior = versaoPropostaAnterior;
	}

	public String getDataAssembleiaCondominio() {
		return dataAssembleiaCondominio;
	}

	public void setDataAssembleiaCondominio(String dataAssembleiaCondominio) {
		this.dataAssembleiaCondominio = dataAssembleiaCondominio;
	}

	public String getDescricaoSituacaoProposta() {
		return descricaoSituacaoProposta;
	}

	public void setDescricaoSituacaoProposta(String descricaoSituacaoProposta) {
		this.descricaoSituacaoProposta = descricaoSituacaoProposta;
	}

	public String getPropostaLevantamentoMercado() {
		return propostaLevantamentoMercado;
	}

	public void setPropostaLevantamentoMercado(String propostaLevantamentoMercado) {
		this.propostaLevantamentoMercado = propostaLevantamentoMercado;
	}

	public String getDescricaoUnidadeConsumoAnual() {
		return descricaoUnidadeConsumoAnual;
	}

	public void setDescricaoUnidadeConsumoAnual(String descricaoUnidadeConsumoAnual) {
		this.descricaoUnidadeConsumoAnual = descricaoUnidadeConsumoAnual;
	}

	public String getDescricaoUnidadeConsumoMensal() {
		return descricaoUnidadeConsumoMensal;
	}

	public void setDescricaoUnidadeConsumoMensal(String descricaoUnidadeConsumoMensal) {
		this.descricaoUnidadeConsumoMensal = descricaoUnidadeConsumoMensal;
	}
	public String getPeriodicidadeJuros() {
		return periodicidadeJuros;
	}

	public void setPeriodicidadeJuros(String periodicidadeJuros) {
		this.periodicidadeJuros = periodicidadeJuros;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Boolean getIndicadorMedicao() {
		return indicadorMedicao;
	}

	public void setIndicadorMedicao(Boolean indicadorMedicao) {
		this.indicadorMedicao = indicadorMedicao;
	}

	public Boolean getIndicadorNovaVersao() {
		return indicadorNovaVersao;
	}

	public void setIndicadorNovaVersao(Boolean indicadorNovaVersao) {
		this.indicadorNovaVersao = indicadorNovaVersao;
	}

	public Boolean getIndicadorParticipacao() {
		return indicadorParticipacao;
	}

	public void setIndicadorParticipacao(Boolean indicadorParticipacao) {
		this.indicadorParticipacao = indicadorParticipacao;
	}
}
