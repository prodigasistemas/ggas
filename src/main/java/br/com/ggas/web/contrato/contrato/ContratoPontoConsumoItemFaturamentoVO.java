/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.rota.Periodicidade;

public class ContratoPontoConsumoItemFaturamentoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7526953177404753960L;

	private long chavePrimariaItemFatura;

	private Integer numeroDiaVencimento;

	private EntidadeConteudo itemFatura;

	private EntidadeConteudo faseReferencia;

	private EntidadeConteudo opcaoFaseReferencia;

	private Boolean vencimentoDiaUtil;

	private Periodicidade periodicidade;

	private Tarifa tarifa;

	private Boolean indicadorDebitoAutomatico;

	private Boolean indicadorDepositoIdentificado;

	private EntidadeConteudo dataReferenciaCambial;

	private EntidadeConteudo diaCotacao;

	private EntidadeConteudo contratoCompra;

	private Date dataInicialAssocTempTarifaPontoConsumo;

	private Date dataFinalAssocTempTarifaPontoConsumo;

	private BigDecimal percminimoQDC;

	private Map<String, Object> dadosItemFaturamento = new HashMap<String, Object>();

	/**
	 * @return the numeroDiaVencimento
	 */
	public Integer getNumeroDiaVencimento() {

		return numeroDiaVencimento;
	}

	/**
	 * @param numeroDiaVencimento
	 *            the numeroDiaVencimento to set
	 */
	public void setNumeroDiaVencimento(Integer numeroDiaVencimento) {

		this.numeroDiaVencimento = numeroDiaVencimento;
	}

	/**
	 * @return the itemFatura
	 */
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/**
	 * @param itemFatura
	 *            the itemFatura to set
	 */
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	/**
	 * @return the faseReferencia
	 */
	public EntidadeConteudo getFaseReferencia() {

		return faseReferencia;
	}

	/**
	 * @param faseReferencia
	 *            the faseReferencia to set
	 */
	public void setFaseReferencia(EntidadeConteudo faseReferencia) {

		this.faseReferencia = faseReferencia;
	}

	/**
	 * @return the opcaoFaseReferencia
	 */
	public EntidadeConteudo getOpcaoFaseReferencia() {

		return opcaoFaseReferencia;
	}

	/**
	 * @param opcaoFaseReferencia
	 *            the opcaoFaseReferencia to set
	 */
	public void setOpcaoFaseReferencia(EntidadeConteudo opcaoFaseReferencia) {

		this.opcaoFaseReferencia = opcaoFaseReferencia;
	}

	/**
	 * @return the vencimentoDiaUtil
	 */
	public Boolean getVencimentoDiaUtil() {

		return vencimentoDiaUtil;
	}

	/**
	 * @param vencimentoDiaUtil
	 *            the vencimentoDiaUtil to set
	 */
	public void setVencimentoDiaUtil(Boolean vencimentoDiaUtil) {

		this.vencimentoDiaUtil = vencimentoDiaUtil;
	}

	/**
	 * @return the periodicidade
	 */
	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	/**
	 * @param periodicidade
	 *            the periodicidade to set
	 */
	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	/**
	 * @return the tarifa
	 */
	public Tarifa getTarifa() {

		return tarifa;
	}

	/**
	 * @param tarifa
	 *            the tarifa to set
	 */
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/**
	 * @return the indicadorDebitoAutomatico
	 */
	public Boolean getIndicadorDebitoAutomatico() {

		return indicadorDebitoAutomatico;
	}

	/**
	 * @param indicadorDebitoAutomatico
	 *            the indicadorDebitoAutomatico to
	 *            set
	 */
	public void setIndicadorDebitoAutomatico(Boolean indicadorDebitoAutomatico) {

		this.indicadorDebitoAutomatico = indicadorDebitoAutomatico;
	}

	/**
	 * @return the indicadorDepositoIdentificado
	 */
	public Boolean getIndicadorDepositoIdentificado() {

		return indicadorDepositoIdentificado;
	}

	/**
	 * @param indicadorDepositoIdentificado
	 *            the
	 *            indicadorDepositoIdentificado to
	 *            set
	 */
	public void setIndicadorDepositoIdentificado(Boolean indicadorDepositoIdentificado) {

		this.indicadorDepositoIdentificado = indicadorDepositoIdentificado;
	}

	/**
	 * @return the chavePrimariaItemFatura
	 */
	public long getChavePrimariaItemFatura() {

		return chavePrimariaItemFatura;
	}

	/**
	 * @param chavePrimariaItemFatura
	 *            the chavePrimariaItemFatura to
	 *            set
	 */
	public void setChavePrimariaItemFatura(long chavePrimariaItemFatura) {

		this.chavePrimariaItemFatura = chavePrimariaItemFatura;
	}

	/**
	 * @return the dadosItemFaturamento
	 */
	public Map<String, Object> getDadosItemFaturamento() {

		return dadosItemFaturamento;
	}

	/**
	 * @param dadosItemFaturamento
	 *            the dadosItemFaturamento to set
	 */
	public void setDadosItemFaturamento(Map<String, Object> dadosItemFaturamento) {

		this.dadosItemFaturamento = dadosItemFaturamento;
	}

	/**
	 * @return the dataReferenciaCambial
	 */
	public EntidadeConteudo getDataReferenciaCambial() {

		return dataReferenciaCambial;
	}

	/**
	 * @param dataReferenciaCambial
	 *            the dataReferenciaCambial to set
	 */
	public void setDataReferenciaCambial(EntidadeConteudo dataReferenciaCambial) {

		this.dataReferenciaCambial = dataReferenciaCambial;
	}

	/**
	 * @return the diaCotacao
	 */
	public EntidadeConteudo getDiaCotacao() {

		return diaCotacao;
	}

	/**
	 * @param diaCotacao
	 *            the diaCotacao to set
	 */
	public void setDiaCotacao(EntidadeConteudo diaCotacao) {

		this.diaCotacao = diaCotacao;
	}

	/**
	 * @return the dataInicioAssocTempTarifaPontoConsumo
	 */
	public Date getDataInicialAssocTempTarifaPontoConsumo() {
		Date data = null;
		if (this.dataInicialAssocTempTarifaPontoConsumo != null) {
			data = (Date) dataInicialAssocTempTarifaPontoConsumo.clone();
		}
		return data;
	}

	/**
	 * @param dataInicioAssocTempTarifaPontoConsumo
	 *            the dataInicioAssocTempTarifaPontoConsumo to set
	 */
	public void setDataInicialAssocTempTarifaPontoConsumo(Date dataInicialAssocTempTarifaPontoConsumo) {
		if (dataInicialAssocTempTarifaPontoConsumo != null) {
			this.dataInicialAssocTempTarifaPontoConsumo = (Date) dataInicialAssocTempTarifaPontoConsumo.clone();
		} else {
			this.dataInicialAssocTempTarifaPontoConsumo = null;
		}
	}

	/**
	 * @return the dataFimAssocTempTarifaPontoConsumo
	 */
	public Date getDataFinalAssocTempTarifaPontoConsumo() {
		Date data = null;
		if (this.dataFinalAssocTempTarifaPontoConsumo != null) {
			data = (Date) dataFinalAssocTempTarifaPontoConsumo.clone();
		}
		return data;
	}

	/**
	 * @param dataFimAssocTempTarifaPontoConsumo
	 *            the dataFimAssocTempTarifaPontoConsumo to set
	 */
	public void setDataFinalAssocTempTarifaPontoConsumo(Date dataFinalAssocTempTarifaPontoConsumo) {
		if (dataFinalAssocTempTarifaPontoConsumo != null) {
			this.dataFinalAssocTempTarifaPontoConsumo = (Date) dataFinalAssocTempTarifaPontoConsumo.clone();
		} else {
			this.dataFinalAssocTempTarifaPontoConsumo = null;
		}
	}

	/**
	 * @return the contratoCompra
	 */
	public EntidadeConteudo getContratoCompra() {

		return contratoCompra;
	}

	/**
	 * @param contratoCompra
	 *            the contratoCompra to set
	 */
	public void setContratoCompra(EntidadeConteudo contratoCompra) {

		this.contratoCompra = contratoCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getChavePrimariaItemFatura() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getChavePrimariaItemFatura()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(this == obj) {
			return true;
		}
		if(obj == null){ 
			return false;
		}
		if(!(obj instanceof ContratoPontoConsumoItemFaturamentoVO)){ 
			return false;
		}
		final ContratoPontoConsumoItemFaturamentoVO other = (ContratoPontoConsumoItemFaturamentoVO) obj;
		if(chavePrimariaItemFatura != other.chavePrimariaItemFatura) {
			return false;
		}
		return true;
	}

	public BigDecimal getPercminimoQDC() {

		return percminimoQDC;
	}

	public void setPercminimoQDC(BigDecimal percminimoQDC) {

		this.percminimoQDC = percminimoQDC;
	}

}
