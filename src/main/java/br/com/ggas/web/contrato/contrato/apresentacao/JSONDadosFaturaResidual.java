package br.com.ggas.web.contrato.contrato.apresentacao;

import java.io.Serializable;

public class JSONDadosFaturaResidual implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3810837958703738584L;
	private Long idPontoConsumo;
	private String dataLeitura;
	private String valorLeitura;
	private String situacaoContrato;
	
	
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public String getValorLeitura() {
		return valorLeitura;
	}
	public void setValorLeitura(String valorLeitura) {
		this.valorLeitura = valorLeitura;
	}
	public String getDataLeitura() {
		return dataLeitura;
	}
	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
	public String getSituacaoContrato() {
		return situacaoContrato;
	}
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

}
