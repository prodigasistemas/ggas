/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;

/**
 *	Classe responsável pelas telas relacionadas ao vinculo entre contrato e ponto de consumo 
 *
 */
@Controller
public class ContratoPontoConsumoAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(ContratoPontoConsumoAction.class);

	private static final String PARAM_ID_PONTO_CONSUMO = "pIdPontoConsumo";

	private static final String PONTO_CONSUMO_REFERENCIA_COPIA = "pontoConsumoReferenciaCopia";

	private static final String LISTA_PONTO_CONSUMO_PARA_COPIA = "listPontoConsumoParaCopia";

	private static final String ARRAY_ID_PONTO_CONSUMO = "arrayIdPontoConsumo";

	private static final String MODALIDADE_CONSUMO = "modalidades";

	protected static final String LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO = "listaImovelPontoConsumoSelecionadoVO";

	protected static final String LISTA_PONTO_CONSUMO_CONTRATO_VO = "listaPontoConsumoContratoVO";

	protected static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String CONTRATO = "contrato";

	protected static final String ITEM_FATURA = "itemFatura";

	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Exibir copia dados ponto consumo popup.
	 * @param idPontoConsumo - {@link Long}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirCopiaDadosPontoConsumoPopup")
	public String exibirCopiaDadosPontoConsumoPopup(@RequestParam(PARAM_ID_PONTO_CONSUMO) Long idPontoConsumo,
			Model model, HttpServletRequest request) {

		try {

			Collection<ImovelPontoConsumoVO> listImovelPontoConsumoVO = (Collection<ImovelPontoConsumoVO>) request
					.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);

			PontoConsumo pontoConsumoReferenciaCopia = null;

			// Pega o ponto de Consumo que vai ser
			// a referência para a cópia de pontos
			// de consumo!
			for (ImovelPontoConsumoVO recImovelPontoConsumoVO : listImovelPontoConsumoVO) {
				for (PontoConsumo recPontoConsumo : recImovelPontoConsumoVO.getListaPontoConsumo()) {
					if (recPontoConsumo.getChavePrimaria() == idPontoConsumo) {
						pontoConsumoReferenciaCopia = recPontoConsumo;
						break;
					}
				}
			}

			List<PontoConsumo> listPontoConsumoParaCopia = new ArrayList<PontoConsumo>();

			// Pega os Pontos de consumo,
			// referente ao mesmo segmento do
			// ponto de consumo de referência!
			for (ImovelPontoConsumoVO recImovelPontoConsumoVO : listImovelPontoConsumoVO) {
				for (PontoConsumo recPontoConsumo : recImovelPontoConsumoVO.getListaPontoConsumo()) {
					if ((!recPontoConsumo.equals(pontoConsumoReferenciaCopia))
							&& (recPontoConsumo.getSegmento().equals(pontoConsumoReferenciaCopia.getSegmento()))) {
						listPontoConsumoParaCopia.add(recPontoConsumo);
					}
				}
			}

			if (!listPontoConsumoParaCopia.isEmpty()) {
				request.getSession().setAttribute(PONTO_CONSUMO_REFERENCIA_COPIA, pontoConsumoReferenciaCopia);
				model.addAttribute(LISTA_PONTO_CONSUMO_PARA_COPIA, listPontoConsumoParaCopia);
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, new GGASException(e));
		}

		return "exibirCopiaDadosPontoConsumoPopup";
	}

	/**
	 * Copiar dados ponto consumo.
	 * @param arrayIdPontoConsumoSelecionados - Array de {@link Long}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("copiarDadosPontoConsumo")
	public String copiarDadosPontoConsumo(@RequestParam(ARRAY_ID_PONTO_CONSUMO) Long[] arrayIdPontoConsumoSelecionados,
			Model model, HttpServletRequest request) {

		try {
			PontoConsumo pontoConsumoReferenciaCopia = (PontoConsumo) request.getSession()
					.getAttribute(PONTO_CONSUMO_REFERENCIA_COPIA);

			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = (HashMap<Long, PontoConsumoContratoVO>) request
					.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);
			// Pega o objeto PontoConsumoContratoVO do
			// objeto que vai ser copiado!
			PontoConsumoContratoVO referPtConsumoVO = listaPontoConsumoContratoVO
					.get(pontoConsumoReferenciaCopia.getChavePrimaria());
			if (referPtConsumoVO != null) {

				for (int i = 0; i < arrayIdPontoConsumoSelecionados.length; i++) {

					if (arrayIdPontoConsumoSelecionados[i] > 0) {
						if (listaPontoConsumoContratoVO.get(arrayIdPontoConsumoSelecionados[i]) != null) {
							listaPontoConsumoContratoVO.remove(arrayIdPontoConsumoSelecionados[i]);
						}

						listaPontoConsumoContratoVO.put(arrayIdPontoConsumoSelecionados[i], this
								.copiarPontoConsumoContratoVO(arrayIdPontoConsumoSelecionados[i], referPtConsumoVO));
					}
				}
				request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);

				if (listaPontoConsumoContratoVO.size() > arrayIdPontoConsumoSelecionados.length) {

					model.addAttribute(ID_PONTO_CONSUMO, pontoConsumoReferenciaCopia.getChavePrimaria());
					this.agruparFaturamento(pontoConsumoReferenciaCopia.getChavePrimaria(), request);

				}

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirCopiaDadosPontoConsumoPopup";

	}

	private PontoConsumoContratoVO copiarPontoConsumoContratoVO(Long idPontoConsumo,
			PontoConsumoContratoVO referPtConsumoVO) throws GGASException  {

		PontoConsumo pontoConsumo = Fachada.getInstancia().buscarPontoConsumoPorChave(idPontoConsumo);
		PontoConsumoContratoVO pontoConsumoContratoVO = new PontoConsumoContratoVO();
		pontoConsumoContratoVO.setPreenchimentoConcluido(Boolean.TRUE);
		pontoConsumoContratoVO.setChavePrimaria(idPontoConsumo);
		pontoConsumoContratoVO.setAbaPrincipal(this.clonarAba(referPtConsumoVO.getAbaPrincipal()));
		pontoConsumoContratoVO.setAbaDadosTecnicos(this.clonarAba(referPtConsumoVO.getAbaDadosTecnicos()));
		pontoConsumoContratoVO.setAbaConsumo(this.clonarAba(referPtConsumoVO.getAbaConsumo()));
		pontoConsumoContratoVO.setAbaResponsabilidades(this.clonarAba(referPtConsumoVO.getAbaResponsabilidades()));
		pontoConsumoContratoVO.setAbaFaturamento(this.clonarAba(referPtConsumoVO.getAbaFaturamento()));
		pontoConsumoContratoVO.setAbaModalidade(this.clonarAba(this.clonarAba(referPtConsumoVO.getAbaModalidade())));

		this.tratarDadosAbaFaturamento(pontoConsumoContratoVO.getAbaFaturamento());
		this.tratarDadosAbaResponsabilidades(pontoConsumoContratoVO.getAbaResponsabilidades(), pontoConsumo);
		this.tratarDadosAbaResponsabilidades(pontoConsumoContratoVO.getAbaResponsabilidades(), pontoConsumo);
		this.tratarDadosAbaModalidade(pontoConsumoContratoVO.getAbaModalidade(), pontoConsumo);

		return pontoConsumoContratoVO;
	}

	@SuppressWarnings("unchecked")
	private void tratarDadosAbaResponsabilidades(Map<String, Object> abaResponsabilidade, PontoConsumo pontoConsumo) {

		for (Map.Entry<String, Object> entry : abaResponsabilidade.entrySet()) {
			String key = entry.getKey();
			List<ContratoCliente> listContratoCliente = (List<ContratoCliente>) entry.getValue();
			List<ContratoCliente> newListContratoCliente = new ArrayList<ContratoCliente>();

			if (listContratoCliente != null) {

				for (ContratoCliente contratoCliente : listContratoCliente) {

					ContratoCliente newContratoCliente = (ContratoCliente) controladorContrato.criarContratoCliente();
					this.copiarPropriedades(newContratoCliente, contratoCliente);
					newContratoCliente.setChavePrimaria(0);
					newContratoCliente.setPontoConsumo(pontoConsumo);

					newListContratoCliente.add(newContratoCliente);

				}

			}

			abaResponsabilidade.put(key, newListContratoCliente);

		}

	}

	@SuppressWarnings("unchecked")
	private void tratarDadosAbaFaturamento(Map<String, Object> abaFaturamento) {

		List<ContratoPontoConsumoItemFaturamentoVO> listVO = (List<ContratoPontoConsumoItemFaturamentoVO>) abaFaturamento
				.get(ITEM_FATURA);
		List<ContratoPontoConsumoItemFaturamentoVO> newListVO = new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();

		if (listVO != null) {

			for (ContratoPontoConsumoItemFaturamentoVO vo : listVO) {

				ContratoPontoConsumoItemFaturamentoVO newVo = new ContratoPontoConsumoItemFaturamentoVO();
				this.copiarPropriedades(newVo, vo);
				newListVO.add(newVo);

			}

		}

		abaFaturamento.put(ITEM_FATURA, newListVO);

	}

	@SuppressWarnings("unchecked")
	private void tratarDadosAbaModalidade(Map<String, Object> abaModalidade, PontoConsumo pontoConsumo)
			throws GGASException {

		Set<ModalidadeConsumoFaturamentoVO> hashVO = (HashSet<ModalidadeConsumoFaturamentoVO>) abaModalidade
				.get(MODALIDADE_CONSUMO);
		Set<ModalidadeConsumoFaturamentoVO> newHashVO = new HashSet<ModalidadeConsumoFaturamentoVO>();

		if (hashVO != null) {

			for (ModalidadeConsumoFaturamentoVO vo : hashVO) {

				ModalidadeConsumoFaturamentoVO newVo = new ModalidadeConsumoFaturamentoVO();
				this.copiarPropriedades(newVo, vo);

				if (vo.getListaCompromissoShipOrPayVO() != null && !vo.getListaCompromissoShipOrPayVO().isEmpty()) {

					Collection<CompromissoTakeOrPayVO> newListaCompromissoShipOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();

					for (CompromissoTakeOrPayVO compromissoShipOrPayVO : vo.getListaCompromissoShipOrPayVO()) {
						CompromissoTakeOrPayVO newCompromissoShipOrPayVO = new CompromissoTakeOrPayVO();
						this.copiarPropriedades(newCompromissoShipOrPayVO, compromissoShipOrPayVO);
						newListaCompromissoShipOrPayVO.add(newCompromissoShipOrPayVO);
					}

					newVo.setListaCompromissoShipOrPayVO(newListaCompromissoShipOrPayVO);

				}

				if (vo.getListaCompromissoTakeOrPayVO() != null && !vo.getListaCompromissoTakeOrPayVO().isEmpty()) {

					Collection<CompromissoTakeOrPayVO> newListaCompromissoTakeOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();

					for (CompromissoTakeOrPayVO compromissoTakeOrPayVO : vo.getListaCompromissoTakeOrPayVO()) {
						CompromissoTakeOrPayVO newCompromissoTakeOrPayVO = new CompromissoTakeOrPayVO();
						this.copiarPropriedades(newCompromissoTakeOrPayVO, compromissoTakeOrPayVO);
						newListaCompromissoTakeOrPayVO.add(newCompromissoTakeOrPayVO);
					}

					newVo.setListaCompromissoTakeOrPayVO(newListaCompromissoTakeOrPayVO);

				}

				if (vo.getListaContratoPontoConsumoModalidadeQDC() != null) {

					Collection<ContratoPontoConsumoModalidadeQDC> newListaPontoConsumoModalidadeQDC = new ArrayList<ContratoPontoConsumoModalidadeQDC>();

					for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : vo
							.getListaContratoPontoConsumoModalidadeQDC()) {

						ContratoPontoConsumoModalidadeQDC newPontoConsumoModalidadeQDC = (ContratoPontoConsumoModalidadeQDC) controladorContrato
								.criarContratoPontoConsumoModalidadeQDC();
						this.copiarPropriedades(newPontoConsumoModalidadeQDC, contratoPontoConsumoModalidadeQDC);
						newPontoConsumoModalidadeQDC.setChavePrimaria(0);

						if (newPontoConsumoModalidadeQDC.getContratoPontoConsumoModalidade() != null) {

							ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = newPontoConsumoModalidadeQDC
									.getContratoPontoConsumoModalidade();

							ContratoPontoConsumo newContratoPontoConsumo = this.criarContratoPontoConsumo(
									contratoPontoConsumoModalidade.getContratoPontoConsumo(), pontoConsumo);
							newPontoConsumoModalidadeQDC.getContratoPontoConsumoModalidade()
									.setContratoPontoConsumo(newContratoPontoConsumo);

							newPontoConsumoModalidadeQDC.getContratoPontoConsumoModalidade()
									.setContratoPontoConsumo(newContratoPontoConsumo);
						}

						newListaPontoConsumoModalidadeQDC.add(newPontoConsumoModalidadeQDC);
					}

					newVo.setListaContratoPontoConsumoModalidadeQDC(newListaPontoConsumoModalidadeQDC);

				}

				newHashVO.add(newVo);
			}

		}

		abaModalidade.put(MODALIDADE_CONSUMO, newHashVO);
	}

	private ContratoPontoConsumo criarContratoPontoConsumo(final ContratoPontoConsumo contratoPontoConsumo,
			PontoConsumo pontoConsumo) throws GGASException {

		ContratoPontoConsumo newContratoPontoConsumo = null;
		ContratoPontoConsumo contratoPontoConsumoLocal = contratoPontoConsumo;

		if (contratoPontoConsumoLocal != null) {

			newContratoPontoConsumo = (ContratoPontoConsumo) controladorContrato.criarContratoPontoConsumo();

			if (contratoPontoConsumoLocal.getChavePrimaria() > 0) {

				contratoPontoConsumoLocal = controladorContrato
						.obterContratoPontoConsumo(contratoPontoConsumoLocal.getChavePrimaria());

			}
			this.copiarPropriedades(newContratoPontoConsumo, contratoPontoConsumoLocal);

			newContratoPontoConsumo.setChavePrimaria(0);
			newContratoPontoConsumo.setPontoConsumo(pontoConsumo);

			if (newContratoPontoConsumo.getListaContratoPontoConsumoModalidade() != null) {

				Collection<ContratoPontoConsumoModalidade> newListaContratoPontoConsumoModalidade = new ArrayList<ContratoPontoConsumoModalidade>();

				for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : newContratoPontoConsumo
						.getListaContratoPontoConsumoModalidade()) {

					ContratoPontoConsumoModalidade newContratoPontoConsumoModalidade = (ContratoPontoConsumoModalidade) controladorContrato
							.criarContratoPontoConsumoModalidade();
					this.copiarPropriedades(newContratoPontoConsumoModalidade, contratoPontoConsumoModalidade);

					newContratoPontoConsumoModalidade.setContratoPontoConsumo(newContratoPontoConsumo);

					if (newContratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC() != null) {

						Collection<ContratoPontoConsumoModalidadeQDC> newListaContratoPontoConsumoModalidadeQDC = 
								new ArrayList<ContratoPontoConsumoModalidadeQDC>();

						for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : newContratoPontoConsumoModalidade
								.getListaContratoPontoConsumoModalidadeQDC()) {
							ContratoPontoConsumoModalidadeQDC newContratoPontoConsumoModalidadeQDC = (ContratoPontoConsumoModalidadeQDC) controladorContrato
									.criarContratoPontoConsumoModalidadeQDC();
							this.copiarPropriedades(newContratoPontoConsumoModalidadeQDC,
									contratoPontoConsumoModalidadeQDC);
							newContratoPontoConsumoModalidadeQDC
									.setContratoPontoConsumoModalidade(newContratoPontoConsumoModalidade);
							newListaContratoPontoConsumoModalidadeQDC.add(newContratoPontoConsumoModalidadeQDC);
						}

						newContratoPontoConsumoModalidade
								.setListaContratoPontoConsumoModalidadeQDC(newListaContratoPontoConsumoModalidadeQDC);
					}

					if (newContratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade() != null) {

						Collection<ContratoPontoConsumoPenalidade> newListaContratoPontoConsumoPenalidade = new ArrayList<ContratoPontoConsumoPenalidade>();

						for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : newContratoPontoConsumoModalidade
								.getListaContratoPontoConsumoPenalidade()) {
							ContratoPontoConsumoPenalidade newContratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato
									.criarContratoPontoConsumoPenalidade();
							this.copiarPropriedades(newContratoPontoConsumoPenalidade, contratoPontoConsumoPenalidade);
							newContratoPontoConsumoPenalidade
									.setContratoPontoConsumoModalidade(newContratoPontoConsumoModalidade);
							newListaContratoPontoConsumoPenalidade.add(newContratoPontoConsumoPenalidade);
						}

						newContratoPontoConsumoModalidade
								.setListaContratoPontoConsumoPenalidade(newListaContratoPontoConsumoPenalidade);
					}

					newListaContratoPontoConsumoModalidade.add(newContratoPontoConsumoModalidade);
				}

				newContratoPontoConsumo.setListaContratoPontoConsumoModalidade(newListaContratoPontoConsumoModalidade);
			}

			if (newContratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento() != null) {

				Collection<ContratoPontoConsumoItemFaturamento> newListaContratoPontoConsumoItemFaturamento = 
						new ArrayList<ContratoPontoConsumoItemFaturamento>();

				for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : newContratoPontoConsumo
						.getListaContratoPontoConsumoItemFaturamento()) {
					ContratoPontoConsumoItemFaturamento newContratoPontoConsumoItemFaturamento = (ContratoPontoConsumoItemFaturamento) controladorContrato
							.criarContratoPontoConsumoItemVencimento();
					this.copiarPropriedades(newContratoPontoConsumoItemFaturamento,
							contratoPontoConsumoItemFaturamento);
					newContratoPontoConsumoItemFaturamento.setContratoPontoConsumo(newContratoPontoConsumo);
					newListaContratoPontoConsumoItemFaturamento.add(newContratoPontoConsumoItemFaturamento);
				}

				newContratoPontoConsumo
						.setListaContratoPontoConsumoItemFaturamento(newListaContratoPontoConsumoItemFaturamento);
			}

			if (newContratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem() != null) {

				Collection<ContratoPontoConsumoPCSAmostragem> newListaContratoPontoConsumoPCSAmostragem = 
						new ArrayList<ContratoPontoConsumoPCSAmostragem>();

				for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : newContratoPontoConsumo
						.getListaContratoPontoConsumoPCSAmostragem()) {
					ContratoPontoConsumoPCSAmostragem newContratoPontoConsumoPCSAmostragem = (ContratoPontoConsumoPCSAmostragem) controladorContrato
							.criarContratoPontoConsumoPCSAmostragem();
					this.copiarPropriedades(newContratoPontoConsumoPCSAmostragem, contratoPontoConsumoPCSAmostragem);
					newContratoPontoConsumoPCSAmostragem.setContratoPontoConsumo(newContratoPontoConsumo);
					newListaContratoPontoConsumoPCSAmostragem.add(newContratoPontoConsumoPCSAmostragem);
				}

				newContratoPontoConsumo
						.setListaContratoPontoConsumoPCSAmostragem(newListaContratoPontoConsumoPCSAmostragem);
			}

			if (newContratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo() != null) {

				Collection<ContratoPontoConsumoPCSIntervalo> newListaContratoPontoConsumoPCSIntervalo =
						new ArrayList<ContratoPontoConsumoPCSIntervalo>();

				for (ContratoPontoConsumoPCSIntervalo contratoPontoConsumoPCSIntervalo : newContratoPontoConsumo
						.getListaContratoPontoConsumoPCSIntervalo()) {
					ContratoPontoConsumoPCSIntervalo newContratoPontoConsumoPCSIntervalo = (ContratoPontoConsumoPCSIntervalo) controladorContrato
							.criarContratoPontoConsumoPCSIntervalo();
					this.copiarPropriedades(newContratoPontoConsumoPCSIntervalo, contratoPontoConsumoPCSIntervalo);
					newContratoPontoConsumoPCSIntervalo.setContratoPontoConsumo(newContratoPontoConsumo);
					newListaContratoPontoConsumoPCSIntervalo.add(newContratoPontoConsumoPCSIntervalo);
				}

				newContratoPontoConsumo
						.setListaContratoPontoConsumoPCSIntervalo(newListaContratoPontoConsumoPCSIntervalo);
			}

		}

		return newContratoPontoConsumo;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> clonarAba(Map<String, Object> aba) {

		return (HashMap<String, Object>) ((HashMap<String, Object>) aba).clone();

	}

	private void copiarPropriedades(Object destino, Object origem) {

		if (destino != null && origem != null) {
			ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
			Util.copyProperties(destino, origem);
		}

	}

	@SuppressWarnings("unchecked")
	private Map<Long, PontoConsumoContratoVO> getListaPontoConsumoContratoVO(HttpServletRequest request) {
		return (HashMap<Long, PontoConsumoContratoVO>) request.getSession()
				.getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);
	}

	@SuppressWarnings("unchecked")
	private void agruparFaturamento(Long idPontoConsumo, HttpServletRequest request) throws GGASException {

		Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
		Long chaveAgrupamentoVolume = Long
				.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME));

		if ((contrato != null) && (contrato.getAgrupamentoCobranca() != null && contrato.getAgrupamentoCobranca())
				&& (idPontoConsumo != null)
				&& contrato.getTipoAgrupamento().getChavePrimaria() == chaveAgrupamentoVolume) {

			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

			PontoConsumoContratoVO voRef = listaPontoConsumoContratoVO.get(idPontoConsumo);

			for (PontoConsumoContratoVO vo : listaPontoConsumoContratoVO.values()) {

				if ((!vo.equals(voRef)) && (vo.isPreenchimentoConcluido())) {

					Set<String> keysMapFaturamento = voRef.getAbaFaturamento().keySet();
					if (keysMapFaturamento != null) {
						Map<String, Object> newMapFaturamento = new HashMap<String, Object>();
						for (String key : keysMapFaturamento) {

							if (!key.equals(ITEM_FATURA)) {

								newMapFaturamento.put(key, voRef.getAbaFaturamento().get(key));

							} else {

								List<ContratoPontoConsumoItemFaturamentoVO> listVO = (List<ContratoPontoConsumoItemFaturamentoVO>) voRef
										.getAbaFaturamento().get(key);
								List<ContratoPontoConsumoItemFaturamentoVO> newListVO = new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();

								if (listVO != null) {
									for (ContratoPontoConsumoItemFaturamentoVO contratoPontoConsumoItemFaturamentoVO : listVO) {
										newListVO.add(contratoPontoConsumoItemFaturamentoVO);
									}
								}
								newMapFaturamento.put(key, newListVO);

							}

						}

						vo.getAbaFaturamento().clear();
						vo.getAbaFaturamento().putAll(newMapFaturamento);

					}

				}

			}

		}

	}

}
