/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 */

package br.com.ggas.web.contrato.contrato.rest;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;
import java.util.Map;

/**
 * Classe responsável por gerenciar as respostas rest relacionadas a contratos
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Controller
public class ContratoActionRest extends GenericActionRest {

	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	/**
	 * Busca um contrato ponto consumo por cliente
	 * Lista todos os pontos de consumo, serializando como JSON
	 * Utiliza o Serializador {@link ContratoPontoConsumoSimplesSerializador}
	 *
	 * @param idCliente id do cliente cujo contrato será buscado
	 * @return retorna a lista de contrato ponto consumo por cliente
	 * @throws GGASException lançada caso ocorra alguma falha durante o processamento
	 */
	@RequestMapping(value = "rest/contrato/obterContratoPontoConsumoPorCliente/{id}", method = { RequestMethod.GET },
			produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> obterContratoPontoConsumoPorCliente(@PathVariable("id") Long idCliente) throws GGASException {
		final Collection<ContratoPontoConsumo> contratoPontoConsumos =
				controladorContrato.obterContratoPontoConsumoPorCliente(idCliente, "cep");

		final String json = serializarJson(contratoPontoConsumos);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Obtém a lista de serializadores para o contrato ponto consumo
	 * Adiciona seriliazadores default utilizado por este controlador
	 *
	 * @see GenericActionRest#getMapaSerializadores()
	 * @return retorna o mapa de serializadores
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder()
				.put(ContratoPontoConsumo.class, new ContratoPontoConsumoSimplesSerializador()).build();
	}
}
