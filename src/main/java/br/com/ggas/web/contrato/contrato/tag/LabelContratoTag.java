/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato.tag;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.util.GGASBaseTag;
import br.com.ggas.util.Util;

public class LabelContratoTag extends GGASBaseTag {

	/**
     * 
     */
	private static final long serialVersionUID = 4842696656012294353L;

	private String forCampo;

	private String forId;

	private String styleClass;

	private String styleId;

	/**
	 * @return the forId
	 */
	public String getForId() {

		return forId;
	}

	/**
	 * @param forId
	 *            the forId to set
	 */
	public void setForId(String forId) {

		this.forId = forId;
	}

	/**
	 * @return the styleId
	 */
	public String getStyleId() {

		return styleId;
	}

	/**
	 * @param styleId
	 *            the styleId to set
	 */
	public void setStyleId(String styleId) {

		this.styleId = styleId;
	}

	/**
	 * @return the styleClass
	 */
	public String getStyleClass() {

		return styleClass;
	}

	/**
	 * @param styleClass
	 *            the styleClass to set
	 */
	public void setStyleClass(String styleClass) {

		this.styleClass = styleClass;
	}

	/**
	 * @return the forCampo
	 */
	public String getForCampo() {

		return forCampo;
	}

	/**
	 * @param forCampo
	 *            the forCampo to set
	 */
	public void setForCampo(String forCampo) {

		this.forCampo = forCampo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.GGASBaseTag#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspException {

		StringBuilder html = new StringBuilder();
		if(!StringUtils.isEmpty(this.forCampo)) {
			String campoSelecao = Util.obterNomeCampoSelecao(this.forCampo);
			String campoObrigatorio = Util.obterNomeCampoObrigatorio(this.forCampo);
			boolean selecionado = this.obterValorAtributoForm(campoSelecao);
			boolean obrigatorio = this.obterValorAtributoForm(campoObrigatorio);

			obrigatorio = ignorarCamposObrigatorios(obrigatorio);
			
			html.append("<label ");

			if(!StringUtils.isEmpty(styleId)) {
				html.append("id='");
				html.append(styleId);
				html.append("' ");
			}

			html.append("class='rotulo");

			if(!StringUtils.isEmpty(styleClass)) {
				html.append(" ").append(styleClass);
			}

			if(!selecionado) {
				html.append(" rotuloDesabilitado");
			}

			if(obrigatorio) {
				html.append(" campoObrigatorio");
			}

			html.append("' ");
			html.append("for='");
			if(!StringUtils.isEmpty(this.forId)) {
				html.append(this.forId);
			} else {
				html.append(this.forCampo);
			}
			html.append("'> ");
			if(obrigatorio) {
				html.append("<span id='span");
				if(!StringUtils.isEmpty(this.forId)) {
					html.append(this.forId);
				} else {
					html.append(this.forCampo);
				}
				html.append("' class='campoObrigatorioSimbolo'>* </span> ");
			}
			html.append(bodyContent.getString());
			html.append("</label>");

		} else {
			html.append(bodyContent.getString());
		}

		try {
			pageContext.getOut().write(html.toString());
		} catch(IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	private boolean ignorarCamposObrigatorios(boolean obrigatorio) {
		ArrayList<String> camposObrigatoriosASeremIgnorados = new ArrayList<>();
		camposObrigatoriosASeremIgnorados.add("apuracaoParadaProgramadaC");
		camposObrigatoriosASeremIgnorados.add("apuracaoFalhaFornecimentoC");
		camposObrigatoriosASeremIgnorados.add("apuracaoCasoFortuitoC");
		camposObrigatoriosASeremIgnorados.add("apuracaoParadaProgramada");
		camposObrigatoriosASeremIgnorados.add("apuracaoFalhaFornecimento");
		camposObrigatoriosASeremIgnorados.add("apuracaoCasoFortuito");
		
		for (String campo : camposObrigatoriosASeremIgnorados) {
			if(this.forCampo.equals(campo)){
				return false;
			}
		}
		return obrigatorio;
	}

}
