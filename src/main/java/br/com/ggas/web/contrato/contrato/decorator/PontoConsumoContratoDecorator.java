/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.contrato.contrato.decorator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.web.contrato.contrato.ContratoVO;
import br.com.ggas.web.contrato.contrato.ImovelPontoConsumoVO;
import br.com.ggas.web.contrato.contrato.PontoConsumoContratoVO;

/**
 * 
 *
 */
public class PontoConsumoContratoDecorator extends TableDecorator {
	
	private static final Logger LOG = Logger.getLogger(ContratoVO.class);

	private static final String LISTA_PONTO_CONSUMO_CONTRATO_VO = "listaPontoConsumoContratoVO";

	private static final String LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO = "listaImovelPontoConsumoSelecionadoVO";
	
	public static final String PONTO_CONSUMO_EM_ALTERACAO = "pontoConsumoEmAlteracao";

	private String contextPath;

	/**
	 * @return the contextPath
	 */
	public String getContextPath() {

		return contextPath;
	}

	/**
	 * @param contextPath
	 *            the contextPath to set
	 */
	public void setContextPath(String contextPath) {

		this.contextPath = contextPath;
	}

	/*
	 * (non-Javadoc)
	 * @see org.displaytag.decorator.Decorator#init(javax.servlet.jsp.PageContext, java.lang.Object, org.displaytag.model.TableModel)
	 */
	@Override
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {

		super.init(pageContext, decorated, tableModel);
		this.contextPath = ((HttpServletRequest) pageContext.getRequest()).getContextPath();
	}

	/*
	 * (non-Javadoc)
	 * @see org.displaytag.decorator.TableDecorator#addRowClass()
	 */
	@Override
	public String addRowClass() {

		String rowClass = super.addRowClass();
		
		Long idPontoConsumoSelecionado = (Long) getAtributo("idPontoConsumo", getPageContext().findAttribute("contratoVO"));
		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();

		if((idPontoConsumoSelecionado != null) && (idPontoConsumoSelecionado.equals(Long.valueOf(pontoConsumo.getChavePrimaria())))) {
			rowClass = "clickedRow";
		}

		if (rowClass != null) {
			return rowClass;
		} else {
			return "";
		}
		
	}

	public String getDescricao() {

		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<a href=\"javascript:popularCamposContratoPontoConsumo('");
		stringBuilder.append(pontoConsumo.getChavePrimaria());
		stringBuilder.append("')\">");
		stringBuilder.append(pontoConsumo.getDescricao());
		stringBuilder.append("</a>");

		return stringBuilder.toString();
	}

	public String getSegmentoDescricao() {

		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<a href=\"javascript:popularCamposContratoPontoConsumo('");
		stringBuilder.append(pontoConsumo.getChavePrimaria());
		stringBuilder.append("')\">");
		stringBuilder.append(pontoConsumo.getSegmento().getDescricao());
		stringBuilder.append("</a>");

		return stringBuilder.toString();
	}

	public String getRamoAtividadeDescricao() {

		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<a href=\"javascript:popularCamposContratoPontoConsumo('");
		stringBuilder.append(pontoConsumo.getChavePrimaria());
		stringBuilder.append("')\">");
		stringBuilder.append(pontoConsumo.getRamoAtividade().getDescricao());
		stringBuilder.append("</a>");

		return stringBuilder.toString();
	}

	public String getChavePrimaria() {

		StringBuilder stringBuilder = new StringBuilder();
		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();
		String idPontoConsumoSelecionado = (String) getPageContext().getRequest().getParameter("idPontoConsumo");

		stringBuilder.append("<input type=\"checkbox\" name=\"chavesPrimariasPontoConsumo\" value=\"");
		stringBuilder.append(pontoConsumo.getChavePrimaria());
		stringBuilder.append("\"");

		if(!StringUtils.isEmpty(idPontoConsumoSelecionado)
						&& idPontoConsumoSelecionado.equals(String.valueOf(pontoConsumo.getChavePrimaria()))) {
			stringBuilder.append(" checked=\"checked\"");
		}

		stringBuilder.append(" id=\"chavePrimariaPontoConsumo").append(pontoConsumo.getChavePrimaria()).append("\"");
		stringBuilder.append(" onclick=\"onclickCheckPontoConsumo(this)\"");
		stringBuilder.append("/>");

		stringBuilder.append("<input type=\"hidden\" name=\"chavesPrimarias\" value=\"");
		stringBuilder.append(pontoConsumo.getChavePrimaria());
		stringBuilder.append("\"");
		stringBuilder.append("/>");

		return stringBuilder.toString();
	}

	@SuppressWarnings("unchecked")
	public String getSelecionado() {

		StringBuilder stringBuilder = new StringBuilder();
		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();
		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = (HashMap<Long, PontoConsumoContratoVO>) getPageContext()
						.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);
		
		Long idPontoConsumoEmAlteracao = (Long)  getPageContext()
				.getSession().getAttribute(PONTO_CONSUMO_EM_ALTERACAO);
		
		if(idPontoConsumoEmAlteracao != null && idPontoConsumoEmAlteracao.longValue() == pontoConsumo.getChavePrimaria()) {
			stringBuilder.append("<img  alt=\"Dados em preenchimento\" title=\"Dados em preenchimento\" src=\"");
			stringBuilder.append(this.contextPath);
			stringBuilder.append("/imagens/seta_direita.gif");
			stringBuilder.append("\" border=\"0\"/>");
		} else if (listaPontoConsumoContratoVO != null && listaPontoConsumoContratoVO.containsKey(pontoConsumo.getChavePrimaria())) {

			PontoConsumoContratoVO pontoConsumoVO = listaPontoConsumoContratoVO.get(pontoConsumo.getChavePrimaria());
			if(pontoConsumoVO.isPreenchimentoConcluido()) {
				stringBuilder.append("<img  alt=\"Dados preenchidos\" title=\"Dados preenchidos\" src=\"");
				stringBuilder.append(this.contextPath);
				stringBuilder.append("/imagens/check1.gif");
				stringBuilder.append("\" border=\"0\"/>");
			} else {
				stringBuilder.append("<img  alt=\"Dados parcialmente preenchidos\" title=\"Dados parcialmente preenchidos\" src=\"");
				stringBuilder.append(this.contextPath);
				stringBuilder.append("/imagens/icon-alert16.gif");
				stringBuilder.append("\" border=\"0\"/>");
			}
		}

		return stringBuilder.toString();
	}

	@SuppressWarnings("unchecked")
	public String getCopiaLiberada() {

		StringBuilder stringBuilder = new StringBuilder();

		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();
		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = (HashMap<Long, PontoConsumoContratoVO>) getPageContext()
						.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);

		if(listaPontoConsumoContratoVO != null && listaPontoConsumoContratoVO.containsKey(pontoConsumo.getChavePrimaria())) {

			PontoConsumoContratoVO pontoConsumoVO = listaPontoConsumoContratoVO.get(pontoConsumo.getChavePrimaria());

			if(pontoConsumoVO.isPreenchimentoConcluido() && existePontoConsumoMesmoSegmento(pontoConsumo)) {

				stringBuilder.append("<a href=\"javascript:exibirPopupCopiarPontoConsumo(").append(pontoConsumo.getChavePrimaria())
						.append(");\"/>");
				stringBuilder.append("<img alt=\"Copiar\" title=\"Copiar\" src=\"").append(this.contextPath)
						.append("/imagens/page_copy.png\" border=\"0\"");
				stringBuilder.append("</a>");

			}

		}

		return stringBuilder.toString();
	}

	/**
	 * Existe ponto consumo mesmo segmento.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	private boolean existePontoConsumoMesmoSegmento(PontoConsumo pontoConsumo) {

		boolean retorno = false;
		Set<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = (HashSet<ImovelPontoConsumoVO>) getPageContext().getSession()
						.getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);
		Collection<PontoConsumo> collPontoConsumo = new ArrayList<PontoConsumo>();

		if(listaImovelPontoConsumoVO != null) {
			for (ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
				collPontoConsumo.addAll(imovelPontoConsumoVO.getListaPontoConsumo());
			}
		}

		for (PontoConsumo pontoConsumoSessao : collPontoConsumo) {
			if((pontoConsumoSessao.getChavePrimaria() != pontoConsumo.getChavePrimaria())
							&& (pontoConsumoSessao.getSegmento().getChavePrimaria() == pontoConsumo.getSegmento().getChavePrimaria())) {
				retorno = true;
				break;
			}
		}

		return retorno;
	}
	
	/**
	 * @param atributo	-	{@link String}		
	 * @param obj		-	{@link Object}
	 */
	private Object getAtributo(String atributo, Object obj) {
		
		try {
			Field field = ContratoVO.class.getDeclaredField(atributo);
			field.setAccessible(Boolean.TRUE);
			return field.get(obj);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return null;
	}

}
