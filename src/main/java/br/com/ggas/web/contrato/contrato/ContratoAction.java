/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.web.contrato.contrato;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
import static br.com.ggas.util.Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO;
import static br.com.ggas.util.Constantes.TRUE;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unboundid.ldap.sdk.BindResult;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.MotivoFimRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoVencimento;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Aba;
import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.AbaModelo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.ContratoAnexo;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.ControladorContratoImpl;
import br.com.ggas.contrato.contrato.impl.ModeloAtributoImpl;
import br.com.ggas.contrato.contrato.impl.ModeloAtributoVO;
import br.com.ggas.contrato.contrato.impl.PontoConsumoEncerrarFaturaVO;
import br.com.ggas.contrato.contrato.impl.PontosConsumoSituacaoRubricaVO;
import br.com.ggas.contrato.contrato.impl.RelatorioContratoHelper;
import br.com.ggas.contrato.contratoadequacaoparceria.ContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.impl.MigrarSaldoQPNRVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.faturamento.exception.ContratoPontosConsumoSegmentosDiferentesException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.dadosmedicao.apresentacao.JSONDadosLeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Agrupador;
import br.com.ggas.util.BASE64DecodedMultipartFile;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.contrato.apresentacao.JSONDadosFaturaResidual;
import br.com.ggas.web.contrato.proposta.FiltroProposta;

/**
 * The Class ContratoAction.
 */
@Controller
public class ContratoAction extends GenericAction {

	/** The Constant LISTA_ANEXOS. */
	private static final String LISTA_ANEXOS = "listaAnexos";

	/** The Constant TAMANHO_MAXIMO_ARQUIVO. */
	private static final int TAMANHO_MAXIMO_ARQUIVO = 5242880;

	/** The Constant NUMERO_CONTRATO. */
	private static final String NUMERO_CONTRATO = "numeroContrato";

	/** The Constant MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS. */
	private static final String MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS = "mapaAtributosAdicionadosRemovidos";

	/** The Constant MODELO_NOVO. */
	private static final String MODELO_NOVO = "modeloNovo";

	/** The Constant EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG. */
	private static final String EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG = "Migração Modelo do Contrato.log";

	/** The Constant EMAIL_ATTACHMENT_FILENAME. */
	private static final String EMAIL_ATTACHMENT_FILENAME = "attachment; filename=";

	/** The Constant EMAIL_CONTENT_DISPOSITION. */
	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	/** The Constant LAYOUT_RELATORIO. */
	private static final String LAYOUT_RELATORIO = "modeloContrato.layoutRelatorio";

	/** The Constant EMAIL_APPLICATION_DOWNLOAD. */
	private static final String EMAIL_APPLICATION_DOWNLOAD = "application/download";

	/** The Constant LABEL_COM_ERRO. */
	private static final String LABEL_COM_ERRO = "com erro";

	/** The Constant LOG_ERRO_MIGRACAO. */
	private static final String LOG_ERRO_MIGRACAO = "logErroMigracao";

	/** The Constant MAPA_ATRIBUTOS_COMUNS. */
	private static final String MAPA_ATRIBUTOS_COMUNS = "mapaAtributosComuns";

	/** The Constant MAPA_ATRIBUTOS_REMOVIDOS. */
	private static final String MAPA_ATRIBUTOS_REMOVIDOS = "mapaAtributosRemovidos";

	/** The Constant MAPA_ATRIBUTOS_ADICIONADOS. */
	private static final String MAPA_ATRIBUTOS_ADICIONADOS = "mapaAtributosAdicionados";

	/** The Constant LISTA_CONTRATO_MIGRACAO. */
	private static final String LISTA_CONTRATO_MIGRACAO = "listaContratoMigracao";

	/** The Constant MAPA_CONTRATO_MIGRACAO. */
	private static final String MAPA_CONTRATO_MIGRACAO = "mapaContratoMigracao";

	/** The Constant INDICE_PENALIDADE_RETIRADA_MAIOR_MENOR. */
	private static final String INDICE_PENALIDADE_RETIRADA_MAIOR_MENOR = "indicePenalidadeRetiradaMaiorMenor";

	/** The Constant LABEL_CONSUMO_REFERENCIA. */
	private static final String LABEL_CONSUMO_REFERENCIA = "Consumo Referência";

	/** The Constant LABEL_DATA_FIM_VIGENCIA. */
	private static final String LABEL_DATA_FIM_VIGENCIA = "Data Fim Vigência";

	/** The Constant LABEL_DATA_INICIO_VIGENCIA. */
	private static final String LABEL_DATA_INICIO_VIGENCIA = "Data Início Vigência";

	/**
	 * The Constant LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO.
	 */
	private static final String LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO =
			"Percentual de Cobrança " + "para Consumo Superior Durante Aviso de Interrupção";

	/** The Constant LABEL_PERCENTUAL_DE_COBRANCA. */
	private static final String LABEL_PERCENTUAL_DE_COBRANCA = "Percentual de Cobrança";

	/** The Constant LABEL_FORMA_DE_CALCULO_PARA_COBRANCA. */
	private static final String LABEL_FORMA_DE_CALCULO_PARA_COBRANCA = "Forma de Cálculo para Cobrança";

	/** The Constant LABEL_TABELA_DE_PRECO_PARA_COBRANCA. */
	private static final String LABEL_TABELA_DE_PRECO_PARA_COBRANCA = "Tabela de Preço para Cobrança";

	/** The Constant LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR. */
	private static final String LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR = "Base de Apuração Retirada Maior/Menor";

	/** The Constant DESCER_POSICAO. */
	private static final String DESCER_POSICAO = "descerPosicao";

	/** The Constant SUBIR_POSICAO. */
	private static final String SUBIR_POSICAO = "subirPosicao";

	/** The Constant INDEX. */
	private static final String INDEX = "index";

	/** The Constant OPERACAO. */
	private static final String OPERACAO = "operacao";

	/** The Constant FATOR_CORRECAO. */
	private static final String FATOR_CORRECAO = "fatorCorrecao";

	/** The Constant RELATORIO_FATURA_NOTA_FISCAL. */
	private static final String RELATORIO_FATURA_NOTA_FISCAL = "relatorioFaturaNotaFiscal";

	/** The Constant RELATORIO_NOTA_DEBITO_CREDITO. */
	private static final String RELATORIO_NOTA_DEBITO_CREDITO = "relatorioNotaDebitoCredito";

	/** The Constant FATURA_GERAL. */
	private static final String FATURA_GERAL = "faturaGeral";

	/** The Constant LISTA_FATURA_ITEM. */
	private static final String LISTA_FATURA_ITEM = "listaFaturaItem";

	/** The Constant CHAVE_PRIMARIA_FATURA. */
	private static final String CHAVE_PRIMARIA_FATURA = "chavePrimariaFatura";
	
	/** The Constant IMPRESSAO. */
	private static final String IMPRESSAO = "impressao";

	/** The Constant VALOR_COBRADO. */
	private static final String VALOR_COBRADO = "valorCobrado";

	/** The Constant ENDERECO_PRINCIPAL. */
	private static final String ENDERECO_PRINCIPAL = "enderecoPrincipal";

	/**
	 * The Constant LABEL_CALCULANDO_PENALIDADES_DO_CONTRATO_ENCERRADO_RESCINDIDO.
	 */
	private static final String LABEL_CALCULANDO_PENALIDADES_DO_CONTRATO_ENCERRADO_RESCINDIDO =
			"\r\n ...Calculando penalidades do " + "contrato encerrado/rescindido. \r\n\r\n";

	/** The Constant PONTOS_SEM_RECISAO. */
	private static final String PONTOS_SEM_RECISAO = "pontosSemRecisao";

	/** The Constant EXIBIR_PONTO_CONSUMO_CONTRATO. */
	private static final String EXIBIR_PONTO_CONSUMO_CONTRATO = "exibirPontoConsumoContrato";

	/** The Constant LISTA_IDS_PONTO_CONSUMO_REMOVIDOS. */
	private static final String LISTA_IDS_PONTO_CONSUMO_REMOVIDOS = "listaIdsPontoConsumoRemovidos";

	/** The Constant LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS. */
	private static final String LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS = "listaIdsPontoConsumoRemovidosAgrupados";

	/** The Constant PERIODO_RECISAO_CAMPO. */
	private static final String PERIODO_RECISAO_CAMPO = "periodoRecisaoCampo";

	/** The Constant ID_CONTRATO_ENCERRAR_RESCINDIR. */
	private static final String ID_CONTRATO_ENCERRAR_RESCINDIR = "idContratoEncerrarRescindir";

	/** The Constant CHAVE_PRIMARIA_CONTRATO. */
	private static final String CHAVE_PRIMARIA_CONTRATO = "chavePrimariaContrato";

	/** The Constant LISTA_PONTO_CONSUMO_REMOVIDOS. */
	private static final String LISTA_PONTO_CONSUMO_REMOVIDOS = "listaPontoConsumoRemovidos";

	/** The Constant IMOVEL. */
	private static final String IMOVEL = "imovel";

	/** The Constant LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR. */
	private static final String LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR = "listaContratosPrincipalEComplementar";

	/** The Constant LISTA_CONTRATO_PENALIDADE. */
	private static final String LISTA_CONTRATO_PENALIDADE = "listaContratoPenalidade";

	/** The Constant INDICADOR_ATUALIZACAO_CADASTRAL. */
	private static final String INDICADOR_ATUALIZACAO_CADASTRAL = "indicadorAtualizacaoCadastral";

	/** The Constant RESPONSABILIDADE. */
	private static final String RESPONSABILIDADE = "responsabilidade";

	/** The Constant ID_RESPONSABILIDADE. */
	private static final String ID_RESPONSABILIDADE = "idResponsabilidade";

	/** The Constant FIM_RELACIONAMENTO. */
	private static final String FIM_RELACIONAMENTO = "fimRelacionamento";

	/** The Constant RELACAO_INICIO. */
	private static final String RELACAO_INICIO = "relacaoInicio";

	/** The Constant EMAIL. */
	private static final String EMAIL = "email";

	/** The Constant ENDERECO_FORMATADO. */
	private static final String ENDERECO_FORMATADO = "enderecoFormatado";

	/** The Constant DOCUMENTO_FORMATADO. */
	private static final String DOCUMENTO_FORMATADO = "documentoFormatado";

	/** The Constant NOME. */
	private static final String NOME = "nome";

	/** The Constant LABEL_TIPO_DE_RESPONSABILIDADE. */
	private static final String LABEL_TIPO_DE_RESPONSABILIDADE = "Tipo de Responsabilidade";

	/** The Constant LABEL_MARGEM_DE_VARIACAO_SHIP_OR_PAY. */
	private static final String LABEL_MARGEM_DE_VARIACAO_SHIP_OR_PAY = "Margem de Variação ShipOrPay";

	/** The Constant LABEL_DATA_DE_FIM_DE_VIGENCIA_TO_P. */
	private static final String LABEL_DATA_DE_FIM_DE_VIGENCIA_TO_P = "Data de Fim de Vigência ToP";

	/** The Constant LABEL_DATA_DE_INICIO_DE_VIGENCIA_TO_P. */
	private static final String LABEL_DATA_DE_INICIO_DE_VIGENCIA_TO_P = "Data de Início de Vigência ToP";

	/** The Constant LABEL_PERCENTUAL_NAO_RECUPERAVEL. */
	private static final String LABEL_PERCENTUAL_NAO_RECUPERAVEL = "Percentual Não Recuperável";

	/** The Constant LABEL_MARGEM_DE_VARIACAO_TAKE_OR_PAY. */
	private static final String LABEL_MARGEM_DE_VARIACAO_TAKE_OR_PAY = "Margem de Variação TakeOrPay";

	/** The Constant LABEL_REFERENCIA_QF_PARADA_PROGRAMADA. */
	private static final String LABEL_REFERENCIA_QF_PARADA_PROGRAMADA = "Referência QF parada programada";

	/** The Constant MODALIDADES. */
	private static final String MODALIDADES = "modalidades";

	/** The Constant LABEL_PERCENTUAL_QNR. */
	private static final String LABEL_PERCENTUAL_QNR = "PercentualQNR";

	/** The Constant LABEL_PERCENTUAL_A_CONSIDERAR_PARA_QNR. */
	private static final String LABEL_PERCENTUAL_A_CONSIDERAR_PARA_QNR = "Percentual a considerar para QNR";

	/** The Constant LABEL_MARGEM_VARIACAO_SHIP_OR_PAY. */
	private static final String LABEL_MARGEM_VARIACAO_SHIP_OR_PAY = "Margem Variação ShipOrPay";

	/** The Constant APURACAO_FALHA_FORNECIMENTO. */
	private static final String APURACAO_FALHA_FORNECIMENTO = "apuracaoFalhaFornecimento";

	/** The Constant APURACAO_CASO_FORTUITO. */
	private static final String APURACAO_CASO_FORTUITO = "apuracaoCasoFortuito";

	/** The Constant APURACAO_PARADA_NAO_PROGRAMADA. */
	private static final String APURACAO_PARADA_NAO_PROGRAMADA = "apuracaoParadaNaoProgramada";

	/** The Constant APURACAO_PARADA_PROGRAMADA. */
	private static final String APURACAO_PARADA_PROGRAMADA = "apuracaoParadaProgramada";

	/** The Constant PERCENTUAL_NAO_RECUPERAVEL. */
	private static final String PERCENTUAL_NAO_RECUPERAVEL = "percentualNaoRecuperavel";

	/** The Constant RECUPERAVEL. */
	private static final String RECUPERAVEL = "recuperavel";

	/** The Constant CONSIDERA_CASO_FORTUITO. */
	private static final String CONSIDERA_CASO_FORTUITO = "consideraCasoFortuito";

	/** The Constant CONSIDERA_FALHA_FORNECIMENTO. */
	private static final String CONSIDERA_FALHA_FORNECIMENTO = "consideraFalhaFornecimento";

	/** The Constant CONSIDERA_PARADA_NAO_PROGRAMADA. */
	private static final String CONSIDERA_PARADA_NAO_PROGRAMADA = "consideraParadaNaoProgramada";

	/** The Constant CONSIDERA_PARADA_PROGRAMADA. */
	private static final String CONSIDERA_PARADA_PROGRAMADA = "consideraParadaProgramada";

	/** The Constant TIPO_AGRUPAMENTO_CONTRATO. */
	private static final String TIPO_AGRUPAMENTO_CONTRATO = "tipoAgrupamentoContrato";

	/** The Constant TIPO_APURACAO. */
	private static final String TIPO_APURACAO = "tipoApuracao";

	/** The Constant PRECO_COBRANCA. */
	private static final String PRECO_COBRANCA = "precoCobranca";

	/** The Constant DATA_FIM_VIGENCIA. */
	private static final String DATA_FIM_VIGENCIA = "dataFimVigencia";

	/** The Constant DATA_INICIO_VIGENCIA. */
	private static final String DATA_INICIO_VIGENCIA = "dataInicioVigencia";

	/** The Constant REFERENCIA_QF_PARADA_PROGRAMADA. */
	private static final String REFERENCIA_QF_PARADA_PROGRAMADA = "referenciaQFParadaProgramada";

	/** The Constant LABEL_PERCENTUAL_MINIMO_EM_RELACAO_A_QDC. */
	private static final String LABEL_PERCENTUAL_MINIMO_EM_RELACAO_A_QDC = "Percentual mínimo em relação à QDC";

	/** The Constant LABEL_PERCENTUAL_MAXIMO_EM_RELACAO_A_QDC. */
	private static final String LABEL_PERCENTUAL_MAXIMO_EM_RELACAO_A_QDC = "Percentual máximo em relação à QDC";

	/** The Constant LABEL_PERCENTUAL_VARIACAO_SUPERIOR_QDC. */
	private static final String LABEL_PERCENTUAL_VARIACAO_SUPERIOR_QDC = "Percentual variação superior QDC";

	/** The Constant DATA_VIGENCIA_QDC. */
	private static final String DATA_VIGENCIA_QDC = "dataVigenciaQDC";

	/** The Constant QDC. */
	private static final String QDC = "qdc";

	/** The Constant LISTA_FAIXAS_PRESSAO_FORNECIMENTO. */
	private static final String LISTA_FAIXAS_PRESSAO_FORNECIMENTO = "listaFaixasPressaoFornecimento";

	/** The Constant PONTO_CONSUMO_NAO_INFORMADO. */
	private static final String PONTO_CONSUMO_NAO_INFORMADO = "pontoConsumoNaoInformado";

	/** The Constant LABEL_IMOVEL. */
	private static final String LABEL_IMOVEL = " imóvel ";

	/** The Constant SEGMENTO. */
	private static final String SEGMENTO = "segmento";

	/** The Constant LISTA_TIPO_AGRUPAMENTO_CONTRATO. */
	private static final String LISTA_TIPO_AGRUPAMENTO_CONTRATO = "listaTipoAgrupamentoContrato";

	/** The Constant LISTA_APURACAO_FALHA_FORNECIMENTO. */
	private static final String LISTA_APURACAO_FALHA_FORNECIMENTO = "listaApuracaoFalhaFornecimento";

	/** The Constant LISTA_APURACAO_CASO_FORTUITO. */
	private static final String LISTA_APURACAO_CASO_FORTUITO = "listaApuracaoCasoFortuito";

	/** The Constant LISTA_APURACAO_PARADA_NAO_PROGRAMADA. */
	private static final String LISTA_APURACAO_PARADA_NAO_PROGRAMADA = "listaApuracaoParadaNaoProgramada";

	/** The Constant LISTA_APURACAO_PARADA_PROGRAMADA. */
	private static final String LISTA_APURACAO_PARADA_PROGRAMADA = "listaApuracaoParadaProgramada";

	/** The Constant LISTA_PRECO_COBRANCA. */
	private static final String LISTA_PRECO_COBRANCA = "listaPrecoCobranca";

	/** The Constant ITEM_FATURA_MARGEM_DISTRIBUICAO. */
	private static final String ITEM_FATURA_MARGEM_DISTRIBUICAO = "itemFaturaMargemDistribuicao";

	/** The Constant ID_TIPO_CONTRATO. */
	private static final String ID_TIPO_CONTRATO = "idTipoContrato";

	/** The Constant LABEL_NUMERO_DO_CONTRATO. */
	private static final String LABEL_NUMERO_DO_CONTRATO = "Número do Contrato";

	/** The Constant LABEL_PERCENTUAL_JUROS_FINANCIAMENTO. */
	private static final String LABEL_PERCENTUAL_JUROS_FINANCIAMENTO = "Percentual Juros Financiamento";

	/** The Constant LABEL_VALOR_PARTICIPACAO_CLIENTE. */
	private static final String LABEL_VALOR_PARTICIPACAO_CLIENTE = "Valor Participação Cliente";

	/** The Constant LABEL_DESCONTO_EFETIVO_ECONOMIA. */
	private static final String LABEL_DESCONTO_EFETIVO_ECONOMIA = "Desconto Efetivo Economia";

	/** The Constant LOCAL_ECONOMIA_ESTIMADA_GN_ANO. */
	private static final String LOCAL_ECONOMIA_ESTIMADA_GN_ANO = "Economia Estimada GN Ano";

	/** The Constant LABEL_ECONOMIA_ESTIMADA_GN_MES. */
	private static final String LABEL_ECONOMIA_ESTIMADA_GN_MES = "Economia Estimada GN Mês";

	/** The Constant LABEL_GASTO_ESTIMADO_GN_MES. */
	private static final String LABEL_GASTO_ESTIMADO_GN_MES = "Gasto Estimado GN Mês";

	/** The Constant LABEL_VALOR_QDC. */
	private static final String LABEL_VALOR_QDC = "Valor QDC";

	/** The Constant LABEL_VALOR_GARANTIA. */
	private static final String LABEL_VALOR_GARANTIA = "Valor Garantia";

	/** The Constant APURACAO_CASO_FORTUITO_C. */
	private static final String APURACAO_CASO_FORTUITO_C = "apuracaoCasoFortuitoC";

	/** The Constant APURACAO_FALHA_FORNECIMENTO_C. */
	private static final String APURACAO_FALHA_FORNECIMENTO_C = "apuracaoFalhaFornecimentoC";

	/** The Constant APURACAO_PARADA_PROGRAMADA_C. */
	private static final String APURACAO_PARADA_PROGRAMADA_C = "apuracaoParadaProgramadaC";

	/** The Constant PERCENTUAL_NAO_RECUPERAVEL_C. */
	private static final String PERCENTUAL_NAO_RECUPERAVEL_C = "percentualNaoRecuperavelC";

	/** The Constant RECUPERAVEL_C. */
	private static final String RECUPERAVEL_C = "recuperavelC";

	/** The Constant CONSIDERA_CASO_FORTUITO_C. */
	private static final String CONSIDERA_CASO_FORTUITO_C = "consideraCasoFortuitoC";

	/** The Constant CONSIDERA_PARADA_PROGRAMADA_C. */
	private static final String CONSIDERA_PARADA_PROGRAMADA_C = "consideraParadaProgramadaC";

	/** The Constant CONSIDERA_FALHA_FORNECIMENTO_C. */
	private static final String CONSIDERA_FALHA_FORNECIMENTO_C = "consideraFalhaFornecimentoC";

	/** The Constant TIPO_APURACAO_C. */
	private static final String TIPO_APURACAO_C = "tipoApuracaoC";

	/** The Constant PRECO_COBRANCA_C. */
	private static final String PRECO_COBRANCA_C = "precoCobrancaC";

	/** The Constant DATA_FIM_VIGENCIA_C. */
	private static final String DATA_FIM_VIGENCIA_C = "dataFimVigenciaC";

	/** The Constant MARGEM_VARIACAO_TAKE_OR_PAY_C. */
	private static final String MARGEM_VARIACAO_TAKE_OR_PAY_C = "margemVariacaoTakeOrPayC";

	/** The Constant DATA_INICIO_VIGENCIA_C. */
	private static final String DATA_INICIO_VIGENCIA_C = "dataInicioVigenciaC";

	/** The Constant CONSUMO_REFERENCIA_TAKE_OR_PAY_C. */
	private static final String CONSUMO_REFERENCIA_TAKE_OR_PAY_C = "consumoReferenciaTakeOrPayC";

	/** The Constant REFERENCIA_QF_PARADA_PROGRAMADA_C. */
	private static final String REFERENCIA_QF_PARADA_PROGRAMADA_C = "referenciaQFParadaProgramadaC";

	/** The Constant PERIODICIDADE_TAKE_OR_PAY_C. */
	private static final String PERIODICIDADE_TAKE_OR_PAY_C = "periodicidadeTakeOrPayC";

	/** The Constant LABEL_PERCENTUAL_FIM_RETIRADA_QPNR. */
	private static final String LABEL_PERCENTUAL_FIM_RETIRADA_QPNR = "Percentual Fim Retirada QPNR";

	/** The Constant LABEL_PERCENTUAL_MINIMO_RETIRADA_QPNR. */
	private static final String LABEL_PERCENTUAL_MINIMO_RETIRADA_QPNR = "Percentual Mínimo Retirada QPNR";

	/** The Constant LABEL_PERCENTUAL_MAXIMO_RETIRADA_QPNR. */
	private static final String LABEL_PERCENTUAL_MAXIMO_RETIRADA_QPNR = "Percentual Máximo Retirada QPNR";

	/** The Constant LABEL_VALOR_INVESTIMENTO. */
	private static final String LABEL_VALOR_INVESTIMENTO = "Valor Investimento";

	/** The Constant LABEL_ARRECADADOR_CONTRATO_CONVENIO. */
	private static final String LABEL_ARRECADADOR_CONTRATO_CONVENIO = "Arrecadador Contrato Convênio";

	/** The Constant LABEL_ARRECADADOR_CONTRATO_CONVENIO_DEBITO_AUTOMATICO. */
	private static final String LABEL_ARRECADADOR_CONTRATO_CONVENIO_DEBITO_AUTOMATICO = "Arrecadador Contrato Convênio Débito Automático";

	/** The Constant LABEL_VALOR_CONTRATO. */
	private static final String LABEL_VALOR_CONTRATO = "Valor Contrato";

	/** The Constant LABEL_NUMERO_DO_EMPENHO. */
	private static final String LABEL_NUMERO_DO_EMPENHO = "Número do Empenho";

	/** The Constant PONTO_CONSUMO. */
	private static final String PONTO_CONSUMO = "pontoConsumo";

	/** The Constant CHAR_UPPER_X. */
	private static final String CHAR_UPPER_X = "X";

	/** The Constant NUMERO_ZERO. */
	private static final String NUMERO_ZERO = "0";

	/** The Constant NUMERO_UM. */
	private static final String NUMERO_UM = "1";

	/** The Constant NUMERO_MENOS_UM. */
	private static final String NUMERO_MENOS_UM = "-1";

	/** The Constant LABEL_REGIME_DE_CONSUMO. */
	private static final String LABEL_REGIME_DE_CONSUMO = "Regime de Consumo";

	/** The Constant LABEL_NUMERO_MAXIMO_DA_REDUCAO. */
	private static final String LABEL_NUMERO_MAXIMO_DA_REDUCAO = "Número Máximo da Redução";

	/** The Constant LABLE_TEMPO_DE_GARANTIA_DA_CONVERSAO. */
	private static final String LABLE_TEMPO_DE_GARANTIA_DA_CONVERSAO = "Tempo de Garantia da conversão";

	/** The Constant LABEL_DATA_DE_INICIO_DE_GARANTIA_DA_CONVERSAO. */
	private static final String LABEL_DATA_DE_INICIO_DE_GARANTIA_DA_CONVERSAO = "Data de Início de Garantia da Conversão";

	/** The Constant LABEL_VOLUME_DE_PERIODO_DE_TESTES. */
	private static final String LABEL_VOLUME_DE_PERIODO_DE_TESTES = "Volume de Período de Testes";

	/** The Constant LABEL_PRAZO_DE_PERIODO_DE_TESTES. */
	private static final String LABEL_PRAZO_DE_PERIODO_DE_TESTES = "Prazo de Período de Testes";

	/** The Constant LABLE_DATA_DE_FIM_DE_PERIODO_DE_TESTES. */
	private static final String LABLE_DATA_DE_FIM_DE_PERIODO_DE_TESTES = "Data de Fim de Período de Testes";

	/** The Constant LABEL_DATA_DE_INICIO_DE_PERIODO_DE_TESTES. */
	private static final String LABEL_DATA_DE_INICIO_DE_PERIODO_DE_TESTES = "Data de Início de Período de Testes";

	/** The Constant LABEL_DIA_DA_COTACAO. */
	private static final String LABEL_DIA_DA_COTACAO = "Dia da Cotação";

	/** The Constant LABEL_DATA_DE_REFERENCIA_CAMBIAL. */
	private static final String LABEL_DATA_DE_REFERENCIA_CAMBIAL = "Data de Referência Cambial";

	/** The Constant LABLE_TARIFA_PARA_PRECO_DO_GAS. */
	private static final String LABLE_TARIFA_PARA_PRECO_DO_GAS = "Tarifa para Preço do Gás";

	/** The Constant LABEL_FASE_DE_VENCIMENTO. */
	private static final String LABEL_FASE_DE_VENCIMENTO = "Fase de Vencimento";

	/** The Constant LABEL_OPCAO_DE_VENCIMENTO. */
	private static final String LABEL_OPCAO_DE_VENCIMENTO = "Opção de Vencimento";

	/** The Constant LABEL_DIA_DO_VENCIMENTO. */
	private static final String LABEL_DIA_DO_VENCIMENTO = "Dia do Vencimento";

	/** The Constant LABEL_ITEM_DA_FATURA. */
	private static final String LABEL_ITEM_DA_FATURA = "Item da Fatura";

	/** The Constant LISTA_DIAS_SELECIONADOS. */
	private static final String LISTA_DIAS_SELECIONADOS = "listaDiasSelecionados";

	/** The Constant LISTA_DIAS_DISPONIVEIS. */
	private static final String LISTA_DIAS_DISPONIVEIS = "listaDiasDisponiveis";

	/** The Constant ENDERECOS. */
	private static final String ENDERECOS = "enderecos";

	/** The Constant PERCENTUAL_QNR. */
	private static final String PERCENTUAL_QNR = "percentualQNR";

	/** The Constant INDICE_PERIODICIDADE_SHIP_OR_PAY. */
	private static final String INDICE_PERIODICIDADE_SHIP_OR_PAY = "indicePeriodicidadeShipOrPay";

	/** The Constant TEMPO_VALIDADE_RETIRADA_QPNR. */
	private static final String TEMPO_VALIDADE_RETIRADA_QPNR = "tempoValidadeRetiradaQPNR";

	/** The Constant DATA_FIM_RETIRADA_QPNR. */
	private static final String DATA_FIM_RETIRADA_QPNR = "dataFimRetiradaQPNR";

	/** The Constant DATA_INICIO_RETIRADA_QPNR. */
	private static final String DATA_INICIO_RETIRADA_QPNR = "dataInicioRetiradaQPNR";

	/** The Constant PERC_FIM_RETIRADA_QPNR. */
	private static final String PERC_FIM_RETIRADA_QPNR = "percFimRetiradaQPNR";

	/** The Constant PERC_MIN_DURANTE_RETIRADA_QPNR. */
	private static final String PERC_MIN_DURANTE_RETIRADA_QPNR = "percMinDuranteRetiradaQPNR";

	/** The Constant PERC_DURANTE_RETIRADA_QPNR. */
	private static final String PERC_DURANTE_RETIRADA_QPNR = "percDuranteRetiradaQPNR";

	/** The Constant INDICE_PERIODICIDADE_TAKE_OR_PAY. */
	private static final String INDICE_PERIODICIDADE_TAKE_OR_PAY = "indicePeriodicidadeTakeOrPay";

	/** The Constant RECUPERACAO_AUTO_TAKE_OR_PAY. */
	private static final String RECUPERACAO_AUTO_TAKE_OR_PAY = "recuperacaoAutoTakeOrPay";

	/** The Constant CONFIRMACAO_AUTOMATICA_QDS. */
	private static final String CONFIRMACAO_AUTOMATICA_QDS = "confirmacaoAutomaticaQDS";

	/** The Constant NUM_MESES_SOLIC_CONSUMO. */
	private static final String NUM_MESES_SOLIC_CONSUMO = "numMesesSolicConsumo";

	/** The Constant DIAS_ANTEC_SOLIC_CONSUMO. */
	private static final String DIAS_ANTEC_SOLIC_CONSUMO = "diasAntecSolicConsumo";

	/** The Constant VARIACAO_SUPERIOR_QDC. */
	private static final String VARIACAO_SUPERIOR_QDC = "variacaoSuperiorQDC";

	/** The Constant QDS_MAIOR_QDC. */
	private static final String QDS_MAIOR_QDC = "qdsMaiorQDC";

	/** The Constant MODALIDADE. */
	private static final String MODALIDADE = "modalidade";

	/** The Constant LABEL_CEP. */
	private static final String LABEL_CEP = "CEP";

	/** The Constant LABEL_FORNECIMENTO_MINIMO_ANUAL. */
	private static final String LABEL_FORNECIMENTO_MINIMO_ANUAL = "Fornecimento Mínimo Anual";

	/** The Constant LABEL_FORNECIMENTO_MINIMO_MENSAL. */
	private static final String LABEL_FORNECIMENTO_MINIMO_MENSAL = "Fornecimento Mínimo Mensal";

	/** The Constant LABEL_FORNECIMENTO_MINIMO_DIARIO. */
	private static final String LABEL_FORNECIMENTO_MINIMO_DIARIO = "Fornecimento Mínimo Diário";

	/** The Constant LABEL_FORNECIMENTO_MAXIMO_DIARIO. */
	private static final String LABEL_FORNECIMENTO_MAXIMO_DIARIO = "Fornecimento Máximo Diário";

	/** The Constant LABEL_HORA_INICIAL_DO_DIA. */
	private static final String LABEL_HORA_INICIAL_DO_DIA = "Hora Inicial do DIA";

	/** The Constant LABEL_TAMANHO_DA_REDUCAO_PARA_RECUPERACAO_DO_PCS. */
	private static final String LABEL_TAMANHO_DA_REDUCAO_PARA_RECUPERACAO_DO_PCS = "Tamanho da Redução para Recuperação do PCS";

	/** The Constant LOCAL_INTERVALO_AMOSTRAGEM. */
	private static final String LOCAL_INTERVALO_AMOSTRAGEM = "Intervalo de Amostragem";

	/** The Constant LABEL_LOCAL_DE_AMOSTRAGEM. */
	private static final String LABEL_LOCAL_DE_AMOSTRAGEM = "Local de Amostragem";

	/** The Constant LABEL_PRESSAO_LIMITE_FORNECIMENTO. */
	private static final String LABEL_PRESSAO_LIMITE_FORNECIMENTO = "Pressão Limite Fornecimento";

	/** The Constant LABEL_PRESSAO_MAXIMA_FORNECIMENTO. */
	private static final String LABEL_PRESSAO_MAXIMA_FORNECIMENTO = "Pressão Máxima Fornecimento";

	/** The Constant LABEL_PRESSAO_MINIMA_FORNECIMENTO. */
	private static final String LABEL_PRESSAO_MINIMA_FORNECIMENTO = "Pressão Mínima Fornecimento";

	/** The Constant LABEL_PRESSAO_COLETADA. */
	private static final String LABEL_PRESSAO_COLETADA = "Pressão Coletada";

	/** The Constant LABEL_VAZAO_INSTANTANEA_MINIMA. */
	private static final String LABEL_VAZAO_INSTANTANEA_MINIMA = "Vazão Instantânea Mínima";

	/** The Constant LABEL_VAZAO_INSTANTANEA_MAXIMA. */
	private static final String LABEL_VAZAO_INSTANTANEA_MAXIMA = "Vazão Instantânea Máxima";

	/** The Constant LABEL_VAZAO_INSTANTANEA. */
	private static final String LABEL_VAZAO_INSTANTANEA = "Vazão Instantânea";

	/** The Constant LABEL_VOLUME_TESTE. */
	private static final String LABEL_VOLUME_TESTE = "Volume Teste";

	/** The Constant LABEL_DATA. */
	private static final String LABEL_DATA = "Data";

	/** The Constant DATA. */
	private static final String DATA = "data";

	/** The Constant LISTA_PENALIDADE_RET_MAIOR_MENOR. */
	private static final String LISTA_PENALIDADE_RET_MAIOR_MENOR = "listaPenalidadeRetMaiorMenor";

	/** The Constant PERC_FIM_RETIRADA_QPNRC. */
	private static final String PERC_FIM_RETIRADA_QPNRC = "percFimRetiradaQPNRC";

	/** The Constant PERC_MIN_DURANTE_RETIRADA_QPNRC. */
	private static final String PERC_MIN_DURANTE_RETIRADA_QPNRC = "percMinDuranteRetiradaQPNRC";

	/** The Constant PERC_DURANTE_RETIRADA_QPNRC. */
	private static final String PERC_DURANTE_RETIRADA_QPNRC = "percDuranteRetiradaQPNRC";

	/** The Constant PERCENTUAL_SOBRE_TARI_GAS. */
	private static final String PERCENTUAL_SOBRE_TARI_GAS = "percentualSobreTariGas";

	/** The Constant RENOVADA. */
	private static final String RENOVADA = "Renovada";

	/** The Constant LABEL_RENOVAVEL. */
	private static final String LABEL_RENOVAVEL = "Renovável";

	/** The Constant LABEL_VALOR_CAMPO. */
	private static final String LABEL_VALOR_CAMPO = "Valor Campo";

	/** The Constant LISTA_CONTRATO_QDC. */
	private static final String LISTA_CONTRATO_QDC = "listaContratoQDC";

	/** The Constant LISTA_CONTRATO_PONTO_CONSUMO. */
	private static final String LISTA_CONTRATO_PONTO_CONSUMO = "listaContratoPontoConsumo";

	/** The Constant LISTA_CONTRATO_PONTO_CONSUMO_PONTO_CONSUMO. */
	private static final String CONTRATO_PONTO_CONSUMO = "listaContratoPontoConsumo.pontoConsumo";

	/** The Constant CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MAX. */
	private static final String CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MAX = "listaContratoPontoConsumo.unidadeVazaoMaximaInstantanea";

	/** The Constant CONTRATO_PONTO_CONSUMO_FAIXA_PRESSAO. */
	private static final String CONTRATO_PONTO_CONSUMO_FAIXA_PRESSAO = "listaContratoPontoConsumo.faixaPressaoFornecimento";

	/** The Constant CONTRATO_CLIENTE_CLIENTE. */
	private static final String CONTRATO_CLIENTE_CLIENTE = "listaContratoCliente.cliente";

	/** The Constant CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MIN. */
	private static final String CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MIN = "listaContratoPontoConsumo.unidadeVazaoMinimaInstantanea";

	/** The Constant PROPOSTA. */
	private static final String PROPOSTA = "proposta";

	/** The Constant INDICADOR_CONTRATO_EXIBIDO. */
	private static final String INDICADOR_CONTRATO_EXIBIDO = "indicadorContratoExibido";

	/** The Constant LABEL_SALDO_MIGRADO. */
	private static final String LABEL_SALDO_MIGRADO = "Saldo Migrado";

	/** The Constant LABEL_VALOR_TRANSFERIDO_QPNR. */
	private static final String LABEL_VALOR_TRANSFERIDO_QPNR = "Valor Transferido QPNR";

	/** The Constant LABEL_SALDO_QPNR. */
	private static final String LABEL_SALDO_QPNR = "Saldo QPNR";

	/** The Constant LABEL_SALDO_QNR. */
	private static final String LABEL_SALDO_QNR = "Saldo QNR";

	/** The Constant LABEL_VALOR_TRANSFERIDO_QNR. */
	private static final String LABEL_VALOR_TRANSFERIDO_QNR = "Valor Transferido QNR";

	/** The Constant ID_CONTRATO. */
	private static final String ID_CONTRATO = "idContrato";

	/** The Constant TIPO_MODELO. */
	private static final String TIPO_MODELO = "tipoModelo";

	/** The Constant HABILITADO. */
	private static final String HABILITADO = "habilitado";

	/** The Constant ABAS. */
	private static final String ABAS = "abas";

	/** The Constant ATRIBUTOS. */
	private static final String ATRIBUTOS = "atributos";

	/** The Constant MODELO_CONTRATO_TIPO_MODELO. */
	private static final String MODELO_CONTRATO_TIPO_MODELO = "modeloContrato.tipoModelo";

	/** The Constant ID_SITUACAO_CONTRATO. */
	private static final String ID_SITUACAO_CONTRATO = "idSituacaoContrato";

	/** The Constant CHAVES_PRIMARIAS. */
	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	/** The Constant CHAVE_PONTO_TRANSFERIR_SALDO. */
	private static final String CHAVE_PONTO_TRANSFERIR_SALDO = "chavePontoTransferirSaldo";

	/** The Constant LISTA_CONTRATOS. */
	private static final String LISTA_CONTRATOS = "listaContratos";

	/** The Constant NUMERO_CONTRATO_MIGRAR. */
	private static final String NUMERO_CONTRATO_MIGRAR = "numeroContratoMigrar";

	/** The Constant LISTA_PONTOS_CONSUMO_TRANSFERIR. */
	private static final String LISTA_PONTOS_CONSUMO_TRANSFERIR = "listaPontosConsumoTransferir";

	/** The Constant IDS_PONTOS_CONSUMO. */
	private static final String IDS_PONTOS_CONSUMO = "idsPontosConsumo";

	/** The Constant LISTA_PONTO_CONSUMO_VENCIMENTO. */
	private static final String LISTA_PONTO_CONSUMO_VENCIMENTO = "listaPontoConsumoVencimento";

	/** The Constant DADOS_PONTO_CONSUMO_VENCIMENTO. */
	private static final String DADOS_PONTO_CONSUMO_VENCIMENTO = "dadosPontoConsumoVencimento";

	/** The Constant SITUACAO_ESCOLHIDA. */
	private static final String SITUACAO_ESCOLHIDA = "situacaoEscolhida";

	/** The Constant CHAVE_CEP. */
	private static final String CHAVE_CEP = "chaveCep";

	/** The Constant PERIODICIDADE_REAV_GARANTIAS. */
	private static final String PERIODICIDADE_REAV_GARANTIAS = "periodicidadeReavGarantias";

	/** The Constant UNIDADE_PRESSAO_FORNEC. */
	private static final String UNIDADE_PRESSAO_FORNEC = "unidadePressaoFornec";

	/** The Constant TODOS_PONTOS_CONSUMO_DADOS_INFORMADOS. */
	private static final String TODOS_PONTOS_CONSUMO_DADOS_INFORMADOS = "todosPontosConsumoDadosInformados";

	/** The Constant ALTERANDO_PONTO_CONSUMO. */
	private static final String ALTERANDO_PONTO_CONSUMO = "alterandoPontoConsumo";

	/** The Constant FAIXA_PRESSAO_FORNECIMENTO. */
	private static final String FAIXA_PRESSAO_FORNECIMENTO = "faixaPressaoFornecimento";

	/** The Constant PERCENTUAL_TARIFA_DOP. */
	private static final String PERCENTUAL_TARIFA_DOP = "percentualTarifaDoP";

	/** The Constant LISTA_AMORTIZACOES. */
	private static final String LISTA_AMORTIZACOES = "listaAmortizacoes";

	/** The Constant SISTEMA_AMORTIZACAO. */
	private static final String SISTEMA_AMORTIZACAO = "sistemaAmortizacao";

	/** The Constant PERCENTUAL_JUROS_FINANCIAMENTO. */
	private static final String PERCENTUAL_JUROS_FINANCIAMENTO = "percentualJurosFinanciamento";

	/** The Constant QTD_PARCELAS_FINANCIAMENTO. */
	private static final String QTD_PARCELAS_FINANCIAMENTO = "qtdParcelasFinanciamento";

	/** The Constant VALOR_PARTICIPACAO_CLIENTE. */
	private static final String VALOR_PARTICIPACAO_CLIENTE = "valorParticipacaoCliente";

	/** The Constant NUM_DIAS_CONSEC_PARADA_CDL. */
	private static final String NUM_DIAS_CONSEC_PARADA_CDL = "numDiasConsecParadaCDL";

	/** The Constant NUM_DIAS_CONSEC_PARADA_CLIENTE. */
	private static final String NUM_DIAS_CONSEC_PARADA_CLIENTE = "numDiasConsecParadaCliente";

	/** The Constant TEMPO_ANTECEDENCIA_REVISAO_GARANTIAS. */
	private static final String TEMPO_ANTECEDENCIA_REVISAO_GARANTIAS = "tempoAntecRevisaoGarantias";

	/** The Constant TEMPO_ANTECEDENCIA_RENOVACAO. */
	private static final String TEMPO_ANTECEDENCIA_RENOVACAO = "tempoAntecedenciaRenovacao";

	/** The Constant IDS_MODALIDADE_CONTRATO. */
	private static final String IDS_MODALIDADE_CONTRATO = "idsModalidadeContrato";

	/** The Constant LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO. */
	private static final String LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO = "listaModalidadeConsumoFaturamentoVO";

	/** The Constant CLIENTE_RESPONSAVEL. */
	private static final String CLIENTE_RESPONSAVEL = "clienteResponsavel";

	/** The Constant DATA_FIM_RELACAO. */
	private static final String DATA_FIM_RELACAO = "dataFimRelacao";

	/** The Constant TIPO_RESPONSABILIDADE. */
	private static final String TIPO_RESPONSABILIDADE = "tipoResponsabilidade";

	/** The Constant DATA_INICIO_RELACAO. */
	private static final String DATA_INICIO_RELACAO = "dataInicioRelacao";

	/** The Constant PERIODICIDADE_TAKE_OR_PAY. */
	private static final String PERIODICIDADE_TAKE_OR_PAY = "periodicidadeTakeOrPay";

	/** The Constant MARGEM_VARIACAO_TAKE_OR_PAY. */
	private static final String MARGEM_VARIACAO_TAKE_OR_PAY = "margemVariacaoTakeOrPay";

	/** The Constant CONSUMO_REFERENCIA_TAKE_OR_PAY. */
	private static final String CONSUMO_REFERENCIA_TAKE_OR_PAY = "consumoReferenciaTakeOrPay";

	/** The Constant PERIODICIDADE_SHIP_OR_PAY. */
	private static final String PERIODICIDADE_SHIP_OR_PAY = "periodicidadeShipOrPay";

	/** The Constant CONSUMO_REFERENCIA_SHIP_OR_PAY. */
	private static final String CONSUMO_REFERENCIA_SHIP_OR_PAY = "consumoReferenciaShipOrPay";

	/** The Constant MARGEM_VARIACAO_SHIP_OR_PAY. */
	private static final String MARGEM_VARIACAO_SHIP_OR_PAY = "margemVariacaoShipOrPay";

	/** The Constant LISTA_COMPROMISSOS_TAKE_OR_PAY_VO. */
	private static final String LISTA_COMPROMISSOS_TAKE_OR_PAY_VO = "listaCompromissosTakeOrPayVO";

	/** The Constant LISTA_COMPROMISSOS_SHIP_OR_PAY_VO. */
	private static final String LISTA_COMPROMISSOS_SHIP_OR_PAY_VO = "listaCompromissosShipOrPayVO";

	/** The Constant IDS_INTERVALO_AMOSTRAGEM_ASSOCIADOS. */
	private static final String IDS_INTERVALO_AMOSTRAGEM_ASSOCIADOS = "idsIntervaloAmostragemAssociados";

	/** The Constant IDS_LOCAL_AMOSTRAGEM_ASSOCIADOS. */
	private static final String IDS_LOCAL_AMOSTRAGEM_ASSOCIADOS = "idsLocalAmostragemAssociados";

	/** The Constant CONSUMO_FAT_FALHA_MEDICAO. */
	private static final String CONSUMO_FAT_FALHA_MEDICAO = "consumoFatFalhaMedicao";

	/** The Constant FATOR_UNICO_CORRECAO. */
	private static final String FATOR_UNICO_CORRECAO = "fatorUnicoCorrecao";

	/** The Constant UNIDADE_FORNEC_MINIMO_ANUAL. */
	private static final String UNIDADE_FORNEC_MINIMO_ANUAL = "unidadeFornecMinimoAnual";

	/** The Constant FORNECIMENTO_MINIMO_ANUAL. */
	private static final String FORNECIMENTO_MINIMO_ANUAL = "fornecimentoMinimoAnual";

	/** The Constant UNIDADE_FORNEC_MINIMO_MENSAL. */
	private static final String UNIDADE_FORNEC_MINIMO_MENSAL = "unidadeFornecMinimoMensal";

	/** The Constant FORNECIMENTO_MINIMO_MENSAL. */
	private static final String FORNECIMENTO_MINIMO_MENSAL = "fornecimentoMinimoMensal";

	/** The Constant UNIDADE_FORNEC_MINIMO_DIARIO. */
	private static final String UNIDADE_FORNEC_MINIMO_DIARIO = "unidadeFornecMinimoDiario";

	/** The Constant FORNECIMENTO_MINIMO_DIARIO. */
	private static final String FORNECIMENTO_MINIMO_DIARIO = "fornecimentoMinimoDiario";

	/** The Constant UNIDADE_FORNEC_MAXIMO_DIARIO. */
	private static final String UNIDADE_FORNEC_MAXIMO_DIARIO = "unidadeFornecMaximoDiario";

	/** The Constant FORNECIMENTO_MAXIMO_DIARIO. */
	private static final String FORNECIMENTO_MAXIMO_DIARIO = "fornecimentoMaximoDiario";

	/** The Constant LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS. */
	private static final String LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS = "listaIntervaloAmostragemAssociados";

	/** The Constant LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS. */
	private static final String LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS = "listaLocalAmostragemAssociados";

	/** The Constant CHAVE_PRIMARIA. */
	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	/** The Constant LISTA_RESPONSABILIDADES. */
	private static final String LISTA_RESPONSABILIDADES = "listaResponsabilidades";

	/** The Constant TARIFA_CONSUMO. */
	private static final String TARIFA_CONSUMO = "tarifaConsumo";

	/** The Constant FAT_PERIODICIDADE. */
	private static final String FAT_PERIODICIDADE = "fatPeriodicidade";

	/** The Constant ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO. */
	private static final String ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO = "arrecadadorConvenioDebitoAutomatico";

	/** The Constant DEPOSITO_IDENTIFICADO. */
	private static final String DEPOSITO_IDENTIFICADO = "depositoIdentificado";

	/** The Constant END_FIS_ENT_FAT_CEP. */
	private static final String END_FIS_ENT_FAT_CEP = "endFisEntFatCEP";

	/** The Constant END_FIS_ENT_FAT_NUMERO. */
	private static final String END_FIS_ENT_FAT_NUMERO = "endFisEntFatNumero";

	/** The Constant END_FIS_ENT_FAT_COMPLEMENTO. */
	private static final String END_FIS_ENT_FAT_COMPLEMENTO = "endFisEntFatComplemento";

	/** The Constant END_FIS_ENT_FAT_EMAIL. */
	private static final String END_FIS_ENT_FAT_EMAIL = "endFisEntFatEmail";

	/** The Constant ENDERECO_PADRAO. */
	private static final String ENDERECO_PADRAO = "enderecoPadrao";

	/** The Constant LISTA_ITEM_FATURAMENTO_CONTRATO_VO. */
	private static final String LISTA_ITEM_FATURAMENTO_CONTRATO_VO = "listaItemFaturamentoContratoVO";

	/** The Constant ITEM_FATURA. */
	protected static final String ITEM_FATURA = "itemFatura";

	/** The Constant DIA_VENCIMENTO_ITEM_FATURA. */
	private static final String DIA_VENCIMENTO_ITEM_FATURA = "diaVencimentoItemFatura";

	/** The Constant OPCAO_VENCIMENTO_ITEM_FATURA. */
	private static final String OPCAO_VENCIMENTO_ITEM_FATURA = "opcaoVencimentoItemFatura";

	/** The Constant FASE_VENCIMENTO_ITEM_FATURA. */
	private static final String FASE_VENCIMENTO_ITEM_FATURA = "faseVencimentoItemFatura";

	/** The Constant INDICADOR_VENC_DIA_NAO_UTIL. */
	private static final String INDICADOR_VENC_DIA_NAO_UTIL = "indicadorVencDiaNaoUtil";

	/** The Constant LISTA_MODALIDADES. */
	private static final String LISTA_MODALIDADES = "listaModalidades";

	/** The Constant INTERVALO_RECUPERACAO_PCS. */
	private static final String INTERVALO_RECUPERACAO_PCS = "intervaloRecuperacaoPCS";

	/** The Constant LOCAL_AMOSTRAGEM_PCS. */
	private static final String LOCAL_AMOSTRAGEM_PCS = "localAmostragemPCS";

	/** The Constant REGIME_CONSUMO. */
	private static final String REGIME_CONSUMO = "regimeConsumo";

	/** The Constant HORA_INICIAL_DIA. */
	private static final String HORA_INICIAL_DIA = "horaInicialDia";

	/** The Constant TAM_REDUCAO_RECUPERACAO_PCS. */
	private static final String TAM_REDUCAO_RECUPERACAO_PCS = "tamReducaoRecuperacaoPCS";

	/** The Constant PERCENTUAL_MULTA. */
	private static final String PERCENTUAL_MULTA = "percentualMulta";

	/** The Constant PERCENTUAL_JUROS_MORA. */
	private static final String PERCENTUAL_JUROS_MORA = "percentualJurosMora";

	/** The Constant MODELO_CONTRATO. */
	private static final String MODELO_CONTRATO = "modeloContrato";

	/** The Constant VALOR_CONTRATO. */
	private static final String VALOR_CONTRATO = "valorContrato";

	/** The Constant LISTA_CONTRATO_CLIENTE. */
	private static final String LISTA_CONTRATO_CLIENTE = "listaContratoCliente";

	/** The Constant LISTA_PONTO_CONSUMO_CONTRATO_VO. */
	protected static final String LISTA_PONTO_CONSUMO_CONTRATO_VO = "listaPontoConsumoContratoVO";

	/** The Constant ID_PONTO_CONSUMO. */
	protected static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	/** The Constant PERIODO_TESTE_DATA_INICIO. */
	private static final String PERIODO_TESTE_DATA_INICIO = "periodoTesteDataInicio";

	/** The Constant PERIODO_TESTE_DATE_FIM. */
	private static final String PERIODO_TESTE_DATE_FIM = "periodoTesteDateFim";

	/** The Constant VOLUME_TESTE. */
	private static final String VOLUME_TESTE = "volumeTeste";

	/** The Constant PRAZO_TESTE. */
	private static final String PRAZO_TESTE = "prazoTeste";

	/** The Constant FAX_DDD. */
	private static final String FAX_DDD = "faxDDD";

	/** The Constant FAX_NUMERO. */
	private static final String FAX_NUMERO = "faxNumero";

	/** The Constant INICIO_GARANTIA_CONVERSAO. */
	private static final String INICIO_GARANTIA_CONVERSAO = "inicioGarantiaConversao";

	/** The Constant NUMERO_DIAS_GARANTIA. */
	private static final String NUMERO_DIAS_GARANTIA = "numeroDiasGarantia";

	/** The Constant EXIGE_APROVACAO. */
	private static final String EXIGE_APROVACAO = "exigeAprovacao";

	/** The Constant NUM_DIAS_RENO_AUTO_CONTRATO. */
	private static final String NUM_DIAS_RENO_AUTO_CONTRATO = "numDiasRenoAutoContrato";

	/** The Constant CHAVE_PRIMARIA_PRINCIPAL. */
	private static final String CHAVE_PRIMARIA_PRINCIPAL = "chavePrimariaPrincipal";

	/** The Constant DATA_REFERENCIA_CAMBIAL. */
	private static final String DATA_REFERENCIA_CAMBIAL = "dataReferenciaCambial";

	/** The Constant DIA_COTACAO. */
	private static final String DIA_COTACAO = "diaCotacao";

	/** The Constant PERC_MINIMO_QDC. */
	private static final String PERC_MINIMO_QDC = "percminimoQDC";

	/** The Constant TIPO_AGRUPAMENTO. */
	private static final String TIPO_AGRUPAMENTO = "tipoAgrupamento";

	/** The Constant CONTRATO_COMPRA. */
	private static final String CONTRATO_COMPRA = "contratoCompra";

	/** The Constant VAZAO_INSTANTANEA. */
	private static final String VAZAO_INSTANTANEA = "vazaoInstantanea";

	/** The Constant UNIDADE_VAZAO_INSTAN. */
	private static final String UNIDADE_VAZAO_INSTAN = "unidadeVazaoInstan";

	/** The Constant VAZAO_INSTANTANEA_MAXIMA. */
	private static final String VAZAO_INSTANTANEA_MAXIMA = "vazaoInstantaneaMaxima";

	/** The Constant UNIDADE_VAZAO_INSTAN_MAXIMA. */
	private static final String UNIDADE_VAZAO_INSTAN_MAXIMA = "unidadeVazaoInstanMaxima";

	/** The Constant VAZAO_INSTANTANEA_MINIMA. */
	private static final String VAZAO_INSTANTANEA_MINIMA = "vazaoInstantaneaMinima";

	/** The Constant UNIDADE_VAZAO_INSTAN_MINIMA. */
	private static final String UNIDADE_VAZAO_INSTAN_MINIMA = "unidadeVazaoInstanMinima";

	/** The Constant PRESSAO_MINIMA_FORNECIMENTO. */
	private static final String PRESSAO_MINIMA_FORNECIMENTO = "pressaoMinimaFornecimento";

	/** The Constant UNIDADE_PRESSAO_MINIMA_FORNEC. */
	private static final String UNIDADE_PRESSAO_MINIMA_FORNEC = "unidadePressaoMinimaFornec";

	/** The Constant PRESSAO_COLETADA. */
	private static final String PRESSAO_COLETADA = "pressaoColetada";

	/** The Constant UNIDADE_PRESSAO_COLETADA. */
	private static final String UNIDADE_PRESSAO_COLETADA = "unidadePressaoColetada";

	/** The Constant PRESSAO_MAXIMA_FORNECIMENTO. */
	private static final String PRESSAO_MAXIMA_FORNECIMENTO = "pressaoMaximaFornecimento";

	/** The Constant UNIDADE_PRESSAO_MAXIMA_FORNEC. */
	private static final String UNIDADE_PRESSAO_MAXIMA_FORNEC = "unidadePressaoMaximaFornec";

	/** The Constant PRESSAO_LIMITE_FORNECIMENTO. */
	private static final String PRESSAO_LIMITE_FORNECIMENTO = "pressaoLimiteFornecimento";

	/** The Constant UNIDADE_PRESSAO_LIMITE_FORNEC. */
	private static final String UNIDADE_PRESSAO_LIMITE_FORNEC = "unidadePressaoLimiteFornec";

	/** The Constant NUM_ANOS_CTRL_PARADA_CLIENTE. */
	private static final String NUM_ANOS_CTRL_PARADA_CLIENTE = "numAnosCtrlParadaCliente";

	/** The Constant MAX_TOTAL_PARADAS_CLIENTE. */
	private static final String MAX_TOTAL_PARADAS_CLIENTE = "maxTotalParadasCliente";

	/** The Constant MAX_ANUAL_PARADAS_CLIENTE. */
	private static final String MAX_ANUAL_PARADAS_CLIENTE = "maxAnualParadasCliente";

	/** The Constant NUM_DIAS_PROGR_PARADA_CLIENTE. */
	private static final String NUM_DIAS_PROGR_PARADA_CLIENTE = "numDiasProgrParadaCliente";

	/** The Constant NUM_ANOS_CTRL_PARADA_CDL. */
	private static final String NUM_ANOS_CTRL_PARADA_CDL = "numAnosCtrlParadaCDL";

	/** The Constant MAX_TOTAL_PARADAS_CDL. */
	private static final String MAX_TOTAL_PARADAS_CDL = "maxTotalParadasCDL";

	/** The Constant MAX_ANUAL_PARADAS_CDL. */
	private static final String MAX_ANUAL_PARADAS_CDL = "maxAnualParadasCDL";

	/** The Constant NUM_DIAS_PROGR_PARADA_CDL. */
	private static final String NUM_DIAS_PROGR_PARADA_CDL = "numDiasProgrParadaCDL";

	/** The Constant DATA_ADITIVO. */
	private static final String DATA_ADITIVO = "dataAditivo";

	/** The Constant NUMERO_ADITIVO. */
	private static final String NUMERO_ADITIVO = "numeroAditivo";

	/** The Constant DESCRICAO_ADITIVO. */
	private static final String DESCRICAO_ADITIVO = "descricaoAditivo";

	/** The Constant CLIENTE_ASSINATURA. */
	private static final String CLIENTE_ASSINATURA = "clienteAssinatura";

	/** The Constant ID_MODELO_CONTRATO. */
	private static final String ID_MODELO_CONTRATO = "idModeloContrato";

	/** The Constant LISTA_INDICE_CORRECAO_MONETARIA. */
	private static final String LISTA_INDICE_CORRECAO_MONETARIA = "listaIndiceCorrecaoMonetaria";

	/** The Constant LISTA_INTERVALO_PCS. */
	private static final String LISTA_INTERVALO_PCS = "listaIntervaloPCS";

	/** The Constant LISTA_LOCAL_AMOSTRAGEM_PCS. */
	private static final String LISTA_LOCAL_AMOSTRAGEM_PCS = "listaLocalAmostragemPCS";

	/** The Constant LISTA_CONTRATO_MODALIDADE. */
	private static final String LISTA_CONTRATO_MODALIDADE = "listaContratoModalidade";

	/** The Constant LISTA_FAIXA_PENALIDADE_SOB_DEM. */
	private static final String LISTA_FAIXA_PENALIDADE_SOB_DEM = "listaFaixaPenalidadeSobDem";

	/** The Constant LISTA_FAIXA_PENALIDADE_SOBRE_DEM. */
	private static final String LISTA_FAIXA_PENALIDADE_SOBRE_DEM = "listaFaixaPenalidadeSobreDem";

	/** The Constant LISTA_CONSUMO_REFERENCIAL. */
	private static final String LISTA_CONSUMO_REFERENCIAL = "listaConsumoReferencial";

	/** The Constant LISTA_PERIODICIDADE_TAKE_OR_PAY. */
	private static final String LISTA_PERIODICIDADE_TAKE_OR_PAY = "listaPeriodicidadeTakeOrPay";

	/** The Constant LISTA_PENALIDADE_RETIRADA_MAIOR_MENOR. */
	private static final String LISTA_PENALIDADE_RETIRADA_MAIOR_MENOR = "listaPenalidadeRetiradaMaiorMenor";

	/** The Constant LISTA_PEREIODICIDADE_RETIRADA_MAIOR_MENOR. */
	private static final String LISTA_PEREIODICIDADE_RETIRADA_MAIOR_MENOR = "listaPeriodicidadeRetMaiorMenor";

	/** The Constant LISTA_REFERENCIA_QF_PARADA_PROGRAMADA. */
	private static final String LISTA_REFERENCIA_QF_PARADA_PROGRAMADA = "listaReferenciaQFParadaProgramada";

	/** The Constant LISTA_PERIODICIDADE_SHIP_OR_PAY. */
	private static final String LISTA_PERIODICIDADE_SHIP_OR_PAY = "listaPeriodicidadeShipOrPay";

	/** The Constant LISTA_PERIODICIDADE. */
	private static final String LISTA_PERIODICIDADE = "listaPeriodicidade";

	/** The Constant LISTA_FORMA_COBRANCA. */
	private static final String LISTA_FORMA_COBRANCA = "listaFormaCobranca";

	/** The Constant LISTA_ARRECADADOR_CONVENIO. */
	private static final String LISTA_ARRECADADOR_CONVENIO = "listaArrecadadorConvenio";

	/** The Constant LISTA_ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO. */
	private static final String LISTA_ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO = "listaArrecadadorConvenioDebitoAutomatico";

	/** The Constant LISTA_ACAO_ANORMALIDADE_CONSUMO. */
	private static final String LISTA_ACAO_ANORMALIDADE_CONSUMO = "listaAcaoAnormalidadeConsumo";

	/** The Constant LISTA_UNIDADE_VOLUME. */
	private static final String LISTA_UNIDADE_VOLUME = "listaUnidadeVolume";

	/** The Constant LISTA_ITEM_FATURA. */
	private static final String LISTA_ITEM_FATURA = "listaItemFatura";

	private static final String LISTA_ENDERECO_PADRAO = "listaEnderecoPadrao";

	/** The Constant LISTA_UNIDADE_VAZAO. */
	private static final String LISTA_UNIDADE_VAZAO = "listaUnidadeVazao";

	/** The Constant LISTA_UNIDADE_PRESSAO. */
	private static final String LISTA_UNIDADE_PRESSAO = "listaUnidadePressao";

	/** The Constant LISTA_FORMA_PAGAMENTO. */
	private static final String LISTA_FORMA_PAGAMENTO = "listaFormaPagamento";

	/** The Constant LISTA_OPCAO_FASE_REF_VENCIMENTO. */
	private static final String LISTA_OPCAO_FASE_REF_VENCIMENTO = "listaOpcaoFaseRefVencimento";

	/** The Constant LISTA_FASE_REFERENCIAL_VENCIMENTO. */
	private static final String LISTA_FASE_REFERENCIAL_VENCIMENTO = "listaFaseReferencialVencimento";

	/** The Constant LISTA_TIPO_FATURAMENTO. */
	private static final String LISTA_TIPO_FATURAMENTO = "listaTipoFaturamento";

	/** The Constant LISTA_CRITERIO_USO. */
	private static final String LISTA_CRITERIO_USO = "listaCriterioUso";

	/** The Constant LISTA_REGIME_CONSUMO. */
	private static final String LISTA_REGIME_CONSUMO = "listaRegimeConsumo";

	/** The Constant LISTA_HORA_INICIAL_DIA. */
	private static final String LISTA_HORA_INICIAL_DIA = "listaHoraInicialDia";

	/** The Constant LISTA_TIPO_GARANTIA_FINANCEIRA. */
	private static final String LISTA_TIPO_GARANTIA_FINANCEIRA = "listaTipoGarantiaFinanceira";

	/** The Constant LISTA_GARANTIA_FINANCEIRA_RENOVADA. */
	private static final String LISTA_GARANTIA_FINANCEIRA_RENOVADA = "listaGarantiaFinanceiraRenovada";

	/** The Constant LISTA_SITUACAO_CONTRATO. */
	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";

	/** The Constant LISTA_MODELOS_CONTRATO. */
	private static final String LISTA_MODELOS_CONTRATO = "listaModelosContrato";

	/** The Constant LISTA_TIPO_CONTRATO. */
	private static final String LISTA_TIPO_CONTRATO = "listaTipoContrato";

	/** The Constant LISTA_IMOVEL_PONTO_CONSUMO_VO. */
	private static final String LISTA_IMOVEL_PONTO_CONSUMO_VO = "listaImovelPontoConsumoVO";

	/** The Constant LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO. */
	protected static final String LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO = "listaImovelPontoConsumoSelecionadoVO";

	/** The Constant LISTA_PONTO_CONSUMO. */
	public static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	/** The Constant LISTA_DATA_REFERENCIA_CAMBIAL. */
	private static final String LISTA_DATA_REFERENCIA_CAMBIAL = "listaDataReferenciaCambial";

	/** The Constant LISTA_DIA_COTACAO. */
	private static final String LISTA_DIA_COTACAO = "listaDiaCotacao";

	/** The Constant LISTA_TIPO_AGRUPAMENTO. */
	private static final String LISTA_TIPO_AGRUPAMENTO = "listaTipoAgrupamento";

	/** The Constant LISTA_VERSOES_PROPOSTA. */
	private static final String LISTA_VERSOES_PROPOSTA = "listaVersoesProposta";

	/** The Constant LISTA_CONTRATO_COMPRA. */
	private static final String LISTA_CONTRATO_COMPRA = "listaContratoCompra";

	/** The Constant LISTA_TIPO_MULTA_RECISORIA. */
	private static final String LISTA_TIPO_MULTA_RECISORIA = "listaTipoMultaRecisoria";

	/** The Constant ECONOMIA_ESTIMADA_GN_ANO. */
	private static final String ECONOMIA_ESTIMADA_GN_ANO = "economiaEstimadaGNAno";

	/** The Constant ECONOMIA_ESTIMADA_GN_MES. */
	private static final String ECONOMIA_ESTIMADA_GN_MES = "economiaEstimadaGNMes";

	/** The Constant GASTO_ESTIMADO_GN_MES. */
	private static final String GASTO_ESTIMADO_GN_MES = "gastoEstimadoGNMes";

	/** The Constant CONTRATO. */
	private static final String CONTRATO = "contrato";

	/** The Constant DATA_FINAL_GARANTIA_FINANCEIRA. */
	private static final String DATA_FINAL_GARANTIA_FINANCEIRA = "dataFinalGarantiaFinanceira";

	/** The Constant DATA_INICIO_GARANTIA_FINANCEIRA. */
	private static final String DATA_INICIO_GARANTIA_FINANCEIRA = "dataInicioGarantiaFinanceira";

	/** The Constant VALOR_GARANTIA_FINANCEIRA. */
	private static final String VALOR_GARANTIA_FINANCEIRA = "valorGarantiaFinanceira";

	/** The Constant SITUACAO_CONTRATO. */
	public static final String SITUACAO_CONTRATO = "situacaoContrato";

	/** The Constant DADOS_CALCULO_INDENIZACAO. */
	private static final String DADOS_CALCULO_INDENIZACAO = "dadosCalculoIndenizacao";

	/** The Constant ID_CLIENTE. */
	private static final String ID_CLIENTE = "idCliente";

	/** The Constant ID_IMOVEL. */
	private static final String ID_IMOVEL = "idImovel";

	/** The Constant QDC_CONTRATO. */
	private static final String QDC_CONTRATO = "QDCContrato";

	/** The Constant NUMERO_PROPOSTA. */
	private static final String NUMERO_PROPOSTA = "numeroProposta";

	/** The Constant PROPOSTA_APROVADA. */
	private static final String PROPOSTA_APROVADA = "propostaAprovada";

	/** The Constant INDEX_LISTA. */
	private static final String INDEX_LISTA = "indexLista";

	/** The Constant ABA_RESPONSABILIDADE. */
	private static final String ABA_RESPONSABILIDADE = "7";

	/** The Constant SUCESSO_MANUTENCAO_LISTA. */
	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	/** The Constant CONTRATO_SEM_MULTA_RECISORIA. */
	private static final String CONTRATO_SEM_MULTA_RECISORIA = "CONTRATO_SEM_MULTA_RECISORIA";

	/** The Constant SUCESSO_ENCERRAR_RESCINDIR. */
	private static final String SUCESSO_ENCERRAR_RESCINDIR = "SUCESSO_ENCERRAR_RESCINDIR";

	/** The Constant SUCESSO_ORDENAR_FATURAMENTO_CONTRATO_COMPLEMENTAR. */
	private static final String SUCESSO_ORDENAR_FATURAMENTO_CONTRATO_COMPLEMENTAR = "SUCESSO_ORDENAR_FATURAMENTO_CONTRATO_COMPLEMENTAR";

	/** The Constant SUCESSO_APROVAR_CONTRATO. */
	private static final String SUCESSO_APROVAR_CONTRATO = "SUCESSO_APROVAR_CONTRATO";

	/** The Constant NENHUM_CONTRATO_AGUARDANDO_APROVACAO. */
	private static final String NENHUM_CONTRATO_AGUARDANDO_APROVACAO = "NENHUM_CONTRATO_AGUARDANDO_APROVACAO";

	/** The Constant VALOR_INVESTIMENTO. */
	private static final String VALOR_INVESTIMENTO = "valorInvestimento";

	/** The Constant LISTA_BASE_APURACAO. */
	private static final String LISTA_BASE_APURACAO = "listaBaseApuracao";

	/** The Constant DADOS_RESUMO_FATURA. */
	private static final String DADOS_RESUMO_FATURA = "dadosResumoFatura";

	/** The Constant NAO_SELECIONADO. */
	private static final String NAO_SELECIONADO = NUMERO_MENOS_UM;

	/** The Constant NOTA_FISCAL_ELETRONICA. */
	private static final String NOTA_FISCAL_ELETRONICA = "indicadorNFE";

	/** The Constant LISTA_TARIFAS. */
	private static final String LISTA_TARIFAS = "listaTarifas";

	/** The Constant EMITIR_FATURA_COM_NFE. */
	private static final String EMITIR_FATURA_COM_NFE = "emitirFaturaComNfe";

	/** The Constant FORWARD_SUCESSO_RESCINDIR. */
	protected static final String FORWARD_SUCESSO_RESCINDIR = "sucesso_rescindir";

	/** The Constant NOTA_DEBITO. */
	protected static final String NOTA_DEBITO = "notaDebito";

	/** The Constant LISTA_PONTO_CONSUMO_VO. */
	private static final String LISTA_PONTO_CONSUMO_VO = "listaPontoConsumoVO";

	/** The Constant LISTA_TIPO_APURACAO. */
	private static final String LISTA_TIPO_APURACAO = "listaTipoApuracao";

	/** The Constant LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO. */
	private static final String LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO = "listaCompromissosTakeOrPayCVO";

	/** The Constant LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO. */
	private static final String LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO = "listaPenalidadesRetMaiorMenorVO";

	/** The Constant PENALIDADE_RETIRADA_MAIOR_MENOR. */
	private static final String PENALIDADE_RETIRADA_MAIOR_MENOR = "penalidadeRetMaiorMenor";

	/** The Constant PERIODICIDADE_RETIRADA_MAIOR_MENOR. */
	private static final String PERIODICIDADE_RETIRADA_MAIOR_MENOR = "periodicidadeRetMaiorMenor";

	/** The Constant BASE_APURACAO_RETIRADA_MAIOR_MENOR. */
	private static final String BASE_APURACAO_RETIRADA_MAIOR_MENOR = "baseApuracaoRetMaiorMenor";

	/** The Constant DATA_INICIO_RETIRADA_MAIOR_MENOR. */
	private static final String DATA_INICIO_RETIRADA_MAIOR_MENOR = "dataIniVigRetirMaiorMenor";

	/** The Constant DATA_FIM_RETIRADA_MAIOR_MENOR. */
	private static final String DATA_FIM_RETIRADA_MAIOR_MENOR = "dataFimVigRetirMaiorMenor";

	/** The Constant PRECO_COBRANCA_RETIRADA_MAIOR_MENOR. */
	private static final String PRECO_COBRANCA_RETIRADA_MAIOR_MENOR = "precoCobrancaRetirMaiorMenor";

	/** The Constant FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR. */
	private static final String FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR = "tipoApuracaoRetirMaiorMenor";

	/** The Constant PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR. */
	private static final String PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR = "percentualCobRetMaiorMenor";

	/** The Constant PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR. */
	private static final String PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR = "percentualCobIntRetMaiorMenor";

	/** The Constant CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR. */
	private static final String CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR = "consumoReferenciaRetMaiorMenor";

	/** The Constant PERCENTUAL_RETIRADA_MAIOR_MENOR. */
	private static final String PERCENTUAL_RETIRADA_MAIOR_MENOR = "percentualRetMaiorMenor";

	/** The Constant PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE = "penalidadeRetMaiorMenorM";

	/** The Constant PERIODICIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String PERIODICIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE = "periodicidadeRetMaiorMenorM";

	/** The Constant BASE_APURACAO_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String BASE_APURACAO_RETIRADA_MAIOR_MENOR_MODALIDADE = "baseApuracaoRetMaiorMenorM";

	/** The Constant DATA_INICIO_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String DATA_INICIO_RETIRADA_MAIOR_MENOR_MODALIDADE = "dataIniVigRetirMaiorMenorM";

	/** The Constant DATA_FIM_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String DATA_FIM_RETIRADA_MAIOR_MENOR_MODALIDADE = "dataFimVigRetirMaiorMenorM";

	/** The Constant PRECO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String PRECO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE = "precoCobrancaRetirMaiorMenorM";

	/** The Constant FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE = "tipoApuracaoRetirMaiorMenorM";

	/** The Constant PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE = "percentualCobRetMaiorMenorM";

	/**
	 * The Constant PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR_MODALIDADE.
	 */
	private static final String PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR_MODALIDADE = "percentualCobIntRetMaiorMenorM";

	/** The Constant CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR_MODALIDADE = "consumoReferRetMaiorMenorM";

	/** The Constant PERCENTUAL_RETIRADA_MAIOR_MENOR_MODALIDADE. */
	private static final String PERCENTUAL_RETIRADA_MAIOR_MENOR_MODALIDADE = "percentualRetMaiorMenorM";

	/** The Constant INDICADOR_IMPOSTO_MAIOR_MENOR. */
	private static final String INDICADOR_IMPOSTO_MAIOR_MENOR = "indicadorImposto";

	/** The Constant INDICADOR_IMPOSTO_MAIOR_MENOR_MODALIDADE. */
	private static final String INDICADOR_IMPOSTO_MAIOR_MENOR_MODALIDADE = "indicadorImpostoM";

	/** The Constant LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO. */
	private static final String LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO = "listaPenalidadesRetMaiorMenorModalidadeVO";

	/** The Constant OBSERVACAO_NOTA_DEBITO. */
	private static final String OBSERVACAO_NOTA_DEBITO = "Nota de débito gerada automaticamente";

	/** The Constant QUANTIDADE_FATURA. */
	public static final BigDecimal QUANTIDADE_FATURA = new BigDecimal(NUMERO_UM);

	public static final String CONTRATO_PARCIAL = "Contrato Parcial";

	/** The Constant ABA_ATUAL. */
	public static final String ABA_ATUAL = "abaAtual";

	/** The Constant ABA_DADOS_GERAIS. */
	public static final String ABA_DADOS_GERAIS = "0";

	/** The Constant ABA_PROPOSTA. */
	public static final String ABA_PROPOSTA = "1";

	/** The Constant ABA_DADOS_FINANCEIROS. */
	public static final String ABA_DADOS_FINANCEIROS = "2";

	/** The Constant ABA_TAKE_OR_PAY. */
	public static final String ABA_TAKE_OR_PAY = "3";

	/** The Constant ABA_PENALIDADES. */
	public static final String ABA_PENALIDADES = "4";

	/** The Constant ABA_PONTOS_DE_CONSUMO. */
	public static final String ABA_PONTOS_DE_CONSUMO = "5";

	/** The Constant ABA_DOCUMENTO_ANEXO. */
	public static final String ABA_DOCUMENTO_ANEXO = "6";

	public static final String PONTO_CONSUMO_EM_ALTERACAO = "pontoConsumoEmAlteracao";

	/** The Constant ADITAR. */
	public static final String ADITAMENTO = "aditar";

	/** The Constant ALTERAR. */
	public static final String ALTERACAO = "alterar";

	/** The Constant CEM. */
	public static final Integer CEM = 100;

	/** The Constant DUAS_CASAS_DECIMAIS. */
	public static final Integer DUAS_CASAS_DECIMAIS = 2;

	/** The Constant LIMPAR_PREENCHIMENTO_PONTO_CONSUMO. */
	public static final String LIMPAR_PREENCHIMENTO_PONTO_CONSUMO = "limparPreenchimentoPontoConsumo";

	/** The Constant FATURAVEL. */
	private static final String FATURAVEL = "faturavel";

	/** The Constant LISTA_CONTRATO_DADOS_FATURAMENTO_VO. */
	private static final String LISTA_CONTRATO_DADOS_FATURAMENTO_VO = "listaContratoDadosFaturamentoVO";
	/** The Constant EXTENSAO. */
	/** The Constant BANCO. */
	private static final String BANCO = "banco";

	/** The Constant BANCO. */
	private static final String AGENCIA = "agencia";

	/** The Constant BANCO. */
	private static final String CONTA_CORRENTE = "contaCorrente";

	/** The Constant LISTA_FORMA_COBRANCA. */
	private static final String LISTA_BANCO = "listaBanco";

	/** The Constant FORMATO_DATA_BR. */
	public static final String FORMATO_DATA_BR = "dd/MM/yyyy";

	private static final String[] EXTENSAO =
			{ ".PDF", ".TXT", ".DOC", ".DOCX", ".XLS", ".XLSX", ".PNG", ".JPG", ".JPEG", ".BMP", ".PPT", ".PPTX" };
	
	private static final String URL_RETORNO_ATUALIZACAO_CASTRAL = "urlRetornoAtualizacaoCastral";

	public static final long ADITADO = 4;
	public static final long ALTERADO = 9;
	public static final long EM_NEGOCIACAO = 5;
	public static final long ATIVO = 1;
	private static final int ENCERRADO = 2;
	public static final String LOCK_INTEGRACAO_CONTRATO = "INTEGRACAO_CONTRATO";

	private boolean chamadoTela = false;
	private Contrato contratoTitularidade;
	private Contrato contratoCopia;
	
	@Autowired
	private ControladorModeloContrato controladorModeloContrato;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorProposta controladorProposta;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	private ControladorBanco controladorBanco;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorArrecadadorConvenio controladorArrecadadorConvenio;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorEndereco controladorEndereco;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorClienteImovel controladorClienteImovel;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorContratoAdequacaoParceria controladorContratoAdequacaoParceria;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorRubrica controladorRubrica;
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorFuncionario controladorFuncionario;
	
	@Autowired
	private ControladorRelatorio controladorRelatorio;
	
	/**
	 * Init Binder
	 * @param binder - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaContrato")
	public String exibirPesquisaContrato(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		limparDadosSessao(request);

		try {
			carregarDados(request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("contratoVO", contratoVO);

		return "exibirPesquisaContrato";
	}

	/**
	 * Método responsável por pesquisar contratos.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarContrato")
	public String pesquisarContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {
		
		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		
		if(contratoVO.getContrato() != null && contratoVO.getContrato().getChavePrimaria() > 0) {
			contratoVO.setChavePrimaria(contratoVO.getContrato().getChavePrimaria());
		}
	

		Map<String, Object> filtro = new HashMap<String, Object>();
		try {
			this.prepararFiltro(filtro, contratoVO, request);
			super.adicionarFiltroPaginacao(request, filtro);
			Collection<Contrato> listaContratos = controladorContrato.consultarContratos(filtro);
			if (listaContratos != null && !listaContratos.isEmpty()) {
				model.addAttribute(LISTA_CONTRATOS, super.criarColecaoPaginada(request, filtro, listaContratos));
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaContrato(contratoVO, request, model);
	}

	/**
	 * Método responsável por exibir a primeira etapa do detalhamento de um contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoContrato")
	public String exibirDetalhamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Long chavePrimaria = contratoVO.getChavePrimaria();
		contratoVO.setFluxoDetalhamento(true);

		try {
			Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria);
			contratoVO.setAgencia(contrato.getAgencia());
			contratoVO.setContaCorrente(contrato.getContaCorrente());
			view = exibirAditamentoContrato(contratoVO, result, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
			view = pesquisarContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a segunda etapa do detalhamento de um contrato.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoContratoPontoConsumo")
	public String exibirDetalhamentoContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		String view = "exibirDetalhamentoContratoPontoConsumo";
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		contratoVO.setFluxoDetalhamento(true);
		contratoVO.setFluxoAlteracao(false);

		contratoVO.setQDCContrato(null);

		try {

			controladorContrato.validarSelecaoImovelPontoConsumo(contratoVO.getChavesPrimariasPontoConsumoVO());

			Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
			contratoVO.setChavePrimaria(contrato.getChavePrimaria());

			popularContratoPasso1(contratoVO, contrato, request, true, true, false, true, model);
			carregarCamposPasso2(request, contratoVO, model);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				carregarCampos(contratoVO, modeloContrato, request, model);
			}

			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			request.getSession().setAttribute(CONTRATO, contrato);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirDetalhamentoContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por excluir os contrato selecionados.
	 * 
	 * @param contratoVO {@link}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirContrato")
	public String excluirContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		List<Long> chavesPrimarias = Arrays.asList((Long[]) contratoVO.getChavesPrimarias());

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {

			this.prepararFiltro(filtro, contratoVO, request);

			Collection<Contrato> listaContratos = controladorContrato.consultarContratos(filtro);

			for (Contrato contrato : listaContratos) {
				if (controladorIntegracao.integracaoPrecedenteContratoAtiva()
						&& !controladorIntegracao.validarIntegracaoContrato(contrato.getChavePrimaria(), contrato.getChavePrimariaPai())) {
					throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);

				}
			}

			controladorContrato.validarRemoverContratos(chavesPrimarias);
			controladorContrato.remover(chavesPrimarias, getDadosAuditoria(request));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Contrato.CONTRATOS);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
		}

		return pesquisarContrato(contratoVO, result, request, model);
	}

	/**
	 * Carregar dados.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarDados(HttpServletRequest request, Model model) throws GGASException {

		ControladorModeloContrato controladorModeloContrato = (ControladorModeloContrato) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorModeloContrato.BEAN_ID_CONTROLADOR_MODELO_CONTRATO);
		
		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		
		
		model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));

		request.getSession().setAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));
		model.addAttribute(LISTA_TIPO_CONTRATO, controladorEntidadeConteudo.obterListaTipoModelo());

	}

	/**
	 * Limpar dados sessao.
	 *
	 * @param request {@link HttpServletRequest}
	 */
	private void limparDadosSessao(HttpServletRequest request) {

		request.getSession().removeAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO);
		request.getSession().removeAttribute(CONTRATO);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);
		request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);
		request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
		request.getSession().removeAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);
		request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);
		request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);
		request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);
		request.getSession().removeAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO);
		request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
		request.getSession().removeAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_VENCIMENTO);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_VO);
		request.getSession().removeAttribute(IDS_PONTOS_CONSUMO);
		request.getSession().removeAttribute(LISTA_PONTOS_CONSUMO_TRANSFERIR);
		request.getSession().removeAttribute(NUMERO_CONTRATO_MIGRAR);
		request.getSession().removeAttribute(LISTA_CONTRATOS);
		request.getSession().removeAttribute(CHAVE_PONTO_TRANSFERIR_SALDO);
		request.getSession().removeAttribute(LISTA_ANEXOS);
		request.getSession().removeAttribute(SITUACAO_ESCOLHIDA);
		request.getSession().removeAttribute(SITUACAO_CONTRATO);

	}

	/**
	 * Pesquisar contrato migrar.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarContratoMigrar")
	public String pesquisarContratoMigrar(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model) {

		try {
			model.addAttribute("contratoVO", contratoVO);
			if (contratoVO.getChavePrimaria() != null) {
				Long[] chaves = { contratoVO.getChavePrimaria() };
				contratoVO.setChavesPrimarias(chaves);
			} else {
				contratoVO.setChavesPrimarias(null);
			}

			Map<String, Object> filtro = new HashMap<String, Object>();

			this.prepararFiltroMigrar(filtro, contratoVO, request);
			filtro.put(ID_SITUACAO_CONTRATO, new Long[] { ATIVO, ADITADO, EM_NEGOCIACAO, ALTERADO });

			super.adicionarFiltroPaginacao(request, filtro);

			Collection<Contrato> listaContratos = null;

			if (contratoVO.getFluxoPesquisa()) {
				listaContratos = controladorContrato.consultarContratos(filtro);
				request.getSession().removeAttribute(LISTA_PONTOS_CONSUMO_TRANSFERIR);
			} else {
				listaContratos = (Collection<Contrato>) request.getSession().getAttribute(LISTA_CONTRATOS);
			}

			if (listaContratos != null && !listaContratos.isEmpty()) {
				model.addAttribute(LISTA_CONTRATOS, super.criarColecaoPaginada(request, filtro, listaContratos));
			} else {
				model.addAttribute("pageSize", 0);
			}

			request.getSession().setAttribute(LISTA_CONTRATOS, listaContratos);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirMigrarSaldo";
	}

	/**
	 * Preparar filtro migrar.
	 *
	 * @param filtro {@link Map}
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltroMigrar(Map<String, Object> filtro, ContratoVO contratoVO, HttpServletRequest request) throws GGASException {

		prepararFiltros(filtro, contratoVO);

	}

	/**
	 * Pesquisar imovel contrato migrar.
	 * 
	 * @param contratoVO - {@link ContratoVO}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarImovelContratoMigrar")
	public String pesquisarImovelContratoMigrar(ContratoVO contratoVO, Model model, HttpServletRequest request) {

		try {
			Long chavePrimariaContratoMigrar = contratoVO.getChavePrimariaContratoMigrar();

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_CONTRATO, chavePrimariaContratoMigrar);

			Collection<PontoConsumo> listaPontoConsumo =
					(Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTOS_CONSUMO_TRANSFERIR);

			if (listaPontoConsumo == null || listaPontoConsumo.isEmpty()) {
				listaPontoConsumo = controladorContrato.obterPontosConsumoContrato(filtro);
			}

			request.getSession().setAttribute(LISTA_PONTOS_CONSUMO_TRANSFERIR, listaPontoConsumo);
			request.setAttribute(LISTA_PONTOS_CONSUMO_TRANSFERIR, listaPontoConsumo);

			contratoVO.setFluxoPesquisa(Boolean.FALSE);
			contratoVO.setValorTransferidoQNR(contratoVO.getSaldoQNR());
			contratoVO.setValorTransferidoQPNR(contratoVO.getSaldoQPNR());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.pesquisarContratoMigrar(contratoVO, null, request, model);
	}

	/**
	 * Método responsável por exibir dados do faturamento dos contrato selecionados.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDadosFaturamentoContratoPopup")
	public String exibirDadosFaturamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		List<ContratoDadosFaturamentoVO> listaContratoDadosFaturamentoVO = new ArrayList<ContratoDadosFaturamentoVO>();
		model.addAttribute("contratoVO", contratoVO);
		try {
			for (Long idContrato : contratoVO.getChavesPrimarias()) {
				Contrato contrato = controladorContrato.obterContratoListasCarregadas(idContrato, true);
				if (contrato != null) {
					ContratoDadosFaturamentoVO contratoDadosFaturamentoVO = new ContratoDadosFaturamentoVO();

					contratoDadosFaturamentoVO.setFaturavel(controladorContrato.verificarContratoFaturavel(contrato.getChavePrimaria()));
					contratoDadosFaturamentoVO.setIdContrato(contrato.getChavePrimaria());
					contratoDadosFaturamentoVO.setNomeCliente(contrato.getClienteAssinatura().getNome());

					contratoDadosFaturamentoVO.setNumeroContratoFormatado(contrato.getNumeroFormatado());

					Collection<ContratoPontoConsumo> listaContratoPontoConsumo = contrato.getListaContratoPontoConsumo();

					if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {

						Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = new HashSet<ImovelPontoConsumoVO>();

						Map<Imovel, Set<PontoConsumo>> imovelPontoConsumo = new HashMap<Imovel, Set<PontoConsumo>>();

						for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
							PontoConsumo pontoConsumo =
									(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(),
											IMOVEL, "instalacaoMedidor.medidor", "rota");
							Set<PontoConsumo> listaPontoConsumo = imovelPontoConsumo.get(pontoConsumo.getImovel());
							if (listaPontoConsumo == null) {
								listaPontoConsumo = new HashSet<PontoConsumo>();
							}
							listaPontoConsumo.add(pontoConsumo);
							imovelPontoConsumo.put(pontoConsumo.getImovel(), listaPontoConsumo);
						}

						Entry<?, ?> entry = null;
						ImovelPontoConsumoVO imovelPontoConsumoVOSelecionado = null;
						for (Iterator<?> it = imovelPontoConsumo.entrySet().iterator(); it.hasNext();) {
							entry = (Entry<?, ?>) it.next();
							imovelPontoConsumoVOSelecionado = new ImovelPontoConsumoVO();
							imovelPontoConsumoVOSelecionado.setChavePrimaria(((Imovel) entry.getKey()).getChavePrimaria());
							imovelPontoConsumoVOSelecionado.setNome(((Imovel) entry.getKey()).getNome());
							imovelPontoConsumoVOSelecionado.getListaPontoConsumo().addAll((HashSet<PontoConsumo>) entry.getValue());
							listaImovelPontoConsumoVO.add(imovelPontoConsumoVOSelecionado);
						}
						contratoDadosFaturamentoVO.setListaImovelPontoConsumoVO(listaImovelPontoConsumoVO);
					}
					listaContratoDadosFaturamentoVO.add(contratoDadosFaturamentoVO);
				}
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(LISTA_CONTRATO_DADOS_FATURAMENTO_VO, listaContratoDadosFaturamentoVO);

		return "exibirDadosFaturamentoContratoPopup";
	}

	/**
	 * Método responsável por exibir a tela de inclusão de contrato passo 1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoContrato")
	public String exibirInclusaoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "exibirInclusaoContrato";
		Map<String, Object> filtro = new HashMap<String, Object>();

		Long chavePrimariaPrincipal = contratoVO.getChavePrimariaPrincipal();
		Long idModeloContrato = contratoVO.getIdModeloContrato();

		try {

			if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {

				Contrato contrato =
						(Contrato) controladorContrato.obter(chavePrimariaPrincipal, MODELO_CONTRATO, MODELO_CONTRATO_TIPO_MODELO);
				String codigoTipoModelo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR);
				EntidadeConteudo tipoModelo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(codigoTipoModelo));

				if (contrato.getModeloContrato().getTipoModelo() != null
						&& contrato.getModeloContrato().getTipoModelo().equals(tipoModelo)) {

					mensagemErro(model, request, Constantes.ERRO_CONTRATO_COMPLEMENTAR_INCLUSAO_COMPLEMENTAR);

					this.limparDadosSessao(request);
					this.carregarDados(request, model);
					view = "exibirPesquisaContrato";
				}
			}

			limparDadosContratoSessao(request);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
				model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoPadrao());
			}

			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			filtro.put(HABILITADO, Boolean.TRUE);

			if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {
				filtro.put(TIPO_MODELO, Long.parseLong(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR)));
			} else {
				filtro.put(TIPO_MODELO, Long.parseLong(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_PRINCIPAL)));
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));
		model.addAttribute(LISTA_TIPO_CONTRATO, controladorEntidadeConteudo.obterListaTipoModelo());

		return view;
	}

	/**
	 * Exibir migrar saldo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirMigrarSaldo")
	public String exibirMigrarSaldo(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		String tela = "exibirPesquisaContrato";
		Map<String, Object> filtro = new HashMap<String, Object>();
		try {
			model.addAttribute("contratoVO", contratoVO);
			if ((contratoVO.getChavePrimaria() != null) && (contratoVO.getChavePrimaria() > 0)) {
				filtro.put(ID_CONTRATO, contratoVO.getChavePrimaria());

				controladorContrato.validarContratoEncerrado(contratoVO.getChavePrimaria());

				Contrato contrato = (Contrato) controladorContrato.obter(contratoVO.getChavePrimaria());

				contratoVO.setNumeroContratoMigrar(contrato.getNumeroFormatado());

				model.addAttribute(NUMERO_CONTRATO_MIGRAR, contrato.getNumeroFormatado());

				request.getSession().setAttribute(NUMERO_CONTRATO_MIGRAR, String.valueOf(contrato.getNumeroFormatado()));

				Collection<PontoConsumo> listaPontoConsumo = controladorContrato.obterPontosConsumoContrato(filtro);

				Collection<PontoConsumoVO> listaPontoConsumoVO = new ArrayList<PontoConsumoVO>();
				for (PontoConsumo ponto : listaPontoConsumo) {
					PontoConsumoVO pontoConsumoVOSelecionado = new PontoConsumoVO();
					pontoConsumoVOSelecionado.setPontoConsumo(ponto);
					listaPontoConsumoVO.add(pontoConsumoVOSelecionado);
				}

				if (!listaPontoConsumoVO.isEmpty()) {
					controladorApuracaoPenalidade.popularQPNRPontoConsumo(listaPontoConsumoVO, contratoVO.getChavePrimaria());
					request.getSession().setAttribute(LISTA_PONTO_CONSUMO_VO, listaPontoConsumoVO);
					model.addAttribute(LISTA_PONTO_CONSUMO_VO, listaPontoConsumoVO);
				}
			}
			tela = "exibirMigrarSaldo";
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Transferir saldo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("transferirSaldo")
	public String transferirSaldo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "forward:/pesquisarImovelContratoMigrar";
		try {
			model.addAttribute("contratoVO", contratoVO);
			Long chavePontoRecebeSaldo = null;

			Long[] chavesTransferirSaldo = contratoVO.getChavePontoTransferirSaldo();

			if (chavesTransferirSaldo != null) {
				for (Long chave : contratoVO.getChavePontoTransferirSaldo()) {
					chavePontoRecebeSaldo = chave;
					break;
				}
			}

			Collection<PontoConsumoVO> listaPontoConsumoVO =
					(Collection<PontoConsumoVO>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_VO);

			Map<Long, BigDecimal> mapaPontoQNR = new HashMap<Long, BigDecimal>();
			Map<Long, BigDecimal> mapaPontoQPNR = new HashMap<Long, BigDecimal>();

			MigrarSaldoQPNRVO migrarSaldo = new MigrarSaldoQPNRVO();
			for (PontoConsumoVO ponto : listaPontoConsumoVO) {
				if (!mapaPontoQNR.containsKey(ponto.getPontoConsumo().getChavePrimaria())) {
					mapaPontoQNR.put(ponto.getPontoConsumo().getChavePrimaria(), Util.converterCampoStringParaValorBigDecimal(
							LABEL_VALOR_TRANSFERIDO_QNR, ponto.getQnr(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
					mapaPontoQPNR.put(ponto.getPontoConsumo().getChavePrimaria(), Util.converterCampoStringParaValorBigDecimal(
							LABEL_VALOR_TRANSFERIDO_QNR, ponto.getQpnr(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
				}

			}

			migrarSaldo.setSaldoQNR(Util.converterCampoStringParaValorBigDecimal(LABEL_SALDO_QNR,
					contratoVO.getSaldoQNR().replace(".", Constantes.STRING_VIRGULA), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));

			migrarSaldo.setSaldoQPNR(Util.converterCampoStringParaValorBigDecimal(LABEL_SALDO_QPNR,
					contratoVO.getSaldoQPNR().replace(".", Constantes.STRING_VIRGULA), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));

			migrarSaldo.setValorTransferidoQNR(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_TRANSFERIDO_QNR,
					contratoVO.getValorTransferidoQNR().replace(".", Constantes.STRING_VIRGULA), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));

			migrarSaldo.setValorTransferidoQPNR(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_TRANSFERIDO_QPNR,
					contratoVO.getValorTransferidoQPNR().replace(".", Constantes.STRING_VIRGULA), Constantes.FORMATO_VALOR_NUMERO,
					Constantes.LOCALE_PADRAO));

			migrarSaldo.setChavePontoRecebeSaldo(chavePontoRecebeSaldo);
			migrarSaldo.setIdsPontosConsumoDoaSaldo(contratoVO.getIdsPontosConsumo());

			migrarSaldo.setMapaPontoQNR(mapaPontoQNR);
			migrarSaldo.setMapaPontoQPNR(mapaPontoQPNR);

			DadosAuditoria dados = this.getDadosAuditoria(request);

			migrarSaldo.setDadosAuditoria(dados);

			controladorApuracaoPenalidade.transferirSaldoQPNR(migrarSaldo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE, LABEL_SALDO_MIGRADO);
			tela = pesquisarContrato(contratoVO, result, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de inclusão de contrato passo 1.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("reexibirInclusaoContrato")
	public String reexibirInclusaoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Boolean isFluxoAlteracao = contratoVO.getFluxoAlteracao();
		Boolean isFluxoAditamento = contratoVO.getFluxoAditamento();
		Boolean isFluxoDetalhamento = contratoVO.getFluxoDetalhamento();
		Boolean isFluxoInclusao = contratoVO.getFluxoInclusao();

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		Boolean mudancaModelo = contratoVO.getMudancaModelo();
		Long chavePrimariaPrincipal = contratoVO.getChavePrimariaPrincipal();

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (request.getSession().getAttribute(SITUACAO_ESCOLHIDA) != null) {
			contratoVO.setSituacaoContrato((String) request.getSession().getAttribute(SITUACAO_ESCOLHIDA));
		}

		String indicadorContratoExibido = contratoVO.getIndicadorContratoExibido();

		model.addAttribute(INDICADOR_CONTRATO_EXIBIDO, indicadorContratoExibido);
		request.getSession().removeAttribute(PONTO_CONSUMO_EM_ALTERACAO);

		ModeloContrato modeloContrato = null;

		try {

			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				if (mudancaModelo != null && mudancaModelo) {

					limparDadosSessao(request);

					if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {

						Contrato contrato = (Contrato) controladorContrato.obter(chavePrimariaPrincipal, MODELO_CONTRATO, PROPOSTA,
								LISTA_CONTRATO_PONTO_CONSUMO, LISTA_CONTRATO_CLIENTE, LISTA_CONTRATO_QDC);

						contrato.setChavePrimaria(0L);
						contrato.setChavePrimariaPrincipal(chavePrimariaPrincipal);
						contrato.setHabilitado(Boolean.TRUE);
						this.popularFormContrato(contrato, contratoVO, request, model);
						this.setarPontoConsumoContratoNoRequest(contratoVO, request, contrato.getListaContratoPontoConsumo(), model);
						this.popularSessaoListaContratoPontoConsumo(request, contrato, true);
						contratoVO.setChavePrimariaPrincipal(chavePrimariaPrincipal);

					}

					carregarCampos(contratoVO, modeloContrato, request, true, model);
					model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoPadrao());

				} else {

					this.carregarCampos(contratoVO, modeloContrato, request, false, model);
					model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoPadrao());
					Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

					if ((contratoSessao != null) && (!isPostBack(request) && !(isFluxoAlteracao || isFluxoAditamento))) {

						this.popularFormContrato(contratoSessao, contratoVO, request, model);

						if (contratoSessao.getProposta() != null) {
							Collection<Proposta> propostas =
									controladorProposta.listarPropostasAprovadasPorNumero(contratoSessao.getProposta().getNumeroProposta());
							model.addAttribute(LISTA_VERSOES_PROPOSTA, propostas);
						}

					}
				}
			}

			this.configurarCampoMonetarioForm(contratoVO, GASTO_ESTIMADO_GN_MES);
			this.configurarCampoMonetarioForm(contratoVO, ECONOMIA_ESTIMADA_GN_MES);
			this.configurarCampoMonetarioForm(contratoVO, ECONOMIA_ESTIMADA_GN_ANO);
			this.configurarCampoMonetarioForm(contratoVO, VALOR_PARTICIPACAO_CLIENTE);
			this.configurarCampoMonetarioForm(contratoVO, VALOR_CONTRATO);
			this.configurarCampoMonetarioForm(contratoVO, VALOR_GARANTIA_FINANCEIRA);
			this.configurarCampoMonetarioForm(contratoVO, VALOR_INVESTIMENTO);
			this.configurarCampoMonetarioForm(contratoVO, "incentivosComerciais");
			this.configurarCampoMonetarioForm(contratoVO, "incentivosInfraestrutura");

			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			model.addAttribute(Constantes.NOME_FORM, "contratoForm");

			model.addAttribute(LISTA_ANEXOS, request.getSession().getAttribute(LISTA_ANEXOS));

			request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
			request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);
			request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);
			request.getSession().removeAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);
			request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
			request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);

			filtro.put(HABILITADO, Boolean.TRUE);

			if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {
				filtro.put(TIPO_MODELO, Long.parseLong(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR)));
			} else {
				filtro.put(TIPO_MODELO, Long.parseLong(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_PRINCIPAL)));
			}
			model.addAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		isFluxoAditamento = Boolean.valueOf(request.getParameter("fluxoAditamento"));

		if (isFluxoAditamento || isFluxoAlteracao) {
			view = "exibirAditamentoContrato";
		} else if (isFluxoDetalhamento) {
			view = "exibirDetalhamentoContrato";
		} else if (isFluxoInclusao) {
			view = "exibirInclusaoContrato";
		}
		return view;
	}

	/**
	 * Configurar campo monetario form.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param chaveCampo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private void configurarCampoMonetarioForm(ContratoVO contratoVO, String chaveCampo) throws GGASException {

		Object valorCampo = contratoVO.getField(chaveCampo, contratoVO);

		if (valorCampo != null && String.valueOf(valorCampo).trim().length() != 0) {
			ContratoVO.setField(chaveCampo, contratoVO, Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_CAMPO,
					String.valueOf(valorCampo), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).toString());
		}

	}

	/**
	 * Método responsável selecionar o imovel para o contrato.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("selecionarImovelContratoFluxoIncluirAbaProposta")
	public String selecionarImovelContratoAbaProposta(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_PROPOSTA);

		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

		try {

			if (contratoSessao != null) {
				String idProposta = contratoVO.getIdProposta();
				setarPropostaNoContrato(contratoVO, contratoSessao, idProposta);
			}

			Long idImovel = contratoVO.getIdImovel();
			Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = this.obterListaImovelPontoConsumoVO(request);
			Imovel imovel = (Imovel) controladorImovel.obter(idImovel, LISTA_PONTO_CONSUMO);
			ImovelPontoConsumoVO imovelPontoConsumoVO = null;
			if (imovel != null) {
				imovelPontoConsumoVO = new ImovelPontoConsumoVO();
				imovelPontoConsumoVO.setChavePrimaria(idImovel);
				imovelPontoConsumoVO.setNome(imovel.getNome());
				imovelPontoConsumoVO.getListaPontoConsumo().addAll(imovel.getListaPontoConsumo());
				if (listaImovelPontoConsumoVO.contains(imovelPontoConsumoVO)) {
					listaImovelPontoConsumoVO.remove(imovelPontoConsumoVO);
				}
				listaImovelPontoConsumoVO.add(imovelPontoConsumoVO);
			}

			request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);
			model.addAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);
	}

	/**
	 * Método responsável selecionar o imovel para o contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(value = { "selecionarImovelContratoFluxoAditarAlterar", "selecionarImovelContratoFluxoIncluir" })
	public String selecionarImovelContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;

		Boolean isFluxoAditamento = contratoVO.getFluxoAditamento();
		Boolean isFluxoAlteracao = contratoVO.getFluxoAlteracao();
		Boolean isFluxoInclusao = contratoVO.getFluxoInclusao();

		model.addAttribute(ABA_ATUAL, ABA_PONTOS_DE_CONSUMO);

		Long idImovel = contratoVO.getIdImovel();
		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = this.obterListaImovelPontoConsumoVO(request);
		Imovel imovel = (Imovel) controladorImovel.obter(idImovel, LISTA_PONTO_CONSUMO);
		ImovelPontoConsumoVO imovelPontoConsumoVO = null;
		if (imovel != null) {
			imovelPontoConsumoVO = new ImovelPontoConsumoVO();
			imovelPontoConsumoVO.setChavePrimaria(idImovel);
			imovelPontoConsumoVO.setNome(imovel.getNome());
			imovelPontoConsumoVO.getListaPontoConsumo().addAll(imovel.getListaPontoConsumo());
			if (listaImovelPontoConsumoVO.contains(imovelPontoConsumoVO)) {
				listaImovelPontoConsumoVO.remove(imovelPontoConsumoVO);
			}
			listaImovelPontoConsumoVO.add(imovelPontoConsumoVO);
		}

		request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);
		model.addAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);

		if (isFluxoAditamento || isFluxoAlteracao) {
			view = reexibirAditamentoContrato(contratoVO, result, request, model);
		} else if (isFluxoInclusao) {
			view = reexibirInclusaoContrato(contratoVO, result, request, model);
		}

		return view;

	}

	/**
	 * Método responsável retirar o imovel do contrato.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(value = { "retirarImovelInclusaoContrato", "retirarImovelAditamentoContrato" })
	public String retirarImovelInclusaoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_PONTOS_DE_CONSUMO);
		Boolean isAcaoLimparCampo = Boolean.valueOf(request.getParameter("isAcaoLimparCampo"));
		model.addAttribute("isAcaoLimparCampo", isAcaoLimparCampo);
		Long idImovel = contratoVO.getIdImovel();
		Long idPontoConsumo = contratoVO.getIdPontoConsumoLista();
		String idSituacao = contratoVO.getSituacaoContrato();
		if (StringUtils.isNotEmpty(idSituacao)) {
			request.getSession().setAttribute(SITUACAO_ESCOLHIDA, idSituacao);
		}
		ImovelPontoConsumoVO imovelPontoConsumo = null;

		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			Collection<PontoConsumo> pontosConsumo = new ArrayList<PontoConsumo>();
			pontosConsumo.add(controladorPontoConsumo.obterPontoConsumo(idPontoConsumo));
			controladorContrato.verificarDependenciaLeituraFaturamento(pontosConsumo);
		}

		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = this.obterListaImovelPontoConsumoVO(request);
		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		if ((idImovel != null) && (idImovel > 0) && (idPontoConsumo != null) && (idPontoConsumo > 0)) {
			if (listaImovelPontoConsumoVO != null) {

				for (ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
					if (imovelPontoConsumoVO.getChavePrimaria() == idImovel) {
						Collection<PontoConsumo> listaPontoConsumo = imovelPontoConsumoVO.getListaPontoConsumo();
						PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
						pontoConsumo.setChavePrimaria(idPontoConsumo);
						listaPontoConsumo.remove(pontoConsumo);
						imovelPontoConsumo = imovelPontoConsumoVO;
					}
				}
			}

			if (imovelPontoConsumo != null
					&& (imovelPontoConsumo.getListaPontoConsumo() == null || imovelPontoConsumo.getListaPontoConsumo().isEmpty())) {
				listaImovelPontoConsumoVO.remove(imovelPontoConsumo);
			}

			if ((listaPontoConsumoContratoVO != null) && (listaPontoConsumoContratoVO.containsKey(idPontoConsumo))) {
				listaPontoConsumoContratoVO.remove(idPontoConsumo);
			}
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);
		} else if ((idImovel != null) && (idImovel > 0) && ((idPontoConsumo == null) || (idPontoConsumo <= 0))) {
			imovelPontoConsumo = new ImovelPontoConsumoVO();
			imovelPontoConsumo.setChavePrimaria(idImovel);

			Long[] chavesPonto = null;
			for (ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
				if ((imovelPontoConsumoVO.getChavePrimaria() == idImovel) && (imovelPontoConsumoVO.getListaPontoConsumo() != null)
						&& (!imovelPontoConsumoVO.getListaPontoConsumo().isEmpty())) {
					chavesPonto = Util.collectionParaArrayChavesPrimarias(imovelPontoConsumoVO.getListaPontoConsumo());
				}
			}

			listaImovelPontoConsumoVO.remove(imovelPontoConsumo);

			if ((chavesPonto != null) && (chavesPonto.length > 0) && (listaPontoConsumoContratoVO != null)) {
				for (int i = 0; i < chavesPonto.length; i++) {
					if (listaPontoConsumoContratoVO.containsKey(chavesPonto[i])) {
						listaPontoConsumoContratoVO.remove(chavesPonto[i]);
					}
				}
				request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);
			}
		}

		request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);
		model.addAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);

		return reexibirAditamentoContrato(contratoVO, result, request, model);

	}

	/**
	 * Método responsável por exibir a tela de inclusão de contrato passo 2.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoContratoPontoConsumo")
	public String exibirInclusaoContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String numeroProposta = contratoVO.getNumeroProposta();
		String dataAssinatura = contratoVO.getDataAssinatura();
		String renovacaoAutomatica = contratoVO.getRenovacaoAutomatica();
		String numDiasRenoAutoContrato = contratoVO.getNumDiasRenoAutoContrato();
		Long idCliente = contratoVO.getIdCliente();
		String numeroEmpenho = contratoVO.getNumeroEmpenho();
		Long chavePrimariaPrincipal = contratoVO.getChavePrimariaPrincipal();
		Long situacaoContrato = Long.parseLong(contratoVO.getSituacaoContrato());
		Long arrecadadorConvenio = Long.parseLong(contratoVO.getArrecadadorConvenio());

		Long[] chavesPontoConsumo = contratoVO.getChavesPrimariasPontoConsumo();

		request.getSession().setAttribute(SITUACAO_ESCOLHIDA, contratoVO.getSituacaoContrato());

		try {
			
			if(Objects.nonNull(idCliente)) {
				this.validarSeClientePossuiDebito(idCliente, request, model);
			}
			
			controladorContrato.validarPontosSelecionados(chavePrimariaPrincipal, chavesPontoConsumo);

			controladorContrato.validarCampoRenovacaoAutomatica(renovacaoAutomatica, numDiasRenoAutoContrato);

			Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

			Boolean propostaAprovada = true;
			if (contratoSessao == null) {
				Contrato contrato = (Contrato) controladorContrato.criar();
				this.popularContratoPasso1(contratoVO, contrato, request, false, false, false, true, model);
				this.popularSessaoListaContratoPontoConsumo(request, contrato, false);
				controladorContrato.validarDatasContrato(contrato);
				controladorContrato.validarDatasContratoComplementar(chavePrimariaPrincipal, contrato);
				propostaAprovada = contrato.isPropostaAprovada();
				// TODO: Ajustar para validar em cima da nova entidade
				controladorContrato.validarDadosContratoComplementar(contrato);
				request.getSession().setAttribute(CONTRATO, contrato);
			} else {
				this.popularContratoPasso1(contratoVO, contratoSessao, request, false, false, false, true, model);
				this.popularSessaoListaContratoPontoConsumo(request, contratoSessao, false);
				controladorContrato.validarDatasContrato(contratoSessao);
				controladorContrato.validarDatasContratoComplementar(chavePrimariaPrincipal, contratoSessao);
				propostaAprovada = contratoSessao.isPropostaAprovada();
				// TODO: Ajustar para validar em cima da nova entidade
				controladorContrato.validarDadosContratoComplementar(contratoSessao);
				request.getSession().setAttribute(CONTRATO, contratoSessao);
			}

			controladorContrato.validarDataAssinaturaContrato(dataAssinatura);
			controladorContrato.validarSelecaoImovelPontoConsumo(chavesPontoConsumo);
			controladorProposta.validarPropostasContrato(numeroProposta, propostaAprovada);
			controladorCliente.validarNumeroEmpenho(idCliente, numeroEmpenho);
			controladorContrato.validarSituacaoDataAssinaturaArrecadadador(dataAssinatura, arrecadadorConvenio, situacaoContrato);

			this.carregarCamposPasso2(request, contratoVO, model);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
			}
			contratoVO.setChavesPrimariasPontoConsumo(null);
			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			contratoVO.setQDCContrato(null);

			contratoVO.setFluxoInclusao(true);

			List<ImovelPontoConsumoVO> listaImovelPontoConsumoSelecionados = new ArrayList<ImovelPontoConsumoVO>(
					(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO));

			if (!listaImovelPontoConsumoSelecionados.isEmpty()) {
				List<PontoConsumo> listaPontoConsumo =
						new ArrayList<PontoConsumo>(listaImovelPontoConsumoSelecionados.get(0).getListaPontoConsumo());
				if (!listaPontoConsumo.isEmpty()) {
					contratoVO.setIdPontoConsumo(listaPontoConsumo.get(0).getChavePrimaria());
					return popularCamposContratoPontoConsumo(contratoVO, result, request, model);
				}
			}
		} catch (GGASException e) {
			model.addAttribute(ABA_ATUAL, ABA_PONTOS_DE_CONSUMO);
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);
	}

	/**
	 * Popular form contrato.
	 *
	 * @param contrato
	 * @param contratoVO
	 * @param request
	 * @param model
	 * @throws GGASException
	 */
	private void popularFormContrato(Contrato contrato, ContratoVO contratoVO, HttpServletRequest request, Model model)
			throws GGASException {

		this.popularFormContratoInfoCliente(contrato, contratoVO);

		if (!Util.isNullOrEmpty(contrato.getAnexos())) {
			request.getSession().setAttribute(LISTA_ANEXOS, new ArrayList<ContratoAnexo>(contrato.getAnexos()));
		}

		contratoVO.setChavePrimaria(contrato.getChavePrimaria());
		contratoVO.setVersao(contrato.getVersao());
		contratoVO.setHabilitado(contrato.isHabilitado());

		if (contrato.getModeloContrato() != null) {
			contratoVO.setIdModeloContrato(contrato.getModeloContrato().getChavePrimaria());
		}
		if (contrato.getNumero() != null) {
			contratoVO.setNumeroContrato(String.valueOf(contrato.getNumero()));
		}
		if (StringUtils.isNotEmpty(contrato.getNumeroFormatado())) {
			contratoVO.setNumeroFormatado(contrato.getNumeroFormatado());
		}
		if (contrato.getNumeroAnterior() != null) {
			contratoVO.setNumeroAnteriorContrato(contrato.getNumeroAnterior());
		}
		if (contrato.getDescricaoContrato() != null) {
			contratoVO.setDescricaoContrato(contrato.getDescricaoContrato());
		}
		if (contrato.getVolumeReferencia() != null) {
			contratoVO.setVolumeReferencia(Util.converterCampoValorDecimalParaString(VALOR_CONTRATO, contrato.getVolumeReferencia(),
					Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
		}
		if (contrato.getSituacao() != null) {
			contratoVO.setSituacaoContrato(String.valueOf(contrato.getSituacao().getChavePrimaria()));
		}

		// O campo situacaoEscolhida estará na
		// sessão e caso esteja preenchido é este
		// que deve ser mantido no form e não o do
		// contrato.
		if (request.getSession().getAttribute(SITUACAO_ESCOLHIDA) != null) {
			contratoVO.setSituacaoContrato(String.valueOf(request.getSession().getAttribute(SITUACAO_ESCOLHIDA)));
		}
		if (contrato.getDataAssinatura() != null) {
			contratoVO.setDataAssinatura(Util.converterDataParaStringSemHora(contrato.getDataAssinatura(), Constantes.FORMATO_DATA_BR));
		}
		if (contrato.getNumeroEmpenho() != null) {
			contratoVO.setNumeroEmpenho(contrato.getNumeroEmpenho());
		}
		if (contrato.getDataVencimentoObrigacoes() != null) {
			contratoVO.setDataVencObrigacoesContratuais(
					Util.converterDataParaStringSemHora(contrato.getDataVencimentoObrigacoes(), Constantes.FORMATO_DATA_BR));
		}

		contratoVO.setIndicadorAnoContratual(String.valueOf(contrato.getIndicadorAnoContratual()));
		contratoVO.setRenovacaoAutomatica(String.valueOf(contrato.getRenovacaoAutomatica()));
		if (contrato.getNumeroDiasRenovacaoAutomatica() != null) {
			contratoVO.setNumDiasRenoAutoContrato(String.valueOf(contrato.getNumeroDiasRenovacaoAutomatica()));
		}
		if (contrato.getDiasAntecedenciaRenovacao() != null) {
			contratoVO.setTempoAntecedenciaRenovacao(String.valueOf(contrato.getDiasAntecedenciaRenovacao()));
		}

		if (contrato.getValorContrato() != null) {
			contratoVO.setValorContrato(
					Util.converterCampoValorDecimalParaString(VALOR_CONTRATO, contrato.getValorContrato(), Constantes.LOCALE_PADRAO));
		}

		this.popularFormContratoInfoQDC(contratoVO, contrato.getListaContratoQDC());

		if (contrato.getNumeroAditivo() != null && contrato.getNumeroAditivo() > 0) {
			contratoVO.setNumeroAditivo(String.valueOf(contrato.getNumeroAditivo()));
		}
		if (contrato.getDataAditivo() != null) {
			contratoVO.setDataAditivo(Util.converterDataParaStringSemHora(contrato.getDataAditivo(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(contrato.getDescricaoAditivo())) {
			contratoVO.setDescricaoAditivo(contrato.getDescricaoAditivo());
		}
		if (contrato.getProposta() != null) {
			contratoVO.setNumeroProposta(contrato.getProposta().getNumeroCompletoProposta());
			contratoVO.setIdProposta(Long.toString(contrato.getProposta().getChavePrimaria()));
		}

		if (contrato.getValorGastoMensal() != null) {
			contratoVO.setGastoEstimadoGNMes(Util.converterCampoValorDecimalParaString(GASTO_ESTIMADO_GN_MES,
					contrato.getValorGastoMensal(), Constantes.LOCALE_PADRAO));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, GASTO_ESTIMADO_GN_MES);
		}

		if (contrato.getMedidaEconomiaMensalGN() != null) {
			contratoVO.setEconomiaEstimadaGNMes(Util.converterCampoValorDecimalParaString(ECONOMIA_ESTIMADA_GN_MES,
					contrato.getMedidaEconomiaMensalGN(), Constantes.LOCALE_PADRAO));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, ECONOMIA_ESTIMADA_GN_MES);
		}

		if (contrato.getMedidaEconomiaAnualGN() != null) {
			contratoVO.setEconomiaEstimadaGNAno(Util.converterCampoValorDecimalParaString(ECONOMIA_ESTIMADA_GN_ANO,
					contrato.getMedidaEconomiaAnualGN(), Constantes.LOCALE_PADRAO));
		}
		if (contrato.getPercentualEconomiaGN() != null) {
			contratoVO.setDescontoEfetivoEconomia(Util.converterCampoPercentualParaString(Contrato.DESCONTO_EFETIVO_ECONOMIA,
					contrato.getPercentualEconomiaGN().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}
		if (contrato.getNumeroDiaVencimentoFinanciamento() != null) {
			contratoVO.setDiaVencFinanciamento(String.valueOf(contrato.getNumeroDiaVencimentoFinanciamento()));
		}

		if (contrato.getValorParticipacaoCliente() != null) {
			contratoVO.setValorParticipacaoCliente(Util.converterCampoValorDecimalParaString(VALOR_PARTICIPACAO_CLIENTE,
					contrato.getValorParticipacaoCliente(), Constantes.LOCALE_PADRAO));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, VALOR_PARTICIPACAO_CLIENTE);
		}

		if (contrato.getQtdParcelasFinanciamento() != null) {
			contratoVO.setQtdParcelasFinanciamento(String.valueOf(contrato.getQtdParcelasFinanciamento()));
		}
		if (contrato.getPercentualJurosFinanciamento() != null) {
			contratoVO.setPercentualJurosFinanciamento(Util.converterCampoPercentualParaString(Contrato.PERCENTUAL_JUROS_FINANCIAMENTO,
					contrato.getPercentualJurosFinanciamento().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getSistemaAmortizacao() != null) {
			contratoVO.setSistemaAmortizacao(String.valueOf(contrato.getSistemaAmortizacao().getChavePrimaria()));
		}

		contratoVO.setClienteAssinatura(NUMERO_ZERO);

		if (contrato.getIndicadorMulta() != null) {
			contratoVO.setIndicadorMultaAtraso(contrato.getIndicadorMulta().toString());
		}
		if (contrato.getIndicadorJuros() != null) {
			contratoVO.setIndicadorJurosMora(contrato.getIndicadorJuros().toString());
		}
		if (contrato.getIndiceFinanceiro() != null) {
			contratoVO.setIndiceCorrecaoMonetaria(String.valueOf(contrato.getIndiceFinanceiro().getChavePrimaria()));
		}
		if (contrato.getGarantiaFinanceira() != null) {
			contratoVO.setTipoGarantiaFinanceira(String.valueOf(contrato.getGarantiaFinanceira().getChavePrimaria()));
		}
		if (StringUtils.isNotEmpty(contrato.getDescricaoGarantiaFinanceira())) {
			contratoVO.setDescGarantiaFinanc(contrato.getDescricaoGarantiaFinanceira());
		}
		if (contrato.getValorGarantiaFinanceira() != null) {
			contratoVO.setValorGarantiaFinanceira(String.valueOf(contrato.getValorGarantiaFinanceira()));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, VALOR_GARANTIA_FINANCEIRA);
		}

		if (contrato.getDataInicioGarantiaFinanciamento() != null) {
			contratoVO.setDataInicioGarantiaFinanceira(
					Util.converterDataParaStringSemHora(contrato.getDataInicioGarantiaFinanciamento(), Constantes.FORMATO_DATA_BR));
		}
		if (contrato.getDataFimGarantiaFinanciamento() != null) {
			contratoVO.setDataFinalGarantiaFinanceira(
					Util.converterDataParaStringSemHora(contrato.getDataFimGarantiaFinanciamento(), Constantes.FORMATO_DATA_BR));
		}
		if (contrato.getRenovacaoGarantiaFinanceira() != null) {
			if (contrato.getRenovacaoGarantiaFinanceira()) {
				contratoVO.setGarantiaFinanceiraRenovada(LABEL_RENOVAVEL);
			} else {
				contratoVO.setGarantiaFinanceiraRenovada(RENOVADA);
			}
		}
		if (contrato.getDiasAntecedenciaRevisaoGarantiaFinanceira() != null) {
			contratoVO.setTempoAntecRevisaoGarantias(String.valueOf(contrato.getDiasAntecedenciaRevisaoGarantiaFinanceira()));
		}
		if (contrato.getPeriodicidadeReavaliacaoGarantias() != null) {
			contratoVO.setPeriodicidadeReavGarantias(String.valueOf(contrato.getPeriodicidadeReavaliacaoGarantias()));
		}

		if (contrato.getListaContratoPenalidade() != null) {

			this.popularListaTakeOrPay(contrato, request, model);

		}
		/*
		 * Extrair esse bloco de código
		 */

		if (contrato.getListaContratoPenalidade() != null) {

			this.popularListaPenalidadeRetiradaMaiorMenorGeral(contrato, request, model);

		}
		if (contrato.getPercentualTarifaDoP() != null) {
			contratoVO.setPercentualTarifaDoP(Util.converterCampoPercentualParaString(Contrato.PERCENTUAL_TARIFA_DOP,
					contrato.getPercentualTarifaDoP().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}
		if (contrato.getPercentualSobreTariGas() != null) {
			contratoVO.setPercentualSobreTariGas(Util.converterCampoPercentualParaString(Contrato.PERCENTUAL_RETIRADA_MENOR,
					contrato.getPercentualSobreTariGas().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}
		if (contrato.getFormaCobranca() != null) {
			contratoVO.setFormaCobranca(String.valueOf(contrato.getFormaCobranca().getChavePrimaria()));
		}

		contratoVO.setDebitoAutomatico(String.valueOf(contrato.getIndicadorDebitoAutomatico()));

		if (contrato.getArrecadadorContratoConvenio() != null) {
			contratoVO.setArrecadadorConvenio(String.valueOf(contrato.getArrecadadorContratoConvenio().getChavePrimaria()));
		}

		if (contrato.getTipoPeriodicidadePenalidade() != null) {
			contratoVO.setTipoPeriodicidadePenalidade(Boolean.toString(contrato.getTipoPeriodicidadePenalidade()));
		}

		if (contrato.getAgrupamentoCobranca() != null) {
			contratoVO.setFaturamentoAgrupamento(Boolean.toString(contrato.getAgrupamentoCobranca()));
		}

		if (contrato.getAgrupamentoConta() != null) {
			contratoVO.setEmissaoFaturaAgrupada(Boolean.toString(contrato.getAgrupamentoConta()));
		}

		if (contrato.getChavePrimariaPai() != null) {
			contratoVO.setChavePrimariaPai(contrato.getChavePrimariaPai());
		}

		if (contrato.getChavePrimariaPrincipal() != null) {
			contratoVO.setChavePrimariaPrincipal(contrato.getChavePrimariaPrincipal());
		}

		if (contrato.getTipoAgrupamento() != null) {
			contratoVO.setTipoAgrupamento(String.valueOf(contrato.getTipoAgrupamento().getChavePrimaria()));
		}

		if (contrato.getValorInvestimento() != null) {
			contratoVO.setValorInvestimento(String.valueOf(contrato.getValorInvestimento()));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, VALOR_INVESTIMENTO);
		}

		if (contrato.getDataInvestimento() != null) {
			contratoVO.setDataInvestimento(Util.converterDataParaStringSemHora(contrato.getDataInvestimento(), Constantes.FORMATO_DATA_BR));
		}

		if (contrato.getMultaRecisoria() != null) {
			contratoVO.setMultaRecisoria(String.valueOf(contrato.getMultaRecisoria().getChavePrimaria()));
		}

		if (contrato.getDataRecisao() != null) {
			contratoVO.setDataRecisao(Util.converterDataParaStringSemHora(contrato.getDataRecisao(), Constantes.FORMATO_DATA_BR));
		}

		if (contrato.getAnoContrato() != null) {
			contratoVO.setAnoContrato(contrato.getAnoContrato());
		}

		if (contrato.getPercentualMulta() != null) {
			contratoVO.setPercentualMulta(Util.converterCampoPercentualParaString(StringUtils.EMPTY,
					contrato.getPercentualMulta().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getPercentualJurosMora() != null) {
			contratoVO.setPercentualJurosMora(Util.converterCampoPercentualParaString(StringUtils.EMPTY,
					contrato.getPercentualJurosMora().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getPercentualQDCContratoQPNR() != null) {
			contratoVO.setPercDuranteRetiradaQPNRC(Util.converterCampoPercentualParaString(PERC_DURANTE_RETIRADA_QPNRC,
					contrato.getPercentualQDCContratoQPNR().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getPercentualMinimoQDCContratoQPNR() != null) {
			contratoVO.setPercMinDuranteRetiradaQPNRC(Util.converterCampoPercentualParaString(PERC_MIN_DURANTE_RETIRADA_QPNRC,
					contrato.getPercentualMinimoQDCContratoQPNR().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getPercentualQDCFimContratoQPNR() != null) {
			contratoVO.setPercFimRetiradaQPNRC(Util.converterCampoPercentualParaString(PERC_FIM_RETIRADA_QPNRC,
					contrato.getPercentualQDCFimContratoQPNR().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (contrato.getDataInicioRetiradaQPNR() != null) {
			contratoVO.setDataInicioRetiradaQPNRC(
					Util.converterDataParaStringSemHora(contrato.getDataInicioRetiradaQPNR(), Constantes.FORMATO_DATA_BR));
		}

		if (contrato.getDataFimRetiradaQPNR() != null) {
			contratoVO.setDataFimRetiradaQPNRC(
					Util.converterDataParaStringSemHora(contrato.getDataFimRetiradaQPNR(), Constantes.FORMATO_DATA_BR));
		}

		if (contrato.getAnosValidadeRetiradaQPNR() != null) {
			contratoVO.setTempoValidadeRetiradaQPNRC(contrato.getAnosValidadeRetiradaQPNR().toString());
		}
		
		if (contrato.getIncentivosComerciais() != null) {
			contratoVO.setIncentivosComerciais(Util.converterCampoValorDecimalParaString("incentivosComerciais",
					contrato.getIncentivosComerciais(), Constantes.LOCALE_PADRAO));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, "incentivosComerciais");
		}
		
		if (contrato.getIncentivosInfraestrutura() != null) {
			contratoVO.setIncentivosInfraestrutura(Util.converterCampoValorDecimalParaString("incentivosInfraestrutura",
					contrato.getIncentivosInfraestrutura(), Constantes.LOCALE_PADRAO));
		} else {
			this.configurarCampoMonetarioForm(contratoVO, "incentivosInfraestrutura");
		}
		
		if(contrato.getPrazoVigencia() != null) {
			contratoVO.setPrazoVigencia(String.valueOf(contrato.getPrazoVigencia()));
		}
		

		model.addAttribute(LISTA_ANEXOS, request.getSession().getAttribute(LISTA_ANEXOS));
	}

	/**
	 * Popular lista penalidade retirada maior menor geral.
	 *
	 * @param contrato the contrato
	 * @param request the request
	 */
	private void popularListaPenalidadeRetiradaMaiorMenorGeral(Contrato contrato, HttpServletRequest request, Model model) {

		Collection<ContratoPenalidade> listaPenalidadeRetMaiorMenorConsulta =
				controladorContrato.listarContratoPenalidadeRetiradaMaiorMenorPorContrato(contrato.getChavePrimaria());

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadeRetMaiorMenor =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		if (listaPenalidadeRetMaiorMenor == null) {

			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorModalidadeVO =
					new ArrayList<PenalidadesRetiradaMaiorMenorVO>();

			if (listaPenalidadeRetMaiorMenorConsulta != null && !listaPenalidadeRetMaiorMenorConsulta.isEmpty()) {

				for (ContratoPenalidade contratoPenalidade : listaPenalidadeRetMaiorMenorConsulta) {

					PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
					penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(contratoPenalidade.getPenalidade());
					penalidadesRetiradaMaiorMenorVO
							.setEntidadePeriodicidadeRetiradaMaiorMenor(contratoPenalidade.getPeriodicidadePenalidade());
					penalidadesRetiradaMaiorMenorVO.setEntidadeBaseApuracaoRetiradaMaiorMenor(contratoPenalidade.getBaseApuracao());
					penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(contratoPenalidade.getDataInicioVigencia());
					penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(contratoPenalidade.getDataFimVigencia());

					penalidadesRetiradaMaiorMenorVO.setEntidadePrecoCobrancaRetiradaMaiorMenor(contratoPenalidade.getPrecoCobranca());
					penalidadesRetiradaMaiorMenorVO.setEntidadeTipoApuracaoRetirMaiorMenor(contratoPenalidade.getTipoApuracao());
					penalidadesRetiradaMaiorMenorVO.setEntidadeConsumoReferenciaRetMaiorMenor(contratoPenalidade.getConsumoReferencia());
					penalidadesRetiradaMaiorMenorVO
							.setValorPercentualCobRetMaiorMenor(contratoPenalidade.getValorPercentualCobRetMaiorMenor());
					penalidadesRetiradaMaiorMenorVO
							.setValorPercentualCobIntRetMaiorMenor(contratoPenalidade.getValorPercentualCobIntRetMaiorMenor());
					penalidadesRetiradaMaiorMenorVO.setValorPercentualRetMaiorMenor(contratoPenalidade.getValorPercentualRetMaiorMenor());
					penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(contratoPenalidade.getIndicadorImposto());

					listaPenalidadesRetMaiorMenorModalidadeVO.add(penalidadesRetiradaMaiorMenorVO);
				}

				request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorModalidadeVO);

			}
		}

		model.addAttribute(LISTA_PENALIDADE_RET_MAIOR_MENOR, listaPenalidadeRetMaiorMenorConsulta);

	}

	/**
	 * Popular form contrato info qdc.
	 *
	 */
	private void popularFormContratoInfoQDC(ContratoVO contratoVO, Collection<ContratoQDC> lista) throws GGASException {

		List<ContratoQDC> listaContratoQDC = new ArrayList<ContratoQDC>(lista);
		BeanComparator comparador = new BeanComparator(DATA);
		Collections.sort((List) listaContratoQDC, comparador);

		if (!listaContratoQDC.isEmpty()) {
			String[] qdcContrato = new String[listaContratoQDC.size()];
			StringBuilder stringBuilder = null;

			int i = 0;

			for (ContratoQDC contratoQDC : listaContratoQDC) {
				stringBuilder = new StringBuilder();
				stringBuilder.append(Util.converterDataParaStringSemHora(contratoQDC.getData(), Constantes.FORMATO_DATA_BR));
				stringBuilder.append(" | ");
				stringBuilder.append(Util.converterCampoCurrencyParaString(contratoQDC.getMedidaVolume(), Constantes.LOCALE_PADRAO));
				qdcContrato[i] = stringBuilder.toString();

				i++;
			}

			contratoVO.setQDCContrato(qdcContrato);
		}
	}

	/**
	 * Popular form contrato info cliente.
	 *
	 * @param contrato the contrato
	 * @param dynaForm the dyna form
	 * @throws GGASException the GGAS exception
	 */
	private void popularFormContratoInfoCliente(Contrato contrato, ContratoVO contratoVO) throws GGASException {

		if (contrato.getClienteAssinatura() != null) {

			Cliente cliente = controladorCliente.obterCliente(contrato.getClienteAssinatura().getChavePrimaria(), ENDERECOS);

			if (cliente != null) {

				contratoVO.setIdCliente(contrato.getClienteAssinatura().getChavePrimaria());
				if (StringUtils.isNotEmpty(cliente.getNome())) {
					contratoVO.setNomeCliente(cliente.getNome());
				} else {
					contratoVO.setNomeCliente(StringUtils.EMPTY);
				}
				if (TipoPessoa.PESSOA_FISICA == cliente.getTipoCliente().getTipoPessoa().getCodigo()) {
					contratoVO.setDocumentoCliente(cliente.getCpfFormatado());
				}
				if (TipoPessoa.PESSOA_JURIDICA == cliente.getTipoCliente().getTipoPessoa().getCodigo()) {
					contratoVO.setDocumentoCliente(cliente.getCnpjFormatado());
				}
				if (StringUtils.isNotEmpty(cliente.getEmailPrincipal())) {
					contratoVO.setEmailCliente(cliente.getEmailPrincipal());
				} else {
					contratoVO.setEmailCliente(StringUtils.EMPTY);
				}
				if (cliente.getEnderecoPrincipal() != null) {
					contratoVO.setEnderecoCliente(cliente.getEnderecoPrincipal().getEnderecoFormatado());
				} else {
					contratoVO.setEnderecoCliente(StringUtils.EMPTY);
				}
			}
		}
	}

	/**
	 * Método responsável por incluir o Contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirContrato")
	public String incluirContrato(@RequestParam(required = false, value = "funcionario") Funcionario funcionario, ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);
		if (contratoSessao != null) {
			contratoSessao.setHabilitado(true);
		}

		try {
			// recuperar qual foi a situacao escolhida
			String situacaoEscolhida = (String) request.getSession().getAttribute(SITUACAO_ESCOLHIDA);

			if (situacaoEscolhida != null && !situacaoEscolhida.isEmpty()) {
				SituacaoContrato situacao = controladorContrato
						.obterSituacaoContrato(Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, situacaoEscolhida));
				contratoSessao.setSituacao(situacao);
			}

			popularContratoPasso2(contratoVO, request, contratoSessao);
			contratoSessao.setDadosAuditoria(this.getDadosAuditoria(request));

			if (contratoSessao.getProposta() != null) {
				controladorProposta.validarPropostasContrato(contratoSessao.getProposta().getNumeroProposta(),
						contratoSessao.isPropostaAprovada());
			}

			// inserir os registro em clienteImovel
			String parametroSistema = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO);
			TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel =
					controladorImovel.buscarTipoRelacionamentoClienteImovel(Long.valueOf(parametroSistema));

			Collection<ClienteImovel> listaClienteImovel = new ArrayList<ClienteImovel>();
			Collection<Long> listaIdImovel = new ArrayList<Long>();

			inserirRegistroClienteImovel(contratoSessao, parametroSistema, tipoRelacionamentoClienteImovel, listaClienteImovel,
					listaIdImovel);

			Long idContrato = controladorContrato.inserirContrato(contratoSessao);
			controladorClienteImovel.inserirClienteImovel(listaClienteImovel);
			if (contratoSessao.getSituacao().getChavePrimaria() == ATIVO) {
				controladorIntegracao.inserirIntegracaoContrato(contratoSessao);
			}
			@SuppressWarnings("unchecked")
			Map<Long, Object> dadosPontoConsumoVencimento =
					(Map<Long, Object>) request.getSession().getAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);

			if (dadosPontoConsumoVencimento != null && !dadosPontoConsumoVencimento.isEmpty()) {
				controladorPontoConsumo.inserirPoncoConsumoVencimento(dadosPontoConsumoVencimento, idContrato);
			}
			
			model.addAttribute(CHAVE_PRIMARIA, idContrato);
			contratoVO.setChavePrimaria(idContrato);
			
			ControladorParametroSistema controladorParametro = ServiceLocator.getInstancia().getControladorParametroSistema();
			
			if ("1".equals(controladorParametro.obterParametroPorCodigo(Constantes.DESATIVAR_PONTO_CONTRATO).getValor())) {
				ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
				List<ContratoPontoConsumo> listaContratoPontoConsumo = (List<ContratoPontoConsumo>) controladorContrato
						.listarContratoPontoConsumo(idContrato);

				for (ContratoPontoConsumo contratoPC : listaContratoPontoConsumo) {
					ativarPontoConsumo(contratoPC.getPontoConsumo());
				}
			}

			mensagemSucesso(model, Constantes.CONTRATO_INCLUIDO_SUCESSO, contratoSessao.getNumeroFormatado());

			this.limparDadosSessao(request);
			
			if(request.getSession().getAttribute("dadosFaturaResidual") != null) {
				
				DadosAuditoria dadosAuditoria = this.getDadosAuditoria(request);
				
				Map<Long, List<String>> dadosFaturaResidual = (Map<Long, List<String>>) request.getSession().getAttribute("dadosFaturaResidual");
				
				controladorContrato.inserirDadosFaturaResidual(dadosFaturaResidual, funcionario != null ? funcionario : dadosAuditoria.getUsuario().getFuncionario());
				
				request.getSession().removeAttribute("dadosFaturaResidual");
				
			}

			view = pesquisarContrato(contratoVO, result, request, model);

			modificarFluxosEstadoDefault(contratoVO);
			
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession().getAttribute("urlRetornoAtualizacaoCastral");
				view = "redirect:" + urlRetornoAtualizacaoCastral;
			}		
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
			view = reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
		}

		return view;

	}

	private void inserirRegistroClienteImovel(Contrato contratoSessao, String parametroSistema,
			TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel, Collection<ClienteImovel> listaClienteImovel,
			Collection<Long> listaIdImovel) throws NegocioException {
		for (ContratoPontoConsumo contratoPontoConsumo : contratoSessao.getListaContratoPontoConsumo()) {
			PontoConsumo pontoConsumo =
					(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);
			ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(),
					contratoSessao.getClienteAssinatura().getChavePrimaria(), Long.valueOf(parametroSistema));

			if (clienteImovel == null && listaIdImovel != null && !listaIdImovel.contains(pontoConsumo.getImovel().getChavePrimaria())) {

				ClienteImovel clienteImovelNovo = (ClienteImovel) controladorImovel.criarClienteImovel();
				if (contratoSessao.getDataAssinatura() != null) {
					clienteImovelNovo.setRelacaoInicio(contratoSessao.getDataAssinatura());
				} else {
					clienteImovelNovo.setRelacaoInicio(new Date());
				}
				clienteImovelNovo.setCliente(contratoSessao.getClienteAssinatura());

				clienteImovelNovo.setImovel(pontoConsumo.getImovel());

				clienteImovelNovo.setTipoRelacionamentoClienteImovel(tipoRelacionamentoClienteImovel);
				listaClienteImovel.add(clienteImovelNovo);
				listaIdImovel.add(pontoConsumo.getImovel().getChavePrimaria());

			}
		}
	}

	/**
	 * Popular contrato passo2.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoPasso2(ContratoVO contratoVO, HttpServletRequest request, Contrato contratoSessao) throws GGASException {

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		Integer versao = contratoVO.getVersao();
		if (versao != null && versao.intValue() > 0) {
			contratoSessao.setVersao(versao);
		}

		if (listaPontoConsumoContratoVO != null && !listaPontoConsumoContratoVO.isEmpty()) {

			Collection<ContratoPontoConsumo> listaContratoPontoConsumo = new HashSet<ContratoPontoConsumo>();
			Collection<ContratoCliente> listaContratoCliente = new HashSet<ContratoCliente>();
			for (Map.Entry<Long, PontoConsumoContratoVO> entry : listaPontoConsumoContratoVO.entrySet()) {
				Long chavePontoConsumoContratoVO = entry.getKey();
				PontoConsumoContratoVO pontoConsumoContratoVO = entry.getValue();
				ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) controladorContrato.criarContratoPontoConsumo();

				PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(chavePontoConsumoContratoVO);

				contratoPontoConsumo.setPontoConsumo(pontoConsumo);
				contratoPontoConsumo.setContrato(contratoSessao);
				contratoPontoConsumo.setDadosAuditoria(this.getDadosAuditoria(request));
				contratoPontoConsumo.setHabilitado(true);
				contratoPontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());

				popularContratoDadosAbaPrincipal(contratoPontoConsumo, pontoConsumoContratoVO.getAbaPrincipal());
				popularContratoDadosAbaDadosTecnicos(contratoPontoConsumo, pontoConsumoContratoVO.getAbaDadosTecnicos());
				popularContratoDadosAbaConsumo(contratoPontoConsumo, pontoConsumoContratoVO.getAbaConsumo());
				popularContratoDadosAbaFaturamento(contratoPontoConsumo, pontoConsumoContratoVO.getAbaFaturamento(), request);
				popularContratoDadosAbaResponsabilidades(contratoSessao, listaContratoCliente,
						pontoConsumoContratoVO.getAbaResponsabilidades());
				popularContratoDadosAbaModalidades(contratoPontoConsumo, pontoConsumoContratoVO.getAbaModalidade(), request);

				listaContratoPontoConsumo.add(contratoPontoConsumo);
			}
			contratoSessao.getListaContratoPontoConsumo().clear();
			contratoSessao.getListaContratoPontoConsumo().addAll(listaContratoPontoConsumo);

			contratoSessao.getListaContratoCliente().clear();
			contratoSessao.getListaContratoCliente().addAll(listaContratoCliente);

		}

		popularContratoPenalidadesETOP(contratoVO, request, contratoSessao);

	}

	/**
	 * Ativa ponto de consumo ao criar contrato
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public void ativarPontoConsumo(PontoConsumo pontoConsumo) throws ConcorrenciaException, NegocioException {
		ControladorPontoConsumo controladorPonto = ServiceLocator.getInstancia().getControladorPontoConsumo();
		
		if(pontoConsumo.isHabilitado() == Boolean.FALSE) {
			pontoConsumo.setHabilitado(Boolean.TRUE);
			controladorPonto.atualizarEmBatch(pontoConsumo, PontoConsumo.class);
		}
	}

	/**
	 * Popular contrato penalidades e take or pay.
	 *
	 * @param form the form
	 * @param request the request
	 * @param contratoSessao the contrato sessao
	 * @throws GGASException the exception
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoPenalidadesETOP(ContratoVO contratoVO, HttpServletRequest request, Contrato contratoSessao)
			throws GGASException {

		Integer versao = contratoVO.getVersao();

		if (versao != null && versao.intValue() > 0) {
			contratoSessao.setVersao(versao);
		}

		contratoSessao.getListaContratoPenalidade().clear();
		// adição da lista do VO de Penalidade Retirada Maior/Menor
		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadeRetMaiorMenor =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		if (listaPenalidadeRetMaiorMenor != null && !listaPenalidadeRetMaiorMenor.isEmpty()) {
			Collection<ContratoPenalidade> listaContratoPenalidade = new HashSet<ContratoPenalidade>();
			for (PenalidadesRetiradaMaiorMenorVO penalidadeRetiMaiorMenorVO : listaPenalidadeRetMaiorMenor) {
				ContratoPenalidade penalidade = converterPenalidadesRetiradaMaiorMenorVO(penalidadeRetiMaiorMenorVO);
				penalidade.setContrato(contratoSessao);
				penalidade.setHabilitado(Boolean.TRUE);
				penalidade.setVersao(0);
				penalidade.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaContratoPenalidade.add(penalidade);
			}
			contratoSessao.getListaContratoPenalidade().addAll(listaContratoPenalidade);
		}
		// fim da adição da lista do VO Penalidade Retirada Maior/Menor

		// adição da lista do VO do ToP de contrato
		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayCVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);

		if (listaCompromissosTakeOrPayCVO != null && !listaCompromissosTakeOrPayCVO.isEmpty()) {
			Collection<ContratoPenalidade> listaContratoPenalidade = new HashSet<ContratoPenalidade>();
			for (CompromissoTakeOrPayVO compromisso : listaCompromissosTakeOrPayCVO) {
				ContratoPenalidade penalidade = converterCompromissoTakeOrPayContratoVO(compromisso);
				penalidade.setContrato(contratoSessao);
				penalidade.setHabilitado(Boolean.TRUE);
				penalidade.setVersao(0);
				penalidade.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaContratoPenalidade.add(penalidade);
			}
			contratoSessao.getListaContratoPenalidade().addAll(listaContratoPenalidade);
		}
		// fim da adição da lista do VO do ToP de contrato

	}

	/**
	 * Popular contrato dados aba principal.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param abaPrincipal the aba principal
	 * @throws GGASException the GGAS exception
	 */
	private void popularContratoDadosAbaPrincipal(ContratoPontoConsumo contratoPontoConsumo, Map<String, Object> abaPrincipal)
			throws GGASException {

		String dataInicioTeste = (String) abaPrincipal.get(PERIODO_TESTE_DATA_INICIO);
		String dataFinalTeste = (String) abaPrincipal.get(PERIODO_TESTE_DATE_FIM);
		String prazoTeste = (String) abaPrincipal.get(PRAZO_TESTE);
		String volumeTeste = (String) abaPrincipal.get(VOLUME_TESTE);
		String faxDDD = (String) abaPrincipal.get(FAX_DDD);
		String numeroFaxOperacional = (String) abaPrincipal.get(FAX_NUMERO);
		String email = (String) abaPrincipal.get(EMAIL);
		String inicioGarantiaConversao = (String) abaPrincipal.get(INICIO_GARANTIA_CONVERSAO);
		String numeroDiasGarantia = (String) abaPrincipal.get(NUMERO_DIAS_GARANTIA);

		if (StringUtils.isNotEmpty(dataInicioTeste)) {
			contratoPontoConsumo
					.setDataInicioTeste(Util.converterCampoStringParaData(LABEL_DATA, dataInicioTeste, Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(dataFinalTeste)) {
			contratoPontoConsumo.setDataFimTeste(Util.converterCampoStringParaData(LABEL_DATA, dataFinalTeste, Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(prazoTeste)) {
			contratoPontoConsumo.setPrazoConsumoTeste(Util.converterCampoStringParaValorInteger(Constantes.FORMATO_DATA_BR, prazoTeste));
		}
		if (StringUtils.isNotEmpty(volumeTeste)) {
			contratoPontoConsumo.setMedidaConsumoTeste(Util.converterCampoStringParaValorBigDecimal(LABEL_VOLUME_TESTE, volumeTeste,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isNotEmpty(faxDDD)) {
			contratoPontoConsumo.setCodigoDDDFaxOperacional(Util.converterCampoStringParaValorInteger(Contrato.CONTRATO_DDD, faxDDD));
		}
		if (StringUtils.isNotEmpty(numeroFaxOperacional)) {
			contratoPontoConsumo.setNumeroFaxOperacional(Integer.valueOf(numeroFaxOperacional));
		}
		if (StringUtils.isNotEmpty(email)) {
			contratoPontoConsumo.setEmailOperacional(email);
		}
		if (StringUtils.isNotEmpty(inicioGarantiaConversao)) {
			contratoPontoConsumo.setDataInicioGarantiaConversao(
					Util.converterCampoStringParaData(LABEL_DATA, inicioGarantiaConversao, Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(numeroDiasGarantia)) {
			contratoPontoConsumo.setDiaGarantiaConversao(
					Util.converterCampoStringParaValorInteger(Contrato.TEMPO_GARANTIA_CONVERSAO, numeroDiasGarantia));
		}
	}

	/**
	 * Popular contrato dados aba dados tecnicos.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param abaDadosTecnicos the aba dados tecnicos
	 * @throws GGASException the GGAS exception
	 * @throws NumberFormatException the number format exception
	 */
	private void popularContratoDadosAbaDadosTecnicos(ContratoPontoConsumo contratoPontoConsumo, Map<String, Object> abaDadosTecnicos)
			throws GGASException {

		String vazaoInstantanea = (String) abaDadosTecnicos.get(VAZAO_INSTANTANEA);
		String unidadeVazaoInstan = (String) abaDadosTecnicos.get(UNIDADE_VAZAO_INSTAN);
		String vazaoInstantaneaMaxima = (String) abaDadosTecnicos.get(VAZAO_INSTANTANEA_MAXIMA);
		String unidadeVazaoInstanMaxima = (String) abaDadosTecnicos.get(UNIDADE_VAZAO_INSTAN_MAXIMA);
		String vazaoInstantaneaMinima = (String) abaDadosTecnicos.get(VAZAO_INSTANTANEA_MINIMA);
		String unidadeVazaoInstanMinima = (String) abaDadosTecnicos.get(UNIDADE_VAZAO_INSTAN_MINIMA);
		String faixaPressaoFornecimento = (String) abaDadosTecnicos.get(FAIXA_PRESSAO_FORNECIMENTO);
		String pressaoMinimaFornecimento = (String) abaDadosTecnicos.get(PRESSAO_MINIMA_FORNECIMENTO);
		String unidadePressaoMinimaFornec = (String) abaDadosTecnicos.get(UNIDADE_PRESSAO_MINIMA_FORNEC);
		String pressaoColetada = (String) abaDadosTecnicos.get(PRESSAO_COLETADA);
		String unidadePressaoColetada = (String) abaDadosTecnicos.get(UNIDADE_PRESSAO_COLETADA);
		String pressaoMaximaFornecimento = (String) abaDadosTecnicos.get(PRESSAO_MAXIMA_FORNECIMENTO);
		String unidadePressaoMaximaFornec = (String) abaDadosTecnicos.get(UNIDADE_PRESSAO_MAXIMA_FORNEC);
		String pressaoLimiteFornecimento = (String) abaDadosTecnicos.get(PRESSAO_LIMITE_FORNECIMENTO);
		String unidadePressaoLimiteFornec = (String) abaDadosTecnicos.get(UNIDADE_PRESSAO_LIMITE_FORNEC);
		String numAnosCtrlParadaCliente = (String) abaDadosTecnicos.get(NUM_ANOS_CTRL_PARADA_CLIENTE);
		String maxTotalParadasCliente = (String) abaDadosTecnicos.get(MAX_TOTAL_PARADAS_CLIENTE);
		String maxAnualParadasCliente = (String) abaDadosTecnicos.get(MAX_ANUAL_PARADAS_CLIENTE);
		String numDiasProgrParadaCliente = (String) abaDadosTecnicos.get(NUM_DIAS_PROGR_PARADA_CLIENTE);
		String numDiasConsecParadaCliente = (String) abaDadosTecnicos.get(NUM_DIAS_CONSEC_PARADA_CLIENTE);
		String numAnosCtrlParadaCDL = (String) abaDadosTecnicos.get(NUM_ANOS_CTRL_PARADA_CDL);
		String maxTotalParadasCDL = (String) abaDadosTecnicos.get(MAX_TOTAL_PARADAS_CDL);
		String maxAnualParadasCDL = (String) abaDadosTecnicos.get(MAX_ANUAL_PARADAS_CDL);
		String numDiasProgrParadaCDL = (String) abaDadosTecnicos.get(NUM_DIAS_PROGR_PARADA_CDL);
		String numDiasConsecParadaCDL = (String) abaDadosTecnicos.get(NUM_DIAS_CONSEC_PARADA_CDL);

		Boolean indicadorParadaProgramada = Boolean.FALSE;

		if (StringUtils.isNotEmpty(vazaoInstantanea) && StringUtils.isNotEmpty(unidadeVazaoInstan)
				&& Long.parseLong(unidadeVazaoInstan) > 0) {
			contratoPontoConsumo.setMedidaVazaoInstantanea(Util.converterCampoStringParaValorBigDecimal(LABEL_VAZAO_INSTANTANEA,
					vazaoInstantanea, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo.setUnidadeVazaoInstantanea((Unidade) controladorUnidade.obter(Long.parseLong(unidadeVazaoInstan)));
		}
		if (StringUtils.isNotEmpty(vazaoInstantaneaMaxima) && StringUtils.isNotEmpty(unidadeVazaoInstanMaxima)
				&& Long.parseLong(unidadeVazaoInstanMaxima) > 0) {
			contratoPontoConsumo.setMedidaVazaoMaximaInstantanea(Util.converterCampoStringParaValorBigDecimal(
					LABEL_VAZAO_INSTANTANEA_MAXIMA, vazaoInstantaneaMaxima, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo
					.setUnidadeVazaoMaximaInstantanea((Unidade) controladorUnidade.obter(Long.parseLong(unidadeVazaoInstanMaxima)));
		}
		if (StringUtils.isNotEmpty(vazaoInstantaneaMinima) && StringUtils.isNotEmpty(unidadeVazaoInstanMinima)
				&& Long.parseLong(unidadeVazaoInstanMinima) > 0) {
			contratoPontoConsumo.setMedidaVazaoMinimaInstantanea(Util.converterCampoStringParaValorBigDecimal(
					LABEL_VAZAO_INSTANTANEA_MINIMA, vazaoInstantaneaMinima, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo
					.setUnidadeVazaoMinimaInstantanea((Unidade) controladorUnidade.obter(Long.parseLong(unidadeVazaoInstanMinima)));
		}
		if (StringUtils.isNotEmpty(faixaPressaoFornecimento) && Long.parseLong(faixaPressaoFornecimento) > 0) {
			FaixaPressaoFornecimento faixaPressao =
					(FaixaPressaoFornecimento) controladorFaixaPressaoFornecimento.obter(Long.parseLong(faixaPressaoFornecimento));
			contratoPontoConsumo.setMedidaPressao(faixaPressao.getMedidaMinimo());
			contratoPontoConsumo.setUnidadePressao(faixaPressao.getUnidadePressao());
			contratoPontoConsumo.setFaixaPressaoFornecimento(faixaPressao);
			contratoPontoConsumo.setNumeroFatorCorrecao(faixaPressao.getNumeroFatorCorrecaoPTZPCS());

		}
		if (StringUtils.isNotEmpty(pressaoColetada) && StringUtils.isNotEmpty(unidadePressaoColetada)
				&& Long.parseLong(unidadePressaoColetada) > 0) {
			contratoPontoConsumo.setMedidaPressaoColetada(Util.converterCampoStringParaValorBigDecimal(LABEL_PRESSAO_COLETADA,
					pressaoColetada, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo.setUnidadePressaoColetada((Unidade) controladorUnidade.obter(Long.parseLong(unidadePressaoColetada)));
		}
		if (StringUtils.isNotEmpty(pressaoMinimaFornecimento) && StringUtils.isNotEmpty(unidadePressaoMinimaFornec)
				&& Long.parseLong(unidadePressaoMinimaFornec) > 0) {
			contratoPontoConsumo.setMedidaPressaoMinima(Util.converterCampoStringParaValorBigDecimal(LABEL_PRESSAO_MINIMA_FORNECIMENTO,
					pressaoMinimaFornecimento, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo.setUnidadePressaoMinima((Unidade) controladorUnidade.obter(Long.parseLong(unidadePressaoMinimaFornec)));
		}
		if (StringUtils.isNotEmpty(pressaoMaximaFornecimento) && StringUtils.isNotEmpty(unidadePressaoMaximaFornec)
				&& Long.parseLong(unidadePressaoMaximaFornec) > 0) {
			contratoPontoConsumo.setMedidaPressaoMaxima(Util.converterCampoStringParaValorBigDecimal(LABEL_PRESSAO_MAXIMA_FORNECIMENTO,
					pressaoMaximaFornecimento, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo.setUnidadePressaoMaxima((Unidade) controladorUnidade.obter(Long.parseLong(unidadePressaoMaximaFornec)));
		}
		if (StringUtils.isNotEmpty(pressaoLimiteFornecimento) && StringUtils.isNotEmpty(unidadePressaoLimiteFornec)
				&& Long.parseLong(unidadePressaoLimiteFornec) > 0) {
			contratoPontoConsumo.setMedidaPressaoLimite(Util.converterCampoStringParaValorBigDecimal(LABEL_PRESSAO_LIMITE_FORNECIMENTO,
					pressaoLimiteFornecimento, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			contratoPontoConsumo.setUnidadePressaoLimite((Unidade) controladorUnidade.obter(Long.parseLong(unidadePressaoLimiteFornec)));
		}
		if (StringUtils.isNotEmpty(numAnosCtrlParadaCliente)) {
			contratoPontoConsumo.setQuantidadeAnosParadaCliente(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_ANOS_CTRL_PARADA_CLIENTE, numAnosCtrlParadaCliente));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(maxTotalParadasCliente)) {
			contratoPontoConsumo.setQuantidadeTotalParadaCliente(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.MAX_TOTAL_PARADAS_CLIENTE, maxTotalParadasCliente));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(maxAnualParadasCliente)) {
			contratoPontoConsumo.setQuantidadeMaximaAnoParadaCliente(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.MAX_ANUAL_PARADAS_CLIENTE, maxAnualParadasCliente));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(numDiasProgrParadaCliente)) {
			contratoPontoConsumo.setDiasAntecedentesParadaCliente(Util
					.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_DIAS_PROGR_PARADA_CLIENTE, numDiasProgrParadaCliente));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(numDiasConsecParadaCliente)) {
			contratoPontoConsumo.setDiasConsecutivosParadaCliente(Util
					.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_DIAS_CONSEC_PARADA_CLIENTE, numDiasConsecParadaCliente));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(numAnosCtrlParadaCDL)) {
			contratoPontoConsumo.setQuantidadeAnosParadaCDL(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_ANOS_CTRL_PARADA_CDL, numAnosCtrlParadaCDL));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(maxTotalParadasCDL)) {
			contratoPontoConsumo.setQuantidadeTotalParadaCDL(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.MAX_TOTAL_PARADAS_CDL, maxTotalParadasCDL));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(maxAnualParadasCDL)) {
			contratoPontoConsumo.setQuantidadeMaximaAnoParadaCDL(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.MAX_ANUAL_PARADAS_CDL, maxAnualParadasCDL));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(numDiasProgrParadaCDL)) {
			contratoPontoConsumo.setDiasAntecedentesParadaCDL(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_DIAS_PROGR_PARADA_CDL, numDiasProgrParadaCDL));
			indicadorParadaProgramada = Boolean.TRUE;
		}
		if (StringUtils.isNotEmpty(numDiasConsecParadaCDL)) {
			contratoPontoConsumo.setDiasConsecutivosParadaCDL(
					Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.NUM_DIAS_CONSEC_PARADA_CDL, numDiasConsecParadaCDL));
			indicadorParadaProgramada = Boolean.TRUE;
		}

		contratoPontoConsumo.setIndicadorParadaProgramada(indicadorParadaProgramada);

	}

	/**
	 * Popular contrato dados aba consumo.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param abaPrincipal the aba principal
	 * @throws GGASException the exception
	 */
	private void popularContratoDadosAbaConsumo(ContratoPontoConsumo contratoPontoConsumo, Map<String, Object> abaPrincipal)
			throws GGASException {

		String localAmostragemPCS = (String) abaPrincipal.get(LOCAL_AMOSTRAGEM_PCS);
		String intervaloRecuperacaoPCS = (String) abaPrincipal.get(INTERVALO_RECUPERACAO_PCS);
		String tamReducaoRecuperacaoPCS = (String) abaPrincipal.get(TAM_REDUCAO_RECUPERACAO_PCS);
		String horaInicialDia = (String) abaPrincipal.get(HORA_INICIAL_DIA);
		String regimeConsumo = (String) abaPrincipal.get(REGIME_CONSUMO);

		String fornecimentoMaximoDiario = (String) abaPrincipal.get(FORNECIMENTO_MAXIMO_DIARIO);
		String unidadeFornecMaximoDiario = (String) abaPrincipal.get(UNIDADE_FORNEC_MAXIMO_DIARIO);

		String fornecimentoMinimoDiario = (String) abaPrincipal.get(FORNECIMENTO_MINIMO_DIARIO);
		String unidadeFornecMinimoDiario = (String) abaPrincipal.get(UNIDADE_FORNEC_MINIMO_DIARIO);

		String fornecimentoMinimoMensal = (String) abaPrincipal.get(FORNECIMENTO_MINIMO_MENSAL);
		String unidadeFornecMinimoMensal = (String) abaPrincipal.get(UNIDADE_FORNEC_MINIMO_MENSAL);

		String fornecimentoMinimoAnual = (String) abaPrincipal.get(FORNECIMENTO_MINIMO_ANUAL);
		String unidadeFornecMinimoAnual = (String) abaPrincipal.get(UNIDADE_FORNEC_MINIMO_ANUAL);

		String fatorUnicoCorrecao = (String) abaPrincipal.get(FATOR_UNICO_CORRECAO);
		String consumoFatFalhaMedicao = (String) abaPrincipal.get(CONSUMO_FAT_FALHA_MEDICAO);

		if (StringUtils.isNotEmpty(localAmostragemPCS)) {

			Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoAmostragem =
					new LinkedHashSet<ContratoPontoConsumoPCSAmostragem>();
			String[] locaisAmostragem = localAmostragemPCS.split(Constantes.STRING_VIRGULA);

			for (int i = 0; i < locaisAmostragem.length; i++) {

				EntidadeConteudo entidadeLocalAmostragemPCS = controladorEntidadeConteudo
						.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(LABEL_LOCAL_DE_AMOSTRAGEM, locaisAmostragem[i]));

				ContratoPontoConsumoPCSAmostragem contratoPontoAmostragem =
						(ContratoPontoConsumoPCSAmostragem) controladorContrato.criarContratoPontoConsumoPCSAmostragem();
				contratoPontoAmostragem.setContratoPontoConsumo(contratoPontoConsumo);
				contratoPontoAmostragem.setLocalAmostragemPCS(entidadeLocalAmostragemPCS);
				contratoPontoAmostragem.setPrioridade(i);
				contratoPontoAmostragem.setHabilitado(true);
				contratoPontoAmostragem.setUltimaAlteracao(Calendar.getInstance().getTime());

				listaContratoPontoAmostragem.add(contratoPontoAmostragem);

			}

			contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().clear();
			contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().addAll(listaContratoPontoAmostragem);

		}

		if (StringUtils.isNotEmpty(intervaloRecuperacaoPCS)) {

			Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoIntervalo =
					new LinkedHashSet<ContratoPontoConsumoPCSIntervalo>();
			String[] invervalosRecuperacaoPCS = intervaloRecuperacaoPCS.split(Constantes.STRING_VIRGULA);

			for (int i = 0; i < invervalosRecuperacaoPCS.length; i++) {

				IntervaloPCS intervaloPCS = controladorHistoricoConsumo
						.obterIntervaloPCS(Util.converterCampoStringParaValorLong(LOCAL_INTERVALO_AMOSTRAGEM, invervalosRecuperacaoPCS[i]));

				ContratoPontoConsumoPCSIntervalo contratoPontoConsumoIntervaloPCS =
						(ContratoPontoConsumoPCSIntervalo) controladorContrato.criarContratoPontoConsumoPCSIntervalo();
				contratoPontoConsumoIntervaloPCS.setContratoPontoConsumo(contratoPontoConsumo);
				contratoPontoConsumoIntervaloPCS.setIntervaloPCS(intervaloPCS);
				contratoPontoConsumoIntervaloPCS.setPrioridade(i);
				if (StringUtils.isNotEmpty(tamReducaoRecuperacaoPCS)) {
					contratoPontoConsumoIntervaloPCS.setTamanho(Util.converterCampoStringParaValorInteger(
							LABEL_TAMANHO_DA_REDUCAO_PARA_RECUPERACAO_DO_PCS, tamReducaoRecuperacaoPCS));
				}
				contratoPontoConsumoIntervaloPCS.setHabilitado(true);
				contratoPontoConsumoIntervaloPCS.setUltimaAlteracao(Calendar.getInstance().getTime());

				listaContratoPontoIntervalo.add(contratoPontoConsumoIntervaloPCS);
			}

			contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().clear();
			contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().addAll(listaContratoPontoIntervalo);

		}

		if (StringUtils.isNotEmpty(horaInicialDia)) {
			contratoPontoConsumo.setNumeroHoraInicial(Util.converterCampoStringParaValorInteger(LABEL_HORA_INICIAL_DO_DIA, horaInicialDia));
		}
		if (StringUtils.isNotEmpty(regimeConsumo) && !regimeConsumo.equals(NAO_SELECIONADO)) {
			EntidadeConteudo regimeConsumoEntidade = controladorEntidadeConteudo
					.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(LABEL_REGIME_DE_CONSUMO, regimeConsumo));
			contratoPontoConsumo.setRegimeConsumo(regimeConsumoEntidade);
		}
		if (StringUtils.isNotEmpty(fornecimentoMaximoDiario)) {
			contratoPontoConsumo.setMedidaFornecimentoMaxDiaria(Util.converterCampoStringParaValorBigDecimal(
					LABEL_FORNECIMENTO_MAXIMO_DIARIO, fornecimentoMaximoDiario, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isNotEmpty(unidadeFornecMaximoDiario)) {
			Unidade unidadeFornecMaxDiario = (Unidade) controladorUnidade
					.obter(Util.converterCampoStringParaValorLong(LABEL_FORNECIMENTO_MAXIMO_DIARIO, unidadeFornecMaximoDiario));
			contratoPontoConsumo.setUnidadeFornecimentoMaxDiaria(unidadeFornecMaxDiario);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoDiario)) {
			contratoPontoConsumo.setMedidaFornecimentoMinDiaria(Util.converterCampoStringParaValorBigDecimal(
					LABEL_FORNECIMENTO_MINIMO_DIARIO, fornecimentoMinimoDiario, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isNotEmpty(unidadeFornecMinimoDiario)) {
			Unidade unidadeFornecMinDiario = (Unidade) controladorUnidade
					.obter(Util.converterCampoStringParaValorLong(LABEL_FORNECIMENTO_MINIMO_DIARIO, unidadeFornecMinimoDiario));
			contratoPontoConsumo.setUnidadeFornecimentoMinDiaria(unidadeFornecMinDiario);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoMensal)) {
			contratoPontoConsumo.setMedidaFornecimentoMinMensal(Util.converterCampoStringParaValorBigDecimal(
					LABEL_FORNECIMENTO_MINIMO_MENSAL, fornecimentoMinimoMensal, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isNotEmpty(unidadeFornecMinimoMensal)) {
			Unidade unidadeFornecMinMensal = (Unidade) controladorUnidade
					.obter(Util.converterCampoStringParaValorLong(LABEL_FORNECIMENTO_MINIMO_MENSAL, unidadeFornecMinimoMensal));
			contratoPontoConsumo.setUnidadeFornecimentoMinMensal(unidadeFornecMinMensal);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoAnual)) {
			contratoPontoConsumo.setMedidaFornecimentoMinAnual(Util.converterCampoStringParaValorBigDecimal(LABEL_FORNECIMENTO_MINIMO_ANUAL,
					fornecimentoMinimoAnual, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (StringUtils.isNotEmpty(unidadeFornecMinimoAnual)) {
			Unidade unidadeFornecMinAnual = (Unidade) controladorUnidade
					.obter(Util.converterCampoStringParaValorLong(LABEL_FORNECIMENTO_MINIMO_ANUAL, unidadeFornecMinimoAnual));
			contratoPontoConsumo.setUnidadeFornecimentoMinAnual(unidadeFornecMinAnual);
		}
		if (StringUtils.isNotEmpty(fatorUnicoCorrecao)) {
			BigDecimal fatorCorrecao = Util.converterCampoStringParaValorBigDecimal(ContratoPontoConsumo.FATOR_UNICO_CORRECAO,
					fatorUnicoCorrecao, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			if (fatorCorrecao.compareTo(BigDecimal.ZERO) > 0) {
				contratoPontoConsumo.setNumeroFatorCorrecao(Util.converterCampoStringParaValorBigDecimal("Fator Único Correção",
						fatorUnicoCorrecao, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			}
		}
		if (StringUtils.isNotEmpty(consumoFatFalhaMedicao) && !NUMERO_MENOS_UM.equals(consumoFatFalhaMedicao)) {

			AcaoAnormalidadeConsumo consumoFatorFalhaMedicao = controladorAnormalidade.obterAcaoAnormalidadeConsumo(
					Util.converterCampoStringParaValorLong("Consumo a ser Faturado em Falha de Medição", consumoFatFalhaMedicao));
			contratoPontoConsumo.setAcaoAnormalidadeConsumoSemLeitura(consumoFatorFalhaMedicao);
		}
	}

	/**
	 * Popular contrato dados aba faturamento.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param abaFaturamento the aba faturamento
	 * @param request the request
	 * @throws GGASException the exception
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoDadosAbaFaturamento(ContratoPontoConsumo contratoPontoConsumo, Map<String, Object> abaFaturamento,
			HttpServletRequest request) throws GGASException {

		String endFisEntFatCEP = (String) abaFaturamento.get(END_FIS_ENT_FAT_CEP);
		String endFisEntFatNumero = (String) abaFaturamento.get(END_FIS_ENT_FAT_NUMERO);
		String endFisEntFatComplemento = (String) abaFaturamento.get(END_FIS_ENT_FAT_COMPLEMENTO);
		String endFisEntFatEmail = (String) abaFaturamento.get(END_FIS_ENT_FAT_EMAIL);
		String chaveCep = (String) abaFaturamento.get(CHAVE_CEP);
		String contratoCompra = (String) abaFaturamento.get(CONTRATO_COMPRA);
		String periodicidade = (String) abaFaturamento.get(FAT_PERIODICIDADE);
		String notaFiscalAtiva = (String) abaFaturamento.get(NOTA_FISCAL_ELETRONICA);

		String emitirFaturaComNfe = (String) abaFaturamento.get(EMITIR_FATURA_COM_NFE);

		Collection<ContratoPontoConsumoItemFaturamentoVO> listaItemFaturamentoContratoVO =
				(Collection<ContratoPontoConsumoItemFaturamentoVO>) abaFaturamento.get(ITEM_FATURA);

		if (StringUtils.isNotEmpty(notaFiscalAtiva)) {
			contratoPontoConsumo.setIsEmiteNotaFiscalEletronica(Boolean.parseBoolean(notaFiscalAtiva));
		}
		if (StringUtils.isNotEmpty(emitirFaturaComNfe)) {
			contratoPontoConsumo.setEmitirFaturaComNfe(Boolean.parseBoolean(emitirFaturaComNfe));
		}
		if ((StringUtils.isNotEmpty(endFisEntFatCEP)) && (StringUtils.isNotEmpty(chaveCep))) {
			Cep cep = controladorEndereco.obterCepPorChave(Util.converterCampoStringParaValorLong(LABEL_CEP, chaveCep));
			contratoPontoConsumo.setCep(cep);
		}
		if (StringUtils.isNotEmpty(endFisEntFatNumero)) {
			contratoPontoConsumo.setNumeroImovel(endFisEntFatNumero);
		}
		if (StringUtils.isNotEmpty(endFisEntFatComplemento)) {
			contratoPontoConsumo.setComplementoEndereco(endFisEntFatComplemento);
		}
		if (StringUtils.isNotEmpty(endFisEntFatEmail)) {
			contratoPontoConsumo.setEmailEntrega(endFisEntFatEmail);
		}
		if (StringUtils.isNotEmpty(contratoCompra) && !contratoCompra.equals(NAO_SELECIONADO)) {
			EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(contratoCompra));
			contratoPontoConsumo.setContratoCompra(entidadeConteudo);
		}
		if (StringUtils.isNotEmpty(periodicidade) && !periodicidade.equals(NAO_SELECIONADO)) {
			Periodicidade periodicidadePesquisada = controladorRota.obterPeriodicidade(Long.valueOf(periodicidade));
			contratoPontoConsumo.setPeriodicidade(periodicidadePesquisada);
		}

		Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento =
				new HashSet<ContratoPontoConsumoItemFaturamento>();

		if (listaItemFaturamentoContratoVO != null && !listaItemFaturamentoContratoVO.isEmpty()) {

			for (ContratoPontoConsumoItemFaturamentoVO itemFaturamentoContratoVO : listaItemFaturamentoContratoVO) {

				ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemVencimento =
						(ContratoPontoConsumoItemFaturamento) controladorContrato.criarContratoPontoConsumoItemVencimento();
				contratoPontoConsumoItemVencimento.setContratoPontoConsumo(contratoPontoConsumo);
				contratoPontoConsumoItemVencimento.setDadosAuditoria(super.getDadosAuditoria(request));
				contratoPontoConsumoItemVencimento.setFaseReferencia(itemFaturamentoContratoVO.getFaseReferencia());
				contratoPontoConsumoItemVencimento
						.setIndicadorDepositoIdentificado(itemFaturamentoContratoVO.getIndicadorDepositoIdentificado());
				contratoPontoConsumoItemVencimento.setNumeroDiaVencimento(itemFaturamentoContratoVO.getNumeroDiaVencimento());
				contratoPontoConsumoItemVencimento.setItemFatura(itemFaturamentoContratoVO.getItemFatura());
				contratoPontoConsumoItemVencimento.setOpcaoFaseReferencia(itemFaturamentoContratoVO.getOpcaoFaseReferencia());
				contratoPontoConsumoItemVencimento.setTarifa(itemFaturamentoContratoVO.getTarifa());
				contratoPontoConsumoItemVencimento.setVencimentoDiaUtil(itemFaturamentoContratoVO.getVencimentoDiaUtil());
				contratoPontoConsumoItemVencimento.setHabilitado(true);
				contratoPontoConsumoItemVencimento.setUltimaAlteracao(Calendar.getInstance().getTime());
				contratoPontoConsumoItemVencimento.setDiaCotacao(itemFaturamentoContratoVO.getDiaCotacao());
				contratoPontoConsumoItemVencimento.setDataReferenciaCambial(itemFaturamentoContratoVO.getDataReferenciaCambial());
				contratoPontoConsumoItemVencimento.setPercminimoQDC(itemFaturamentoContratoVO.getPercminimoQDC());
				listaContratoPontoConsumoItemFaturamento.add(contratoPontoConsumoItemVencimento);
			}
		}

		contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().clear();
		contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().addAll(listaContratoPontoConsumoItemFaturamento);

	}

	/**
	 * Popular contrato dados aba responsabilidades.
	 *
	 * @param contrato the contrato
	 * @param listaContratoCliente the lista contrato cliente
	 * @param abaResponsabilidades the aba responsabilidades
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoDadosAbaResponsabilidades(Contrato contrato, Collection<ContratoCliente> listaContratoCliente,
			Map<String, Object> abaResponsabilidades) {

		Collection<ContratoCliente> listaContratoClienteSessao =
				(Collection<ContratoCliente>) abaResponsabilidades.get(LISTA_CONTRATO_CLIENTE);

		if (listaContratoClienteSessao != null && !listaContratoClienteSessao.isEmpty()) {
			for (ContratoCliente contratoCliente : listaContratoClienteSessao) {
				contratoCliente.setContrato(contrato);
				listaContratoCliente.add(contratoCliente);
			}
		}
	}

	/**
	 * Método responsável por reexibir a tela de inclusão de contrato passo 2.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("reexibirInclusaoContratoPontoConsumo")
	public String reexibirInclusaoContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Boolean isFluxoDetalhamento = contratoVO.getFluxoDetalhamento();
		Boolean isFluxoAditamento = contratoVO.getFluxoAditamento();
		Boolean isFluxoAlteracao = contratoVO.getFluxoAlteracao();
		Boolean isFluxoInclusao = contratoVO.getFluxoInclusao();
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String cepAlterado = contratoVO.getEndFisEntFatCEP();
		String chaveCepAlterado = contratoVO.getChaveCep();
		
		try {

			this.carregarCamposPasso2(request, contratoVO, model);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
				this.tratarCamposSelectAbaConsumo(contratoVO, model);
			}

			model.addAttribute(MODELO_CONTRATO, modeloContrato);

			Collection<ContratoCliente> listaContratoCliente =
					(Collection<ContratoCliente>) request.getSession().getAttribute(LISTA_CONTRATO_CLIENTE);
			if (listaContratoCliente != null) {
				model.addAttribute(LISTA_CONTRATO_CLIENTE, exibirListaResponsabilidades(listaContratoCliente));
			}

			if ((Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession()
					.getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO) != null) {
				model.addAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO,
						request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO));
			}

			if (super.isPostBack(request)) {
				Collection<ModalidadeConsumoFaturamentoVO> listaModalidadeConsumoFaturamentoVO =
						(Collection<ModalidadeConsumoFaturamentoVO>) request.getSession()
								.getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
				model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, listaModalidadeConsumoFaturamentoVO);
			}
			
			if (cepAlterado != null && !cepAlterado.isEmpty() && chaveCepAlterado != null
					&& !chaveCepAlterado.isEmpty()) {
				contratoVO.setEndFisEntFatCEP(cepAlterado);
				contratoVO.setChaveCep(chaveCepAlterado);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if (isFluxoDetalhamento) {
			view = "exibirDetalhamentoContratoPontoConsumo";
		} else if (isFluxoAditamento || isFluxoAlteracao) {
			view = "exibirAditamentoContratoPontoConsumo";
		} else if (isFluxoInclusao) {
			view = "exibirInclusaoContratoPontoConsumo";
		}

		return view;
	}

	/**
	 * Aplica os dados do formulário do passo 2 nos pontos de consumo selecionados.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 */
	@RequestMapping(value = { "aplicarDadosContratoPontoConsumoAditamento", "aplicarDadosContratoPontoConsumoInclusao" })
	public String aplicarDadosContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, InvocationTargetException, IllegalAccessException {

		String view = null;
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		Long chavePontoConsumo = contratoVO.getIdPontoConsumo();
		List<Long> listaChavePontoConsumo = new ArrayList<Long>(Arrays.asList((Long[]) contratoVO.getChavesPrimariasPontoConsumo()));

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		if (listaPontoConsumoContratoVO == null) {
			listaPontoConsumoContratoVO = new HashMap<Long, PontoConsumoContratoVO>();
		}

		try {

			if (chavePontoConsumo != null && chavePontoConsumo > 0) {
				aplicarDadosAbaPrincipal(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, true);
				aplicarDadosAbaDadosTecnicos(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, true);
				aplicarDadosAbaConsumo(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, true);
				aplicarDadosAbaFaturamento(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, true);
				aplicarDadosAbaModalidades(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, true);
				aplicarDadosAbaResponsabilidade(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, true);
				contratoVO.setIdPontoConsumo(null);
			}

			request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
			request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
			request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);

			request.getSession().removeAttribute(PONTO_CONSUMO_EM_ALTERACAO);

			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);

			Long idCliente = contratoVO.getIdCliente();

			this.limparDadosAbas(contratoVO, request, model);
			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
			}
			model.addAttribute(MODELO_CONTRATO, modeloContrato);

			contratoVO.setChavesPrimariasPontoConsumo(null);
			this.carregarCamposPasso2(request, contratoVO, model);

			contratoVO.setIdCliente(idCliente);

			ListIterator<Long> itPontoConsumo = listaChavePontoConsumo.listIterator(listaChavePontoConsumo.indexOf(chavePontoConsumo));
			itPontoConsumo.next();
			if (itPontoConsumo.hasNext()) {
				Long chaveProximoPontoConsumo = itPontoConsumo.next();
				contratoVO.setIdPontoConsumo(chaveProximoPontoConsumo);
				view = popularCamposContratoPontoConsumo(contratoVO, result, request, model);
			} else {
				view = reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Tratar campos select aba consumo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void tratarCamposSelectAbaConsumo(ContratoVO contratoVO, Model model) throws GGASException {

		Long[] idsLocalAmostragemAssociados = contratoVO.getIdsLocalAmostragemAssociados();
		Long[] idsIntervaloAmostragemAssociados = contratoVO.getIdsIntervaloAmostragemAssociados();

		Collection<EntidadeConteudo> listaLocalAmostragemAssociados = new ArrayList<EntidadeConteudo>();
		if (idsLocalAmostragemAssociados != null && idsLocalAmostragemAssociados.length > 0) {
			for (Long chaveLocal : idsLocalAmostragemAssociados) {
				EntidadeConteudo localAmostragem = controladorEntidadeConteudo.obterEntidadeConteudo(chaveLocal);
				listaLocalAmostragemAssociados.add(localAmostragem);
			}

			Collection<EntidadeConteudo> listaLocalAmostragemDisp = controladorEntidadeConteudo.listarLocalAmostragemPCS();
			listaLocalAmostragemDisp.removeAll(listaLocalAmostragemAssociados);
			model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, listaLocalAmostragemDisp);
			model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS, listaLocalAmostragemAssociados);
		}

		Collection<IntervaloPCS> listaIntervaloAmostragemAssociados = new ArrayList<IntervaloPCS>();
		if (idsIntervaloAmostragemAssociados != null && idsIntervaloAmostragemAssociados.length > 0) {
			for (Long chaveIntervalo : idsIntervaloAmostragemAssociados) {
				IntervaloPCS intervaloPCS = controladorHistoricoConsumo.obterIntervaloPCS(chaveIntervalo);
				listaIntervaloAmostragemAssociados.add(intervaloPCS);
			}

			Collection<IntervaloPCS> listaIntervaloPCSDisp = controladorHistoricoConsumo.listarIntervaloPCS();
			listaIntervaloPCSDisp.removeAll(listaIntervaloAmostragemAssociados);
			model.addAttribute(LISTA_INTERVALO_PCS, listaIntervaloPCSDisp);
			model.addAttribute(LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS, listaIntervaloAmostragemAssociados);
		}

	}

	/**
	 * Popular campos dados tecnicos ponto de consumo vo.
	 *
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param contratoVO {@link ContratoVO}
	 */
	public void popularCamposDadosTecnicosPontoDeConsumoVO(PontoConsumoContratoVO pontoConsumoContratoVO, ContratoVO contratoVO) {

		if (pontoConsumoContratoVO != null) {
			contratoVO.setVazaoInstantanea((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(VAZAO_INSTANTANEA));
			contratoVO.setUnidadeVazaoInstan((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_VAZAO_INSTAN));
			contratoVO.setVazaoInstantaneaMinima((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(VAZAO_INSTANTANEA_MINIMA));
			contratoVO.setUnidadeVazaoInstanMinima((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_VAZAO_INSTAN_MINIMA));
			contratoVO.setVazaoInstantaneaMaxima((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(VAZAO_INSTANTANEA_MAXIMA));
			contratoVO.setUnidadeVazaoInstanMaxima((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_VAZAO_INSTAN_MAXIMA));
			contratoVO.setFaixaPressaoFornecimento((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(FAIXA_PRESSAO_FORNECIMENTO));
			contratoVO.setPressaoColetada((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(PRESSAO_COLETADA));
			contratoVO.setUnidadePressaoColetada((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_PRESSAO_COLETADA));
			contratoVO.setPressaoMinimaFornecimento((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(PRESSAO_MINIMA_FORNECIMENTO));
			contratoVO.setUnidadePressaoMinimaFornec(
					(String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_PRESSAO_MINIMA_FORNEC));
			contratoVO.setPressaoMaximaFornecimento((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(PRESSAO_MAXIMA_FORNECIMENTO));
			contratoVO.setUnidadePressaoMaximaFornec(
					(String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_PRESSAO_MAXIMA_FORNEC));
			contratoVO.setPressaoLimiteFornecimento((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(PRESSAO_LIMITE_FORNECIMENTO));
			contratoVO.setUnidadePressaoLimiteFornec(
					(String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(UNIDADE_PRESSAO_LIMITE_FORNEC));
			contratoVO.setNumAnosCtrlParadaCliente((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_ANOS_CTRL_PARADA_CLIENTE));
			contratoVO.setMaxTotalParadasCliente((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(MAX_TOTAL_PARADAS_CLIENTE));
			contratoVO.setMaxAnualParadasCliente((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(MAX_ANUAL_PARADAS_CLIENTE));
			contratoVO
					.setNumDiasProgrParadaCliente((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_DIAS_PROGR_PARADA_CLIENTE));
			contratoVO.setNumDiasConsecParadaCliente(
					(String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_DIAS_CONSEC_PARADA_CLIENTE));
			contratoVO.setNumAnosCtrlParadaCDL((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_ANOS_CTRL_PARADA_CDL));
			contratoVO.setMaxTotalParadasCDL((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(MAX_TOTAL_PARADAS_CDL));
			contratoVO.setMaxAnualParadasCDL((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(MAX_ANUAL_PARADAS_CDL));
			contratoVO.setNumDiasProgrParadaCDL((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_DIAS_PROGR_PARADA_CDL));
			contratoVO.setNumDiasConsecParadaCDL((String) pontoConsumoContratoVO.getAbaDadosTecnicos().get(NUM_DIAS_CONSEC_PARADA_CDL));
		}
	}

	/**
	 * Método responsável por popular os campos das abas do contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(value = { "popularCamposContratoPontoConsumoDetalhamento", "popularCamposContratoPontoConsumoInclusao",
			"popularCamposContratoPontoConsumoAditamento" })
	public String popularCamposContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Boolean isFluxoDetalhamento = contratoVO.getFluxoDetalhamento();
		Boolean isFluxoAditamento = contratoVO.getFluxoAditamento();
		Boolean isFluxoInclusao = contratoVO.getFluxoInclusao();
		Boolean isFluxoAlteracao = contratoVO.getFluxoAlteracao();

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		Long idPontoConsumo = contratoVO.getIdPontoConsumo();
		contratoVO.setChavesPrimariasPontoConsumo(new Long[] { idPontoConsumo });

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		PontoConsumoContratoVO pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(idPontoConsumo);

		request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
		request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
		request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);

		try {

			Map<String, Object> dados = this.popularCamposCPCInfoComum(request, contratoVO, model);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
				this.carregarCamposPasso2(request, contratoVO, model);
				this.popularCamposSelectAbaConsumo(request, contratoVO, dados, model);
			}

			model.addAttribute(ALTERANDO_PONTO_CONSUMO, true);
			model.addAttribute(MODELO_CONTRATO, modeloContrato);

			request.getSession().setAttribute(PONTO_CONSUMO_EM_ALTERACAO, idPontoConsumo);

			Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

			this.setarEnderecoFormatadoNoForm(contratoVO);
			this.preencherFormCampoFimGarantiaConversao(contratoVO);

			// preenche os dados de volta na tela para exibição.
			this.popularCamposDadosTecnicosPontoDeConsumoVO(pontoConsumoContratoVO, contratoVO);
			this.popularCamposAbaConsumo(pontoConsumoContratoVO, contratoVO);
			this.popularCamposAbaFaturamento(contratoSessao, pontoConsumoContratoVO, contratoVO);
			this.popularCamposAbaResponsabilidade(pontoConsumoContratoVO, contratoVO);
			this.popularCamposAbaModalidade(pontoConsumoContratoVO, contratoVO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if (isFluxoDetalhamento) {
			view = "exibirDetalhamentoContratoPontoConsumo";
		} else if (isFluxoAditamento) {
			view = "exibirAditamentoContratoPontoConsumo";
		} else if (isFluxoAlteracao) {
			view = "exibirAditamentoContratoPontoConsumo"; 
		} else if (isFluxoInclusao) {
			view = "exibirInclusaoContratoPontoConsumo";
		}

		return view;
	}

	/**
	 * Popular campos aba consumo.
	 *
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param contratoVO {@link ContratoVO}
	 */
	public void popularCamposAbaConsumo(PontoConsumoContratoVO pontoConsumoContratoVO, ContratoVO contratoVO) {

		if (pontoConsumoContratoVO != null) {
			contratoVO
					.setIdsLocalAmostragemAssociados((Long[]) pontoConsumoContratoVO.getAbaConsumo().get(IDS_LOCAL_AMOSTRAGEM_ASSOCIADOS));
			contratoVO.setIdsIntervaloAmostragemAssociados(
					(Long[]) pontoConsumoContratoVO.getAbaConsumo().get(IDS_INTERVALO_AMOSTRAGEM_ASSOCIADOS));
			contratoVO.setTamReducaoRecuperacaoPCS((String) pontoConsumoContratoVO.getAbaConsumo().get(TAM_REDUCAO_RECUPERACAO_PCS));
			contratoVO.setHoraInicialDia((String) pontoConsumoContratoVO.getAbaConsumo().get(HORA_INICIAL_DIA));
			contratoVO.setRegimeConsumo((String) pontoConsumoContratoVO.getAbaConsumo().get(REGIME_CONSUMO));
			contratoVO.setFornecimentoMaximoDiario((String) pontoConsumoContratoVO.getAbaConsumo().get(FORNECIMENTO_MAXIMO_DIARIO));
			contratoVO.setUnidadeFornecMaximoDiario((String) pontoConsumoContratoVO.getAbaConsumo().get(UNIDADE_FORNEC_MAXIMO_DIARIO));
			contratoVO.setFornecimentoMinimoDiario((String) pontoConsumoContratoVO.getAbaConsumo().get(FORNECIMENTO_MINIMO_DIARIO));
			contratoVO.setUnidadeFornecMinimoDiario((String) pontoConsumoContratoVO.getAbaConsumo().get(UNIDADE_FORNEC_MINIMO_DIARIO));
			contratoVO.setFornecimentoMinimoMensal((String) pontoConsumoContratoVO.getAbaConsumo().get(FORNECIMENTO_MINIMO_MENSAL));
			contratoVO.setUnidadeFornecMinimoMensal((String) pontoConsumoContratoVO.getAbaConsumo().get(UNIDADE_FORNEC_MINIMO_MENSAL));
			contratoVO.setFornecimentoMinimoAnual((String) pontoConsumoContratoVO.getAbaConsumo().get(FORNECIMENTO_MINIMO_ANUAL));
			contratoVO.setUnidadeFornecMinimoAnual((String) pontoConsumoContratoVO.getAbaConsumo().get(UNIDADE_FORNEC_MINIMO_ANUAL));
			contratoVO.setFatorUnicoCorrecao((String) pontoConsumoContratoVO.getAbaConsumo().get(FATOR_UNICO_CORRECAO));
			contratoVO.setConsumoFatFalhaMedicao((String) pontoConsumoContratoVO.getAbaConsumo().get(CONSUMO_FAT_FALHA_MEDICAO));
		}
	}

	/**
	 * Popular campos aba responsabilidade.
	 *
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	public void popularCamposAbaResponsabilidade(PontoConsumoContratoVO pontoConsumoContratoVO, ContratoVO contratoVO)
			throws GGASException {

		if (pontoConsumoContratoVO != null) {
			contratoVO.setTipoResponsabilidade((String) pontoConsumoContratoVO.getAbaResponsabilidades().get(TIPO_RESPONSABILIDADE));
			contratoVO.setDataInicioRelacao((String) pontoConsumoContratoVO.getAbaResponsabilidades().get(DATA_INICIO_RELACAO));
			contratoVO.setDataFimRelacao((String) pontoConsumoContratoVO.getAbaResponsabilidades().get(DATA_FIM_RELACAO));
		}
	}

	/**
	 * Popular campos aba modalidade.
	 *
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	public void popularCamposAbaModalidade(PontoConsumoContratoVO pontoConsumoContratoVO, ContratoVO contratoVO) throws GGASException {

		if (pontoConsumoContratoVO != null) {
			Map<String, Object> abaModalidade = pontoConsumoContratoVO.getAbaModalidade();

			contratoVO.setModalidade((String) abaModalidade.get(MODALIDADE));
			contratoVO.setQdsMaiorQDC((String) abaModalidade.get(QDS_MAIOR_QDC));
			contratoVO.setVariacaoSuperiorQDC((String) abaModalidade.get(VARIACAO_SUPERIOR_QDC));
			contratoVO.setDiasAntecSolicConsumo((String) abaModalidade.get(DIAS_ANTEC_SOLIC_CONSUMO));
			contratoVO.setNumMesesSolicConsumo((String) abaModalidade.get(NUM_MESES_SOLIC_CONSUMO));
			contratoVO.setConfirmacaoAutomaticaQDS((String) abaModalidade.get(CONFIRMACAO_AUTOMATICA_QDS));
			contratoVO.setPeriodicidadeTakeOrPay((String) abaModalidade.get(PERIODICIDADE_TAKE_OR_PAY));
			contratoVO.setConsumoReferenciaTakeOrPay((String) abaModalidade.get(CONSUMO_REFERENCIA_TAKE_OR_PAY));
			contratoVO.setMargemVariacaoTakeOrPay((String) abaModalidade.get(MARGEM_VARIACAO_TAKE_OR_PAY));
			contratoVO.setRecuperacaoAutoTakeOrPay((String) abaModalidade.get(RECUPERACAO_AUTO_TAKE_OR_PAY));
			contratoVO.setIndicePeriodicidadeTakeOrPay((Integer[]) abaModalidade.get(INDICE_PERIODICIDADE_TAKE_OR_PAY));
			contratoVO.setPercDuranteRetiradaQPNR((String) abaModalidade.get(PERC_DURANTE_RETIRADA_QPNR));
			contratoVO.setPercMinDuranteRetiradaQPNR((String) abaModalidade.get(PERC_MIN_DURANTE_RETIRADA_QPNR));
			contratoVO.setPercFimRetiradaQPNR((String) abaModalidade.get(PERC_FIM_RETIRADA_QPNR));
			contratoVO.setDataInicioRetiradaQPNR((String) abaModalidade.get(DATA_INICIO_RETIRADA_QPNR));
			contratoVO.setDataFimRetiradaQPNR((String) abaModalidade.get(DATA_FIM_RETIRADA_QPNR));
			contratoVO.setTempoValidadeRetiradaQPNR((String) abaModalidade.get(TEMPO_VALIDADE_RETIRADA_QPNR));
			contratoVO.setPeriodicidadeShipOrPay((String) abaModalidade.get(PERIODICIDADE_SHIP_OR_PAY));
			contratoVO.setConsumoReferenciaShipOrPay((String) abaModalidade.get(CONSUMO_REFERENCIA_SHIP_OR_PAY));
			contratoVO.setMargemVariacaoShipOrPay((String) abaModalidade.get(MARGEM_VARIACAO_SHIP_OR_PAY));
			contratoVO.setIndicePeriodicidadeShipOrPay((Integer[]) abaModalidade.get(INDICE_PERIODICIDADE_SHIP_OR_PAY));
			contratoVO.setIdsModalidadeContrato((Long[]) abaModalidade.get(IDS_MODALIDADE_CONTRATO));
			contratoVO.setPercentualQNR((String) abaModalidade.get(PERCENTUAL_QNR));
		}
	}

	/**
	 * Popular campos aba faturamento.
	 *
	 * @param contrato {@link Contrato}
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	public void popularCamposAbaFaturamento(Contrato contrato, PontoConsumoContratoVO pontoConsumoContratoVO, ContratoVO contratoVO)
			throws GGASException {

		Long idCliente = contratoVO.getIdCliente();
		String email = contratoVO.getEndFisEntFatEmail();
		String enderecoPadrao = contratoVO.getEnderecoPadrao();

		Cliente cliente = null;

		if (idCliente != null && idCliente != 0L && StringUtils.isEmpty(email)) {

			cliente = controladorCliente.obterCliente(idCliente, ENDERECOS);
			if (cliente != null) {
				contratoVO.setEndFisEntFatEmail(cliente.getEmailPrincipal());
			}
		}

		boolean clienteEndereco = Boolean.parseBoolean(enderecoPadrao);

		Cliente enderecoCliente = (Cliente) controladorCliente.obter(contrato.getClienteAssinatura().getChavePrimaria(), ENDERECOS);
		Long idPontoConsumo = contratoVO.getIdPontoConsumo();
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumoPorContratoPontoConsumo(contrato.getChavePrimaria(), idPontoConsumo);

		if(contratoPontoConsumo != null && contratoPontoConsumo.getCep() != null) {
			contratoVO.setEndFisEntFatCEP(contratoPontoConsumo.getCep().getCep());
			contratoVO.setEndFisEntFatComplemento(contratoPontoConsumo.getComplementoEndereco());
			contratoVO.setEndFisEntFatNumero(contratoPontoConsumo.getNumeroImovel());
			contratoVO.setEndFisEntFatEmail(contratoPontoConsumo.getEmailEntrega());
		} else if (clienteEndereco && enderecoCliente != null) {

			contratoVO.setEndFisEntFatCEP(enderecoCliente.getEnderecoPrincipal().getCep().getCep());
			contratoVO.setEndFisEntFatComplemento(enderecoCliente.getEnderecoPrincipal().getComplemento());
			contratoVO.setEndFisEntFatNumero(enderecoCliente.getEnderecoPrincipal().getNumero());
			contratoVO.setEndFisEntFatEmail(enderecoCliente.getEmailPrincipal());

		} else if (!clienteEndereco && enderecoPadrao != null) {
			checarPontoConsumo(contratoVO, idPontoConsumo);
		} else {
			contratoVO.setEndFisEntFatCEP(StringUtils.EMPTY);
			contratoVO.setEndFisEntFatComplemento(StringUtils.EMPTY);
			contratoVO.setEndFisEntFatNumero(StringUtils.EMPTY);
		}

		if (pontoConsumoContratoVO != null) {

			contratoVO.setContratoCompra((String) pontoConsumoContratoVO.getAbaFaturamento().get(CONTRATO_COMPRA));
			contratoVO.setFatPeriodicidade((String) pontoConsumoContratoVO.getAbaFaturamento().get(FAT_PERIODICIDADE));
			contratoVO.setIndicadorNFE((String) pontoConsumoContratoVO.getAbaFaturamento().get(NOTA_FISCAL_ELETRONICA));
			contratoVO.setTarifaConsumo((String) pontoConsumoContratoVO.getAbaFaturamento().get(TARIFA_CONSUMO));
			contratoVO.setPercminimoQDC((String) pontoConsumoContratoVO.getAbaFaturamento().get(PERC_MINIMO_QDC));
			contratoVO.setDataReferenciaCambial((String) pontoConsumoContratoVO.getAbaFaturamento().get(DATA_REFERENCIA_CAMBIAL));
			contratoVO.setDiaCotacao((String) pontoConsumoContratoVO.getAbaFaturamento().get(DIA_COTACAO));
			contratoVO.setDepositoIdentificado((String) pontoConsumoContratoVO.getAbaFaturamento().get(DEPOSITO_IDENTIFICADO));
			contratoVO.setDiaVencimentoItemFatura((String) pontoConsumoContratoVO.getAbaFaturamento().get(DIA_VENCIMENTO_ITEM_FATURA));
			contratoVO.setOpcaoVencimentoItemFatura((String) pontoConsumoContratoVO.getAbaFaturamento().get(OPCAO_VENCIMENTO_ITEM_FATURA));
			contratoVO.setFaseVencimentoItemFatura((String) pontoConsumoContratoVO.getAbaFaturamento().get(FASE_VENCIMENTO_ITEM_FATURA));
			contratoVO.setIndicadorVencDiaNaoUtil((String) pontoConsumoContratoVO.getAbaFaturamento().get(INDICADOR_VENC_DIA_NAO_UTIL));
			contratoVO.setEmitirFaturaComNfe((String) pontoConsumoContratoVO.getAbaFaturamento().get(EMITIR_FATURA_COM_NFE));
		}
	}

	private void checarPontoConsumo(ContratoVO contratoVO, Long idPontoConsumo) throws GGASException {
		if (idPontoConsumo != null && idPontoConsumo != 0L) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, "quadraFace", "quadraFace.endereco",
					"quadraFace.endereco.cep");

			if (pontoConsumo != null) {
				if (pontoConsumo.getCep() != null) {
					contratoVO.setEndFisEntFatCEP(pontoConsumo.getCep().getCep());
					if (pontoConsumo.getQuadraFace() != null && pontoConsumo.getQuadraFace().getEndereco() != null
							&& pontoConsumo.getQuadraFace().getEndereco().getCep() != null) {
						contratoVO.setChaveCep(String.valueOf(pontoConsumo.getQuadraFace().getEndereco().getCep().getChavePrimaria()));
					}
				}
				contratoVO.setEndFisEntFatComplemento(pontoConsumo.getDescricaoComplemento());
				contratoVO.setEndFisEntFatNumero(pontoConsumo.getNumeroImovel());
			}
		}
	}

	/**
	 * Preencher form campo fim garantia conversao.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	public void preencherFormCampoFimGarantiaConversao(ContratoVO contratoVO) throws GGASException {

		String inicioGarantiaConversao = contratoVO.getInicioGarantiaConversao();
		String numeroDiasGarantia = contratoVO.getNumeroDiasGarantia();

		if (StringUtils.isNotEmpty(inicioGarantiaConversao) && StringUtils.isNotEmpty(numeroDiasGarantia)) {
			DateTime date =
					new DateTime(Util.converterCampoStringParaData(LABEL_DATA, inicioGarantiaConversao, Constantes.FORMATO_DATA_BR));
			Integer quantidadeDias = Util.converterCampoStringParaValorInteger(numeroDiasGarantia);
			date = date.plusDays(quantidadeDias);
			contratoVO.setFimGarantiaConversao(Util.converterDataParaStringSemHora(date.toDate(), Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * Sets the ar endereco formatado no form.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void setarEnderecoFormatadoNoForm(ContratoVO contratoVO) throws GGASException {

		String cep = contratoVO.getEndFisEntFatCEP();
		String numero = contratoVO.getEndFisEntFatNumero();
		if (StringUtils.isNotEmpty(cep)) {
			contratoVO.setEnderecoFormatado(controladorContrato.obterEnderecoFaturamentoFormatado(cep, numero));
		}
	}

	/**
	 * Popular campos CPC info comum.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param contratoVO {@link ContratoVO}
	 * @param model {@link Model}
	 * @return Map {@link MAp}
	 * @throws GGASException {@link GGASException}
	 */
	private Map<String, Object> popularCamposCPCInfoComum(HttpServletRequest request, ContratoVO contratoVO, Model model)
			throws GGASException {
		return popularCamposCPCInfoComum(request, contratoVO, true, model);
	}

	/**
	 * Popular campos cpc info comum.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param contratoVO {@link ContratoVO}
	 * @param popularCamposAbaModalidades {@link PontoConsumoContratoVO}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private Map<String, Object> popularCamposCPCInfoComum(HttpServletRequest request, ContratoVO contratoVO,
			boolean popularCamposAbaModalidades, Model model) throws GGASException {

		Long idPontoConsumo = contratoVO.getIdPontoConsumo();

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		Map<String, Object> dados = new HashMap<String, Object>();
		if (popularCamposAbaModalidades && MapUtils.isNotEmpty(listaPontoConsumoContratoVO)) {

			PontoConsumoContratoVO pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(idPontoConsumo);

			if (pontoConsumoContratoVO != null) {

				dados.putAll(pontoConsumoContratoVO.getAbaPrincipal());
				dados.putAll(pontoConsumoContratoVO.getAbaDadosTecnicos());
				dados.putAll(pontoConsumoContratoVO.getAbaConsumo());

				this.popularCamposAbaFaturamento(contratoVO, request, pontoConsumoContratoVO, dados, model);
				this.popularCamposAbaModalidades(request, pontoConsumoContratoVO, model);

				Collection<ContratoCliente> listaContratoCliente =
						(Collection<ContratoCliente>) pontoConsumoContratoVO.getAbaResponsabilidades().get(LISTA_CONTRATO_CLIENTE);

				if (listaContratoCliente != null) {

					request.getSession().setAttribute(LISTA_CONTRATO_CLIENTE, listaContratoCliente);

					model.addAttribute(LISTA_CONTRATO_CLIENTE, exibirListaResponsabilidades(listaContratoCliente));
				}

				if (!dados.isEmpty()) {
					Entry<?, ?> dado = null;
					for (Iterator<?> it = dados.entrySet().iterator(); it.hasNext();) {
						dado = (Entry<?, ?>) it.next();
						contratoVO.setField((String) dado.getKey(), contratoVO, dado.getValue());
					}
				}

			} else {
				this.limparDadosAbas(contratoVO, request, model);
			}
		}

		return dados;
	}

	/**
	 * Gets the lista ponto consumo contrato VO.
	 *
	 * @param request {@link HttpServletRequest}
	 * @return Map {@link Map}
	 */
	private Map<Long, PontoConsumoContratoVO> getListaPontoConsumoContratoVO(HttpServletRequest request) {
		return (HashMap<Long, PontoConsumoContratoVO>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);
	}

	/**
	 * Popular campos select aba consumo.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param contratoVO {@link ContratoVO}
	 * @param dados {@link Map}
	 * @param model {@link Model}
	 * @param GGASException {@link GGASException}
	 */
	private void popularCamposSelectAbaConsumo(HttpServletRequest request, ContratoVO contratoVO, Map<String, Object> dados, Model model)
			throws GGASException {

		if (!dados.isEmpty()) {
			Entry<?, ?> dado = null;
			for (Iterator<?> it = dados.entrySet().iterator(); it.hasNext();) {
				dado = (Entry<?, ?>) it.next();

				if (contratoVO.contemAtributo((String) dado.getKey())) {
					boolean indTratarCampos = false;

					if (FATOR_UNICO_CORRECAO.equals(dado.getKey())) {
						String valor = (String) dado.getValue();
						contratoVO.setFatorUnicoCorrecao(valor);
						model.addAttribute(FATOR_UNICO_CORRECAO, valor);

					}
					if (LOCAL_AMOSTRAGEM_PCS.equals(dado.getKey())) {
						String valor = (String) dado.getValue();
						String[] chavesLocal = valor.split(Constantes.STRING_VIRGULA);
						contratoVO.setIdsLocalAmostragemAssociados(Util.arrayStringParaArrayLong(chavesLocal));
						indTratarCampos = true;
					}

					if (INTERVALO_RECUPERACAO_PCS.equals(dado.getKey())) {
						String valor = (String) dado.getValue();
						String[] chavesIntervalo = valor.split(Constantes.STRING_VIRGULA);
						contratoVO.setIdsIntervaloAmostragemAssociados(Util.arrayStringParaArrayLong(chavesIntervalo));
						indTratarCampos = true;
					}

					if (indTratarCampos) {
						this.tratarCamposSelectAbaConsumo(contratoVO, model);
					}
				}
			}
		} else {
			preencherValoresPadraoAbaConsumo(contratoVO, request, model);
		}
	}

	/**
	 * Preenche os valores padrao para atributos de contrato da aba de consumo
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void preencherValoresPadraoAbaConsumo(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		if (contratoVO != null) {

			boolean indTratarCampos = false;
			String valor = null;

			valor = contratoVO.getFatorUnicoCorrecao();
			if (StringUtils.isNotEmpty(valor)) {
				model.addAttribute(FATOR_UNICO_CORRECAO, valor);
			}

			valor = contratoVO.getLocalAmostragemPCS();
			if (StringUtils.isNotEmpty(valor)) {
				Long[] idsLocalAmostragemAssociados = Util.arrayStringParaArrayLong(valor.split(Constantes.STRING_VIRGULA));
				contratoVO.setIdsLocalAmostragemAssociados(idsLocalAmostragemAssociados);
				indTratarCampos = true;
			}

			valor = contratoVO.getIntervaloRecuperacaoPCS();
			if (StringUtils.isNotEmpty(valor)) {
				Long[] idsIntervaloAmostragemAssociados = Util.arrayStringParaArrayLong(valor.split(Constantes.STRING_VIRGULA));
				contratoVO.setIdsIntervaloAmostragemAssociados(idsIntervaloAmostragemAssociados);
				indTratarCampos = true;
			}

			if (indTratarCampos) {
				this.tratarCamposSelectAbaConsumo(contratoVO, model);
			}
		}
	}

	/**
	 * Popular campos aba faturamento.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param pontoConsumoContratoVO {@link PontoConsumoContratoVO}
	 * @param dados {@link Map}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void popularCamposAbaFaturamento(ContratoVO contratoVO, HttpServletRequest request,
			PontoConsumoContratoVO pontoConsumoContratoVO, Map<String, Object> dados, Model model) throws NegocioException {

		dados.put(END_FIS_ENT_FAT_CEP, pontoConsumoContratoVO.getAbaFaturamento().get(END_FIS_ENT_FAT_CEP));
		dados.put(END_FIS_ENT_FAT_NUMERO, pontoConsumoContratoVO.getAbaFaturamento().get(END_FIS_ENT_FAT_NUMERO));
		dados.put(END_FIS_ENT_FAT_COMPLEMENTO, pontoConsumoContratoVO.getAbaFaturamento().get(END_FIS_ENT_FAT_COMPLEMENTO));
		dados.put(END_FIS_ENT_FAT_EMAIL, pontoConsumoContratoVO.getAbaFaturamento().get(END_FIS_ENT_FAT_EMAIL));
		dados.put(CHAVE_CEP, pontoConsumoContratoVO.getAbaFaturamento().get(CHAVE_CEP));
		dados.put(CONTRATO_COMPRA, String.valueOf(pontoConsumoContratoVO.getAbaFaturamento().get(CONTRATO_COMPRA)));
		dados.put(FAT_PERIODICIDADE, String.valueOf(pontoConsumoContratoVO.getAbaFaturamento().get(FAT_PERIODICIDADE)));
		dados.put(NOTA_FISCAL_ELETRONICA, String.valueOf(pontoConsumoContratoVO.getAbaFaturamento().get(NOTA_FISCAL_ELETRONICA)));
		dados.put(EMITIR_FATURA_COM_NFE, String.valueOf(pontoConsumoContratoVO.getAbaFaturamento().get(EMITIR_FATURA_COM_NFE)));

		request.getSession().setAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, pontoConsumoContratoVO.getAbaFaturamento().get(ITEM_FATURA));
		request.getSession().setAttribute(ENDERECO_PADRAO, pontoConsumoContratoVO.getAbaFaturamento().get(ENDERECO_PADRAO));

		prepararListaDatasDisponiveisVencimento(request, model);

	}

	/**
	 * Preparar lista datas disponiveis vencimento.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void prepararListaDatasDisponiveisVencimento(HttpServletRequest request, Model model) throws NegocioException {

		Long[] listaDiasDisponiveis = (Long[]) model.asMap().get(LISTA_DIAS_DISPONIVEIS);

		if (listaDiasDisponiveis == null) {
			ConstanteSistema constanteDiasVencimento =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_DIAS_POSSIVEIS_VENCIMENTO);

			List<Long> listaDiasVencimentoPossiveis = controladorEntidadeConteudo.listarDiasVencimento(constanteDiasVencimento.getClasse());

			listaDiasDisponiveis = new Long[listaDiasVencimentoPossiveis.size()];

			for (int i = 0; i < listaDiasVencimentoPossiveis.size(); i++) {
				listaDiasDisponiveis[i] = listaDiasVencimentoPossiveis.get(i);
			}

			model.addAttribute(LISTA_DIAS_DISPONIVEIS, listaDiasDisponiveis);
		}
	}

	/**
	 * Preparar lista dias vencimentos possiveis.
	 *
	 * @param listaDiasDisponiveis {@link Long}
	 * @param listaDiasSelecionados {@link Long}
	 * @param idPontoConsumo {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void prepararListaDiasVencimentosPossiveis(Long[] listaDiasDisponiveis, Long[] listaDiasSelecionados, Long idPontoConsumo,
			HttpServletRequest request, Model model) throws GGASException {

		List<PontoConsumoVencimento> listaPontoConsumoVencimento = new ArrayList<PontoConsumoVencimento>();
		List<Long> listaD = new ArrayList<Long>();
		List<Long> listaS = new ArrayList<Long>();

		for (int i = 0; i < listaDiasSelecionados.length; i++) {
			listaS.add(listaDiasSelecionados[i]);
		}
		for (int i = 0; i < listaDiasDisponiveis.length; i++) {
			listaD.add(listaDiasDisponiveis[i]);
		}

		Collections.sort(listaD);
		Collections.sort(listaS);

		for (int i = 0; i < listaDiasSelecionados.length; i++) {
			listaDiasSelecionados[i] = listaS.get(i);
		}
		for (int i = 0; i < listaDiasDisponiveis.length; i++) {
			listaDiasDisponiveis[i] = listaD.get(i);
		}

		model.addAttribute(LISTA_DIAS_DISPONIVEIS, listaDiasDisponiveis);
		model.addAttribute(LISTA_DIAS_SELECIONADOS, listaDiasSelecionados);

		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(idPontoConsumo);
		PontoConsumoVencimento pontoConsumoVencimento;
		for (int i = 0; i < listaDiasSelecionados.length; i++) {
			pontoConsumoVencimento = new PontoConsumoVencimento();
			pontoConsumoVencimento.setDiaDisponivel(Integer.parseInt(String.valueOf(listaDiasSelecionados[i])));
			pontoConsumoVencimento.setHabilitado(Boolean.TRUE);
			pontoConsumoVencimento.setUltimaAlteracao(Calendar.getInstance().getTime());
			pontoConsumoVencimento.setPontoConsumo(pontoConsumo);
			listaPontoConsumoVencimento.add(pontoConsumoVencimento);
		}
		Map<Long, Object> dadosPontoConsumoVencimento =
				(Map<Long, Object>) request.getSession().getAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);
		if (dadosPontoConsumoVencimento == null) {
			dadosPontoConsumoVencimento = new HashMap<Long, Object>();
		}

		if (dadosPontoConsumoVencimento.containsKey(idPontoConsumo)) {
			dadosPontoConsumoVencimento.remove(idPontoConsumo);
			dadosPontoConsumoVencimento.put(idPontoConsumo, listaPontoConsumoVencimento);
		} else {
			dadosPontoConsumoVencimento.put(idPontoConsumo, listaPontoConsumoVencimento);
		}

		request.getSession().setAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO, dadosPontoConsumoVencimento);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_VENCIMENTO, listaPontoConsumoVencimento);

	}

	/**
	 * Método responsável por adicionar um vencimento a lista de vencimento a ser aplicada a contratos de ponto de consumo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarItemFaturamento")
	public String adicionarItemFaturamento(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();

		Long idPontoConsumo = contratoVO.getIdPontoConsumo();
		Long[] listaDiasDisponiveis = {};
		Long[] listaDiasSelecionados = {};

		if (contratoVO.getListaDiasDisponiveis() != null) {
			listaDiasDisponiveis = contratoVO.getListaDiasDisponiveis();
		}

		if (contratoVO.getListaDiasSelecionados() != null) {
			listaDiasSelecionados = contratoVO.getListaDiasSelecionados();
		}

		model.addAttribute(LISTA_DIAS_DISPONIVEIS, listaDiasDisponiveis);
		model.addAttribute(LISTA_DIAS_SELECIONADOS, listaDiasSelecionados);

		try {

			prepararListaDiasVencimentosPossiveis(listaDiasDisponiveis, listaDiasSelecionados, idPontoConsumo, request, model);

			String itemFatura = contratoVO.getItemFatura();
			String diaVencimentoItemFatura = contratoVO.getDiaVencimentoItemFatura();
			contratoVO.setDiaVencimentoItemFaturaValorFixo(diaVencimentoItemFatura);
			String opcaoVencimentoItemFatura = contratoVO.getOpcaoVencimentoItemFatura();
			String faseVencimentoItemFatura = contratoVO.getFaseVencimentoItemFatura();
			String indicadorVencDiaNaoUtil = contratoVO.getIndicadorVencDiaNaoUtil();

			String tarifaConsumo = contratoVO.getTarifaConsumo();
			String depositoIdentificado = contratoVO.getDepositoIdentificado();
			String dataReferenciaCambial = contratoVO.getDataReferenciaCambial();
			String diaCotacao = contratoVO.getDiaCotacao();
			String percminimoQDC = contratoVO.getPercminimoQDC();

			boolean verificarDataReferenciaDiaCotacao = false;

			Tarifa objetoTarifaConsumo = null;
			EntidadeConteudo objetoDataReferencia = null;
			EntidadeConteudo objetoDiaCotacao = null;

			Long idModeloContrato = contratoVO.getIdModeloContrato();

			Collection<ContratoPontoConsumoItemFaturamentoVO> listaItemFaturamentoContratoVO =
					(Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession()
							.getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			ContratoPontoConsumoItemFaturamentoVO itemFaturamentoContratoVO = new ContratoPontoConsumoItemFaturamentoVO();
			itemFaturamentoContratoVO.setChavePrimariaItemFatura(Long.parseLong(itemFatura));

			Map<String, Object> dadosItemFaturamento = oMapper.convertValue(contratoVO, Map.class);

			EntidadeConteudo opcaoFaseReferencia = null;
			EntidadeConteudo faseReferencia = null;

			if (StringUtils.isNotEmpty(itemFatura) && !NAO_SELECIONADO.equals(itemFatura)) {
				Util.validarDominioCaracteres(LABEL_ITEM_DA_FATURA, itemFatura, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(itemFatura));
				itemFaturamentoContratoVO.setItemFatura(entidadeConteudo);
			}
			if (StringUtils.isNotEmpty(diaVencimentoItemFatura) && !NAO_SELECIONADO.equals(diaVencimentoItemFatura)) {
				Util.validarDominioCaracteres(LABEL_DIA_DO_VENCIMENTO, diaVencimentoItemFatura, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				itemFaturamentoContratoVO.setNumeroDiaVencimento(Integer.valueOf(diaVencimentoItemFatura));
			}
			if (StringUtils.isNotEmpty(opcaoVencimentoItemFatura) && !NAO_SELECIONADO.equals(opcaoVencimentoItemFatura)) {
				Util.validarDominioCaracteres(LABEL_OPCAO_DE_VENCIMENTO, opcaoVencimentoItemFatura, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				opcaoFaseReferencia = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(opcaoVencimentoItemFatura));
				itemFaturamentoContratoVO.setOpcaoFaseReferencia(opcaoFaseReferencia);
			}
			if (StringUtils.isNotEmpty(faseVencimentoItemFatura) && !NAO_SELECIONADO.equals(faseVencimentoItemFatura)) {
				Util.validarDominioCaracteres(LABEL_FASE_DE_VENCIMENTO, faseVencimentoItemFatura, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				faseReferencia = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(faseVencimentoItemFatura));
				itemFaturamentoContratoVO.setFaseReferencia(faseReferencia);
			}
			if (StringUtils.isNotEmpty(indicadorVencDiaNaoUtil)) {
				itemFaturamentoContratoVO.setVencimentoDiaUtil(Boolean.valueOf(indicadorVencDiaNaoUtil));
			}
			if (StringUtils.isNotEmpty(tarifaConsumo) && !NAO_SELECIONADO.equals(tarifaConsumo)) {
				Util.validarDominioCaracteres(LABLE_TARIFA_PARA_PRECO_DO_GAS, tarifaConsumo, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				objetoTarifaConsumo = controladorTarifa.obterTarifa(Long.parseLong(tarifaConsumo));
				itemFaturamentoContratoVO.setTarifa(objetoTarifaConsumo);
			}
			if (StringUtils.isNotEmpty(depositoIdentificado)) {
				itemFaturamentoContratoVO.setIndicadorDepositoIdentificado(Boolean.valueOf(depositoIdentificado));
			}
			if (StringUtils.isNotEmpty(dataReferenciaCambial) && !NAO_SELECIONADO.equals(dataReferenciaCambial)) {
				Util.validarDominioCaracteres(LABEL_DATA_DE_REFERENCIA_CAMBIAL, dataReferenciaCambial,
						Constantes.EXPRESSAO_REGULAR_NUMEROS);
				objetoDataReferencia = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(dataReferenciaCambial));
				itemFaturamentoContratoVO.setDataReferenciaCambial(objetoDataReferencia);
			}
			if (StringUtils.isNotEmpty(diaCotacao) && !NAO_SELECIONADO.equals(diaCotacao)) {
				Util.validarDominioCaracteres(LABEL_DIA_DA_COTACAO, diaCotacao, Constantes.EXPRESSAO_REGULAR_NUMEROS);
				objetoDiaCotacao = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(diaCotacao));
				itemFaturamentoContratoVO.setDiaCotacao(objetoDiaCotacao);
			}

			if (StringUtils.isNotEmpty(percminimoQDC)) {
				BigDecimal perc = Util.converterCampoStringParaValorBigDecimal(PERC_MINIMO_QDC, percminimoQDC,
						Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				itemFaturamentoContratoVO.setPercminimoQDC(perc.multiply(BigDecimal.valueOf(0.01)));
			}

			itemFaturamentoContratoVO.setDadosItemFaturamento(dadosItemFaturamento);

			if (listaItemFaturamentoContratoVO == null) {
				listaItemFaturamentoContratoVO = new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();
			}

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}

			controladorContrato.validarAdicaoContratoPontoConsumoItemFaturamento(modeloContrato, dadosItemFaturamento, faseReferencia,
					opcaoFaseReferencia);

			if (objetoTarifaConsumo != null) {
				verificarDataReferenciaDiaCotacao =
						controladorTarifa.verificarUnidadeMonetariaTarifaVigencia(objetoTarifaConsumo.getChavePrimaria());
			}

			controladorContrato.validarDataReferenciaDiaCotacao(verificarDataReferenciaDiaCotacao, objetoDataReferencia, objetoDiaCotacao);

			if (listaItemFaturamentoContratoVO.contains(itemFaturamentoContratoVO)) {
				listaItemFaturamentoContratoVO.remove(itemFaturamentoContratoVO);
			}

			listaItemFaturamentoContratoVO.add(itemFaturamentoContratoVO);

			request.getSession().setAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, listaItemFaturamentoContratoVO);

			// Atualiza a lista de item faturamento de
			// um contratoPontoConsumo da sessão (caso
			// não exista um novo contratoPontoConsumo
			// é criado).
			Long idPontoConsumoRef = contratoVO.getIdPontoConsumo();
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

			if (idPontoConsumoRef != null && idPontoConsumoRef > 0 && listaPontoConsumoContratoVO != null) {
				aplicarDadosAbaFaturamento(contratoVO, idPontoConsumoRef, listaPontoConsumoContratoVO, request, false);
			}

			limparDadosItemFaturamentoAbaFaturamento(contratoVO);
			if (modeloContrato != null) {
				this.carregarDadosModelo(contratoVO, modeloContrato, model);
			}
			this.agruparFaturamento(contratoVO, request);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Agrupar faturamento.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	protected void agruparFaturamento(ContratoVO contratoVO, HttpServletRequest request) throws GGASException {

		Long idPontoConsumoRef = contratoVO.getIdPontoConsumo();

		Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
		Long chaveAgrupamentoVolume =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME));

		if ((contrato != null) && (contrato.getAgrupamentoCobranca() != null && contrato.getAgrupamentoCobranca())
				&& (idPontoConsumoRef != null) && contrato.getTipoAgrupamento().getChavePrimaria() == chaveAgrupamentoVolume) {

			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

			PontoConsumoContratoVO voRef = listaPontoConsumoContratoVO.get(idPontoConsumoRef);

			for (PontoConsumoContratoVO vo : listaPontoConsumoContratoVO.values()) {

				if ((!vo.equals(voRef)) && (vo.isPreenchimentoConcluido())) {

					Set<String> keysMapFaturamento = voRef.getAbaFaturamento().keySet();
					if (keysMapFaturamento != null) {
						Map<String, Object> newMapFaturamento = new HashMap<String, Object>();
						for (String key : keysMapFaturamento) {

							if (!key.equals(ITEM_FATURA)) {

								newMapFaturamento.put(key, voRef.getAbaFaturamento().get(key));

							} else {

								List<ContratoPontoConsumoItemFaturamentoVO> listVO =
										(List<ContratoPontoConsumoItemFaturamentoVO>) voRef.getAbaFaturamento().get(key);
								List<ContratoPontoConsumoItemFaturamentoVO> newListVO =
										new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();

								if (listVO != null) {
									for (ContratoPontoConsumoItemFaturamentoVO contratoPontoConsumoItemFaturamentoVO : listVO) {
										newListVO.add(contratoPontoConsumoItemFaturamentoVO);
									}
								}
								newMapFaturamento.put(key, newListVO);

							}

						}

						vo.getAbaFaturamento().clear();
						vo.getAbaFaturamento().putAll(newMapFaturamento);

					}

				}

			}

		}

	}

	/**
	 * Método responsável por exibir a alteração de um item de faturamento do contrato. consumo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirAlteracaoItemFaturamentoCadastrado")
	public String exibirAlteracaoItemFaturamentoCadastrado(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Long idItemFaturamento = contratoVO.getIdItemFaturamento();

		Collection<ContratoPontoConsumoItemFaturamentoVO> listaItemFaturamentoContratoVO =
				(Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		Long idPontoConsumo = contratoVO.getIdPontoConsumo();
		contratoVO.setChavesPrimariasPontoConsumo(new Long[] { idPontoConsumo });

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			this.carregarCampos(contratoVO, modeloContrato, request, model);
		}

		if (listaItemFaturamentoContratoVO != null && !listaItemFaturamentoContratoVO.isEmpty() && idItemFaturamento != null) {
			for (ContratoPontoConsumoItemFaturamentoVO itemFaturamentoContratoVO : listaItemFaturamentoContratoVO) {
				if (itemFaturamentoContratoVO.getChavePrimariaItemFatura() == idItemFaturamento) {
					this.popularFormAlteracaoItemFaturamentoCadastrado(contratoVO, itemFaturamentoContratoVO, request, model);
					break;
				}
			}
		}

		this.popularCamposCPCInfoComum(request, contratoVO, model);
		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Popular form alteracao item faturamento cadastrado.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param itemFaturamentoContratoVO {@link ContratoPontosConsumoSegmentosDiferentesException}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void popularFormAlteracaoItemFaturamentoCadastrado(ContratoVO contratoVO,
			ContratoPontoConsumoItemFaturamentoVO itemFaturamentoContratoVO, HttpServletRequest request, Model model) throws GGASException {

		if (itemFaturamentoContratoVO.getItemFatura() != null) {
			contratoVO.setItemFatura(String.valueOf(itemFaturamentoContratoVO.getItemFatura().getChavePrimaria()));

			String codigoItemGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);

			if (itemFaturamentoContratoVO.getItemFatura().getChavePrimaria() == Long.parseLong(codigoItemGas)) {
				Long idPontoConsumo = contratoVO.getIdPontoConsumo();
				List<PontoConsumoVencimento> listaConsumoVencimento =
						(List<PontoConsumoVencimento>) controladorPontoConsumo.listarPontoConsumoVencimento(idPontoConsumo);

				Map<Long, Object> dadosPontoConsumoVencimento =
						(Map<Long, Object>) request.getSession().getAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);
				if (dadosPontoConsumoVencimento != null
						&& (List<PontoConsumoVencimento>) dadosPontoConsumoVencimento.get(idPontoConsumo) != null) {
					listaConsumoVencimento = (List<PontoConsumoVencimento>) dadosPontoConsumoVencimento.get(idPontoConsumo);
				}

				Long[] listaDiasCadastrados = new Long[listaConsumoVencimento.size()];
				for (int i = 0; i < listaDiasCadastrados.length; i++) {
					listaDiasCadastrados[i] = Long.parseLong(String.valueOf(listaConsumoVencimento.get(i).getDiaDisponivel()));
				}
				model.addAttribute(LISTA_DIAS_SELECIONADOS, listaDiasCadastrados);

				ConstanteSistema constanteDiasVencimento =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_DIAS_POSSIVEIS_VENCIMENTO);

				List<Long> listaDiasVencimentoPossiveis =
						controladorEntidadeConteudo.listarDiasVencimento(constanteDiasVencimento.getClasse());

				Long[] listaDiasDisponiveis = new Long[listaDiasVencimentoPossiveis.size()];
				for (int i = 0; i < listaDiasDisponiveis.length; i++) {
					listaDiasDisponiveis[i] = listaDiasVencimentoPossiveis.get(i);
				}

				List<Long> listaD = new ArrayList<Long>();
				List<Long> listaS = new ArrayList<Long>();

				for (int i = 0; i < listaDiasCadastrados.length; i++) {
					listaS.add(listaDiasCadastrados[i]);
				}
				for (int i = 0; i < listaDiasDisponiveis.length; i++) {
					listaD.add(listaDiasDisponiveis[i]);
				}

				listaD.removeAll(listaS);
				Collections.sort(listaD);
				Collections.sort(listaS);

				listaDiasDisponiveis = new Long[listaD.size()];
				for (int i = 0; i < listaD.size(); i++) {
					listaDiasDisponiveis[i] = listaD.get(i);
				}
				model.addAttribute(LISTA_DIAS_DISPONIVEIS, listaDiasDisponiveis);

			}
		}

		contratoVO.setEndFisEntFatCEP((String) itemFaturamentoContratoVO.getDadosItemFaturamento().get(END_FIS_ENT_FAT_CEP));
		contratoVO.setEndFisEntFatNumero((String) itemFaturamentoContratoVO.getDadosItemFaturamento().get(END_FIS_ENT_FAT_NUMERO));
		contratoVO
				.setEndFisEntFatComplemento((String) itemFaturamentoContratoVO.getDadosItemFaturamento().get(END_FIS_ENT_FAT_COMPLEMENTO));
		contratoVO.setEndFisEntFatEmail((String) itemFaturamentoContratoVO.getDadosItemFaturamento().get(END_FIS_ENT_FAT_EMAIL));

		if (itemFaturamentoContratoVO.getContratoCompra() != null) {
			contratoVO.setContratoCompra(String.valueOf(itemFaturamentoContratoVO.getContratoCompra().getChavePrimaria()));
		}

		if (itemFaturamentoContratoVO.getNumeroDiaVencimento() != null) {
			contratoVO.setDiaVencimentoItemFatura(String.valueOf(itemFaturamentoContratoVO.getNumeroDiaVencimento()));
		}

		if (itemFaturamentoContratoVO.getOpcaoFaseReferencia() != null) {
			contratoVO.setOpcaoVencimentoItemFatura(String.valueOf(itemFaturamentoContratoVO.getOpcaoFaseReferencia().getChavePrimaria()));
		}

		if (itemFaturamentoContratoVO.getFaseReferencia() != null) {
			contratoVO.setFaseVencimentoItemFatura(String.valueOf(itemFaturamentoContratoVO.getFaseReferencia().getChavePrimaria()));
		} else {
			contratoVO.setFaseVencimentoItemFatura(StringUtils.EMPTY);
		}

		if (itemFaturamentoContratoVO.getVencimentoDiaUtil() != null) {
			contratoVO.setIndicadorVencDiaNaoUtil(String.valueOf(itemFaturamentoContratoVO.getVencimentoDiaUtil()));
		}

		if (itemFaturamentoContratoVO.getTarifa() != null) {
			contratoVO.setTarifaConsumo(String.valueOf(itemFaturamentoContratoVO.getTarifa().getChavePrimaria()));
		}

		if (itemFaturamentoContratoVO.getIndicadorDepositoIdentificado() != null) {
			contratoVO.setDepositoIdentificado(String.valueOf(itemFaturamentoContratoVO.getIndicadorDepositoIdentificado()));
		}

		if (itemFaturamentoContratoVO.getDiaCotacao() != null) {
			contratoVO.setDiaCotacao(String.valueOf(itemFaturamentoContratoVO.getDiaCotacao().getChavePrimaria()));
		}

		if (itemFaturamentoContratoVO.getDataReferenciaCambial() != null) {
			contratoVO.setDataReferenciaCambial(String.valueOf(itemFaturamentoContratoVO.getDataReferenciaCambial().getChavePrimaria()));
		}

		if (itemFaturamentoContratoVO.getPercminimoQDC() != null) {

			BigDecimal perc = itemFaturamentoContratoVO.getPercminimoQDC().multiply(BigDecimal.valueOf(CEM));

			contratoVO.setPercminimoQDC(Util.converterCampoCurrencyParaString(perc, Constantes.LOCALE_PADRAO));

		} else {

			contratoVO.setPercminimoQDC(StringUtils.EMPTY);
		}

		model.addAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO));

	}

	/**
	 * Método responsável por adicionar um vencimento a lista de vencimento a ser aplicada a contratos de ponto de consumo.
	 * 
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirItemFaturamento")
	public String excluirItemFaturamento(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long[] itensFaturamento = {};

		if (contratoVO.getItensFaturamento() != null) {
			itensFaturamento = contratoVO.getItensFaturamento();
		}

		Collection<ContratoPontoConsumoItemFaturamentoVO> listaItemFaturamentoContratoVO =
				(Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);

		Collection<ContratoPontoConsumoItemFaturamentoVO> itensExcluidos = new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();
		if (listaItemFaturamentoContratoVO != null && !listaItemFaturamentoContratoVO.isEmpty()) {
			for (ContratoPontoConsumoItemFaturamentoVO itemVencimento : listaItemFaturamentoContratoVO) {
				for (Long chaveItemFaturamento : itensFaturamento) {
					if (Long.valueOf(itemVencimento.getChavePrimariaItemFatura()).equals(chaveItemFaturamento)) {
						itensExcluidos.add(itemVencimento);
					}
				}
			}
			listaItemFaturamentoContratoVO.removeAll(itensExcluidos);
		}

		request.getSession().setAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, listaItemFaturamentoContratoVO);

		try {
			tratarCamposSelectAbaConsumo(contratoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		limparDadosItemFaturamentoAbaFaturamento(contratoVO);

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Limpar dados item faturamento aba faturamento.
	 *
	 * @param dynaForm the dyna form
	 */
	private void limparDadosItemFaturamentoAbaFaturamento(ContratoVO contratoVO) {

		contratoVO.setItemFatura(NAO_SELECIONADO);
		contratoVO.setDiaVencimentoItemFatura(StringUtils.EMPTY);
		contratoVO.setOpcaoVencimentoItemFatura(NAO_SELECIONADO);
		contratoVO.setFaseVencimentoItemFatura(NAO_SELECIONADO);
		contratoVO.setIndicadorVencDiaNaoUtil(StringUtils.EMPTY);
		contratoVO.setTarifaConsumo(NAO_SELECIONADO);
		contratoVO.setFormaCobranca(NAO_SELECIONADO);
		contratoVO.setArrecadadorConvenio(NAO_SELECIONADO);
		contratoVO.setDepositoIdentificado(null);
		contratoVO.setIdItemFaturamento(null);
		contratoVO.setDiaCotacao(NAO_SELECIONADO);
		contratoVO.setDataReferenciaCambial(NAO_SELECIONADO);
		contratoVO.setContratoCompra(StringUtils.EMPTY);
	}

	/**
	 * Limpar dados aba responsabilidades.
	 *
	 * @param contratoVO {@link ContratoVO}
	 */
	private void limparDadosAbaResponsabilidades(ContratoVO contratoVO) {

		contratoVO.setIdCliente(null);
		contratoVO.setNomeCliente(StringUtils.EMPTY);
		contratoVO.setDocumentoCliente(StringUtils.EMPTY);
		contratoVO.setEnderecoCliente(StringUtils.EMPTY);
		contratoVO.setEmailCliente(StringUtils.EMPTY);
		contratoVO.setDataInicioRelacao(StringUtils.EMPTY);
		contratoVO.setTipoResponsabilidade(StringUtils.EMPTY);
	}

	/**
	 * Limpar dados abas.
	 *
	 */
	private void limparDadosAbas(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		// Aba Principal
		contratoVO.setPeriodoTesteDataInicio(StringUtils.EMPTY);
		contratoVO.setPeriodoTesteDateFim(StringUtils.EMPTY);
		contratoVO.setPrazoTeste(StringUtils.EMPTY);
		contratoVO.setVolumeTeste(StringUtils.EMPTY);
		contratoVO.setFaxDDD(StringUtils.EMPTY);
		contratoVO.setFaxNumero(StringUtils.EMPTY);
		contratoVO.setEmail(StringUtils.EMPTY);
		contratoVO.setInicioGarantiaConversao(StringUtils.EMPTY);
		contratoVO.setNumeroDiasGarantia(StringUtils.EMPTY);

		// Aba Dados Técnicos
		contratoVO.setVazaoInstantanea(StringUtils.EMPTY);
		contratoVO.setUnidadeVazaoInstan(NAO_SELECIONADO);
		contratoVO.setVazaoInstantaneaMaxima(StringUtils.EMPTY);
		contratoVO.setUnidadeVazaoInstanMaxima(NAO_SELECIONADO);
		contratoVO.setVazaoInstantaneaMinima(StringUtils.EMPTY);
		contratoVO.setUnidadeVazaoInstanMinima(NAO_SELECIONADO);
		contratoVO.setFaixaPressaoFornecimento(NAO_SELECIONADO);
		contratoVO.setPressaoMinimaFornecimento(StringUtils.EMPTY);
		contratoVO.setUnidadePressaoMinimaFornec(NAO_SELECIONADO);
		contratoVO.setPressaoMaximaFornecimento(StringUtils.EMPTY);
		contratoVO.setUnidadePressaoMaximaFornec(NAO_SELECIONADO);
		contratoVO.setPressaoLimiteFornecimento(StringUtils.EMPTY);
		contratoVO.setUnidadePressaoLimiteFornec(NAO_SELECIONADO);
		contratoVO.setNumAnosCtrlParadaCliente(StringUtils.EMPTY);
		contratoVO.setMaxTotalParadasCliente(StringUtils.EMPTY);
		contratoVO.setMaxAnualParadasCliente(StringUtils.EMPTY);
		contratoVO.setNumDiasProgrParadaCliente(StringUtils.EMPTY);
		contratoVO.setNumAnosCtrlParadaCDL(StringUtils.EMPTY);
		contratoVO.setMaxTotalParadasCDL(StringUtils.EMPTY);
		contratoVO.setMaxAnualParadasCDL(StringUtils.EMPTY);
		contratoVO.setNumDiasProgrParadaCDL(StringUtils.EMPTY);
		contratoVO.setNumDiasConsecParadaCliente(StringUtils.EMPTY);
		contratoVO.setNumDiasConsecParadaCDL(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatCEP(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatNumero(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatComplemento(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatEmail(StringUtils.EMPTY);

		// Aba Consumo
		this.limparCamposAbaConsumo(contratoVO, model);

		// Aba Faturamento
		contratoVO.setTarifaConsumo(NAO_SELECIONADO);
		contratoVO.setFatPeriodicidade(NAO_SELECIONADO);
		contratoVO.setFormaCobranca(NAO_SELECIONADO);
		contratoVO.setArrecadadorConvenio(NAO_SELECIONADO);
		contratoVO.setDepositoIdentificado(null);
		contratoVO.setEndFisEntFatCEP(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatNumero(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatComplemento(StringUtils.EMPTY);
		contratoVO.setEndFisEntFatEmail(StringUtils.EMPTY);
		contratoVO.setDiaCotacao(NAO_SELECIONADO);
		contratoVO.setDataReferenciaCambial(NAO_SELECIONADO);
		limparDadosItemFaturamentoAbaFaturamento(contratoVO);
		limparDadosAbaResponsabilidades(contratoVO);

		this.limparCamposAbaModalidade(contratoVO, request);
	}

	/**
	 * Limpar campos aba consumo.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void limparCamposAbaConsumo(ContratoVO contratoVO, Model model) throws GGASException {

		contratoVO.setTamReducaoRecuperacaoPCS(StringUtils.EMPTY);
		contratoVO.setHoraInicialDia(StringUtils.EMPTY);
		contratoVO.setRegimeConsumo(NAO_SELECIONADO);
		contratoVO.setFornecimentoMaximoDiario(StringUtils.EMPTY);
		contratoVO.setUnidadeFornecMaximoDiario(NAO_SELECIONADO);
		contratoVO.setFornecimentoMinimoDiario(StringUtils.EMPTY);
		contratoVO.setUnidadeFornecMinimoDiario(NAO_SELECIONADO);
		contratoVO.setFornecimentoMinimoMensal(StringUtils.EMPTY);
		contratoVO.setUnidadeFornecMinimoMensal(NAO_SELECIONADO);
		contratoVO.setFornecimentoMinimoAnual(StringUtils.EMPTY);
		contratoVO.setUnidadeFornecMinimoAnual(NAO_SELECIONADO);
		contratoVO.setFatorUnicoCorrecao(StringUtils.EMPTY);
		contratoVO.setConsumoFatFalhaMedicao(NAO_SELECIONADO);

		model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, controladorEntidadeConteudo.listarLocalAmostragemPCS());
		model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS, new ArrayList<EntidadeConteudo>());
		model.addAttribute(LISTA_INTERVALO_PCS, controladorHistoricoConsumo.listarIntervaloPCS());
		model.addAttribute(LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS, new ArrayList<IntervaloPCS>());

	}

	/**
	 * Aplicar dados aba principal.
	 *
	 */
	private void aplicarDadosAbaPrincipal(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, HttpServletRequest request, boolean isValidarCamposObrigatorios)
			throws GGASException {

		Map<String, Object> abaPrincipal = new HashMap<String, Object>();

		String periodoTesteDataInicio = contratoVO.getPeriodoTesteDataInicio();
		String periodoTesteDateFim = contratoVO.getPeriodoTesteDateFim();
		String prazoTeste = contratoVO.getPrazoTeste();
		String volumeTeste = contratoVO.getVolumeTeste();
		String faxDDD = contratoVO.getFaxDDD();
		String faxNumero = contratoVO.getFaxNumero();
		String email = contratoVO.getEmail();
		String inicioGarantiaConversao = contratoVO.getInicioGarantiaConversao();
		String numeroDiasGarantia = contratoVO.getNumeroDiasGarantia();
		Long idModeloContrato = contratoVO.getIdModeloContrato();

		if (StringUtils.isNotEmpty(periodoTesteDataInicio)) {
			Util.converterCampoStringParaData(LABEL_DATA_DE_INICIO_DE_PERIODO_DE_TESTES, periodoTesteDataInicio,
					Constantes.FORMATO_DATA_BR);
			abaPrincipal.put(PERIODO_TESTE_DATA_INICIO, periodoTesteDataInicio);
		}
		if (StringUtils.isNotEmpty(periodoTesteDateFim)) {
			Util.converterCampoStringParaData(LABLE_DATA_DE_FIM_DE_PERIODO_DE_TESTES, periodoTesteDateFim, Constantes.FORMATO_DATA_BR);
			abaPrincipal.put(PERIODO_TESTE_DATE_FIM, periodoTesteDateFim);
		}
		if (StringUtils.isNotEmpty(prazoTeste)) {
			Util.validarDominioCaracteres(LABEL_PRAZO_DE_PERIODO_DE_TESTES, prazoTeste, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaPrincipal.put(PRAZO_TESTE, prazoTeste);
		}
		if (StringUtils.isNotEmpty(volumeTeste)) {
			Util.validarDominioCaracteres(LABEL_VOLUME_DE_PERIODO_DE_TESTES, volumeTeste, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaPrincipal.put(VOLUME_TESTE, volumeTeste);
		}
		if (StringUtils.isNotEmpty(faxDDD)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.DDD_CONTRATO_OPERACIONAL, faxDDD, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaPrincipal.put(FAX_DDD, faxDDD);
		}
		if (StringUtils.isNotEmpty(faxNumero)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUMERO_CONTATO_OPERACIONAL, faxNumero, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaPrincipal.put(FAX_NUMERO, faxNumero);
		}
		if (StringUtils.isNotEmpty(email)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.EMAIL_CONTATO_OPERACIONAL, email, Constantes.EXPRESSAO_REGULAR_EMAIL);
			abaPrincipal.put(EMAIL, email);
		}
		if (StringUtils.isNotEmpty(inicioGarantiaConversao)) {
			Util.converterCampoStringParaData(LABEL_DATA_DE_INICIO_DE_GARANTIA_DA_CONVERSAO, inicioGarantiaConversao,
					Constantes.FORMATO_DATA_BR);
			abaPrincipal.put(INICIO_GARANTIA_CONVERSAO, inicioGarantiaConversao);
		}
		if (StringUtils.isNotEmpty(numeroDiasGarantia)) {
			Util.validarDominioCaracteres(LABLE_TEMPO_DE_GARANTIA_DA_CONVERSAO, numeroDiasGarantia, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaPrincipal.put(NUMERO_DIAS_GARANTIA, numeroDiasGarantia);
		}

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.PRINCIPAL);

		if (isValidarCamposObrigatorios) {
			controladorContrato.validarCamposObrigatoriosContrato(abaPrincipal, modeloContrato, aba, false);
		}

		Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);

		if (isValidarCamposObrigatorios) {
			controladorContrato.validarDatasAbaPrincipal(abaPrincipal, contrato);
		}

		PontoConsumoContratoVO pontoConsumoContratoVO;
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
			if (pontoConsumoContratoVO == null) {
				pontoConsumoContratoVO = new PontoConsumoContratoVO();
				pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
			}
			pontoConsumoContratoVO.setAbaPrincipal(abaPrincipal);
			listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
		}
	}

	/**
	 * Aplicar dados aba dados tecnicos.
	 *
	 */
	private void aplicarDadosAbaDadosTecnicos(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, boolean isValidarCamposObrigatorios) throws GGASException {

		String vazaoInstantanea = contratoVO.getVazaoInstantanea();
		String unidadeVazaoInstan = contratoVO.getUnidadeVazaoInstan();
		String vazaoInstantaneaMaxima = contratoVO.getVazaoInstantaneaMaxima();
		String unidadeVazaoInstanMaxima = contratoVO.getUnidadeVazaoInstanMaxima();
		String vazaoInstantaneaMinima = contratoVO.getVazaoInstantaneaMinima();
		String unidadeVazaoInstanMinima = contratoVO.getUnidadeVazaoInstanMinima();
		String faixaPressaoFornecimento = contratoVO.getFaixaPressaoFornecimento();
		String pressaoColetada = contratoVO.getPressaoColetada();
		String unidadePressaoColetada = contratoVO.getUnidadePressaoColetada();
		String pressaoMinimaFornecimento = contratoVO.getPressaoMinimaFornecimento();
		String unidadePressaoMinimaFornec = contratoVO.getUnidadePressaoMinimaFornec();
		String pressaoMaximaFornecimento = contratoVO.getPressaoMaximaFornecimento();
		String unidadePressaoMaximaFornec = contratoVO.getUnidadePressaoMaximaFornec();
		String pressaoLimiteFornecimento = contratoVO.getPressaoLimiteFornecimento();
		String unidadePressaoLimiteFornec = contratoVO.getUnidadePressaoLimiteFornec();
		String numAnosCtrlParadaCliente = contratoVO.getNumAnosCtrlParadaCliente();
		String maxTotalParadasCliente = contratoVO.getMaxTotalParadasCliente();
		String maxAnualParadasCliente = contratoVO.getMaxAnualParadasCliente();
		String numDiasProgrParadaCliente = contratoVO.getNumDiasProgrParadaCliente();
		String numDiasConsecParadaCliente = contratoVO.getNumDiasConsecParadaCliente();
		String numAnosCtrlParadaCDL = contratoVO.getNumAnosCtrlParadaCDL();
		String maxTotalParadasCDL = contratoVO.getMaxTotalParadasCDL();
		String maxAnualParadasCDL = contratoVO.getMaxAnualParadasCDL();
		String numDiasProgrParadaCDL = contratoVO.getNumDiasProgrParadaCDL();
		String numDiasConsecParadaCDL = contratoVO.getNumDiasConsecParadaCDL();
		String unidadeFornecMaximoDiario = contratoVO.getUnidadeFornecMaximoDiario();
		String unidadeFornecMinimoDiario = contratoVO.getUnidadeFornecMinimoDiario();
		Long idModeloContrato = contratoVO.getIdModeloContrato();

		Map<String, Object> abaTecnicos = new HashMap<String, Object>();
		if (StringUtils.isNotEmpty(vazaoInstantanea)) {
			Util.validarDominioCaracteres(LABEL_VAZAO_INSTANTANEA, vazaoInstantanea, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(VAZAO_INSTANTANEA, vazaoInstantanea);
		}
		if (StringUtils.isNotEmpty(unidadeVazaoInstan) && Long.parseLong(unidadeVazaoInstan) > 0) {
			abaTecnicos.put(UNIDADE_VAZAO_INSTAN, unidadeVazaoInstan);
		}
		if (StringUtils.isNotEmpty(vazaoInstantaneaMaxima)) {
			Util.validarDominioCaracteres(LABEL_VAZAO_INSTANTANEA_MAXIMA, vazaoInstantaneaMaxima,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(VAZAO_INSTANTANEA_MAXIMA, vazaoInstantaneaMaxima);
		}
		if (StringUtils.isNotEmpty(unidadeVazaoInstanMaxima) && Long.parseLong(unidadeVazaoInstanMaxima) > 0) {
			abaTecnicos.put(UNIDADE_VAZAO_INSTAN_MAXIMA, unidadeVazaoInstanMaxima);
		}
		if (StringUtils.isNotEmpty(vazaoInstantaneaMinima)) {
			Util.validarDominioCaracteres(LABEL_VAZAO_INSTANTANEA_MINIMA, vazaoInstantaneaMinima,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(VAZAO_INSTANTANEA_MINIMA, vazaoInstantaneaMinima);
		}
		if (StringUtils.isNotEmpty(unidadeVazaoInstanMinima) && Long.parseLong(unidadeVazaoInstanMinima) > 0) {
			abaTecnicos.put(UNIDADE_VAZAO_INSTAN_MINIMA, unidadeVazaoInstanMinima);
		}
		if (StringUtils.isNotEmpty(faixaPressaoFornecimento)) {
			abaTecnicos.put(FAIXA_PRESSAO_FORNECIMENTO, faixaPressaoFornecimento);
			abaTecnicos.put(UNIDADE_PRESSAO_FORNEC, 1);
		}
		if (StringUtils.isNotEmpty(pressaoColetada)) {
			Util.validarDominioCaracteres(LABEL_PRESSAO_COLETADA, pressaoColetada, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(PRESSAO_COLETADA, pressaoColetada);
		}
		if (StringUtils.isNotEmpty(unidadePressaoColetada) && Long.parseLong(unidadePressaoColetada) > 0) {
			abaTecnicos.put(UNIDADE_PRESSAO_COLETADA, unidadePressaoColetada);
		}
		if (StringUtils.isNotEmpty(pressaoMinimaFornecimento)) {
			Util.validarDominioCaracteres(LABEL_PRESSAO_MINIMA_FORNECIMENTO, pressaoMinimaFornecimento,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(PRESSAO_MINIMA_FORNECIMENTO, pressaoMinimaFornecimento);
		}
		if (StringUtils.isNotEmpty(unidadePressaoMinimaFornec) && Long.parseLong(unidadePressaoMinimaFornec) > 0) {
			abaTecnicos.put(UNIDADE_PRESSAO_MINIMA_FORNEC, unidadePressaoMinimaFornec);
		}
		if (StringUtils.isNotEmpty(pressaoMaximaFornecimento)) {
			Util.validarDominioCaracteres(LABEL_PRESSAO_MAXIMA_FORNECIMENTO, pressaoMaximaFornecimento,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(PRESSAO_MAXIMA_FORNECIMENTO, pressaoMaximaFornecimento);
		}
		if (StringUtils.isNotEmpty(unidadePressaoMaximaFornec) && Long.parseLong(unidadePressaoMaximaFornec) > 0) {
			abaTecnicos.put(UNIDADE_PRESSAO_MAXIMA_FORNEC, unidadePressaoMaximaFornec);
		}
		if (StringUtils.isNotEmpty(pressaoLimiteFornecimento)) {
			Util.validarDominioCaracteres(LABEL_PRESSAO_LIMITE_FORNECIMENTO, pressaoLimiteFornecimento,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaTecnicos.put(PRESSAO_LIMITE_FORNECIMENTO, pressaoLimiteFornecimento);
		}
		if (StringUtils.isNotEmpty(unidadePressaoLimiteFornec) && Long.parseLong(unidadePressaoLimiteFornec) > 0) {
			abaTecnicos.put(UNIDADE_PRESSAO_LIMITE_FORNEC, unidadePressaoLimiteFornec);
		}
		if (StringUtils.isNotEmpty(numAnosCtrlParadaCliente)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_ANOS_CTRL_PARADA_CLIENTE, numAnosCtrlParadaCliente,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_ANOS_CTRL_PARADA_CLIENTE, numAnosCtrlParadaCliente);
		}
		if (StringUtils.isNotEmpty(maxTotalParadasCliente)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.MAX_TOTAL_PARADAS_CLIENTE, maxTotalParadasCliente,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(MAX_TOTAL_PARADAS_CLIENTE, maxTotalParadasCliente);
		}
		if (StringUtils.isNotEmpty(maxAnualParadasCliente)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.MAX_ANUAL_PARADAS_CLIENTE, maxAnualParadasCliente,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(MAX_ANUAL_PARADAS_CLIENTE, maxAnualParadasCliente);
		}
		if (StringUtils.isNotEmpty(numDiasProgrParadaCliente)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_DIAS_PROGR_PARADA_CLIENTE, numDiasProgrParadaCliente,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_DIAS_PROGR_PARADA_CLIENTE, numDiasProgrParadaCliente);
		}
		if (StringUtils.isNotEmpty(numDiasConsecParadaCliente)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_DIAS_CONSEC_PARADA_CLIENTE, numDiasConsecParadaCliente,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_DIAS_CONSEC_PARADA_CLIENTE, numDiasConsecParadaCliente);
		}
		if (StringUtils.isNotEmpty(numAnosCtrlParadaCDL)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_ANOS_CTRL_PARADA_CDL, numAnosCtrlParadaCDL,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_ANOS_CTRL_PARADA_CDL, numAnosCtrlParadaCDL);
		}
		if (StringUtils.isNotEmpty(maxTotalParadasCDL)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.MAX_TOTAL_PARADAS_CDL, maxTotalParadasCDL,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(MAX_TOTAL_PARADAS_CDL, maxTotalParadasCDL);
		}
		if (StringUtils.isNotEmpty(maxAnualParadasCDL)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.MAX_ANUAL_PARADAS_CDL, maxAnualParadasCDL,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(MAX_ANUAL_PARADAS_CDL, maxAnualParadasCDL);
		}
		if (StringUtils.isNotEmpty(numDiasProgrParadaCDL)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_DIAS_PROGR_PARADA_CDL, numDiasProgrParadaCDL,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_DIAS_PROGR_PARADA_CDL, numDiasProgrParadaCDL);
		}
		if (StringUtils.isNotEmpty(numDiasConsecParadaCDL)) {
			Util.validarDominioCaracteres(ContratoPontoConsumo.NUM_DIAS_CONSEC_PARADA_CDL, numDiasConsecParadaCDL,
					Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaTecnicos.put(NUM_DIAS_CONSEC_PARADA_CDL, numDiasConsecParadaCDL);
		}
		if (StringUtils.isNotEmpty(unidadeFornecMaximoDiario) && Long.parseLong(unidadeFornecMaximoDiario) > 0) {
			abaTecnicos.put(UNIDADE_FORNEC_MAXIMO_DIARIO, unidadeFornecMaximoDiario);
		}
		if (StringUtils.isNotEmpty(unidadeFornecMinimoDiario) && Long.parseLong(unidadeFornecMinimoDiario) > 0) {
			abaTecnicos.put(UNIDADE_FORNEC_MINIMO_DIARIO, unidadeFornecMinimoDiario);
		}

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.DADOS_TECNICOS);

		if (isValidarCamposObrigatorios) {
			controladorContrato.validarCamposObrigatoriosContrato(abaTecnicos, modeloContrato, aba, false);
			controladorContrato.validarCamposMinimoMaximo(abaTecnicos);
		}

		PontoConsumoContratoVO pontoConsumoContratoVO;

		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
			if (pontoConsumoContratoVO == null) {
				pontoConsumoContratoVO = new PontoConsumoContratoVO();
				pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
			}
			pontoConsumoContratoVO.setAbaDadosTecnicos(abaTecnicos);
			listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
		}
	}

	/**
	 * Aplicar dados aba consumo.
	 *
	 */
	private void aplicarDadosAbaConsumo(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, HttpServletRequest request, boolean isValidarCamposObrigatorios)
			throws GGASException {

		Long[] idsLocalAmostragemAssociados = {};

		if (contratoVO.getIdsLocalAmostragemAssociados() != null) {
			idsLocalAmostragemAssociados = contratoVO.getIdsLocalAmostragemAssociados();
		}
		Long[] idsIntervaloAmostragemAssociados = {};

		if (contratoVO.getIdsIntervaloAmostragemAssociados() != null) {
			idsIntervaloAmostragemAssociados = contratoVO.getIdsIntervaloAmostragemAssociados();
		}

		String tamReducaoRecuperacaoPCS = contratoVO.getTamReducaoRecuperacaoPCS();
		String horaInicialDia = contratoVO.getHoraInicialDia();
		String regimeConsumo = contratoVO.getRegimeConsumo();

		String fornecimentoMaximoDiario = contratoVO.getFornecimentoMaximoDiario();
		String unidadeFornecMaximoDiario = contratoVO.getUnidadeFornecMaximoDiario();

		String fornecimentoMinimoDiario = contratoVO.getFornecimentoMinimoDiario();
		String unidadeFornecMinimoDiario = contratoVO.getUnidadeFornecMinimoDiario();

		String fornecimentoMinimoMensal = contratoVO.getFornecimentoMinimoMensal();
		String unidadeFornecMinimoMensal = contratoVO.getUnidadeFornecMinimoMensal();

		String fornecimentoMinimoAnual = contratoVO.getFornecimentoMinimoAnual();
		String unidadeFornecMinimoAnual = contratoVO.getUnidadeFornecMinimoAnual();

		String fatorUnicoCorrecao = contratoVO.getFatorUnicoCorrecao();
		String consumoFatFalhaMedicao = contratoVO.getConsumoFatFalhaMedicao();

		Long idModeloContrato = contratoVO.getIdModeloContrato();

		controladorContrato.validarCampoPCS(idsIntervaloAmostragemAssociados, tamReducaoRecuperacaoPCS);

		Map<String, Object> abaConsumo = new HashMap<String, Object>();

		if (idsLocalAmostragemAssociados != null && idsLocalAmostragemAssociados.length > 0) {
			abaConsumo.put(LOCAL_AMOSTRAGEM_PCS, Util.arrayParaString(idsLocalAmostragemAssociados, Constantes.STRING_VIRGULA));
		}

		if (idsIntervaloAmostragemAssociados != null && idsIntervaloAmostragemAssociados.length > 0) {
			abaConsumo.put(INTERVALO_RECUPERACAO_PCS, Util.arrayParaString(idsIntervaloAmostragemAssociados, Constantes.STRING_VIRGULA));
		}

		if (StringUtils.isNotEmpty(tamReducaoRecuperacaoPCS)) {
			Util.validarDominioCaracteres(LABEL_NUMERO_MAXIMO_DA_REDUCAO, tamReducaoRecuperacaoPCS, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaConsumo.put(TAM_REDUCAO_RECUPERACAO_PCS, tamReducaoRecuperacaoPCS);
		}

		if (StringUtils.isNotEmpty(tamReducaoRecuperacaoPCS)) {
			Util.validarDominioCaracteres(LABEL_NUMERO_MAXIMO_DA_REDUCAO, tamReducaoRecuperacaoPCS, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaConsumo.put(TAM_REDUCAO_RECUPERACAO_PCS, tamReducaoRecuperacaoPCS);
		}
		if (StringUtils.isNotEmpty(horaInicialDia) && !NAO_SELECIONADO.equals(horaInicialDia)) {
			Util.validarDominioCaracteres(LABEL_HORA_INICIAL_DO_DIA, horaInicialDia, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaConsumo.put(HORA_INICIAL_DIA, horaInicialDia);
		}
		if (StringUtils.isNotEmpty(regimeConsumo) && !regimeConsumo.equals(NAO_SELECIONADO)) {
			Util.validarDominioCaracteres(LABEL_REGIME_DE_CONSUMO, regimeConsumo, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			abaConsumo.put(REGIME_CONSUMO, regimeConsumo);
		}
		if (StringUtils.isNotEmpty(fornecimentoMaximoDiario) && StringUtils.isNotEmpty(unidadeFornecMaximoDiario)
				&& Long.parseLong(unidadeFornecMaximoDiario) > 0) {
			Util.validarDominioCaracteres(LABEL_FORNECIMENTO_MAXIMO_DIARIO, fornecimentoMaximoDiario,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaConsumo.put(FORNECIMENTO_MAXIMO_DIARIO, fornecimentoMaximoDiario);
			abaConsumo.put(UNIDADE_FORNEC_MAXIMO_DIARIO, unidadeFornecMaximoDiario);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoDiario) && StringUtils.isNotEmpty(unidadeFornecMinimoDiario)
				&& Long.parseLong(unidadeFornecMinimoDiario) > 0) {
			Util.validarDominioCaracteres(LABEL_FORNECIMENTO_MINIMO_DIARIO, fornecimentoMinimoDiario,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaConsumo.put(FORNECIMENTO_MINIMO_DIARIO, fornecimentoMinimoDiario);
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_DIARIO, unidadeFornecMinimoDiario);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoMensal) && StringUtils.isNotEmpty(unidadeFornecMinimoMensal)
				&& Long.parseLong(unidadeFornecMinimoMensal) > 0) {
			Util.validarDominioCaracteres(LABEL_FORNECIMENTO_MINIMO_MENSAL, fornecimentoMinimoMensal,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaConsumo.put(FORNECIMENTO_MINIMO_MENSAL, fornecimentoMinimoMensal);
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_MENSAL, unidadeFornecMinimoMensal);
		}
		if (StringUtils.isNotEmpty(fornecimentoMinimoAnual) && StringUtils.isNotEmpty(unidadeFornecMinimoAnual)
				&& Long.parseLong(unidadeFornecMinimoAnual) > 0) {
			Util.validarDominioCaracteres(LABEL_FORNECIMENTO_MINIMO_ANUAL, fornecimentoMinimoAnual,
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			abaConsumo.put(FORNECIMENTO_MINIMO_ANUAL, fornecimentoMinimoAnual);
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_ANUAL, unidadeFornecMinimoAnual);
		}
		if (StringUtils.isNotEmpty(fatorUnicoCorrecao)) {
			abaConsumo.put(FATOR_UNICO_CORRECAO, fatorUnicoCorrecao);
		}
		if (StringUtils.isNotEmpty(consumoFatFalhaMedicao) && !NUMERO_MENOS_UM.equals(consumoFatFalhaMedicao)) {
			abaConsumo.put(CONSUMO_FAT_FALHA_MEDICAO, consumoFatFalhaMedicao);
		}

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.CONSUMO);

		Map<String, Object> dadosAbaConsumoValidacao = new HashMap<String, Object>(abaConsumo);
		if (!controladorContrato.isObrigatorioTamanhoReducaoRecuperacaoPCS(idsIntervaloAmostragemAssociados)) {
			dadosAbaConsumoValidacao.put(TAM_REDUCAO_RECUPERACAO_PCS, NUMERO_UM);
		}

		if (isValidarCamposObrigatorios) {
			controladorContrato.validarCamposObrigatoriosContrato(dadosAbaConsumoValidacao, modeloContrato, aba, false);
			controladorContrato.validarCamposMinimoMaximo(abaConsumo);
		}

		PontoConsumoContratoVO pontoConsumoContratoVO;
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
			if (pontoConsumoContratoVO == null) {
				pontoConsumoContratoVO = new PontoConsumoContratoVO();
				pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
			}
			pontoConsumoContratoVO.setAbaConsumo(abaConsumo);

			listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
		}

	}

	/**
	 * Aplicar dados aba faturamento.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void aplicarDadosAbaFaturamento(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, HttpServletRequest request, boolean isValidarCamposObrigatorios)
			throws GGASException {

		Long constanteGrupamentoPorVolume =
				Long.parseLong(controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME).getValor());

		String endFisEntFatCEP = contratoVO.getEndFisEntFatCEP();
		String endFisEntFatNumero = contratoVO.getEndFisEntFatNumero();
		String endFisEntFatComplemento = contratoVO.getEndFisEntFatComplemento();
		String enderecoPadrao = contratoVO.getEnderecoPadrao();
		String endFisEntFatEmail = contratoVO.getEndFisEntFatEmail();
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String chaveCep = contratoVO.getChaveCep();
		String contratoCompra = contratoVO.getContratoCompra();
		String periodicidade = contratoVO.getFatPeriodicidade();
		String notaFiscalAtiva = contratoVO.getIndicadorNFE();

		String emitirFaturaComNfe = contratoVO.getEmitirFaturaComNfe();

		Collection<ContratoPontoConsumoItemFaturamentoVO> listaVencimentoItemFaturaContratoVO =
				(Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);

		Map<String, Object> abaFaturamento = new HashMap<String, Object>();

		if (StringUtils.isNotEmpty(notaFiscalAtiva)) {
			abaFaturamento.put(NOTA_FISCAL_ELETRONICA, notaFiscalAtiva);
		}
		if (StringUtils.isNotEmpty(emitirFaturaComNfe)) {
			abaFaturamento.put(EMITIR_FATURA_COM_NFE, emitirFaturaComNfe);
		}

		if ((StringUtils.isNotEmpty(endFisEntFatCEP)) && (StringUtils.isNotEmpty(chaveCep))) {
			abaFaturamento.put(END_FIS_ENT_FAT_CEP, endFisEntFatCEP);
			abaFaturamento.put(CHAVE_CEP, chaveCep);
		}

		if (listaVencimentoItemFaturaContratoVO != null && !listaVencimentoItemFaturaContratoVO.isEmpty()) {
			abaFaturamento.put(ITEM_FATURA, listaVencimentoItemFaturaContratoVO);
		}
		abaFaturamento.put(END_FIS_ENT_FAT_NUMERO, endFisEntFatNumero);
		abaFaturamento.put(END_FIS_ENT_FAT_COMPLEMENTO, endFisEntFatComplemento);
		abaFaturamento.put(ENDERECO_PADRAO, enderecoPadrao);
		if (StringUtils.isNotEmpty(endFisEntFatEmail)) {
			String[] emails = endFisEntFatEmail.split(";");
			for (String email : emails) {
				Util.validarDominioCaracteres("Email", email, Constantes.EXPRESSAO_REGULAR_EMAIL);
			}
			abaFaturamento.put(END_FIS_ENT_FAT_EMAIL, endFisEntFatEmail);
		}

		if (StringUtils.isNotEmpty(contratoCompra) && !contratoCompra.equals(NAO_SELECIONADO)) {
			abaFaturamento.put(CONTRATO_COMPRA, contratoCompra);
		}

		if (StringUtils.isNotEmpty(periodicidade) && !periodicidade.equals(NAO_SELECIONADO)) {
			abaFaturamento.put(FAT_PERIODICIDADE, periodicidade);
		}
		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		if (isValidarCamposObrigatorios) {

			controladorContrato
					.validarCamposObrigatoriosContrato(abaFaturamento,
							Arrays.asList(new String[] { ITEM_FATURA, END_FIS_ENT_FAT_CEP, END_FIS_ENT_FAT_NUMERO,
									END_FIS_ENT_FAT_COMPLEMENTO, END_FIS_ENT_FAT_EMAIL, CONTRATO_COMPRA, FAT_PERIODICIDADE, }),
							modeloContrato);
		}

		PontoConsumoContratoVO pontoConsumoContratoVO;
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
			if (pontoConsumoContratoVO == null) {
				pontoConsumoContratoVO = new PontoConsumoContratoVO();
				pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
			}
			Boolean isAgrupamento = Boolean.FALSE;
			Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
			if (contrato != null) {
				isAgrupamento = contrato.getAgrupamentoCobranca();
			}
			if (isAgrupamento != null && isAgrupamento
					&& constanteGrupamentoPorVolume == contrato.getTipoAgrupamento().getChavePrimaria()) {
				pontoConsumoContratoVO.setAbaFaturamento(abaFaturamento);
				listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
				Collection<PontoConsumoContratoVO> listaMapalistaPontoConsumoContratoVO = listaPontoConsumoContratoVO.values();
				for (PontoConsumoContratoVO pontoConsumoContratoVO2 : listaMapalistaPontoConsumoContratoVO) {
					pontoConsumoContratoVO2.setAbaFaturamento(abaFaturamento);
				}
			} else {
				pontoConsumoContratoVO.setAbaFaturamento(abaFaturamento);
				listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
			}
		}

	}

	/**
	 * Popular contrato passo1.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoPasso1(ContratoVO contratoVO, Contrato contrato, HttpServletRequest request, boolean aditivo,
			boolean detalhamento, boolean salvar, boolean isValidarCamposObrigatorios, Model model) throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();

		Long idCliente = contratoVO.getIdCliente();
		Integer anoContrato = contratoVO.getAnoContrato();
		Long chavePrimaria = contratoVO.getChavePrimaria();
		String numeroContrato = contratoVO.getNumeroContrato();
		String numeroAnteriorContrato = contratoVO.getNumeroAnteriorContrato();
		String situacaoContrato = contratoVO.getSituacaoContrato();
		String dataAssinatura = contratoVO.getDataAssinatura();
		String numeroEmpenho = contratoVO.getNumeroEmpenho();
		String vencimentoObrigacoesContratuais = contratoVO.getDataVencObrigacoesContratuais();
		String indicadorAnoContratual = String.valueOf(contratoVO.getIndicadorAnoContratual());
		String renovacaoAutomatica = String.valueOf(contratoVO.getRenovacaoAutomatica());
		String valorContrato = contratoVO.getValorContrato();
		Long[] chavesPrimariasPontoConsumo = contratoVO.getChavesPrimariasPontoConsumo();
		String[] valoresQDCContrato = contratoVO.getQDCContrato();
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String idProposta = contratoVO.getIdProposta();
		String faturamentoAgrupamento = contratoVO.getFaturamentoAgrupamento();
		String tipoPeriodicidadePenalidade = contratoVO.getTipoPeriodicidadePenalidade();
		String emissaoFaturaAgrupada = contratoVO.getEmissaoFaturaAgrupada();
		String numDiasRenoAutoContrato = contratoVO.getNumDiasRenoAutoContrato();
		String numeroAditivo = contratoVO.getNumeroAditivo();
		String descricaoAditivo = contratoVO.getDescricaoAditivo();
		String dataAditivo = contratoVO.getDataAditivo();
		String tempoAntecedenciaRenovacao = contratoVO.getTempoAntecedenciaRenovacao();
		String garantiaFinanceiraRenovada = contratoVO.getGarantiaFinanceiraRenovada();
		String valorParticipacaoCliente = contratoVO.getValorParticipacaoCliente();
		String idFormaCobranca = contratoVO.getFormaCobranca();
		String idArrecadadorConvenio = contratoVO.getArrecadadorConvenio();
		String debitoAutomatico = contratoVO.getDebitoAutomatico();
		String idArrecadadorConvenioDebitoAutomatico = contratoVO.getArrecadadorConvenioDebitoAutomatico();
		String participaECartas = contratoVO.getParticipaECartas();
		String idBanco = contratoVO.getBanco();
		String agencia = contratoVO.getAgencia();
		String contaCorrente = contratoVO.getContaCorrente();
		Long chavePrimariaPai = contratoVO.getChavePrimariaPai();
		String idTipoAgrupamento = contratoVO.getTipoAgrupamento();
		boolean isFluxoAditamento = contratoVO.getFluxoAditamento();
		String indicadorEmissaoAgrupada = contratoVO.getIndicadorEmissaoAgrupada();
		String valorInvestimento = contratoVO.getValorInvestimento();
		String dataInvestimento = contratoVO.getDataInvestimento();
		String multaRecisoria = contratoVO.getMultaRecisoria();
		String dataRecisao = contratoVO.getDataRecisao();
		String descricaoContrato = contratoVO.getDescricaoContrato();
		String volumeReferencia = contratoVO.getVolumeReferencia();
		Long chavePrimariaPrincipal = contratoVO.getChavePrimariaPrincipal();
		String percDuranteRetiradaQPNR = contratoVO.getPercDuranteRetiradaQPNRC();
		String percMinDuranteRetiradaQPNR = contratoVO.getPercMinDuranteRetiradaQPNRC();
		String percFimRetiradaQPNR = contratoVO.getPercFimRetiradaQPNRC();
		String dataInicioRetiradaQPNR = contratoVO.getDataInicioRetiradaQPNRC();
		String dataFimRetiradaQPNR = contratoVO.getDataFimRetiradaQPNRC();
		String tempoValidadeRetiradaQPNR = contratoVO.getTempoValidadeRetiradaQPNRC();
		String percentualTarifaDoP = contratoVO.getPercentualTarifaDoP();
		String percentualSobreTariGas = contratoVO.getPercentualSobreTariGas();
		String prazoVigencia = contratoVO.getPrazoVigencia();
		String incentivosComerciais = contratoVO.getIncentivosComerciais();
		String incentivosInfraestrutura = contratoVO.getIncentivosInfraestrutura();

		Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.DADOS_GERAIS);

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, "atributos.abaAtributo",
					"atributos.abaAtributo.aba", ABAS);
		}

		Map<String, Object> contratoVOmap = oMapper.convertValue(contratoVO, Map.class);

		// Variaveis setadas para a validação
		Map<String, Object> dadosPasso1 = new HashMap<String, Object>(contratoVOmap);

		dadosPasso1.put(QDC_CONTRATO, valoresQDCContrato);

		if (!aditivo) {
			dadosPasso1.put(NUMERO_ADITIVO, String.valueOf(NUMERO_ZERO));
			dadosPasso1.put(DESCRICAO_ADITIVO, CHAR_UPPER_X);
			dadosPasso1.put(DATA_ADITIVO, CHAR_UPPER_X);
		}

		if (idCliente != null && idCliente > 0) {
			dadosPasso1.put(CLIENTE_ASSINATURA, String.valueOf(idCliente));
		}

		dadosPasso1.put(NUMERO_PROPOSTA, contratoVO.getNumeroProposta());

		// Se a validação for chamada do salvar da
		// primeira tela.
		if (salvar) {
			chavesPrimariasPontoConsumo = new Long[] { 1L };
		}

		dadosPasso1.put(PONTO_CONSUMO, chavesPrimariasPontoConsumo);

		if (StringUtils.isNotEmpty(renovacaoAutomatica) && !Boolean.valueOf(renovacaoAutomatica)) {
			dadosPasso1.put(NUM_DIAS_RENO_AUTO_CONTRATO, NUMERO_ZERO);
		} else if (StringUtils.isNotEmpty(renovacaoAutomatica) && Boolean.valueOf(renovacaoAutomatica)) {
			dadosPasso1.put(TEMPO_ANTECEDENCIA_RENOVACAO, NUMERO_ZERO);
		}

		if (StringUtils.isNotEmpty(garantiaFinanceiraRenovada) && !LABEL_RENOVAVEL.equals(garantiaFinanceiraRenovada)) {
			dadosPasso1.put(TEMPO_ANTECEDENCIA_REVISAO_GARANTIAS, NUMERO_ZERO);
			dadosPasso1.put(DATA_INICIO_GARANTIA_FINANCEIRA, NUMERO_ZERO);
			dadosPasso1.put(DATA_FINAL_GARANTIA_FINANCEIRA, NUMERO_ZERO);
		} else if ((StringUtils.isNotEmpty(garantiaFinanceiraRenovada)) && !RENOVADA.equals(garantiaFinanceiraRenovada)) {
			dadosPasso1.put(PERIODICIDADE_REAV_GARANTIAS, NUMERO_ZERO);
		} else {
			contrato.setGarantiaFinanceira(null);
		}

		if (StringUtils.isEmpty(valorParticipacaoCliente)) {
			dadosPasso1.put(QTD_PARCELAS_FINANCIAMENTO, CHAR_UPPER_X);
			dadosPasso1.put(PERCENTUAL_JUROS_FINANCIAMENTO, CHAR_UPPER_X);
			dadosPasso1.put(SISTEMA_AMORTIZACAO, CHAR_UPPER_X);
		}

		if (StringUtils.isNotEmpty(faturamentoAgrupamento) && !Boolean.valueOf(faturamentoAgrupamento) && idTipoAgrupamento != null
				&& idTipoAgrupamento.equals(NAO_SELECIONADO)) {
			dadosPasso1.put(TIPO_AGRUPAMENTO, CHAR_UPPER_X);
		}

		if (StringUtils.isNotEmpty(tipoPeriodicidadePenalidade)) {
			contrato.setTipoPeriodicidadePenalidade(Boolean.valueOf(tipoPeriodicidadePenalidade));
		}

		if (StringUtils.isNotEmpty(faturamentoAgrupamento)) {
			contrato.setAgrupamentoCobranca(Boolean.valueOf(faturamentoAgrupamento));
		}

		if (StringUtils.isNotEmpty(indicadorEmissaoAgrupada)) {
			contrato.setAgrupamentoConta(Boolean.valueOf(indicadorEmissaoAgrupada));
		}

		if (detalhamento) {
			chavesPrimariasPontoConsumo = contratoVO.getChavesPrimariasPontoConsumoVO();
		}

		if (anoContrato != null && anoContrato > 0) {
			contrato.setAnoContrato(anoContrato);
		}

		if (modeloContrato != null) {
			contrato.setModeloContrato(modeloContrato);
			contrato.setPropostaAprovada(this.getValorAtributoPropostaAprovada(modeloContrato));
			dadosPasso1.put(EXIGE_APROVACAO, contrato.isExigeAprovacao());
			contrato.setExigeAprovacao(getValorAtributoExigeAprovacao(modeloContrato));
			dadosPasso1.put(PROPOSTA_APROVADA, contrato.isPropostaAprovada());

		}

		if ((chavePrimaria != null) && (chavePrimaria > 0)) {
			contrato.setChavePrimaria(chavePrimaria);
		}

		if (idCliente != null && idCliente > 0) {
			Cliente clienteAssinatura = controladorCliente.obterCliente(idCliente);
			contrato.setClienteAssinatura(clienteAssinatura);
		} else {
			contrato.setClienteAssinatura(null);
		}

		if (StringUtils.isNotEmpty(numeroContrato)) {
			contrato.setNumero(Util.converterCampoStringParaValorInteger(Contrato.NUMERO_CONTRATO, numeroContrato));
		}

		if (StringUtils.isNotEmpty(numeroAnteriorContrato)) {
			contrato.setNumeroAnterior(numeroAnteriorContrato);
		}

		if (StringUtils.isNotEmpty(situacaoContrato) && !situacaoContrato.equals(NAO_SELECIONADO)) {
			SituacaoContrato situacao = controladorContrato
					.obterSituacaoContrato(Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, situacaoContrato));
			contrato.setSituacao(situacao);
			request.getSession().setAttribute(SITUACAO_ESCOLHIDA, situacaoContrato);
		} else {
			contrato.setSituacao(null);
		}

		if (StringUtils.isNotEmpty(dataAssinatura)) {
			contrato.setDataAssinatura(
					Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, dataAssinatura, Constantes.FORMATO_DATA_BR));
		} else {
			contrato.setDataAssinatura(null);
		}

		if (StringUtils.isNotEmpty(numeroEmpenho)) {
			Util.validarDominioCaracteres(LABEL_NUMERO_DO_EMPENHO, numeroEmpenho, Constantes.EXPRESSAO_REGULAR_NUMEROS);
			contrato.setNumeroEmpenho(numeroEmpenho);
		} else {
			contrato.setNumeroEmpenho(null);
		}

		if (StringUtils.isNotEmpty(vencimentoObrigacoesContratuais)) {
			contrato.setDataVencimentoObrigacoes(Util.converterCampoStringParaData(Contrato.VENCIMENTO_OBG_CONTRATUAIS,
					vencimentoObrigacoesContratuais, Constantes.FORMATO_DATA_BR));
		} else {
			contrato.setDataVencimentoObrigacoes(null);
		}

		if (StringUtils.isNotEmpty(indicadorAnoContratual)) {
			contrato.setIndicadorAnoContratual(Boolean.valueOf(indicadorAnoContratual));
		}

		if (StringUtils.isNotEmpty(renovacaoAutomatica)) {
			contrato.setRenovacaoAutomatica(Boolean.valueOf(renovacaoAutomatica));
			if (contrato.getRenovacaoAutomatica() && StringUtils.isNotEmpty(numDiasRenoAutoContrato)) {
				contrato.setNumeroDiasRenovacaoAutomatica(
						Util.converterCampoStringParaValorInteger(Contrato.NUMERO_DIAS_RENOVACAO_AUTOMATICA, numDiasRenoAutoContrato));
			} else if (!contrato.getRenovacaoAutomatica() && StringUtils.isNotEmpty(tempoAntecedenciaRenovacao)) {
				contrato.setDiasAntecedenciaRenovacao(
						Util.converterCampoStringParaValorInteger(Contrato.DIAS_ANTECEDENCIA_RENOVACAO, tempoAntecedenciaRenovacao));
			}
		}

		if (StringUtils.isNotEmpty(valorContrato)) {
			contrato.setValorContrato(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_CONTRATO, valorContrato,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setValorContrato(null);
		}

		if (StringUtils.isNotEmpty(emissaoFaturaAgrupada)) {
			contrato.setAgrupamentoConta(Boolean.valueOf(emissaoFaturaAgrupada));
		}

		if (StringUtils.isNotEmpty(numeroAditivo)) {
			contrato.setNumeroAditivo(Util.converterCampoStringParaValorInteger(Contrato.NUMERO_ADITIVO, numeroAditivo));
		}

		if (StringUtils.isNotEmpty(dataAditivo)) {
			contrato.setDataAditivo(Util.converterCampoStringParaData(Contrato.DATA_ADITIVO, dataAditivo, Constantes.FORMATO_DATA_BR));
		} else {
			contrato.setDataAditivo(null);
		}

		if (StringUtils.isNotEmpty(descricaoAditivo)) {
			contrato.setDescricaoAditivo(descricaoAditivo);
		} else {
			contrato.setDescricaoAditivo(null);
		}

		if (StringUtils.isNotEmpty(idFormaCobranca) && !idFormaCobranca.equals(NAO_SELECIONADO)) {
			EntidadeConteudo formaCobranca = controladorEntidadeConteudo
					.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, idFormaCobranca));
			contrato.setFormaCobranca(formaCobranca);
		} else {
			contrato.setFormaCobranca(null);
		}

		if (StringUtils.isNotEmpty(idArrecadadorConvenio) && !idArrecadadorConvenio.equals(NAO_SELECIONADO)) {
			ArrecadadorContratoConvenio arrecadadorConvenio = controladorArrecadadorConvenio.obterArrecadadorConvenio(
					Util.converterCampoStringParaValorLong(LABEL_ARRECADADOR_CONTRATO_CONVENIO, idArrecadadorConvenio));
			contrato.setArrecadadorContratoConvenio(arrecadadorConvenio);
		} else {
			contrato.setArrecadadorContratoConvenio(null);
		}

		if (StringUtils.isNotEmpty(debitoAutomatico)) {
			contrato.setIndicadorDebitoAutomatico(Boolean.valueOf(debitoAutomatico));
		}

		if (StringUtils.isNotEmpty(participaECartas)) {
			contrato.setIndicadorParticipanteECartas(Boolean.valueOf(participaECartas));
		}

		if (StringUtils.isNotEmpty(idArrecadadorConvenioDebitoAutomatico)
				&& !idArrecadadorConvenioDebitoAutomatico.equals(NAO_SELECIONADO)) {
			ArrecadadorContratoConvenio arrecadadorConvenioDebitoAutomatico =
					controladorArrecadadorConvenio.obterArrecadadorConvenio(Util.converterCampoStringParaValorLong(
							LABEL_ARRECADADOR_CONTRATO_CONVENIO_DEBITO_AUTOMATICO, idArrecadadorConvenioDebitoAutomatico));
			contrato.setArrecadadorContratoConvenioDebitoAutomatico(arrecadadorConvenioDebitoAutomatico);
		} else {
			contrato.setArrecadadorContratoConvenioDebitoAutomatico(null);
		}

		if (StringUtils.isNotEmpty(debitoAutomatico) && !Boolean.valueOf(debitoAutomatico) && idBanco != null
				&& idArrecadadorConvenioDebitoAutomatico.equals(NAO_SELECIONADO)) {
			dadosPasso1.put(ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO, CHAR_UPPER_X);
		}

		if (StringUtils.isNotEmpty(debitoAutomatico) && !Boolean.valueOf(debitoAutomatico) && idBanco != null
				&& idBanco.equals(NAO_SELECIONADO)) {
			dadosPasso1.put(BANCO, CHAR_UPPER_X);
			dadosPasso1.put(AGENCIA, CHAR_UPPER_X);
			dadosPasso1.put(CONTA_CORRENTE, CHAR_UPPER_X);
		}

		final boolean debitoSelecionado = StringUtils.isNotEmpty(debitoAutomatico) && Boolean.valueOf(debitoAutomatico);
		if (debitoSelecionado && StringUtils.isNotEmpty(idBanco) && !idBanco.equals(NAO_SELECIONADO)) {
			Banco banco = controladorCobranca.obterBanco(Util.converterCampoStringParaValorLong("Banco", idBanco));
			contrato.setBanco(banco);
		} else {
			contrato.setBanco(null);
		}

		if (StringUtils.isEmpty(agencia)) {
			contrato.setAgencia(null);
		} else {
			contrato.setAgencia(agencia);
		}
		if (StringUtils.isEmpty(contaCorrente)) {
			contrato.setContaCorrente(null);
		} else {
			contrato.setContaCorrente(contaCorrente);
		}

		if ((chavePrimaria != null) && (chavePrimaria == 0)) {

			contrato.setChavePrimariaPai(null);

		} else if (chavePrimariaPai != null && chavePrimariaPai != 0) {

			contrato.setChavePrimariaPai(chavePrimariaPai);

		}

		if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {
			contrato.setChavePrimariaPrincipal(chavePrimariaPrincipal);
		} else {
			contrato.setChavePrimariaPrincipal(null);
		}

		if (StringUtils.isNotEmpty(faturamentoAgrupamento) && StringUtils.isNotEmpty(idTipoAgrupamento)
				&& !idTipoAgrupamento.equals(NAO_SELECIONADO) && Boolean.valueOf(faturamentoAgrupamento)) {
			EntidadeConteudo tipoAgrupamento = controladorEntidadeConteudo
					.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(Contrato.TIPO_AGRUPAMENTO, idTipoAgrupamento));
			contrato.setTipoAgrupamento(tipoAgrupamento);
		}

		if (StringUtils.isNotEmpty(valorInvestimento)) {
			contrato.setValorInvestimento(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_INVESTIMENTO, valorInvestimento,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setValorInvestimento(null);
		}

		if (StringUtils.isNotEmpty(dataInvestimento)) {
			contrato.setDataInvestimento(
					Util.converterCampoStringParaData(Contrato.DATA_INVESTIMENTO, dataInvestimento, Constantes.FORMATO_DATA_BR));
		} else {
			contrato.setDataInvestimento(null);
		}

		if (StringUtils.isNotEmpty(multaRecisoria) && !multaRecisoria.equals(NAO_SELECIONADO)) {
			contrato.setMultaRecisoria(controladorEntidadeConteudo
					.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(Contrato.MULTA_RECISORIA, multaRecisoria)));
		}

		if (StringUtils.isNotEmpty(descricaoContrato)) {
			contrato.setDescricaoContrato(descricaoContrato);
		}

		if (StringUtils.isNotEmpty(volumeReferencia)) {
			contrato.setVolumeReferencia(Util.converterCampoStringParaValorBigDecimal(Contrato.VOLUME_REFERENCIA, volumeReferencia,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isNotEmpty(dataRecisao)) {
			contrato.setDataRecisao(Util.converterCampoStringParaData(Contrato.DATA_RECISAO, dataRecisao, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(percDuranteRetiradaQPNR)) {
			contrato.setPercentualQDCContratoQPNR(Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_MAXIMO_RETIRADA_QPNR,
					percDuranteRetiradaQPNR, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		}

		if (StringUtils.isNotEmpty(percMinDuranteRetiradaQPNR)) {
			contrato.setPercentualMinimoQDCContratoQPNR(Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_MINIMO_RETIRADA_QPNR,
					percMinDuranteRetiradaQPNR, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		}

		if (StringUtils.isNotEmpty(percFimRetiradaQPNR)) {
			contrato.setPercentualQDCFimContratoQPNR(Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_FIM_RETIRADA_QPNR,
					percFimRetiradaQPNR, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		}

		if (StringUtils.isNotEmpty(dataInicioRetiradaQPNR)) {
			contrato.setDataInicioRetiradaQPNR(Util.converterCampoStringParaData(ContratoPontoConsumo.DATA_INICIO_RECUPERACAO,
					dataInicioRetiradaQPNR, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataFimRetiradaQPNR)) {
			contrato.setDataFimRetiradaQPNR(Util.converterCampoStringParaData(ContratoPontoConsumo.DATA_MAXIMA_RECUPERACAO,
					dataFimRetiradaQPNR, Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(tempoValidadeRetiradaQPNR)) {
			contrato.setAnosValidadeRetiradaQPNR(Util.converterCampoStringParaValorInteger(
					ContratoPontoConsumoModalidade.VALIDADE_RETIRADA_QPNR, tempoValidadeRetiradaQPNR));
		}
		if (StringUtils.isNotEmpty(percentualTarifaDoP)) {
			contrato.setPercentualTarifaDoP(Util.converterCampoStringParaValorBigDecimal(PERCENTUAL_TARIFA_DOP, percentualTarifaDoP,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		}
		if (StringUtils.isNotEmpty(percentualSobreTariGas)) {
			contrato.setPercentualSobreTariGas(Util.converterCampoStringParaValorBigDecimal(PERCENTUAL_SOBRE_TARI_GAS,
					percentualSobreTariGas, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		}
		
		if (StringUtils.isNotEmpty(prazoVigencia)) {
			contrato.setPrazoVigencia(Integer.valueOf(prazoVigencia));
		}
		
		if (StringUtils.isNotEmpty(incentivosComerciais)) {
			contrato.setIncentivosComerciais(Util.converterCampoStringParaValorBigDecimal("Incentivos Comerciais",
					incentivosComerciais, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}	
		
		if (StringUtils.isNotEmpty(incentivosInfraestrutura)) {
			contrato.setIncentivosInfraestrutura(Util.converterCampoStringParaValorBigDecimal("Incentivos Infraestrutura",
					incentivosInfraestrutura, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		this.setarMultaNoContrato(contratoVO, contrato);
		this.setarGarantiaNoContrato(contratoVO, contrato);
		this.setarPropostaNoContrato(contratoVO, contrato, idProposta);
		this.setarPontosConsumoSelecionadosNoRequest(request, chavesPrimariasPontoConsumo, model);
		this.setarQDCNoContrato(contrato, valoresQDCContrato);
		if (request.getSession().getAttribute(LISTA_ANEXOS) != null) {
			Collection<ContratoAnexo> listaAnexos = (Collection<ContratoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
			if (!Util.isNullOrEmpty(listaAnexos)) {
				dadosPasso1.put("documentosAnexos", true);
				this.setarAnexos(listaAnexos, contrato);
			}
		}

		String[] camposPenalidadePorRetirada = new String[] { NUMERO_CONTRATO, PRESSAO_COLETADA, PERCENTUAL_JUROS_MORA, PERCENTUAL_MULTA,
				PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE, PENALIDADE_RETIRADA_MAIOR_MENOR, PERIODICIDADE_RETIRADA_MAIOR_MENOR,
				BASE_APURACAO_RETIRADA_MAIOR_MENOR, DATA_INICIO_RETIRADA_MAIOR_MENOR, DATA_FIM_RETIRADA_MAIOR_MENOR,
				PRECO_COBRANCA_RETIRADA_MAIOR_MENOR, FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR, PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR,
				PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR, CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR,
				PERCENTUAL_RETIRADA_MAIOR_MENOR, PERIODICIDADE_TAKE_OR_PAY_C, REFERENCIA_QF_PARADA_PROGRAMADA_C,
				CONSUMO_REFERENCIA_TAKE_OR_PAY_C, MARGEM_VARIACAO_TAKE_OR_PAY_C, DATA_INICIO_VIGENCIA_C, DATA_FIM_VIGENCIA_C,
				PRECO_COBRANCA_C, TIPO_APURACAO_C, CONSIDERA_PARADA_PROGRAMADA_C, CONSIDERA_FALHA_FORNECIMENTO_C, CONSIDERA_CASO_FORTUITO_C,
				RECUPERAVEL_C, PERCENTUAL_NAO_RECUPERAVEL_C, APURACAO_PARADA_PROGRAMADA_C, APURACAO_PARADA_PROGRAMADA_C,
				APURACAO_CASO_FORTUITO_C, APURACAO_FALHA_FORNECIMENTO_C };

		if (modeloContrato != null && camposPenalidadePorRetirada != null) {
			removerObrigatoriedadeAtributoModeloContrato(modeloContrato, camposPenalidadePorRetirada);
		}

		popularContratoPenalidadesETOP(contratoVO, request, contrato);

		if (!detalhamento) {
			if (isValidarCamposObrigatorios) {
				boolean validarCamposAditar = false;
				if (isFluxoAditamento) {
					validarCamposAditar = true;
				}
				controladorContrato.validarCamposObrigatoriosContrato(dadosPasso1, modeloContrato, aba, validarCamposAditar);
			}
			controladorContrato.validarDataQdcContrato(dadosPasso1, contrato);
			controladorContrato.validarJurosMoraContrato(contrato);
		}

	}

	/**
	 * Setar anexos no contrato
	 *
	 * @param anexos
	 */
	private void setarAnexos(Collection<ContratoAnexo> anexos, Contrato contrato) {
		contrato.setAnexos(new HashSet<ContratoAnexo>(anexos));
		for (ContratoAnexo anexo : anexos) {
			anexo.setContrato(contrato);
		}
	}

	/**
	 * @param modeloContrato
	 * @return indicador de proposta aprovada
	 */
	private boolean getValorAtributoPropostaAprovada(ModeloContrato modeloContrato) {

		Collection<ModeloAtributo> atributos = modeloContrato.getAtributos();

		for (ModeloAtributo atributo : atributos) {
			AbaAtributo abaAtributo = atributo.getAbaAtributo();
			if ("propostaAprovada".equals(abaAtributo.getNomeColuna())) {
				return Boolean.valueOf(atributo.getPadrao());
			}
		}

		return true;
	}

	/**
	 * @param modeloContrato
	 * @return indicador de proposta aprovada
	 */
	private boolean getValorAtributoExigeAprovacao(ModeloContrato modeloContrato) {

		Collection<ModeloAtributo> atributos = modeloContrato.getAtributos();

		for (ModeloAtributo atributo : atributos) {
			AbaAtributo abaAtributo = atributo.getAbaAtributo();
			if (EXIGE_APROVACAO.equals(abaAtributo.getNomeColuna())) {
				return Boolean.valueOf(atributo.getPadrao());
			}
		}

		return false;
	}

	/**
	 * Remover obrigatoriedade atributo modelo contrato.
	 *
	 * @param modeloContrato the modelo contrato
	 * @param nomesAtributos the nomes atributos
	 */
	private void removerObrigatoriedadeAtributoModeloContrato(ModeloContrato modeloContrato, String... nomesAtributos) {

		for (ModeloAtributo atributo : modeloContrato.getAtributos()) {
			for (String nomeAtributo : nomesAtributos) {
				if (atributo.getAbaAtributo().getNomeColuna().equals(nomeAtributo)) {
					atributo.getAbaAtributo().setDependenciaObrigatoria(false);
					atributo.setObrigatorio(false);
				}
			}
		}
	}

	/**
	 * Setar garantia no contrato.
	 *
	 */
	private void setarGarantiaNoContrato(ContratoVO contratoVO, Contrato contrato) throws GGASException {

		String tipoGarantiaFinanceira = contratoVO.getTipoGarantiaFinanceira();
		String descGarantiaFinanc = contratoVO.getDescGarantiaFinanc();
		String valorGarantia = contratoVO.getValorGarantiaFinanceira();
		String dataInicioGarantiaFinanceira = contratoVO.getDataInicioGarantiaFinanceira();
		String dataFinalGarantiaFinanceira = contratoVO.getDataFinalGarantiaFinanceira();
		String garantiaFinanceiraRenovada = contratoVO.getGarantiaFinanceiraRenovada();
		String tempoAntecRevisaoGarantias = contratoVO.getTempoAntecRevisaoGarantias();
		String periodicidadeReavaliacaoGarantias = contratoVO.getPeriodicidadeReavGarantias();

		if (StringUtils.isNotEmpty(tipoGarantiaFinanceira) && !tipoGarantiaFinanceira.equals(NAO_SELECIONADO)) {
			EntidadeConteudo garantiaFinanceira = controladorEntidadeConteudo.obterEntidadeConteudo(
					Util.converterCampoStringParaValorLong(Contrato.TIPO_GARANTIA_FINANCEIRA, tipoGarantiaFinanceira));
			contrato.setGarantiaFinanceira(garantiaFinanceira);
		} else {
			contrato.setGarantiaFinanceira(null);
		}
		if (StringUtils.isNotEmpty(descGarantiaFinanc)) {
			contrato.setDescricaoGarantiaFinanceira(descGarantiaFinanc);
		} else {
			contrato.setDescricaoGarantiaFinanceira(null);
		}
		if (StringUtils.isNotEmpty(valorGarantia)) {
			contrato.setValorGarantiaFinanceira(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_GARANTIA, valorGarantia,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setValorGarantiaFinanceira(null);
		}

		if (!NUMERO_MENOS_UM.equals(garantiaFinanceiraRenovada) && StringUtils.isNotEmpty(garantiaFinanceiraRenovada)) {
			if (LABEL_RENOVAVEL.equals(garantiaFinanceiraRenovada)) {
				contrato.setRenovacaoGarantiaFinanceira(Boolean.TRUE);
			} else if (RENOVADA.equals(garantiaFinanceiraRenovada)) {
				contrato.setRenovacaoGarantiaFinanceira(Boolean.FALSE);
			}

			if (contrato.getRenovacaoGarantiaFinanceira()) {
				if (StringUtils.isNotEmpty(tempoAntecRevisaoGarantias)) {
					contrato.setDiasAntecedenciaRevisaoGarantiaFinanceira(Util
							.converterCampoStringParaValorInteger(Contrato.DIAS_ANTECEDENCIA_REVISAO_GARANTIA, tempoAntecRevisaoGarantias));
				} else {
					contrato.setDiasAntecedenciaRevisaoGarantiaFinanceira(null);
				}
				if (StringUtils.isNotEmpty(dataInicioGarantiaFinanceira)) {
					contrato.setDataInicioGarantiaFinanciamento(Util.converterCampoStringParaData(Contrato.DATA_INICIO_GARANTIA_FINANCEIRA,
							dataInicioGarantiaFinanceira, Constantes.FORMATO_DATA_BR));
				} else {
					contrato.setDataInicioGarantiaFinanciamento(null);
				}
				if (StringUtils.isNotEmpty(dataFinalGarantiaFinanceira)) {
					contrato.setDataFimGarantiaFinanciamento(Util.converterCampoStringParaData(Contrato.DATA_FINAL_GARANTIA_FINANCEIRA,
							dataFinalGarantiaFinanceira, Constantes.FORMATO_DATA_BR));
				} else {
					contrato.setDataFimGarantiaFinanciamento(null);
				}
			} else {
				if (StringUtils.isNotEmpty(periodicidadeReavaliacaoGarantias)) {
					contrato.setPeriodicidadeReavaliacaoGarantias(Util.converterCampoStringParaValorInteger(
							Contrato.PERIODICIDADE_REAVALIACAO_GARANTIAS, periodicidadeReavaliacaoGarantias));
				} else {
					contrato.setPeriodicidadeReavaliacaoGarantias(null);
				}
			}
		}
	}

	/**
	 * Setar multa no contrato.
	 *
	 */
	private void setarMultaNoContrato(ContratoVO contratoVO, Contrato contrato) throws GGASException {

		String indicadorMultaAtraso = contratoVO.getIndicadorMultaAtraso();
		String indicadorJurosMora = contratoVO.getIndicadorJurosMora();
		String indiceCorrecaoMonetaria = contratoVO.getIndiceCorrecaoMonetaria();
		String percentualMulta = contratoVO.getPercentualMulta();
		String percentualJurosMora = contratoVO.getPercentualJurosMora();

		if (StringUtils.isNotEmpty(indicadorMultaAtraso)) {
			contrato.setIndicadorMulta(Boolean.valueOf(indicadorMultaAtraso));
		}
		if (StringUtils.isNotEmpty(indicadorJurosMora)) {
			contrato.setIndicadorJuros(Boolean.valueOf(indicadorJurosMora));
		}
		if (StringUtils.isNotEmpty(indiceCorrecaoMonetaria) && !indiceCorrecaoMonetaria.equals(NAO_SELECIONADO)) {
			IndiceFinanceiro indiceFinanceiro = controladorTarifa
					.obterIndiceFinanceiro(Util.converterCampoStringParaValorLong(Contrato.CORRECAO_MONETARIA, indiceCorrecaoMonetaria));
			contrato.setIndiceFinanceiro(indiceFinanceiro);
		} else {
			contrato.setIndiceFinanceiro(null);
		}
		if (StringUtils.isNotEmpty(percentualMulta)) {
			BigDecimal valorPercentualMulta = Util.converterCampoStringParaValorBigDecimal(StringUtils.EMPTY, percentualMulta,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			contrato.setPercentualMulta(valorPercentualMulta);
		}
		if (StringUtils.isNotEmpty(percentualJurosMora)) {
			BigDecimal valorPercentualJurosMora = Util.converterCampoStringParaValorBigDecimal(StringUtils.EMPTY, percentualJurosMora,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			contrato.setPercentualJurosMora(valorPercentualJurosMora);
		}
	}

	/**
	 * Setar qdc no contrato.
	 *
	 */
	private void setarQDCNoContrato(Contrato contrato, String[] valoresQDCContrato) throws GGASException {

		if (!contrato.getListaContratoQDC().isEmpty()) {
			contrato.getListaContratoQDC().clear();
		}

		if ((valoresQDCContrato != null) && (valoresQDCContrato.length > 0)) {

			for (int i = 0; i < valoresQDCContrato.length; i++) {

				String valorQDCContrato = valoresQDCContrato[i];
				String dataQDC = valorQDCContrato.split(Constantes.STRING_ESPACO)[0];
				String valorQDC = valorQDCContrato.split(Constantes.STRING_ESPACO)[2];

				ContratoQDC contratoQDC = (ContratoQDC) controladorContrato.criarContratoQDC();
				contratoQDC.setData(Util.converterCampoStringParaData(Contrato.DATA, dataQDC, Constantes.FORMATO_DATA_BR));
				contratoQDC.setMedidaVolume(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_QDC, valorQDC,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
				contratoQDC.setHabilitado(true);
				contratoQDC.setUltimaAlteracao(Calendar.getInstance().getTime());

				contrato.getListaContratoQDC().add(contratoQDC);
			}

		}

	}

	/**
	 * Setar proposta no contrato.
	 *
	 */
	private void setarPropostaNoContrato(ContratoVO contratoVO, Contrato contrato, String idProposta) throws GGASException {

		String gastoEstimadoGNMes = contratoVO.getGastoEstimadoGNMes();
		String economiaEstimadaGNMes = contratoVO.getEconomiaEstimadaGNMes();
		String economiaEstimadaGNAno = contratoVO.getEconomiaEstimadaGNAno();
		String descontoEfetivoEconomia = contratoVO.getDescontoEfetivoEconomia();
		String diaVencFinanciamento = contratoVO.getDiaVencFinanciamento();
		String valorParticipacaoCliente = contratoVO.getValorParticipacaoCliente();
		String qtdParcelasFinanciamento = contratoVO.getQtdParcelasFinanciamento();
		String percentualJurosFinanciamento = contratoVO.getPercentualJurosFinanciamento();
		String sistemaAmortizacao = contratoVO.getSistemaAmortizacao();

		if (StringUtils.isNotEmpty(idProposta)) {
			Proposta proposta = (Proposta) controladorProposta.obter(Long.parseLong(idProposta));
			contrato.setProposta(proposta);
		} else {
			contrato.setProposta(null);
		}
		if (StringUtils.isNotEmpty(gastoEstimadoGNMes)) {
			contrato.setValorGastoMensal(Util.converterCampoStringParaValorBigDecimal(LABEL_GASTO_ESTIMADO_GN_MES, gastoEstimadoGNMes,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setValorGastoMensal(null);
		}
		if (StringUtils.isNotEmpty(economiaEstimadaGNMes)) {
			contrato.setMedidaEconomiaMensalGN(Util.converterCampoStringParaValorBigDecimal(LABEL_ECONOMIA_ESTIMADA_GN_MES,
					economiaEstimadaGNMes, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setMedidaEconomiaMensalGN(null);
		}
		if (StringUtils.isNotEmpty(economiaEstimadaGNAno)) {
			contrato.setMedidaEconomiaAnualGN(Util.converterCampoStringParaValorBigDecimal(LOCAL_ECONOMIA_ESTIMADA_GN_ANO,
					economiaEstimadaGNAno, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setMedidaEconomiaAnualGN(null);
		}
		if (StringUtils.isNotEmpty(descontoEfetivoEconomia)) {
			contrato.setPercentualEconomiaGN(Util.converterCampoStringParaValorBigDecimal(LABEL_DESCONTO_EFETIVO_ECONOMIA,
					descontoEfetivoEconomia, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		} else {
			contrato.setPercentualEconomiaGN(null);
		}
		if (StringUtils.isNotEmpty(diaVencFinanciamento)) {
			contrato.setNumeroDiaVencimentoFinanciamento(
					Util.converterCampoStringParaValorInteger(Contrato.DIA_VENCIMENTO_FINANCIAMENTO, diaVencFinanciamento));
		} else {
			contrato.setNumeroDiaVencimentoFinanciamento(null);
		}
		if (StringUtils.isNotEmpty(valorParticipacaoCliente)) {
			contrato.setValorParticipacaoCliente(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_PARTICIPACAO_CLIENTE,
					valorParticipacaoCliente, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			contrato.setValorParticipacaoCliente(null);
		}
		if (StringUtils.isNotEmpty(qtdParcelasFinanciamento)) {
			contrato.setQtdParcelasFinanciamento(
					Util.converterCampoStringParaValorInteger(Contrato.QUANTIDADE_PARCELAS_FINANCIAMENTO, qtdParcelasFinanciamento));
		} else {
			contrato.setQtdParcelasFinanciamento(null);
		}
		if (StringUtils.isNotEmpty(percentualJurosFinanciamento)) {
			contrato.setPercentualJurosFinanciamento(
					Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_JUROS_FINANCIAMENTO, percentualJurosFinanciamento,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
		} else {
			contrato.setPercentualJurosFinanciamento(null);
		}
		if (StringUtils.isNotEmpty(sistemaAmortizacao) && !sistemaAmortizacao.equals(NAO_SELECIONADO)) {
			EntidadeConteudo sistema = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(sistemaAmortizacao));
			contrato.setSistemaAmortizacao(sistema);
		} else {
			contrato.setSistemaAmortizacao(null);
		}

	}

	/**
	 * Setar pontos consumo selecionados no request.
	 *
	 */
	private void setarPontosConsumoSelecionadosNoRequest(HttpServletRequest request, Long[] chavesPrimariasPontoConsumo, Model model)
			throws GGASException {

		if (chavesPrimariasPontoConsumo != null && chavesPrimariasPontoConsumo.length > 0) {
			Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = this.obterListaImovelPontoConsumoVO(request);
			Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoSelecionadoVO = new HashSet<ImovelPontoConsumoVO>();
			List<Long> listaChavesPontoConsumo = Arrays.asList(chavesPrimariasPontoConsumo);
			PontoConsumo pontoConsumo = null;

			ImovelPontoConsumoVO imovelPontoConsumoVOSelecionado = null;
			Collection<PontoConsumo> listaPontoConsumo = null;
			for (ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
				listaPontoConsumo = new HashSet<PontoConsumo>();
				listaPontoConsumo.addAll(imovelPontoConsumoVO.getListaPontoConsumo());
				for (Iterator<PontoConsumo> iterator = listaPontoConsumo.iterator(); iterator.hasNext();) {
					pontoConsumo = iterator.next();
					if (!listaChavesPontoConsumo.contains(pontoConsumo.getChavePrimaria())) {
						iterator.remove();
					}
				}
				if (!listaPontoConsumo.isEmpty()) {
					imovelPontoConsumoVOSelecionado = new ImovelPontoConsumoVO();
					imovelPontoConsumoVOSelecionado.setChavePrimaria(imovelPontoConsumoVO.getChavePrimaria());
					imovelPontoConsumoVOSelecionado.setNome(imovelPontoConsumoVO.getNome());
					imovelPontoConsumoVOSelecionado.getListaPontoConsumo().addAll(listaPontoConsumo);
					listaImovelPontoConsumoSelecionadoVO.add(imovelPontoConsumoVOSelecionado);
				}
			}
			request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO, listaImovelPontoConsumoSelecionadoVO);
			model.addAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO, listaImovelPontoConsumoSelecionadoVO);

		} else {
			request.getSession().removeAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);
			request.removeAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);
		}
	}

	/**
	 * Preparar filtro.
	 *
	 */
	private void prepararFiltro(Map<String, Object> filtro, ContratoVO contratoVO, HttpServletRequest request) throws GGASException {

		prepararFiltros(filtro, contratoVO);
		String situacaoContraro = contratoVO.getSituacaoContrato();

		if (StringUtils.isNotEmpty(situacaoContraro) && (!NUMERO_MENOS_UM.equals(situacaoContraro))) {
			filtro.put(ID_SITUACAO_CONTRATO, new Long[] { Long.valueOf(situacaoContraro) });
		}

	}

	/**
	 * Preparar filtros.
	 *
	 * @param filtro {@link Map}
	 * @param contratoVO {@link ContratoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltros(Map<String, Object> filtro, ContratoVO contratoVO) throws GGASException {

		String numeroContrato = contratoVO.getNumeroContrato();
		String faturavel = contratoVO.getFaturavel();
		Boolean habilitado = contratoVO.getHabilitado();

		Long[] chavesPrimarias = { contratoVO.getChavePrimaria() };
		if (contratoVO.getChavePrimaria() == null) {
			chavesPrimarias = null;
		}

		Long idCliente = contratoVO.getIdCliente();
		Long idImovel = contratoVO.getIdImovel();
		Long idTipoContrato = contratoVO.getIdTipoContrato();
		Long idModeloContrato = contratoVO.getIdModeloContrato();

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put("numero", Util.converterCampoStringParaValorInteger(LABEL_NUMERO_DO_CONTRATO, numeroContrato));
		}

		if (StringUtils.isNotEmpty(faturavel)) {
			filtro.put(FATURAVEL, Boolean.valueOf(faturavel));
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		}

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if ((idImovel != null) && (idImovel > 0)) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		if ((idTipoContrato != null) && (idTipoContrato > 0)) {
			filtro.put(ID_TIPO_CONTRATO, idTipoContrato);
		}

		if ((idModeloContrato != null) && (idModeloContrato > 0)) {
			filtro.put(ID_MODELO_CONTRATO, idModeloContrato);
		}
	}

	/**
	 * Carregar campos.
	 *
	 */
	private void carregarCampos(ContratoVO contratoVO, ModeloContrato modeloContrato, HttpServletRequest request, Model model)
			throws GGASException {

		carregarCampos(contratoVO, modeloContrato, request, true, model);
	}

	/**
	 * Carregar campos.
	 *
	 */
	private void carregarCampos(ContratoVO contratoVO, ModeloContrato modeloContrato, HttpServletRequest request,
			boolean carregarValoresPadrao, Model model) throws GGASException {

		contratoVO.setIdModeloContrato(modeloContrato.getChavePrimaria());
		String numeroProposta = contratoVO.getNumeroProposta();

		this.carregarDadosModelo(contratoVO, modeloContrato, model);

		Collection<AbaModelo> abas = modeloContrato.getAbas();

		if (abas != null && !abas.isEmpty()) {

			for (AbaModelo abaModelo : abas) {
				if (Aba.PRINCIPAL.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaPrincipais(true);
				}
				if (Aba.DADOS_TECNICOS.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaTecnicos(true);
				}
				if (Aba.CONSUMO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaConsumo(true);
				}
				if (Aba.FATURAMENTO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaFaturamento(true);
				}
				if (Aba.REGRAS_FATURAMENTO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaRegrasFaturamento(true);
				}
				if (Aba.RESPONSABILIDADES.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					contratoVO.setSelAbaResponsabilidade(true);
				}
			}
		}

		String itemFatura = contratoVO.getItemFatura();
		Long[] chavesPrimariasPontoConsumo = contratoVO.getChavesPrimariasPontoConsumo();

		if ((itemFatura == null || itemFatura.trim().length() == 0)
				|| (chavesPrimariasPontoConsumo == null || chavesPrimariasPontoConsumo.length == 0)) {

			model.addAttribute(LISTA_TARIFAS, new ArrayList<Tarifa>());

		} else {

			List<Tarifa> listaTarifa =
					controladorTarifa.obterTarifasPorRamoAtividade(chavesPrimariasPontoConsumo[0], Long.valueOf(itemFatura));

			if (listaTarifa == null || listaTarifa.isEmpty()) {
				listaTarifa = controladorTarifa.obterTarifas(chavesPrimariasPontoConsumo[0], Long.valueOf(itemFatura));
			} else {
				List<Tarifa> listaTarifaSegmento = controladorTarifa.obterTarifas(chavesPrimariasPontoConsumo[0], Long.valueOf(itemFatura));
				if (listaTarifaSegmento != null && !listaTarifaSegmento.isEmpty()) {
					for (Tarifa tarifa : listaTarifaSegmento) {
						if (!listaTarifa.contains(tarifa)) {
							listaTarifa.add(tarifa);
						}
					}
				}
			}

			List<Tarifa> listaTarifaFinal = new ArrayList<Tarifa>();
			if (listaTarifa != null && !listaTarifa.isEmpty()) {
				for (Tarifa tarifa : listaTarifa) {
					if (!listaTarifaFinal.contains(tarifa) && tarifa.getTipoContrato() != null
							&& tarifa.getTipoContrato().getChavePrimaria() == modeloContrato.getTipoModelo().getChavePrimaria()) {
						listaTarifaFinal.add(tarifa);
					}
				}
			}

			model.addAttribute(LISTA_TARIFAS, listaTarifaFinal);
		}

		String fatorUnicoCorrecao = contratoVO.getFatorUnicoCorrecao();

		if (fatorUnicoCorrecao != null && StringUtils.isNotEmpty(fatorUnicoCorrecao)) {
			model.addAttribute(FATOR_UNICO_CORRECAO, fatorUnicoCorrecao);
		}

		List<String> listaGarantiaFinanceiraRenovada = new ArrayList<String>();
		listaGarantiaFinanceiraRenovada.add(LABEL_RENOVAVEL);
		listaGarantiaFinanceiraRenovada.add(RENOVADA);

		model.addAttribute(LISTA_TIPO_GARANTIA_FINANCEIRA, controladorEntidadeConteudo.obterListaTipoGarantiaFinanceira());
		model.addAttribute(LISTA_GARANTIA_FINANCEIRA_RENOVADA, listaGarantiaFinanceiraRenovada);
		model.addAttribute(LISTA_HORA_INICIAL_DIA, controladorEntidadeConteudo.obterListaHoraInicialDia());
		model.addAttribute(LISTA_CRITERIO_USO, controladorEntidadeConteudo.obterListaCriterioUso());
		model.addAttribute(LISTA_TIPO_FATURAMENTO, controladorEntidadeConteudo.obterListaTipoFaturamento());
		model.addAttribute(LISTA_FASE_REFERENCIAL_VENCIMENTO, controladorEntidadeConteudo.obterListaFaseReferencialVencimento());
		model.addAttribute(LISTA_OPCAO_FASE_REF_VENCIMENTO, controladorEntidadeConteudo.obterListaOpcaoFaseRefVencimento());
		model.addAttribute(LISTA_FORMA_PAGAMENTO, controladorEntidadeConteudo.obterListaFormaPagamento());
		model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());
		model.addAttribute(LISTA_UNIDADE_VAZAO, controladorUnidade.listarUnidadesVazao());
		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFatura());
		model.addAttribute(LISTA_ENDERECO_PADRAO, controladorEntidadeConteudo.obterListaEnderecoPadrao());
		model.addAttribute(LISTA_UNIDADE_VOLUME, controladorUnidade.listarUnidadesVolume());
		model.addAttribute(LISTA_ACAO_ANORMALIDADE_CONSUMO, controladorAnormalidade.listarAcaoAnormalidadeConsumoSemLeitura());
		model.addAttribute(LISTA_FORMA_COBRANCA, controladorEntidadeConteudo.listarFormaCobranca());
		model.addAttribute(LISTA_ARRECADADOR_CONVENIO, obterListaArrecadadorConvenioBoletoEArrecadacao());
		model.addAttribute(LISTA_ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO, obterListaArrecadadorConvenioDebitoAutomatico());
		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());

		model.addAttribute(LISTA_PERIODICIDADE, controladorRota.listarPeriodicidades());
		model.addAttribute(LISTA_PENALIDADE_RETIRADA_MAIOR_MENOR, controladorApuracaoPenalidade.listarPenalidadesRetMaiorMenor());
		model.addAttribute(LISTA_PEREIODICIDADE_RETIRADA_MAIOR_MENOR,
				controladorEntidadeConteudo.obterListaPeriodicidadeRetiradaMaiorMenor());
		model.addAttribute(LISTA_PERIODICIDADE_TAKE_OR_PAY, controladorEntidadeConteudo.obterListaPeriodicidadeTOP());
		model.addAttribute(LISTA_PERIODICIDADE_SHIP_OR_PAY, controladorEntidadeConteudo.obterListaPeriodicidadeSOP());
		model.addAttribute(LISTA_CONSUMO_REFERENCIAL, controladorEntidadeConteudo.obterListaConsumoReferencial());
		model.addAttribute(LISTA_FAIXA_PENALIDADE_SOBRE_DEM, controladorContrato.obterListaPenalidadeDemandaSobre());
		model.addAttribute(LISTA_FAIXA_PENALIDADE_SOB_DEM, controladorContrato.obterListaPenalidadeDemandaSob());
		model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());
		model.addAttribute(LISTA_INDICE_CORRECAO_MONETARIA, controladorTarifa.listaIndiceFinanceiro());
		model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, controladorEntidadeConteudo.listarLocalAmostragemPCS());
		model.addAttribute(LISTA_INTERVALO_PCS, controladorHistoricoConsumo.listarIntervaloPCS());
		model.addAttribute(LISTA_REGIME_CONSUMO, controladorEntidadeConteudo.obterListaRegimeConsumo());
		model.addAttribute(LISTA_RESPONSABILIDADES, controladorEntidadeConteudo.obterListaResponsabilidades());
		model.addAttribute(LISTA_DATA_REFERENCIA_CAMBIAL, controladorEntidadeConteudo.obterListaDataReferenciaCambial());
		model.addAttribute(LISTA_DIA_COTACAO, controladorEntidadeConteudo.obterListaDiaCotacao());
		model.addAttribute(LISTA_TIPO_AGRUPAMENTO, controladorEntidadeConteudo.obterListaTipoAgrupamento());
		model.addAttribute(LISTA_CONTRATO_COMPRA, controladorEntidadeConteudo.obterListaContratoCompra());
		model.addAttribute(LISTA_BASE_APURACAO, controladorEntidadeConteudo.listarBasesApuracaoPenalidadeMaiorMenor());
		model.addAttribute(LISTA_TIPO_MULTA_RECISORIA, controladorEntidadeConteudo.obterListaTipoMultaRecisoria());
		model.addAttribute(ITEM_FATURA_MARGEM_DISTRIBUICAO,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));
		model.addAttribute(LISTA_REFERENCIA_QF_PARADA_PROGRAMADA, controladorEntidadeConteudo.obterListaReferenciaQFParadaProgramada());
		model.addAttribute(LISTA_TIPO_APURACAO,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.TIPO_APURACAO));
		model.addAttribute(LISTA_PRECO_COBRANCA,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PRECO_COBRANCA));
		model.addAttribute(LISTA_APURACAO_PARADA_PROGRAMADA,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_PROGRAMADA));
		model.addAttribute(LISTA_APURACAO_PARADA_NAO_PROGRAMADA,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_NAO_PROGRAMADA));
		model.addAttribute(LISTA_APURACAO_CASO_FORTUITO,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_CASO_FORTUITO));
		model.addAttribute(LISTA_APURACAO_FALHA_FORNECIMENTO,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_FALHA_FORNECIMENTO));
		model.addAttribute(LISTA_TIPO_AGRUPAMENTO_CONTRATO,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.TIPO_AGRUPAMENTO));

		if (StringUtils.isNotEmpty(numeroProposta)) {
			model.addAttribute(LISTA_VERSOES_PROPOSTA, controladorProposta.listarPropostasAprovadasPorNumero(numeroProposta));
		}

		model.addAttribute(LISTA_AMORTIZACOES, controladorEntidadeConteudo.listarAmortizacoes());

		prepararListaDatasDisponiveisVencimento(request, model);

	}

	/**
	 * Carregar campos passo2.
	 *
	 * @param request {@link HttpServletResponse}
	 * @param contratoVO {@link ContratoVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void carregarCamposPasso2(HttpServletRequest request, ContratoVO contratoVO, Model model) throws GGASException {

		model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, controladorEntidadeConteudo.listarLocalAmostragemPCS());
		model.addAttribute(LISTA_INTERVALO_PCS, controladorHistoricoConsumo.listarIntervaloPCS());
		model.addAttribute(LISTA_REGIME_CONSUMO, controladorEntidadeConteudo.obterListaRegimeConsumo());
		model.addAttribute(LISTA_RESPONSABILIDADES, controladorEntidadeConteudo.obterListaResponsabilidades());

		this.carregarCamposPassoAbaModalidade(request, model);
		this.verificarLimpezaPreenchimentoPontoConsumo(request);

		boolean todosPontosConsumoDadosInformados = true;
		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO =
				(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		if (listaImovelPontoConsumoVO != null && !listaImovelPontoConsumoVO.isEmpty()) {
			StringBuilder imovelPontoConsumo = new StringBuilder();
			for (ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
				if (imovelPontoConsumoVO.getListaPontoConsumo() != null && !imovelPontoConsumoVO.getListaPontoConsumo().isEmpty()) {
					for (PontoConsumo pontoConsumo : imovelPontoConsumoVO.getListaPontoConsumo()) {
						if (listaPontoConsumoContratoVO == null
								|| !listaPontoConsumoContratoVO.containsKey(pontoConsumo.getChavePrimaria())) {
							todosPontosConsumoDadosInformados = false;
							imovelPontoConsumo.append(LABEL_IMOVEL).append(imovelPontoConsumoVO.getNome())
									.append(Constantes.STRING_HIFEN_ESPACO).append(pontoConsumo.getDescricao())
									.append(Constantes.STRING_VIRGULA_ESPACO);

						} else {
							PontoConsumoContratoVO pontoConsumoVO = listaPontoConsumoContratoVO.get(pontoConsumo.getChavePrimaria());
							if (!pontoConsumoVO.isPreenchimentoConcluido()) {
								todosPontosConsumoDadosInformados = false;
								imovelPontoConsumo.append(LABEL_IMOVEL).append(imovelPontoConsumoVO.getNome())
										.append(Constantes.STRING_HIFEN_ESPACO).append(pontoConsumo.getDescricao())
										.append(Constantes.STRING_VIRGULA_ESPACO);

							}
						}
					}
				}
			}
			String aux = StringUtils.EMPTY;
			if (!imovelPontoConsumo.toString().isEmpty()) {
				aux = imovelPontoConsumo.substring(0, imovelPontoConsumo.length() - 2);
			}
			model.addAttribute(PONTO_CONSUMO_NAO_INFORMADO, aux);

		}

		model.addAttribute(TODOS_PONTOS_CONSUMO_DADOS_INFORMADOS, todosPontosConsumoDadosInformados);

		Long idPontoConsumo = contratoVO.getIdPontoConsumo();

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, SEGMENTO);

			contratoVO.setChaveCep(String.valueOf(pontoConsumo.getCep().getChavePrimaria()));
			contratoVO.setEndFisEntFatCEP(pontoConsumo.getCep().getCep());
			Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento =
					controladorFaixaPressaoFornecimento.listarFaixaPressaoFornecimentoPorSegmento(pontoConsumo.getSegmento());

			Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoSemSegmentoAssociado =
					controladorFaixaPressaoFornecimento.listarFaixaPressaoFornecimentoSemSegmentoAssociado();

			listaFaixaPressaoFornecimento.addAll(listaFaixaPressaoFornecimentoSemSegmentoAssociado);

			model.addAttribute(LISTA_FAIXAS_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);
			
			
			ContratoPontoConsumo contratoPontoAnterior = controladorContrato.obterUltimoContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
			
			if (contratoPontoAnterior != null) {
				model.addAttribute("vazaoInstantaneaMaximaAnterior",
						contratoPontoAnterior.getMedidaVazaoMaximaInstantanea() != null
								? contratoPontoAnterior.getMedidaVazaoMaximaInstantanea() + " "
										+ contratoPontoAnterior.getUnidadeVazaoMaximaInstantanea()
												.getDescricaoAbreviada()
								: "");
				model.addAttribute("pressaoFornecimentoAnterior",
						contratoPontoAnterior.getFaixaPressaoFornecimento() != null
								? contratoPontoAnterior.getFaixaPressaoFornecimento().getDescricaoFormatada()
								: "");
				model.addAttribute("fornecimentoMaximoDiarioAnterior",
						contratoPontoAnterior.getMedidaFornecimentoMaxDiaria() != null
								? contratoPontoAnterior.getMedidaFornecimentoMaxDiaria() + " "
										+ contratoPontoAnterior.getUnidadeFornecimentoMaxDiaria()
												.getDescricaoAbreviada()
								: "");
			}

		}
	}

	/**
	 * Método responsável por adicionar uma modalidade ao contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarModalidadeContrato")
	public String adicionarModalidadeContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> abaModalidades = oMapper.convertValue(contratoVO, Map.class);

		Long idModeloContrato = contratoVO.getIdModeloContrato();

		// ###Modalidade
		String modalidade = contratoVO.getModalidade();

		// ######QDC
		String[] qdcContrato = contratoVO.getQDCContrato();

		String prazoRevizaoQDC = contratoVO.getPrazoRevizaoQDC();
		// Se o array estiver preenchido, validar
		// os campos qdc, dataVigenciaQDC,

		// ######QDS
		String qdsMaiorQDC = contratoVO.getQdsMaiorQDC();
		String variacaoSuperiorQDC = contratoVO.getVariacaoSuperiorQDC();
		String diasAntecSolicConsumo = contratoVO.getDiasAntecSolicConsumo();
		String numMesesSolicConsumo = contratoVO.getNumMesesSolicConsumo();
		String confirmacaoAutomaticaQDS = contratoVO.getConfirmacaoAutomaticaQDS();
		String indicadorProgramacaoConsumo = contratoVO.getIndicadorProgramacaoConsumo();

		String horaLimiteProgramacaoDiaria = contratoVO.getHoraLimiteProgramacaoDiaria();
		String horaLimiteProgIntradiaria = contratoVO.getHoraLimiteProgIntradiaria();

		String horaLimiteAceitacaoDiaria = contratoVO.getHoraLimiteAceitacaoDiaria();
		String horaLimiteAceitIntradiaria = contratoVO.getHoraLimiteAceitIntradiaria();

		// ######Take or Pay
		String recuperacaoAutoTakeOrPay = contratoVO.getRecuperacaoAutoTakeOrPay();
		// Se a lista compromisso TOP vo estiver
		// na sessao e preenchida, validar os 3
		// campos acima

		// ########### QPNR
		String percDuranteRetiradaQPNR = contratoVO.getPercDuranteRetiradaQPNR();
		String percMinDuranteRetiradaQPNR = contratoVO.getPercMinDuranteRetiradaQPNR();
		String percFimRetiradaQPNR = contratoVO.getPercFimRetiradaQPNR();
		String dataInicioRetiradaQPNR = contratoVO.getDataInicioRetiradaQPNR();
		String dataFimRetiradaQPNR = contratoVO.getDataFimRetiradaQPNR();
		String tempoValidadeRetiradaQPNR = contratoVO.getTempoValidadeRetiradaQPNR();
		Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);

		try {

			controladorContrato.validarDatasAbaModalidade(dataInicioRetiradaQPNR, dataFimRetiradaQPNR, contrato);

			// #######Ship or Pay
			String consumoReferenciaShipOrPay = contratoVO.getConsumoReferenciaShipOrPay();
			String margemVariacaoShipOrPay = contratoVO.getMargemVariacaoShipOrPay();

			Collection<ModalidadeConsumoFaturamentoVO> listaModalidadeConsumoFaturamentoVO =
					(Collection<ModalidadeConsumoFaturamentoVO>) request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
			if (listaModalidadeConsumoFaturamentoVO == null) {
				listaModalidadeConsumoFaturamentoVO = new HashSet<ModalidadeConsumoFaturamentoVO>();
			}

			ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO = new ModalidadeConsumoFaturamentoVO();

			ContratoModalidade entModalidade = null;
			if (StringUtils.isNotEmpty(modalidade) && !modalidade.equals(NAO_SELECIONADO)) {
				entModalidade = controladorContrato
						.obterContratoModalidade(Util.converterCampoStringParaValorLong(ContratoPontoConsumo.MODALIDADE, modalidade));
				modalidadeConsumoFaturamentoVO.setModalidade(entModalidade);

			}

			if (qdcContrato != null && qdcContrato.length > 0) {
				abaModalidades.put(QDC, NUMERO_UM);
				abaModalidades.put(DATA_VIGENCIA_QDC, NUMERO_UM);

				for (int i = 0; i < qdcContrato.length; i++) {

					String valorQDCContrato = qdcContrato[i];
					String dataQDC = valorQDCContrato.split(Constantes.STRING_ESPACO)[0];
					String valorQDC = valorQDCContrato.split(Constantes.STRING_ESPACO)[2];

					ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC =
							(ContratoPontoConsumoModalidadeQDC) controladorContrato.criarContratoPontoConsumoModalidadeQDC();
					contratoPontoConsumoModalidadeQDC
							.setDataVigencia(Util.converterCampoStringParaData(LABEL_DATA, dataQDC, Constantes.FORMATO_DATA_BR));
					contratoPontoConsumoModalidadeQDC.setMedidaVolume(Util.converterCampoStringParaValorBigDecimal(LABEL_VALOR_QDC,
							valorQDC, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
					contratoPontoConsumoModalidadeQDC.setUltimaAlteracao(Calendar.getInstance().getTime());
					contratoPontoConsumoModalidadeQDC.setDadosAuditoria(getDadosAuditoria(request));
					modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC().add(contratoPontoConsumoModalidadeQDC);
				}
			}

			if (StringUtils.isNotEmpty(prazoRevizaoQDC)) {
				modalidadeConsumoFaturamentoVO.setPrazoRevizaoQDC(Util.converterCampoStringParaData(ContratoPontoConsumo.PRAZO_REVISAO_QDC,
						prazoRevizaoQDC, Constantes.FORMATO_DATA_BR));
			}

			if (StringUtils.isNotEmpty(qdsMaiorQDC)) {
				modalidadeConsumoFaturamentoVO.setQdsMaiorQDC(Boolean.valueOf(qdsMaiorQDC));
			}

			if (StringUtils.isNotEmpty(variacaoSuperiorQDC)) {
				modalidadeConsumoFaturamentoVO
						.setVariacaoSuperiorQDC(Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_VARIACAO_SUPERIOR_QDC,
								variacaoSuperiorQDC, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			}

			if (StringUtils.isNotEmpty(diasAntecSolicConsumo)) {
				modalidadeConsumoFaturamentoVO.setDiasAntecSolicConsumo(Util.converterCampoStringParaValorInteger(
						ContratoPontoConsumo.DIAS_ANTECEDENCIA_SOLICITACAO_CONSUMO, diasAntecSolicConsumo));
			}

			if (StringUtils.isNotEmpty(numMesesSolicConsumo)) {
				modalidadeConsumoFaturamentoVO.setNumMesesSolicConsumo(
						Util.converterCampoStringParaValorInteger(ContratoPontoConsumo.MESES_SOLICITADOS, numMesesSolicConsumo));
			}

			if (StringUtils.isNotEmpty(confirmacaoAutomaticaQDS)) {
				modalidadeConsumoFaturamentoVO.setConfirmacaoAutomaticaQDS(Boolean.valueOf(confirmacaoAutomaticaQDS));
			}

			if (StringUtils.isNotEmpty(indicadorProgramacaoConsumo)) {
				modalidadeConsumoFaturamentoVO.setIndicadorProgramacaoConsumo(Boolean.valueOf(indicadorProgramacaoConsumo));
			}

			if (!horaLimiteProgramacaoDiaria.isEmpty()) {
				modalidadeConsumoFaturamentoVO.setHoraLimiteProgramacaoDiaria(Integer.parseInt(horaLimiteProgramacaoDiaria));
			}

			if (!horaLimiteProgIntradiaria.isEmpty()) {
				modalidadeConsumoFaturamentoVO.setHoraLimiteProgramacaoIntraDiaria(Integer.parseInt(horaLimiteProgIntradiaria));
			}

			if (!horaLimiteAceitacaoDiaria.isEmpty()) {
				modalidadeConsumoFaturamentoVO.setHoraLimiteAceitacaoDiaria(Integer.parseInt(horaLimiteAceitacaoDiaria));
			}

			if (!horaLimiteAceitIntradiaria.isEmpty()) {
				modalidadeConsumoFaturamentoVO.setHoraLimiteAceitIntradiaria(Integer.parseInt(horaLimiteAceitIntradiaria));
			}

			Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayVO =
					(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);

			Collection<CompromissoTakeOrPayVO> listaCompromissosShipOrPayVO =
					(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);

			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetiradaMaiorMenorVO =
					(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession()
							.getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);
			if (listaPenalidadesRetiradaMaiorMenorVO != null) {

				modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO().clear();
				modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO().addAll(listaPenalidadesRetiradaMaiorMenorVO);
			}

			BigDecimal percentualMaximoQDC = BigDecimal.ZERO;
			BigDecimal percentualMinomoQDC = BigDecimal.ZERO;
			if (StringUtils.isNotEmpty(percDuranteRetiradaQPNR)) {
				// TODO: colocar mensagem no arquivo de mensagens.
				percentualMaximoQDC = Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_MAXIMO_EM_RELACAO_A_QDC,
						percDuranteRetiradaQPNR, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}

			if (StringUtils.isNotEmpty(percMinDuranteRetiradaQPNR)) {
				// TODO: colocar mensagem no arquivo de mensagens.
				percentualMinomoQDC =
						Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_MINIMO_EM_RELACAO_A_QDC, percMinDuranteRetiradaQPNR,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}

			controladorContrato.validarPercentualMaximoQDC(percentualMinomoQDC, percentualMaximoQDC);

			if (listaCompromissosTakeOrPayVO != null && !listaCompromissosTakeOrPayVO.isEmpty()) {
				for (CompromissoTakeOrPayVO compromissoTakeOrPayVO : listaCompromissosTakeOrPayVO) {
					if (percentualMaximoQDC != null && !(percentualMaximoQDC.compareTo(BigDecimal.ZERO) == 0)) {
						controladorContrato.validarCompromissoTakeOrPayQPNR(compromissoTakeOrPayVO.getPeriodicidadeTakeOrPay(),
								compromissoTakeOrPayVO.getMargemVariacaoTakeOrPay(), percentualMaximoQDC);
					}

				}

				abaModalidades.put(PERIODICIDADE_TAKE_OR_PAY, NUMERO_UM);
				abaModalidades.put(CONSUMO_REFERENCIA_TAKE_OR_PAY, NUMERO_UM);
				abaModalidades.put(MARGEM_VARIACAO_TAKE_OR_PAY, NUMERO_UM);

				abaModalidades.put(REFERENCIA_QF_PARADA_PROGRAMADA, NUMERO_MENOS_UM);
				abaModalidades.put(DATA_INICIO_VIGENCIA, Constantes.STRING_ESPACO);
				abaModalidades.put(DATA_FIM_VIGENCIA, Constantes.STRING_ESPACO);
				abaModalidades.put(PRECO_COBRANCA, NUMERO_MENOS_UM);
				abaModalidades.put(TIPO_APURACAO, NUMERO_MENOS_UM);
				abaModalidades.put(TIPO_AGRUPAMENTO_CONTRATO, NUMERO_MENOS_UM);
				abaModalidades.put(CONSIDERA_PARADA_PROGRAMADA, NUMERO_ZERO);
				abaModalidades.put(CONSIDERA_PARADA_NAO_PROGRAMADA, NUMERO_ZERO);
				abaModalidades.put(CONSIDERA_FALHA_FORNECIMENTO, NUMERO_ZERO);
				abaModalidades.put(CONSIDERA_CASO_FORTUITO, NUMERO_ZERO);
				abaModalidades.put(RECUPERAVEL, NUMERO_ZERO);
				abaModalidades.put(PERCENTUAL_NAO_RECUPERAVEL, NUMERO_ZERO);
				abaModalidades.put(APURACAO_PARADA_PROGRAMADA, NUMERO_MENOS_UM);
				abaModalidades.put(APURACAO_PARADA_NAO_PROGRAMADA, NUMERO_MENOS_UM);
				abaModalidades.put(APURACAO_CASO_FORTUITO, NUMERO_MENOS_UM);
				abaModalidades.put(APURACAO_FALHA_FORNECIMENTO, NUMERO_MENOS_UM);

				modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO().clear();
				modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO().addAll(listaCompromissosTakeOrPayVO);
			}

			if (listaCompromissosShipOrPayVO != null && !listaCompromissosShipOrPayVO.isEmpty()) {
				for (CompromissoTakeOrPayVO compromissoShipOrPayVO : listaCompromissosShipOrPayVO) {
					if (percentualMaximoQDC != null && !(percentualMaximoQDC.compareTo(BigDecimal.ZERO) == 0)) {
						controladorContrato.validarCompromissoTakeOrPayQPNR(compromissoShipOrPayVO.getPeriodicidadeTakeOrPay(),
								compromissoShipOrPayVO.getMargemVariacaoTakeOrPay(), percentualMaximoQDC);
					}

				}

				abaModalidades.put(PERIODICIDADE_SHIP_OR_PAY, NUMERO_UM);
				abaModalidades.put(CONSUMO_REFERENCIA_SHIP_OR_PAY, NUMERO_UM);
				abaModalidades.put(MARGEM_VARIACAO_SHIP_OR_PAY, NUMERO_UM);
				modalidadeConsumoFaturamentoVO.setListaCompromissoShipOrPayVO(listaCompromissosShipOrPayVO);
			}

			if (StringUtils.isNotEmpty(recuperacaoAutoTakeOrPay)) {
				modalidadeConsumoFaturamentoVO.setRecuperacaoAutoTakeOrPay(Boolean.valueOf(recuperacaoAutoTakeOrPay));
			}

			if (StringUtils.isNotEmpty(percDuranteRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setPercDuranteRetiradaQPNR(percentualMaximoQDC);
			}

			if (StringUtils.isNotEmpty(percMinDuranteRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setPercMinDuranteRetiradaQPNR(percentualMinomoQDC);
			}

			if (StringUtils.isNotEmpty(percFimRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setPercFimRetiradaQPNR(
						Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_FIM_RETIRADA_QPNR, percFimRetiradaQPNR,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM)));
			}

			if (StringUtils.isNotEmpty(dataInicioRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setDataInicioRetiradaQPNR(Util.converterCampoStringParaData(
						ContratoPontoConsumo.DATA_INICIO_RECUPERACAO, dataInicioRetiradaQPNR, Constantes.FORMATO_DATA_BR));
			}

			if (StringUtils.isNotEmpty(dataFimRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setDataFimRetiradaQPNR(Util.converterCampoStringParaData(
						ContratoPontoConsumo.DATA_MAXIMA_RECUPERACAO, dataFimRetiradaQPNR, Constantes.FORMATO_DATA_BR));
			}
			if (StringUtils.isNotEmpty(tempoValidadeRetiradaQPNR)) {
				modalidadeConsumoFaturamentoVO.setTempoValidadeRetiradaQPNR(Util.converterCampoStringParaValorInteger(
						ContratoPontoConsumoModalidade.VALIDADE_RETIRADA_QPNR, tempoValidadeRetiradaQPNR));
			}

			EntidadeConteudo entConsumoReferenciaShipOrPay = null;
			if (StringUtils.isNotEmpty(consumoReferenciaShipOrPay) && !consumoReferenciaShipOrPay.equals(NAO_SELECIONADO)) {
				entConsumoReferenciaShipOrPay = controladorEntidadeConteudo.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(
						ContratoPontoConsumo.CONSUMO_REFERENCIA_SHIP_OR_PAY, consumoReferenciaShipOrPay));
				modalidadeConsumoFaturamentoVO.setConsumoReferenciaShipOrPay(entConsumoReferenciaShipOrPay);
			}

			if (StringUtils.isNotEmpty(margemVariacaoShipOrPay)) {
				modalidadeConsumoFaturamentoVO
						.setMargemVariacaoShipOrPay(Util.converterCampoStringParaValorBigDecimal(LABEL_MARGEM_VARIACAO_SHIP_OR_PAY,
								margemVariacaoShipOrPay, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			}

			// ########### QNR
			String percentualConsiderarQNR = contratoVO.getPercentualQNR();
			BigDecimal percentualQNR = BigDecimal.ZERO;
			if (StringUtils.isNotEmpty(percentualConsiderarQNR)) {
				// TODO: colocar mensagem no arquivo de mensagens.
				percentualQNR = Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_A_CONSIDERAR_PARA_QNR,
						percentualConsiderarQNR, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}
			modalidadeConsumoFaturamentoVO.setPercentualQNR(percentualQNR);

			Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.REGRAS_FATURAMENTO);

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}

			String[] camposPenalidadePorRetirada = new String[] { TIPO_AGRUPAMENTO_CONTRATO, PERIODICIDADE_RETIRADA_MAIOR_MENOR,
					PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE, PERIODICIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE,
					BASE_APURACAO_RETIRADA_MAIOR_MENOR_MODALIDADE, DATA_INICIO_RETIRADA_MAIOR_MENOR_MODALIDADE,
					DATA_FIM_RETIRADA_MAIOR_MENOR_MODALIDADE, PRECO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE,
					FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE, PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE,
					PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR_MODALIDADE, CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR_MODALIDADE,
					PERCENTUAL_RETIRADA_MAIOR_MENOR_MODALIDADE, PERIODICIDADE_TAKE_OR_PAY, REFERENCIA_QF_PARADA_PROGRAMADA,
					CONSUMO_REFERENCIA_TAKE_OR_PAY, MARGEM_VARIACAO_TAKE_OR_PAY, INDICADOR_IMPOSTO_MAIOR_MENOR,
					INDICADOR_IMPOSTO_MAIOR_MENOR_MODALIDADE, DATA_INICIO_VIGENCIA, DATA_FIM_VIGENCIA, PRECO_COBRANCA, TIPO_APURACAO,
					CONSIDERA_PARADA_PROGRAMADA, CONSIDERA_PARADA_NAO_PROGRAMADA, CONSIDERA_FALHA_FORNECIMENTO, CONSIDERA_CASO_FORTUITO,
					RECUPERAVEL, PERCENTUAL_NAO_RECUPERAVEL, APURACAO_PARADA_PROGRAMADA, APURACAO_PARADA_NAO_PROGRAMADA,
					APURACAO_CASO_FORTUITO, APURACAO_FALHA_FORNECIMENTO };

			if (modeloContrato != null && camposPenalidadePorRetirada != null) {
				removerObrigatoriedadeAtributoModeloContrato(modeloContrato, camposPenalidadePorRetirada);
			}

			controladorContrato.validarCamposObrigatoriosContrato(abaModalidades, modeloContrato, aba, false);

			String dataSomaQDC = null;
			if (modalidadeConsumoFaturamentoVO.getModalidade() != null) {
				Long idModalidadeContrato = contratoVO.getIdModalidadeContrato();
				if ((idModalidadeContrato == null) || (idModalidadeContrato <= 0)) {
					controladorContrato.validarModalidadeExistente(listaModalidadeConsumoFaturamentoVO, modalidadeConsumoFaturamentoVO);
				}

				Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO =
						(Map<Long, PontoConsumoContratoVO>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);

				List<ModalidadeConsumoFaturamentoVO> listaModalidades =
						this.recuperarModalidadeConsumoFaturamentoVO(listaPontoConsumoContratoVO);

				List<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDCOrdenada = new ArrayList<ContratoPontoConsumoModalidadeQDC>();
				for (ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamento : listaModalidades) {

					// Caso a Modalidade não seja
					// igual àquela selecionada em
					// tela, adiciona os QDCs
					if (!modalidadeConsumoFaturamento.equals(modalidadeConsumoFaturamentoVO)) {

						Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC =
								modalidadeConsumoFaturamento.getListaContratoPontoConsumoModalidadeQDC();
						listaModalidadeQDCOrdenada.addAll(listaModalidadeQDC);
					}
				}

				// Adiciona os QDCs da Modalidade
				// igual àquela selecionada em tela
				Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC =
						modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC();
				listaModalidadeQDCOrdenada.addAll(listaModalidadeQDC);

				dataSomaQDC = controladorContrato.validarAlteracaoQDCContrato(listaModalidadeQDCOrdenada, contrato.getListaContratoQDC(),
						contrato.getDataVencimentoObrigacoes());

				listaModalidadeConsumoFaturamentoVO.remove(modalidadeConsumoFaturamentoVO);
				listaModalidadeConsumoFaturamentoVO.add(modalidadeConsumoFaturamentoVO);
			}

			request.getSession().setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, listaModalidadeConsumoFaturamentoVO);
			model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, listaModalidadeConsumoFaturamentoVO);

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			contratoVO.setModalidade(NAO_SELECIONADO);
			contratoVO.setDataVigenciaQDC(StringUtils.EMPTY);
			contratoVO.setQDCContrato(new String[] {});
			contratoVO.setIndicadorProgramacaoConsumo(NAO_SELECIONADO);
			contratoVO.setQdsMaiorQDC(NAO_SELECIONADO);
			contratoVO.setVariacaoSuperiorQDC(StringUtils.EMPTY);
			contratoVO.setHoraLimiteProgramacaoDiaria(StringUtils.EMPTY);
			contratoVO.setHoraLimiteProgIntradiaria(StringUtils.EMPTY);
			contratoVO.setHoraLimiteAceitacaoDiaria(StringUtils.EMPTY);
			contratoVO.setHoraLimiteAceitIntradiaria(StringUtils.EMPTY);
			contratoVO.setConfirmacaoAutomaticaQDS(StringUtils.EMPTY);

			// Carregar os campos padrao do modelo
			if (modeloContrato != null) {
				this.carregarDadosModelo(contratoVO, modeloContrato, model);
			}
			if (dataSomaQDC != null) {
				mensagemAlerta(model, ControladorContrato.ERRO_NEGOCIO_SOMA_QDC_MODALIDADE_MAIOR_QDC_CONTRATO, dataSomaQDC);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Método responsável por exibir a alteracao de uma modalidade de contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirAlteracaoModalidadeCadastrada")
	public String exibirAlteracaoModalidadeCadastrada(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long idModalidadeContrato = contratoVO.getIdModalidadeContrato();

		Collection<ModalidadeConsumoFaturamentoVO> listaMcfVO =
				(Collection<ModalidadeConsumoFaturamentoVO>) request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);

		if (listaMcfVO != null && !listaMcfVO.isEmpty() && idModalidadeContrato != null) {

			for (ModalidadeConsumoFaturamentoVO mcfVO : listaMcfVO) {
				if (mcfVO.getModalidade().getChavePrimaria() == idModalidadeContrato) {
					this.popularFormAlteracaoModalidadeCadastrada(mcfVO, contratoVO, request, model);
					break;
				}
			}
		}

		this.popularCamposCPCInfoComum(request, contratoVO, false, model);
		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Popular form alteracao modalidade cadastrada.
	 *
	 */
	private void popularFormAlteracaoModalidadeCadastrada(ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO,
			ContratoVO contratoVO, HttpServletRequest request, Model model) {

		if (modalidadeConsumoFaturamentoVO.getModalidade() != null) {
			contratoVO.setModalidade(String.valueOf(modalidadeConsumoFaturamentoVO.getModalidade().getChavePrimaria()));
		}

		Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC =
				modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC();
		if (listaContratoPontoConsumoModalidadeQDC != null && !listaContratoPontoConsumoModalidadeQDC.isEmpty()) {
			String[] qdcContrato = new String[listaContratoPontoConsumoModalidadeQDC.size()];
			StringBuilder stringBuilder = null;
			int i = 0;
			for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaContratoPontoConsumoModalidadeQDC) {
				stringBuilder = new StringBuilder();
				stringBuilder.append(Util.converterDataParaStringSemHora(contratoPontoConsumoModalidadeQDC.getDataVigencia(),
						Constantes.FORMATO_DATA_BR));
				stringBuilder.append(" | ");
				stringBuilder.append(Util.converterCampoCurrencyParaString(contratoPontoConsumoModalidadeQDC.getMedidaVolume(),
						Constantes.LOCALE_PADRAO));
				qdcContrato[i] = stringBuilder.toString();
				i++;
			}
			contratoVO.setQDCContrato(qdcContrato);
		}

		if (modalidadeConsumoFaturamentoVO.getPrazoRevizaoQDC() != null) {
			contratoVO.setPrazoRevizaoQDC(
					Util.converterDataParaStringSemHora(modalidadeConsumoFaturamentoVO.getPrazoRevizaoQDC(), Constantes.FORMATO_DATA_BR));
		}

		if (modalidadeConsumoFaturamentoVO.getQdsMaiorQDC() != null) {
			contratoVO.setQdsMaiorQDC(String.valueOf(modalidadeConsumoFaturamentoVO.getQdsMaiorQDC()));
		}

		if (modalidadeConsumoFaturamentoVO.getVariacaoSuperiorQDC() != null) {
			String converterCampoPercentualParaString = Util.converterCampoPercentualParaString(VARIACAO_SUPERIOR_QDC,
					modalidadeConsumoFaturamentoVO.getVariacaoSuperiorQDC(), Constantes.LOCALE_PADRAO);
			contratoVO.setVariacaoSuperiorQDC(converterCampoPercentualParaString);
		} else {
			contratoVO.setVariacaoSuperiorQDC(StringUtils.EMPTY);
		}

		if (modalidadeConsumoFaturamentoVO.getDiasAntecSolicConsumo() != null) {
			contratoVO.setDiasAntecSolicConsumo(String.valueOf(modalidadeConsumoFaturamentoVO.getDiasAntecSolicConsumo()));
		}

		if (modalidadeConsumoFaturamentoVO.getNumMesesSolicConsumo() != null) {
			contratoVO.setNumMesesSolicConsumo(String.valueOf(modalidadeConsumoFaturamentoVO.getNumMesesSolicConsumo()));
		}

		if (modalidadeConsumoFaturamentoVO.getConfirmacaoAutomaticaQDS() != null) {
			contratoVO.setConfirmacaoAutomaticaQDS(String.valueOf(modalidadeConsumoFaturamentoVO.getConfirmacaoAutomaticaQDS()));
		}

		if (modalidadeConsumoFaturamentoVO.getIndicadorProgramacaoConsumo() != null) {
			contratoVO.setIndicadorProgramacaoConsumo(String.valueOf(modalidadeConsumoFaturamentoVO.getIndicadorProgramacaoConsumo()));
		}

		if (modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoDiaria() != null) {
			contratoVO.setHoraLimiteProgramacaoDiaria(String.valueOf(modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoDiaria()));
		}

		if (modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoIntraDiaria() != null) {
			contratoVO.setHoraLimiteProgIntradiaria(String.valueOf(modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoIntraDiaria()));
		}
		if (modalidadeConsumoFaturamentoVO.getHoraLimiteAceitacaoDiaria() != null) {
			contratoVO.setHoraLimiteAceitacaoDiaria(String.valueOf(modalidadeConsumoFaturamentoVO.getHoraLimiteAceitacaoDiaria()));
		}

		if (modalidadeConsumoFaturamentoVO.getHoraLimiteAceitIntradiaria() != null) {
			contratoVO.setHoraLimiteAceitIntradiaria(String.valueOf(modalidadeConsumoFaturamentoVO.getHoraLimiteAceitIntradiaria()));
		}

		request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO,
				modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO());

		request.getSession().setAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO,
				modalidadeConsumoFaturamentoVO.getListaCompromissoShipOrPayVO());

		request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO,
				modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO());

		model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO());

		model.addAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, modalidadeConsumoFaturamentoVO.getListaCompromissoShipOrPayVO());

		model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO,
				modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO());

		if (modalidadeConsumoFaturamentoVO.getRecuperacaoAutoTakeOrPay() != null) {
			contratoVO.setRecuperacaoAutoTakeOrPay(String.valueOf(modalidadeConsumoFaturamentoVO.getRecuperacaoAutoTakeOrPay()));
		}

		if (modalidadeConsumoFaturamentoVO.getPercDuranteRetiradaQPNR() != null) {
			contratoVO.setPercDuranteRetiradaQPNR(Util.converterCampoPercentualParaString(PERC_DURANTE_RETIRADA_QPNR,
					modalidadeConsumoFaturamentoVO.getPercDuranteRetiradaQPNR().multiply(BigDecimal.valueOf(CEM)),
					Constantes.LOCALE_PADRAO));
		}

		if (modalidadeConsumoFaturamentoVO.getPercMinDuranteRetiradaQPNR() != null) {
			contratoVO.setPercMinDuranteRetiradaQPNR(Util.converterCampoPercentualParaString(PERC_MIN_DURANTE_RETIRADA_QPNR,
					modalidadeConsumoFaturamentoVO.getPercMinDuranteRetiradaQPNR().multiply(BigDecimal.valueOf(CEM)),
					Constantes.LOCALE_PADRAO));
		}

		if (modalidadeConsumoFaturamentoVO.getPercFimRetiradaQPNR() != null) {
			contratoVO.setPercFimRetiradaQPNR(Util.converterCampoPercentualParaString(PERC_FIM_RETIRADA_QPNR,
					modalidadeConsumoFaturamentoVO.getPercFimRetiradaQPNR().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		if (modalidadeConsumoFaturamentoVO.getDataInicioRetiradaQPNR() != null) {
			contratoVO.setDataInicioRetiradaQPNR(Util.converterDataParaStringSemHora(
					modalidadeConsumoFaturamentoVO.getDataInicioRetiradaQPNR(), Constantes.FORMATO_DATA_BR));
		}

		if (modalidadeConsumoFaturamentoVO.getDataFimRetiradaQPNR() != null) {
			contratoVO.setDataFimRetiradaQPNR(Util.converterDataParaStringSemHora(modalidadeConsumoFaturamentoVO.getDataFimRetiradaQPNR(),
					Constantes.FORMATO_DATA_BR));
		}

		if (modalidadeConsumoFaturamentoVO.getTempoValidadeRetiradaQPNR() != null) {
			contratoVO.setTempoValidadeRetiradaQPNR(modalidadeConsumoFaturamentoVO.getTempoValidadeRetiradaQPNR().toString());
		}

		if (modalidadeConsumoFaturamentoVO.getConsumoReferenciaShipOrPay() != null) {
			contratoVO.setConsumoReferenciaShipOrPay(
					String.valueOf(modalidadeConsumoFaturamentoVO.getConsumoReferenciaShipOrPay().getChavePrimaria()));
		}

		if (modalidadeConsumoFaturamentoVO.getMargemVariacaoShipOrPay() != null) {
			contratoVO.setMargemVariacaoShipOrPay(Util.converterCampoPercentualParaString(MARGEM_VARIACAO_SHIP_OR_PAY,
					modalidadeConsumoFaturamentoVO.getMargemVariacaoShipOrPay(), Constantes.LOCALE_PADRAO));
		}

		if (modalidadeConsumoFaturamentoVO.getPercentualQNR() != null) {
			contratoVO.setPercentualQNR(Util.converterCampoPercentualParaString(LABEL_PERCENTUAL_QNR,
					modalidadeConsumoFaturamentoVO.getPercentualQNR().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));
	}

	/**
	 * Limpar campos aba modalidade.
	 *
	 */
	private void limparCamposAbaModalidade(ContratoVO contratoVO, HttpServletRequest request) {

		contratoVO.setModalidade(NAO_SELECIONADO);
		contratoVO.setDataVigenciaQDC(StringUtils.EMPTY);
		contratoVO.setQDCContrato(new String[] {});
		contratoVO.setPrazoRevizaoQDC(StringUtils.EMPTY);
		contratoVO.setQdsMaiorQDC(StringUtils.EMPTY);
		contratoVO.setDiasAntecSolicConsumo(StringUtils.EMPTY);
		contratoVO.setNumMesesSolicConsumo(StringUtils.EMPTY);
		contratoVO.setNumHorasSolicConsumo(StringUtils.EMPTY);
		contratoVO.setConfirmacaoAutomaticaQDS(StringUtils.EMPTY);
		contratoVO.setPeriodicidadeTakeOrPay(NAO_SELECIONADO);
		contratoVO.setReferenciaQFParadaProgramada(NAO_SELECIONADO);
		contratoVO.setConsumoReferenciaTakeOrPay(NAO_SELECIONADO);
		contratoVO.setMargemVariacaoTakeOrPay(StringUtils.EMPTY);
		contratoVO.setRecuperacaoAutoTakeOrPay(StringUtils.EMPTY);
		contratoVO.setPercDuranteRetiradaQPNR(StringUtils.EMPTY);
		contratoVO.setPercMinDuranteRetiradaQPNR(StringUtils.EMPTY);
		contratoVO.setPercFimRetiradaQPNR(StringUtils.EMPTY);
		contratoVO.setDataInicioRetiradaQPNR(StringUtils.EMPTY);
		contratoVO.setDataFimRetiradaQPNR(StringUtils.EMPTY);
		contratoVO.setConsumoReferenciaShipOrPay(NAO_SELECIONADO);
		contratoVO.setConsumoReferenciaShipOrPay(StringUtils.EMPTY);
		contratoVO.setIdModalidadeContrato(null);
		contratoVO.setTempoValidadeRetiradaQPNR(StringUtils.EMPTY);

		request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);

		request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);

	}

	/**
	 * Método responsável por excluir uma modalidade ao contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirModalidadeContrato")
	public String excluirModalidadeContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long[] idsModalidadeContrato = contratoVO.getIdsModalidadeContrato();

		Collection<ModalidadeConsumoFaturamentoVO> listaModalidadeConsumoFaturamentoVO =
				(Collection<ModalidadeConsumoFaturamentoVO>) request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);

		try {
			tratarCamposSelectAbaConsumo(contratoVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if (idsModalidadeContrato != null && idsModalidadeContrato.length > 0 && listaModalidadeConsumoFaturamentoVO != null
				&& !listaModalidadeConsumoFaturamentoVO.isEmpty()) {
			List<Long> listaChavesModalidadeContrato = Arrays.asList(idsModalidadeContrato);
			for (Iterator<ModalidadeConsumoFaturamentoVO> iterator = listaModalidadeConsumoFaturamentoVO.iterator(); iterator.hasNext();) {
				ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO = iterator.next();
				if (listaChavesModalidadeContrato.contains(modalidadeConsumoFaturamentoVO.getModalidade().getChavePrimaria())) {
					iterator.remove();
				}
			}
			request.getSession().setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, listaModalidadeConsumoFaturamentoVO);
		}

		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, listaModalidadeConsumoFaturamentoVO);

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Aplicar dados aba modalidades.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void aplicarDadosAbaModalidades(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, HttpServletRequest request, boolean isValidarCamposObrigatorios)
			throws GGASException {

		Map<String, Object> abaModalidades = new HashMap<String, Object>();

		Collection<ModalidadeConsumoFaturamentoVO> listaModalidadeConsumoFaturamentoVO =
				(Collection<ModalidadeConsumoFaturamentoVO>) request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);

		if (listaModalidadeConsumoFaturamentoVO != null && !listaModalidadeConsumoFaturamentoVO.isEmpty()) {

			Collection<ModalidadeConsumoFaturamentoVO> modalidades = new HashSet<ModalidadeConsumoFaturamentoVO>();
			modalidades.addAll(listaModalidadeConsumoFaturamentoVO);
			abaModalidades.put(MODALIDADES, modalidades);
			PontoConsumoContratoVO pontoConsumoContratoVO;
			if (chavePontoConsumo != null && chavePontoConsumo > 0) {
				pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
				if (pontoConsumoContratoVO == null) {
					pontoConsumoContratoVO = new PontoConsumoContratoVO();
					pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
				}
				pontoConsumoContratoVO.setAbaModalidade(abaModalidades);
				listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
			}

		} else {
			Long idModeloContrato = contratoVO.getIdModeloContrato();
			Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.REGRAS_FATURAMENTO);
			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}
			if (isValidarCamposObrigatorios) {
				controladorContrato.validarCamposObrigatoriosContrato(abaModalidades, modeloContrato, aba, false);
			}
		}
	}

	/**
	 * Popular campos aba modalidades.
	 *
	 */
	private void popularCamposAbaModalidades(HttpServletRequest request, PontoConsumoContratoVO pontoConsumoContratoVO, Model model)
			throws GGASException {

		request.getSession().setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				pontoConsumoContratoVO.getAbaModalidade().get(MODALIDADES));

		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO, pontoConsumoContratoVO.getAbaModalidade().get(MODALIDADES));

		carregarCamposPassoAbaModalidade(request, model);

	}

	/**
	 * Método responsável por adicionar o compromisso de Take Or Pay da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarCompromissoTOPAbaModalidade")
	public String adicionarCompromissoTOPAbaModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String periodicidadeTakeOrPay = contratoVO.getPeriodicidadeTakeOrPay();
		String referenciaQFParadaProgramada = contratoVO.getReferenciaQFParadaProgramada();

		String consumoReferenciaTakeOrPay = contratoVO.getConsumoReferenciaTakeOrPay();
		String margemVariacaoTakeOrPay = contratoVO.getMargemVariacaoTakeOrPay();
		String dataInicioVigencia = contratoVO.getDataInicioVigencia();
		String dataFimVigencia = contratoVO.getDataFimVigencia();
		String precoCobranca = contratoVO.getPrecoCobranca();
		String tipoApuracao = contratoVO.getTipoApuracao();
		String tipoAgrupamentoContrato = contratoVO.getTipoAgrupamentoContrato();
		String consideraParadaProgramada = contratoVO.getConsideraParadaProgramada();
		String consideraFalhaFornecimento = contratoVO.getConsideraFalhaFornecimento();
		String consideraCasoFortuito = contratoVO.getConsideraCasoFortuito();
		String recuperavel = contratoVO.getRecuperavel();
		String percentualNaoRecuperavel = contratoVO.getPercentualNaoRecuperavel();
		String apuracaoParadaProgramada = contratoVO.getApuracaoParadaProgramada();
		String apuracaoCasoFortuito = contratoVO.getApuracaoCasoFortuito();
		String apuracaoFalhaFornecimento = contratoVO.getApuracaoFalhaFornecimento();

		EntidadeConteudo entPeriodicidadeTakeOrPay = null;
		EntidadeConteudo entConsumoReferenciaTakeOrPay = null;
		BigDecimal valorMargemVariacaoTakeOrPay = null;
		BigDecimal valorPercentualNaoRecuperavel = null;
		EntidadeConteudo entidadeReferenciaQFParadaProgramada = null;
		EntidadeConteudo entidadePrecoCobranca = null;
		EntidadeConteudo entidadeTipoApuracao = null;
		EntidadeConteudo entidadeTipoAgrupamentoContrato = null;
		EntidadeConteudo entidadeApuracaoParadaProgramada = null;
		EntidadeConteudo entidadeApuracaoCasoFortuito = null;
		EntidadeConteudo entidadeApuracaoFalhaFornecimento = null;

		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);

		try {

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (listaCompromissosTakeOrPayVO == null) {
				listaCompromissosTakeOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();
			}

			if (StringUtils.isNotEmpty(periodicidadeTakeOrPay) && !periodicidadeTakeOrPay.equals(NAO_SELECIONADO)) {
				entPeriodicidadeTakeOrPay = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_TAKE_OR_PAY, periodicidadeTakeOrPay));
			}
			if (StringUtils.isNotEmpty(referenciaQFParadaProgramada) && !referenciaQFParadaProgramada.equals(NAO_SELECIONADO)) {
				entidadeReferenciaQFParadaProgramada = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(LABEL_REFERENCIA_QF_PARADA_PROGRAMADA, referenciaQFParadaProgramada));
			}

			if (StringUtils.isNotEmpty(consumoReferenciaTakeOrPay) && !consumoReferenciaTakeOrPay.equals(NAO_SELECIONADO)) {
				entConsumoReferenciaTakeOrPay = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.CONSUMO_REFERENCIA_TAKE_OR_PAY, consumoReferenciaTakeOrPay));
			}
			if (StringUtils.isNotEmpty(margemVariacaoTakeOrPay)) {
				valorMargemVariacaoTakeOrPay = Util.converterCampoStringParaValorBigDecimal(LABEL_MARGEM_DE_VARIACAO_TAKE_OR_PAY,
						margemVariacaoTakeOrPay, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}
			if (StringUtils.isNotEmpty(precoCobranca) && !precoCobranca.equals(NAO_SELECIONADO)) {
				entidadePrecoCobranca = controladorEntidadeConteudo
						.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(Contrato.TABELA_PRECO_COBRANCA, precoCobranca));
			}
			if (StringUtils.isNotEmpty(tipoApuracao) && !tipoApuracao.equals(NAO_SELECIONADO)) {
				entidadeTipoApuracao = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, tipoApuracao));
			}
			if (StringUtils.isNotEmpty(apuracaoParadaProgramada) && !apuracaoParadaProgramada.equals(NAO_SELECIONADO)) {
				entidadeApuracaoParadaProgramada = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoParadaProgramada));
			}
			if (StringUtils.isNotEmpty(apuracaoCasoFortuito) && !apuracaoCasoFortuito.equals(NAO_SELECIONADO)) {
				entidadeApuracaoCasoFortuito = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoCasoFortuito));
			}
			if (StringUtils.isNotEmpty(apuracaoFalhaFornecimento) && !apuracaoFalhaFornecimento.equals(NAO_SELECIONADO)) {
				entidadeApuracaoFalhaFornecimento = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoFalhaFornecimento));
			}
			if (StringUtils.isNotEmpty(tipoAgrupamentoContrato) && !tipoAgrupamentoContrato.equals(NAO_SELECIONADO)) {
				entidadeTipoAgrupamentoContrato = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, tipoAgrupamentoContrato));
			}
			if (StringUtils.isNotEmpty(percentualNaoRecuperavel)) {
				valorPercentualNaoRecuperavel =
						Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_NAO_RECUPERAVEL, percentualNaoRecuperavel,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			} else {
				valorPercentualNaoRecuperavel = BigDecimal.ZERO;
			}

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}

			if (entPeriodicidadeTakeOrPay != null && entConsumoReferenciaTakeOrPay != null && valorMargemVariacaoTakeOrPay != null) {

				Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);

				CompromissoTakeOrPayVO compromissoTakeOrPayVO = new CompromissoTakeOrPayVO();
				compromissoTakeOrPayVO.setContrato(contrato);
				compromissoTakeOrPayVO.setPeriodicidadeTakeOrPay(entPeriodicidadeTakeOrPay);
				compromissoTakeOrPayVO.setConsumoReferenciaTakeOrPay(entConsumoReferenciaTakeOrPay);
				compromissoTakeOrPayVO.setMargemVariacaoTakeOrPay(valorMargemVariacaoTakeOrPay);
				compromissoTakeOrPayVO.setReferenciaQFParadaProgramada(entidadeReferenciaQFParadaProgramada);
				compromissoTakeOrPayVO.setPrecoCobranca(entidadePrecoCobranca);
				compromissoTakeOrPayVO.setTipoApuracao(entidadeTipoApuracao);
				compromissoTakeOrPayVO.setTipoAgrupamentoContrato(entidadeTipoAgrupamentoContrato);
				compromissoTakeOrPayVO.setApuracaoParadaProgramada(entidadeApuracaoParadaProgramada);
				compromissoTakeOrPayVO.setApuracaoCasoFortuito(entidadeApuracaoCasoFortuito);
				compromissoTakeOrPayVO.setApuracaoFalhaFornecimento(entidadeApuracaoFalhaFornecimento);
				compromissoTakeOrPayVO.setPercentualNaoRecuperavel(valorPercentualNaoRecuperavel);

				if (StringUtils.isNotEmpty(dataInicioVigencia)) {
					compromissoTakeOrPayVO.setDataInicioVigencia(Util.converterCampoStringParaData(LABEL_DATA_DE_INICIO_DE_VIGENCIA_TO_P,
							dataInicioVigencia, Constantes.FORMATO_DATA_BR));
				}
				if (StringUtils.isNotEmpty(dataFimVigencia)) {
					compromissoTakeOrPayVO.setDataFimVigencia(Util.converterCampoStringParaData(LABEL_DATA_DE_FIM_DE_VIGENCIA_TO_P,
							dataFimVigencia, Constantes.FORMATO_DATA_BR));
				}
				if (StringUtils.isNotEmpty(recuperavel)) {
					compromissoTakeOrPayVO.setRecuperavel(Boolean.valueOf(recuperavel));
				}
				if (StringUtils.isNotEmpty(consideraParadaProgramada)) {
					compromissoTakeOrPayVO.setConsideraParadaProgramada(Boolean.valueOf(consideraParadaProgramada));
				}
				if (StringUtils.isNotEmpty(consideraCasoFortuito)) {
					compromissoTakeOrPayVO.setConsideraCasoFortuito(Boolean.valueOf(consideraCasoFortuito));
				}
				if (StringUtils.isNotEmpty(consideraFalhaFornecimento)) {
					compromissoTakeOrPayVO.setConsideraFalhaFornecimento(Boolean.valueOf(consideraFalhaFornecimento));
				}
				popularFormTakeOrPay(compromissoTakeOrPayVO, contratoVO);

				String[] camposTakeOrPayModalidade = new String[] { PERIODICIDADE_TAKE_OR_PAY, REFERENCIA_QF_PARADA_PROGRAMADA,
						CONSUMO_REFERENCIA_TAKE_OR_PAY, MARGEM_VARIACAO_TAKE_OR_PAY, DATA_INICIO_VIGENCIA, DATA_FIM_VIGENCIA,
						PRECO_COBRANCA, TIPO_APURACAO, CONSIDERA_PARADA_PROGRAMADA, CONSIDERA_PARADA_NAO_PROGRAMADA,
						CONSIDERA_FALHA_FORNECIMENTO, CONSIDERA_CASO_FORTUITO, PERCENTUAL_NAO_RECUPERAVEL, APURACAO_PARADA_PROGRAMADA,
						APURACAO_PARADA_NAO_PROGRAMADA, APURACAO_CASO_FORTUITO, APURACAO_FALHA_FORNECIMENTO };

				controladorContrato.validarCamposObrigatoriosContrato(oMapper.convertValue(contratoVO, Map.class),
						Arrays.asList(camposTakeOrPayModalidade), modeloContrato);

				validarAdicaoNovoCompromisso(compromissoTakeOrPayVO, listaCompromissosTakeOrPayVO);

				listaCompromissosTakeOrPayVO.add(compromissoTakeOrPayVO);
			}

			request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, listaCompromissosTakeOrPayVO);
			model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, listaCompromissosTakeOrPayVO);
			model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
					request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

			contratoVO.setPeriodicidadeTakeOrPay(NAO_SELECIONADO);
			contratoVO.setConsumoReferenciaTakeOrPay(NAO_SELECIONADO);
			contratoVO.setMargemVariacaoTakeOrPay(StringUtils.EMPTY);
			contratoVO.setReferenciaQFParadaProgramada(NAO_SELECIONADO);
			contratoVO.setDataInicioVigencia(StringUtils.EMPTY);
			contratoVO.setDataFimVigencia(StringUtils.EMPTY);
			contratoVO.setPercentualNaoRecuperavel(StringUtils.EMPTY);
			contratoVO.setPrecoCobranca(NAO_SELECIONADO);
			contratoVO.setTipoApuracao(NAO_SELECIONADO);
			contratoVO.setTipoAgrupamentoContrato(NAO_SELECIONADO);
			contratoVO.setApuracaoParadaProgramada(NAO_SELECIONADO);
			contratoVO.setApuracaoParadaNaoProgramada(NAO_SELECIONADO);
			contratoVO.setApuracaoCasoFortuito(NAO_SELECIONADO);
			contratoVO.setApuracaoFalhaFornecimento(NAO_SELECIONADO);

			// Carregar os campos padrao do modelo
			if (modeloContrato != null) {
				this.carregarDadosModelo(contratoVO, modeloContrato, model);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Método responsável por excluir o compromisso de Take Or Pay da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("excluirCompromissoTOPAbaModalidade")
	public String excluirCompromissoTOPAbaModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Integer[] indicePeriodicidadeTakeOrPay = contratoVO.getIndicePeriodicidadeTakeOrPay();

		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);

		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		if (indicePeriodicidadeTakeOrPay != null && indicePeriodicidadeTakeOrPay.length > 0 && listaCompromissosTakeOrPayVO != null
				&& !listaCompromissosTakeOrPayVO.isEmpty()) {
			for (int i = indicePeriodicidadeTakeOrPay.length - 1; i >= 0; i--) {
				((List<CompromissoTakeOrPayVO>) listaCompromissosTakeOrPayVO).remove(indicePeriodicidadeTakeOrPay[i].intValue());

			}
			request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, listaCompromissosTakeOrPayVO);
		}

		model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, listaCompromissosTakeOrPayVO);
		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	// ship or pay

	/**
	 * Método responsável por adicionar o compromisso de Ship Or Pay da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarCompromissoSOPAbaModalidade")
	public String adicionarCompromissoSOPAbaModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String periodicidadeShipOrPay = contratoVO.getPeriodicidadeShipOrPay();
		String consumoReferenciaShipOrPay = contratoVO.getConsumoReferenciaShipOrPay();
		String margemVariacaoShipOrPay = contratoVO.getMargemVariacaoShipOrPay();
		EntidadeConteudo entPeriodicidadeShipOrPay = null;
		EntidadeConteudo entConsumoReferenciaShipOrPay = null;
		BigDecimal valorMargemVariacaoShipOrPay = null;

		Collection<CompromissoTakeOrPayVO> listaCompromissosShipOrPayVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);

		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		if (listaCompromissosShipOrPayVO == null) {
			listaCompromissosShipOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();
		}

		if (StringUtils.isNotEmpty(periodicidadeShipOrPay) && !periodicidadeShipOrPay.equals(NAO_SELECIONADO)) {
			entPeriodicidadeShipOrPay = controladorEntidadeConteudo.obterEntidadeConteudo(
					Util.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_SHIP_OR_PAY, periodicidadeShipOrPay));
		}
		if (StringUtils.isNotEmpty(consumoReferenciaShipOrPay) && !consumoReferenciaShipOrPay.equals(NAO_SELECIONADO)) {
			entConsumoReferenciaShipOrPay = controladorEntidadeConteudo.obterEntidadeConteudo(
					Util.converterCampoStringParaValorLong(Contrato.CONSUMO_REFERENCIA_SHIP_OR_PAY, consumoReferenciaShipOrPay));
		}
		if (StringUtils.isNotEmpty(margemVariacaoShipOrPay)) {
			valorMargemVariacaoShipOrPay = Util.converterCampoStringParaValorBigDecimal(LABEL_MARGEM_DE_VARIACAO_SHIP_OR_PAY,
					margemVariacaoShipOrPay, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
		}

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		controladorContrato.validarCamposObrigatoriosContrato(oMapper.convertValue(contratoVO, Map.class),
				Arrays.asList(new String[] { PERIODICIDADE_SHIP_OR_PAY, CONSUMO_REFERENCIA_SHIP_OR_PAY, MARGEM_VARIACAO_SHIP_OR_PAY }),
				modeloContrato);

		if (entPeriodicidadeShipOrPay != null && entConsumoReferenciaShipOrPay != null && valorMargemVariacaoShipOrPay != null) {
			Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);

			CompromissoTakeOrPayVO compromissoShipOrPayVO = new CompromissoTakeOrPayVO();
			compromissoShipOrPayVO.setPeriodicidadeTakeOrPay(entPeriodicidadeShipOrPay);
			compromissoShipOrPayVO.setConsumoReferenciaTakeOrPay(entConsumoReferenciaShipOrPay);
			compromissoShipOrPayVO.setMargemVariacaoTakeOrPay(valorMargemVariacaoShipOrPay);
			compromissoShipOrPayVO.setContrato(contrato);

			validarAdicaoNovoCompromisso(compromissoShipOrPayVO, listaCompromissosShipOrPayVO);

			if (!listaCompromissosShipOrPayVO.contains(compromissoShipOrPayVO)) {
				listaCompromissosShipOrPayVO.add(compromissoShipOrPayVO);
			}
		}

		request.getSession().setAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, listaCompromissosShipOrPayVO);
		model.addAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, listaCompromissosShipOrPayVO);
		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

		contratoVO.setPeriodicidadeShipOrPay(NAO_SELECIONADO);
		contratoVO.setConsumoReferenciaShipOrPay(NAO_SELECIONADO);
		contratoVO.setMargemVariacaoShipOrPay(StringUtils.EMPTY);

		// Carregar os campos padrao do modelo
		// TODO: definir solução para carregar dados do modelo sem perder os dados do
		// form.
		if (modeloContrato != null) {
			this.carregarDadosModelo(contratoVO, modeloContrato, model);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Método responsável por excluir o compromisso de Ship Or Pay da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirCompromissoSOPAbaModalidade")
	public String excluirCompromissoSOPAbaModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Integer[] indicePeriodicidadeShipOrPay = contratoVO.getIndicePeriodicidadeShipOrPay();

		Collection<CompromissoTakeOrPayVO> listaCompromissosShipOrPayVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);

		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		if (indicePeriodicidadeShipOrPay != null && indicePeriodicidadeShipOrPay.length > 0 && listaCompromissosShipOrPayVO != null
				&& !listaCompromissosShipOrPayVO.isEmpty()) {
			for (int i = indicePeriodicidadeShipOrPay.length - 1; i >= 0; i--) {
				((List<CompromissoTakeOrPayVO>) listaCompromissosShipOrPayVO).remove(indicePeriodicidadeShipOrPay[i].intValue());

			}
			request.getSession().setAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, listaCompromissosShipOrPayVO);
		}

		model.addAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, listaCompromissosShipOrPayVO);
		model.addAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Carregar campos passo aba modalidade.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void carregarCamposPassoAbaModalidade(HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(LISTA_MODALIDADES, controladorContrato.listarContratoModalidade());
		model.addAttribute(LISTA_PERIODICIDADE_TAKE_OR_PAY, controladorEntidadeConteudo.obterListaPeriodicidadeTOP());
		model.addAttribute(LISTA_PERIODICIDADE_SHIP_OR_PAY, controladorEntidadeConteudo.obterListaPeriodicidadeSOP());
		model.addAttribute(LISTA_CONSUMO_REFERENCIAL, controladorEntidadeConteudo.obterListaConsumoReferencial());
		model.addAttribute(LISTA_REFERENCIA_QF_PARADA_PROGRAMADA, controladorEntidadeConteudo.obterListaReferenciaQFParadaProgramada());

		Collection<ContratoPontoConsumoItemFaturamentoVO> listaVencimentoItemFaturaContratoVO =
				(Collection<ContratoPontoConsumoItemFaturamentoVO>) request.getSession().getAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);

		if (listaVencimentoItemFaturaContratoVO != null) {
			model.addAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, listaVencimentoItemFaturaContratoVO);
		}
		model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO, request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO));

		model.addAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO, request.getSession().getAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO));

		model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO,
				request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO));

	}

	/**
	 * Popular contrato dados aba modalidades.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void popularContratoDadosAbaModalidades(ContratoPontoConsumo contratoPontoConsumo, Map<String, Object> abaModalidades,
			HttpServletRequest request) throws GGASException {

		Collection<ModalidadeConsumoFaturamentoVO> listaMcfVO =
				(Collection<ModalidadeConsumoFaturamentoVO>) abaModalidades.get(MODALIDADES);

		if (CollectionUtils.isNotEmpty(listaMcfVO)) {

			for (ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO : listaMcfVO) {

				ContratoPontoConsumoModalidade contratoPontoConsumoModalidade =
						(ContratoPontoConsumoModalidade) controladorContrato.criarContratoPontoConsumoModalidade();
				contratoPontoConsumoModalidade.setUltimaAlteracao(new Date());
				contratoPontoConsumoModalidade.setHabilitado(true);
				contratoPontoConsumoModalidade.setContratoPontoConsumo(contratoPontoConsumo);
				contratoPontoConsumoModalidade.setDadosAuditoria(getDadosAuditoria(request));
				contratoPontoConsumoModalidade.setContratoModalidade(modalidadeConsumoFaturamentoVO.getModalidade());
				contratoPontoConsumoModalidade.setDataFimRetiradaQPNR(modalidadeConsumoFaturamentoVO.getDataFimRetiradaQPNR());
				contratoPontoConsumoModalidade.setDataInicioRetiradaQPNR(modalidadeConsumoFaturamentoVO.getDataInicioRetiradaQPNR());
				contratoPontoConsumoModalidade.setAnosValidadeRetiradaQPNR(modalidadeConsumoFaturamentoVO.getTempoValidadeRetiradaQPNR());
				contratoPontoConsumoModalidade.setPercentualVarSuperiorQDC(modalidadeConsumoFaturamentoVO.getVariacaoSuperiorQDC());
				contratoPontoConsumoModalidade
						.setHoraLimiteProgramacaoDiaria(modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoDiaria());
				contratoPontoConsumoModalidade
						.setHoraLimiteProgIntradiaria(modalidadeConsumoFaturamentoVO.getHoraLimiteProgramacaoIntraDiaria());
				contratoPontoConsumoModalidade.setHoraLimiteAceitacaoDiaria(modalidadeConsumoFaturamentoVO.getHoraLimiteAceitacaoDiaria());
				contratoPontoConsumoModalidade
						.setHoraLimiteAceitIntradiaria(modalidadeConsumoFaturamentoVO.getHoraLimiteAceitIntradiaria());
				contratoPontoConsumoModalidade.setDiasAntecedenciaQDS(modalidadeConsumoFaturamentoVO.getDiasAntecSolicConsumo());

				contratoPontoConsumoModalidade
						.setIndicadorProgramacaoConsumo(modalidadeConsumoFaturamentoVO.getIndicadorProgramacaoConsumo());

				contratoPontoConsumoModalidade.setIndicadorQDSMaiorQDC(modalidadeConsumoFaturamentoVO.getQdsMaiorQDC());

				contratoPontoConsumoModalidade.setIndicadorRetiradaAutomatica(modalidadeConsumoFaturamentoVO.getRecuperacaoAutoTakeOrPay());

				contratoPontoConsumoModalidade
						.setIndicadorConfirmacaoAutomaticaQDS(modalidadeConsumoFaturamentoVO.getConfirmacaoAutomaticaQDS());

				contratoPontoConsumoModalidade.setMesesQDS(modalidadeConsumoFaturamentoVO.getNumMesesSolicConsumo());

				if (modalidadeConsumoFaturamentoVO.getPrazoRevizaoQDC() != null) {
					contratoPontoConsumoModalidade.setPrazoRevisaoQDC(modalidadeConsumoFaturamentoVO.getPrazoRevizaoQDC());
				}

				if (modalidadeConsumoFaturamentoVO.getPercDuranteRetiradaQPNR() != null) {
					contratoPontoConsumoModalidade
							.setPercentualQDCContratoQPNR(modalidadeConsumoFaturamentoVO.getPercDuranteRetiradaQPNR());
				}

				if (modalidadeConsumoFaturamentoVO.getPercMinDuranteRetiradaQPNR() != null) {
					contratoPontoConsumoModalidade
							.setPercentualMinimoQDCContratoQPNR(modalidadeConsumoFaturamentoVO.getPercMinDuranteRetiradaQPNR());
				}

				if (modalidadeConsumoFaturamentoVO.getPercFimRetiradaQPNR() != null) {
					contratoPontoConsumoModalidade.setPercentualQDCFimContratoQPNR(modalidadeConsumoFaturamentoVO.getPercFimRetiradaQPNR());
				}

				if (modalidadeConsumoFaturamentoVO.getVariacaoSuperiorQDC() != null) {
					contratoPontoConsumoModalidade.setPercentualVarSuperiorQDC(modalidadeConsumoFaturamentoVO.getVariacaoSuperiorQDC());
				}

				if (modalidadeConsumoFaturamentoVO.getPercentualQNR() != null) {
					contratoPontoConsumoModalidade.setPercentualQNR(modalidadeConsumoFaturamentoVO.getPercentualQNR());
				}

				popularListasModalidadeConsumoFatura(request, modalidadeConsumoFaturamentoVO, contratoPontoConsumoModalidade);

				contratoPontoConsumo.getListaContratoPontoConsumoModalidade().add(contratoPontoConsumoModalidade);

			}
		}
	}

	/**
	 * Popular listas modalidade consumo fatura.
	 *
	 */
	private void popularListasModalidadeConsumoFatura(HttpServletRequest request,
			ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
			throws GGASException {

		if (modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO() != null) {
			popularListaPenalidadesRetiradaMaiorMenorVO(request, modalidadeConsumoFaturamentoVO, contratoPontoConsumoModalidade);
		}

		if (modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO() != null) {
			popularListaCompromissoTakeOrPayVO(request, modalidadeConsumoFaturamentoVO, contratoPontoConsumoModalidade);
		}

		if (modalidadeConsumoFaturamentoVO.getListaCompromissoShipOrPayVO() != null) {
			popularListaCompromissoShipOrPayVO(request, modalidadeConsumoFaturamentoVO, contratoPontoConsumoModalidade);
		}

		if (modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC() != null) {
			popularListaContratoPontoConsumoModalidadeQDC(modalidadeConsumoFaturamentoVO, contratoPontoConsumoModalidade);
		}

	}

	/**
	 * Popular lista penalidades retirada maior menor VO.
	 *
	 */
	private void popularListaPenalidadesRetiradaMaiorMenorVO(HttpServletRequest request,
			ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
			throws GGASException {
		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = null;
		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadeRetiradaMaiorMenorVO =
				modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO();
		for (PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO : listaPenalidadeRetiradaMaiorMenorVO) {
			contratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();
			contratoPontoConsumoPenalidade.setDadosAuditoria(getDadosAuditoria(request));
			contratoPontoConsumoPenalidade.setHabilitado(true);
			contratoPontoConsumoPenalidade.setUltimaAlteracao(Calendar.getInstance().getTime());

			contratoPontoConsumoPenalidade.setPenalidade(penalidadesRetiradaMaiorMenorVO.getPenalidadeRetiradaMaiorMenor());
			contratoPontoConsumoPenalidade
					.setPeriodicidadePenalidade(penalidadesRetiradaMaiorMenorVO.getEntidadePeriodicidadeRetiradaMaiorMenor());
			contratoPontoConsumoPenalidade.setBaseApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeBaseApuracaoRetiradaMaiorMenor());
			contratoPontoConsumoPenalidade.setTipoApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeTipoApuracaoRetirMaiorMenor());
			contratoPontoConsumoPenalidade
					.setConsumoReferencia(penalidadesRetiradaMaiorMenorVO.getEntidadeConsumoReferenciaRetMaiorMenor());
			contratoPontoConsumoPenalidade.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
			contratoPontoConsumoPenalidade.setDataInicioVigencia(penalidadesRetiradaMaiorMenorVO.getDataInicioVigencia());
			contratoPontoConsumoPenalidade.setDataFimVigencia(penalidadesRetiradaMaiorMenorVO.getDataFimVigencia());
			contratoPontoConsumoPenalidade
					.setValorPercentualCobIntRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualCobIntRetMaiorMenor());
			contratoPontoConsumoPenalidade
					.setValorPercentualCobRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualCobRetMaiorMenor());
			contratoPontoConsumoPenalidade
					.setValorPercentualRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualRetMaiorMenor());
			contratoPontoConsumoPenalidade.setIndicadorImposto(penalidadesRetiradaMaiorMenorVO.getIndicadorImposto());

			contratoPontoConsumoPenalidade.setTipoApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeTipoApuracaoRetirMaiorMenor());

			contratoPontoConsumoPenalidade.setPrecoCobranca(penalidadesRetiradaMaiorMenorVO.getEntidadePrecoCobrancaRetiradaMaiorMenor());

			contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade().add(contratoPontoConsumoPenalidade);
		}
	}

	/**
	 * Popular lista compromisso take or pay VO.
	 *
	 */
	private void popularListaCompromissoTakeOrPayVO(HttpServletRequest request,
			ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
			throws GGASException {
		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = null;
		Collection<CompromissoTakeOrPayVO> listaCompromissoTakeOrPayVO = modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO();
		for (CompromissoTakeOrPayVO compromissoTakeOrPayVO : listaCompromissoTakeOrPayVO) {
			contratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();
			contratoPontoConsumoPenalidade.setConsumoReferencia(compromissoTakeOrPayVO.getConsumoReferenciaTakeOrPay());
			contratoPontoConsumoPenalidade.setDadosAuditoria(getDadosAuditoria(request));
			contratoPontoConsumoPenalidade.setHabilitado(true);
			contratoPontoConsumoPenalidade.setUltimaAlteracao(Calendar.getInstance().getTime());
			if (compromissoTakeOrPayVO.getMargemVariacaoTakeOrPay() != null) {
				contratoPontoConsumoPenalidade.setPercentualMargemVariacao(compromissoTakeOrPayVO.getMargemVariacaoTakeOrPay());
			}

			String valorTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
			Penalidade penalidadeTakeOrPay = controladorContrato.obterPenalidade(Long.parseLong(valorTakeOrPay));

			contratoPontoConsumoPenalidade.setPenalidade(penalidadeTakeOrPay);

			contratoPontoConsumoPenalidade.setPeriodicidadePenalidade(compromissoTakeOrPayVO.getPeriodicidadeTakeOrPay());
			contratoPontoConsumoPenalidade.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
			contratoPontoConsumoPenalidade.setReferenciaQFParadaProgramada(compromissoTakeOrPayVO.getReferenciaQFParadaProgramada());

			contratoPontoConsumoPenalidade.setApuracaoCasoFortuito(compromissoTakeOrPayVO.getApuracaoCasoFortuito());
			contratoPontoConsumoPenalidade.setApuracaoFalhaFornecimento(compromissoTakeOrPayVO.getApuracaoFalhaFornecimento());
			contratoPontoConsumoPenalidade.setApuracaoParadaProgramada(compromissoTakeOrPayVO.getApuracaoParadaProgramada());
			contratoPontoConsumoPenalidade.setConsideraCasoFortuito(compromissoTakeOrPayVO.getConsideraCasoFortuito());
			contratoPontoConsumoPenalidade.setConsideraFalhaFornecimento(compromissoTakeOrPayVO.getConsideraFalhaFornecimento());
			contratoPontoConsumoPenalidade.setConsideraParadaProgramada(compromissoTakeOrPayVO.getConsideraParadaProgramada());
			contratoPontoConsumoPenalidade.setDataFimVigencia(compromissoTakeOrPayVO.getDataFimVigencia());
			contratoPontoConsumoPenalidade.setDataInicioVigencia(compromissoTakeOrPayVO.getDataInicioVigencia());
			contratoPontoConsumoPenalidade.setPercentualNaoRecuperavel(compromissoTakeOrPayVO.getPercentualNaoRecuperavel());
			contratoPontoConsumoPenalidade.setPrecoCobranca(compromissoTakeOrPayVO.getPrecoCobranca());
			contratoPontoConsumoPenalidade.setRecuperavel(compromissoTakeOrPayVO.getRecuperavel());
			contratoPontoConsumoPenalidade.setTipoApuracao(compromissoTakeOrPayVO.getTipoApuracao());

			contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade().add(contratoPontoConsumoPenalidade);
		}
	}

	/**
	 * Popular lista compromisso ship or pay VO.
	 *
	 */
	private void popularListaCompromissoShipOrPayVO(HttpServletRequest request,
			ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
			throws GGASException {

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = null;
		Collection<CompromissoTakeOrPayVO> listaCompromissoShipOrPayVO = modalidadeConsumoFaturamentoVO.getListaCompromissoShipOrPayVO();

		for (CompromissoTakeOrPayVO compromissoShipOrPayVO : listaCompromissoShipOrPayVO) {

			contratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();
			contratoPontoConsumoPenalidade.setConsumoReferencia(compromissoShipOrPayVO.getConsumoReferenciaTakeOrPay());
			contratoPontoConsumoPenalidade.setDadosAuditoria(getDadosAuditoria(request));
			contratoPontoConsumoPenalidade.setHabilitado(true);
			contratoPontoConsumoPenalidade.setUltimaAlteracao(Calendar.getInstance().getTime());

			if (compromissoShipOrPayVO.getMargemVariacaoTakeOrPay() != null) {
				contratoPontoConsumoPenalidade.setPercentualMargemVariacao(compromissoShipOrPayVO.getMargemVariacaoTakeOrPay());
			}

			String valorShipOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
			Penalidade penalidadeShipOrPay = controladorContrato.obterPenalidade(Long.parseLong(valorShipOrPay));

			contratoPontoConsumoPenalidade.setPenalidade(penalidadeShipOrPay);

			contratoPontoConsumoPenalidade.setPeriodicidadePenalidade(compromissoShipOrPayVO.getPeriodicidadeTakeOrPay());
			contratoPontoConsumoPenalidade.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
			contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade().add(contratoPontoConsumoPenalidade);

		}
	}

	/**
	 * Popular lista contrato ponto consumo modalidade QDC.
	 *
	 */
	private void popularListaContratoPontoConsumoModalidadeQDC(ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		Collection<ContratoPontoConsumoModalidadeQDC> listaCPCQDC =
				modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC();

		for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaCPCQDC) {

			ContratoPontoConsumoModalidadeQDC contratoPCModQDCNovo =
					(ContratoPontoConsumoModalidadeQDC) controladorContrato.criarContratoPontoConsumoModalidadeQDC();
			contratoPCModQDCNovo.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidadeQDC.getContratoPontoConsumoModalidade());
			contratoPCModQDCNovo.setDadosAuditoria(contratoPontoConsumoModalidadeQDC.getDadosAuditoria());
			contratoPCModQDCNovo.setHabilitado(contratoPontoConsumoModalidadeQDC.isHabilitado());
			contratoPCModQDCNovo.setDataVigencia(contratoPontoConsumoModalidadeQDC.getDataVigencia());
			contratoPCModQDCNovo.setMedidaVolume(contratoPontoConsumoModalidadeQDC.getMedidaVolume());
			contratoPCModQDCNovo.setUltimaAlteracao(contratoPontoConsumoModalidadeQDC.getUltimaAlteracao());
			contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().add(contratoPCModQDCNovo);
		}
	}

	/**
	 * Obter lista imovel ponto consumo vo.
	 *
	 */
	@SuppressWarnings("unchecked")
	private Collection<ImovelPontoConsumoVO> obterListaImovelPontoConsumoVO(HttpServletRequest request) {

		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO =
				(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO);
		if (listaImovelPontoConsumoVO == null) {
			listaImovelPontoConsumoVO = new HashSet<ImovelPontoConsumoVO>();
		}
		return listaImovelPontoConsumoVO;
	}

	/**
	 * Método responsável por adicionar um relacionamento entre contrato e cliente.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarContratoCliente")
	public String adicionarContratoCliente(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ATRIBUTO_ABA_ID, ABA_RESPONSABILIDADE);
		model.addAttribute("contratoVO", contratoVO);
		Integer indexLista = contratoVO.getIndexLista();
		boolean isAcaoAdicionarAbaResponsabilidade = false;
		Collection<ContratoCliente> listaContratoCliente =
				(Collection<ContratoCliente>) request.getSession().getAttribute(LISTA_CONTRATO_CLIENTE);

		try {

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (listaContratoCliente == null) {
				listaContratoCliente = new ArrayList<ContratoCliente>();
			}

			if ((indexLista == null) || (indexLista < 0)) {
				ContratoCliente contratoCliente = (ContratoCliente) controladorContrato.criarContratoCliente();
				this.popularContratoCliente(contratoCliente, contratoVO, request);
				controladorContrato.validarRelacaoResponsabilidade(listaContratoCliente, contratoCliente);
				Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
				controladorContrato.validarDatasAbaResponsabilidade(contratoCliente, contrato);
				listaContratoCliente.add(contratoCliente);
			} else {
				this.alterarContratoCliente(indexLista, contratoVO, listaContratoCliente, request);
			}
			isAcaoAdicionarAbaResponsabilidade = true;
			request.getSession().setAttribute(LISTA_CONTRATO_CLIENTE, listaContratoCliente);
			model.addAttribute(LISTA_CONTRATO_CLIENTE, exibirListaResponsabilidades(listaContratoCliente));
			model.addAttribute("isAcaoAdicionarAbaResponsabilidade", isAcaoAdicionarAbaResponsabilidade);
			this.limparDadosAbaResponsabilidades(contratoVO);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			isAcaoAdicionarAbaResponsabilidade = false;
		}

		model.addAttribute(ALTERANDO_PONTO_CONSUMO, true);
		model.addAttribute("idCliente", contratoVO.getIdCliente());

		// TODO carregar dados do modelo.

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Popular contrato cliente.
	 *
	 */
	private void popularContratoCliente(ContratoCliente contratoCliente, ContratoVO contratoVO, HttpServletRequest request)
			throws GGASException {

		Long idCliente = contratoVO.getIdCliente();
		String relacaoInicio = contratoVO.getDataInicioRelacao();
		String responsabilidade = contratoVO.getTipoResponsabilidade();
		String relacaoFim = contratoVO.getDataFimRelacao();

		if (idCliente != null && idCliente > 0) {
			Cliente cliente = controladorCliente.obterCliente(idCliente);
			contratoCliente.setCliente(cliente);
		}
		if (StringUtils.isNotEmpty(relacaoInicio)) {
			contratoCliente.setRelacaoInicio(Util.converterCampoStringParaData(ContratoCliente.CONTRATO_CLIENTE_RELACAO_INCICIO,
					relacaoInicio, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(relacaoFim)) {
			contratoCliente.setRelacaoFim(Util.converterCampoStringParaData(ContratoCliente.CONTRATO_CLIENTE_RELACAO_FIM, relacaoFim,
					Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(responsabilidade)) {
			Long idResponsabilidade = Util.converterCampoStringParaValorLong(LABEL_TIPO_DE_RESPONSABILIDADE, responsabilidade);
			if ((idResponsabilidade != null) && (idResponsabilidade > 0)) {
				EntidadeConteudo entidadeConteudoResponsabilidade = controladorEntidadeConteudo.obterEntidadeConteudo(idResponsabilidade);
				contratoCliente.setResponsabilidade(entidadeConteudoResponsabilidade);
			}
		}

		contratoCliente.setHabilitado(true);
		contratoCliente.setUltimaAlteracao(Calendar.getInstance().getTime());
		contratoCliente.setDadosAuditoria(this.getDadosAuditoria(request));

		controladorContrato.validarDadosResponsabilidade(contratoCliente);
	}

	/**
	 * Alterar contrato cliente.
	 *
	 */
	private void alterarContratoCliente(Integer indexLista, ContratoVO contratoVO, Collection<ContratoCliente> listaContratoCliente,
			HttpServletRequest request) throws GGASException {

		if ((listaContratoCliente != null) && (indexLista != null)) {
			ContratoCliente contratoClienteAlterado = ((List<ContratoCliente>) listaContratoCliente).get(indexLista);
			if (contratoClienteAlterado != null) {
				this.popularContratoCliente(contratoClienteAlterado, contratoVO, request);
			}
			controladorContrato.validarRelacaoResponsabilidade(listaContratoCliente, contratoClienteAlterado);
			Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
			controladorContrato.validarDatasAbaResponsabilidade(contratoClienteAlterado, contrato);
		}

	}

	/**
	 * Exibir lista responsabilidades.
	 *
	 */
	private List<Map<String, Object>> exibirListaResponsabilidades(Collection<ContratoCliente> listaContratoCliente) throws GGASException {

		List<Map<String, Object>> responsabilidades = new ArrayList<Map<String, Object>>();

		if (listaContratoCliente != null && !listaContratoCliente.isEmpty()) {
			for (ContratoCliente contratoCliente : listaContratoCliente) {
				if (contratoCliente.isHabilitado()) {
					Map<String, Object> dados = new HashMap<String, Object>();

					Cliente cliente = controladorCliente.obterCliente(contratoCliente.getCliente().getChavePrimaria(), ENDERECOS);
					dados.put(ID_CLIENTE, cliente.getChavePrimaria());
					dados.put(NOME, cliente.getNome());

					if (StringUtils.isNotEmpty(cliente.getCpf())) {
						dados.put(DOCUMENTO_FORMATADO, cliente.getCpfFormatado());
					} else {
						if (StringUtils.isNotEmpty(cliente.getCnpj())) {
							dados.put(DOCUMENTO_FORMATADO, cliente.getCnpjFormatado());
						} else {
							dados.put(DOCUMENTO_FORMATADO, StringUtils.EMPTY);
						}
					}
					if (cliente.getEnderecoPrincipal() != null) {
						dados.put(ENDERECO_FORMATADO, cliente.getEnderecoPrincipal().getEnderecoFormatado());
					}
					dados.put(EMAIL, cliente.getEmailPrincipal());
					if (contratoCliente.getRelacaoInicio() != null) {
						dados.put(RELACAO_INICIO,
								Util.converterDataParaStringSemHora(contratoCliente.getRelacaoInicio(), Constantes.FORMATO_DATA_BR));
					}
					if (contratoCliente.getRelacaoFim() != null) {
						dados.put(FIM_RELACIONAMENTO,
								Util.converterDataParaStringSemHora(contratoCliente.getRelacaoFim(), Constantes.FORMATO_DATA_BR));
					}
					if (contratoCliente.getResponsabilidade() != null) {
						dados.put(ID_RESPONSABILIDADE, contratoCliente.getResponsabilidade().getChavePrimaria());
						dados.put(RESPONSABILIDADE, contratoCliente.getResponsabilidade().getDescricao());
					}
					responsabilidades.add(dados);
				}
			}
		}

		return responsabilidades;
	}

	/**
	 * Aplicar dados aba responsabilidade.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void aplicarDadosAbaResponsabilidade(ContratoVO contratoVO, Long chavePontoConsumo,
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO, HttpServletRequest request, boolean isValidarCamposObrigatorios)
			throws GGASException, InvocationTargetException, IllegalAccessException {

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		Collection<ContratoCliente> listaContratoCliente =
				(Collection<ContratoCliente>) request.getSession().getAttribute(LISTA_CONTRATO_CLIENTE);

		Map<String, Object> abaResponsabilidades = new HashMap<String, Object>();
		if (listaContratoCliente != null && !listaContratoCliente.isEmpty()) {
			// Para efeito de validação de campos
			// obrigatórios
			abaResponsabilidades.put(CLIENTE_RESPONSAVEL, NUMERO_UM);
			abaResponsabilidades.put(DATA_INICIO_RELACAO, NUMERO_UM);
			abaResponsabilidades.put(TIPO_RESPONSABILIDADE, NUMERO_UM);
			abaResponsabilidades.put(DATA_FIM_RELACAO, NUMERO_UM);
		}

		ModeloContrato modeloContrato = null;
		if (idModeloContrato != null && idModeloContrato > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
		}

		Aba aba = controladorContrato.obterAbaContratoPorCodigo(Aba.RESPONSABILIDADES);

		boolean preenchimentoConcluido = false;
		if (isValidarCamposObrigatorios) {
			controladorContrato.validarCamposObrigatoriosContrato(abaResponsabilidades, modeloContrato, aba, false);
			preenchimentoConcluido = true;
		}

		Collection<ContratoCliente> listaContratoClienteCopia;
		PontoConsumoContratoVO pontoConsumoContratoVO;

		abaResponsabilidades = new HashMap<String, Object>();
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			pontoConsumoContratoVO = listaPontoConsumoContratoVO.get(chavePontoConsumo);
			if (pontoConsumoContratoVO == null) {
				pontoConsumoContratoVO = new PontoConsumoContratoVO();
				pontoConsumoContratoVO.setChavePrimaria(chavePontoConsumo);
			}

			if (listaContratoCliente != null) {
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavePontoConsumo);
				listaContratoClienteCopia = new ArrayList<ContratoCliente>();

				for (ContratoCliente contratoCliente : listaContratoCliente) {
					ContratoCliente copiaContratoCliente = (ContratoCliente) controladorContrato.criarContratoCliente();

					copiarContratoCliente(copiaContratoCliente, contratoCliente);

					// NOVO CONTRATO CLIENTE
					copiaContratoCliente.setPontoConsumo(pontoConsumo);
					listaContratoClienteCopia.add(copiaContratoCliente);
				}
				abaResponsabilidades.put(LISTA_CONTRATO_CLIENTE, listaContratoClienteCopia);
			}
			pontoConsumoContratoVO.setAbaResponsabilidades(abaResponsabilidades);
			pontoConsumoContratoVO.setPreenchimentoConcluido(preenchimentoConcluido);
			listaPontoConsumoContratoVO.put(chavePontoConsumo, pontoConsumoContratoVO);
		}
	}

	/**
	 * @param copiaContratoCliente
	 * @param contratoCliente
	 * @return Object
	 */
	private Object copiarContratoCliente(ContratoCliente copiaContratoCliente, ContratoCliente contratoCliente) {
		return Util.copyProperties(copiaContratoCliente, contratoCliente);
	}

	/**
	 * Remover contrato cliente.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerContratoCliente")
	public String removerContratoCliente(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Integer indexLista = contratoVO.getIndexLista();
		String relacaoFim = contratoVO.getDataFimRelacao();

		// Valida o token para garantir que não
		// aconteça um duplo submit.

		Collection<ContratoCliente> listaContratoCliente =
				(Collection<ContratoCliente>) request.getSession().getAttribute(LISTA_CONTRATO_CLIENTE);

		try {

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (listaContratoCliente != null && indexLista != null) {
				ContratoCliente contratoCliente = (ContratoCliente) ((List) listaContratoCliente).get(indexLista.intValue());
				if ((relacaoFim != null) && (StringUtils.isNotEmpty(relacaoFim))) {
					popularContratoCliente(contratoCliente, contratoVO, request);
					Contrato contrato = (Contrato) request.getSession().getAttribute(CONTRATO);
					controladorContrato.validarDatasAbaResponsabilidade(contratoCliente, contrato);
					contratoCliente.setHabilitado(false);
				} else {
					((List) listaContratoCliente).remove(indexLista.intValue());
				}
			}

			model.addAttribute(LISTA_CONTRATO_CLIENTE, exibirListaResponsabilidades(listaContratoCliente));
			request.getSession().setAttribute(LISTA_CONTRATO_CLIENTE, listaContratoCliente);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		model.addAttribute(ATRIBUTO_ABA_ID, ABA_RESPONSABILIDADE);

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Limpar dados contrato sessao.
	 *
	 */
	private void limparDadosContratoSessao(HttpServletRequest request) {

		request.getSession().setAttribute(CONTRATO, null);
		request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, null);
		request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO, null);
		request.getSession().setAttribute(LISTA_CONTRATO_CLIENTE, null);
		request.getSession().setAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO, null);
		request.getSession().setAttribute(LISTA_ANEXOS, null);
	}

	/**
	 * Método responsável por exibir a tela de aditar contrato passo 1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirAditamentoContrato")
	public String exibirAditamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Long chavePrimaria = null;
		
		if(contratoVO.getContrato() != null && contratoVO.getContrato().getChavePrimaria() > 0) {
			chavePrimaria = contratoVO.getContrato().getChavePrimaria();
			contratoVO.setFluxoAlteracao(Boolean.TRUE);
		} else {
			chavePrimaria = contratoVO.getChavePrimaria();
		}

		Boolean isAcaoLimparCampo = Boolean.valueOf(request.getParameter("isAcaoLimparCampo"));
		model.addAttribute("isAcaoLimparCampo", isAcaoLimparCampo);

		boolean isFluxoDetalhamento = contratoVO.getFluxoDetalhamento();
		boolean isFluxoAlteracao = contratoVO.getFluxoAlteracao();
		boolean isFluxoAditamento = contratoVO.getFluxoAditamento();

		Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);

		if (indicadorAtualizacaoCadastralImediata != null) {
			isFluxoDetalhamento = false;
			isFluxoAlteracao = true;
		}

		try {

			Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria);

			if (!isFluxoDetalhamento && contrato != null && controladorIntegracao.integracaoPrecedenteContratoAtiva()
					&& !controladorIntegracao.validarIntegracaoContrato(contrato.getChavePrimaria(), contrato.getChavePrimariaPai())) {
				throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
			}

			// Se não for o fluxo de aditamento não
			// validar essa regra
			if (isFluxoAditamento) {
				controladorContrato.validarAditamentoContrato(chavePrimaria);
			}

			if (isFluxoAlteracao) {

				Long idAguardandoAprovacao = Long.valueOf(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_AGUARDANDO_APROVACAO));

				if (contrato.getSituacao().getChavePrimaria() != SituacaoContrato.EM_CRIACAO
						&& contrato.getSituacao().getChavePrimaria() != idAguardandoAprovacao) {
					controladorContrato.validarAditamentoContrato(chavePrimaria);
				}

			} else {
				contratoVO.setFluxoAditamento(true);
			}

			limparDadosContratoSessao(request);

			contrato = (Contrato) controladorContrato.obter(chavePrimaria, MODELO_CONTRATO, PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO,
					LISTA_CONTRATO_CLIENTE, LISTA_CONTRATO_QDC, LISTA_CONTRATO_PENALIDADE);

			ModeloContrato modeloContrato =
					(ModeloContrato) controladorModeloContrato.obter(contrato.getModeloContrato().getChavePrimaria(), ATRIBUTOS, ABAS);

			if (request.getSession().getAttribute(SITUACAO_ESCOLHIDA) != null) {
				request.getSession().removeAttribute(SITUACAO_ESCOLHIDA);
			}

			if (contrato.getDataAditivo() != null) {
				model.addAttribute(DATA_ADITIVO,
						Util.converterDataParaStringSemHora(contrato.getDataAditivo(), Constantes.FORMATO_DATA_BR));
			}

			this.carregarCampos(contratoVO, modeloContrato, request, false, model);

			if (contrato.getSituacao().getChavePrimaria() == ATIVO && isFluxoAlteracao) {
				model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoAlterado());
			} else if (contrato.getSituacao().getChavePrimaria() == ATIVO && !isFluxoAlteracao) {
				model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoAditado());
			} else {
				model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
			}

			Long idArrecadadorContratoConvenio = controladorContrato.consultarIdArrecadadorContratoConvenio(contrato.getChavePrimaria());
			Banco banco = controladorContrato.consultarBancoContrato(contrato.getChavePrimaria());
			contrato.setBanco(banco);

			this.popularFormContrato(contrato, contratoVO, request, model);
			this.setarPontoConsumoContratoNoRequest(contratoVO, request, contrato.getListaContratoPontoConsumo(), model);
			this.popularSessaoListaContratoPontoConsumo(request, contrato, true);

			if (contrato.getNumeroAditivo() != null && contrato.getNumeroAditivo() != 0) {
				contratoVO.setNumeroAditivo(String.valueOf(contrato.getNumeroAditivo()));
			} else {
				contratoVO.setNumeroAditivo(String.valueOf(1));
			}

			String numeroProposta = contratoVO.getNumeroProposta();

			if (StringUtils.isNotEmpty(numeroProposta)) {
				model.addAttribute(LISTA_VERSOES_PROPOSTA, controladorProposta.listarPropostasAprovadasPorNumero(numeroProposta));
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
			filtro.put(CHAVE_PRIMARIA_PRINCIPAL, contrato.getChavePrimariaPrincipal());
			Collection<Contrato> listaContratosPrincipalEComplementar = controladorContrato.consultarContratoPrincipalEComplementar(filtro);
			model.addAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR, listaContratosPrincipalEComplementar);

			model.addAttribute(AGENCIA, contrato.getAgencia());

			if (contrato.getBanco() != null) {
				model.addAttribute(BANCO, contrato.getBanco().getChavePrimaria());
			}

			model.addAttribute(ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO, idArrecadadorContratoConvenio);
			model.addAttribute(CONTA_CORRENTE, contrato.getContaCorrente());

			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			request.getSession().setAttribute(CONTRATO, contrato);
			
			view = reexibirInclusaoContrato(contratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
			view = pesquisarContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Método auxiliar responsável por exibir a tela de aditar e alterar contrato passo 2.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void validarContratoPontoConsumo(ContratoVO contratoVO, HttpServletRequest request, boolean validarPonto, Model model)
			throws GGASException {

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String numeroProposta = contratoVO.getNumeroProposta();
		Long chavePrimaria = contratoVO.getChavePrimaria();
		String dataAssinatura = contratoVO.getDataAssinatura();
		String idArrecadadorConvenio = contratoVO.getArrecadadorConvenio();
		Long idArrecadadorConvenioL = Util.converterCampoStringParaValorLong(Contrato.ARRECADADOR_CONVENIO, idArrecadadorConvenio);
		String idSituacao = contratoVO.getSituacaoContrato();
		Long idSituacaoContrato = Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, idSituacao);

		Long[] chavesPontoConsumo = contratoVO.getChavesPrimariasPontoConsumo();

		controladorContrato.validarSelecaoImovelPontoConsumo(chavesPontoConsumo);
		controladorContrato.validarPontosSelecionados(chavePrimaria, chavesPontoConsumo);
		controladorContrato.validarSituacaoDataAssinaturaArrecadadador(dataAssinatura, idArrecadadorConvenioL, idSituacaoContrato);

		Contrato contrato = (Contrato) controladorContrato.criar();

		request.getSession().setAttribute(CONTRATO, contrato);

		this.popularContratoPasso1(contratoVO, contrato, request, true, false, false, true, model);

		this.carregarCamposPasso2(request, contratoVO, model);

		controladorProposta.validarPropostasContrato(numeroProposta, contrato.isPropostaAprovada());

		Boolean todosPontosConsumoDadosInformados = (Boolean) model.asMap().get(TODOS_PONTOS_CONSUMO_DADOS_INFORMADOS);
		String pontoConsumoNaoInformado = (String) model.asMap().get(PONTO_CONSUMO_NAO_INFORMADO);

		if (validarPonto) {
			controladorContrato.validarTodosPontosConsumoDadosInformados(todosPontosConsumoDadosInformados, pontoConsumoNaoInformado);
		}

		controladorContrato.validarDatasContrato(contrato);
		if (contrato.getChavePrimariaPrincipal() != null && contrato.getChavePrimariaPrincipal() > 0) {
			controladorContrato.validarDatasContratoComplementar(contrato.getChavePrimariaPrincipal(), contrato);
			controladorContrato.validarDadosContratoComplementar(contrato);
		}

		String dataSomaQDC = null;
		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO =
				(Map<Long, PontoConsumoContratoVO>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO);

		if (listaPontoConsumoContratoVO != null) {

			List<ModalidadeConsumoFaturamentoVO> listaModalidades =
					this.recuperarModalidadeConsumoFaturamentoVO(listaPontoConsumoContratoVO);

			List<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDCOrdenada = new ArrayList<ContratoPontoConsumoModalidadeQDC>();
			for (ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamento : listaModalidades) {
				Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC =
						modalidadeConsumoFaturamento.getListaContratoPontoConsumoModalidadeQDC();
				listaModalidadeQDCOrdenada.addAll(listaModalidadeQDC);
			}

			if (!listaModalidadeQDCOrdenada.isEmpty()) {
				dataSomaQDC = controladorContrato.validarAlteracaoQDCContrato(listaModalidadeQDCOrdenada, contrato.getListaContratoQDC(),
						contrato.getDataVencimentoObrigacoes());
			}

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
				this.carregarCampos(contratoVO, modeloContrato, request, model);
			}

			model.addAttribute(MODELO_CONTRATO, modeloContrato);
			contratoVO.setQDCContrato(null);

			if (dataSomaQDC != null) {
				mensagemAlerta(model, ControladorContrato.ERRO_NEGOCIO_SOMA_QDC_MODALIDADE_MAIOR_QDC_CONTRATO, dataSomaQDC);
			}
		}
	}

	/**
	 * Método auxiliar responsável por recuperar as 'Modalidades' dos 'Pontos de Consumo'.
	 *
	 * @param listaPontoConsumoContratoVO the lista ponto consumo contrato vo
	 * @return List<ModalidadeConsumoFaturamentoVO >
	 */
	@SuppressWarnings("unchecked")
	private List<ModalidadeConsumoFaturamentoVO> recuperarModalidadeConsumoFaturamentoVO(
			Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO) {

		List<ModalidadeConsumoFaturamentoVO> listaModalidades = new ArrayList<ModalidadeConsumoFaturamentoVO>();

		if (listaPontoConsumoContratoVO != null) {

			for (Iterator<Entry<Long, PontoConsumoContratoVO>> it = listaPontoConsumoContratoVO.entrySet().iterator(); it.hasNext();) {

				Entry<Long, PontoConsumoContratoVO> entry = it.next();
				Collection<ModalidadeConsumoFaturamentoVO> listaModalidadesAdicionadas =
						(Collection<ModalidadeConsumoFaturamentoVO>) entry.getValue().getAbaModalidade().get(MODALIDADES);
				if (listaModalidadesAdicionadas != null) {
					listaModalidades.addAll(listaModalidadesAdicionadas);
				}
			}

		}

		return listaModalidades;
	}

	// /**
	// * Método responsável por limpar o preenchimento do ponto de consumo.
	// *
	// * @param request O objeto request
	// * @return void
	// */

	@SuppressWarnings("unchecked")
	private void verificarLimpezaPreenchimentoPontoConsumo(HttpServletRequest request) {

		Boolean limparPreenchimentoPontoConsumo = Boolean.valueOf(request.getParameter(LIMPAR_PREENCHIMENTO_PONTO_CONSUMO));

		if (!limparPreenchimentoPontoConsumo) {
			return;
		}

		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO =
				(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO);

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		for (ImovelPontoConsumoVO imovelPontoConsumoVO : Util.emptyIfNull(listaImovelPontoConsumoVO)) {

			for (PontoConsumo pontoConsumo : Util.emptyIfNull(imovelPontoConsumoVO.getListaPontoConsumo())) {

				if (listaPontoConsumoContratoVO != null && listaPontoConsumoContratoVO.containsKey(pontoConsumo.getChavePrimaria())) {

					PontoConsumoContratoVO pontoConsumoVO = listaPontoConsumoContratoVO.get(pontoConsumo.getChavePrimaria());
					pontoConsumoVO.setPreenchimentoConcluido(false);

				}
			}

		}

	}

	/**
	 * Método responsável por exibir a tela de aditar contrato passo 2.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirAditamentoContratoPontoConsumo")
	public String exibirAditamentoContratoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "exibirAditamentoContratoPontoConsumo";

		try {

			this.validarContratoPontoConsumo(contratoVO, request, Boolean.FALSE, model);
			this.exibirMsgAlertaAditamentoAlteracaoContrato(request, model);

			List<ImovelPontoConsumoVO> listaImovelPontoConsumoSelecionados = new ArrayList<ImovelPontoConsumoVO>(
					(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute(LISTA_IMOVEL_PONTO_CONSUMO_SELECIONADO_VO));

			if (!listaImovelPontoConsumoSelecionados.isEmpty()) {
				List<PontoConsumo> listaPontoConsumo =
						new ArrayList<PontoConsumo>(listaImovelPontoConsumoSelecionados.get(0).getListaPontoConsumo());
				if (!listaPontoConsumo.isEmpty()) {
					contratoVO.setIdPontoConsumo(listaPontoConsumo.get(0).getChavePrimaria());
					view = popularCamposContratoPontoConsumo(contratoVO, result, request, model);
				}
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAditamentoContrato(contratoVO, result, request, model);
		}

		return view;

	}

	/**
	 * Método responsável por exibir a tela de aditar contrato passo 1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("reexibirAditamentoContrato")
	public String reexibirAditamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long idModeloContrato = contratoVO.getIdModeloContrato();

		try {

			ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);

			this.carregarCampos(contratoVO, modeloContrato, request, false, model);
			model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
			Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

			if (contratoSessao != null && StringUtils.isNotEmpty(contratoSessao.getNumeroFormatado())) {
				contratoVO.setNumeroFormatado(contratoSessao.getNumeroFormatado());
			}

			if (contratoSessao != null) {
				this.popularFormContrato(contratoSessao, contratoVO, request, model);
			}

			String numeroProposta = contratoVO.getNumeroProposta();
			if (StringUtils.isNotEmpty(numeroProposta)) {
				model.addAttribute(LISTA_VERSOES_PROPOSTA, controladorProposta.listarPropostasAprovadasPorNumero(numeroProposta));
			}

			model.addAttribute(MODELO_CONTRATO, modeloContrato);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);

		request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
		request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);
		request.getSession().removeAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);

		return reexibirInclusaoContrato(contratoVO, result, request, model);
	}

	/**
	 * Setar ponto consumo contrato no request.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void setarPontoConsumoContratoNoRequest(ContratoVO contratoVO, HttpServletRequest request,
			Collection<ContratoPontoConsumo> listaContratoPontoConsumo, Model model) throws GGASException {

		List<Long> chavesPrimariasPontoConsumo = new ArrayList<Long>();
		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {
			Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = new HashSet<ImovelPontoConsumoVO>();

			Map<Imovel, Set<PontoConsumo>> imovelPontoConsumo = new HashMap<Imovel, Set<PontoConsumo>>();

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

				PontoConsumo pontoConsumo =
						(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);

				Set<PontoConsumo> listaPontoConsumo = imovelPontoConsumo.get(pontoConsumo.getImovel());

				if (listaPontoConsumo == null) {
					listaPontoConsumo = new HashSet<PontoConsumo>();
				}

				listaPontoConsumo.add(pontoConsumo);
				imovelPontoConsumo.put(pontoConsumo.getImovel(), listaPontoConsumo);
				chavesPrimariasPontoConsumo.add(pontoConsumo.getChavePrimaria());
			}

			Entry<?, ?> entry = null;
			ImovelPontoConsumoVO imovelPontoConsumoVOSelecionado = null;

			for (Iterator<?> it = imovelPontoConsumo.entrySet().iterator(); it.hasNext();) {

				entry = (Entry<?, ?>) it.next();
				imovelPontoConsumoVOSelecionado = new ImovelPontoConsumoVO();
				imovelPontoConsumoVOSelecionado.setChavePrimaria(((Imovel) entry.getKey()).getChavePrimaria());
				imovelPontoConsumoVOSelecionado.setNome(((Imovel) entry.getKey()).getNome());
				imovelPontoConsumoVOSelecionado.getListaPontoConsumo().addAll((HashSet<PontoConsumo>) entry.getValue());
				listaImovelPontoConsumoVO.add(imovelPontoConsumoVOSelecionado);
			}

			request.getSession().setAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);
			model.addAttribute(LISTA_IMOVEL_PONTO_CONSUMO_VO, listaImovelPontoConsumoVO);

			contratoVO.setChavesPrimariasPontoConsumo(chavesPrimariasPontoConsumo.toArray(new Long[chavesPrimariasPontoConsumo.size()]));
		}
	}

	/**
	 * Método auxiliar responsável por aditar o contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	public void aditar(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		Contrato contratoAditadoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);
		popularContratoPasso2(contratoVO, request, contratoAditadoSessao);
		contratoAditadoSessao.setDadosAuditoria(this.getDadosAuditoria(request));

		Contrato contrato = controladorContrato.obterContratoListasCarregadas(contratoAditadoSessao.getChavePrimaria(), Boolean.TRUE);
		Cliente cliente = contrato.getClienteAssinatura();
		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = contrato.getListaContratoPontoConsumo();

		Long chavePrimaria = contratoVO.getChavePrimaria();
		Contrato contratoAlterado = (Contrato) controladorContrato.obter(chavePrimaria, PROPOSTA);
		contratoAlterado.setDadosAuditoria(this.getDadosAuditoria(request));

		if ((contratoAlterado.getProposta() != null) && (contratoAditadoSessao.getProposta() != null)) {
			controladorProposta.validarPropostasContrato(contratoAditadoSessao.getProposta().getNumeroProposta(),
					contratoAditadoSessao.isPropostaAprovada());
		}

		// Correção para não duplicar registros
		// nas lista do contrato
		if (contrato.getSituacao().getChavePrimaria() == SituacaoContrato.EM_CRIACAO) {
			povoarChavesListasContrstoEmCriacao(contratoAditadoSessao, contrato);
		}

		Long idContrato = controladorContrato.aditarContrato(contratoAditadoSessao, contratoAlterado);

		alterarClienteImovel(cliente, listaContratoPontoConsumo, contratoAditadoSessao);

		contratoVO.setChavePrimaria(idContrato);

		limparFormularioCliente(contratoVO);

		this.limparDadosSessao(request);

		mensagemSucesso(model, Constantes.CONTRATO_ADITADO_SUCESSO, contratoAditadoSessao.getNumeroFormatado());

	}

	/**
	 * Método auxiliar responsável por alterar o contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	public void alterar(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);
		popularContratoPasso2(contratoVO, request, contratoSessao);
		contratoSessao.setDadosAuditoria(this.getDadosAuditoria(request));

		Contrato contrato = controladorContrato.obterContratoListasCarregadas(contratoSessao.getChavePrimaria(), Boolean.TRUE);
		Cliente cliente = contrato.getClienteAssinatura();
		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = contrato.getListaContratoPontoConsumo();

		Long chavePrimaria = contratoVO.getChavePrimaria();
		Contrato contratoAlterado = (Contrato) controladorContrato.obter(chavePrimaria, PROPOSTA);
		contratoAlterado.setDadosAuditoria(this.getDadosAuditoria(request));

		if ((contratoAlterado.getProposta() != null) && (contratoSessao.getProposta() != null)) {
			controladorProposta.validarPropostasContrato(contratoSessao.getProposta().getNumeroProposta(),
					contratoSessao.isPropostaAprovada());
		}

		contratoSessao.setNumeroAditivo(0);

		// Correção para não duplicar registros
		// nas lista do contrato
		if (contrato.getSituacao().getChavePrimaria() == SituacaoContrato.EM_CRIACAO) {
			povoarChavesListasContrstoEmCriacao(contratoSessao, contrato);
		}

		Long idContrato = controladorContrato.atualizarContrato(contratoSessao, contratoAlterado);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CHAVE_PRIMARIA, chavePrimaria);

		Collection<Contrato> listaContratosPrincipalEComplementar = controladorContrato.consultarContratoPrincipalEComplementar(filtro);
		for (Iterator<Contrato> iterator = listaContratosPrincipalEComplementar.iterator(); iterator.hasNext();) {
			Contrato contratoFilho = iterator.next();
			if (contratoFilho.getChavePrimariaPrincipal() != null && contratoFilho.getChavePrimariaPrincipal().equals(chavePrimaria)) {
				contratoFilho.setChavePrimariaPrincipal(idContrato);
				controladorContrato.atualizar(contratoFilho);
			}
		}

		alterarClienteImovel(cliente, listaContratoPontoConsumo, contratoSessao);

		Map<Long, Object> dadosPontoConsumoVencimento =
				(Map<Long, Object>) request.getSession().getAttribute(DADOS_PONTO_CONSUMO_VENCIMENTO);

		if (dadosPontoConsumoVencimento != null && !dadosPontoConsumoVencimento.isEmpty()) {
			controladorPontoConsumo.removerPontoConsumoVencimento(dadosPontoConsumoVencimento, idContrato);
			controladorPontoConsumo.inserirPoncoConsumoVencimento(dadosPontoConsumoVencimento, idContrato);
		}

		contratoVO.setChavePrimaria(idContrato);

		limparFormularioCliente(contratoVO);

		this.limparDadosSessao(request);

		mensagemSucesso(model, Constantes.CONTRATO_ALTERADO_SUCESSO, contratoSessao.getNumeroFormatado());
	}

	/**
	 * Limpar formulario cliente.
	 *
	 */
	private void limparFormularioCliente(ContratoVO contratoVO) {

		contratoVO.setIdCliente(null);
		contratoVO.setNomeCliente(StringUtils.EMPTY);
		contratoVO.setDocumentoCliente(StringUtils.EMPTY);
		contratoVO.setEmailCliente(StringUtils.EMPTY);
		contratoVO.setEnderecoCliente(StringUtils.EMPTY);
		contratoVO.setNumeroContrato(StringUtils.EMPTY);
		contratoVO.setSituacaoContrato(StringUtils.EMPTY);
	}

	/**
	 * Povoa as chaves primárias dos filhos cadastrados do contrato Em Criação que está sendo modificado.
	 *
	 * @param emCriacao the em criacao
	 * @param emBanco Versão salva o contrato em criação com suas listas carregadas.
	 */
	private void povoarChavesListasContrstoEmCriacao(Contrato emCriacao, Contrato emBanco) {

		// povoando contrato ponto consumo
		if (emCriacao.getListaContratoPontoConsumo() != null && !emCriacao.getListaContratoPontoConsumo().isEmpty()
				&& emBanco.getListaContratoPontoConsumo() != null && !emBanco.getListaContratoPontoConsumo().isEmpty()) {
			for (ContratoPontoConsumo contratoPontoConsumo : emCriacao.getListaContratoPontoConsumo()) {

				ContratoPontoConsumo tempBanco = null;
				for (ContratoPontoConsumo cpcBanco : emBanco.getListaContratoPontoConsumo()) {
					if (contratoPontoConsumo.equalsNegocio(cpcBanco)) {
						contratoPontoConsumo.setChavePrimaria(cpcBanco.getChavePrimaria());
						tempBanco = cpcBanco;
						break;
					}
				}

				if (tempBanco != null) {

					// listaContratoPontoConsumoModalidade
					// ContratoPontoConsumoModalidade
					if (contratoPontoConsumo.getListaContratoPontoConsumoModalidade() != null
							&& !contratoPontoConsumo.getListaContratoPontoConsumoModalidade().isEmpty()
							&& tempBanco.getListaContratoPontoConsumoModalidade() != null
							&& !tempBanco.getListaContratoPontoConsumoModalidade().isEmpty()) {
						for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumo
								.getListaContratoPontoConsumoModalidade()) {

							ContratoPontoConsumoModalidade tempModalidadeBanco = null;
							for (ContratoPontoConsumoModalidade cpcmBanco : tempBanco.getListaContratoPontoConsumoModalidade()) {
								if (contratoPontoConsumoModalidade.equalsNegocio(cpcmBanco)) {
									contratoPontoConsumoModalidade.setChavePrimaria(cpcmBanco.getChavePrimaria());
									tempModalidadeBanco = cpcmBanco;
									break;
								}
							}

							if (tempModalidadeBanco != null) {

								// listaContratoPontoConsumoModalidadeQDC
								// ContratoPontoConsumoModalidadeQDC
								if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC() != null
										&& !contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().isEmpty()
										&& tempModalidadeBanco.getListaContratoPontoConsumoModalidadeQDC() != null
										&& !tempModalidadeBanco.getListaContratoPontoConsumoModalidadeQDC().isEmpty()) {
									for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : contratoPontoConsumoModalidade
											.getListaContratoPontoConsumoModalidadeQDC()) {
										for (ContratoPontoConsumoModalidadeQDC cpcmQDCBanco : tempModalidadeBanco
												.getListaContratoPontoConsumoModalidadeQDC()) {
											if (contratoPontoConsumoModalidadeQDC.equalsNegocio(cpcmQDCBanco)) {
												contratoPontoConsumoModalidadeQDC.setChavePrimaria(cpcmQDCBanco.getChavePrimaria());
												break;
											}
										}
									}
								}

								// listaContratoPontoConsumoPenalidade
								// ContratoPontoConsumoPenalidade
								if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade() != null
										&& !contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade().isEmpty()
										&& tempModalidadeBanco.getListaContratoPontoConsumoPenalidade() != null
										&& !tempModalidadeBanco.getListaContratoPontoConsumoPenalidade().isEmpty()) {
									for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : contratoPontoConsumoModalidade
											.getListaContratoPontoConsumoPenalidade()) {
										for (ContratoPontoConsumoPenalidade cpcpBanco : tempModalidadeBanco
												.getListaContratoPontoConsumoPenalidade()) {
											if (contratoPontoConsumoPenalidade.equalsNegocio(cpcpBanco)) {
												contratoPontoConsumoPenalidade.setChavePrimaria(cpcpBanco.getChavePrimaria());
												break;
											}
										}
									}
								}
							}

						}
					}

					// listaContratoPontoConsumoItemFaturamento
					// ContratoPontoConsumoItemFaturamento
					if (contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento() != null
							&& !contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().isEmpty()
							&& tempBanco.getListaContratoPontoConsumoItemFaturamento() != null
							&& !tempBanco.getListaContratoPontoConsumoItemFaturamento().isEmpty()) {
						for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : contratoPontoConsumo
								.getListaContratoPontoConsumoItemFaturamento()) {
							for (ContratoPontoConsumoItemFaturamento cpcifBanco : tempBanco.getListaContratoPontoConsumoItemFaturamento()) {
								if (contratoPontoConsumoItemFaturamento.equalsNegocio(cpcifBanco)) {
									contratoPontoConsumoItemFaturamento.setChavePrimaria(cpcifBanco.getChavePrimaria());
									break;
								}
							}
						}
					}

					// listaContratoPontoConsumoPCSAmostragem
					// ContratoPontoConsumoPCSAmostragem
					if (contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem() != null
							&& !contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().isEmpty()
							&& tempBanco.getListaContratoPontoConsumoPCSAmostragem() != null
							&& !tempBanco.getListaContratoPontoConsumoPCSAmostragem().isEmpty()) {
						for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : contratoPontoConsumo
								.getListaContratoPontoConsumoPCSAmostragem()) {
							for (ContratoPontoConsumoPCSAmostragem cpcPCSaBanco : tempBanco.getListaContratoPontoConsumoPCSAmostragem()) {
								if (contratoPontoConsumoPCSAmostragem.equalsNegocio(cpcPCSaBanco)) {
									contratoPontoConsumoPCSAmostragem.setChavePrimaria(cpcPCSaBanco.getChavePrimaria());
									break;
								}
							}
						}
					}

					// listaContratoPontoConsumoPCSIntervalo
					// ContratoPontoConsumoPCSIntervalo
					if (contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo() != null
							&& !contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().isEmpty()
							&& tempBanco.getListaContratoPontoConsumoPCSIntervalo() != null
							&& !tempBanco.getListaContratoPontoConsumoPCSIntervalo().isEmpty()) {
						for (ContratoPontoConsumoPCSIntervalo contratoPontoConsumoPCSIntervalo : contratoPontoConsumo
								.getListaContratoPontoConsumoPCSIntervalo()) {
							for (ContratoPontoConsumoPCSIntervalo cpcPCSiBanco : tempBanco.getListaContratoPontoConsumoPCSIntervalo()) {
								if (contratoPontoConsumoPCSIntervalo.equalsNegocio(cpcPCSiBanco)) {
									contratoPontoConsumoPCSIntervalo.setChavePrimaria(cpcPCSiBanco.getChavePrimaria());
									break;
								}
							}
						}
					}

				}
			}
		}

		// povoando contrato qdc
		if (emCriacao.getListaContratoQDC() != null && !emCriacao.getListaContratoQDC().isEmpty() && emBanco.getListaContratoQDC() != null
				&& !emBanco.getListaContratoQDC().isEmpty()) {
			for (ContratoQDC contratoQDC : emCriacao.getListaContratoQDC()) {
				if (contratoQDC.getContrato() == null) {
					contratoQDC.setContrato(emCriacao);
				}

				for (ContratoQDC cQDCBanco : emBanco.getListaContratoQDC()) {
					if (contratoQDC.equalsNegocio(cQDCBanco)) {
						contratoQDC.setChavePrimaria(cQDCBanco.getChavePrimaria());
						break;
					}
				}
			}
		}
	}

	/**
	 * Método responsável por aditar o contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("aditarContrato")
	public String aditarContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long chavePrimaria = contratoVO.getChavePrimaria();
		Contrato contratoAlterado = (Contrato) controladorContrato.obter(chavePrimaria, PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO);
		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

		aditar(contratoVO, request, model);

		List<PontoConsumo> listaPontoConsumoRemovidos = listaPontosConsumoRemovidos(contratoAlterado, contratoSessao);

		model.addAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS, listaPontoConsumoRemovidos);

		model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);

		return pesquisarContrato(contratoVO, result, request, model);

	}

	/**
	 * Método responsável por alterar o clienteImovel apenas se o cliente e/ou imóvel forem alterados no contrato.
	 *
	 * @param cliente {@link Cliente}
	 * @param listaContratoPontoConsumo {@link Collection}
	 * @param contratoSessao {@link Contrato}
	 * @throws GGASException {@link GGASException}
	 */
	public void alterarClienteImovel(Cliente cliente, Collection<ContratoPontoConsumo> listaContratoPontoConsumo, Contrato contratoSessao)
			throws GGASException {

		Collection<ClienteImovel> listaClienteImovel = new ArrayList<ClienteImovel>();
		Collection<Long> listaIdImovel = new ArrayList<Long>();
		Collection<Long> listaIdImovelAux = new ArrayList<Long>();
		Collection<ContratoPontoConsumo> listaContratoPontoConsumoRemovido = new ArrayList<ContratoPontoConsumo>();
		Collection<ContratoPontoConsumo> listaContratoPontoConsumoRemovidoAux = new ArrayList<ContratoPontoConsumo>();

		// preenche a lista listaIdImovelAux com
		// os Ids dos Imóveis de acordo com os
		// pontos de consumo
		// selecionados pelo usuário
		for (ContratoPontoConsumo contratoPontoConsumo : contratoSessao.getListaContratoPontoConsumo()) {
			listaIdImovelAux.add(contratoPontoConsumo.getPontoConsumo().getImovel().getChavePrimaria());
		}

		// preenche a lista
		// listaContratoPontoConsumoRemovido com
		// os pontos de consumo
		// removidos pelo usuário de acordo com a
		// listaContratoPontoConsumo antes
		// do contrato ser atualizado
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			if (!listaIdImovelAux.contains(contratoPontoConsumo.getPontoConsumo().getImovel().getChavePrimaria())) {
				listaContratoPontoConsumoRemovido.add(contratoPontoConsumo);
			}
		}

		String parametroSistemaResponCli =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO);
		TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel =
				controladorImovel.buscarTipoRelacionamentoClienteImovel(Long.parseLong(parametroSistemaResponCli));

		if (!cliente.equals(contratoSessao.getClienteAssinatura()) || !listaContratoPontoConsumoRemovido.isEmpty()) {

			ParametroSistema parametroSistemaMotivoFimRel = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_ENCERRAR_CONTRATO);
			MotivoFimRelacionamentoClienteImovel motivoFimRelacionamentoClienteImovel =
					controladorImovel.obterMotivoFimRelacionamentoClienteImovel(Long.parseLong(parametroSistemaMotivoFimRel.getValor()));

			if (!listaContratoPontoConsumoRemovido.isEmpty()) {
				listaContratoPontoConsumoRemovidoAux.addAll(listaContratoPontoConsumoRemovido);
			} else {
				listaContratoPontoConsumoRemovidoAux.addAll(listaContratoPontoConsumo);
			}

			// atualizar os registros antigos de
			// clienteImovel
			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumoRemovidoAux) {

				PontoConsumo pontoConsumo =
						(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);
				ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(),
						cliente.getChavePrimaria(), Long.parseLong(parametroSistemaResponCli));

				if (clienteImovel != null && listaIdImovel != null
						&& !listaIdImovel.contains(pontoConsumo.getImovel().getChavePrimaria())) {

					if (contratoSessao.getDataAditivo() != null) {
						clienteImovel.setRelacaoFim(contratoSessao.getDataAditivo());
					} else {
						clienteImovel.setRelacaoFim(Calendar.getInstance().getTime());
					}

					clienteImovel.setMotivoFimRelacionamentoClienteImovel(motivoFimRelacionamentoClienteImovel);
					controladorClienteImovel.atualizar(clienteImovel);

					listaClienteImovel.add(clienteImovel);
					listaIdImovel.add(pontoConsumo.getImovel().getChavePrimaria());
				}
			}
		}

		listaClienteImovel.clear();
		listaIdImovel.clear();

		// inserir os registros novos de
		// clienteImovel
		for (ContratoPontoConsumo contratoPontoConsumo : contratoSessao.getListaContratoPontoConsumo()) {

			PontoConsumo pontoConsumo =
					(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);
			ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(),
					contratoSessao.getClienteAssinatura().getChavePrimaria(), Long.valueOf(parametroSistemaResponCli));

			if (clienteImovel == null && listaIdImovel != null && !listaIdImovel.contains(pontoConsumo.getImovel().getChavePrimaria())) {

				ClienteImovel clienteImovelNovo = (ClienteImovel) controladorImovel.criarClienteImovel();

				if (contratoSessao.getDataAditivo() != null) {
					clienteImovelNovo.setRelacaoInicio(contratoSessao.getDataAditivo());
				} else {
					clienteImovelNovo.setRelacaoInicio(Calendar.getInstance().getTime());
				}

				clienteImovelNovo.setCliente(contratoSessao.getClienteAssinatura());
				clienteImovelNovo.setImovel(pontoConsumo.getImovel());
				clienteImovelNovo.setTipoRelacionamentoClienteImovel(tipoRelacionamentoClienteImovel);

				controladorClienteImovel.inserir(clienteImovelNovo);

				listaClienteImovel.add(clienteImovelNovo);
				listaIdImovel.add(pontoConsumo.getImovel().getChavePrimaria());
			}
		}
	}

	/**
	 * Método responsável por alterar o contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarContrato")
	public String alterarContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;

		Long chavePrimaria = contratoVO.getChavePrimaria();

		try {

			Contrato contratoAlterado = (Contrato) controladorContrato.obter(chavePrimaria, PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO);
			Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

			alterar(contratoVO, request, model);

			List<PontoConsumo> listaPontoConsumoRemovidos = listaPontosConsumoRemovidos(contratoAlterado, contratoSessao);
			model.addAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS, listaPontoConsumoRemovidos);

			model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);

			view = pesquisarContrato(contratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;

	}

	/**
	 * Método responsável por aditar o contrato na primeira tela do contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 */
	@RequestMapping("exibirSalvarAditamentoPontoConsumo")
	public String exibirSalvarAditamentoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		String view = null;
		Long chavePrimaria = contratoVO.getChavePrimaria();

		try {

			validarContratoPontoConsumo(contratoVO, request, Boolean.TRUE, model);
			aditarContrato(contratoVO, result, request, model);

			Collection<PontoConsumo> listaPontoConsumoRemovidos = (Collection) request.getAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS);

			if (listaPontoConsumoRemovidos == null) {
				view = exibirPesquisaContrato(contratoVO, request, model);
				contratoVO.setFluxoAditamento(false);

				if (model.asMap().containsKey("mensagemTipo") && ("mensagemSucesso").equals(model.asMap().get("mensagemTipo"))) {
					model.addAttribute("aditamentoContratoSalvoSucesso", true);
				}

			} else {
				contratoVO.setChavePrimaria(chavePrimaria);
				model.addAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS, listaPontoConsumoRemovidos);
				view = exibirPontoConsumoContrato(contratoVO, result, request, model);
			}
			
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession().getAttribute("urlRetornoAtualizacaoCastral");
				view = "redirect:" + urlRetornoAtualizacaoCastral;
			}			

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = reexibirAditamentoContrato(contratoVO, result, request, model);
		}

		return view;

	}

	/**
	 * Lista pontos consumo removidos.
	 *
	 */
	private List<PontoConsumo> listaPontosConsumoRemovidos(Contrato contratoAlterado, Contrato contratoSessao) {

		List<PontoConsumo> listaPontoConsumoBanco = new ArrayList<PontoConsumo>();
		for (ContratoPontoConsumo contratoPontoConsumo : contratoAlterado.getListaContratoPontoConsumo()) {
			listaPontoConsumoBanco
					.add(controladorPontoConsumo.obterPontoConsumo(contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
		}
		List<PontoConsumo> listaPontoConsumoSessao = new ArrayList<PontoConsumo>();
		for (ContratoPontoConsumo contratoPontoConsumo : contratoSessao.getListaContratoPontoConsumo()) {
			listaPontoConsumoSessao
					.add(controladorPontoConsumo.obterPontoConsumo(contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
		}

		listaPontoConsumoBanco.removeAll(listaPontoConsumoSessao);

		return listaPontoConsumoBanco;
	}

	/**
	 * Método responsável por alterar o contrato na primeira tela do contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 */
	@RequestMapping("exibirSalvarAlteracaoPontoConsumo")
	public String exibirSalvarAlteracaoPontoConsumo(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		String view = null;
		Long chavePrimaria = contratoVO.getChavePrimaria();

		try {

			validarContratoPontoConsumo(contratoVO, request, Boolean.TRUE, model);
			alterarContrato(contratoVO, result, request, model);
			Collection<PontoConsumo> listaPontoConsumoRemovidos = (Collection) model.asMap().get(LISTA_PONTO_CONSUMO_REMOVIDOS);

			if (listaPontoConsumoRemovidos.isEmpty()) {
				if (model.asMap().containsKey("mensagemTipo") && ("mensagemSucesso").equals(model.asMap().get("mensagemTipo"))) {
					model.addAttribute("alteracaoContratoSalvoSucesso", true);
				}
				contratoVO.setFluxoAlteracao(false);
				view = "exibirPesquisaContrato";
			} else {
				contratoVO.setChavePrimaria(chavePrimaria);
				model.addAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS, listaPontoConsumoRemovidos);
				view = exibirPontoConsumoContrato(contratoVO, result, request, model);
			}
			
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession().getAttribute("urlRetornoAtualizacaoCastral");
				view = "redirect:" + urlRetornoAtualizacaoCastral;
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = reexibirInclusaoContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Popular sessao lista contrato ponto consumo.
	 *
	 */
	private void popularSessaoListaContratoPontoConsumo(HttpServletRequest request, Contrato contrato, boolean isAditamento)
			throws GGASException {

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = new HashMap<Long, PontoConsumoContratoVO>();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
				controladorContrato.listarContratoPontoConsumo(contrato.getChavePrimaria());

		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

				PontoConsumoContratoVO pontoConsumoContratoVO = new PontoConsumoContratoVO();

				if (isAditamento) {
					pontoConsumoContratoVO.setPreenchimentoConcluido(true);
				}

				pontoConsumoContratoVO.setChavePrimaria(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());

				popularDadosAbaPrincipal(contratoPontoConsumo, pontoConsumoContratoVO);
				popularDadosAbaDadosTecnicos(contratoPontoConsumo, pontoConsumoContratoVO);
				popularDadosAbaConsumo(contratoPontoConsumo, pontoConsumoContratoVO, request);
				popularDadosAbaFaturamento(contratoPontoConsumo, pontoConsumoContratoVO);
				popularDadosAbaModalidades(contratoPontoConsumo, pontoConsumoContratoVO);
				popularDadosAbaResponsabilidade(contratoPontoConsumo, pontoConsumoContratoVO, contrato);

				listaPontoConsumoContratoVO.put(pontoConsumoContratoVO.getChavePrimaria(), pontoConsumoContratoVO);
			}
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);
	}

	/**
	 * Popular dados aba principal.
	 *
	 */
	private void popularDadosAbaPrincipal(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO) {

		Map<String, Object> abaPrincipal = new HashMap<String, Object>();

		if (contratoPontoConsumo.getDataInicioTeste() != null) {
			abaPrincipal.put(PERIODO_TESTE_DATA_INICIO,
					Util.converterDataParaStringSemHora(contratoPontoConsumo.getDataInicioTeste(), Constantes.FORMATO_DATA_BR));
		}
		if (contratoPontoConsumo.getDataFimTeste() != null) {
			abaPrincipal.put(PERIODO_TESTE_DATE_FIM,
					Util.converterDataParaStringSemHora(contratoPontoConsumo.getDataFimTeste(), Constantes.FORMATO_DATA_BR));
		}
		if (contratoPontoConsumo.getPrazoConsumoTeste() != null) {
			abaPrincipal.put(PRAZO_TESTE, contratoPontoConsumo.getPrazoConsumoTeste().toString());
		}
		if (contratoPontoConsumo.getMedidaConsumoTeste() != null) {
			abaPrincipal.put(VOLUME_TESTE, Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaConsumoTeste(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getCodigoDDDFaxOperacional() != null) {
			abaPrincipal.put(FAX_DDD, contratoPontoConsumo.getCodigoDDDFaxOperacional().toString());
		}
		if (contratoPontoConsumo.getNumeroFaxOperacional() != null) {
			abaPrincipal.put(FAX_NUMERO, contratoPontoConsumo.getNumeroFaxOperacional().toString());
		}
		if (contratoPontoConsumo.getEmailOperacional() != null) {
			abaPrincipal.put(EMAIL, contratoPontoConsumo.getEmailOperacional());
		}
		if (contratoPontoConsumo.getDataInicioGarantiaConversao() != null) {
			abaPrincipal.put(INICIO_GARANTIA_CONVERSAO,
					Util.converterDataParaStringSemHora(contratoPontoConsumo.getDataInicioGarantiaConversao(), Constantes.FORMATO_DATA_BR));
		}
		if (contratoPontoConsumo.getDiaGarantiaConversao() != null) {
			abaPrincipal.put(NUMERO_DIAS_GARANTIA, contratoPontoConsumo.getDiaGarantiaConversao().toString());
		}
		pontoConsumoContratoVO.setAbaPrincipal(abaPrincipal);
	}

	/**
	 * Popular dados aba dados tecnicos.
	 *
	 */
	private void popularDadosAbaDadosTecnicos(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO)
			throws GGASException {

		Map<String, Object> abaDadosTecnicos = new HashMap<String, Object>();

		if (contratoPontoConsumo.getMedidaVazaoInstantanea() != null) {
			abaDadosTecnicos.put(VAZAO_INSTANTANEA, Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaVazaoInstantanea(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadeVazaoInstantanea() != null) {
			abaDadosTecnicos.put(UNIDADE_VAZAO_INSTAN,
					String.valueOf(contratoPontoConsumo.getUnidadeVazaoInstantanea().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() != null) {
			abaDadosTecnicos.put(VAZAO_INSTANTANEA_MINIMA, Util.converterCampoValorParaString(
					contratoPontoConsumo.getMedidaVazaoMinimaInstantanea(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea() != null) {
			abaDadosTecnicos.put(UNIDADE_VAZAO_INSTAN_MINIMA,
					String.valueOf(contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
			abaDadosTecnicos.put(VAZAO_INSTANTANEA_MAXIMA, Util.converterCampoValorParaString(
					contratoPontoConsumo.getMedidaVazaoMaximaInstantanea(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() != null) {
			abaDadosTecnicos.put(UNIDADE_VAZAO_INSTAN_MAXIMA,
					String.valueOf(contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaPressao() != null && contratoPontoConsumo.getUnidadePressao() != null) {
			FaixaPressaoFornecimento faixa =
					controladorFaixaPressaoFornecimento.obterFaixaPressaoFornecimento(contratoPontoConsumo.getPontoConsumo().getSegmento(),
							contratoPontoConsumo.getMedidaPressao(), contratoPontoConsumo.getUnidadePressao());
			if (faixa != null) {
				abaDadosTecnicos.put(FAIXA_PRESSAO_FORNECIMENTO, String.valueOf(faixa.getChavePrimaria()));
			}
		}
		if (contratoPontoConsumo.getMedidaPressaoColetada() != null) {
			abaDadosTecnicos.put(PRESSAO_COLETADA, Util.converterCampoValorParaString(contratoPontoConsumo.getMedidaPressaoColetada(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadePressaoColetada() != null) {
			abaDadosTecnicos.put(UNIDADE_PRESSAO_COLETADA,
					String.valueOf(contratoPontoConsumo.getUnidadePressaoColetada().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaPressaoMinima() != null) {
			abaDadosTecnicos.put(PRESSAO_MINIMA_FORNECIMENTO, Util.converterCampoValorParaString(
					contratoPontoConsumo.getMedidaPressaoMinima(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadePressaoMinima() != null) {
			abaDadosTecnicos.put(UNIDADE_PRESSAO_MINIMA_FORNEC,
					String.valueOf(contratoPontoConsumo.getUnidadePressaoMinima().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaPressaoMaxima() != null) {
			abaDadosTecnicos.put(PRESSAO_MAXIMA_FORNECIMENTO, Util.converterCampoValorParaString(
					contratoPontoConsumo.getMedidaPressaoMaxima(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadePressaoMaxima() != null) {
			abaDadosTecnicos.put(UNIDADE_PRESSAO_MAXIMA_FORNEC,
					String.valueOf(contratoPontoConsumo.getUnidadePressaoMaxima().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaPressaoLimite() != null) {
			abaDadosTecnicos.put(PRESSAO_LIMITE_FORNECIMENTO, Util.converterCampoValorParaString(
					contratoPontoConsumo.getMedidaPressaoLimite(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		if (contratoPontoConsumo.getUnidadePressaoLimite() != null) {
			abaDadosTecnicos.put(UNIDADE_PRESSAO_LIMITE_FORNEC,
					String.valueOf(contratoPontoConsumo.getUnidadePressaoLimite().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getQuantidadeAnosParadaCliente() != null) {
			abaDadosTecnicos.put(NUM_ANOS_CTRL_PARADA_CLIENTE, contratoPontoConsumo.getQuantidadeAnosParadaCliente().toString());
		}
		if (contratoPontoConsumo.getQuantidadeTotalParadaCliente() != null) {
			abaDadosTecnicos.put(MAX_TOTAL_PARADAS_CLIENTE, contratoPontoConsumo.getQuantidadeTotalParadaCliente().toString());
		}
		if (contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente() != null) {
			abaDadosTecnicos.put(MAX_ANUAL_PARADAS_CLIENTE, contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente().toString());
		}
		if (contratoPontoConsumo.getDiasAntecedentesParadaCliente() != null) {
			abaDadosTecnicos.put(NUM_DIAS_PROGR_PARADA_CLIENTE, contratoPontoConsumo.getDiasAntecedentesParadaCliente().toString());
		}
		if (contratoPontoConsumo.getDiasConsecutivosParadaCliente() != null) {
			abaDadosTecnicos.put(NUM_DIAS_CONSEC_PARADA_CLIENTE, contratoPontoConsumo.getDiasConsecutivosParadaCliente().toString());
		}
		if (contratoPontoConsumo.getQuantidadeAnosParadaCDL() != null) {
			abaDadosTecnicos.put(NUM_ANOS_CTRL_PARADA_CDL, contratoPontoConsumo.getQuantidadeAnosParadaCDL().toString());
		}
		if (contratoPontoConsumo.getQuantidadeTotalParadaCDL() != null) {
			abaDadosTecnicos.put(MAX_TOTAL_PARADAS_CDL, contratoPontoConsumo.getQuantidadeTotalParadaCDL().toString());
		}
		if (contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL() != null) {
			abaDadosTecnicos.put(MAX_ANUAL_PARADAS_CDL, contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL().toString());
		}
		if (contratoPontoConsumo.getDiasAntecedentesParadaCDL() != null) {
			abaDadosTecnicos.put(NUM_DIAS_PROGR_PARADA_CDL, contratoPontoConsumo.getDiasAntecedentesParadaCDL().toString());
		}
		if (contratoPontoConsumo.getDiasConsecutivosParadaCDL() != null) {
			abaDadosTecnicos.put(NUM_DIAS_CONSEC_PARADA_CDL, contratoPontoConsumo.getDiasConsecutivosParadaCDL().toString());
		}
		

		pontoConsumoContratoVO.setAbaDadosTecnicos(abaDadosTecnicos);
	}

	/**
	 * Popular dados aba consumo.
	 *
	 */
	private void popularDadosAbaConsumo(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO,
			HttpServletRequest request) throws GGASException {

		Map<String, Object> abaConsumo = new HashMap<String, Object>();
		Collection<EntidadeConteudo> listaLocalAmostragem = controladorContrato.consultarLocalAmostragemPCSPorCPC(contratoPontoConsumo);

		if (listaLocalAmostragem != null && !listaLocalAmostragem.isEmpty()) {
			Long[] chavesLocalAmostragem = Util.collectionParaArrayChavesPrimarias(listaLocalAmostragem);
			abaConsumo.put(LOCAL_AMOSTRAGEM_PCS, Util.arrayParaString(chavesLocalAmostragem, Constantes.STRING_VIRGULA));
		}

		Collection<IntervaloPCS> listaIntervaloPCS = controladorContrato.consultarIntervaloPCSPorCPC(contratoPontoConsumo);

		if (listaIntervaloPCS != null && !listaIntervaloPCS.isEmpty()) {
			Long[] chavesIntervaloPCS = Util.collectionParaArrayChavesPrimarias(listaIntervaloPCS);
			abaConsumo.put(INTERVALO_RECUPERACAO_PCS, Util.arrayParaString(chavesIntervaloPCS, Constantes.STRING_VIRGULA));
		}

		Integer tamanhoReducao = controladorContrato.obterTamanhoIntervaloPCSPorCPC(contratoPontoConsumo);
		if (tamanhoReducao != null) {
			abaConsumo.put(TAM_REDUCAO_RECUPERACAO_PCS, String.valueOf(tamanhoReducao));
		}

		if (contratoPontoConsumo.getNumeroHoraInicial() != null) {
			abaConsumo.put(HORA_INICIAL_DIA, String.valueOf(contratoPontoConsumo.getNumeroHoraInicial()));
		}
		if (contratoPontoConsumo.getRegimeConsumo() != null) {
			abaConsumo.put(REGIME_CONSUMO, String.valueOf(contratoPontoConsumo.getRegimeConsumo().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() != null
				&& contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria() != null) {
			// TODO colocar rótulo no properties e
			// rever o getLocale().
			abaConsumo.put(FORNECIMENTO_MAXIMO_DIARIO, Util.converterCampoValorDecimalParaString(LABEL_FORNECIMENTO_MAXIMO_DIARIO,
					contratoPontoConsumo.getMedidaFornecimentoMaxDiaria(), request.getLocale(), DUAS_CASAS_DECIMAIS));
			abaConsumo.put(UNIDADE_FORNEC_MAXIMO_DIARIO,
					String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaFornecimentoMinDiaria() != null
				&& contratoPontoConsumo.getUnidadeFornecimentoMinDiaria() != null) {
			// TODO colocar rótulo no properties e
			// rever o getLocale().
			abaConsumo.put(FORNECIMENTO_MINIMO_DIARIO, Util.converterCampoValorDecimalParaString(LABEL_FORNECIMENTO_MINIMO_DIARIO,
					contratoPontoConsumo.getMedidaFornecimentoMinDiaria(), request.getLocale(), DUAS_CASAS_DECIMAIS));
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_DIARIO,
					String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMinDiaria().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaFornecimentoMinMensal() != null
				&& contratoPontoConsumo.getUnidadeFornecimentoMinMensal() != null) {
			// TODO colocar rótulo no properties e
			// rever o getLocale().
			abaConsumo.put(FORNECIMENTO_MINIMO_MENSAL, Util.converterCampoValorDecimalParaString(LABEL_FORNECIMENTO_MINIMO_MENSAL,
					contratoPontoConsumo.getMedidaFornecimentoMinMensal(), request.getLocale(), DUAS_CASAS_DECIMAIS));
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_MENSAL,
					String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMinMensal().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getMedidaFornecimentoMinAnual() != null && contratoPontoConsumo.getUnidadeFornecimentoMinAnual() != null) {
			// TODO colocar rótulo no properties e
			// rever o getLocale().
			abaConsumo.put(FORNECIMENTO_MINIMO_ANUAL, Util.converterCampoValorDecimalParaString(LABEL_FORNECIMENTO_MINIMO_ANUAL,
					contratoPontoConsumo.getMedidaFornecimentoMinAnual(), request.getLocale(), DUAS_CASAS_DECIMAIS));
			abaConsumo.put(UNIDADE_FORNEC_MINIMO_ANUAL,
					String.valueOf(contratoPontoConsumo.getUnidadeFornecimentoMinAnual().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getNumeroFatorCorrecao() != null) {
			abaConsumo.put(FATOR_UNICO_CORRECAO, Util.converterCampoValorDecimalParaString(ContratoPontoConsumo.FATOR_UNICO_CORRECAO,
					contratoPontoConsumo.getNumeroFatorCorrecao(), Constantes.LOCALE_PADRAO, Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL));
		}
		if (contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura() != null) {
			abaConsumo.put(CONSUMO_FAT_FALHA_MEDICAO,
					String.valueOf(contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura().getChavePrimaria()));
		}

		pontoConsumoContratoVO.setAbaConsumo(abaConsumo);
	}

	/**
	 * Popular dados aba faturamento.
	 *
	 */
	private void popularDadosAbaFaturamento(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO)
			throws GGASException {

		Map<String, Object> abaFaturamento = new HashMap<String, Object>();

		if (contratoPontoConsumo.getCep() != null) {
			abaFaturamento.put(END_FIS_ENT_FAT_CEP, contratoPontoConsumo.getCep().getCep());
			abaFaturamento.put(CHAVE_CEP, String.valueOf(contratoPontoConsumo.getCep().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getNumeroImovel() != null) {
			abaFaturamento.put(END_FIS_ENT_FAT_NUMERO, contratoPontoConsumo.getNumeroImovel().toString());
		}
		if (contratoPontoConsumo.getComplementoEndereco() != null) {
			abaFaturamento.put(END_FIS_ENT_FAT_COMPLEMENTO, contratoPontoConsumo.getComplementoEndereco());
		}
		if (contratoPontoConsumo.getEmailEntrega() != null) {
			abaFaturamento.put(END_FIS_ENT_FAT_EMAIL, contratoPontoConsumo.getEmailEntrega());
		}
		if (contratoPontoConsumo.getContratoCompra() != null) {
			abaFaturamento.put(CONTRATO_COMPRA, String.valueOf(contratoPontoConsumo.getContratoCompra().getChavePrimaria()));
		}
		if (contratoPontoConsumo.getPeriodicidade() != null) {
			abaFaturamento.put(FAT_PERIODICIDADE, String.valueOf(contratoPontoConsumo.getPeriodicidade().getChavePrimaria()));
		}

		abaFaturamento.put(NOTA_FISCAL_ELETRONICA, String.valueOf(contratoPontoConsumo.getIsEmiteNotaFiscalEletronica()));

		abaFaturamento.put(EMITIR_FATURA_COM_NFE, String.valueOf(contratoPontoConsumo.getEmitirFaturaComNfe()));

		Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento =
				controladorContrato.listarItensVencimentoPorContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());

		if (listaContratoPontoConsumoItemFaturamento != null && !listaContratoPontoConsumoItemFaturamento.isEmpty()) {

			Collection<ContratoPontoConsumoItemFaturamentoVO> listaVencimentoItemFaturaContratoVO =
					new ArrayList<ContratoPontoConsumoItemFaturamentoVO>();

			for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : listaContratoPontoConsumoItemFaturamento) {

				ContratoPontoConsumoItemFaturamentoVO itemFaturamentoContratoVO = new ContratoPontoConsumoItemFaturamentoVO();

				itemFaturamentoContratoVO
						.setChavePrimariaItemFatura(contratoPontoConsumoItemFaturamento.getItemFatura().getChavePrimaria());
				itemFaturamentoContratoVO.setFaseReferencia(contratoPontoConsumoItemFaturamento.getFaseReferencia());
				itemFaturamentoContratoVO
						.setIndicadorDepositoIdentificado(contratoPontoConsumoItemFaturamento.getIndicadorDepositoIdentificado());
				itemFaturamentoContratoVO.setNumeroDiaVencimento(contratoPontoConsumoItemFaturamento.getNumeroDiaVencimento());
				itemFaturamentoContratoVO.setItemFatura(contratoPontoConsumoItemFaturamento.getItemFatura());
				itemFaturamentoContratoVO.setOpcaoFaseReferencia(contratoPontoConsumoItemFaturamento.getOpcaoFaseReferencia());
				itemFaturamentoContratoVO.setTarifa(contratoPontoConsumoItemFaturamento.getTarifa());
				itemFaturamentoContratoVO.setVencimentoDiaUtil(contratoPontoConsumoItemFaturamento.getVencimentoDiaUtil());
				itemFaturamentoContratoVO.setDiaCotacao(contratoPontoConsumoItemFaturamento.getDiaCotacao());
				itemFaturamentoContratoVO.setDataReferenciaCambial(contratoPontoConsumoItemFaturamento.getDataReferenciaCambial());
				itemFaturamentoContratoVO.setPercminimoQDC(contratoPontoConsumoItemFaturamento.getPercminimoQDC());

				listaVencimentoItemFaturaContratoVO.add(itemFaturamentoContratoVO);
			}
			abaFaturamento.put(ITEM_FATURA, listaVencimentoItemFaturaContratoVO);
		}

		pontoConsumoContratoVO.setAbaFaturamento(abaFaturamento);
	}

	/**
	 * Popular dados aba modalidades.
	 *
	 */
	private void popularDadosAbaModalidades(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO)
			throws GGASException {

		Map<String, Object> abaModalidade = new HashMap<String, Object>();

		String valorTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		Penalidade penalidadeTakeOrPay = controladorContrato.obterPenalidade(Long.parseLong(valorTakeOrPay));

		String valorShipOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
		Penalidade penalidadeShipOrPay = controladorContrato.obterPenalidade(Long.parseLong(valorShipOrPay));

		String valorPenalidadeRetMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		Penalidade penalidadeRetMaior = controladorContrato.obterPenalidade(Long.parseLong(valorPenalidadeRetMaior));

		String valorPenalidadeRetMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);
		Penalidade penalidadeRetMenor = controladorContrato.obterPenalidade(Long.parseLong(valorPenalidadeRetMenor));

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade =
				controladorContrato.listarModalidadesPorContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());

		if (listaContratoPontoConsumoModalidade != null && !listaContratoPontoConsumoModalidade.isEmpty()) {

			Collection<ModalidadeConsumoFaturamentoVO> listaModalidadeConsumoFaturamentoVO = new HashSet<ModalidadeConsumoFaturamentoVO>();

			for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {

				ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO = new ModalidadeConsumoFaturamentoVO();
				if (contratoPontoConsumoModalidade.getContratoModalidade() != null) {
					modalidadeConsumoFaturamentoVO.setModalidade(contratoPontoConsumoModalidade.getContratoModalidade());
				}
				if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC() != null) {
					modalidadeConsumoFaturamentoVO.getListaContratoPontoConsumoModalidadeQDC()
							.addAll(contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC());
				}
				modalidadeConsumoFaturamentoVO.setDataFimRetiradaQPNR(contratoPontoConsumoModalidade.getDataFimRetiradaQPNR());
				modalidadeConsumoFaturamentoVO.setDataInicioRetiradaQPNR(contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR());
				modalidadeConsumoFaturamentoVO.setDiasAntecSolicConsumo(contratoPontoConsumoModalidade.getDiasAntecedenciaQDS());
				modalidadeConsumoFaturamentoVO.setTempoValidadeRetiradaQPNR(contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR());
				modalidadeConsumoFaturamentoVO
						.setIndicadorProgramacaoConsumo(contratoPontoConsumoModalidade.getIndicadorProgramacaoConsumo());
				modalidadeConsumoFaturamentoVO.setQdsMaiorQDC(contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC());
				modalidadeConsumoFaturamentoVO.setVariacaoSuperiorQDC(contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC());
				modalidadeConsumoFaturamentoVO
						.setConfirmacaoAutomaticaQDS(contratoPontoConsumoModalidade.getIndicadorConfirmacaoAutomaticaQDS());
				modalidadeConsumoFaturamentoVO.setRecuperacaoAutoTakeOrPay(contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica());
				modalidadeConsumoFaturamentoVO.setNumMesesSolicConsumo(contratoPontoConsumoModalidade.getMesesQDS());
				modalidadeConsumoFaturamentoVO.setPercDuranteRetiradaQPNR(contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR());
				modalidadeConsumoFaturamentoVO
						.setPercMinDuranteRetiradaQPNR(contratoPontoConsumoModalidade.getPercentualMinimoQDCContratoQPNR());
				modalidadeConsumoFaturamentoVO.setPercFimRetiradaQPNR(contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR());
				modalidadeConsumoFaturamentoVO.setPercentualQNR(contratoPontoConsumoModalidade.getPercentualQNR());
				if (contratoPontoConsumoModalidade.getHoraLimiteProgramacaoDiaria() != null) {
					modalidadeConsumoFaturamentoVO
							.setHoraLimiteProgramacaoDiaria(contratoPontoConsumoModalidade.getHoraLimiteProgramacaoDiaria());
				}
				if (contratoPontoConsumoModalidade.getHoraLimiteProgIntradiaria() != null) {
					modalidadeConsumoFaturamentoVO
							.setHoraLimiteProgramacaoIntraDiaria(contratoPontoConsumoModalidade.getHoraLimiteProgIntradiaria());
				}

				if (contratoPontoConsumoModalidade.getHoraLimiteAceitacaoDiaria() != null) {
					modalidadeConsumoFaturamentoVO
							.setHoraLimiteAceitacaoDiaria(contratoPontoConsumoModalidade.getHoraLimiteAceitacaoDiaria());
				}
				if (contratoPontoConsumoModalidade.getHoraLimiteAceitIntradiaria() != null) {
					modalidadeConsumoFaturamentoVO
							.setHoraLimiteAceitIntradiaria(contratoPontoConsumoModalidade.getHoraLimiteAceitIntradiaria());

				}

				if (contratoPontoConsumoModalidade.getPrazoRevisaoQDC() != null) {
					modalidadeConsumoFaturamentoVO.setPrazoRevizaoQDC(contratoPontoConsumoModalidade.getPrazoRevisaoQDC());
				}
				if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade() != null) {
					Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = controladorContrato
							.listarConsumoPenalidadePorContratoPontoConsumoModalidade(contratoPontoConsumoModalidade.getChavePrimaria());
					CompromissoTakeOrPayVO compromissoTakeOrPayVO = null;
					PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = null;
					for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaContratoPontoConsumoPenalidade) {
						compromissoTakeOrPayVO = new CompromissoTakeOrPayVO();
						compromissoTakeOrPayVO.setConsumoReferenciaTakeOrPay(contratoPontoConsumoPenalidade.getConsumoReferencia());
						compromissoTakeOrPayVO.setPeriodicidadeTakeOrPay(contratoPontoConsumoPenalidade.getPeriodicidadePenalidade());
						compromissoTakeOrPayVO.setMargemVariacaoTakeOrPay(contratoPontoConsumoPenalidade.getPercentualMargemVariacao());
						compromissoTakeOrPayVO
								.setReferenciaQFParadaProgramada(contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada());

						compromissoTakeOrPayVO.setDataInicioVigencia(contratoPontoConsumoPenalidade.getDataInicioVigencia());
						compromissoTakeOrPayVO.setDataFimVigencia(contratoPontoConsumoPenalidade.getDataFimVigencia());
						compromissoTakeOrPayVO.setRecuperavel(contratoPontoConsumoPenalidade.getRecuperavel());
						compromissoTakeOrPayVO.setConsideraParadaProgramada(contratoPontoConsumoPenalidade.getConsideraParadaProgramada());
						compromissoTakeOrPayVO.setConsideraCasoFortuito(contratoPontoConsumoPenalidade.getConsideraCasoFortuito());
						compromissoTakeOrPayVO
								.setConsideraFalhaFornecimento(contratoPontoConsumoPenalidade.getConsideraFalhaFornecimento());
						compromissoTakeOrPayVO
								.setReferenciaQFParadaProgramada(contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada());
						compromissoTakeOrPayVO.setPrecoCobranca(contratoPontoConsumoPenalidade.getPrecoCobranca());
						compromissoTakeOrPayVO.setTipoApuracao(contratoPontoConsumoPenalidade.getTipoApuracao());
						compromissoTakeOrPayVO.setTipoAgrupamentoContrato(contratoPontoConsumoPenalidade.getTipoAgrupamentoContrato());
						compromissoTakeOrPayVO.setApuracaoParadaProgramada(contratoPontoConsumoPenalidade.getApuracaoParadaProgramada());
						compromissoTakeOrPayVO.setApuracaoCasoFortuito(contratoPontoConsumoPenalidade.getApuracaoCasoFortuito());
						compromissoTakeOrPayVO.setApuracaoFalhaFornecimento(contratoPontoConsumoPenalidade.getApuracaoFalhaFornecimento());
						compromissoTakeOrPayVO.setPercentualNaoRecuperavel(contratoPontoConsumoPenalidade.getPercentualNaoRecuperavel());

						if ((contratoPontoConsumoPenalidade.getPenalidade() != null) && (contratoPontoConsumoPenalidade.getPenalidade()
								.getChavePrimaria() == penalidadeTakeOrPay.getChavePrimaria())) {
							modalidadeConsumoFaturamentoVO.getListaCompromissoTakeOrPayVO().add(compromissoTakeOrPayVO);
						} else if ((contratoPontoConsumoPenalidade.getPenalidade() != null) && (contratoPontoConsumoPenalidade
								.getPenalidade().getChavePrimaria() == penalidadeShipOrPay.getChavePrimaria())) {
							modalidadeConsumoFaturamentoVO.getListaCompromissoShipOrPayVO().add(compromissoTakeOrPayVO);

						} else if (contratoPontoConsumoPenalidade.getPenalidade() != null
								&& (contratoPontoConsumoPenalidade.getPenalidade().getChavePrimaria() == penalidadeRetMaior
										.getChavePrimaria())
								|| contratoPontoConsumoPenalidade.getPenalidade() != null && (contratoPontoConsumoPenalidade.getPenalidade()
										.getChavePrimaria() == penalidadeRetMenor.getChavePrimaria())) {
							penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
							penalidadesRetiradaMaiorMenorVO.setEntidadePeriodicidadeRetiradaMaiorMenor(
									contratoPontoConsumoPenalidade.getPeriodicidadePenalidade());
							penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(contratoPontoConsumoPenalidade.getDataInicioVigencia());
							penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(contratoPontoConsumoPenalidade.getDataFimVigencia());
							penalidadesRetiradaMaiorMenorVO
									.setEntidadeConsumoReferenciaRetMaiorMenor(contratoPontoConsumoPenalidade.getConsumoReferencia());
							penalidadesRetiradaMaiorMenorVO.setValorPercentualCobRetMaiorMenor(
									contratoPontoConsumoPenalidade.getValorPercentualCobRetMaiorMenor());
							penalidadesRetiradaMaiorMenorVO.setValorPercentualCobIntRetMaiorMenor(
									contratoPontoConsumoPenalidade.getValorPercentualCobIntRetMaiorMenor());
							penalidadesRetiradaMaiorMenorVO
									.setValorPercentualRetMaiorMenor(contratoPontoConsumoPenalidade.getValorPercentualRetMaiorMenor());
							penalidadesRetiradaMaiorMenorVO
									.setEntidadeTipoApuracaoRetirMaiorMenor(contratoPontoConsumoPenalidade.getTipoApuracao());
							penalidadesRetiradaMaiorMenorVO
									.setEntidadePrecoCobrancaRetiradaMaiorMenor(contratoPontoConsumoPenalidade.getPrecoCobranca());
							penalidadesRetiradaMaiorMenorVO
									.setEntidadeBaseApuracaoRetiradaMaiorMenor(contratoPontoConsumoPenalidade.getBaseApuracao());
							penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(contratoPontoConsumoPenalidade.getIndicadorImposto());

							if (contratoPontoConsumoPenalidade.getPenalidade() != null && (contratoPontoConsumoPenalidade.getPenalidade()
									.getChavePrimaria() == penalidadeRetMaior.getChavePrimaria())) {
								penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidadeRetMaior);
								modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO()
										.add(penalidadesRetiradaMaiorMenorVO);
							} else if (contratoPontoConsumoPenalidade.getPenalidade() != null && (contratoPontoConsumoPenalidade
									.getPenalidade().getChavePrimaria() == penalidadeRetMenor.getChavePrimaria())) {
								penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidadeRetMenor);
								modalidadeConsumoFaturamentoVO.getListaPenalidadesRetiradaMaiorMenorVO()
										.add(penalidadesRetiradaMaiorMenorVO);
							}

						}
					}
				}
				listaModalidadeConsumoFaturamentoVO.add(modalidadeConsumoFaturamentoVO);
			}
			abaModalidade.put(MODALIDADES, listaModalidadeConsumoFaturamentoVO);
		}
		pontoConsumoContratoVO.setAbaModalidade(abaModalidade);
	}

	/**
	 * Popular dados aba responsabilidade.
	 *
	 */
	private void popularDadosAbaResponsabilidade(ContratoPontoConsumo contratoPontoConsumo, PontoConsumoContratoVO pontoConsumoContratoVO,
			Contrato contrato) throws GGASException {

		Map<String, Object> abaResponsabilidades = new HashMap<String, Object>();
		Collection<ContratoCliente> listaContratoCliente = controladorContrato.listarContratoClientePorPontoConsumo(
				contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), contrato.getChavePrimaria());

		abaResponsabilidades.put(LISTA_CONTRATO_CLIENTE, listaContratoCliente);
		pontoConsumoContratoVO.setAbaResponsabilidades(abaResponsabilidades);
	}

	/**
	 * Validar adicao novo compromisso.
	 *
	 */
	private void validarAdicaoNovoCompromisso(CompromissoTakeOrPayVO compromissoTakeOrPayVO,
			Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayVO) throws GGASException {

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = converterCompromissoTakeOrPayVO(compromissoTakeOrPayVO);
		Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = new ArrayList<ContratoPontoConsumoPenalidade>();
		for (CompromissoTakeOrPayVO compromisso : listaCompromissosTakeOrPayVO) {
			ContratoPontoConsumoPenalidade penalidade = converterCompromissoTakeOrPayVO(compromisso);
			listaContratoPontoConsumoPenalidade.add(penalidade);
		}

		Contrato contrato = compromissoTakeOrPayVO.getContrato();

		controladorContrato.validaFormaDeApuracao(compromissoTakeOrPayVO);
		controladorContrato.validarIntervaloDataVigenciaComDataAssinaturaContrato(contrato, null, contratoPontoConsumoPenalidade);
		controladorContrato.validarAdicaoPenalidade(contratoPontoConsumoPenalidade, listaContratoPontoConsumoPenalidade);
	}

	/**
	 * Converter compromisso take or pay vo.
	 *
	 */
	private ContratoPontoConsumoPenalidade converterCompromissoTakeOrPayVO(CompromissoTakeOrPayVO compromisso) {

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade;
		contratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();
		contratoPontoConsumoPenalidade.setConsumoReferencia(compromisso.getConsumoReferenciaTakeOrPay());

		if (compromisso.getMargemVariacaoTakeOrPay() != null) {
			contratoPontoConsumoPenalidade.setPercentualMargemVariacao(compromisso.getMargemVariacaoTakeOrPay());
		}

		contratoPontoConsumoPenalidade.setPeriodicidadePenalidade(compromisso.getPeriodicidadeTakeOrPay());
		contratoPontoConsumoPenalidade.setReferenciaQFParadaProgramada(compromisso.getReferenciaQFParadaProgramada());
		contratoPontoConsumoPenalidade.setApuracaoCasoFortuito(compromisso.getApuracaoCasoFortuito());
		contratoPontoConsumoPenalidade.setApuracaoFalhaFornecimento(compromisso.getApuracaoFalhaFornecimento());
		contratoPontoConsumoPenalidade.setApuracaoParadaProgramada(compromisso.getApuracaoParadaProgramada());
		contratoPontoConsumoPenalidade.setConsideraCasoFortuito(compromisso.getConsideraCasoFortuito());
		contratoPontoConsumoPenalidade.setConsideraFalhaFornecimento(compromisso.getConsideraFalhaFornecimento());
		contratoPontoConsumoPenalidade.setConsideraParadaProgramada(compromisso.getConsideraParadaProgramada());
		contratoPontoConsumoPenalidade.setDataFimVigencia(compromisso.getDataFimVigencia());
		contratoPontoConsumoPenalidade.setDataInicioVigencia(compromisso.getDataInicioVigencia());
		contratoPontoConsumoPenalidade.setPercentualNaoRecuperavel(compromisso.getPercentualNaoRecuperavel());
		contratoPontoConsumoPenalidade.setPrecoCobranca(compromisso.getPrecoCobranca());
		contratoPontoConsumoPenalidade.setRecuperavel(compromisso.getRecuperavel());
		contratoPontoConsumoPenalidade.setTipoApuracao(compromisso.getTipoApuracao());

		return contratoPontoConsumoPenalidade;
	}

	/**
	 * Carregar dados modelo.
	 *
	 */
	private void carregarDadosModelo(ContratoVO contratoVO, ModeloContrato modeloContrato, Model model) {

		Collection<ModeloAtributo> atributos = modeloContrato.getAtributos();

		if (atributos != null && !atributos.isEmpty()) {
			String campo = null;
			String campoSelecao = null;
			String campoObrigatorio = null;
			String campoValorFixo = null;

			for (ModeloAtributo modeloAtributo : atributos) {
				campo = modeloAtributo.getAbaAtributo().getNomeColuna();
				if (StringUtils.isNotEmpty(campo)) {
					campoSelecao = Util.obterNomeCampoSelecao(campo);
					campoObrigatorio = Util.obterNomeCampoObrigatorio(campo);
					campoValorFixo = Util.obterNomeCampoValorFixo(campo);

					prepararCamposView(contratoVO, campo, campoSelecao, campoObrigatorio, campoValorFixo, modeloAtributo, model);
				}
			}
		}
	}

	private void prepararCamposView(ContratoVO contratoVO, String campo, String campoSelecao, String campoObrigatorio,
			String campoValorFixo, ModeloAtributo modeloAtributo, Model model) {

		ContratoVO.setField(campoSelecao, contratoVO, Boolean.TRUE);

		ContratoVO.setField(campoObrigatorio, contratoVO, modeloAtributo.isObrigatorio());

		if (this.verificarCampoNulo(contratoVO.getField(campo, contratoVO))) {

			ContratoVO.setField(campo, contratoVO, modeloAtributo.getPadrao());
		}

		ContratoVO.setField(campoValorFixo, contratoVO, modeloAtributo.isValorFixo());
	}

	/**
	 * Verificar campo nulo.
	 *
	 */
	private boolean verificarCampoNulo(Object campo) {

		boolean retorno = false;

		if (campo instanceof String) {
			retorno = StringUtils.isEmpty(String.valueOf(campo)) || NAO_SELECIONADO.equals(campo);
		} else {
			retorno = (campo == null);
		}

		return retorno;

	}

	/**
	 * Salvar contrato parcial passo1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("salvarContratoParcialPasso1")
	public String salvarContratoParcialPasso1(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

		request.getSession().setAttribute(SITUACAO_ESCOLHIDA, contratoVO.getSituacaoContrato());
		
		Long idCliente = contratoVO.getIdCliente();

		try {
			if(Objects.nonNull(idCliente)) {
				this.validarSeClientePossuiDebito(idCliente, request, model);
			}
				
			
			if (contratoSessao == null) {
				Contrato contrato = (Contrato) controladorContrato.criar();
				contrato.setDadosAuditoria(this.getDadosAuditoria(request));
				this.popularContratoPasso1(contratoVO, contrato, request, false, false, false, false, model);
				request.getSession().setAttribute(CONTRATO, contrato);
				contratoSessao = contrato;
			} else {
				contratoSessao.setDadosAuditoria(this.getDadosAuditoria(request));
				this.popularContratoPasso1(contratoVO, contratoSessao, request, false, false, false, false, model);
				request.getSession().setAttribute(CONTRATO, contratoSessao);
			}

			controladorContrato.salvarContratoParcial(contratoSessao);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, CONTRATO_PARCIAL);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);
	}

	/**
	 * Salvar contrato parcial passo2.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 */
	@RequestMapping("salvarContratoParcialPasso2")
	public String salvarContratoParcialPasso2(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, InvocationTargetException, IllegalAccessException {

		Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);
		Long chavePontoConsumo = contratoVO.getIdPontoConsumo();

		Map<Long, PontoConsumoContratoVO> listaPontoConsumoContratoVO = getListaPontoConsumoContratoVO(request);

		if (listaPontoConsumoContratoVO == null) {
			listaPontoConsumoContratoVO = new HashMap<Long, PontoConsumoContratoVO>();
		}

		try {

			// Aplicar dados, no caso de salvar
			// parcial não deve realizar validações.
			if (chavePontoConsumo != null && chavePontoConsumo > 0) {
				aplicarDadosAbaPrincipal(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, false);
				aplicarDadosAbaDadosTecnicos(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, false);
				aplicarDadosAbaConsumo(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, false);
				aplicarDadosAbaFaturamento(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, false);
				aplicarDadosAbaModalidades(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, false);
				aplicarDadosAbaResponsabilidade(contratoVO, chavePontoConsumo, listaPontoConsumoContratoVO, request, false);
			}

			// Seta a lista na sessão, o método
			// popularContratoPasso2 irá usar essa
			// lista.
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_CONTRATO_VO, listaPontoConsumoContratoVO);

			// Popular contrato
			popularContratoPasso2(contratoVO, request, contratoSessao);
			contratoSessao.setDadosAuditoria(this.getDadosAuditoria(request));

			// Salvar parcial
			controladorContrato.salvarContratoParcial(contratoSessao);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);

	}

	/**
	 * Exibir msg alerta aditamento alteracao contrato.
	 *
	 */
	private void exibirMsgAlertaAditamentoAlteracaoContrato(HttpServletRequest request, Model model) throws GGASException {

		if (controladorContrato.isAlteracaoContratoAgrupadoPorValorParaPorVolume((Contrato) request.getSession().getAttribute(CONTRATO))) {

			mensagemAlerta(model, Constantes.CONTRATO_ALTERACAO_CONTRATO_AGRUPADO_POR_VALOR_PARA_POR_VOLUME);

		}

	}

	/**
	 * Método responsável por exibir a tela de encerrar contrato passo 1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 */
	@RequestMapping("exibirPontoConsumoContrato")
	public String exibirPontoConsumoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		
		ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		
		ControladorContratoAdequacaoParceria controladorContratoAdequacaoParceria = (ControladorContratoAdequacaoParceria) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorContratoAdequacaoParceria.BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA);
		
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		
		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		
		
		
		chamadoTela = contratoVO.getChamadoTela();
		String dataRecisao = null;
		String retorno = "exibirPontoConsumoContrato";

		try {
			Long chavePrimaria = contratoVO.getChavePrimaria();

			Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria);

			if (contrato != null && controladorIntegracao.integracaoPrecedenteContratoAtiva()
					&& !controladorIntegracao.validarIntegracaoContrato(contrato.getChavePrimaria(), contrato.getChavePrimariaPai())) {
				throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
			}

			DadosAuditoria dadosAuditoria = this.getDadosAuditoria(request);
			String chavePrimariaContrato = request.getParameter(CHAVE_PRIMARIA_CONTRATO);

			if (chavePrimaria == null) {
				if (request.getSession().getAttribute(ID_CONTRATO_ENCERRAR_RESCINDIR) != null) {
					dataRecisao = (String) request.getSession().getAttribute(PERIODO_RECISAO_CAMPO);
					request.getSession().removeAttribute(PERIODO_RECISAO_CAMPO);

					chavePrimaria = Long.parseLong(request.getSession().getAttribute(ID_CONTRATO_ENCERRAR_RESCINDIR).toString());
					contratoVO.setChavePrimaria(chavePrimaria);
					request.getSession().removeAttribute(ID_CONTRATO_ENCERRAR_RESCINDIR);

					String listaIdsPontoConsumoRemovidosAgrupados =
							request.getSession().getAttribute(LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS).toString();
					if (listaIdsPontoConsumoRemovidosAgrupados != null) {
						contratoVO.setListaIdsPontoConsumoRemovidosAgrupados(listaIdsPontoConsumoRemovidosAgrupados);
						request.getSession().removeAttribute(LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS);
					}
				} else {
					chavePrimaria = Long.parseLong(chavePrimariaContrato);
				}
			}

			controladorContrato.validarEncerrarRescindirContrato(chavePrimaria);
			Contrato selecionado = controladorContrato.carregarContratoEncerramentoRescisao(chavePrimaria);
			// Verifica se o contrato que está
			// sendo encerrado possui ou não um contrato adequação parceria
			// a ele vinculado, se existir, é verificado se o total de gás
			// consumido pelo ponto de consumo está dentro do consumo mínimo
			// estabelecido no contrato, se o consumo for inferior ao que foi
			// estaebelcido no contrato, é gerado uma nota de débito e o contrato
			// adequação é finalizado, caso não, apenas o contrato adequação é finalizado.
			Fatura notaDebitoCredito = (Fatura) controladorFatura.criar();
			String numeroContrato = selecionado.getNumeroCompletoContrato();
			Map<String, Object> filtro = new HashMap<String, Object>();

			this.prepararFiltroContratoAdequacao(filtro, numeroContrato);
			// Verifica se o contrato que está sendo
			// finalizado possui um contrato adequação parceria vinculado.
			Collection<ContratoAdequacaoParceria> listaContratos =
					controladorContratoAdequacaoParceria.consultarContratosAdequacaoParceria(filtro);
			if (listaContratos != null && !listaContratos.isEmpty()) {
				ContratoAdequacaoParceria contratoAdequacaoParceria = listaContratos.iterator().next();
				// Se entrar nesta condição, indica que o ponto de consumo não consumiu o mínimo
				// estabelecido no contrato.
				if (gerarNotaDebito(selecionado, contratoAdequacaoParceria, request, notaDebitoCredito,
						contratoAdequacaoParceria.getPontoConsumo(), selecionado.getClienteAssinatura())) {

					controladorFatura.validarDadosNotaDebito(notaDebitoCredito);
					controladorFatura.inserirNotaDebitoCredito(notaDebitoCredito, null, null);
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Fatura.NOTA_DEBITO_CREDITO);

				}
			}

			controladorParametroSistema.obterParametroPorCodigo(Constantes.CODIGO_SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO);
			String situacaoConsumo =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PONTO_CONSUMO_AGUARDANDO_ATIVACAO);

			SituacaoConsumo situacao = controladorImovel.obterSituacaoConsumo(Long.parseLong(situacaoConsumo));
			int count = 0;

			for (ContratoPontoConsumo contratoPontoConsumo : selecionado.getListaContratoPontoConsumo()) {
				ContratoPontoConsumo contratoPontoConsumoAux =
						controladorContrato.obterContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
				if (contratoPontoConsumoAux.getPontoConsumo().getSituacaoConsumo().equals(situacao)) {
					count++;
				}
			}

			if (selecionado.getListaContratoPontoConsumo().size() == count) {
				finalizarContrato(selecionado, dadosAuditoria, request, model, null);
				if (chamadoTela) {
					retorno = "exibirEncerrarContratoChamado";
				} else {
					retorno = "exibirPesquisaContrato";
				}
			} else {

				Collection<PontoConsumo> listaPontoConsumoRemovidos = new ArrayList<PontoConsumo>();
				if (request.getAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS) != null) {
					listaPontoConsumoRemovidos =
							new ArrayList<PontoConsumo>((Collection<PontoConsumo>) request.getAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS));

					if (selecionado.getAgrupamentoCobranca()) {
						contratoVO.setListaIdsPontoConsumoRemovidosAgrupados(Util
								.arrayParaString(listaPontoConsumoRemovidos.toArray(new EntidadeNegocio[] {}), Constantes.STRING_VIRGULA));
					}
				}

				Collection<PontoConsumo> pontos = new ArrayList<PontoConsumo>();
				if (listaPontoConsumoRemovidos.isEmpty()) {
					for (ContratoPontoConsumo contratoPontoConsumo : selecionado.getListaContratoPontoConsumo()) {
						pontos.add((PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(),
								IMOVEL));
					}

					if (selecionado.getSituacao().getChavePrimaria() == ADITADO
							|| selecionado.getSituacao().getChavePrimaria() == ALTERADO) {
						Contrato atual = controladorContrato.consultarContratoAtivoPorContratoPai(selecionado.getChavePrimariaPai());

						if (atual != null && atual.getListaContratoPontoConsumo() != null) {
							for (ContratoPontoConsumo contratoPontoConsumo : atual.getListaContratoPontoConsumo()) {
								pontos.remove(controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
							}
							listaPontoConsumoRemovidos = pontos;
						}
					}
				} else {
					pontos = listaPontoConsumoRemovidos;
				}

				Collection<PontoConsumoEncerrarFaturaVO> listaFaturaVO = new ArrayList<PontoConsumoEncerrarFaturaVO>();
				PontoConsumoEncerrarFaturaVO pontoFaturaVO;
				int qtdFaturasEncerramento = 0;

				for (PontoConsumo pontoConsumo : pontos) {
					pontoFaturaVO = new PontoConsumoEncerrarFaturaVO();

					pontoFaturaVO.setChavePrimaria(pontoConsumo.getChavePrimaria());
					pontoFaturaVO.setDescricao(pontoConsumo.getDescricao());
					pontoFaturaVO.setDescricaoSegmento(pontoConsumo.getSegmento().getDescricao());
					pontoFaturaVO.setDescricaoSituacao(pontoConsumo.getSituacaoConsumo().getDescricao());
					pontoFaturaVO.setIdFatura(controladorFatura.verificarEncerramento(pontoConsumo.getChavePrimaria(), selecionado));
					
					FiltroHistoricoOperacaoMedidor filtroHistorico = new FiltroHistoricoOperacaoMedidor();
					filtroHistorico.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
					filtroHistorico.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
					filtroHistorico.setLimiteResultados(1);
					HistoricoOperacaoMedidor historico = controladorMedidor.obterUnicoHistoricoOperacaoMedidor(filtroHistorico);
					
					if (historico != null
							&& (historico.getOperacaoMedidor().getChavePrimaria() == OperacaoMedidor.CODIGO_BLOQUEIO
									&& !"CORTADO".equals(pontoConsumo.getSituacaoConsumo().getDescricao())
									&& !"SUSPENSO".equals(pontoConsumo.getSituacaoConsumo().getDescricao()))
							|| (historico.getOperacaoMedidor().getChavePrimaria() == OperacaoMedidor.CODIGO_REATIVACAO
									&& historico.getIndicadorOperacaoConjunta())) {
						pontoFaturaVO.setDataLeitura(Util.converterDataParaString(historico.getDataRealizada()));
						pontoFaturaVO.setValorLeitura(historico.getNumeroLeitura().toString());
					}
					

					if (pontoFaturaVO.getIdFatura() != null) {
						qtdFaturasEncerramento++;
					}

					listaFaturaVO.add(pontoFaturaVO);
				}

				// TODO verificar a melhor forma de
				// comunicação com o incluirfatura
				// a situação do contrato esta sendo
				// alterada pois na comunicação com o
				// incluirfatura
				// necessita que a situacao do
				// contrato esteja ativa

				if (dataRecisao != null && !StringUtils.EMPTY.equals(dataRecisao)) {
					model.addAttribute(PERIODO_RECISAO_CAMPO, dataRecisao);
					request.getSession().removeAttribute(PERIODO_RECISAO_CAMPO);
				} else {
					model.addAttribute(PERIODO_RECISAO_CAMPO, DataUtil.converterDataParaString(new Date()));
				}

				model.addAttribute(LISTA_PONTO_CONSUMO, listaFaturaVO);
				model.addAttribute(ID_CONTRATO_ENCERRAR_RESCINDIR, chavePrimaria);
				model.addAttribute("listaFuncionario", obterFuncionarios());
				model.addAttribute("funcionarioSelecionado", dadosAuditoria.getUsuario().getFuncionario());

				contratoVO.setIndicadorFaturamentoAgrupado(selecionado.getAgrupamentoCobranca().toString());
				contratoVO.setFinalizacaoEncerramento(listaFaturaVO.size() == qtdFaturasEncerramento);

				if (listaPontoConsumoRemovidos != null && !listaPontoConsumoRemovidos.isEmpty()) {
					String listaIdsPontoConsumoRemovidos =
							Util.arrayParaString(listaPontoConsumoRemovidos.toArray(new EntidadeNegocio[] {}), Constantes.STRING_VIRGULA);
					contratoVO.setListaIdsPontoConsumoRemovidos(listaIdsPontoConsumoRemovidos);
				}
				if (request.getAttribute(LISTA_PONTO_CONSUMO_REMOVIDOS) != null) {
					retorno = EXIBIR_PONTO_CONSUMO_CONTRATO;
				}

			}

		} catch (GGASException e) {
			if (e.getChaveErro() != null && "Ocorreu um erro ao gerar o arquivo PDF".equals(e.getChaveErro())) {
				e = new NegocioException("ERRO_GERAR_PDF");
			}
			mensagemErroParametrizado(model, request, e);
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
			retorno = pesquisarContrato(contratoVO, result, request, model);
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
			modificarFluxosEstadoDefault(contratoVO);
			contratoVO.setChavePrimaria(null);
			retorno = pesquisarContrato(contratoVO, result, request, model);
		}
		
		model.addAttribute("contratoVO", contratoVO);

		return retorno;
	}

	/**
	 * Método responsável por exibir a mudanca de titularidade.
	 *
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPopupMudancaTitularidade")
	public String exibirPopupMudancaTitularidade() throws GGASException {

		return "exibirPopupMudancaTitularidade";
	}

	/**
	 * Método responsável por exibir a tela de encerrar contrato passo 1.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws CloneNotSupportedException {@link CloneNotSupportedException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 */
	@RequestMapping("exibirEncerrarRescindirContrato")
	public String exibirEncerrarRescindirContrato(@RequestParam(required = false, value = "funcionario") Funcionario funcionario, ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, CloneNotSupportedException, IllegalAccessException, InvocationTargetException {

		String retorno = "exibirEncerrarRescindirContrato";
		DadosAuditoria dadosAuditoria = this.getDadosAuditoria(request);

		Long chavePrimaria = contratoVO.getChavePrimaria();
		if (chavePrimaria == null) {
			chavePrimaria = Long.valueOf(request.getParameter(ID_CONTRATO_ENCERRAR_RESCINDIR));
		}
		Long idPontoConsumoEncerrar = contratoVO.getIdPontoConsumoEncerrar();
		if (idPontoConsumoEncerrar == null && request.getParameter(ID_PONTO_CONSUMO) != null
				&& !request.getParameter(ID_PONTO_CONSUMO).isEmpty()) {
			idPontoConsumoEncerrar = Long.valueOf(request.getParameter(ID_PONTO_CONSUMO));
		}

		String listaIdsPontoConsumoRemovidos = contratoVO.getListaIdsPontoConsumoRemovidos();
		boolean temRemovidos = listaIdsPontoConsumoRemovidos != null && !StringUtils.EMPTY.equals(listaIdsPontoConsumoRemovidos);

		Collection<Long> idPontosSemMulta = getListaIds(contratoVO, PONTOS_SEM_RECISAO);

		DateTime dataRecisao = null;
		String periodoRecisao = null;
		boolean campoObrigatorioDataRecisaoVazio = false;
		periodoRecisao = request.getParameter(PERIODO_RECISAO_CAMPO);
		if (periodoRecisao == null && request.getSession().getAttribute(PERIODO_RECISAO_CAMPO) != null) {
			// setado na FaturaAction linha:
			// 472 (rev 12484 TKT# 3958)
			periodoRecisao = (String) request.getSession().getAttribute(PERIODO_RECISAO_CAMPO);
		}

		try {

			if (periodoRecisao != null && !StringUtils.EMPTY.equals(periodoRecisao)) {
				dataRecisao = new DateTime(
						Util.converterCampoStringParaData(ControladorContratoImpl.CONTRATO_DATA_RECISAO, periodoRecisao, FORMATO_DATA_BR));
			}

			if (idPontoConsumoEncerrar == null) {
				if (dataRecisao == null) {
					campoObrigatorioDataRecisaoVazio = true;
					throw new GGASException(Constantes.ERRO_NEGOCIO_CONTRATO_CAMPO_DATA_RECISAO_OBRIGATORIO);
				} else {
					controladorContrato.atualizarDataRecisaoContrato(chavePrimaria, dataRecisao.toDate(), dadosAuditoria);
				}
			}

			Contrato selecionado = controladorContrato.carregarContratoEncerramentoRescisao(chavePrimaria);

			// Caso seja uma recisão e tenha um
			// tipo de multa recisória definida
			// para o contrato.
			if (selecionado.getMultaRecisoria() != null && selecionado.getDataVencimentoObrigacoes() != null
					&& selecionado.getDataVencimentoObrigacoes().after(Calendar.getInstance().getTime())) {

				if (idPontoConsumoEncerrar != null && idPontoConsumoEncerrar > 0) {
					CalculoIndenizacaoVO dadosCalculo =
							controladorContrato.prepararCalculoIndenizacaoRescisao(selecionado, idPontoConsumoEncerrar);
					model.addAttribute(DADOS_CALCULO_INDENIZACAO, dadosCalculo);
					contratoVO.setCobrarIndenizacao(true);
				}

				this.popularFormContrato(selecionado, contratoVO, request, model);
				model.addAttribute(SITUACAO_CONTRATO, selecionado.getSituacao());
				model.addAttribute(LISTA_PONTO_CONSUMO,
						carregarListaRubrica(selecionado, contratoVO, idPontosSemMulta, listaIdsPontoConsumoRemovidos));

				contratoVO.setPontosSemRecisao(Util.arrayParaString(idPontosSemMulta.toArray(new Long[] {}), Constantes.STRING_VIRGULA));
				if (temRemovidos) {
					contratoVO.setListaIdsPontoConsumoRemovidos(listaIdsPontoConsumoRemovidos);
				}
			} else {
				// Caso seja um encerramento ou não
				// haja multa recisória a ser cobrada.

				retorno = "exibirPesquisaContrato";

				// Calcula penalidades apenas dos
				// pontos removidos
				if (temRemovidos) {
					processarPenalidadesPontosRemovidosContato(contratoVO, selecionado, dadosAuditoria);
				} else {
					// Encerra o contrato e calcula
					// penalidades para todos os
					// pontos de consumo dele.
					finalizarContrato(selecionado, dadosAuditoria, request, model, funcionario);
					finalizarClienteImovel(selecionado, dataRecisao);
				}

				if (selecionado.getMultaRecisoria() == null && selecionado.getDataVencimentoObrigacoes() != null
						&& selecionado.getDataVencimentoObrigacoes().after(Calendar.getInstance().getTime())) {
					mensagemSucesso(model, CONTRATO_SEM_MULTA_RECISORIA);
				} else {
					mensagemSucesso(model, SUCESSO_ENCERRAR_RESCINDIR);
				}

			}

			modificarFluxosEstadoDefault(contratoVO);
			
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession().getAttribute("urlRetornoAtualizacaoCastral");
				retorno = "redirect:" + urlRetornoAtualizacaoCastral;
			}

		} catch (GGASException e) {

			LOG.error(e.getStackTrace(), e);
			Contrato selecionado = controladorContrato.obterContratoListasCarregadas(chavePrimaria, null);

			this.popularFormContrato(selecionado, contratoVO, request, model);
			model.addAttribute(SITUACAO_CONTRATO, selecionado.getSituacao());
			model.addAttribute(LISTA_PONTO_CONSUMO,
					carregarListaRubrica(selecionado, contratoVO, idPontosSemMulta, listaIdsPontoConsumoRemovidos));

			contratoVO.setPontosSemRecisao(Util.arrayParaString(idPontosSemMulta.toArray(new Long[] {}), Constantes.STRING_VIRGULA));

			if (temRemovidos) {
				contratoVO.setListaIdsPontoConsumoRemovidos(listaIdsPontoConsumoRemovidos);
			}

			mensagemErroParametrizado(model, request, e);

			if (Constantes.ERRO_DADOS_INVALIDOS.equals(e.getChaveErro()) || campoObrigatorioDataRecisaoVazio) {
				retorno = exibirPontoConsumoContrato(contratoVO, result, request, model);
				model.addAttribute("periodoRecisaoCampo", periodoRecisao);
			}

		}

		if (chamadoTela) {

			retorno = exibirPopupMudancaTitularidade();

		}
		
		model.addAttribute("contratoVO", contratoVO);

		return retorno;

	}

	/**
	 * Finalizar cliente imovel.
	 *
	 * @param selecionado {@link Contrato}
	 * @param dataRecisao {@link DateTime}
	 * @throws GGASException {@link GGASException}
	 */
	public void finalizarClienteImovel(Contrato selecionado, DateTime dataRecisao) throws GGASException {

		String parametroSistemaResponCli =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO);
		ParametroSistema parametroSistemaMotivoFimRel = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_ENCERRAR_CONTRATO);
		MotivoFimRelacionamentoClienteImovel motivoFimRelacionamentoClienteImovel =
				controladorImovel.obterMotivoFimRelacionamentoClienteImovel(Long.parseLong(parametroSistemaMotivoFimRel.getValor()));

		for (ContratoPontoConsumo contratoPontoConsumo : selecionado.getListaContratoPontoConsumo()) {

			PontoConsumo pontoConsumo =
					(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);
			ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(),
					selecionado.getClienteAssinatura().getChavePrimaria(), Long.parseLong(parametroSistemaResponCli));

			if (clienteImovel != null) {
				clienteImovel.setRelacaoFim(dataRecisao.toDate());
				clienteImovel.setMotivoFimRelacionamentoClienteImovel(motivoFimRelacionamentoClienteImovel);
				controladorClienteImovel.atualizar(clienteImovel);
			}
		}
	}

	/**
	 * Método responsável por finalizar um contrato (encerramento ou recisão). A finalização altera a situação e a data de recisão do
	 * contrato caso necessário e calcula as penalidades que estejam relacionadas a ele.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws InvocationTargetException {@link InvocationTargetException}
	 * @throws IllegalAccessException {@link IllegalAccessException}
	 */
	@RequestMapping("finalizarContrato")
	public String finalizarContrato(ContratoVO contratoVO, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		Long chavePrimaria = contratoVO.getChavePrimaria();
		String listaIdsPontoConsumoRemovidos = contratoVO.getListaIdsPontoConsumoRemovidos();
		boolean temRemovidos = listaIdsPontoConsumoRemovidos != null && !StringUtils.EMPTY.equals(listaIdsPontoConsumoRemovidos);

		DadosAuditoria dadosAuditoria = this.getDadosAuditoria(request);
		Contrato selecionado = controladorContrato.obterContratoListasCarregadas(chavePrimaria, null);

		// Calcula penalidades apenas dos pontos
		// removidos
		if (temRemovidos) {
			processarPenalidadesPontosRemovidosContato(contratoVO, selecionado, dadosAuditoria);

		} else {
			// Encerra o contrato e calcula
			// penalidades para todos os pontos de
			// consumo dele.
			finalizarContrato(selecionado, dadosAuditoria, request, model, null);
		}

		return "exibirPesquisaContrato";
	}

	/**
	 * Processar penalidades pontos removidos contato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param selecionado {@link Contrato}
	 * @param dadosAuditoria {@link DadosAuditoria}
	 * @throws GGASException {@link GGASException}
	 */
	private void processarPenalidadesPontosRemovidosContato(ContratoVO contratoVO, Contrato selecionado, DadosAuditoria dadosAuditoria)
			throws GGASException {

		if (!controladorApuracaoPenalidade.obterListaPenalidasPorContrato(selecionado.getChavePrimaria()).isEmpty()) {
			Collection<Long> idsPontosRemovidos = getListaIds(contratoVO, LISTA_IDS_PONTO_CONSUMO_REMOVIDOS);
			Collection<PontoConsumo> pontosRemovidos = new ArrayList<PontoConsumo>();
			for (Long idPonto : idsPontosRemovidos) {
				pontosRemovidos.add((PontoConsumo) controladorPontoConsumo.obter(idPonto, IMOVEL, SEGMENTO));
			}

			StringBuilder logProcessamento = new StringBuilder();
			logProcessamento.append(LABEL_CALCULANDO_PENALIDADES_DO_CONTRATO_ENCERRADO_RESCINDIDO);
			controladorApuracaoPenalidade.processarPenalidadesContrato(selecionado, pontosRemovidos, dadosAuditoria, logProcessamento);
		}
	}

	/**
	 * Finalizar contrato.
	 * 
	 * @param selecionado {@link Contrato}
	 * @param dadosAuditoria {@link DadosAuditoria}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void finalizarContrato(Contrato selecionado, DadosAuditoria dadosAuditoria, HttpServletRequest request, Model model, Funcionario funcionario)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		desativarPontoConsumo(selecionado);
		
		
		if(request.getSession().getAttribute("dadosFaturaResidual") != null) {
			Map<Long, List<String>> dadosFaturaResidual = (Map<Long, List<String>>) request.getSession().getAttribute("dadosFaturaResidual");
			
			controladorContrato.inserirDadosFaturaResidual(dadosFaturaResidual, funcionario != null ? funcionario : dadosAuditoria.getUsuario().getFuncionario());
			
			request.getSession().removeAttribute("dadosFaturaResidual");
			
		}
		
		selecionado.setDadosAuditoria(dadosAuditoria);

		// Verifica a existência e calcula as
		// penalidades associadas ao contrato
		// selecionado.
		if (!controladorApuracaoPenalidade.obterListaPenalidasPorContrato(selecionado.getChavePrimaria()).isEmpty()) {
			StringBuilder logProcessamento = new StringBuilder();
			logProcessamento.append(LABEL_CALCULANDO_PENALIDADES_DO_CONTRATO_ENCERRADO_RESCINDIDO);
			controladorApuracaoPenalidade.processarPenalidadesContrato(selecionado, null, dadosAuditoria, logProcessamento);
		}

		Contrato novo;
		try {
			novo = controladorContrato.popularEncerrarContrato(selecionado, dadosAuditoria);
		} catch (Exception e) {
			throw new GGASException(e);
		}

		// Atualizando situação do contrato
		novo.setSituacao(controladorContrato.obterSituacaoContrato(ENCERRADO));
		novo.setHabilitado(Boolean.FALSE);

		// Atualizando data de recisão do contrato
		String periodoRecisao = request.getParameter(PERIODO_RECISAO_CAMPO);
		if (periodoRecisao != null && !StringUtils.EMPTY.equals(periodoRecisao)) {
			novo.setDataRecisao(Util.converterCampoStringParaData(StringUtils.EMPTY, periodoRecisao, Constantes.FORMATO_DATA_BR));
		} else {
			novo.setDataRecisao(Util.zerarHorario(new DateTime()).toDate());
		}

		if (novo.getDataVencimentoObrigacoes() == null) {
			novo.setDataVencimentoObrigacoes(novo.getDataRecisao());
		}

		// Altera o contrato selecionado, caso ele
		// não esteja encerrado ou a data de
		// recisão tenha mudado.
		if (!novo.getDataRecisao().equals(selecionado.getDataRecisao()) || !novo.getSituacao().equals(selecionado.getSituacao())) {

			if (selecionado.getChavePrimariaPai() == null) {
				novo.setChavePrimariaPai(selecionado.getChavePrimaria());
			} else {
				novo.setChavePrimariaPai(selecionado.getChavePrimariaPai());
			}

			controladorContrato.atualizarContrato(novo, selecionado);
		}

		if (chamadoTela) {
			copiarContrato(novo);
		}

		mensagemSucesso(model, SUCESSO_ENCERRAR_RESCINDIR);

	}

	/**
	 * Desativa ponto de consumo ao encerrar contrato
	 * @param selecionado - {@link Contrato}
	 * @param funcionario 
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public void desativarPontoConsumo(Contrato selecionado) throws ConcorrenciaException, NegocioException {
		ControladorContrato controladorCont = ServiceLocator.getInstancia().getControladorContrato();
		
		ControladorParametroSistema controladorParametro = ServiceLocator.getInstancia()
				.getControladorParametroSistema();
		
		PontoConsumo pontoConsumo = controladorCont.obterPontoConsumoContrato(selecionado);
		if ("1".equals(controladorParametro.obterParametroPorCodigo(Constantes.DESATIVAR_PONTO_CONTRATO).getValor())) {
			ControladorPontoConsumo controladorPonto = ServiceLocator.getInstancia()
					.getControladorPontoConsumo();

			if (pontoConsumo.isHabilitado() == Boolean.TRUE) {
				pontoConsumo.setHabilitado(Boolean.FALSE);
				controladorPonto.atualizarEmBatch(pontoConsumo, PontoConsumo.class);
			}
			
		}
	}

	/**
	 * Método responsável por incluir a nota de débito referente à multa recisória.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirNotaMultaRecisoria")
	public String incluirNotaMultaRecisoria(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			model.addAttribute("contratoVO", contratoVO);
			DadosAuditoria dadosAuditoria = this.getDadosAuditoria(request);

			String dataVencimento = contratoVO.getDataVencimento();
			boolean cobrarIndenizacao = contratoVO.getCobrarIndenizacao();
			Long chavePrimaria = contratoVO.getChavePrimaria();
			String listaIdsPontoConsumoRemovidos = contratoVO.getListaIdsPontoConsumoRemovidos();
			boolean temRemovidos = listaIdsPontoConsumoRemovidos != null && !StringUtils.EMPTY.equals(listaIdsPontoConsumoRemovidos);

			Contrato selecionado = controladorContrato.obterContratoListasCarregadas(chavePrimaria, null);
			selecionado.setClienteAssinatura(
					controladorCliente.obterCliente(selecionado.getClienteAssinatura().getChavePrimaria(), ENDERECO_PRINCIPAL));
			selecionado.setDadosAuditoria(dadosAuditoria);

			String valorCobrado = request.getParameter(VALOR_COBRADO);
			
			Long idPontoConsumoEncerrar = contratoVO.getIdPontoConsumoEncerrar();
			if(idPontoConsumoEncerrar == null) {
				idPontoConsumoEncerrar = 0L;
			}
			
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumoEncerrar, SEGMENTO);
			Collection<Long> idPontosSemMulta = getListaIds(contratoVO, PONTOS_SEM_RECISAO);

			criaNotaDebito(request, model, dadosAuditoria, dataVencimento, cobrarIndenizacao, selecionado, valorCobrado, pontoConsumo,
					idPontosSemMulta);

			this.popularFormContrato(selecionado, contratoVO, request, model);
			model.addAttribute(SITUACAO_CONTRATO, selecionado.getSituacao());
			model.addAttribute(LISTA_PONTO_CONSUMO,
					carregarListaRubrica(selecionado, contratoVO, idPontosSemMulta, listaIdsPontoConsumoRemovidos));

			contratoVO.setPontosSemRecisao(Util.arrayParaString(idPontosSemMulta.toArray(new Long[] {}), Constantes.STRING_VIRGULA));
			contratoVO.setCobrarIndenizacao(true);

			if (temRemovidos) {
				contratoVO.setListaIdsPontoConsumoRemovidos(listaIdsPontoConsumoRemovidos);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, request, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
		}

		return "forward:exibirEncerrarRescindirContrato";
	}

	private void criaNotaDebito(HttpServletRequest request, Model model, DadosAuditoria dadosAuditoria, String dataVencimento,
			boolean cobrarIndenizacao, Contrato selecionado, String valorCobrado, PontoConsumo pontoConsumo,
			Collection<Long> idPontosSemMulta) {
		try {
			if (pontoConsumo != null) {
				if (cobrarIndenizacao) {
					if (valorCobrado != null && !StringUtils.EMPTY.equals(valorCobrado)) {
						valorCobrado = Util.removerVirgulasPontos(valorCobrado.trim());
						BigDecimal valor =
								new BigDecimal(Util.formatarValorMonetarioString(valorCobrado, valorCobrado.length(), DUAS_CASAS_DECIMAIS));

						// Cirando nota de débito
						// no valor da multa
						// recisória do contrato
						// (caso haja) e
						// cancelando o
						// débito anterior de
						// multa recisória para o
						// cliente desse contrato
						// (caso haja).
						controladorContrato.validarDataVencimento(
								Util.converterCampoStringParaData(StringUtils.EMPTY, dataVencimento, FORMATO_DATA_BR));
						controladorFatura.registrarNotaDebitoMultaRecisoria(selecionado, pontoConsumo, valor, dataVencimento);

						mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Fatura.NOTA_DEBITO_CREDITO);
					}
				} else {
					// Cancelando o débito
					// anterior de multa recisória
					// para esse ponto de consumo
					// (caso haja).
					Long idRubricaMultaRecisoria = Long
							.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_MULTA_RECISORIA));
					Rubrica rubrica = (Rubrica) controladorRubrica.obter(idRubricaMultaRecisoria);
					controladorFatura.cancelarNotaCreditoDebitoAnteriorPorClienteRubrica(selecionado, pontoConsumo, rubrica, dadosAuditoria,
							false);

					idPontosSemMulta.add(pontoConsumo.getChavePrimaria());
				}
			}
		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			if (e.getChaveErro() != null) {
				if (e.getParametrosErro() != null) {
					mensagemErroParametrizado(model, e.getChaveErro(), request, e.getParametrosErro());
				} else {
					mensagemErro(model, e.getChaveErro());
				}
			}
		}
	}

	/**
	 * Carregar lista rubrica.
	 *
	 * @param selecionado {@link Contrato}
	 * @param contratoVO {@link ContratoVO}
	 * @param idPontosSemMulta {@link Collection}
	 * @param listaIdsPontoConsumoRemovidos {@link String}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private Collection<PontosConsumoSituacaoRubricaVO> carregarListaRubrica(Contrato selecionado, ContratoVO contratoVO,
			Collection<Long> idPontosSemMulta, String listaIdsPontoConsumoRemovidos) throws GGASException {

		Collection<PontosConsumoSituacaoRubricaVO> listaRubrica = new ArrayList<PontosConsumoSituacaoRubricaVO>();

		Collection<Long> idsPonto = new ArrayList<Long>();

		if (listaIdsPontoConsumoRemovidos != null && !StringUtils.EMPTY.equals(listaIdsPontoConsumoRemovidos)) {
			idsPonto = Arrays.asList(Util.arrayStringParaArrayLong(listaIdsPontoConsumoRemovidos.split(Constantes.STRING_VIRGULA)));
		} else {
			for (ContratoPontoConsumo contratoPontoConsumo : selecionado.getListaContratoPontoConsumo()) {
				idsPonto.add(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
			}
		}

		PontosConsumoSituacaoRubricaVO rubricaVO;
		PontoConsumo ponto;
		boolean multaTratada;
		int qtdMultasTratadas = 0;

		for (Long idPonto : idsPonto) {
			ponto = (PontoConsumo) controladorPontoConsumo.obter(idPonto, IMOVEL);
			rubricaVO = new PontosConsumoSituacaoRubricaVO();

			rubricaVO.setChavePrimaria(ponto.getChavePrimaria());
			rubricaVO.setDescricaoPontoConsumo(ponto.getDescricao());
			rubricaVO.setDescricaoRamoAtividade(ponto.getRamoAtividade().getDescricao());
			rubricaVO.setDescricaoSegmento(ponto.getSegmento().getDescricao());
			rubricaVO.setNomeImovel(ponto.getImovel().getNome());
			rubricaVO.setIdNotaDebito(controladorFatura.pontoConsumoPossuiNotaDebitoRecisao(ponto.getChavePrimaria(), selecionado));

			multaTratada = rubricaVO.getIdNotaDebito() != null || idPontosSemMulta.contains(ponto.getChavePrimaria());
			rubricaVO.setRubrica(multaTratada);

			if (multaTratada) {
				qtdMultasTratadas++;
			}

			listaRubrica.add(rubricaVO);
		}

		contratoVO.setFinalizacaoContrato(listaRubrica.size() == qtdMultasTratadas);

		return listaRubrica;
	}

	/**
	 * Recupera os identificadores dos pontos de consumo cuja multa não será cobrada.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param propriedade {@link String}
	 * @return Collection {@link Collection}
	 */
	private Collection<Long> getListaIds(ContratoVO contratoVO, String propriedade) {

		Collection<Long> listaIdsPropriedade = new ArrayList<Long>();

		String valorPropriedade = (String) contratoVO.getField(propriedade, contratoVO);
		if (valorPropriedade != null && !StringUtils.EMPTY.equals(valorPropriedade)) {
			listaIdsPropriedade.addAll(Arrays.asList(Util.arrayStringParaArrayLong(valorPropriedade.split(Constantes.STRING_VIRGULA))));
		}

		return listaIdsPropriedade;
	}

	/**
	 * Método responsável por imprimir a fatura de encerramento.
	 *
	 * @param contratoVO {@link Contrato}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirFatura")
	public String imprimirFatura(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			Model model) throws GGASException {

		String impressao = request.getParameter(IMPRESSAO);
		String chaveFatura = request.getParameter(CHAVE_PRIMARIA_FATURA);

		if (chaveFatura != null && !StringUtils.EMPTY.equals(chaveFatura)) {
			Fatura fatura = (Fatura) controladorFatura.obter(Long.valueOf(chaveFatura), LISTA_FATURA_ITEM, FATURA_GERAL);
			fatura.setDadosAuditoria(getDadosAuditoria(request));

			String nomeArquivo = StringUtils.EMPTY;
			byte[] relatorio = null;

			if (NOTA_DEBITO.equals(impressao)) {
				nomeArquivo = RELATORIO_NOTA_DEBITO_CREDITO;
				relatorio = controladorFatura.gerarRelatorioNotaDebitoCredito(fatura, Boolean.TRUE);
			} else {
				nomeArquivo = RELATORIO_FATURA_NOTA_FISCAL;
				relatorio = controladorFatura.gerarImpressaoFaturaSegundaVia(fatura, null);
			}

			imprimirPDFRelatorio(nomeArquivo, relatorio, response);
		}
		model.addAttribute(LISTA_IDS_PONTO_CONSUMO_REMOVIDOS, contratoVO.getListaIdsPontoConsumoRemovidos());

		return null;
	}

	/**
	 * Obter fator correcao ptzpc spor faixa pressao.
	 *
	 * @param chaveFaixaPressaoFornecimento {@link Long}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	public String obterFatorCorrecaoPTZPCSporFaixaPressao(Long chaveFaixaPressaoFornecimento) throws GGASException {

		String fatorCorrecao = null;

		if (chaveFaixaPressaoFornecimento != null && chaveFaixaPressaoFornecimento > 0) {
			FaixaPressaoFornecimento faixaPressaoFornecimento =
					(FaixaPressaoFornecimento) controladorFaixaPressaoFornecimento.obter(chaveFaixaPressaoFornecimento);
			if (faixaPressaoFornecimento != null) {
				fatorCorrecao =
						Util.converterCampoValorDecimalParaString(FATOR_CORRECAO, faixaPressaoFornecimento.getNumeroFatorCorrecaoPTZPCS(),
								Constantes.LOCALE_PADRAO, Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
			}
		}

		return fatorCorrecao;
	}

	/**
	 * Exibir ordem faturamento contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirOrdemFaturamentoContrato")
	public String exibirOrdemFaturamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			Contrato contrato = (Contrato) controladorContrato.obter(contratoVO.getChavePrimaria());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_PRIMARIA, contratoVO.getChavePrimaria());
			filtro.put(CHAVE_PRIMARIA_PRINCIPAL, contrato.getChavePrimariaPrincipal());
			Collection<Contrato> listaContratosPrincipalEComplementar = controladorContrato.consultarContratoPrincipalEComplementar(filtro);
			request.getSession().setAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR, listaContratosPrincipalEComplementar);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirOrdemFaturamentoContrato";
	}

	/**
	 * Alterar ordem faturamento contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarOrdemFaturamentoContrato")
	public String alterarOrdemFaturamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String acao = request.getParameter(OPERACAO);
		int index = Integer.parseInt(request.getParameter(INDEX));

		List<Contrato> listaContratos = new ArrayList<Contrato>();

		if (request.getSession().getAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR) != null) {
			listaContratos.addAll((Collection<Contrato>) request.getSession().getAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR));
		}

		Contrato contrato1 = null;
		Contrato contrato2 = null;
		Integer ordem1 = null;
		Integer ordem2 = null;

		if (SUBIR_POSICAO.equals(acao)) {
			contrato1 = listaContratos.get(index);
			contrato2 = listaContratos.get(index - 1);
			ordem1 = contrato1.getOrdemFaturamento();
			ordem2 = contrato2.getOrdemFaturamento();
			contrato1.setOrdemFaturamento(ordem2);
			contrato2.setOrdemFaturamento(ordem1);

			listaContratos.set(index, contrato2);
			listaContratos.set(index - 1, contrato1);

		} else if (DESCER_POSICAO.equals(acao)) {
			contrato1 = listaContratos.get(index);
			contrato2 = listaContratos.get(index + 1);
			ordem1 = contrato1.getOrdemFaturamento();
			ordem2 = contrato2.getOrdemFaturamento();
			contrato1.setOrdemFaturamento(ordem2);
			contrato2.setOrdemFaturamento(ordem1);

			listaContratos.set(index, contrato2);
			listaContratos.set(index + 1, contrato1);
		}

		request.getSession().setAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR, listaContratos);
		model.addAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR, listaContratos);

		return "exibirOrdemFaturamentoContrato";

	}

	/**
	 * Salvar ordem faturamento contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("salvarOrdemFaturamentoContrato")
	public String salvarOrdemFaturamentoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		List<Contrato> listaContratos = new ArrayList<Contrato>();

		try {
			if (request.getSession().getAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR) != null) {
				listaContratos.addAll((Collection<Contrato>) request.getSession().getAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR));
			}
			for (Contrato contrato : listaContratos) {
				Integer ordem = listaContratos.indexOf(contrato) + 1;
				Contrato contratoSalvo = (Contrato) controladorContrato.obter(contrato.getChavePrimaria());
				contratoSalvo.setOrdemFaturamento(ordem);
				contratoSalvo.setDadosAuditoria(getDadosAuditoria(request));
				controladorContrato.atualizar(contratoSalvo);
			}
			request.getSession().setAttribute(LISTA_CONTRATOS_PRINCIPAL_E_COMPLEMENTAR, null);
			mensagemSucesso(model, SUCESSO_ORDENAR_FATURAMENTO_CONTRATO_COMPLEMENTAR);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaContrato(contratoVO, request, model);
	}

	/**
	 * Método responsável por adicionar a Penalidade Retirada Maior/Menor.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarPenalidadeRetiradaMaiorMenorModalidade")
	public String adicionarPenalidadeRetiradaMaiorMenorModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();
		model.addAttribute("contratoVO", contratoVO);
		Penalidade penalidade = null;
		EntidadeConteudo entPeriodicidadeRetiradaMaiorMenor = null;
		EntidadeConteudo entBaseApuracaoRetiradaMaiorMenor = null;

		EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor = null;
		EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor = null;
		EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor = null;

		BigDecimal valorPercentualCobRetMaiorMenor = null;
		BigDecimal valorPercentualCobIntRetMaiorMenor = null;
		BigDecimal valorPercentualRetMaiorMenor = null;

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorModalidadeVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession()
						.getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);

		if (listaPenalidadesRetMaiorMenorModalidadeVO == null) {
			listaPenalidadesRetMaiorMenorModalidadeVO = new ArrayList<PenalidadesRetiradaMaiorMenorVO>();
		}

		ModeloContrato modeloContrato = null;
		if (contratoVO.getIdModeloContrato() != null && contratoVO.getIdModeloContrato() > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(contratoVO.getIdModeloContrato(), ATRIBUTOS, ABAS);
		}

		String[] camposPenalidadePorRetirada = new String[] { PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PERIODICIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE, BASE_APURACAO_RETIRADA_MAIOR_MENOR_MODALIDADE,
				DATA_INICIO_RETIRADA_MAIOR_MENOR_MODALIDADE, DATA_FIM_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PRECO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE, FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE, PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR_MODALIDADE,
				CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR_MODALIDADE, PERCENTUAL_RETIRADA_MAIOR_MENOR_MODALIDADE };

		try {
			Map<String, Object> mapaContratoVO = oMapper.convertValue(contratoVO, Map.class);
			controladorContrato.validarCamposObrigatoriosContrato(mapaContratoVO, Arrays.asList(camposPenalidadePorRetirada),
					modeloContrato);

			if (contratoVO.getPenalidadeRetMaiorMenor() != null && StringUtils.isNotEmpty(contratoVO.getPenalidadeRetMaiorMenor().trim())
					&& !contratoVO.getPenalidadeRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				penalidade = controladorContrato.obterPenalidade(Long.parseLong(contratoVO.getPenalidadeRetMaiorMenor().trim()));
			}

			if (contratoVO.getPeriodicidadeRetMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getPeriodicidadeRetMaiorMenor().trim())
					&& !contratoVO.getPeriodicidadeRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				entPeriodicidadeRetiradaMaiorMenor = controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(
						Contrato.PERIODICIDADE_RETIRADA_MAIOR_MENOR, contratoVO.getPeriodicidadeRetMaiorMenor().trim()));
			}
			if (contratoVO.getBaseApuracaoRetMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getBaseApuracaoRetMaiorMenor().trim())
					&& !contratoVO.getBaseApuracaoRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				entBaseApuracaoRetiradaMaiorMenor = controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(
						LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR, contratoVO.getBaseApuracaoRetMaiorMenor().trim()));
			}

			if (contratoVO.getPrecoCobrancaRetirMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getPrecoCobrancaRetirMaiorMenor().trim())
					&& !contratoVO.getPrecoCobrancaRetirMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				entidadePrecoCobrancaRetiradaMaiorMenor =
						controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(LABEL_TABELA_DE_PRECO_PARA_COBRANCA,
								contratoVO.getPrecoCobrancaRetirMaiorMenor().trim()));
			}
			if (contratoVO.getTipoApuracaoRetirMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getTipoApuracaoRetirMaiorMenor().trim())
					&& !contratoVO.getTipoApuracaoRetirMaiorMenorM().trim().equals(NAO_SELECIONADO)) {
				entidadeTipoApuracaoRetirMaiorMenor =
						controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(LABEL_FORMA_DE_CALCULO_PARA_COBRANCA,
								contratoVO.getTipoApuracaoRetirMaiorMenor().trim()));
			}
			if (contratoVO.getPercentualCobRetMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getPercentualCobRetMaiorMenor().trim())
					&& !contratoVO.getPercentualCobRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				valorPercentualCobRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_DE_COBRANCA,
						contratoVO.getPercentualCobRetMaiorMenor().trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
						.divide(BigDecimal.valueOf(CEM));
			}
			if (contratoVO.getPercentualCobRetMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getPercentualCobRetMaiorMenor().trim())
					&& !contratoVO.getPercentualCobRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				valorPercentualCobIntRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal(
						LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
						contratoVO.getPercentualCobRetMaiorMenor().trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
						.divide(BigDecimal.valueOf(CEM));
			}
			if (contratoVO.getConsumoReferenciaRetMaiorMenor() != null
					&& StringUtils.isNotEmpty(contratoVO.getConsumoReferenciaRetMaiorMenor().trim())
					&& !contratoVO.getConsumoReferenciaRetMaiorMenor().equals(NAO_SELECIONADO)) {
				entidadeConsumoReferenciaRetMaiorMenor =
						controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(LABEL_CONSUMO_REFERENCIA,
								contratoVO.getConsumoReferenciaRetMaiorMenor().trim()));
			}
			if (contratoVO.getPercentualRetMaiorMenor() != null && StringUtils.isNotEmpty(contratoVO.getPercentualRetMaiorMenor().trim())
					&& !contratoVO.getPercentualRetMaiorMenor().trim().equals(NAO_SELECIONADO)) {
				valorPercentualRetMaiorMenor = Util
						.converterCampoStringParaValorBigDecimal(
								LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
								contratoVO.getPercentualRetMaiorMenor().trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
						.divide(BigDecimal.valueOf(CEM));
			}

			if (penalidade != null && entPeriodicidadeRetiradaMaiorMenor != null) {
				PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
				penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidade);
				penalidadesRetiradaMaiorMenorVO.setEntidadePeriodicidadeRetiradaMaiorMenor(entPeriodicidadeRetiradaMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeBaseApuracaoRetiradaMaiorMenor(entBaseApuracaoRetiradaMaiorMenor);
				if (StringUtils.isNotEmpty(contratoVO.getDataIniVigRetirMaiorMenor())
						&& !contratoVO.getDataIniVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
					penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(Util.converterCampoStringParaData(LABEL_DATA_INICIO_VIGENCIA,
							contratoVO.getDataIniVigRetirMaiorMenor(), Constantes.FORMATO_DATA_BR));
				}
				if (StringUtils.isNotEmpty(contratoVO.getDataFimVigRetirMaiorMenor())
						&& !contratoVO.getDataFimVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
					penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(Util.converterCampoStringParaData(LABEL_DATA_FIM_VIGENCIA,
							contratoVO.getDataFimVigRetirMaiorMenor(), Constantes.FORMATO_DATA_BR));
				}

				penalidadesRetiradaMaiorMenorVO.setEntidadePrecoCobrancaRetiradaMaiorMenor(entidadePrecoCobrancaRetiradaMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeTipoApuracaoRetirMaiorMenor(entidadeTipoApuracaoRetirMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeConsumoReferenciaRetMaiorMenor(entidadeConsumoReferenciaRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualCobRetMaiorMenor(valorPercentualCobRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualCobIntRetMaiorMenor(valorPercentualCobIntRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualRetMaiorMenor(valorPercentualRetMaiorMenor);
				if (StringUtils.isNotEmpty(contratoVO.getIndicadorImposto())) {
					penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(Boolean.valueOf(contratoVO.getIndicadorImposto()));
				}
				validarAdicaoNovaPenalidadeRetiradaMaiorMenorModalidade(penalidadesRetiradaMaiorMenorVO,
						listaPenalidadesRetMaiorMenorModalidadeVO);

				if (!listaPenalidadesRetMaiorMenorModalidadeVO.contains(penalidadesRetiradaMaiorMenorVO)) {
					listaPenalidadesRetMaiorMenorModalidadeVO.add(penalidadesRetiradaMaiorMenorVO);
				}

			}

			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO,
					listaPenalidadesRetMaiorMenorModalidadeVO);
			request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidadesRetMaiorMenorModalidadeVO);
			request.setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
					request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

			contratoVO.setPenalidadeRetMaiorMenorM(NAO_SELECIONADO);
			contratoVO.setPeriodicidadeRetMaiorMenorM(NAO_SELECIONADO);
			contratoVO.setPrecoCobrancaRetirMaiorMenorM(NAO_SELECIONADO);
			contratoVO.setTipoApuracaoRetirMaiorMenorM(NAO_SELECIONADO);
			contratoVO.setConsumoReferRetMaiorMenorM(NAO_SELECIONADO);
			contratoVO.setDataIniVigRetirMaiorMenorM(StringUtils.EMPTY);
			contratoVO.setDataFimVigRetirMaiorMenorM(StringUtils.EMPTY);
			contratoVO.setPercentualCobRetMaiorMenorM(StringUtils.EMPTY);
			contratoVO.setPercentualRetMaiorMenorM(StringUtils.EMPTY);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if (modeloContrato != null) {
			this.carregarDadosModelo(contratoVO, modeloContrato, model);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Método responsável por adicionar a Penalidade Retirada Maior/Menor.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarPenalidadeRetiradaMaiorMenorModalidadeAditamento")
	public String adicionarPenalidadeRetiradaMaiorMenorModalidadeAditamento(ContratoVO contratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();
		model.addAttribute("contratoVO", contratoVO);
		Penalidade penalidade = null;
		EntidadeConteudo entPeriodicidadeRetiradaMaiorMenor = null;
		EntidadeConteudo entBaseApuracaoRetiradaMaiorMenor = null;

		EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor = null;
		EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor = null;
		EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor = null;

		BigDecimal valorPercentualCobRetMaiorMenor = null;
		BigDecimal valorPercentualCobIntRetMaiorMenor = null;
		BigDecimal valorPercentualRetMaiorMenor = null;

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorModalidadeVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession()
						.getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);

		if (listaPenalidadesRetMaiorMenorModalidadeVO == null) {
			listaPenalidadesRetMaiorMenorModalidadeVO = new ArrayList<PenalidadesRetiradaMaiorMenorVO>();
		}

		ModeloContrato modeloContrato = null;
		if (contratoVO.getIdModeloContrato() != null && contratoVO.getIdModeloContrato() > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(contratoVO.getIdModeloContrato(), ATRIBUTOS, ABAS);
		}

		String[] camposPenalidadePorRetirada = new String[] { PENALIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PERIODICIDADE_RETIRADA_MAIOR_MENOR_MODALIDADE, BASE_APURACAO_RETIRADA_MAIOR_MENOR_MODALIDADE,
				DATA_INICIO_RETIRADA_MAIOR_MENOR_MODALIDADE, DATA_FIM_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PRECO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE, FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE,
				PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR_MODALIDADE, PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR_MODALIDADE,
				CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR_MODALIDADE, PERCENTUAL_RETIRADA_MAIOR_MENOR_MODALIDADE };

		Map<String, Object> mapaContratoVO = oMapper.convertValue(contratoVO, Map.class);
		controladorContrato.validarCamposObrigatoriosContrato(mapaContratoVO, Arrays.asList(camposPenalidadePorRetirada), modeloContrato);

		if (StringUtils.isNotEmpty(contratoVO.getPenalidadeRetMaiorMenor())
				&& !contratoVO.getPenalidadeRetMaiorMenor().equals(NAO_SELECIONADO)) {
			penalidade = controladorContrato.obterPenalidade(Long.parseLong(contratoVO.getPenalidadeRetMaiorMenor()));

		}

		if (StringUtils.isNotEmpty(contratoVO.getPeriodicidadeRetMaiorMenor())
				&& !contratoVO.getPeriodicidadeRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entPeriodicidadeRetiradaMaiorMenor =
					controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_RETIRADA_MAIOR_MENOR,
							contratoVO.getPeriodicidadeRetMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getBaseApuracaoRetMaiorMenor())
				&& !contratoVO.getBaseApuracaoRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entBaseApuracaoRetiradaMaiorMenor =
					controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR,
							contratoVO.getBaseApuracaoRetMaiorMenor()));
		}

		if (StringUtils.isNotEmpty(contratoVO.getPrecoCobrancaRetirMaiorMenor())
				&& !contratoVO.getPrecoCobrancaRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadePrecoCobrancaRetiradaMaiorMenor = controladorEntidadeConteudo.obter(Util
					.converterCampoStringParaValorLong(LABEL_TABELA_DE_PRECO_PARA_COBRANCA, contratoVO.getPrecoCobrancaRetirMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getTipoApuracaoRetirMaiorMenor())
				&& !contratoVO.getTipoApuracaoRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadeTipoApuracaoRetirMaiorMenor = controladorEntidadeConteudo.obter(Util
					.converterCampoStringParaValorLong(LABEL_FORMA_DE_CALCULO_PARA_COBRANCA, contratoVO.getTipoApuracaoRetirMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualCobRetMaiorMenor())
				&& !contratoVO.getPercentualCobRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualCobRetMaiorMenor =
					Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_DE_COBRANCA, contratoVO.getPercentualCobRetMaiorMenor(),
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualCobIntRetMaiorMenor())
				&& !contratoVO.getPercentualCobIntRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualCobIntRetMaiorMenor = Util
					.converterCampoStringParaValorBigDecimal(
							LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
							contratoVO.getPercentualCobIntRetMaiorMenor(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
					.divide(BigDecimal.valueOf(CEM));
		}
		if (StringUtils.isNotEmpty(contratoVO.getConsumoReferenciaRetMaiorMenor())
				&& !contratoVO.getConsumoReferenciaRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadeConsumoReferenciaRetMaiorMenor = controladorEntidadeConteudo.obter(
					Util.converterCampoStringParaValorLong(LABEL_CONSUMO_REFERENCIA, contratoVO.getConsumoReferenciaRetMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualRetMaiorMenor())
				&& !contratoVO.getPercentualRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualRetMaiorMenor = Util
					.converterCampoStringParaValorBigDecimal(
							LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
							contratoVO.getPercentualRetMaiorMenor(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
					.divide(BigDecimal.valueOf(CEM));
		}

		if (penalidade != null && entPeriodicidadeRetiradaMaiorMenor != null) {
			PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
			penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidade);
			penalidadesRetiradaMaiorMenorVO.setEntidadePeriodicidadeRetiradaMaiorMenor(entPeriodicidadeRetiradaMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeBaseApuracaoRetiradaMaiorMenor(entBaseApuracaoRetiradaMaiorMenor);
			if (StringUtils.isNotEmpty(contratoVO.getDataIniVigRetirMaiorMenor())
					&& !contratoVO.getDataIniVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
				penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(Util.converterCampoStringParaData(LABEL_DATA_INICIO_VIGENCIA,
						contratoVO.getDataIniVigRetirMaiorMenor(), Constantes.FORMATO_DATA_BR));
			}
			if (StringUtils.isNotEmpty(contratoVO.getDataFimVigRetirMaiorMenor())
					&& !contratoVO.getDataFimVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
				penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(Util.converterCampoStringParaData(LABEL_DATA_FIM_VIGENCIA,
						contratoVO.getDataFimVigRetirMaiorMenor(), Constantes.FORMATO_DATA_BR));
			}

			penalidadesRetiradaMaiorMenorVO.setEntidadePrecoCobrancaRetiradaMaiorMenor(entidadePrecoCobrancaRetiradaMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeTipoApuracaoRetirMaiorMenor(entidadeTipoApuracaoRetirMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeConsumoReferenciaRetMaiorMenor(entidadeConsumoReferenciaRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualCobRetMaiorMenor(valorPercentualCobRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualCobIntRetMaiorMenor(valorPercentualCobIntRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualRetMaiorMenor(valorPercentualRetMaiorMenor);
			if (StringUtils.isNotEmpty(contratoVO.getIndicadorImpostoM())) {
				penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(Boolean.valueOf(contratoVO.getIndicadorImpostoM()));
			}
			validarAdicaoNovaPenalidadeRetiradaMaiorMenorModalidade(penalidadesRetiradaMaiorMenorVO,
					listaPenalidadesRetMaiorMenorModalidadeVO);

			if (!listaPenalidadesRetMaiorMenorModalidadeVO.contains(penalidadesRetiradaMaiorMenorVO)) {
				listaPenalidadesRetMaiorMenorModalidadeVO.add(penalidadesRetiradaMaiorMenorVO);
			}

		}

		request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidadesRetMaiorMenorModalidadeVO);
		request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidadesRetMaiorMenorModalidadeVO);
		request.setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

		contratoVO.setPenalidadeRetMaiorMenorM(NAO_SELECIONADO);
		contratoVO.setPeriodicidadeRetMaiorMenorM(NAO_SELECIONADO);
		contratoVO.setPrecoCobrancaRetirMaiorMenorM(NAO_SELECIONADO);
		contratoVO.setTipoApuracaoRetirMaiorMenorM(NAO_SELECIONADO);
		contratoVO.setConsumoReferRetMaiorMenorM(NAO_SELECIONADO);
		contratoVO.setDataIniVigRetirMaiorMenorM(StringUtils.EMPTY);
		contratoVO.setDataFimVigRetirMaiorMenorM(StringUtils.EMPTY);
		contratoVO.setPercentualCobRetMaiorMenorM(StringUtils.EMPTY);
		contratoVO.setPercentualRetMaiorMenorM(StringUtils.EMPTY);

		if (modeloContrato != null) {
			this.carregarDadosModelo(contratoVO, modeloContrato, model);
		}
		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);

	}

	/**
	 * Validar adicao nova penalidade retirada maior menor modalidade.
	 *
	 */
	private void validarAdicaoNovaPenalidadeRetiradaMaiorMenorModalidade(PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO,
			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorModalidadeVO) {

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
				converterPenalidadeRetiradaMaiorMenorModalidadeVO(penalidadesRetiradaMaiorMenorVO);
		Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = new ArrayList<ContratoPontoConsumoPenalidade>();
		for (PenalidadesRetiradaMaiorMenorVO penModalidade : listaPenalidadesRetMaiorMenorModalidadeVO) {
			ContratoPontoConsumoPenalidade penalidade = converterPenalidadeRetiradaMaiorMenorModalidadeVO(penModalidade);
			listaContratoPontoConsumoPenalidade.add(penalidade);
		}

		// TODO:fazer a validação
		if (contratoPontoConsumoPenalidade != null) {
			// Ainda será implementado, Por favor retirar o comentário após a implementação.
		}

	}

	/**
	 * Converter penalidade retirada maior menor modalidade vo.
	 *
	 */
	private ContratoPontoConsumoPenalidade converterPenalidadeRetiradaMaiorMenorModalidadeVO(
			PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO) {

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade;
		contratoPontoConsumoPenalidade = (ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();

		contratoPontoConsumoPenalidade.setPenalidade(penalidadesRetiradaMaiorMenorVO.getPenalidadeRetiradaMaiorMenor());
		contratoPontoConsumoPenalidade
				.setPeriodicidadePenalidade(penalidadesRetiradaMaiorMenorVO.getEntidadePeriodicidadeRetiradaMaiorMenor());
		contratoPontoConsumoPenalidade.setBaseApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeBaseApuracaoRetiradaMaiorMenor());
		contratoPontoConsumoPenalidade.setTipoApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeTipoApuracaoRetirMaiorMenor());
		contratoPontoConsumoPenalidade.setDataInicioVigencia(penalidadesRetiradaMaiorMenorVO.getDataInicioVigencia());
		contratoPontoConsumoPenalidade.setDataFimVigencia(penalidadesRetiradaMaiorMenorVO.getDataFimVigencia());
		return contratoPontoConsumoPenalidade;
	}

	/**
	 * Método responsável por adicionar a Penalidade Retirada Maior/Menor.
	 * 
	 * @param contratoVO {@link ContratoVO} {@link ContratoVO}
	 * @param result {@link BindingResult} {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarPenalidadeRetiradaMaiorMenorGeral")
	public String adicionarPenalidadeRetiradaMaiorMenorGeral(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();
		model.addAttribute("contratoVO", contratoVO);
		model.addAttribute(ABA_ATUAL, ABA_PENALIDADES);

		Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenor();

		String dataAssinatura = contratoVO.getDataAssinatura();
		String vencimentoObrigacoesContratuais = contratoVO.getDataVencObrigacoesContratuais();

		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String penalidadeRetMaiorMenor = contratoVO.getPenalidadeRetMaiorMenor();
		String periodicidadeRetMaiorMenor = contratoVO.getPeriodicidadeRetMaiorMenor();
		String baseApuracaoRetMaiorMenor = contratoVO.getBaseApuracaoRetMaiorMenor();
		String dataIniVigRetirMaiorMenor = contratoVO.getDataIniVigRetirMaiorMenor();
		String dataFimVigRetirMaiorMenor = contratoVO.getDataFimVigRetirMaiorMenor();

		String precoCobrancaRetirMaiorMenor = contratoVO.getPrecoCobrancaRetirMaiorMenor();

		String tipoApuracaoRetirMaiorMenor = contratoVO.getTipoApuracaoRetirMaiorMenor();

		String percentualCobRetMaiorMenor = contratoVO.getPercentualCobRetMaiorMenor();
		String percentualCobIntRetMaiorMenor = contratoVO.getPercentualCobIntRetMaiorMenor();
		String consumoReferenciaRetMaiorMenor = contratoVO.getConsumoReferenciaRetMaiorMenor();
		String percentualRetMaiorMenor = contratoVO.getPercentualRetMaiorMenor();
		String indicadorImposto = contratoVO.getIndicadorImposto();

		Penalidade penalidade = null;
		EntidadeConteudo entPeriodicidadeRetiradaMaiorMenor = null;
		EntidadeConteudo entBaseApuracaoRetiradaMaiorMenor = null;

		EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor = null;
		EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor = null;
		EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor = null;

		BigDecimal valorPercentualCobRetMaiorMenor = null;
		BigDecimal valorPercentualCobIntRetMaiorMenor = null;
		BigDecimal valorPercentualRetMaiorMenor = null;

		try {

			controladorContrato.validarDataAssinaturaContrato(dataAssinatura);

			Date dataAssinaturaContrato =
					Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, dataAssinatura, Constantes.FORMATO_DATA_BR);
			Date dataVencimentoContrato = null;
			if (!vencimentoObrigacoesContratuais.isEmpty()) {
				dataVencimentoContrato = Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, vencimentoObrigacoesContratuais,
						Constantes.FORMATO_DATA_BR);
			}

			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO =
					(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession()
							.getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
			model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (listaPenalidadesRetMaiorMenorVO == null) {
				listaPenalidadesRetMaiorMenorVO = new ArrayList<PenalidadesRetiradaMaiorMenorVO>();
			}

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}

			String[] camposPenalidadePorRetirada = new String[] { PENALIDADE_RETIRADA_MAIOR_MENOR, PERIODICIDADE_RETIRADA_MAIOR_MENOR,
					BASE_APURACAO_RETIRADA_MAIOR_MENOR, DATA_INICIO_RETIRADA_MAIOR_MENOR, DATA_FIM_RETIRADA_MAIOR_MENOR,
					PRECO_COBRANCA_RETIRADA_MAIOR_MENOR, FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR,
					PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR, PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR,
					CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR, PERCENTUAL_RETIRADA_MAIOR_MENOR };

			controladorContrato.validarCamposObrigatoriosContrato(oMapper.convertValue(contratoVO, Map.class),
					Arrays.asList(camposPenalidadePorRetirada), modeloContrato);

			Date dataInicioVigencia = null;
			if (StringUtils.isNotEmpty(dataIniVigRetirMaiorMenor) && !dataIniVigRetirMaiorMenor.equals(NAO_SELECIONADO)) {
				dataInicioVigencia = Util.converterCampoStringParaData(LABEL_DATA_INICIO_VIGENCIA, dataIniVigRetirMaiorMenor,
						Constantes.FORMATO_DATA_BR);
			}

			Date dataFimVigencia = null;
			if (StringUtils.isNotEmpty(dataFimVigRetirMaiorMenor) && !dataFimVigRetirMaiorMenor.equals(NAO_SELECIONADO)) {
				dataFimVigencia =
						Util.converterCampoStringParaData(LABEL_DATA_FIM_VIGENCIA, dataFimVigRetirMaiorMenor, Constantes.FORMATO_DATA_BR);
			}

			if (dataInicioVigencia != null && dataFimVigencia != null && dataInicioVigencia.compareTo(dataFimVigencia) > 0) {
				throw new GGASException(ControladorPerfilParcelamento.ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO, true);
			}

			if (penalidadeRetMaiorMenor != null && StringUtils.isNotEmpty(penalidadeRetMaiorMenor.trim())
					&& !penalidadeRetMaiorMenor.equals(NAO_SELECIONADO)) {
				penalidade = controladorContrato.obterPenalidade(Long.parseLong(penalidadeRetMaiorMenor.trim()));

			}

			if (periodicidadeRetMaiorMenor != null && StringUtils.isNotEmpty(periodicidadeRetMaiorMenor.trim())
					&& !periodicidadeRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				entPeriodicidadeRetiradaMaiorMenor = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_RETIRADA_MAIOR_MENOR, periodicidadeRetMaiorMenor.trim()));
			}
			if (baseApuracaoRetMaiorMenor != null && StringUtils.isNotEmpty(baseApuracaoRetMaiorMenor.trim())
					&& !baseApuracaoRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				entBaseApuracaoRetiradaMaiorMenor = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR, baseApuracaoRetMaiorMenor.trim()));
			}

			if (precoCobrancaRetirMaiorMenor != null && StringUtils.isNotEmpty(precoCobrancaRetirMaiorMenor.trim())
					&& !precoCobrancaRetirMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				entidadePrecoCobrancaRetiradaMaiorMenor = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(LABEL_TABELA_DE_PRECO_PARA_COBRANCA, precoCobrancaRetirMaiorMenor.trim()));
			}
			if (tipoApuracaoRetirMaiorMenor != null && StringUtils.isNotEmpty(tipoApuracaoRetirMaiorMenor.trim())
					&& !tipoApuracaoRetirMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				entidadeTipoApuracaoRetirMaiorMenor = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(LABEL_FORMA_DE_CALCULO_PARA_COBRANCA, tipoApuracaoRetirMaiorMenor.trim()));
			}
			if (percentualCobRetMaiorMenor != null && StringUtils.isNotEmpty(percentualCobRetMaiorMenor.trim())
					&& !percentualCobRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				valorPercentualCobRetMaiorMenor =
						Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_DE_COBRANCA, percentualCobRetMaiorMenor.trim(),
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}
			if (percentualCobIntRetMaiorMenor != null && StringUtils.isNotEmpty(percentualCobIntRetMaiorMenor.trim())
					&& !percentualCobIntRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				valorPercentualCobIntRetMaiorMenor = Util
						.converterCampoStringParaValorBigDecimal(
								LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
								percentualCobIntRetMaiorMenor.trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
						.divide(BigDecimal.valueOf(CEM));
			}
			if (consumoReferenciaRetMaiorMenor != null && StringUtils.isNotEmpty(consumoReferenciaRetMaiorMenor.trim())
					&& !consumoReferenciaRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				entidadeConsumoReferenciaRetMaiorMenor = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(LABEL_CONSUMO_REFERENCIA, consumoReferenciaRetMaiorMenor.trim()));
			}
			if (percentualRetMaiorMenor != null && StringUtils.isNotEmpty(percentualRetMaiorMenor.trim())
					&& !percentualRetMaiorMenor.trim().equals(NAO_SELECIONADO)) {
				valorPercentualRetMaiorMenor = Util
						.converterCampoStringParaValorBigDecimal(
								LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
								percentualRetMaiorMenor.trim(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
						.divide(BigDecimal.valueOf(CEM));
			}

			if (penalidade != null && entPeriodicidadeRetiradaMaiorMenor != null) {
				PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
				penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidade);
				penalidadesRetiradaMaiorMenorVO.setEntidadePeriodicidadeRetiradaMaiorMenor(entPeriodicidadeRetiradaMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeBaseApuracaoRetiradaMaiorMenor(entBaseApuracaoRetiradaMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(dataInicioVigencia);
				penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(dataFimVigencia);

				penalidadesRetiradaMaiorMenorVO.setEntidadePrecoCobrancaRetiradaMaiorMenor(entidadePrecoCobrancaRetiradaMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeTipoApuracaoRetirMaiorMenor(entidadeTipoApuracaoRetirMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setEntidadeConsumoReferenciaRetMaiorMenor(entidadeConsumoReferenciaRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualCobRetMaiorMenor(valorPercentualCobRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualCobIntRetMaiorMenor(valorPercentualCobIntRetMaiorMenor);
				penalidadesRetiradaMaiorMenorVO.setValorPercentualRetMaiorMenor(valorPercentualRetMaiorMenor);
				if (StringUtils.isNotEmpty(indicadorImposto)) {
					penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(Boolean.valueOf(indicadorImposto));
				}

				Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);
				Contrato contrato = null;
				if (contratoSessao != null) {
					contrato = contratoSessao;
				} else {
					contrato = (Contrato) controladorContrato.criar();
					contrato.setDataAssinatura(dataAssinaturaContrato);
					contrato.setDataVencimentoObrigacoes(dataVencimentoContrato);
				}
				penalidadesRetiradaMaiorMenorVO.setContrato(contrato);

				validarAdicaoNovaPenalidadeRetiradaMaiorMenorContrato(penalidadesRetiradaMaiorMenorVO, listaPenalidadesRetMaiorMenorVO);

				if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor.length > 0
						&& indicePenalidadeRetiradaMaiorMenor[0] != null) {

					List<PenalidadesRetiradaMaiorMenorVO> listaPenalidades =
							(List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidadesRetMaiorMenorVO;
					listaPenalidades.set(indicePenalidadeRetiradaMaiorMenor[0], penalidadesRetiradaMaiorMenorVO);

					listaPenalidadesRetMaiorMenorVO = listaPenalidades;

				} else {
					if (!listaPenalidadesRetMaiorMenorVO.contains(penalidadesRetiradaMaiorMenorVO)) {
						listaPenalidadesRetMaiorMenorVO.add(penalidadesRetiradaMaiorMenorVO);
					} else {
						throw new GGASException(Constantes.ERRO_NEGOCIO_PERIODICIDADE_DUPLICIDADE);
					}
				}

			}

			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
			model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

			contratoVO.setPenalidadeRetMaiorMenor(NAO_SELECIONADO);
			contratoVO.setPeriodicidadeRetMaiorMenor(NAO_SELECIONADO);
			contratoVO.setBaseApuracaoRetMaiorMenor(NAO_SELECIONADO);
			contratoVO.setPrecoCobrancaRetirMaiorMenor(NAO_SELECIONADO);
			contratoVO.setTipoApuracaoRetirMaiorMenor(NAO_SELECIONADO);
			contratoVO.setConsumoReferenciaRetMaiorMenor(NAO_SELECIONADO);

			contratoVO.setDataIniVigRetirMaiorMenor(StringUtils.EMPTY);
			contratoVO.setDataFimVigRetirMaiorMenor(StringUtils.EMPTY);
			contratoVO.setPercentualCobRetMaiorMenor(StringUtils.EMPTY);
			contratoVO.setPercentualCobIntRetMaiorMenor(StringUtils.EMPTY);
			contratoVO.setPercentualRetMaiorMenor(StringUtils.EMPTY);

			this.carregarDadosModelo(contratoVO, modeloContrato, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Método responsável por adicionar a Penalidade Retirada Maior/Menor.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarPenalidadeRetiradaMaiorMenorGeralAditamento")
	public String adicionarPenalidadeRetiradaMaiorMenorGeralAditamento(ContratoVO contratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();
		model.addAttribute("contratoVO", contratoVO);
		Penalidade penalidade = null;
		EntidadeConteudo entPeriodicidadeRetiradaMaiorMenor = null;
		EntidadeConteudo entBaseApuracaoRetiradaMaiorMenor = null;

		EntidadeConteudo entidadePrecoCobrancaRetiradaMaiorMenor = null;
		EntidadeConteudo entidadeTipoApuracaoRetirMaiorMenor = null;
		EntidadeConteudo entidadeConsumoReferenciaRetMaiorMenor = null;

		BigDecimal valorPercentualCobRetMaiorMenor = null;
		BigDecimal valorPercentualCobIntRetMaiorMenor = null;
		BigDecimal valorPercentualRetMaiorMenor = null;

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
		request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		if (listaPenalidadesRetMaiorMenorVO == null) {
			listaPenalidadesRetMaiorMenorVO = new ArrayList<PenalidadesRetiradaMaiorMenorVO>();
		}

		ModeloContrato modeloContrato = null;
		if (contratoVO.getIdModeloContrato() != null && contratoVO.getIdModeloContrato() > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(contratoVO.getIdModeloContrato(), ATRIBUTOS, ABAS);
		}

		String[] camposPenalidadePorRetirada = new String[] { PENALIDADE_RETIRADA_MAIOR_MENOR, PERIODICIDADE_RETIRADA_MAIOR_MENOR,
				BASE_APURACAO_RETIRADA_MAIOR_MENOR, DATA_INICIO_RETIRADA_MAIOR_MENOR, DATA_FIM_RETIRADA_MAIOR_MENOR,
				PRECO_COBRANCA_RETIRADA_MAIOR_MENOR, FORMA_CALCULO_COBRANCA_RETIRADA_MAIOR_MENOR, PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR,
				PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR, CONSUMO_REFERENCIA_RETIRADA_MAIOR_MENOR,
				PERCENTUAL_RETIRADA_MAIOR_MENOR };

		Map<String, Object> mapaContratoVO = oMapper.convertValue(contratoVO, Map.class);
		controladorContrato.validarCamposObrigatoriosContrato(mapaContratoVO, Arrays.asList(camposPenalidadePorRetirada), modeloContrato);

		Date dataInicioVigencia = null;
		if (StringUtils.isNotEmpty(contratoVO.getDataIniVigRetirMaiorMenor())
				&& !contratoVO.getDataIniVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			dataInicioVigencia = Util.converterCampoStringParaData(LABEL_DATA_INICIO_VIGENCIA, contratoVO.getDataIniVigRetirMaiorMenor(),
					Constantes.FORMATO_DATA_BR);
		}

		Date dataFimVigencia = null;
		if (StringUtils.isNotEmpty(contratoVO.getDataFimVigRetirMaiorMenor())
				&& !contratoVO.getDataFimVigRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			dataFimVigencia = Util.converterCampoStringParaData(LABEL_DATA_FIM_VIGENCIA, contratoVO.getDataFimVigRetirMaiorMenor(),
					Constantes.FORMATO_DATA_BR);
		}

		if (dataInicioVigencia != null && dataFimVigencia != null && dataInicioVigencia.compareTo(dataFimVigencia) > 0) {
			throw new GGASException(ControladorPerfilParcelamento.ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO, true);
		}

		if (StringUtils.isNotEmpty(contratoVO.getPenalidadeRetMaiorMenor())
				&& !contratoVO.getPenalidadeRetMaiorMenor().equals(NAO_SELECIONADO)) {
			penalidade = (Penalidade) controladorContrato.obter(Long.parseLong(contratoVO.getPenalidadeRetMaiorMenor()));

		}

		if (StringUtils.isNotEmpty(contratoVO.getPeriodicidadeRetMaiorMenor())
				&& !contratoVO.getPeriodicidadeRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entPeriodicidadeRetiradaMaiorMenor =
					controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_RETIRADA_MAIOR_MENOR,
							contratoVO.getPeriodicidadeRetMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getBaseApuracaoRetMaiorMenor())
				&& !contratoVO.getBaseApuracaoRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entBaseApuracaoRetiradaMaiorMenor =
					controladorEntidadeConteudo.obter(Util.converterCampoStringParaValorLong(LABEL_BASE_DE_APURACAO_RETIRADA_MAIOR_MENOR,
							contratoVO.getBaseApuracaoRetMaiorMenor()));
		}

		if (StringUtils.isNotEmpty(contratoVO.getPrecoCobrancaRetirMaiorMenor())
				&& !contratoVO.getPrecoCobrancaRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadePrecoCobrancaRetiradaMaiorMenor = controladorEntidadeConteudo.obter(Util
					.converterCampoStringParaValorLong(LABEL_TABELA_DE_PRECO_PARA_COBRANCA, contratoVO.getPrecoCobrancaRetirMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getTipoApuracaoRetirMaiorMenor())
				&& !contratoVO.getTipoApuracaoRetirMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadeTipoApuracaoRetirMaiorMenor = controladorEntidadeConteudo.obter(Util
					.converterCampoStringParaValorLong(LABEL_FORMA_DE_CALCULO_PARA_COBRANCA, contratoVO.getTipoApuracaoRetirMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualCobRetMaiorMenor())
				&& !contratoVO.getPercentualCobRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualCobRetMaiorMenor =
					Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_DE_COBRANCA, contratoVO.getPercentualCobRetMaiorMenor(),
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualCobIntRetMaiorMenor())
				&& !contratoVO.getPercentualCobIntRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualCobIntRetMaiorMenor = Util
					.converterCampoStringParaValorBigDecimal(
							LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
							contratoVO.getPercentualCobIntRetMaiorMenor(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
					.divide(BigDecimal.valueOf(CEM));
		}
		if (StringUtils.isNotEmpty(contratoVO.getConsumoReferenciaRetMaiorMenor())
				&& !contratoVO.getConsumoReferenciaRetMaiorMenor().equals(NAO_SELECIONADO)) {
			entidadeConsumoReferenciaRetMaiorMenor = controladorEntidadeConteudo.obter(
					Util.converterCampoStringParaValorLong(LABEL_CONSUMO_REFERENCIA, contratoVO.getConsumoReferenciaRetMaiorMenor()));
		}
		if (StringUtils.isNotEmpty(contratoVO.getPercentualRetMaiorMenor())
				&& !contratoVO.getPercentualRetMaiorMenor().equals(NAO_SELECIONADO)) {
			valorPercentualRetMaiorMenor = Util
					.converterCampoStringParaValorBigDecimal(
							LABEL_PERCENTUAL_DE_COBRANCA_PARA_CONSUMO_SUPERIOR_DURANTE_AVISO_DE_INTERRUPCAO,
							contratoVO.getPercentualRetMaiorMenor(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO)
					.divide(BigDecimal.valueOf(CEM));
		}

		if (penalidade != null && entPeriodicidadeRetiradaMaiorMenor != null) {
			PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO = new PenalidadesRetiradaMaiorMenorVO();
			penalidadesRetiradaMaiorMenorVO.setPenalidadeRetiradaMaiorMenor(penalidade);
			penalidadesRetiradaMaiorMenorVO.setEntidadePeriodicidadeRetiradaMaiorMenor(entPeriodicidadeRetiradaMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeBaseApuracaoRetiradaMaiorMenor(entBaseApuracaoRetiradaMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setDataInicioVigencia(dataInicioVigencia);
			penalidadesRetiradaMaiorMenorVO.setDataFimVigencia(dataFimVigencia);

			penalidadesRetiradaMaiorMenorVO.setEntidadePrecoCobrancaRetiradaMaiorMenor(entidadePrecoCobrancaRetiradaMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeTipoApuracaoRetirMaiorMenor(entidadeTipoApuracaoRetirMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setEntidadeConsumoReferenciaRetMaiorMenor(entidadeConsumoReferenciaRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualCobRetMaiorMenor(valorPercentualCobRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualCobIntRetMaiorMenor(valorPercentualCobIntRetMaiorMenor);
			penalidadesRetiradaMaiorMenorVO.setValorPercentualRetMaiorMenor(valorPercentualRetMaiorMenor);
			if (StringUtils.isNotEmpty(contratoVO.getIndicadorImposto())) {
				penalidadesRetiradaMaiorMenorVO.setIndicadorImposto(Boolean.valueOf(contratoVO.getIndicadorImposto()));
			}

			validarAdicaoNovaPenalidadeRetiradaMaiorMenorContrato(penalidadesRetiradaMaiorMenorVO, listaPenalidadesRetMaiorMenorVO);

			if (!listaPenalidadesRetMaiorMenorVO.contains(penalidadesRetiradaMaiorMenorVO)) {
				listaPenalidadesRetMaiorMenorVO.add(penalidadesRetiradaMaiorMenorVO);
			}

		}

		request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
		request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

		contratoVO.setPenalidadeRetMaiorMenor(NAO_SELECIONADO);
		contratoVO.setPeriodicidadeRetMaiorMenor(NAO_SELECIONADO);
		contratoVO.setBaseApuracaoRetMaiorMenor(NAO_SELECIONADO);
		contratoVO.setPrecoCobrancaRetirMaiorMenor(NAO_SELECIONADO);
		contratoVO.setTipoApuracaoRetirMaiorMenor(NAO_SELECIONADO);
		contratoVO.setConsumoReferenciaRetMaiorMenor(NAO_SELECIONADO);

		contratoVO.setDataIniVigRetirMaiorMenor(StringUtils.EMPTY);
		contratoVO.setDataFimVigRetirMaiorMenor(StringUtils.EMPTY);
		contratoVO.setPercentualCobRetMaiorMenor(StringUtils.EMPTY);
		contratoVO.setPercentualCobIntRetMaiorMenor(StringUtils.EMPTY);
		contratoVO.setPercentualRetMaiorMenor(StringUtils.EMPTY);
		this.carregarDadosModelo(contratoVO, modeloContrato, model);

		return reexibirAditamentoContrato(contratoVO, result, request, model);

	}

	/**
	 * Excluir penalidade retirada maior menor geral.
	 * 
	 * @param contratoVO {@link ContratoVO} {@link ContratoVO}
	 * @param result {@link BindingResult} {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirPenalidadeRetiradaMaiorMenorGeral")
	public String excluirPenalidadeRetiradaMaiorMenorGeral(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_PENALIDADES);

		Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenor();

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor.length > 0
				&& listaPenalidadesRetMaiorMenorVO != null && !listaPenalidadesRetMaiorMenorVO.isEmpty()) {
			for (int i = indicePenalidadeRetiradaMaiorMenor.length - 1; i >= 0; i--) {
				((List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidadesRetMaiorMenorVO)
						.remove(indicePenalidadeRetiradaMaiorMenor[i].intValue());

			}
			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
		}

		model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Excluir penalidade retirada maior menor geral.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirPenalidadeRetiradaMaiorMenorGeralAditamento")
	public String excluirPenalidadeRetiradaMaiorMenorGeralAditamento(ContratoVO contratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenor();

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor.length > 0
				&& listaPenalidadesRetMaiorMenorVO != null && !listaPenalidadesRetMaiorMenorVO.isEmpty()) {
			for (int i = indicePenalidadeRetiradaMaiorMenor.length - 1; i >= 0; i--) {
				((List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidadesRetMaiorMenorVO)
						.remove(indicePenalidadeRetiradaMaiorMenor[i].intValue());

			}
			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);
		}

		model.addAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO, listaPenalidadesRetMaiorMenorVO);

		return reexibirAditamentoContrato(contratoVO, result, request, model);

	}

	/**
	 * Exibir penalidade retirada maior menor geral para edição.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirEditarPenalidadeRetiradaMaiorMenorGeral")
	public String exibirEditarPenalidadeRetiradaMaiorMenorGeral(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_PENALIDADES);

		Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenor();

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO =
				(Collection<PenalidadesRetiradaMaiorMenorVO>) request.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_VO);

		if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor[0] != null
				&& listaPenalidadesRetMaiorMenorVO != null && !listaPenalidadesRetMaiorMenorVO.isEmpty()) {

			PenalidadesRetiradaMaiorMenorVO penalidadeRetiradaMaiorMenorVO =
					((List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidadesRetMaiorMenorVO).get(indicePenalidadeRetiradaMaiorMenor[0]);

			popularFormPenalidade(penalidadeRetiradaMaiorMenorVO, contratoVO);
			model.addAttribute(INDICE_PENALIDADE_RETIRADA_MAIOR_MENOR, indicePenalidadeRetiradaMaiorMenor[0]);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Exibir compromisso Take or Pay para edição.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirEditarCompromissoTOP")
	public String exibirEditarCompromissoTOP(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_TAKE_OR_PAY);

		Integer[] indicePeriodicidadeTakeOrPay = contratoVO.getIndicePeriodicidadeTakeOrPay();

		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayCVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);

		if (indicePeriodicidadeTakeOrPay != null && indicePeriodicidadeTakeOrPay[0] != null && listaCompromissosTakeOrPayCVO != null
				&& !listaCompromissosTakeOrPayCVO.isEmpty()) {

			CompromissoTakeOrPayVO compromissoTakeOrPayVO =
					((List<CompromissoTakeOrPayVO>) listaCompromissosTakeOrPayCVO).get(indicePeriodicidadeTakeOrPay[0]);

			popularFormTakeOrPay(compromissoTakeOrPayVO, contratoVO);
			model.addAttribute(INDICE_PERIODICIDADE_TAKE_OR_PAY, indicePeriodicidadeTakeOrPay[0]);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Popular form contrato com campos de take or pay preechidos.
	 *
	 */
	private void popularFormTakeOrPay(CompromissoTakeOrPayVO compromissoTakeOrPayVO, ContratoVO contratoVO) {

		if (compromissoTakeOrPayVO.getPeriodicidadeTakeOrPay() != null) {
			contratoVO.setPeriodicidadeTakeOrPayC(String.valueOf(compromissoTakeOrPayVO.getPeriodicidadeTakeOrPay().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getReferenciaQFParadaProgramada() != null) {
			contratoVO.setReferenciaQFParadaProgramadaC(
					String.valueOf(compromissoTakeOrPayVO.getReferenciaQFParadaProgramada().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getConsumoReferenciaTakeOrPay() != null) {
			contratoVO.setConsumoReferenciaTakeOrPayC(
					String.valueOf(compromissoTakeOrPayVO.getConsumoReferenciaTakeOrPay().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getMargemVariacaoTakeOrPay() != null) {
			contratoVO.setMargemVariacaoTakeOrPayC(Util.converterCampoPercentualParaString(MARGEM_VARIACAO_TAKE_OR_PAY_C,
					compromissoTakeOrPayVO.getMargemVariacaoTakeOrPay().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}

		popularFormDataVigenciaTakeOrPay(compromissoTakeOrPayVO, contratoVO);

		if (compromissoTakeOrPayVO.getPrecoCobranca() != null) {
			contratoVO.setPrecoCobrancaC(String.valueOf(compromissoTakeOrPayVO.getPrecoCobranca().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getTipoApuracao() != null) {
			contratoVO.setTipoApuracaoC(String.valueOf(compromissoTakeOrPayVO.getTipoApuracao().getChavePrimaria()));
		}
		contratoVO.setConsideraParadaProgramadaC(String.valueOf(compromissoTakeOrPayVO.getConsideraParadaProgramada()));
		contratoVO.setConsideraFalhaFornecimentoC(String.valueOf(compromissoTakeOrPayVO.getConsideraFalhaFornecimento()));
		contratoVO.setConsideraCasoFortuitoC(String.valueOf(compromissoTakeOrPayVO.getConsideraCasoFortuito()));
		contratoVO.setRecuperavelC(String.valueOf(compromissoTakeOrPayVO.getRecuperavel()));

		if (compromissoTakeOrPayVO.getPercentualNaoRecuperavel() != null) {
			contratoVO.setPercentualNaoRecuperavelC(Util.converterCampoPercentualParaString(PERCENTUAL_NAO_RECUPERAVEL_C,
					compromissoTakeOrPayVO.getPercentualNaoRecuperavel().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
		}
		populaFormFormaApuracaoTakeOrPay(compromissoTakeOrPayVO, contratoVO);
	}

	private void populaFormFormaApuracaoTakeOrPay(CompromissoTakeOrPayVO compromissoTakeOrPayVO, ContratoVO contratoVO) {
		if (compromissoTakeOrPayVO.getApuracaoParadaProgramada() != null) {
			contratoVO
					.setApuracaoParadaProgramadaC(String.valueOf(compromissoTakeOrPayVO.getApuracaoParadaProgramada().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getApuracaoCasoFortuito() != null) {
			contratoVO.setApuracaoCasoFortuitoC(String.valueOf(compromissoTakeOrPayVO.getApuracaoCasoFortuito().getChavePrimaria()));
		}
		if (compromissoTakeOrPayVO.getApuracaoFalhaFornecimento() != null) {
			contratoVO.setApuracaoFalhaFornecimentoC(
					String.valueOf(compromissoTakeOrPayVO.getApuracaoFalhaFornecimento().getChavePrimaria()));
		}
	}

	private void popularFormDataVigenciaTakeOrPay(CompromissoTakeOrPayVO compromissoTakeOrPayVO, ContratoVO contratoVO) {
		if (compromissoTakeOrPayVO.getDataInicioVigencia() != null) {
			contratoVO.setDataInicioVigenciaC(
					Util.converterDataParaStringSemHora(compromissoTakeOrPayVO.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR));
		}
		if (compromissoTakeOrPayVO.getDataFimVigencia() != null) {
			contratoVO.setDataFimVigenciaC(
					Util.converterDataParaStringSemHora(compromissoTakeOrPayVO.getDataFimVigencia(), Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * Popular form contrato com campos de penalidade preechidos.
	 *
	 */
	private void popularFormPenalidade(PenalidadesRetiradaMaiorMenorVO penalidadeRetiradaMaiorMenorVO, ContratoVO contratoVO) {

		contratoVO.setPenalidadeRetMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getPenalidadeRetiradaMaiorMenor().getChavePrimaria()));
		contratoVO.setPeriodicidadeRetMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getEntidadePeriodicidadeRetiradaMaiorMenor().getChavePrimaria()));
		contratoVO.setBaseApuracaoRetMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getEntidadeBaseApuracaoRetiradaMaiorMenor().getChavePrimaria()));
		contratoVO.setDataIniVigRetirMaiorMenor(
				Util.converterDataParaStringSemHora(penalidadeRetiradaMaiorMenorVO.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR));
		contratoVO.setDataFimVigRetirMaiorMenor(
				Util.converterDataParaStringSemHora(penalidadeRetiradaMaiorMenorVO.getDataFimVigencia(), Constantes.FORMATO_DATA_BR));
		contratoVO.setPrecoCobrancaRetirMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getEntidadePrecoCobrancaRetiradaMaiorMenor().getChavePrimaria()));
		contratoVO.setTipoApuracaoRetirMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getEntidadeTipoApuracaoRetirMaiorMenor().getChavePrimaria()));
		contratoVO.setPercentualCobRetMaiorMenor(Util.converterCampoPercentualParaString(PERCENTUAL_COBRANCA_RETIRADA_MAIOR_MENOR,
				penalidadeRetiradaMaiorMenorVO.getValorPercentualCobRetMaiorMenor().multiply(BigDecimal.valueOf(CEM)),
				Constantes.LOCALE_PADRAO));

		if (penalidadeRetiradaMaiorMenorVO.getValorPercentualCobIntRetMaiorMenor() != null) {
			contratoVO.setPercentualCobIntRetMaiorMenor(
					Util.converterCampoPercentualParaString(PERCENTUAL_COBRANCA_INTERRUPCAO_RETIRADA_MAIOR_MENOR,
							penalidadeRetiradaMaiorMenorVO.getValorPercentualCobIntRetMaiorMenor().multiply(BigDecimal.valueOf(CEM)),
							Constantes.LOCALE_PADRAO));
		}

		contratoVO.setConsumoReferenciaRetMaiorMenor(
				String.valueOf(penalidadeRetiradaMaiorMenorVO.getEntidadeConsumoReferenciaRetMaiorMenor().getChavePrimaria()));

		contratoVO.setPercentualRetMaiorMenor(Util.converterCampoPercentualParaString(PERCENTUAL_RETIRADA_MAIOR_MENOR,
				penalidadeRetiradaMaiorMenorVO.getValorPercentualRetMaiorMenor().multiply(BigDecimal.valueOf(CEM)),
				Constantes.LOCALE_PADRAO));

		contratoVO.setIndicadorImposto(String.valueOf(penalidadeRetiradaMaiorMenorVO.getIndicadorImposto()));

	}

	/**
	 * Validar adicao nova penalidade retirada maior menor contrato.
	 *
	 * @param penalidadesRetiradaMaiorMenorVO the penalidades retirada maior menor vo
	 * @param listaPenalidadesRetMaiorMenorVO the lista penalidades ret maior menor vo
	 * @throws GGASException the GGAS exception
	 */
	private void validarAdicaoNovaPenalidadeRetiradaMaiorMenorContrato(PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO,
			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidadesRetMaiorMenorVO) throws GGASException {

		ContratoPenalidade contratoPenalidade = converterPenalidadesRetiradaMaiorMenorVO(penalidadesRetiradaMaiorMenorVO);

		Collection<ContratoPenalidade> listaContratoPenalidade = new ArrayList<ContratoPenalidade>();
		for (PenalidadesRetiradaMaiorMenorVO penalidadeRetMaiorMenor : listaPenalidadesRetMaiorMenorVO) {
			ContratoPenalidade penalidade = converterPenalidadesRetiradaMaiorMenorVO(penalidadeRetMaiorMenor);
			listaContratoPenalidade.add(penalidade);
		}

		Contrato contrato = penalidadesRetiradaMaiorMenorVO.getContrato();
		if (contratoPenalidade != null) {
			controladorContrato.validarIntervaloDataVigenciaComDataAssinaturaContrato(contrato, contratoPenalidade, null);
			controladorContrato.validarAdicaoContratoPenalidade(contratoPenalidade, listaContratoPenalidade);
		}
	}

	/**
	 * Converter penalidades retirada maior menor vo.
	 *
	 * @param penalidadesRetiradaMaiorMenorVO {@link PenalidadesRetiradaMaiorMenorVO}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private ContratoPenalidade converterPenalidadesRetiradaMaiorMenorVO(
			PenalidadesRetiradaMaiorMenorVO penalidadesRetiradaMaiorMenorVO) {

		ContratoPenalidade contratoPenalidade = (ContratoPenalidade) controladorContrato.criarContratoPenalidade();

		contratoPenalidade.setContrato(penalidadesRetiradaMaiorMenorVO.getContrato());
		contratoPenalidade.setPenalidade(penalidadesRetiradaMaiorMenorVO.getPenalidadeRetiradaMaiorMenor());
		contratoPenalidade.setPeriodicidadePenalidade(penalidadesRetiradaMaiorMenorVO.getEntidadePeriodicidadeRetiradaMaiorMenor());
		contratoPenalidade.setBaseApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeBaseApuracaoRetiradaMaiorMenor());
		contratoPenalidade.setDataInicioVigencia(penalidadesRetiradaMaiorMenorVO.getDataInicioVigencia());
		contratoPenalidade.setDataFimVigencia(penalidadesRetiradaMaiorMenorVO.getDataFimVigencia());
		contratoPenalidade.setPrecoCobranca(penalidadesRetiradaMaiorMenorVO.getEntidadePrecoCobrancaRetiradaMaiorMenor());
		contratoPenalidade.setTipoApuracao(penalidadesRetiradaMaiorMenorVO.getEntidadeTipoApuracaoRetirMaiorMenor());
		contratoPenalidade.setConsumoReferencia(penalidadesRetiradaMaiorMenorVO.getEntidadeConsumoReferenciaRetMaiorMenor());
		contratoPenalidade.setValorPercentualCobRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualCobRetMaiorMenor());
		contratoPenalidade.setValorPercentualCobIntRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualCobIntRetMaiorMenor());
		contratoPenalidade.setValorPercentualRetMaiorMenor(penalidadesRetiradaMaiorMenorVO.getValorPercentualRetMaiorMenor());
		contratoPenalidade.setIndicadorImposto(penalidadesRetiradaMaiorMenorVO.getIndicadorImposto());

		return contratoPenalidade;
	}

	/**
	 * Adicionar compromisso top aba geral.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarCompromissoTOPAbaGeral")
	public String adicionarCompromissoTOPAbaGeral(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		ObjectMapper oMapper = new ObjectMapper();

		model.addAttribute(ABA_ATUAL, ABA_TAKE_OR_PAY);

		Integer[] indicePeriodicidadeTakeOrPay = contratoVO.getIndicePeriodicidadeTakeOrPay();

		String dataAssinatura = contratoVO.getDataAssinatura();
		String vencimentoObrigacoesContratuais = contratoVO.getDataVencObrigacoesContratuais();
		Long idModeloContrato = contratoVO.getIdModeloContrato();
		String periodicidadeTakeOrPayC = contratoVO.getPeriodicidadeTakeOrPayC();
		String referenciaQFParadaProgramadaC = contratoVO.getReferenciaQFParadaProgramadaC();
		String consumoReferenciaTakeOrPayC = contratoVO.getConsumoReferenciaTakeOrPayC();
		String margemVariacaoTakeOrPayC = contratoVO.getMargemVariacaoTakeOrPayC();
		String dataInicioVigenciaC = contratoVO.getDataInicioVigenciaC();
		String dataFimVigenciaC = contratoVO.getDataFimVigenciaC();
		String precoCobrancaC = contratoVO.getPrecoCobrancaC();
		String tipoApuracaoC = contratoVO.getTipoApuracaoC();
		String consideraParadaProgramadaC = contratoVO.getConsideraParadaProgramadaC();
		String consideraFalhaFornecimentoC = contratoVO.getConsideraFalhaFornecimentoC();
		String consideraCasoFortuitoC = contratoVO.getConsideraCasoFortuitoC();
		String recuperavelC = contratoVO.getRecuperavelC();
		String percentualNaoRecuperavelC = contratoVO.getPercentualNaoRecuperavelC();
		String apuracaoParadaProgramadaC = contratoVO.getApuracaoParadaProgramadaC();
		String apuracaoCasoFortuitoC = contratoVO.getApuracaoCasoFortuitoC();
		String apuracaoFalhaFornecimentoC = contratoVO.getApuracaoFalhaFornecimentoC();

		EntidadeConteudo entPeriodicidadeTakeOrPayC = null;
		EntidadeConteudo entConsumoReferenciaTakeOrPayC = null;
		BigDecimal valorMargemVariacaoTakeOrPayC = null;
		BigDecimal valorPercentualNaoRecuperavelC = null;
		EntidadeConteudo entidadeReferenciaQFParadaProgramadaC = null;
		EntidadeConteudo entidadePrecoCobrancaC = null;
		EntidadeConteudo entidadeTipoApuracaoC = null;
		EntidadeConteudo entidadeApuracaoParadaProgramadaC = null;
		EntidadeConteudo entidadeApuracaoCasoFortuitoC = null;
		EntidadeConteudo entidadeApuracaoFalhaFornecimentoC = null;

		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayCVO = null;

		if (request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO) != null) {
			listaCompromissosTakeOrPayCVO =
					(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);

			request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);
			model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);
		}

		try {

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (listaCompromissosTakeOrPayCVO == null) {
				listaCompromissosTakeOrPayCVO = new ArrayList<CompromissoTakeOrPayVO>();
			}

			if (periodicidadeTakeOrPayC != null && StringUtils.isNotEmpty(periodicidadeTakeOrPayC.trim())
					&& !periodicidadeTakeOrPayC.equals(NAO_SELECIONADO)) {
				entPeriodicidadeTakeOrPayC = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.PERIODICIDADE_TAKE_OR_PAY, periodicidadeTakeOrPayC.trim()));
			}
			if (referenciaQFParadaProgramadaC != null && StringUtils.isNotEmpty(referenciaQFParadaProgramadaC.trim())
					&& !referenciaQFParadaProgramadaC.equals(NAO_SELECIONADO)) {
				entidadeReferenciaQFParadaProgramadaC = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(LABEL_REFERENCIA_QF_PARADA_PROGRAMADA, referenciaQFParadaProgramadaC.trim()));
			}

			if (consumoReferenciaTakeOrPayC != null && StringUtils.isNotEmpty(consumoReferenciaTakeOrPayC.trim())
					&& !consumoReferenciaTakeOrPayC.equals(NAO_SELECIONADO)) {
				entConsumoReferenciaTakeOrPayC = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(Contrato.CONSUMO_REFERENCIA_TAKE_OR_PAY, consumoReferenciaTakeOrPayC.trim()));
			}
			if (StringUtils.isNotEmpty(margemVariacaoTakeOrPayC)) {
				valorMargemVariacaoTakeOrPayC =
						Util.converterCampoStringParaValorBigDecimal(LABEL_MARGEM_DE_VARIACAO_TAKE_OR_PAY, margemVariacaoTakeOrPayC,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			}
			if (consumoReferenciaTakeOrPayC != null && StringUtils.isNotEmpty(consumoReferenciaTakeOrPayC.trim())
					&& !consumoReferenciaTakeOrPayC.equals(NAO_SELECIONADO)) {
				entConsumoReferenciaTakeOrPayC = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(Contrato.CONSUMO_REFERENCIA_TAKE_OR_PAY, consumoReferenciaTakeOrPayC.trim()));
			}
			if (StringUtils.isNotEmpty(precoCobrancaC) && !precoCobrancaC.equals(NAO_SELECIONADO)) {
				entidadePrecoCobrancaC = controladorEntidadeConteudo
						.obterEntidadeConteudo(Util.converterCampoStringParaValorLong(Contrato.TABELA_PRECO_COBRANCA, precoCobrancaC));
			}
			if (tipoApuracaoC != null && StringUtils.isNotEmpty(tipoApuracaoC.trim()) && !tipoApuracaoC.equals(NAO_SELECIONADO)) {
				entidadeTipoApuracaoC = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, tipoApuracaoC.trim()));
			}

			if (apuracaoParadaProgramadaC != null && StringUtils.isNotEmpty(apuracaoParadaProgramadaC.trim())
					&& !apuracaoParadaProgramadaC.equals(NAO_SELECIONADO)) {
				entidadeApuracaoParadaProgramadaC = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoParadaProgramadaC.trim()));
			}
			if (apuracaoCasoFortuitoC != null && StringUtils.isNotEmpty(apuracaoCasoFortuitoC.trim())
					&& !apuracaoCasoFortuitoC.equals(NAO_SELECIONADO)) {
				entidadeApuracaoCasoFortuitoC = controladorEntidadeConteudo.obterEntidadeConteudo(
						Util.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoCasoFortuitoC.trim()));
			}
			if (apuracaoFalhaFornecimentoC != null && StringUtils.isNotEmpty(apuracaoFalhaFornecimentoC.trim())
					&& !apuracaoFalhaFornecimentoC.equals(NAO_SELECIONADO)) {
				entidadeApuracaoFalhaFornecimentoC = controladorEntidadeConteudo.obterEntidadeConteudo(Util
						.converterCampoStringParaValorLong(Contrato.FORMA_CALCULO_COBRANCA_RECUPERACAO, apuracaoFalhaFornecimentoC.trim()));
			}
			if (StringUtils.isNotEmpty(percentualNaoRecuperavelC)) {
				valorPercentualNaoRecuperavelC =
						Util.converterCampoStringParaValorBigDecimal(LABEL_PERCENTUAL_NAO_RECUPERAVEL, percentualNaoRecuperavelC,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(CEM));
			} else {
				valorPercentualNaoRecuperavelC = BigDecimal.ZERO;
			}

			ModeloContrato modeloContrato = null;
			if (idModeloContrato != null && idModeloContrato > 0) {
				modeloContrato = (ModeloContrato) controladorModeloContrato.obter(idModeloContrato, ATRIBUTOS, ABAS);
			}

			controladorContrato.validarDataAssinaturaContrato(dataAssinatura);
			Date dataAssinaturaContrato =
					Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, dataAssinatura, Constantes.FORMATO_DATA_BR);
			Date dataVencimentoContrato = null;
			if (!vencimentoObrigacoesContratuais.isEmpty()) {
				dataVencimentoContrato = Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, vencimentoObrigacoesContratuais,
						Constantes.FORMATO_DATA_BR);
			}

			String[] camposTakeOrPay = new String[] { PERIODICIDADE_TAKE_OR_PAY_C, REFERENCIA_QF_PARADA_PROGRAMADA_C,
					CONSUMO_REFERENCIA_TAKE_OR_PAY_C, MARGEM_VARIACAO_TAKE_OR_PAY_C, DATA_INICIO_VIGENCIA_C, DATA_FIM_VIGENCIA_C,
					PRECO_COBRANCA_C, TIPO_APURACAO_C, CONSIDERA_PARADA_PROGRAMADA_C, CONSIDERA_FALHA_FORNECIMENTO_C,
					CONSIDERA_CASO_FORTUITO_C, PERCENTUAL_NAO_RECUPERAVEL_C, APURACAO_PARADA_PROGRAMADA_C, APURACAO_PARADA_PROGRAMADA_C,
					APURACAO_CASO_FORTUITO_C, APURACAO_FALHA_FORNECIMENTO_C };

			controladorContrato.validarCamposObrigatoriosContrato(oMapper.convertValue(contratoVO, Map.class),
					Arrays.asList(camposTakeOrPay), modeloContrato);

			Date dataInicioVigencia = null;
			if (StringUtils.isNotEmpty(dataInicioVigenciaC)) {
				dataInicioVigencia =
						Util.converterCampoStringParaData(LABEL_DATA_INICIO_VIGENCIA, dataInicioVigenciaC, Constantes.FORMATO_DATA_BR);
			}

			Date dataFimVigencia = null;
			if (StringUtils.isNotEmpty(dataFimVigenciaC)) {
				dataFimVigencia = Util.converterCampoStringParaData(LABEL_DATA_FIM_VIGENCIA, dataFimVigenciaC, Constantes.FORMATO_DATA_BR);
			}

			if (dataInicioVigencia != null && dataFimVigencia != null && dataInicioVigencia.compareTo(dataFimVigencia) >= 0) {
				throw new GGASException(ControladorPerfilParcelamento.ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO, true);
			}

			if (entPeriodicidadeTakeOrPayC != null && entConsumoReferenciaTakeOrPayC != null && valorMargemVariacaoTakeOrPayC != null) {

				Contrato contrato = (Contrato) controladorContrato.criar();
				contrato.setDataAssinatura(dataAssinaturaContrato);
				contrato.setDataVencimentoObrigacoes(dataVencimentoContrato);

				CompromissoTakeOrPayVO compromissoTakeOrPayVO = new CompromissoTakeOrPayVO();
				compromissoTakeOrPayVO.setContrato(contrato);
				compromissoTakeOrPayVO.setPeriodicidadeTakeOrPay(entPeriodicidadeTakeOrPayC);
				compromissoTakeOrPayVO.setConsumoReferenciaTakeOrPay(entConsumoReferenciaTakeOrPayC);
				compromissoTakeOrPayVO.setMargemVariacaoTakeOrPay(valorMargemVariacaoTakeOrPayC);
				compromissoTakeOrPayVO.setReferenciaQFParadaProgramada(entidadeReferenciaQFParadaProgramadaC);
				compromissoTakeOrPayVO.setPrecoCobranca(entidadePrecoCobrancaC);
				compromissoTakeOrPayVO.setTipoApuracao(entidadeTipoApuracaoC);
				compromissoTakeOrPayVO.setApuracaoParadaProgramada(entidadeApuracaoParadaProgramadaC);
				compromissoTakeOrPayVO.setApuracaoCasoFortuito(entidadeApuracaoCasoFortuitoC);
				compromissoTakeOrPayVO.setApuracaoFalhaFornecimento(entidadeApuracaoFalhaFornecimentoC);
				compromissoTakeOrPayVO.setPercentualNaoRecuperavel(valorPercentualNaoRecuperavelC);
				compromissoTakeOrPayVO.setDataInicioVigencia(dataInicioVigencia);
				compromissoTakeOrPayVO.setDataFimVigencia(dataFimVigencia);

				if (StringUtils.isNotEmpty(recuperavelC)) {
					compromissoTakeOrPayVO.setRecuperavel(Boolean.valueOf(recuperavelC));
				}
				if (StringUtils.isNotEmpty(consideraParadaProgramadaC)) {
					compromissoTakeOrPayVO.setConsideraParadaProgramada(Boolean.valueOf(consideraParadaProgramadaC));
				}
				if (StringUtils.isNotEmpty(consideraCasoFortuitoC)) {
					compromissoTakeOrPayVO.setConsideraCasoFortuito(Boolean.valueOf(consideraCasoFortuitoC));
				}
				if (StringUtils.isNotEmpty(consideraFalhaFornecimentoC)) {
					compromissoTakeOrPayVO.setConsideraFalhaFornecimento(Boolean.valueOf(consideraFalhaFornecimentoC));
				}

				validarAdicaoNovoCompromissoContrato(compromissoTakeOrPayVO, listaCompromissosTakeOrPayCVO);

				if (indicePeriodicidadeTakeOrPay != null && indicePeriodicidadeTakeOrPay.length > 0
						&& indicePeriodicidadeTakeOrPay[0] != null) {

					List<CompromissoTakeOrPayVO> listaCompromissos = (List<CompromissoTakeOrPayVO>) listaCompromissosTakeOrPayCVO;
					listaCompromissos.set(indicePeriodicidadeTakeOrPay[0], compromissoTakeOrPayVO);

					listaCompromissosTakeOrPayCVO = listaCompromissos;

				} else {
					if (!listaCompromissosTakeOrPayCVO.contains(compromissoTakeOrPayVO)) {
						listaCompromissosTakeOrPayCVO.add(compromissoTakeOrPayVO);
					} else {
						throw new GGASException("Penalidade com periodicidade igual e períodos sobrepostos");
					}
				}

			}

			request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);
			model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);

			contratoVO.setPeriodicidadeTakeOrPayC(NAO_SELECIONADO);
			contratoVO.setConsumoReferenciaTakeOrPayC(NAO_SELECIONADO);
			contratoVO.setMargemVariacaoTakeOrPayC(StringUtils.EMPTY);
			contratoVO.setReferenciaQFParadaProgramadaC(NAO_SELECIONADO);
			contratoVO.setDataInicioVigenciaC(StringUtils.EMPTY);
			contratoVO.setDataFimVigenciaC(StringUtils.EMPTY);
			contratoVO.setPercentualNaoRecuperavelC(StringUtils.EMPTY);
			contratoVO.setPrecoCobrancaC(NAO_SELECIONADO);
			contratoVO.setTipoApuracaoC(NAO_SELECIONADO);
			contratoVO.setApuracaoParadaProgramadaC(NAO_SELECIONADO);
			contratoVO.setApuracaoCasoFortuitoC(NAO_SELECIONADO);
			contratoVO.setApuracaoFalhaFornecimentoC(NAO_SELECIONADO);

			// Carregar os campos padrao do modelo
			this.carregarDadosModelo(contratoVO, modeloContrato, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Validar adicao novo compromisso contrato.
	 *
	 */
	private void validarAdicaoNovoCompromissoContrato(CompromissoTakeOrPayVO compromissoTakeOrPayVO,
			Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayVO) throws GGASException {

		ContratoPenalidade contratoPenalidade = converterCompromissoTakeOrPayContratoVO(compromissoTakeOrPayVO);

		Collection<ContratoPenalidade> listaContratoPenalidade = new ArrayList<ContratoPenalidade>();
		for (CompromissoTakeOrPayVO compromisso : listaCompromissosTakeOrPayVO) {
			ContratoPenalidade penalidade = converterCompromissoTakeOrPayContratoVO(compromisso);
			listaContratoPenalidade.add(penalidade);
		}

		Contrato contrato = compromissoTakeOrPayVO.getContrato();

		controladorContrato.validaFormaDeApuracao(compromissoTakeOrPayVO);
		controladorContrato.validarIntervaloDataVigenciaComDataAssinaturaContrato(contrato, contratoPenalidade, null);
		controladorContrato.validarAdicaoContratoPenalidade(contratoPenalidade, listaContratoPenalidade);

	}

	/**
	 * Converter compromisso take or pay contrato vo.
	 *
	 */
	private ContratoPenalidade converterCompromissoTakeOrPayContratoVO(CompromissoTakeOrPayVO compromisso) throws GGASException {

		ContratoPenalidade contratoPenalidade = (ContratoPenalidade) controladorContrato.criarContratoPenalidade();

		String valorTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		Penalidade penalidadeTakeOrPay = controladorContrato.obterPenalidade(Long.parseLong(valorTakeOrPay));

		contratoPenalidade.setContrato(compromisso.getContrato());
		contratoPenalidade.setApuracaoCasoFortuito(compromisso.getApuracaoCasoFortuito());
		contratoPenalidade.setApuracaoFalhaFornecimento(compromisso.getApuracaoFalhaFornecimento());
		contratoPenalidade.setApuracaoParadaProgramada(compromisso.getApuracaoParadaProgramada());
		contratoPenalidade.setConsideraCasoFortuito(compromisso.getConsideraCasoFortuito());
		contratoPenalidade.setConsideraFalhaFornecimento(compromisso.getConsideraFalhaFornecimento());
		contratoPenalidade.setConsideraParadaProgramada(compromisso.getConsideraParadaProgramada());
		contratoPenalidade.setConsumoReferencia(compromisso.getConsumoReferenciaTakeOrPay());
		contratoPenalidade.setDataFimVigencia(compromisso.getDataFimVigencia());
		contratoPenalidade.setDataInicioVigencia(compromisso.getDataInicioVigencia());
		contratoPenalidade.setPenalidade(penalidadeTakeOrPay);
		contratoPenalidade.setPercentualMargemVariacao(compromisso.getMargemVariacaoTakeOrPay());
		contratoPenalidade.setPercentualNaoRecuperavel(compromisso.getPercentualNaoRecuperavel());
		contratoPenalidade.setPeriodicidadePenalidade(compromisso.getPeriodicidadeTakeOrPay());
		contratoPenalidade.setPrecoCobranca(compromisso.getPrecoCobranca());
		contratoPenalidade.setRecuperavel(compromisso.getRecuperavel());
		contratoPenalidade.setReferenciaQFParadaProgramada(compromisso.getReferenciaQFParadaProgramada());
		contratoPenalidade.setTipoApuracao(compromisso.getTipoApuracao());

		return contratoPenalidade;
	}

	/**
	 * Excluir compromisso top aba geral inclusao.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirCompromissoTOPAbaGeral")
	public String excluirCompromissoTOPAbaGeral(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ABA_ATUAL, ABA_TAKE_OR_PAY);
		Integer[] indicePeriodicidadeTakeOrPay = contratoVO.getIndicePeriodicidadeTakeOrPay();

		Collection<CompromissoTakeOrPayVO> listaCompromissosTakeOrPayCVO =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);

		if (indicePeriodicidadeTakeOrPay != null && indicePeriodicidadeTakeOrPay.length > 0 && listaCompromissosTakeOrPayCVO != null
				&& !listaCompromissosTakeOrPayCVO.isEmpty()) {
			for (int i = indicePeriodicidadeTakeOrPay.length - 1; i >= 0; i--) {
				((List<CompromissoTakeOrPayVO>) listaCompromissosTakeOrPayCVO).remove(indicePeriodicidadeTakeOrPay[i].intValue());

			}
			request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);
		}

		model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaCompromissosTakeOrPayCVO);

		return reexibirInclusaoContrato(contratoVO, result, request, model);

	}

	/**
	 * Popular lista take or pay.
	 *
	 * @param contrato the contrato
	 * @param request the request
	 */
	private void popularListaTakeOrPay(Contrato contrato, HttpServletRequest request, Model model) {

		Collection<ContratoPenalidade> listaPenalidadeTakeOrPayConsulta =
				controladorContrato.listarContratoPenalidadeTakeOrPayPorContrato(contrato.getChavePrimaria());

		Collection<CompromissoTakeOrPayVO> listaPenalidadeTakeOrPay =
				(Collection<CompromissoTakeOrPayVO>) request.getSession().getAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO);

		if (listaPenalidadeTakeOrPay == null) {

			Collection<CompromissoTakeOrPayVO> listaPenalidadeTakeOrPayVO = new ArrayList<CompromissoTakeOrPayVO>();

			if (listaPenalidadeTakeOrPayConsulta != null && !listaPenalidadeTakeOrPayConsulta.isEmpty()) {

				for (ContratoPenalidade contratoPenalidade : listaPenalidadeTakeOrPayConsulta) {

					CompromissoTakeOrPayVO compromissoTakeOrPayVO = new CompromissoTakeOrPayVO();

					compromissoTakeOrPayVO.setContrato(contrato);
					compromissoTakeOrPayVO.setPeriodicidadeTakeOrPay(contratoPenalidade.getPeriodicidadePenalidade());
					compromissoTakeOrPayVO.setConsumoReferenciaTakeOrPay(contratoPenalidade.getConsumoReferencia());
					compromissoTakeOrPayVO.setMargemVariacaoTakeOrPay(contratoPenalidade.getPercentualMargemVariacao());
					compromissoTakeOrPayVO.setReferenciaQFParadaProgramada(contratoPenalidade.getReferenciaQFParadaProgramada());
					compromissoTakeOrPayVO.setPrecoCobranca(contratoPenalidade.getPrecoCobranca());
					compromissoTakeOrPayVO.setTipoApuracao(contratoPenalidade.getTipoApuracao());
					compromissoTakeOrPayVO.setApuracaoParadaProgramada(contratoPenalidade.getApuracaoParadaProgramada());
					compromissoTakeOrPayVO.setApuracaoCasoFortuito(contratoPenalidade.getApuracaoCasoFortuito());
					compromissoTakeOrPayVO.setApuracaoFalhaFornecimento(contratoPenalidade.getApuracaoFalhaFornecimento());
					compromissoTakeOrPayVO.setPercentualNaoRecuperavel(contratoPenalidade.getPercentualNaoRecuperavel());
					compromissoTakeOrPayVO.setDataInicioVigencia(contratoPenalidade.getDataInicioVigencia());
					compromissoTakeOrPayVO.setDataFimVigencia(contratoPenalidade.getDataFimVigencia());
					compromissoTakeOrPayVO.setRecuperavel(contratoPenalidade.getRecuperavel());
					compromissoTakeOrPayVO.setConsideraParadaProgramada(contratoPenalidade.getConsideraParadaProgramada());
					compromissoTakeOrPayVO.setConsideraCasoFortuito(contratoPenalidade.getConsideraCasoFortuito());
					compromissoTakeOrPayVO.setConsideraFalhaFornecimento(contratoPenalidade.getConsideraFalhaFornecimento());

					listaPenalidadeTakeOrPayVO.add(compromissoTakeOrPayVO);
				}

				request.getSession().setAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaPenalidadeTakeOrPayVO);
				model.addAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_C_VO, listaPenalidadeTakeOrPayVO);
			}
		}

	}

	/**
	 * Método responsável por excluir o Penalidade Retirada Maior/Menor da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirPenalidadeRetiradaMaiorMenorModalidade")
	public String excluirPenalidadeRetiradaMaiorMenorModalidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		model.addAttribute("contratoVO", contratoVO);
		try {
			Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenorModalidade();

			Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidaRetirMaiorMenor = (Collection<PenalidadesRetiradaMaiorMenorVO>) request
					.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);

			this.tratarCamposSelectAbaConsumo(contratoVO, model);

			if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor.length > 0
					&& listaPenalidaRetirMaiorMenor != null && !listaPenalidaRetirMaiorMenor.isEmpty()) {
				for (int i = indicePenalidadeRetiradaMaiorMenor.length - 1; i >= 0; i--) {
					((List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidaRetirMaiorMenor)
							.remove(indicePenalidadeRetiradaMaiorMenor[i].intValue());

				}
				request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidaRetirMaiorMenor);
			}

			request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidaRetirMaiorMenor);
			request.setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
					request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return reexibirInclusaoContratoPontoConsumo(contratoVO, result, request, model);
	}

	/**
	 * Método responsável por excluir o Penalidade Retirada Maior/Menor da aba modalidade.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirPenalidadeRetiradaMaiorMenorModalidadeAditamento")
	public String excluirPenalidadeRetiradaMaiorMenorModalidadeAditamento(ContratoVO contratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute("contratoVO", contratoVO);
		Integer[] indicePenalidadeRetiradaMaiorMenor = contratoVO.getIndicePenalidadeRetiradaMaiorMenorModalidade();

		Collection<PenalidadesRetiradaMaiorMenorVO> listaPenalidaRetirMaiorMenor = (Collection<PenalidadesRetiradaMaiorMenorVO>) request
				.getSession().getAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);
		this.tratarCamposSelectAbaConsumo(contratoVO, model);

		if (indicePenalidadeRetiradaMaiorMenor != null && indicePenalidadeRetiradaMaiorMenor.length > 0
				&& listaPenalidaRetirMaiorMenor != null && !listaPenalidaRetirMaiorMenor.isEmpty()) {
			for (int i = indicePenalidadeRetiradaMaiorMenor.length - 1; i >= 0; i--) {
				((List<PenalidadesRetiradaMaiorMenorVO>) listaPenalidaRetirMaiorMenor)
						.remove(indicePenalidadeRetiradaMaiorMenor[i].intValue());

			}
			request.getSession().setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidaRetirMaiorMenor);
		}

		request.setAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO, listaPenalidaRetirMaiorMenor);
		request.setAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO,
				request.getSession().getAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO));

		return reexibirAditamentoContrato(contratoVO, result, request, model);
	}

	/**
	 * Exibir migracao modelo contrato.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param listaContrato {@link Collection}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	public void exibirMigracaoModeloContrato(HttpServletRequest request, Collection<Contrato> listaContrato, Model model)
			throws GGASException {

		listaContrato = filtrarContratoPorDatas(listaContrato);
		Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = agruparContratosProCodigoDescricao(listaContrato);

		model.addAttribute(MAPA_CONTRATO_MIGRACAO, mapaContratoMigracao);
		request.getSession().setAttribute(MAPA_CONTRATO_MIGRACAO, mapaContratoMigracao); // NOSONAR {mapa necessário
																							// para funcionamento da
																							// tela}
		model.addAttribute(LISTA_CONTRATO_MIGRACAO, listaContrato);

		carregarDados(request, model);

	}

	/**
	 * Agrupar contratos pro codigo descricao.
	 *
	 * @param listaContrato the lista contrato
	 * @return the map
	 */
	private Map<Pair<Long, String>, Collection<Contrato>> agruparContratosProCodigoDescricao(Collection<Contrato> listaContrato) {
		return Util.agrupar(listaContrato, new Agrupador<Contrato, Pair<Long, String>>() {
			@Override
			public Pair<Long, String> getChaveAgrupadora(Contrato elemento) {
				ModeloContrato modeloContrato = elemento.getModeloContrato();
				Long first = modeloContrato.getChavePrimaria();
				String second = modeloContrato.getDescricao(true);
				return new Pair<Long, String>(first, second);
			}
		});
	}

	/**
	 * Exibir migracao modelo contrato fluxo contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirMigracaoModeloContratoFluxoContrato")
	public String exibirMigracaoModeloContratoFluxoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirMigracaoModeloContrato";
		Long[] chavesPrimarias = contratoVO.getChavesPrimarias();
		Long[] chavesSessao = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);

		if (chavesPrimarias[0] == 0 && chavesSessao != null) {
			chavesPrimarias = chavesSessao;
		}
		if (chavesPrimarias[0] != 0) {
			request.getSession().setAttribute(CHAVES_PRIMARIAS, chavesPrimarias);
		}

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		filtro.put(HABILITADO, Boolean.TRUE);

		try {

			controladorContrato.validarMigracaoModeloContrato(chavesPrimarias[0]);
			Collection<Contrato> listaContrato = controladorContrato.consultarContratos(filtro);

			this.exibirMigracaoModeloContrato(request, listaContrato, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Exibir migracao modelo contrato campos alterados.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirMigracaoModeloContratoCamposAlterados")
	public String exibirMigracaoModeloContratoCamposAlterados(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirMigracaoModeloContratoCamposAlterados";
		Long chaveModeloContrato = contratoVO.getIdModeloContrato();

		try {
			if (chaveModeloContrato == null || chaveModeloContrato < 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ModeloContrato.MODELO_CONTRATO);
			}

			alterarDadosMigracaoContrato(chaveModeloContrato, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirMigracaoModeloContratoFluxoContrato(contratoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Salvar migracao modelo contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("salvarMigracaoModeloContrato")
	public String salvarMigracaoModeloContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String view = null;
		Long chaveModeloContrato = contratoVO.getIdModeloContrato();
		
		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
				(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

		try {

			ModeloContrato novoModeloContrato = (ModeloContrato) controladorModeloContrato.obter(chaveModeloContrato, ATRIBUTOS, ABAS);

			Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = obterMapaContratoMigracao(request);

			Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados =
					(Map<String, Collection<ModeloAtributo>>) request.getSession().getAttribute(MAPA_ATRIBUTOS_ADICIONADOS);

			Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos =
					(Map<String, Collection<ModeloAtributo>>) request.getSession().getAttribute(MAPA_ATRIBUTOS_REMOVIDOS);

			Map<String, Collection<ModeloAtributo>> mapaAtributosComuns =
					(Map<String, Collection<ModeloAtributo>>) request.getSession().getAttribute(MAPA_ATRIBUTOS_COMUNS);

			String logErroMigracao = controladorContrato.salvarMigracaoModeloContrato(mapaContratoMigracao, mapaAtributosAdicionados,
					mapaAtributosRemovidos, mapaAtributosComuns, novoModeloContrato);

			request.getSession().setAttribute(LOG_ERRO_MIGRACAO, logErroMigracao.getBytes());

			if (logErroMigracao.contains(LABEL_COM_ERRO)) {

				mensagemAlerta(model, Constantes.ALERTA_ENTIDADE_MIGRACAO_CONTRATO, "Contrato(s)");

			} else {

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_MIGRACAO_CONTRATO, request, Contrato.CONTRATOS);

			}

			view = pesquisarContrato(contratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirMigracaoModeloContratoCamposAlterados(contratoVO, result, request, model);
		}

		return view;

	}

	/**
	 * Obter mapa contrato migracao.
	 *
	 * @param request the request
	 * @return the map
	 */
	private Map<Pair<Long, String>, Collection<Contrato>> obterMapaContratoMigracao(HttpServletRequest request) {
		return (HashMap<Pair<Long, String>, Collection<Contrato>>) request.getSession().getAttribute(MAPA_CONTRATO_MIGRACAO);

	}

	/**
	 * Exibir log erro migracao.
	 *
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @return null
	 * @throws IOException - {@link IOException}
	 */
	@RequestMapping("exibirLogErroMigracaoContrato")
	public String exibirLogErroMigracaoContrato(HttpServletRequest request, HttpServletResponse response) throws IOException {

		byte[] logErroMigracao = (byte[]) request.getSession().getAttribute(LOG_ERRO_MIGRACAO);

		if (logErroMigracao != null) {
			ServletOutputStream servletOutputStream = response.getOutputStream();

			response.setContentType(EMAIL_APPLICATION_DOWNLOAD);
			response.setContentLength(logErroMigracao.length);
			response.setHeader(CONTENT_DISPOSITION, EMAIL_ATTACHMENT_FILENAME + EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG);

			servletOutputStream.write(logErroMigracao, 0, logErroMigracao.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		request.getSession().removeAttribute(LOG_ERRO_MIGRACAO);

		return null;
	}

	/**
	 * Carregar atributos alterados migracao.
	 *
	 * @param mapaA the mapa a
	 * @param mapaB the mapa b
	 * @return the collection
	 */
	private Collection<ModeloAtributo> carregarAtributosAlteradosMigracao(Map<String, ModeloAtributo> mapaA,
			Map<String, ModeloAtributo> mapaB) {

		Collection<ModeloAtributo> listaAtributos = new ArrayList<ModeloAtributo>();

		for (Entry<String, ModeloAtributo> entry : mapaA.entrySet()) {

			if (!mapaB.containsKey(entry.getKey())) {
				ModeloAtributo modeloAtributo = mapaA.get(entry.getKey());
				listaAtributos.add(modeloAtributo);
			}
		}

		return listaAtributos;
	}

	/**
	 * Alterar dados migracao contrato.
	 *
	 */
	private void alterarDadosMigracaoContrato(Long chaveModeloContrato, HttpServletRequest request, Model model) throws GGASException {

		ModeloContrato modeloNovo = (ModeloContrato) controladorModeloContrato.obter(chaveModeloContrato, ATRIBUTOS, ABAS);
		model.addAttribute(MODELO_NOVO, modeloNovo);
		Collection<ModeloAtributo> atributosModeloNovo = modeloNovo.getAtributos();

		Map<String, Collection<ModeloAtributoVO>> mapaAtributosAdicionadosRemovidos = new HashMap<String, Collection<ModeloAtributoVO>>();

		Map<String, ModeloAtributo> mapaAtributosModeloNovo = new HashMap<String, ModeloAtributo>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosComuns = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, ModeloAtributo> mapaAtributosModeloAntigo = null;

		for (ModeloAtributo modeloAtributo : atributosModeloNovo) {

			AbaAtributo abaAtributo = modeloAtributo.getAbaAtributo();
			String descricaoAtributo = abaAtributo.getNomeColuna();

			mapaAtributosModeloNovo.put(descricaoAtributo, modeloAtributo);
		}

		Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = obterMapaContratoMigracao(request);

		for (Entry<Pair<Long, String>, Collection<Contrato>> entry : mapaContratoMigracao.entrySet()) {

			Long chavePrimariaModeloAntigo = entry.getKey().getFirst();

			if (modeloNovo.getChavePrimaria() != chavePrimariaModeloAntigo) {
				String descricaoModelo = entry.getKey().getSecond();

				ModeloContrato modeloAntigo = (ModeloContrato) controladorModeloContrato.obter(chavePrimariaModeloAntigo, ATRIBUTOS, ABAS);
				Collection<ModeloAtributo> atributosModeloAntigo = modeloAntigo.getAtributos();

				mapaAtributosModeloAntigo = new HashMap<String, ModeloAtributo>();
				for (ModeloAtributo modeloAtributoAntigo : atributosModeloAntigo) {
					AbaAtributo atributoAntigo = modeloAtributoAntigo.getAbaAtributo();
					String descricaoAtributoAntigo = atributoAntigo.getNomeColuna();

					mapaAtributosModeloAntigo.put(descricaoAtributoAntigo, modeloAtributoAntigo);
				}

				// RETORNA OS ATRIBUTOS QUE EXISTEM NO MODELO ANTIGO E NÃO EXISTEM NO MODELO
				// NOVO
				Collection<ModeloAtributo> listaAtributosRemovidos =
						carregarAtributosAlteradosMigracao(mapaAtributosModeloAntigo, mapaAtributosModeloNovo);
				mapaAtributosRemovidos.put(modeloNovo.getDescricao(true), listaAtributosRemovidos);

				// RETORNA OS ATRIBUTOS QUE EXISTEM NO MODELO NOVO E NÃO EXISTEM NO MODELO
				// ANTIGO
				Collection<ModeloAtributo> listaAtributosAdicionados =
						carregarAtributosAlteradosMigracao(mapaAtributosModeloNovo, mapaAtributosModeloAntigo);
				mapaAtributosAdicionados.put(modeloNovo.getDescricao(true), listaAtributosAdicionados);

				// MANTEM APENAS OS ATRIBUTOS COMUNS AOS DOIS MODELOS
				Collection<ModeloAtributo> listaAtributosComuns = mapaAtributosModeloNovo.values();
				listaAtributosComuns.removeAll(listaAtributosAdicionados);
				mapaAtributosComuns.put(modeloNovo.getDescricao(true), listaAtributosComuns);

				Collection<ModeloAtributoVO> listaAtributosAgrupados =
						agruparAtributosModeloContrato(listaAtributosAdicionados, listaAtributosRemovidos);
				mapaAtributosAdicionadosRemovidos.put(descricaoModelo, listaAtributosAgrupados);
			}

		}

		model.addAttribute(MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS, mapaAtributosAdicionadosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS, mapaAtributosAdicionadosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_ADICIONADOS, mapaAtributosAdicionados);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_REMOVIDOS, mapaAtributosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_COMUNS, mapaAtributosComuns);
	}

	/**
	 * Agrupar atributos modelo contrato.
	 *
	 * @param listaAtributosAdicionados {@link Collection} lista atributos adicionados
	 * @param listaAtributosRemovidos {@link Collection} lista atributos removidos
	 * @return Collecao de ModeloAtributoVO {@link Collection}
	 */
	private Collection<ModeloAtributoVO> agruparAtributosModeloContrato(Collection<ModeloAtributo> listaAtributosAdicionados,
			Collection<ModeloAtributo> listaAtributosRemovidos) {

		Collection<ModeloAtributoVO> listaAdicionadosRemovidos = new ArrayList<ModeloAtributoVO>();

		int quantidadeRemovidos = listaAtributosRemovidos.size();
		int quantidadeAdicionados = listaAtributosAdicionados.size();
		int quantidadeMaior = 0;

		if (quantidadeRemovidos >= quantidadeAdicionados) {
			quantidadeMaior = quantidadeRemovidos;
		} else {
			quantidadeMaior = quantidadeAdicionados;
		}

		for (int i = 0; i < quantidadeMaior; i++) {
			ModeloAtributoVO modeloAtributoVO = new ModeloAtributoVO();
			ModeloAtributo modeloAtributo = new ModeloAtributoImpl();
			if (i < quantidadeAdicionados) {
				ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosAdicionados).get(i);
				AbaAtributo atributoLista = modeloAtributoLista.getAbaAtributo();
				modeloAtributoVO.setObrigatorio(modeloAtributo.isObrigatorio());
				modeloAtributoVO.setPadrao(modeloAtributo.getPadrao());
				modeloAtributoVO.setCampoAdicionado(atributoLista.getDescricao());
				modeloAtributoVO.setNomeColunaAdicionado(atributoLista.getNomeColuna());
			}

			if (quantidadeAdicionados >= quantidadeRemovidos) {
				if (i < quantidadeRemovidos) {
					ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosRemovidos).get(i);
					AbaAtributo removido = modeloAtributoLista.getAbaAtributo();
					modeloAtributoVO.setObrigatorio(modeloAtributo.isObrigatorio());
					modeloAtributoVO.setPadrao(modeloAtributo.getPadrao());
					modeloAtributoVO.setCampoRemovido(removido.getDescricao());
					modeloAtributoVO.setNomeColunaRemovido(removido.getNomeColuna());
				}
			} else if (quantidadeRemovidos > quantidadeAdicionados) {
				ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosRemovidos).get(i);
				AbaAtributo removido = modeloAtributoLista.getAbaAtributo();
				modeloAtributoVO.setCampoRemovido(removido.getDescricao());
				modeloAtributoVO.setNomeColunaRemovido(removido.getNomeColuna());
			}

			listaAdicionadosRemovidos.add(modeloAtributoVO);
		}

		return listaAdicionadosRemovidos;
	}

	/**
	 * Método responsável por verificar se o consumo mínimo estabelecido no contrato foi cumprido ou não.
	 *
	 * @param contrato {@link Contrato}
	 * @param contratoAdequacaoParceria {@link ContratoAdequacaoParceria}
	 * @param request {@link HttpServletRequest}
	 * @param notaDebitoCredito {@link Fatura}
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param cliente {@link Cliente}
	 * @return true, if successful {@link boolean}
	 * @throws GGASException {@link GGASException}
	 */
	private boolean gerarNotaDebito(Contrato contrato, ContratoAdequacaoParceria contratoAdequacaoParceria, HttpServletRequest request,
			Fatura notaDebitoCredito, PontoConsumo pontoConsumo, Cliente cliente) throws GGASException {

		FaturaItem faturaItem = (FaturaItem) controladorFatura.criarFaturaItem();

		// Obtendo dados para popular fatura e nota de débito
		BigDecimal valorPagoCdl = contratoAdequacaoParceria.getValorEmpresa();
		BigDecimal percentualMulta = contratoAdequacaoParceria.getPercentualMulta().divide(new BigDecimal(CEM));
		IndiceFinanceiro indiceFinanceiro = contratoAdequacaoParceria.getIndiceFinanceiro();
		Integer prazoMeses = contratoAdequacaoParceria.getPrazo();
		Date dtVencimentoNotaDebito = Util.getDataCorrente(false);
		dtVencimentoNotaDebito.setDate(dtVencimentoNotaDebito.getDate() + 10);
		TipoDocumento tipoDocumento = controladorArrecadacao.obterTipoDocumento(Long.valueOf(2));
		Rubrica rubrica = (Rubrica) controladorRubrica.obter(4);
		Long idSituacaoNormal = Long.valueOf(
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL));
		CreditoDebitoSituacao creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(idSituacaoNormal);
		Long idSituacaoPendente =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obterEntidadeConteudo(idSituacaoPendente);

		Date dataAssinatura = contratoAdequacaoParceria.getDataInicioVigencia();
		Calendar data = Calendar.getInstance();
		data.setTime(dataAssinatura);
		Date dataFim = data.getTime();
		dataFim.setDate(dataAssinatura.getDate() + prazoMeses * 30);

		Integer diferencaMeses = Util.intervaloDatas(dataAssinatura, new Date()) / 30;

		boolean isGerarMulta = false;
		if (prazoMeses > diferencaMeses) {

			BigDecimal valorIndiceFinanceiro =
					Util.atualizatValorIGPM(dataAssinatura, valorPagoCdl, indiceFinanceiro.getDescricaoAbreviada());
			BigDecimal valorReajustado = valorIndiceFinanceiro.multiply(percentualMulta).add(valorIndiceFinanceiro);
			valorReajustado = valorReajustado.setScale(4, BigDecimal.ROUND_DOWN);
			Double fator = (new BigDecimal(prazoMeses.intValue() - diferencaMeses.intValue())).doubleValue() / prazoMeses;
			BigDecimal fatorBig = new BigDecimal(fator).setScale(6, BigDecimal.ROUND_DOWN);
			BigDecimal valorTotal = valorReajustado.multiply(fatorBig);

			notaDebitoCredito.setCliente(cliente);
			notaDebitoCredito.setContrato(contrato);
			notaDebitoCredito.setPontoConsumo(pontoConsumo);
			notaDebitoCredito.setSegmento(pontoConsumo.getSegmento());
			notaDebitoCredito.setContratoAtual(contrato);
			notaDebitoCredito.setValorConciliado(BigDecimal.ZERO);
			notaDebitoCredito.setValorTotal(valorTotal);
			notaDebitoCredito.setTipoDocumento(tipoDocumento);
			notaDebitoCredito.setObservacaoNota(OBSERVACAO_NOTA_DEBITO);
			notaDebitoCredito.setCreditoDebitoSituacao(creditoDebitoSituacao);
			notaDebitoCredito.setSituacaoPagamento(situacaoPagamento);
			notaDebitoCredito.setCobrada(Boolean.TRUE);
			notaDebitoCredito.setDataVencimento(dtVencimentoNotaDebito);
			notaDebitoCredito.getListaFaturaItem().add(faturaItem);
			notaDebitoCredito.setDataEmissao(Util.getDataCorrente(false));
			notaDebitoCredito.setDadosAuditoria(getDadosAuditoria(request));
			notaDebitoCredito.setProvisaoDevedoresDuvidosos(Boolean.FALSE);

			faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
			faturaItem.setDadosAuditoria(getDadosAuditoria(request));
			faturaItem.setRubrica(rubrica);
			faturaItem.setQuantidade(QUANTIDADE_FATURA);
			faturaItem.setValorUnitario(valorTotal);
			faturaItem.setValorTotal(valorTotal);
			faturaItem.setFatura(notaDebitoCredito);
			faturaItem.setNumeroSequencial(1);

			isGerarMulta = true;

		}

		controladorContratoAdequacaoParceria.removerContratoAdequacaoParceria(contratoAdequacaoParceria);

		return isGerarMulta;

	}

	/**
	 * Preparar filtro contrato adequacao.
	 *
	 * @param filtro {@link Map}
	 * @param numeroContrato {@link String}
	 */
	private void prepararFiltroContratoAdequacao(Map<String, Object> filtro, String numeroContrato) {

		if (StringUtils.isNotEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTRATO, numeroContrato);
		}

		String habilitado = Constantes.TRUE;
		if (StringUtils.isNotEmpty(habilitado)) {
			filtro.put(HABILITADO, Boolean.valueOf(habilitado));
		}

	}

	/**
	 * Exibir Popup Pesquisa de Proposta.
	 *
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaPropostaPopup")
	public String exibirPesquisaPropostaPopup() {

		return "exibirPesquisaPropostaPopup";
	}

	/**
	 * Pesquisar Lista de Propostas.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarPropostaPopup")
	public String pesquisarPropostaPopup(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		FiltroProposta filtroProposta = null;
		ObjectMapper oMapper = new ObjectMapper();

		try {
			filtroProposta = new FiltroProposta(oMapper.convertValue(contratoVO, Map.class));
			Collection<Proposta> listaPropostas = controladorProposta.consultarPropostas(filtroProposta);
			model.addAttribute("listaPropostas", listaPropostas);
		} catch (ParseException e) {
			LOG.error(e.getStackTrace(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaPropostaPopup();

	}

	/**
	 * Imprimir Contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirImprimirContrato")
	public String exibirImprimirContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Long chavePrimaria = contratoVO.getChavePrimaria();

		if (chavePrimaria == null) {
			chavePrimaria = Long.valueOf(request.getParameter(CHAVE_PRIMARIA));
		}

		Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria, MODELO_CONTRATO, PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO,
				LISTA_CONTRATO_CLIENTE, LISTA_CONTRATO_QDC, LISTA_CONTRATO_PENALIDADE, LAYOUT_RELATORIO, CLIENTE_ASSINATURA,
				CONTRATO_PONTO_CONSUMO, CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MAX, CONTRATO_PONTO_CONSUMO_UNIDADE_VAZAO_MIN,
				LISTA_CONTRATO_CLIENTE, CONTRATO_CLIENTE_CLIENTE, CONTRATO_PONTO_CONSUMO_FAIXA_PRESSAO);

		ConstanteSistema constanteTipoRelatorio = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_MODELO_CONTRATO);
		Long tipoRelatorio = constanteTipoRelatorio != null ? constanteTipoRelatorio.getValorLong() : 0l;
		

		Collection<RelatorioToken> listaTokens = controladorRelatorio.obterListaRelatorioTokens(tipoRelatorio);

		RelatorioContratoHelper contratoHelper = new RelatorioContratoHelper();
		contratoHelper.setContrato(contrato);
		if (contrato.getModeloContrato().getLayoutRelatorio() != null) {
			contratoHelper.setLayout(contrato.getModeloContrato().getLayoutRelatorio().getLayout());
		}
		contratoHelper.setTitulo(contrato.getModeloContrato().getDescricao());

		contratoHelper.processarLayout(listaTokens);

		model.addAttribute("layoutRelatorio", contratoHelper.getLayout());

		return "exibirImprimirContrato";
	}

	/**
	 * Aprovar Contrato.
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("aprovarContrato")
	public String aprovarContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			model.addAttribute("contratoVO", contratoVO);
			Long contratoAtivo = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			SituacaoContrato ativo = controladorContrato.obterSituacaoContrato(contratoAtivo);

			Long aguardandoAprovacaoId = Long
					.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_AGUARDANDO_APROVACAO));

			boolean aprovou = false;
			for (Long chavePrimaria : contratoVO.getChavesPrimarias()) {
				Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria);
				if (contrato.getSituacao().getChavePrimaria() == aguardandoAprovacaoId) {
					contrato.setDadosAuditoria(dadosAuditoria);
					contrato.setSituacao(ativo);
					contrato.setUltimaAlteracao(new Date());
					controladorContrato.atualizar(contrato);
					aprovou = true;
				}
			}

			if (aprovou) {
				mensagemSucesso(model, SUCESSO_APROVAR_CONTRATO);
			} else {
				mensagemAlerta(model, NENHUM_CONTRATO_AGUARDANDO_APROVACAO);
			}

		} catch (NumberFormatException e) {
			mensagemErro(model, request, new NegocioException(e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		return pesquisarContrato(contratoVO, result, request, model);
	}

	/**
	 * Remover anexo do contrato
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("removerAnexoContrato")
	public ModelAndView removerAnexoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model) {
		
		ModelAndView gridAnexos = new ModelAndView("gridAnexos");
		
		model.addAttribute("contratoVO", contratoVO);
		model.addAttribute(ABA_ATUAL, ABA_DOCUMENTO_ANEXO);
		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		Collection<ContratoAnexo> listaAnexos = (Collection<ContratoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		if (listaAnexos != null && indexLista != null) {
			ContratoAnexo contratoAnexo = ((ArrayList<ContratoAnexo>) listaAnexos).get(indexLista.intValue());
			listaAnexos.remove(contratoAnexo);
		}

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);
		model.addAttribute(LISTA_ANEXOS, listaAnexos);
		gridAnexos.addObject(LISTA_ANEXOS, listaAnexos);

		return gridAnexos;
	}

	/**
	 * Limpa campo do anexo do contrato
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("limparCampoArquivoContrato")
	public ModelAndView limparCampoArquivoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("divCampoArquivo");

		model.addObject("arquivoAnexo", null);

		return model;
		
	}

	/**
	 * Imprime anexo do contrato
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirArquivoChamadoContrato")
	public void imprimirArquivoChamado(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response) throws GGASException {

		String parameter = request.getParameter("chaveAnexo");

		if (StringUtils.isNotEmpty(parameter)) {
			ContratoAnexo contratoAnexo = controladorContrato.obterContratoAnexo(Long.parseLong(parameter));
			if (contratoAnexo != null && contratoAnexo.getDocumentoAnexo() != null) {
				byte[] arquivo = contratoAnexo.getDocumentoAnexo();

				ServletOutputStream servletOutputStream;
				try {
					servletOutputStream = response.getOutputStream();
					String nomeArquivo = contratoAnexo.getNomeArquivo();
					response.setContentLength(arquivo.length);
					response.addHeader(CONTENT_DISPOSITION, EMAIL_ATTACHMENT_FILENAME + nomeArquivo);
					servletOutputStream.write(arquivo, 0, arquivo.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				} catch (IOException e) {
					LOG.error(e);
					throw new GGASException(e);
				}
			}
		}
	}

	/**
	 * Visualizar campo do anexo do contrato
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 */
	@RequestMapping("visualizarAnexoContrato")
	public void visualizarAnexoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {

		Collection<ContratoAnexo> listaAnexos = (Collection<ContratoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
		Integer indexLista = Integer.valueOf(request.getParameter(INDEX_LISTA));

		for (int i = 0; listaAnexos != null && i < listaAnexos.size(); i++) {

			ContratoAnexo contratoAnexo = ((ArrayList<ContratoAnexo>) listaAnexos).get(i);
			if (i == indexLista && contratoAnexo != null) {

				byte[] anexo = contratoAnexo.getDocumentoAnexo();

				ServletOutputStream servletOutputStream;
				try {
					servletOutputStream = response.getOutputStream();
					String nomeArquivo = contratoAnexo.getNomeArquivo();
					response.setContentLength(anexo.length);
					response.addHeader(CONTENT_DISPOSITION, EMAIL_ATTACHMENT_FILENAME + nomeArquivo);
					servletOutputStream.write(anexo, 0, anexo.length);
					servletOutputStream.flush();
					servletOutputStream.close();

				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * Adiciona anexo do contrato
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("adicionarAnexoContrato")
	public ModelAndView adicionarAnexoContrato(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model) {

		ModelAndView gridAnexos = new ModelAndView("gridAnexos");

		
		request.setAttribute(ABA_ATUAL, ABA_DOCUMENTO_ANEXO);

		ContratoAnexo contratoAnexo = new ContratoAnexo();
		Collection<ContratoAnexo> listaAnexos = (Collection<ContratoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		try {
			popularAnexo(contratoAnexo, contratoVO, request);

			controladorContrato.validarDadosContratoAnexo(contratoAnexo);

			if (indexLista == null || indexLista < 0) {

				contratoAnexo.setDadosAuditoria(getDadosAuditoria(request));

				if (listaAnexos == null || listaAnexos.isEmpty()) {
					listaAnexos = new ArrayList<>();
				}

				controladorContrato.verificarDescricaoContratoAnexo(indexLista, listaAnexos, contratoAnexo);
				listaAnexos.add(contratoAnexo);
			} else {
				this.alterarAnexo(contratoAnexo, indexLista, listaAnexos, request, contratoVO);
			}

		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		gridAnexos.addObject(LISTA_ANEXOS, listaAnexos);

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);

		return gridAnexos;
	}

	/**
	 * Popula dados do anexo
	 *
	 * @param contratoAnexo {@link ContratoAnexo}
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void popularAnexo(ContratoAnexo contratoAnexo, ContratoVO contratoVO, HttpServletRequest request) throws NegocioException {
		
		MultipartHttpServletRequest multiPartRequest = (MultipartHttpServletRequest) request;
		MultipartFile arquivoTemp = multiPartRequest.getFile("arquivoAnexo");
		
//		MultipartFile[] listaArquivos = contratoVO.getAnexo();
		List<MultipartFile> listaArquivos = new ArrayList<MultipartFile>();
		listaArquivos.add(arquivoTemp);
		
//		String descricaoAnexo = Util.utf8ToISO(contratoVO.getDescricaoAnexo());
		String chavePrimariaAnexo = request.getParameter("chavePrimariaAnexo");
		
		String descricaoAnexo = Util.utf8ToISO(request.getParameter("descricaoAnexo"));

		if (StringUtils.isNotEmpty(descricaoAnexo)) {
			contratoAnexo.setDescricaoAnexo(descricaoAnexo);
		}
		if(arquivoTemp != null) {
			try {
				contratoAnexo.setDocumentoAnexo(arquivoTemp.getBytes());
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(e);
			}
		}
		
		String nomeArquivo = "";
		String contentType = "";
		byte[] dadosArquivo = null;
		for (MultipartFile file : listaArquivos) {
			if (file != null && file.getSize() > 0) {
				nomeArquivo = file.getOriginalFilename();
//				nomeArquivo = file.getName();
				contentType = file.getContentType();
				try {
					dadosArquivo = file.getBytes();
				} catch (IOException e) {
					LOG.error(e);
					throw new NegocioException();
				}
			}
		}

		BASE64DecodedMultipartFile arquivo = new BASE64DecodedMultipartFile(dadosArquivo);

		arquivo.setName(nomeArquivo);
		arquivo.setContentType(contentType);

		boolean condicao = false;
		try {
			if (arquivo != null && arquivo.getBytes() != null) {
				condicao = Util.validarImagemDoArquivo(arquivo, EXTENSAO, TAMANHO_MAXIMO_ARQUIVO);
			}
		} catch (IOException e1) {
			LOG.error(e1);
			throw new NegocioException(e1);
		}

		if (condicao && !arquivo.isEmpty()) {
			try {
				contratoAnexo.setDocumentoAnexo(arquivo.getBytes());
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(e);
			}
			contratoAnexo.setNomeArquivo(arquivo.getOriginalFilename());
		}
		if (chavePrimariaAnexo != null) {
			if(!chavePrimariaAnexo.isEmpty()) {
				contratoAnexo.setChavePrimaria(Long.parseLong(chavePrimariaAnexo));
			}
		}
		contratoAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());
		request.getSession().setAttribute("contratoAnexo", contratoAnexo);
	}

	/**
	 * Altera anexo do contrato
	 *
	 * @param contratoAnexoAlterado {@link ContratoAnexo}
	 * @param indexListaAnexo {@link Integer}
	 * @param listaAnexo {@link Collection}
	 * @param request {@link HttpServletRequest}
	 * @param contratoVO {@link ContratoVO}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void alterarAnexo(ContratoAnexo contratoAnexoAlterado, Integer indexListaAnexo, Collection<ContratoAnexo> listaAnexo,
			HttpServletRequest request, ContratoVO contratoVO) throws NegocioException {

		request.setAttribute(ABA_ATUAL, ABA_DOCUMENTO_ANEXO);
		if (listaAnexo != null && indexListaAnexo != null) {
			controladorContrato.verificarDescricaoContratoAnexo(indexListaAnexo, listaAnexo, contratoAnexoAlterado);
			ContratoAnexo contratoAnexoExistente = null;
			ArrayList<ContratoAnexo> listaAnexoArray = new ArrayList<ContratoAnexo>(listaAnexo);
			for (int i = 0; i < listaAnexo.size(); i++) {
				contratoAnexoExistente = listaAnexoArray.get(i);
				if (i == indexListaAnexo && contratoAnexoExistente != null) {
					popularAnexo(contratoAnexoExistente, contratoVO, request);
					contratoAnexoExistente.setDadosAuditoria(getDadosAuditoria(request));
				}
			}
		}
	}

	/**
	 * 
	 * @param contrato
	 */
	private void copiarContrato(Contrato contrato) {

		contratoTitularidade = contrato;

		for (ContratoPontoConsumo listaContratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			PontoConsumo pontoConsumo = listaContratoPontoConsumo.getPontoConsumo();

			contratoCopia = new ContratoImpl();
			Collection<ContratoPontoConsumo> contratoPontoConsumoCopia = new HashSet<ContratoPontoConsumo>();
			ContratoPontoConsumo listaContratoPontoConsumoCopia = new ContratoPontoConsumoImpl();
			Util.copyProperties(listaContratoPontoConsumoCopia, listaContratoPontoConsumo);
			listaContratoPontoConsumoCopia.setPontoConsumo(pontoConsumo);
			listaContratoPontoConsumoCopia.setContrato(contratoCopia);
			contratoPontoConsumoCopia.add(listaContratoPontoConsumoCopia);
			contratoCopia.setListaContratoPontoConsumo(contratoPontoConsumoCopia);
		}

	}

	/**
	 * Inclui uma mudanca de titularidade realizando uma copia de um contrato existente como base
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirMudancaTitularidade")
	public String incluirMudancaTitularidade(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			if ((contratoVO.getIdCliente() == 0L) || (StringUtils.isEmpty(contratoVO.getDataAssinatura()))) {
				mensagemErro(model, Constantes.ERRO_MUDANCA_TITULARIDADE_PREENCHIMENTO);
			} else {

				Cliente clienteAssinatura = (Cliente) controladorCliente.obter(contratoVO.getIdCliente());
				contratoCopia.setClienteAssinatura(clienteAssinatura);

				Date dataAssinaturaContrato = Util.converterCampoStringParaData(Contrato.DATA_ASSINATURA, contratoVO.getDataAssinatura(),
						Constantes.FORMATO_DATA_BR);
				contratoCopia.setDataAssinatura(dataAssinaturaContrato);

				inclusaoContratoTitularidade(contratoVO, request, model);

				// Setar Forma de Cobrança no Objeto contratoCopia

				String idFormaCobranca = String.valueOf(contratoTitularidade.getFormaCobranca().getChavePrimaria());

				EntidadeConteudo formaCobranca = controladorEntidadeConteudo
						.obter(Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, idFormaCobranca));
				contratoCopia.setFormaCobranca(formaCobranca);

				// Fim setar Forma de Cobrança

				// Setar modelo de contrato no Objeto contratoCopia

				Contrato contratoBusca = (Contrato) controladorContrato.obter(contratoTitularidade.getChavePrimaria(), MODELO_CONTRATO,
						PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO, LISTA_CONTRATO_CLIENTE, LISTA_CONTRATO_QDC);

				ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato
						.obter(contratoBusca.getModeloContrato().getChavePrimaria(), ATRIBUTOS, ABAS);

				if (modeloContrato != null) {
					contratoCopia.setModeloContrato(modeloContrato);
					contratoCopia.setPropostaAprovada(this.getValorAtributoPropostaAprovada(modeloContrato));
					contratoCopia.setExigeAprovacao(getValorAtributoExigeAprovacao(modeloContrato));

				}

				contratoCopia.setSituacao(controladorContrato.obterSituacaoContrato(ATIVO));
				contratoCopia.setArrecadadorContratoConvenio(contratoTitularidade.getArrecadadorContratoConvenio());
				contratoCopia.setTipoPeriodicidadePenalidade(contratoTitularidade.getTipoPeriodicidadePenalidade());
				contratoCopia.setNumeroAnterior(contratoTitularidade.getNumeroAnterior());
				contratoCopia.setVersao(contratoTitularidade.getVersao());
				contratoCopia.setAgrupamentoConta(contratoTitularidade.getAgrupamentoConta());
				contratoCopia.setAgrupamentoCobranca(contratoTitularidade.getAgrupamentoCobranca());
				contratoCopia.setDataVencimentoObrigacoes(contratoTitularidade.getDataVencimentoObrigacoes());
				contratoCopia.setValorContrato(contratoTitularidade.getValorContrato());
				contratoCopia.setIndicadorMulta(contratoTitularidade.getIndicadorMulta());
				contratoCopia.setIndicadorJuros(contratoTitularidade.getIndicadorJuros());
				contratoCopia.setChavePrimariaPai(contratoTitularidade.getChavePrimariaPai());
				contratoCopia.setDescricaoContrato(contratoTitularidade.getDescricaoAditivo());
				contratoCopia.setIndicadorAnoContratual(contratoTitularidade.getIndicadorAnoContratual());
				contratoCopia.setPercentualMulta(contratoTitularidade.getPercentualMulta());
				contratoCopia.setPercentualJurosMora(contratoTitularidade.getPercentualJurosMora());
				contratoCopia.setDescricaoContrato(contratoTitularidade.getDescricaoContrato());
				contratoCopia.setHabilitado(true);
				contratoCopia.setChavePrimaria(0);
				contratoCopia.setNumero(null);
				contratoCopia.setChavePrimariaPai(null);
				contratoCopia.setNumeroCompletoContrato(null);
				contratoCopia.setDataRecisao(null);
				contratoCopia.setNumeroAditivo(null);
				contratoCopia.setAnoContrato(null);
				contratoCopia.setValorContrato(null);
				contratoCopia.setUltimaAlteracao(null);

				String situacaoEscolhida = (String) request.getSession().getAttribute(SITUACAO_ESCOLHIDA);
				if (situacaoEscolhida != null && !situacaoEscolhida.isEmpty()) {
					SituacaoContrato situacao = controladorContrato
							.obterSituacaoContrato(Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, situacaoEscolhida));
					contratoCopia.setSituacao(situacao);
				}

				contratoCopia.setDadosAuditoria(this.getDadosAuditoria(request));

				if (contratoCopia.getProposta() != null) {
					controladorProposta.validarPropostasContrato(contratoCopia.getProposta().getNumeroProposta(),
							contratoCopia.isPropostaAprovada());
				}

				String parametroSistema = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO);
				TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel =
						controladorImovel.buscarTipoRelacionamentoClienteImovel(Long.valueOf(parametroSistema));

				Collection<ClienteImovel> listaClienteImovel = new ArrayList<ClienteImovel>();
				Collection<Long> listaIdImovel = new ArrayList<Long>();

				for (ContratoPontoConsumo contratoPontoConsumo : contratoCopia.getListaContratoPontoConsumo()) {
					PontoConsumo pontoConsumo =
							(PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), IMOVEL);
					ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(),
							contratoCopia.getClienteAssinatura().getChavePrimaria(), Long.valueOf(parametroSistema));

					if (clienteImovel == null && listaIdImovel != null
							&& !listaIdImovel.contains(pontoConsumo.getImovel().getChavePrimaria())) {
						ClienteImovel clienteImovelNovo = (ClienteImovel) controladorImovel.criarClienteImovel();
						if (contratoTitularidade.getDataAssinatura() != null) {
							clienteImovelNovo.setRelacaoInicio(contratoTitularidade.getDataAssinatura());
						} else {
							clienteImovelNovo.setRelacaoInicio(new Date());
						}
						clienteImovelNovo.setCliente(contratoCopia.getClienteAssinatura());
						clienteImovelNovo.setImovel(pontoConsumo.getImovel());

						clienteImovelNovo.setTipoRelacionamentoClienteImovel(tipoRelacionamentoClienteImovel);
						listaClienteImovel.add(clienteImovelNovo);

						listaIdImovel.add(pontoConsumo.getImovel().getChavePrimaria());

					}
				}

				Long idContrato = controladorContrato.inserirContrato(contratoCopia);
				controladorClienteImovel.inserirClienteImovel(listaClienteImovel);
				request.setAttribute(CHAVE_PRIMARIA, idContrato);
				contratoVO.setChavePrimaria(idContrato);
				mensagemSucesso(model, Constantes.CONTRATO_INCLUIDO_SUCESSO, contratoCopia.getNumeroFormatado());

				this.limparDadosSessao(request);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirPopupMudancaTitularidade";
	}

	/**
	 * Inclui Contrato Titularidade
	 *
	 * @param contratoVO {@link ContratoVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	public void inclusaoContratoTitularidade(ContratoVO contratoVO, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (request.getSession().getAttribute(SITUACAO_ESCOLHIDA) != null) {
			contratoVO.setSituacaoContrato(String.valueOf(request.getSession().getAttribute(SITUACAO_ESCOLHIDA)));
		}

		request.setAttribute(INDICADOR_CONTRATO_EXIBIDO, contratoVO.getIndicadorContratoExibido());
		request.getSession().removeAttribute(PONTO_CONSUMO_EM_ALTERACAO);

		ModeloContrato modeloContrato = null;
		if (contratoVO.getIdModeloContrato() != null && contratoVO.getIdModeloContrato() > 0) {
			modeloContrato = (ModeloContrato) controladorModeloContrato.obter(contratoVO.getIdModeloContrato(), ATRIBUTOS, ABAS);
			if (contratoVO.getMudancaModelo() != null && contratoVO.getMudancaModelo()) {
				limparDadosSessao(request);

				if (contratoVO.getChavePrimariaPrincipal() != null && contratoVO.getChavePrimariaPrincipal() > 0) {

					Contrato contrato = (Contrato) controladorContrato.obter(contratoVO.getChavePrimariaPrincipal(), MODELO_CONTRATO,
							PROPOSTA, LISTA_CONTRATO_PONTO_CONSUMO, LISTA_CONTRATO_CLIENTE, LISTA_CONTRATO_QDC);

					contrato.setChavePrimaria(0L);
					contrato.setChavePrimariaPrincipal(contratoVO.getChavePrimariaPrincipal());
					contrato.setHabilitado(Boolean.TRUE);
					this.popularFormContrato(contrato, contratoVO, request, model);
					this.setarPontoConsumoContratoNoRequest(contratoVO, request, contrato.getListaContratoPontoConsumo(), model);
					this.popularSessaoListaContratoPontoConsumo(request, contrato, true);
				}

				this.carregarCampos(contratoVO, modeloContrato, request, true, model);
				request.setAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoPadrao());

			} else {
				this.carregarCampos(contratoVO, modeloContrato, request, false, model);
				request.setAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContratoPadrao());
				Contrato contratoSessao = (Contrato) request.getSession().getAttribute(CONTRATO);

				if ((contratoSessao != null) && (!isPostBack(request) && !(contratoVO.getFluxoMigracao() != null
						&& (contratoVO.getFluxoMigracao().equals(ALTERACAO) || contratoVO.getFluxoMigracao().equals(ADITAMENTO))))) {
					this.popularFormContrato(contratoSessao, contratoVO, request, model);
					if (contratoSessao.getProposta() != null) {
						Collection<Proposta> propostas =
								controladorProposta.listarPropostasAprovadasPorNumero(contratoSessao.getProposta().getNumeroProposta());
						request.setAttribute(LISTA_VERSOES_PROPOSTA, propostas);
					}
				}
			}
		}

		this.configurarCampoMonetarioForm(contratoVO, GASTO_ESTIMADO_GN_MES);
		this.configurarCampoMonetarioForm(contratoVO, ECONOMIA_ESTIMADA_GN_MES);
		this.configurarCampoMonetarioForm(contratoVO, ECONOMIA_ESTIMADA_GN_ANO);
		this.configurarCampoMonetarioForm(contratoVO, VALOR_PARTICIPACAO_CLIENTE);
		this.configurarCampoMonetarioForm(contratoVO, VALOR_CONTRATO);
		this.configurarCampoMonetarioForm(contratoVO, VALOR_GARANTIA_FINANCEIRA);
		this.configurarCampoMonetarioForm(contratoVO, VALOR_INVESTIMENTO);

		request.setAttribute(MODELO_CONTRATO, modeloContrato);
		request.setAttribute(Constantes.NOME_FORM, "contratoForm");

		request.setAttribute(LISTA_ANEXOS, request.getSession().getAttribute(LISTA_ANEXOS));

		request.getSession().removeAttribute(LISTA_MODALIDADE_CONSUMO_FATURAMENTO_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_TAKE_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_PENALIDADES_RETIRADA_MAIOR_MENOR_MODALIDADE_VO);
		request.getSession().removeAttribute(LISTA_COMPROMISSOS_SHIP_OR_PAY_VO);
		request.getSession().removeAttribute(LISTA_ITEM_FATURAMENTO_CONTRATO_VO);
		request.getSession().removeAttribute(LISTA_CONTRATO_CLIENTE);

		filtro.put(HABILITADO, Boolean.TRUE);
		if (contratoVO.getChavePrimariaPrincipal() != null && contratoVO.getChavePrimariaPrincipal() > 0) {
			filtro.put(TIPO_MODELO, Long.parseLong(
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR)));
		} else {
			filtro.put(TIPO_MODELO, Long.parseLong(
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_PRINCIPAL)));
		}
		request.setAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));
	}

	private Collection<Contrato> filtrarContratoPorDatas(Collection<Contrato> listaContrato) {
		Collection<Contrato> listaContratoAtual = new ArrayList<>();
		for (Contrato contrato : listaContrato) {

			if (contrato.getDataAssinatura() != null && contrato.getDataRecisao() == null
					&& contrato.getDataAssinatura().compareTo(DataUtil.gerarDataHoje()) <= 0) {
				listaContratoAtual.add(contrato);
			}
		}
		return listaContratoAtual;
	}

	/**
	 * @return listaArrecadadorConvenioBoleto - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioBoletoEArrecadacao() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, TRUE);

		Collection<ArrecadadorContratoConvenio> listaArrecadadorConvenioBoleto =
				ServiceLocator.getInstancia().getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);

		listaArrecadadorConvenioBoleto.removeAll(this.obterListaArrecadadorConvenioDebitoAutomatico());

		return listaArrecadadorConvenioBoleto;
	}

	/**
	 * @return Uma lista arrecadador convenio debito Automatico - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioDebitoAutomatico() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, TRUE);
		filtro.put("tipoConvenio", Long.valueOf(ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO)));

		return ServiceLocator.getInstancia().getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);
	}

	private void modificarFluxosEstadoDefault(ContratoVO contratoVO) {

		contratoVO.setFluxoPesquisa(true);
		contratoVO.setFluxoAditamento(false);
		contratoVO.setFluxoAlteracao(false);
		contratoVO.setFluxoInclusao(false);
		contratoVO.setFluxoDetalhamento(false);

	}

	public boolean isChamadoTela() {
		return chamadoTela;
	}

	public void setChamadoTela(boolean chamadoTela) {
		this.chamadoTela = chamadoTela;
	}

	public Contrato getContratoTitularidade() {
		return contratoTitularidade;
	}

	public void setContratoTitularidade(Contrato contratoTitularidade) {
		this.contratoTitularidade = contratoTitularidade;
	}

	public Contrato getContratoCopia() {
		return contratoCopia;
	}

	public void setContratoCopia(Contrato contratoCopia) {
		this.contratoCopia = contratoCopia;
	}
	
	@RequestMapping("salvarDataLeitura")
	public ModelAndView salvarDataLeitura(@RequestBody JSONDadosFaturaResidual json, HttpServletRequest request)
			throws ParseException, GGASException {
		ModelAndView model = new ModelAndView("exibirEncerrarRescindirContrato");
		Map<Long, List<String>> dadosFaturaResidual = null;
		List<String> dadosDataLeitura = new ArrayList<>();
		Long chavePontoConsumo = json.getIdPontoConsumo();
		dadosDataLeitura.add(json.getDataLeitura());
		dadosDataLeitura.add(json.getValorLeitura());
		dadosDataLeitura.add(json.getSituacaoContrato());

		if (request.getSession().getAttribute("dadosFaturaResidual") != null) {
			dadosFaturaResidual = (Map<Long, List<String>>) request.getSession().getAttribute("dadosFaturaResidual");

			if (dadosFaturaResidual.containsKey(chavePontoConsumo)) {
				dadosFaturaResidual.remove(chavePontoConsumo);
			}

		} else {
			dadosFaturaResidual = new HashMap<>();
		}

		dadosFaturaResidual.put(chavePontoConsumo, dadosDataLeitura);

		request.getSession().setAttribute("dadosFaturaResidual", dadosFaturaResidual);

		return model;

	}
	
	@RequestMapping("exibirInclusaoDadosFaturaResidualPopup")
	public String exibirInclusaoDadosFaturaResidualPopup(ContratoVO contratoVO, BindingResult result, HttpServletRequest request, Model model)
			throws ParseException, GGASException {
		
		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO = this.obterListaImovelPontoConsumoVO(request);
		Collection<PontoConsumo> listaPontoConsumo = new HashSet<>();
		
		for(ImovelPontoConsumoVO imovelPontoConsumoVO : listaImovelPontoConsumoVO) {
			listaPontoConsumo.addAll(imovelPontoConsumoVO.getListaPontoConsumo());
		}
		
		
		model.addAttribute("listaPontoConsumoResidual", listaPontoConsumo);
		model.addAttribute("listaFuncionario", obterFuncionarios());
		model.addAttribute("funcionarioSelecionado", getDadosAuditoria(request).getUsuario().getFuncionario());

		

		return "exibirInclusaoDadosFaturaResidualPopup";

	}
	
	/**
	 * Obter funcionarios.
	 *
	 * @return collection
	 * @throws GGASException
	 */
	private Collection<Funcionario> obterFuncionarios() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(HABILITADO, true);

		return controladorFuncionario.consultarFuncionarios(filtro);

	}
	
	
	private void validarSeClientePossuiDebito(Long idCliente, HttpServletRequest request, Model model) throws GGASException {
		String indicadorClienteComDebito = (String) request.getParameter("indicadorClienteComDebito");
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		Cliente cliente = Fachada.getInstancia().obterCliente(idCliente);
			
		if((Objects.isNull(indicadorClienteComDebito) || !indicadorClienteComDebito.equals("ignorar")) && cliente != null) {
			
			String identificacaoCliente = cliente.getCpf() != null ? cliente.getCpf() : cliente.getCnpj();
			
			List<Object> faturasPendentes = controladorFatura.listarFaturasAbertoPorClienteGSA(identificacaoCliente);
			if (!faturasPendentes.isEmpty()) {
				model.addAttribute("indicadorClienteComDebito", Boolean.TRUE.toString());
				throw new GGASException("Cliente com debito");
			}
		}
		
	}


}