/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.util.GGASBaseTag;
import br.com.ggas.util.Util;

public class CampoSelecionadoTag extends GGASBaseTag {

	private String campo;

	/**
     * 
     */
	private static final long serialVersionUID = -8208804912554295126L;

	/**
	 * @return the campo
	 */
	public String getCampo() {

		return campo;
	}

	/**
	 * @param campo
	 *            the campo to set
	 */
	public void setCampo(String campo) {

		this.campo = campo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.GGASBaseTag#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspException {

		StringBuilder html = new StringBuilder();
		if(!StringUtils.isEmpty(this.campo)) {
			String campoSelecao = Util.obterNomeCampoSelecao(this.campo);
			boolean selecionado = this.obterValorAtributoForm(campoSelecao);
			if(!selecionado) {
				html.append("disabled='disabled'");
			}
		}

		try {
			pageContext.getOut().write(html.toString());
		} catch(IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

}
