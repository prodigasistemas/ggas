/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.modelocontrato;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Aba;
import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.AbaModelo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.contrato.LayoutRelatorio;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.contrato.contrato.impl.ModeloAtributoImpl;
import br.com.ggas.contrato.contrato.impl.ModeloAtributoVO;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Agrupador;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Módelo.
 */
@Controller
public class ModeloContratoAction extends GenericAction {

	private static final String TRUE = "true";

	private static final String MODELO_CONTRATO_VO = "modeloContratoVO";

	private static final String TIPO_MODELO = "tipoModelo";

	public static final String ABA_RESPONSABILIDADE = "selAbaResponsabilidade";

	public static final String ABA_REGRAS_FATURAMENTO = "selAbaRegrasFaturamento";

	public static final String ABA_FATURAMENTO = "selAbaFaturamento";

	public static final String ABA_CONSUMO = "selAbaConsumo";

	public static final String ABA_TECNICOS = "selAbaTecnicos";

	public static final String ABA_PRINCIPAIS = "selAbaPrincipais";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String NAO_SELECIONADO = "-1";

	private static final String LISTA_ARRECADADOR_CONVENIO = "listaArrecadadorConvenio";
	
	private static final String LISTA_ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO = "listaArrecadadorConvenioDebitoAutomatico";
	
	/** The Constant INTERVALO_RECUPERACAO_PCS. */
	private static final String INTERVALO_RECUPERACAO_PCS = "intervaloRecuperacaoPCS";

	/** The Constant LOCAL_AMOSTRAGEM_PCS. */
	private static final String LOCAL_AMOSTRAGEM_PCS = "localAmostragemPCS";

	/** The Constant LISTA_INTERVALO_PCS. */
	private static final String LISTA_INTERVALO_PCS = "listaIntervaloPCS";

	/** The Constant LISTA_LOCAL_AMOSTRAGEM_PCS. */
	private static final String LISTA_LOCAL_AMOSTRAGEM_PCS = "listaLocalAmostragemPCS";
	
	/** The Constant LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS. */
	private static final String LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS = "listaIntervaloAmostragemAssociados";

	/** The Constant LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS. */
	private static final String LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS = "listaLocalAmostragemAssociados";	
	
	/** The Constant CHAVES_PRIMARIAS. */
	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";
	
	/** The Constant CHAVES_MODELO_CONTRATO. */
	private static final String CHAVES_MODELO_CONTRATO = "chavesModeloContrato";

	/** The Constant FLUXO_MIGRACAO. */
	private static final String FLUXO_MIGRACAO = "fluxoMigracao";
	
	/** The Constant FLUXO_MODELO. */
	private static final String FLUXO_MODELO = "fluxoModelo";
	
	/** The Constant MAPA_CONTRATO_MIGRACAO. */
	private static final String MAPA_CONTRATO_MIGRACAO = "mapaContratoMigracao";
	
	/** The Constant LISTA_CONTRATO_MIGRACAO. */
	private static final String LISTA_CONTRATO_MIGRACAO = "listaContratoMigracao";
	
	/** The Constant LISTA_SITUACAO_CONTRATO. */
	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";

	/** The Constant LISTA_MODELOS_CONTRATO. */
	private static final String LISTA_MODELOS_CONTRATO = "listaModelosContrato";

	/** The Constant LISTA_TIPO_CONTRATO. */
	private static final String LISTA_TIPO_CONTRATO = "listaTipoContrato";
	
	/** The Constant ABAS. */
	private static final String ABAS = "abas";

	/** The Constant ATRIBUTOS. */
	private static final String ATRIBUTOS = "atributos";
	
	/** The Constant MODELO_NOVO. */
	private static final String MODELO_NOVO = "modeloNovo";
	
	/** The Constant MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS. */
	private static final String MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS = "mapaAtributosAdicionadosRemovidos";
	
	/** The Constant MAPA_ATRIBUTOS_COMUNS. */
	private static final String MAPA_ATRIBUTOS_COMUNS = "mapaAtributosComuns";

	/** The Constant MAPA_ATRIBUTOS_REMOVIDOS. */
	private static final String MAPA_ATRIBUTOS_REMOVIDOS = "mapaAtributosRemovidos";

	/** The Constant MAPA_ATRIBUTOS_ADICIONADOS. */
	private static final String MAPA_ATRIBUTOS_ADICIONADOS = "mapaAtributosAdicionados";
	
	/** The Constant LOG_ERRO_MIGRACAO. */
	private static final String LOG_ERRO_MIGRACAO = "logErroMigracao";
	
	/** The Constant LABEL_COM_ERRO. */
	private static final String LABEL_COM_ERRO = "com erro";
	
	/** The Constant EMAIL_APPLICATION_DOWNLOAD. */
	private static final String EMAIL_APPLICATION_DOWNLOAD = "application/download";
	
	/** The Constant EMAIL_CONTENT_DISPOSITION. */
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	
	/** The Constant EMAIL_ATTACHMENT_FILENAME. */
	private static final String EMAIL_ATTACHMENT_FILENAME = "attachment; filename=";
	
	/** The Constant EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG. */
	private static final String EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG = "Migração Modelo do Contrato.log";

	private static final String LAYOUT_RELATORIO = "layoutRelatorio";

	private boolean isSelected;

	private boolean isObrigatorioVal;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorModeloContrato controladorModeloContrato;
	
	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Autowired
	private ControladorUnidade controladorUnidade;
	
	@Autowired
	private ControladorTarifa controladorTarifa;
	
	@Autowired
	private ControladorAnormalidade controladorAnormalidade;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;
	
	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorBanco controladorBanco;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorRelatorio controladorRelatorio;

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de modelo de contrato.
	 * 
	 * @param modeloContratoVO				-	{@link ModeloContratoVO}
	 * @param request						-	{@link HttpServletRequest}
	 * @param model							-	{@link Model}
	 * @return exibirPesquisaModeloContrato	-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	@RequestMapping("exibirPesquisaModeloContrato")
	public String exibirPesquisaModeloContrato(ModeloContratoVO modeloContratoVO, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			model.addAttribute("listaTipoModelo", controladorEntidadeConteudo.obterListaTipoModelo());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(MODELO_CONTRATO_VO, modeloContratoVO);

		return "exibirPesquisaModeloContrato";
	}

	/**
	 * Método responsável por pesquisar modelos de
	 * contrato.
	 * 
	 * @param modeloContratoVO				-	{@link ModeloContratoVO}
	 * @param result						-	{@link BindingResult}
	 * @param request						-	{@link HttpServletRequest}
	 * @param model							-	{@link Model}
	 * @return exibirPesquisaModeloContrato	-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	@RequestMapping("pesquisarModeloContrato")
	public String pesquisarModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(modeloContratoVO);

		try {
			Collection<ModeloContrato> listaModeloContrato = controladorModeloContrato.consultarModelosContrato(filtro);
			model.addAttribute("listaModeloContrato", listaModeloContrato);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaModeloContrato(modeloContratoVO, request, model);
	}
	
	/**
	 * Método responsável por exibir o
	 * detalhamento de um modelo de contrato.
	 * 
	 * @param modeloContratoVO						-	{@link ModeloContratoVO}
	 * @param result								-	{@link BindingResult}
	 * @param request								-	{@link HttpServletRequest}
	 * @param model									-	{@link Model}
	 * @return exibirDetalhamentoModeloContrato 	-	{@link String}
	 * @throws GGASException						-	{@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoModeloContrato")
	public String exibirDetalhamentoModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoModeloContrato";
		Long chavePrimaria = modeloContratoVO.getChavePrimaria();

		try {
			ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.obter(chavePrimaria, ATRIBUTOS,
					ABAS, LAYOUT_RELATORIO);
			this.popularForm(modeloContratoVO, request, modeloContrato, model);
			this.carregarCampos(request, model);
			model.addAttribute(MODELO_CONTRATO_VO, modeloContratoVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);
		}

		return view;
	}
	
	/**
	 * Método responsável por exibir a tela de
	 * inclusão de modelo contrato.
	 * 
	 * @param modeloContratoVO				-	{@link ModeloContratoVO}
	 * @param request						-	{@link HttpServletRequest}
	 * @param model							-	{@link Model}
	 * @return exibirInclusaoModeloContrato	-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	@RequestMapping("exibirInclusaoModeloContrato")
	public String exibirInclusaoModeloContrato(ModeloContratoVO modeloContratoVO, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);

		this.carregarCampos(request, model);

		if (!isPostBack(request)) {

			String horaInicioDia = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_HORA_INICIO_DIA);
			modeloContratoVO.setHoraInicialDia(horaInicioDia);

			String diasAntecedenciaRenovacaoContratual = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIAS_ANTECEDENCIA_RENOVACAO_CONTRATUAL);
			modeloContratoVO.setTempoAntecedenciaRenovacao(diasAntecedenciaRenovacaoContratual);

			String indicadorAnoContratual = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ANO_CONTRATUAL);
			modeloContratoVO.setIndicadorAnoContratual(converterAnoContratual(indicadorAnoContratual));
			
			String tipoPeriodicidadePenalidade = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_PERIODICIDADE_PENALIDADE);
			modeloContratoVO.setTipoPeriodicidadePenalidade(BooleanUtil.converterStringCharParaStringBooleano(tipoPeriodicidadePenalidade));
			
			String boletoBancario = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_BOLETO_BANCARIO);
			modeloContratoVO.setFormaCobranca(boletoBancario);
			
			String itemFaturaGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);
			modeloContratoVO.setItemFatura(itemFaturaGas);
		}

		model.addAttribute(MODELO_CONTRATO_VO, modeloContratoVO);

		return "exibirInclusaoModeloContrato";
	}
	
	/**
	 * Método responsável por incluir um modelo de
	 * contrato.
	 * 
	 * @param modeloContratoVO				-	{@link ModeloContratoVO}
	 * @param result						-	{@link BindingResult}
	 * @param request						-	{@link HttpServletRequest}
	 * @param model							-	{@link Model}
	 * @return exibirInclusaoModeloContrato	-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	@RequestMapping("incluirModeloContrato")
	public String incluirModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		isSelected = Boolean.valueOf(false);
		isObrigatorioVal = Boolean.valueOf(false);

		try {
			validarToken(request);
			ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.criar();

			popularModelo(modeloContrato, modeloContratoVO, request, model);
			validarModalidade(modeloContratoVO);
			validarTipoModelo(modeloContrato);

			if (controladorConstanteSistema.isContratoTipoModeloContratoComplementar(modeloContrato.getTipoModelo())) {
				controladorModeloContrato.validarModeloContratoComplementar(modeloContrato);
			}
			controladorModeloContrato.inserir(modeloContrato);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, ModeloContrato.MODELO_CONTRATO);

			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirInclusaoModeloContrato(modeloContratoVO, request, model);
		}

		return view;

	}
	
	/**
	 * Método responsável por exibir uma tela de
	 * alteração de modelo.
	 * 
	 * @param modeloContratoVO					-	{@link ModeloContratoVO}
	 * @param result							-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirAlteracaoModeloContrato	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoModeloContrato")
	public String exibirAlteracaoModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);
		
		String view = "exibirAlteracaoModeloContrato";
		Long chavePrimaria = modeloContratoVO.getChavePrimaria();

		try {

			controladorModeloContrato.validarAtualizarModeloContrato(chavePrimaria);
			ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.obter(chavePrimaria, ATRIBUTOS,
					ABAS, LAYOUT_RELATORIO);
			popularForm(modeloContratoVO, request, modeloContrato, model);
			carregarCampos(request, model);

		} catch (GGASException e) {

			mensagemErroParametrizado(model, request, e);
			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);

		}

		return view;
	}

	/**
	 * Método responsável por alterar um modelo.
	 * 
	 * @param modeloContratoVO			-	{@link ModeloContratoVO}
	 * @param result					-	{@link BindingResult}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @return pesquisarModeloContrato	-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@RequestMapping("alterarModeloContrato")
	public String alterarModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		isSelected = Boolean.valueOf(false);
		isObrigatorioVal = Boolean.valueOf(false);
		Long chavePrimaria = modeloContratoVO.getChavePrimaria();

		try {

			validarToken(request);

			ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.criar();
			modeloContrato.setChavePrimaria(chavePrimaria);
			popularModelo(modeloContrato, modeloContratoVO, request, model);
			validarModalidade(modeloContratoVO);

			if (modeloContrato.getTipoModelo().getChavePrimaria() == Long.parseLong(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR))) {
				controladorModeloContrato.validarModeloContratoComplementar(modeloContrato);
			}

			modeloContrato.setDadosAuditoria(getDadosAuditoria(request));
			controladorModeloContrato.atualizar(modeloContrato);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, ModeloContrato.MODELO_CONTRATO);

			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoModeloContrato(modeloContratoVO, result, request, model);
		}

		return view;
	}
	
	/**
	 * Método responsável por excluir os modelos
	 * de contrato selecionados.
	 * 
	 * @param modeloContratoVO			-	{@link ModeloContratoVO}
	 * @param result					-	{@link BindingResult}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @return pesquisarModeloContrato	-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@RequestMapping("removerModeloContrato")
	public String removerModeloContrato(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Long[] chavesPrimarias = modeloContratoVO.getChavesPrimarias();

		try {
			controladorModeloContrato.validarRemoverModelosContrato(chavesPrimarias);
			controladorModeloContrato.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, ModeloContrato.MODELOS_CONTRATO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarModeloContrato(modeloContratoVO, result, request, model);
	}

	/**
	 * Converter ano contratual.
	 * 
	 * @param valorAnoContratual		-	{@link String}		
	 * @return indicadorAnoContratual	-	{@link String}
	 * @throws FormatoInvalidoException	-	{@link FormatoInvalidoException}
	 */
	private String converterAnoContratual(String valorAnoContratual) throws FormatoInvalidoException {

		String indicadorAnoContratual = "false";

		if (Util.converterCampoStringParaValorInteger("Ano Contratual", valorAnoContratual) == 1) {
			indicadorAnoContratual = TRUE;
		}

		return indicadorAnoContratual;
	}

	/**
	 * @param modeloContrato	-	{@link ModeloContrato}
	 * @throws NegocioException	-	{@link NegocioException}
	 */
	private void validarTipoModelo(ModeloContrato modeloContrato) throws NegocioException {
		if (modeloContrato.getTipoModelo() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					Constantes.MODELO_CONTRATO_TIPO_MODELO);
		}
	}

	/**
	 * Validar modalidade.
	 * 
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @throws NegocioException	-	{@link NegocioException}
	 */
	private void validarModalidade(ModeloContratoVO modeloContratoVO) throws NegocioException {

		Boolean selec;
		Boolean obrig;
		selec = modeloContratoVO.getSelModalidade();
		obrig = modeloContratoVO.getObgModalidade();

		if (isSelected && selec == null) {

			selec = false;
			controladorModeloContrato.validarDadosRegraFaturamento(selec, obrig);
		}
		if (isObrigatorioVal && obrig == null) {

			obrig = false;
			controladorModeloContrato.validarDadosRegraFaturamento(selec, obrig);
		}

	}

	/**
	 * Popular modelo.
	 * 
	 * @param modeloContrato	-	{@link ModeloContrato}
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 */
	private void popularModelo(ModeloContrato modeloContrato, ModeloContratoVO modeloContratoVO,
			HttpServletRequest request, Model model) {

		String descricao = modeloContratoVO.getDescricao();
		String layout = modeloContratoVO.getLayoutRelatorio();
		String descricaoAbreviada = modeloContratoVO.getDescricaoAbreviada();
		String tipoModelo = modeloContratoVO.getTipoModelo();
		String itemFatura = modeloContratoVO.getItemFatura();
		String formaCobranca = modeloContratoVO.getFormaCobranca();
		Boolean habilitado = modeloContratoVO.getHabilitado();
		Integer versao = modeloContratoVO.getVersao();

		Long[] idsLocalAmostragemAssociados = modeloContratoVO.getIdsLocalAmostragemAssociados();
		Long[] idsIntervaloAmostragemAssociados = modeloContratoVO.getIdsIntervaloAmostragemAssociados();

		model.addAttribute("localAmostragemPCS",
				Util.arrayParaString(idsLocalAmostragemAssociados, Constantes.STRING_VIRGULA));
		model.addAttribute("intervaloRecuperacaoPCS",
				Util.arrayParaString(idsIntervaloAmostragemAssociados, Constantes.STRING_VIRGULA));
		request.setAttribute("localAmostragemPCS", Util.arrayParaString(idsLocalAmostragemAssociados, Constantes.STRING_VIRGULA));
		request.setAttribute("intervaloRecuperacaoPCS", Util.arrayParaString(idsIntervaloAmostragemAssociados, Constantes.STRING_VIRGULA));

		if (modeloContrato != null) {

			if (versao != null && versao.intValue() > 0) {
				modeloContrato.setVersao(versao);
			}

			if (StringUtils.isNotEmpty(descricao)) {
				modeloContrato.setDescricao(descricao);
			} else {
				modeloContrato.setDescricao(null);
			}

			if (StringUtils.isNotEmpty(layout)) {
				LayoutRelatorio layoutRelatorio = (LayoutRelatorio) controladorModeloContrato.criarLayoutRelatorio();
				layoutRelatorio.setLayout(layout);
				modeloContrato.setLayoutRelatorio(layoutRelatorio);
			} else {
				modeloContrato.setLayoutRelatorio(null);
			}

			if (StringUtils.isNotEmpty(descricaoAbreviada)) {
				modeloContrato.setDescricaoAbreviada(descricaoAbreviada);
			} else {
				modeloContrato.setDescricaoAbreviada(null);
			}

			if (tipoModelo != null && StringUtils.isNotEmpty(tipoModelo) && Long.parseLong(tipoModelo) > 0) {
				modeloContrato
						.setTipoModelo(controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(tipoModelo)));
			} else {
				modeloContrato.setTipoModelo(null);
			}

			if (habilitado != null) {
				modeloContrato.setHabilitado(habilitado);
			}

			modeloContrato.setFormaCobranca(formaCobranca);
			modeloContrato.setItemFatura(itemFatura);

			modeloContrato.getAtributos().clear();
			modeloContrato.getAbas().clear();

			try {

				Collection<AbaAtributo> atributos = controladorModeloContrato.listarAbasAtributos();
				Collection<Aba> abas = new HashSet<>();
				if (atributos != null && !atributos.isEmpty()) {

					String campo;
					String campoSelecao;
					String campoObrigatorio;
					String campoValorFixo;
					boolean atributoSelecionado;

					for (AbaAtributo abaAtributo : atributos) {
						if (abaAtributo.getAbaAtributoPai() == null) {
							campo = abaAtributo.getNomeColuna();

							if (StringUtils.isNotEmpty(campo)) {
								campoSelecao = Util.obterNomeCampoSelecao(campo);
								campoObrigatorio = Util.obterNomeCampoObrigatorio(campo);
								campoValorFixo = Util.obterNomeCampoValorFixo(campo);
								atributoSelecionado = this.adicionarAtributoModeloContrato(modeloContrato, request,
										campoSelecao, campoObrigatorio, campoValorFixo, campo, abaAtributo);
								if (atributoSelecionado) {
									abas.add(abaAtributo.getAba());
								}
							}
						}
					}

					if (abas != null && !abas.isEmpty()) {
						for (Aba aba : abas) {
							AbaModelo abaModelo = (AbaModelo) controladorModeloContrato.criarAbaModelo();
							abaModelo.setAba(aba);
							abaModelo.setUltimaAlteracao(Calendar.getInstance().getTime());
							modeloContrato.getAbas().add(abaModelo);
						}
					}
				}

				modeloContrato.setDadosAuditoria(getDadosAuditoria(request));
				model.addAttribute("modeloContrato", modeloContrato);

			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
			}

		}
	}

	/**
	 * Adicionar atributo modelo contrato.
	 * 
	 * @param modeloContrato	-	{@link ModeloContrato}
	 * @param request			-	{@link HttpServletRequest}
	 * @param selecionadoParam	-	{@link String}
	 * @param obrigatorioParam	-	{@link String}
	 * @param valorFixoParam	-	{@link String}
	 * @param padraoParam		-	{@link String}
	 * @param abaAtributo		-	{@link AbaAtributo}
	 * @return isSelecionado	-	{@link Boolean}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private boolean adicionarAtributoModeloContrato(ModeloContrato modeloContrato, HttpServletRequest request,
			String selecionadoParam, String obrigatorioParam, String valorFixoParam, String padraoParam,
			AbaAtributo abaAtributo) throws GGASException {

		Boolean selec = null;
		Boolean obrig = null;
		Boolean valorFixo = false;

		String valorPadrao = null;

		if (request.getParameterMap().containsKey(selecionadoParam)) {
			selec = Boolean.valueOf(request.getParameter(selecionadoParam));
		}
		if (request.getParameterMap().containsKey(obrigatorioParam)) {
			obrig = Boolean.valueOf(request.getParameter(obrigatorioParam));
		}
		if (request.getParameterMap().containsKey(valorFixoParam)) {
			valorFixo = Boolean.valueOf(request.getParameter(valorFixoParam));
		}
		if (request.getParameterMap().containsKey(padraoParam)) {
			valorPadrao = request.getParameter(padraoParam);
		} else if(request.getAttribute(padraoParam) != null) {
			valorPadrao = (String) request.getAttribute(padraoParam);
		}

		boolean isSelecionado;
		boolean isObrigatorio;
		boolean isValorFixo;

		if (selec == null || !selec) {
			isSelecionado = false;
		} else {
			isSelecionado = true;
		}

		if (obrig == null || !obrig) {
			isObrigatorio = false;
		} else {
			isObrigatorio = true;
		}

		if (valorFixo == null || !valorFixo) {
			isValorFixo = false;
		} else {
			isValorFixo = true;
		}

		if (isSelecionado) {

			if (abaAtributo.getAba().getDescricaoAbreviada().equals(Aba.REGRAS_FATURAMENTO)) {
				isSelected = Boolean.valueOf(false);
			}

			ModeloAtributo atributo = (ModeloAtributo) controladorModeloContrato.criarModeloAtributo();
			atributo.setAbaAtributo(abaAtributo);
			atributo.setObrigatorio(isObrigatorio);
			validarObrigatoriedade(abaAtributo, isObrigatorio);

			if (StringUtils.isNotEmpty(valorPadrao) && !NAO_SELECIONADO.equals(valorPadrao)) {
				atributo.setPadrao(valorPadrao);
			}

			controladorModeloContrato.validarDadoAtributoModeloContrato(abaAtributo, valorPadrao);
			validarValorFixo(isValorFixo, valorPadrao, abaAtributo.getDescricao());

			atributo.setUltimaAlteracao(Calendar.getInstance().getTime());
			atributo.setValorFixo(isValorFixo);
			atributo.setHabilitado(Boolean.TRUE);
			modeloContrato.getAtributos().add(atributo);

			Collection<AbaAtributo> filhos = abaAtributo.getFilhos();
			if (filhos != null && !filhos.isEmpty()) {
				for (AbaAtributo abaAtributoFilho : filhos) {
					valorPadrao = null;
					atributo = (ModeloAtributo) controladorModeloContrato.criarModeloAtributo();
					atributo.setAbaAtributo(abaAtributoFilho);
					atributo.setHabilitado(Boolean.TRUE);

					if (request.getParameterMap().containsKey(abaAtributoFilho.getNomeColuna())) {
						valorPadrao = String.valueOf(request.getParameter(abaAtributoFilho.getNomeColuna()));
					} else if(request.getAttribute(abaAtributoFilho.getNomeColuna()) != null) {
						valorPadrao = (String) request.getAttribute(abaAtributoFilho.getNomeColuna());
					}

					if (StringUtils.isNotEmpty(valorPadrao)) {
						atributo.setPadrao(valorPadrao);
					}

					if (request.getParameterMap()
							.containsKey(Util.obterNomeCampoValorFixo(abaAtributoFilho.getNomeColuna()))) {

						valorFixo = Boolean.valueOf(
								request.getParameter(Util.obterNomeCampoValorFixo(abaAtributoFilho.getNomeColuna())));

						if (valorFixo == null || !valorFixo) {
							atributo.setValorFixo(false);
						} else {
							validarValorFixo(true, valorPadrao, abaAtributoFilho.getDescricao());
							atributo.setValorFixo(true);
						}

					}

					if (isObrigatorio) {

						if (abaAtributoFilho.getDependenciaObrigatoria() != null
								&& abaAtributoFilho.getDependenciaObrigatoria().equals(Boolean.TRUE)) {
							atributo.setObrigatorio(abaAtributoFilho.getDependenciaObrigatoria());
						} else {
							if (request.getParameterMap()
									.containsKey(Util.obterNomeCampoObrigatorio(abaAtributoFilho.getNomeColuna()))) {
								obrig = Boolean.valueOf(request.getParameter(
										Util.obterNomeCampoObrigatorio(abaAtributoFilho.getNomeColuna())));
							} else {
								obrig = Boolean.FALSE;
							}

							if (obrig == null || !obrig) {
								atributo.setObrigatorio(false);
							} else {
								atributo.setObrigatorio(true);
							}

						}
					}

					controladorModeloContrato.validarDadoAtributoModeloContrato(abaAtributoFilho, valorPadrao);

					atributo.setUltimaAlteracao(Calendar.getInstance().getTime());
					modeloContrato.getAtributos().add(atributo);
				}
			}

		}

		return isSelecionado;
	}

	private void validarValorFixo(boolean isValorFixo, String valorPadrao, String nomeCampo) throws GGASException {

		if (isValorFixo) {
			if (StringUtils.isEmpty(valorPadrao) || NAO_SELECIONADO.equals(valorPadrao)) {
				throw new GGASException("É preciso cadastrar um Valor Padrão para marcar o campo " + nomeCampo + " como Valor Fixo");
			}
		}

	}

	/**
	 * Validar obrigatoriedade.
	 *
	 * @param abaAtributo	-	{@link AbaAtributo}
	 * @param isObrigatorio	-	{@link Boolean}
	 */
	private void validarObrigatoriedade(AbaAtributo abaAtributo, boolean isObrigatorio) {

		if (isObrigatorio && abaAtributo.getAba().getDescricaoAbreviada().equals(Aba.REGRAS_FATURAMENTO)) {
			isObrigatorioVal = Boolean.valueOf(false);
		}
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void carregarCampos(HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute("listaSituacaoContrato", controladorModeloContrato.obterListaSituacaoContratoPadrao());

		List<String> listaGarantiaFinanceiraRenovada = new ArrayList<String>();
		Map<String, Object> filtro = new HashMap<String, Object>();

		listaGarantiaFinanceiraRenovada.add("Renovável");
		listaGarantiaFinanceiraRenovada.add("Renovada");
		
		ConstanteSistema constanteTipoRelatorio = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_MODELO_CONTRATO);
		Long tipoRelatorio = constanteTipoRelatorio != null ? constanteTipoRelatorio.getValorLong() : 0l;
		

		Collection<RelatorioToken> tokens = controladorRelatorio.obterListaRelatorioTokens(tipoRelatorio);
		
		model.addAttribute("listaTokens", tokens);

		model.addAttribute("listaGarantiaFinanceiraRenovada", listaGarantiaFinanceiraRenovada);
		model.addAttribute("listaTipoGarantiaFinanceira",
				controladorEntidadeConteudo.obterListaTipoGarantiaFinanceira());
		model.addAttribute("listaHoraInicialDia", controladorEntidadeConteudo.obterListaHoraInicialDia());
		model.addAttribute("listaRegimeConsumo", controladorEntidadeConteudo.obterListaRegimeConsumo());
		model.addAttribute("listaCriterioUso", controladorEntidadeConteudo.obterListaCriterioUso());
		model.addAttribute("listaTipoFaturamento", controladorEntidadeConteudo.obterListaTipoFaturamento());
		model.addAttribute("listaTarifas", controladorTarifa.listarTarifas());
		model.addAttribute("listaFaseReferencialVencimento",
				controladorEntidadeConteudo.obterListaFaseReferencialVencimento());
		model.addAttribute("listaOpcaoFaseRefVencimento",
				controladorEntidadeConteudo.obterListaOpcaoFaseRefVencimento());
		model.addAttribute("listaFormaPagamento", controladorEntidadeConteudo.obterListaFormaPagamento());

		model.addAttribute("listaUnidadePressao", controladorUnidade.listarUnidadesPressao());
		model.addAttribute("listaUnidadeVazao", controladorUnidade.listarUnidadesVazao());
		model.addAttribute("listaItemFatura", controladorEntidadeConteudo.obterListaItemFatura());
		model.addAttribute("listaEnderecoPadrao", controladorEntidadeConteudo.obterListaEnderecoPadrao());
		model.addAttribute("listaUnidadeVolume", controladorUnidade.listarUnidadesVolume());
		model.addAttribute("listaAcaoAnormalidadeConsumo",
				controladorAnormalidade.listarAcaoAnormalidadeConsumoSemLeitura());
		model.addAttribute("listaFormaCobranca", controladorEntidadeConteudo.listarFormaCobranca());

		filtro.put(HABILITADO, TRUE);
		ServiceLocator.getInstancia().getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);

		model.addAttribute(LISTA_ARRECADADOR_CONVENIO, obterListaArrecadadorConvenioBoletoEArrecadacao());
		model.addAttribute(LISTA_ARRECADADOR_CONVENIO_DEBITO_AUTOMATICO,
				obterListaArrecadadorConvenioDebitoAutomatico());

		model.addAttribute("listaPeriodicidade", controladorRota.listarPeriodicidades());
		model.addAttribute("listaPeriodicidadeTakeOrPay", controladorEntidadeConteudo.obterListaPeriodicidadeTOP());
		model.addAttribute("listaPeriodicidadeRetMaiorMenor",
				controladorEntidadeConteudo.obterListaPeriodicidadeRetiradaMaiorMenor());
		model.addAttribute("listaPenalidadeRetiradaMaiorMenor",
				controladorApuracaoPenalidade.listarPenalidadesRetMaiorMenor());
		model.addAttribute("listaConsumoReferencial", controladorEntidadeConteudo.obterListaConsumoReferencial());
		model.addAttribute("listaFaixaPenalidadeSobreDem", controladorContrato.obterListaPenalidadeDemandaSobre());
		model.addAttribute("listaFaixaPenalidadeSobDem", controladorContrato.obterListaPenalidadeDemandaSob());
		model.addAttribute("listaContratoModalidade", controladorContrato.listarContratoModalidade());
		model.addAttribute("listaIndiceMonetario", controladorTarifa.listaIndiceFinanceiro());
		model.addAttribute("listaTipoResponsabilidade", controladorEntidadeConteudo.obterListaResponsabilidades());
		model.addAttribute("listaAmortizacoes", controladorEntidadeConteudo.listarAmortizacoes());
		model.addAttribute("listaFaixasPressaoFornecimento",
				controladorFaixaPressaoFornecimento.listarTodasFaixasPressaoFornecimento());
		model.addAttribute("listaDataReferenciaCambial", controladorEntidadeConteudo.obterListaDataReferenciaCambial());
		model.addAttribute("listaDiaCotacao", controladorEntidadeConteudo.obterListaDiaCotacao());
		model.addAttribute("listaTipoAgrupamento", controladorEntidadeConteudo.obterListaTipoAgrupamento());
		model.addAttribute("listaContratoCompra", controladorEntidadeConteudo.obterListaContratoCompra());
		model.addAttribute("listaTipoMultaRecisoria", controladorEntidadeConteudo.obterListaTipoMultaRecisoria());
		model.addAttribute("listaBaseApuracao", controladorEntidadeConteudo.listarBasesApuracaoPenalidadeMaiorMenor());
		model.addAttribute("itemFaturaMargemDistribuicao",
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));
		model.addAttribute("listaTipoModelo", controladorEntidadeConteudo.obterListaTipoModelo());
		model.addAttribute("listaReferenciaQFParadaProgramada",
				controladorEntidadeConteudo.obterListaReferenciaQFParadaProgramada());
		model.addAttribute("listaTipoApuracao",
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.TIPO_APURACAO));
		model.addAttribute("listaPrecoCobranca", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PRECO_COBRANCA));
		model.addAttribute("listaApuracaoParadaProgramada", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_PROGRAMADA));
		model.addAttribute("listaApuracaoParadaNaoProgramada", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_NAO_PROGRAMADA));
		model.addAttribute("listaApuracaoCasoFortuito", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_CASO_FORTUITO));
		model.addAttribute("listaApuracaoFalhaFornecimento", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.PARADA_FALHA_FORNECIMENTO));
		model.addAttribute("listaTipoAgrupamentoContrato", controladorEntidadeConteudo
				.listarEntidadeConteudoPorDescricaoEntidadeClasse(Constantes.TIPO_AGRUPAMENTO));

		model.addAttribute("listaBancos", controladorBanco.listarBanco());

		if (model.asMap().get(LISTA_LOCAL_AMOSTRAGEM_PCS) == null) {
			model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, controladorEntidadeConteudo.listarLocalAmostragemPCS());
		}
		if (model.asMap().get(LISTA_INTERVALO_PCS) == null) {
			model.addAttribute(LISTA_INTERVALO_PCS, controladorHistoricoConsumo.listarIntervaloPCS());
		}
		// Campos com dependências
		Collection<AbaAtributo> abaAtributos = controladorModeloContrato.listarAbasAtributos();
		if (abaAtributos != null && !abaAtributos.isEmpty()) {
			for (AbaAtributo abaAtributo : abaAtributos) {
				if (abaAtributo.getDependenciaObrigatoria() != null) {
					request.setAttribute(Util.obterNomeCampoDependente(abaAtributo.getNomeColuna()),
							abaAtributo.getDependenciaObrigatoria());
				}
			}
		}
	}

	/**
	 * Popular form.
	 * 
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param request			-	{@link HttpServletRequest}
	 * @param modeloContrato	-	{@link ModeloContratoVO}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void popularForm(ModeloContratoVO modeloContratoVO, HttpServletRequest request,
			ModeloContrato modeloContrato, Model model) throws GGASException {

		boolean isAlterar = Boolean.valueOf(request.getParameter("isAlterar"));

		if (!super.isPostBack(request)) {

			modeloContratoVO.setChavePrimaria(modeloContrato.getChavePrimaria());
			modeloContratoVO.setVersao(modeloContrato.getVersao());
			modeloContratoVO.setDescricao(modeloContrato.getDescricao());

			if (modeloContrato.getLayoutRelatorio() != null) {
				modeloContratoVO.setLayoutRelatorio(modeloContrato.getLayoutRelatorio().getLayout());
			}

			modeloContratoVO.setDescricaoAbreviada(modeloContrato.getDescricaoAbreviada());
			modeloContratoVO.setHabilitado(modeloContrato.isHabilitado());

			if (modeloContrato.isHabilitado()) {
				modeloContratoVO.setIndicadorUsoTelaDetalhamento("Ativo");
			} else {
				modeloContratoVO.setIndicadorUsoTelaDetalhamento("Inativo");
			}

			if (modeloContrato.getTipoModelo() != null) {
				modeloContratoVO.setTipoModelo(String.valueOf(modeloContrato.getTipoModelo().getChavePrimaria()));
			}

			Collection<ModeloAtributo> atributos = modeloContrato.getAtributos();
			StringBuffer texto = new StringBuffer();
			if (atributos != null && !atributos.isEmpty()) {

				String campo = null;
				String campoSelecao = null;
				String campoObrigatorio = null;
				String campoValorFixo = null;

				for (ModeloAtributo modeloAtributo : atributos) {
					campo = modeloAtributo.getAbaAtributo().getNomeColuna();
					texto = texto.append("\nchave = ").append(campo).append(" valor = ").append(modeloAtributo.getPadrao());
					if (StringUtils.isNotEmpty(campo)) {

						campoSelecao = Util.obterNomeCampoSelecao(campo);
						campoObrigatorio = Util.obterNomeCampoObrigatorio(campo);
						campoValorFixo = Util.obterNomeCampoValorFixo(campo);

						
						if (isAlterar) {
							popularVO(modeloContratoVO, campoSelecao, campoObrigatorio, campoValorFixo, campo,
									modeloAtributo);
						} else {
							prepararExibicaoCamposDetalhar(campoSelecao, campoObrigatorio,
									campoValorFixo, model, campo, modeloAtributo);
						}

						if (LOCAL_AMOSTRAGEM_PCS.equals(campo)) {
							String valor = modeloAtributo.getPadrao();
							if (valor != null && !valor.isEmpty()) {
								String[] chavesLocal = valor.split(Constantes.STRING_VIRGULA);
								modeloContratoVO
										.setIdsLocalAmostragemAssociados(Util.arrayStringParaArrayLong(chavesLocal));
								this.tratarCamposSelectAbaConsumo(modeloContratoVO, model);
							}
						}

						if (INTERVALO_RECUPERACAO_PCS.equals(campo)) {
							String valor = modeloAtributo.getPadrao();
							if (valor != null && !valor.isEmpty()) {
								String[] chavesIntervalo = valor.split(Constantes.STRING_VIRGULA);
								modeloContratoVO.setIdsIntervaloAmostragemAssociados(
										Util.arrayStringParaArrayLong(chavesIntervalo));
								this.tratarCamposSelectAbaConsumo(modeloContratoVO, model);
							}
						}

					}
				}
			}

			Collection<AbaModelo> abas = modeloContrato.getAbas();
			
			verificarAbas(modeloContratoVO, abas);

		}
	}

	/**
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param abas				-	{@link Collection}
	 */
	private void verificarAbas(ModeloContratoVO modeloContratoVO, Collection<AbaModelo> abas) {
		
		if (abas != null && !abas.isEmpty()) {
			for (AbaModelo abaModelo : abas) {
				if (Aba.DADOS_TECNICOS.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					modeloContratoVO.setSelAbaTecnicos(true);
				}
				if (Aba.CONSUMO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					modeloContratoVO.setSelAbaConsumo(true);
				}
				if (Aba.FATURAMENTO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					modeloContratoVO.setSelAbaFaturamento(true);
				}
				if (Aba.REGRAS_FATURAMENTO.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					modeloContratoVO.setSelAbaRegrasFaturamento(true);
				}
				if (Aba.RESPONSABILIDADES.equals(abaModelo.getAba().getDescricaoAbreviada())) {
					modeloContratoVO.setSelAbaResponsabilidade(true);
				}
			}
		}
		
	}

	/**
	 * @param campoSelecao		-	{@link String}
	 * @param campoObrigatorio	-	{@link String}
	 * @param campoValorFixo	-	{@link String}
	 * @param model				-	{@link Model}
	 * @param campo				-	{@link String}
	 * @param modeloAtributo	-	{@link ModeloAtributo}
	 */
	private void prepararExibicaoCamposDetalhar(String campoSelecao,
			String campoObrigatorio, String campoValorFixo, Model model, String campo, ModeloAtributo modeloAtributo) {

		if (!model.asMap().containsKey(campoSelecao)) {
			model.addAttribute(campoSelecao, Boolean.TRUE);
		}

		if (!model.asMap().containsKey(campoObrigatorio)) {
			model.addAttribute(campoObrigatorio, modeloAtributo.isObrigatorio());
		}

		if (!model.asMap().containsKey(campoValorFixo)) {
			model.addAttribute(campoValorFixo, modeloAtributo.isValorFixo());
		}

		if (!model.asMap().containsKey(campo)) {
			model.addAttribute(campo, modeloAtributo.getPadrao());
		}

	}

	/**
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param campoSelecao		-	{@link String}
	 * @param campoObrigatorio	-	{@link String}
	 * @param campoValorFixo	-	{@link String}
	 * @param campo				-	{@link String}
	 * @param modeloAtributo	-	{@link ModeloAtributo}
	 */
	private void popularVO(ModeloContratoVO modeloContratoVO, String campoSelecao, String campoObrigatorio,
			String campoValorFixo, String campo, ModeloAtributo modeloAtributo) {

			ModeloContratoVO.setField(campoSelecao, modeloContratoVO, Boolean.TRUE);
			ModeloContratoVO.setField(campoObrigatorio, modeloContratoVO, modeloAtributo.isObrigatorio());
			ModeloContratoVO.setField(campoValorFixo, modeloContratoVO, modeloAtributo.isValorFixo());
			ModeloContratoVO.setField(campo, modeloContratoVO, modeloAtributo.getPadrao());
	}

	/**
	 * Tratar campos select aba consumo.
	 * 
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void tratarCamposSelectAbaConsumo(ModeloContratoVO modeloContratoVO, Model model) throws GGASException {

		Long[] idsLocalAmostragemAssociados = modeloContratoVO.getIdsLocalAmostragemAssociados();
		Long[] idsIntervaloAmostragemAssociados = modeloContratoVO.getIdsIntervaloAmostragemAssociados();

		Collection<EntidadeConteudo> listaLocalAmostragemAssociados = new ArrayList<EntidadeConteudo>();
		if (idsLocalAmostragemAssociados != null && idsLocalAmostragemAssociados.length > 0) {
			for (Long chaveLocal : idsLocalAmostragemAssociados) {
				EntidadeConteudo localAmostragem = controladorEntidadeConteudo.obterEntidadeConteudo(chaveLocal);
				listaLocalAmostragemAssociados.add(localAmostragem);
			}

			Collection<EntidadeConteudo> listaLocalAmostragemDisp = controladorEntidadeConteudo
					.listarLocalAmostragemPCS();
			listaLocalAmostragemDisp.removeAll(listaLocalAmostragemAssociados);
			model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_PCS, listaLocalAmostragemDisp);
			model.addAttribute(LISTA_LOCAL_AMOSTRAGEM_ASSOCIADOS, listaLocalAmostragemAssociados);
		}

		Collection<IntervaloPCS> listaIntervaloAmostragemAssociados = new ArrayList<IntervaloPCS>();
		if (idsIntervaloAmostragemAssociados != null && idsIntervaloAmostragemAssociados.length > 0) {
			for (Long chaveIntervalo : idsIntervaloAmostragemAssociados) {
				IntervaloPCS intervaloPCS = controladorHistoricoConsumo.obterIntervaloPCS(chaveIntervalo);
				listaIntervaloAmostragemAssociados.add(intervaloPCS);
			}

			Collection<IntervaloPCS> listaIntervaloPCSDisp = controladorHistoricoConsumo.listarIntervaloPCS();
			listaIntervaloPCSDisp.removeAll(listaIntervaloAmostragemAssociados);
			model.addAttribute(LISTA_INTERVALO_PCS, listaIntervaloPCSDisp);
			model.addAttribute(LISTA_INTERVALO_AMOSTRAGEM_ASSOCIADOS, listaIntervaloAmostragemAssociados);
		}

	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @return filtro			-	{@link Map}
	 */
	private Map<String, Object> prepararFiltro(ModeloContratoVO modeloContratoVO) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		String descricao = modeloContratoVO.getDescricao();
		Boolean habilitado = modeloContratoVO.getHabilitado();
		String tipoModelo = modeloContratoVO.getTipoModelo();

		if (StringUtils.isNotEmpty(descricao)) {
			filtro.put(DESCRICAO, descricao);
		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
		if (tipoModelo != null && !tipoModelo.isEmpty() && Long.parseLong(tipoModelo) > 0) {
			filtro.put(TIPO_MODELO, Long.parseLong(tipoModelo));
		}

		return filtro;
	}
	
	/**
	 * @return listaArrecadadorConvenioBoleto	-	{@link Collection}
	 * @throws NegocioException					-	{@link NegocioException}
	 */
	private Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioBoletoEArrecadacao()
			throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, TRUE);

		Collection<ArrecadadorContratoConvenio> listaArrecadadorConvenioBoleto = ServiceLocator.getInstancia()
				.getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);

		listaArrecadadorConvenioBoleto.removeAll(this.obterListaArrecadadorConvenioDebitoAutomatico());

		return listaArrecadadorConvenioBoleto;
	}
	
	
	/**
	 * @return Uma lista arrecadador convenio debito Automatico	-	{@link Collection}
	 * @throws NegocioException									-	{@link NegocioException}
	 */
	private Collection<ArrecadadorContratoConvenio> obterListaArrecadadorConvenioDebitoAutomatico()
			throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, TRUE);
		filtro.put("tipoConvenio", Long.valueOf(ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO)));

		return ServiceLocator.getInstancia().getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtro);
	}
	
	/**
	 * Exibir migracao modelo contrato fluxo modelo.
	 *
	 * @param modeloContratoVO		-	{@link ModeloContratoVO}
	 * @param result				-	{@link BindingResult}
	 * @param request				-	{@link HttpServletRequest}
	 * @param model					-	{@link Model}
	 * @return exibirMigracaoModelo	-	{@link String}
	 * @throws GGASException		-	{@link GGASException}
	 */
	@RequestMapping("exibirMigracaoModelo")
	public String exibirMigracaoModelo(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirMigracaoModelo";
		Long[] chavesModeloContrato = {};
		Long[] chavesPrimarias = modeloContratoVO.getChavesPrimarias();
		Long[] chavesSessao = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);

		if ((chavesPrimarias == null || chavesPrimarias.length == 0) && chavesSessao != null) {
			chavesPrimarias = chavesSessao;
		}

		try {

			if (chavesPrimarias != null && chavesPrimarias.length != 0) {
				List<Long> chavesPrimariasHereditariedade = new ArrayList<Long>();
				for (int i = 0; i < chavesPrimarias.length; i++) {
					Long chavePrimaria = chavesPrimarias[i];
					chavesPrimariasHereditariedade.add(chavePrimaria);
					ModeloContrato modeloContrato = (ModeloContrato) controladorModeloContrato.obter(chavePrimaria);
					while (modeloContrato.getModeloContratoVersaoAnterior() != null) {
						modeloContrato = modeloContrato.getModeloContratoVersaoAnterior();
						chavesPrimariasHereditariedade.add(modeloContrato.getChavePrimaria());
					}
				}
				chavesModeloContrato = chavesPrimariasHereditariedade
						.toArray(new Long[chavesPrimariasHereditariedade.size()]);
				request.getSession().setAttribute(CHAVES_PRIMARIAS, chavesModeloContrato);

			}

			Collection<Contrato> listaContrato = listarContratos(chavesModeloContrato);

			listaContrato = filtrarContratoPorDatas(listaContrato);
			Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = agruparContratosProCodigoDescricao(
					listaContrato);

			model.addAttribute(MAPA_CONTRATO_MIGRACAO, mapaContratoMigracao);
			request.getSession().setAttribute(MAPA_CONTRATO_MIGRACAO, mapaContratoMigracao); // NOSONAR {mapa necessário
																								// para funcionamento da
																								// tela}
			model.addAttribute(LISTA_CONTRATO_MIGRACAO, listaContrato);

			carregarDados(request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);
		}

		model.addAttribute(MODELO_CONTRATO_VO, modeloContratoVO);

		return view;

	}
	
	/**
	 * Exibir migracao modelo contrato campos alterados.
	 *
	 * @param modeloContratoVO					-	{@link ModeloContratoVO}
	 * @param result							-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirMigracaoCamposAlterados	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("exibirMigracaoCamposAlterados")
	public String exibirMigracaoCamposAlterados(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Long chaveModeloContrato = null;
		if(modeloContratoVO.getIdModeloContrato() != null) {
			chaveModeloContrato = modeloContratoVO.getIdModeloContrato();
		}

		try {

			if (chaveModeloContrato == null || chaveModeloContrato < 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ModeloContrato.MODELO_CONTRATO);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		if(chaveModeloContrato != null) {
			alterarDadosMigracaoContrato(chaveModeloContrato, request, model);
		}

		model.addAttribute(MODELO_CONTRATO_VO, modeloContratoVO);

		return "exibirMigracaoCamposAlterados";
	}
	
	/**
	 * Salvar migracao modelo contrato.
	 *
	 * @param modeloContratoVO			-	{@link ModeloContratoVO}
	 * @param result					-	{@link BindingResult}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @return pesquisarModeloContrato	-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("salvarMigracaoModelo")
	public String salvarMigracaoModelo(ModeloContratoVO modeloContratoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		Long chaveModeloContrato = modeloContratoVO.getIdModeloContrato();

		try {

			ModeloContrato novoModeloContrato = (ModeloContrato) controladorModeloContrato.obter(chaveModeloContrato,
					ATRIBUTOS, ABAS);

			Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = obterMapaContratoMigracao(request);
			Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados = (Map<String, Collection<ModeloAtributo>>) request
					.getSession().getAttribute(MAPA_ATRIBUTOS_ADICIONADOS);
			Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos = (Map<String, Collection<ModeloAtributo>>) request
					.getSession().getAttribute(MAPA_ATRIBUTOS_REMOVIDOS);
			Map<String, Collection<ModeloAtributo>> mapaAtributosComuns = (Map<String, Collection<ModeloAtributo>>) request
					.getSession().getAttribute(MAPA_ATRIBUTOS_COMUNS);

			String logErroMigracao = controladorContrato.salvarMigracaoModeloContrato(mapaContratoMigracao,
					mapaAtributosAdicionados, mapaAtributosRemovidos, mapaAtributosComuns, novoModeloContrato);

			request.getSession().setAttribute(LOG_ERRO_MIGRACAO, logErroMigracao.getBytes());

			if (logErroMigracao.contains(LABEL_COM_ERRO)) {
				mensagemErroParametrizado(model, Constantes.ALERTA_ENTIDADE_MIGRACAO_CONTRATO, request,
						Contrato.CONTRATOS);
			} else {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_MIGRACAO_CONTRATO, request, Contrato.CONTRATOS);
			}

			view = pesquisarModeloContrato(modeloContratoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirMigracaoCamposAlterados(modeloContratoVO, result, request, model);
		}

		return view;

	}
	
	/**
	 * Exibir log erro migracao.
	 *
	 * @param request		-	{@link HttpServletRequest}
	 * @param response		-	{@link HttpServletResponse}
	 * @return null 
	 * @throws IOException	-	{@link IOException}
	 */
	@RequestMapping("exibirLogErroMigracaoModeloContrato")
	public String exibirLogErroMigracaoModeloContrato(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		byte[] logErroMigracao = (byte[]) request.getSession().getAttribute(LOG_ERRO_MIGRACAO);

		if (logErroMigracao != null) {
			ServletOutputStream servletOutputStream = response.getOutputStream();

			response.setContentType(EMAIL_APPLICATION_DOWNLOAD);
			response.setContentLength(logErroMigracao.length);
			response.setHeader(CONTENT_DISPOSITION, EMAIL_ATTACHMENT_FILENAME + EMAIL_MIGRACAO_MODELO_DO_CONTRATO_LOG);

			servletOutputStream.write(logErroMigracao, 0, logErroMigracao.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		request.getSession().removeAttribute(LOG_ERRO_MIGRACAO);

		return null;
	}
	
	/**
	 * Alterar dados migracao contrato.
	 *
	 * @param chaveModeloContrato	-	{@link Long}
	 * @param request				-	{@link HttpServletRequest}
	 * @param model					-	{@link Model}
	 * @throws GGASException		-	{@link GGASException}
	 */
	private void alterarDadosMigracaoContrato(Long chaveModeloContrato, HttpServletRequest request, Model model)
			throws GGASException {

		ModeloContrato modeloNovo = (ModeloContrato) controladorModeloContrato.obter(chaveModeloContrato, ATRIBUTOS,
				ABAS);
		model.addAttribute(MODELO_NOVO, modeloNovo);
		Collection<ModeloAtributo> atributosModeloNovo = modeloNovo.getAtributos();

		Map<String, Collection<ModeloAtributoVO>> mapaAtributosAdicionadosRemovidos = new HashMap<String, Collection<ModeloAtributoVO>>();

		Map<String, ModeloAtributo> mapaAtributosModeloNovo = new HashMap<String, ModeloAtributo>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, Collection<ModeloAtributo>> mapaAtributosComuns = new HashMap<String, Collection<ModeloAtributo>>();
		Map<String, ModeloAtributo> mapaAtributosModeloAntigo = null;

		for (ModeloAtributo modeloAtributo : atributosModeloNovo) {

			AbaAtributo abaAtributo = modeloAtributo.getAbaAtributo();
			String descricaoAtributo = abaAtributo.getNomeColuna();

			mapaAtributosModeloNovo.put(descricaoAtributo, modeloAtributo);
		}

		Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao = obterMapaContratoMigracao(request);

		for (Entry<Pair<Long, String>, Collection<Contrato>> entry : mapaContratoMigracao.entrySet()) {

			Long chavePrimariaModeloAntigo = entry.getKey().getFirst();

			if (modeloNovo.getChavePrimaria() != chavePrimariaModeloAntigo) {
				String descricaoModelo = entry.getKey().getSecond();

				ModeloContrato modeloAntigo = (ModeloContrato) controladorModeloContrato
						.obter(chavePrimariaModeloAntigo, ATRIBUTOS, ABAS);
				Collection<ModeloAtributo> atributosModeloAntigo = modeloAntigo.getAtributos();

				mapaAtributosModeloAntigo = new HashMap<String, ModeloAtributo>();
				for (ModeloAtributo modeloAtributoAntigo : atributosModeloAntigo) {
					AbaAtributo atributoAntigo = modeloAtributoAntigo.getAbaAtributo();
					String descricaoAtributoAntigo = atributoAntigo.getNomeColuna();

					mapaAtributosModeloAntigo.put(descricaoAtributoAntigo, modeloAtributoAntigo);
				}

				// RETORNA OS ATRIBUTOS QUE EXISTEM NO MODELO ANTIGO E NÃO EXISTEM NO MODELO
				// NOVO
				Collection<ModeloAtributo> listaAtributosRemovidos = carregarAtributosAlteradosMigracao(
						mapaAtributosModeloAntigo, mapaAtributosModeloNovo);
				mapaAtributosRemovidos.put(modeloNovo.getDescricao(true), listaAtributosRemovidos);

				// RETORNA OS ATRIBUTOS QUE EXISTEM NO MODELO NOVO E NÃO EXISTEM NO MODELO
				// ANTIGO
				Collection<ModeloAtributo> listaAtributosAdicionados = carregarAtributosAlteradosMigracao(
						mapaAtributosModeloNovo, mapaAtributosModeloAntigo);
				mapaAtributosAdicionados.put(modeloNovo.getDescricao(true), listaAtributosAdicionados);

				// MANTEM APENAS OS ATRIBUTOS COMUNS AOS DOIS MODELOS
				Collection<ModeloAtributo> listaAtributosComuns = mapaAtributosModeloNovo.values();
				listaAtributosComuns.removeAll(listaAtributosAdicionados);
				mapaAtributosComuns.put(modeloNovo.getDescricao(true), listaAtributosComuns);

				Collection<ModeloAtributoVO> listaAtributosAgrupados = agruparAtributosModeloContrato(
						listaAtributosAdicionados, listaAtributosRemovidos);
				mapaAtributosAdicionadosRemovidos.put(descricaoModelo, listaAtributosAgrupados);
			}

		}

		model.addAttribute(MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS, mapaAtributosAdicionadosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_ADICIONADOS_REMOVIDOS, mapaAtributosAdicionadosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_ADICIONADOS, mapaAtributosAdicionados);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_REMOVIDOS, mapaAtributosRemovidos);
		request.getSession().setAttribute(MAPA_ATRIBUTOS_COMUNS, mapaAtributosComuns);
	}
	
	/**
	 * Obter mapa contrato migracao.
	 *
	 * @param request					-	{@link HttpServletRequest}
	 * @return um mapa ContratoMigracao	-	{@link Map}
	 */
	@SuppressWarnings("unchecked")
	private Map<Pair<Long, String>, Collection<Contrato>> obterMapaContratoMigracao(HttpServletRequest request) {
		return (HashMap<Pair<Long, String>, Collection<Contrato>>) request.getSession()
				.getAttribute(MAPA_CONTRATO_MIGRACAO);

	}
	
	/**
	 * Listar contratos.
	 *
	 * @param chavesModeloContrato				-	{@link Long}
	 * @return uma lista de acordo com o filtro	-	{@link Collection}
	 * @throws GGASException					-	{@link GGASException}
	 */
	private Collection<Contrato> listarContratos(Long[] chavesModeloContrato) throws GGASException {
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVES_MODELO_CONTRATO, chavesModeloContrato);
		filtro.put(FLUXO_MIGRACAO, FLUXO_MODELO);
		filtro.put(HABILITADO, Boolean.TRUE);
		return controladorContrato.consultarContratos(filtro);
	}
	
	/**
	 * @param listaContrato			-	{@link Collection}
	 * @return listaContratoAtual	-	{@link Collection}
	 */
	private Collection<Contrato> filtrarContratoPorDatas(Collection<Contrato> listaContrato) {

		Collection<Contrato> listaContratoAtual = new ArrayList<>();

		for (Contrato contrato : listaContrato) {

			if (contrato.getDataRecisao() == null
					&& contrato.getDataAssinatura().compareTo(DataUtil.gerarDataHoje()) <= 0) {
				listaContratoAtual.add(contrato);
			}
		}

		return listaContratoAtual;
	}
	
	/**
	 * Agrupar contratos pro codigo descricao.
	 *
	 * @param listaContrato	-	{@link Collection}
	 * @return mapa			-	{@link Map}
	 */
	private Map<Pair<Long, String>, Collection<Contrato>> agruparContratosProCodigoDescricao(
			Collection<Contrato> listaContrato) {
		
		return Util.agrupar(listaContrato, new Agrupador<Contrato, Pair<Long, String>>() {
			
			@Override
			public Pair<Long, String> getChaveAgrupadora(Contrato elemento) {
				ModeloContrato modeloContrato = elemento.getModeloContrato();
				Long first = modeloContrato.getChavePrimaria();
				String second = modeloContrato.getDescricao(true);
				return new Pair<Long, String>(first, second);
			}
			
		});
	}
	
	/**
	 * Carregar dados.
	 *
	 * @param request			-	{@link HttpServletRequest}			
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void carregarDados(HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		model.addAttribute(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());

		filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_MODELOS_CONTRATO, controladorModeloContrato.consultarModelosContrato(filtro));

		request.getSession().setAttribute(LISTA_MODELOS_CONTRATO,
				controladorModeloContrato.consultarModelosContrato(filtro));
		model.addAttribute(LISTA_TIPO_CONTRATO, controladorEntidadeConteudo.obterListaTipoModelo());

	}
	
	/**
	 * Carregar atributos alterados migracao.
	 *
	 * @param mapaA				-	{@link Map}
	 * @param mapaB				-	{@link Map}
	 * @return listaAtributos	-	{@link Collection}
	 */
	private Collection<ModeloAtributo> carregarAtributosAlteradosMigracao(Map<String, ModeloAtributo> mapaA,
			Map<String, ModeloAtributo> mapaB) {

		Collection<ModeloAtributo> listaAtributos = new ArrayList<ModeloAtributo>();

		for (Entry<String, ModeloAtributo> entry : mapaA.entrySet()) {

			if (!mapaB.containsKey(entry.getKey())) {
				ModeloAtributo modeloAtributo = mapaA.get(entry.getKey());
				listaAtributos.add(modeloAtributo);
			}
		}

		return listaAtributos;
	}
	
	/**
	 * Agrupar atributos modelo contrato.
	 *
	 * @param listaAtributosAdicionados		-	{@link Collection}
	 * @param listaAtributosRemovidos		-	{@link Collection}
	 * @return listaAdicionadosRemovidos	-	{@link Collection}
	 */
	private Collection<ModeloAtributoVO> agruparAtributosModeloContrato(
			Collection<ModeloAtributo> listaAtributosAdicionados, Collection<ModeloAtributo> listaAtributosRemovidos) {

		Collection<ModeloAtributoVO> listaAdicionadosRemovidos = new ArrayList<ModeloAtributoVO>();

		int quantidadeRemovidos = listaAtributosRemovidos.size();
		int quantidadeAdicionados = listaAtributosAdicionados.size();
		int quantidadeMaior = 0;

		if (quantidadeRemovidos >= quantidadeAdicionados) {
			quantidadeMaior = quantidadeRemovidos;
		} else {
			quantidadeMaior = quantidadeAdicionados;
		}

		for (int i = 0; i < quantidadeMaior; i++) {
			ModeloAtributoVO modeloAtributoVO = new ModeloAtributoVO();
			ModeloAtributo modeloAtributo = new ModeloAtributoImpl();
			if (i < quantidadeAdicionados) {
				ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosAdicionados).get(i);
				AbaAtributo atributoLista = modeloAtributoLista.getAbaAtributo();
				modeloAtributoVO.setObrigatorio(modeloAtributo.isObrigatorio());
				modeloAtributoVO.setPadrao(modeloAtributo.getPadrao());
				modeloAtributoVO.setCampoAdicionado(atributoLista.getDescricao());
				modeloAtributoVO.setNomeColunaAdicionado(atributoLista.getNomeColuna());
			}

			if (quantidadeAdicionados >= quantidadeRemovidos) {
				if (i < quantidadeRemovidos) {
					ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosRemovidos).get(i);
					AbaAtributo removido = modeloAtributoLista.getAbaAtributo();
					modeloAtributoVO.setObrigatorio(modeloAtributo.isObrigatorio());
					modeloAtributoVO.setPadrao(modeloAtributo.getPadrao());
					modeloAtributoVO.setCampoRemovido(removido.getDescricao());
					modeloAtributoVO.setNomeColunaRemovido(removido.getNomeColuna());
				}
			} else {
				ModeloAtributo modeloAtributoLista = ((List<ModeloAtributo>) listaAtributosRemovidos).get(i);
				AbaAtributo removido = modeloAtributoLista.getAbaAtributo();
				modeloAtributoVO.setCampoRemovido(removido.getDescricao());
				modeloAtributoVO.setNomeColunaRemovido(removido.getNomeColuna());
			}

			listaAdicionadosRemovidos.add(modeloAtributoVO);
		}

		return listaAdicionadosRemovidos;
	}
	
}
