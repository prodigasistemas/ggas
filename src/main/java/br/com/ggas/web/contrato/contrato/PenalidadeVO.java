/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Penalidades.
 * 
 * @author bruno silva
 * 
 * @see PenalidadeAction
 * 
 */
public class PenalidadeVO {
	
	private Boolean cobrada;

	private String numeroContrato;
	private String dataApuracaoInicio;
	private String dataApuracaoFim;
	private String indicadorPesquisa;
	private String tipoPesquisa;
	private String desconto;
	private String descontoValor;
	private String observacoes;
	private String periodoApuracao;
	private String[] desc;
	private String[] descValor;
	private String[] obs;
	private String[] vlFinal;
	private String[] descMetro;
	
	private Long idCliente;
	private Long idImovel;
	private Long idContrato;
	private Long idPontoConsumo;
	private Long idPenalidade;
	private Long idModalidade;
	private Long idApuracaoQtdaPenalidadePeriodicidade;
	
	
	private Long[] idPenalidadesAssociadas;
	private Long[] idPenalidadesDisponiveis;
	private Long[] idsApuracaoQtdaPenalidadePeriodicidade;
	
	
	/**
	 * @return cobrada {@link Boolean}
	 */
	public Boolean getCobrada() {
		return cobrada;
	}
	/**
	 * @param cobrada {@link Boolean}
	 */
	public void setCobrada(Boolean cobrada) {
		this.cobrada = cobrada;
	}
	
	/**
	 * @return numeroContrato {@link Sting}
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}
	/**
	 * @param numeroContrato {@link Sting}
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	/**
	 * @return dataApuracaoInicio {@link Sting}
	 */
	public String getDataApuracaoInicio() {
		return dataApuracaoInicio;
	}
	/**
	 * @param dataApuracaoInicio {@link Sting}
	 */
	public void setDataApuracaoInicio(String dataApuracaoInicio) {
		this.dataApuracaoInicio = dataApuracaoInicio;
	}
	/**
	 * @return dataApuracaoFim {@link Sting}
	 */
	public String getDataApuracaoFim() {
		return dataApuracaoFim;
	}
	/**
	 * @param dataApuracaoFim {@link Sting}
	 */
	public void setDataApuracaoFim(String dataApuracaoFim) {
		this.dataApuracaoFim = dataApuracaoFim;
	}
	/**
	 * @return indicadorPesquisa {@link Sting}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa {@link Sting}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	/**
	 * @return tipoPesquisa {@link Sting}
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	/**
	 * @param tipoPesquisa {@link Sting}
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	/**
	 * @return desconto {@link Sting}
	 */
	public String getDesconto() {
		return desconto;
	}
	/**
	 * @param desconto {@link Sting}
	 */
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	/**
	 * @return descontoValor {@link Sting}
	 */
	public String getDescontoValor() {
		return descontoValor;
	}
	/**
	 * @param descontoValor {@link Sting}
	 */
	public void setDescontoValor(String descontoValor) {
		this.descontoValor = descontoValor;
	}
	/**
	 * @return observacoes {@link Sting}
	 */
	public String getObservacoes() {
		return observacoes;
	}
	/**
	 * @param observacoes {@link Sting}
	 */
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	/**
	 * @return periodoApuracao {@link Sting}
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}
	/**
	 * @param periodoApuracao {@link Sting}
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}
	/**
	 * @return desc {@link String}
	 */
	public String[] getDesc() {
		String[] descTmp = null;
		if (desc != null) {
			descTmp = desc.clone();
		}
		return descTmp;
	}
	/**
	 * @return desc {@link String}
	 */
	public void setDesc(String[] desc) {
		if(desc != null) {
			this.desc = desc.clone();
		}else {
			this.desc = null;
		}
	}
	/**
	 * @return descValor {@link String}
	 */
	public String[] getDescValor() {
		String[] descValorTmp = null;
		if (descValor != null) {
			descValorTmp = descValor.clone();
		}
		return descValorTmp;
	}
	/**
	 * @return descValor {@link String}
	 */
	public void setDescValor(String[] descValor) {
		if(descValor != null) {
			this.descValor = descValor.clone();
		}else {
			this.descValor = null;
		}
	}
	/**
	 * @return obs {@link String}
	 */
	public String[] getObs() {
		String[] obsTmp = null;
		if (obs != null) {
			obsTmp = obs.clone();
		}
		return obsTmp;
	}
	/**
	 * @return obs {@link String}
	 */
	public void setObs(String[] obs) {
		if(obs != null) {
			this.obs = obs.clone();
		}else {
			this.obs = null;
		}
	}
	/**
	 * @return vlFinal {@link String}
	 */
	public String[] getVlFinal() {
		String[] vlFinalTmp = null;
		if (vlFinal != null) {
			vlFinalTmp = vlFinal.clone();
		}
		return vlFinalTmp;
	}
	/**
	 * @return vlFinal {@link String}
	 */
	public void setVlFinal(String[] vlFinal) {
		if(vlFinal != null) {
			this.vlFinal = vlFinal.clone();
		}else {
			this.vlFinal = null;
		}
	}
	/**
	 * @return descMetro {@link String}
	 */
	public String[] getDescMetro() {
		String[] descMetroTmp = null;
		if (descMetro != null) {
			descMetroTmp = descMetro.clone();
		}
		return descMetroTmp;
	}
	/**
	 * @return descMetro {@link String}
	 */
	public void setDescMetro(String[] descMetro) {
		if(descMetro != null) {
			this.descMetro = descMetro.clone();
		}else {
			this.descMetro = null;
		}
	}
	
	
	/**
	 * @return idCliente {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return idImovel {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idContrato {@link Long}
	 */
	public Long getIdContrato() {
		return idContrato;
	}
	/**
	 * @param idContrato {@link Long}
	 */
	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}
	/**
	 * @return idPontoConsumo {@link Long}
	 */
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	/**
	 * @param idPontoConsumo {@link Long}
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	/**
	 * @return idPenalidade {@link Long}
	 */
	public Long getIdPenalidade() {
		return idPenalidade;
	}
	/**
	 * @param idPenalidade {@link Long}
	 */
	public void setIdPenalidade(Long idPenalidade) {
		this.idPenalidade = idPenalidade;
	}
	/**
	 * @return idModalidade {@link Long}
	 */
	public Long getIdModalidade() {
		return idModalidade;
	}
	/**
	 * @param idModalidade {@link Long}
	 */
	public void setIdModalidade(Long idModalidade) {
		this.idModalidade = idModalidade;
	}
	/**
	 * @return idApuracaoQtdaPenalidadePeriodicidade {@link Long}
	 */
	public Long getIdApuracaoQtdaPenalidadePeriodicidade() {
		return idApuracaoQtdaPenalidadePeriodicidade;
	}
	/**
	 * @param idApuracaoQtdaPenalidadePeriodicidade {@link Long}
	 */
	public void setIdApuracaoQtdaPenalidadePeriodicidade(Long idApuracaoQtdaPenalidadePeriodicidade) {
		this.idApuracaoQtdaPenalidadePeriodicidade = idApuracaoQtdaPenalidadePeriodicidade;
	}
	
	
	/**
	 * @return idPenalidadesAssociadas {@link Long}
	 */
	public Long[] getIdPenalidadesAssociadas() {
		Long[] idPenalidadesAssociadasTmp = null;
		if (idPenalidadesAssociadas != null) {
			idPenalidadesAssociadasTmp = idPenalidadesAssociadas.clone();
		}
		return idPenalidadesAssociadasTmp;
	}
	/**
	 * @param idPenalidadesAssociadas {@link Long}
	 */
	public void setIdPenalidadesAssociadas(Long[] idPenalidadesAssociadas) {
		if (idPenalidadesAssociadas != null) {
			this.idPenalidadesAssociadas = idPenalidadesAssociadas.clone();
		} else {
			this.idPenalidadesAssociadas = null;
		}
	}
	/**
	 * @return idPenalidadesDisponiveis {@link Long}
	 */
	public Long[] getIdPenalidadesDisponiveis() {
		Long[] idPenalidadesDisponiveisTmp = null;
		if (idPenalidadesDisponiveis != null) {
			idPenalidadesDisponiveisTmp = idPenalidadesDisponiveis.clone();
		}
		return idPenalidadesDisponiveisTmp;
	}
	/**
	 * @param idPenalidadesDisponiveis {@link Long}
	 */
	public void setIdPenalidadesDisponiveis(Long[] idPenalidadesDisponiveis) {
		if(idPenalidadesDisponiveis != null) {
			this.idPenalidadesDisponiveis = idPenalidadesDisponiveis.clone();
		}else {
			this.idPenalidadesDisponiveis = null;
		}
	}
	
	/**
	 * @return idsApuracaoQtdaPenalidadePeriodicidade {@link Long}
	 */
	public Long[] getIdsApuracaoQtdaPenalidadePeriodicidade() {
		Long[] idsApuracaoQtdaPenalidadePeriodicidadeTmp = null;
		if (idsApuracaoQtdaPenalidadePeriodicidade != null) {
			idsApuracaoQtdaPenalidadePeriodicidadeTmp = idsApuracaoQtdaPenalidadePeriodicidade.clone();
		}
		return idsApuracaoQtdaPenalidadePeriodicidadeTmp;
	}
	/**
	 * @param idsApuracaoQtdaPenalidadePeriodicidade {@link Long}
	 */
	public void setIdsApuracaoQtdaPenalidadePeriodicidade(Long[] idsApuracaoQtdaPenalidadePeriodicidade) {
		if(idsApuracaoQtdaPenalidadePeriodicidade != null) {
			this.idsApuracaoQtdaPenalidadePeriodicidade = idsApuracaoQtdaPenalidadePeriodicidade.clone();
		}else {
			this.idsApuracaoQtdaPenalidadePeriodicidade = null;
		}
	}
	
}
