/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.modelocontrato.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.util.GGASBaseTag;
import br.com.ggas.util.Util;

public class CheckBoxObrigatoriedadeDependenteTag extends GGASBaseTag {

	/**
     * 
     */
	private static final long serialVersionUID = 7925382220087536747L;

	private String styleId;

	private String styleClass;

	private String name;

	private String value;

	private String campoPai;

	/**
	 * @return the styleId
	 */
	public String getStyleId() {

		return styleId;
	}

	/**
	 * @param styleId
	 *            the styleId to set
	 */
	public void setStyleId(String styleId) {

		this.styleId = styleId;
	}

	/**
	 * @return the campoPai
	 */
	public String getCampoPai() {

		return campoPai;
	}

	/**
	 * @param campoPai
	 *            the campoPai to set
	 */
	public void setCampoPai(String campoPai) {

		this.campoPai = campoPai;
	}

	/**
	 * @return the styleClass
	 */
	public String getStyleClass() {

		return styleClass;
	}

	/**
	 * @param styleClass
	 *            the styleClass to set
	 */
	public void setStyleClass(String styleClass) {

		this.styleClass = styleClass;
	}

	/**
	 * @return the name
	 */
	public String getName() {

		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {

		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {

		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.GGASBaseTag#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspException {

		StringBuilder html = new StringBuilder();

		String campoDependente = this.name.replaceFirst("obg", "dep");
		Boolean dependente = this.obterValorAtributoRequest(campoDependente);
		if(dependente != null) {
			if(dependente.equals(Boolean.TRUE)) {
				html.append("<img  alt=\"Dependente\" title=\"Dependente\" src=\"");
				html.append(this.getContextPath());
				html.append("/imagens/dependente.gif");
				html.append("\" border=\"0\">");
			} else {
				html.append("<input type=\"checkbox\" ");
				if(!StringUtils.isEmpty(this.styleId)) {
					html.append("id=\"");
					html.append(this.styleId);
					html.append("\" ");
				}
				if(!StringUtils.isEmpty(this.styleClass)) {
					html.append("class=\"");
					html.append(this.styleClass);
					html.append("\" ");
				}
				html.append("name=\"");
				html.append(this.name);
				html.append("\" ");
				html.append("value=\"");
				html.append(this.value);
				html.append("\" ");

				boolean checkado = this.obterValorAtributoForm(this.name);
				if(checkado) {
					html.append("checked=\"checked\" ");
				}

				if(!StringUtils.isEmpty(this.campoPai)) {
					html.append("onclick=\" ");
					html.append("if (this.form.");
					html.append(Util.obterNomeCampoObrigatorio(this.campoPai));
					html.append(".checked == true || (this.form.");
					html.append(Util.obterNomeCampoObrigatorio(this.campoPai));
					html.append(".value == 'true' && this.form.");
					html.append(Util.obterNomeCampoObrigatorio(this.campoPai));
					html.append(".type == 'hidden') ");
					html.append(") { return true; } else { this.checked = false; } \" ");
				}

				html.append("/> ");

				html.append("<script>adicionarEvento(");
				html.append("document.forms[0].");
				html.append(Util.obterNomeCampoSelecao(this.campoPai));
				html.append(",");
				html.append("'click', function() { permitirSelecaoObrigatorio('");
				html.append(Util.obterNomeCampoSelecao(this.campoPai));
				html.append("', '");
				html.append(this.name);
				html.append("'); ");
				html.append("} );</script>");

				html.append("<script>adicionarEvento(");
				html.append("document.forms[0].");
				html.append(Util.obterNomeCampoObrigatorio(this.campoPai));
				html.append(",");
				html.append("'click', function() { verificarSelecaoObrigatorio('");
				html.append(this.name);
				html.append("', '");
				html.append(Util.obterNomeCampoObrigatorio(this.campoPai));
				html.append("'); ");

				html.append("} );</script>");

			}
		}

		try {
			pageContext.getOut().write(html.toString());
		} catch(IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

}
