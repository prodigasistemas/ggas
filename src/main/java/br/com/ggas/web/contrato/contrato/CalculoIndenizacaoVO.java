/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.contrato;

import static br.com.ggas.util.Constantes.FORMATO_DATA_BR;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import br.com.ggas.util.Util;

public class CalculoIndenizacaoVO {

	private Long tipoMulta;

	private BigDecimal valorInvestimento;

	private DateTime dataInvestimento;

	private DateTime dataRecisao;

	private BigDecimal igpmInvestimento;

	private BigDecimal igpmRecisao;

	private BigDecimal mesesPeriodoConsumo;

	private BigDecimal valorAdicionalMensal;

	private BigDecimal investimentoCorrigido;

	private BigDecimal qtdDiariaContratada;

	private BigDecimal qtdMinimaContratada;

	private BigDecimal qtdConsumida;

	private BigDecimal diasRestantes;

	private BigDecimal tarifaGas;

	private BigDecimal valorTotal;

	public Long getTipoMulta() {

		return tipoMulta;
	}

	public void setTipoMulta(Long tipoMulta) {

		this.tipoMulta = tipoMulta;
	}

	public BigDecimal getValorInvestimento() {

		return valorInvestimento;
	}

	public void setValorInvestimento(BigDecimal valorInvestimento) {

		this.valorInvestimento = valorInvestimento;
	}

	public DateTime getDataInvestimento() {

		return dataInvestimento;
	}

	public void setDataInvestimento(DateTime dataInvestimento) {

		this.dataInvestimento = dataInvestimento;
	}

	public DateTime getDataRecisao() {

		return dataRecisao;
	}

	public void setDataRecisao(DateTime dataRecisao) {

		this.dataRecisao = dataRecisao;
	}

	public BigDecimal getIgpmInvestimento() {

		return igpmInvestimento;
	}

	public void setIgpmInvestimento(BigDecimal igpmInvestimento) {

		this.igpmInvestimento = igpmInvestimento;
	}

	public BigDecimal getIgpmRecisao() {

		return igpmRecisao;
	}

	public void setIgpmRecisao(BigDecimal igpmRecisao) {

		this.igpmRecisao = igpmRecisao;
	}

	public BigDecimal getMesesPeriodoConsumo() {

		return mesesPeriodoConsumo;
	}

	public void setMesesPeriodoConsumo(BigDecimal mesesPeriodoConsumo) {

		this.mesesPeriodoConsumo = mesesPeriodoConsumo;
	}

	public BigDecimal getValorAdicionalMensal() {

		return valorAdicionalMensal;
	}

	public void setValorAdicionalMensal(BigDecimal valorAdicionalMensal) {

		this.valorAdicionalMensal = valorAdicionalMensal;
	}

	public BigDecimal getInvestimentoCorrigido() {

		return investimentoCorrigido;
	}

	public void setInvestimentoCorrigido(BigDecimal investimentoCorrigido) {

		this.investimentoCorrigido = investimentoCorrigido;
	}

	public BigDecimal getQtdDiariaContratada() {

		return qtdDiariaContratada;
	}

	public void setQtdDiariaContratada(BigDecimal qtdDiariaContratada) {

		this.qtdDiariaContratada = qtdDiariaContratada;
	}

	public BigDecimal getQtdMinimaContratada() {

		return qtdMinimaContratada;
	}

	public void setQtdMinimaContratada(BigDecimal qtdMinimaContratada) {

		this.qtdMinimaContratada = qtdMinimaContratada;
	}

	public BigDecimal getQtdConsumida() {

		return qtdConsumida;
	}

	public void setQtdConsumida(BigDecimal qtdConsumida) {

		this.qtdConsumida = qtdConsumida;
	}

	public BigDecimal getDiasRestantes() {

		return diasRestantes;
	}

	public void setDiasRestantes(BigDecimal diasRestantes) {

		this.diasRestantes = diasRestantes;
	}

	public BigDecimal getTarifaGas() {

		return tarifaGas;
	}

	public void setTarifaGas(BigDecimal tarifaGas) {

		this.tarifaGas = tarifaGas;
	}

	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	public String getDataInvestimentoFormatada() {

		return Util.converterDataParaStringSemHora(this.dataInvestimento.toDate(), FORMATO_DATA_BR);
	}

	public String getDataRecisaoFormatada() {

		return Util.converterDataParaStringSemHora(this.dataRecisao.toDate(), FORMATO_DATA_BR);
	}
}
