package br.com.ggas.web.contrato.contrato;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.web.contrato.modelocontrato.ModeloContratoVO;

/**
 * Classe VO relacionada as telas da funcionalidade Contrato
 *
 */
public class ContratoVO {
	
	private static final Logger LOG = Logger.getLogger(ContratoVO.class);
	
	private String renovacaoAutomatica;
	private String indicadorAnoContratual;
	private String descricao;
	private String descricaoAbreviada;
	private String fluxoMigracao;		
	private String saldoQNR;
	private String saldoQPNR;
	private String valorTransferidoQNR;
	private String valorTransferidoQPNR;
	private String listaIdsPontoConsumoRemovidos;
	private String listaIdsPontoConsumoRemovidosAgrupados;
	private String pontosSemRecisao;
	private String numeroContratoComAditivo;
	private String numeroContrato;
	private String numeroContratoMigrar;
	private String numeroAnteriorContrato;
	private String dataAssinatura;
	private String situacaoContrato;
	private String numeroAditivo;
	private String dataAditivo;
	private String dataVencimento;
	private String descricaoAditivo;
	private String valorContrato;
	private String indicadorMultaAtraso;
	private String indicadorJurosMora;
	private String indiceCorrecaoMonetaria;
	private String numeroProposta;	 		
	private String idProposta;
	private String tarifaGNAplicada;
	private String gastoEstimadoGNMes;
	private String economiaEstimadaGNMes;
	private String economiaEstimadaGNAno;
	private String descontoEfetivoEconomia;
	private String diaVencFinanciamento;
	private String descGarantiaFinanc;
	private String numeroEmpenho;	 		
	private String clienteAssinatura;
	private String numDiasRenoAutoContrato;
	private String exigeAprovacao;
	private String enderecoPadrao;
	private String faturamentoAgrupamento;
	private String participaECartas;
	private String tipoPeriodicidadePenalidade;
	private String emissaoFaturaAgrupada;
	private String dataVencObrigacoesContratuais;
	private String propostaAprovada;
	private String tempoAntecedenciaRenovacao;
	private String tempoAntecRevisaoGarantias;
	private String periodicidadeReavGarantias;
	private String valorParticipacaoCliente;
	private String qtdParcelasFinanciamento;
	private String percentualJurosFinanciamento;
	private String sistemaAmortizacao;	 		
	private String percentualTarifaDoP;	 		
	private String percentualMulta;
	private String percentualJurosMora;
	private String tipoAgrupamento;	 			 		
	private String contratoCompra;	 			 		
	private String percentualReferenciaSobreDem;	 		
	private String percentualReferenciaSobDem;	 		
	private String percentualSobreTariGas;	 		
	private String baseApuracaoRetMaior;
	private String baseApuracaoRetMenor;
	private String multaRecisoria;
	private String valorInvestimento;
	private String dataInvestimento;
	private String dataRecisao;
	private String descricaoContrato;
	private String volumeReferencia;
	private String periodicidadeTakeOrPayC;
	private String consumoReferenciaTakeOrPayC;
	private String margemVariacaoTakeOrPayC;
	private String recuperacaoAutoTakeOrPayC;
	private String referenciaQFParadaProgramadaC;
	private String percentualMargemVariacaoC;
	private String consumoReferenciaC;
	private String dataInicioVigenciaC;
	private String dataFimVigenciaC;
	private String percentualNaoRecuperavelC;
	private String consideraParadaProgramadaC;
	private String consideraFalhaFornecimentoC;
	private String consideraCasoFortuitoC;
	private String recuperavelC;
	private String tipoApuracaoC;
	private String precoCobrancaC;
	private String apuracaoParadaProgramadaC;
	private String apuracaoCasoFortuitoC;
	private String apuracaoFalhaFornecimentoC;
	private String tipoAgrupamentoContrato;
	private String penalidadeRetMaiorMenor;
	private String penalidadeRetMaiorMenorM;
	private String periodicidadeRetMaiorMenor;
	private String periodicidadeRetMaiorMenorM;
	private String baseApuracaoRetMaiorMenor;
	private String baseApuracaoRetMaiorMenorM;
	private String dataIniVigRetirMaiorMenor;
	private String dataIniVigRetirMaiorMenorM;
	private String dataFimVigRetirMaiorMenor;
	private String dataFimVigRetirMaiorMenorM;
	private String precoCobrancaRetirMaiorMenor;
	private String precoCobrancaRetirMaiorMenorM;
	private String tipoApuracaoRetirMaiorMenor;
	private String tipoApuracaoRetirMaiorMenorM;
	private String percentualCobRetMaiorMenor;
	private String percentualCobRetMaiorMenorM;
	private String percentualCobIntRetMaiorMenor;
	private String percentualCobIntRetMaiorMenorM;
	private String consumoReferenciaRetMaiorMenor;
	private String consumoReferRetMaiorMenorM;
	private String percentualRetMaiorMenor;
	private String percentualRetMaiorMenorM;
	private String dataInicioRetiradaQPNRC;
	private String dataFimRetiradaQPNRC;
	private String percDuranteRetiradaQPNRC;
	private String percMinDuranteRetiradaQPNRC;
	private String percFimRetiradaQPNRC;
	private String tempoValidadeRetiradaQPNRC;
	private String indicadorImposto;
	private String indicadorImpostoM;
	private String periodoTesteDataInicio;
	private String periodoTesteDateFim;
	private String volumeTeste;
	private String prazoTeste;
	private String faxDDD;
	private String faxNumero;
	private String email;
	private String numeroDiasGarantia;
	private String inicioGarantiaConversao;
	private String fimGarantiaConversao;	
	private String vazaoInstantanea;	 		
	private String unidadeVazaoInstan;
	private String vazaoInstantaneaMaxima;
	private String unidadeVazaoInstanMaxima;
	private String vazaoInstantaneaMinima;
	private String unidadeVazaoInstanMinima;
	private String faixaPressaoFornecimento;
	private String pressaoMinimaFornecimento;
	private String pressaoColetada;
	private String unidadePressaoColetada;
	private String unidadePressaoMinimaFornec;
	private String pressaoMaximaFornecimento;
	private String unidadePressaoMaximaFornec;
	private String pressaoLimiteFornecimento;
	private String unidadePressaoLimiteFornec;	 		
	private String numAnosCtrlParadaCliente;
	private String maxTotalParadasCliente;
	private String maxAnualParadasCliente;
	private String numDiasProgrParadaCliente;
	private String numDiasConsecParadaCliente;
	private String numAnosCtrlParadaCDL;
	private String maxTotalParadasCDL;
	private String maxAnualParadasCDL;
	private String numDiasProgrParadaCDL;
	private String numDiasConsecParadaCDL;
	private String tipoFaturamento;
	private String tarifaConsumo;
	private String fatPeriodicidade;
	private String formaCobranca;
	private String arrecadadorConvenio;
	private String debitoAutomatico;
	private String arrecadadorConvenioDebitoAutomatico;
	private String depositoIdentificado;
	private String endFisEntFatCEP;
	private String endFisEntFatNumero;
	private String endFisEntFatComplemento;
	private String endFisEntFatEmail;
	private String itemFatura;
	private String percminimoQDC;
	private String diaVencimentoItemFatura;
	private String diaVencimentoItemFaturaValorFixo;
	private String opcaoVencimentoItemFatura;
	private String faseVencimentoItemFatura;
	private String indicadorVencDiaNaoUtil;
	private String diaCotacao;
	private String dataReferenciaCambial;
	private String indicadorNFE;
	private String emitirFaturaComNfe;
	private String banco;
	private String agencia;
	private String contaCorrente;
	private String tipoGarantiaFinanceira;
	private String descricaoGarantia;
	private String valorGarantiaFinanceira;
	private String dataInicioGarantiaFinanceira;
	private String dataFinalGarantiaFinanceira;
	private String garantiaFinanceiraRenovada; 	
	private String horaInicialDia;
	private String regimeConsumo;
	private String fornecimentoMaximoDiario;
	private String unidadeFornecMaximoDiario;
	private String fornecimentoMinimoDiario;
	private String unidadeFornecMinimoDiario;
	private String fornecimentoMinimoMensal;
	private String unidadeFornecMinimoMensal;
	private String fornecimentoMinimoAnual;
	private String unidadeFornecMinimoAnual;
	private String fatorUnicoCorrecao;
	private String consumoFatFalhaMedicao;
	private String modalidade;
	private String qdc;
	private String dataVigenciaQDC;
	private String prazoRevizaoQDC;
	private String programacaoConsumo;
	private String qdsMaiorQDC;
	private String diasAntecSolicConsumo;
	private String indicadorProgramacaoConsumo;
	private String horaLimiteProgramacaoDiaria;
	private String horaLimiteProgIntradiaria;
	private String horaLimiteAceitacaoDiaria;
	private String horaLimiteAceitIntradiaria;
	private String variacaoSuperiorQDC;
	private String numMesesSolicConsumo;
	private String numHorasSolicConsumo;
	private String confirmacaoAutomaticaQDS;
	private String dataInicioRetiradaQPNR;
	private String dataFimRetiradaQPNR;
	private String percDuranteRetiradaQPNR;
	private String percMinDuranteRetiradaQPNR;
	private String percFimRetiradaQPNR;
	private String localAmostragemPCS;
	private String intervaloRecuperacaoPCS;
	private String tamReducaoRecuperacaoPCS;
	private String periodicidadeTakeOrPay;
	private String periodicidadeShipOrPay;
	private String consumoReferenciaTakeOrPay;
	private String margemVariacaoTakeOrPay;
	private String recuperacaoAutoTakeOrPay;
	private String consumoReferenciaShipOrPay;
	private String margemVariacaoShipOrPay;
	private String consumoReferenciaSobreDem;
	private String faixaPenalidadeSobreDem;
	private String consumoReferenciaSobDem;
	private String faixaPenalidadeSobDem;
	private String percentualQNR;
	private String clienteResponsavel;
	private String tipoResponsabilidade;
	private String dataInicioRelacao;
	private String dataFimRelacao;
	private String indicadorPesquisa;
	private String nomeCliente;
	private String documentoCliente;
	private String enderecoCliente;
	private String emailCliente;
	private String nomeFantasiaImovel;
	private String matriculaImovel;
	private String numeroImovel;
	private String cidadeImovel;
	private String condominio;
	private String tempoValidadeRetiradaQPNR;
	private String enderecoFormatado;
	private String numeroFormatado;
	private String chaveCep;		 	
	private String referenciaQFParadaProgramada;
	private String percentualMargemVariacao;
	private String consumoReferencia;
	private String dataInicioVigencia;
	private String dataFimVigencia;
	private String percentualNaoRecuperavel;
	private String consideraParadaProgramada;
	private String consideraParadaNaoProgramada;
	private String consideraFalhaFornecimento;
	private String consideraCasoFortuito;
	private String recuperavel;
	private String tipoApuracao;
	private String precoCobranca;
	private String apuracaoParadaProgramada;
	private String apuracaoParadaNaoProgramada;
	private String apuracaoCasoFortuito;
	private String apuracaoFalhaFornecimento;
	private String descricaoAnexo;
	private String descricaoImovel;
	private String dataInicio;
	private String dataFim;
	private String cpfCnpj;
	private String cepImovel;
	private String indicadorFaturamentoAgrupado = "false";
	private String indicadorEmissaoAgrupada = "false";
	private String indicadorContratoExibido = "false";
	private String indicadorImovelSelecionado = "false";
	private String faturavel = "";
	private String prazoVigencia;
	private String incentivosComerciais;
	private String incentivosInfraestrutura;	
	
	private String pontoConsumo;
	private String unidadePressaoFornec;
	private String QDCContrato;
	
	private String[] qDCContrato;
	
	
	private boolean fluxoAditamento = false;
	private boolean fluxoDetalhamento = false;
	private boolean fluxoInclusao = false;
	private boolean fluxoAlteracao = false;
	private boolean fluxoPesquisa = false;
	
	
	private Boolean habilitado = true;
	private Boolean mudancaModelo;
	private Boolean selAbaGeral;
	private Boolean selAbaPrincipais;
	private Boolean selAbaTecnicos;
	private Boolean selAbaConsumo;
	private Boolean selAbaFaturamento;
	private Boolean selAbaRegrasFaturamento;
	private Boolean selAbaResponsabilidade;	
	private Boolean selNumeroContrato;
	private Boolean obgNumeroContrato;
	private Boolean selNumeroAnteriorContrato;
	private Boolean obgNumeroAnteriorContrato; 	
	private Boolean selDataAssinatura;
	private Boolean obgDataAssinatura; 	
	private Boolean selSituacaoContrato;
	private Boolean obgSituacaoContrato;
	private Boolean valorFixoSituacaoContrato;
	private Boolean selNumeroAditivo;
	private Boolean obgNumeroAditivo;
	private Boolean selDataAditivo;
	private Boolean obgDataAditivo;		
	private Boolean selDescricaoAditivo;
	private Boolean obgDescricaoAditivo;
	private Boolean selValorContrato;
	private Boolean obgValorContrato; 	 
	private Boolean selIndicadorMultaAtraso;
	private Boolean obgIndicadorMultaAtraso;
	private Boolean valorFixoIndicadorMultaAtraso; 
	private Boolean selIndicadorJurosMora;
	private Boolean obgIndicadorJurosMora;
	private Boolean valorFixoIndicadorJurosMora;
	private Boolean selIndiceCorrecaoMonetaria;
	private Boolean obgIndiceCorrecaoMonetaria;	
	private Boolean valorFixoIndiceCorrecaoMonetaria; 
	private Boolean selNumeroProposta;
	private Boolean obgNumeroProposta;	 		
	private Boolean selTarifaGNAplicada;
	private Boolean obgTarifaGNAplicada;
	private Boolean selGastoEstimadoGNMes;
	private Boolean obgGastoEstimadoGNMes;
	private Boolean selEconomiaEstimadaGNMes;
	private Boolean obgEconomiaEstimadaGNMes;
	private Boolean selEconomiaEstimadaGNAno;
	private Boolean obgEconomiaEstimadaGNAno;
	private Boolean selDescontoEfetivoEconomia;
	private Boolean obgDescontoEfetivoEconomia;
	private Boolean selDiaVencFinanciamento;
	private Boolean obgDiaVencFinanciamento;
	private Boolean valorFixoDiaVencFinanciamento;	
	private Boolean selDescGarantiaFinanc;
	private Boolean obgDescGarantiaFinanc;
	private Boolean valorFixoDescGarantiaFinanc;		 		
	private Boolean selNumeroEmpenho;
	private Boolean obgNumeroEmpenho;	 		
	private Boolean selClienteAssinatura;
	private Boolean obgClienteAssinatura;		 		
	private Boolean selNumDiasRenoAutoContrato;
	private Boolean obgNumDiasRenoAutoContrato;
	private Boolean valorFixoNumDiasRenoAutoContrato;	 			 			 		
	private Boolean selExigeAprovacao;
	private Boolean obgExigeAprovacao;	 		
	private Boolean selEnderecoPadrao;
	private Boolean obgEnderecoPadrao;	 		
	private Boolean selFaturamentoAgrupamento;
	private Boolean obgFaturamentoAgrupamento;
	private Boolean valorFixoFaturamentoAgrupamento;
	private Boolean selParticipaECartas;
	private Boolean obgParticipaECartas;
	private Boolean valorFixoParticipaECartas;
	private Boolean selTipoPeriodicidadePenalidade;
	private Boolean obgTipoPeriodicidadePenalidade;
	private Boolean valorFixoTipoPeriodicidadePenalidade;
	private Boolean selEmissaoFaturaAgrupada;
	private Boolean obgEmissaoFaturaAgrupada;
	private Boolean valorFixoEmissaoFaturaAgrupada;	 			 
	private Boolean selDataVigenciaContrato;
	private Boolean obgDataVigenciaContrato;
	private Boolean selDataVencObrigacoesContratuais;
	private Boolean obgDataVencObrigacoesContratuais;
	private Boolean selIndicadorAnoContratual;
	private Boolean obgIndicadorAnoContratual;
	private Boolean valorFixoIndicadorAnoContratual;
	private Boolean selRenovacaoAutomatica;
	private Boolean obgRenovacaoAutomatica;
	private Boolean valorFixoRenovacaoAutomatica;
	private Boolean selTempoAntecedenciaRenovacao;
	private Boolean obgTempoAntecedenciaRenovacao;
	private Boolean valorFixoTempoAntecedenciaRenovacao;
	private Boolean selTempoAntecRevisaoGarantias;
	private Boolean obgTempoAntecRevisaoGarantias;
	private Boolean valorFixoTempoAntecRevisaoGarantias;
	private Boolean selPeriodicidadeReavGarantias;
	private Boolean obgPeriodicidadeReavGarantias;
	private Boolean valorFixoPeriodicidadeReavGarantias;
	private Boolean selValorParticipacaoCliente;
	private Boolean obgValorParticipacaoCliente;
	private Boolean selQtdParcelasFinanciamento;
	private Boolean obgQtdParcelasFinanciamento;
	private Boolean selPercentualJurosFinanciamento;
	private Boolean obgPercentualJurosFinanciamento;
	private Boolean selSistemaAmortizacao;
	private Boolean obgSistemaAmortizacao;
	private Boolean selPercentualTarifaDoP;
	private Boolean obgPercentualTarifaDoP;
	private Boolean valorFixoPercentualTarifaDoP;
	private Boolean selPercentualMulta;
	private Boolean obgPercentualMulta;
	private Boolean valorFixoPercentualMulta;
	private Boolean selPercentualJurosMora;
	private Boolean obgPercentualJurosMora;
	private Boolean valorFixoPercentualJurosMora;
	private Boolean selTipoAgrupamento;
	private Boolean obgTipoAgrupamento; 
	private Boolean selContratoCompra;
	private Boolean obgContratoCompra;
	private Boolean valorFixoContratoCompra;
	private Boolean selPercentualReferenciaSobreDem;
	private Boolean obgPercentualReferenciaSobreDem;
	private Boolean selPercentualReferenciaSobDem;
	private Boolean obgPercentualReferenciaSobDem;
	private Boolean selPercentualSobreTariGas;
	private Boolean obgPercentualSobreTariGas;
	private Boolean valorFixoPercentualSobreTariGas;
	private Boolean selBaseApuracaoRetMaior;
	private Boolean obgBaseApuracaoRetMaior;
	private Boolean selBaseApuracaoRetMenor;
	private Boolean obgBaseApuracaoRetMenor;
	private Boolean selMultaRecisoria;
	private Boolean obgMultaRecisoria;
	private Boolean valorFixoMultaRecisoria;
	private Boolean selValorInvestimento;
	private Boolean obgValorInvestimento;
	private Boolean selDataInvestimento;
	private Boolean obgDataInvestimento;
	private Boolean selDataRecisao;
	private Boolean obgDataRecisao;
	private Boolean selDescricaoContrato;
	private Boolean obgDescricaoContrato;
	private Boolean selVolumeReferencia;
	private Boolean obgVolumeReferencia;
	private Boolean selDocumentosAnexos;
	private Boolean obgDocumentosAnexos;
	private Boolean selPeriodicidadeTakeOrPayC;
	private Boolean obgPeriodicidadeTakeOrPayC;
	private Boolean valorFixoPeriodicidadeTakeOrPayC;
	private Boolean selConsumoReferenciaTakeOrPayC;
	private Boolean obgConsumoReferenciaTakeOrPayC;
	private Boolean valorFixoConsumoReferenciaTakeOrPayC;
	private Boolean selMargemVariacaoTakeOrPayC;
	private Boolean obgMargemVariacaoTakeOrPayC;
	private Boolean valorFixoMargemVariacaoTakeOrPayC;
	private Boolean selRecuperacaoAutoTakeOrPayC;
	private Boolean obgRecuperacaoAutoTakeOrPayC;	 		
	private Boolean selReferenciaQFParadaProgramadaC;
	private Boolean obgReferenciaQFParadaProgramadaC;
	private Boolean valorFixoReferenciaQFParadaProgramadaC;
	private Boolean selTipoAgrupamentoContrato;
	private Boolean obgTipoAgrupamentoContrato;
	private Boolean valorFixoTipoAgrupamento;
	private Boolean selPercentualMargemVariacaoC;
	private Boolean selPeriodicidadePenalidadeC;
	private Boolean selConsumoReferenciaC;
	private Boolean selPenalidadeC;
	private Boolean selDataInicioVigenciaC;
	private Boolean selDataFimVigenciaC;
	private Boolean selPercentualNaoRecuperavelC;
	private Boolean selConsideraParadaProgramadaC;
	private Boolean selConsideraFalhaFornecimentoC;
	private Boolean selConsideraCasoFortuitoC;
	private Boolean selRecuperavelC;
	private Boolean selTipoApuracaoC;
	private Boolean selPrecoCobrancaC;
	private Boolean selApuracaoParadaProgramadaC;
	private Boolean selApuracaoCasoFortuitoC;
	private Boolean selApuracaoFalhaFornecimentoC;
	private Boolean obgPercentualMargemVariacaoC;
	private Boolean obgPeriodicidadePenalidadeC;
	private Boolean obgConsumoReferenciaC;
	private Boolean obgPenalidadeC;
	private Boolean obgDataInicioVigenciaC;
	private Boolean obgDataFimVigenciaC;
	private Boolean obgPercentualNaoRecuperavelC;
	private Boolean obgConsideraParadaProgramadaC;
	private Boolean obgConsideraFalhaFornecimentoC;
	private Boolean obgConsideraCasoFortuitoC;
	private Boolean obgRecuperavelC;
	private Boolean obgTipoApuracaoC;
	private Boolean obgPrecoCobrancaC;
	private Boolean obgApuracaoParadaProgramadaC;
	private Boolean obgApuracaoCasoFortuitoC;
	private Boolean obgApuracaoFalhaFornecimentoC;
	private Boolean valorFixoPercentualNaoRecuperavelC;
	private Boolean valorFixoConsideraParadaProgramadaC;
	private Boolean valorFixoConsideraFalhaFornecimentoC;
	private Boolean valorFixoConsideraCasoFortuitoC;
	private Boolean valorFixoRecuperavelC;
	private Boolean valorFixoTipoApuracaoC;
	private Boolean valorFixoPrecoCobrancaC;
	private Boolean valorFixoApuracaoParadaProgramadaC;
	private Boolean valorFixoApuracaoCasoFortuitoC;
	private Boolean valorFixoApuracaoFalhaFornecimentoC;		
	private Boolean selPenalidadeRetMaiorMenor;
	private Boolean obgPenalidadeRetMaiorMenor;
	private Boolean valorFixoPenalidadeRetMaiorMenor;
	private Boolean selPenalidadeRetMaiorMenorM;
	private Boolean obgPenalidadeRetMaiorMenorM;
	private Boolean selPeriodicidadeRetMaiorMenor;
	private Boolean obgPeriodicidadeRetMaiorMenor;
	private Boolean valorFixoPeriodicidadeRetMaiorMenor;
	private Boolean selPeriodicidadeRetMaiorMenorM;
	private Boolean obgPeriodicidadeRetMaiorMenorM;
	private Boolean selBaseApuracaoRetMaiorMenor;
	private Boolean obgBaseApuracaoRetMaiorMenor;
	private Boolean valorFixoBaseApuracaoRetMaiorMenor;
	private Boolean selBaseApuracaoRetMaiorMenorM;
	private Boolean obgBaseApuracaoRetMaiorMenorM;
	private Boolean selDataIniVigRetirMaiorMenor;
	private Boolean obgDataIniVigRetirMaiorMenor;
	private Boolean selDataIniVigRetirMaiorMenorM;
	private Boolean obgDataIniVigRetirMaiorMenorM;
	private Boolean selDataFimVigRetirMaiorMenor;
	private Boolean obgDataFimVigRetirMaiorMenor;
	private Boolean selDataFimVigRetirMaiorMenorM;
	private Boolean obgDataFimVigRetirMaiorMenorM;
	private Boolean selPrecoCobrancaRetirMaiorMenor;
	private Boolean obgPrecoCobrancaRetirMaiorMenor;
	private Boolean valorFixoPrecoCobrancaRetirMaiorMenor;
	private Boolean selPrecoCobrancaRetirMaiorMenorM;
	private Boolean obgPrecoCobrancaRetirMaiorMenorM;
	private Boolean selTipoApuracaoRetirMaiorMenor;
	private Boolean obgTipoApuracaoRetirMaiorMenor;
	private Boolean valorFixoTipoApuracaoRetirMaiorMenor;
	private Boolean selTipoApuracaoRetirMaiorMenorM;
	private Boolean obgTipoApuracaoRetirMaiorMenorM;
	private Boolean selPercentualCobRetMaiorMenor;
	private Boolean obgPercentualCobRetMaiorMenor;
	private Boolean valorFixoPercentualCobRetMaiorMenor;
	private Boolean selPercentualCobRetMaiorMenorM;
	private Boolean obgPercentualCobRetMaiorMenorM;
	private Boolean selPercentualCobIntRetMaiorMenor;
	private Boolean obgPercentualCobIntRetMaiorMenor;
	private Boolean valorFixoPercentualCobIntRetMaiorMenor;
	private Boolean selPercentualCobIntRetMaiorMenorM;
	private Boolean obgPercentualCobIntRetMaiorMenorM;
	private Boolean selConsumoReferenciaRetMaiorMenor;
	private Boolean obgConsumoReferenciaRetMaiorMenor;
	private Boolean valorFixoConsumoReferenciaRetMaiorMenor;
	private Boolean selConsumoReferRetMaiorMenorM;
	private Boolean obgConsumoReferRetMaiorMenorM;
	private Boolean selPercentualRetMaiorMenor;
	private Boolean obgPercentualRetMaiorMenor;
	private Boolean valorFixoPercentualRetMaiorMenor;
	private Boolean selPercentualRetMaiorMenorM;
	private Boolean obgPercentualRetMaiorMenorM;
	private Boolean valorFixoPercentualRetMaiorMenorM;
	private Boolean selDataInicioRetiradaQPNRC;
	private Boolean obgDataInicioRetiradaQPNRC;
	private Boolean selDataFimRetiradaQPNRC;
	private Boolean obgDataFimRetiradaQPNRC;
	private Boolean selPercDuranteRetiradaQPNRC;
	private Boolean obgPercDuranteRetiradaQPNRC;
	private Boolean valorFixoPercDuranteRetiradaQPNRC;
	private Boolean selPercMinDuranteRetiradaQPNRC;
	private Boolean obgPercMinDuranteRetiradaQPNRC;
	private Boolean valorFixoPercMinDuranteRetiradaQPNRC;
	private Boolean selPercFimRetiradaQPNRC;
	private Boolean obgPercFimRetiradaQPNRC;
	private Boolean valorFixoPercFimRetiradaQPNRC;
	private Boolean selTempoValidadeRetiradaQPNRC;
	private Boolean obgTempoValidadeRetiradaQPNRC;
	private Boolean valorFixoTempoValidadeRetiradaQPNRC;
	private Boolean selIndicadorImposto;
	private Boolean obgIndicadorImposto;	
	private Boolean valorFixoIndicadorImposto;
	private Boolean selIndicadorImpostoM;
	private Boolean obgIndicadorImpostoM;	
	private Boolean valorFixoIndicadorImpostoM;
	private Boolean selPeriodoTesteDataInicio;
	private Boolean obgPeriodoTesteDataInicio;
	private Boolean selPeriodoTesteDateFim;
	private Boolean obgPeriodoTesteDateFim;
	private Boolean selVolumeTeste;
	private Boolean obgVolumeTeste;
	private Boolean selPrazoTeste;
	private Boolean obgPrazoTeste;
	private Boolean selFaxDDD;
	private Boolean obgFaxDDD;
	private Boolean selFaxNumero;
	private Boolean obgFaxNumero;
	private Boolean selEmail;
	private Boolean obgEmail;
	private Boolean selNumeroDiasGarantia;
	private Boolean obgNumeroDiasGarantia;
	private Boolean valorFixoNumeroDiasGarantia;
	private Boolean selInicioGarantiaConversao;
	private Boolean obgInicioGarantiaConversao;
	private Boolean selVazaoInstantanea;
	private Boolean obgVazaoInstantanea;
	private Boolean valorFixoVazaoInstantanea;
	private Boolean selVazaoInstantaneaMaxima;
	private Boolean obgVazaoInstantaneaMaxima;
	private Boolean valorFixoVazaoInstantaneaMaxima;
	private Boolean selVazaoInstantaneaMinima;
	private Boolean obgVazaoInstantaneaMinima;
	private Boolean valorFixoVazaoInstantaneaMinima;
	private Boolean selFaixaPressaoFornecimento;
	private Boolean obgFaixaPressaoFornecimento;
	private Boolean valorFixoFaixaPressaoFornecimento;
	private Boolean selPressaoMinimaFornecimento;
	private Boolean obgPressaoMinimaFornecimento;
	private Boolean valorFixoPressaoMinimaFornecimento;
	private Boolean selPressaoColetada;
	private Boolean obgPressaoColetada;
	private Boolean valorFixoPressaoColetada;
	private Boolean selPressaoMaximaFornecimento;
	private Boolean obgPressaoMaximaFornecimento;	 	
	private Boolean valorFixoPressaoMaximaFornecimento;	 	
	private Boolean selPressaoLimiteFornecimento;
	private Boolean obgPressaoLimiteFornecimento;
	private Boolean valorFixoPressaoLimiteFornecimento;
	private Boolean selNumAnosCtrlParadaCliente;
	private Boolean obgNumAnosCtrlParadaCliente;
	private Boolean valorFixoNumAnosCtrlParadaCliente;
	private Boolean selMaxTotalParadasCliente;
	private Boolean obgMaxTotalParadasCliente;	
	private Boolean valorFixoMaxTotalParadasCliente;	
	private Boolean selMaxAnualParadasCliente;
	private Boolean obgMaxAnualParadasCliente; 
	private Boolean valorFixoMaxAnualParadasCliente; 
	private Boolean selNumDiasProgrParadaCliente;
	private Boolean obgNumDiasProgrParadaCliente;	
	private Boolean valorFixoNumDiasProgrParadaCliente;	
	private Boolean selNumDiasConsecParadaCliente;
	private Boolean obgNumDiasConsecParadaCliente; 			
	private Boolean valorFixoNumDiasConsecParadaCliente; 			
	private Boolean selNumAnosCtrlParadaCDL;
	private Boolean obgNumAnosCtrlParadaCDL;
	private Boolean valorFixoNumAnosCtrlParadaCDL;
	private Boolean selMaxTotalParadasCDL;
	private Boolean obgMaxTotalParadasCDL;	
	private Boolean valorFixoMaxTotalParadasCDL;	
	private Boolean selMaxAnualParadasCDL;
	private Boolean obgMaxAnualParadasCDL; 
	private Boolean valorFixoMaxAnualParadasCDL; 
	private Boolean selNumDiasProgrParadaCDL;
	private Boolean obgNumDiasProgrParadaCDL;
	private Boolean valorFixoNumDiasProgrParadaCDL;
	private Boolean selNumDiasConsecParadaCDL;
	private Boolean obgNumDiasConsecParadaCDL;
	private Boolean valorFixoNumDiasConsecParadaCDL;
	private Boolean selTipoFaturamento;
	private Boolean obgTipoFaturamento;
	private Boolean selTarifaConsumo;
	private Boolean obgTarifaConsumo;
	private Boolean valorFixoTarifaConsumo;
	private Boolean selFatPeriodicidade;
	private Boolean obgFatPeriodicidade;
	private Boolean valorFixoFatPeriodicidade;
	private Boolean selFormaCobranca;
	private Boolean obgFormaCobranca;
	private Boolean valorFixoFormaCobranca;
	private Boolean selArrecadadorConvenio;
	private Boolean obgArrecadadorConvenio;
	private Boolean valorFixoArrecadadorConvenio;
	private Boolean selDebitoAutomatico;
	private Boolean obgDebitoAutomatico;
	private Boolean valorFixoDebitoAutomatico;
	private Boolean selArrecadadorConvenioDebitoAutomatico;
	private Boolean obgArrecadadorConvenioDebitoAutomatico;
	private Boolean valorFixoArrecadadorConvenioDebitoAutomatico;	 		
	private Boolean selDepositoIdentificado;
	private Boolean obgDepositoIdentificado;
	private Boolean valorFixoDepositoIdentificado;
	private Boolean selEndFisEntFatCEP;
	private Boolean obgEndFisEntFatCEP;
	private Boolean selEndFisEntFatNumero;
	private Boolean obgEndFisEntFatNumero;
	private Boolean selEndFisEntFatComplemento;
	private Boolean obgEndFisEntFatComplemento;
	private Boolean selEndFisEntFatEmail;
	private Boolean obgEndFisEntFatEmail;	 		
	private Boolean selItemFatura;
	private Boolean obgItemFatura;
	private Boolean valorFixoItemFatura;
	private Boolean selPercminimoQDC;
	private Boolean obgPercminimoQDC;
	private Boolean valorFixoPercminimoQDC;
	private Boolean selDiaVencimentoItemFatura;
	private Boolean obgDiaVencimentoItemFatura;
	private Boolean valorFixoDiaVencimentoItemFatura;
	private Boolean selOpcaoVencimentoItemFatura;
	private Boolean obgOpcaoVencimentoItemFatura;
	private Boolean valorFixoOpcaoVencimentoItemFatura;
	private Boolean selFaseVencimentoItemFatura;
	private Boolean obgFaseVencimentoItemFatura;
	private Boolean valorFixoFaseVencimentoItemFatura;
	private Boolean selIndicadorVencDiaNaoUtil;
	private Boolean obgIndicadorVencDiaNaoUtil;	 
	private Boolean valorFixoIndicadorVencDiaNaoUtil;	 
	private Boolean selDiaCotacao;
	private Boolean obgDiaCotacao;
	private Boolean valorFixoDiaCotacao;
	private Boolean selDataReferenciaCambial;
	private Boolean obgDataReferenciaCambial;
	private Boolean valorFixoDataReferenciaCambial;
	private Boolean selIndicadorNFE;
	private Boolean obgIndicadorNFE;
	private Boolean valorFixoIndicadorNFE;
	private Boolean selEmitirFaturaComNfe;
	private Boolean obgEmitirFaturaComNfe;
	private Boolean valorFixoEmitirFaturaComNfe;
	private Boolean selBanco;
	private Boolean obgBanco;
	private Boolean valorFixoBanco;
	private Boolean selAgencia;
	private Boolean obgAgencia;
	private Boolean valorFixoAgencia;
	private Boolean selContaCorrente;
	private Boolean obgContaCorrente;
	private Boolean valorFixoContaCorrente;
	private Boolean selTipoGarantiaFinanceira;
	private Boolean obgTipoGarantiaFinanceira;
	private Boolean valorFixoTipoGarantiaFinanceira;
	private Boolean selDescricaoGarantia;
	private Boolean obgDescricaoGarantia;
	private Boolean selValorGarantiaFinanceira;
	private Boolean obgValorGarantiaFinanceira;
	private Boolean valorFixoValorGarantiaFinanceira;
	private Boolean selDataInicioGarantiaFinanceira;
	private Boolean obgDataInicioGarantiaFinanceira;
	private Boolean selDataFinalGarantiaFinanceira;
	private Boolean obgDataFinalGarantiaFinanceira;
	private Boolean selGarantiaFinanceiraRenovada;
	private Boolean obgGarantiaFinanceiraRenovada;
	private Boolean valorFixoGarantiaFinanceiraRenovada; 		
	private Boolean selHoraInicialDia;
	private Boolean obgHoraInicialDia;
	private Boolean valorFixoHoraInicialDia;
	private Boolean selRegimeConsumo;
	private Boolean obgRegimeConsumo;
	private Boolean valorFixoRegimeConsumo;
	private Boolean selFornecimentoMaximoDiario;
	private Boolean obgFornecimentoMaximoDiario;
	private Boolean valorFixoFornecimentoMaximoDiario;
	private Boolean selFornecimentoMinimoDiario;
	private Boolean obgFornecimentoMinimoDiario;
	private Boolean valorFixoFornecimentoMinimoDiario;
	private Boolean selFornecimentoMinimoMensal;
	private Boolean obgFornecimentoMinimoMensal;
	private Boolean valorFixoFornecimentoMinimoMensal;
	private Boolean selFornecimentoMinimoAnual;
	private Boolean obgFornecimentoMinimoAnual;
	private Boolean valorFixoFornecimentoMinimoAnual;
	private Boolean selFatorUnicoCorrecao;
	private Boolean obgFatorUnicoCorrecao;
	private Boolean selConsumoFatFalhaMedicao;
	private Boolean obgConsumoFatFalhaMedicao;
	private Boolean valorFixoConsumoFatFalhaMedicao;
	private Boolean selModalidade;
	private Boolean obgModalidade;	 		
	private Boolean selQdc;
	private Boolean obgQdc;	 		
	private Boolean selDataVigenciaQDC;
	private Boolean obgDataVigenciaQDC;
	private Boolean selPrazoRevizaoQDC;
	private Boolean obgPrazoRevizaoQDC;
	private Boolean selProgramacaoConsumo;
	private Boolean obgProgramacaoConsumo;
	private Boolean selQdsMaiorQDC;
	private Boolean obgQdsMaiorQDC;
	private Boolean selDiasAntecSolicConsumo;
	private Boolean obgDiasAntecSolicConsumo;
	private Boolean selIndicadorProgramacaoConsumo;
	private Boolean obgIndicadorProgramacaoConsumo;
	private Boolean selHoraLimiteProgramacaoDiaria;
	private Boolean obgHoraLimiteProgramacaoDiaria;
	private Boolean selHoraLimiteProgIntradiaria;
	private Boolean obgHoraLimiteProgIntradiaria;
	private Boolean selHoraLimiteAceitacaoDiaria;
	private Boolean obgHoraLimiteAceitacaoDiaria;
	private Boolean selHoraLimiteAceitIntradiaria;
	private Boolean obgHoraLimiteAceitIntradiaria;
	private Boolean selVariacaoSuperiorQDC;
	private Boolean obgVariacaoSuperiorQDC;
	private Boolean selNumMesesSolicConsumo;
	private Boolean obgNumMesesSolicConsumo;
	private Boolean selNumHorasSolicConsumo;
	private Boolean obgNumHorasSolicConsumo;	 		
	private Boolean selConfirmacaoAutomaticaQDS;
	private Boolean obgConfirmacaoAutomaticaQDS;	 		
	private Boolean selDataInicioRetiradaQPNR;
	private Boolean obgDataInicioRetiradaQPNR;	 		
	private Boolean selDataFimRetiradaQPNR;
	private Boolean obgDataFimRetiradaQPNR;	 		
	private Boolean selPercDuranteRetiradaQPNR;
	private Boolean obgPercDuranteRetiradaQPNR;	
	private Boolean selPercMinDuranteRetiradaQPNR;
	private Boolean obgPercMinDuranteRetiradaQPNR; 		
	private Boolean selPercFimRetiradaQPNR;
	private Boolean obgPercFimRetiradaQPNR;	 		
	private Boolean selLocalAmostragemPCS;
	private Boolean obgLocalAmostragemPCS;	 		
	private Boolean selIntervaloRecuperacaoPCS;
	private Boolean obgIntervaloRecuperacaoPCS;	 		
	private Boolean selTamReducaoRecuperacaoPCS;
	private Boolean obgTamReducaoRecuperacaoPCS;
	private Boolean selPeriodicidadeTakeOrPay;
	private Boolean obgPeriodicidadeTakeOrPay;
	private Boolean selPeriodicidadeShipOrPay;
	private Boolean obgPeriodicidadeShipOrPay;
	private Boolean selConsumoReferenciaTakeOrPay;
	private Boolean obgConsumoReferenciaTakeOrPay;
	private Boolean selMargemVariacaoTakeOrPay;
	private Boolean obgMargemVariacaoTakeOrPay;
	private Boolean selRecuperacaoAutoTakeOrPay;
	private Boolean obgRecuperacaoAutoTakeOrPay;
	private Boolean selConsumoReferenciaShipOrPay;
	private Boolean obgConsumoReferenciaShipOrPay;
	private Boolean selMargemVariacaoShipOrPay;
	private Boolean obgMargemVariacaoShipOrPay;
	private Boolean selConsumoReferenciaSobreDem;
	private Boolean obgConsumoReferenciaSobreDem;	 		
	private Boolean selFaixaPenalidadeSobreDem;
	private Boolean obgFaixaPenalidadeSobreDem;
	private Boolean selConsumoReferenciaSobDem;
	private Boolean obgConsumoReferenciaSobDem;
	private Boolean selFaixaPenalidadeSobDem;
	private Boolean obgFaixaPenalidadeSobDem;
	private Boolean selPercentualQNR;
	private Boolean obgPercentualQNR;
	private Boolean selClienteResponsavel;
	private Boolean obgClienteResponsavel;
	private Boolean selTipoResponsabilidade;
	private Boolean obgTipoResponsabilidade;
	private Boolean valorFixoTipoResponsabilidade;
	private Boolean selDataInicioRelacao;
	private Boolean obgDataInicioRelacao;
	private Boolean selDataFimRelacao;
	private Boolean obgDataFimRelacao;
	private Boolean selQDCContrato;
	private Boolean obgQDCContrato; 
	private Boolean selTempoValidadeRetiradaQPNR;
	private Boolean obgTempoValidadeRetiradaQPNR;
	private Boolean selReferenciaQFParadaProgramada;
	private Boolean obgReferenciaQFParadaProgramada;	
	private Boolean selPercentualMargemVariacao;
	private Boolean selPeriodicidadePenalidade;
	private Boolean selConsumoReferencia;
	private Boolean selPenalidade;
	private Boolean selDataInicioVigencia;
	private Boolean selDataFimVigencia;
	private Boolean selPercentualNaoRecuperavel;
	private Boolean selConsideraParadaProgramada;
	private Boolean selConsideraParadaNaoProgramada;
	private Boolean selConsideraFalhaFornecimento;
	private Boolean selConsideraCasoFortuito;
	private Boolean selRecuperavel;
	private Boolean selTipoApuracao;
	private Boolean selPrecoCobranca;
	private Boolean selApuracaoParadaProgramada;
	private Boolean selApuracaoParadaNaoProgramada;
	private Boolean selApuracaoCasoFortuito;
	private Boolean selApuracaoFalhaFornecimento;
	private Boolean obgPercentualMargemVariacao;
	private Boolean obgPeriodicidadePenalidade;
	private Boolean obgConsumoReferencia;
	private Boolean obgPenalidade;
	private Boolean obgDataInicioVigencia;
	private Boolean obgDataFimVigencia;
	private Boolean obgPercentualNaoRecuperavel;
	private Boolean obgConsideraParadaProgramada;
	private Boolean obgConsideraParadaNaoProgramada;
	private Boolean obgConsideraFalhaFornecimento;
	private Boolean obgConsideraCasoFortuito;
	private Boolean obgRecuperavel;
	private Boolean obgTipoApuracao;
	private Boolean obgPrecoCobranca;
	private Boolean obgApuracaoParadaProgramada;
	private Boolean obgApuracaoParadaNaoProgramada;
	private Boolean obgApuracaoCasoFortuito;
	private Boolean obgApuracaoFalhaFornecimento;
	private Boolean finalizacaoEncerramento = false;
	private Boolean chamadoTela = false;
	private Boolean cobrarIndenizacao = true;
	private Boolean finalizacaoContrato = false;
	private Boolean selPrazoVigencia;
	private Boolean obgPrazoVigencia; 	
	private Boolean selIncentivosComerciais;
	private Boolean obgIncentivosComerciais; 		
	private Boolean selIncentivosInfraestrutura;
	private Boolean obgIncentivosInfraestrutura; 		
	
	private Boolean valorFixoSistemaAmortizacao;
	private Boolean valorFixoValorParticipacaoCliente;
	private Boolean selPontoConsumo;
	private Boolean obgPontoConsumo;
	private Boolean valorFixoPontoConsumo;
	private Boolean valorFixoDataInicioRelacao;
	private Boolean valorFixoDataAditivo;
	private Boolean valorFixoNumeroAditivo;
	private Boolean selUnidadeFornecMinimoMensal;
	private Boolean obgUnidadeFornecMinimoMensal;
	private Boolean valorFixoUnidadeFornecMinimoMensal;
	private Boolean valorFixoClienteResponsavel;
	private Boolean valorFixoEconomiaEstimadaGNMes;
	private Boolean valorFixoTamReducaoRecuperacaoPCS;
	private Boolean valorFixoDataAssinatura;
	private Boolean selUnidadeFornecMaximoDiario;
	private Boolean obgUnidadeFornecMaximoDiario;
	private Boolean valorFixoUnidadeFornecMaximoDiario;
	private Boolean valorFixoClienteAssinatura;
	private Boolean selUnidadeVazaoInstanMaxima;
	private Boolean obgUnidadeVazaoInstanMaxima;
	private Boolean valorFixoUnidadeVazaoInstanMaxima;
	private Boolean valorFixoValorInvestimento;
	private Boolean valorFixoEconomiaEstimadaGNAno;
	private Boolean valorFixoLocalAmostragemPCS;
	private Boolean valorFixoNumeroProposta;
	private Boolean valorFixoDataInvestimento;
	private Boolean valorFixoEndFisEntFatComplemento;
	private Boolean valorFixoPercentualJurosFinanciamento;
	private Boolean valorFixoNumeroContrato;
	private Boolean valorFixoDescricaoAditivo;
	private Boolean selUnidadePressaoFornec;
	private Boolean obgUnidadePressaoFornec;
	private Boolean valorFixoUnidadePressaoFornec;
	private Boolean valorFixoEndFisEntFatNumero;
	private Boolean valorFixoGastoEstimadoGNMes;
	private Boolean valorFixoEnderecoPadrao;
	private Boolean selPropostaAprovada;
	private Boolean obgPropostaAprovada;
	private Boolean valorFixoPropostaAprovada;
	private Boolean valorFixoQtdParcelasFinanciamento;
	private Boolean valorFixoEndFisEntFatCEP;
	private Boolean valorFixoExigeAprovacao;
	private Boolean valorFixoDescontoEfetivoEconomia;
	private Boolean valorFixoDataFimRelacao;
	private Boolean valorFixoIntervaloRecuperacaoPCS;
	private Boolean valorFixoApuracaoCasoFortuito;
	private Boolean valorFixoDataFimVigencia;
	private Boolean valorFixoDataInicioRetiradaQPNR;
	private Boolean valorFixoPercMinDuranteRetiradaQPNR;
	private Boolean valorFixoTempoValidadeRetiradaQPNR;
	private Boolean valorFixoRecuperavel;
	private Boolean valorFixoTipoApuracao;
	private Boolean valorFixoDataFinalGarantiaFinanceira;
	private Boolean valorFixoEndFisEntFatEmail;
	private Boolean valorFixoPercentualNaoRecuperavel;
	private Boolean valorFixoConsideraCasoFortuito;
	private Boolean valorFixoTipoAgrupamentoContrato;
	private Boolean valorFixoMargemVariacaoTakeOrPay;
	private Boolean valorFixoReferenciaQFParadaProgramada;
	private Boolean valorFixoConsideraParadaProgramada;
	private Boolean valorFixoApuracaoFalhaFornecimento;
	private Boolean valorFixoPrazoRevizaoQDC;
	private Boolean valorFixoDataVigenciaQDC;
	private Boolean valorFixoConsideraFalhaFornecimento;
	private Boolean valorFixoPrecoCobranca;
	private Boolean valorFixoQdc;
	private Boolean valorFixoDataInicioGarantiaFinanceira;
	private Boolean valorFixoPeriodicidadeTakeOrPay;
	private Boolean selUnidadeFornecMinimoAnual;
	private Boolean obgUnidadeFornecMinimoAnual;
	private Boolean valorFixoUnidadeFornecMinimoAnual;
	private Boolean valorFixoModalidade;
	private Boolean valorFixoDataFimRetiradaQPNR;
	private Boolean valorFixoRecuperacaoAutoTakeOrPay;
	private Boolean valorFixoQDCContrato;
	private Boolean valorFixoPercDuranteRetiradaQPNR;
	private Boolean valorFixoDataInicioVigencia;
	private Boolean valorFixoDataVencObrigacoesContratuais;
	private Boolean valorFixoApuracaoParadaProgramada;
	private Boolean valorFixoPercFimRetiradaQPNR;
	private Boolean valorFixoConsumoReferenciaTakeOrPay;
	private Boolean selUnidadePressaoMaximaFornec; 
	private Boolean obgUnidadePressaoMaximaFornec; 
	private Boolean valorFixoUnidadePressaoMaximaFornec; 
	private Boolean selUnidadePressaoMinimaFornec; 
	private Boolean obgUnidadePressaoMinimaFornec; 
	private Boolean valorFixoUnidadePressaoMinimaFornec; 
	private Boolean selUnidadeVazaoInstanMinima; 
	private Boolean obgUnidadeVazaoInstanMinima; 
	private Boolean valorFixoUnidadeVazaoInstanMinima; 
	private Boolean valorFixoFaixaPenalidadeSobreDem;
	private Boolean valorFixoBaseApuracaoRetMenor;
	private Boolean valorFixoPercentualReferenciaSobDem;
	private Boolean valorFixoConsumoReferenciaSobreDem;
	private Boolean valorFixoConsumoReferenciaSobDem;
	private Boolean valorFixoBaseApuracaoRetMaior;
	private Boolean valorFixoFaixaPenalidadeSobDem;
	private Boolean valorFixoPercentualReferenciaSobreDem;
	private Boolean selUnidadePressaoLimiteFornec;
	private Boolean obgUnidadePressaoLimiteFornec;
	private Boolean valorFixoUnidadePressaoLimiteFornec;
	private Boolean valorFixoNumeroAnteriorContrato;
	private Boolean valorFixoFaxNumero;
	private Boolean valorFixoFaxDDD;
	private Boolean selUnidadeFornecMinimoDiario;
	private Boolean obgUnidadeFornecMinimoDiario;
	private Boolean valorFixoUnidadeFornecMinimoDiario;
	private Boolean valorFixoEmail;
	private Boolean selUnidadeVazaoInstan;
	private Boolean obgUnidadeVazaoInstan;
	private Boolean valorFixoUnidadeVazaoInstan;
	private Boolean valorFixoValorContrato;
	private Boolean selApuracaoParadaNaoProgramadaC;
	private Boolean obgApuracaoParadaNaoProgramadaC;
	private Boolean valorFixoPrazoVigencia;
	private Boolean valorFixoIncentivosComerciais;
	private Boolean valorFixoIncetivosInfraestrutura;
	
	
    private Integer versao;
	private Integer anoContrato;
	private Integer indexLista = -1;
	
	private Integer[] indicePeriodicidadeTakeOrPay;
	private Integer[] indicePeriodicidadeShipOrPay;
	private Integer[] indicePenalidadeRetiradaMaiorMenor;
	private Integer[] indicePenalidadeRetiradaMaiorMenorModalidade;


	
	private Long chavePrimaria;
	private Long idModeloContrato;
	private Long idPontoConsumoEncerrar;
	private Long idPontoConsumo;
	private Long idPontoConsumoLista;
	private Long chavePrimariaContratoMigrar;
	private Long idTipoContrato;
	private Long chavePrimariaPrincipal;
	private Long idPeriodicidadePenalidadeC;
	private Long idPenalidadeC;
	private Long chavePrimariaPai;
	private Long idItemFaturamento;		
	private Long idCliente;
	private Long idImovel;
	private Long idModalidadeContrato;
	private Long idPeriodicidadePenalidade;
	private Long idPenalidade;
	private Long chavePrimariaAnexo;
	private Long idPontoConsumoReferenciaCopia;
	
	private Long[] chavesPrimarias;
	private Long[] chavesPrimariasPontoConsumo;
	private Long[] chavesPrimariasPontoConsumoVO;
	private Long[] chavePontoTransferirSaldo;
	private Long[] idsPontosConsumo;
	private Long[] itensFaturamento;
	private Long[] listaDiasDisponiveis;
	private Long[] listaDiasSelecionados;
	private Long[] idsLocalAmostragemAssociados;
	private Long[] idsIntervaloAmostragemAssociados;
	private Long[] idsModalidadeContrato;
	private Long[] arrayIdPontoConsumo;
	private MultipartFile[] anexo;
	
	private Boolean isTelaChamado;
	
	private Contrato contrato;
	
	public String getRenovacaoAutomatica() {
		return renovacaoAutomatica;
	}
	public void setRenovacaoAutomatica(String renovacaoAutomatica) {
		this.renovacaoAutomatica = renovacaoAutomatica;
	}
	public String getIndicadorAnoContratual() {
		return indicadorAnoContratual;
	}
	public void setIndicadorAnoContratual(String indicadorAnoContratual) {
		this.indicadorAnoContratual = indicadorAnoContratual;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricaoAbreviada() {
		return descricaoAbreviada;
	}
	public void setDescricaoAbreviada(String descricaoAbreviada) {
		this.descricaoAbreviada = descricaoAbreviada;
	}
	public String getFluxoMigracao() {
		return fluxoMigracao;
	}
	public void setFluxoMigracao(String fluxoMigracao) {
		this.fluxoMigracao = fluxoMigracao;
	}
	public String getSaldoQNR() {
		return saldoQNR;
	}
	public void setSaldoQNR(String saldoQNR) {
		this.saldoQNR = saldoQNR;
	}
	public String getSaldoQPNR() {
		return saldoQPNR;
	}
	public void setSaldoQPNR(String saldoQPNR) {
		this.saldoQPNR = saldoQPNR;
	}
	public String getValorTransferidoQNR() {
		return valorTransferidoQNR;
	}
	public void setValorTransferidoQNR(String valorTransferidoQNR) {
		this.valorTransferidoQNR = valorTransferidoQNR;
	}
	public String getValorTransferidoQPNR() {
		return valorTransferidoQPNR;
	}
	public void setValorTransferidoQPNR(String valorTransferidoQPNR) {
		this.valorTransferidoQPNR = valorTransferidoQPNR;
	}
	public String getListaIdsPontoConsumoRemovidos() {
		return listaIdsPontoConsumoRemovidos;
	}
	public void setListaIdsPontoConsumoRemovidos(String listaIdsPontoConsumoRemovidos) {
		this.listaIdsPontoConsumoRemovidos = listaIdsPontoConsumoRemovidos;
	}
	public String getListaIdsPontoConsumoRemovidosAgrupados() {
		return listaIdsPontoConsumoRemovidosAgrupados;
	}
	public void setListaIdsPontoConsumoRemovidosAgrupados(String listaIdsPontoConsumoRemovidosAgrupados) {
		this.listaIdsPontoConsumoRemovidosAgrupados = listaIdsPontoConsumoRemovidosAgrupados;
	}
	public String getPontosSemRecisao() {
		return pontosSemRecisao;
	}
	public void setPontosSemRecisao(String pontosSemRecisao) {
		this.pontosSemRecisao = pontosSemRecisao;
	}
	public String getNumeroContratoComAditivo() {
		return numeroContratoComAditivo;
	}
	public void setNumeroContratoComAditivo(String numeroContratoComAditivo) {
		this.numeroContratoComAditivo = numeroContratoComAditivo;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNumeroContratoMigrar() {
		return numeroContratoMigrar;
	}
	public void setNumeroContratoMigrar(String numeroContratoMigrar) {
		this.numeroContratoMigrar = numeroContratoMigrar;
	}
	public String getNumeroAnteriorContrato() {
		return numeroAnteriorContrato;
	}
	public void setNumeroAnteriorContrato(String numeroAnteriorContrato) {
		this.numeroAnteriorContrato = numeroAnteriorContrato;
	}
	public String getDataAssinatura() {
		return dataAssinatura;
	}
	public void setDataAssinatura(String dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}
	public String getSituacaoContrato() {
		return situacaoContrato;
	}
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}
	public String getNumeroAditivo() {
		return numeroAditivo;
	}
	public void setNumeroAditivo(String numeroAditivo) {
		this.numeroAditivo = numeroAditivo;
	}
	public String getDataAditivo() {
		return dataAditivo;
	}
	public void setDataAditivo(String dataAditivo) {
		this.dataAditivo = dataAditivo;
	}
	public String getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public String getDescricaoAditivo() {
		return descricaoAditivo;
	}
	public void setDescricaoAditivo(String descricaoAditivo) {
		this.descricaoAditivo = descricaoAditivo;
	}
	public String getValorContrato() {
		return valorContrato;
	}
	public void setValorContrato(String valorContrato) {
		this.valorContrato = valorContrato;
	}
	public String getIndicadorMultaAtraso() {
		return indicadorMultaAtraso;
	}
	public void setIndicadorMultaAtraso(String indicadorMultaAtraso) {
		this.indicadorMultaAtraso = indicadorMultaAtraso;
	}
	public String getIndicadorJurosMora() {
		return indicadorJurosMora;
	}
	public void setIndicadorJurosMora(String indicadorJurosMora) {
		this.indicadorJurosMora = indicadorJurosMora;
	}
	public String getIndiceCorrecaoMonetaria() {
		return indiceCorrecaoMonetaria;
	}
	public void setIndiceCorrecaoMonetaria(String indiceCorrecaoMonetaria) {
		this.indiceCorrecaoMonetaria = indiceCorrecaoMonetaria;
	}
	public String getNumeroProposta() {
		return numeroProposta;
	}
	public void setNumeroProposta(String numeroProposta) {
		this.numeroProposta = numeroProposta;
	}
	public String getIdProposta() {
		return idProposta;
	}
	public void setIdProposta(String idProposta) {
		this.idProposta = idProposta;
	}
	public String getTarifaGNAplicada() {
		return tarifaGNAplicada;
	}
	public void setTarifaGNAplicada(String tarifaGNAplicada) {
		this.tarifaGNAplicada = tarifaGNAplicada;
	}
	public String getGastoEstimadoGNMes() {
		return gastoEstimadoGNMes;
	}
	public void setGastoEstimadoGNMes(String gastoEstimadoGNMes) {
		this.gastoEstimadoGNMes = gastoEstimadoGNMes;
	}
	public String getEconomiaEstimadaGNMes() {
		return economiaEstimadaGNMes;
	}
	public void setEconomiaEstimadaGNMes(String economiaEstimadaGNMes) {
		this.economiaEstimadaGNMes = economiaEstimadaGNMes;
	}
	public String getEconomiaEstimadaGNAno() {
		return economiaEstimadaGNAno;
	}
	public void setEconomiaEstimadaGNAno(String economiaEstimadaGNAno) {
		this.economiaEstimadaGNAno = economiaEstimadaGNAno;
	}
	public String getDescontoEfetivoEconomia() {
		return descontoEfetivoEconomia;
	}
	public void setDescontoEfetivoEconomia(String descontoEfetivoEconomia) {
		this.descontoEfetivoEconomia = descontoEfetivoEconomia;
	}
	public String getDiaVencFinanciamento() {
		return diaVencFinanciamento;
	}
	public void setDiaVencFinanciamento(String diaVencFinanciamento) {
		this.diaVencFinanciamento = diaVencFinanciamento;
	}
	public String getDescGarantiaFinanc() {
		return descGarantiaFinanc;
	}
	public void setDescGarantiaFinanc(String descGarantiaFinanc) {
		this.descGarantiaFinanc = descGarantiaFinanc;
	}
	public String getNumeroEmpenho() {
		return numeroEmpenho;
	}
	public void setNumeroEmpenho(String numeroEmpenho) {
		this.numeroEmpenho = numeroEmpenho;
	}
	public String getClienteAssinatura() {
		return clienteAssinatura;
	}
	public void setClienteAssinatura(String clienteAssinatura) {
		this.clienteAssinatura = clienteAssinatura;
	}
	public String getNumDiasRenoAutoContrato() {
		return numDiasRenoAutoContrato;
	}
	public void setNumDiasRenoAutoContrato(String numDiasRenoAutoContrato) {
		this.numDiasRenoAutoContrato = numDiasRenoAutoContrato;
	}
	public String getExigeAprovacao() {
		return exigeAprovacao;
	}
	public void setExigeAprovacao(String exigeAprovacao) {
		this.exigeAprovacao = exigeAprovacao;
	}
	public String getEnderecoPadrao() {
		return enderecoPadrao;
	}
	public void setEnderecoPadrao(String enderecoPadrao) {
		this.enderecoPadrao = enderecoPadrao;
	}
	public String getFaturamentoAgrupamento() {
		return faturamentoAgrupamento;
	}
	public void setFaturamentoAgrupamento(String faturamentoAgrupamento) {
		this.faturamentoAgrupamento = faturamentoAgrupamento;
	}
	public String getParticipaECartas() {
		return participaECartas;
	}
	public void setParticipaECartas(String participaECartas) {
		this.participaECartas = participaECartas;
	}
	public String getTipoPeriodicidadePenalidade() {
		return tipoPeriodicidadePenalidade;
	}
	public void setTipoPeriodicidadePenalidade(String tipoPeriodicidadePenalidade) {
		this.tipoPeriodicidadePenalidade = tipoPeriodicidadePenalidade;
	}
	public String getEmissaoFaturaAgrupada() {
		return emissaoFaturaAgrupada;
	}
	public void setEmissaoFaturaAgrupada(String emissaoFaturaAgrupada) {
		this.emissaoFaturaAgrupada = emissaoFaturaAgrupada;
	}
	public String getDataVencObrigacoesContratuais() {
		return dataVencObrigacoesContratuais;
	}
	public void setDataVencObrigacoesContratuais(String dataVencObrigacoesContratuais) {
		this.dataVencObrigacoesContratuais = dataVencObrigacoesContratuais;
	}
	public String getPropostaAprovada() {
		return propostaAprovada;
	}
	public void setPropostaAprovada(String propostaAprovada) {
		this.propostaAprovada = propostaAprovada;
	}
	public String getTempoAntecedenciaRenovacao() {
		return tempoAntecedenciaRenovacao;
	}
	public void setTempoAntecedenciaRenovacao(String tempoAntecedenciaRenovacao) {
		this.tempoAntecedenciaRenovacao = tempoAntecedenciaRenovacao;
	}
	public String getTempoAntecRevisaoGarantias() {
		return tempoAntecRevisaoGarantias;
	}
	public void setTempoAntecRevisaoGarantias(String tempoAntecRevisaoGarantias) {
		this.tempoAntecRevisaoGarantias = tempoAntecRevisaoGarantias;
	}
	public String getPeriodicidadeReavGarantias() {
		return periodicidadeReavGarantias;
	}
	public void setPeriodicidadeReavGarantias(String periodicidadeReavGarantias) {
		this.periodicidadeReavGarantias = periodicidadeReavGarantias;
	}
	public String getValorParticipacaoCliente() {
		return valorParticipacaoCliente;
	}
	public void setValorParticipacaoCliente(String valorParticipacaoCliente) {
		this.valorParticipacaoCliente = valorParticipacaoCliente;
	}
	public String getQtdParcelasFinanciamento() {
		return qtdParcelasFinanciamento;
	}
	public void setQtdParcelasFinanciamento(String qtdParcelasFinanciamento) {
		this.qtdParcelasFinanciamento = qtdParcelasFinanciamento;
	}
	public String getPercentualJurosFinanciamento() {
		return percentualJurosFinanciamento;
	}
	public void setPercentualJurosFinanciamento(String percentualJurosFinanciamento) {
		this.percentualJurosFinanciamento = percentualJurosFinanciamento;
	}
	public String getSistemaAmortizacao() {
		return sistemaAmortizacao;
	}
	public void setSistemaAmortizacao(String sistemaAmortizacao) {
		this.sistemaAmortizacao = sistemaAmortizacao;
	}
	public String getPercentualTarifaDoP() {
		return percentualTarifaDoP;
	}
	public void setPercentualTarifaDoP(String percentualTarifaDoP) {
		this.percentualTarifaDoP = percentualTarifaDoP;
	}
	public String getPercentualMulta() {
		return percentualMulta;
	}
	public void setPercentualMulta(String percentualMulta) {
		this.percentualMulta = percentualMulta;
	}
	public String getPercentualJurosMora() {
		return percentualJurosMora;
	}
	public void setPercentualJurosMora(String percentualJurosMora) {
		this.percentualJurosMora = percentualJurosMora;
	}
	public String getTipoAgrupamento() {
		return tipoAgrupamento;
	}
	public void setTipoAgrupamento(String tipoAgrupamento) {
		this.tipoAgrupamento = tipoAgrupamento;
	}
	public String getContratoCompra() {
		return contratoCompra;
	}
	public void setContratoCompra(String contratoCompra) {
		this.contratoCompra = contratoCompra;
	}
	public String getPercentualReferenciaSobreDem() {
		return percentualReferenciaSobreDem;
	}
	public void setPercentualReferenciaSobreDem(String percentualReferenciaSobreDem) {
		this.percentualReferenciaSobreDem = percentualReferenciaSobreDem;
	}
	public String getPercentualReferenciaSobDem() {
		return percentualReferenciaSobDem;
	}
	public void setPercentualReferenciaSobDem(String percentualReferenciaSobDem) {
		this.percentualReferenciaSobDem = percentualReferenciaSobDem;
	}
	public String getPercentualSobreTariGas() {
		return percentualSobreTariGas;
	}
	public void setPercentualSobreTariGas(String percentualSobreTariGas) {
		this.percentualSobreTariGas = percentualSobreTariGas;
	}
	public String getBaseApuracaoRetMaior() {
		return baseApuracaoRetMaior;
	}
	public void setBaseApuracaoRetMaior(String baseApuracaoRetMaior) {
		this.baseApuracaoRetMaior = baseApuracaoRetMaior;
	}
	public String getBaseApuracaoRetMenor() {
		return baseApuracaoRetMenor;
	}
	public void setBaseApuracaoRetMenor(String baseApuracaoRetMenor) {
		this.baseApuracaoRetMenor = baseApuracaoRetMenor;
	}
	public String getMultaRecisoria() {
		return multaRecisoria;
	}
	public void setMultaRecisoria(String multaRecisoria) {
		this.multaRecisoria = multaRecisoria;
	}
	public String getValorInvestimento() {
		return valorInvestimento;
	}
	public void setValorInvestimento(String valorInvestimento) {
		this.valorInvestimento = valorInvestimento;
	}
	public String getDataInvestimento() {
		return dataInvestimento;
	}
	public void setDataInvestimento(String dataInvestimento) {
		this.dataInvestimento = dataInvestimento;
	}
	public String getDataRecisao() {
		return dataRecisao;
	}
	public void setDataRecisao(String dataRecisao) {
		this.dataRecisao = dataRecisao;
	}
	public String getDescricaoContrato() {
		return descricaoContrato;
	}
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}
	public String getVolumeReferencia() {
		return volumeReferencia;
	}
	public void setVolumeReferencia(String volumeReferencia) {
		this.volumeReferencia = volumeReferencia;
	}
	public String getPeriodicidadeTakeOrPayC() {
		return periodicidadeTakeOrPayC;
	}
	public void setPeriodicidadeTakeOrPayC(String periodicidadeTakeOrPayC) {
		this.periodicidadeTakeOrPayC = periodicidadeTakeOrPayC;
	}
	public String getConsumoReferenciaTakeOrPayC() {
		return consumoReferenciaTakeOrPayC;
	}
	public void setConsumoReferenciaTakeOrPayC(String consumoReferenciaTakeOrPayC) {
		this.consumoReferenciaTakeOrPayC = consumoReferenciaTakeOrPayC;
	}
	public String getMargemVariacaoTakeOrPayC() {
		return margemVariacaoTakeOrPayC;
	}
	public void setMargemVariacaoTakeOrPayC(String margemVariacaoTakeOrPayC) {
		this.margemVariacaoTakeOrPayC = margemVariacaoTakeOrPayC;
	}
	public String getRecuperacaoAutoTakeOrPayC() {
		return recuperacaoAutoTakeOrPayC;
	}
	public void setRecuperacaoAutoTakeOrPayC(String recuperacaoAutoTakeOrPayC) {
		this.recuperacaoAutoTakeOrPayC = recuperacaoAutoTakeOrPayC;
	}
	public String getReferenciaQFParadaProgramadaC() {
		return referenciaQFParadaProgramadaC;
	}
	public void setReferenciaQFParadaProgramadaC(String referenciaQFParadaProgramadaC) {
		this.referenciaQFParadaProgramadaC = referenciaQFParadaProgramadaC;
	}
	public String getPercentualMargemVariacaoC() {
		return percentualMargemVariacaoC;
	}
	public void setPercentualMargemVariacaoC(String percentualMargemVariacaoC) {
		this.percentualMargemVariacaoC = percentualMargemVariacaoC;
	}
	public String getConsumoReferenciaC() {
		return consumoReferenciaC;
	}
	public void setConsumoReferenciaC(String consumoReferenciaC) {
		this.consumoReferenciaC = consumoReferenciaC;
	}
	public String getDataInicioVigenciaC() {
		return dataInicioVigenciaC;
	}
	public void setDataInicioVigenciaC(String dataInicioVigenciaC) {
		this.dataInicioVigenciaC = dataInicioVigenciaC;
	}
	public String getDataFimVigenciaC() {
		return dataFimVigenciaC;
	}
	public void setDataFimVigenciaC(String dataFimVigenciaC) {
		this.dataFimVigenciaC = dataFimVigenciaC;
	}
	public String getPercentualNaoRecuperavelC() {
		return percentualNaoRecuperavelC;
	}
	public void setPercentualNaoRecuperavelC(String percentualNaoRecuperavelC) {
		this.percentualNaoRecuperavelC = percentualNaoRecuperavelC;
	}
	public String getConsideraParadaProgramadaC() {
		return consideraParadaProgramadaC;
	}
	public void setConsideraParadaProgramadaC(String consideraParadaProgramadaC) {
		this.consideraParadaProgramadaC = consideraParadaProgramadaC;
	}
	public String getConsideraFalhaFornecimentoC() {
		return consideraFalhaFornecimentoC;
	}
	public void setConsideraFalhaFornecimentoC(String consideraFalhaFornecimentoC) {
		this.consideraFalhaFornecimentoC = consideraFalhaFornecimentoC;
	}
	public String getConsideraCasoFortuitoC() {
		return consideraCasoFortuitoC;
	}
	public void setConsideraCasoFortuitoC(String consideraCasoFortuitoC) {
		this.consideraCasoFortuitoC = consideraCasoFortuitoC;
	}
	public String getRecuperavelC() {
		return recuperavelC;
	}
	public void setRecuperavelC(String recuperavelC) {
		this.recuperavelC = recuperavelC;
	}
	public String getTipoApuracaoC() {
		return tipoApuracaoC;
	}
	public void setTipoApuracaoC(String tipoApuracaoC) {
		this.tipoApuracaoC = tipoApuracaoC;
	}
	public String getPrecoCobrancaC() {
		return precoCobrancaC;
	}
	public void setPrecoCobrancaC(String precoCobrancaC) {
		this.precoCobrancaC = precoCobrancaC;
	}
	public String getApuracaoParadaProgramadaC() {
		return apuracaoParadaProgramadaC;
	}
	public void setApuracaoParadaProgramadaC(String apuracaoParadaProgramadaC) {
		this.apuracaoParadaProgramadaC = apuracaoParadaProgramadaC;
	}
	public String getApuracaoCasoFortuitoC() {
		return apuracaoCasoFortuitoC;
	}
	public void setApuracaoCasoFortuitoC(String apuracaoCasoFortuitoC) {
		this.apuracaoCasoFortuitoC = apuracaoCasoFortuitoC;
	}
	public String getApuracaoFalhaFornecimentoC() {
		return apuracaoFalhaFornecimentoC;
	}
	public void setApuracaoFalhaFornecimentoC(String apuracaoFalhaFornecimentoC) {
		this.apuracaoFalhaFornecimentoC = apuracaoFalhaFornecimentoC;
	}
	public String getTipoAgrupamentoContrato() {
		return tipoAgrupamentoContrato;
	}
	public void setTipoAgrupamentoContrato(String tipoAgrupamentoContrato) {
		this.tipoAgrupamentoContrato = tipoAgrupamentoContrato;
	}
	public String getPenalidadeRetMaiorMenor() {
		return penalidadeRetMaiorMenor;
	}
	public void setPenalidadeRetMaiorMenor(String penalidadeRetMaiorMenor) {
		this.penalidadeRetMaiorMenor = penalidadeRetMaiorMenor;
	}
	public String getPenalidadeRetMaiorMenorM() {
		return penalidadeRetMaiorMenorM;
	}
	public void setPenalidadeRetMaiorMenorM(String penalidadeRetMaiorMenorM) {
		this.penalidadeRetMaiorMenorM = penalidadeRetMaiorMenorM;
	}
	public String getPeriodicidadeRetMaiorMenor() {
		return periodicidadeRetMaiorMenor;
	}
	public void setPeriodicidadeRetMaiorMenor(String periodicidadeRetMaiorMenor) {
		this.periodicidadeRetMaiorMenor = periodicidadeRetMaiorMenor;
	}
	public String getPeriodicidadeRetMaiorMenorM() {
		return periodicidadeRetMaiorMenorM;
	}
	public void setPeriodicidadeRetMaiorMenorM(String periodicidadeRetMaiorMenorM) {
		this.periodicidadeRetMaiorMenorM = periodicidadeRetMaiorMenorM;
	}
	public String getBaseApuracaoRetMaiorMenor() {
		return baseApuracaoRetMaiorMenor;
	}
	public void setBaseApuracaoRetMaiorMenor(String baseApuracaoRetMaiorMenor) {
		this.baseApuracaoRetMaiorMenor = baseApuracaoRetMaiorMenor;
	}
	public String getBaseApuracaoRetMaiorMenorM() {
		return baseApuracaoRetMaiorMenorM;
	}
	public void setBaseApuracaoRetMaiorMenorM(String baseApuracaoRetMaiorMenorM) {
		this.baseApuracaoRetMaiorMenorM = baseApuracaoRetMaiorMenorM;
	}
	public String getDataIniVigRetirMaiorMenor() {
		return dataIniVigRetirMaiorMenor;
	}
	public void setDataIniVigRetirMaiorMenor(String dataIniVigRetirMaiorMenor) {
		this.dataIniVigRetirMaiorMenor = dataIniVigRetirMaiorMenor;
	}
	public String getDataIniVigRetirMaiorMenorM() {
		return dataIniVigRetirMaiorMenorM;
	}
	public void setDataIniVigRetirMaiorMenorM(String dataIniVigRetirMaiorMenorM) {
		this.dataIniVigRetirMaiorMenorM = dataIniVigRetirMaiorMenorM;
	}
	public String getDataFimVigRetirMaiorMenor() {
		return dataFimVigRetirMaiorMenor;
	}
	public void setDataFimVigRetirMaiorMenor(String dataFimVigRetirMaiorMenor) {
		this.dataFimVigRetirMaiorMenor = dataFimVigRetirMaiorMenor;
	}
	public String getDataFimVigRetirMaiorMenorM() {
		return dataFimVigRetirMaiorMenorM;
	}
	public void setDataFimVigRetirMaiorMenorM(String dataFimVigRetirMaiorMenorM) {
		this.dataFimVigRetirMaiorMenorM = dataFimVigRetirMaiorMenorM;
	}
	public String getPrecoCobrancaRetirMaiorMenor() {
		return precoCobrancaRetirMaiorMenor;
	}
	public void setPrecoCobrancaRetirMaiorMenor(String precoCobrancaRetirMaiorMenor) {
		this.precoCobrancaRetirMaiorMenor = precoCobrancaRetirMaiorMenor;
	}
	public String getPrecoCobrancaRetirMaiorMenorM() {
		return precoCobrancaRetirMaiorMenorM;
	}
	public void setPrecoCobrancaRetirMaiorMenorM(String precoCobrancaRetirMaiorMenorM) {
		this.precoCobrancaRetirMaiorMenorM = precoCobrancaRetirMaiorMenorM;
	}
	public String getTipoApuracaoRetirMaiorMenor() {
		return tipoApuracaoRetirMaiorMenor;
	}
	public void setTipoApuracaoRetirMaiorMenor(String tipoApuracaoRetirMaiorMenor) {
		this.tipoApuracaoRetirMaiorMenor = tipoApuracaoRetirMaiorMenor;
	}
	public String getTipoApuracaoRetirMaiorMenorM() {
		return tipoApuracaoRetirMaiorMenorM;
	}
	public void setTipoApuracaoRetirMaiorMenorM(String tipoApuracaoRetirMaiorMenorM) {
		this.tipoApuracaoRetirMaiorMenorM = tipoApuracaoRetirMaiorMenorM;
	}
	public String getPercentualCobRetMaiorMenor() {
		return percentualCobRetMaiorMenor;
	}
	public void setPercentualCobRetMaiorMenor(String percentualCobRetMaiorMenor) {
		this.percentualCobRetMaiorMenor = percentualCobRetMaiorMenor;
	}
	public String getPercentualCobRetMaiorMenorM() {
		return percentualCobRetMaiorMenorM;
	}
	public void setPercentualCobRetMaiorMenorM(String percentualCobRetMaiorMenorM) {
		this.percentualCobRetMaiorMenorM = percentualCobRetMaiorMenorM;
	}
	public String getPercentualCobIntRetMaiorMenor() {
		return percentualCobIntRetMaiorMenor;
	}
	public void setPercentualCobIntRetMaiorMenor(String percentualCobIntRetMaiorMenor) {
		this.percentualCobIntRetMaiorMenor = percentualCobIntRetMaiorMenor;
	}
	public String getPercentualCobIntRetMaiorMenorM() {
		return percentualCobIntRetMaiorMenorM;
	}
	public void setPercentualCobIntRetMaiorMenorM(String percentualCobIntRetMaiorMenorM) {
		this.percentualCobIntRetMaiorMenorM = percentualCobIntRetMaiorMenorM;
	}
	public String getConsumoReferenciaRetMaiorMenor() {
		return consumoReferenciaRetMaiorMenor;
	}
	public void setConsumoReferenciaRetMaiorMenor(String consumoReferenciaRetMaiorMenor) {
		this.consumoReferenciaRetMaiorMenor = consumoReferenciaRetMaiorMenor;
	}
	public String getConsumoReferRetMaiorMenorM() {
		return consumoReferRetMaiorMenorM;
	}
	public void setConsumoReferRetMaiorMenorM(String consumoReferRetMaiorMenorM) {
		this.consumoReferRetMaiorMenorM = consumoReferRetMaiorMenorM;
	}
	public String getPercentualRetMaiorMenor() {
		return percentualRetMaiorMenor;
	}
	public void setPercentualRetMaiorMenor(String percentualRetMaiorMenor) {
		this.percentualRetMaiorMenor = percentualRetMaiorMenor;
	}
	public String getPercentualRetMaiorMenorM() {
		return percentualRetMaiorMenorM;
	}
	public void setPercentualRetMaiorMenorM(String percentualRetMaiorMenorM) {
		this.percentualRetMaiorMenorM = percentualRetMaiorMenorM;
	}
	public String getDataInicioRetiradaQPNRC() {
		return dataInicioRetiradaQPNRC;
	}
	public void setDataInicioRetiradaQPNRC(String dataInicioRetiradaQPNRC) {
		this.dataInicioRetiradaQPNRC = dataInicioRetiradaQPNRC;
	}
	public String getDataFimRetiradaQPNRC() {
		return dataFimRetiradaQPNRC;
	}
	public void setDataFimRetiradaQPNRC(String dataFimRetiradaQPNRC) {
		this.dataFimRetiradaQPNRC = dataFimRetiradaQPNRC;
	}
	public String getPercDuranteRetiradaQPNRC() {
		return percDuranteRetiradaQPNRC;
	}
	public void setPercDuranteRetiradaQPNRC(String percDuranteRetiradaQPNRC) {
		this.percDuranteRetiradaQPNRC = percDuranteRetiradaQPNRC;
	}
	public String getPercMinDuranteRetiradaQPNRC() {
		return percMinDuranteRetiradaQPNRC;
	}
	public void setPercMinDuranteRetiradaQPNRC(String percMinDuranteRetiradaQPNRC) {
		this.percMinDuranteRetiradaQPNRC = percMinDuranteRetiradaQPNRC;
	}
	public String getPercFimRetiradaQPNRC() {
		return percFimRetiradaQPNRC;
	}
	public void setPercFimRetiradaQPNRC(String percFimRetiradaQPNRC) {
		this.percFimRetiradaQPNRC = percFimRetiradaQPNRC;
	}
	public String getTempoValidadeRetiradaQPNRC() {
		return tempoValidadeRetiradaQPNRC;
	}
	public void setTempoValidadeRetiradaQPNRC(String tempoValidadeRetiradaQPNRC) {
		this.tempoValidadeRetiradaQPNRC = tempoValidadeRetiradaQPNRC;
	}
	public String getIndicadorImposto() {
		return indicadorImposto;
	}
	public void setIndicadorImposto(String indicadorImposto) {
		this.indicadorImposto = indicadorImposto;
	}
	public String getIndicadorImpostoM() {
		return indicadorImpostoM;
	}
	public void setIndicadorImpostoM(String indicadorImpostoM) {
		this.indicadorImpostoM = indicadorImpostoM;
	}
	public String getPeriodoTesteDataInicio() {
		return periodoTesteDataInicio;
	}
	public void setPeriodoTesteDataInicio(String periodoTesteDataInicio) {
		this.periodoTesteDataInicio = periodoTesteDataInicio;
	}
	public String getPeriodoTesteDateFim() {
		return periodoTesteDateFim;
	}
	public void setPeriodoTesteDateFim(String periodoTesteDateFim) {
		this.periodoTesteDateFim = periodoTesteDateFim;
	}
	public String getVolumeTeste() {
		return volumeTeste;
	}
	public void setVolumeTeste(String volumeTeste) {
		this.volumeTeste = volumeTeste;
	}
	public String getPrazoTeste() {
		return prazoTeste;
	}
	public void setPrazoTeste(String prazoTeste) {
		this.prazoTeste = prazoTeste;
	}
	public String getFaxDDD() {
		return faxDDD;
	}
	public void setFaxDDD(String faxDDD) {
		this.faxDDD = faxDDD;
	}
	public String getFaxNumero() {
		return faxNumero;
	}
	public void setFaxNumero(String faxNumero) {
		this.faxNumero = faxNumero;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumeroDiasGarantia() {
		return numeroDiasGarantia;
	}
	public void setNumeroDiasGarantia(String numeroDiasGarantia) {
		this.numeroDiasGarantia = numeroDiasGarantia;
	}
	public String getInicioGarantiaConversao() {
		return inicioGarantiaConversao;
	}
	public void setInicioGarantiaConversao(String inicioGarantiaConversao) {
		this.inicioGarantiaConversao = inicioGarantiaConversao;
	}
	public String getFimGarantiaConversao() {
		return fimGarantiaConversao;
	}
	public void setFimGarantiaConversao(String fimGarantiaConversao) {
		this.fimGarantiaConversao = fimGarantiaConversao;
	}
	public String getVazaoInstantanea() {
		return vazaoInstantanea;
	}
	public void setVazaoInstantanea(String vazaoInstantanea) {
		this.vazaoInstantanea = vazaoInstantanea;
	}
	public String getUnidadeVazaoInstan() {
		return unidadeVazaoInstan;
	}
	public void setUnidadeVazaoInstan(String unidadeVazaoInstan) {
		this.unidadeVazaoInstan = unidadeVazaoInstan;
	}
	public String getVazaoInstantaneaMaxima() {
		return vazaoInstantaneaMaxima;
	}
	public void setVazaoInstantaneaMaxima(String vazaoInstantaneaMaxima) {
		this.vazaoInstantaneaMaxima = vazaoInstantaneaMaxima;
	}
	public String getUnidadeVazaoInstanMaxima() {
		return unidadeVazaoInstanMaxima;
	}
	public void setUnidadeVazaoInstanMaxima(String unidadeVazaoInstanMaxima) {
		this.unidadeVazaoInstanMaxima = unidadeVazaoInstanMaxima;
	}
	public String getVazaoInstantaneaMinima() {
		return vazaoInstantaneaMinima;
	}
	public void setVazaoInstantaneaMinima(String vazaoInstantaneaMinima) {
		this.vazaoInstantaneaMinima = vazaoInstantaneaMinima;
	}
	public String getUnidadeVazaoInstanMinima() {
		return unidadeVazaoInstanMinima;
	}
	public void setUnidadeVazaoInstanMinima(String unidadeVazaoInstanMinima) {
		this.unidadeVazaoInstanMinima = unidadeVazaoInstanMinima;
	}
	public String getFaixaPressaoFornecimento() {
		return faixaPressaoFornecimento;
	}
	public void setFaixaPressaoFornecimento(String faixaPressaoFornecimento) {
		this.faixaPressaoFornecimento = faixaPressaoFornecimento;
	}
	public String getPressaoMinimaFornecimento() {
		return pressaoMinimaFornecimento;
	}
	public void setPressaoMinimaFornecimento(String pressaoMinimaFornecimento) {
		this.pressaoMinimaFornecimento = pressaoMinimaFornecimento;
	}
	public String getPressaoColetada() {
		return pressaoColetada;
	}
	public void setPressaoColetada(String pressaoColetada) {
		this.pressaoColetada = pressaoColetada;
	}
	public String getUnidadePressaoColetada() {
		return unidadePressaoColetada;
	}
	public void setUnidadePressaoColetada(String unidadePressaoColetada) {
		this.unidadePressaoColetada = unidadePressaoColetada;
	}
	public String getUnidadePressaoMinimaFornec() {
		return unidadePressaoMinimaFornec;
	}
	public void setUnidadePressaoMinimaFornec(String unidadePressaoMinimaFornec) {
		this.unidadePressaoMinimaFornec = unidadePressaoMinimaFornec;
	}
	public String getPressaoMaximaFornecimento() {
		return pressaoMaximaFornecimento;
	}
	public void setPressaoMaximaFornecimento(String pressaoMaximaFornecimento) {
		this.pressaoMaximaFornecimento = pressaoMaximaFornecimento;
	}
	public String getUnidadePressaoMaximaFornec() {
		return unidadePressaoMaximaFornec;
	}
	public void setUnidadePressaoMaximaFornec(String unidadePressaoMaximaFornec) {
		this.unidadePressaoMaximaFornec = unidadePressaoMaximaFornec;
	}
	public String getPressaoLimiteFornecimento() {
		return pressaoLimiteFornecimento;
	}
	public void setPressaoLimiteFornecimento(String pressaoLimiteFornecimento) {
		this.pressaoLimiteFornecimento = pressaoLimiteFornecimento;
	}
	public String getUnidadePressaoLimiteFornec() {
		return unidadePressaoLimiteFornec;
	}
	public void setUnidadePressaoLimiteFornec(String unidadePressaoLimiteFornec) {
		this.unidadePressaoLimiteFornec = unidadePressaoLimiteFornec;
	}
	public String getNumAnosCtrlParadaCliente() {
		return numAnosCtrlParadaCliente;
	}
	public void setNumAnosCtrlParadaCliente(String numAnosCtrlParadaCliente) {
		this.numAnosCtrlParadaCliente = numAnosCtrlParadaCliente;
	}
	public String getMaxTotalParadasCliente() {
		return maxTotalParadasCliente;
	}
	public void setMaxTotalParadasCliente(String maxTotalParadasCliente) {
		this.maxTotalParadasCliente = maxTotalParadasCliente;
	}
	public String getMaxAnualParadasCliente() {
		return maxAnualParadasCliente;
	}
	public void setMaxAnualParadasCliente(String maxAnualParadasCliente) {
		this.maxAnualParadasCliente = maxAnualParadasCliente;
	}
	public String getNumDiasProgrParadaCliente() {
		return numDiasProgrParadaCliente;
	}
	public void setNumDiasProgrParadaCliente(String numDiasProgrParadaCliente) {
		this.numDiasProgrParadaCliente = numDiasProgrParadaCliente;
	}
	public String getNumDiasConsecParadaCliente() {
		return numDiasConsecParadaCliente;
	}
	public void setNumDiasConsecParadaCliente(String numDiasConsecParadaCliente) {
		this.numDiasConsecParadaCliente = numDiasConsecParadaCliente;
	}
	public String getNumAnosCtrlParadaCDL() {
		return numAnosCtrlParadaCDL;
	}
	public void setNumAnosCtrlParadaCDL(String numAnosCtrlParadaCDL) {
		this.numAnosCtrlParadaCDL = numAnosCtrlParadaCDL;
	}
	public String getMaxTotalParadasCDL() {
		return maxTotalParadasCDL;
	}
	public void setMaxTotalParadasCDL(String maxTotalParadasCDL) {
		this.maxTotalParadasCDL = maxTotalParadasCDL;
	}
	public String getMaxAnualParadasCDL() {
		return maxAnualParadasCDL;
	}
	public void setMaxAnualParadasCDL(String maxAnualParadasCDL) {
		this.maxAnualParadasCDL = maxAnualParadasCDL;
	}
	public String getNumDiasProgrParadaCDL() {
		return numDiasProgrParadaCDL;
	}
	public void setNumDiasProgrParadaCDL(String numDiasProgrParadaCDL) {
		this.numDiasProgrParadaCDL = numDiasProgrParadaCDL;
	}
	public String getNumDiasConsecParadaCDL() {
		return numDiasConsecParadaCDL;
	}
	public void setNumDiasConsecParadaCDL(String numDiasConsecParadaCDL) {
		this.numDiasConsecParadaCDL = numDiasConsecParadaCDL;
	}
	public String getTipoFaturamento() {
		return tipoFaturamento;
	}
	public void setTipoFaturamento(String tipoFaturamento) {
		this.tipoFaturamento = tipoFaturamento;
	}
	public String getTarifaConsumo() {
		return tarifaConsumo;
	}
	public void setTarifaConsumo(String tarifaConsumo) {
		this.tarifaConsumo = tarifaConsumo;
	}
	public String getFatPeriodicidade() {
		return fatPeriodicidade;
	}
	public void setFatPeriodicidade(String fatPeriodicidade) {
		this.fatPeriodicidade = fatPeriodicidade;
	}
	public String getFormaCobranca() {
		return formaCobranca;
	}
	public void setFormaCobranca(String formaCobranca) {
		this.formaCobranca = formaCobranca;
	}
	public String getArrecadadorConvenio() {
		return arrecadadorConvenio;
	}
	public void setArrecadadorConvenio(String arrecadadorConvenio) {
		this.arrecadadorConvenio = arrecadadorConvenio;
	}
	public String getDebitoAutomatico() {
		return debitoAutomatico;
	}
	public void setDebitoAutomatico(String debitoAutomatico) {
		this.debitoAutomatico = debitoAutomatico;
	}
	public String getArrecadadorConvenioDebitoAutomatico() {
		return arrecadadorConvenioDebitoAutomatico;
	}
	public void setArrecadadorConvenioDebitoAutomatico(String arrecadadorConvenioDebitoAutomatico) {
		this.arrecadadorConvenioDebitoAutomatico = arrecadadorConvenioDebitoAutomatico;
	}
	public String getDepositoIdentificado() {
		return depositoIdentificado;
	}
	public void setDepositoIdentificado(String depositoIdentificado) {
		this.depositoIdentificado = depositoIdentificado;
	}
	public String getEndFisEntFatCEP() {
		return endFisEntFatCEP;
	}
	public void setEndFisEntFatCEP(String endFisEntFatCEP) {
		this.endFisEntFatCEP = endFisEntFatCEP;
	}
	public String getEndFisEntFatNumero() {
		return endFisEntFatNumero;
	}
	public void setEndFisEntFatNumero(String endFisEntFatNumero) {
		this.endFisEntFatNumero = endFisEntFatNumero;
	}
	public String getEndFisEntFatComplemento() {
		return endFisEntFatComplemento;
	}
	public void setEndFisEntFatComplemento(String endFisEntFatComplemento) {
		this.endFisEntFatComplemento = endFisEntFatComplemento;
	}
	public String getEndFisEntFatEmail() {
		return endFisEntFatEmail;
	}
	public void setEndFisEntFatEmail(String endFisEntFatEmail) {
		this.endFisEntFatEmail = endFisEntFatEmail;
	}
	public String getItemFatura() {
		return itemFatura;
	}
	public void setItemFatura(String itemFatura) {
		this.itemFatura = itemFatura;
	}
	public String getPercminimoQDC() {
		return percminimoQDC;
	}
	public void setPercminimoQDC(String percminimoQDC) {
		this.percminimoQDC = percminimoQDC;
	}
	public String getDiaVencimentoItemFatura() {
		return diaVencimentoItemFatura;
	}
	public void setDiaVencimentoItemFatura(String diaVencimentoItemFatura) {
		this.diaVencimentoItemFatura = diaVencimentoItemFatura;
	}
	public String getDiaVencimentoItemFaturaValorFixo() {
		return diaVencimentoItemFaturaValorFixo;
	}
	public void setDiaVencimentoItemFaturaValorFixo(String diaVencimentoItemFaturaValorFixo) {
		this.diaVencimentoItemFaturaValorFixo = diaVencimentoItemFaturaValorFixo;
	}
	public String getOpcaoVencimentoItemFatura() {
		return opcaoVencimentoItemFatura;
	}
	public void setOpcaoVencimentoItemFatura(String opcaoVencimentoItemFatura) {
		this.opcaoVencimentoItemFatura = opcaoVencimentoItemFatura;
	}
	public String getFaseVencimentoItemFatura() {
		return faseVencimentoItemFatura;
	}
	public void setFaseVencimentoItemFatura(String faseVencimentoItemFatura) {
		this.faseVencimentoItemFatura = faseVencimentoItemFatura;
	}
	public String getIndicadorVencDiaNaoUtil() {
		return indicadorVencDiaNaoUtil;
	}
	public void setIndicadorVencDiaNaoUtil(String indicadorVencDiaNaoUtil) {
		this.indicadorVencDiaNaoUtil = indicadorVencDiaNaoUtil;
	}
	public String getDiaCotacao() {
		return diaCotacao;
	}
	public void setDiaCotacao(String diaCotacao) {
		this.diaCotacao = diaCotacao;
	}
	public String getDataReferenciaCambial() {
		return dataReferenciaCambial;
	}
	public void setDataReferenciaCambial(String dataReferenciaCambial) {
		this.dataReferenciaCambial = dataReferenciaCambial;
	}
	public String getIndicadorNFE() {
		return indicadorNFE;
	}
	public void setIndicadorNFE(String indicadorNFE) {
		this.indicadorNFE = indicadorNFE;
	}
	public String getEmitirFaturaComNfe() {
		return emitirFaturaComNfe;
	}
	public void setEmitirFaturaComNfe(String emitirFaturaComNfe) {
		this.emitirFaturaComNfe = emitirFaturaComNfe;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public String getTipoGarantiaFinanceira() {
		return tipoGarantiaFinanceira;
	}
	public void setTipoGarantiaFinanceira(String tipoGarantiaFinanceira) {
		this.tipoGarantiaFinanceira = tipoGarantiaFinanceira;
	}
	public String getDescricaoGarantia() {
		return descricaoGarantia;
	}
	public void setDescricaoGarantia(String descricaoGarantia) {
		this.descricaoGarantia = descricaoGarantia;
	}
	public String getValorGarantiaFinanceira() {
		return valorGarantiaFinanceira;
	}
	public void setValorGarantiaFinanceira(String valorGarantiaFinanceira) {
		this.valorGarantiaFinanceira = valorGarantiaFinanceira;
	}
	public String getDataInicioGarantiaFinanceira() {
		return dataInicioGarantiaFinanceira;
	}
	public void setDataInicioGarantiaFinanceira(String dataInicioGarantiaFinanceira) {
		this.dataInicioGarantiaFinanceira = dataInicioGarantiaFinanceira;
	}
	public String getDataFinalGarantiaFinanceira() {
		return dataFinalGarantiaFinanceira;
	}
	public void setDataFinalGarantiaFinanceira(String dataFinalGarantiaFinanceira) {
		this.dataFinalGarantiaFinanceira = dataFinalGarantiaFinanceira;
	}
	public String getGarantiaFinanceiraRenovada() {
		return garantiaFinanceiraRenovada;
	}
	public void setGarantiaFinanceiraRenovada(String garantiaFinanceiraRenovada) {
		this.garantiaFinanceiraRenovada = garantiaFinanceiraRenovada;
	}
	public String getHoraInicialDia() {
		return horaInicialDia;
	}
	public void setHoraInicialDia(String horaInicialDia) {
		this.horaInicialDia = horaInicialDia;
	}
	public String getRegimeConsumo() {
		return regimeConsumo;
	}
	public void setRegimeConsumo(String regimeConsumo) {
		this.regimeConsumo = regimeConsumo;
	}
	public String getFornecimentoMaximoDiario() {
		return fornecimentoMaximoDiario;
	}
	public void setFornecimentoMaximoDiario(String fornecimentoMaximoDiario) {
		this.fornecimentoMaximoDiario = fornecimentoMaximoDiario;
	}
	public String getUnidadeFornecMaximoDiario() {
		return unidadeFornecMaximoDiario;
	}
	public void setUnidadeFornecMaximoDiario(String unidadeFornecMaximoDiario) {
		this.unidadeFornecMaximoDiario = unidadeFornecMaximoDiario;
	}
	public String getFornecimentoMinimoDiario() {
		return fornecimentoMinimoDiario;
	}
	public void setFornecimentoMinimoDiario(String fornecimentoMinimoDiario) {
		this.fornecimentoMinimoDiario = fornecimentoMinimoDiario;
	}
	public String getUnidadeFornecMinimoDiario() {
		return unidadeFornecMinimoDiario;
	}
	public void setUnidadeFornecMinimoDiario(String unidadeFornecMinimoDiario) {
		this.unidadeFornecMinimoDiario = unidadeFornecMinimoDiario;
	}
	public String getFornecimentoMinimoMensal() {
		return fornecimentoMinimoMensal;
	}
	public void setFornecimentoMinimoMensal(String fornecimentoMinimoMensal) {
		this.fornecimentoMinimoMensal = fornecimentoMinimoMensal;
	}
	public String getUnidadeFornecMinimoMensal() {
		return unidadeFornecMinimoMensal;
	}
	public void setUnidadeFornecMinimoMensal(String unidadeFornecMinimoMensal) {
		this.unidadeFornecMinimoMensal = unidadeFornecMinimoMensal;
	}
	public String getFornecimentoMinimoAnual() {
		return fornecimentoMinimoAnual;
	}
	public void setFornecimentoMinimoAnual(String fornecimentoMinimoAnual) {
		this.fornecimentoMinimoAnual = fornecimentoMinimoAnual;
	}
	public String getUnidadeFornecMinimoAnual() {
		return unidadeFornecMinimoAnual;
	}
	public void setUnidadeFornecMinimoAnual(String unidadeFornecMinimoAnual) {
		this.unidadeFornecMinimoAnual = unidadeFornecMinimoAnual;
	}
	public String getFatorUnicoCorrecao() {
		return fatorUnicoCorrecao;
	}
	public void setFatorUnicoCorrecao(String fatorUnicoCorrecao) {
		this.fatorUnicoCorrecao = fatorUnicoCorrecao;
	}
	public String getConsumoFatFalhaMedicao() {
		return consumoFatFalhaMedicao;
	}
	public void setConsumoFatFalhaMedicao(String consumoFatFalhaMedicao) {
		this.consumoFatFalhaMedicao = consumoFatFalhaMedicao;
	}
	public String getModalidade() {
		return modalidade;
	}
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}
	public String getQdc() {
		return qdc;
	}
	public void setQdc(String qdc) {
		this.qdc = qdc;
	}
	public String getDataVigenciaQDC() {
		return dataVigenciaQDC;
	}
	public void setDataVigenciaQDC(String dataVigenciaQDC) {
		this.dataVigenciaQDC = dataVigenciaQDC;
	}
	public String getPrazoRevizaoQDC() {
		return prazoRevizaoQDC;
	}
	public void setPrazoRevizaoQDC(String prazoRevizaoQDC) {
		this.prazoRevizaoQDC = prazoRevizaoQDC;
	}
	public String getProgramacaoConsumo() {
		return programacaoConsumo;
	}
	public void setProgramacaoConsumo(String programacaoConsumo) {
		this.programacaoConsumo = programacaoConsumo;
	}
	public String getQdsMaiorQDC() {
		return qdsMaiorQDC;
	}
	public void setQdsMaiorQDC(String qdsMaiorQDC) {
		this.qdsMaiorQDC = qdsMaiorQDC;
	}
	public String getDiasAntecSolicConsumo() {
		return diasAntecSolicConsumo;
	}
	public void setDiasAntecSolicConsumo(String diasAntecSolicConsumo) {
		this.diasAntecSolicConsumo = diasAntecSolicConsumo;
	}
	public String getIndicadorProgramacaoConsumo() {
		return indicadorProgramacaoConsumo;
	}
	public void setIndicadorProgramacaoConsumo(String indicadorProgramacaoConsumo) {
		this.indicadorProgramacaoConsumo = indicadorProgramacaoConsumo;
	}
	public String getHoraLimiteProgramacaoDiaria() {
		return horaLimiteProgramacaoDiaria;
	}
	public void setHoraLimiteProgramacaoDiaria(String horaLimiteProgramacaoDiaria) {
		this.horaLimiteProgramacaoDiaria = horaLimiteProgramacaoDiaria;
	}
	public String getHoraLimiteProgIntradiaria() {
		return horaLimiteProgIntradiaria;
	}
	public void setHoraLimiteProgIntradiaria(String horaLimiteProgIntradiaria) {
		this.horaLimiteProgIntradiaria = horaLimiteProgIntradiaria;
	}
	public String getHoraLimiteAceitacaoDiaria() {
		return horaLimiteAceitacaoDiaria;
	}
	public void setHoraLimiteAceitacaoDiaria(String horaLimiteAceitacaoDiaria) {
		this.horaLimiteAceitacaoDiaria = horaLimiteAceitacaoDiaria;
	}
	public String getHoraLimiteAceitIntradiaria() {
		return horaLimiteAceitIntradiaria;
	}
	public void setHoraLimiteAceitIntradiaria(String horaLimiteAceitIntradiaria) {
		this.horaLimiteAceitIntradiaria = horaLimiteAceitIntradiaria;
	}
	public String getVariacaoSuperiorQDC() {
		return variacaoSuperiorQDC;
	}
	public void setVariacaoSuperiorQDC(String variacaoSuperiorQDC) {
		this.variacaoSuperiorQDC = variacaoSuperiorQDC;
	}
	public String getNumMesesSolicConsumo() {
		return numMesesSolicConsumo;
	}
	public void setNumMesesSolicConsumo(String numMesesSolicConsumo) {
		this.numMesesSolicConsumo = numMesesSolicConsumo;
	}
	public String getNumHorasSolicConsumo() {
		return numHorasSolicConsumo;
	}
	public void setNumHorasSolicConsumo(String numHorasSolicConsumo) {
		this.numHorasSolicConsumo = numHorasSolicConsumo;
	}
	public String getConfirmacaoAutomaticaQDS() {
		return confirmacaoAutomaticaQDS;
	}
	public void setConfirmacaoAutomaticaQDS(String confirmacaoAutomaticaQDS) {
		this.confirmacaoAutomaticaQDS = confirmacaoAutomaticaQDS;
	}
	public String getDataInicioRetiradaQPNR() {
		return dataInicioRetiradaQPNR;
	}
	public void setDataInicioRetiradaQPNR(String dataInicioRetiradaQPNR) {
		this.dataInicioRetiradaQPNR = dataInicioRetiradaQPNR;
	}
	public String getDataFimRetiradaQPNR() {
		return dataFimRetiradaQPNR;
	}
	public void setDataFimRetiradaQPNR(String dataFimRetiradaQPNR) {
		this.dataFimRetiradaQPNR = dataFimRetiradaQPNR;
	}
	public String getPercDuranteRetiradaQPNR() {
		return percDuranteRetiradaQPNR;
	}
	public void setPercDuranteRetiradaQPNR(String percDuranteRetiradaQPNR) {
		this.percDuranteRetiradaQPNR = percDuranteRetiradaQPNR;
	}
	public String getPercMinDuranteRetiradaQPNR() {
		return percMinDuranteRetiradaQPNR;
	}
	public void setPercMinDuranteRetiradaQPNR(String percMinDuranteRetiradaQPNR) {
		this.percMinDuranteRetiradaQPNR = percMinDuranteRetiradaQPNR;
	}
	public String getPercFimRetiradaQPNR() {
		return percFimRetiradaQPNR;
	}
	public void setPercFimRetiradaQPNR(String percFimRetiradaQPNR) {
		this.percFimRetiradaQPNR = percFimRetiradaQPNR;
	}
	public String getLocalAmostragemPCS() {
		return localAmostragemPCS;
	}
	public void setLocalAmostragemPCS(String localAmostragemPCS) {
		this.localAmostragemPCS = localAmostragemPCS;
	}
	public String getIntervaloRecuperacaoPCS() {
		return intervaloRecuperacaoPCS;
	}
	public void setIntervaloRecuperacaoPCS(String intervaloRecuperacaoPCS) {
		this.intervaloRecuperacaoPCS = intervaloRecuperacaoPCS;
	}
	public String getTamReducaoRecuperacaoPCS() {
		return tamReducaoRecuperacaoPCS;
	}
	public void setTamReducaoRecuperacaoPCS(String tamReducaoRecuperacaoPCS) {
		this.tamReducaoRecuperacaoPCS = tamReducaoRecuperacaoPCS;
	}
	public String getPeriodicidadeTakeOrPay() {
		return periodicidadeTakeOrPay;
	}
	public void setPeriodicidadeTakeOrPay(String periodicidadeTakeOrPay) {
		this.periodicidadeTakeOrPay = periodicidadeTakeOrPay;
	}
	public String getPeriodicidadeShipOrPay() {
		return periodicidadeShipOrPay;
	}
	public void setPeriodicidadeShipOrPay(String periodicidadeShipOrPay) {
		this.periodicidadeShipOrPay = periodicidadeShipOrPay;
	}
	public String getConsumoReferenciaTakeOrPay() {
		return consumoReferenciaTakeOrPay;
	}
	public void setConsumoReferenciaTakeOrPay(String consumoReferenciaTakeOrPay) {
		this.consumoReferenciaTakeOrPay = consumoReferenciaTakeOrPay;
	}
	public String getMargemVariacaoTakeOrPay() {
		return margemVariacaoTakeOrPay;
	}
	public void setMargemVariacaoTakeOrPay(String margemVariacaoTakeOrPay) {
		this.margemVariacaoTakeOrPay = margemVariacaoTakeOrPay;
	}
	public String getRecuperacaoAutoTakeOrPay() {
		return recuperacaoAutoTakeOrPay;
	}
	public void setRecuperacaoAutoTakeOrPay(String recuperacaoAutoTakeOrPay) {
		this.recuperacaoAutoTakeOrPay = recuperacaoAutoTakeOrPay;
	}
	public String getConsumoReferenciaShipOrPay() {
		return consumoReferenciaShipOrPay;
	}
	public void setConsumoReferenciaShipOrPay(String consumoReferenciaShipOrPay) {
		this.consumoReferenciaShipOrPay = consumoReferenciaShipOrPay;
	}
	public String getMargemVariacaoShipOrPay() {
		return margemVariacaoShipOrPay;
	}
	public void setMargemVariacaoShipOrPay(String margemVariacaoShipOrPay) {
		this.margemVariacaoShipOrPay = margemVariacaoShipOrPay;
	}
	public String getConsumoReferenciaSobreDem() {
		return consumoReferenciaSobreDem;
	}
	public void setConsumoReferenciaSobreDem(String consumoReferenciaSobreDem) {
		this.consumoReferenciaSobreDem = consumoReferenciaSobreDem;
	}
	public String getFaixaPenalidadeSobreDem() {
		return faixaPenalidadeSobreDem;
	}
	public void setFaixaPenalidadeSobreDem(String faixaPenalidadeSobreDem) {
		this.faixaPenalidadeSobreDem = faixaPenalidadeSobreDem;
	}
	public String getConsumoReferenciaSobDem() {
		return consumoReferenciaSobDem;
	}
	public void setConsumoReferenciaSobDem(String consumoReferenciaSobDem) {
		this.consumoReferenciaSobDem = consumoReferenciaSobDem;
	}
	public String getFaixaPenalidadeSobDem() {
		return faixaPenalidadeSobDem;
	}
	public void setFaixaPenalidadeSobDem(String faixaPenalidadeSobDem) {
		this.faixaPenalidadeSobDem = faixaPenalidadeSobDem;
	}
	public String getPercentualQNR() {
		return percentualQNR;
	}
	public void setPercentualQNR(String percentualQNR) {
		this.percentualQNR = percentualQNR;
	}
	public String getClienteResponsavel() {
		return clienteResponsavel;
	}
	public void setClienteResponsavel(String clienteResponsavel) {
		this.clienteResponsavel = clienteResponsavel;
	}
	public String getTipoResponsabilidade() {
		return tipoResponsabilidade;
	}
	public void setTipoResponsabilidade(String tipoResponsabilidade) {
		this.tipoResponsabilidade = tipoResponsabilidade;
	}
	public String getDataInicioRelacao() {
		return dataInicioRelacao;
	}
	public void setDataInicioRelacao(String dataInicioRelacao) {
		this.dataInicioRelacao = dataInicioRelacao;
	}
	public String getDataFimRelacao() {
		return dataFimRelacao;
	}
	public void setDataFimRelacao(String dataFimRelacao) {
		this.dataFimRelacao = dataFimRelacao;
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getDocumentoCliente() {
		return documentoCliente;
	}
	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}
	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}
	public String getMatriculaImovel() {
		return matriculaImovel;
	}
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}
	public String getNumeroImovel() {
		return numeroImovel;
	}
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	public String getCidadeImovel() {
		return cidadeImovel;
	}
	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}
	public String getCondominio() {
		return condominio;
	}
	public void setCondominio(String condominio) {
		this.condominio = condominio;
	}
	public String getTempoValidadeRetiradaQPNR() {
		return tempoValidadeRetiradaQPNR;
	}
	public void setTempoValidadeRetiradaQPNR(String tempoValidadeRetiradaQPNR) {
		this.tempoValidadeRetiradaQPNR = tempoValidadeRetiradaQPNR;
	}
	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}
	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}
	public String getNumeroFormatado() {
		return numeroFormatado;
	}
	public void setNumeroFormatado(String numeroFormatado) {
		this.numeroFormatado = numeroFormatado;
	}
	public String getChaveCep() {
		return chaveCep;
	}
	public void setChaveCep(String chaveCep) {
		this.chaveCep = chaveCep;
	}
	public String getReferenciaQFParadaProgramada() {
		return referenciaQFParadaProgramada;
	}
	public void setReferenciaQFParadaProgramada(String referenciaQFParadaProgramada) {
		this.referenciaQFParadaProgramada = referenciaQFParadaProgramada;
	}
	public String getPercentualMargemVariacao() {
		return percentualMargemVariacao;
	}
	public void setPercentualMargemVariacao(String percentualMargemVariacao) {
		this.percentualMargemVariacao = percentualMargemVariacao;
	}
	public String getConsumoReferencia() {
		return consumoReferencia;
	}
	public void setConsumoReferencia(String consumoReferencia) {
		this.consumoReferencia = consumoReferencia;
	}
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}
	public String getPercentualNaoRecuperavel() {
		return percentualNaoRecuperavel;
	}
	public void setPercentualNaoRecuperavel(String percentualNaoRecuperavel) {
		this.percentualNaoRecuperavel = percentualNaoRecuperavel;
	}
	public String getConsideraParadaProgramada() {
		return consideraParadaProgramada;
	}
	public void setConsideraParadaProgramada(String consideraParadaProgramada) {
		this.consideraParadaProgramada = consideraParadaProgramada;
	}
	public String getConsideraParadaNaoProgramada() {
		return consideraParadaNaoProgramada;
	}
	public void setConsideraParadaNaoProgramada(String consideraParadaNaoProgramada) {
		this.consideraParadaNaoProgramada = consideraParadaNaoProgramada;
	}
	public String getConsideraFalhaFornecimento() {
		return consideraFalhaFornecimento;
	}
	public void setConsideraFalhaFornecimento(String consideraFalhaFornecimento) {
		this.consideraFalhaFornecimento = consideraFalhaFornecimento;
	}
	public String getConsideraCasoFortuito() {
		return consideraCasoFortuito;
	}
	public void setConsideraCasoFortuito(String consideraCasoFortuito) {
		this.consideraCasoFortuito = consideraCasoFortuito;
	}
	public String getRecuperavel() {
		return recuperavel;
	}
	public void setRecuperavel(String recuperavel) {
		this.recuperavel = recuperavel;
	}
	public String getTipoApuracao() {
		return tipoApuracao;
	}
	public void setTipoApuracao(String tipoApuracao) {
		this.tipoApuracao = tipoApuracao;
	}
	public String getPrecoCobranca() {
		return precoCobranca;
	}
	public void setPrecoCobranca(String precoCobranca) {
		this.precoCobranca = precoCobranca;
	}
	public String getApuracaoParadaProgramada() {
		return apuracaoParadaProgramada;
	}
	public void setApuracaoParadaProgramada(String apuracaoParadaProgramada) {
		this.apuracaoParadaProgramada = apuracaoParadaProgramada;
	}
	public String getApuracaoParadaNaoProgramada() {
		return apuracaoParadaNaoProgramada;
	}
	public void setApuracaoParadaNaoProgramada(String apuracaoParadaNaoProgramada) {
		this.apuracaoParadaNaoProgramada = apuracaoParadaNaoProgramada;
	}
	public String getApuracaoCasoFortuito() {
		return apuracaoCasoFortuito;
	}
	public void setApuracaoCasoFortuito(String apuracaoCasoFortuito) {
		this.apuracaoCasoFortuito = apuracaoCasoFortuito;
	}
	public String getApuracaoFalhaFornecimento() {
		return apuracaoFalhaFornecimento;
	}
	public void setApuracaoFalhaFornecimento(String apuracaoFalhaFornecimento) {
		this.apuracaoFalhaFornecimento = apuracaoFalhaFornecimento;
	}
	public String getDescricaoAnexo() {
		return descricaoAnexo;
	}
	public void setDescricaoAnexo(String descricaoAnexo) {
		this.descricaoAnexo = descricaoAnexo;
	}
	public String getDescricaoImovel() {
		return descricaoImovel;
	}
	public void setDescricaoImovel(String descricaoImovel) {
		this.descricaoImovel = descricaoImovel;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getCepImovel() {
		return cepImovel;
	}
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	public String getIndicadorFaturamentoAgrupado() {
		return indicadorFaturamentoAgrupado;
	}
	public void setIndicadorFaturamentoAgrupado(String indicadorFaturamentoAgrupado) {
		this.indicadorFaturamentoAgrupado = indicadorFaturamentoAgrupado;
	}
	public String getIndicadorEmissaoAgrupada() {
		return indicadorEmissaoAgrupada;
	}
	public void setIndicadorEmissaoAgrupada(String indicadorEmissaoAgrupada) {
		this.indicadorEmissaoAgrupada = indicadorEmissaoAgrupada;
	}
	public String getIndicadorContratoExibido() {
		return indicadorContratoExibido;
	}
	public void setIndicadorContratoExibido(String indicadorContratoExibido) {
		this.indicadorContratoExibido = indicadorContratoExibido;
	}
	public String getIndicadorImovelSelecionado() {
		return indicadorImovelSelecionado;
	}
	public void setIndicadorImovelSelecionado(String indicadorImovelSelecionado) {
		this.indicadorImovelSelecionado = indicadorImovelSelecionado;
	}
	public String getFaturavel() {
		return faturavel;
	}
	public void setFaturavel(String faturavel) {
		this.faturavel = faturavel;
	}
	public String[] getQDCContrato() {
		return qDCContrato;
	}
	public void setQDCContrato(String[] qDCContrato) {
		this.qDCContrato = qDCContrato;
	}
	
	
	public boolean getFluxoAditamento() {
		return fluxoAditamento;
	}
	public void setFluxoAditamento(boolean fluxoAditamento) {
		this.fluxoAditamento = fluxoAditamento;
	}
	public boolean getFluxoDetalhamento() {
		return fluxoDetalhamento;
	}
	public void setFluxoDetalhamento(boolean fluxoDetalhamento) {
		this.fluxoDetalhamento = fluxoDetalhamento;
	}
	public boolean getFluxoInclusao() {
		return fluxoInclusao;
	}
	public void setFluxoInclusao(boolean fluxoInclusao) {
		this.fluxoInclusao = fluxoInclusao;
	}
	public boolean getFluxoAlteracao() {
		return fluxoAlteracao;
	}
	public void setFluxoAlteracao(boolean fluxoAlteracao) {
		this.fluxoAlteracao = fluxoAlteracao;
	}
	public boolean getFluxoPesquisa() {
		return fluxoPesquisa;
	}
	public void setFluxoPesquisa(boolean fluxoPesquisa) {
		this.fluxoPesquisa = fluxoPesquisa;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public Boolean getMudancaModelo() {
		return mudancaModelo;
	}
	public void setMudancaModelo(Boolean mudancaModelo) {
		this.mudancaModelo = mudancaModelo;
	}
	public Boolean getSelAbaGeral() {
		return selAbaGeral;
	}
	public void setSelAbaGeral(Boolean selAbaGeral) {
		this.selAbaGeral = selAbaGeral;
	}
	public Boolean getSelAbaPrincipais() {
		return selAbaPrincipais;
	}
	public void setSelAbaPrincipais(Boolean selAbaPrincipais) {
		this.selAbaPrincipais = selAbaPrincipais;
	}
	public Boolean getSelAbaTecnicos() {
		return selAbaTecnicos;
	}
	public void setSelAbaTecnicos(Boolean selAbaTecnicos) {
		this.selAbaTecnicos = selAbaTecnicos;
	}
	public Boolean getSelAbaConsumo() {
		return selAbaConsumo;
	}
	public void setSelAbaConsumo(Boolean selAbaConsumo) {
		this.selAbaConsumo = selAbaConsumo;
	}
	public Boolean getSelAbaFaturamento() {
		return selAbaFaturamento;
	}
	public void setSelAbaFaturamento(Boolean selAbaFaturamento) {
		this.selAbaFaturamento = selAbaFaturamento;
	}
	public Boolean getSelAbaRegrasFaturamento() {
		return selAbaRegrasFaturamento;
	}
	public void setSelAbaRegrasFaturamento(Boolean selAbaRegrasFaturamento) {
		this.selAbaRegrasFaturamento = selAbaRegrasFaturamento;
	}
	public Boolean getSelAbaResponsabilidade() {
		return selAbaResponsabilidade;
	}
	public void setSelAbaResponsabilidade(Boolean selAbaResponsabilidade) {
		this.selAbaResponsabilidade = selAbaResponsabilidade;
	}
	public Boolean getSelNumeroContrato() {
		return selNumeroContrato;
	}
	public void setSelNumeroContrato(Boolean selNumeroContrato) {
		this.selNumeroContrato = selNumeroContrato;
	}
	public Boolean getObgNumeroContrato() {
		return obgNumeroContrato;
	}
	public void setObgNumeroContrato(Boolean obgNumeroContrato) {
		this.obgNumeroContrato = obgNumeroContrato;
	}
	public Boolean getSelNumeroAnteriorContrato() {
		return selNumeroAnteriorContrato;
	}
	public void setSelNumeroAnteriorContrato(Boolean selNumeroAnteriorContrato) {
		this.selNumeroAnteriorContrato = selNumeroAnteriorContrato;
	}
	public Boolean getObgNumeroAnteriorContrato() {
		return obgNumeroAnteriorContrato;
	}
	public void setObgNumeroAnteriorContrato(Boolean obgNumeroAnteriorContrato) {
		this.obgNumeroAnteriorContrato = obgNumeroAnteriorContrato;
	}
	public Boolean getSelDataAssinatura() {
		return selDataAssinatura;
	}
	public void setSelDataAssinatura(Boolean selDataAssinatura) {
		this.selDataAssinatura = selDataAssinatura;
	}
	public Boolean getObgDataAssinatura() {
		return obgDataAssinatura;
	}
	public void setObgDataAssinatura(Boolean obgDataAssinatura) {
		this.obgDataAssinatura = obgDataAssinatura;
	}
	public Boolean getSelSituacaoContrato() {
		return selSituacaoContrato;
	}
	public void setSelSituacaoContrato(Boolean selSituacaoContrato) {
		this.selSituacaoContrato = selSituacaoContrato;
	}
	public Boolean getObgSituacaoContrato() {
		return obgSituacaoContrato;
	}
	public void setObgSituacaoContrato(Boolean obgSituacaoContrato) {
		this.obgSituacaoContrato = obgSituacaoContrato;
	}
	public Boolean getValorFixoSituacaoContrato() {
		return valorFixoSituacaoContrato;
	}
	public void setValorFixoSituacaoContrato(Boolean valorFixoSituacaoContrato) {
		this.valorFixoSituacaoContrato = valorFixoSituacaoContrato;
	}
	public Boolean getSelNumeroAditivo() {
		return selNumeroAditivo;
	}
	public void setSelNumeroAditivo(Boolean selNumeroAditivo) {
		this.selNumeroAditivo = selNumeroAditivo;
	}
	public Boolean getObgNumeroAditivo() {
		return obgNumeroAditivo;
	}
	public void setObgNumeroAditivo(Boolean obgNumeroAditivo) {
		this.obgNumeroAditivo = obgNumeroAditivo;
	}
	public Boolean getSelDataAditivo() {
		return selDataAditivo;
	}
	public void setSelDataAditivo(Boolean selDataAditivo) {
		this.selDataAditivo = selDataAditivo;
	}
	public Boolean getObgDataAditivo() {
		return obgDataAditivo;
	}
	public void setObgDataAditivo(Boolean obgDataAditivo) {
		this.obgDataAditivo = obgDataAditivo;
	}
	public Boolean getSelDescricaoAditivo() {
		return selDescricaoAditivo;
	}
	public void setSelDescricaoAditivo(Boolean selDescricaoAditivo) {
		this.selDescricaoAditivo = selDescricaoAditivo;
	}
	public Boolean getObgDescricaoAditivo() {
		return obgDescricaoAditivo;
	}
	public void setObgDescricaoAditivo(Boolean obgDescricaoAditivo) {
		this.obgDescricaoAditivo = obgDescricaoAditivo;
	}
	public Boolean getSelValorContrato() {
		return selValorContrato;
	}
	public void setSelValorContrato(Boolean selValorContrato) {
		this.selValorContrato = selValorContrato;
	}
	public Boolean getObgValorContrato() {
		return obgValorContrato;
	}
	public void setObgValorContrato(Boolean obgValorContrato) {
		this.obgValorContrato = obgValorContrato;
	}
	public Boolean getSelIndicadorMultaAtraso() {
		return selIndicadorMultaAtraso;
	}
	public void setSelIndicadorMultaAtraso(Boolean selIndicadorMultaAtraso) {
		this.selIndicadorMultaAtraso = selIndicadorMultaAtraso;
	}
	public Boolean getObgIndicadorMultaAtraso() {
		return obgIndicadorMultaAtraso;
	}
	public void setObgIndicadorMultaAtraso(Boolean obgIndicadorMultaAtraso) {
		this.obgIndicadorMultaAtraso = obgIndicadorMultaAtraso;
	}
	public Boolean getValorFixoIndicadorMultaAtraso() {
		return valorFixoIndicadorMultaAtraso;
	}
	public void setValorFixoIndicadorMultaAtraso(Boolean valorFixoIndicadorMultaAtraso) {
		this.valorFixoIndicadorMultaAtraso = valorFixoIndicadorMultaAtraso;
	}
	public Boolean getSelIndicadorJurosMora() {
		return selIndicadorJurosMora;
	}
	public void setSelIndicadorJurosMora(Boolean selIndicadorJurosMora) {
		this.selIndicadorJurosMora = selIndicadorJurosMora;
	}
	public Boolean getObgIndicadorJurosMora() {
		return obgIndicadorJurosMora;
	}
	public void setObgIndicadorJurosMora(Boolean obgIndicadorJurosMora) {
		this.obgIndicadorJurosMora = obgIndicadorJurosMora;
	}
	public Boolean getValorFixoIndicadorJurosMora() {
		return valorFixoIndicadorJurosMora;
	}
	public void setValorFixoIndicadorJurosMora(Boolean valorFixoIndicadorJurosMora) {
		this.valorFixoIndicadorJurosMora = valorFixoIndicadorJurosMora;
	}
	public Boolean getSelIndiceCorrecaoMonetaria() {
		return selIndiceCorrecaoMonetaria;
	}
	public void setSelIndiceCorrecaoMonetaria(Boolean selIndiceCorrecaoMonetaria) {
		this.selIndiceCorrecaoMonetaria = selIndiceCorrecaoMonetaria;
	}
	public Boolean getObgIndiceCorrecaoMonetaria() {
		return obgIndiceCorrecaoMonetaria;
	}
	public void setObgIndiceCorrecaoMonetaria(Boolean obgIndiceCorrecaoMonetaria) {
		this.obgIndiceCorrecaoMonetaria = obgIndiceCorrecaoMonetaria;
	}
	public Boolean getValorFixoIndiceCorrecaoMonetaria() {
		return valorFixoIndiceCorrecaoMonetaria;
	}
	public void setValorFixoIndiceCorrecaoMonetaria(Boolean valorFixoIndiceCorrecaoMonetaria) {
		this.valorFixoIndiceCorrecaoMonetaria = valorFixoIndiceCorrecaoMonetaria;
	}
	public Boolean getSelNumeroProposta() {
		return selNumeroProposta;
	}
	public void setSelNumeroProposta(Boolean selNumeroProposta) {
		this.selNumeroProposta = selNumeroProposta;
	}
	public Boolean getObgNumeroProposta() {
		return obgNumeroProposta;
	}
	public void setObgNumeroProposta(Boolean obgNumeroProposta) {
		this.obgNumeroProposta = obgNumeroProposta;
	}
	public Boolean getSelTarifaGNAplicada() {
		return selTarifaGNAplicada;
	}
	public void setSelTarifaGNAplicada(Boolean selTarifaGNAplicada) {
		this.selTarifaGNAplicada = selTarifaGNAplicada;
	}
	public Boolean getObgTarifaGNAplicada() {
		return obgTarifaGNAplicada;
	}
	public void setObgTarifaGNAplicada(Boolean obgTarifaGNAplicada) {
		this.obgTarifaGNAplicada = obgTarifaGNAplicada;
	}
	public Boolean getSelGastoEstimadoGNMes() {
		return selGastoEstimadoGNMes;
	}
	public void setSelGastoEstimadoGNMes(Boolean selGastoEstimadoGNMes) {
		this.selGastoEstimadoGNMes = selGastoEstimadoGNMes;
	}
	public Boolean getObgGastoEstimadoGNMes() {
		return obgGastoEstimadoGNMes;
	}
	public void setObgGastoEstimadoGNMes(Boolean obgGastoEstimadoGNMes) {
		this.obgGastoEstimadoGNMes = obgGastoEstimadoGNMes;
	}
	public Boolean getSelEconomiaEstimadaGNMes() {
		return selEconomiaEstimadaGNMes;
	}
	public void setSelEconomiaEstimadaGNMes(Boolean selEconomiaEstimadaGNMes) {
		this.selEconomiaEstimadaGNMes = selEconomiaEstimadaGNMes;
	}
	public Boolean getObgEconomiaEstimadaGNMes() {
		return obgEconomiaEstimadaGNMes;
	}
	public void setObgEconomiaEstimadaGNMes(Boolean obgEconomiaEstimadaGNMes) {
		this.obgEconomiaEstimadaGNMes = obgEconomiaEstimadaGNMes;
	}
	public Boolean getSelEconomiaEstimadaGNAno() {
		return selEconomiaEstimadaGNAno;
	}
	public void setSelEconomiaEstimadaGNAno(Boolean selEconomiaEstimadaGNAno) {
		this.selEconomiaEstimadaGNAno = selEconomiaEstimadaGNAno;
	}
	public Boolean getObgEconomiaEstimadaGNAno() {
		return obgEconomiaEstimadaGNAno;
	}
	public void setObgEconomiaEstimadaGNAno(Boolean obgEconomiaEstimadaGNAno) {
		this.obgEconomiaEstimadaGNAno = obgEconomiaEstimadaGNAno;
	}
	public Boolean getSelDescontoEfetivoEconomia() {
		return selDescontoEfetivoEconomia;
	}
	public void setSelDescontoEfetivoEconomia(Boolean selDescontoEfetivoEconomia) {
		this.selDescontoEfetivoEconomia = selDescontoEfetivoEconomia;
	}
	public Boolean getObgDescontoEfetivoEconomia() {
		return obgDescontoEfetivoEconomia;
	}
	public void setObgDescontoEfetivoEconomia(Boolean obgDescontoEfetivoEconomia) {
		this.obgDescontoEfetivoEconomia = obgDescontoEfetivoEconomia;
	}
	public Boolean getSelDiaVencFinanciamento() {
		return selDiaVencFinanciamento;
	}
	public void setSelDiaVencFinanciamento(Boolean selDiaVencFinanciamento) {
		this.selDiaVencFinanciamento = selDiaVencFinanciamento;
	}
	public Boolean getObgDiaVencFinanciamento() {
		return obgDiaVencFinanciamento;
	}
	public void setObgDiaVencFinanciamento(Boolean obgDiaVencFinanciamento) {
		this.obgDiaVencFinanciamento = obgDiaVencFinanciamento;
	}
	public Boolean getValorFixoDiaVencFinanciamento() {
		return valorFixoDiaVencFinanciamento;
	}
	public void setValorFixoDiaVencFinanciamento(Boolean valorFixoDiaVencFinanciamento) {
		this.valorFixoDiaVencFinanciamento = valorFixoDiaVencFinanciamento;
	}
	public Boolean getSelDescGarantiaFinanc() {
		return selDescGarantiaFinanc;
	}
	public void setSelDescGarantiaFinanc(Boolean selDescGarantiaFinanc) {
		this.selDescGarantiaFinanc = selDescGarantiaFinanc;
	}
	public Boolean getObgDescGarantiaFinanc() {
		return obgDescGarantiaFinanc;
	}
	public void setObgDescGarantiaFinanc(Boolean obgDescGarantiaFinanc) {
		this.obgDescGarantiaFinanc = obgDescGarantiaFinanc;
	}
	public Boolean getValorFixoDescGarantiaFinanc() {
		return valorFixoDescGarantiaFinanc;
	}
	public void setValorFixoDescGarantiaFinanc(Boolean valorFixoDescGarantiaFinanc) {
		this.valorFixoDescGarantiaFinanc = valorFixoDescGarantiaFinanc;
	}
	public Boolean getSelNumeroEmpenho() {
		return selNumeroEmpenho;
	}
	public void setSelNumeroEmpenho(Boolean selNumeroEmpenho) {
		this.selNumeroEmpenho = selNumeroEmpenho;
	}
	public Boolean getObgNumeroEmpenho() {
		return obgNumeroEmpenho;
	}
	public void setObgNumeroEmpenho(Boolean obgNumeroEmpenho) {
		this.obgNumeroEmpenho = obgNumeroEmpenho;
	}
	public Boolean getSelClienteAssinatura() {
		return selClienteAssinatura;
	}
	public void setSelClienteAssinatura(Boolean selClienteAssinatura) {
		this.selClienteAssinatura = selClienteAssinatura;
	}
	public Boolean getObgClienteAssinatura() {
		return obgClienteAssinatura;
	}
	public void setObgClienteAssinatura(Boolean obgClienteAssinatura) {
		this.obgClienteAssinatura = obgClienteAssinatura;
	}
	public Boolean getSelNumDiasRenoAutoContrato() {
		return selNumDiasRenoAutoContrato;
	}
	public void setSelNumDiasRenoAutoContrato(Boolean selNumDiasRenoAutoContrato) {
		this.selNumDiasRenoAutoContrato = selNumDiasRenoAutoContrato;
	}
	public Boolean getObgNumDiasRenoAutoContrato() {
		return obgNumDiasRenoAutoContrato;
	}
	public void setObgNumDiasRenoAutoContrato(Boolean obgNumDiasRenoAutoContrato) {
		this.obgNumDiasRenoAutoContrato = obgNumDiasRenoAutoContrato;
	}
	public Boolean getValorFixoNumDiasRenoAutoContrato() {
		return valorFixoNumDiasRenoAutoContrato;
	}
	public void setValorFixoNumDiasRenoAutoContrato(Boolean valorFixoNumDiasRenoAutoContrato) {
		this.valorFixoNumDiasRenoAutoContrato = valorFixoNumDiasRenoAutoContrato;
	}
	public Boolean getSelExigeAprovacao() {
		return selExigeAprovacao;
	}
	public void setSelExigeAprovacao(Boolean selExigeAprovacao) {
		this.selExigeAprovacao = selExigeAprovacao;
	}
	public Boolean getObgExigeAprovacao() {
		return obgExigeAprovacao;
	}
	public void setObgExigeAprovacao(Boolean obgExigeAprovacao) {
		this.obgExigeAprovacao = obgExigeAprovacao;
	}
	public Boolean getSelEnderecoPadrao() {
		return selEnderecoPadrao;
	}
	public void setSelEnderecoPadrao(Boolean selEnderecoPadrao) {
		this.selEnderecoPadrao = selEnderecoPadrao;
	}
	public Boolean getObgEnderecoPadrao() {
		return obgEnderecoPadrao;
	}
	public void setObgEnderecoPadrao(Boolean obgEnderecoPadrao) {
		this.obgEnderecoPadrao = obgEnderecoPadrao;
	}
	public Boolean getSelFaturamentoAgrupamento() {
		return selFaturamentoAgrupamento;
	}
	public void setSelFaturamentoAgrupamento(Boolean selFaturamentoAgrupamento) {
		this.selFaturamentoAgrupamento = selFaturamentoAgrupamento;
	}
	public Boolean getObgFaturamentoAgrupamento() {
		return obgFaturamentoAgrupamento;
	}
	public void setObgFaturamentoAgrupamento(Boolean obgFaturamentoAgrupamento) {
		this.obgFaturamentoAgrupamento = obgFaturamentoAgrupamento;
	}
	public Boolean getValorFixoFaturamentoAgrupamento() {
		return valorFixoFaturamentoAgrupamento;
	}
	public void setValorFixoFaturamentoAgrupamento(Boolean valorFixoFaturamentoAgrupamento) {
		this.valorFixoFaturamentoAgrupamento = valorFixoFaturamentoAgrupamento;
	}
	public Boolean getSelParticipaECartas() {
		return selParticipaECartas;
	}
	public void setSelParticipaECartas(Boolean selParticipaECartas) {
		this.selParticipaECartas = selParticipaECartas;
	}
	public Boolean getObgParticipaECartas() {
		return obgParticipaECartas;
	}
	public void setObgParticipaECartas(Boolean obgParticipaECartas) {
		this.obgParticipaECartas = obgParticipaECartas;
	}
	public Boolean getValorFixoParticipaECartas() {
		return valorFixoParticipaECartas;
	}
	public void setValorFixoParticipaECartas(Boolean valorFixoParticipaECartas) {
		this.valorFixoParticipaECartas = valorFixoParticipaECartas;
	}
	public Boolean getSelTipoPeriodicidadePenalidade() {
		return selTipoPeriodicidadePenalidade;
	}
	public void setSelTipoPeriodicidadePenalidade(Boolean selTipoPeriodicidadePenalidade) {
		this.selTipoPeriodicidadePenalidade = selTipoPeriodicidadePenalidade;
	}
	public Boolean getObgTipoPeriodicidadePenalidade() {
		return obgTipoPeriodicidadePenalidade;
	}
	public void setObgTipoPeriodicidadePenalidade(Boolean obgTipoPeriodicidadePenalidade) {
		this.obgTipoPeriodicidadePenalidade = obgTipoPeriodicidadePenalidade;
	}
	public Boolean getValorFixoTipoPeriodicidadePenalidade() {
		return valorFixoTipoPeriodicidadePenalidade;
	}
	public void setValorFixoTipoPeriodicidadePenalidade(Boolean valorFixoTipoPeriodicidadePenalidade) {
		this.valorFixoTipoPeriodicidadePenalidade = valorFixoTipoPeriodicidadePenalidade;
	}
	public Boolean getSelEmissaoFaturaAgrupada() {
		return selEmissaoFaturaAgrupada;
	}
	public void setSelEmissaoFaturaAgrupada(Boolean selEmissaoFaturaAgrupada) {
		this.selEmissaoFaturaAgrupada = selEmissaoFaturaAgrupada;
	}
	public Boolean getObgEmissaoFaturaAgrupada() {
		return obgEmissaoFaturaAgrupada;
	}
	public void setObgEmissaoFaturaAgrupada(Boolean obgEmissaoFaturaAgrupada) {
		this.obgEmissaoFaturaAgrupada = obgEmissaoFaturaAgrupada;
	}
	public Boolean getValorFixoEmissaoFaturaAgrupada() {
		return valorFixoEmissaoFaturaAgrupada;
	}
	public void setValorFixoEmissaoFaturaAgrupada(Boolean valorFixoEmissaoFaturaAgrupada) {
		this.valorFixoEmissaoFaturaAgrupada = valorFixoEmissaoFaturaAgrupada;
	}
	public Boolean getSelDataVigenciaContrato() {
		return selDataVigenciaContrato;
	}
	public void setSelDataVigenciaContrato(Boolean selDataVigenciaContrato) {
		this.selDataVigenciaContrato = selDataVigenciaContrato;
	}
	public Boolean getObgDataVigenciaContrato() {
		return obgDataVigenciaContrato;
	}
	public void setObgDataVigenciaContrato(Boolean obgDataVigenciaContrato) {
		this.obgDataVigenciaContrato = obgDataVigenciaContrato;
	}
	public Boolean getSelDataVencObrigacoesContratuais() {
		return selDataVencObrigacoesContratuais;
	}
	public void setSelDataVencObrigacoesContratuais(Boolean selDataVencObrigacoesContratuais) {
		this.selDataVencObrigacoesContratuais = selDataVencObrigacoesContratuais;
	}
	public Boolean getObgDataVencObrigacoesContratuais() {
		return obgDataVencObrigacoesContratuais;
	}
	public void setObgDataVencObrigacoesContratuais(Boolean obgDataVencObrigacoesContratuais) {
		this.obgDataVencObrigacoesContratuais = obgDataVencObrigacoesContratuais;
	}
	public Boolean getSelIndicadorAnoContratual() {
		return selIndicadorAnoContratual;
	}
	public void setSelIndicadorAnoContratual(Boolean selIndicadorAnoContratual) {
		this.selIndicadorAnoContratual = selIndicadorAnoContratual;
	}
	public Boolean getObgIndicadorAnoContratual() {
		return obgIndicadorAnoContratual;
	}
	public void setObgIndicadorAnoContratual(Boolean obgIndicadorAnoContratual) {
		this.obgIndicadorAnoContratual = obgIndicadorAnoContratual;
	}
	public Boolean getValorFixoIndicadorAnoContratual() {
		return valorFixoIndicadorAnoContratual;
	}
	public void setValorFixoIndicadorAnoContratual(Boolean valorFixoIndicadorAnoContratual) {
		this.valorFixoIndicadorAnoContratual = valorFixoIndicadorAnoContratual;
	}
	public Boolean getSelRenovacaoAutomatica() {
		return selRenovacaoAutomatica;
	}
	public void setSelRenovacaoAutomatica(Boolean selRenovacaoAutomatica) {
		this.selRenovacaoAutomatica = selRenovacaoAutomatica;
	}
	public Boolean getObgRenovacaoAutomatica() {
		return obgRenovacaoAutomatica;
	}
	public void setObgRenovacaoAutomatica(Boolean obgRenovacaoAutomatica) {
		this.obgRenovacaoAutomatica = obgRenovacaoAutomatica;
	}
	public Boolean getValorFixoRenovacaoAutomatica() {
		return valorFixoRenovacaoAutomatica;
	}
	public void setValorFixoRenovacaoAutomatica(Boolean valorFixoRenovacaoAutomatica) {
		this.valorFixoRenovacaoAutomatica = valorFixoRenovacaoAutomatica;
	}
	public Boolean getSelTempoAntecedenciaRenovacao() {
		return selTempoAntecedenciaRenovacao;
	}
	public void setSelTempoAntecedenciaRenovacao(Boolean selTempoAntecedenciaRenovacao) {
		this.selTempoAntecedenciaRenovacao = selTempoAntecedenciaRenovacao;
	}
	public Boolean getObgTempoAntecedenciaRenovacao() {
		return obgTempoAntecedenciaRenovacao;
	}
	public void setObgTempoAntecedenciaRenovacao(Boolean obgTempoAntecedenciaRenovacao) {
		this.obgTempoAntecedenciaRenovacao = obgTempoAntecedenciaRenovacao;
	}
	public Boolean getValorFixoTempoAntecedenciaRenovacao() {
		return valorFixoTempoAntecedenciaRenovacao;
	}
	public void setValorFixoTempoAntecedenciaRenovacao(Boolean valorFixoTempoAntecedenciaRenovacao) {
		this.valorFixoTempoAntecedenciaRenovacao = valorFixoTempoAntecedenciaRenovacao;
	}
	public Boolean getSelTempoAntecRevisaoGarantias() {
		return selTempoAntecRevisaoGarantias;
	}
	public void setSelTempoAntecRevisaoGarantias(Boolean selTempoAntecRevisaoGarantias) {
		this.selTempoAntecRevisaoGarantias = selTempoAntecRevisaoGarantias;
	}
	public Boolean getObgTempoAntecRevisaoGarantias() {
		return obgTempoAntecRevisaoGarantias;
	}
	public void setObgTempoAntecRevisaoGarantias(Boolean obgTempoAntecRevisaoGarantias) {
		this.obgTempoAntecRevisaoGarantias = obgTempoAntecRevisaoGarantias;
	}
	public Boolean getValorFixoTempoAntecRevisaoGarantias() {
		return valorFixoTempoAntecRevisaoGarantias;
	}
	public void setValorFixoTempoAntecRevisaoGarantias(Boolean valorFixoTempoAntecRevisaoGarantias) {
		this.valorFixoTempoAntecRevisaoGarantias = valorFixoTempoAntecRevisaoGarantias;
	}
	public Boolean getSelPeriodicidadeReavGarantias() {
		return selPeriodicidadeReavGarantias;
	}
	public void setSelPeriodicidadeReavGarantias(Boolean selPeriodicidadeReavGarantias) {
		this.selPeriodicidadeReavGarantias = selPeriodicidadeReavGarantias;
	}
	public Boolean getObgPeriodicidadeReavGarantias() {
		return obgPeriodicidadeReavGarantias;
	}
	public void setObgPeriodicidadeReavGarantias(Boolean obgPeriodicidadeReavGarantias) {
		this.obgPeriodicidadeReavGarantias = obgPeriodicidadeReavGarantias;
	}
	public Boolean getValorFixoPeriodicidadeReavGarantias() {
		return valorFixoPeriodicidadeReavGarantias;
	}
	public void setValorFixoPeriodicidadeReavGarantias(Boolean valorFixoPeriodicidadeReavGarantias) {
		this.valorFixoPeriodicidadeReavGarantias = valorFixoPeriodicidadeReavGarantias;
	}
	public Boolean getSelValorParticipacaoCliente() {
		return selValorParticipacaoCliente;
	}
	public void setSelValorParticipacaoCliente(Boolean selValorParticipacaoCliente) {
		this.selValorParticipacaoCliente = selValorParticipacaoCliente;
	}
	public Boolean getObgValorParticipacaoCliente() {
		return obgValorParticipacaoCliente;
	}
	public void setObgValorParticipacaoCliente(Boolean obgValorParticipacaoCliente) {
		this.obgValorParticipacaoCliente = obgValorParticipacaoCliente;
	}
	public Boolean getSelQtdParcelasFinanciamento() {
		return selQtdParcelasFinanciamento;
	}
	public void setSelQtdParcelasFinanciamento(Boolean selQtdParcelasFinanciamento) {
		this.selQtdParcelasFinanciamento = selQtdParcelasFinanciamento;
	}
	public Boolean getObgQtdParcelasFinanciamento() {
		return obgQtdParcelasFinanciamento;
	}
	public void setObgQtdParcelasFinanciamento(Boolean obgQtdParcelasFinanciamento) {
		this.obgQtdParcelasFinanciamento = obgQtdParcelasFinanciamento;
	}
	public Boolean getSelPercentualJurosFinanciamento() {
		return selPercentualJurosFinanciamento;
	}
	public void setSelPercentualJurosFinanciamento(Boolean selPercentualJurosFinanciamento) {
		this.selPercentualJurosFinanciamento = selPercentualJurosFinanciamento;
	}
	public Boolean getObgPercentualJurosFinanciamento() {
		return obgPercentualJurosFinanciamento;
	}
	public void setObgPercentualJurosFinanciamento(Boolean obgPercentualJurosFinanciamento) {
		this.obgPercentualJurosFinanciamento = obgPercentualJurosFinanciamento;
	}
	public Boolean getSelSistemaAmortizacao() {
		return selSistemaAmortizacao;
	}
	public void setSelSistemaAmortizacao(Boolean selSistemaAmortizacao) {
		this.selSistemaAmortizacao = selSistemaAmortizacao;
	}
	public Boolean getObgSistemaAmortizacao() {
		return obgSistemaAmortizacao;
	}
	public void setObgSistemaAmortizacao(Boolean obgSistemaAmortizacao) {
		this.obgSistemaAmortizacao = obgSistemaAmortizacao;
	}
	public Boolean getSelPercentualTarifaDoP() {
		return selPercentualTarifaDoP;
	}
	public void setSelPercentualTarifaDoP(Boolean selPercentualTarifaDoP) {
		this.selPercentualTarifaDoP = selPercentualTarifaDoP;
	}
	public Boolean getObgPercentualTarifaDoP() {
		return obgPercentualTarifaDoP;
	}
	public void setObgPercentualTarifaDoP(Boolean obgPercentualTarifaDoP) {
		this.obgPercentualTarifaDoP = obgPercentualTarifaDoP;
	}
	public Boolean getValorFixoPercentualTarifaDoP() {
		return valorFixoPercentualTarifaDoP;
	}
	public void setValorFixoPercentualTarifaDoP(Boolean valorFixoPercentualTarifaDoP) {
		this.valorFixoPercentualTarifaDoP = valorFixoPercentualTarifaDoP;
	}
	public Boolean getSelPercentualMulta() {
		return selPercentualMulta;
	}
	public void setSelPercentualMulta(Boolean selPercentualMulta) {
		this.selPercentualMulta = selPercentualMulta;
	}
	public Boolean getObgPercentualMulta() {
		return obgPercentualMulta;
	}
	public void setObgPercentualMulta(Boolean obgPercentualMulta) {
		this.obgPercentualMulta = obgPercentualMulta;
	}
	public Boolean getValorFixoPercentualMulta() {
		return valorFixoPercentualMulta;
	}
	public void setValorFixoPercentualMulta(Boolean valorFixoPercentualMulta) {
		this.valorFixoPercentualMulta = valorFixoPercentualMulta;
	}
	public Boolean getSelPercentualJurosMora() {
		return selPercentualJurosMora;
	}
	public void setSelPercentualJurosMora(Boolean selPercentualJurosMora) {
		this.selPercentualJurosMora = selPercentualJurosMora;
	}
	public Boolean getObgPercentualJurosMora() {
		return obgPercentualJurosMora;
	}
	public void setObgPercentualJurosMora(Boolean obgPercentualJurosMora) {
		this.obgPercentualJurosMora = obgPercentualJurosMora;
	}
	public Boolean getValorFixoPercentualJurosMora() {
		return valorFixoPercentualJurosMora;
	}
	public void setValorFixoPercentualJurosMora(Boolean valorFixoPercentualJurosMora) {
		this.valorFixoPercentualJurosMora = valorFixoPercentualJurosMora;
	}
	public Boolean getSelTipoAgrupamento() {
		return selTipoAgrupamento;
	}
	public void setSelTipoAgrupamento(Boolean selTipoAgrupamento) {
		this.selTipoAgrupamento = selTipoAgrupamento;
	}
	public Boolean getObgTipoAgrupamento() {
		return obgTipoAgrupamento;
	}
	public void setObgTipoAgrupamento(Boolean obgTipoAgrupamento) {
		this.obgTipoAgrupamento = obgTipoAgrupamento;
	}
	public Boolean getSelContratoCompra() {
		return selContratoCompra;
	}
	public void setSelContratoCompra(Boolean selContratoCompra) {
		this.selContratoCompra = selContratoCompra;
	}
	public Boolean getObgContratoCompra() {
		return obgContratoCompra;
	}
	public void setObgContratoCompra(Boolean obgContratoCompra) {
		this.obgContratoCompra = obgContratoCompra;
	}
	public Boolean getValorFixoContratoCompra() {
		return valorFixoContratoCompra;
	}
	public void setValorFixoContratoCompra(Boolean valorFixoContratoCompra) {
		this.valorFixoContratoCompra = valorFixoContratoCompra;
	}
	public Boolean getSelPercentualReferenciaSobreDem() {
		return selPercentualReferenciaSobreDem;
	}
	public void setSelPercentualReferenciaSobreDem(Boolean selPercentualReferenciaSobreDem) {
		this.selPercentualReferenciaSobreDem = selPercentualReferenciaSobreDem;
	}
	public Boolean getObgPercentualReferenciaSobreDem() {
		return obgPercentualReferenciaSobreDem;
	}
	public void setObgPercentualReferenciaSobreDem(Boolean obgPercentualReferenciaSobreDem) {
		this.obgPercentualReferenciaSobreDem = obgPercentualReferenciaSobreDem;
	}
	public Boolean getSelPercentualReferenciaSobDem() {
		return selPercentualReferenciaSobDem;
	}
	public void setSelPercentualReferenciaSobDem(Boolean selPercentualReferenciaSobDem) {
		this.selPercentualReferenciaSobDem = selPercentualReferenciaSobDem;
	}
	public Boolean getObgPercentualReferenciaSobDem() {
		return obgPercentualReferenciaSobDem;
	}
	public void setObgPercentualReferenciaSobDem(Boolean obgPercentualReferenciaSobDem) {
		this.obgPercentualReferenciaSobDem = obgPercentualReferenciaSobDem;
	}
	public Boolean getSelPercentualSobreTariGas() {
		return selPercentualSobreTariGas;
	}
	public void setSelPercentualSobreTariGas(Boolean selPercentualSobreTariGas) {
		this.selPercentualSobreTariGas = selPercentualSobreTariGas;
	}
	public Boolean getObgPercentualSobreTariGas() {
		return obgPercentualSobreTariGas;
	}
	public void setObgPercentualSobreTariGas(Boolean obgPercentualSobreTariGas) {
		this.obgPercentualSobreTariGas = obgPercentualSobreTariGas;
	}
	public Boolean getValorFixoPercentualSobreTariGas() {
		return valorFixoPercentualSobreTariGas;
	}
	public void setValorFixoPercentualSobreTariGas(Boolean valorFixoPercentualSobreTariGas) {
		this.valorFixoPercentualSobreTariGas = valorFixoPercentualSobreTariGas;
	}
	public Boolean getSelBaseApuracaoRetMaior() {
		return selBaseApuracaoRetMaior;
	}
	public void setSelBaseApuracaoRetMaior(Boolean selBaseApuracaoRetMaior) {
		this.selBaseApuracaoRetMaior = selBaseApuracaoRetMaior;
	}
	public Boolean getObgBaseApuracaoRetMaior() {
		return obgBaseApuracaoRetMaior;
	}
	public void setObgBaseApuracaoRetMaior(Boolean obgBaseApuracaoRetMaior) {
		this.obgBaseApuracaoRetMaior = obgBaseApuracaoRetMaior;
	}
	public Boolean getSelBaseApuracaoRetMenor() {
		return selBaseApuracaoRetMenor;
	}
	public void setSelBaseApuracaoRetMenor(Boolean selBaseApuracaoRetMenor) {
		this.selBaseApuracaoRetMenor = selBaseApuracaoRetMenor;
	}
	public Boolean getObgBaseApuracaoRetMenor() {
		return obgBaseApuracaoRetMenor;
	}
	public void setObgBaseApuracaoRetMenor(Boolean obgBaseApuracaoRetMenor) {
		this.obgBaseApuracaoRetMenor = obgBaseApuracaoRetMenor;
	}
	public Boolean getSelMultaRecisoria() {
		return selMultaRecisoria;
	}
	public void setSelMultaRecisoria(Boolean selMultaRecisoria) {
		this.selMultaRecisoria = selMultaRecisoria;
	}
	public Boolean getObgMultaRecisoria() {
		return obgMultaRecisoria;
	}
	public void setObgMultaRecisoria(Boolean obgMultaRecisoria) {
		this.obgMultaRecisoria = obgMultaRecisoria;
	}
	public Boolean getValorFixoMultaRecisoria() {
		return valorFixoMultaRecisoria;
	}
	public void setValorFixoMultaRecisoria(Boolean valorFixoMultaRecisoria) {
		this.valorFixoMultaRecisoria = valorFixoMultaRecisoria;
	}
	public Boolean getSelValorInvestimento() {
		return selValorInvestimento;
	}
	public void setSelValorInvestimento(Boolean selValorInvestimento) {
		this.selValorInvestimento = selValorInvestimento;
	}
	public Boolean getObgValorInvestimento() {
		return obgValorInvestimento;
	}
	public void setObgValorInvestimento(Boolean obgValorInvestimento) {
		this.obgValorInvestimento = obgValorInvestimento;
	}
	public Boolean getSelDataInvestimento() {
		return selDataInvestimento;
	}
	public void setSelDataInvestimento(Boolean selDataInvestimento) {
		this.selDataInvestimento = selDataInvestimento;
	}
	public Boolean getObgDataInvestimento() {
		return obgDataInvestimento;
	}
	public void setObgDataInvestimento(Boolean obgDataInvestimento) {
		this.obgDataInvestimento = obgDataInvestimento;
	}
	public Boolean getSelDataRecisao() {
		return selDataRecisao;
	}
	public void setSelDataRecisao(Boolean selDataRecisao) {
		this.selDataRecisao = selDataRecisao;
	}
	public Boolean getObgDataRecisao() {
		return obgDataRecisao;
	}
	public void setObgDataRecisao(Boolean obgDataRecisao) {
		this.obgDataRecisao = obgDataRecisao;
	}
	public Boolean getSelDescricaoContrato() {
		return selDescricaoContrato;
	}
	public void setSelDescricaoContrato(Boolean selDescricaoContrato) {
		this.selDescricaoContrato = selDescricaoContrato;
	}
	public Boolean getObgDescricaoContrato() {
		return obgDescricaoContrato;
	}
	public void setObgDescricaoContrato(Boolean obgDescricaoContrato) {
		this.obgDescricaoContrato = obgDescricaoContrato;
	}
	public Boolean getSelVolumeReferencia() {
		return selVolumeReferencia;
	}
	public void setSelVolumeReferencia(Boolean selVolumeReferencia) {
		this.selVolumeReferencia = selVolumeReferencia;
	}
	public Boolean getObgVolumeReferencia() {
		return obgVolumeReferencia;
	}
	public void setObgVolumeReferencia(Boolean obgVolumeReferencia) {
		this.obgVolumeReferencia = obgVolumeReferencia;
	}
	public Boolean getSelDocumentosAnexos() {
		return selDocumentosAnexos;
	}
	public void setSelDocumentosAnexos(Boolean selDocumentosAnexos) {
		this.selDocumentosAnexos = selDocumentosAnexos;
	}
	public Boolean getObgDocumentosAnexos() {
		return obgDocumentosAnexos;
	}
	public void setObgDocumentosAnexos(Boolean obgDocumentosAnexos) {
		this.obgDocumentosAnexos = obgDocumentosAnexos;
	}
	public Boolean getSelPeriodicidadeTakeOrPayC() {
		return selPeriodicidadeTakeOrPayC;
	}
	public void setSelPeriodicidadeTakeOrPayC(Boolean selPeriodicidadeTakeOrPayC) {
		this.selPeriodicidadeTakeOrPayC = selPeriodicidadeTakeOrPayC;
	}
	public Boolean getObgPeriodicidadeTakeOrPayC() {
		return obgPeriodicidadeTakeOrPayC;
	}
	public void setObgPeriodicidadeTakeOrPayC(Boolean obgPeriodicidadeTakeOrPayC) {
		this.obgPeriodicidadeTakeOrPayC = obgPeriodicidadeTakeOrPayC;
	}
	public Boolean getValorFixoPeriodicidadeTakeOrPayC() {
		return valorFixoPeriodicidadeTakeOrPayC;
	}
	public void setValorFixoPeriodicidadeTakeOrPayC(Boolean valorFixoPeriodicidadeTakeOrPayC) {
		this.valorFixoPeriodicidadeTakeOrPayC = valorFixoPeriodicidadeTakeOrPayC;
	}
	public Boolean getSelConsumoReferenciaTakeOrPayC() {
		return selConsumoReferenciaTakeOrPayC;
	}
	public void setSelConsumoReferenciaTakeOrPayC(Boolean selConsumoReferenciaTakeOrPayC) {
		this.selConsumoReferenciaTakeOrPayC = selConsumoReferenciaTakeOrPayC;
	}
	public Boolean getObgConsumoReferenciaTakeOrPayC() {
		return obgConsumoReferenciaTakeOrPayC;
	}
	public void setObgConsumoReferenciaTakeOrPayC(Boolean obgConsumoReferenciaTakeOrPayC) {
		this.obgConsumoReferenciaTakeOrPayC = obgConsumoReferenciaTakeOrPayC;
	}
	public Boolean getValorFixoConsumoReferenciaTakeOrPayC() {
		return valorFixoConsumoReferenciaTakeOrPayC;
	}
	public void setValorFixoConsumoReferenciaTakeOrPayC(Boolean valorFixoConsumoReferenciaTakeOrPayC) {
		this.valorFixoConsumoReferenciaTakeOrPayC = valorFixoConsumoReferenciaTakeOrPayC;
	}
	public Boolean getSelMargemVariacaoTakeOrPayC() {
		return selMargemVariacaoTakeOrPayC;
	}
	public void setSelMargemVariacaoTakeOrPayC(Boolean selMargemVariacaoTakeOrPayC) {
		this.selMargemVariacaoTakeOrPayC = selMargemVariacaoTakeOrPayC;
	}
	public Boolean getObgMargemVariacaoTakeOrPayC() {
		return obgMargemVariacaoTakeOrPayC;
	}
	public void setObgMargemVariacaoTakeOrPayC(Boolean obgMargemVariacaoTakeOrPayC) {
		this.obgMargemVariacaoTakeOrPayC = obgMargemVariacaoTakeOrPayC;
	}
	public Boolean getValorFixoMargemVariacaoTakeOrPayC() {
		return valorFixoMargemVariacaoTakeOrPayC;
	}
	public void setValorFixoMargemVariacaoTakeOrPayC(Boolean valorFixoMargemVariacaoTakeOrPayC) {
		this.valorFixoMargemVariacaoTakeOrPayC = valorFixoMargemVariacaoTakeOrPayC;
	}
	public Boolean getSelRecuperacaoAutoTakeOrPayC() {
		return selRecuperacaoAutoTakeOrPayC;
	}
	public void setSelRecuperacaoAutoTakeOrPayC(Boolean selRecuperacaoAutoTakeOrPayC) {
		this.selRecuperacaoAutoTakeOrPayC = selRecuperacaoAutoTakeOrPayC;
	}
	public Boolean getObgRecuperacaoAutoTakeOrPayC() {
		return obgRecuperacaoAutoTakeOrPayC;
	}
	public void setObgRecuperacaoAutoTakeOrPayC(Boolean obgRecuperacaoAutoTakeOrPayC) {
		this.obgRecuperacaoAutoTakeOrPayC = obgRecuperacaoAutoTakeOrPayC;
	}
	public Boolean getSelReferenciaQFParadaProgramadaC() {
		return selReferenciaQFParadaProgramadaC;
	}
	public void setSelReferenciaQFParadaProgramadaC(Boolean selReferenciaQFParadaProgramadaC) {
		this.selReferenciaQFParadaProgramadaC = selReferenciaQFParadaProgramadaC;
	}
	public Boolean getObgReferenciaQFParadaProgramadaC() {
		return obgReferenciaQFParadaProgramadaC;
	}
	public void setObgReferenciaQFParadaProgramadaC(Boolean obgReferenciaQFParadaProgramadaC) {
		this.obgReferenciaQFParadaProgramadaC = obgReferenciaQFParadaProgramadaC;
	}
	public Boolean getValorFixoReferenciaQFParadaProgramadaC() {
		return valorFixoReferenciaQFParadaProgramadaC;
	}
	public void setValorFixoReferenciaQFParadaProgramadaC(Boolean valorFixoReferenciaQFParadaProgramadaC) {
		this.valorFixoReferenciaQFParadaProgramadaC = valorFixoReferenciaQFParadaProgramadaC;
	}
	public Boolean getSelTipoAgrupamentoContrato() {
		return selTipoAgrupamentoContrato;
	}
	public void setSelTipoAgrupamentoContrato(Boolean selTipoAgrupamentoContrato) {
		this.selTipoAgrupamentoContrato = selTipoAgrupamentoContrato;
	}
	public Boolean getObgTipoAgrupamentoContrato() {
		return obgTipoAgrupamentoContrato;
	}
	public void setObgTipoAgrupamentoContrato(Boolean obgTipoAgrupamentoContrato) {
		this.obgTipoAgrupamentoContrato = obgTipoAgrupamentoContrato;
	}
	public Boolean getValorFixoTipoAgrupamento() {
		return valorFixoTipoAgrupamento;
	}
	public void setValorFixoTipoAgrupamento(Boolean valorFixoTipoAgrupamento) {
		this.valorFixoTipoAgrupamento = valorFixoTipoAgrupamento;
	}
	public Boolean getSelPercentualMargemVariacaoC() {
		return selPercentualMargemVariacaoC;
	}
	public void setSelPercentualMargemVariacaoC(Boolean selPercentualMargemVariacaoC) {
		this.selPercentualMargemVariacaoC = selPercentualMargemVariacaoC;
	}
	public Boolean getSelPeriodicidadePenalidadeC() {
		return selPeriodicidadePenalidadeC;
	}
	public void setSelPeriodicidadePenalidadeC(Boolean selPeriodicidadePenalidadeC) {
		this.selPeriodicidadePenalidadeC = selPeriodicidadePenalidadeC;
	}
	public Boolean getSelConsumoReferenciaC() {
		return selConsumoReferenciaC;
	}
	public void setSelConsumoReferenciaC(Boolean selConsumoReferenciaC) {
		this.selConsumoReferenciaC = selConsumoReferenciaC;
	}
	public Boolean getSelPenalidadeC() {
		return selPenalidadeC;
	}
	public void setSelPenalidadeC(Boolean selPenalidadeC) {
		this.selPenalidadeC = selPenalidadeC;
	}
	public Boolean getSelDataInicioVigenciaC() {
		return selDataInicioVigenciaC;
	}
	public void setSelDataInicioVigenciaC(Boolean selDataInicioVigenciaC) {
		this.selDataInicioVigenciaC = selDataInicioVigenciaC;
	}
	public Boolean getSelDataFimVigenciaC() {
		return selDataFimVigenciaC;
	}
	public void setSelDataFimVigenciaC(Boolean selDataFimVigenciaC) {
		this.selDataFimVigenciaC = selDataFimVigenciaC;
	}
	public Boolean getSelPercentualNaoRecuperavelC() {
		return selPercentualNaoRecuperavelC;
	}
	public void setSelPercentualNaoRecuperavelC(Boolean selPercentualNaoRecuperavelC) {
		this.selPercentualNaoRecuperavelC = selPercentualNaoRecuperavelC;
	}
	public Boolean getSelConsideraParadaProgramadaC() {
		return selConsideraParadaProgramadaC;
	}
	public void setSelConsideraParadaProgramadaC(Boolean selConsideraParadaProgramadaC) {
		this.selConsideraParadaProgramadaC = selConsideraParadaProgramadaC;
	}
	public Boolean getSelConsideraFalhaFornecimentoC() {
		return selConsideraFalhaFornecimentoC;
	}
	public void setSelConsideraFalhaFornecimentoC(Boolean selConsideraFalhaFornecimentoC) {
		this.selConsideraFalhaFornecimentoC = selConsideraFalhaFornecimentoC;
	}
	public Boolean getSelConsideraCasoFortuitoC() {
		return selConsideraCasoFortuitoC;
	}
	public void setSelConsideraCasoFortuitoC(Boolean selConsideraCasoFortuitoC) {
		this.selConsideraCasoFortuitoC = selConsideraCasoFortuitoC;
	}
	public Boolean getSelRecuperavelC() {
		return selRecuperavelC;
	}
	public void setSelRecuperavelC(Boolean selRecuperavelC) {
		this.selRecuperavelC = selRecuperavelC;
	}
	public Boolean getSelTipoApuracaoC() {
		return selTipoApuracaoC;
	}
	public void setSelTipoApuracaoC(Boolean selTipoApuracaoC) {
		this.selTipoApuracaoC = selTipoApuracaoC;
	}
	public Boolean getSelPrecoCobrancaC() {
		return selPrecoCobrancaC;
	}
	public void setSelPrecoCobrancaC(Boolean selPrecoCobrancaC) {
		this.selPrecoCobrancaC = selPrecoCobrancaC;
	}
	public Boolean getSelApuracaoParadaProgramadaC() {
		return selApuracaoParadaProgramadaC;
	}
	public void setSelApuracaoParadaProgramadaC(Boolean selApuracaoParadaProgramadaC) {
		this.selApuracaoParadaProgramadaC = selApuracaoParadaProgramadaC;
	}
	public Boolean getSelApuracaoCasoFortuitoC() {
		return selApuracaoCasoFortuitoC;
	}
	public void setSelApuracaoCasoFortuitoC(Boolean selApuracaoCasoFortuitoC) {
		this.selApuracaoCasoFortuitoC = selApuracaoCasoFortuitoC;
	}
	public Boolean getSelApuracaoFalhaFornecimentoC() {
		return selApuracaoFalhaFornecimentoC;
	}
	public void setSelApuracaoFalhaFornecimentoC(Boolean selApuracaoFalhaFornecimentoC) {
		this.selApuracaoFalhaFornecimentoC = selApuracaoFalhaFornecimentoC;
	}
	public Boolean getObgPercentualMargemVariacaoC() {
		return obgPercentualMargemVariacaoC;
	}
	public void setObgPercentualMargemVariacaoC(Boolean obgPercentualMargemVariacaoC) {
		this.obgPercentualMargemVariacaoC = obgPercentualMargemVariacaoC;
	}
	public Boolean getObgPeriodicidadePenalidadeC() {
		return obgPeriodicidadePenalidadeC;
	}
	public void setObgPeriodicidadePenalidadeC(Boolean obgPeriodicidadePenalidadeC) {
		this.obgPeriodicidadePenalidadeC = obgPeriodicidadePenalidadeC;
	}
	public Boolean getObgConsumoReferenciaC() {
		return obgConsumoReferenciaC;
	}
	public void setObgConsumoReferenciaC(Boolean obgConsumoReferenciaC) {
		this.obgConsumoReferenciaC = obgConsumoReferenciaC;
	}
	public Boolean getObgPenalidadeC() {
		return obgPenalidadeC;
	}
	public void setObgPenalidadeC(Boolean obgPenalidadeC) {
		this.obgPenalidadeC = obgPenalidadeC;
	}
	public Boolean getObgDataInicioVigenciaC() {
		return obgDataInicioVigenciaC;
	}
	public void setObgDataInicioVigenciaC(Boolean obgDataInicioVigenciaC) {
		this.obgDataInicioVigenciaC = obgDataInicioVigenciaC;
	}
	public Boolean getObgDataFimVigenciaC() {
		return obgDataFimVigenciaC;
	}
	public void setObgDataFimVigenciaC(Boolean obgDataFimVigenciaC) {
		this.obgDataFimVigenciaC = obgDataFimVigenciaC;
	}
	public Boolean getObgPercentualNaoRecuperavelC() {
		return obgPercentualNaoRecuperavelC;
	}
	public void setObgPercentualNaoRecuperavelC(Boolean obgPercentualNaoRecuperavelC) {
		this.obgPercentualNaoRecuperavelC = obgPercentualNaoRecuperavelC;
	}
	public Boolean getObgConsideraParadaProgramadaC() {
		return obgConsideraParadaProgramadaC;
	}
	public void setObgConsideraParadaProgramadaC(Boolean obgConsideraParadaProgramadaC) {
		this.obgConsideraParadaProgramadaC = obgConsideraParadaProgramadaC;
	}
	public Boolean getObgConsideraFalhaFornecimentoC() {
		return obgConsideraFalhaFornecimentoC;
	}
	public void setObgConsideraFalhaFornecimentoC(Boolean obgConsideraFalhaFornecimentoC) {
		this.obgConsideraFalhaFornecimentoC = obgConsideraFalhaFornecimentoC;
	}
	public Boolean getObgConsideraCasoFortuitoC() {
		return obgConsideraCasoFortuitoC;
	}
	public void setObgConsideraCasoFortuitoC(Boolean obgConsideraCasoFortuitoC) {
		this.obgConsideraCasoFortuitoC = obgConsideraCasoFortuitoC;
	}
	public Boolean getObgRecuperavelC() {
		return obgRecuperavelC;
	}
	public void setObgRecuperavelC(Boolean obgRecuperavelC) {
		this.obgRecuperavelC = obgRecuperavelC;
	}
	public Boolean getObgTipoApuracaoC() {
		return obgTipoApuracaoC;
	}
	public void setObgTipoApuracaoC(Boolean obgTipoApuracaoC) {
		this.obgTipoApuracaoC = obgTipoApuracaoC;
	}
	public Boolean getObgPrecoCobrancaC() {
		return obgPrecoCobrancaC;
	}
	public void setObgPrecoCobrancaC(Boolean obgPrecoCobrancaC) {
		this.obgPrecoCobrancaC = obgPrecoCobrancaC;
	}
	public Boolean getObgApuracaoParadaProgramadaC() {
		return obgApuracaoParadaProgramadaC;
	}
	public void setObgApuracaoParadaProgramadaC(Boolean obgApuracaoParadaProgramadaC) {
		this.obgApuracaoParadaProgramadaC = obgApuracaoParadaProgramadaC;
	}
	public Boolean getObgApuracaoCasoFortuitoC() {
		return obgApuracaoCasoFortuitoC;
	}
	public void setObgApuracaoCasoFortuitoC(Boolean obgApuracaoCasoFortuitoC) {
		this.obgApuracaoCasoFortuitoC = obgApuracaoCasoFortuitoC;
	}
	public Boolean getObgApuracaoFalhaFornecimentoC() {
		return obgApuracaoFalhaFornecimentoC;
	}
	public void setObgApuracaoFalhaFornecimentoC(Boolean obgApuracaoFalhaFornecimentoC) {
		this.obgApuracaoFalhaFornecimentoC = obgApuracaoFalhaFornecimentoC;
	}
	public Boolean getValorFixoPercentualNaoRecuperavelC() {
		return valorFixoPercentualNaoRecuperavelC;
	}
	public void setValorFixoPercentualNaoRecuperavelC(Boolean valorFixoPercentualNaoRecuperavelC) {
		this.valorFixoPercentualNaoRecuperavelC = valorFixoPercentualNaoRecuperavelC;
	}
	public Boolean getValorFixoConsideraParadaProgramadaC() {
		return valorFixoConsideraParadaProgramadaC;
	}
	public void setValorFixoConsideraParadaProgramadaC(Boolean valorFixoConsideraParadaProgramadaC) {
		this.valorFixoConsideraParadaProgramadaC = valorFixoConsideraParadaProgramadaC;
	}
	public Boolean getValorFixoConsideraFalhaFornecimentoC() {
		return valorFixoConsideraFalhaFornecimentoC;
	}
	public void setValorFixoConsideraFalhaFornecimentoC(Boolean valorFixoConsideraFalhaFornecimentoC) {
		this.valorFixoConsideraFalhaFornecimentoC = valorFixoConsideraFalhaFornecimentoC;
	}
	public Boolean getValorFixoConsideraCasoFortuitoC() {
		return valorFixoConsideraCasoFortuitoC;
	}
	public void setValorFixoConsideraCasoFortuitoC(Boolean valorFixoConsideraCasoFortuitoC) {
		this.valorFixoConsideraCasoFortuitoC = valorFixoConsideraCasoFortuitoC;
	}
	public Boolean getValorFixoRecuperavelC() {
		return valorFixoRecuperavelC;
	}
	public void setValorFixoRecuperavelC(Boolean valorFixoRecuperavelC) {
		this.valorFixoRecuperavelC = valorFixoRecuperavelC;
	}
	public Boolean getValorFixoTipoApuracaoC() {
		return valorFixoTipoApuracaoC;
	}
	public void setValorFixoTipoApuracaoC(Boolean valorFixoTipoApuracaoC) {
		this.valorFixoTipoApuracaoC = valorFixoTipoApuracaoC;
	}
	public Boolean getValorFixoPrecoCobrancaC() {
		return valorFixoPrecoCobrancaC;
	}
	public void setValorFixoPrecoCobrancaC(Boolean valorFixoPrecoCobrancaC) {
		this.valorFixoPrecoCobrancaC = valorFixoPrecoCobrancaC;
	}
	public Boolean getValorFixoApuracaoParadaProgramadaC() {
		return valorFixoApuracaoParadaProgramadaC;
	}
	public void setValorFixoApuracaoParadaProgramadaC(Boolean valorFixoApuracaoParadaProgramadaC) {
		this.valorFixoApuracaoParadaProgramadaC = valorFixoApuracaoParadaProgramadaC;
	}
	public Boolean getValorFixoApuracaoCasoFortuitoC() {
		return valorFixoApuracaoCasoFortuitoC;
	}
	public void setValorFixoApuracaoCasoFortuitoC(Boolean valorFixoApuracaoCasoFortuitoC) {
		this.valorFixoApuracaoCasoFortuitoC = valorFixoApuracaoCasoFortuitoC;
	}
	public Boolean getValorFixoApuracaoFalhaFornecimentoC() {
		return valorFixoApuracaoFalhaFornecimentoC;
	}
	public void setValorFixoApuracaoFalhaFornecimentoC(Boolean valorFixoApuracaoFalhaFornecimentoC) {
		this.valorFixoApuracaoFalhaFornecimentoC = valorFixoApuracaoFalhaFornecimentoC;
	}
	public Boolean getSelPenalidadeRetMaiorMenor() {
		return selPenalidadeRetMaiorMenor;
	}
	public void setSelPenalidadeRetMaiorMenor(Boolean selPenalidadeRetMaiorMenor) {
		this.selPenalidadeRetMaiorMenor = selPenalidadeRetMaiorMenor;
	}
	public Boolean getObgPenalidadeRetMaiorMenor() {
		return obgPenalidadeRetMaiorMenor;
	}
	public void setObgPenalidadeRetMaiorMenor(Boolean obgPenalidadeRetMaiorMenor) {
		this.obgPenalidadeRetMaiorMenor = obgPenalidadeRetMaiorMenor;
	}
	public Boolean getValorFixoPenalidadeRetMaiorMenor() {
		return valorFixoPenalidadeRetMaiorMenor;
	}
	public void setValorFixoPenalidadeRetMaiorMenor(Boolean valorFixoPenalidadeRetMaiorMenor) {
		this.valorFixoPenalidadeRetMaiorMenor = valorFixoPenalidadeRetMaiorMenor;
	}
	public Boolean getSelPenalidadeRetMaiorMenorM() {
		return selPenalidadeRetMaiorMenorM;
	}
	public void setSelPenalidadeRetMaiorMenorM(Boolean selPenalidadeRetMaiorMenorM) {
		this.selPenalidadeRetMaiorMenorM = selPenalidadeRetMaiorMenorM;
	}
	public Boolean getObgPenalidadeRetMaiorMenorM() {
		return obgPenalidadeRetMaiorMenorM;
	}
	public void setObgPenalidadeRetMaiorMenorM(Boolean obgPenalidadeRetMaiorMenorM) {
		this.obgPenalidadeRetMaiorMenorM = obgPenalidadeRetMaiorMenorM;
	}
	public Boolean getSelPeriodicidadeRetMaiorMenor() {
		return selPeriodicidadeRetMaiorMenor;
	}
	public void setSelPeriodicidadeRetMaiorMenor(Boolean selPeriodicidadeRetMaiorMenor) {
		this.selPeriodicidadeRetMaiorMenor = selPeriodicidadeRetMaiorMenor;
	}
	public Boolean getObgPeriodicidadeRetMaiorMenor() {
		return obgPeriodicidadeRetMaiorMenor;
	}
	public void setObgPeriodicidadeRetMaiorMenor(Boolean obgPeriodicidadeRetMaiorMenor) {
		this.obgPeriodicidadeRetMaiorMenor = obgPeriodicidadeRetMaiorMenor;
	}
	public Boolean getValorFixoPeriodicidadeRetMaiorMenor() {
		return valorFixoPeriodicidadeRetMaiorMenor;
	}
	public void setValorFixoPeriodicidadeRetMaiorMenor(Boolean valorFixoPeriodicidadeRetMaiorMenor) {
		this.valorFixoPeriodicidadeRetMaiorMenor = valorFixoPeriodicidadeRetMaiorMenor;
	}
	public Boolean getSelPeriodicidadeRetMaiorMenorM() {
		return selPeriodicidadeRetMaiorMenorM;
	}
	public void setSelPeriodicidadeRetMaiorMenorM(Boolean selPeriodicidadeRetMaiorMenorM) {
		this.selPeriodicidadeRetMaiorMenorM = selPeriodicidadeRetMaiorMenorM;
	}
	public Boolean getObgPeriodicidadeRetMaiorMenorM() {
		return obgPeriodicidadeRetMaiorMenorM;
	}
	public void setObgPeriodicidadeRetMaiorMenorM(Boolean obgPeriodicidadeRetMaiorMenorM) {
		this.obgPeriodicidadeRetMaiorMenorM = obgPeriodicidadeRetMaiorMenorM;
	}
	public Boolean getSelBaseApuracaoRetMaiorMenor() {
		return selBaseApuracaoRetMaiorMenor;
	}
	public void setSelBaseApuracaoRetMaiorMenor(Boolean selBaseApuracaoRetMaiorMenor) {
		this.selBaseApuracaoRetMaiorMenor = selBaseApuracaoRetMaiorMenor;
	}
	public Boolean getObgBaseApuracaoRetMaiorMenor() {
		return obgBaseApuracaoRetMaiorMenor;
	}
	public void setObgBaseApuracaoRetMaiorMenor(Boolean obgBaseApuracaoRetMaiorMenor) {
		this.obgBaseApuracaoRetMaiorMenor = obgBaseApuracaoRetMaiorMenor;
	}
	public Boolean getValorFixoBaseApuracaoRetMaiorMenor() {
		return valorFixoBaseApuracaoRetMaiorMenor;
	}
	public void setValorFixoBaseApuracaoRetMaiorMenor(Boolean valorFixoBaseApuracaoRetMaiorMenor) {
		this.valorFixoBaseApuracaoRetMaiorMenor = valorFixoBaseApuracaoRetMaiorMenor;
	}
	public Boolean getSelBaseApuracaoRetMaiorMenorM() {
		return selBaseApuracaoRetMaiorMenorM;
	}
	public void setSelBaseApuracaoRetMaiorMenorM(Boolean selBaseApuracaoRetMaiorMenorM) {
		this.selBaseApuracaoRetMaiorMenorM = selBaseApuracaoRetMaiorMenorM;
	}
	public Boolean getObgBaseApuracaoRetMaiorMenorM() {
		return obgBaseApuracaoRetMaiorMenorM;
	}
	public void setObgBaseApuracaoRetMaiorMenorM(Boolean obgBaseApuracaoRetMaiorMenorM) {
		this.obgBaseApuracaoRetMaiorMenorM = obgBaseApuracaoRetMaiorMenorM;
	}
	public Boolean getSelDataIniVigRetirMaiorMenor() {
		return selDataIniVigRetirMaiorMenor;
	}
	public void setSelDataIniVigRetirMaiorMenor(Boolean selDataIniVigRetirMaiorMenor) {
		this.selDataIniVigRetirMaiorMenor = selDataIniVigRetirMaiorMenor;
	}
	public Boolean getObgDataIniVigRetirMaiorMenor() {
		return obgDataIniVigRetirMaiorMenor;
	}
	public void setObgDataIniVigRetirMaiorMenor(Boolean obgDataIniVigRetirMaiorMenor) {
		this.obgDataIniVigRetirMaiorMenor = obgDataIniVigRetirMaiorMenor;
	}
	public Boolean getSelDataIniVigRetirMaiorMenorM() {
		return selDataIniVigRetirMaiorMenorM;
	}
	public void setSelDataIniVigRetirMaiorMenorM(Boolean selDataIniVigRetirMaiorMenorM) {
		this.selDataIniVigRetirMaiorMenorM = selDataIniVigRetirMaiorMenorM;
	}
	public Boolean getObgDataIniVigRetirMaiorMenorM() {
		return obgDataIniVigRetirMaiorMenorM;
	}
	public void setObgDataIniVigRetirMaiorMenorM(Boolean obgDataIniVigRetirMaiorMenorM) {
		this.obgDataIniVigRetirMaiorMenorM = obgDataIniVigRetirMaiorMenorM;
	}
	public Boolean getSelDataFimVigRetirMaiorMenor() {
		return selDataFimVigRetirMaiorMenor;
	}
	public void setSelDataFimVigRetirMaiorMenor(Boolean selDataFimVigRetirMaiorMenor) {
		this.selDataFimVigRetirMaiorMenor = selDataFimVigRetirMaiorMenor;
	}
	public Boolean getObgDataFimVigRetirMaiorMenor() {
		return obgDataFimVigRetirMaiorMenor;
	}
	public void setObgDataFimVigRetirMaiorMenor(Boolean obgDataFimVigRetirMaiorMenor) {
		this.obgDataFimVigRetirMaiorMenor = obgDataFimVigRetirMaiorMenor;
	}
	public Boolean getSelDataFimVigRetirMaiorMenorM() {
		return selDataFimVigRetirMaiorMenorM;
	}
	public void setSelDataFimVigRetirMaiorMenorM(Boolean selDataFimVigRetirMaiorMenorM) {
		this.selDataFimVigRetirMaiorMenorM = selDataFimVigRetirMaiorMenorM;
	}
	public Boolean getObgDataFimVigRetirMaiorMenorM() {
		return obgDataFimVigRetirMaiorMenorM;
	}
	public void setObgDataFimVigRetirMaiorMenorM(Boolean obgDataFimVigRetirMaiorMenorM) {
		this.obgDataFimVigRetirMaiorMenorM = obgDataFimVigRetirMaiorMenorM;
	}
	public Boolean getSelPrecoCobrancaRetirMaiorMenor() {
		return selPrecoCobrancaRetirMaiorMenor;
	}
	public void setSelPrecoCobrancaRetirMaiorMenor(Boolean selPrecoCobrancaRetirMaiorMenor) {
		this.selPrecoCobrancaRetirMaiorMenor = selPrecoCobrancaRetirMaiorMenor;
	}
	public Boolean getObgPrecoCobrancaRetirMaiorMenor() {
		return obgPrecoCobrancaRetirMaiorMenor;
	}
	public void setObgPrecoCobrancaRetirMaiorMenor(Boolean obgPrecoCobrancaRetirMaiorMenor) {
		this.obgPrecoCobrancaRetirMaiorMenor = obgPrecoCobrancaRetirMaiorMenor;
	}
	public Boolean getValorFixoPrecoCobrancaRetirMaiorMenor() {
		return valorFixoPrecoCobrancaRetirMaiorMenor;
	}
	public void setValorFixoPrecoCobrancaRetirMaiorMenor(Boolean valorFixoPrecoCobrancaRetirMaiorMenor) {
		this.valorFixoPrecoCobrancaRetirMaiorMenor = valorFixoPrecoCobrancaRetirMaiorMenor;
	}
	public Boolean getSelPrecoCobrancaRetirMaiorMenorM() {
		return selPrecoCobrancaRetirMaiorMenorM;
	}
	public void setSelPrecoCobrancaRetirMaiorMenorM(Boolean selPrecoCobrancaRetirMaiorMenorM) {
		this.selPrecoCobrancaRetirMaiorMenorM = selPrecoCobrancaRetirMaiorMenorM;
	}
	public Boolean getObgPrecoCobrancaRetirMaiorMenorM() {
		return obgPrecoCobrancaRetirMaiorMenorM;
	}
	public void setObgPrecoCobrancaRetirMaiorMenorM(Boolean obgPrecoCobrancaRetirMaiorMenorM) {
		this.obgPrecoCobrancaRetirMaiorMenorM = obgPrecoCobrancaRetirMaiorMenorM;
	}
	public Boolean getSelTipoApuracaoRetirMaiorMenor() {
		return selTipoApuracaoRetirMaiorMenor;
	}
	public void setSelTipoApuracaoRetirMaiorMenor(Boolean selTipoApuracaoRetirMaiorMenor) {
		this.selTipoApuracaoRetirMaiorMenor = selTipoApuracaoRetirMaiorMenor;
	}
	public Boolean getObgTipoApuracaoRetirMaiorMenor() {
		return obgTipoApuracaoRetirMaiorMenor;
	}
	public void setObgTipoApuracaoRetirMaiorMenor(Boolean obgTipoApuracaoRetirMaiorMenor) {
		this.obgTipoApuracaoRetirMaiorMenor = obgTipoApuracaoRetirMaiorMenor;
	}
	public Boolean getValorFixoTipoApuracaoRetirMaiorMenor() {
		return valorFixoTipoApuracaoRetirMaiorMenor;
	}
	public void setValorFixoTipoApuracaoRetirMaiorMenor(Boolean valorFixoTipoApuracaoRetirMaiorMenor) {
		this.valorFixoTipoApuracaoRetirMaiorMenor = valorFixoTipoApuracaoRetirMaiorMenor;
	}
	public Boolean getSelTipoApuracaoRetirMaiorMenorM() {
		return selTipoApuracaoRetirMaiorMenorM;
	}
	public void setSelTipoApuracaoRetirMaiorMenorM(Boolean selTipoApuracaoRetirMaiorMenorM) {
		this.selTipoApuracaoRetirMaiorMenorM = selTipoApuracaoRetirMaiorMenorM;
	}
	public Boolean getObgTipoApuracaoRetirMaiorMenorM() {
		return obgTipoApuracaoRetirMaiorMenorM;
	}
	public void setObgTipoApuracaoRetirMaiorMenorM(Boolean obgTipoApuracaoRetirMaiorMenorM) {
		this.obgTipoApuracaoRetirMaiorMenorM = obgTipoApuracaoRetirMaiorMenorM;
	}
	public Boolean getSelPercentualCobRetMaiorMenor() {
		return selPercentualCobRetMaiorMenor;
	}
	public void setSelPercentualCobRetMaiorMenor(Boolean selPercentualCobRetMaiorMenor) {
		this.selPercentualCobRetMaiorMenor = selPercentualCobRetMaiorMenor;
	}
	public Boolean getObgPercentualCobRetMaiorMenor() {
		return obgPercentualCobRetMaiorMenor;
	}
	public void setObgPercentualCobRetMaiorMenor(Boolean obgPercentualCobRetMaiorMenor) {
		this.obgPercentualCobRetMaiorMenor = obgPercentualCobRetMaiorMenor;
	}
	public Boolean getValorFixoPercentualCobRetMaiorMenor() {
		return valorFixoPercentualCobRetMaiorMenor;
	}
	public void setValorFixoPercentualCobRetMaiorMenor(Boolean valorFixoPercentualCobRetMaiorMenor) {
		this.valorFixoPercentualCobRetMaiorMenor = valorFixoPercentualCobRetMaiorMenor;
	}
	public Boolean getSelPercentualCobRetMaiorMenorM() {
		return selPercentualCobRetMaiorMenorM;
	}
	public void setSelPercentualCobRetMaiorMenorM(Boolean selPercentualCobRetMaiorMenorM) {
		this.selPercentualCobRetMaiorMenorM = selPercentualCobRetMaiorMenorM;
	}
	public Boolean getObgPercentualCobRetMaiorMenorM() {
		return obgPercentualCobRetMaiorMenorM;
	}
	public void setObgPercentualCobRetMaiorMenorM(Boolean obgPercentualCobRetMaiorMenorM) {
		this.obgPercentualCobRetMaiorMenorM = obgPercentualCobRetMaiorMenorM;
	}
	public Boolean getSelPercentualCobIntRetMaiorMenor() {
		return selPercentualCobIntRetMaiorMenor;
	}
	public void setSelPercentualCobIntRetMaiorMenor(Boolean selPercentualCobIntRetMaiorMenor) {
		this.selPercentualCobIntRetMaiorMenor = selPercentualCobIntRetMaiorMenor;
	}
	public Boolean getObgPercentualCobIntRetMaiorMenor() {
		return obgPercentualCobIntRetMaiorMenor;
	}
	public void setObgPercentualCobIntRetMaiorMenor(Boolean obgPercentualCobIntRetMaiorMenor) {
		this.obgPercentualCobIntRetMaiorMenor = obgPercentualCobIntRetMaiorMenor;
	}
	public Boolean getValorFixoPercentualCobIntRetMaiorMenor() {
		return valorFixoPercentualCobIntRetMaiorMenor;
	}
	public void setValorFixoPercentualCobIntRetMaiorMenor(Boolean valorFixoPercentualCobIntRetMaiorMenor) {
		this.valorFixoPercentualCobIntRetMaiorMenor = valorFixoPercentualCobIntRetMaiorMenor;
	}
	public Boolean getSelPercentualCobIntRetMaiorMenorM() {
		return selPercentualCobIntRetMaiorMenorM;
	}
	public void setSelPercentualCobIntRetMaiorMenorM(Boolean selPercentualCobIntRetMaiorMenorM) {
		this.selPercentualCobIntRetMaiorMenorM = selPercentualCobIntRetMaiorMenorM;
	}
	public Boolean getObgPercentualCobIntRetMaiorMenorM() {
		return obgPercentualCobIntRetMaiorMenorM;
	}
	public void setObgPercentualCobIntRetMaiorMenorM(Boolean obgPercentualCobIntRetMaiorMenorM) {
		this.obgPercentualCobIntRetMaiorMenorM = obgPercentualCobIntRetMaiorMenorM;
	}
	public Boolean getSelConsumoReferenciaRetMaiorMenor() {
		return selConsumoReferenciaRetMaiorMenor;
	}
	public void setSelConsumoReferenciaRetMaiorMenor(Boolean selConsumoReferenciaRetMaiorMenor) {
		this.selConsumoReferenciaRetMaiorMenor = selConsumoReferenciaRetMaiorMenor;
	}
	public Boolean getObgConsumoReferenciaRetMaiorMenor() {
		return obgConsumoReferenciaRetMaiorMenor;
	}
	public void setObgConsumoReferenciaRetMaiorMenor(Boolean obgConsumoReferenciaRetMaiorMenor) {
		this.obgConsumoReferenciaRetMaiorMenor = obgConsumoReferenciaRetMaiorMenor;
	}
	public Boolean getValorFixoConsumoReferenciaRetMaiorMenor() {
		return valorFixoConsumoReferenciaRetMaiorMenor;
	}
	public void setValorFixoConsumoReferenciaRetMaiorMenor(Boolean valorFixoConsumoReferenciaRetMaiorMenor) {
		this.valorFixoConsumoReferenciaRetMaiorMenor = valorFixoConsumoReferenciaRetMaiorMenor;
	}
	public Boolean getSelConsumoReferRetMaiorMenorM() {
		return selConsumoReferRetMaiorMenorM;
	}
	public void setSelConsumoReferRetMaiorMenorM(Boolean selConsumoReferRetMaiorMenorM) {
		this.selConsumoReferRetMaiorMenorM = selConsumoReferRetMaiorMenorM;
	}
	public Boolean getObgConsumoReferRetMaiorMenorM() {
		return obgConsumoReferRetMaiorMenorM;
	}
	public void setObgConsumoReferRetMaiorMenorM(Boolean obgConsumoReferRetMaiorMenorM) {
		this.obgConsumoReferRetMaiorMenorM = obgConsumoReferRetMaiorMenorM;
	}
	public Boolean getSelPercentualRetMaiorMenor() {
		return selPercentualRetMaiorMenor;
	}
	public void setSelPercentualRetMaiorMenor(Boolean selPercentualRetMaiorMenor) {
		this.selPercentualRetMaiorMenor = selPercentualRetMaiorMenor;
	}
	public Boolean getObgPercentualRetMaiorMenor() {
		return obgPercentualRetMaiorMenor;
	}
	public void setObgPercentualRetMaiorMenor(Boolean obgPercentualRetMaiorMenor) {
		this.obgPercentualRetMaiorMenor = obgPercentualRetMaiorMenor;
	}
	public Boolean getValorFixoPercentualRetMaiorMenor() {
		return valorFixoPercentualRetMaiorMenor;
	}
	public void setValorFixoPercentualRetMaiorMenor(Boolean valorFixoPercentualRetMaiorMenor) {
		this.valorFixoPercentualRetMaiorMenor = valorFixoPercentualRetMaiorMenor;
	}
	public Boolean getSelPercentualRetMaiorMenorM() {
		return selPercentualRetMaiorMenorM;
	}
	public void setSelPercentualRetMaiorMenorM(Boolean selPercentualRetMaiorMenorM) {
		this.selPercentualRetMaiorMenorM = selPercentualRetMaiorMenorM;
	}
	public Boolean getObgPercentualRetMaiorMenorM() {
		return obgPercentualRetMaiorMenorM;
	}
	public void setObgPercentualRetMaiorMenorM(Boolean obgPercentualRetMaiorMenorM) {
		this.obgPercentualRetMaiorMenorM = obgPercentualRetMaiorMenorM;
	}
	public Boolean getValorFixoPercentualRetMaiorMenorM() {
		return valorFixoPercentualRetMaiorMenorM;
	}
	public void setValorFixoPercentualRetMaiorMenorM(Boolean valorFixoPercentualRetMaiorMenorM) {
		this.valorFixoPercentualRetMaiorMenorM = valorFixoPercentualRetMaiorMenorM;
	}
	public Boolean getSelDataInicioRetiradaQPNRC() {
		return selDataInicioRetiradaQPNRC;
	}
	public void setSelDataInicioRetiradaQPNRC(Boolean selDataInicioRetiradaQPNRC) {
		this.selDataInicioRetiradaQPNRC = selDataInicioRetiradaQPNRC;
	}
	public Boolean getObgDataInicioRetiradaQPNRC() {
		return obgDataInicioRetiradaQPNRC;
	}
	public void setObgDataInicioRetiradaQPNRC(Boolean obgDataInicioRetiradaQPNRC) {
		this.obgDataInicioRetiradaQPNRC = obgDataInicioRetiradaQPNRC;
	}
	public Boolean getSelDataFimRetiradaQPNRC() {
		return selDataFimRetiradaQPNRC;
	}
	public void setSelDataFimRetiradaQPNRC(Boolean selDataFimRetiradaQPNRC) {
		this.selDataFimRetiradaQPNRC = selDataFimRetiradaQPNRC;
	}
	public Boolean getObgDataFimRetiradaQPNRC() {
		return obgDataFimRetiradaQPNRC;
	}
	public void setObgDataFimRetiradaQPNRC(Boolean obgDataFimRetiradaQPNRC) {
		this.obgDataFimRetiradaQPNRC = obgDataFimRetiradaQPNRC;
	}
	public Boolean getSelPercDuranteRetiradaQPNRC() {
		return selPercDuranteRetiradaQPNRC;
	}
	public void setSelPercDuranteRetiradaQPNRC(Boolean selPercDuranteRetiradaQPNRC) {
		this.selPercDuranteRetiradaQPNRC = selPercDuranteRetiradaQPNRC;
	}
	public Boolean getObgPercDuranteRetiradaQPNRC() {
		return obgPercDuranteRetiradaQPNRC;
	}
	public void setObgPercDuranteRetiradaQPNRC(Boolean obgPercDuranteRetiradaQPNRC) {
		this.obgPercDuranteRetiradaQPNRC = obgPercDuranteRetiradaQPNRC;
	}
	public Boolean getValorFixoPercDuranteRetiradaQPNRC() {
		return valorFixoPercDuranteRetiradaQPNRC;
	}
	public void setValorFixoPercDuranteRetiradaQPNRC(Boolean valorFixoPercDuranteRetiradaQPNRC) {
		this.valorFixoPercDuranteRetiradaQPNRC = valorFixoPercDuranteRetiradaQPNRC;
	}
	public Boolean getSelPercMinDuranteRetiradaQPNRC() {
		return selPercMinDuranteRetiradaQPNRC;
	}
	public void setSelPercMinDuranteRetiradaQPNRC(Boolean selPercMinDuranteRetiradaQPNRC) {
		this.selPercMinDuranteRetiradaQPNRC = selPercMinDuranteRetiradaQPNRC;
	}
	public Boolean getObgPercMinDuranteRetiradaQPNRC() {
		return obgPercMinDuranteRetiradaQPNRC;
	}
	public void setObgPercMinDuranteRetiradaQPNRC(Boolean obgPercMinDuranteRetiradaQPNRC) {
		this.obgPercMinDuranteRetiradaQPNRC = obgPercMinDuranteRetiradaQPNRC;
	}
	public Boolean getValorFixoPercMinDuranteRetiradaQPNRC() {
		return valorFixoPercMinDuranteRetiradaQPNRC;
	}
	public void setValorFixoPercMinDuranteRetiradaQPNRC(Boolean valorFixoPercMinDuranteRetiradaQPNRC) {
		this.valorFixoPercMinDuranteRetiradaQPNRC = valorFixoPercMinDuranteRetiradaQPNRC;
	}
	public Boolean getSelPercFimRetiradaQPNRC() {
		return selPercFimRetiradaQPNRC;
	}
	public void setSelPercFimRetiradaQPNRC(Boolean selPercFimRetiradaQPNRC) {
		this.selPercFimRetiradaQPNRC = selPercFimRetiradaQPNRC;
	}
	public Boolean getObgPercFimRetiradaQPNRC() {
		return obgPercFimRetiradaQPNRC;
	}
	public void setObgPercFimRetiradaQPNRC(Boolean obgPercFimRetiradaQPNRC) {
		this.obgPercFimRetiradaQPNRC = obgPercFimRetiradaQPNRC;
	}
	public Boolean getValorFixoPercFimRetiradaQPNRC() {
		return valorFixoPercFimRetiradaQPNRC;
	}
	public void setValorFixoPercFimRetiradaQPNRC(Boolean valorFixoPercFimRetiradaQPNRC) {
		this.valorFixoPercFimRetiradaQPNRC = valorFixoPercFimRetiradaQPNRC;
	}
	public Boolean getSelTempoValidadeRetiradaQPNRC() {
		return selTempoValidadeRetiradaQPNRC;
	}
	public void setSelTempoValidadeRetiradaQPNRC(Boolean selTempoValidadeRetiradaQPNRC) {
		this.selTempoValidadeRetiradaQPNRC = selTempoValidadeRetiradaQPNRC;
	}
	public Boolean getObgTempoValidadeRetiradaQPNRC() {
		return obgTempoValidadeRetiradaQPNRC;
	}
	public void setObgTempoValidadeRetiradaQPNRC(Boolean obgTempoValidadeRetiradaQPNRC) {
		this.obgTempoValidadeRetiradaQPNRC = obgTempoValidadeRetiradaQPNRC;
	}
	public Boolean getValorFixoTempoValidadeRetiradaQPNRC() {
		return valorFixoTempoValidadeRetiradaQPNRC;
	}
	public void setValorFixoTempoValidadeRetiradaQPNRC(Boolean valorFixoTempoValidadeRetiradaQPNRC) {
		this.valorFixoTempoValidadeRetiradaQPNRC = valorFixoTempoValidadeRetiradaQPNRC;
	}
	public Boolean getSelIndicadorImposto() {
		return selIndicadorImposto;
	}
	public void setSelIndicadorImposto(Boolean selIndicadorImposto) {
		this.selIndicadorImposto = selIndicadorImposto;
	}
	public Boolean getObgIndicadorImposto() {
		return obgIndicadorImposto;
	}
	public void setObgIndicadorImposto(Boolean obgIndicadorImposto) {
		this.obgIndicadorImposto = obgIndicadorImposto;
	}
	public Boolean getValorFixoIndicadorImposto() {
		return valorFixoIndicadorImposto;
	}
	public void setValorFixoIndicadorImposto(Boolean valorFixoIndicadorImposto) {
		this.valorFixoIndicadorImposto = valorFixoIndicadorImposto;
	}
	public Boolean getSelIndicadorImpostoM() {
		return selIndicadorImpostoM;
	}
	public void setSelIndicadorImpostoM(Boolean selIndicadorImpostoM) {
		this.selIndicadorImpostoM = selIndicadorImpostoM;
	}
	public Boolean getObgIndicadorImpostoM() {
		return obgIndicadorImpostoM;
	}
	public void setObgIndicadorImpostoM(Boolean obgIndicadorImpostoM) {
		this.obgIndicadorImpostoM = obgIndicadorImpostoM;
	}
	public Boolean getValorFixoIndicadorImpostoM() {
		return valorFixoIndicadorImpostoM;
	}
	public void setValorFixoIndicadorImpostoM(Boolean valorFixoIndicadorImpostoM) {
		this.valorFixoIndicadorImpostoM = valorFixoIndicadorImpostoM;
	}
	public Boolean getSelPeriodoTesteDataInicio() {
		return selPeriodoTesteDataInicio;
	}
	public void setSelPeriodoTesteDataInicio(Boolean selPeriodoTesteDataInicio) {
		this.selPeriodoTesteDataInicio = selPeriodoTesteDataInicio;
	}
	public Boolean getObgPeriodoTesteDataInicio() {
		return obgPeriodoTesteDataInicio;
	}
	public void setObgPeriodoTesteDataInicio(Boolean obgPeriodoTesteDataInicio) {
		this.obgPeriodoTesteDataInicio = obgPeriodoTesteDataInicio;
	}
	public Boolean getSelPeriodoTesteDateFim() {
		return selPeriodoTesteDateFim;
	}
	public void setSelPeriodoTesteDateFim(Boolean selPeriodoTesteDateFim) {
		this.selPeriodoTesteDateFim = selPeriodoTesteDateFim;
	}
	public Boolean getObgPeriodoTesteDateFim() {
		return obgPeriodoTesteDateFim;
	}
	public void setObgPeriodoTesteDateFim(Boolean obgPeriodoTesteDateFim) {
		this.obgPeriodoTesteDateFim = obgPeriodoTesteDateFim;
	}
	public Boolean getSelVolumeTeste() {
		return selVolumeTeste;
	}
	public void setSelVolumeTeste(Boolean selVolumeTeste) {
		this.selVolumeTeste = selVolumeTeste;
	}
	public Boolean getObgVolumeTeste() {
		return obgVolumeTeste;
	}
	public void setObgVolumeTeste(Boolean obgVolumeTeste) {
		this.obgVolumeTeste = obgVolumeTeste;
	}
	public Boolean getSelPrazoTeste() {
		return selPrazoTeste;
	}
	public void setSelPrazoTeste(Boolean selPrazoTeste) {
		this.selPrazoTeste = selPrazoTeste;
	}
	public Boolean getObgPrazoTeste() {
		return obgPrazoTeste;
	}
	public void setObgPrazoTeste(Boolean obgPrazoTeste) {
		this.obgPrazoTeste = obgPrazoTeste;
	}
	public Boolean getSelFaxDDD() {
		return selFaxDDD;
	}
	public void setSelFaxDDD(Boolean selFaxDDD) {
		this.selFaxDDD = selFaxDDD;
	}
	public Boolean getObgFaxDDD() {
		return obgFaxDDD;
	}
	public void setObgFaxDDD(Boolean obgFaxDDD) {
		this.obgFaxDDD = obgFaxDDD;
	}
	public Boolean getSelFaxNumero() {
		return selFaxNumero;
	}
	public void setSelFaxNumero(Boolean selFaxNumero) {
		this.selFaxNumero = selFaxNumero;
	}
	public Boolean getObgFaxNumero() {
		return obgFaxNumero;
	}
	public void setObgFaxNumero(Boolean obgFaxNumero) {
		this.obgFaxNumero = obgFaxNumero;
	}
	public Boolean getSelEmail() {
		return selEmail;
	}
	public void setSelEmail(Boolean selEmail) {
		this.selEmail = selEmail;
	}
	public Boolean getObgEmail() {
		return obgEmail;
	}
	public void setObgEmail(Boolean obgEmail) {
		this.obgEmail = obgEmail;
	}
	public Boolean getSelNumeroDiasGarantia() {
		return selNumeroDiasGarantia;
	}
	public void setSelNumeroDiasGarantia(Boolean selNumeroDiasGarantia) {
		this.selNumeroDiasGarantia = selNumeroDiasGarantia;
	}
	public Boolean getObgNumeroDiasGarantia() {
		return obgNumeroDiasGarantia;
	}
	public void setObgNumeroDiasGarantia(Boolean obgNumeroDiasGarantia) {
		this.obgNumeroDiasGarantia = obgNumeroDiasGarantia;
	}
	public Boolean getValorFixoNumeroDiasGarantia() {
		return valorFixoNumeroDiasGarantia;
	}
	public void setValorFixoNumeroDiasGarantia(Boolean valorFixoNumeroDiasGarantia) {
		this.valorFixoNumeroDiasGarantia = valorFixoNumeroDiasGarantia;
	}
	public Boolean getSelInicioGarantiaConversao() {
		return selInicioGarantiaConversao;
	}
	public void setSelInicioGarantiaConversao(Boolean selInicioGarantiaConversao) {
		this.selInicioGarantiaConversao = selInicioGarantiaConversao;
	}
	public Boolean getObgInicioGarantiaConversao() {
		return obgInicioGarantiaConversao;
	}
	public void setObgInicioGarantiaConversao(Boolean obgInicioGarantiaConversao) {
		this.obgInicioGarantiaConversao = obgInicioGarantiaConversao;
	}
	public Boolean getSelVazaoInstantanea() {
		return selVazaoInstantanea;
	}
	public void setSelVazaoInstantanea(Boolean selVazaoInstantanea) {
		this.selVazaoInstantanea = selVazaoInstantanea;
	}
	public Boolean getObgVazaoInstantanea() {
		return obgVazaoInstantanea;
	}
	public void setObgVazaoInstantanea(Boolean obgVazaoInstantanea) {
		this.obgVazaoInstantanea = obgVazaoInstantanea;
	}
	public Boolean getValorFixoVazaoInstantanea() {
		return valorFixoVazaoInstantanea;
	}
	public void setValorFixoVazaoInstantanea(Boolean valorFixoVazaoInstantanea) {
		this.valorFixoVazaoInstantanea = valorFixoVazaoInstantanea;
	}
	public Boolean getSelVazaoInstantaneaMaxima() {
		return selVazaoInstantaneaMaxima;
	}
	public void setSelVazaoInstantaneaMaxima(Boolean selVazaoInstantaneaMaxima) {
		this.selVazaoInstantaneaMaxima = selVazaoInstantaneaMaxima;
	}
	public Boolean getObgVazaoInstantaneaMaxima() {
		return obgVazaoInstantaneaMaxima;
	}
	public void setObgVazaoInstantaneaMaxima(Boolean obgVazaoInstantaneaMaxima) {
		this.obgVazaoInstantaneaMaxima = obgVazaoInstantaneaMaxima;
	}
	public Boolean getValorFixoVazaoInstantaneaMaxima() {
		return valorFixoVazaoInstantaneaMaxima;
	}
	public void setValorFixoVazaoInstantaneaMaxima(Boolean valorFixoVazaoInstantaneaMaxima) {
		this.valorFixoVazaoInstantaneaMaxima = valorFixoVazaoInstantaneaMaxima;
	}
	public Boolean getSelVazaoInstantaneaMinima() {
		return selVazaoInstantaneaMinima;
	}
	public void setSelVazaoInstantaneaMinima(Boolean selVazaoInstantaneaMinima) {
		this.selVazaoInstantaneaMinima = selVazaoInstantaneaMinima;
	}
	public Boolean getObgVazaoInstantaneaMinima() {
		return obgVazaoInstantaneaMinima;
	}
	public void setObgVazaoInstantaneaMinima(Boolean obgVazaoInstantaneaMinima) {
		this.obgVazaoInstantaneaMinima = obgVazaoInstantaneaMinima;
	}
	public Boolean getValorFixoVazaoInstantaneaMinima() {
		return valorFixoVazaoInstantaneaMinima;
	}
	public void setValorFixoVazaoInstantaneaMinima(Boolean valorFixoVazaoInstantaneaMinima) {
		this.valorFixoVazaoInstantaneaMinima = valorFixoVazaoInstantaneaMinima;
	}
	public Boolean getSelFaixaPressaoFornecimento() {
		return selFaixaPressaoFornecimento;
	}
	public void setSelFaixaPressaoFornecimento(Boolean selFaixaPressaoFornecimento) {
		this.selFaixaPressaoFornecimento = selFaixaPressaoFornecimento;
	}
	public Boolean getObgFaixaPressaoFornecimento() {
		return obgFaixaPressaoFornecimento;
	}
	public void setObgFaixaPressaoFornecimento(Boolean obgFaixaPressaoFornecimento) {
		this.obgFaixaPressaoFornecimento = obgFaixaPressaoFornecimento;
	}
	public Boolean getValorFixoFaixaPressaoFornecimento() {
		return valorFixoFaixaPressaoFornecimento;
	}
	public void setValorFixoFaixaPressaoFornecimento(Boolean valorFixoFaixaPressaoFornecimento) {
		this.valorFixoFaixaPressaoFornecimento = valorFixoFaixaPressaoFornecimento;
	}
	public Boolean getSelPressaoMinimaFornecimento() {
		return selPressaoMinimaFornecimento;
	}
	public void setSelPressaoMinimaFornecimento(Boolean selPressaoMinimaFornecimento) {
		this.selPressaoMinimaFornecimento = selPressaoMinimaFornecimento;
	}
	public Boolean getObgPressaoMinimaFornecimento() {
		return obgPressaoMinimaFornecimento;
	}
	public void setObgPressaoMinimaFornecimento(Boolean obgPressaoMinimaFornecimento) {
		this.obgPressaoMinimaFornecimento = obgPressaoMinimaFornecimento;
	}
	public Boolean getValorFixoPressaoMinimaFornecimento() {
		return valorFixoPressaoMinimaFornecimento;
	}
	public void setValorFixoPressaoMinimaFornecimento(Boolean valorFixoPressaoMinimaFornecimento) {
		this.valorFixoPressaoMinimaFornecimento = valorFixoPressaoMinimaFornecimento;
	}
	public Boolean getSelPressaoColetada() {
		return selPressaoColetada;
	}
	public void setSelPressaoColetada(Boolean selPressaoColetada) {
		this.selPressaoColetada = selPressaoColetada;
	}
	public Boolean getObgPressaoColetada() {
		return obgPressaoColetada;
	}
	public void setObgPressaoColetada(Boolean obgPressaoColetada) {
		this.obgPressaoColetada = obgPressaoColetada;
	}
	public Boolean getValorFixoPressaoColetada() {
		return valorFixoPressaoColetada;
	}
	public void setValorFixoPressaoColetada(Boolean valorFixoPressaoColetada) {
		this.valorFixoPressaoColetada = valorFixoPressaoColetada;
	}
	public Boolean getSelPressaoMaximaFornecimento() {
		return selPressaoMaximaFornecimento;
	}
	public void setSelPressaoMaximaFornecimento(Boolean selPressaoMaximaFornecimento) {
		this.selPressaoMaximaFornecimento = selPressaoMaximaFornecimento;
	}
	public Boolean getObgPressaoMaximaFornecimento() {
		return obgPressaoMaximaFornecimento;
	}
	public void setObgPressaoMaximaFornecimento(Boolean obgPressaoMaximaFornecimento) {
		this.obgPressaoMaximaFornecimento = obgPressaoMaximaFornecimento;
	}
	public Boolean getValorFixoPressaoMaximaFornecimento() {
		return valorFixoPressaoMaximaFornecimento;
	}
	public void setValorFixoPressaoMaximaFornecimento(Boolean valorFixoPressaoMaximaFornecimento) {
		this.valorFixoPressaoMaximaFornecimento = valorFixoPressaoMaximaFornecimento;
	}
	public Boolean getSelPressaoLimiteFornecimento() {
		return selPressaoLimiteFornecimento;
	}
	public void setSelPressaoLimiteFornecimento(Boolean selPressaoLimiteFornecimento) {
		this.selPressaoLimiteFornecimento = selPressaoLimiteFornecimento;
	}
	public Boolean getObgPressaoLimiteFornecimento() {
		return obgPressaoLimiteFornecimento;
	}
	public void setObgPressaoLimiteFornecimento(Boolean obgPressaoLimiteFornecimento) {
		this.obgPressaoLimiteFornecimento = obgPressaoLimiteFornecimento;
	}
	public Boolean getValorFixoPressaoLimiteFornecimento() {
		return valorFixoPressaoLimiteFornecimento;
	}
	public void setValorFixoPressaoLimiteFornecimento(Boolean valorFixoPressaoLimiteFornecimento) {
		this.valorFixoPressaoLimiteFornecimento = valorFixoPressaoLimiteFornecimento;
	}
	public Boolean getSelNumAnosCtrlParadaCliente() {
		return selNumAnosCtrlParadaCliente;
	}
	public void setSelNumAnosCtrlParadaCliente(Boolean selNumAnosCtrlParadaCliente) {
		this.selNumAnosCtrlParadaCliente = selNumAnosCtrlParadaCliente;
	}
	public Boolean getObgNumAnosCtrlParadaCliente() {
		return obgNumAnosCtrlParadaCliente;
	}
	public void setObgNumAnosCtrlParadaCliente(Boolean obgNumAnosCtrlParadaCliente) {
		this.obgNumAnosCtrlParadaCliente = obgNumAnosCtrlParadaCliente;
	}
	public Boolean getValorFixoNumAnosCtrlParadaCliente() {
		return valorFixoNumAnosCtrlParadaCliente;
	}
	public void setValorFixoNumAnosCtrlParadaCliente(Boolean valorFixoNumAnosCtrlParadaCliente) {
		this.valorFixoNumAnosCtrlParadaCliente = valorFixoNumAnosCtrlParadaCliente;
	}
	public Boolean getSelMaxTotalParadasCliente() {
		return selMaxTotalParadasCliente;
	}
	public void setSelMaxTotalParadasCliente(Boolean selMaxTotalParadasCliente) {
		this.selMaxTotalParadasCliente = selMaxTotalParadasCliente;
	}
	public Boolean getObgMaxTotalParadasCliente() {
		return obgMaxTotalParadasCliente;
	}
	public void setObgMaxTotalParadasCliente(Boolean obgMaxTotalParadasCliente) {
		this.obgMaxTotalParadasCliente = obgMaxTotalParadasCliente;
	}
	public Boolean getValorFixoMaxTotalParadasCliente() {
		return valorFixoMaxTotalParadasCliente;
	}
	public void setValorFixoMaxTotalParadasCliente(Boolean valorFixoMaxTotalParadasCliente) {
		this.valorFixoMaxTotalParadasCliente = valorFixoMaxTotalParadasCliente;
	}
	public Boolean getSelMaxAnualParadasCliente() {
		return selMaxAnualParadasCliente;
	}
	public void setSelMaxAnualParadasCliente(Boolean selMaxAnualParadasCliente) {
		this.selMaxAnualParadasCliente = selMaxAnualParadasCliente;
	}
	public Boolean getObgMaxAnualParadasCliente() {
		return obgMaxAnualParadasCliente;
	}
	public void setObgMaxAnualParadasCliente(Boolean obgMaxAnualParadasCliente) {
		this.obgMaxAnualParadasCliente = obgMaxAnualParadasCliente;
	}
	public Boolean getValorFixoMaxAnualParadasCliente() {
		return valorFixoMaxAnualParadasCliente;
	}
	public void setValorFixoMaxAnualParadasCliente(Boolean valorFixoMaxAnualParadasCliente) {
		this.valorFixoMaxAnualParadasCliente = valorFixoMaxAnualParadasCliente;
	}
	public Boolean getSelNumDiasProgrParadaCliente() {
		return selNumDiasProgrParadaCliente;
	}
	public void setSelNumDiasProgrParadaCliente(Boolean selNumDiasProgrParadaCliente) {
		this.selNumDiasProgrParadaCliente = selNumDiasProgrParadaCliente;
	}
	public Boolean getObgNumDiasProgrParadaCliente() {
		return obgNumDiasProgrParadaCliente;
	}
	public void setObgNumDiasProgrParadaCliente(Boolean obgNumDiasProgrParadaCliente) {
		this.obgNumDiasProgrParadaCliente = obgNumDiasProgrParadaCliente;
	}
	public Boolean getValorFixoNumDiasProgrParadaCliente() {
		return valorFixoNumDiasProgrParadaCliente;
	}
	public void setValorFixoNumDiasProgrParadaCliente(Boolean valorFixoNumDiasProgrParadaCliente) {
		this.valorFixoNumDiasProgrParadaCliente = valorFixoNumDiasProgrParadaCliente;
	}
	public Boolean getSelNumDiasConsecParadaCliente() {
		return selNumDiasConsecParadaCliente;
	}
	public void setSelNumDiasConsecParadaCliente(Boolean selNumDiasConsecParadaCliente) {
		this.selNumDiasConsecParadaCliente = selNumDiasConsecParadaCliente;
	}
	public Boolean getObgNumDiasConsecParadaCliente() {
		return obgNumDiasConsecParadaCliente;
	}
	public void setObgNumDiasConsecParadaCliente(Boolean obgNumDiasConsecParadaCliente) {
		this.obgNumDiasConsecParadaCliente = obgNumDiasConsecParadaCliente;
	}
	public Boolean getValorFixoNumDiasConsecParadaCliente() {
		return valorFixoNumDiasConsecParadaCliente;
	}
	public void setValorFixoNumDiasConsecParadaCliente(Boolean valorFixoNumDiasConsecParadaCliente) {
		this.valorFixoNumDiasConsecParadaCliente = valorFixoNumDiasConsecParadaCliente;
	}
	public Boolean getSelNumAnosCtrlParadaCDL() {
		return selNumAnosCtrlParadaCDL;
	}
	public void setSelNumAnosCtrlParadaCDL(Boolean selNumAnosCtrlParadaCDL) {
		this.selNumAnosCtrlParadaCDL = selNumAnosCtrlParadaCDL;
	}
	public Boolean getObgNumAnosCtrlParadaCDL() {
		return obgNumAnosCtrlParadaCDL;
	}
	public void setObgNumAnosCtrlParadaCDL(Boolean obgNumAnosCtrlParadaCDL) {
		this.obgNumAnosCtrlParadaCDL = obgNumAnosCtrlParadaCDL;
	}
	public Boolean getValorFixoNumAnosCtrlParadaCDL() {
		return valorFixoNumAnosCtrlParadaCDL;
	}
	public void setValorFixoNumAnosCtrlParadaCDL(Boolean valorFixoNumAnosCtrlParadaCDL) {
		this.valorFixoNumAnosCtrlParadaCDL = valorFixoNumAnosCtrlParadaCDL;
	}
	public Boolean getSelMaxTotalParadasCDL() {
		return selMaxTotalParadasCDL;
	}
	public void setSelMaxTotalParadasCDL(Boolean selMaxTotalParadasCDL) {
		this.selMaxTotalParadasCDL = selMaxTotalParadasCDL;
	}
	public Boolean getObgMaxTotalParadasCDL() {
		return obgMaxTotalParadasCDL;
	}
	public void setObgMaxTotalParadasCDL(Boolean obgMaxTotalParadasCDL) {
		this.obgMaxTotalParadasCDL = obgMaxTotalParadasCDL;
	}
	public Boolean getValorFixoMaxTotalParadasCDL() {
		return valorFixoMaxTotalParadasCDL;
	}
	public void setValorFixoMaxTotalParadasCDL(Boolean valorFixoMaxTotalParadasCDL) {
		this.valorFixoMaxTotalParadasCDL = valorFixoMaxTotalParadasCDL;
	}
	public Boolean getSelMaxAnualParadasCDL() {
		return selMaxAnualParadasCDL;
	}
	public void setSelMaxAnualParadasCDL(Boolean selMaxAnualParadasCDL) {
		this.selMaxAnualParadasCDL = selMaxAnualParadasCDL;
	}
	public Boolean getObgMaxAnualParadasCDL() {
		return obgMaxAnualParadasCDL;
	}
	public void setObgMaxAnualParadasCDL(Boolean obgMaxAnualParadasCDL) {
		this.obgMaxAnualParadasCDL = obgMaxAnualParadasCDL;
	}
	public Boolean getValorFixoMaxAnualParadasCDL() {
		return valorFixoMaxAnualParadasCDL;
	}
	public void setValorFixoMaxAnualParadasCDL(Boolean valorFixoMaxAnualParadasCDL) {
		this.valorFixoMaxAnualParadasCDL = valorFixoMaxAnualParadasCDL;
	}
	public Boolean getSelNumDiasProgrParadaCDL() {
		return selNumDiasProgrParadaCDL;
	}
	public void setSelNumDiasProgrParadaCDL(Boolean selNumDiasProgrParadaCDL) {
		this.selNumDiasProgrParadaCDL = selNumDiasProgrParadaCDL;
	}
	public Boolean getObgNumDiasProgrParadaCDL() {
		return obgNumDiasProgrParadaCDL;
	}
	public void setObgNumDiasProgrParadaCDL(Boolean obgNumDiasProgrParadaCDL) {
		this.obgNumDiasProgrParadaCDL = obgNumDiasProgrParadaCDL;
	}
	public Boolean getValorFixoNumDiasProgrParadaCDL() {
		return valorFixoNumDiasProgrParadaCDL;
	}
	public void setValorFixoNumDiasProgrParadaCDL(Boolean valorFixoNumDiasProgrParadaCDL) {
		this.valorFixoNumDiasProgrParadaCDL = valorFixoNumDiasProgrParadaCDL;
	}
	public Boolean getSelNumDiasConsecParadaCDL() {
		return selNumDiasConsecParadaCDL;
	}
	public void setSelNumDiasConsecParadaCDL(Boolean selNumDiasConsecParadaCDL) {
		this.selNumDiasConsecParadaCDL = selNumDiasConsecParadaCDL;
	}
	public Boolean getObgNumDiasConsecParadaCDL() {
		return obgNumDiasConsecParadaCDL;
	}
	public void setObgNumDiasConsecParadaCDL(Boolean obgNumDiasConsecParadaCDL) {
		this.obgNumDiasConsecParadaCDL = obgNumDiasConsecParadaCDL;
	}
	public Boolean getValorFixoNumDiasConsecParadaCDL() {
		return valorFixoNumDiasConsecParadaCDL;
	}
	public void setValorFixoNumDiasConsecParadaCDL(Boolean valorFixoNumDiasConsecParadaCDL) {
		this.valorFixoNumDiasConsecParadaCDL = valorFixoNumDiasConsecParadaCDL;
	}
	public Boolean getSelTipoFaturamento() {
		return selTipoFaturamento;
	}
	public void setSelTipoFaturamento(Boolean selTipoFaturamento) {
		this.selTipoFaturamento = selTipoFaturamento;
	}
	public Boolean getObgTipoFaturamento() {
		return obgTipoFaturamento;
	}
	public void setObgTipoFaturamento(Boolean obgTipoFaturamento) {
		this.obgTipoFaturamento = obgTipoFaturamento;
	}
	public Boolean getSelTarifaConsumo() {
		return selTarifaConsumo;
	}
	public void setSelTarifaConsumo(Boolean selTarifaConsumo) {
		this.selTarifaConsumo = selTarifaConsumo;
	}
	public Boolean getObgTarifaConsumo() {
		return obgTarifaConsumo;
	}
	public void setObgTarifaConsumo(Boolean obgTarifaConsumo) {
		this.obgTarifaConsumo = obgTarifaConsumo;
	}
	public Boolean getValorFixoTarifaConsumo() {
		return valorFixoTarifaConsumo;
	}
	public void setValorFixoTarifaConsumo(Boolean valorFixoTarifaConsumo) {
		this.valorFixoTarifaConsumo = valorFixoTarifaConsumo;
	}
	public Boolean getSelFatPeriodicidade() {
		return selFatPeriodicidade;
	}
	public void setSelFatPeriodicidade(Boolean selFatPeriodicidade) {
		this.selFatPeriodicidade = selFatPeriodicidade;
	}
	public Boolean getObgFatPeriodicidade() {
		return obgFatPeriodicidade;
	}
	public void setObgFatPeriodicidade(Boolean obgFatPeriodicidade) {
		this.obgFatPeriodicidade = obgFatPeriodicidade;
	}
	public Boolean getValorFixoFatPeriodicidade() {
		return valorFixoFatPeriodicidade;
	}
	public void setValorFixoFatPeriodicidade(Boolean valorFixoFatPeriodicidade) {
		this.valorFixoFatPeriodicidade = valorFixoFatPeriodicidade;
	}
	public Boolean getSelFormaCobranca() {
		return selFormaCobranca;
	}
	public void setSelFormaCobranca(Boolean selFormaCobranca) {
		this.selFormaCobranca = selFormaCobranca;
	}
	public Boolean getObgFormaCobranca() {
		return obgFormaCobranca;
	}
	public void setObgFormaCobranca(Boolean obgFormaCobranca) {
		this.obgFormaCobranca = obgFormaCobranca;
	}
	public Boolean getValorFixoFormaCobranca() {
		return valorFixoFormaCobranca;
	}
	public void setValorFixoFormaCobranca(Boolean valorFixoFormaCobranca) {
		this.valorFixoFormaCobranca = valorFixoFormaCobranca;
	}
	public Boolean getSelArrecadadorConvenio() {
		return selArrecadadorConvenio;
	}
	public void setSelArrecadadorConvenio(Boolean selArrecadadorConvenio) {
		this.selArrecadadorConvenio = selArrecadadorConvenio;
	}
	public Boolean getObgArrecadadorConvenio() {
		return obgArrecadadorConvenio;
	}
	public void setObgArrecadadorConvenio(Boolean obgArrecadadorConvenio) {
		this.obgArrecadadorConvenio = obgArrecadadorConvenio;
	}
	public Boolean getValorFixoArrecadadorConvenio() {
		return valorFixoArrecadadorConvenio;
	}
	public void setValorFixoArrecadadorConvenio(Boolean valorFixoArrecadadorConvenio) {
		this.valorFixoArrecadadorConvenio = valorFixoArrecadadorConvenio;
	}
	public Boolean getSelDebitoAutomatico() {
		return selDebitoAutomatico;
	}
	public void setSelDebitoAutomatico(Boolean selDebitoAutomatico) {
		this.selDebitoAutomatico = selDebitoAutomatico;
	}
	public Boolean getObgDebitoAutomatico() {
		return obgDebitoAutomatico;
	}
	public void setObgDebitoAutomatico(Boolean obgDebitoAutomatico) {
		this.obgDebitoAutomatico = obgDebitoAutomatico;
	}
	public Boolean getValorFixoDebitoAutomatico() {
		return valorFixoDebitoAutomatico;
	}
	public void setValorFixoDebitoAutomatico(Boolean valorFixoDebitoAutomatico) {
		this.valorFixoDebitoAutomatico = valorFixoDebitoAutomatico;
	}
	public Boolean getSelArrecadadorConvenioDebitoAutomatico() {
		return selArrecadadorConvenioDebitoAutomatico;
	}
	public void setSelArrecadadorConvenioDebitoAutomatico(Boolean selArrecadadorConvenioDebitoAutomatico) {
		this.selArrecadadorConvenioDebitoAutomatico = selArrecadadorConvenioDebitoAutomatico;
	}
	public Boolean getObgArrecadadorConvenioDebitoAutomatico() {
		return obgArrecadadorConvenioDebitoAutomatico;
	}
	public void setObgArrecadadorConvenioDebitoAutomatico(Boolean obgArrecadadorConvenioDebitoAutomatico) {
		this.obgArrecadadorConvenioDebitoAutomatico = obgArrecadadorConvenioDebitoAutomatico;
	}
	public Boolean getValorFixoArrecadadorConvenioDebitoAutomatico() {
		return valorFixoArrecadadorConvenioDebitoAutomatico;
	}
	public void setValorFixoArrecadadorConvenioDebitoAutomatico(Boolean valorFixoArrecadadorConvenioDebitoAutomatico) {
		this.valorFixoArrecadadorConvenioDebitoAutomatico = valorFixoArrecadadorConvenioDebitoAutomatico;
	}
	public Boolean getSelDepositoIdentificado() {
		return selDepositoIdentificado;
	}
	public void setSelDepositoIdentificado(Boolean selDepositoIdentificado) {
		this.selDepositoIdentificado = selDepositoIdentificado;
	}
	public Boolean getObgDepositoIdentificado() {
		return obgDepositoIdentificado;
	}
	public void setObgDepositoIdentificado(Boolean obgDepositoIdentificado) {
		this.obgDepositoIdentificado = obgDepositoIdentificado;
	}
	public Boolean getValorFixoDepositoIdentificado() {
		return valorFixoDepositoIdentificado;
	}
	public void setValorFixoDepositoIdentificado(Boolean valorFixoDepositoIdentificado) {
		this.valorFixoDepositoIdentificado = valorFixoDepositoIdentificado;
	}
	public Boolean getSelEndFisEntFatCEP() {
		return selEndFisEntFatCEP;
	}
	public void setSelEndFisEntFatCEP(Boolean selEndFisEntFatCEP) {
		this.selEndFisEntFatCEP = selEndFisEntFatCEP;
	}
	public Boolean getObgEndFisEntFatCEP() {
		return obgEndFisEntFatCEP;
	}
	public void setObgEndFisEntFatCEP(Boolean obgEndFisEntFatCEP) {
		this.obgEndFisEntFatCEP = obgEndFisEntFatCEP;
	}
	public Boolean getSelEndFisEntFatNumero() {
		return selEndFisEntFatNumero;
	}
	public void setSelEndFisEntFatNumero(Boolean selEndFisEntFatNumero) {
		this.selEndFisEntFatNumero = selEndFisEntFatNumero;
	}
	public Boolean getObgEndFisEntFatNumero() {
		return obgEndFisEntFatNumero;
	}
	public void setObgEndFisEntFatNumero(Boolean obgEndFisEntFatNumero) {
		this.obgEndFisEntFatNumero = obgEndFisEntFatNumero;
	}
	public Boolean getSelEndFisEntFatComplemento() {
		return selEndFisEntFatComplemento;
	}
	public void setSelEndFisEntFatComplemento(Boolean selEndFisEntFatComplemento) {
		this.selEndFisEntFatComplemento = selEndFisEntFatComplemento;
	}
	public Boolean getObgEndFisEntFatComplemento() {
		return obgEndFisEntFatComplemento;
	}
	public void setObgEndFisEntFatComplemento(Boolean obgEndFisEntFatComplemento) {
		this.obgEndFisEntFatComplemento = obgEndFisEntFatComplemento;
	}
	public Boolean getSelEndFisEntFatEmail() {
		return selEndFisEntFatEmail;
	}
	public void setSelEndFisEntFatEmail(Boolean selEndFisEntFatEmail) {
		this.selEndFisEntFatEmail = selEndFisEntFatEmail;
	}
	public Boolean getObgEndFisEntFatEmail() {
		return obgEndFisEntFatEmail;
	}
	public void setObgEndFisEntFatEmail(Boolean obgEndFisEntFatEmail) {
		this.obgEndFisEntFatEmail = obgEndFisEntFatEmail;
	}
	public Boolean getSelItemFatura() {
		return selItemFatura;
	}
	public void setSelItemFatura(Boolean selItemFatura) {
		this.selItemFatura = selItemFatura;
	}
	public Boolean getObgItemFatura() {
		return obgItemFatura;
	}
	public void setObgItemFatura(Boolean obgItemFatura) {
		this.obgItemFatura = obgItemFatura;
	}
	public Boolean getValorFixoItemFatura() {
		return valorFixoItemFatura;
	}
	public void setValorFixoItemFatura(Boolean valorFixoItemFatura) {
		this.valorFixoItemFatura = valorFixoItemFatura;
	}
	public Boolean getSelPercminimoQDC() {
		return selPercminimoQDC;
	}
	public void setSelPercminimoQDC(Boolean selPercminimoQDC) {
		this.selPercminimoQDC = selPercminimoQDC;
	}
	public Boolean getObgPercminimoQDC() {
		return obgPercminimoQDC;
	}
	public void setObgPercminimoQDC(Boolean obgPercminimoQDC) {
		this.obgPercminimoQDC = obgPercminimoQDC;
	}
	public Boolean getValorFixoPercminimoQDC() {
		return valorFixoPercminimoQDC;
	}
	public void setValorFixoPercminimoQDC(Boolean valorFixoPercminimoQDC) {
		this.valorFixoPercminimoQDC = valorFixoPercminimoQDC;
	}
	public Boolean getSelDiaVencimentoItemFatura() {
		return selDiaVencimentoItemFatura;
	}
	public void setSelDiaVencimentoItemFatura(Boolean selDiaVencimentoItemFatura) {
		this.selDiaVencimentoItemFatura = selDiaVencimentoItemFatura;
	}
	public Boolean getObgDiaVencimentoItemFatura() {
		return obgDiaVencimentoItemFatura;
	}
	public void setObgDiaVencimentoItemFatura(Boolean obgDiaVencimentoItemFatura) {
		this.obgDiaVencimentoItemFatura = obgDiaVencimentoItemFatura;
	}
	public Boolean getValorFixoDiaVencimentoItemFatura() {
		return valorFixoDiaVencimentoItemFatura;
	}
	public void setValorFixoDiaVencimentoItemFatura(Boolean valorFixoDiaVencimentoItemFatura) {
		this.valorFixoDiaVencimentoItemFatura = valorFixoDiaVencimentoItemFatura;
	}
	public Boolean getSelOpcaoVencimentoItemFatura() {
		return selOpcaoVencimentoItemFatura;
	}
	public void setSelOpcaoVencimentoItemFatura(Boolean selOpcaoVencimentoItemFatura) {
		this.selOpcaoVencimentoItemFatura = selOpcaoVencimentoItemFatura;
	}
	public Boolean getObgOpcaoVencimentoItemFatura() {
		return obgOpcaoVencimentoItemFatura;
	}
	public void setObgOpcaoVencimentoItemFatura(Boolean obgOpcaoVencimentoItemFatura) {
		this.obgOpcaoVencimentoItemFatura = obgOpcaoVencimentoItemFatura;
	}
	public Boolean getValorFixoOpcaoVencimentoItemFatura() {
		return valorFixoOpcaoVencimentoItemFatura;
	}
	public void setValorFixoOpcaoVencimentoItemFatura(Boolean valorFixoOpcaoVencimentoItemFatura) {
		this.valorFixoOpcaoVencimentoItemFatura = valorFixoOpcaoVencimentoItemFatura;
	}
	public Boolean getSelFaseVencimentoItemFatura() {
		return selFaseVencimentoItemFatura;
	}
	public void setSelFaseVencimentoItemFatura(Boolean selFaseVencimentoItemFatura) {
		this.selFaseVencimentoItemFatura = selFaseVencimentoItemFatura;
	}
	public Boolean getObgFaseVencimentoItemFatura() {
		return obgFaseVencimentoItemFatura;
	}
	public void setObgFaseVencimentoItemFatura(Boolean obgFaseVencimentoItemFatura) {
		this.obgFaseVencimentoItemFatura = obgFaseVencimentoItemFatura;
	}
	public Boolean getValorFixoFaseVencimentoItemFatura() {
		return valorFixoFaseVencimentoItemFatura;
	}
	public void setValorFixoFaseVencimentoItemFatura(Boolean valorFixoFaseVencimentoItemFatura) {
		this.valorFixoFaseVencimentoItemFatura = valorFixoFaseVencimentoItemFatura;
	}
	public Boolean getSelIndicadorVencDiaNaoUtil() {
		return selIndicadorVencDiaNaoUtil;
	}
	public void setSelIndicadorVencDiaNaoUtil(Boolean selIndicadorVencDiaNaoUtil) {
		this.selIndicadorVencDiaNaoUtil = selIndicadorVencDiaNaoUtil;
	}
	public Boolean getObgIndicadorVencDiaNaoUtil() {
		return obgIndicadorVencDiaNaoUtil;
	}
	public void setObgIndicadorVencDiaNaoUtil(Boolean obgIndicadorVencDiaNaoUtil) {
		this.obgIndicadorVencDiaNaoUtil = obgIndicadorVencDiaNaoUtil;
	}
	public Boolean getValorFixoIndicadorVencDiaNaoUtil() {
		return valorFixoIndicadorVencDiaNaoUtil;
	}
	public void setValorFixoIndicadorVencDiaNaoUtil(Boolean valorFixoIndicadorVencDiaNaoUtil) {
		this.valorFixoIndicadorVencDiaNaoUtil = valorFixoIndicadorVencDiaNaoUtil;
	}
	public Boolean getSelDiaCotacao() {
		return selDiaCotacao;
	}
	public void setSelDiaCotacao(Boolean selDiaCotacao) {
		this.selDiaCotacao = selDiaCotacao;
	}
	public Boolean getObgDiaCotacao() {
		return obgDiaCotacao;
	}
	public void setObgDiaCotacao(Boolean obgDiaCotacao) {
		this.obgDiaCotacao = obgDiaCotacao;
	}
	public Boolean getValorFixoDiaCotacao() {
		return valorFixoDiaCotacao;
	}
	public void setValorFixoDiaCotacao(Boolean valorFixoDiaCotacao) {
		this.valorFixoDiaCotacao = valorFixoDiaCotacao;
	}
	public Boolean getSelDataReferenciaCambial() {
		return selDataReferenciaCambial;
	}
	public void setSelDataReferenciaCambial(Boolean selDataReferenciaCambial) {
		this.selDataReferenciaCambial = selDataReferenciaCambial;
	}
	public Boolean getObgDataReferenciaCambial() {
		return obgDataReferenciaCambial;
	}
	public void setObgDataReferenciaCambial(Boolean obgDataReferenciaCambial) {
		this.obgDataReferenciaCambial = obgDataReferenciaCambial;
	}
	public Boolean getValorFixoDataReferenciaCambial() {
		return valorFixoDataReferenciaCambial;
	}
	public void setValorFixoDataReferenciaCambial(Boolean valorFixoDataReferenciaCambial) {
		this.valorFixoDataReferenciaCambial = valorFixoDataReferenciaCambial;
	}
	public Boolean getSelIndicadorNFE() {
		return selIndicadorNFE;
	}
	public void setSelIndicadorNFE(Boolean selIndicadorNFE) {
		this.selIndicadorNFE = selIndicadorNFE;
	}
	public Boolean getObgIndicadorNFE() {
		return obgIndicadorNFE;
	}
	public void setObgIndicadorNFE(Boolean obgIndicadorNFE) {
		this.obgIndicadorNFE = obgIndicadorNFE;
	}
	public Boolean getValorFixoIndicadorNFE() {
		return valorFixoIndicadorNFE;
	}
	public void setValorFixoIndicadorNFE(Boolean valorFixoIndicadorNFE) {
		this.valorFixoIndicadorNFE = valorFixoIndicadorNFE;
	}
	public Boolean getSelEmitirFaturaComNfe() {
		return selEmitirFaturaComNfe;
	}
	public void setSelEmitirFaturaComNfe(Boolean selEmitirFaturaComNfe) {
		this.selEmitirFaturaComNfe = selEmitirFaturaComNfe;
	}
	public Boolean getObgEmitirFaturaComNfe() {
		return obgEmitirFaturaComNfe;
	}
	public void setObgEmitirFaturaComNfe(Boolean obgEmitirFaturaComNfe) {
		this.obgEmitirFaturaComNfe = obgEmitirFaturaComNfe;
	}
	public Boolean getValorFixoEmitirFaturaComNfe() {
		return valorFixoEmitirFaturaComNfe;
	}
	public void setValorFixoEmitirFaturaComNfe(Boolean valorFixoEmitirFaturaComNfe) {
		this.valorFixoEmitirFaturaComNfe = valorFixoEmitirFaturaComNfe;
	}
	public Boolean getSelBanco() {
		return selBanco;
	}
	public void setSelBanco(Boolean selBanco) {
		this.selBanco = selBanco;
	}
	public Boolean getObgBanco() {
		return obgBanco;
	}
	public void setObgBanco(Boolean obgBanco) {
		this.obgBanco = obgBanco;
	}
	public Boolean getValorFixoBanco() {
		return valorFixoBanco;
	}
	public void setValorFixoBanco(Boolean valorFixoBanco) {
		this.valorFixoBanco = valorFixoBanco;
	}
	public Boolean getSelAgencia() {
		return selAgencia;
	}
	public void setSelAgencia(Boolean selAgencia) {
		this.selAgencia = selAgencia;
	}
	public Boolean getObgAgencia() {
		return obgAgencia;
	}
	public void setObgAgencia(Boolean obgAgencia) {
		this.obgAgencia = obgAgencia;
	}
	public Boolean getValorFixoAgencia() {
		return valorFixoAgencia;
	}
	public void setValorFixoAgencia(Boolean valorFixoAgencia) {
		this.valorFixoAgencia = valorFixoAgencia;
	}
	public Boolean getSelContaCorrente() {
		return selContaCorrente;
	}
	public void setSelContaCorrente(Boolean selContaCorrente) {
		this.selContaCorrente = selContaCorrente;
	}
	public Boolean getObgContaCorrente() {
		return obgContaCorrente;
	}
	public void setObgContaCorrente(Boolean obgContaCorrente) {
		this.obgContaCorrente = obgContaCorrente;
	}
	public Boolean getValorFixoContaCorrente() {
		return valorFixoContaCorrente;
	}
	public void setValorFixoContaCorrente(Boolean valorFixoContaCorrente) {
		this.valorFixoContaCorrente = valorFixoContaCorrente;
	}
	public Boolean getSelTipoGarantiaFinanceira() {
		return selTipoGarantiaFinanceira;
	}
	public void setSelTipoGarantiaFinanceira(Boolean selTipoGarantiaFinanceira) {
		this.selTipoGarantiaFinanceira = selTipoGarantiaFinanceira;
	}
	public Boolean getObgTipoGarantiaFinanceira() {
		return obgTipoGarantiaFinanceira;
	}
	public void setObgTipoGarantiaFinanceira(Boolean obgTipoGarantiaFinanceira) {
		this.obgTipoGarantiaFinanceira = obgTipoGarantiaFinanceira;
	}
	public Boolean getValorFixoTipoGarantiaFinanceira() {
		return valorFixoTipoGarantiaFinanceira;
	}
	public void setValorFixoTipoGarantiaFinanceira(Boolean valorFixoTipoGarantiaFinanceira) {
		this.valorFixoTipoGarantiaFinanceira = valorFixoTipoGarantiaFinanceira;
	}
	public Boolean getSelDescricaoGarantia() {
		return selDescricaoGarantia;
	}
	public void setSelDescricaoGarantia(Boolean selDescricaoGarantia) {
		this.selDescricaoGarantia = selDescricaoGarantia;
	}
	public Boolean getObgDescricaoGarantia() {
		return obgDescricaoGarantia;
	}
	public void setObgDescricaoGarantia(Boolean obgDescricaoGarantia) {
		this.obgDescricaoGarantia = obgDescricaoGarantia;
	}
	public Boolean getSelValorGarantiaFinanceira() {
		return selValorGarantiaFinanceira;
	}
	public void setSelValorGarantiaFinanceira(Boolean selValorGarantiaFinanceira) {
		this.selValorGarantiaFinanceira = selValorGarantiaFinanceira;
	}
	public Boolean getObgValorGarantiaFinanceira() {
		return obgValorGarantiaFinanceira;
	}
	public void setObgValorGarantiaFinanceira(Boolean obgValorGarantiaFinanceira) {
		this.obgValorGarantiaFinanceira = obgValorGarantiaFinanceira;
	}
	public Boolean getValorFixoValorGarantiaFinanceira() {
		return valorFixoValorGarantiaFinanceira;
	}
	public void setValorFixoValorGarantiaFinanceira(Boolean valorFixoValorGarantiaFinanceira) {
		this.valorFixoValorGarantiaFinanceira = valorFixoValorGarantiaFinanceira;
	}
	public Boolean getSelDataInicioGarantiaFinanceira() {
		return selDataInicioGarantiaFinanceira;
	}
	public void setSelDataInicioGarantiaFinanceira(Boolean selDataInicioGarantiaFinanceira) {
		this.selDataInicioGarantiaFinanceira = selDataInicioGarantiaFinanceira;
	}
	public Boolean getObgDataInicioGarantiaFinanceira() {
		return obgDataInicioGarantiaFinanceira;
	}
	public void setObgDataInicioGarantiaFinanceira(Boolean obgDataInicioGarantiaFinanceira) {
		this.obgDataInicioGarantiaFinanceira = obgDataInicioGarantiaFinanceira;
	}
	public Boolean getSelDataFinalGarantiaFinanceira() {
		return selDataFinalGarantiaFinanceira;
	}
	public void setSelDataFinalGarantiaFinanceira(Boolean selDataFinalGarantiaFinanceira) {
		this.selDataFinalGarantiaFinanceira = selDataFinalGarantiaFinanceira;
	}
	public Boolean getObgDataFinalGarantiaFinanceira() {
		return obgDataFinalGarantiaFinanceira;
	}
	public void setObgDataFinalGarantiaFinanceira(Boolean obgDataFinalGarantiaFinanceira) {
		this.obgDataFinalGarantiaFinanceira = obgDataFinalGarantiaFinanceira;
	}
	public Boolean getSelGarantiaFinanceiraRenovada() {
		return selGarantiaFinanceiraRenovada;
	}
	public void setSelGarantiaFinanceiraRenovada(Boolean selGarantiaFinanceiraRenovada) {
		this.selGarantiaFinanceiraRenovada = selGarantiaFinanceiraRenovada;
	}
	public Boolean getObgGarantiaFinanceiraRenovada() {
		return obgGarantiaFinanceiraRenovada;
	}
	public void setObgGarantiaFinanceiraRenovada(Boolean obgGarantiaFinanceiraRenovada) {
		this.obgGarantiaFinanceiraRenovada = obgGarantiaFinanceiraRenovada;
	}
	public Boolean getValorFixoGarantiaFinanceiraRenovada() {
		return valorFixoGarantiaFinanceiraRenovada;
	}
	public void setValorFixoGarantiaFinanceiraRenovada(Boolean valorFixoGarantiaFinanceiraRenovada) {
		this.valorFixoGarantiaFinanceiraRenovada = valorFixoGarantiaFinanceiraRenovada;
	}
	public Boolean getSelHoraInicialDia() {
		return selHoraInicialDia;
	}
	public void setSelHoraInicialDia(Boolean selHoraInicialDia) {
		this.selHoraInicialDia = selHoraInicialDia;
	}
	public Boolean getObgHoraInicialDia() {
		return obgHoraInicialDia;
	}
	public void setObgHoraInicialDia(Boolean obgHoraInicialDia) {
		this.obgHoraInicialDia = obgHoraInicialDia;
	}
	public Boolean getValorFixoHoraInicialDia() {
		return valorFixoHoraInicialDia;
	}
	public void setValorFixoHoraInicialDia(Boolean valorFixoHoraInicialDia) {
		this.valorFixoHoraInicialDia = valorFixoHoraInicialDia;
	}
	public Boolean getSelRegimeConsumo() {
		return selRegimeConsumo;
	}
	public void setSelRegimeConsumo(Boolean selRegimeConsumo) {
		this.selRegimeConsumo = selRegimeConsumo;
	}
	public Boolean getObgRegimeConsumo() {
		return obgRegimeConsumo;
	}
	public void setObgRegimeConsumo(Boolean obgRegimeConsumo) {
		this.obgRegimeConsumo = obgRegimeConsumo;
	}
	public Boolean getValorFixoRegimeConsumo() {
		return valorFixoRegimeConsumo;
	}
	public void setValorFixoRegimeConsumo(Boolean valorFixoRegimeConsumo) {
		this.valorFixoRegimeConsumo = valorFixoRegimeConsumo;
	}
	public Boolean getSelFornecimentoMaximoDiario() {
		return selFornecimentoMaximoDiario;
	}
	public void setSelFornecimentoMaximoDiario(Boolean selFornecimentoMaximoDiario) {
		this.selFornecimentoMaximoDiario = selFornecimentoMaximoDiario;
	}
	public Boolean getObgFornecimentoMaximoDiario() {
		return obgFornecimentoMaximoDiario;
	}
	public void setObgFornecimentoMaximoDiario(Boolean obgFornecimentoMaximoDiario) {
		this.obgFornecimentoMaximoDiario = obgFornecimentoMaximoDiario;
	}
	public Boolean getValorFixoFornecimentoMaximoDiario() {
		return valorFixoFornecimentoMaximoDiario;
	}
	public void setValorFixoFornecimentoMaximoDiario(Boolean valorFixoFornecimentoMaximoDiario) {
		this.valorFixoFornecimentoMaximoDiario = valorFixoFornecimentoMaximoDiario;
	}
	public Boolean getSelFornecimentoMinimoDiario() {
		return selFornecimentoMinimoDiario;
	}
	public void setSelFornecimentoMinimoDiario(Boolean selFornecimentoMinimoDiario) {
		this.selFornecimentoMinimoDiario = selFornecimentoMinimoDiario;
	}
	public Boolean getObgFornecimentoMinimoDiario() {
		return obgFornecimentoMinimoDiario;
	}
	public void setObgFornecimentoMinimoDiario(Boolean obgFornecimentoMinimoDiario) {
		this.obgFornecimentoMinimoDiario = obgFornecimentoMinimoDiario;
	}
	public Boolean getValorFixoFornecimentoMinimoDiario() {
		return valorFixoFornecimentoMinimoDiario;
	}
	public void setValorFixoFornecimentoMinimoDiario(Boolean valorFixoFornecimentoMinimoDiario) {
		this.valorFixoFornecimentoMinimoDiario = valorFixoFornecimentoMinimoDiario;
	}
	public Boolean getSelFornecimentoMinimoMensal() {
		return selFornecimentoMinimoMensal;
	}
	public void setSelFornecimentoMinimoMensal(Boolean selFornecimentoMinimoMensal) {
		this.selFornecimentoMinimoMensal = selFornecimentoMinimoMensal;
	}
	public Boolean getObgFornecimentoMinimoMensal() {
		return obgFornecimentoMinimoMensal;
	}
	public void setObgFornecimentoMinimoMensal(Boolean obgFornecimentoMinimoMensal) {
		this.obgFornecimentoMinimoMensal = obgFornecimentoMinimoMensal;
	}
	public Boolean getValorFixoFornecimentoMinimoMensal() {
		return valorFixoFornecimentoMinimoMensal;
	}
	public void setValorFixoFornecimentoMinimoMensal(Boolean valorFixoFornecimentoMinimoMensal) {
		this.valorFixoFornecimentoMinimoMensal = valorFixoFornecimentoMinimoMensal;
	}
	public Boolean getSelFornecimentoMinimoAnual() {
		return selFornecimentoMinimoAnual;
	}
	public void setSelFornecimentoMinimoAnual(Boolean selFornecimentoMinimoAnual) {
		this.selFornecimentoMinimoAnual = selFornecimentoMinimoAnual;
	}
	public Boolean getObgFornecimentoMinimoAnual() {
		return obgFornecimentoMinimoAnual;
	}
	public void setObgFornecimentoMinimoAnual(Boolean obgFornecimentoMinimoAnual) {
		this.obgFornecimentoMinimoAnual = obgFornecimentoMinimoAnual;
	}
	public Boolean getValorFixoFornecimentoMinimoAnual() {
		return valorFixoFornecimentoMinimoAnual;
	}
	public void setValorFixoFornecimentoMinimoAnual(Boolean valorFixoFornecimentoMinimoAnual) {
		this.valorFixoFornecimentoMinimoAnual = valorFixoFornecimentoMinimoAnual;
	}
	public Boolean getSelFatorUnicoCorrecao() {
		return selFatorUnicoCorrecao;
	}
	public void setSelFatorUnicoCorrecao(Boolean selFatorUnicoCorrecao) {
		this.selFatorUnicoCorrecao = selFatorUnicoCorrecao;
	}
	public Boolean getObgFatorUnicoCorrecao() {
		return obgFatorUnicoCorrecao;
	}
	public void setObgFatorUnicoCorrecao(Boolean obgFatorUnicoCorrecao) {
		this.obgFatorUnicoCorrecao = obgFatorUnicoCorrecao;
	}
	public Boolean getSelConsumoFatFalhaMedicao() {
		return selConsumoFatFalhaMedicao;
	}
	public void setSelConsumoFatFalhaMedicao(Boolean selConsumoFatFalhaMedicao) {
		this.selConsumoFatFalhaMedicao = selConsumoFatFalhaMedicao;
	}
	public Boolean getObgConsumoFatFalhaMedicao() {
		return obgConsumoFatFalhaMedicao;
	}
	public void setObgConsumoFatFalhaMedicao(Boolean obgConsumoFatFalhaMedicao) {
		this.obgConsumoFatFalhaMedicao = obgConsumoFatFalhaMedicao;
	}
	public Boolean getValorFixoConsumoFatFalhaMedicao() {
		return valorFixoConsumoFatFalhaMedicao;
	}
	public void setValorFixoConsumoFatFalhaMedicao(Boolean valorFixoConsumoFatFalhaMedicao) {
		this.valorFixoConsumoFatFalhaMedicao = valorFixoConsumoFatFalhaMedicao;
	}
	public Boolean getSelModalidade() {
		return selModalidade;
	}
	public void setSelModalidade(Boolean selModalidade) {
		this.selModalidade = selModalidade;
	}
	public Boolean getObgModalidade() {
		return obgModalidade;
	}
	public void setObgModalidade(Boolean obgModalidade) {
		this.obgModalidade = obgModalidade;
	}
	public Boolean getSelQdc() {
		return selQdc;
	}
	public void setSelQdc(Boolean selQdc) {
		this.selQdc = selQdc;
	}
	public Boolean getObgQdc() {
		return obgQdc;
	}
	public void setObgQdc(Boolean obgQdc) {
		this.obgQdc = obgQdc;
	}
	public Boolean getSelDataVigenciaQDC() {
		return selDataVigenciaQDC;
	}
	public void setSelDataVigenciaQDC(Boolean selDataVigenciaQDC) {
		this.selDataVigenciaQDC = selDataVigenciaQDC;
	}
	public Boolean getObgDataVigenciaQDC() {
		return obgDataVigenciaQDC;
	}
	public void setObgDataVigenciaQDC(Boolean obgDataVigenciaQDC) {
		this.obgDataVigenciaQDC = obgDataVigenciaQDC;
	}
	public Boolean getSelPrazoRevizaoQDC() {
		return selPrazoRevizaoQDC;
	}
	public void setSelPrazoRevizaoQDC(Boolean selPrazoRevizaoQDC) {
		this.selPrazoRevizaoQDC = selPrazoRevizaoQDC;
	}
	public Boolean getObgPrazoRevizaoQDC() {
		return obgPrazoRevizaoQDC;
	}
	public void setObgPrazoRevizaoQDC(Boolean obgPrazoRevizaoQDC) {
		this.obgPrazoRevizaoQDC = obgPrazoRevizaoQDC;
	}
	public Boolean getSelProgramacaoConsumo() {
		return selProgramacaoConsumo;
	}
	public void setSelProgramacaoConsumo(Boolean selProgramacaoConsumo) {
		this.selProgramacaoConsumo = selProgramacaoConsumo;
	}
	public Boolean getObgProgramacaoConsumo() {
		return obgProgramacaoConsumo;
	}
	public void setObgProgramacaoConsumo(Boolean obgProgramacaoConsumo) {
		this.obgProgramacaoConsumo = obgProgramacaoConsumo;
	}
	public Boolean getSelQdsMaiorQDC() {
		return selQdsMaiorQDC;
	}
	public void setSelQdsMaiorQDC(Boolean selQdsMaiorQDC) {
		this.selQdsMaiorQDC = selQdsMaiorQDC;
	}
	public Boolean getObgQdsMaiorQDC() {
		return obgQdsMaiorQDC;
	}
	public void setObgQdsMaiorQDC(Boolean obgQdsMaiorQDC) {
		this.obgQdsMaiorQDC = obgQdsMaiorQDC;
	}
	public Boolean getSelDiasAntecSolicConsumo() {
		return selDiasAntecSolicConsumo;
	}
	public void setSelDiasAntecSolicConsumo(Boolean selDiasAntecSolicConsumo) {
		this.selDiasAntecSolicConsumo = selDiasAntecSolicConsumo;
	}
	public Boolean getObgDiasAntecSolicConsumo() {
		return obgDiasAntecSolicConsumo;
	}
	public void setObgDiasAntecSolicConsumo(Boolean obgDiasAntecSolicConsumo) {
		this.obgDiasAntecSolicConsumo = obgDiasAntecSolicConsumo;
	}
	public Boolean getSelIndicadorProgramacaoConsumo() {
		return selIndicadorProgramacaoConsumo;
	}
	public void setSelIndicadorProgramacaoConsumo(Boolean selIndicadorProgramacaoConsumo) {
		this.selIndicadorProgramacaoConsumo = selIndicadorProgramacaoConsumo;
	}
	public Boolean getObgIndicadorProgramacaoConsumo() {
		return obgIndicadorProgramacaoConsumo;
	}
	public void setObgIndicadorProgramacaoConsumo(Boolean obgIndicadorProgramacaoConsumo) {
		this.obgIndicadorProgramacaoConsumo = obgIndicadorProgramacaoConsumo;
	}
	public Boolean getSelHoraLimiteProgramacaoDiaria() {
		return selHoraLimiteProgramacaoDiaria;
	}
	public void setSelHoraLimiteProgramacaoDiaria(Boolean selHoraLimiteProgramacaoDiaria) {
		this.selHoraLimiteProgramacaoDiaria = selHoraLimiteProgramacaoDiaria;
	}
	public Boolean getObgHoraLimiteProgramacaoDiaria() {
		return obgHoraLimiteProgramacaoDiaria;
	}
	public void setObgHoraLimiteProgramacaoDiaria(Boolean obgHoraLimiteProgramacaoDiaria) {
		this.obgHoraLimiteProgramacaoDiaria = obgHoraLimiteProgramacaoDiaria;
	}
	public Boolean getSelHoraLimiteProgIntradiaria() {
		return selHoraLimiteProgIntradiaria;
	}
	public void setSelHoraLimiteProgIntradiaria(Boolean selHoraLimiteProgIntradiaria) {
		this.selHoraLimiteProgIntradiaria = selHoraLimiteProgIntradiaria;
	}
	public Boolean getObgHoraLimiteProgIntradiaria() {
		return obgHoraLimiteProgIntradiaria;
	}
	public void setObgHoraLimiteProgIntradiaria(Boolean obgHoraLimiteProgIntradiaria) {
		this.obgHoraLimiteProgIntradiaria = obgHoraLimiteProgIntradiaria;
	}
	public Boolean getSelHoraLimiteAceitacaoDiaria() {
		return selHoraLimiteAceitacaoDiaria;
	}
	public void setSelHoraLimiteAceitacaoDiaria(Boolean selHoraLimiteAceitacaoDiaria) {
		this.selHoraLimiteAceitacaoDiaria = selHoraLimiteAceitacaoDiaria;
	}
	public Boolean getObgHoraLimiteAceitacaoDiaria() {
		return obgHoraLimiteAceitacaoDiaria;
	}
	public void setObgHoraLimiteAceitacaoDiaria(Boolean obgHoraLimiteAceitacaoDiaria) {
		this.obgHoraLimiteAceitacaoDiaria = obgHoraLimiteAceitacaoDiaria;
	}
	public Boolean getSelHoraLimiteAceitIntradiaria() {
		return selHoraLimiteAceitIntradiaria;
	}
	public void setSelHoraLimiteAceitIntradiaria(Boolean selHoraLimiteAceitIntradiaria) {
		this.selHoraLimiteAceitIntradiaria = selHoraLimiteAceitIntradiaria;
	}
	public Boolean getObgHoraLimiteAceitIntradiaria() {
		return obgHoraLimiteAceitIntradiaria;
	}
	public void setObgHoraLimiteAceitIntradiaria(Boolean obgHoraLimiteAceitIntradiaria) {
		this.obgHoraLimiteAceitIntradiaria = obgHoraLimiteAceitIntradiaria;
	}
	public Boolean getSelVariacaoSuperiorQDC() {
		return selVariacaoSuperiorQDC;
	}
	public void setSelVariacaoSuperiorQDC(Boolean selVariacaoSuperiorQDC) {
		this.selVariacaoSuperiorQDC = selVariacaoSuperiorQDC;
	}
	public Boolean getObgVariacaoSuperiorQDC() {
		return obgVariacaoSuperiorQDC;
	}
	public void setObgVariacaoSuperiorQDC(Boolean obgVariacaoSuperiorQDC) {
		this.obgVariacaoSuperiorQDC = obgVariacaoSuperiorQDC;
	}
	public Boolean getSelNumMesesSolicConsumo() {
		return selNumMesesSolicConsumo;
	}
	public void setSelNumMesesSolicConsumo(Boolean selNumMesesSolicConsumo) {
		this.selNumMesesSolicConsumo = selNumMesesSolicConsumo;
	}
	public Boolean getObgNumMesesSolicConsumo() {
		return obgNumMesesSolicConsumo;
	}
	public void setObgNumMesesSolicConsumo(Boolean obgNumMesesSolicConsumo) {
		this.obgNumMesesSolicConsumo = obgNumMesesSolicConsumo;
	}
	public Boolean getSelNumHorasSolicConsumo() {
		return selNumHorasSolicConsumo;
	}
	public void setSelNumHorasSolicConsumo(Boolean selNumHorasSolicConsumo) {
		this.selNumHorasSolicConsumo = selNumHorasSolicConsumo;
	}
	public Boolean getObgNumHorasSolicConsumo() {
		return obgNumHorasSolicConsumo;
	}
	public void setObgNumHorasSolicConsumo(Boolean obgNumHorasSolicConsumo) {
		this.obgNumHorasSolicConsumo = obgNumHorasSolicConsumo;
	}
	public Boolean getSelConfirmacaoAutomaticaQDS() {
		return selConfirmacaoAutomaticaQDS;
	}
	public void setSelConfirmacaoAutomaticaQDS(Boolean selConfirmacaoAutomaticaQDS) {
		this.selConfirmacaoAutomaticaQDS = selConfirmacaoAutomaticaQDS;
	}
	public Boolean getObgConfirmacaoAutomaticaQDS() {
		return obgConfirmacaoAutomaticaQDS;
	}
	public void setObgConfirmacaoAutomaticaQDS(Boolean obgConfirmacaoAutomaticaQDS) {
		this.obgConfirmacaoAutomaticaQDS = obgConfirmacaoAutomaticaQDS;
	}
	public Boolean getSelDataInicioRetiradaQPNR() {
		return selDataInicioRetiradaQPNR;
	}
	public void setSelDataInicioRetiradaQPNR(Boolean selDataInicioRetiradaQPNR) {
		this.selDataInicioRetiradaQPNR = selDataInicioRetiradaQPNR;
	}
	public Boolean getObgDataInicioRetiradaQPNR() {
		return obgDataInicioRetiradaQPNR;
	}
	public void setObgDataInicioRetiradaQPNR(Boolean obgDataInicioRetiradaQPNR) {
		this.obgDataInicioRetiradaQPNR = obgDataInicioRetiradaQPNR;
	}
	public Boolean getSelDataFimRetiradaQPNR() {
		return selDataFimRetiradaQPNR;
	}
	public void setSelDataFimRetiradaQPNR(Boolean selDataFimRetiradaQPNR) {
		this.selDataFimRetiradaQPNR = selDataFimRetiradaQPNR;
	}
	public Boolean getObgDataFimRetiradaQPNR() {
		return obgDataFimRetiradaQPNR;
	}
	public void setObgDataFimRetiradaQPNR(Boolean obgDataFimRetiradaQPNR) {
		this.obgDataFimRetiradaQPNR = obgDataFimRetiradaQPNR;
	}
	public Boolean getSelPercDuranteRetiradaQPNR() {
		return selPercDuranteRetiradaQPNR;
	}
	public void setSelPercDuranteRetiradaQPNR(Boolean selPercDuranteRetiradaQPNR) {
		this.selPercDuranteRetiradaQPNR = selPercDuranteRetiradaQPNR;
	}
	public Boolean getObgPercDuranteRetiradaQPNR() {
		return obgPercDuranteRetiradaQPNR;
	}
	public void setObgPercDuranteRetiradaQPNR(Boolean obgPercDuranteRetiradaQPNR) {
		this.obgPercDuranteRetiradaQPNR = obgPercDuranteRetiradaQPNR;
	}
	public Boolean getSelPercMinDuranteRetiradaQPNR() {
		return selPercMinDuranteRetiradaQPNR;
	}
	public void setSelPercMinDuranteRetiradaQPNR(Boolean selPercMinDuranteRetiradaQPNR) {
		this.selPercMinDuranteRetiradaQPNR = selPercMinDuranteRetiradaQPNR;
	}
	public Boolean getObgPercMinDuranteRetiradaQPNR() {
		return obgPercMinDuranteRetiradaQPNR;
	}
	public void setObgPercMinDuranteRetiradaQPNR(Boolean obgPercMinDuranteRetiradaQPNR) {
		this.obgPercMinDuranteRetiradaQPNR = obgPercMinDuranteRetiradaQPNR;
	}
	public Boolean getSelPercFimRetiradaQPNR() {
		return selPercFimRetiradaQPNR;
	}
	public void setSelPercFimRetiradaQPNR(Boolean selPercFimRetiradaQPNR) {
		this.selPercFimRetiradaQPNR = selPercFimRetiradaQPNR;
	}
	public Boolean getObgPercFimRetiradaQPNR() {
		return obgPercFimRetiradaQPNR;
	}
	public void setObgPercFimRetiradaQPNR(Boolean obgPercFimRetiradaQPNR) {
		this.obgPercFimRetiradaQPNR = obgPercFimRetiradaQPNR;
	}
	public Boolean getSelLocalAmostragemPCS() {
		return selLocalAmostragemPCS;
	}
	public void setSelLocalAmostragemPCS(Boolean selLocalAmostragemPCS) {
		this.selLocalAmostragemPCS = selLocalAmostragemPCS;
	}
	public Boolean getObgLocalAmostragemPCS() {
		return obgLocalAmostragemPCS;
	}
	public void setObgLocalAmostragemPCS(Boolean obgLocalAmostragemPCS) {
		this.obgLocalAmostragemPCS = obgLocalAmostragemPCS;
	}
	public Boolean getSelIntervaloRecuperacaoPCS() {
		return selIntervaloRecuperacaoPCS;
	}
	public void setSelIntervaloRecuperacaoPCS(Boolean selIntervaloRecuperacaoPCS) {
		this.selIntervaloRecuperacaoPCS = selIntervaloRecuperacaoPCS;
	}
	public Boolean getObgIntervaloRecuperacaoPCS() {
		return obgIntervaloRecuperacaoPCS;
	}
	public void setObgIntervaloRecuperacaoPCS(Boolean obgIntervaloRecuperacaoPCS) {
		this.obgIntervaloRecuperacaoPCS = obgIntervaloRecuperacaoPCS;
	}
	public Boolean getSelTamReducaoRecuperacaoPCS() {
		return selTamReducaoRecuperacaoPCS;
	}
	public void setSelTamReducaoRecuperacaoPCS(Boolean selTamReducaoRecuperacaoPCS) {
		this.selTamReducaoRecuperacaoPCS = selTamReducaoRecuperacaoPCS;
	}
	public Boolean getObgTamReducaoRecuperacaoPCS() {
		return obgTamReducaoRecuperacaoPCS;
	}
	public void setObgTamReducaoRecuperacaoPCS(Boolean obgTamReducaoRecuperacaoPCS) {
		this.obgTamReducaoRecuperacaoPCS = obgTamReducaoRecuperacaoPCS;
	}
	public Boolean getSelPeriodicidadeTakeOrPay() {
		return selPeriodicidadeTakeOrPay;
	}
	public void setSelPeriodicidadeTakeOrPay(Boolean selPeriodicidadeTakeOrPay) {
		this.selPeriodicidadeTakeOrPay = selPeriodicidadeTakeOrPay;
	}
	public Boolean getObgPeriodicidadeTakeOrPay() {
		return obgPeriodicidadeTakeOrPay;
	}
	public void setObgPeriodicidadeTakeOrPay(Boolean obgPeriodicidadeTakeOrPay) {
		this.obgPeriodicidadeTakeOrPay = obgPeriodicidadeTakeOrPay;
	}
	public Boolean getSelPeriodicidadeShipOrPay() {
		return selPeriodicidadeShipOrPay;
	}
	public void setSelPeriodicidadeShipOrPay(Boolean selPeriodicidadeShipOrPay) {
		this.selPeriodicidadeShipOrPay = selPeriodicidadeShipOrPay;
	}
	public Boolean getObgPeriodicidadeShipOrPay() {
		return obgPeriodicidadeShipOrPay;
	}
	public void setObgPeriodicidadeShipOrPay(Boolean obgPeriodicidadeShipOrPay) {
		this.obgPeriodicidadeShipOrPay = obgPeriodicidadeShipOrPay;
	}
	public Boolean getSelConsumoReferenciaTakeOrPay() {
		return selConsumoReferenciaTakeOrPay;
	}
	public void setSelConsumoReferenciaTakeOrPay(Boolean selConsumoReferenciaTakeOrPay) {
		this.selConsumoReferenciaTakeOrPay = selConsumoReferenciaTakeOrPay;
	}
	public Boolean getObgConsumoReferenciaTakeOrPay() {
		return obgConsumoReferenciaTakeOrPay;
	}
	public void setObgConsumoReferenciaTakeOrPay(Boolean obgConsumoReferenciaTakeOrPay) {
		this.obgConsumoReferenciaTakeOrPay = obgConsumoReferenciaTakeOrPay;
	}
	public Boolean getSelMargemVariacaoTakeOrPay() {
		return selMargemVariacaoTakeOrPay;
	}
	public void setSelMargemVariacaoTakeOrPay(Boolean selMargemVariacaoTakeOrPay) {
		this.selMargemVariacaoTakeOrPay = selMargemVariacaoTakeOrPay;
	}
	public Boolean getObgMargemVariacaoTakeOrPay() {
		return obgMargemVariacaoTakeOrPay;
	}
	public void setObgMargemVariacaoTakeOrPay(Boolean obgMargemVariacaoTakeOrPay) {
		this.obgMargemVariacaoTakeOrPay = obgMargemVariacaoTakeOrPay;
	}
	public Boolean getSelRecuperacaoAutoTakeOrPay() {
		return selRecuperacaoAutoTakeOrPay;
	}
	public void setSelRecuperacaoAutoTakeOrPay(Boolean selRecuperacaoAutoTakeOrPay) {
		this.selRecuperacaoAutoTakeOrPay = selRecuperacaoAutoTakeOrPay;
	}
	public Boolean getObgRecuperacaoAutoTakeOrPay() {
		return obgRecuperacaoAutoTakeOrPay;
	}
	public void setObgRecuperacaoAutoTakeOrPay(Boolean obgRecuperacaoAutoTakeOrPay) {
		this.obgRecuperacaoAutoTakeOrPay = obgRecuperacaoAutoTakeOrPay;
	}
	public Boolean getSelConsumoReferenciaShipOrPay() {
		return selConsumoReferenciaShipOrPay;
	}
	public void setSelConsumoReferenciaShipOrPay(Boolean selConsumoReferenciaShipOrPay) {
		this.selConsumoReferenciaShipOrPay = selConsumoReferenciaShipOrPay;
	}
	public Boolean getObgConsumoReferenciaShipOrPay() {
		return obgConsumoReferenciaShipOrPay;
	}
	public void setObgConsumoReferenciaShipOrPay(Boolean obgConsumoReferenciaShipOrPay) {
		this.obgConsumoReferenciaShipOrPay = obgConsumoReferenciaShipOrPay;
	}
	public Boolean getSelMargemVariacaoShipOrPay() {
		return selMargemVariacaoShipOrPay;
	}
	public void setSelMargemVariacaoShipOrPay(Boolean selMargemVariacaoShipOrPay) {
		this.selMargemVariacaoShipOrPay = selMargemVariacaoShipOrPay;
	}
	public Boolean getObgMargemVariacaoShipOrPay() {
		return obgMargemVariacaoShipOrPay;
	}
	public void setObgMargemVariacaoShipOrPay(Boolean obgMargemVariacaoShipOrPay) {
		this.obgMargemVariacaoShipOrPay = obgMargemVariacaoShipOrPay;
	}
	public Boolean getSelConsumoReferenciaSobreDem() {
		return selConsumoReferenciaSobreDem;
	}
	public void setSelConsumoReferenciaSobreDem(Boolean selConsumoReferenciaSobreDem) {
		this.selConsumoReferenciaSobreDem = selConsumoReferenciaSobreDem;
	}
	public Boolean getObgConsumoReferenciaSobreDem() {
		return obgConsumoReferenciaSobreDem;
	}
	public void setObgConsumoReferenciaSobreDem(Boolean obgConsumoReferenciaSobreDem) {
		this.obgConsumoReferenciaSobreDem = obgConsumoReferenciaSobreDem;
	}
	public Boolean getSelFaixaPenalidadeSobreDem() {
		return selFaixaPenalidadeSobreDem;
	}
	public void setSelFaixaPenalidadeSobreDem(Boolean selFaixaPenalidadeSobreDem) {
		this.selFaixaPenalidadeSobreDem = selFaixaPenalidadeSobreDem;
	}
	public Boolean getObgFaixaPenalidadeSobreDem() {
		return obgFaixaPenalidadeSobreDem;
	}
	public void setObgFaixaPenalidadeSobreDem(Boolean obgFaixaPenalidadeSobreDem) {
		this.obgFaixaPenalidadeSobreDem = obgFaixaPenalidadeSobreDem;
	}
	public Boolean getSelConsumoReferenciaSobDem() {
		return selConsumoReferenciaSobDem;
	}
	public void setSelConsumoReferenciaSobDem(Boolean selConsumoReferenciaSobDem) {
		this.selConsumoReferenciaSobDem = selConsumoReferenciaSobDem;
	}
	public Boolean getObgConsumoReferenciaSobDem() {
		return obgConsumoReferenciaSobDem;
	}
	public void setObgConsumoReferenciaSobDem(Boolean obgConsumoReferenciaSobDem) {
		this.obgConsumoReferenciaSobDem = obgConsumoReferenciaSobDem;
	}
	public Boolean getSelFaixaPenalidadeSobDem() {
		return selFaixaPenalidadeSobDem;
	}
	public void setSelFaixaPenalidadeSobDem(Boolean selFaixaPenalidadeSobDem) {
		this.selFaixaPenalidadeSobDem = selFaixaPenalidadeSobDem;
	}
	public Boolean getObgFaixaPenalidadeSobDem() {
		return obgFaixaPenalidadeSobDem;
	}
	public void setObgFaixaPenalidadeSobDem(Boolean obgFaixaPenalidadeSobDem) {
		this.obgFaixaPenalidadeSobDem = obgFaixaPenalidadeSobDem;
	}
	public Boolean getSelPercentualQNR() {
		return selPercentualQNR;
	}
	public void setSelPercentualQNR(Boolean selPercentualQNR) {
		this.selPercentualQNR = selPercentualQNR;
	}
	public Boolean getObgPercentualQNR() {
		return obgPercentualQNR;
	}
	public void setObgPercentualQNR(Boolean obgPercentualQNR) {
		this.obgPercentualQNR = obgPercentualQNR;
	}
	public Boolean getSelClienteResponsavel() {
		return selClienteResponsavel;
	}
	public void setSelClienteResponsavel(Boolean selClienteResponsavel) {
		this.selClienteResponsavel = selClienteResponsavel;
	}
	public Boolean getObgClienteResponsavel() {
		return obgClienteResponsavel;
	}
	public void setObgClienteResponsavel(Boolean obgClienteResponsavel) {
		this.obgClienteResponsavel = obgClienteResponsavel;
	}
	public Boolean getSelTipoResponsabilidade() {
		return selTipoResponsabilidade;
	}
	public void setSelTipoResponsabilidade(Boolean selTipoResponsabilidade) {
		this.selTipoResponsabilidade = selTipoResponsabilidade;
	}
	public Boolean getObgTipoResponsabilidade() {
		return obgTipoResponsabilidade;
	}
	public void setObgTipoResponsabilidade(Boolean obgTipoResponsabilidade) {
		this.obgTipoResponsabilidade = obgTipoResponsabilidade;
	}
	public Boolean getValorFixoTipoResponsabilidade() {
		return valorFixoTipoResponsabilidade;
	}
	public void setValorFixoTipoResponsabilidade(Boolean valorFixoTipoResponsabilidade) {
		this.valorFixoTipoResponsabilidade = valorFixoTipoResponsabilidade;
	}
	public Boolean getSelDataInicioRelacao() {
		return selDataInicioRelacao;
	}
	public void setSelDataInicioRelacao(Boolean selDataInicioRelacao) {
		this.selDataInicioRelacao = selDataInicioRelacao;
	}
	public Boolean getObgDataInicioRelacao() {
		return obgDataInicioRelacao;
	}
	public void setObgDataInicioRelacao(Boolean obgDataInicioRelacao) {
		this.obgDataInicioRelacao = obgDataInicioRelacao;
	}
	public Boolean getSelDataFimRelacao() {
		return selDataFimRelacao;
	}
	public void setSelDataFimRelacao(Boolean selDataFimRelacao) {
		this.selDataFimRelacao = selDataFimRelacao;
	}
	public Boolean getObgDataFimRelacao() {
		return obgDataFimRelacao;
	}
	public void setObgDataFimRelacao(Boolean obgDataFimRelacao) {
		this.obgDataFimRelacao = obgDataFimRelacao;
	}
	public Boolean getSelQDCContrato() {
		return selQDCContrato;
	}
	public void setSelQDCContrato(Boolean selQDCContrato) {
		this.selQDCContrato = selQDCContrato;
	}
	public Boolean getObgQDCContrato() {
		return obgQDCContrato;
	}
	public void setObgQDCContrato(Boolean obgQDCContrato) {
		this.obgQDCContrato = obgQDCContrato;
	}
	public Boolean getSelTempoValidadeRetiradaQPNR() {
		return selTempoValidadeRetiradaQPNR;
	}
	public void setSelTempoValidadeRetiradaQPNR(Boolean selTempoValidadeRetiradaQPNR) {
		this.selTempoValidadeRetiradaQPNR = selTempoValidadeRetiradaQPNR;
	}
	public Boolean getObgTempoValidadeRetiradaQPNR() {
		return obgTempoValidadeRetiradaQPNR;
	}
	public void setObgTempoValidadeRetiradaQPNR(Boolean obgTempoValidadeRetiradaQPNR) {
		this.obgTempoValidadeRetiradaQPNR = obgTempoValidadeRetiradaQPNR;
	}
	public Boolean getSelReferenciaQFParadaProgramada() {
		return selReferenciaQFParadaProgramada;
	}
	public void setSelReferenciaQFParadaProgramada(Boolean selReferenciaQFParadaProgramada) {
		this.selReferenciaQFParadaProgramada = selReferenciaQFParadaProgramada;
	}
	public Boolean getObgReferenciaQFParadaProgramada() {
		return obgReferenciaQFParadaProgramada;
	}
	public void setObgReferenciaQFParadaProgramada(Boolean obgReferenciaQFParadaProgramada) {
		this.obgReferenciaQFParadaProgramada = obgReferenciaQFParadaProgramada;
	}
	public Boolean getSelPercentualMargemVariacao() {
		return selPercentualMargemVariacao;
	}
	public void setSelPercentualMargemVariacao(Boolean selPercentualMargemVariacao) {
		this.selPercentualMargemVariacao = selPercentualMargemVariacao;
	}
	public Boolean getSelPeriodicidadePenalidade() {
		return selPeriodicidadePenalidade;
	}
	public void setSelPeriodicidadePenalidade(Boolean selPeriodicidadePenalidade) {
		this.selPeriodicidadePenalidade = selPeriodicidadePenalidade;
	}
	public Boolean getSelConsumoReferencia() {
		return selConsumoReferencia;
	}
	public void setSelConsumoReferencia(Boolean selConsumoReferencia) {
		this.selConsumoReferencia = selConsumoReferencia;
	}
	public Boolean getSelPenalidade() {
		return selPenalidade;
	}
	public void setSelPenalidade(Boolean selPenalidade) {
		this.selPenalidade = selPenalidade;
	}
	public Boolean getSelDataInicioVigencia() {
		return selDataInicioVigencia;
	}
	public void setSelDataInicioVigencia(Boolean selDataInicioVigencia) {
		this.selDataInicioVigencia = selDataInicioVigencia;
	}
	public Boolean getSelDataFimVigencia() {
		return selDataFimVigencia;
	}
	public void setSelDataFimVigencia(Boolean selDataFimVigencia) {
		this.selDataFimVigencia = selDataFimVigencia;
	}
	public Boolean getSelPercentualNaoRecuperavel() {
		return selPercentualNaoRecuperavel;
	}
	public void setSelPercentualNaoRecuperavel(Boolean selPercentualNaoRecuperavel) {
		this.selPercentualNaoRecuperavel = selPercentualNaoRecuperavel;
	}
	public Boolean getSelConsideraParadaProgramada() {
		return selConsideraParadaProgramada;
	}
	public void setSelConsideraParadaProgramada(Boolean selConsideraParadaProgramada) {
		this.selConsideraParadaProgramada = selConsideraParadaProgramada;
	}
	public Boolean getSelConsideraParadaNaoProgramada() {
		return selConsideraParadaNaoProgramada;
	}
	public void setSelConsideraParadaNaoProgramada(Boolean selConsideraParadaNaoProgramada) {
		this.selConsideraParadaNaoProgramada = selConsideraParadaNaoProgramada;
	}
	public Boolean getSelConsideraFalhaFornecimento() {
		return selConsideraFalhaFornecimento;
	}
	public void setSelConsideraFalhaFornecimento(Boolean selConsideraFalhaFornecimento) {
		this.selConsideraFalhaFornecimento = selConsideraFalhaFornecimento;
	}
	public Boolean getSelConsideraCasoFortuito() {
		return selConsideraCasoFortuito;
	}
	public void setSelConsideraCasoFortuito(Boolean selConsideraCasoFortuito) {
		this.selConsideraCasoFortuito = selConsideraCasoFortuito;
	}
	public Boolean getSelRecuperavel() {
		return selRecuperavel;
	}
	public void setSelRecuperavel(Boolean selRecuperavel) {
		this.selRecuperavel = selRecuperavel;
	}
	public Boolean getSelTipoApuracao() {
		return selTipoApuracao;
	}
	public void setSelTipoApuracao(Boolean selTipoApuracao) {
		this.selTipoApuracao = selTipoApuracao;
	}
	public Boolean getSelPrecoCobranca() {
		return selPrecoCobranca;
	}
	public void setSelPrecoCobranca(Boolean selPrecoCobranca) {
		this.selPrecoCobranca = selPrecoCobranca;
	}
	public Boolean getSelApuracaoParadaProgramada() {
		return selApuracaoParadaProgramada;
	}
	public void setSelApuracaoParadaProgramada(Boolean selApuracaoParadaProgramada) {
		this.selApuracaoParadaProgramada = selApuracaoParadaProgramada;
	}
	public Boolean getSelApuracaoParadaNaoProgramada() {
		return selApuracaoParadaNaoProgramada;
	}
	public void setSelApuracaoParadaNaoProgramada(Boolean selApuracaoParadaNaoProgramada) {
		this.selApuracaoParadaNaoProgramada = selApuracaoParadaNaoProgramada;
	}
	public Boolean getSelApuracaoCasoFortuito() {
		return selApuracaoCasoFortuito;
	}
	public void setSelApuracaoCasoFortuito(Boolean selApuracaoCasoFortuito) {
		this.selApuracaoCasoFortuito = selApuracaoCasoFortuito;
	}
	public Boolean getSelApuracaoFalhaFornecimento() {
		return selApuracaoFalhaFornecimento;
	}
	public void setSelApuracaoFalhaFornecimento(Boolean selApuracaoFalhaFornecimento) {
		this.selApuracaoFalhaFornecimento = selApuracaoFalhaFornecimento;
	}
	public Boolean getObgPercentualMargemVariacao() {
		return obgPercentualMargemVariacao;
	}
	public void setObgPercentualMargemVariacao(Boolean obgPercentualMargemVariacao) {
		this.obgPercentualMargemVariacao = obgPercentualMargemVariacao;
	}
	public Boolean getObgPeriodicidadePenalidade() {
		return obgPeriodicidadePenalidade;
	}
	public void setObgPeriodicidadePenalidade(Boolean obgPeriodicidadePenalidade) {
		this.obgPeriodicidadePenalidade = obgPeriodicidadePenalidade;
	}
	public Boolean getObgConsumoReferencia() {
		return obgConsumoReferencia;
	}
	public void setObgConsumoReferencia(Boolean obgConsumoReferencia) {
		this.obgConsumoReferencia = obgConsumoReferencia;
	}
	public Boolean getObgPenalidade() {
		return obgPenalidade;
	}
	public void setObgPenalidade(Boolean obgPenalidade) {
		this.obgPenalidade = obgPenalidade;
	}
	public Boolean getObgDataInicioVigencia() {
		return obgDataInicioVigencia;
	}
	public void setObgDataInicioVigencia(Boolean obgDataInicioVigencia) {
		this.obgDataInicioVigencia = obgDataInicioVigencia;
	}
	public Boolean getObgDataFimVigencia() {
		return obgDataFimVigencia;
	}
	public void setObgDataFimVigencia(Boolean obgDataFimVigencia) {
		this.obgDataFimVigencia = obgDataFimVigencia;
	}
	public Boolean getObgPercentualNaoRecuperavel() {
		return obgPercentualNaoRecuperavel;
	}
	public void setObgPercentualNaoRecuperavel(Boolean obgPercentualNaoRecuperavel) {
		this.obgPercentualNaoRecuperavel = obgPercentualNaoRecuperavel;
	}
	public Boolean getObgConsideraParadaProgramada() {
		return obgConsideraParadaProgramada;
	}
	public void setObgConsideraParadaProgramada(Boolean obgConsideraParadaProgramada) {
		this.obgConsideraParadaProgramada = obgConsideraParadaProgramada;
	}
	public Boolean getObgConsideraParadaNaoProgramada() {
		return obgConsideraParadaNaoProgramada;
	}
	public void setObgConsideraParadaNaoProgramada(Boolean obgConsideraParadaNaoProgramada) {
		this.obgConsideraParadaNaoProgramada = obgConsideraParadaNaoProgramada;
	}
	public Boolean getObgConsideraFalhaFornecimento() {
		return obgConsideraFalhaFornecimento;
	}
	public void setObgConsideraFalhaFornecimento(Boolean obgConsideraFalhaFornecimento) {
		this.obgConsideraFalhaFornecimento = obgConsideraFalhaFornecimento;
	}
	public Boolean getObgConsideraCasoFortuito() {
		return obgConsideraCasoFortuito;
	}
	public void setObgConsideraCasoFortuito(Boolean obgConsideraCasoFortuito) {
		this.obgConsideraCasoFortuito = obgConsideraCasoFortuito;
	}
	public Boolean getObgRecuperavel() {
		return obgRecuperavel;
	}
	public void setObgRecuperavel(Boolean obgRecuperavel) {
		this.obgRecuperavel = obgRecuperavel;
	}
	public Boolean getObgTipoApuracao() {
		return obgTipoApuracao;
	}
	public void setObgTipoApuracao(Boolean obgTipoApuracao) {
		this.obgTipoApuracao = obgTipoApuracao;
	}
	public Boolean getObgPrecoCobranca() {
		return obgPrecoCobranca;
	}
	public void setObgPrecoCobranca(Boolean obgPrecoCobranca) {
		this.obgPrecoCobranca = obgPrecoCobranca;
	}
	public Boolean getObgApuracaoParadaProgramada() {
		return obgApuracaoParadaProgramada;
	}
	public void setObgApuracaoParadaProgramada(Boolean obgApuracaoParadaProgramada) {
		this.obgApuracaoParadaProgramada = obgApuracaoParadaProgramada;
	}
	public Boolean getObgApuracaoParadaNaoProgramada() {
		return obgApuracaoParadaNaoProgramada;
	}
	public void setObgApuracaoParadaNaoProgramada(Boolean obgApuracaoParadaNaoProgramada) {
		this.obgApuracaoParadaNaoProgramada = obgApuracaoParadaNaoProgramada;
	}
	public Boolean getObgApuracaoCasoFortuito() {
		return obgApuracaoCasoFortuito;
	}
	public void setObgApuracaoCasoFortuito(Boolean obgApuracaoCasoFortuito) {
		this.obgApuracaoCasoFortuito = obgApuracaoCasoFortuito;
	}
	public Boolean getObgApuracaoFalhaFornecimento() {
		return obgApuracaoFalhaFornecimento;
	}
	public void setObgApuracaoFalhaFornecimento(Boolean obgApuracaoFalhaFornecimento) {
		this.obgApuracaoFalhaFornecimento = obgApuracaoFalhaFornecimento;
	}
	public Boolean getFinalizacaoEncerramento() {
		return finalizacaoEncerramento;
	}
	public void setFinalizacaoEncerramento(Boolean finalizacaoEncerramento) {
		this.finalizacaoEncerramento = finalizacaoEncerramento;
	}
	public Boolean getChamadoTela() {
		return chamadoTela;
	}
	public void setChamadoTela(Boolean chamadoTela) {
		this.chamadoTela = chamadoTela;
	}
	public Boolean getCobrarIndenizacao() {
		return cobrarIndenizacao;
	}
	public void setCobrarIndenizacao(Boolean cobrarIndenizacao) {
		this.cobrarIndenizacao = cobrarIndenizacao;
	}
	public Boolean getFinalizacaoContrato() {
		return finalizacaoContrato;
	}
	public void setFinalizacaoContrato(Boolean finalizacaoContrato) {
		this.finalizacaoContrato = finalizacaoContrato;
	}
	public Integer getVersao() {
		return versao;
	}
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	
	
	
	public Integer getAnoContrato() {
		return anoContrato;
	}
	public void setAnoContrato(Integer anoContrato) {
		this.anoContrato = anoContrato;
	}
	public Integer getIndexLista() {
		return indexLista;
	}
	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}
	public Integer[] getIndicePeriodicidadeTakeOrPay() {
		return indicePeriodicidadeTakeOrPay;
	}
	public void setIndicePeriodicidadeTakeOrPay(Integer[] indicePeriodicidadeTakeOrPay) {
		this.indicePeriodicidadeTakeOrPay = indicePeriodicidadeTakeOrPay;
	}
	public Integer[] getIndicePeriodicidadeShipOrPay() {
		return indicePeriodicidadeShipOrPay;
	}
	public void setIndicePeriodicidadeShipOrPay(Integer[] indicePeriodicidadeShipOrPay) {
		this.indicePeriodicidadeShipOrPay = indicePeriodicidadeShipOrPay;
	}
	public Integer[] getIndicePenalidadeRetiradaMaiorMenor() {
		return indicePenalidadeRetiradaMaiorMenor;
	}
	public void setIndicePenalidadeRetiradaMaiorMenor(Integer[] indicePenalidadeRetiradaMaiorMenor) {
		this.indicePenalidadeRetiradaMaiorMenor = indicePenalidadeRetiradaMaiorMenor;
	}
	public Integer[] getIndicePenalidadeRetiradaMaiorMenorModalidade() {
		return indicePenalidadeRetiradaMaiorMenorModalidade;
	}
	public void setIndicePenalidadeRetiradaMaiorMenorModalidade(Integer[] indicePenalidadeRetiradaMaiorMenorModalidade) {
		this.indicePenalidadeRetiradaMaiorMenorModalidade = indicePenalidadeRetiradaMaiorMenorModalidade;
	}
	
	
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Long getIdModeloContrato() {
		return idModeloContrato;
	}
	public void setIdModeloContrato(Long idModeloContrato) {
		this.idModeloContrato = idModeloContrato;
	}
	public Long getIdPontoConsumoEncerrar() {
		return idPontoConsumoEncerrar;
	}
	public void setIdPontoConsumoEncerrar(Long idPontoConsumoEncerrar) {
		this.idPontoConsumoEncerrar = idPontoConsumoEncerrar;
	}
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public Long getIdPontoConsumoLista() {
		return idPontoConsumoLista;
	}
	public void setIdPontoConsumoLista(Long idPontoConsumoLista) {
		this.idPontoConsumoLista = idPontoConsumoLista;
	}
	public Long getChavePrimariaContratoMigrar() {
		return chavePrimariaContratoMigrar;
	}
	public void setChavePrimariaContratoMigrar(Long chavePrimariaContratoMigrar) {
		this.chavePrimariaContratoMigrar = chavePrimariaContratoMigrar;
	}
	public Long getIdTipoContrato() {
		return idTipoContrato;
	}
	public void setIdTipoContrato(Long idTipoContrato) {
		this.idTipoContrato = idTipoContrato;
	}
	public Long getChavePrimariaPrincipal() {
		return chavePrimariaPrincipal;
	}
	public void setChavePrimariaPrincipal(Long chavePrimariaPrincipal) {
		this.chavePrimariaPrincipal = chavePrimariaPrincipal;
	}
	public Long getIdPeriodicidadePenalidadeC() {
		return idPeriodicidadePenalidadeC;
	}
	public void setIdPeriodicidadePenalidadeC(Long idPeriodicidadePenalidadeC) {
		this.idPeriodicidadePenalidadeC = idPeriodicidadePenalidadeC;
	}
	public Long getIdPenalidadeC() {
		return idPenalidadeC;
	}
	public void setIdPenalidadeC(Long idPenalidadeC) {
		this.idPenalidadeC = idPenalidadeC;
	}
	public Long getChavePrimariaPai() {
		return chavePrimariaPai;
	}
	public void setChavePrimariaPai(Long chavePrimariaPai) {
		this.chavePrimariaPai = chavePrimariaPai;
	}
	public Long getIdItemFaturamento() {
		return idItemFaturamento;
	}
	public void setIdItemFaturamento(Long idItemFaturamento) {
		this.idItemFaturamento = idItemFaturamento;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public Long getIdModalidadeContrato() {
		return idModalidadeContrato;
	}
	public void setIdModalidadeContrato(Long idModalidadeContrato) {
		this.idModalidadeContrato = idModalidadeContrato;
	}
	public Long getIdPeriodicidadePenalidade() {
		return idPeriodicidadePenalidade;
	}
	public void setIdPeriodicidadePenalidade(Long idPeriodicidadePenalidade) {
		this.idPeriodicidadePenalidade = idPeriodicidadePenalidade;
	}
	public Long getIdPenalidade() {
		return idPenalidade;
	}
	public void setIdPenalidade(Long idPenalidade) {
		this.idPenalidade = idPenalidade;
	}
	public Long getChavePrimariaAnexo() {
		return chavePrimariaAnexo;
	}
	public void setChavePrimariaAnexo(Long chavePrimariaAnexo) {
		this.chavePrimariaAnexo = chavePrimariaAnexo;
	}
	public Long getIdPontoConsumoReferenciaCopia() {
		return idPontoConsumoReferenciaCopia;
	}
	public void setIdPontoConsumoReferenciaCopia(Long idPontoConsumoReferenciaCopia) {
		this.idPontoConsumoReferenciaCopia = idPontoConsumoReferenciaCopia;
	}
	public Long[] getChavesPrimarias() {
		return chavesPrimarias;
	}
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		this.chavesPrimarias = chavesPrimarias;
	}
	public Long[] getChavesPrimariasPontoConsumo() {
		return chavesPrimariasPontoConsumo;
	}
	public void setChavesPrimariasPontoConsumo(Long[] chavesPrimariasPontoConsumo) {
		this.chavesPrimariasPontoConsumo = chavesPrimariasPontoConsumo;
	}
	public Long[] getChavesPrimariasPontoConsumoVO() {
		return chavesPrimariasPontoConsumoVO;
	}
	public void setChavesPrimariasPontoConsumoVO(Long[] chavesPrimariasPontoConsumoVO) {
		this.chavesPrimariasPontoConsumoVO = chavesPrimariasPontoConsumoVO;
	}
	public Long[] getChavePontoTransferirSaldo() {
		return chavePontoTransferirSaldo;
	}
	public void setChavePontoTransferirSaldo(Long[] chavePontoTransferirSaldo) {
		this.chavePontoTransferirSaldo = chavePontoTransferirSaldo;
	}
	public Long[] getIdsPontosConsumo() {
		return idsPontosConsumo;
	}
	public void setIdsPontosConsumo(Long[] idsPontosConsumo) {
		this.idsPontosConsumo = idsPontosConsumo;
	}
	public Long[] getItensFaturamento() {
		return itensFaturamento;
	}
	public void setItensFaturamento(Long[] itensFaturamento) {
		this.itensFaturamento = itensFaturamento;
	}
	public Long[] getListaDiasDisponiveis() {
		return listaDiasDisponiveis;
	}
	public void setListaDiasDisponiveis(Long[] listaDiasDisponiveis) {
		this.listaDiasDisponiveis = listaDiasDisponiveis;
	}
	public Long[] getListaDiasSelecionados() {
		return listaDiasSelecionados;
	}
	public void setListaDiasSelecionados(Long[] listaDiasSelecionados) {
		this.listaDiasSelecionados = listaDiasSelecionados;
	}
	public Long[] getIdsLocalAmostragemAssociados() {
		return idsLocalAmostragemAssociados;
	}
	public void setIdsLocalAmostragemAssociados(Long[] idsLocalAmostragemAssociados) {
		this.idsLocalAmostragemAssociados = idsLocalAmostragemAssociados;
	}
	public Long[] getIdsIntervaloAmostragemAssociados() {
		return idsIntervaloAmostragemAssociados;
	}
	public void setIdsIntervaloAmostragemAssociados(Long[] idsIntervaloAmostragemAssociados) {
		this.idsIntervaloAmostragemAssociados = idsIntervaloAmostragemAssociados;
	}
	public Long[] getIdsModalidadeContrato() {
		return idsModalidadeContrato;
	}
	public void setIdsModalidadeContrato(Long[] idsModalidadeContrato) {
		this.idsModalidadeContrato = idsModalidadeContrato;
	}
	public Long[] getArrayIdPontoConsumo() {
		return arrayIdPontoConsumo;
	}
	public void setArrayIdPontoConsumo(Long[] arrayIdPontoConsumo) {
		this.arrayIdPontoConsumo = arrayIdPontoConsumo;
	}
	public MultipartFile[] getAnexo() {
		return anexo;
	}
	public void setAnexo(MultipartFile[] anexo) {
		this.anexo = anexo;
	}
	
	
	
	public String getPontoConsumo() {
		return pontoConsumo;
	}
	public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}
	public String getUnidadePressaoFornec() {
		return unidadePressaoFornec;
	}
	public void setUnidadePressaoFornec(String unidadePressaoFornec) {
		this.unidadePressaoFornec = unidadePressaoFornec;
	}
	public Boolean getValorFixoSistemaAmortizacao() {
		return valorFixoSistemaAmortizacao;
	}
	public void setValorFixoSistemaAmortizacao(Boolean valorFixoSistemaAmortizacao) {
		this.valorFixoSistemaAmortizacao = valorFixoSistemaAmortizacao;
	}
	public Boolean getValorFixoValorParticipacaoCliente() {
		return valorFixoValorParticipacaoCliente;
	}
	public void setValorFixoValorParticipacaoCliente(Boolean valorFixoValorParticipacaoCliente) {
		this.valorFixoValorParticipacaoCliente = valorFixoValorParticipacaoCliente;
	}
	public Boolean getSelPontoConsumo() {
		return selPontoConsumo;
	}
	public void setSelPontoConsumo(Boolean selPontoConsumo) {
		this.selPontoConsumo = selPontoConsumo;
	}
	public Boolean getObgPontoConsumo() {
		return obgPontoConsumo;
	}
	public void setObgPontoConsumo(Boolean obgPontoConsumo) {
		this.obgPontoConsumo = obgPontoConsumo;
	}
	public Boolean getValorFixoPontoConsumo() {
		return valorFixoPontoConsumo;
	}
	public void setValorFixoPontoConsumo(Boolean valorFixoPontoConsumo) {
		this.valorFixoPontoConsumo = valorFixoPontoConsumo;
	}
	public Boolean getValorFixoDataInicioRelacao() {
		return valorFixoDataInicioRelacao;
	}
	public void setValorFixoDataInicioRelacao(Boolean valorFixoDataInicioRelacao) {
		this.valorFixoDataInicioRelacao = valorFixoDataInicioRelacao;
	}
	public Boolean getValorFixoDataAditivo() {
		return valorFixoDataAditivo;
	}
	public void setValorFixoDataAditivo(Boolean valorFixoDataAditivo) {
		this.valorFixoDataAditivo = valorFixoDataAditivo;
	}
	public Boolean getValorFixoNumeroAditivo() {
		return valorFixoNumeroAditivo;
	}
	public void setValorFixoNumeroAditivo(Boolean valorFixoNumeroAditivo) {
		this.valorFixoNumeroAditivo = valorFixoNumeroAditivo;
	}
	public Boolean getSelUnidadeFornecMinimoMensal() {
		return selUnidadeFornecMinimoMensal;
	}
	public void setSelUnidadeFornecMinimoMensal(Boolean selUnidadeFornecMinimoMensal) {
		this.selUnidadeFornecMinimoMensal = selUnidadeFornecMinimoMensal;
	}
	public Boolean getObgUnidadeFornecMinimoMensal() {
		return obgUnidadeFornecMinimoMensal;
	}
	public void setObgUnidadeFornecMinimoMensal(Boolean obgUnidadeFornecMinimoMensal) {
		this.obgUnidadeFornecMinimoMensal = obgUnidadeFornecMinimoMensal;
	}
	public Boolean getValorFixoUnidadeFornecMinimoMensal() {
		return valorFixoUnidadeFornecMinimoMensal;
	}
	public void setValorFixoUnidadeFornecMinimoMensal(Boolean valorFixoUnidadeFornecMinimoMensal) {
		this.valorFixoUnidadeFornecMinimoMensal = valorFixoUnidadeFornecMinimoMensal;
	}
	public Boolean getValorFixoClienteResponsavel() {
		return valorFixoClienteResponsavel;
	}
	public void setValorFixoClienteResponsavel(Boolean valorFixoClienteResponsavel) {
		this.valorFixoClienteResponsavel = valorFixoClienteResponsavel;
	}
	public Boolean getValorFixoEconomiaEstimadaGNMes() {
		return valorFixoEconomiaEstimadaGNMes;
	}
	public void setValorFixoEconomiaEstimadaGNMes(Boolean valorFixoEconomiaEstimadaGNMes) {
		this.valorFixoEconomiaEstimadaGNMes = valorFixoEconomiaEstimadaGNMes;
	}
	public Boolean getValorFixoTamReducaoRecuperacaoPCS() {
		return valorFixoTamReducaoRecuperacaoPCS;
	}
	public void setValorFixoTamReducaoRecuperacaoPCS(Boolean valorFixoTamReducaoRecuperacaoPCS) {
		this.valorFixoTamReducaoRecuperacaoPCS = valorFixoTamReducaoRecuperacaoPCS;
	}
	public Boolean getValorFixoDataAssinatura() {
		return valorFixoDataAssinatura;
	}
	public void setValorFixoDataAssinatura(Boolean valorFixoDataAssinatura) {
		this.valorFixoDataAssinatura = valorFixoDataAssinatura;
	}
	public Boolean getSelUnidadeFornecMaximoDiario() {
		return selUnidadeFornecMaximoDiario;
	}
	public void setSelUnidadeFornecMaximoDiario(Boolean selUnidadeFornecMaximoDiario) {
		this.selUnidadeFornecMaximoDiario = selUnidadeFornecMaximoDiario;
	}
	public Boolean getObgUnidadeFornecMaximoDiario() {
		return obgUnidadeFornecMaximoDiario;
	}
	public void setObgUnidadeFornecMaximoDiario(Boolean obgUnidadeFornecMaximoDiario) {
		this.obgUnidadeFornecMaximoDiario = obgUnidadeFornecMaximoDiario;
	}
	public Boolean getValorFixoUnidadeFornecMaximoDiario() {
		return valorFixoUnidadeFornecMaximoDiario;
	}
	public void setValorFixoUnidadeFornecMaximoDiario(Boolean valorFixoUnidadeFornecMaximoDiario) {
		this.valorFixoUnidadeFornecMaximoDiario = valorFixoUnidadeFornecMaximoDiario;
	}
	public Boolean getValorFixoClienteAssinatura() {
		return valorFixoClienteAssinatura;
	}
	public void setValorFixoClienteAssinatura(Boolean valorFixoClienteAssinatura) {
		this.valorFixoClienteAssinatura = valorFixoClienteAssinatura;
	}
	public Boolean getSelUnidadeVazaoInstanMaxima() {
		return selUnidadeVazaoInstanMaxima;
	}
	public void setSelUnidadeVazaoInstanMaxima(Boolean selUnidadeVazaoInstanMaxima) {
		this.selUnidadeVazaoInstanMaxima = selUnidadeVazaoInstanMaxima;
	}
	public Boolean getObgUnidadeVazaoInstanMaxima() {
		return obgUnidadeVazaoInstanMaxima;
	}
	public void setObgUnidadeVazaoInstanMaxima(Boolean obgUnidadeVazaoInstanMaxima) {
		this.obgUnidadeVazaoInstanMaxima = obgUnidadeVazaoInstanMaxima;
	}
	public Boolean getValorFixoUnidadeVazaoInstanMaxima() {
		return valorFixoUnidadeVazaoInstanMaxima;
	}
	public void setValorFixoUnidadeVazaoInstanMaxima(Boolean valorFixoUnidadeVazaoInstanMaxima) {
		this.valorFixoUnidadeVazaoInstanMaxima = valorFixoUnidadeVazaoInstanMaxima;
	}
	public Boolean getValorFixoValorInvestimento() {
		return valorFixoValorInvestimento;
	}
	public void setValorFixoValorInvestimento(Boolean valorFixoValorInvestimento) {
		this.valorFixoValorInvestimento = valorFixoValorInvestimento;
	}
	public Boolean getValorFixoEconomiaEstimadaGNAno() {
		return valorFixoEconomiaEstimadaGNAno;
	}
	public void setValorFixoEconomiaEstimadaGNAno(Boolean valorFixoEconomiaEstimadaGNAno) {
		this.valorFixoEconomiaEstimadaGNAno = valorFixoEconomiaEstimadaGNAno;
	}
	public Boolean getValorFixoLocalAmostragemPCS() {
		return valorFixoLocalAmostragemPCS;
	}
	public void setValorFixoLocalAmostragemPCS(Boolean valorFixoLocalAmostragemPCS) {
		this.valorFixoLocalAmostragemPCS = valorFixoLocalAmostragemPCS;
	}
	public Boolean getValorFixoNumeroProposta() {
		return valorFixoNumeroProposta;
	}
	public void setValorFixoNumeroProposta(Boolean valorFixoNumeroProposta) {
		this.valorFixoNumeroProposta = valorFixoNumeroProposta;
	}
	public Boolean getValorFixoDataInvestimento() {
		return valorFixoDataInvestimento;
	}
	public void setValorFixoDataInvestimento(Boolean valorFixoDataInvestimento) {
		this.valorFixoDataInvestimento = valorFixoDataInvestimento;
	}
	public Boolean getValorFixoEndFisEntFatComplemento() {
		return valorFixoEndFisEntFatComplemento;
	}
	public void setValorFixoEndFisEntFatComplemento(Boolean valorFixoEndFisEntFatComplemento) {
		this.valorFixoEndFisEntFatComplemento = valorFixoEndFisEntFatComplemento;
	}
	public Boolean getValorFixoPercentualJurosFinanciamento() {
		return valorFixoPercentualJurosFinanciamento;
	}
	public void setValorFixoPercentualJurosFinanciamento(Boolean valorFixoPercentualJurosFinanciamento) {
		this.valorFixoPercentualJurosFinanciamento = valorFixoPercentualJurosFinanciamento;
	}
	public Boolean getValorFixoNumeroContrato() {
		return valorFixoNumeroContrato;
	}
	public void setValorFixoNumeroContrato(Boolean valorFixoNumeroContrato) {
		this.valorFixoNumeroContrato = valorFixoNumeroContrato;
	}
	public Boolean getValorFixoDescricaoAditivo() {
		return valorFixoDescricaoAditivo;
	}
	public void setValorFixoDescricaoAditivo(Boolean valorFixoDescricaoAditivo) {
		this.valorFixoDescricaoAditivo = valorFixoDescricaoAditivo;
	}
	public Boolean getSelUnidadePressaoFornec() {
		return selUnidadePressaoFornec;
	}
	public void setSelUnidadePressaoFornec(Boolean selUnidadePressaoFornec) {
		this.selUnidadePressaoFornec = selUnidadePressaoFornec;
	}
	public Boolean getObgUnidadePressaoFornec() {
		return obgUnidadePressaoFornec;
	}
	public void setObgUnidadePressaoFornec(Boolean obgUnidadePressaoFornec) {
		this.obgUnidadePressaoFornec = obgUnidadePressaoFornec;
	}
	public Boolean getValorFixoUnidadePressaoFornec() {
		return valorFixoUnidadePressaoFornec;
	}
	public void setValorFixoUnidadePressaoFornec(Boolean valorFixoUnidadePressaoFornec) {
		this.valorFixoUnidadePressaoFornec = valorFixoUnidadePressaoFornec;
	}
	public Boolean getValorFixoEndFisEntFatNumero() {
		return valorFixoEndFisEntFatNumero;
	}
	public void setValorFixoEndFisEntFatNumero(Boolean valorFixoEndFisEntFatNumero) {
		this.valorFixoEndFisEntFatNumero = valorFixoEndFisEntFatNumero;
	}
	public Boolean getValorFixoGastoEstimadoGNMes() {
		return valorFixoGastoEstimadoGNMes;
	}
	public void setValorFixoGastoEstimadoGNMes(Boolean valorFixoGastoEstimadoGNMes) {
		this.valorFixoGastoEstimadoGNMes = valorFixoGastoEstimadoGNMes;
	}
	public Boolean getValorFixoEnderecoPadrao() {
		return valorFixoEnderecoPadrao;
	}
	public void setValorFixoEnderecoPadrao(Boolean valorFixoEnderecoPadrao) {
		this.valorFixoEnderecoPadrao = valorFixoEnderecoPadrao;
	}
	public Boolean getSelPropostaAprovada() {
		return selPropostaAprovada;
	}
	public void setSelPropostaAprovada(Boolean selPropostaAprovada) {
		this.selPropostaAprovada = selPropostaAprovada;
	}
	public Boolean getObgPropostaAprovada() {
		return obgPropostaAprovada;
	}
	public void setObgPropostaAprovada(Boolean obgPropostaAprovada) {
		this.obgPropostaAprovada = obgPropostaAprovada;
	}
	public Boolean getValorFixoPropostaAprovada() {
		return valorFixoPropostaAprovada;
	}
	public void setValorFixoPropostaAprovada(Boolean valorFixoPropostaAprovada) {
		this.valorFixoPropostaAprovada = valorFixoPropostaAprovada;
	}
	public Boolean getValorFixoQtdParcelasFinanciamento() {
		return valorFixoQtdParcelasFinanciamento;
	}
	public void setValorFixoQtdParcelasFinanciamento(Boolean valorFixoQtdParcelasFinanciamento) {
		this.valorFixoQtdParcelasFinanciamento = valorFixoQtdParcelasFinanciamento;
	}
	public Boolean getValorFixoEndFisEntFatCEP() {
		return valorFixoEndFisEntFatCEP;
	}
	public void setValorFixoEndFisEntFatCEP(Boolean valorFixoEndFisEntFatCEP) {
		this.valorFixoEndFisEntFatCEP = valorFixoEndFisEntFatCEP;
	}
	public Boolean getValorFixoExigeAprovacao() {
		return valorFixoExigeAprovacao;
	}
	public void setValorFixoExigeAprovacao(Boolean valorFixoExigeAprovacao) {
		this.valorFixoExigeAprovacao = valorFixoExigeAprovacao;
	}
	public Boolean getValorFixoDescontoEfetivoEconomia() {
		return valorFixoDescontoEfetivoEconomia;
	}
	public void setValorFixoDescontoEfetivoEconomia(Boolean valorFixoDescontoEfetivoEconomia) {
		this.valorFixoDescontoEfetivoEconomia = valorFixoDescontoEfetivoEconomia;
	}
	public Boolean getValorFixoDataFimRelacao() {
		return valorFixoDataFimRelacao;
	}
	public void setValorFixoDataFimRelacao(Boolean valorFixoDataFimRelacao) {
		this.valorFixoDataFimRelacao = valorFixoDataFimRelacao;
	}
	public Boolean getValorFixoIntervaloRecuperacaoPCS() {
		return valorFixoIntervaloRecuperacaoPCS;
	}
	public void setValorFixoIntervaloRecuperacaoPCS(Boolean valorFixoIntervaloRecuperacaoPCS) {
		this.valorFixoIntervaloRecuperacaoPCS = valorFixoIntervaloRecuperacaoPCS;
	}
	
	public Boolean getValorFixoApuracaoCasoFortuito() {
		return valorFixoApuracaoCasoFortuito;
	}
	public void setValorFixoApuracaoCasoFortuito(Boolean valorFixoApuracaoCasoFortuito) {
		this.valorFixoApuracaoCasoFortuito = valorFixoApuracaoCasoFortuito;
	}
	public Boolean getValorFixoDataFimVigencia() {
		return valorFixoDataFimVigencia;
	}
	public void setValorFixoDataFimVigencia(Boolean valorFixoDataFimVigencia) {
		this.valorFixoDataFimVigencia = valorFixoDataFimVigencia;
	}
	public Boolean getValorFixoDataInicioRetiradaQPNR() {
		return valorFixoDataInicioRetiradaQPNR;
	}
	public void setValorFixoDataInicioRetiradaQPNR(Boolean valorFixoDataInicioRetiradaQPNR) {
		this.valorFixoDataInicioRetiradaQPNR = valorFixoDataInicioRetiradaQPNR;
	}
	public Boolean getValorFixoPercMinDuranteRetiradaQPNR() {
		return valorFixoPercMinDuranteRetiradaQPNR;
	}
	public void setValorFixoPercMinDuranteRetiradaQPNR(Boolean valorFixoPercMinDuranteRetiradaQPNR) {
		this.valorFixoPercMinDuranteRetiradaQPNR = valorFixoPercMinDuranteRetiradaQPNR;
	}
	public Boolean getValorFixoTempoValidadeRetiradaQPNR() {
		return valorFixoTempoValidadeRetiradaQPNR;
	}
	public void setValorFixoTempoValidadeRetiradaQPNR(Boolean valorFixoTempoValidadeRetiradaQPNR) {
		this.valorFixoTempoValidadeRetiradaQPNR = valorFixoTempoValidadeRetiradaQPNR;
	}
	public Boolean getValorFixoRecuperavel() {
		return valorFixoRecuperavel;
	}
	public void setValorFixoRecuperavel(Boolean valorFixoRecuperavel) {
		this.valorFixoRecuperavel = valorFixoRecuperavel;
	}
	public Boolean getValorFixoTipoApuracao() {
		return valorFixoTipoApuracao;
	}
	public void setValorFixoTipoApuracao(Boolean valorFixoTipoApuracao) {
		this.valorFixoTipoApuracao = valorFixoTipoApuracao;
	}
	public Boolean getValorFixoDataFinalGarantiaFinanceira() {
		return valorFixoDataFinalGarantiaFinanceira;
	}
	public void setValorFixoDataFinalGarantiaFinanceira(Boolean valorFixoDataFinalGarantiaFinanceira) {
		this.valorFixoDataFinalGarantiaFinanceira = valorFixoDataFinalGarantiaFinanceira;
	}
	public Boolean getValorFixoEndFisEntFatEmail() {
		return valorFixoEndFisEntFatEmail;
	}
	public void setValorFixoEndFisEntFatEmail(Boolean valorFixoEndFisEntFatEmail) {
		this.valorFixoEndFisEntFatEmail = valorFixoEndFisEntFatEmail;
	}
	public Boolean getValorFixoPercentualNaoRecuperavel() {
		return valorFixoPercentualNaoRecuperavel;
	}
	public void setValorFixoPercentualNaoRecuperavel(Boolean valorFixoPercentualNaoRecuperavel) {
		this.valorFixoPercentualNaoRecuperavel = valorFixoPercentualNaoRecuperavel;
	}
	public Boolean getValorFixoConsideraCasoFortuito() {
		return valorFixoConsideraCasoFortuito;
	}
	public void setValorFixoConsideraCasoFortuito(Boolean valorFixoConsideraCasoFortuito) {
		this.valorFixoConsideraCasoFortuito = valorFixoConsideraCasoFortuito;
	}
	public Boolean getValorFixoTipoAgrupamentoContrato() {
		return valorFixoTipoAgrupamentoContrato;
	}
	public void setValorFixoTipoAgrupamentoContrato(Boolean valorFixoTipoAgrupamentoContrato) {
		this.valorFixoTipoAgrupamentoContrato = valorFixoTipoAgrupamentoContrato;
	}
	public Boolean getValorFixoMargemVariacaoTakeOrPay() {
		return valorFixoMargemVariacaoTakeOrPay;
	}
	public void setValorFixoMargemVariacaoTakeOrPay(Boolean valorFixoMargemVariacaoTakeOrPay) {
		this.valorFixoMargemVariacaoTakeOrPay = valorFixoMargemVariacaoTakeOrPay;
	}
	public Boolean getValorFixoReferenciaQFParadaProgramada() {
		return valorFixoReferenciaQFParadaProgramada;
	}
	public void setValorFixoReferenciaQFParadaProgramada(Boolean valorFixoReferenciaQFParadaProgramada) {
		this.valorFixoReferenciaQFParadaProgramada = valorFixoReferenciaQFParadaProgramada;
	}
	public Boolean getValorFixoConsideraParadaProgramada() {
		return valorFixoConsideraParadaProgramada;
	}
	public void setValorFixoConsideraParadaProgramada(Boolean valorFixoConsideraParadaProgramada) {
		this.valorFixoConsideraParadaProgramada = valorFixoConsideraParadaProgramada;
	}
	public Boolean getValorFixoApuracaoFalhaFornecimento() {
		return valorFixoApuracaoFalhaFornecimento;
	}
	public void setValorFixoApuracaoFalhaFornecimento(Boolean valorFixoApuracaoFalhaFornecimento) {
		this.valorFixoApuracaoFalhaFornecimento = valorFixoApuracaoFalhaFornecimento;
	}
	public Boolean getValorFixoPrazoRevizaoQDC() {
		return valorFixoPrazoRevizaoQDC;
	}
	public void setValorFixoPrazoRevizaoQDC(Boolean valorFixoPrazoRevizaoQDC) {
		this.valorFixoPrazoRevizaoQDC = valorFixoPrazoRevizaoQDC;
	}
	public Boolean getValorFixoDataVigenciaQDC() {
		return valorFixoDataVigenciaQDC;
	}
	public void setValorFixoDataVigenciaQDC(Boolean valorFixoDataVigenciaQDC) {
		this.valorFixoDataVigenciaQDC = valorFixoDataVigenciaQDC;
	}
	public Boolean getValorFixoConsideraFalhaFornecimento() {
		return valorFixoConsideraFalhaFornecimento;
	}
	public void setValorFixoConsideraFalhaFornecimento(Boolean valorFixoConsideraFalhaFornecimento) {
		this.valorFixoConsideraFalhaFornecimento = valorFixoConsideraFalhaFornecimento;
	}
	public Boolean getValorFixoPrecoCobranca() {
		return valorFixoPrecoCobranca;
	}
	public void setValorFixoPrecoCobranca(Boolean valorFixoPrecoCobranca) {
		this.valorFixoPrecoCobranca = valorFixoPrecoCobranca;
	}
	public Boolean getValorFixoQdc() {
		return valorFixoQdc;
	}
	public void setValorFixoQdc(Boolean valorFixoQdc) {
		this.valorFixoQdc = valorFixoQdc;
	}
	public Boolean getValorFixoDataInicioGarantiaFinanceira() {
		return valorFixoDataInicioGarantiaFinanceira;
	}
	public void setValorFixoDataInicioGarantiaFinanceira(Boolean valorFixoDataInicioGarantiaFinanceira) {
		this.valorFixoDataInicioGarantiaFinanceira = valorFixoDataInicioGarantiaFinanceira;
	}
	public Boolean getValorFixoPeriodicidadeTakeOrPay() {
		return valorFixoPeriodicidadeTakeOrPay;
	}
	public void setValorFixoPeriodicidadeTakeOrPay(Boolean valorFixoPeriodicidadeTakeOrPay) {
		this.valorFixoPeriodicidadeTakeOrPay = valorFixoPeriodicidadeTakeOrPay;
	}
	public Boolean getSelUnidadeFornecMinimoAnual() {
		return selUnidadeFornecMinimoAnual;
	}
	public void setSelUnidadeFornecMinimoAnual(Boolean selUnidadeFornecMinimoAnual) {
		this.selUnidadeFornecMinimoAnual = selUnidadeFornecMinimoAnual;
	}
	public Boolean getObgUnidadeFornecMinimoAnual() {
		return obgUnidadeFornecMinimoAnual;
	}
	public void setObgUnidadeFornecMinimoAnual(Boolean obgUnidadeFornecMinimoAnual) {
		this.obgUnidadeFornecMinimoAnual = obgUnidadeFornecMinimoAnual;
	}
	public Boolean getValorFixoUnidadeFornecMinimoAnual() {
		return valorFixoUnidadeFornecMinimoAnual;
	}
	public void setValorFixoUnidadeFornecMinimoAnual(Boolean valorFixoUnidadeFornecMinimoAnual) {
		this.valorFixoUnidadeFornecMinimoAnual = valorFixoUnidadeFornecMinimoAnual;
	}
	public Boolean getValorFixoModalidade() {
		return valorFixoModalidade;
	}
	public void setValorFixoModalidade(Boolean valorFixoModalidade) {
		this.valorFixoModalidade = valorFixoModalidade;
	}
	public Boolean getValorFixoDataFimRetiradaQPNR() {
		return valorFixoDataFimRetiradaQPNR;
	}
	public void setValorFixoDataFimRetiradaQPNR(Boolean valorFixoDataFimRetiradaQPNR) {
		this.valorFixoDataFimRetiradaQPNR = valorFixoDataFimRetiradaQPNR;
	}
	public Boolean getValorFixoRecuperacaoAutoTakeOrPay() {
		return valorFixoRecuperacaoAutoTakeOrPay;
	}
	public void setValorFixoRecuperacaoAutoTakeOrPay(Boolean valorFixoRecuperacaoAutoTakeOrPay) {
		this.valorFixoRecuperacaoAutoTakeOrPay = valorFixoRecuperacaoAutoTakeOrPay;
	}
	public Boolean getValorFixoQDCContrato() {
		return valorFixoQDCContrato;
	}
	public void setValorFixoQDCContrato(Boolean valorFixoQDCContrato) {
		this.valorFixoQDCContrato = valorFixoQDCContrato;
	}
	public Boolean getValorFixoPercDuranteRetiradaQPNR() {
		return valorFixoPercDuranteRetiradaQPNR;
	}
	public void setValorFixoPercDuranteRetiradaQPNR(Boolean valorFixoPercDuranteRetiradaQPNR) {
		this.valorFixoPercDuranteRetiradaQPNR = valorFixoPercDuranteRetiradaQPNR;
	}
	public Boolean getValorFixoDataInicioVigencia() {
		return valorFixoDataInicioVigencia;
	}
	public void setValorFixoDataInicioVigencia(Boolean valorFixoDataInicioVigencia) {
		this.valorFixoDataInicioVigencia = valorFixoDataInicioVigencia;
	}
	public Boolean getValorFixoDataVencObrigacoesContratuais() {
		return valorFixoDataVencObrigacoesContratuais;
	}
	public void setValorFixoDataVencObrigacoesContratuais(Boolean valorFixoDataVencObrigacoesContratuais) {
		this.valorFixoDataVencObrigacoesContratuais = valorFixoDataVencObrigacoesContratuais;
	}
	public Boolean getValorFixoApuracaoParadaProgramada() {
		return valorFixoApuracaoParadaProgramada;
	}
	public void setValorFixoApuracaoParadaProgramada(Boolean valorFixoApuracaoParadaProgramada) {
		this.valorFixoApuracaoParadaProgramada = valorFixoApuracaoParadaProgramada;
	}
	public Boolean getValorFixoPercFimRetiradaQPNR() {
		return valorFixoPercFimRetiradaQPNR;
	}
	public void setValorFixoPercFimRetiradaQPNR(Boolean valorFixoPercFimRetiradaQPNR) {
		this.valorFixoPercFimRetiradaQPNR = valorFixoPercFimRetiradaQPNR;
	}
	public Boolean getValorFixoConsumoReferenciaTakeOrPay() {
		return valorFixoConsumoReferenciaTakeOrPay;
	}
	public void setValorFixoConsumoReferenciaTakeOrPay(Boolean valorFixoConsumoReferenciaTakeOrPay) {
		this.valorFixoConsumoReferenciaTakeOrPay = valorFixoConsumoReferenciaTakeOrPay;
	}
	
	public Boolean getSelUnidadePressaoMaximaFornec() {
		return selUnidadePressaoMaximaFornec;
	}
	public void setSelUnidadePressaoMaximaFornec(Boolean selUnidadePressaoMaximaFornec) {
		this.selUnidadePressaoMaximaFornec = selUnidadePressaoMaximaFornec;
	}
	public Boolean getObgUnidadePressaoMaximaFornec() {
		return obgUnidadePressaoMaximaFornec;
	}
	public void setObgUnidadePressaoMaximaFornec(Boolean obgUnidadePressaoMaximaFornec) {
		this.obgUnidadePressaoMaximaFornec = obgUnidadePressaoMaximaFornec;
	}
	public Boolean getValorFixoUnidadePressaoMaximaFornec() {
		return valorFixoUnidadePressaoMaximaFornec;
	}
	public void setValorFixoUnidadePressaoMaximaFornec(Boolean valorFixoUnidadePressaoMaximaFornec) {
		this.valorFixoUnidadePressaoMaximaFornec = valorFixoUnidadePressaoMaximaFornec;
	}
	public Boolean getSelUnidadePressaoMinimaFornec() {
		return selUnidadePressaoMinimaFornec;
	}
	public void setSelUnidadePressaoMinimaFornec(Boolean selUnidadePressaoMinimaFornec) {
		this.selUnidadePressaoMinimaFornec = selUnidadePressaoMinimaFornec;
	}
	public Boolean getObgUnidadePressaoMinimaFornec() {
		return obgUnidadePressaoMinimaFornec;
	}
	public void setObgUnidadePressaoMinimaFornec(Boolean obgUnidadePressaoMinimaFornec) {
		this.obgUnidadePressaoMinimaFornec = obgUnidadePressaoMinimaFornec;
	}
	public Boolean getValorFixoUnidadePressaoMinimaFornec() {
		return valorFixoUnidadePressaoMinimaFornec;
	}
	public void setValorFixoUnidadePressaoMinimaFornec(Boolean valorFixoUnidadePressaoMinimaFornec) {
		this.valorFixoUnidadePressaoMinimaFornec = valorFixoUnidadePressaoMinimaFornec;
	}
	public Boolean getSelUnidadeVazaoInstanMinima() {
		return selUnidadeVazaoInstanMinima;
	}
	public void setSelUnidadeVazaoInstanMinima(Boolean selUnidadeVazaoInstanMinima) {
		this.selUnidadeVazaoInstanMinima = selUnidadeVazaoInstanMinima;
	}
	public Boolean getObgUnidadeVazaoInstanMinima() {
		return obgUnidadeVazaoInstanMinima;
	}
	public void setObgUnidadeVazaoInstanMinima(Boolean obgUnidadeVazaoInstanMinima) {
		this.obgUnidadeVazaoInstanMinima = obgUnidadeVazaoInstanMinima;
	}
	public Boolean getValorFixoUnidadeVazaoInstanMinima() {
		return valorFixoUnidadeVazaoInstanMinima;
	}
	public void setValorFixoUnidadeVazaoInstanMinima(Boolean valorFixoUnidadeVazaoInstanMinima) {
		this.valorFixoUnidadeVazaoInstanMinima = valorFixoUnidadeVazaoInstanMinima;
	}
	
	public Boolean getValorFixoFaixaPenalidadeSobreDem() {
		return valorFixoFaixaPenalidadeSobreDem;
	}
	public void setValorFixoFaixaPenalidadeSobreDem(Boolean valorFixoFaixaPenalidadeSobreDem) {
		this.valorFixoFaixaPenalidadeSobreDem = valorFixoFaixaPenalidadeSobreDem;
	}
	public Boolean getValorFixoBaseApuracaoRetMenor() {
		return valorFixoBaseApuracaoRetMenor;
	}
	public void setValorFixoBaseApuracaoRetMenor(Boolean valorFixoBaseApuracaoRetMenor) {
		this.valorFixoBaseApuracaoRetMenor = valorFixoBaseApuracaoRetMenor;
	}
	public Boolean getValorFixoPercentualReferenciaSobDem() {
		return valorFixoPercentualReferenciaSobDem;
	}
	public void setValorFixoPercentualReferenciaSobDem(Boolean valorFixoPercentualReferenciaSobDem) {
		this.valorFixoPercentualReferenciaSobDem = valorFixoPercentualReferenciaSobDem;
	}
	public Boolean getValorFixoConsumoReferenciaSobreDem() {
		return valorFixoConsumoReferenciaSobreDem;
	}
	public void setValorFixoConsumoReferenciaSobreDem(Boolean valorFixoConsumoReferenciaSobreDem) {
		this.valorFixoConsumoReferenciaSobreDem = valorFixoConsumoReferenciaSobreDem;
	}
	public Boolean getValorFixoConsumoReferenciaSobDem() {
		return valorFixoConsumoReferenciaSobDem;
	}
	public void setValorFixoConsumoReferenciaSobDem(Boolean valorFixoConsumoReferenciaSobDem) {
		this.valorFixoConsumoReferenciaSobDem = valorFixoConsumoReferenciaSobDem;
	}
	public Boolean getValorFixoBaseApuracaoRetMaior() {
		return valorFixoBaseApuracaoRetMaior;
	}
	public void setValorFixoBaseApuracaoRetMaior(Boolean valorFixoBaseApuracaoRetMaior) {
		this.valorFixoBaseApuracaoRetMaior = valorFixoBaseApuracaoRetMaior;
	}
	public Boolean getValorFixoFaixaPenalidadeSobDem() {
		return valorFixoFaixaPenalidadeSobDem;
	}
	public void setValorFixoFaixaPenalidadeSobDem(Boolean valorFixoFaixaPenalidadeSobDem) {
		this.valorFixoFaixaPenalidadeSobDem = valorFixoFaixaPenalidadeSobDem;
	}
	public Boolean getValorFixoPercentualReferenciaSobreDem() {
		return valorFixoPercentualReferenciaSobreDem;
	}
	public void setValorFixoPercentualReferenciaSobreDem(Boolean valorFixoPercentualReferenciaSobreDem) {
		this.valorFixoPercentualReferenciaSobreDem = valorFixoPercentualReferenciaSobreDem;
	}
	
	public Boolean getSelUnidadePressaoLimiteFornec() {
		return selUnidadePressaoLimiteFornec;
	}
	public void setSelUnidadePressaoLimiteFornec(Boolean selUnidadePressaoLimiteFornec) {
		this.selUnidadePressaoLimiteFornec = selUnidadePressaoLimiteFornec;
	}
	public Boolean getObgUnidadePressaoLimiteFornec() {
		return obgUnidadePressaoLimiteFornec;
	}
	public void setObgUnidadePressaoLimiteFornec(Boolean obgUnidadePressaoLimiteFornec) {
		this.obgUnidadePressaoLimiteFornec = obgUnidadePressaoLimiteFornec;
	}
	public Boolean getValorFixoUnidadePressaoLimiteFornec() {
		return valorFixoUnidadePressaoLimiteFornec;
	}
	public void setValorFixoUnidadePressaoLimiteFornec(Boolean valorFixoUnidadePressaoLimiteFornec) {
		this.valorFixoUnidadePressaoLimiteFornec = valorFixoUnidadePressaoLimiteFornec;
	}
	public Boolean getValorFixoNumeroAnteriorContrato() {
		return valorFixoNumeroAnteriorContrato;
	}
	public void setValorFixoNumeroAnteriorContrato(Boolean valorFixoNumeroAnteriorContrato) {
		this.valorFixoNumeroAnteriorContrato = valorFixoNumeroAnteriorContrato;
	}
	public Boolean getValorFixoFaxNumero() {
		return valorFixoFaxNumero;
	}
	public void setValorFixoFaxNumero(Boolean valorFixoFaxNumero) {
		this.valorFixoFaxNumero = valorFixoFaxNumero;
	}
	public Boolean getValorFixoFaxDDD() {
		return valorFixoFaxDDD;
	}
	public void setValorFixoFaxDDD(Boolean valorFixoFaxDDD) {
		this.valorFixoFaxDDD = valorFixoFaxDDD;
	}
	public Boolean getSelUnidadeFornecMinimoDiario() {
		return selUnidadeFornecMinimoDiario;
	}
	public void setSelUnidadeFornecMinimoDiario(Boolean selUnidadeFornecMinimoDiario) {
		this.selUnidadeFornecMinimoDiario = selUnidadeFornecMinimoDiario;
	}
	public Boolean getObgUnidadeFornecMinimoDiario() {
		return obgUnidadeFornecMinimoDiario;
	}
	public void setObgUnidadeFornecMinimoDiario(Boolean obgUnidadeFornecMinimoDiario) {
		this.obgUnidadeFornecMinimoDiario = obgUnidadeFornecMinimoDiario;
	}
	public Boolean getValorFixoUnidadeFornecMinimoDiario() {
		return valorFixoUnidadeFornecMinimoDiario;
	}
	public void setValorFixoUnidadeFornecMinimoDiario(Boolean valorFixoUnidadeFornecMinimoDiario) {
		this.valorFixoUnidadeFornecMinimoDiario = valorFixoUnidadeFornecMinimoDiario;
	}
	public Boolean getValorFixoEmail() {
		return valorFixoEmail;
	}
	public void setValorFixoEmail(Boolean valorFixoEmail) {
		this.valorFixoEmail = valorFixoEmail;
	}
	public Boolean getSelUnidadeVazaoInstan() {
		return selUnidadeVazaoInstan;
	}
	public void setSelUnidadeVazaoInstan(Boolean selUnidadeVazaoInstan) {
		this.selUnidadeVazaoInstan = selUnidadeVazaoInstan;
	}
	public Boolean getObgUnidadeVazaoInstan() {
		return obgUnidadeVazaoInstan;
	}
	public void setObgUnidadeVazaoInstan(Boolean obgUnidadeVazaoInstan) {
		this.obgUnidadeVazaoInstan = obgUnidadeVazaoInstan;
	}
	public Boolean getValorFixoUnidadeVazaoInstan() {
		return valorFixoUnidadeVazaoInstan;
	}
	public void setValorFixoUnidadeVazaoInstan(Boolean valorFixoUnidadeVazaoInstan) {
		this.valorFixoUnidadeVazaoInstan = valorFixoUnidadeVazaoInstan;
	}
	public Boolean getValorFixoValorContrato() {
		return valorFixoValorContrato;
	}
	public void setValorFixoValorContrato(Boolean valorFixoValorContrato) {
		this.valorFixoValorContrato = valorFixoValorContrato;
	}
	
	public Boolean getSelApuracaoParadaNaoProgramadaC() {
		return selApuracaoParadaNaoProgramadaC;
	}
	public void setSelApuracaoParadaNaoProgramadaC(Boolean selApuracaoParadaNaoProgramadaC) {
		this.selApuracaoParadaNaoProgramadaC = selApuracaoParadaNaoProgramadaC;
	}
	public Boolean getObgApuracaoParadaNaoProgramadaC() {
		return obgApuracaoParadaNaoProgramadaC;
	}
	public void setObgApuracaoParadaNaoProgramadaC(Boolean obgApuracaoParadaNaoProgramadaC) {
		this.obgApuracaoParadaNaoProgramadaC = obgApuracaoParadaNaoProgramadaC;
	}
	/**
	 * @param fieldName			-	{@link String}		
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @param value				-	{@link Object}
	 */
	static void setField(String fieldName, ContratoVO contratoVO, Object value) {
		
		try {
			Field field = ContratoVO.class.getDeclaredField(fieldName);
			field.setAccessible(Boolean.TRUE);
			field.set(contratoVO, value);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Get Field
	 * 
	 * @param fieldName			-	{@link String}		
	 * @param contratoVO	-	{@link ContratoVO}
	 * @return Object - {@link Object}
	 */
	public Object getField(String fieldName, ContratoVO contratoVO) {
		
		try {
			Field field = ContratoVO.class.getDeclaredField(fieldName);
			field.setAccessible(Boolean.TRUE);
			return field.get(contratoVO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return null;
	}
	
	/**
	 * Verifica se existe atributo no objeto
	 * 
	 * @param fieldName			-	{@link String}		
	 * @param modeloContratoVO	-	{@link ModeloContratoVO}
	 * @return Boolean - {@link Boolean}
	 */
	public Boolean contemAtributo(String fieldName) {
		
		try {
			Field field = ContratoVO.class.getDeclaredField(fieldName);
			field.setAccessible(Boolean.TRUE);
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return false;
	}
	public Boolean getIsTelaChamado() {
		return isTelaChamado;
	}
	public void setIsTelaChamado(Boolean isTelaChamado) {
		this.isTelaChamado = isTelaChamado;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public String getPrazoVigencia() {
		return prazoVigencia;
	}
	public void setPrazoVigencia(String prazoVigencia) {
		this.prazoVigencia = prazoVigencia;
	}
	public String getIncentivosComerciais() {
		return incentivosComerciais;
	}
	public void setIncentivosComerciais(String incentivosComerciais) {
		this.incentivosComerciais = incentivosComerciais;
	}
	public String getIncentivosInfraestrutura() {
		return incentivosInfraestrutura;
	}
	public void setIncentivosInfraestrutura(String incentivosInfraestrutura) {
		this.incentivosInfraestrutura = incentivosInfraestrutura;
	}
	public String[] getqDCContrato() {
		return qDCContrato;
	}
	public void setqDCContrato(String[] qDCContrato) {
		this.qDCContrato = qDCContrato;
	}
	public Boolean getSelPrazoVigencia() {
		return selPrazoVigencia;
	}
	public void setSelPrazoVigencia(Boolean selPrazoVigencia) {
		this.selPrazoVigencia = selPrazoVigencia;
	}
	public Boolean getObgPrazoVigencia() {
		return obgPrazoVigencia;
	}
	public void setObgPrazoVigencia(Boolean obgPrazoVigencia) {
		this.obgPrazoVigencia = obgPrazoVigencia;
	}
	public Boolean getSelIncentivosComerciais() {
		return selIncentivosComerciais;
	}
	public void setSelIncentivosComerciais(Boolean selIncentivosComerciais) {
		this.selIncentivosComerciais = selIncentivosComerciais;
	}
	public Boolean getObgIncentivosComerciais() {
		return obgIncentivosComerciais;
	}
	public void setObgIncentivosComerciais(Boolean obgIncentivosComerciais) {
		this.obgIncentivosComerciais = obgIncentivosComerciais;
	}
	public Boolean getSelIncentivosInfraestrutura() {
		return selIncentivosInfraestrutura;
	}
	public void setSelIncentivosInfraestrutura(Boolean selIncentivosInfraestrutura) {
		this.selIncentivosInfraestrutura = selIncentivosInfraestrutura;
	}
	public Boolean getObgIncentivosInfraestrutura() {
		return obgIncentivosInfraestrutura;
	}
	public void setObgIncentivosInfraestrutura(Boolean obgIncentivosInfraestrutura) {
		this.obgIncentivosInfraestrutura = obgIncentivosInfraestrutura;
	}
	public Boolean getValorFixoPrazoVigencia() {
		return valorFixoPrazoVigencia;
	}
	public void setValorFixoPrazoVigencia(Boolean valorFixoPrazoVigencia) {
		this.valorFixoPrazoVigencia = valorFixoPrazoVigencia;
	}
	public Boolean getValorFixoIncentivosComerciais() {
		return valorFixoIncentivosComerciais;
	}
	public void setValorFixoIncentivosComerciais(Boolean valorFixoIncentivosComerciais) {
		this.valorFixoIncentivosComerciais = valorFixoIncentivosComerciais;
	}
	public Boolean getValorFixoIncetivosInfraestrutura() {
		return valorFixoIncetivosInfraestrutura;
	}
	public void setValorFixoIncetivosInfraestrutura(Boolean valorFixoIncetivosInfraestrutura) {
		this.valorFixoIncetivosInfraestrutura = valorFixoIncetivosInfraestrutura;
	}

}
