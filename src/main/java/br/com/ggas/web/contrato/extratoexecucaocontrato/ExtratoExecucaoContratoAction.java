package br.com.ggas.web.contrato.extratoexecucaocontrato;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.QuantidadeVolumesVO;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.programacao.ApuracaoQuantidadePenalidadePeriVO;
import br.com.ggas.web.contrato.programacao.ApuracaoRecuperacaoQuantidadeVO;

/**
 * Classe responsável pelas operações referentes ao extrato de execução dos contratos.
 *
 */
@Controller
public class ExtratoExecucaoContratoAction extends GenericAction {

	private static final String EXECUCAO_CONTRATO_VO = "execucaoContratoVO";

	private static final String ATRIBUTO_CLIENTE_ASSINATURA_ENDERECOS = "clienteAssinatura.enderecos";

	private static final String DATA_INICIAL = "Data Inicial";

	private static final String LISTA_CONTRATO_QDC = "listaContratoQDC";

	private static final String MODELO_CONTRATO = "modeloContrato";

	private static final String CLIENTE_ASSINATURA = "clienteAssinatura";

	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";

	private static final String LISTA_CONTRATO = "listaContratos";

	private static final String LISTA_HISTORICO_ADITIVO = "listaHistoricoAditivo";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String LISTA_CONTRATO_MODALIDADE = "listaContratoModalidade";

	private static final String LISTA_TIPO_CONSULTA = "listaTipoConsulta";

	private static final String LISTA_PENALIDADE_A_MAIOR = "listaPenalidadeMaior";

	private static final String LISTA_PENALIDADE_A_MENOR = "listaPenalidadeMenor";

	private static final String LISTA_APURACAO_TOP = "listaApuracaoTOP";

	private static final String LISTA_APURACAO_SOP = "listaApuracaoSOP";

	private static final String LISTA_APURACAO_DOP = "listaApuracaoDOP";

	private static final String LISTA_APURACAO_GAS_FORA_ESPECIFICACAO = "listaApuracaoGasForaEspecificacao";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String ID_CONTRATO = "idContrato";

	private static final String TIPO_CONSULTA = "tipoConsulta";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String CONSULTAR_QDA_VOLUMES = "consultarQdaVolumes";

	private static final String ID_PENALIDADES_ASSOCIADAS = "idPenalidadesAssociadas";

	private static final String LISTA_APURACAO_RECUPERACAO_VO = "listaApuracaoRecuperacaoVO";

	private static final String CODIGO_COMPROMISSO = "codigoCompromisso";

	private static final String CODIGO_PENALIDADE = "codigoPenalidade";

	private static final String CODIGO_VOLUME = "codigoVolume";

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

	@Autowired
	private ControladorProgramacao controladorProgramacao;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Reexibir detalhe extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @param execucaoContratoVO - {@link ExtratoExecucaoContratoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("reexibirDetalheExtratoExecucaoContrato")
	public String reexibirDetalheExtratoExecucaoContrato(Model model, ExtratoExecucaoContratoVO execucaoContratoVO,
			BindingResult bindingResult)  {

		model.addAttribute(CONSULTAR_QDA_VOLUMES, Boolean.TRUE);

		return this.exibirDetalheExtratoExecucaoContrato(model, execucaoContratoVO, bindingResult);
	}

	/**
	 * Exibir detalhe extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @param execucaoContratoVO - {@link ExtratoExecucaoContratoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirDetalheExtratoExecucaoContrato")
	public String exibirDetalheExtratoExecucaoContrato(Model model, ExtratoExecucaoContratoVO execucaoContratoVO,
			BindingResult bindingResult) {

		try {
			Long idContrato = execucaoContratoVO.getIdContrato();

			if ((idContrato != null) && (idContrato > 0)) {

				Contrato contrato = (Contrato) controladorContrato.obter(idContrato, MODELO_CONTRATO, CLIENTE_ASSINATURA,
						ATRIBUTO_CLIENTE_ASSINATURA_ENDERECOS, LISTA_CONTRATO_QDC);

				this.configurarTelaDetalheExtratoExecucaoContrato(contrato, model, execucaoContratoVO);

				execucaoContratoVO.setIdContrato(idContrato);
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		model.addAttribute(CODIGO_COMPROMISSO, QuantidadeVolumesVO.CHAVE_APURACAO);
		model.addAttribute(EXECUCAO_CONTRATO_VO, execucaoContratoVO);

		return "exibirDetalheExtratoExecucaoContrato";

	}

	/**
	 * Detalhar extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @param execucaoContratoVO - {@link ExtratoExecucaoContratoVO}
	 * @return view - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @param bindingResult - {@link BindingResult} 
	 * @throws ParseException - {@link ParseException}
	 */
	@RequestMapping("detalharExtratoExecucaoContrato")
	public String detalharExtratoExecucaoContrato(Model model, HttpServletRequest request, ExtratoExecucaoContratoVO execucaoContratoVO,
			BindingResult bindingResult) throws ParseException {

		try {

			Long idContrato = execucaoContratoVO.getIdContrato();

			if ((idContrato != null) && (idContrato > 0)) {

				Contrato contrato = (Contrato) controladorContrato.obter(idContrato, MODELO_CONTRATO, CLIENTE_ASSINATURA,
						ATRIBUTO_CLIENTE_ASSINATURA_ENDERECOS, LISTA_CONTRATO_QDC);

				execucaoContratoVO.setIdContrato(idContrato);

				this.configurarDadosGeraisExtratoExecucaoContrato(contrato, model, execucaoContratoVO);

				model.addAttribute(LISTA_TIPO_CONSULTA, QuantidadeVolumesVO.TIPOS_QDA_VOLUME);
				model.addAttribute(CODIGO_COMPROMISSO, QuantidadeVolumesVO.CHAVE_APURACAO);
				model.addAttribute(CODIGO_PENALIDADE, QuantidadeVolumesVO.CHAVE_PENALIDADE);
				model.addAttribute(CODIGO_VOLUME, QuantidadeVolumesVO.CHAVE_VOLUME);
				model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());
				model.addAttribute(INTERVALO_ANOS_DATA, this.obterIntervaloAnosData());

				Integer tipoConsulta = execucaoContratoVO.getTipoConsulta();

				if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_VOLUME)) {

					this.configurarDadosQuantidadeVolumeExtratoExecucaoContrato(model, contrato, execucaoContratoVO);

				} else if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_PENALIDADE)) {

					this.configurarDadosPenalidade(model, execucaoContratoVO);

				} else if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_APURACAO)) {

					this.configurarDadosApuracao(model, execucaoContratoVO);
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(EXECUCAO_CONTRATO_VO, execucaoContratoVO);

		return "exibirDetalheExtratoExecucaoContrato";

	}

	/**
	 * Exibir extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param execucaoContratoVO - {@link ExtratoExecucaoContratoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirExtratoExecucaoContrato")
	public String exibirExtratoExecucaoContrato(Model model, HttpServletRequest request, ExtratoExecucaoContratoVO execucaoContratoVO,
			BindingResult bindingResult) {

		try {
			Long idContrato = execucaoContratoVO.getIdContrato();

			if ((idContrato != null) && (idContrato > 0)) {

				Contrato contrato = (Contrato) controladorContrato.obter(idContrato, MODELO_CONTRATO, CLIENTE_ASSINATURA,
						ATRIBUTO_CLIENTE_ASSINATURA_ENDERECOS, LISTA_CONTRATO_QDC);

				this.configurarTelaGeralExtratoExecucaoContrato(contrato, model, execucaoContratoVO);
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(EXECUCAO_CONTRATO_VO, execucaoContratoVO);

		return "exibirExtratoExecucaoContrato";
	}

	/**
	 * Exibir pesquisa extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirPesquisaExtratoExecucaoContrato")
	public String exibirPesquisaExtratoExecucaoContrato(Model model) {

		try {
			ExtratoExecucaoContratoVO execucaoContratoVO = new ExtratoExecucaoContratoVO();
			execucaoContratoVO.setHabilitado(String.valueOf(Boolean.TRUE));
			model.addAttribute(EXECUCAO_CONTRATO_VO, execucaoContratoVO);
			this.carregarDados(model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaExtratoExecucaoContrato";

	}

	/**
	 * Pesquisar extrato execucao contrato.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param execucaoContratoVO - {@link ExtratoExecucaoContratoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("pesquisarExtratoExecucaoContrato")
	public String pesquisarExtratoExecucaoContrato(Model model, HttpServletRequest request, ExtratoExecucaoContratoVO execucaoContratoVO,
			BindingResult bindingResult) {

		model.addAttribute(EXECUCAO_CONTRATO_VO, execucaoContratoVO);

		try {
			Map<String, Object> filtro = this.obterFiltroPesquisaContrato(execucaoContratoVO);

			super.adicionarFiltroPaginacao(request, filtro);

			Collection<Contrato> listaContratos = controladorContrato.consultarContratos(filtro);

			request.setAttribute(LISTA_CONTRATO, super.criarColecaoPaginada(request, filtro, listaContratos));

			this.carregarDados(model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaExtratoExecucaoContrato";

	}

	private Map<String, Object> obterFiltroPesquisaContrato(ExtratoExecucaoContratoVO execucaoContratoVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();

		Long idCliente = execucaoContratoVO.getIdCliente();
		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		Long idImovel = execucaoContratoVO.getIdImovel();
		if ((idImovel != null) && (idImovel > 0)) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		String numeroContrato = execucaoContratoVO.getNumeroContrato();
		if (!StringUtils.isEmpty(numeroContrato)) {
			filtro.put("numero", Util.converterCampoStringParaValorInteger("Número do Contrato", numeroContrato));
		}

		String habilitado = execucaoContratoVO.getHabilitado();
		if (!StringUtils.isEmpty(habilitado)) {
			filtro.put("habilitado", Boolean.valueOf(habilitado));
		}

		String situacaoContraro = execucaoContratoVO.getSituacaoContrato();
		if (!StringUtils.isEmpty(situacaoContraro) && (!"-1".equals(situacaoContraro))) {
			filtro.put("idSituacaoContrato", new Long[] { Long.valueOf(situacaoContraro) });
		}

		filtro.put("situacaoDesprezada", SituacaoContrato.EM_CRIACAO);

		return filtro;
	}

	private void carregarDados(Model model) throws GGASException {

		Collection<SituacaoContrato> listaSituacao = Fachada.getInstancia().obterListaSituacaoContrato();
		Collection<SituacaoContrato> newListaSituacao = new ArrayList<>();

		if (listaSituacao != null) {
			for (SituacaoContrato situacaoContrato : listaSituacao) {
				if (situacaoContrato.getChavePrimaria() != SituacaoContrato.EM_CRIACAO) {
					newListaSituacao.add(situacaoContrato);
				}
			}
		}

		model.addAttribute(LISTA_SITUACAO_CONTRATO, newListaSituacao);

	}

	private void configurarTelaGeralExtratoExecucaoContrato(Contrato contrato, Model model, ExtratoExecucaoContratoVO execucaoContratoVO)
			throws GGASException {

		this.configurarDadosGeraisExtratoExecucaoContrato(contrato, model, execucaoContratoVO);
		this.configurarDadosPontoConsumoContratoExtratoExecucaoContrato(model, execucaoContratoVO);
		this.configurarDadosHistoricoAditivosExtratoExecucaoContrato(model, execucaoContratoVO);

	}

	private void configurarDadosHistoricoAditivosExtratoExecucaoContrato(Model model, ExtratoExecucaoContratoVO execucaoContratoVO)
			throws GGASException {

		model.addAttribute(LISTA_HISTORICO_ADITIVO,
				controladorContrato.obterHistoricoAditivos(this.obterFiltroPesquisaHistoricoAditivo(execucaoContratoVO)));

	}

	private Map<String, Object> obterFiltroPesquisaHistoricoAditivo(ExtratoExecucaoContratoVO execucaoContratoVO) {

		Map<String, Object> filtro = new HashMap<>();

		Long idContrato = execucaoContratoVO.getIdContrato();
		if ((idContrato != null) && (idContrato > 0)) {
			filtro.put(ID_CONTRATO, idContrato);
		}

		return filtro;
	}

	private void configurarDadosPontoConsumoContratoExtratoExecucaoContrato(Model model, ExtratoExecucaoContratoVO execucaoContratoVO)
			throws GGASException {

		model.addAttribute(LISTA_PONTO_CONSUMO,
				controladorContrato.obterPontosConsumoContrato(this.obterFiltroPesquisaPontoConsumoContrato(execucaoContratoVO)));

	}

	private void configurarDadosApuracao(Model model, ExtratoExecucaoContratoVO execucaoContratoVO) throws GGASException, ParseException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		Map<String, Object> filtro;
		BigDecimal totalQDCTOP = BigDecimal.ZERO;
		BigDecimal totalQDSTOP = BigDecimal.ZERO;
		BigDecimal totalQDPTOP = BigDecimal.ZERO;
		BigDecimal totalQNRTOP = BigDecimal.ZERO;
		BigDecimal totalQPNRTOP = BigDecimal.ZERO;

		BigDecimal totalQDCSOP = BigDecimal.ZERO;
		BigDecimal totalQDSSOP = BigDecimal.ZERO;
		BigDecimal totalQDPSOP = BigDecimal.ZERO;
		BigDecimal totalQNRSOP = BigDecimal.ZERO;
		BigDecimal totalQPNRSOP = BigDecimal.ZERO;

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long[] chavesPontoConsumo = this.obterChavesPontosConsumoContrato(execucaoContratoVO);

		// TOP
		Long codPenalidadeTop =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));
		filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeTop);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeTOP =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);

		Collection<ApuracaoQuantidadePenalidadePeriVO> listaPeriVO = new HashSet<>();

		iteraListaApuracaoQuantidadePenalidadePeriodicidadeTOP(listaApuracaoQuantidadePenalidadePeriodicidadeTOP, listaPeriVO);

		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao =
				controladorProgramacao.consultarVolumeExecucaoContratoSolicitacao(df.parse(execucaoContratoVO.getDataConsultaInicial()),
						df.parse(execucaoContratoVO.getDataConsultaFinal()), chavesPontoConsumo);

		for (ApuracaoQuantidadePenalidadePeriVO apTOP : listaPeriVO) {
			for (SolicitacaoConsumoPontoConsumo solic : listaSolicitacao) {
				if (solic.getDataSolicitacao().equals(apTOP.getDataInicioApuracao()) && solic.getValorQDS() != null) {
					apTOP.setValorQDS(String.valueOf(solic.getValorQDS()));
				}

			}
		}

		for (ApuracaoQuantidadePenalidadePeriVO totaisTOP : listaPeriVO) {
			if (totaisTOP.getValorQDC() != null && !(totaisTOP.getValorQDC().isEmpty())) {
				totalQDCTOP = totalQDCTOP.add(new BigDecimal(totaisTOP.getValorQDC()));
			}
			if (totaisTOP.getValorQDS() != null && !(totaisTOP.getValorQDS().isEmpty())) {
				totalQDSTOP = totalQDSTOP.add(new BigDecimal(totaisTOP.getValorQDS()));
			}
			if (totaisTOP.getValorQDP() != null && !(totaisTOP.getValorQDP().isEmpty())) {
				totalQDPTOP = totalQDPTOP.add(new BigDecimal(totaisTOP.getValorQDP()));
			}
			if (totaisTOP.getValorQNR() != null && !(totaisTOP.getValorQNR().isEmpty())) {
				totalQNRTOP = totalQNRTOP.add(new BigDecimal(totaisTOP.getValorQNR()));
			}
			if (totaisTOP.getValorQPNR() != null && !(totaisTOP.getValorQPNR().isEmpty())) {
				totalQPNRTOP = totalQPNRTOP.add(new BigDecimal(totaisTOP.getValorQPNR()));
			}
		}

		execucaoContratoVO.setTotalQDCTOP(String.valueOf(totalQDCTOP));
		execucaoContratoVO.setTotalQDSTOP(String.valueOf(totalQDSTOP));
		execucaoContratoVO.setTotalQDPTOP(String.valueOf(totalQDPTOP));
		execucaoContratoVO.setTotalQNRTOP(String.valueOf(totalQNRTOP));
		execucaoContratoVO.setTotalQPNRTOP(String.valueOf(totalQPNRTOP.toString()));

		model.addAttribute(LISTA_APURACAO_TOP, this.ordenarListaVO(listaPeriVO));

		// SOP
		Long codPenalidadeSOP =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY));
		filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeSOP);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeSOP =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);

		Collection<ApuracaoQuantidadePenalidadePeriVO> listaPeriVOSOP = new HashSet<>();

		iteraListaApuracaoQuantidadePenalidadePeriodicidadeTOP(listaApuracaoQuantidadePenalidadePeriodicidadeSOP, listaPeriVOSOP);

		for (ApuracaoQuantidadePenalidadePeriVO apSOP : listaPeriVOSOP) {
			for (SolicitacaoConsumoPontoConsumo solic : listaSolicitacao) {
				if (solic.getDataSolicitacao().equals(apSOP.getDataInicioApuracao()) && solic.getValorQDS() != null) {
					apSOP.setValorQDS(String.valueOf(solic.getValorQDS()));
				}

			}
		}

		for (ApuracaoQuantidadePenalidadePeriVO totaisSOP : listaPeriVOSOP) {

			if (totaisSOP.getValorQDC() != null && !(totaisSOP.getValorQDC().isEmpty())) {
				totalQDCSOP = totalQDCSOP.add(new BigDecimal(totaisSOP.getValorQDC()));
			}
			if (totaisSOP.getValorQDS() != null && !(totaisSOP.getValorQDS().isEmpty())) {
				totalQDSSOP = totalQDSSOP.add(new BigDecimal(totaisSOP.getValorQDS()));
			}
			if (totaisSOP.getValorQDP() != null && !(totaisSOP.getValorQDP().isEmpty())) {
				totalQDPSOP = totalQDPSOP.add(new BigDecimal(totaisSOP.getValorQDP()));
			}
			if (totaisSOP.getValorQNR() != null && !(totaisSOP.getValorQNR().isEmpty())) {
				totalQNRSOP = totalQNRSOP.add(new BigDecimal(totaisSOP.getValorQNR()));
			}
			if (totaisSOP.getValorQPNR() != null && !(totaisSOP.getValorQPNR().isEmpty())) {
				totalQPNRSOP = totalQPNRSOP.add(new BigDecimal(totaisSOP.getValorQPNR()));
			}

		}

		execucaoContratoVO.setTotalQDCSOP(String.valueOf(totalQDCSOP));
		execucaoContratoVO.setTotalQDSSOP(String.valueOf(totalQDSSOP));
		execucaoContratoVO.setTotalQDPSOP(String.valueOf(totalQDPSOP));
		execucaoContratoVO.setTotalQNRSOP(String.valueOf(totalQNRSOP));
		execucaoContratoVO.setTotalQPNRSOP(String.valueOf(totalQPNRSOP));

		model.addAttribute(LISTA_APURACAO_SOP, this.ordenarListaVO(listaPeriVOSOP));

	}

	private void iteraListaApuracaoQuantidadePenalidadePeriodicidadeTOP(
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeTOP,
			Collection<ApuracaoQuantidadePenalidadePeriVO> listaPeriVO) {
		for (ApuracaoQuantidadePenalidadePeriodicidade top : listaApuracaoQuantidadePenalidadePeriodicidadeTOP) {
			ApuracaoQuantidadePenalidadePeriVO periVO = new ApuracaoQuantidadePenalidadePeriVO();
			periVO.setDataInicioApuracao(top.getDataInicioApuracao());
			periVO.setDataFimApuracao(top.getDataFimApuracao());
			periVO.setPeriodoApuracao(top.getPeriodoApuracao());
			if (top.getQtdaDiariaProgramada() != null) {
				periVO.setValorQDP(String.valueOf(top.getQtdaDiariaProgramada()));
			} else {
				periVO.setValorQDP("");
			}
			if (top.getQtdaDiariaContratada() != null) {
				periVO.setValorQDC(String.valueOf(top.getQtdaDiariaContratada()));
			} else {
				periVO.setValorQDC("");
			}
			if (top.getQtdaNaoRetirada() != null) {
				periVO.setValorQNR(String.valueOf(top.getQtdaNaoRetirada()));
			} else {
				periVO.setValorQNR("");
			}
			if (top.getQtdaPagaNaoRetirada() != null) {
				periVO.setValorQPNR(String.valueOf(top.getQtdaPagaNaoRetirada()));
			} else {
				periVO.setValorQPNR("");
			}
			listaPeriVO.add(periVO);
		}
	}

	private void configurarDadosPenalidade(Model model, ExtratoExecucaoContratoVO execucaoContratoVO) throws GGASException, ParseException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		BigDecimal totalQDC = BigDecimal.ZERO;
		BigDecimal totalQDR = BigDecimal.ZERO;
		BigDecimal totalRetiradaMaior = BigDecimal.ZERO;
		BigDecimal totalValorCalculadoRetiradaMaior = BigDecimal.ZERO;
		BigDecimal totalPenalidade = BigDecimal.ZERO;
		BigDecimal totalQDCMenor = BigDecimal.ZERO;
		BigDecimal totalQDRMenor = BigDecimal.ZERO;
		BigDecimal totalRetiradaMenor = BigDecimal.ZERO;
		BigDecimal totalPenalidadeMenor = BigDecimal.ZERO;
		BigDecimal totalValorCalculadoRetiradaMenor = BigDecimal.ZERO;

		BigDecimal totalQDCDOP = BigDecimal.ZERO;
		BigDecimal totalQDSDOP = BigDecimal.ZERO;
		BigDecimal totalQDPDOP = BigDecimal.ZERO;
		BigDecimal totalQNRDOP = BigDecimal.ZERO;

		BigDecimal totalQDCGAS = BigDecimal.ZERO;
		BigDecimal totalQDSGAS = BigDecimal.ZERO;
		BigDecimal totalQDPGAS = BigDecimal.ZERO;
		BigDecimal totalQNRGAS = BigDecimal.ZERO;

		BigDecimal totalValorCalculadoDOP = BigDecimal.ZERO;
		BigDecimal totalValorCalculadoGAS = BigDecimal.ZERO;

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long codPenalidadeRetiradaMaior =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));

		Map<String, Object> filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeRetiradaMaior);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeMaior =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);
		model.addAttribute(LISTA_PENALIDADE_A_MAIOR, this.ordenarListaPenalidade(listaApuracaoQuantidadePenalidadePeriodicidadeMaior));

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : listaApuracaoQuantidadePenalidadePeriodicidadeMaior) {
			if (apuracao.getPenalidade().getChavePrimaria() == codPenalidadeRetiradaMaior) {
				if (apuracao.getQtdaDiariaContratada() != null) {
					totalQDC = totalQDC.add(apuracao.getQtdaDiariaContratada());
				}
				if (apuracao.getQtdaDiariaRetirada() != null) {
					totalQDR = totalQDR.add(apuracao.getQtdaDiariaRetirada());
				}
				if (apuracao.getQtdaDiariaRetiradaMaior() != null) {
					totalRetiradaMaior = totalRetiradaMaior.add(apuracao.getQtdaDiariaRetiradaMaior());
				}
				if (apuracao.getValorCobrado() != null) {
					totalPenalidade = totalPenalidade.add(apuracao.getValorCobrado());
				}
				if (apuracao.getValorCalculado() != null) {
					totalValorCalculadoRetiradaMaior = totalValorCalculadoRetiradaMaior.add(apuracao.getValorCalculado());
				}
			}

		}
		execucaoContratoVO.setValorTotalQDC(String.valueOf(totalQDC));
		execucaoContratoVO.setValorTotalQDR(String.valueOf(totalQDR));
		execucaoContratoVO.setValorTotalRetiradaMaior(String.valueOf(totalRetiradaMaior));
		execucaoContratoVO.setTotalPenalidades(String.valueOf(totalPenalidade));
		execucaoContratoVO.setTotalValorCalculadoRetiradaMaior(String.valueOf(totalValorCalculadoRetiradaMaior));

		Long codPenalidadeRetiradaMenor =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));
		filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeRetiradaMenor);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeMenor =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoMenor : listaApuracaoQuantidadePenalidadePeriodicidadeMenor) {
			if (apuracaoMenor.getPenalidade().getChavePrimaria() == codPenalidadeRetiradaMenor) {
				if (apuracaoMenor.getQtdaDiariaContratada() != null) {
					totalQDCMenor = totalQDCMenor.add(apuracaoMenor.getQtdaDiariaContratada());
				}
				if (apuracaoMenor.getQtdaDiariaRetirada() != null) {
					totalQDRMenor = totalQDRMenor.add(apuracaoMenor.getQtdaDiariaRetirada());
				}
				if (apuracaoMenor.getQtdaNaoRetirada() != null) {
					totalRetiradaMenor = totalRetiradaMenor.add(apuracaoMenor.getQtdaNaoRetirada());
				}
				if (apuracaoMenor.getValorCobrado() != null) {
					totalPenalidadeMenor = totalPenalidadeMenor.add(apuracaoMenor.getValorCobrado());
				}
				if (apuracaoMenor.getValorCalculado() != null) {
					totalValorCalculadoRetiradaMenor = totalValorCalculadoRetiradaMenor.add(apuracaoMenor.getValorCalculado());
				}
			}

		}

		execucaoContratoVO.setValorTotalQDCMenor(String.valueOf(totalQDCMenor));
		execucaoContratoVO.setValorTotalQDRMenor(String.valueOf(totalQDRMenor));
		execucaoContratoVO.setValorTotalRetiradaMenor(String.valueOf(totalRetiradaMenor));
		execucaoContratoVO.setTotalPenalidadesMenor(String.valueOf(totalPenalidadeMenor));
		execucaoContratoVO.setTotalValorCalculadoRetiradaMenor(String.valueOf(totalValorCalculadoRetiradaMenor));

		model.addAttribute(LISTA_PENALIDADE_A_MENOR, ordenarListaPenalidade(listaApuracaoQuantidadePenalidadePeriodicidadeMenor));

		// DOP
		Long codPenalidadeDOP =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY));
		filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeDOP);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeDOP =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);

		Collection<ApuracaoQuantidadePenalidadePeriVO> listaPeriVODOP = new HashSet<>();

		for (ApuracaoQuantidadePenalidadePeriodicidade dop : listaApuracaoQuantidadePenalidadePeriodicidadeDOP) {
			ApuracaoQuantidadePenalidadePeriVO periVODOP = new ApuracaoQuantidadePenalidadePeriVO();
			periVODOP.setDataInicioApuracao(dop.getDataInicioApuracao());
			periVODOP.setDataFimApuracao(dop.getDataFimApuracao());
			periVODOP.setPeriodoApuracao(dop.getPeriodoApuracao());

			if (dop.getQtdaDiariaProgramada() != null) {
				periVODOP.setValorQDP(String.valueOf(dop.getQtdaDiariaProgramada()));
			} else {
				periVODOP.setValorQDP("");
			}
			if (dop.getQtdaDiariaContratada() != null) {
				periVODOP.setValorQDC(String.valueOf(dop.getQtdaDiariaContratada()));
			} else {
				periVODOP.setValorQDC("");
			}
			if (dop.getQtdaNaoRetirada() != null) {
				periVODOP.setValorQNR(String.valueOf(dop.getQtdaNaoRetirada()));
			} else {
				periVODOP.setValorQNR("");
			}
			if (dop.getQtdaPagaNaoRetirada() != null) {
				periVODOP.setValorQPNR(String.valueOf(dop.getQtdaPagaNaoRetirada()));
			} else {
				periVODOP.setValorQPNR("");
			}
			if (dop.getQtdaDiariaContratadaMedia() != null) {
				periVODOP.setValorQDCM(String.valueOf(dop.getQtdaDiariaContratadaMedia()));
			} else {
				periVODOP.setValorQDCM("");
			}
			if (dop.getValorCalculado() != null) {
				periVODOP.setValorCalculado(String.valueOf(dop.getValorCalculado()));
				totalValorCalculadoDOP = totalValorCalculadoDOP.add(dop.getValorCalculado());
			} else {
				periVODOP.setValorCalculado("");
			}

			execucaoContratoVO.setTotalValorCalculadoDOP(String.valueOf(totalValorCalculadoDOP));

			listaPeriVODOP.add(periVODOP);
		}

		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao =
				controladorProgramacao.consultarVolumeExecucaoContratoSolicitacao(df.parse(execucaoContratoVO.getDataConsultaInicial()),
						df.parse(execucaoContratoVO.getDataConsultaFinal()), this.obterChavesPontosConsumoContrato(execucaoContratoVO));

		for (ApuracaoQuantidadePenalidadePeriVO apDOP : listaPeriVODOP) {
			for (SolicitacaoConsumoPontoConsumo solic : listaSolicitacao) {
				if (solic.getDataSolicitacao().equals(apDOP.getDataInicioApuracao()) && solic.getValorQDS() != null) {
					apDOP.setValorQDS(String.valueOf(solic.getValorQDS()));
				}
			}
		}

		for (ApuracaoQuantidadePenalidadePeriVO totaisDOP : listaPeriVODOP) {

			if (totaisDOP.getValorQDC() != null && !(totaisDOP.getValorQDC().isEmpty())) {
				totalQDCDOP = totalQDCDOP.add(new BigDecimal(totaisDOP.getValorQDC()));
			}
			if (totaisDOP.getValorQDS() != null && !(totaisDOP.getValorQDS().isEmpty())) {
				totalQDSDOP = totalQDSDOP.add(new BigDecimal(totaisDOP.getValorQDS()));
			}
			if (totaisDOP.getValorQDP() != null && !(totaisDOP.getValorQDP().isEmpty())) {
				totalQDPDOP = totalQDPDOP.add(new BigDecimal(totaisDOP.getValorQDP()));
			}
			if (totaisDOP.getValorQNR() != null && !(totaisDOP.getValorQNR().isEmpty())) {
				totalQNRDOP = totalQNRDOP.add(new BigDecimal(totaisDOP.getValorQNR()));
			}

		}

		execucaoContratoVO.setTotalQDCDOP(String.valueOf(totalQDCDOP));
		execucaoContratoVO.setTotalQDSDOP(String.valueOf(totalQDSDOP));
		execucaoContratoVO.setTotalQDPDOP(String.valueOf(totalQDPDOP));
		execucaoContratoVO.setTotalQNRDOP(String.valueOf(totalQNRDOP));

		model.addAttribute(LISTA_APURACAO_DOP, this.ordenarListaVO(listaPeriVODOP));

		// GAS
		Long codPenalidadeGasForaEspecificacao = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO));
		filtro = this.obterFiltroPesquisaPenalidades(execucaoContratoVO, codPenalidadeGasForaEspecificacao);

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidadeGAS =
				controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);

		Collection<ApuracaoQuantidadePenalidadePeriVO> listaPeriVOGAS = new HashSet<>();

		for (ApuracaoQuantidadePenalidadePeriodicidade gas : listaApuracaoQuantidadePenalidadePeriodicidadeGAS) {
			ApuracaoQuantidadePenalidadePeriVO periVOGAS = new ApuracaoQuantidadePenalidadePeriVO();
			periVOGAS.setDataInicioApuracao(gas.getDataInicioApuracao());
			periVOGAS.setDataFimApuracao(gas.getDataFimApuracao());
			periVOGAS.setPeriodoApuracao(gas.getPeriodoApuracao());

			if (gas.getQtdaDiariaProgramada() != null) {
				periVOGAS.setValorQDP(String.valueOf(gas.getQtdaDiariaProgramada()));
			} else {
				periVOGAS.setValorQDP("");
			}
			if (gas.getQtdaDiariaContratada() != null) {
				periVOGAS.setValorQDC(String.valueOf(gas.getQtdaDiariaContratada()));
			} else {
				periVOGAS.setValorQDC("");
			}
			if (gas.getQtdaNaoRetirada() != null) {
				periVOGAS.setValorQNR(String.valueOf(gas.getQtdaNaoRetirada()));
			} else {
				periVOGAS.setValorQNR("");
			}
			if (gas.getQtdaPagaNaoRetirada() != null) {
				periVOGAS.setValorQPNR(String.valueOf(gas.getQtdaPagaNaoRetirada()));
			} else {
				periVOGAS.setValorQPNR("");
			}
			if (gas.getQtdaDiariaContratadaMedia() != null) {
				periVOGAS.setValorQDCM(String.valueOf(gas.getQtdaDiariaContratadaMedia()));
			} else {
				periVOGAS.setValorQDCM("");
			}
			if (gas.getValorCalculado() != null) {
				periVOGAS.setValorCalculado(String.valueOf(gas.getValorCalculado()));
				totalValorCalculadoGAS = totalValorCalculadoDOP.add(gas.getValorCalculado());
			} else {
				periVOGAS.setValorCalculado("");
			}

			execucaoContratoVO.setTotalValorCalculadoGAS(String.valueOf(totalValorCalculadoGAS));

			listaPeriVOGAS.add(periVOGAS);
		}

		for (ApuracaoQuantidadePenalidadePeriVO apGAS : listaPeriVOGAS) {
			for (SolicitacaoConsumoPontoConsumo solic : listaSolicitacao) {
				if (solic.getDataSolicitacao().equals(apGAS.getDataInicioApuracao()) && solic.getValorQDS() != null) {
					apGAS.setValorQDS(String.valueOf(solic.getValorQDS()));
				}
			}
		}

		for (ApuracaoQuantidadePenalidadePeriVO totaisGAS : listaPeriVOGAS) {

			if (totaisGAS.getValorQDC() != null && !(totaisGAS.getValorQDC().isEmpty())) {
				totalQDCGAS = totalQDCGAS.add(new BigDecimal(totaisGAS.getValorQDC()));
			}
			if (totaisGAS.getValorQDS() != null && !(totaisGAS.getValorQDS().isEmpty())) {
				totalQDSGAS = totalQDSGAS.add(new BigDecimal(totaisGAS.getValorQDS()));
			}
			if (totaisGAS.getValorQDP() != null && !(totaisGAS.getValorQDP().isEmpty())) {
				totalQDPGAS = totalQDPGAS.add(new BigDecimal(totaisGAS.getValorQDP()));
			}
			if (totaisGAS.getValorQNR() != null && !(totaisGAS.getValorQNR().isEmpty())) {
				totalQNRGAS = totalQNRDOP.add(new BigDecimal(totaisGAS.getValorQNR()));
			}

		}

		execucaoContratoVO.setTotalQDCGAS(String.valueOf(totalQDCGAS));
		execucaoContratoVO.setTotalQDSGAS(String.valueOf(totalQDSGAS));
		execucaoContratoVO.setTotalQDPGAS(String.valueOf(totalQDPGAS));
		execucaoContratoVO.setTotalQNRGAS(String.valueOf(totalQNRGAS));

		model.addAttribute(LISTA_APURACAO_GAS_FORA_ESPECIFICACAO, this.ordenarListaVO(listaPeriVOGAS));

	}

	private List<ApuracaoQuantidadePenalidadePeriVO> ordenarListaVO(Collection<ApuracaoQuantidadePenalidadePeriVO> listaApuracao) {

		List<ApuracaoQuantidadePenalidadePeriVO> lista = new ArrayList<>();
		for (ApuracaoQuantidadePenalidadePeriVO apuracao : listaApuracao) {
			lista.add(apuracao);
		}
		Collections.sort(lista, new Comparator<ApuracaoQuantidadePenalidadePeriVO>() {

			@Override
			public int compare(ApuracaoQuantidadePenalidadePeriVO o1, ApuracaoQuantidadePenalidadePeriVO o2) {

				return o1.getDataInicioApuracao().compareTo(o2.getDataInicioApuracao());
			}
		});
		return lista;
	}

	private Map<String, Object> obterFiltroPesquisaPenalidades(ExtratoExecucaoContratoVO execucaoContratoVO, Long codPenalidade)
			throws GGASException {

		Long idContrato = execucaoContratoVO.getIdContrato();
		Integer tipoConsulta = execucaoContratoVO.getTipoConsulta();
		Long contratoModalidade = execucaoContratoVO.getContratoModalidade();
		String dataInicio = execucaoContratoVO.getDataConsultaInicial();
		String dataFim = execucaoContratoVO.getDataConsultaFinal();

		Long[] idPenalidadesAssociadas = new Long[1];
		idPenalidadesAssociadas[0] = codPenalidade;

		Map<String, Object> filtro = new HashMap<>();

		if (idContrato != null && idContrato > 0) {
			filtro.put(ID_CONTRATO, idContrato);
		}

		if (idPenalidadesAssociadas != null && idPenalidadesAssociadas.length != 0) {
			filtro.put(ID_PENALIDADES_ASSOCIADAS, idPenalidadesAssociadas);
		}

		if (StringUtils.isNotEmpty(dataInicio)) {
			filtro.put("dataApuracaoInicio",
					Util.converterCampoStringParaData(DATA_INICIAL, dataInicio, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataFim)) {
			filtro.put("dataApuracaoFim", Util.converterCampoStringParaData("Data Final", dataFim, Constantes.FORMATO_DATA_BR));
		}

		if (tipoConsulta != null && tipoConsulta > 0) {
			filtro.put(TIPO_CONSULTA, tipoConsulta);
		}

		if (contratoModalidade != null && contratoModalidade > 0) {
			filtro.put("idContratoModalidade", contratoModalidade);
		}

		controladorApuracaoPenalidade.validarFiltroPesquisaPenalidadesExtratoExecucaoContrato(filtro);
		Contrato contrato = null;
		if (idContrato != null) {
			contrato = (Contrato) controladorContrato.obter(idContrato);
		}

		if (contrato != null && contrato.getChavePrimariaPai() != null) {
			filtro.remove(ID_CONTRATO);
			filtro.put(ID_CONTRATO, contrato.getChavePrimariaPai());
		}

		return filtro;
	}

	private List<ApuracaoQuantidadePenalidadePeriodicidade> ordenarListaPenalidade(
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidade) {

		List<ApuracaoQuantidadePenalidadePeriodicidade> lista = new ArrayList<>();
		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : listaPenalidade) {
			lista.add(apuracao);
		}
		Collections.sort(lista, new Comparator<ApuracaoQuantidadePenalidadePeriodicidade>() {

			@Override
			public int compare(ApuracaoQuantidadePenalidadePeriodicidade o1, ApuracaoQuantidadePenalidadePeriodicidade o2) {

				return o1.getDataInicioApuracao().compareTo(o2.getDataInicioApuracao());
			}
		});
		return lista;
	}

	private void configurarDadosQuantidadeVolumeExtratoExecucaoContrato(Model model, Contrato contrato,
			ExtratoExecucaoContratoVO execucaoContratoVO) throws GGASException {

		BigDecimal totalVolumeQDC = BigDecimal.ZERO;
		BigDecimal totalVolumeQDS = BigDecimal.ZERO;
		BigDecimal totalVolumeQDP = BigDecimal.ZERO;
		BigDecimal totalVolumeQDR = BigDecimal.ZERO;
		BigDecimal totalVolumeQR = BigDecimal.ZERO;

		Long[] chavesPontoConsumo = this.obterChavesPontosConsumoContrato(execucaoContratoVO);

		if (chavesPontoConsumo != null) {

			Long idFalhaFornecimento = EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO;
			Long idForcaMaior = EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO;
			Long idParadaProgramada = EntidadeConteudo.CHAVE_PARADA_PROGRAMADA;
			Long idParadaNaoProgramada = EntidadeConteudo.CHAVE_PARADA_NAO_PROGRAMA;

			Long[] chavesTipoParada = new Long[4];
			chavesTipoParada[0] = idFalhaFornecimento;
			chavesTipoParada[1] = idForcaMaior;
			chavesTipoParada[2] = idParadaProgramada;
			chavesTipoParada[3] = idParadaNaoProgramada;

			Date dataInicial = null;
			Date dataFinal = null;
			String dtInicial = String.valueOf(execucaoContratoVO.getDataConsultaInicial());
			String dtFinal = String.valueOf(execucaoContratoVO.getDataConsultaFinal());

			if (dtInicial != null && !dtInicial.isEmpty()) {
				dataInicial = Util.converterCampoStringParaData(DATA_INICIAL, dtInicial, Constantes.FORMATO_DATA_BR);
			}
			if (dtFinal != null && !dtFinal.isEmpty()) {
				dataFinal = Util.converterCampoStringParaData(DATA_INICIAL, dtFinal, Constantes.FORMATO_DATA_BR);
			}

			Collection<ParadaProgramada> listaParadaProgramada = controladorProgramacao
					.consultarParadasProgramadasPeriodo(chavesPontoConsumo, chavesTipoParada, dataInicial, dataFinal, null);

			execucaoContratoVO.setFalhasFornecimento(this
					.collectionParaArrayDatasParadaProgramada(this.ordenarParadaProgramada(listaParadaProgramada), idFalhaFornecimento));
			execucaoContratoVO.setParadasProgramada(
					this.collectionParaArrayDatasParadaProgramada(this.ordenarParadaProgramada(listaParadaProgramada), idParadaProgramada));
			execucaoContratoVO.setCasosFortuitoForcaMaior(
					this.collectionParaArrayDatasParadaProgramada(this.ordenarParadaProgramada(listaParadaProgramada), idForcaMaior));
			execucaoContratoVO.setParadaNaoProgramada(this
					.collectionParaArrayDatasParadaProgramada(this.ordenarParadaProgramada(listaParadaProgramada), idParadaNaoProgramada));

			Collection<ApuracaoRecuperacaoQuantidadeVO> listaApuracaoRecuperacaoVO = controladorApuracaoPenalidade
					.obterApuracaoRecuperacaoQuantidadeVOs(contrato.getChavePrimaria(), dataInicial, dataFinal, chavesPontoConsumo);

			for (ApuracaoRecuperacaoQuantidadeVO recupera : listaApuracaoRecuperacaoVO) {
				if (recupera.getValorQDC() != null && !recupera.getValorQDC().isEmpty()) {
					totalVolumeQDC = totalVolumeQDC.add(new BigDecimal(recupera.getValorQDC()));
				}
				if (recupera.getValorQDS() != null && !(recupera.getValorQDS().isEmpty())) {
					totalVolumeQDS = totalVolumeQDS.add(new BigDecimal(recupera.getValorQDS()));
				}
				if (recupera.getValorQDP() != null && !(recupera.getValorQDP().isEmpty())) {
					totalVolumeQDP = totalVolumeQDP.add(new BigDecimal(recupera.getValorQDP()));
				}
				if (recupera.getValorQDR() != null && !(recupera.getValorQDR().isEmpty())) {
					totalVolumeQDR = totalVolumeQDR.add(new BigDecimal(recupera.getValorQDR()));
				}
				if (recupera.getValorQR() != null && !(recupera.getValorQR().isEmpty())) {
					totalVolumeQR = totalVolumeQR.add(new BigDecimal(recupera.getValorQR()));
				}
			}

			execucaoContratoVO.setTotalVolumeQDC(String.valueOf(totalVolumeQDC));
			execucaoContratoVO.setTotalVolumeQDS(String.valueOf(totalVolumeQDS));
			execucaoContratoVO.setTotalVolumeQDP(String.valueOf(totalVolumeQDP));
			execucaoContratoVO.setTotalVolumeQDR(String.valueOf(totalVolumeQDR));
			execucaoContratoVO.setTotalVolumeQR(String.valueOf(totalVolumeQR));

			Collection<ApuracaoRecuperacaoQuantidadeVO> listaOrdenada = this.ordenarLista(listaApuracaoRecuperacaoVO);

			model.addAttribute(LISTA_APURACAO_RECUPERACAO_VO, listaOrdenada);

		}

	}

	private List<ApuracaoRecuperacaoQuantidadeVO> ordenarLista(Collection<ApuracaoRecuperacaoQuantidadeVO> listaApuracao) {

		List<ApuracaoRecuperacaoQuantidadeVO> lista = new ArrayList<>();
		for (ApuracaoRecuperacaoQuantidadeVO apuracao : listaApuracao) {
			lista.add(apuracao);
		}
		Collections.sort(lista, new Comparator<ApuracaoRecuperacaoQuantidadeVO>() {

			@Override
			public int compare(ApuracaoRecuperacaoQuantidadeVO o1, ApuracaoRecuperacaoQuantidadeVO o2) {

				return o1.getData().compareTo(o2.getData());
			}
		});
		return lista;
	}

	private String[] collectionParaArrayDatasParadaProgramada(Collection<ParadaProgramada> listaParadaProgramada, Long tipoParada) {

		List<String> novoArray = new ArrayList<>();
		for (ParadaProgramada paradaProgramada : listaParadaProgramada) {
			if (paradaProgramada.getTipoParada().getChavePrimaria() == tipoParada) {
				novoArray.add(Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(), Constantes.FORMATO_DATA_BR));
			}
		}
		String[] retorno = new String[novoArray.size()];
		novoArray.toArray(retorno);

		return retorno;
	}

	private List<ParadaProgramada> ordenarParadaProgramada(Collection<ParadaProgramada> listaParada) {

		List<ParadaProgramada> lista = new ArrayList<>();
		for (ParadaProgramada parada : listaParada) {
			lista.add(parada);
		}
		Collections.sort(lista, new Comparator<ParadaProgramada>() {

			@Override
			public int compare(ParadaProgramada o1, ParadaProgramada o2) {

				return o1.getDataParada().compareTo(o2.getDataParada());
			}
		});
		return lista;
	}

	private Long[] obterChavesPontosConsumoContrato(ExtratoExecucaoContratoVO execucaoContratoVO) throws GGASException {

		Collection<PontoConsumo> listaPontoConsumo =
				controladorContrato.obterPontosConsumoContrato(this.obterFiltroPesquisaPontoConsumoContrato(execucaoContratoVO));

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {

			Long[] chavesPontoConsumo = new Long[listaPontoConsumo.size()];

			int i = 0;

			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				chavesPontoConsumo[i] = pontoConsumo.getChavePrimaria();
				i++;
			}

			return chavesPontoConsumo;

		} else {
			return new Long[0];
		}

	}

	private Map<String, Object> obterFiltroPesquisaPontoConsumoContrato(ExtratoExecucaoContratoVO execucaoContratoVO) {

		Map<String, Object> filtro = new HashMap<>();

		Long idContrato = execucaoContratoVO.getIdContrato();
		if ((idContrato != null) && (idContrato > 0)) {
			filtro.put(ID_CONTRATO, idContrato);
		}

		return filtro;

	}

	private void configurarTelaDetalheExtratoExecucaoContrato(Contrato contrato, Model model, ExtratoExecucaoContratoVO execucaoContratoVO)
			throws GGASException {

		this.configurarDadosGeraisExtratoExecucaoContrato(contrato, model, execucaoContratoVO);

		model.addAttribute(LISTA_TIPO_CONSULTA, QuantidadeVolumesVO.TIPOS_QDA_VOLUME);
		model.addAttribute(LISTA_CONTRATO_MODALIDADE, controladorContrato.listarContratoModalidade());
		model.addAttribute(INTERVALO_ANOS_DATA, this.obterIntervaloAnosData());

	}

	private String obterIntervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno;
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void configurarDadosGeraisExtratoExecucaoContrato(Contrato contrato, Model model, ExtratoExecucaoContratoVO execucaoContratoVO)
			throws GGASException {

		model.addAttribute(MODELO_CONTRATO, contrato.getModeloContrato());

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		execucaoContratoVO.setNumeroContrato("" + contrato.getNumeroFormatado());
		if (contrato.getDataAssinatura() != null) {
			execucaoContratoVO.setDataAssinaturaContrato(df.format(contrato.getDataAssinatura()));
		} else {
			execucaoContratoVO.setDataAssinaturaContrato("");
		}

		if (contrato.getDataVencimentoObrigacoes() != null) {
			execucaoContratoVO.setDataVencimentoObrigacoesContratuais(df.format(contrato.getDataVencimentoObrigacoes()));
		} else {
			execucaoContratoVO.setDataVencimentoObrigacoesContratuais("");
		}

		if (contrato.getClienteAssinatura() != null) {
			execucaoContratoVO.setNomeCliente("" + contrato.getClienteAssinatura().getNome());
			execucaoContratoVO.setCnpjCpfCliente(contrato.getClienteAssinatura().getNumeroDocumentoFormatado());

			if (contrato.getClienteAssinatura().getEnderecos() != null && !contrato.getClienteAssinatura().getEnderecos().isEmpty()) {
				execucaoContratoVO.setEnderecoCliente("" + contrato.getClienteAssinatura().getEnderecoPrincipal().getEnderecoFormatado());
			}

			if (contrato.getClienteAssinatura().getEmailPrincipal() != null) {
				execucaoContratoVO.setEmailCliente("" + contrato.getClienteAssinatura().getEmailPrincipal());
			}
		}

		List<ContratoQDC> listaContratoQDC = new ArrayList<>(contrato.getListaContratoQDC());
		BeanComparator comparador = new BeanComparator("data");
		Collections.sort((List) listaContratoQDC, comparador);

		if (!listaContratoQDC.isEmpty()) {
			String[] qDCContrato = new String[listaContratoQDC.size()];
			StringBuilder stringBuilder;
			int i = 0;
			for (ContratoQDC contratoQDC : listaContratoQDC) {
				stringBuilder = new StringBuilder();
				stringBuilder.append(Util.converterDataParaStringSemHora(contratoQDC.getData(), Constantes.FORMATO_DATA_BR));
				stringBuilder.append(" | ");
				stringBuilder.append(Util.converterCampoCurrencyParaString(contratoQDC.getMedidaVolume(), Constantes.LOCALE_PADRAO));
				qDCContrato[i] = stringBuilder.toString();
				i++;
			}
			execucaoContratoVO.setQdcContrato(qDCContrato);
		}

	}
}
