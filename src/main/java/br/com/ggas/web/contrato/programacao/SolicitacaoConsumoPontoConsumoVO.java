/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

@XmlRootElement
public class SolicitacaoConsumoPontoConsumoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1894183794959890201L;

	private Long idPontoConsumo;

	private Date dataSolicitacao;

	private String valorQDC;

	private String valorQDS;

	private String valorQPNR;

	private String valorQDP;

	private Boolean indicadorAceite;

	private String descricaoMotivoNaoAceite;

	private String nomeCliente;

	private String descricaoPontoConsumo;

	private Long idModalidade;

	private String descricaoModalidade;
	
	private String comentario;

	private String indicadorTipoProgramacaoConsumo;

	/**
	 * @return the descricaoMotivoNaoAceite
	 */
	public String getDescricaoMotivoNaoAceite() {

		return descricaoMotivoNaoAceite;
	}

	/**
	 * @param descricaoMotivoNaoAceite
	 *            the descricaoMotivoNaoAceite to
	 *            set
	 */
	public void setDescricaoMotivoNaoAceite(String descricaoMotivoNaoAceite) {

		this.descricaoMotivoNaoAceite = descricaoMotivoNaoAceite;
	}

	/**
	 * @return the idPontoConsumo
	 */
	public Long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the dataSolicitacao
	 */
	public Date getDataSolicitacao() {
		Date data = null;
		if (this.dataSolicitacao != null) {
			data = (Date) dataSolicitacao.clone();
		}
		return data;
	}

	/**
	 * @param dataSolicitacao
	 *            the dataSolicitacao to set
	 */
	public void setDataSolicitacao(Date dataSolicitacao) {

		this.dataSolicitacao = dataSolicitacao;
	}

	/**
	 * @return the valorQDC
	 */
	public String getValorQDC() {

		return valorQDC;
	}

	/**
	 * @param valorQDC
	 *            the valorQDC to set
	 */
	public void setValorQDC(String valorQDC) {

		this.valorQDC = valorQDC;
	}

	/**
	 * @return the valorQDS
	 */
	public String getValorQDS() {

		return valorQDS;
	}

	/**
	 * @param valorQDS
	 *            the valorQDS to set
	 */
	public void setValorQDS(String valorQDS) {

		this.valorQDS = valorQDS;
	}

	/**
	 * @return the valorQPNR
	 */
	public String getValorQPNR() {

		return valorQPNR;
	}

	/**
	 * @param valorQPNR
	 *            the valorQPNR to set
	 */
	public void setValorQPNR(String valorQPNR) {

		this.valorQPNR = valorQPNR;
	}

	/**
	 * @return the valorQDP
	 */
	public String getValorQDP() {

		return valorQDP;
	}

	/**
	 * @param valorQDP
	 *            the valorQDP to set
	 */
	public void setValorQDP(String valorQDP) {

		this.valorQDP = valorQDP;
	}

	/**
	 * @return the indicadorAceite
	 */
	public Boolean getIndicadorAceite() {

		return indicadorAceite;
	}

	/**
	 * @param indicadorAceite
	 *            the indicadorAceite to set
	 */
	public void setIndicadorAceite(Boolean indicadorAceite) {

		this.indicadorAceite = indicadorAceite;
	}

	/**
	 * Método responsável por retornar a data de
	 * solicitacao no formato dd/MM/yyyy
	 * 
	 * @return
	 */
	public String getDataFormatada() {

		return Util.converterDataParaStringSemHora(this.getDataSolicitacao(), Constantes.FORMATO_DATA_BR);
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public Long getIdModalidade() {

		return idModalidade;
	}

	public void setIdModalidade(Long idModalidade) {

		this.idModalidade = idModalidade;
	}

	public String getComentario() {

		return comentario;
	}

	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	public String getIndicadorTipoProgramacaoConsumo() {

		return indicadorTipoProgramacaoConsumo;
	}

	public void setIndicadorTipoProgramacaoConsumo(String indicadorTipoProgramacaoConsumo) {

		this.indicadorTipoProgramacaoConsumo = indicadorTipoProgramacaoConsumo;
	}
	
	public String getDescricaoModalidade() {
		
		return descricaoModalidade;
	}

	public void setDescricaoModalidade(String descricaoModalidade) {
	
		this.descricaoModalidade = descricaoModalidade;
	}

}
