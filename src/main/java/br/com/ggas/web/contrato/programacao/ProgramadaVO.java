package br.com.ggas.web.contrato.programacao;

/**
 * 
 * @author pedro Classe responsável por trafegar os dados relacionados as telas sobre Parada, Programada, Não Programada, e Falaha de
 *         fornecimento.
 */
public class ProgramadaVO {

	private Long[] chavesPrimarias;
	private Long[] chavesContratos;
	private Long[] unidades;
	private Long[] chavesPontoConsumoParada;
	private String[] pressoes;
	private String[] datasPressao;
	private Long idCliente;
	private Long idTipoParada;
	private Long idContrato;
	private String numeroContrato;
	private String datasProgramadas;
	private String nomeSolicitante;
	private String comentario;
	private String dataAviso;
	private String volumeFornecimento;
	private Boolean falhaFornecimento;
	private Boolean indicadorSolicitante;

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public Long[] getChavesContratos() {
		Long[] chavesContratosTmp = null;
		if (this.chavesContratos != null) {
			chavesContratosTmp = chavesContratos.clone();
		}
		return chavesContratosTmp;
	}

	public void setChavesContratos(Long[] chavesContratosTmp) {
		if (chavesContratosTmp != null) {
			this.chavesContratos = chavesContratosTmp.clone();
		} else {
			this.chavesContratos = null;
		}
	}

	public Long[] getChavesPrimarias() {
		Long[] chavesPrimariasTmp = null;
		if (this.unidades != null) {
			chavesPrimariasTmp = chavesPrimarias.clone();
		}
		return chavesPrimariasTmp;
	}

	public void setChavesPrimarias(Long[] chavesPrimariasTmp) {
		if (chavesPrimariasTmp != null) {
			this.chavesPrimarias = chavesPrimariasTmp.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

	public Long[] getUnidades() {
		Long[] unidadesTmp = null;
		if (this.unidades != null) {
			unidadesTmp = unidades.clone();
		}
		return unidadesTmp;
	}

	public void setUnidades(Long[] unidades) {
		if(unidades != null) {
			this.unidades = unidades.clone();
		} else {
			this.unidades = null;
		}
	}

	public Long[] getChavesPontoConsumoParada() {
		Long[] chavesPontoConsumoParadaTmp = null;
		if (this.chavesPontoConsumoParada != null) {
			chavesPontoConsumoParadaTmp = this.chavesPontoConsumoParada.clone();
		}
		return chavesPontoConsumoParadaTmp;
	}

	public void setChavesPontoConsumoParada(Long[] chavesPontoConsumoParadaTmp) {
		if (chavesPontoConsumoParadaTmp != null) {
			this.chavesPontoConsumoParada = chavesPontoConsumoParadaTmp.clone();
		} else {
			this.chavesPontoConsumoParada = null;
		}
	}

	public String[] getPressoes() {
		String[] pressoesTmp = null;
		if (this.pressoes != null) {
			pressoesTmp = pressoes.clone();
		}
		return pressoesTmp;
	}

	public void setPressoes(String[] pressoes) {
		if (pressoes != null) {
			this.pressoes = pressoes.clone();
		} else {
			this.pressoes = null;
		}
	}

	public String[] getDatasPressao() {
		String[] datasPressaoTmp = null;
		if (this.datasPressao != null) {
			datasPressaoTmp = datasPressao.clone();
		}
		return datasPressaoTmp;
	}

	public void setDatasPressao(String[] datasPressaoTmp) {
		if (datasPressaoTmp != null) {
			this.datasPressao = datasPressaoTmp.clone();
		} else {
			this.datasPressao = null;
		}
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdTipoParada() {
		return idTipoParada;
	}

	public void setIdTipoParada(Long idTipoParada) {
		this.idTipoParada = idTipoParada;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getDatasProgramadas() {
		return datasProgramadas;
	}

	public void setDatasProgramadas(String datasProgramadas) {
		this.datasProgramadas = datasProgramadas;
	}

	public String getNomeSolicitante() {
		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getDataAviso() {
		return dataAviso;
	}

	public void setDataAviso(String dataAviso) {
		this.dataAviso = dataAviso;
	}

	public String getVolumeFornecimento() {
		return volumeFornecimento;
	}

	public void setVolumeFornecimento(String volumeFornecimento) {
		this.volumeFornecimento = volumeFornecimento;
	}

	public Boolean getFalhaFornecimento() {
		return falhaFornecimento;
	}

	public void setFalhaFornecimento(Boolean falhaFornecimento) {
		this.falhaFornecimento = falhaFornecimento;
	}

	public Boolean getIndicadorSolicitante() {
		return indicadorSolicitante;
	}

	public void setIndicadorSolicitante(Boolean indicadorSolicitante) {
		this.indicadorSolicitante = indicadorSolicitante;
	}
}
