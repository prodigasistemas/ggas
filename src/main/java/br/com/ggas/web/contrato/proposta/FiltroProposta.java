package br.com.ggas.web.contrato.proposta;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe responsável por converter os campos pelos quais o usuário pode filtrar as proposta da tela para o modelo
 */
public class FiltroProposta {

	private static final String DATA_INICIO = "dataInicio";

	private static final String DATA_FIM = "dataFim";

	private static final String HABILITADO = "habilitado";

	private String numeroProposta;

	private Date dataInicio;

	private Date dataFim;

	private String descricaoImovel;

	private String matriculaImovel;

	private String nomeCliente;

	private String cpfCnpj;

	private String cepImovel;

	private Boolean habilitado;

	/**
	 * @param form
	 * @throws ParseException
	 */
	public FiltroProposta(Map<String, Object> map) throws ParseException {
		
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");

		numeroProposta = (String) map.get("numeroProposta");

		if (StringUtils.isNotEmpty((String) map.get(DATA_INICIO))) {
			dataInicio = formatador.parse((String) map.get(DATA_INICIO));
		}

		if (StringUtils.isNotEmpty((String) map.get(DATA_FIM))) {
			dataFim = formatador.parse((String) map.get(DATA_FIM));
		}

		descricaoImovel = (String) map.get("descricaoImovel");
		matriculaImovel = (String) map.get("matriculaImovel");
		nomeCliente = (String) map.get("nomeCliente");
		cpfCnpj = (String) map.get("cpfCnpj");
		cepImovel = (String) map.get("cepImovel");
		habilitado =  null;
		
		if (StringUtils.isNotEmpty((String) map.get(HABILITADO))) {
			habilitado = (Boolean) map.get(HABILITADO);
		}
	}

	/**
	 * @return numeroProposta
	 */
	public String getNumeroProposta() {
		return numeroProposta;
	}

	/**
	 * @param numeroProposta
	 */
	public void setNumeroProposta(String numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

	/**
	 * @return dataInicio
	 */
	public Date getDataInicio() {
		Date data = null;
		if(dataInicio != null) {
			data = (Date) dataInicio.clone();
		}
		return data;
	}

	/**
	 * @param dataInicio
	 */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * @return dataFim
	 */
	public Date getDataFim() {
		Date data = null;
		if(dataFim != null) {
			data = (Date) dataFim.clone();
		}
		return data;
	}

	/**
	 * @param dataFim
	 */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * @return descricaoImovel
	 */
	public String getDescricaoImovel() {
		return descricaoImovel;
	}

	/**
	 * @param descricaoImovel
	 */
	public void setDescricaoImovel(String descricaoImovel) {
		this.descricaoImovel = descricaoImovel;
	}

	/**
	 * @return descricaoImovel
	 */
	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	/**
	 * @param matriculaImovel
	 */
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * @param cpfCnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * @return cepImovel
	 */
	public String getCepImovel() {
		return cepImovel;
	}

	/**
	 * @param cepImovel
	 */
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}

	/**
	 * @return habilitado
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}

	/**
	 * @param habilitado
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

}
