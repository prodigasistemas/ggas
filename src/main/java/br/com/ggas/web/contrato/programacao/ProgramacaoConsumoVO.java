package br.com.ggas.web.contrato.programacao;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProgramacaoConsumoVO implements Serializable {

	private static final long serialVersionUID = -4589526457492879536L;
	private String numeroContrato;
	private String dataAtual;
	private String opcao;
	private String  dataSolicitacaoComentario;
	private Long idCliente;
	private Long idCityGate;
	private Long idPontoConsumo;
	private Long chaveModalidade;
	private String[] dataSolicitacao;
	private String[] valorQDC;
	private String[] descricaoMotivoNaoAceite;
	private String[] valorQDS;
	private String[] valorQPNR;
	private String[] valorQDP;
	private Long[] idModalidade;
	private Long[] idPontoConsumoLista;
	private BigDecimal saldoARecuperarAux;
	private Integer ano;
	private Integer mes;
	private Integer anoExibido;
	private Integer mesExibido;
	private String nomeImovel;
	private String descricaoPontoConsumo;
	private Boolean inputModificado;
	private String indicadorPesquisa;
	
	
	public String getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getDataAtual() {
		return dataAtual;
	}
	public void setDataAtual(String dataAtual) {
		this.dataAtual = dataAtual;
	}
	public String getOpcao() {
		return opcao;
	}
	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}
	public String getDataSolicitacaoComentario() {
		return dataSolicitacaoComentario;
	}
	public void setDataSolicitacaoComentario(String dataSolicitacaoComentario) {
		this.dataSolicitacaoComentario = dataSolicitacaoComentario;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdCityGate() {
		return idCityGate;
	}
	public void setIdCityGate(Long idCityGate) {
		this.idCityGate = idCityGate;
	}
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public Long getChaveModalidade() {
		return chaveModalidade;
	}
	public void setChaveModalidade(Long chaveModalidade) {
		this.chaveModalidade = chaveModalidade;
	}
	public String[] getDataSolicitacao() {
		return dataSolicitacao;
	}
	public void setDataSolicitacao(String[] dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}
	public String[] getValorQDC() {
		return valorQDC;
	}
	public void setValorQDC(String[] valorQDC) {
		this.valorQDC = valorQDC;
	}
	public String[] getDescricaoMotivoNaoAceite() {
		return descricaoMotivoNaoAceite;
	}
	public void setDescricaoMotivoNaoAceite(String[] descricaoMotivoNaoAceite) {
		this.descricaoMotivoNaoAceite = descricaoMotivoNaoAceite;
	}
	public String[] getValorQDS() {
		return valorQDS;
	}
	public void setValorQDS(String[] valorQDS) {
		this.valorQDS = valorQDS;
	}
	public String[] getValorQPNR() {
		return valorQPNR;
	}
	public void setValorQPNR(String[] valorQPNR) {
		this.valorQPNR = valorQPNR;
	}
	public String[] getValorQDP() {
		return valorQDP;
	}
	public void setValorQDP(String[] valorQDP) {
		this.valorQDP = valorQDP;
	}
	public Long[] getIdModalidade() {
		return idModalidade;
	}
	public void setIdModalidade(Long[] idModalidade) {
		this.idModalidade = idModalidade;
	}
	public Long[] getIdPontoConsumoLista() {
		return idPontoConsumoLista;
	}
	public void setIdPontoConsumoLista(Long[] idPontoConsumoLista) {
		this.idPontoConsumoLista = idPontoConsumoLista;
	}
	public BigDecimal getSaldoARecuperarAux() {
		return saldoARecuperarAux;
	}
	public void setSaldoARecuperarAux(BigDecimal saldoARecuperarAux) {
		this.saldoARecuperarAux = saldoARecuperarAux;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public Integer getAnoExibido() {
		return anoExibido;
	}
	public void setAnoExibido(Integer anoExibido) {
		this.anoExibido = anoExibido;
	}
	public Integer getMesExibido() {
		return mesExibido;
	}
	public void setMesExibido(Integer mesExibido) {
		this.mesExibido = mesExibido;
	}
	public String getNomeImovel() {
		return nomeImovel;
	}
	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public Boolean getInputModificado() {
		return inputModificado;
	}
	public void setInputModificado(Boolean inputModificado) {
		this.inputModificado = inputModificado;
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
}
