package br.com.ggas.web.contrato.extratoexecucaocontrato;

/**
 * Método responsável pela representações dos valores utilizados 
 * nas operações de Crédito / Débito a Realizar
 * @author esantana
 *
 */
public class ExtratoExecucaoContratoVO {

	private Long idContrato;
	
	private String numeroContrato;
	
	private String dataAssinaturaContrato;
	
	private String dataVencimentoObrigacoesContratuais;
	
	private String nomeCliente;
	
	private String documentoCliente;
	
	private String cnpjCpfCliente;
	
	private String enderecoCliente;
	
	private String emailCliente;
	
	private String[] qdcContrato;

	private Integer tipoConsulta;
	
	private String indicadorPesquisa;
	
	private String dataConsultaInicial;
	
	private String dataConsultaFinal;
	
	private String[] falhasFornecimento;
	
	private String[] paradasProgramada;
	
	private String[] casosFortuitoForcaMaior;
	
	private String[] paradaNaoProgramada;
	
	private String totalVolumeQDC;
	
	private String totalVolumeQDS;
	
	private String totalVolumeQDP;
	
	private String totalVolumeQDR;
	
	private String totalVolumeQR;
	
	private Long contratoModalidade;
	
	private String valorTotalQDC;
	
	private String valorTotalQDR;
	
	private String valorTotalRetiradaMaior;
	
	private String totalPenalidades;
	
	private String totalValorCalculadoRetiradaMaior;
	
	private String valorTotalQDCMenor;
	
	private String valorTotalQDRMenor;
	
	private String valorTotalRetiradaMenor;
	
	private String totalPenalidadesMenor;
	
	private String totalValorCalculadoRetiradaMenor;
	
	private String totalValorCalculadoDOP;
	
	private String totalQDCDOP;
	
	private String totalQDSDOP;
	
	private String totalQDPDOP;
	
	private String totalQNRDOP;
	
	private String totalValorCalculadoGAS;
	
	private String totalQDCGAS;
	
	private String totalQDSGAS;
	
	private String totalQDPGAS;
	
	private String totalQNRGAS;
	
	private String totalQDCTOP;
	
	private String totalQDSTOP;
	
	private String totalQDPTOP;
	
	private String totalQNRTOP;
	
	private String totalQPNRTOP;
	
	private String totalQDCSOP;
	
	private String totalQDSSOP;
	
	private String totalQDPSOP;
	
	private String totalQNRSOP;
	
	private String totalQPNRSOP;
	
	private Long idCliente;
	
	private Long idImovel;
	
	private String nomeFantasiaImovel;
	
	private String matriculaImovel;
	
	private String numeroImovel;
	
	private String cidadeImovel;
	
	private String condominio;
	
	private String habilitado;
	
	private String situacaoContrato;
	
	
	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getCidadeImovel() {
		return cidadeImovel;
	}

	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}

	public String getCondominio() {
		return condominio;
	}

	public void setCondominio(String condominio) {
		this.condominio = condominio;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	public String getTotalQDCSOP() {
		return totalQDCSOP;
	}

	public void setTotalQDCSOP(String totalQDCSOP) {
		this.totalQDCSOP = totalQDCSOP;
	}

	public String getTotalQDSSOP() {
		return totalQDSSOP;
	}

	public void setTotalQDSSOP(String totalQDSSOP) {
		this.totalQDSSOP = totalQDSSOP;
	}

	public String getTotalQDPSOP() {
		return totalQDPSOP;
	}

	public void setTotalQDPSOP(String totalQDPSOP) {
		this.totalQDPSOP = totalQDPSOP;
	}

	public String getTotalQNRSOP() {
		return totalQNRSOP;
	}

	public void setTotalQNRSOP(String totalQNRSOP) {
		this.totalQNRSOP = totalQNRSOP;
	}

	public String getTotalQPNRSOP() {
		return totalQPNRSOP;
	}

	public void setTotalQPNRSOP(String totalQPNRSOP) {
		this.totalQPNRSOP = totalQPNRSOP;
	}

	public String getTotalQDCTOP() {
		return totalQDCTOP;
	}

	public void setTotalQDCTOP(String totalQDCTOP) {
		this.totalQDCTOP = totalQDCTOP;
	}

	public String getTotalQDSTOP() {
		return totalQDSTOP;
	}

	public void setTotalQDSTOP(String totalQDSTOP) {
		this.totalQDSTOP = totalQDSTOP;
	}

	public String getTotalQDPTOP() {
		return totalQDPTOP;
	}

	public void setTotalQDPTOP(String totalQDPTOP) {
		this.totalQDPTOP = totalQDPTOP;
	}

	public String getTotalQNRTOP() {
		return totalQNRTOP;
	}

	public void setTotalQNRTOP(String totalQNRTOP) {
		this.totalQNRTOP = totalQNRTOP;
	}

	public String getTotalQPNRTOP() {
		return totalQPNRTOP;
	}

	public void setTotalQPNRTOP(String totalQPNRTOP) {
		this.totalQPNRTOP = totalQPNRTOP;
	}

	public String getTotalQDSDOP() {
		return totalQDSDOP;
	}

	public void setTotalQDSDOP(String totalQDSDOP) {
		this.totalQDSDOP = totalQDSDOP;
	}

	public String getTotalQDPDOP() {
		return totalQDPDOP;
	}

	public void setTotalQDPDOP(String totalQDPDOP) {
		this.totalQDPDOP = totalQDPDOP;
	}

	public String getTotalQNRDOP() {
		return totalQNRDOP;
	}

	public void setTotalQNRDOP(String totalQNRDOP) {
		this.totalQNRDOP = totalQNRDOP;
	}

	public String getValorTotalQDCMenor() {
		return valorTotalQDCMenor;
	}

	public void setValorTotalQDCMenor(String valorTotalQDCMenor) {
		this.valorTotalQDCMenor = valorTotalQDCMenor;
	}

	public String getValorTotalQDRMenor() {
		return valorTotalQDRMenor;
	}

	public void setValorTotalQDRMenor(String valorTotalQDRMenor) {
		this.valorTotalQDRMenor = valorTotalQDRMenor;
	}

	public String getValorTotalRetiradaMenor() {
		return valorTotalRetiradaMenor;
	}

	public void setValorTotalRetiradaMenor(String valorTotalRetiradaMenor) {
		this.valorTotalRetiradaMenor = valorTotalRetiradaMenor;
	}

	public String getTotalPenalidadesMenor() {
		return totalPenalidadesMenor;
	}

	public void setTotalPenalidadesMenor(String totalPenalidadesMenor) {
		this.totalPenalidadesMenor = totalPenalidadesMenor;
	}

	public String getTotalValorCalculadoRetiradaMenor() {
		return totalValorCalculadoRetiradaMenor;
	}

	public void setTotalValorCalculadoRetiradaMenor(String totalValorCalculadoRetiradaMenor) {
		this.totalValorCalculadoRetiradaMenor = totalValorCalculadoRetiradaMenor;
	}

	public String getTotalVolumeQDC() {
		return totalVolumeQDC;
	}

	public void setTotalVolumeQDC(String totalVolumeQDC) {
		this.totalVolumeQDC = totalVolumeQDC;
	}

	public String getTotalVolumeQDS() {
		return totalVolumeQDS;
	}

	public void setTotalVolumeQDS(String totalVolumeQDS) {
		this.totalVolumeQDS = totalVolumeQDS;
	}

	public String getTotalVolumeQDP() {
		return totalVolumeQDP;
	}

	public void setTotalVolumeQDP(String totalVolumeQDP) {
		this.totalVolumeQDP = totalVolumeQDP;
	}

	public String getTotalVolumeQDR() {
		return totalVolumeQDR;
	}

	public void setTotalVolumeQDR(String totalVolumeQDR) {
		this.totalVolumeQDR = totalVolumeQDR;
	}

	public String getTotalVolumeQR() {
		return totalVolumeQR;
	}

	public void setTotalVolumeQR(String totalVolumeQR) {
		this.totalVolumeQR = totalVolumeQR;
	}

	public String[] getFalhasFornecimento() {
		return falhasFornecimento;
	}

	public void setFalhasFornecimento(String[] falhasFornecimento) {
		this.falhasFornecimento = falhasFornecimento;
	}

	public String[] getParadasProgramada() {
		return paradasProgramada;
	}

	public void setParadasProgramada(String[] paradasProgramada) {
		this.paradasProgramada = paradasProgramada;
	}

	public String[] getCasosFortuitoForcaMaior() {
		return casosFortuitoForcaMaior;
	}

	public void setCasosFortuitoForcaMaior(String[] casosFortuitoForcaMaior) {
		this.casosFortuitoForcaMaior = casosFortuitoForcaMaior;
	}

	public String[] getParadaNaoProgramada() {
		return paradaNaoProgramada;
	}

	public void setParadaNaoProgramada(String[] paradaNaoProgramada) {
		this.paradaNaoProgramada = paradaNaoProgramada;
	}

	public Long getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getDataAssinaturaContrato() {
		return dataAssinaturaContrato;
	}

	public void setDataAssinaturaContrato(String dataAssinaturaContrato) {
		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	public String getDataVencimentoObrigacoesContratuais() {
		return dataVencimentoObrigacoesContratuais;
	}

	public void setDataVencimentoObrigacoesContratuais(String dataVencimentoObrigacoesContratuais) {
		this.dataVencimentoObrigacoesContratuais = dataVencimentoObrigacoesContratuais;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getCnpjCpfCliente() {
		return cnpjCpfCliente;
	}

	public void setCnpjCpfCliente(String cnpjCpfCliente) {
		this.cnpjCpfCliente = cnpjCpfCliente;
	}

	public String getEnderecoCliente() {
		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public Integer getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(Integer tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getDataConsultaInicial() {
		return dataConsultaInicial;
	}

	public void setDataConsultaInicial(String dataConsultaInicial) {
		this.dataConsultaInicial = dataConsultaInicial;
	}

	public String getDataConsultaFinal() {
		return dataConsultaFinal;
	}

	public void setDataConsultaFinal(String dataConsultaFinal) {
		this.dataConsultaFinal = dataConsultaFinal;
	}

	public Long getContratoModalidade() {
		return contratoModalidade;
	}

	public void setContratoModalidade(Long contratoModalidade) {
		this.contratoModalidade = contratoModalidade;
	}

	public String getValorTotalQDC() {
		return valorTotalQDC;
	}

	public void setValorTotalQDC(String valorTotalQDC) {
		this.valorTotalQDC = valorTotalQDC;
	}

	public String getValorTotalQDR() {
		return valorTotalQDR;
	}

	public void setValorTotalQDR(String valorTotalQDR) {
		this.valorTotalQDR = valorTotalQDR;
	}

	public String getValorTotalRetiradaMaior() {
		return valorTotalRetiradaMaior;
	}

	public void setValorTotalRetiradaMaior(String valorTotalRetiradaMaior) {
		this.valorTotalRetiradaMaior = valorTotalRetiradaMaior;
	}

	public String getTotalPenalidades() {
		return totalPenalidades;
	}

	public void setTotalPenalidades(String totalPenalidades) {
		this.totalPenalidades = totalPenalidades;
	}

	public String getTotalValorCalculadoRetiradaMaior() {
		return totalValorCalculadoRetiradaMaior;
	}

	public void setTotalValorCalculadoRetiradaMaior(String totalValorCalculadoRetiradaMaior) {
		this.totalValorCalculadoRetiradaMaior = totalValorCalculadoRetiradaMaior;
	}

	public String getTotalValorCalculadoDOP() {
		return totalValorCalculadoDOP;
	}

	public void setTotalValorCalculadoDOP(String totalValorCalculadoDOP) {
		this.totalValorCalculadoDOP = totalValorCalculadoDOP;
	}

	public String getTotalQDCDOP() {
		return totalQDCDOP;
	}

	public void setTotalQDCDOP(String totalQDCDOP) {
		this.totalQDCDOP = totalQDCDOP;
	}

	public String getTotalValorCalculadoGAS() {
		return totalValorCalculadoGAS;
	}

	public void setTotalValorCalculadoGAS(String totalValorCalculadoGAS) {
		this.totalValorCalculadoGAS = totalValorCalculadoGAS;
	}

	public String getTotalQDCGAS() {
		return totalQDCGAS;
	}

	public void setTotalQDCGAS(String totalQDCGAS) {
		this.totalQDCGAS = totalQDCGAS;
	}

	public String getTotalQDSGAS() {
		return totalQDSGAS;
	}

	public void setTotalQDSGAS(String totalQDSGAS) {
		this.totalQDSGAS = totalQDSGAS;
	}

	public String getTotalQDPGAS() {
		return totalQDPGAS;
	}

	public void setTotalQDPGAS(String totalQDPGAS) {
		this.totalQDPGAS = totalQDPGAS;
	}

	public String getTotalQNRGAS() {
		return totalQNRGAS;
	}

	public void setTotalQNRGAS(String totalQNRGAS) {
		this.totalQNRGAS = totalQNRGAS;
	}

	public String getDocumentoCliente() {
		return documentoCliente;
	}

	public void setDocumentoCliente(String documentoCliente) {
		this.documentoCliente = documentoCliente;
	}

	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

	public String[] getQdcContrato() {
		return qdcContrato;
	}

	public void setQdcContrato(String[] qdcContrato) {
		this.qdcContrato = qdcContrato;
	}
	
	
}
