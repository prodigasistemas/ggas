/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

@XmlRootElement
public class CityGateSolicitacaoConsumoPontoConsumoVO implements Serializable, Comparable<CityGateSolicitacaoConsumoPontoConsumoVO> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2541165115581772456L;

	private String listaDescricaoCityGate;
	
	private Collection<SolicitacaoConsumoPontoConsumoVO> listaSolicitacaoConsumoVO = 
					new ArrayList<SolicitacaoConsumoPontoConsumoVO>();

	private BigDecimal totalQds;

	/**
	 * @return the listaDescricaoCityGate
	 */
	public String getListaDescricaoCityGate() {
	
		return listaDescricaoCityGate;
	}

	
	/**
	 * @param listaDescricaoCityGate the listaDescricaoCityGate to set
	 */
	public void setListaDescricaoCityGate(String listaDescricaoCityGate) {
	
		this.listaDescricaoCityGate = listaDescricaoCityGate;
	}

	
	/**
	 * @return the listaSolicitacaoConsumoVO
	 */
	public Collection<SolicitacaoConsumoPontoConsumoVO> getListaSolicitacaoConsumoVO() {
	
		return listaSolicitacaoConsumoVO;
	}

	
	/**
	 * @param listaSolicitacaoConsumoVO the listaSolicitacaoConsumoVO to set
	 */
	public void setListaSolicitacaoConsumoVO(Collection<SolicitacaoConsumoPontoConsumoVO> listaSolicitacaoConsumoVO) {
	
		this.listaSolicitacaoConsumoVO = listaSolicitacaoConsumoVO;
	}

	
	/**
	 * @return the totalQds
	 */
	public BigDecimal getTotalQds() {
	
		return totalQds;
	}
	
	public String getTotalQdsFormatado() {

		return Util.converterCampoValorParaString(Util.coalescenciaNula(totalQds, BigDecimal.ZERO), Constantes.FORMATO_VALOR_NUMERO_INTEIRO,
				Constantes.LOCALE_PADRAO);
	}

	
	/**
	 * @param totalQds the totalQds to set
	 */
	public void setTotalQds(BigDecimal totalQds) {
	
		this.totalQds = totalQds;
	}
	
	public void adicionarTotalQds(BigDecimal totalQds) {
		if (this.totalQds == null) {
			this.totalQds = BigDecimal.ZERO;
		}
		this.totalQds = this.totalQds.add(totalQds);
	}

	@Override
	public int compareTo(CityGateSolicitacaoConsumoPontoConsumoVO o) {
		CityGateSolicitacaoConsumoPontoConsumoVO cityGateSolicitacaoConsumoPontoConsumoVO = o;
		return this.getListaDescricaoCityGate().compareTo(
						cityGateSolicitacaoConsumoPontoConsumoVO.getListaDescricaoCityGate());
	}
	
	public static CityGateSolicitacaoConsumoPontoConsumoVO buscarObjeto(
					Collection<CityGateSolicitacaoConsumoPontoConsumoVO> listaCityGateSolicitacaoConsumoPontoConsumoVO, 
					String listaDescricaoCityGate) {
		for (CityGateSolicitacaoConsumoPontoConsumoVO cityGateSolicitacaoConsumoPontoConsumoVO : 
						listaCityGateSolicitacaoConsumoPontoConsumoVO) {
			if (cityGateSolicitacaoConsumoPontoConsumoVO.getListaDescricaoCityGate().equals(listaDescricaoCityGate)) {
				return cityGateSolicitacaoConsumoPontoConsumoVO;
			}
		}
		return null;
	}


	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		if(listaDescricaoCityGate == null){
			result = prime * result + 0;
		} else {
			result = prime * result + listaDescricaoCityGate.hashCode();
		}
		
		if(listaSolicitacaoConsumoVO == null){
			result = prime * result + 0;
		} else {
			result = prime * result + listaSolicitacaoConsumoVO.hashCode();
		}
		
		if(totalQds == null){
			result = prime * result + 0;
		} else {
			result = prime * result + totalQds.hashCode();
		}
		
		return result;
	}


	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CityGateSolicitacaoConsumoPontoConsumoVO other = (CityGateSolicitacaoConsumoPontoConsumoVO) obj;
		if (listaDescricaoCityGate == null) {
			if (other.listaDescricaoCityGate != null) {
				return false;
			}
		} else if (!listaDescricaoCityGate.equals(other.listaDescricaoCityGate)) {
			return false;
		}
		if (listaSolicitacaoConsumoVO == null) {
			if (other.listaSolicitacaoConsumoVO != null) {
				return false;
			}
		} else if (!listaSolicitacaoConsumoVO.equals(other.listaSolicitacaoConsumoVO)) {
			return false;
		}
		if (totalQds == null) {
			if (other.totalQds != null) {
				return false;
			}
		} else if (!(totalQds.compareTo(other.totalQds)==0)) {
			return false;
		}
		return true;
	}
	
	
}
