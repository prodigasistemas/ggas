package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.util.Collection;


/**
 * Classe responsável pela representação de dados do faturamento
 *
 */
public class ContratoDadosFaturamentoVO implements Serializable {

	private static final long serialVersionUID = 3871555986666913133L;

	private Long idContrato;
	
	private String nomeCliente;
	
	private String numeroContratoFormatado;
	
	private Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO;
	
	private Boolean faturavel;
	
	/**
	 * @return idContrato
	 */
	public Long getIdContrato() {
		return idContrato;
	}

	/**
	 * @param idContrato
	 */
	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}

	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	/**
	 * @return numeroContratoFormatado
	 */
	public String getNumeroContratoFormatado() {
		return numeroContratoFormatado;
	}

	/**
	 * @param numeroContratoFormatado
	 */
	public void setNumeroContratoFormatado(String numeroContratoFormatado) {
		this.numeroContratoFormatado = numeroContratoFormatado;
	}

	/**
	 * @return listaImovelPontoConsumoVO
	 */
	public Collection<ImovelPontoConsumoVO> getListaImovelPontoConsumoVO() {
		return listaImovelPontoConsumoVO;
	}

	/**
	 * @param listaImovelPontoConsumoVO
	 */
	public void setListaImovelPontoConsumoVO(Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO) {
		this.listaImovelPontoConsumoVO = listaImovelPontoConsumoVO;
	}

	/**
	 * @return the faturavel
	 */
	public Boolean getFaturavel() {
		return faturavel;
	}

	/**
	 * @param faturavel the faturavel to set
	 */
	public void setFaturavel(Boolean faturavel) {
		this.faturavel = faturavel;
	}

}
