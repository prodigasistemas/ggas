/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.contrato.contrato;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 *
 */
public class PontoConsumoContratoVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 9082653997011289437L;

	private long chavePrimaria;

	private boolean isPreenchimentoConcluido;

	private Map<String, Object> abaPrincipal = new HashMap<String, Object>();

	private Map<String, Object> abaDadosTecnicos = new HashMap<String, Object>();

	private Map<String, Object> abaConsumo = new HashMap<String, Object>();

	private Map<String, Object> abaResponsabilidades = new HashMap<String, Object>();

	private Map<String, Object> abaFaturamento = new HashMap<String, Object>();

	private Map<String, Object> abaModalidade = new HashMap<String, Object>();

	/**
	 * @return the isPreenchimentoConcluido
	 */
	public boolean isPreenchimentoConcluido() {

		return isPreenchimentoConcluido;
	}

	/**
	 * @param isPreenchimentoConcluido
	 *            the isPreenchimentoConcluido to
	 *            set
	 */
	public void setPreenchimentoConcluido(boolean isPreenchimentoConcluido) {

		this.isPreenchimentoConcluido = isPreenchimentoConcluido;
	}

	/**
	 * @return the abaPrincipal
	 */
	public Map<String, Object> getAbaPrincipal() {

		return abaPrincipal;
	}

	/**
	 * @param abaPrincipal
	 *            the abaPrincipal to set
	 */
	public void setAbaPrincipal(Map<String, Object> abaPrincipal) {

		this.abaPrincipal = abaPrincipal;
	}

	/**
	 * @return the abaDadosTecnicos
	 */
	public Map<String, Object> getAbaDadosTecnicos() {

		return abaDadosTecnicos;
	}

	/**
	 * @param abaDadosTecnicos
	 *            the abaDadosTecnicos to set
	 */
	public void setAbaDadosTecnicos(Map<String, Object> abaDadosTecnicos) {

		this.abaDadosTecnicos = abaDadosTecnicos;
	}

	/**
	 * @return the abaConsumo
	 */
	public Map<String, Object> getAbaConsumo() {

		return abaConsumo;
	}

	/**
	 * @param abaConsumo
	 *            the abaConsumo to set
	 */
	public void setAbaConsumo(Map<String, Object> abaConsumo) {

		this.abaConsumo = abaConsumo;
	}

	/**
	 * @return the abaResponsabilidades
	 */
	public Map<String, Object> getAbaResponsabilidades() {

		return abaResponsabilidades;
	}

	/**
	 * @param abaResponsabilidades
	 *            the abaResponsabilidades to set
	 */
	public void setAbaResponsabilidades(Map<String, Object> abaResponsabilidades) {

		this.abaResponsabilidades = abaResponsabilidades;
	}

	/**
	 * @return the chavePrimaria
	 */
	public long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            the chavePrimaria to set
	 */
	public void setChavePrimaria(long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return the abaFaturamento
	 */
	public Map<String, Object> getAbaFaturamento() {

		return abaFaturamento;
	}

	/**
	 * @param abaFaturamento
	 *            the abaFaturamento to set
	 */
	public void setAbaFaturamento(Map<String, Object> abaFaturamento) {

		this.abaFaturamento = abaFaturamento;
	}

	/**
	 * @return the abaModalidade
	 */
	public Map<String, Object> getAbaModalidade() {

		return abaModalidade;
	}

	/**
	 * @param abaModalidade
	 *            the abaModalidade to set
	 */
	public void setAbaModalidade(Map<String, Object> abaModalidade) {

		this.abaModalidade = abaModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj){ 
			return true;
		}
		if(!(obj instanceof PontoConsumoContratoVO)) {
			return false;
		}
		if(this.getChavePrimaria() > 0) {
			PontoConsumoContratoVO that = (PontoConsumoContratoVO) obj;
			return new EqualsBuilder().append(this.getChavePrimaria(), that.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

}
