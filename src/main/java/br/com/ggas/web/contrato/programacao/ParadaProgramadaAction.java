/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contrato.programacao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas as paradas programadas
 */
@Controller
public class ParadaProgramadaAction extends GenericAction {

	private static final String LISTA_CONTRATOS = "listaContratos";

	private static final String TIPO_PARADA_DESCRICAO = "tipoParadaDescricao";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_UNIDADE_PRESSAO = "listaUnidadePressao";

	private static final String VOLUME_FORNECIMENTO = "Volume Referência";
	
	private static final long CHAVE_PARADA_NAO_PROGRAMA = 72;
	
	private static final long CHAVE_PARADA_CASO_FORTUITO = 73;
	
	private static final long CHAVE_FALHA_FORNECIMENTO = 74;
	
	@Autowired
	private ControladorProgramacao controladorProgramacao;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorCliente controladorCliente;
	/**
	 * Método responsável por exibir a tela de pesquisa de ponto de consumo.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaParadaProgramada")
	public String exibirPesquisaParadaProgramada(ProgramadaVO programadaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String view = "exibirPesquisaParadaProgramada";
		EntidadeConteudo tipoParada = null;
		model.addAttribute("programadaVO", programadaVO);
		try {
			if (programadaVO.getIdTipoParada() != null) {
				tipoParada = controladorProgramacao.validarTipoParada(programadaVO.getIdTipoParada());
				model.addAttribute(TIPO_PARADA_DESCRICAO, tipoParada.getDescricao());
			}

			request.getSession().setAttribute("somaQDCPontoConsumo", null);

			if ((programadaVO.getIdTipoParada() != null) && (programadaVO.getIdTipoParada() == CHAVE_FALHA_FORNECIMENTO)) {
				view = "exibirPesquisaParadaProgramada";
			} else if ((programadaVO.getIdTipoParada() != null)
					&& (programadaVO.getIdTipoParada() == CHAVE_PARADA_NAO_PROGRAMA)) {
				view = "exibirPesquisaParadaNaoProgramada";
			} else if ((programadaVO.getIdTipoParada() != null)
					&& (programadaVO.getIdTipoParada() == CHAVE_PARADA_CASO_FORTUITO)) {
				view = "exibirPesquisaParadaCasoFortuitoForcaMaior";
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por pesquisar os imóveis para parada programada pelo cliente.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 */
	@RequestMapping("pesquisarPontosConsumoParadaProgramada")
	public String pesquisarPontosConsumoParadaProgramada(ProgramadaVO programadaVO, BindingResult result, HttpServletRequest request,
			Model model) throws NegocioException, FormatoInvalidoException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute("programadaVO", programadaVO);

		try {
			if ((programadaVO.getIdCliente() != null) && (programadaVO.getIdCliente() > 0)) {
				filtro.put("idCliente", programadaVO.getIdCliente());
			}
			
			if (!StringUtils.isEmpty(programadaVO.getNumeroContrato())) {
				filtro.put(NUMERO_CONTRATO,
						Util.converterCampoStringParaValorInteger("Número do Contrato", programadaVO.getNumeroContrato()));
			}

			Collection<Contrato> listaContratos = new ArrayList<Contrato>();
			if (!filtro.isEmpty()) {
				listaContratos = controladorContrato.consultarContratoParadaProgramada(filtro);
			}

			if ((listaContratos != null) && (!listaContratos.isEmpty())) {
				request.setAttribute(LISTA_CONTRATOS, listaContratos);
			} else {
				super.criarColecaoPaginada(request, filtro, listaContratos);
				request.setAttribute(LISTA_CONTRATOS, listaContratos);
			}
			
			Cliente cliente = controladorCliente.obterCliente(programadaVO.getIdCliente(), Cliente.PROPRIEDADES_LAZY);
			model.addAttribute("cliente", cliente);
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		} catch (FormatoInvalidoException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaParadaProgramada(programadaVO, result, request, model);
	}

	/**
	 * Método responsável por exibir a página de manter parada programada de ponto de consumo.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirManutencaoParadaProgramada")
	public String exibirManutencaoParadaProgramada(ProgramadaVO programadaVO, BindingResult result,
			@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "exibirManutencaoParadaProgramada";
		model.addAttribute("programadaVO", programadaVO);
		
		try {
			carregarCampos(programadaVO, chavesPrimarias, model);
			

			if ((programadaVO.getIdTipoParada() != null) && (programadaVO.getIdTipoParada() == CHAVE_FALHA_FORNECIMENTO)) {
				view = "exibirManutencaoParadaProgramada";
			} else if ((programadaVO.getIdTipoParada() != null)
					&& (programadaVO.getIdTipoParada() == CHAVE_PARADA_NAO_PROGRAMA)) {
				view = "exibirManutencaoParadaNaoProgramada";
			} else if ((programadaVO.getIdTipoParada() != null)
					&& (programadaVO.getIdTipoParada() == CHAVE_PARADA_CASO_FORTUITO)) {
				view = "exibirManutencaoParadaCasoFortuitoForcaMaior";
			}
			saveToken(request);
		} catch (NegocioException e) {
			super.mensagemErroParametrizado(model, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por incluir uma parada programada de ponto de consumo.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @param chavesPrimarias {@link Long}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws GGASException {@link GGASException}
	 * @throws ParseException {@link ParseException}
	 */
	@RequestMapping("incluirParadaProgramada")
	public String incluirParadaProgramada(ProgramadaVO programadaVO, BindingResult result, HttpServletRequest request,
			Long[] chavesPrimarias, Model model)
			throws GGASException, ParseException {

		model.addAttribute("programadaVO", programadaVO);
		try {
			Calendar calendar = Calendar.getInstance();
			model.addAttribute("anoAtual", calendar.get(Calendar.YEAR));
			validarToken(request);

			String[] listaDatasParadas = null;

			if (!StringUtils.isEmpty(programadaVO.getDatasProgramadas())) {
				listaDatasParadas = programadaVO.getDatasProgramadas().split(";");
			}
			
			int[] status = controladorProgramacao.inserirParadasProgramadas(chavesPrimarias, listaDatasParadas,
					obterUsuario(request), getDadosAuditoria(request), programadaVO.getNomeSolicitante(), programadaVO.getComentario(),
					programadaVO.getIdTipoParada(), programadaVO.getPressoes(), programadaVO.getDatasPressao(), programadaVO.getUnidades(),
					programadaVO.getChavesPontoConsumoParada(), programadaVO.getDataAviso(), programadaVO.getVolumeFornecimento());
			if (status[1] > 0) {
				mensagemSucesso(model, ControladorProgramacao.SUCESSO_INCLUSAO_PARADA_PROGRAMADA_EXISTENTE,
						new Object[] { status[0], status[1] });
			} else {
				if (status[0] == 0) {
					mensagemSucesso(model, ControladorProgramacao.SUCESSO_ALTERACAO_PARADA_PROGRAMADA);
				} else {
					mensagemSucesso(model, ControladorProgramacao.SUCESSO_INCLUSAO_PARADA_PROGRAMADA, status[0]);
				}
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarPontosConsumoParadaProgramada(programadaVO, result, request, model);
	}

	/**
	 * Método responsável por incluir uma parada de falha de fornecimento de ponto de consumo.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws ParseException {@link ParseException}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirParadaFalhaFornecimento")
	public String incluirParadaFalhaFornecimento(ProgramadaVO programadaVO, BindingResult result,
			HttpServletRequest request, Long[] chavesPrimarias, Model model) throws ParseException, GGASException {

		String[] listaDatasParadas = null;
		String view;
		model.addAttribute("programadaVO", programadaVO);
		try {
			
			Util.isDataValida(programadaVO.getDataAviso(), Constantes.FORMATO_DATA_BR);
			if (!StringUtils.isEmpty(programadaVO.getDatasProgramadas())) {
				listaDatasParadas = programadaVO.getDatasProgramadas().split(";");
			}

			int[] status = controladorProgramacao.inserirParadasProgramadas(chavesPrimarias, listaDatasParadas,
					obterUsuario(request), getDadosAuditoria(request), programadaVO.getNomeSolicitante(),
					programadaVO.getComentario(), programadaVO.getIdTipoParada(), programadaVO.getPressoes(),
					programadaVO.getDatasPressao(), programadaVO.getUnidades(),
					programadaVO.getChavesPontoConsumoParada(), programadaVO.getDataAviso(),
					programadaVO.getVolumeFornecimento());

			if (status[1] > 0) {
				mensagemSucesso(model, ControladorProgramacao.SUCESSO_INCLUSAO_PARADA_PROGRAMADA_EXISTENTE,
						new Object[] { status[0], status[1] });
			} else {
				if (status[0] == 0) {
					mensagemSucesso(model, ControladorProgramacao.SUCESSO_ALTERACAO_PARADA_PROGRAMADA);
				} else {
					mensagemSucesso(model, ControladorProgramacao.SUCESSO_INCLUSAO_PARADA_PROGRAMADA, status[0]);
				}
			}
			view = pesquisarPontosConsumoParadaProgramada(programadaVO, result, request, model);
		} catch (ParseException e) {
			view = exibirManutencaoParadaProgramada(programadaVO, result, chavesPrimarias, request, model);
			mensagemErroParametrizado(model, request, new GGASException(e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirManutencaoParadaProgramada(programadaVO, result, chavesPrimarias, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por atualizar os dados sobre as datas.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @param chavesPrimarias {@link Long}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("atualizarParadaProgramada")
	public String atualizarParadaProgramada(ProgramadaVO programadaVO, BindingResult result, HttpServletRequest request,
			Long[] chavesPrimarias, Model model)
			throws GGASException {

		String[] listaDatasParadas = null;
		model.addAttribute("programadaVO", programadaVO);
		try {
			if (!StringUtils.isEmpty(programadaVO.getDatasProgramadas())) {
				listaDatasParadas = programadaVO.getDatasProgramadas().split(";");
			}

			Collection<ParadaProgramadaVO> listaParadaProgramadaVO = controladorProgramacao.manterParadasProgramadas(
					chavesPrimarias, listaDatasParadas, obterUsuario(request), getDadosAuditoria(request),
					programadaVO.getNomeSolicitante(), programadaVO.getComentario(), programadaVO.getIdTipoParada(),
					programadaVO.getDataAviso(), programadaVO.getVolumeFornecimento());

			carregarCamposAtualizacao(programadaVO, listaParadaProgramadaVO, model, chavesPrimarias);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirManutencaoParadaProgramada(programadaVO, result, chavesPrimarias, request, model);
	}

	/**
	 * Montar dados pressao.
	 * 
	 * @param listaParadaProgramadaVONovas {@link Collection}
	 * @param pressoes {@link String}
	 * @param datasPressao {@link String}
	 * @param unidadesPressao {@link Long}
	 * @param chavesPontoConsumoParada {@link Long}
	 */
	private void montarDadosPressao(Collection<ParadaProgramadaVO> listaParadaProgramadaVONovas, String[] pressoes, String[] datasPressao,
			Long[] unidadesPressao, Long[] chavesPontoConsumoParada) {

		List<String> listaDatasPressao = Arrays.asList(datasPressao);

		for (ParadaProgramadaVO paradaProgramadaVO : listaParadaProgramadaVONovas) {
			if (listaDatasPressao.contains(paradaProgramadaVO.getDataParada())) {
				for (int i = 0; i < datasPressao.length; i++) {
					if ((datasPressao[i].equals(paradaProgramadaVO.getDataParada()))
							&& (paradaProgramadaVO.getPontoConsumo().getChavePrimaria() == chavesPontoConsumoParada[i])) {
						paradaProgramadaVO.setMenorPressao(pressoes[i]);
						paradaProgramadaVO.setUnidadeMenorPressao(String.valueOf(unidadesPressao[i]));
					}
				}
			}
		}

	}

	/**
	 * Carregar campos atualizacao.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param listaParadaProgramadaVONovas {@link Collection}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCamposAtualizacao(ProgramadaVO programadaVO, Collection<ParadaProgramadaVO> listaParadaProgramadaVONovas,
			Model model, Long[] chavesPrimarias) throws GGASException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Calendar calendar = Calendar.getInstance();
			model.addAttribute("anoAtual", calendar.get(Calendar.YEAR));
			
			EntidadeConteudo tipoParada = controladorProgramacao.validarTipoParada(programadaVO.getIdTipoParada());
			Collection<ContratoPontoConsumo> listaContratoPonto =
					controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(chavesPrimarias);
			if (tipoParada.getChavePrimaria() == CHAVE_FALHA_FORNECIMENTO) {
				programadaVO.setFalhaFornecimento(true);
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<PontoConsumo> pontosConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);

			model.addAttribute(TIPO_PARADA_DESCRICAO, tipoParada.getDescricao());
			model.addAttribute("listaContratoPonto", exibirListaContratoPonto(listaContratoPonto));
			model.addAttribute("pontosConsumo", pontosConsumo);
			model.addAttribute("intervaloAnosData", intervaloAnosData());
			model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());
			
			Collection<ParadaProgramada> paradasProgramadasRepetidas = controladorProgramacao
					.consultarParadasProgramadasEmComum(chavesPrimarias, programadaVO.getIdTipoParada());
			
			Collection<ParadaProgramada> paradasProgramadas = controladorProgramacao.listarParadasNaoRepetidas(paradasProgramadasRepetidas);
			paradasProgramadas.addAll(montarParadasForm(programadaVO, chavesPrimarias, model));
			List<ParadaProgramadaVO> paradasProgramadasVO = new ArrayList<ParadaProgramadaVO>();
			ParadaProgramadaVO paradaProgramadaVO = null;

			for (ParadaProgramada parada : paradasProgramadas) {
				paradaProgramadaVO = new ParadaProgramadaVO();

				calendar.setTime(parada.getDataParada());

				paradaProgramadaVO.setAno(String.valueOf(calendar.get(Calendar.YEAR)));
				paradaProgramadaVO.setMes(String.valueOf(calendar.get(Calendar.MONTH)));
				paradaProgramadaVO.setDia(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
				paradaProgramadaVO.setNomeSolicitante(parada.getNomeSolicitante());
				if(parada.getDataParada() != null) {
					paradaProgramadaVO.setDataParada(Util.converterDataParaStringSemHora(parada.getDataParada(), Constantes.FORMATO_DATA_BR));
				}
				if(parada.getDataAviso() != null) {
					paradaProgramadaVO.setDataAviso(Util.converterDataParaStringSemHora(parada.getDataAviso(), Constantes.FORMATO_DATA_BR));
				}
				paradaProgramadaVO.setVolumeFornecimento(Util.converterCampoValorDecimalParaString("Volume Fornecimento",
						parada.getVolumeFornecimento(), Constantes.LOCALE_PADRAO, 0));
				paradaProgramadaVO.setComentario(parada.getComentario());
				if (parada.getMenorPressao() != null) {
					paradaProgramadaVO.setMenorPressao(Util.converterCampoValorDecimalParaString("Menor Pressão", parada.getMenorPressao(),
							Constantes.LOCALE_PADRAO, 4));
				}
				if (parada.getUnidadeMenorPressao() != null) {
					paradaProgramadaVO.setUnidadeMenorPressao(String.valueOf(parada.getUnidadeMenorPressao().getChavePrimaria()));
				}
				paradaProgramadaVO.setPontoConsumo(parada.getPontoConsumo());
				paradasProgramadasVO.add(paradaProgramadaVO);
			}
			paradasProgramadasVO.addAll(listaParadaProgramadaVONovas);

			ordernarListaParadasProgramadasVOPeloMes(paradasProgramadasVO);

			if (programadaVO.getDatasPressao() != null) {
				this.montarDadosPressao(paradasProgramadasVO, programadaVO.getPressoes(), programadaVO.getDatasPressao(),
						programadaVO.getUnidades(), programadaVO.getChavesPontoConsumoParada());
			}

			model.addAttribute("paradasProgramadas", paradasProgramadasVO);
		}
		model.addAttribute("programadaVO", programadaVO);
	}

	/**
	 * Carregar campos.
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCampos(ProgramadaVO programadaVO, Long[] chavesPrimarias, Model model) throws GGASException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Calendar calendar = Calendar.getInstance();
			model.addAttribute("anoAtual", calendar.get(Calendar.YEAR));
			
			EntidadeConteudo tipoParada = controladorProgramacao.validarTipoParada(programadaVO.getIdTipoParada());
			Collection<ContratoPontoConsumo> listaContratoPonto =
					controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(chavesPrimarias);
			if (tipoParada.getChavePrimaria() == CHAVE_FALHA_FORNECIMENTO) {
				programadaVO.setFalhaFornecimento(true);
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<PontoConsumo> pontosConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);
			Collection<ParadaProgramada> paradasProgramadasRepetidas = controladorProgramacao
					.consultarParadasProgramadasEmComum(chavesPrimarias, programadaVO.getIdTipoParada());
			Collection<ParadaProgramada> paradasProgramadas = controladorProgramacao.listarParadasNaoRepetidas(paradasProgramadasRepetidas);
			paradasProgramadas.addAll(montarParadasForm(programadaVO, chavesPrimarias, model));

			
			model.addAttribute(TIPO_PARADA_DESCRICAO, tipoParada.getDescricao());
			model.addAttribute("pontosConsumo", pontosConsumo);
			model.addAttribute("intervaloAnosData", intervaloAnosData());
			model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());
			model.addAttribute("listaContratoPonto", exibirListaContratoPonto(listaContratoPonto));
			
			List<ParadaProgramadaVO> paradasProgramadasVO = new ArrayList<ParadaProgramadaVO>();
			ParadaProgramadaVO paradaProgramadaVO = null;

			for (ParadaProgramada parada : paradasProgramadas) {
				paradaProgramadaVO = new ParadaProgramadaVO();

				calendar.setTime(parada.getDataParada());

				paradaProgramadaVO.setAno(String.valueOf(calendar.get(Calendar.YEAR)));
				paradaProgramadaVO.setMes(String.valueOf(calendar.get(Calendar.MONTH)));
				paradaProgramadaVO.setDia(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
				paradaProgramadaVO.setNomeSolicitante(parada.getNomeSolicitante());
				paradaProgramadaVO.setDataParada(Util.converterDataParaStringSemHora(parada.getDataParada(), Constantes.FORMATO_DATA_BR));
				paradaProgramadaVO.setComentario(parada.getComentario());
				if (parada.getDataAviso() != null) {
					paradaProgramadaVO.setDataAviso(Util.converterDataParaStringSemHora(parada.getDataAviso(), Constantes.FORMATO_DATA_BR));
				}
				if (parada.getVolumeFornecimento() != null) {
					paradaProgramadaVO.setVolumeFornecimento(Util.converterCampoValorDecimalParaString("Volume Fornecimento",
							parada.getVolumeFornecimento(), Constantes.LOCALE_PADRAO, 0));
				}
				if (parada.getMenorPressao() != null) {
					paradaProgramadaVO.setMenorPressao(Util.converterCampoValorDecimalParaString("Menor Pressão", parada.getMenorPressao(),
							Constantes.LOCALE_PADRAO, 8));
				}
				if (parada.getUnidadeMenorPressao() != null) {
					paradaProgramadaVO.setUnidadeMenorPressao(String.valueOf(parada.getUnidadeMenorPressao().getChavePrimaria()));
				}
				paradaProgramadaVO.setPontoConsumo(parada.getPontoConsumo());
				paradasProgramadasVO.add(paradaProgramadaVO);
			}

			ordernarListaParadasProgramadasVOPeloMes(paradasProgramadasVO);

			model.addAttribute("paradasProgramadas", paradasProgramadasVO);
		}
		model.addAttribute("programadaVO", programadaVO);

	}

	/**
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @return Collection {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	private Collection<ParadaProgramada> montarParadasForm(ProgramadaVO programadaVO, Long[] chavesPrimarias, Model model)
			throws GGASException {
		Collection<ParadaProgramada> paradasForm = new HashSet<ParadaProgramada>();
		String[] arrDatas = StringUtils.split(programadaVO.getDatasProgramadas(), ';');

		if (arrDatas != null) {
			for (String strData : arrDatas) {
				ParadaProgramada paradaProgramada = (ParadaProgramada) controladorProgramacao.criar();
				
				paradaProgramada.setDataParada(DataUtil.converterParaData(strData));
				paradaProgramada.setNomeSolicitante(programadaVO.getNomeSolicitante());
				paradaProgramada.setComentario(programadaVO.getComentario());
				paradaProgramada.setVolumeFornecimento(
						NumeroUtil.converterCampoStringParaValorBigDecimal(VOLUME_FORNECIMENTO, programadaVO.getVolumeFornecimento()));
				paradaProgramada.setPontoConsumo(controladorPontoConsumo.obterPontoConsumo((chavesPrimarias)[0]));
				if (StringUtils.isNotEmpty(programadaVO.getDataAviso())) {
					try {
						paradaProgramada.setDataAviso(Util.converterCampoStringParaData("Data de Aviso", programadaVO.getDataAviso(),
								Constantes.FORMATO_DATA_BR));
					} catch (GGASException e) {
						mensagemErroParametrizado(model, e);
					}
				}
				paradasForm.add(paradaProgramada);
			}
		}

		return paradasForm;
	}

	/**
	 * Exibir lista contrato ponto.
	 * 
	 * @param listaContratoPonto {@link Collection}
	 * @return List {@link List}
	 */
	private List<Map<String, Object>> exibirListaContratoPonto(Collection<ContratoPontoConsumo> listaContratoPonto) {

		List<Map<String, Object>> contratosPonto = new ArrayList<Map<String, Object>>();

		if (listaContratoPonto != null && !listaContratoPonto.isEmpty()) {
			for (ContratoPontoConsumo contratoPonto : listaContratoPonto) {
				Map<String, Object> dados = new HashMap<String, Object>();

				dados.put("chaveCliente", contratoPonto.getContrato().getClienteAssinatura().getChavePrimaria());
				dados.put(NUMERO_CONTRATO, contratoPonto.getContrato().getNumeroFormatado());
				dados.put("nomeImovel", contratoPonto.getPontoConsumo().getImovel().getNome());
				dados.put("pontoConsumo", contratoPonto.getPontoConsumo().getDescricao());

				contratosPonto.add(dados);
			}
		}

		return contratosPonto;
	}

	/**
	 * Intervalo anos data.
	 * 
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	/**
	 * Ordernar lista paradas programadas vo pelo mes.
	 * 
	 * @param listaParadasProgramadasVO {@link List}
	 */
	private void ordernarListaParadasProgramadasVOPeloMes(List<ParadaProgramadaVO> listaParadasProgramadasVO) {

		if (!listaParadasProgramadasVO.isEmpty()) {
			Collections.sort(listaParadasProgramadasVO, new Comparator<ParadaProgramadaVO>() {

				@Override
				public int compare(ParadaProgramadaVO o1, ParadaProgramadaVO o2) {

					return Integer.valueOf(o1.getMes()).compareTo(Integer.valueOf(o2.getMes()));
				}
			});

			Collections.sort(listaParadasProgramadasVO, new Comparator<ParadaProgramadaVO>() {

				@Override
				public int compare(ParadaProgramadaVO o1, ParadaProgramadaVO o2) {

					return Integer.valueOf(o1.getDia()).compareTo(Integer.valueOf(o2.getDia()));
				}
			});
		}
	}

	/**
	 * Método responsável por remover os pontos de consumo de um cliente na tela de pesquisa.
	 * 
	 * 
	 * @param programadaVO {@link ProgramadaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerClientePesquisaParadaProgramada")
	public String removerClientePesquisaParadaProgramada(ProgramadaVO programadaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			if ((programadaVO.getIdContrato() != null) && (programadaVO.getIdContrato() > 0) && (programadaVO.getChavesContratos() != null)
					&& (programadaVO.getChavesContratos().length > 0)
					&& (ArrayUtils.contains(programadaVO.getChavesContratos(), programadaVO.getIdContrato()))) {
				programadaVO.setChavesContratos((Long[]) ArrayUtils.remove(programadaVO.getChavesContratos(),
						ArrayUtils.indexOf(programadaVO.getChavesContratos(), programadaVO.getIdContrato())));
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			if ((programadaVO.getChavesContratos() != null) && (programadaVO.getChavesContratos().length > 0)) {
				filtro.put(CHAVES_PRIMARIAS, programadaVO.getChavesContratos());

				Long idSituacaoAtivo =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));

				filtro.put("idSituacaoContrato", new Long[] { idSituacaoAtivo });

				Collection<Contrato> contratos = controladorContrato.consultarContratos(filtro);
				// TODO consulta está retornando
				// contratos repetidos. Ajustar
				// consulta.
				Set<Contrato> listaContratos = new HashSet<Contrato>();
				listaContratos.addAll(contratos);
				model.addAttribute(LISTA_CONTRATOS, listaContratos);
			}
		} catch (NumberFormatException e) {
			mensagemErroParametrizado(model, request, new GGASException(e));
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return exibirPesquisaParadaProgramada(programadaVO, result, request, model);
	}
	
	/**
	 * Metodo para limpar tela
	 * @param programadaVO - {@link ProgramadaVO}
	 * @param result - {@link BlidingResult}
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException the GGAS Exception
	 */
	@RequestMapping("limparTela")
	public String limpar(ProgramadaVO programadaVO, BindingResult result,
			@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {
			programadaVO.setVolumeFornecimento(null);
			programadaVO.setPressoes(null);
			programadaVO.setUnidades(null);
			
		return exibirManutencaoParadaProgramada(programadaVO, result, chavesPrimarias, request, model);
	}
	
	/**
	 * Responsável por redefinir o tratamento de {@link String} contendo decimais separados por vírgula.
	 * 
	 * @param binder - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}
}
