/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.web.parametrosistema;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.impl.TarifaImpl;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Controlador responsável pela tela de Parametros do sistema
 *
 */
@Controller
public class ParametroSistemaAction extends GenericAction {

	private static final String REINICIAR_SEQ_NUM_CONTRATO_ANUAL_CONTRATO = "reiniciarSeqNumContratoAnualContrato";

	private static final String ID_TARIFA_REC_COMPROMISSO_CONTRATO = "idTarifaRecCompromissoContrato";

	private static final String OBS_IMPRESSA_NOTA_DEBITO_CONTRATO = "obsImpressaNotaDebitoContrato";

	private static final String ID_PERIODICIDADE_CONTRATO = "idPeriodicidadeContrato";

	private static final String DIAS_ALERTA_RENOVACAO_CONTRATUAL_CONTRATO = "diasAlertaRenovacaoContratualContrato";

	private static final String SEQ_NUM_CONTRATO = "seqNumContrato";

	private static final String ANO_NOVOS_CONTRATOS = "anoNovosContratos";

	private static final String MOTIVO_FIM_RELACIONAMENTO_CLI_ENCERRAR_CONTRATO = "motivoFimRelacionamentoCliEncerrarContrato";

	private static final String DIAS_EMAIL_CHAMADOS_ATRASADOS = "diasEmailChamadosAtrasados";

	private static final String ENVIA_PROPOSTA_EMAIL = "enviaPropostaEmail";

	private static final String ENVIA_SMS = "enviaSms";

	private static final String PROXY_PORTA_SMS = "proxyPortaSms";

	private static final String PROXY_HOST_SMS = "proxyHostSms";

	private static final String MENSAGEM_SMS = "mensagemSms";

	private static final String USUARIO_SMS = "usuarioSms";

	private static final String PROJETO_SMS = "projetoSms";

	private static final String CREDENCIAL_SMS = "credencialSms";

	private static final String TAMANHO_MAXIMO_ANEXO = "tamanhoMaximoAnexo";

	private static final String QUANTIDADE_ANOS_VIGENCIA_FERIADO_GERAL = "quantidadeAnosVigenciaFeriadoGeral";

	private static final String REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO_GERAL = "referenciaIntegracaoNotaTitulo";

	private static final String TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO = "tempoRefreshControleProcesso";

	private static final String LOCAL_HOST_GERAL = "localHostGeral";

	private static final String ASSINATURA_EMAIL = "assinaturaEmail";

	private static final String SERVIDOR_SMTP_AUTH_GERAL = "servidorSmtpAuthGeral";

	private static final String EMAIL_REMETENTE_DEFAULT_GERAL = "emailRemetenteDefaultGeral";

	private static final String SERVIDOR_SMTP_PASSAWORD_GERAL = "servidorSmtpPassawordGeral";

	private static final String SERVIDOR_SMTP_USER_GERAL = "servidorSmtpUserGeral";

	private static final String SERVIDOR_SMTP_PORT_GERAL = "servidorSmtpPortGeral";

	private static final String SERVIDOR_SMTP_HOST_GERAL = "servidorSmtpHostGeral";
	
	private static final String SERVIDOR_EMAIL_CONFIGURADO = "servidorEmailConfigurado";

	private static final String SERVIDOR_AUTENTICACAO_AD_HOST = "servidorAutenticacaoAdHostGeral";

	private static final String SERVIDOR_AUTENTICACAO_AD_PORTA = "servidorAutenticacaoAdPortaGeral";

	private static final String SERVIDOR_AUTENTICACAO_AD_DOMINIO = "servidorAutenticacaoAdDominioGeral";

	private static final String AUTENTICACAO_POR_AD_WINDOWS = "autenticacaoPorAD";

	private static final String CAIXA_ALTA = "caixaAlta";

	private static final String ACEITA_CARACTER_ESPECIAL = "aceitaCaracterEspecial";

	private static final String PERIODICIDADE_SENHA_RENOVACAO_GERAL = "periodicidadeSenhaRenovacaoGeral";

	private static final String TAM_MAX_SENHA_GERAL = "tamMaxSenhaGeral";

	private static final String TAM_MIN_SENHA_GERAL = "tamMinSenhaGeral";

	private static final String CARACTERE_MAIUSCULO_GERAL = "caractereMaiusculoGeral";

	private static final String CARACTERE_MINUSCULO_GERAL = "caractereMinusculoGeral";

	private static final String CARACTERE_NUM_GERAL = "caractereNumGeral";

	private static final String QTD_DECIM_APRESENT_CONSUMO_APURADO = "qtdDecimApresentConsumoApurado";

	private static final String QTD_ESCALA_CALCULO_GERAL = "qtdEscalaCalculoGeral";

	private static final String QTD_ANOS_CALENDARIO_GERAL = "qtdAnosCalendarioGeral";

	private static final String NOME_EMPRESA_GERAL = "nomeEmpresaGeral";

	private static final String MUNICIPIO_IMPLANTADO_GERAL = "municipioImplantadoGeral";

	private static final String HORA_INICIO_DIA_GERAL = "horaInicioDiaGeral";

	private static final String DIRETORIO_RELATORIO_PDF = "diretorioRelatorioPDF";

	private static final String SEQUENCIA_PROPOSTA = "sequenciaProposta";

	private static final String CONTATO_IMOVEL_OBRIGATORIO = "contatoImovelObrigatorio";

	private static final String EXIGE_EMAIL_PRINCIPAL_PESSOA = "exigeEmailPrincipalPessoa";

	private static final String EXIGE_MATRICULA_FUNCIONARIO = "exigeMatriculaFuncionario";

	private static final String ACEITA_INCLUSAO_PONTO_CONSUMO_ROTA = "aceitaInclusaoPontoConsumoRota";

	private static final String CONSIDERAR_PERIODO_TESTES_CONTRATO = "considerarPeriodoTestesContrato";

	private static final String MASCARA_INSCRICAO_ESTADUAL_CADASTRO = "mascaraInscricaoEstadualCadastro";

	private static final String TIPO_ENDERECO_CORRESPONDENCIA_CADASTRO = "tipoEnderecoCorrespondenciaCadastro";

	private static final String UNIDADE_FEDERACAO_IMPLANTACAO_CADASTRO = "unidadeFederacaoImplantacaoCadastro";

	private static final String INDICADOR_VALIDAR_MASCARA_INSCRICAO_ESTADUAL = "indicadorValidarMascaraInscricaoEstadual";

	private static final String QUANTIDADE_MEDICAO_HORARIA_PARA_MEDIA = "quantidadeMedicaoHorariaParaMedia";

	private static final String QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA = "quantidadeMinimaRegistrosMedicoesHora";

	private static final String UNIDADE_TEMPERATURA_MEDICAO = "unidadeTemperaturaMedicao";

	private static final String UNIDADE_PRESSAO_MEDICAO = "unidadePressaoMedicao";

	private static final String MEDIR_ROTA_FISCALIZACAO = "medirRotaFiscalizacao";

	private static final String ARREDONDAMENTO_FATOR_PCS = "arredondamentoFatorPCS";

	private static final String DIRETORIO_IMPORTAR_LEITURA_UPLOAD_MEDICAO = "diretorioImportarLeituraUploadMedicao";
	
	private static final String DIRETORIO_REGISTRO_FOTO = "diretorioImportarRegistroFotoLeitura";
	
	private static final String DIRETORIO_FALHA_REGISTRO_FOTO = "diretorioFalhaRegistroFotoLeitura";

	private static final String PCS_REFERENCIA_MEDICAO = "pcsReferenciaMedicao";

	private static final String TEMPERATURA_CONDICAO_REFERENCIA_MEDICAO = "temperaturaCondicaoReferenciaMedicao";

	private static final String PRESSAO_CONDICAO_REFERENCIA_MEDICAO = "pressaoCondicaoReferenciaMedicao";

	private static final String PERMITE_ALTERACAO_PCSZ_MEDICAO = "permiteAlteracaoPcszMedicao";

	private static final String PERMITE_ALTERACAO_PCS_FATURADO_MEDICAO = "permiteAlteracaoPcsFaturadoMedicao";

	private static final String NUMERO_DIGITOS_CORRETOR_VAZAO = "numeroDigitosCorretorVazao";

	private static final String TIPO_DADO_ENDERECO_REMOTO = "tipoDadoEnderecoRemoto";

	private static final String TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO = "tipoIntegracaoSistemaSupervisorio";

	private static final String COMENTARIO_MEDICAO_DIARIA_HORARIA = "comentarioMedicaoDiariaHoraria";

	private static final String COMENTARIO_ANALISE_EXCECAO_MEDICAO = "comentarioAnaliseExcecaoMedicao";

	private static final String SUPERVISORIOFATORZVALORFIXO1 = "supervisoriofatorzvalorfixo1";

	private static final String EXIBE_COLUNAS_SUPERVISORIO = "exibeColunasSupervisorio";

	private static final String TIPO_COMPOSICAO_CONSUMO_PADRAO_MEDICAO = "tipoComposicaoConsumoPadraoMedicao";

	private static final String ANORMALIDADE_CONSUMO_ZERO = "anormalidadeConsumoZero";

	private static final String DIRETOR_FINANCEIRO_TEXTO = "diretorFinanceiroTexto";

	private static final String DIRETORIO_ARQUIVOS_SEFAZ = "diretorioArquivosSefaz";

	private static final String SOMAR_VOLUME_FATURA_IMPRESSAO = "somarVolumeFaturaImpressao";

	private static final String CONCEDE_DESCONTO_INADIMPLENTE = "concedeDescontoInadimplente";

	private static final String FATURA_VALOR_ZERO = "faturaValorZero";
	
	private static final String FATURA_CONSUMO_ZERO = "faturaConsumoZero";

	private static final String QUANTIDADE_DIAS_APRESENTACAO_VENCIMENTO = "quantidadeDiasApresentacaoVencimento";
	
	private static final String QUANTIDADE_MESES_PRESCRICAO = "quantidadeMesesPrescricao";
	
	private static final String CANCELA_FATURA_APOS_PRAZO = "cancelaFaturaAposPrazo";

	private static final String EMISSAO_SERIE_ELETRONICA = "emissaoSerieEletronica";

	private static final String USUARIO_FATURAMENTO = "usuarioFaturamento";

	private static final String INDICADOR_AMBIENTE_SISTEMA_NFE_FATURAMENTO = "indicadorAmbienteSistemaNFEFaturamento";

	private static final String NR_VERSAO_XMLNFE_FATURAMENTO = "nrVersaoXMLNFEFaturamento";

	private static final String REGIME_TRIBUTARIO_FATURAMENTO = "regimeTributarioFaturamento";

	private static final String VOLUME_REF_TARIFA_MEDIA_FATURAMENTO = "volumeRefTarifaMediaFaturamento";

	private static final String VALOR_MIN_GERACAO_DOC_COB_FATURAMENTO = "valorMinGeracaoDocCobFaturamento";

	private static final String TIPO_SUBSTITUTO_FATURAMENTO = "tipoSubstitutoFaturamento";

	private static final String PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO = "percentualValorFaturaLimiteDesconto";

	private static final String TIPO_DOC_CREDITO_REALIZAR_FATURAMENTO = "tipoDocCreditoRealizarFaturamento";

	private static final String TEXTO_EMAIL_FATURA = "textoEmailFatura";

	private static final String VIGENCIA_SUBSTITUICAO_TRIBUTARIA = "vigenciaSubstituicaoTributaria";

	private static final String TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO_DESTACADO =
			"textoSubstituicaoTributariaNotaFaturamentoDestacado";

	private static final String TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO = "textoSubstituicaoTributariaNotaFaturamento";

	private static final String TAMANHO_DESC_COMPLEMENTAR_NOTA_FATURAMENTO = "tamanhoDescComplementarNotaFaturamento";

	private static final String TAMANHO_DESC_COMPLEMENTAR_CRE_DEB_FATURAMENTO = "tamanhoDescComplementarCreDebFaturamento";

	private static final String PRAZO_MIN_PARA_ENTRAGA_FATURA_VENCIMENTO = "prazoMinParaEntragaFaturaVencimento";

	private static final String QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO = "qtdaMinCronogramasAbertoFaturamento";

	private static final String QTDA_MAX_CRONOGRAMAS_ABERTOS_FATURAMENTO = "qtdaMaxCronogramasAbertosFaturamento";

	private static final String QTDA_MAX_FATURAMENTO_MEDIA_FATURAMENTO = "qtdaMaxFaturamentoMediaFaturamento";

	private static final String QTDA_MAX_FATURAMENTO_INFORMADO_CLIENTE_FATURAMENTO = "qtdaMaxFaturamentoInformadoClienteFaturamento";

	private static final String QTDA_FATURA_LOTE_FATURAMENTO = "qtdaFaturaLoteFaturamento";

	private static final String QTDA_DIAS_CANCELAMENTO_NOTA_FISCAL_FATURAMENTO = "qtdaDiasCancelamentoNotaFiscalFaturamento";

	private static final String INTERVALO_MAXIMO_COBRANCA_FATURA_FATURAMENTO = "intervaloMaximoCobrancaFaturaFaturamento";

	private static final String NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO_FATURAMENTO = "numeroDiasMaximoProrrogacaoVencimentoFaturamento";

	private static final String REAJUSTE_COMPLEMENTAR_FATURAMENTO = "reajusteComplementarFaturamento";

	private static final String DIAS_MINIMO_VENCIMENTO_FATURAMENTO = "diasMinimoVencimentoFaturamento";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL = "exibirMenuAgenciaVirtualConsultaNotaFiscal";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO = "exibirMenuAgenciaVirtualConsumoPorPeriodo";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL =
			"exibirMenuAgenciaVirtualConsultaDeclaracaoQuitacaoAnual";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL =
			"exibirMenuAgenciaVirtualAutenticacaoDeclaracaoQuitacaoAnual";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO = "exibirMenuAgenciaVirtualContratoAdesao";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL = "exibirMenuAgenciaVirtualAlteracaoCadastral";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO = "exibirMenuAgenciaVirtualAlteracaoDataVencimento";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO = "exibirMenuAgenciaVirtualInterrupcaoFornecimento";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA =
			"exibirMenuAgenciaVirtualReligacaoUnidadeConsumidora";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS = "exibirMenuAgenciaVirtualLigacaoGas";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO = "exibirMenuAgenciaVirtualProgramacaoConsumo";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA = "exibirMenuAgenciaVirtualCromatografia";

	private static final String EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO = "exibirMenuAgenciaVirtualFaleConosco";

	private static final String DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO = "dataLimiteReligacaoGasPorDesligamentoIndevido";

	private static final String DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO = "dataLimiteLigacaoGasAltaPressao";
	
	private static final String NUMERO_DIAS_CHECAGEM_CORTE = "numeroDiasChecagemCorte";
	
	private static final String PRAZO_MINIMO_CRITICIDADE = "prazoMinimoCriticidade";
	
	private static final String PRAZO_MAXIMO_CRITICIDADE = "prazoMaximoCriticidade";
	
	private static final String VALOR_MINIMO_CRITICIDADE = "valorMinimoCriticidade";
	
	private static final String VALOR_MEDIO_CRITICIDADE = "valorMedioCriticidade";
	
	private static final String VALOR_MAXIMO_CRITICIDADE = "valorMaximoCriticidade";

	private static final String DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO = "dataLimiteLigacaoGasMediaPressao";

	private static final String DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO = "dataLimiteLigacaoGasBaixaPressao";

	private static final String OBRIGATORIEDADE_TELEFONE = "obrigatoriedadeTelefone";

	private static final String DATA_LIMITE_RELIGACAO_GAS = "dataLimiteReligacaoGas";

	private static final String MSG_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO = "msgErroQdsMaiorQueQdcTolerado";

	private static final String DIA_LIMITE_APROVACAO_PROGRAMACAO_CONSUMO = "diaLimiteAprovacaoProgramacaoConsumo";

	private static final String PRAZO_ALTERACAO_VENCIMENTO = "prazoAlteracaoVencimento";

	private static final String VENCIMENTO_DEBITO_ATRASO = "vencimentoDebitoAtraso";

	private static final String VENCIMENTO_AUTOMATICO = "vencimentoAutomatico";

	private static final String DIA_LIMITE_PROGRAMACAO_CONSUMO = "diaLimiteProgramacaoConsumo";

	private static final String QUANTIDADE_MESES_PROGRAMACAO_CONSUMO = "quantidadeMesesProgramacaoConsumo";

	private static final String PASSAPORTE = "passaporte";

	private static final String MANIFESTACAO = "manifestacao";

	private static final String CARACTER_MINUSCULO_AG = "caracterMinusculoAg";

	private static final String CARACTER_MAIUSCULO_AG = "caracterMaiusculoAg";

	private static final String TAMANHO_MIN_SENHA_AG = "tamanhoMinSenhaAg";

	private static final String TAMANHO_MAX_SENHA_AG = "tamanhoMaxSenhaAg";

	private static final String ACESSO_AGENCIA = "acessoAgencia";

	private static final String QUESTIONARIO_SATISFACAO = "questionarioSatisfacao";

	private static final String PROTOCOLO_AUTOMATICO = "protocoloAutomatico";

	private static final String PERGUNTA_SATISFACAO = "perguntaSatisfacao";

	private static final String PDD_VALOR_LIMITE_ANUAL = "pddValorLimiteAnual";

	private static final String MASCARA_NUMERO_CONTA = "mascaraNumeroConta";

	private static final String IND_CONTABILIZA_DESCONTO_TARIFA = "indContabilizaDescontoTarifa";

	private static final String PDD_VALOR_LIMITE_SEMESTRAL = "pddValorLimiteSemestral";

	private static final String QTD_CLIENTES_RELATORIO_LOTE_COBRANCA = "qtdClientesRelatorioLoteCobranca";

	private static final String ENVIAR_EMAIL_AVISO_NOTIFICACAO_CORTE = "enviarEmailAvisoNotificacaoCorte";

	private static final String EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO = "exibirMensagemNotificacaoCorte";
	
	private static final String MENSAGEM_NOTIFICACAO_CORTE = "mensagemNotificacaoCorte";

	private static final String PARTICIPA_CONVENIO_ECARTAS = "participaECartas";

	private static final String NUMERO_CONTRATO_ECARTAS = "numeroContratoECartas";

	private static final String NUMERO_CARTAO_POSTAL_ECARTAS = "numeroCartaoPostalECartas";

	private static final String QUANTIDADE_FATURA_POR_LOTE_ECARTAS = "quantidadeFaturaPorLoteECartas";

	private static final String DATA_VENC_COBRANCA = "dataVencCobranca";

	private static final String DEBITO_SITUACAO_PARCELADA_COBRANCA = "debitoSituacaoParceladaCobranca";

	private static final String DOC_COBRANCA_RECEB_MENOR_ARRECADACAO = "docCobrancaRecebMenorArrecadacao";

	private static final String GERA_CREDITO_RECEB_MAIOR_DUPLICIDADE_ARRECADACAO = "geraCreditoRecebMaiorDuplicidadeArrecadacao";

	private static final String EMPRESA_FEBRABAN_ARRECADACAO = "empresaFebrabanArrecadacao";

	private static final String RECEBIMENTO_DOCTO_INEXIS_ARRECADACAO = "recebimentoDoctoInexisArrecadacao";

	private static final String ARQUIVOS_ARRECADACAO = "arquivosArrecadacao";

	private static final String ARQUIVOS_RETORNO_ARRECADACAO = "arquivosRetornoArrecadacao";

	private static final String ARQUIVOS_REMESSA_ARRECADACAO = "arquivosRemessaArrecadacao";

	private static final String ARQUIVOS_UPLOAD_ARRECADACAO = "arquivosUploadArrecadacao";

	private static final String QTD_DIAS_VENC_DOC_COMPLEMENTAR_ARRECADACAO = "qtdDiasVencDocComplementarArrecadacao";

	private static final String VALOR_MIN_DOC_COBRANCA_REC_MENOR_ARRECADACAO = "valorMinDocCobrancaRecMenorArrecadacao";

	private static final String ACAO_RECEBIMENTO_A_MENOR = "acaoRecebimentoAMenor";

	private static final String MOTIVO_REFATURAR_FATURAMENTO = "motivoRefaturarFaturamento";

	private static final String MOTIVO_INCLUSAO_FATURAMENTO = "motivoInclusaoFaturamento";

	private static final String DEBITO_MOTIVO_CANCELAMENTO_FATURAMENTO = "debitoMotivoCancelamentoFaturamento";

	private static final String DEBITO_SITUACAO_NORMAL_FATURAMENTO = "debitoSituacaoNormalFaturamento";

	private static final String DIRETORIO_ARQUIVOS_FATURA_FATURAMENTO = "diretorioArquivosFaturaFaturamento";

	private static final String DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO = "diretorioArquivosSimulacaoFaturamento";

	private static final String FILTRO_ITEM_FATURA_PRECO_GAS_FATURAMENTO = "filtroItemFaturaPrecoGasFaturamento";

	private static final String CREDITO_VOLUME_AUTOMATICAMENTO_FATURAMENTO = "creditoVolumeAutomaticamentoFaturamento";

	private static final String TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO = "tipoArredondamentoConsumoMedicao";

	private static final String TIPO_ARREDONDAMENTO_CALCULO_GERAL = "tipoArredondamentoCalculoGeral";

	private static final String REFERENCIA_FATURAMENTO = "referenciaFaturamento";

	private static final String REFERENCIA_CONTABIL_GERAL = "referenciaContabilGeral";

	private static final String ATRIBUTO_PARAMETROS = "parametros";

	private static final String MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO = "MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO";

	private static final String LISTA_ANORMALIDADE_CONSUMO = "listaAnormalidadeConsumo";

	private static final String LISTA_CLASSE_UNIDADE = "listaClasseUnidade";

	private static final String LISTA_OPERACAO_MEDIDOR = "listaOperacaoMedidor";

	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";

	private static final String LISTA_SITUACAO_LEITURA = "listaSituacaoLeitura";

	private static final String LISTA_TIPO_COMPOSICAO_CONSUMO = "listaTipoComposicaoConsumo";

	private static final String LISTA_NUMERO_DIGITOS_CONSUMO_CORRETOR_VAZAO = "listaNumeroDigitosCorretorVazao";

	private static final String LISTA_TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO = "listaTipoIntegracaoSistemaSupervisorio";

	private static final String LISTA_TIPO_DADO_ENDERECO_REMOTO = "listaTipoDadoEnderecoRemoto";

	private static final String LISTA_TIPO_CONSUMO = "listaTipoConsumo";

	private static final String LISTA_TIPO_MEDICAO = "listaTipoMedicao";

	private static final String LISTA_TIPO_AGRUPAMENTO = "listaTipoAgrupamento";

	private static final String LISTA_CREDITO_DEBITO_SITUACAO = "listaCreditoDebitoSituacao";

	private static final String LISTA_ENTIDADE_CLASSE = "listaEntidadeClasse";

	private static final String LISTA_FINANCIAMENTO_TIPO = "listaFinanciamentoTipo";

	private static final String LISTA_INDICE_FINANCEIRO = "listaIndiceFinanceiro";

	private static final String LISTA_CREDITO_ORIGEM = "listaCreditoOrigem";

	private static final String LISTA_RUBRICA = "listaRubrica";

	private static final String LISTA_TIPO_DOCUMENTO = "listaTipoDocumento";

	private static final String LISTA_FORMA_COBRANCA_PARCELAMENTO = "listaFormaCobrancaParcelamento";

	private static final String LISTA_FORMA_INCLUSAO_FATURA = "listaFormaInclusaoFatura";

	private static final String LISTA_PERIOTICIDADE_COBRANCA = "listaPerioticidadeCobranca";

	private static final String LISTA_TIPO_FATURAMENTO = "listaTipoFaturamento";

	private static final String LISTA_TIPO_OPERACAO = "listaTipoOperacao";

	private static final String LISTA_MOTIVO_CANCELAMENTO = "listaMotivoCancelamento";

	private static final String LISTA_ITEM_FATURA = "listaItemFatura";

	private static final String LISTA_REAJUSTE_CASCATA = "listaReajusteCascata";

	private static final String LISTA_TIPO_CREDITO_DEBITO = "listaTipoCreditoDebito";

	private static final String LISTA_TIPO_SUBSTITUTO = "listaTipoSubstituto";

	private static final String LISTA_TRIBUTOS = "listaTributos";

	private static final String LISTA_VOLUME_REFERENCIA_TARIFA_MEDIA_TOP = "listaVolumeReferenciaTarifaMediaTop";

	private static final String LISTA_SITUACAO_IMOVEL = "listaSituacaoImovel";

	private static final String LISTA_UNIDADE_FEDERACAO = "listaUnidadeFederacao";

	private static final String LISTA_TIPO_ENDERECO = "listaTipoEndereco";

	private static final String LISTA_ESFERA_PODER = "listaEsferaPoder";

	private static final String LISTA_LANCAMENTO_ITEM_CONTABIL = "listaLancamentoItemContabil";

	private static final String LISTA_SITUACAO_TRIBUTARIA_PIS_COFINS = "listaSituacaoTributariaPisCofins";

	private static final String LISTA_REGIME_TRIBUTARIO = "listaRegimeTributario";

	private static final String LISTA_STATUS_NFE = "listaStatusNfe";

	private static final String LISTA_TIPO_MEDIDOR_MOTIVO_MOVIMENTACAO_ENTRADA = "listaMotivoMovMedidorEntrada";

	private static final String LISTA_INDICADOR_AMBIENTE_NFE = "listaIndicadorAmbienteNFE";

	private static final String LISTA_STATUS_TARIFA = "listaStatusTarifa";

	private static final String LISTA_VIGENCIA_SUBSTITUICAO_TRIBUTARIA = "listaVigenciaSubstituicaoTributaria";

	private static final String LISTA_UNIDADE_PRESSAO_MEDICAO = "listaUnidadePressaoMedicao";

	private static final String LISTA_UNIDADE_TEMPERATURA_MEDICAO = "listaUnidadeTemperaturaMedicao";

	private static final String UTILIZAR_MULTIPLOS_CICLOS = "utilizarMultiplosCiclos";
	
	private static final String OBRIGATORIO_RETORNO_PROTOCOLO = "obrigatorioRetornoProtocolo";
	
	private static final String LATITUDE_EMPRESA = "latitudeEmpresa";
	
	private static final String LONGITUDE_EMPRESA = "longitudeEmpresa";
	
	private static final String NUMERO_FATURAS_ROTEIRIZAR = "numeroFaturasRoteirizar";
	
	private static final String VALIDA_CONTRATO = "validaContrato";
	
	private static final String DESATIVAR_PONTO_CONTRATO = "desativarPontoContrato";
	
	private static final String CRIAR_CRONOGRAMAS_ENCERRAR = "criarCronogramasEncerrar";

	private static final String INDICADOR_AMBIENTE_PRODUCAO = "indicadorAmbienteProducao";

	private static final String INDICADOR_FATURAMENTO_PARALELO = "faturamentoParalelo";
					
	private static final String INDICADOR_CONTABILIZACAO_LANCAMENTOS = "contabilizacaoLancamentos";
	
	private static final String REGISTRA_HISTORICO_MEDICAO = "registraHistoricoMedicao";
	
	private static final String CALCULAR_MEDIA_DIARIA_COLETOR = "calcularMediaDiariaColetor";
	
	private static final String PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO = "porcentagemConsideradaFaturamentoElevado";
	
	private static final String QUANTIDADE_REFERENCIAS_CALCULO_MEDIA = "quantidadeReferenciasCalculoMedia";
	
	private static final String CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS = "consideraUltimaLeituraGerarDados";
	
	private static final String CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO = "considerarAnomaliaFaturamentoContrato";
	
	private static final String NOME_CERTIFICADO_DIGITAL = "nomeCertificadoDigital";

	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	private ControladorUsuario controladorUsuario;

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorRecebimento controladorRecebimento;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorTributo controladorTributo;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorMedidor controladorMedidor;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorEndereco controladorEndereco;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Método responsável por consultar os parametros do sistema.
	 *
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("consultarParametroSistema")
	public String consultarParametroSistema(Model model) throws GGASException {

		List<ParametroSistema> parametros = controladorParametroSistema.consultarParametroSistema();
		model.addAttribute(ATRIBUTO_PARAMETROS, parametros);

		return "consultarParametros";
	}

	/**
	 * Método responsável por alterar o valor dos parametros do sistema.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws IOException 
	 */
	@RequestMapping("alterarParametros")
	public String alterarParametros(HttpServletRequest request, Model model)
			throws GGASException, IOException {

		try {
			configurarDadosTelaParametrizacao(model);
			popularDadosTela(request, model);
			Map<String, Map<String, String>> mapa = new LinkedHashMap<>();
			mapa.put("Geral", this.obterDadosForm(this.obterMapaParametroAtributoFormGeral(), model));
			mapa.put("Cadastro", this.obterDadosForm(this.obterMapaParametroAtributoFormCadastro(), model));
			mapa.put("Contrato", this.obterDadosForm(this.obterMapaParametroAtributoFormContrato(), model));
			mapa.put("Medição", this.obterDadosForm(this.obterMapaParametroAtributoFormMedicao(), model));
			mapa.put("Faturamento", this.obterDadosForm(this.obterMapaParametroAtributoFormFaturamento(), model));
			mapa.put("Arrecadação", this.obterDadosForm(this.obterMapaParametroAtributoFormArrecadacao(), model));
			mapa.put("Cobrança", this.obterDadosForm(this.obterMapaParametroAtributoFormCobranca(), model));
			mapa.put("Contabilidade", this.obterDadosForm(this.obterMapaParametroAtributoFormContabilidade(), model));
			mapa.put("Atendimento", this.obterDadosForm(this.obterMapaParametroAtributoFormAtendimento(), model));
			mapa.put("Agência Virtual",
					this.obterDadosForm(this.obterMapaParametroAtributoFormAgenciaVirtual(), model));
			mapa.put("Segurança", this.obterDadosForm(this.obterMapaParametroAtributoFormSeguranca(), model));

			this.validarToken(request);
			String emailRemetenteDefaultGeral = request.getParameter(EMAIL_REMETENTE_DEFAULT_GERAL);
			controladorUsuario.verificarEmailValido(emailRemetenteDefaultGeral);

			validarReferenciaIntegracaoNotaTitulo(model);

			controladorParametroSistema.salvarAlteracaoParametros(mapa, this.getDadosAuditoria(request));

			verificarInclusaoCertificadoDigital(request);

			atualizarSessaoCriteriosCampos(request);

			mensagemSucesso(model, MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
	        request.getSession().removeAttribute("arquivoCertificado");
	        request.getSession().removeAttribute("senhaCertificado");
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		this.saveToken(request);

		return "alterarParametro";
	}

	private void verificarInclusaoCertificadoDigital(HttpServletRequest request) throws NegocioException, ConcorrenciaException, IOException {
		MultipartFile arquivoCertificado = (MultipartFile) request.getSession().getAttribute("arquivoCertificado");
		String senhaCertificado = (String) request.getSession().getAttribute("senhaCertificado");
		
		if(arquivoCertificado != null && !StringUtils.isEmpty(senhaCertificado)) {
			controladorParametroSistema.incluirCertificadoDigital(arquivoCertificado, senhaCertificado);
		}
		
	}

	private void validarReferenciaIntegracaoNotaTitulo(Model model) throws NegocioException {
		Map<String, Object> mapaAtributosForm = model.asMap();
		String valorReferenciaIntegracaoNotaTitulo
				= String.valueOf(mapaAtributosForm.get(REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO_GERAL));
		ParametroSistema parametroSistema =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);
		if (!parametroSistema.getValor().equals(valorReferenciaIntegracaoNotaTitulo)) {
			ConstanteSistema referenciaIntegracaoContrato =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);
			ConstanteSistema referenciaIntegracaoCliente =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CLIENTE);
			boolean existeRegistroIntegrado = false;
			if (referenciaIntegracaoContrato.getValor().equals(parametroSistema.getValor())) {
				existeRegistroIntegrado = controladorIntegracao.existeIntegracaoContratoProcessada();
			} else if (referenciaIntegracaoCliente.getValor().equals(parametroSistema.getValor())) {
				existeRegistroIntegrado = controladorIntegracao.existeIntegracaoClienteProcessada();
			}
			if (existeRegistroIntegrado) {
				throw new NegocioException(Constantes.ERRO_JA_EXISTEM_REGISTROS_INTEGRADOS, true);
			}
		}
	}

	/**
	 * Método responsável por receber dados de tela.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularDadosTela(HttpServletRequest request, Model model) {
		popularAbaGeralTela(request, model);
		popularAbaCadastroTela(request, model);
		popularAbaContratoTela(request, model);
		popularAbaMedicaoTela(request, model);
		popularAbaFaturamentoTela(request, model);
		popularAbaArrecadacaoTela(request, model);
		popularAbaCobrancaTela(request, model);
		popularAbaContabilidadeTela(request, model);
		popularAbaAtendimentoTela(request, model);
		popularAbaAgenciaVirtualTela(request, model);
		popularAbaSegurancaTela(request, model);
	}


	/**
	 * Método responsável por popular dados relacionados a aba Agencia Virtual
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaAgenciaVirtualTela(HttpServletRequest request, Model model) {
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA)));
		model.addAttribute(EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO,
				String.valueOf(request.getParameter(EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba de Atendimento
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaAtendimentoTela(HttpServletRequest request, Model model) {

		model.addAttribute(PROTOCOLO_AUTOMATICO, String.valueOf(request.getParameter(PROTOCOLO_AUTOMATICO)));
		model.addAttribute(QUESTIONARIO_SATISFACAO, String.valueOf(request.getParameter(QUESTIONARIO_SATISFACAO)));
		model.addAttribute(ACESSO_AGENCIA, String.valueOf(request.getParameter(ACESSO_AGENCIA)));
		model.addAttribute(TAMANHO_MAX_SENHA_AG, String.valueOf(request.getParameter(TAMANHO_MAX_SENHA_AG)));
		model.addAttribute(TAMANHO_MIN_SENHA_AG, String.valueOf(request.getParameter(TAMANHO_MIN_SENHA_AG)));
		model.addAttribute(CARACTER_MAIUSCULO_AG, String.valueOf(request.getParameter(CARACTER_MAIUSCULO_AG)));
		model.addAttribute(CARACTER_MINUSCULO_AG, String.valueOf(request.getParameter(CARACTER_MINUSCULO_AG)));
		model.addAttribute(MANIFESTACAO, String.valueOf(request.getParameter(MANIFESTACAO)));
		model.addAttribute(PASSAPORTE, String.valueOf(request.getParameter(PASSAPORTE)));
		model.addAttribute(QUANTIDADE_MESES_PROGRAMACAO_CONSUMO,
				String.valueOf(request.getParameter(QUANTIDADE_MESES_PROGRAMACAO_CONSUMO)));
		model.addAttribute(DIA_LIMITE_PROGRAMACAO_CONSUMO, String.valueOf(request.getParameter(DIA_LIMITE_PROGRAMACAO_CONSUMO)));
		model.addAttribute(VENCIMENTO_AUTOMATICO, String.valueOf(request.getParameter(VENCIMENTO_AUTOMATICO)));
		model.addAttribute(VENCIMENTO_DEBITO_ATRASO, String.valueOf(request.getParameter(VENCIMENTO_DEBITO_ATRASO)));
		model.addAttribute(PRAZO_ALTERACAO_VENCIMENTO, String.valueOf(request.getParameter(PRAZO_ALTERACAO_VENCIMENTO)));
		model.addAttribute(DIA_LIMITE_APROVACAO_PROGRAMACAO_CONSUMO,
				String.valueOf(request.getParameter(DIA_LIMITE_APROVACAO_PROGRAMACAO_CONSUMO)));
		model.addAttribute(MSG_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO, String.valueOf(request.getParameter(MSG_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO)));
		model.addAttribute(DATA_LIMITE_RELIGACAO_GAS, String.valueOf(request.getParameter(DATA_LIMITE_RELIGACAO_GAS)));
		model.addAttribute(OBRIGATORIEDADE_TELEFONE, String.valueOf(request.getParameter(OBRIGATORIEDADE_TELEFONE)));
		model.addAttribute(DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO,
				String.valueOf(request.getParameter(DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO)));
		model.addAttribute(DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO,
				String.valueOf(request.getParameter(DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO)));
		model.addAttribute(DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO,
				String.valueOf(request.getParameter(DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO)));
		model.addAttribute(DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO,
				String.valueOf(request.getParameter(DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO)));
		model.addAttribute(PERGUNTA_SATISFACAO, String.valueOf(request.getParameter(PERGUNTA_SATISFACAO)));
		model.addAttribute(NUMERO_DIAS_CHECAGEM_CORTE,
				String.valueOf(request.getParameter(NUMERO_DIAS_CHECAGEM_CORTE)));
		model.addAttribute(PRAZO_MINIMO_CRITICIDADE,
				String.valueOf(request.getParameter(PRAZO_MINIMO_CRITICIDADE)));
		model.addAttribute(PRAZO_MAXIMO_CRITICIDADE,
				String.valueOf(request.getParameter(PRAZO_MAXIMO_CRITICIDADE)));
		model.addAttribute(VALOR_MINIMO_CRITICIDADE,
				String.valueOf(request.getParameter(VALOR_MINIMO_CRITICIDADE)));
		model.addAttribute(VALOR_MEDIO_CRITICIDADE,
				String.valueOf(request.getParameter(VALOR_MEDIO_CRITICIDADE)));
		model.addAttribute(VALOR_MEDIO_CRITICIDADE,
				String.valueOf(request.getParameter(VALOR_MEDIO_CRITICIDADE)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Contabilidade
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaContabilidadeTela(HttpServletRequest request, Model model) {
		model.addAttribute(MASCARA_NUMERO_CONTA, String.valueOf(request.getParameter(MASCARA_NUMERO_CONTA)));
		model.addAttribute(IND_CONTABILIZA_DESCONTO_TARIFA, String.valueOf(request.getParameter(IND_CONTABILIZA_DESCONTO_TARIFA)));
		model.addAttribute(PDD_VALOR_LIMITE_ANUAL, String.valueOf(request.getParameter(PDD_VALOR_LIMITE_ANUAL)));
		model.addAttribute(PDD_VALOR_LIMITE_SEMESTRAL, String.valueOf(request.getParameter(PDD_VALOR_LIMITE_SEMESTRAL)));
		model.addAttribute(REFERENCIA_CONTABIL_GERAL, String.valueOf(request.getParameter(REFERENCIA_CONTABIL_GERAL)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Cobrança
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaCobrancaTela(HttpServletRequest request, Model model) {
		model.addAttribute(DEBITO_SITUACAO_PARCELADA_COBRANCA, String.valueOf(request.getParameter(DEBITO_SITUACAO_PARCELADA_COBRANCA)));
		model.addAttribute(DATA_VENC_COBRANCA, String.valueOf(request.getParameter(DATA_VENC_COBRANCA)));
		model.addAttribute(ENVIAR_EMAIL_AVISO_NOTIFICACAO_CORTE,
				String.valueOf(request.getParameter(ENVIAR_EMAIL_AVISO_NOTIFICACAO_CORTE)));
		model.addAttribute(EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO,
				String.valueOf(request.getParameter(EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO)));
		model.addAttribute(QTD_CLIENTES_RELATORIO_LOTE_COBRANCA,
				String.valueOf(request.getParameter(QTD_CLIENTES_RELATORIO_LOTE_COBRANCA)));
		model.addAttribute(PARTICIPA_CONVENIO_ECARTAS, String.valueOf(request.getParameter(PARTICIPA_CONVENIO_ECARTAS)));
		model.addAttribute(NUMERO_CONTRATO_ECARTAS, String.valueOf(request.getParameter(NUMERO_CONTRATO_ECARTAS)));
		model.addAttribute(NUMERO_CARTAO_POSTAL_ECARTAS, String.valueOf(request.getParameter(NUMERO_CARTAO_POSTAL_ECARTAS)));
		model.addAttribute(QUANTIDADE_FATURA_POR_LOTE_ECARTAS, String.valueOf(request.getParameter(QUANTIDADE_FATURA_POR_LOTE_ECARTAS)));
		model.addAttribute(OBRIGATORIO_RETORNO_PROTOCOLO, String.valueOf(request.getParameter(OBRIGATORIO_RETORNO_PROTOCOLO)));
		model.addAttribute(MENSAGEM_NOTIFICACAO_CORTE,
				String.valueOf(request.getParameter(MENSAGEM_NOTIFICACAO_CORTE)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Arrecadação
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaArrecadacaoTela(HttpServletRequest request, Model model) {
		model.addAttribute(NOME_EMPRESA_GERAL, String.valueOf(request.getParameter(NOME_EMPRESA_GERAL)));
		model.addAttribute(GERA_CREDITO_RECEB_MAIOR_DUPLICIDADE_ARRECADACAO,
				String.valueOf(request.getParameter(GERA_CREDITO_RECEB_MAIOR_DUPLICIDADE_ARRECADACAO)));
		model.addAttribute(EMPRESA_FEBRABAN_ARRECADACAO, String.valueOf(request.getParameter(EMPRESA_FEBRABAN_ARRECADACAO)));
		model.addAttribute(RECEBIMENTO_DOCTO_INEXIS_ARRECADACAO,
				String.valueOf(request.getParameter(RECEBIMENTO_DOCTO_INEXIS_ARRECADACAO)));
		model.addAttribute(ARQUIVOS_ARRECADACAO, String.valueOf(request.getParameter(ARQUIVOS_ARRECADACAO)));
		model.addAttribute(ARQUIVOS_REMESSA_ARRECADACAO, String.valueOf(request.getParameter(ARQUIVOS_REMESSA_ARRECADACAO)));
		model.addAttribute(ARQUIVOS_RETORNO_ARRECADACAO, String.valueOf(request.getParameter(ARQUIVOS_RETORNO_ARRECADACAO)));
		model.addAttribute(ARQUIVOS_UPLOAD_ARRECADACAO, String.valueOf(request.getParameter(ARQUIVOS_UPLOAD_ARRECADACAO)));
		model.addAttribute(DOC_COBRANCA_RECEB_MENOR_ARRECADACAO,
				String.valueOf(request.getParameter(DOC_COBRANCA_RECEB_MENOR_ARRECADACAO)));
		model.addAttribute(QTD_DIAS_VENC_DOC_COMPLEMENTAR_ARRECADACAO,
				String.valueOf(request.getParameter(QTD_DIAS_VENC_DOC_COMPLEMENTAR_ARRECADACAO)));
		model.addAttribute(VALOR_MIN_DOC_COBRANCA_REC_MENOR_ARRECADACAO,
				String.valueOf(request.getParameter(VALOR_MIN_DOC_COBRANCA_REC_MENOR_ARRECADACAO)));
		model.addAttribute(ACAO_RECEBIMENTO_A_MENOR, String.valueOf(request.getParameter(ACAO_RECEBIMENTO_A_MENOR)));
		model.addAttribute("arrecadacaoCampoArquivoCodigoConvenio",
				String.valueOf(request.getParameter("arrecadacaoCampoArquivoCodigoConvenio")));
		model.addAttribute("arrecadacaoCodigoConvenio",
				String.valueOf(request.getParameter("arrecadacaoCodigoConvenio")));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Faturamento
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaFaturamentoTela(HttpServletRequest request, Model model) {
		model.addAttribute(MOTIVO_REFATURAR_FATURAMENTO, String.valueOf(request.getParameter(MOTIVO_REFATURAR_FATURAMENTO)));
		model.addAttribute(MOTIVO_INCLUSAO_FATURAMENTO, String.valueOf(request.getParameter(MOTIVO_INCLUSAO_FATURAMENTO)));
		model.addAttribute(DEBITO_MOTIVO_CANCELAMENTO_FATURAMENTO,
				String.valueOf(request.getParameter(DEBITO_MOTIVO_CANCELAMENTO_FATURAMENTO)));
		model.addAttribute(DEBITO_SITUACAO_NORMAL_FATURAMENTO, String.valueOf(request.getParameter(DEBITO_SITUACAO_NORMAL_FATURAMENTO)));
		model.addAttribute(DIRETORIO_ARQUIVOS_FATURA_FATURAMENTO,
				String.valueOf(request.getParameter(DIRETORIO_ARQUIVOS_FATURA_FATURAMENTO)));
		model.addAttribute(DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO,
				String.valueOf(request.getParameter(DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO)));
		model.addAttribute(FILTRO_ITEM_FATURA_PRECO_GAS_FATURAMENTO,
				String.valueOf(request.getParameter(FILTRO_ITEM_FATURA_PRECO_GAS_FATURAMENTO)));
		model.addAttribute(CREDITO_VOLUME_AUTOMATICAMENTO_FATURAMENTO,
				String.valueOf(request.getParameter(CREDITO_VOLUME_AUTOMATICAMENTO_FATURAMENTO)));
		model.addAttribute(REAJUSTE_COMPLEMENTAR_FATURAMENTO, String.valueOf(request.getParameter(REAJUSTE_COMPLEMENTAR_FATURAMENTO)));
		model.addAttribute(NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO_FATURAMENTO,
				String.valueOf(request.getParameter(NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO_FATURAMENTO)));
		model.addAttribute(DIAS_MINIMO_VENCIMENTO_FATURAMENTO, String.valueOf(request.getParameter(DIAS_MINIMO_VENCIMENTO_FATURAMENTO)));
		model.addAttribute(INTERVALO_MAXIMO_COBRANCA_FATURA_FATURAMENTO,
				String.valueOf(request.getParameter(INTERVALO_MAXIMO_COBRANCA_FATURA_FATURAMENTO)));
		model.addAttribute(QTDA_DIAS_CANCELAMENTO_NOTA_FISCAL_FATURAMENTO,
				String.valueOf(request.getParameter(QTDA_DIAS_CANCELAMENTO_NOTA_FISCAL_FATURAMENTO)));
		model.addAttribute(QTDA_FATURA_LOTE_FATURAMENTO, String.valueOf(request.getParameter(QTDA_FATURA_LOTE_FATURAMENTO)));
		model.addAttribute(QTDA_MAX_FATURAMENTO_INFORMADO_CLIENTE_FATURAMENTO,
				String.valueOf(request.getParameter(QTDA_MAX_FATURAMENTO_INFORMADO_CLIENTE_FATURAMENTO)));
		model.addAttribute(QTDA_MAX_FATURAMENTO_MEDIA_FATURAMENTO,
				String.valueOf(request.getParameter(QTDA_MAX_FATURAMENTO_MEDIA_FATURAMENTO)));
		model.addAttribute(QTDA_MAX_CRONOGRAMAS_ABERTOS_FATURAMENTO,
				String.valueOf(request.getParameter(QTDA_MAX_CRONOGRAMAS_ABERTOS_FATURAMENTO)));

		if (!request.getParameter(QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO).equals("")) {
			model.addAttribute(QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO,
					Long.valueOf(request.getParameter(QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO)));
		} else {
			model.addAttribute(QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO, null);
		}

		model.addAttribute(PRAZO_MIN_PARA_ENTRAGA_FATURA_VENCIMENTO,
				String.valueOf(request.getParameter(PRAZO_MIN_PARA_ENTRAGA_FATURA_VENCIMENTO)));
		model.addAttribute(TAMANHO_DESC_COMPLEMENTAR_CRE_DEB_FATURAMENTO,
				String.valueOf(request.getParameter(TAMANHO_DESC_COMPLEMENTAR_CRE_DEB_FATURAMENTO)));
		model.addAttribute(TAMANHO_DESC_COMPLEMENTAR_NOTA_FATURAMENTO,
				String.valueOf(request.getParameter(TAMANHO_DESC_COMPLEMENTAR_NOTA_FATURAMENTO)));
		model.addAttribute(TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO,
				String.valueOf(request.getParameter(TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO)));
		model.addAttribute(TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO_DESTACADO,
				String.valueOf(request.getParameter(TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO_DESTACADO)));
		model.addAttribute(VIGENCIA_SUBSTITUICAO_TRIBUTARIA, String.valueOf(request.getParameter(VIGENCIA_SUBSTITUICAO_TRIBUTARIA)));
		model.addAttribute(TEXTO_EMAIL_FATURA, String.valueOf(request.getParameter(TEXTO_EMAIL_FATURA)));
		model.addAttribute(TIPO_DOC_CREDITO_REALIZAR_FATURAMENTO,
				String.valueOf(request.getParameter(TIPO_DOC_CREDITO_REALIZAR_FATURAMENTO)));
		model.addAttribute(PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO,
				String.valueOf(request.getParameter(PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO)));
		model.addAttribute(TIPO_SUBSTITUTO_FATURAMENTO, String.valueOf(request.getParameter(TIPO_SUBSTITUTO_FATURAMENTO)));
		model.addAttribute(VALOR_MIN_GERACAO_DOC_COB_FATURAMENTO,
				String.valueOf(request.getParameter(VALOR_MIN_GERACAO_DOC_COB_FATURAMENTO)));
		model.addAttribute(VOLUME_REF_TARIFA_MEDIA_FATURAMENTO, String.valueOf(request.getParameter(VOLUME_REF_TARIFA_MEDIA_FATURAMENTO)));
		model.addAttribute(REFERENCIA_FATURAMENTO, String.valueOf(request.getParameter(REFERENCIA_FATURAMENTO)));
		model.addAttribute(REGIME_TRIBUTARIO_FATURAMENTO, String.valueOf(request.getParameter(REGIME_TRIBUTARIO_FATURAMENTO)));
		model.addAttribute(NR_VERSAO_XMLNFE_FATURAMENTO, String.valueOf(request.getParameter(NR_VERSAO_XMLNFE_FATURAMENTO)));
		model.addAttribute(INDICADOR_AMBIENTE_SISTEMA_NFE_FATURAMENTO,
				String.valueOf(request.getParameter(INDICADOR_AMBIENTE_SISTEMA_NFE_FATURAMENTO)));
		model.addAttribute(USUARIO_FATURAMENTO, String.valueOf(request.getParameter(USUARIO_FATURAMENTO)));
		model.addAttribute(EMISSAO_SERIE_ELETRONICA, String.valueOf(request.getParameter(EMISSAO_SERIE_ELETRONICA)));
		model.addAttribute(QUANTIDADE_DIAS_APRESENTACAO_VENCIMENTO,
				String.valueOf(request.getParameter(QUANTIDADE_DIAS_APRESENTACAO_VENCIMENTO)));
		model.addAttribute(FATURA_VALOR_ZERO, String.valueOf(request.getParameter(FATURA_VALOR_ZERO)));
		model.addAttribute(CONCEDE_DESCONTO_INADIMPLENTE, String.valueOf(request.getParameter(CONCEDE_DESCONTO_INADIMPLENTE)));
		model.addAttribute(SOMAR_VOLUME_FATURA_IMPRESSAO, String.valueOf(request.getParameter(SOMAR_VOLUME_FATURA_IMPRESSAO)));
		model.addAttribute(DIRETOR_FINANCEIRO_TEXTO, String.valueOf(request.getParameter(DIRETOR_FINANCEIRO_TEXTO)));
		model.addAttribute(DIRETORIO_ARQUIVOS_SEFAZ, String.valueOf(request.getParameter(DIRETORIO_ARQUIVOS_SEFAZ)));
		model.addAttribute(UTILIZAR_MULTIPLOS_CICLOS, String.valueOf(request.getParameter(UTILIZAR_MULTIPLOS_CICLOS)));
		model.addAttribute(FATURA_CONSUMO_ZERO, String.valueOf(request.getParameter(FATURA_CONSUMO_ZERO)));
		model.addAttribute(CRIAR_CRONOGRAMAS_ENCERRAR, String.valueOf(request.getParameter(CRIAR_CRONOGRAMAS_ENCERRAR)));
		model.addAttribute(INDICADOR_FATURAMENTO_PARALELO,
				String.valueOf(request.getParameter(INDICADOR_FATURAMENTO_PARALELO)));
		model.addAttribute(INDICADOR_CONTABILIZACAO_LANCAMENTOS,
				String.valueOf(request.getParameter(INDICADOR_CONTABILIZACAO_LANCAMENTOS)));
		model.addAttribute(QUANTIDADE_MESES_PRESCRICAO,
				String.valueOf(request.getParameter(QUANTIDADE_MESES_PRESCRICAO)));
		model.addAttribute(CANCELA_FATURA_APOS_PRAZO,
				String.valueOf(request.getParameter(CANCELA_FATURA_APOS_PRAZO)));
		model.addAttribute(CALCULAR_MEDIA_DIARIA_COLETOR,
				String.valueOf(request.getParameter(CALCULAR_MEDIA_DIARIA_COLETOR)));
		model.addAttribute(PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO,
				String.valueOf(request.getParameter(PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO)));
		model.addAttribute(QUANTIDADE_REFERENCIAS_CALCULO_MEDIA,
				String.valueOf(request.getParameter(QUANTIDADE_REFERENCIAS_CALCULO_MEDIA)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Medição
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaMedicaoTela(HttpServletRequest request, Model model) {
		model.addAttribute(ANORMALIDADE_CONSUMO_ZERO, String.valueOf(request.getParameter(ANORMALIDADE_CONSUMO_ZERO)));
		model.addAttribute(TIPO_COMPOSICAO_CONSUMO_PADRAO_MEDICAO,
				String.valueOf(request.getParameter(TIPO_COMPOSICAO_CONSUMO_PADRAO_MEDICAO)));
		model.addAttribute(EXIBE_COLUNAS_SUPERVISORIO, String.valueOf(request.getParameter(EXIBE_COLUNAS_SUPERVISORIO)));
		model.addAttribute(SUPERVISORIOFATORZVALORFIXO1, String.valueOf(request.getParameter(SUPERVISORIOFATORZVALORFIXO1)));
		model.addAttribute(COMENTARIO_ANALISE_EXCECAO_MEDICAO, String.valueOf(request.getParameter(COMENTARIO_ANALISE_EXCECAO_MEDICAO)));
		model.addAttribute(COMENTARIO_MEDICAO_DIARIA_HORARIA, String.valueOf(request.getParameter(COMENTARIO_MEDICAO_DIARIA_HORARIA)));
		model.addAttribute(TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO,
				String.valueOf(request.getParameter(TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO)));
		model.addAttribute(TIPO_DADO_ENDERECO_REMOTO, String.valueOf(request.getParameter(TIPO_DADO_ENDERECO_REMOTO)));
		model.addAttribute(NUMERO_DIGITOS_CORRETOR_VAZAO, String.valueOf(request.getParameter(NUMERO_DIGITOS_CORRETOR_VAZAO)));
		model.addAttribute(PERMITE_ALTERACAO_PCS_FATURADO_MEDICAO,
				String.valueOf(request.getParameter(PERMITE_ALTERACAO_PCS_FATURADO_MEDICAO)));
		model.addAttribute(PERMITE_ALTERACAO_PCSZ_MEDICAO, String.valueOf(request.getParameter(PERMITE_ALTERACAO_PCSZ_MEDICAO)));
		model.addAttribute(TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO, String.valueOf(request.getParameter(TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO)));
		model.addAttribute(PRESSAO_CONDICAO_REFERENCIA_MEDICAO, String.valueOf(request.getParameter(PRESSAO_CONDICAO_REFERENCIA_MEDICAO)));
		model.addAttribute(TEMPERATURA_CONDICAO_REFERENCIA_MEDICAO,
				String.valueOf(request.getParameter(TEMPERATURA_CONDICAO_REFERENCIA_MEDICAO)));
		model.addAttribute(PCS_REFERENCIA_MEDICAO, String.valueOf(request.getParameter(PCS_REFERENCIA_MEDICAO)));
		model.addAttribute(DIRETORIO_IMPORTAR_LEITURA_UPLOAD_MEDICAO,
				String.valueOf(request.getParameter(DIRETORIO_IMPORTAR_LEITURA_UPLOAD_MEDICAO)));
		model.addAttribute(DIRETORIO_REGISTRO_FOTO,
				String.valueOf(request.getParameter(DIRETORIO_REGISTRO_FOTO)));
		model.addAttribute(DIRETORIO_FALHA_REGISTRO_FOTO,
				String.valueOf(request.getParameter(DIRETORIO_FALHA_REGISTRO_FOTO)));	
		model.addAttribute(ARREDONDAMENTO_FATOR_PCS, String.valueOf(request.getParameter(ARREDONDAMENTO_FATOR_PCS)));
		model.addAttribute(MEDIR_ROTA_FISCALIZACAO, String.valueOf(request.getParameter(MEDIR_ROTA_FISCALIZACAO)));
		model.addAttribute(UNIDADE_PRESSAO_MEDICAO, String.valueOf(request.getParameter(UNIDADE_PRESSAO_MEDICAO)));
		model.addAttribute(UNIDADE_TEMPERATURA_MEDICAO, String.valueOf(request.getParameter(UNIDADE_TEMPERATURA_MEDICAO)));
		model.addAttribute(QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA,
				String.valueOf(request.getParameter(QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA)));
		model.addAttribute(QUANTIDADE_MEDICAO_HORARIA_PARA_MEDIA,
				String.valueOf(request.getParameter(QUANTIDADE_MEDICAO_HORARIA_PARA_MEDIA)));
		model.addAttribute(VALIDA_CONTRATO, String.valueOf(request.getParameter(VALIDA_CONTRATO)));
		model.addAttribute(REGISTRA_HISTORICO_MEDICAO, String.valueOf(request.getParameter(REGISTRA_HISTORICO_MEDICAO)));
		model.addAttribute(CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS, String.valueOf(request.getParameter(CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS)));
		
	}

	/**
	 * Método responsável por popular dados relacionados a aba Contrato
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaContratoTela(HttpServletRequest request, Model model) {
		model.addAttribute(ANO_NOVOS_CONTRATOS, String.valueOf(request.getParameter(ANO_NOVOS_CONTRATOS)));
		model.addAttribute(SEQ_NUM_CONTRATO, String.valueOf(request.getParameter(SEQ_NUM_CONTRATO)));
		model.addAttribute(REINICIAR_SEQ_NUM_CONTRATO_ANUAL_CONTRATO,
				String.valueOf(request.getParameter(REINICIAR_SEQ_NUM_CONTRATO_ANUAL_CONTRATO)));
		model.addAttribute(DIAS_ALERTA_RENOVACAO_CONTRATUAL_CONTRATO,
				String.valueOf(request.getParameter(DIAS_ALERTA_RENOVACAO_CONTRATUAL_CONTRATO)));
		model.addAttribute(ID_PERIODICIDADE_CONTRATO, String.valueOf(request.getParameter(ID_PERIODICIDADE_CONTRATO)));
		model.addAttribute(CONSIDERAR_PERIODO_TESTES_CONTRATO, String.valueOf(request.getParameter(CONSIDERAR_PERIODO_TESTES_CONTRATO)));
		model.addAttribute(OBS_IMPRESSA_NOTA_DEBITO_CONTRATO, String.valueOf(request.getParameter(OBS_IMPRESSA_NOTA_DEBITO_CONTRATO)));
		model.addAttribute(ID_TARIFA_REC_COMPROMISSO_CONTRATO, String.valueOf(request.getParameter(ID_TARIFA_REC_COMPROMISSO_CONTRATO)));
		model.addAttribute(MOTIVO_FIM_RELACIONAMENTO_CLI_ENCERRAR_CONTRATO,
				String.valueOf(request.getParameter(MOTIVO_FIM_RELACIONAMENTO_CLI_ENCERRAR_CONTRATO)));
		model.addAttribute(CONSIDERAR_PERIODO_TESTES_CONTRATO, String.valueOf(request.getParameter(CONSIDERAR_PERIODO_TESTES_CONTRATO)));
		model.addAttribute(CONTATO_IMOVEL_OBRIGATORIO, String.valueOf(request.getParameter(CONTATO_IMOVEL_OBRIGATORIO)));
		model.addAttribute(SEQUENCIA_PROPOSTA, String.valueOf(request.getParameter(SEQUENCIA_PROPOSTA)));
		model.addAttribute(ENVIA_PROPOSTA_EMAIL, String.valueOf(request.getParameter(ENVIA_PROPOSTA_EMAIL)));
		model.addAttribute(DESATIVAR_PONTO_CONTRATO, String.valueOf(request.getParameter(DESATIVAR_PONTO_CONTRATO)));
		model.addAttribute(CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO, String.valueOf(request.getParameter(CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO)));

	}

	/**
	 * Método responsável por popular dados relacionados a aba Cadastro
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaCadastroTela(HttpServletRequest request, Model model) {
		model.addAttribute(INDICADOR_VALIDAR_MASCARA_INSCRICAO_ESTADUAL,
				String.valueOf(request.getParameter(INDICADOR_VALIDAR_MASCARA_INSCRICAO_ESTADUAL)));
		model.addAttribute("esferaPoderMinicipalCadastro", String.valueOf(request.getParameter("esferaPoderMinicipalCadastro")));
		model.addAttribute(MASCARA_INSCRICAO_ESTADUAL_CADASTRO, String.valueOf(request.getParameter(MASCARA_INSCRICAO_ESTADUAL_CADASTRO)));
		model.addAttribute(UNIDADE_FEDERACAO_IMPLANTACAO_CADASTRO,
				String.valueOf(request.getParameter(UNIDADE_FEDERACAO_IMPLANTACAO_CADASTRO)));
		model.addAttribute(TIPO_ENDERECO_CORRESPONDENCIA_CADASTRO,
				String.valueOf(request.getParameter(TIPO_ENDERECO_CORRESPONDENCIA_CADASTRO)));
		model.addAttribute(EXIGE_MATRICULA_FUNCIONARIO, String.valueOf(request.getParameter(EXIGE_MATRICULA_FUNCIONARIO)));
		model.addAttribute(EXIGE_EMAIL_PRINCIPAL_PESSOA, String.valueOf(request.getParameter(EXIGE_EMAIL_PRINCIPAL_PESSOA)));
		model.addAttribute(ACEITA_INCLUSAO_PONTO_CONSUMO_ROTA, String.valueOf(request.getParameter(ACEITA_INCLUSAO_PONTO_CONSUMO_ROTA)));
	}

	/**
	 * Método responsável por popular dados relacionados a aba Geral
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 */
	private void popularAbaGeralTela(HttpServletRequest request, Model model) {
		model.addAttribute(DIRETORIO_RELATORIO_PDF, String.valueOf(request.getParameter(DIRETORIO_RELATORIO_PDF)));
		model.addAttribute(HORA_INICIO_DIA_GERAL, String.valueOf(request.getParameter(HORA_INICIO_DIA_GERAL)));
		model.addAttribute(MUNICIPIO_IMPLANTADO_GERAL, String.valueOf(request.getParameter(MUNICIPIO_IMPLANTADO_GERAL)));
		model.addAttribute(QTD_ANOS_CALENDARIO_GERAL, String.valueOf(request.getParameter(QTD_ANOS_CALENDARIO_GERAL)));
		model.addAttribute(QTD_ESCALA_CALCULO_GERAL, String.valueOf(request.getParameter(QTD_ESCALA_CALCULO_GERAL)));
		model.addAttribute(QTD_DECIM_APRESENT_CONSUMO_APURADO,
				String.valueOf(request.getParameter(QTD_DECIM_APRESENT_CONSUMO_APURADO)));
		model.addAttribute(CARACTERE_NUM_GERAL, String.valueOf(request.getParameter(CARACTERE_NUM_GERAL)));
		model.addAttribute(CARACTERE_MINUSCULO_GERAL, String.valueOf(request.getParameter(CARACTERE_MINUSCULO_GERAL)));
		model.addAttribute(CARACTERE_MAIUSCULO_GERAL, String.valueOf(request.getParameter(CARACTERE_MAIUSCULO_GERAL)));
		model.addAttribute(TAM_MIN_SENHA_GERAL, String.valueOf(request.getParameter(TAM_MIN_SENHA_GERAL)));
		model.addAttribute(TAM_MAX_SENHA_GERAL, String.valueOf(request.getParameter(TAM_MAX_SENHA_GERAL)));
		model.addAttribute(PERIODICIDADE_SENHA_RENOVACAO_GERAL,
				String.valueOf(request.getParameter(PERIODICIDADE_SENHA_RENOVACAO_GERAL)));
		model.addAttribute(ACEITA_CARACTER_ESPECIAL, String.valueOf(request.getParameter(ACEITA_CARACTER_ESPECIAL)));
		model.addAttribute(CAIXA_ALTA, String.valueOf(request.getParameter(CAIXA_ALTA)));
		model.addAttribute(SERVIDOR_SMTP_HOST_GERAL, String.valueOf(request.getParameter(SERVIDOR_SMTP_HOST_GERAL)));
		model.addAttribute(SERVIDOR_SMTP_PORT_GERAL, String.valueOf(request.getParameter(SERVIDOR_SMTP_PORT_GERAL)));
		model.addAttribute(SERVIDOR_SMTP_USER_GERAL, String.valueOf(request.getParameter(SERVIDOR_SMTP_USER_GERAL)));
		model.addAttribute(SERVIDOR_SMTP_PASSAWORD_GERAL, String.valueOf(request.getParameter(SERVIDOR_SMTP_PASSAWORD_GERAL)));
		model.addAttribute(SERVIDOR_AUTENTICACAO_AD_HOST, String.valueOf(request.getParameter(SERVIDOR_AUTENTICACAO_AD_HOST)));
		model.addAttribute(SERVIDOR_AUTENTICACAO_AD_PORTA, String.valueOf(request.getParameter(SERVIDOR_AUTENTICACAO_AD_PORTA)));
		model.addAttribute(SERVIDOR_AUTENTICACAO_AD_DOMINIO, String.valueOf(request.getParameter(SERVIDOR_AUTENTICACAO_AD_DOMINIO)));
		model.addAttribute(AUTENTICACAO_POR_AD_WINDOWS, String.valueOf(request.getParameter(AUTENTICACAO_POR_AD_WINDOWS)));
		model.addAttribute(EMAIL_REMETENTE_DEFAULT_GERAL, String.valueOf(request.getParameter(EMAIL_REMETENTE_DEFAULT_GERAL)));
		model.addAttribute(SERVIDOR_SMTP_AUTH_GERAL, String.valueOf(request.getParameter(SERVIDOR_SMTP_AUTH_GERAL)));
		model.addAttribute(SERVIDOR_EMAIL_CONFIGURADO, String.valueOf(request.getParameter(SERVIDOR_EMAIL_CONFIGURADO)));
		model.addAttribute(ASSINATURA_EMAIL, String.valueOf(String.valueOf(request.getParameter(ASSINATURA_EMAIL))));
		model.addAttribute(LOCAL_HOST_GERAL, String.valueOf(request.getParameter(LOCAL_HOST_GERAL)));
		model.addAttribute(QUANTIDADE_ANOS_VIGENCIA_FERIADO_GERAL,
				String.valueOf(request.getParameter(QUANTIDADE_ANOS_VIGENCIA_FERIADO_GERAL)));
		model.addAttribute(REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO_GERAL,
				String.valueOf(request.getParameter(REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO_GERAL)));
		model.addAttribute(TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO,
				String.valueOf(request.getParameter(TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO)));
		model.addAttribute(TAMANHO_MAXIMO_ANEXO, String.valueOf(request.getParameter(TAMANHO_MAXIMO_ANEXO)));
		model.addAttribute(CREDENCIAL_SMS, String.valueOf(request.getParameter(CREDENCIAL_SMS)));
		model.addAttribute(PROJETO_SMS, String.valueOf(request.getParameter(PROJETO_SMS)));
		model.addAttribute(USUARIO_SMS, String.valueOf(request.getParameter(USUARIO_SMS)));
		model.addAttribute(MENSAGEM_SMS, String.valueOf(request.getParameter(MENSAGEM_SMS)));
		model.addAttribute(PROXY_HOST_SMS, String.valueOf(request.getParameter(PROXY_HOST_SMS)));
		model.addAttribute(PROXY_PORTA_SMS, String.valueOf(request.getParameter(PROXY_PORTA_SMS)));
		model.addAttribute(ENVIA_SMS, String.valueOf(request.getParameter(ENVIA_SMS)));
		model.addAttribute(DIAS_EMAIL_CHAMADOS_ATRASADOS, String.valueOf(request.getParameter(DIAS_EMAIL_CHAMADOS_ATRASADOS)));
		model.addAttribute(LATITUDE_EMPRESA, String.valueOf(request.getParameter(LATITUDE_EMPRESA)));
		model.addAttribute(LONGITUDE_EMPRESA, String.valueOf(request.getParameter(LONGITUDE_EMPRESA)));
		model.addAttribute(INDICADOR_AMBIENTE_PRODUCAO, String.valueOf(request.getParameter(INDICADOR_AMBIENTE_PRODUCAO)));
	}

	/**
	 * Atualizar sessao criterios campos.
	 *
	 * @param request {@link HttpServletRequest}
	 */
	private void atualizarSessaoCriteriosCampos(HttpServletRequest request) throws GGASException {

		/***
		 * TKT #597 [ Geral ] :: GGAS deverá aceitar nomes acentuados e retirar na geração de arquivos para integração com outros sistemas
		 */
		ParametroSistema ps;
		ps = controladorParametroSistema.obterParametroSistemaPorCodigoSemCache(Constantes.PARAMETRO_CAIXA_ALTA); //.obterParametroPorCodigo(Constantes.PARAMETRO_CAIXA_ALTA);
		String isCaixaAlta = "";
		if (ps != null) {
			isCaixaAlta = ps.getValor();
		}
		request.getSession().setAttribute("isCaixaAlta", isCaixaAlta);

		ps = controladorParametroSistema.obterParametroSistemaPorCodigoSemCache(Constantes.PARAMETRO_ACEITA_CARACTER_ESPECIAL);
		String isPermiteCaracteresEspeciais = "";
		if (ps != null) {
			isPermiteCaracteresEspeciais = ps.getValor();
		}
		request.getSession().setAttribute("isPermiteCaracteresEspeciais", isPermiteCaracteresEspeciais);

		/***
		 * fim TKT #597
		 */
	}

	/**
	 * Método responsável por exibir a tela de parametros.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirParametrizacao")
	public String exibirParametrizacao(HttpServletRequest request, Model model) throws GGASException {
		this.saveToken(request);

		try {
	        request.getSession().removeAttribute("arquivoCertificado");
	        request.getSession().removeAttribute("senhaCertificado");
			this.configurarDadosTelaParametrizacao(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirParametrizacao";
	}

	/**
	 * Configurar dados tela parametrizacao.
	 *
	 * @param model {@link Model}
	 */
	private void configurarDadosTelaParametrizacao(Model model) throws GGASException {

		this.popularAbaGeral(model);
		this.popularAbaContrato(model);
		this.popularAbaArrecadacao(model);
		this.popularAbaMedicao(model);
		this.popularAbaFaturamento(model);
		this.popularAbaCobranca(model);
		this.popularAbaCadastro(model);
		this.popularAbaContabilidade(model);
		this.popularAbaAtendimento(model);
		this.popularAbaAgenciaVirtual(model);
		this.popularAbaSeguranca(model);
	}

	/**
	 * Popular aba contrato.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaContrato(Model model) throws GGASException {

		model.addAttribute("listaRubricaEmUso", controladorRubrica.listarRubricasEmUsoSistema());
		model.addAttribute("listaBaseApuracao", controladorEntidadeConteudo.listarBasesApuracaoPenalidadeMaiorMenor());
		model.addAttribute("listarFormaCobranca", controladorEntidadeConteudo.listarFormaCobranca());
		model.addAttribute("listaSituacaoContrato", controladorContrato.listarSituacaoContrato());
		model.addAttribute("listaPenalidades", controladorApuracaoPenalidade.listarPenalidades());
		model.addAttribute("listaConsumoReferencial", controladorEntidadeConteudo.obterListaConsumoReferencial());
		Tarifa naoSeAplica = new TarifaImpl();
		naoSeAplica.setChavePrimaria(Long.valueOf(-1));
		naoSeAplica.setDescricao("Não se Aplica");
		Collection<Tarifa> taritas = controladorTarifa.listarTarifas();
		taritas.add(naoSeAplica);
		model.addAttribute("listaTarifas", taritas);
		model.addAttribute("listaTipoRelacionamentoClienteImovel", controladorImovel.listarTipoRelacionamentoClienteImovel());
		model.addAttribute("listaMotivoFimRelacionamentoClienteImovel", controladorImovel.listarMotivoFimRelacionamentoClienteImovel());

		this.popularForm(this.obterMapaParametroAtributoFormContrato(), model);
	}

	/**
	 * Popular aba arrecadacao.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaArrecadacao(Model model) throws GGASException {

		model.addAttribute("listaRecebimentoSituacao", controladorRecebimento.listarRecebimentoSituacaoExistente());
		model.addAttribute("listaBaixaRecebimento",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_ACAO_RECEBIMENTO_A_MENOR));
		model.addAttribute("listaSituacaoPagamento",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_SITUACAO_PAGAMENTO));
		model.addAttribute("listaTipoConvenio",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_FORMA_ARRECADACAO));
		model.addAttribute("listaCreditoDebitoOrigem", controladorCreditoDebito.listarCreditoOrigem());
		model.addAttribute("listaRubricaEmUso", controladorRubrica.listarRubricasEmUsoSistema());

		this.popularForm(this.obterMapaParametroAtributoFormArrecadacao(), model);
	}

	/**
	 * Popular aba geral.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaGeral(Model model) throws GGASException {

		model.addAttribute("listaMunicipio", controladorMunicipio.listarMunicipios());
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaMedidasArredondamento",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_ARREDONDAMENTO));
		this.popularForm(this.obterMapaParametroAtributoFormGeral(), model);
	}

	/**
	 * Popular aba cobranca.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaCobranca(Model model) throws GGASException {

		model.addAttribute("listaAmortizacao", controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_AMORTIZACAO));
		model.addAttribute("listaCobrancaDebitoSituacao", controladorCobranca.listarCobrancaDebitoSituacao());
		model.addAttribute("listaTipoDocumento", controladorArrecadacao.listarTipoDocumentoExistente());
		model.addAttribute("listaEncargoTributario",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_ENCARGOS_TRIBUTARIOS));

		this.popularForm(this.obterMapaParametroAtributoFormCobranca(), model);
	}

	/**
	 * Popular aba medicao.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaMedicao(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormMedicao(), model);

		model.addAttribute(LISTA_TIPO_MEDICAO, controladorHistoricoMedicao.listarTipoMedicao());
		model.addAttribute(LISTA_TIPO_CONSUMO, controladorHistoricoConsumo.listarTiposConsumo());
		model.addAttribute(LISTA_CLASSE_UNIDADE, controladorEntidadeConteudo.listarClasseUnidade());
		model.addAttribute(LISTA_OPERACAO_MEDIDOR, controladorMedidor.listarOperacaoMedidor());
		model.addAttribute(LISTA_SITUACAO_MEDIDOR, controladorMedidor.listarSituacaoMedidor());
		model.addAttribute(LISTA_SITUACAO_LEITURA, controladorLeituraMovimento.listarSituacaoLeitura());
		model.addAttribute(LISTA_ANORMALIDADE_CONSUMO, controladorAnormalidade.listarAnormalidadesConsumo());
		model.addAttribute(LISTA_TIPO_COMPOSICAO_CONSUMO, controladorHistoricoConsumo.listarTipoComposicaoConsumo());
		model.addAttribute(LISTA_TIPO_DADO_ENDERECO_REMOTO, controladorMedidor.listarTipoDadosEnderecoRemoto());
		model.addAttribute(LISTA_TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO, controladorMedidor.listarTipoIntegracao());
		model.addAttribute(LISTA_NUMERO_DIGITOS_CONSUMO_CORRETOR_VAZAO, controladorMedidor.listarNumeroDigitosConsumo());
		model.addAttribute(LISTA_TIPO_MEDIDOR_MOTIVO_MOVIMENTACAO_ENTRADA, controladorMedidor.listarMotivoMovimentacaoMedidor());
		model.addAttribute(LISTA_UNIDADE_PRESSAO_MEDICAO, controladorUnidade.listarUnidadesPressao());
		model.addAttribute(LISTA_UNIDADE_TEMPERATURA_MEDICAO, controladorUnidade.listarUnidadesTemperatura());
	}

	/**
	 * Popular aba faturamento.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaFaturamento(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormFaturamento(), model);

		model.addAttribute(LISTA_TIPO_AGRUPAMENTO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_TIPO_AGRUPAMENTO));
		model.addAttribute(LISTA_CREDITO_DEBITO_SITUACAO, controladorCreditoDebito.listarCreditoDebitoSituacao());
		model.addAttribute(LISTA_ENTIDADE_CLASSE, controladorEntidadeConteudo.listarEntidadeClasse());
		model.addAttribute(LISTA_FINANCIAMENTO_TIPO, controladorRubrica.listarFinanciamentoTipo());
		model.addAttribute(LISTA_INDICE_FINANCEIRO, controladorTarifa.listaIndiceFinanceiro());
		model.addAttribute(LISTA_CREDITO_ORIGEM, controladorCreditoDebito.listarCreditoOrigem());
		model.addAttribute(LISTA_RUBRICA, controladorRubrica.listarRubricas());
		model.addAttribute(LISTA_TIPO_DOCUMENTO, controladorArrecadacao.listarTipoDocumento());
		model.addAttribute(LISTA_FORMA_COBRANCA_PARCELAMENTO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_FORMA_COBRANCA_PARCELAMENTO));
		model.addAttribute(LISTA_FORMA_INCLUSAO_FATURA,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_FORMA_INCLUSAO_FATURA));
		model.addAttribute(LISTA_PERIOTICIDADE_COBRANCA,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_PERIODICIDADE_COBRANCA));
		model.addAttribute(LISTA_TIPO_FATURAMENTO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_TIPO_FATURAMENTO));
		model.addAttribute(LISTA_TIPO_OPERACAO, controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_TIPO_OPERACAO));
		model.addAttribute(LISTA_MOTIVO_CANCELAMENTO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_MOTIVO_CANCELAMENTO));
		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_ITEM_FATURA));
		model.addAttribute(LISTA_REAJUSTE_CASCATA,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_REAJUSTE_CASCATA));
		model.addAttribute(LISTA_TIPO_CREDITO_DEBITO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_TIPO_CREDITO_DEBITO));
		model.addAttribute(LISTA_TIPO_SUBSTITUTO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_TIPO_SUSBTITUTO));
		model.addAttribute(LISTA_TRIBUTOS, controladorTributo.listarTributos());
		model.addAttribute(LISTA_VOLUME_REFERENCIA_TARIFA_MEDIA_TOP,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_VOLUME_REFERENCIA_TARIFA_MEDIA_TOP));
		model.addAttribute(LISTA_LANCAMENTO_ITEM_CONTABIL, controladorRubrica.listarLancamentoItemContabil());
		model.addAttribute(LISTA_SITUACAO_TRIBUTARIA_PIS_COFINS,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_SITUACAO_TRIBUTARIA_PIS_COFINS));
		model.addAttribute(LISTA_REGIME_TRIBUTARIO,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_REGIME_TRIBUTARIO));
		model.addAttribute(LISTA_STATUS_NFE,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_STATUS_NOTA_FISCAL_ELETRONICA));
		model.addAttribute(LISTA_INDICADOR_AMBIENTE_NFE,
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_AMBIENTE_SISTEMA_NFE));
		model.addAttribute(LISTA_STATUS_TARIFA, controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_STATUS_TARIFA));
		model.addAttribute(LISTA_VIGENCIA_SUBSTITUICAO_TRIBUTARIA,
				controladorEntidadeConteudo.obterListaTipoVigenciaSubstituicaoTributaria());
		model.addAttribute(LISTA_CREDITO_DEBITO_SITUACAO, controladorCreditoDebito.listarCreditoDebitoSituacao());
		model.addAttribute("listaOpcaoDiaVencimento",
				controladorEntidadeConteudo.listarEntidadeConteudo(EntidadeClasse.CODIGO_OPCAO_FASE_REFERENC_VENCIMENTO));
		model.addAttribute("listaUsuarios", controladorUsuario.consultarUsuarios(null));
	}

	/**
	 * Popular aba cadastro.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaCadastro(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormCadastro(), model);
		model.addAttribute(LISTA_SITUACAO_IMOVEL, controladorImovel.listarImovelSituacao());
		model.addAttribute(LISTA_UNIDADE_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		model.addAttribute(LISTA_TIPO_ENDERECO, controladorCliente.listarTipoEndereco());
		model.addAttribute(LISTA_ESFERA_PODER, controladorCliente.listarEsferaPoder());
		model.addAttribute("listaSituacoesPontoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
	}

	/**
	 * Popular form.
	 *
	 * @param mapaParametroAtributoForm {@link Map}
	 * @param model {@link Model}
	 */
	private void popularForm(Map<String, String> mapaParametroAtributoForm, Model model) throws GGASException {

		Map<String, Object> mapaAtributoFormValor = model.asMap();

		for (Map.Entry<String, String> entry : mapaParametroAtributoForm.entrySet()) {
			String nomeAtributo = entry.getValue();
			String valorAtributo = String.valueOf(mapaAtributoFormValor.get(nomeAtributo));

			if ("null".equals(valorAtributo) || StringUtils.isEmpty(valorAtributo)) {
				ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(entry.getKey());
				if (parametro != null) {
					valorAtributo = parametro.getValor();

					if (REFERENCIA_FATURAMENTO.equals(nomeAtributo) || REFERENCIA_CONTABIL_GERAL.equals(nomeAtributo)) {
						valorAtributo = Util.formatarAnoMes(Integer.parseInt(valorAtributo));
					} else if (TIPO_ARREDONDAMENTO_CALCULO_GERAL.equals(nomeAtributo)
							|| TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO.equals(nomeAtributo)) {
						EntidadeConteudo entidadeConteudo =
								controladorEntidadeConteudo.obterPorCodigo(valorAtributo, EntidadeClasse.CODIGO_ARREDONDAMENTO);
						valorAtributo = String.valueOf(entidadeConteudo.getChavePrimaria());
					}
				}
			}
			model.addAttribute(nomeAtributo, valorAtributo);
		}
	}

	/**
	 * Obter dados form.
	 *
	 * @param mapaParametroAtributoForm {@link Map}
	 * @param model {@link Model}
	 */
	private Map<String, String> obterDadosForm(Map<String, String> mapaParametroAtributoForm, Model model) throws GGASException {

		Map<String, String> mapa = new TreeMap<String, String>();
		Map<String, Object> mapaAtributosForm = model.asMap();

		for (Map.Entry<String, String> entry : mapaParametroAtributoForm.entrySet()) {
			String keyForm = entry.getValue();
			String valor = String.valueOf(mapaAtributosForm.get(keyForm));

			if (valor != null && StringUtils.isNotEmpty(valor.trim())) {
				if (REFERENCIA_FATURAMENTO.equals(keyForm) || REFERENCIA_CONTABIL_GERAL.equals(keyForm)) {
					valor = Util.formatarReferenciaMesAno(valor);
				} else if (TIPO_ARREDONDAMENTO_CALCULO_GERAL.equals(keyForm) || TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO.equals(keyForm)) {
					EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obter(Long.valueOf(valor));
					valor = entidadeConteudo.getCodigo();
				}

				mapa.put(entry.getKey(), valor);
			} else {
				mapa.put(entry.getKey(), null);
			}

		}

		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form medicao.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormMedicao() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put("CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO", ANORMALIDADE_CONSUMO_ZERO);
		mapa.put(Constantes.PARAMETRO_MODULO_MEDICAO_CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO, TIPO_COMPOSICAO_CONSUMO_PADRAO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_MODULO_MEDICAO_EXIBE_COLUNAS_SUPERVISORIO, EXIBE_COLUNAS_SUPERVISORIO);
		mapa.put(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1, SUPERVISORIOFATORZVALORFIXO1);
		mapa.put(Constantes.PARAMETRO_EXIGE_COMENTARIO_ANALISE_EXCECAO, COMENTARIO_ANALISE_EXCECAO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_EXIGE_COMENTARIO_MEDICAO_DIARIA_HORARIA_SUPERVISORIO, COMENTARIO_MEDICAO_DIARIA_HORARIA);
		mapa.put(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS, TIPO_INTEGRACAO_SISTEMA_SUPERVISORIO);
		mapa.put(Constantes.PARAMETRO_TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIOS, TIPO_DADO_ENDERECO_REMOTO);
		mapa.put(Constantes.PARAMETRO_NUMERO_MAXIMO_DIGITOS_CORRETOR, NUMERO_DIGITOS_CORRETOR_VAZAO);
		mapa.put(Constantes.PARAMETRO_PERMITE_ALTERACAO_PCS_CITY_GATE_FATURADO, PERMITE_ALTERACAO_PCS_FATURADO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_PERMITE_ALTERACAO_PCSZ_IMOVEL, PERMITE_ALTERACAO_PCSZ_MEDICAO);
		mapa.put(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO, TIPO_ARREDONDAMENTO_CONSUMO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_PRESSAO_CONDICAO_REFERENCIA, PRESSAO_CONDICAO_REFERENCIA_MEDICAO);
		mapa.put(Constantes.PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA, TEMPERATURA_CONDICAO_REFERENCIA_MEDICAO);
		mapa.put(Constantes.PARAMETRO_PCS_REFERENCIA, PCS_REFERENCIA_MEDICAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_IMPORTAR_LEITURA_UPLOAD, DIRETORIO_IMPORTAR_LEITURA_UPLOAD_MEDICAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_REGISTRO_FOTO, DIRETORIO_REGISTRO_FOTO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_FALHA_REGISTRO_FOTO, DIRETORIO_FALHA_REGISTRO_FOTO);
		mapa.put(Constantes.PARAMETRO_ARREDONDAMENTO_FATOR_PCS, ARREDONDAMENTO_FATOR_PCS);
		mapa.put(Constantes.PARAMETRO_MEDIR_ROTA_FISCALIZACAO, MEDIR_ROTA_FISCALIZACAO);
		mapa.put(Constantes.PARAMETRO_UNIDADE_PRESSAO_MEDICAO, UNIDADE_PRESSAO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO, UNIDADE_TEMPERATURA_MEDICAO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA, QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA, QUANTIDADE_MEDICAO_HORARIA_PARA_MEDIA);
		mapa.put(Constantes.PARAMETRO_VALIDA_CONTRATO, VALIDA_CONTRATO);
		mapa.put(Constantes.PARAMETRO_REGISTRA_HISTORICO_MEDICAO, REGISTRA_HISTORICO_MEDICAO);
		mapa.put(Constantes.PARAMETRO_CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS, CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS);

		

		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form faturamento.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormFaturamento() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR, MOTIVO_REFATURAR_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_CODIGO_MOTIVO_INCLUSAO_FATURAR_NORMAL, MOTIVO_INCLUSAO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO, DEBITO_MOTIVO_CANCELAMENTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_CREDITO_DEBITO_SITUACAO_NORMAL, DEBITO_SITUACAO_NORMAL_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_FATURA, DIRETORIO_ARQUIVOS_FATURA_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO, DIRETORIO_ARQUIVOS_SIMULACAO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_FILTRO_ITEM_FATURA_PRECO_GAS, FILTRO_ITEM_FATURA_PRECO_GAS_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_GERA_CREDITO_VOLUME_AUTOMATICAMENTE, CREDITO_VOLUME_AUTOMATICAMENTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_METODO_DE_REAJUSTE_CASCATA_PROPORCIONAL_COMPLEMENTAR,
				REAJUSTE_COMPLEMENTAR_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO, NUMERO_DIAS_MAXIMO_PRORROGACAO_VENCIMENTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_NUMERO_DIAS_MINIMO_EMISSAO_VENCIMENTO, DIAS_MINIMO_VENCIMENTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_INTERVALO_MAXIMO_COBRANCA_FATURA, INTERVALO_MAXIMO_COBRANCA_FATURA_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QTD_DIAS_CANCELAMENTO_NOTA_FISCAL, QTDA_DIAS_CANCELAMENTO_NOTA_FISCAL_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_DE_FATURA_POR_LOTE, QTDA_FATURA_LOTE_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_INFORMADO_CLIENTE, QTDA_MAX_FATURAMENTO_INFORMADO_CLIENTE_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_FATURAMENTO_MEDIA, QTDA_MAX_FATURAMENTO_MEDIA_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS, QTDA_MAX_CRONOGRAMAS_ABERTOS_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS, QTDA_MIN_CRONOGRAMAS_ABERTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_PRAZO_MIN_ENTREGA_FATURA_VENCIMENTO, PRAZO_MIN_PARA_ENTRAGA_FATURA_VENCIMENTO);
		mapa.put(Constantes.PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR, TAMANHO_DESC_COMPLEMENTAR_CRE_DEB_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_NOTA, TAMANHO_DESC_COMPLEMENTAR_NOTA_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA, TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_DESTACADO,
				TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA_FATURAMENTO_DESTACADO);
		mapa.put(Constantes.P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA, VIGENCIA_SUBSTITUICAO_TRIBUTARIA);
		mapa.put(Constantes.PARAMETRO_TEXTO_EMAIL_FATURA, TEXTO_EMAIL_FATURA);

		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR, TIPO_DOC_CREDITO_REALIZAR_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_MODULO_FATURAMENTO_PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO, PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO);
		mapa.put(Constantes.PARAMETRO_TIPO_SUBSTITUTO, TIPO_SUBSTITUTO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_VALOR_MINIMO_GERACAO_DOCUMENTO_COBRANCA, VALOR_MIN_GERACAO_DOC_COB_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_VOLUME_REF_TARIFA_MEDIA, VOLUME_REF_TARIFA_MEDIA_FATURAMENTO);

		mapa.put(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO, REFERENCIA_FATURAMENTO);

		mapa.put(Constantes.PARAMETRO_REGIME_TRIBUTARIO, REGIME_TRIBUTARIO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_NR_VERSAO_XML_NFE, NR_VERSAO_XMLNFE_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_CODIGO_INDICADOR_AMBIENTE_SISTEMA_NFE, INDICADOR_AMBIENTE_SISTEMA_NFE_FATURAMENTO);

		mapa.put(Constantes.PARAMETRO_USUARIO_GGAS, USUARIO_FATURAMENTO);
		mapa.put(Constantes.PARAMETRO_EMISSAO_SERIE_ELETRONICA, EMISSAO_SERIE_ELETRONICA);
		mapa.put(Constantes.QTD_DIAS_FIXO_ESTENDIDO, QUANTIDADE_DIAS_APRESENTACAO_VENCIMENTO);
		mapa.put(Constantes.PARAMETRO_FATURA_VALOR_ZERO, FATURA_VALOR_ZERO);
		mapa.put(Constantes.PARAMETRO_CONCEDE_DESCONTO_INADIMPLENTE, CONCEDE_DESCONTO_INADIMPLENTE);
		mapa.put(Constantes.AGRUPAR_VOLUMES_IMPRESSAO_FATURA_MAIS_TARIFA_CICLO, SOMAR_VOLUME_FATURA_IMPRESSAO);
		mapa.put(Constantes.PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO, DIRETOR_FINANCEIRO_TEXTO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_SEFAZ, DIRETORIO_ARQUIVOS_SEFAZ);
		mapa.put(Constantes.PARAMETRO_UTILIZAR_MULTIPLOS_CICLOS, UTILIZAR_MULTIPLOS_CICLOS);
		mapa.put(Constantes.PARAMETRO_FATURA_CONSUMO_ZERO, FATURA_CONSUMO_ZERO);
		mapa.put(Constantes.PARAMETRO_CRIAR_CRONOGRAMAS_ENCERRAR, CRIAR_CRONOGRAMAS_ENCERRAR);
		mapa.put(Constantes.INDICADOR_FATURAMENTO_PARALELO, INDICADOR_FATURAMENTO_PARALELO);
		mapa.put(Constantes.INDICADOR_CONTABILIZAR_LANCAMENTO_CONTABIL, INDICADOR_CONTABILIZACAO_LANCAMENTOS);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_MESES_PRESCRICAO, QUANTIDADE_MESES_PRESCRICAO);
		mapa.put(Constantes.PARAMETRO_CANCELA_FATURA_APOS_PRAZO, CANCELA_FATURA_APOS_PRAZO);
		mapa.put(Constantes.PARAMETRO_CALCULAR_COLETOR_TARIFA_DIARIA, CALCULAR_MEDIA_DIARIA_COLETOR);
		mapa.put(Constantes.PARAMETRO_PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO, PORCENTAGEM_CONSIDERADA_FATURAMENTO_ELEVADO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_REFERENCIAS_CALCULO_MEDIA, QUANTIDADE_REFERENCIAS_CALCULO_MEDIA);

			
		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form contrato.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormContrato() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.PARAMETRO_ANO_CONTRATO, ANO_NOVOS_CONTRATOS);
		mapa.put(Constantes.PARAMETRO_SEQUENCIA_CONTRATO, SEQ_NUM_CONTRATO);
		mapa.put(Constantes.PARAMETRO_REINICIO_ANUAL_SEQUENCIA_CONTRATO, REINICIAR_SEQ_NUM_CONTRATO_ANUAL_CONTRATO);
		mapa.put(Constantes.PARAMETRO_ANO_CONTRATUAL, "indicadorAnoContratualContrato");
		mapa.put(Constantes.PARAMETRO_DIAS_ANTECEDENCIA_RENOVACAO_CONTRATUAL, DIAS_ALERTA_RENOVACAO_CONTRATUAL_CONTRATO);
		mapa.put(Constantes.PARAMETRO_PERIODICIDADE_VALIDACAO_COMPROMISSO_TOP_QPNR, ID_PERIODICIDADE_CONTRATO);
		mapa.put(Constantes.PARAMETRO_EXPURGAR_PERIODO_TESTE_APURACAO, CONSIDERAR_PERIODO_TESTES_CONTRATO);
		mapa.put(Constantes.PARAMETRO_MENSAGEM_NOTA_DEBITO_MULTA_RESCISORIA, OBS_IMPRESSA_NOTA_DEBITO_CONTRATO);
		mapa.put(Constantes.PARAMETRO_TARIFA_RECUPERACAO_COMPROMISSO, ID_TARIFA_REC_COMPROMISSO_CONTRATO);
		mapa.put(Constantes.PARAMETRO_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_ENCERRAR_CONTRATO, MOTIVO_FIM_RELACIONAMENTO_CLI_ENCERRAR_CONTRATO);
		mapa.put(Constantes.PARAMETRO_CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO, CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO);

		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form arrecadacao.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormArrecadacao() {

		Map<String, String> mapa = new TreeMap<String, String>();

		mapa.put(Constantes.PARAMETRO_GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE, GERA_CREDITO_RECEB_MAIOR_DUPLICIDADE_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_CODIGO_EMPRESA_FEBRABAN, EMPRESA_FEBRABAN_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_MODULO_ARRECADACAO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE,
				RECEBIMENTO_DOCTO_INEXIS_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_ARRECADACAO, ARQUIVOS_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_REMESSA, ARQUIVOS_REMESSA_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_RETORNO, ARQUIVOS_RETORNO_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_UPLOAD, ARQUIVOS_UPLOAD_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR, DOC_COBRANCA_RECEB_MENOR_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_DIAS_VENCIMENTO_DOCUMENTO_COMPLEMENTAR, QTD_DIAS_VENC_DOC_COMPLEMENTAR_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_VALOR_MIN_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR, VALOR_MIN_DOC_COBRANCA_REC_MENOR_ARRECADACAO);
		mapa.put(Constantes.PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR, ACAO_RECEBIMENTO_A_MENOR);

		mapa.put(Constantes.ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO, "arrecadacaoCampoArquivoCodigoConvenio");
		mapa.put(Constantes.ARRECADACAO_CODIGO_CONVENIO, "arrecadacaoCodigoConvenio");

		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form geral.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormGeral() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.LATITUDE_EMPRESA, LATITUDE_EMPRESA);
		mapa.put(Constantes.LONGITUDE_EMPRESA, LONGITUDE_EMPRESA);
		mapa.put(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF, DIRETORIO_RELATORIO_PDF);
		mapa.put(Constantes.PARAMETRO_HORA_INICIO_DIA, HORA_INICIO_DIA_GERAL);
		mapa.put(Constantes.PARAMETRO_MUNICIPIO_IMPLANTADOS, MUNICIPIO_IMPLANTADO_GERAL);
		mapa.put(Constantes.PARAMETRO_NOME_EMPRESA, NOME_EMPRESA_GERAL);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO, QTD_ANOS_CALENDARIO_GERAL);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO, QTD_ESCALA_CALCULO_GERAL);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO, QTD_DECIM_APRESENT_CONSUMO_APURADO);
		mapa.put(Constantes.PARAMETRO_REFERENCIA_CONTABIL, REFERENCIA_CONTABIL_GERAL);

		mapa.put(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO, TIPO_ARREDONDAMENTO_CALCULO_GERAL);
		mapa.put(Constantes.PARAMETRO_CODIGO_CARACTERE_NUMERO, CARACTERE_NUM_GERAL);
		mapa.put(Constantes.PARAMETRO_CODIGO_CARACTERE_MINUSCULO, CARACTERE_MINUSCULO_GERAL);
		mapa.put(Constantes.PARAMETRO_CODIGO_CARACTERE_MAIUSCULO, CARACTERE_MAIUSCULO_GERAL);
		mapa.put(Constantes.PARAMETRO_TAMANHO_MINIMO_SENHA, TAM_MIN_SENHA_GERAL);
		mapa.put(Constantes.PARAMETRO_TAMANHO_MAXIMO_SENHA, TAM_MAX_SENHA_GERAL);
		mapa.put(Constantes.PARAMETRO_PERIODICIDADE_SENHA_RENOVACAO, PERIODICIDADE_SENHA_RENOVACAO_GERAL);
		mapa.put(Constantes.PARAMETRO_ACEITA_CARACTER_ESPECIAL, ACEITA_CARACTER_ESPECIAL);
		mapa.put(Constantes.PARAMETRO_CAIXA_ALTA, CAIXA_ALTA);

		mapa.put(Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_HOST, SERVIDOR_AUTENTICACAO_AD_HOST);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_PORTA, SERVIDOR_AUTENTICACAO_AD_PORTA);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_DOMINIO, SERVIDOR_AUTENTICACAO_AD_DOMINIO);
		mapa.put(Constantes.PARAMETRO_AUTENTICACAO_POR_AD_WINDOWS, AUTENTICACAO_POR_AD_WINDOWS);

		mapa.put(Constantes.PARAMETRO_SERVIDOR_SMTP_HOST, SERVIDOR_SMTP_HOST_GERAL);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_SMTP_PORT, SERVIDOR_SMTP_PORT_GERAL);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_SMTP_USER, SERVIDOR_SMTP_USER_GERAL);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_SMTP_PASSWD, SERVIDOR_SMTP_PASSAWORD_GERAL);
		mapa.put(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO, EMAIL_REMETENTE_DEFAULT_GERAL);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_SMTP_AUTH, SERVIDOR_SMTP_AUTH_GERAL);
		mapa.put(Constantes.PARAMETRO_ASSINATURA_EMAIL, ASSINATURA_EMAIL);
		mapa.put(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO, SERVIDOR_EMAIL_CONFIGURADO);

		mapa.put(Constantes.PARAMETRO_SERVIDOR_IP, LOCAL_HOST_GERAL);

		mapa.put(Constantes.PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO, QUANTIDADE_ANOS_VIGENCIA_FERIADO_GERAL);
		mapa.put(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO, REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO_GERAL);
		mapa.put(Constantes.PARAMETRO_TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO, TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO);

		mapa.put(Constantes.PARAMETRO_TAMANHO_MAXIMO_ANEXO, TAMANHO_MAXIMO_ANEXO);

		// <!-- parametros para configuração do SMS -->
		mapa.put(Constantes.CREDENCIAL_SMS, CREDENCIAL_SMS);
		mapa.put(Constantes.PROJETO_SMS, PROJETO_SMS);
		mapa.put(Constantes.USUARIO_AUXILIAR_SMS, USUARIO_SMS);
		mapa.put(Constantes.MENSAGEN_SMS, MENSAGEM_SMS);
		mapa.put(Constantes.PROXY_HOST_SMS, PROXY_HOST_SMS);
		mapa.put(Constantes.PROXY_PORTA_SMS, PROXY_PORTA_SMS);
		mapa.put(Constantes.ENVIA_SMS, ENVIA_SMS);

		// Parâmetro Indica se Deve serEnviado Email ao Contato do Imóvel ao Salvar a
		// Proposta
		mapa.put(Constantes.ENVIAR_EMAIL_AO_SALVAR_PROPOSTA, ENVIA_PROPOSTA_EMAIL);

		mapa.put(Constantes.PARAMETRO_NM_DIAS_EMAIL_CHAMADOS_ATRASADOS, DIAS_EMAIL_CHAMADOS_ATRASADOS);
		mapa.put(Constantes.INDICADOR_AMBIENTE_PRODUCAO, INDICADOR_AMBIENTE_PRODUCAO);
		
		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form cobranca.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormCobranca() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.PARAMETRO_OBRIGATORIO_RETORNO_PROTOCOLO, OBRIGATORIO_RETORNO_PROTOCOLO);
		mapa.put(Constantes.PARAMETRO_MODULO_COBRANCA_CREDITO_DEBITO_SITUACAO_PARCELADA, DEBITO_SITUACAO_PARCELADA_COBRANCA);
		mapa.put(Constantes.PARAMETRO_DATA_VENCIMENTO_COBRANCA_CONTRA_APRESENTACAO, DATA_VENC_COBRANCA);
		mapa.put(Constantes.PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL, ENVIAR_EMAIL_AVISO_NOTIFICACAO_CORTE);
		mapa.put(Constantes.PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO, EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO);
		mapa.put(Constantes.PARAMETRO_PARTICIPA_CONVENIO_ECARTAS, PARTICIPA_CONVENIO_ECARTAS);
		mapa.put(Constantes.PARAMETRO_NUMERO_CONTRATO_ECARTAS, NUMERO_CONTRATO_ECARTAS);
		mapa.put(Constantes.PARAMETRO_NUMERO_CARTAO_POSTAL_ECARTAS, NUMERO_CARTAO_POSTAL_ECARTAS);
		mapa.put(Constantes.PARAMETRO_QUANTIDADE_FATURA_POR_LOTE_ECARTAS, QUANTIDADE_FATURA_POR_LOTE_ECARTAS);
		mapa.put(Constantes.QUANTIDADE_CLIENTES_RELATORIO_LOTE, QTD_CLIENTES_RELATORIO_LOTE_COBRANCA);
		mapa.put(Constantes.MENSAGEM_NOTIFICACAO_CORTE, MENSAGEM_NOTIFICACAO_CORTE);
		
		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form cadastro.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormCadastro() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.SEQUENCIA_NUMERO_PROPOSTA, SEQUENCIA_PROPOSTA);
		mapa.put(Constantes.PARAMETRO_VALIDAR_MASCARA_INSCRICAO_ESTADUAL, INDICADOR_VALIDAR_MASCARA_INSCRICAO_ESTADUAL);
		mapa.put(Constantes.PARAMETRO_MASCARA_INSCRICAO_ESTADUAL, MASCARA_INSCRICAO_ESTADUAL_CADASTRO);
		mapa.put(Constantes.PARAMETRO_UNIDADE_FEDERACAO_IMPLANTACAO, UNIDADE_FEDERACAO_IMPLANTACAO_CADASTRO);
		mapa.put(Constantes.PARAMETRO_MODULO_CADASTRO_CODIGO_TIPO_ENDERECO_CORRESPONDENCIA, TIPO_ENDERECO_CORRESPONDENCIA_CADASTRO);
		mapa.put(Constantes.PARAMETRO_EXIGE_MATRICULA_FUNCIONARIO, EXIGE_MATRICULA_FUNCIONARIO);
		mapa.put(Constantes.PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA, ACEITA_INCLUSAO_PONTO_CONSUMO_ROTA);
		mapa.put(Constantes.PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA, EXIGE_EMAIL_PRINCIPAL_PESSOA);

		// Parâmetro Indica Se é Obrigatório o Imóvel Possuir Contato Para Realizar
		// Inclusão Da Proposta
		mapa.put(Constantes.OBRIGATORIO_CONTATO_DO_IMOVEL_PARA_INCLUSAO_PROPOSTA, CONTATO_IMOVEL_OBRIGATORIO);
		mapa.put(Constantes.DESATIVAR_PONTO_CONTRATO, DESATIVAR_PONTO_CONTRATO);
		return mapa;

	}

	/**
	 * Popular aba contabilidade.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaContabilidade(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormContabilidade(), model);
	}

	/**
	 * Popular aba atendimento.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaAtendimento(Model model) throws GGASException {

		QuestionarioVO questionarioVO = new QuestionarioVO();
		questionarioVO.setHabilitado(true);
		questionarioVO.setTipoQuestionarioChamado(true);
		model.addAttribute("listaQuestionario", controladorQuestionario.consultarQuestionario(questionarioVO));

		QuestionarioPergunta pergunta = null;
		ParametroSistema parametroSistema =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_PERGUNTA_QUESTIONARIO_SATISFACAO);
		if (!"null".equals(parametroSistema.getValor()) && StringUtils.isNotBlank(parametroSistema.getValor())) {
			Long perguntaId = Long.parseLong(parametroSistema.getValor());
			if (perguntaId != null && perguntaId > 0) {
				pergunta = ServiceLocator.getInstancia().getControladorQuestionario().obterPergunta(perguntaId);
			}
		}
		model.addAttribute("questionarioPerguntaSatisfacao", pergunta);
		this.popularForm(this.obterMapaParametroAtributoFormAtendimento(), model);
	}

	/**
	 * Popular aba atendimento.
	 *
	 * @param model {@link Model}
	 */
	private void popularAbaAgenciaVirtual(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormAgenciaVirtual(), model);
	}

	/**
	 * Obter mapa parametro atributo form contabilidade.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormContabilidade() {

		Map<String, String> mapa = new TreeMap<String, String>();

		mapa.put(Constantes.PARAMETRO_MASCARA_NUMERO_CONTA, MASCARA_NUMERO_CONTA);
		mapa.put(Constantes.PARAMETRO_CONTABILIZAR_DESCONTO_TARIFA, IND_CONTABILIZA_DESCONTO_TARIFA);
		mapa.put(Constantes.PARAMETRO_PDD_VALOR_LIMITE_ANUAL, PDD_VALOR_LIMITE_ANUAL);
		mapa.put(Constantes.PARAMETRO_PDD_VALOR_LIMITE_SEMESTRAL, PDD_VALOR_LIMITE_SEMESTRAL);

		return mapa;
	}

	/**
	 * Obter mapa parametro atributo form atendimento.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormAtendimento() {

		Map<String, String> mapa = new TreeMap<String, String>();

		mapa.put(Constantes.P_NUMERO_PROTOCOLO_AUTOMATICO, PROTOCOLO_AUTOMATICO);
		mapa.put(Constantes.ACESSO_AGENCIA_VIRTUAL, ACESSO_AGENCIA);
		mapa.put(Constantes.TAMANHO_MAXIMO_SENHA_AGENCIA, TAMANHO_MAX_SENHA_AG);
		mapa.put(Constantes.TAMANHO_MINIMO_SENHA_AGENCIA, TAMANHO_MIN_SENHA_AG);
		mapa.put(Constantes.CODIGO_CARACTERE_MAIUSCULO_AGENCIA, CARACTER_MAIUSCULO_AG);
		mapa.put(Constantes.CODIGO_CARACTERE_MINUSCULO_AGENCIA, CARACTER_MINUSCULO_AG);

		mapa.put(Constantes.P_CHAMADO_MANIFESTACAO_OBRIGATORIO, MANIFESTACAO);
		mapa.put(Constantes.P_PASSAPORTE_CLIENTE, PASSAPORTE);
		mapa.put(Constantes.QUANTIDADE_MESES_INCLUSAO_PROGRAMACAO_CONSUMO, QUANTIDADE_MESES_PROGRAMACAO_CONSUMO);
		mapa.put(Constantes.DIA_LIMITE_PROGRAMACAO_CONSUMO, DIA_LIMITE_PROGRAMACAO_CONSUMO);

		mapa.put(Constantes.ALTERA_VENCIMENTO_FATURA_AUTOMATICO, VENCIMENTO_AUTOMATICO);
		mapa.put(Constantes.ALTERA_VENCIMENTO_FATURA_DEBITO_ATRASO, VENCIMENTO_DEBITO_ATRASO);
		mapa.put(Constantes.PRAZO_ALTERA_VENCIMENTO_FATURA_AUTOMATICO, PRAZO_ALTERACAO_VENCIMENTO);

		mapa.put(Constantes.LIMITE_APROVACAO_PROG_CONSUMO_MENSAL, DIA_LIMITE_APROVACAO_PROGRAMACAO_CONSUMO);
		mapa.put(Constantes.MENSAGEM_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO, MSG_ERRO_QDS_MAIOR_QUE_QDC_TOLERADO);
		mapa.put(Constantes.DATA_LIMITE_RELIGACAO_GAS, DATA_LIMITE_RELIGACAO_GAS);

		mapa.put(Constantes.TELEFONE_OBRIGATORIO_SOLICITACAO_GAS, OBRIGATORIEDADE_TELEFONE);
		mapa.put(Constantes.DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO, DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO);
		mapa.put(Constantes.DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO, DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO);
		mapa.put(Constantes.DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO, DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO);
		mapa.put(Constantes.DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO, DATA_LIMITE_RELIGACAO_GAS_POR_DESLIGAMENTO_INDEVIDO);
		mapa.put(Constantes.PARAMETRO_PERGUNTA_QUESTIONARIO_SATISFACAO, PERGUNTA_SATISFACAO);
		mapa.put(Constantes.NUMERO_DIAS_CHECAGEM_CORTE, NUMERO_DIAS_CHECAGEM_CORTE);
		mapa.put(Constantes.PRAZO_MINIMO_CRITICIDADE, PRAZO_MINIMO_CRITICIDADE);
		mapa.put(Constantes.PRAZO_MAXIMO_CRITICIDADE, PRAZO_MAXIMO_CRITICIDADE);
		mapa.put(Constantes.VALOR_MINIMO_CRITICIDADE, VALOR_MINIMO_CRITICIDADE);
		mapa.put(Constantes.VALOR_MEDIO_CRITICIDADE, VALOR_MEDIO_CRITICIDADE);
		mapa.put(Constantes.VALOR_MAXIMO_CRITICIDADE, VALOR_MAXIMO_CRITICIDADE);

		return mapa;

	}

	/**
	 * Obter mapa parametro atributo form agencia.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormAgenciaVirtual() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL, EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_NOTA_FISCAL);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO, EXIBIR_MENU_AGENCIA_VIRTUAL_CONSUMO_POR_PERIODO);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL,
				EXIBIR_MENU_AGENCIA_VIRTUAL_CONSULTA_DECLARACAO_QUITACAO_ANUAL);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL,
				EXIBIR_MENU_AGENCIA_VIRTUAL_AUTENTICACAO_DECLARACAO_QUITACAO_ANUAL);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO, EXIBIR_MENU_AGENCIA_VIRTUAL_CONTRATO_ADESAO);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL, EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_CADASTRAL);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO, EXIBIR_MENU_AGENCIA_VIRTUAL_ALTERACAO_DATA_VENCIMENTO);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO, EXIBIR_MENU_AGENCIA_VIRTUAL_INTERRUPCAO_FORNECIMENTO);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA,
				EXIBIR_MENU_AGENCIA_VIRTUAL_RELIGACAO_UNIDADE_CONSUMIDORA);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS, EXIBIR_MENU_AGENCIA_VIRTUAL_LIGACAO_GAS);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO, EXIBIR_MENU_AGENCIA_VIRTUAL_PROGRAMACAO_CONSUMO);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA, EXIBIR_MENU_AGENCIA_VIRTUAL_CROMATOGRAFIA);
		mapa.put(Constantes.EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO, EXIBIR_MENU_AGENCIA_VIRTUAL_FALE_CONOSCO);

		return mapa;
	}
	

	private void popularAbaSegurancaTela(HttpServletRequest request, Model model) {
		model.addAttribute(NOME_CERTIFICADO_DIGITAL, String.valueOf(request.getParameter(NOME_CERTIFICADO_DIGITAL)));
	}
	
	private void popularAbaSeguranca(Model model) throws GGASException {

		this.popularForm(this.obterMapaParametroAtributoFormSeguranca(), model);
	}
	
	/**
	 * Obter mapa parametro atributo form agencia.
	 *
	 * @return Map {@link Map}
	 */
	private Map<String, String> obterMapaParametroAtributoFormSeguranca() {

		Map<String, String> mapa = new TreeMap<String, String>();
		mapa.put(Constantes.PARAMETRO_NOME_CERTIFICADO_DIGITAL, NOME_CERTIFICADO_DIGITAL);
		return mapa;
	}
	
	@RequestMapping("validarCertificado")
	public ResponseEntity<String> validarCertificado(@RequestParam("arquivo") MultipartFile arquivo,
			@RequestParam("senhaCertificado") String senhaCertificado, HttpServletRequest request) throws IOException, NegocioException {
		Map<String, String> resposta = new HashMap<String, String>();
		Gson gson = new Gson();
		String jsonObj = "";
		String senhaCodificada = Util.codificarBase64(senhaCertificado);
		Empresa empresa = Fachada.getInstancia().obterEmpresaPrincipal();
		String cnpj = "";
		
		if(empresa != null && empresa.getCliente() != null && !StringUtils.isEmpty(empresa.getCliente().getCnpj())) {
			cnpj = empresa.getCliente().getCnpj();
		}
		
		String nomeArquivo = arquivo.getOriginalFilename();
		Boolean isSenhaValida = Util.validarSenhaCertificado(arquivo.getBytes(), senhaCodificada);
		
		//Boolean isMesmoCnpjEmpresa = Util.validarCnpjCertificado(arquivo.getBytes(), senhaCodificada, cnpj);
		
		if(!isSenhaValida) {
			resposta.put("status", "error");
			resposta.put("message", "Senha nao pertence ao certificado carregado!");
		} else {
			resposta.put("status", "success");
			resposta.put("message", "Certificado validado com sucesso!");
			
		}
        resposta.put("nomeArquivo", nomeArquivo);
        resposta.put("senhaCertificado", senhaCodificada);
        
        jsonObj = gson.toJson(resposta);
        
        request.getSession().setAttribute("arquivoCertificado", arquivo);
        request.getSession().setAttribute("senhaCertificado", senhaCodificada);

		return new ResponseEntity<>(jsonObj, HttpStatus.OK);

	}
}
