/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/03/2015 16:02:00
 @author crsilva
 */

package br.com.ggas.web.contratoadequacaoparceria;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contratoadequacaoparceria.ContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.impl.ContratoAdequacaoParceriaImpl;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas a Adequação do Contrato e
 * Parceria
 *
 */
@Controller
public class ContratoAdequacaoParceriaAction extends GenericAction {

	private static final String CONTRATO_PARCERIA = "contratoParceria";

	private static final String CHECK_PONTO_CONSUMO = "checkPontoConsumo";

	private static final String PERCENTUAL_MULTA = "percentualMulta";

	private static final String DATA_INICIO_VIGENCIA = "dataInicioVigencia";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String ID_IMOVEL = "idImovel";

	private static final String ID_CLIENTE = "idCliente";

	public static final String FLUXO_PAGINAS = "fluxoPaginas";

	public static final String HABILITADO = "habilitado";

	private List<PontoConsumo> listaPontoConsumo;

	private Map<String, Object> filtros;

	@Autowired
	private ControladorContratoAdequacaoParceria controladorContratoAdequacaoParceria;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorTarifa controladorTarifa;

	/**
	 * Método respnsável por exibir a tela de contrado de adequação parceria
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaContratoAdequacao")
	public String exibirPesquisaContratoAdequacao(HttpServletRequest request, Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		// tratarLockPesquisa(request);
		listaPontoConsumo = null;

		return "exibirPesquisaContratoAdequacao";
	}

	/**
	 * Método responsável por pesquisar os contratos de adequação de parceria
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param habilitado
	 *            {@link Boolean}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("pesquisarContratoAdequacaoParceria")
	public String pesquisarContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			BindingResult result, HttpServletRequest request,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, Model model)
			throws NegocioException {

		obterIndicadorPesquisa(request, model);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);

		Map<String, Object> filtro = new HashMap<String, Object>();
		this.prepararFiltroContratoAdequacao(filtro, request, contratoAdequacaoParceria, habilitado, model);
		try {
			model.addAttribute("listaContratosAdequacaoParceria",
					controladorContratoAdequacaoParceria.consultarContratosAdequacaoParceria(filtro));
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}
		saveToken(request);

		return "exibirPesquisaContratoAdequacao";
	}

	/**
	 * Método responsável chamar a tela de inclusão de um novo contrato adequação
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoContratoAdequacao")
	public String exibirInclusaoContratoAdequacao(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		obterIndicadorPesquisa(request, model);

		saveToken(request);

		Map<String, Object> filtro = new HashMap<String, Object>();

		this.prepararFiltroContratoAdequacao(filtro, request, contratoAdequacaoParceria, true, model);

		try {
			this.carregarCampos(contratoAdequacaoParceria, request, true, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		if (filtros != null) {
			model.addAttribute("listaPontoConsumoAssociacao", listaPontoConsumo);
		}

		return "exibirInclusaoContratoAdequacao";
	}

	/**
	 * Método responsável por obter o indicador de pesquisa relacionada a pesquisa.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 */
	private void obterIndicadorPesquisa(HttpServletRequest request, Model model) {
		String indicadorPesquisa = request.getParameter("indicadorPesquisa");
		model.addAttribute("indicadorPesquisa", indicadorPesquisa);
	}

	/**
	 * 
	 * @param contratoAdequacaoParceria
	 * @param request
	 * @param model
	 */
	private void manterDadosTela(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria, HttpServletRequest request,
			Model model) {
		String valorPagoCliente = request.getParameter("valorCliente");
		String prazoEstabelecido = request.getParameter("prazo");
		String percentualMulta = request.getParameter(PERCENTUAL_MULTA);
		String dataInicioVigencia = request.getParameter(DATA_INICIO_VIGENCIA);
		String valorPagoCdl = contratoAdequacaoParceria.getValorPagoPelaCdl();

		model.addAttribute(DATA_INICIO_VIGENCIA, dataInicioVigencia);
		model.addAttribute("valorPagoCliente", valorPagoCliente);
		model.addAttribute("valorPagoPelaCdl", valorPagoCdl);
		model.addAttribute(PERCENTUAL_MULTA, percentualMulta);
		model.addAttribute("prazoEstabelecido", prazoEstabelecido);
	}

	/**
	 * Método responsável por incluir contrato adequacao parceria ou redirecionar
	 * para a tela de inclusão caso algum erro ocorra.
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("incluirContratoAdequacaoParceria")
	public String incluirContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		obterIndicadorPesquisa(request, model);
		String view = "forward:exibirInclusaoContratoAdequacao";
		model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);
		ContratoAdequacaoParceriaImpl contratoAdequacao = new ContratoAdequacaoParceriaImpl();

		if (request.getParameter(CHECK_PONTO_CONSUMO) != null && !request.getParameter(CHECK_PONTO_CONSUMO).isEmpty()) {
			model.addAttribute(CHECK_PONTO_CONSUMO, String.valueOf(request.getParameter(CHECK_PONTO_CONSUMO)));
		}

		try {
			getDadosAuditoria(request);

			Contrato contratoSelecionado = obterContratoSelecionado(contratoAdequacaoParceria, request);

			PontoConsumo pontoConsumo = null;
			boolean exists = false;

			// Verifica se o ponto de consumo selecionado já se encontra associado a um
			// contrato adequação parceria
			// retornando booleano e passa para o método
			// validarDadosInclusaoContratoAdequacaoParceria, onde é feita a checagem dos
			// dados
			// que estão sendo inserido.
			manterDadosTela(contratoAdequacaoParceria, request, model);
			controladorContratoAdequacaoParceria.validarDadosInclusaoContratoAdequacaoParceria(
					contratoAdequacaoParceria, request, true, exists, contratoSelecionado, pontoConsumo, false);
			popularContratoAdequacaoParceria(contratoAdequacao, contratoAdequacaoParceria, request, true,
					contratoSelecionado, model);

			controladorContratoAdequacaoParceria.inserir(contratoAdequacao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, ContratoAdequacaoParceria.MENSAGEM_SUCESSO);
			view = pesquisarContratoAdequacaoParceria(contratoAdequacao, result, request,
					contratoAdequacao.isHabilitado(), model);
		} catch (NumberFormatException e) {
			mensagemErro(model, request, new NegocioException(e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por obter o contrato selecionado através do Contrato
	 * Adequacao de Parceria
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return Contrato {@link Contrato}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private Contrato obterContratoSelecionado(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			HttpServletRequest request) throws FormatoInvalidoException, NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		this.prepararFiltro(filtro, request, contratoAdequacaoParceria);
		Collection<Contrato> contratos = controladorContrato.consultarContratos(filtro);
		Contrato contratoSelecionado = null;
		if (contratos != null && !contratos.isEmpty()) {
			contratoSelecionado = contratos.iterator().next();
			contratoSelecionado = (Contrato) controladorContrato.obter(contratoSelecionado.getChavePrimaria(),
					"listaContratoPontoConsumo", "listaContratoPontoConsumo.pontoConsumo");
		}
		return contratoSelecionado;
	}

	/**
	 * Método responsável por atualizar os dados de uma contrato adequação parceria
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("atualizarContratoAdequacaoParceria")
	public String atualizarContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAtualizacaoContratoAdequacaoParceria";
		obterIndicadorPesquisa(request, model);
		Contrato contratoSelecionado = obterContratoSelecionado(contratoAdequacaoParceria, request);
		try {
			if (request.getParameter(CHECK_PONTO_CONSUMO) != null
					&& !request.getParameter(CHECK_PONTO_CONSUMO).isEmpty()) {
				model.addAttribute(CHECK_PONTO_CONSUMO, String.valueOf(request.getParameter(CHECK_PONTO_CONSUMO)));
			}

			Long chavePrimaria = contratoAdequacaoParceria.getChavePrimaria();
			ContratoAdequacaoParceriaImpl contratoAdequacao = (ContratoAdequacaoParceriaImpl) controladorContratoAdequacaoParceria
					.consultarContratoAdequacaoParceriaPorChave(chavePrimaria);
			model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);
			manterDadosTela(contratoAdequacaoParceria, request, model);
			controladorContratoAdequacaoParceria.validarDadosInclusaoContratoAdequacaoParceria(
					contratoAdequacaoParceria, request, false, false, contratoSelecionado, null, false);
			popularContratoAdequacaoParceria(contratoAdequacao, contratoAdequacaoParceria, request, false,
					contratoSelecionado, model);
			controladorContratoAdequacaoParceria.atualizarContratoAdequacaoPerceria(contratoAdequacao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ATUALIZADA, request,
					ContratoAdequacaoParceria.MENSAGEM_SUCESSO);
			view = pesquisarContratoAdequacaoParceria(contratoAdequacao, result, request,
					contratoAdequacao.isHabilitado(), model);
		} catch (NumberFormatException e) {
			mensagemErro(model, request, new NegocioException(e));
			view = exibirAtualizacaoContratoAdequacaoParceria(contratoAdequacaoParceria.getChavePrimaria(), request,
					model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAtualizacaoContratoAdequacaoParceria(contratoAdequacaoParceria.getChavePrimaria(), request,
					model);
		}
		carregarCampos(contratoAdequacaoParceria, request, false, model);

		return view;
	}

	/**
	 * Popular contrato adequacao parceria.
	 * 
	 * @param contratoAdequacao
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param isInclusao
	 *            {@link Boolean}
	 * @param contratoSelecionado
	 *            {@link Contrato}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacao,
			ContratoAdequacaoParceriaImpl contratoAdequacaoParceria, HttpServletRequest request, Boolean isInclusao,
			Contrato contratoSelecionado, Model model) throws GGASException {
		String chavePontoConsumo = null;

		if (contratoAdequacaoParceria.getNumeroContratoPrincipal() != null) {
			contratoAdequacao.setNumeroContratoPrincipal(contratoAdequacaoParceria.getNumeroContratoPrincipal());
		} else if (contratoAdequacaoParceria.getContrato() != null
				&& !contratoAdequacaoParceria.getContrato().getNumeroCompletoContrato().isEmpty()) {
			contratoAdequacao
					.setNumeroContratoPrincipal(contratoAdequacaoParceria.getContrato().getNumeroCompletoContrato());
		}

		if (contratoAdequacaoParceria.getPontoConsumo() != null) {
			chavePontoConsumo = String.valueOf(contratoAdequacaoParceria.getPontoConsumo().getChavePrimaria());
		} else if (request.getParameter(CHECK_PONTO_CONSUMO) != null) {
			chavePontoConsumo = String.valueOf(request.getParameter(CHECK_PONTO_CONSUMO));
		}

		if (isInclusao) {
			PontoConsumo pontoConsumo = null;
			if (StringUtils.isNotEmpty(chavePontoConsumo)) {
				pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(Long.parseLong(chavePontoConsumo));
				contratoAdequacao.setPontoConsumo(pontoConsumo);
			}

			contratoAdequacao.setDataCadastro(new Date());
		}

		String valorPagoCliente = (String) model.asMap().get("valorPagoCliente");
		String prazoEstabelecido = (String) model.asMap().get("prazoEstabelecido");
		String percentualMulta = (String) model.asMap().get(PERCENTUAL_MULTA);
		String dataInicioVigencia = (String) model.asMap().get(DATA_INICIO_VIGENCIA);
		String valorPagoCdl = (String) model.asMap().get("valorPagoPelaCdl");
		Long idIndiceFincanceiro = null;

		if (contratoSelecionado != null) {
			contratoAdequacao.setContrato(contratoSelecionado);

		}

		if (dataInicioVigencia != null) {
			contratoAdequacao.setDataInicioVigencia(
					Util.converterCampoStringParaData("Data Início", dataInicioVigencia, Constantes.FORMATO_DATA_BR));
		}
		if (valorPagoCliente != null && !"".equals(valorPagoCliente) && valorPagoCliente.length() >= 0) {
			contratoAdequacao.setValorCliente(Util.converterCampoStringParaValorBigDecimal("Valor pago pelo Cliente",
					valorPagoCliente, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (prazoEstabelecido != null && !prazoEstabelecido.isEmpty()) {
			contratoAdequacao.setPrazo(Util.converterCampoStringParaValorInteger("Prazo", prazoEstabelecido));
		}

		contratoAdequacao.setValorEmpresa(Util.converterCampoStringParaValorBigDecimal("Valor Pago CDL", valorPagoCdl,
				Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		contratoAdequacao.setValorPagoPelaCdl(valorPagoCdl);
		contratoAdequacao.setPercentualMulta(Util.converterCampoStringParaValorBigDecimal("Percentual de Multa",
				percentualMulta, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));

		if (contratoAdequacaoParceria.getIndiceFinanceiro() != null) {
			idIndiceFincanceiro = contratoAdequacaoParceria.getIndiceFinanceiro().getChavePrimaria();
		}

		if (idIndiceFincanceiro != null && idIndiceFincanceiro > 0) {
			contratoAdequacao.setIndiceFinanceiro(controladorTarifa.obterIndiceFinanceiro(idIndiceFincanceiro));
		}

	}

	/**
	 * Método responsável pela exclusão de contrato adequação parceria
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("removerContratoAdequacaoParceria")
	public String removerContratoAdequacaoParceria(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		try {
			ContratoAdequacaoParceria contratoAdequacaoParceria = controladorContratoAdequacaoParceria
					.consultarContratoAdequacaoParceriaPorChave(chavePrimaria);

			if (contratoAdequacaoParceria != null) {
				controladorContratoAdequacaoParceria.removerContratoAdequacaoParceria(contratoAdequacaoParceria);
			}
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Contrato.CONTRATOS);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "forward:pesquisarContratoAdequacaoParceria";
	}

	/**
	 * Método responsável por detalhar cada contrato e enviar para a listagem dos
	 * contratos em tela.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirDetalhamentoContratoAdequacaoParceria")
	public String exibirDetalhamentoContratoAdequacaoParceria(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, Model model) {

		String view = "exibirDetalhamentoContratoAdequacaoParceria";

		try {
			ContratoAdequacaoParceriaImpl contratoAdequacaoParceria = (ContratoAdequacaoParceriaImpl) controladorContratoAdequacaoParceria
					.consultarContratoAdequacaoParceriaPorChave(chavePrimaria);
			model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);

			if(contratoAdequacaoParceria != null) {
				model.addAttribute(DATA_INICIO_VIGENCIA,
						Util.converterCampoDataParaStringComDiaDaSemana(contratoAdequacaoParceria.getDataInicioVigencia()));
			}
			
			this.obterFiltroPesquisaAssociacao(request);

			List<ContratoAdequacaoParceria> listaPontoConsumo = new ArrayList<ContratoAdequacaoParceria>();

			listaPontoConsumo.add(contratoAdequacaoParceria);

			model.addAttribute("listaContratoAdequacao", listaPontoConsumo);

			carregarCampos(contratoAdequacaoParceria, request, false, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaContratoAdequacao(request, model);
		}

		return view;

	}

	/**
	 * Método responsável pesquisar os pontos de associação para um imóvel
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("pesquisarAssociacaoPontoConsumo")
	public String pesquisarAssociacaoPontoConsumo(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		filtros = this.obterFiltroPesquisaAssociacao(request);
		this.prepararFiltroContratoAdequacao(filtros, request, contratoAdequacaoParceria,
				contratoAdequacaoParceria.isHabilitado(), model);

		String valorPagoCliente = request.getParameter("valorCliente");
		String percentualMulta = request.getParameter(PERCENTUAL_MULTA);
		String dataInicioVigencia = request.getParameter(DATA_INICIO_VIGENCIA);
		model.addAttribute(DATA_INICIO_VIGENCIA, dataInicioVigencia);
		model.addAttribute("valorPagoCliente", valorPagoCliente);
		model.addAttribute(PERCENTUAL_MULTA, percentualMulta);

		listaPontoConsumo = new ArrayList<PontoConsumo>();
		Long idImovel = null;

		if (!request.getParameter(ID_IMOVEL).isEmpty()) {
			idImovel = Long.parseLong(request.getParameter(ID_IMOVEL));
		}

		if (idImovel != null && idImovel > 0) {
			listaPontoConsumo.addAll(controladorImovel.listarPontoConsumoPorChaveImovel(idImovel));
		} else {
			listaPontoConsumo.addAll(controladorPontoConsumo.listarPontosConsumo(filtros));
		}
		model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);

		return this.exibirInclusaoContratoAdequacao(contratoAdequacaoParceria, result, request, model);
	}

	/**
	 * Obter filtro pesquisa associacao.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return Map {@link Map}
	 */
	private Map<String, Object> obterFiltroPesquisaAssociacao(HttpServletRequest request) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		Long idImovel = null;
		Long idCliente = null;

		if (request.getParameter(ID_CLIENTE) != null && !request.getParameter(ID_CLIENTE).isEmpty()) {
			idCliente = Long.parseLong(request.getParameter(ID_CLIENTE));
		}

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (request.getParameter(ID_IMOVEL) != null && !request.getParameter(ID_IMOVEL).isEmpty()) {
			idImovel = Long.parseLong(request.getParameter(ID_IMOVEL));
		}

		if (idImovel != null && idImovel > 0) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		return filtro;
	}

	/**
	 * Carregar campos, popular dados do contrato e do ponto de consumo.
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param carregarListaIndiceFinanceiro
	 *            {@link boolean}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link Model}
	 */
	private void carregarCampos(ContratoAdequacaoParceria contratoAdequacaoParceria, HttpServletRequest request,
			boolean carregarListaIndiceFinanceiro, Model model) throws GGASException {

		if (!carregarListaIndiceFinanceiro) {
			ImovelImpl imovel = null;
			PontoConsumoImpl pontoConsumo = null;

			if (contratoAdequacaoParceria != null && contratoAdequacaoParceria.getPontoConsumo() != null) {
				imovel = (ImovelImpl) contratoAdequacaoParceria.getPontoConsumo().getImovel();
				pontoConsumo = (PontoConsumoImpl) contratoAdequacaoParceria.getPontoConsumo();
			}

			// Populando dados do Contrato
			model.addAttribute("imovel", imovel);

			// Populando Dados do Ponto de Consumo
			model.addAttribute("pontoConsumo", pontoConsumo);

			Collection<IndiceFinanceiro> listaIndiceFinanceiro = controladorTarifa.listaIndiceFinanceiro();
			model.addAttribute("listaIndiceFinanceiro", listaIndiceFinanceiro);
		} else {
			Collection<IndiceFinanceiro> listaIndiceFinanceiro = controladorTarifa.listaIndiceFinanceiro();
			model.addAttribute("listaIndiceFinanceiro", listaIndiceFinanceiro);
		}
	}

	/**
	 * Método responsável por exibir a tela de atualizar contrato adequação
	 * parceria.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirAtualizacaoContratoAdequacaoParceria")
	public String exibirAtualizacaoContratoAdequacaoParceria(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAtualizacaoContratoAdequacaoParceria";
		obterIndicadorPesquisa(request, model);
		try {
			ContratoAdequacaoParceriaImpl contratoAdequacao = (ContratoAdequacaoParceriaImpl) controladorContratoAdequacaoParceria
					.consultarContratoAdequacaoParceriaPorChave(chavePrimaria);

			if (!model.containsAttribute(CONTRATO_PARCERIA)) {
				if (contratoAdequacao.getContrato() != null) {
					contratoAdequacao
							.setNumeroContratoPrincipal(contratoAdequacao.getContrato().getNumeroCompletoContrato());
				}
				model.addAttribute(CONTRATO_PARCERIA, contratoAdequacao);
				model.addAttribute("valorPagoPelaCdl", contratoAdequacao.getValorEmpresa());
				model.addAttribute("valorPagoCliente", contratoAdequacao.getValorCliente());
				model.addAttribute(PERCENTUAL_MULTA, contratoAdequacao.getPercentualMulta());
				model.addAttribute("prazoEstabelecido", contratoAdequacao.getPrazo());
				model.addAttribute(DATA_INICIO_VIGENCIA,
						Util.converterCampoDataParaStringComDiaDaSemana(contratoAdequacao.getDataInicioVigencia()));
			}

			this.obterFiltroPesquisaAssociacao(request);
			List<ContratoAdequacaoParceria> listaContratoAdequacao = new ArrayList<ContratoAdequacaoParceria>();
			listaContratoAdequacao.add(contratoAdequacao);
			model.addAttribute("listaContratoAdequacao", listaContratoAdequacao);
			carregarCampos(contratoAdequacao, request, false, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaContratoAdequacao(request, model);
		}

		saveToken(request);

		return view;
	}

	/**
	 * Preparar filtro para pesquisa.
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 */
	public void prepararFiltro(Map<String, Object> filtro, HttpServletRequest request,
			ContratoAdequacaoParceriaImpl contratoAdequacaoParceria) throws FormatoInvalidoException {

		if (request.getParameter(ID_CLIENTE) != null && !request.getParameter(ID_CLIENTE).isEmpty()) {
			filtro.put("ID_CLIENTE", Long.parseLong(request.getParameter(ID_CLIENTE)));
		}

		if (request.getParameter(ID_IMOVEL) != null && !request.getParameter(ID_IMOVEL).isEmpty()) {
			filtro.put("ID_IMOVEL", Long.parseLong(request.getParameter(ID_IMOVEL)));
		}
		if (request.getParameter(ID_IMOVEL) != null && !request.getParameter(ID_IMOVEL).isEmpty()) {
			filtro.put("ID_CONTRATO", Long.parseLong(request.getParameter(ID_IMOVEL)));
		}

		if (contratoAdequacaoParceria.getPrazo() != null) {
			filtro.put("PRAZO", contratoAdequacaoParceria.getPrazo());
		}

		if (contratoAdequacaoParceria.getValorEmpresa() != null) {
			filtro.put("VALOR_OBRA", contratoAdequacaoParceria.getValorEmpresa());
		}

		if (contratoAdequacaoParceria.getValorCliente() != null) {
			filtro.put("VALOR_PAGO_CLIENTE", contratoAdequacaoParceria.getValorCliente());
		}

		if (contratoAdequacaoParceria.getNumeroContratoPrincipal() != null
				&& !contratoAdequacaoParceria.getNumeroContratoPrincipal().isEmpty()) {
			filtro.put("numero", Util.converterCampoStringParaValorInteger("Número do Contrato",
					contratoAdequacaoParceria.getNumeroContratoPrincipal()));
		}
	}

	/**
	 * Preparar filtro contrato adequacao.
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param model
	 *            {@link Model}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	private void prepararFiltroContratoAdequacao(Map<String, Object> filtro, HttpServletRequest request,
			ContratoAdequacaoParceriaImpl contratoAdequacaoParceria, Boolean habilitado, Model model)
			throws NegocioException {

		if (contratoAdequacaoParceria.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, contratoAdequacaoParceria.getChavePrimaria());
		}

		Long idCliente = null;
		if (request.getParameter(ID_CLIENTE) != null && !request.getParameter(ID_CLIENTE).isEmpty()) {
			idCliente = Long.parseLong(request.getParameter(ID_CLIENTE));
			model.addAttribute(ID_CLIENTE, idCliente);
		}
		if (idCliente != null) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (StringUtils.isNotEmpty(contratoAdequacaoParceria.getNumeroContratoPrincipal())) {
			filtro.put("numeroContrato", contratoAdequacaoParceria.getNumeroContratoPrincipal());
		} else if (contratoAdequacaoParceria.getContrato() != null
				&& !contratoAdequacaoParceria.getContrato().getNumeroCompletoContrato().isEmpty()) {
			filtro.put("numeroContrato", contratoAdequacaoParceria.getContrato().getNumeroCompletoContrato());
		}

		Long idImovel = null;
		if (request.getParameter(ID_IMOVEL) != null && !request.getParameter(ID_IMOVEL).isEmpty()) {
			idImovel = Long.parseLong(request.getParameter(ID_IMOVEL));
			model.addAttribute(ID_IMOVEL, idImovel);
		}

		if (idImovel != null) {
			filtro.put(ID_IMOVEL, idImovel);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
	}

	/**
	 * Método responsável por carregar o numero do contrato relacionado ao ponto de
	 * consumo selecionado.
	 * 
	 * @param contratoAdequacaoParceria
	 *            {@link ContratoAdequacaoParceriaImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param isInclusao
	 *            {@link Boolean}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("carregarNumeroContrato")
	public String carregarNumeroContrato(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria, BindingResult result,
			HttpServletRequest request, @RequestParam("isInclusao") Boolean isInclusao, Model model)
			throws GGASException {

		// seta número do contrato principal no campo "numero do contrato principal", de
		// acordo com
		// o ponto de consumo selecionado.
		String chavepontoConsumo = null;
		if (request.getParameter(CHECK_PONTO_CONSUMO) != null && !request.getParameter(CHECK_PONTO_CONSUMO).isEmpty()) {
			chavepontoConsumo = String.valueOf(request.getParameter(CHECK_PONTO_CONSUMO));
			model.addAttribute(CHECK_PONTO_CONSUMO, chavepontoConsumo);
		}

		String valorPagoCliente = request.getParameter("valorCliente");
		String percentualMulta = request.getParameter(PERCENTUAL_MULTA);
		String dataInicioVigencia = request.getParameter(DATA_INICIO_VIGENCIA);
		model.addAttribute(DATA_INICIO_VIGENCIA, dataInicioVigencia);
		model.addAttribute("valorPagoCliente", valorPagoCliente);
		model.addAttribute(PERCENTUAL_MULTA, percentualMulta);

		ContratoPontoConsumo contrato = null;
		try {
			if (chavepontoConsumo != null) {
				contrato = controladorContrato
						.consultarContratoPontoConsumoPorPontoConsumo(Long.parseLong(chavepontoConsumo));
			}
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		if (contrato != null) {
			contratoAdequacaoParceria.setNumeroContratoPrincipal(contrato.getContrato().getNumeroCompletoContrato());
		} else {
			contratoAdequacaoParceria.setNumeroContratoPrincipal(null);
		}
		model.addAttribute(CONTRATO_PARCERIA, contratoAdequacaoParceria);
		return this.exibirInclusaoContratoAdequacao(contratoAdequacaoParceria, result, request, model);
	}

}
