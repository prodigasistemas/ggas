
package br.com.ggas.web.performance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.CronogramaAtividadeFaturamentoImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.RotaImpl;
import br.com.ggas.util.DadosAuditoriaUtil;

@Controller
public class PerformanceAction {

	@Autowired
	@Qualifier("controladorRota")
	private ControladorRota controladorRota;

	@Autowired
	@Qualifier("controladorCronogramaFaturamento")
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier("controladorHistoricoConsumo")
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	private static final long ID_GRUPO_ATIVIDADE_FATURAMENTO = 184L;

	private static final long ID_GRUPO_FATURAMENTO = 2L;
	
	private static final long ID_ROTA = 30L;

	private static final  String REDIRECT = "forward:/";

	private static final Logger LOG = Logger.getLogger(PerformanceAction.class);
	
	
	/**
	 * Método auxiliar para teste de performance de Batch.
	 * 
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("faturarGrupo")
	public String faturarGrupo() throws GGASException {

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(ID_GRUPO_FATURAMENTO);
		Rota rota = (Rota) controladorRota.obter(ID_ROTA, RotaImpl.class);
		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) controladorCronogramaFaturamento
						.obter(ID_GRUPO_ATIVIDADE_FATURAMENTO, CronogramaAtividadeFaturamentoImpl.class);

		controladorFatura.faturarGrupo(grupoFaturamento, rota, new DadosAuditoriaUtil().getDadosAuditoria(), new StringBuilder(),
						cronogramaAtividadeFaturamento, Boolean.FALSE, null, null);

		return REDIRECT;
	}

	/**
	 * Método auxiliar para teste de performance de Batch.
	 * 
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("emitirFatura")
	public String emitirFatura() throws GGASException {

		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("idGrupoFaturamento", "2");
		parametros.put("idRota", "30");

		Processo p = new ProcessoImpl();
		p.setParametros(parametros);
		Usuario user = (Usuario) controladorUsuario.obter(1L);
		p.setUsuario(user);

		try {
			controladorFatura.processarEmissaoFatura(p, new StringBuilder());
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return "forward:/";
		}
		
		return REDIRECT;
	}

	/**
	 * Método auxiliar para teste de performance de Batch.
	 * 
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("consistirLeitura")
	public String consistirLeitura() throws GGASException {

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(ID_GRUPO_FATURAMENTO);
		Rota rota = (Rota) controladorRota.obter(ID_ROTA, RotaImpl.class);

		StringBuilder logProcessamento = new StringBuilder();
		List<Rota> listRota = new ArrayList<Rota>();
		listRota.add(rota);

		Collection<PontoConsumo> pontosAtivos = controladorPontoConsumo.consultarPontoConsumo(grupoFaturamento, rota);
		logProcessamento.append("\n\rQuantidade de Pontos de Consumo ativos: ").append(pontosAtivos.size());
		controladorHistoricoConsumo.consistirLeituraMedicao(pontosAtivos, null, null, logProcessamento, false, null);

		controladorRota.atualizarCronogramaPorRota(AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA, listRota, logProcessamento);

		return REDIRECT;
	}

	/**
	 * Método auxiliar para teste de performance de Batch.
	 * 
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("encerrarCicloFaturamento")
	public String encerrarCicloFaturamento() throws GGASException {

		Map<String, String> parametros = new HashMap<String, String>();
		Processo p = new ProcessoImpl();
		p.setParametros(parametros);
		Usuario user = (Usuario) controladorUsuario.obter(1L);
		p.setUsuario(user);
		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(ID_GRUPO_FATURAMENTO);
		
		controladorFatura.processarEncerrarCicloFaturamento(p, new StringBuilder(), grupoFaturamento);

		return REDIRECT;
	}

	// GerarDadosDeLeituraBatch

	// EncerrarCicloFaturamentoBatch

	// ConsolidarDadosSupervisorioMedicaoDiariaBatch

	// RegistrarLeiturasAnormalidadesBatch

}
