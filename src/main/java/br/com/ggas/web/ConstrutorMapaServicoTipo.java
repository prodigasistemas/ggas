/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.web;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

/**
 * Classe reponsável por construir um {@link Map} de dados 
 * em etapas a partir do {@link ServicoTipo}. 
 */
public class ConstrutorMapaServicoTipo {
	
	private static final String NAO = "Não";
	private static final String SIM = "Sim";
	private static final String REGULAMENTADO = "regulamentado";
	private static final String PRIORIDADE = "prioridade";
	private static final String DESCRICAO = "descricao";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	
	private ServicoTipo servicoTipo;
	private String chavePrimaria;
	private String descricaoStr;
	private String prioridadeStr;
	private String indicadorServicoRegulamento;

	/**
	 * Cria um {@link ConstrutorMapaServicoTipo} capaz de gerar um
	 * mapa de dados usando atributos presentes no argumento.
	 *
	 * @param servicoTipo O {@link ServicoTipo} com os atributos necessários
	 */
	public ConstrutorMapaServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}
	
	/**
	 * Preenche a chave primaria.
	 *
	 * @return Este {@link ConstrutorMapaServicoTipo}
	 * 
	 */
	public ConstrutorMapaServicoTipo preencherChavePrimaria() {
		chavePrimaria = String.valueOf(servicoTipo.getChavePrimaria());
		return this;
	}
	
	/**
	 * Preenche a descrição.
	 *
	 * @return Este {@link ConstrutorMapaServicoTipo}
	 * 
	 */
	public ConstrutorMapaServicoTipo preencherDescricao() {
		descricaoStr = servicoTipo.getDescricao();
		return this;
	}

	/**
	 * Preenche a prioridade.
	 *
	 * @return Este {@link ConstrutorMapaServicoTipo}
	 */
	public ConstrutorMapaServicoTipo preencherPrioridade() {
		prioridadeStr = servicoTipo.getServicoTipoPrioridade().getDescricao();
		return this;
	}
	
	/**
	 * Preenche o indicador de regulamentação.
	 *
	 * @return Este {@link ConstrutorMapaServicoTipo}
	 */
	public ConstrutorMapaServicoTipo preencherIndicadorRegulamentacao() {
		if(servicoTipo.getIndicadorServicoRegulamento()) {
			indicadorServicoRegulamento = SIM;
		} else {
			indicadorServicoRegulamento = NAO;
		}
		return this;
	}

	/**
	 * Constrói um novo mapa preenchido.
	 *
	 * @return Um {@link Map} contendo os valores preenchidos
	 */
	public Map<String, String> construir() {
		Map<String, String> dados = new HashMap<>();
		dados.put(CHAVE_PRIMARIA, chavePrimaria);
		dados.put(DESCRICAO, descricaoStr);
		dados.put(PRIORIDADE, prioridadeStr);
		dados.put(REGULAMENTADO, indicadorServicoRegulamento);
		return dados;
	}
}
