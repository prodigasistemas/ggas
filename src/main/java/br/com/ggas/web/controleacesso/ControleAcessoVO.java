package br.com.ggas.web.controleacesso;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
/**
 * Classe responsável pela telas relacionadas ao controle de acesso
 *
 */
public class ControleAcessoVO {

	private Long chavePrimaria;
	private Empresa empresa;
	private UnidadeOrganizacional unidadeOrganizacional;
	private Funcionario funcionario;
	private String login;
	private String usuarioDominio;
	private String periodicidadeSenha;
	private String email;
	private Boolean adminAtendimento;
	private Boolean senhaExpirada;
	private Long[] papelSelecionado;
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getUsuarioDominio() {
		return usuarioDominio;
	}
	public void setUsuarioDominio(String usuarioDominio) {
		this.usuarioDominio = usuarioDominio;
	}
	public String getPeriodicidadeSenha() {
		return periodicidadeSenha;
	}
	public void setPeriodicidadeSenha(String periodicidadeSenha) {
		this.periodicidadeSenha = periodicidadeSenha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getAdminAtendimento() {
		return adminAtendimento;
	}
	public void setAdminAtendimento(Boolean adminAtendimento) {
		this.adminAtendimento = adminAtendimento;
	}
	public Boolean getSenhaExpirada() {
		return senhaExpirada;
	}
	public void setSenhaExpirada(Boolean senhaExpirada) {
		this.senhaExpirada = senhaExpirada;
	}
	public Long[] getPapelSelecionado() {
		Long[] papel = null;
		if(papelSelecionado != null) {
			papel = papelSelecionado.clone();
		}
		return papel;
	}
	public void setPapelSelecionado(Long[] papelSelecionado) {
		
		if(papelSelecionado != null) {
			this.papelSelecionado = papelSelecionado.clone();
		} else {
			this.papelSelecionado = null;
		}
	}
	
}
