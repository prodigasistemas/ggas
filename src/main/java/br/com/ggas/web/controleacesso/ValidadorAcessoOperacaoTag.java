/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.controleacesso;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * Valida acesso pelo modulo e operacao
 *
 */
public class ValidadorAcessoOperacaoTag extends BodyTagSupport {

	private static final long serialVersionUID = -1613081949194535461L;

	private String operacao;
	private String modulo;

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.BodyTagSupport#doAfterBody()
	 */
	@Override
	public int doAfterBody() throws JspException {

		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.ATRIBUTO_USUARIO_LOGADO);

		ControladorAcesso controladorAcesso =
				(ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		boolean autorizado = false;

		try {
			if (StringUtils.isNotEmpty(getOperacao()) && StringUtils.isNotEmpty(getModulo())) {

				Permissao permissao = controladorAcesso.buscarPermissao(usuario, getOperacao(), getModulo());
				if (permissao != null){
					autorizado = true;
				}
	
				BodyContent bodycontent = getBodyContent();
				String body = bodycontent.getString();
				JspWriter out = bodycontent.getEnclosingWriter();
				if (body != null) {
					if (autorizado) {
						out.print(body.toString());
					} else {
						int indicePrimeiraChaveAbrindo = body.indexOf('<');
						int indicePrimeiraChaveFechando = body.indexOf('>');
						int indiceProxChaveAbrindo = body.indexOf('<', indicePrimeiraChaveAbrindo + 1);
						while (indiceProxChaveAbrindo > 0 && indiceProxChaveAbrindo < indicePrimeiraChaveAbrindo) {
							indiceProxChaveAbrindo = body.indexOf('<', indicePrimeiraChaveFechando + 1);
							indicePrimeiraChaveFechando = body.indexOf('>', indiceProxChaveAbrindo + 1);
						}
						String antes = body.substring(0, indicePrimeiraChaveFechando);
						String depois = body.substring(indicePrimeiraChaveFechando, body.length());
	
						out.print(antes + " disabled " + depois);
					}
				}
			}
		} catch (IOException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return SKIP_BODY;
	}

	/**
	 * Retorna operacao
	 * 
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Atribui operacao
	 * 
	 * @param operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Retorna modulo
	 * 
	 * @return modulo
	 */
	public String getModulo() {
		return modulo;
	}

	/**
	 * Atribui modulo
	 * 
	 * @param modulo
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
}
