package br.com.ggas.web.controleacesso;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.AutorizacoesPendentesVO;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorHistoricoSenha;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.HistoricoSenha;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.UsuarioImpl;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.GGASApplicationContext;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import com.unboundid.ldap.sdk.BindRequest;
import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SimpleBindRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Classe responsável pelas telas relacionadas ao controle de acesso do Sistema
 *
 */
@Controller
public class ControleAcessoAction extends GenericAction {

	private static final String FORM_USUARIO = "usuario";

	private static final String DESCRICAO = "descricao";

	private static final String ID_PERFIL = "idPerfil";

	private static final int LIMITE_SENHA = 8;

	private static final String LISTA_PAPEIS_SELECIONADOS = "listaPapeisSelecionados";

	private static final String CONTROLE_DE_ACESSO = "controleDeAcesso";

	private static final String ALTERAR_FUNCIONARIO = "alterarFuncionario";

	private static final String DETALHAR_FUNCIONARIO = "detalharFuncionario";

	private static final String LISTA_UNIDADE_ORGANIZACIONAL = "listaUnidadeOrganizacional";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_EMPRESA = "listaEmpresa";

	private static final String LISTA_PAPEL = "listaPapel";

	private static final String EMAIL = "email";

	private static final Logger LOG = Logger.getLogger(ControladorAcesso.class);

	private static final String FORM_CHAVE_PRIMARIA = "chavePrimaria";

	private static final String FORM_CHAVES_PRIMARIAS = "chavesPrimarias";

	protected static final String FORWARD_CADASTRO_USUARIO = "telaCadastroUsuarioForward";

	private static final String FORM_LOGIN = "login";

	private static final String MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO = "MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO";

	private static final String ERRO_ENTIDADE_EXISTE_REGISTROS_FILHOS = "ERRO_ENTIDADE_EXISTE_REGISTROS_FILHOS";

	private static final String EMAIL_ASSUNTO = "Senha GGAS";

	private static final String EMAIL_MENSAGEM_CONTEUDO = "Sua senha é: ";

	private static final String ID_EMPRESA = "idEmpresa";

	private static final String ID_UNIDADE_ORGANIZACIONAL = "idUnidadeOrganizacional";

	private static final String HEADER_PARAM = "attachment";

	private static final String MEDIA_TYPE = "application/pdf";

	protected static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	protected static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_PENDENCIAS = "exibirPendencias";

	private static final String EXIBIR_PENDENCIAS_POPUP = "exibirPendenciasPopup";

	public static final String ATRIBUTO_USUARIO_LOGADO = "usuarioLogado";

	private static final String MODULOS_MENU = "modulosMenu";

	private static final String LISTA_FAVORITOS = "listaFavoritos";

	private static final String PRIMEIRO_ACESSO = "primeiroAcesso";
	
	public static final int LDAP_RESPONSE_TIMEOUT = 10000;
	
	public static final char SEPARADOR_DE_DOMINIO = '@';

	private static final String ATRIBUTO_MENSAGEM_TELA = "mensagemTela";

	private static final String MENSAGEM_ACESSO_NEGADO = "Acesso negado!";

	@Autowired
	private ControladorMenu controladorMenu;

	@Autowired
	private ControladorPapel controladorPapel;

	@Autowired
	private ControladorUsuario controladorUsuario;

	@Autowired
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	private ControladorHistoricoSenha controladorHistoricoSenha;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorAcesso controladorAcesso;
	
	@Autowired
	private ControladorAlcada controladorAlcada;

	/**
	 * Método responsável por autenticar um
	 * usuário no sistema.
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param usuario - {@link UsuarioImpl}
	 * @return view - {@link String}
	 */
	@RequestMapping("efetuarLogon")
	public String efetuarLogon(Model model, HttpServletRequest request, UsuarioImpl usuario){

		String view = "exibirHome";

		model.addAttribute(FORM_USUARIO, usuario);

		try {
			Usuario usuarioLogado;
			ParametroSistema autenticacaoPorAD = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_AUTENTICACAO_POR_AD_WINDOWS);
			if (NumberUtils.INTEGER_ONE.equals(Integer.valueOf(autenticacaoPorAD.getValor()))
					&& usuarioExisteNoLDAP(usuario.getLogin(), usuario.getSenha())) {
				usuarioLogado = controladorUsuario.buscarUsuarioBancoPorUsuarioLDAP(usuario.getLogin());
				if (usuarioLogado == null) {
					throw new NegocioException(Constantes.ERRO_ACESSO_NEGADO, true);
				}
				usuarioLogado.setUltimoAcesso(Calendar.getInstance().getTime());
				usuarioLogado.setTentativasSenhaErrada(0);
				DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
				usuarioLogado.setDadosAuditoria(dadosAuditoria);
				controladorUsuario.atualizar(usuarioLogado, ServiceLocator.getInstancia().getClassPorID(Usuario.BEAN_ID_USUARIO));
			} else {
				usuarioLogado = (Usuario) controladorUsuario.criar();
				usuarioLogado.setLogin(usuario.getLogin());
				usuarioLogado.setSenha(usuario.getSenha());
				if (!controladorAcesso.validarLogon(usuarioLogado)) {
					model.addAttribute(PRIMEIRO_ACESSO, Boolean.TRUE);
					return "forward:/logon";
				} else {
					DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
					usuarioLogado.setDadosAuditoria(dadosAuditoria);
					usuarioLogado = controladorAcesso.autenticarUsuario(usuarioLogado);
				}
			}

			if (usuarioLogado != null) {
				request.getSession().setAttribute(ATRIBUTO_USUARIO_LOGADO, usuarioLogado);
				GGASApplicationContext.getInstancia().registarUsuarioConectado(request.getSession().getId(), usuarioLogado);

				AutorizacoesPendentesVO pendentes = controladorAlcada.obterRegistrosPendentesPorUsuario(usuarioLogado, Boolean.TRUE);
				model.addAttribute(EXIBIR_PENDENCIAS, pendentes.possuiPendencias());
				request.getSession().setAttribute(EXIBIR_PENDENCIAS_POPUP, String.valueOf(Boolean.FALSE));
				model.addAttribute(LISTA_FAVORITOS, usuarioLogado.getFavoritos());
				Collection<Menu> modulos = controladorMenu.listarModulos(usuarioLogado);

				Util.atualizaListaMenu(modulos, usuarioLogado.getFavoritos());
				ocultarItensMenuSemFilhos(modulos);

				request.getSession().setAttribute(MODULOS_MENU, modulos);
			}
			registrarUsuarioLogado(request, usuarioLogado);
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/logon";
		}

		return view;
	}

	/**
	 * Valida se as credencias informadas são se um usuário LDAP
	 * @param login O login do usuário
	 * @param senha A senha do usuário
	 * @return true se o usuário existe no servidor, false caso contrário
	 */
	private boolean usuarioExisteNoLDAP(String login, String senha) {
		try {
		    ParametroSistema host = controladorParametroSistema.obterParametroPorCodigo(
		    		Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_HOST);
            ParametroSistema porta = controladorParametroSistema.obterParametroPorCodigo(
            		Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_PORTA);
            ParametroSistema dominio = controladorParametroSistema.obterParametroPorCodigo(
            		Constantes.PARAMETRO_SERVIDOR_AUTENTICACAO_AD_DOMINIO);
            LDAPConnection conexao = montarConexaoLDAP(host.getValor(), Integer.parseInt(porta.getValor()));
            ResultCode result = consultarUsuarioLDAP(login, senha, dominio.getValor(), conexao);

            return result.equals(ResultCode.SUCCESS);
		} catch (NegocioException e) {
			LOG.error("Não foi possível identificar se o usuário existe no servidor LDAP", e);
			return false;
		}
	}

	/**
	 * Estabelece uma conexão com um servidor LDAP
	 * @param host O endereço do servidor
	 * @param porta A porta para conexão
	 * @return A conexão montada em caso de sucesso e uma conexão vazia caso contrário
	 */
	private LDAPConnection montarConexaoLDAP(String host, int porta) throws NegocioException {
		LDAPConnection conexao = new LDAPConnection();
		try {
			conexao.connect(host, porta, LDAP_RESPONSE_TIMEOUT);
		} catch (LDAPException e) {
			LOG.error("Falha ao montar conexão com servidor LDAP", e);
			throw new NegocioException(Constantes.ERRO_HOST_DESCONHECIDO, true);
		}
		return conexao;
	}

	/**
	 * Verifica se as credenciais informadas estão presentes no servidor LDAP informado
	 * @param login O login do usuario
	 * @param senha A senha do usuario
	 * @param dominio O domínio do servidor
	 * @param conexao A conexao com o servidor LDAP
	 * @return Sucesso caso o usuário seja encontrado no servidor LDAP e falha caso contrário
	 */
	private ResultCode consultarUsuarioLDAP(String login, String senha, String dominio, LDAPConnection conexao) {
		try {
			String bindDn = login + SEPARADOR_DE_DOMINIO + dominio;
			BindRequest bindRequest = new SimpleBindRequest(bindDn, senha);
			bindRequest.setResponseTimeoutMillis(LDAP_RESPONSE_TIMEOUT);
			BindResult result = conexao.bind(bindRequest);
			return result.getResultCode();
		} catch (LDAPException e) {
			LOG.error("Falha ao consultar usuario no servidor LDAP", e);
			return ResultCode.OTHER;
		} finally {
			if (conexao != null) {
				conexao.close();
			}
		}
	}

    private void ocultarItensMenuSemFilhos(Collection<Menu> modulos) {
		modulos.forEach(modulo -> modulo.getListaMenu().forEach(itemMenu -> {
			if (itemMenu.isIndicadorExibirSemFilhos()) {
				boolean ocultar = itemMenu.getListaMenu().stream().noneMatch(Menu::isIndicadorMostrarMenu);
				if (ocultar) {
					itemMenu.setListaMenu(Collections.emptyList());
				}
			}
		}));
	}
	
	
	/**
	 *  Método responsável por fazer um logoff no
	 * 	sistema.
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("efetuarLogoff")
	public String efetuarLogoff(HttpServletRequest request) {

		request.getSession().removeAttribute(ATRIBUTO_USUARIO_LOGADO);
		request.getSession().invalidate();
		return "forward:/logon";
	}
	
	/**
	 * Recuperar senha.
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param loginHidden - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("recuperarSenha")
	public String recuperarSenha(Model model, HttpServletRequest request,
			UsuarioImpl usuarioForm,
			@RequestParam(value="loginHidden", required=false) String loginHidden){

		model.addAttribute(FORM_USUARIO, usuarioForm);
		
		try {
			Boolean senhaGerada = Boolean.FALSE;

			controladorUsuario.validarRecuperarSenha(loginHidden);

			try {
				senhaGerada = controladorUsuario.recuperarSenha(loginHidden, getDadosAuditoria(request));
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
				if(e.getClass().equals(MailSendException.class)) {
					super.mensagemAlerta(model, Constantes.ERRO_EMAIL_NAO_ENVIADO);
				}
			}

			if (senhaGerada) {
				super.mensagemSucesso(model, Constantes.SUCESSO_SENHA_ENVIADA);
			}
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "forward:/logon";
	}
	
	/**
	 * Alterar senha.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param senhaAtual - {@link String}
	 * @param novaSenha - {@link String}
	 * @param confirmacaoNovaSenha - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarSenha")
	public String alterarSenha(Model model, HttpServletRequest request,
			@RequestParam(value="senhaAtualHidden", required=false) String senhaAtual,
			@RequestParam(value="senhaNovaHidden", required=false) String novaSenha,
			@RequestParam(value="confirmacaoSenhaNovaHidden", required=false) String confirmacaoNovaSenha) {

		UsuarioImpl usuario = (UsuarioImpl) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);

		this.alterarPrimeiraSenha(model, request, usuario, senhaAtual, novaSenha, confirmacaoNovaSenha);

		return "index";
	}
	
	/**
	 * Alterar primeira senha.
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param senhaAtual - {@link String}
	 * @param novaSenha - {@link String}
	 * @param confirmacaoNovaSenha - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarPrimeiraSenha")
	public String alterarPrimeiraSenha(Model model, HttpServletRequest request,
			UsuarioImpl usuarioForm,
			@RequestParam(value="senhaAtualHidden", required=false) String senhaAtual,
			@RequestParam(value="senhaNovaHidden", required=false) String novaSenha,
			@RequestParam(value="confirmacaoSenhaNovaHidden", required=false) String confirmacaoNovaSenha) {

		model.addAttribute(FORM_USUARIO, usuarioForm);
		model.addAttribute(PRIMEIRO_ACESSO, Boolean.TRUE);
		
		try {
			Boolean senhaAlterada = Boolean.FALSE;
			
			senhaAlterada = controladorUsuario.alterarSenha(usuarioForm.getLogin(), senhaAtual, novaSenha, 
					confirmacaoNovaSenha, getDadosAuditoria(request));

			if (senhaAlterada) {
				model.addAttribute(PRIMEIRO_ACESSO, Boolean.FALSE);
				super.mensagemSucesso(model, Constantes.SUCESSO_SENHA_ALTERADA);
			}
			
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}
		
		return "forward:/logon";
	}

	
	/**
	 * Registra na request usuario logado
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param usuarioLogado - {@link Usuario}
	 * @throws GGASException - {@link GGASException}
	 */
	private void registrarUsuarioLogado(HttpServletRequest request, Usuario usuarioLogado) throws GGASException {
		Map<String, Boolean> mapa = new HashMap<String, Boolean>();
		Map<String, Collection<Operacao>> mapaOperacoes = new HashMap<String, Collection<Operacao>>();

		if (usuarioLogado != null && usuarioLogado.getFuncionario() != null) {
			Collection<Recurso> listaRecursos = controladorAcesso.listarRecursosUsuario(usuarioLogado.getChavePrimaria());
			mapaOperacoes = montarMapaPermissoes(listaRecursos);

			for (Recurso recurso : listaRecursos) {
				if (!mapa.containsKey(recurso.getRecurso())) {
					mapa.put(recurso.getRecurso(), true);
				}
			}

		}

		request.getSession().setAttribute("mapaControleAcesso", mapa);
		request.getSession().setAttribute("mapaOperacoes", mapaOperacoes);

		/***
		 * TKT #597
		 * [ Geral ] :: GGAS deverá aceitar nomes
		 * acentuados e retirar
		 * na geração de arquivos para integração
		 * com outros sistemas
		 */

		ParametroSistema ps;
		ps = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_CAIXA_ALTA);
		String isCaixaAlta = "";
		if (ps != null) {
			isCaixaAlta = ps.getValor();
		}
		request.getSession().setAttribute("isCaixaAlta", isCaixaAlta);

		ps = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_ACEITA_CARACTER_ESPECIAL);
		String isPermiteCaracteresEspeciais = "";
		if (ps != null) {
			isPermiteCaracteresEspeciais = ps.getValor();
		}
		request.getSession().setAttribute("isPermiteCaracteresEspeciais", isPermiteCaracteresEspeciais);

		/***
		 * fim TKT #597
		 */
	}
	
	/**
	 * Montar mapa permissoes.
	 * 
	 * @param listaRecursos - {@link Collection}
	 * @return mapa de permissões
	 */
	private Map<String, Collection<Operacao>> montarMapaPermissoes(Collection<Recurso> listaRecursos) {

		Map<String, Collection<Operacao>> mapa = new HashMap<String, Collection<Operacao>>();

		for (Recurso recurso : listaRecursos) {
			if (!mapa.containsKey(recurso.getRecurso())) {
				Set<Operacao> lista = new HashSet<Operacao>();
				lista.add(recurso.getOperacao());
				mapa.put(recurso.getRecurso(), lista);
			} else {
				mapa.get(recurso.getRecurso()).add(recurso.getOperacao());
			}
		}

		return mapa;
	}
	
	/**
	 * Método responsável por exibir a página de login.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping("logon")
	public String logon() {
		return "logon";
	}
	
	/**
	 * Método responsável pela exibição do template principal.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping("index")
	public String index() {
		return "index";
	}
	
	/**
	 * Exibir home.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirHome")
	public String exibirHome(@RequestParam(required=false, value = "acessoNegado") Boolean acessoNegado, 
			HttpServletRequest request, Model model) {

		try {
			model.addAttribute(EXIBIR_PENDENCIAS, true);

			request.getSession().setAttribute(EXIBIR_PENDENCIAS_POPUP, String.valueOf(Boolean.FALSE));
			Usuario usuarioLogado = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			request.setAttribute(LISTA_FAVORITOS, usuarioLogado.getFavoritos());
			Collection<Menu> modulos = controladorMenu.listarModulos(new HashMap<String, Object>());

			Long[] idPapelUsuario = new Long[usuarioLogado.getPapeis().size()];
			int posicao = 0;
			for (Papel currentPapel : usuarioLogado.getPapeis()) {
				idPapelUsuario[posicao] = currentPapel.getChavePrimaria();
				posicao++;
			}

			for (Menu currentMenu : modulos) {
				Util.removeMenuSemPermissaoSistema(currentMenu, idPapelUsuario);
			}
			if(acessoNegado != null && acessoNegado) {
				model.addAttribute(ATRIBUTO_MENSAGEM_TELA, MENSAGEM_ACESSO_NEGADO);
			}
			Util.atualizaListaMenu(modulos, usuarioLogado.getFavoritos());
			ocultarItensMenuSemFilhos(modulos);
			request.getSession().setAttribute(MODULOS_MENU, modulos);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirHome";
	}
	
	/**
	 * Logar com usuario do dominio.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("logarUsuarioDominio")
	public String logarUsuarioDominio(Model model, HttpServletRequest request) {

		String view = "exibirHome";
		
		try {
			
			String loginDominio = System.getProperty("user.name");

			Usuario usuario = controladorUsuario.obterUsuarioPorLoginDominio(loginDominio);

			if (usuario != null) {

				if (usuario.isPrimeiroAcesso()) {
					model.addAttribute(PRIMEIRO_ACESSO,Boolean.TRUE);
					return "forward:/logon";
				} else {
					request.getSession().setAttribute(ATRIBUTO_USUARIO_LOGADO, usuario);
					GGASApplicationContext.getInstancia().registarUsuarioConectado(request.getSession().getId(), usuario);

					AutorizacoesPendentesVO pendentes = controladorAlcada.obterRegistrosPendentesPorUsuario(usuario, Boolean.TRUE);
					model.addAttribute(EXIBIR_PENDENCIAS, pendentes.possuiPendencias());
					request.getSession().setAttribute(EXIBIR_PENDENCIAS_POPUP, String.valueOf(Boolean.FALSE));
					model.addAttribute(LISTA_FAVORITOS, usuario.getFavoritos());
					Collection<Menu> modulos = controladorMenu.listarModulos(usuario);
					Util.atualizaListaMenu(modulos, usuario.getFavoritos());
					ocultarItensMenuSemFilhos(modulos);

					request.getSession().setAttribute(MODULOS_MENU, modulos);
					registrarUsuarioLogado(request, usuario);
				}
			} else {
				throw new GGASException("Usuário de dominio não cadastrado no GGAS.");
			}

		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/logon";
		}
		
		return view;
	}

	/**
	 * Método responsável pela exibição da rela relacionada a pesquisa do controle de acesso.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaControleAcessoFunc")
	public String exibirPesquisaControleAcessoFunc(HttpServletRequest request, Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		model.addAttribute(LISTA_PAPEL, controladorPapel.consultarPapel(null));
		Boolean retornaTela = Boolean.parseBoolean(request.getParameter("retornaTela"));

		if (retornaTela) {
			pesquisarUsuario(null, null, true, request, model);
		}

		return "exibirPesquisaControleAcessoFunc";
	}

	/**
	 * Método responsável pela pesquisa de usuário
	 * 
	 * @param descricao {@link String}
	 * @param idPapel {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("pesquisarUsuario")
	public String pesquisarUsuario(@RequestParam(value = DESCRICAO, required = false) String descricao,
			@RequestParam(value = ID_PERFIL, required = false) Long idPapel, @RequestParam("habilitado") Boolean habilitado,
			HttpServletRequest request, Model model) throws NegocioException {

		model.addAttribute(ID_PERFIL, idPapel);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(DESCRICAO, descricao);

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			filtro.put("nome", descricao);
		}

		if (idPapel != null && idPapel > 0) {
			Long[] chaves = { idPapel };
			filtro.put("chavesPapeis", chaves);
		}

		if (habilitado != null) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		}

		Collection<Usuario> listaUsuario = controladorUsuario.consultarUsuario(filtro);
		model.addAttribute("listaUsuarios", listaUsuario);
		model.addAttribute(LISTA_PAPEL, controladorPapel.consultarPapel(null));

		return "exibirPesquisaControleAcessoFunc";
	}

	/**
	 * Método responsável pela exibição das telas de inclusão e alteração relacionadas ao controle de acesso de usuários.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirInclusaoControleAcessoFunc")
	public String exibirInclusaoControleAcessoFunc(@RequestParam(value = FORM_CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		String view = "";
		Boolean detalharFuncionario = Boolean.parseBoolean(request.getParameter(DETALHAR_FUNCIONARIO));
		Boolean alterarFuncionario = Boolean.parseBoolean(request.getParameter(ALTERAR_FUNCIONARIO));

		model.addAttribute(DETALHAR_FUNCIONARIO, detalharFuncionario);
		model.addAttribute(ALTERAR_FUNCIONARIO, alterarFuncionario);

		Usuario usuario = null;
		UnidadeOrganizacional unidadeOrganizacionalFuncionario;
		Collection<Papel> listaPapelCadastrado = new ArrayList<Papel>();
		Collection<Papel> listaPapel = controladorUsuario.consultarPapeisUsuario();

		model.addAttribute(LISTA_EMPRESA, controladorEmpresa.consultarEmpresas(null));

		if (chavePrimaria != null && chavePrimaria > 0) {
			usuario = controladorUsuario.obterUsuario(chavePrimaria);
		}

		if (usuario != null) {
			unidadeOrganizacionalFuncionario = usuario.getFuncionario().getUnidadeOrganizacional();

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_EMPRESA, usuario.getFuncionario().getEmpresa().getChavePrimaria());
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);
			model.addAttribute(LISTA_UNIDADE_ORGANIZACIONAL, unidades);

			filtro = new HashMap<String, Object>();

			if (unidadeOrganizacionalFuncionario != null) {
				filtro.put(ID_UNIDADE_ORGANIZACIONAL, usuario.getFuncionario().getUnidadeOrganizacional().getChavePrimaria());
			}

			tratarListaPapeisSelecionados(usuario, listaPapelCadastrado, listaPapel);

			model.addAttribute(LISTA_PAPEIS_SELECIONADOS, usuario.getPapeis());
			model.addAttribute(LISTA_PAPEL, listaPapelCadastrado);
		} else {
			// caso seja inclusão
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
			Collection<Funcionario> funcionarios = controladorFuncionario.consultarFuncionarios(filtro);
			model.addAttribute("listaFuncionarios", funcionarios);
		}

		if (detalharFuncionario != null && detalharFuncionario) {
			view = "exibirDetalhamentoFunc";
			UsuarioImpl usuarioDetalhamento = (UsuarioImpl) controladorUsuario.obterUsuario(chavePrimaria);
			ControleAcessoVO controleAcesso = new ControleAcessoVO();
			popularControleAcesso(controleAcesso, usuarioDetalhamento, model);
			model.addAttribute(CONTROLE_DE_ACESSO, controleAcesso);
		} else if (alterarFuncionario != null && alterarFuncionario) {
			// quando redirecionado para tela de alteração
			if (chavePrimaria != null) {
				UsuarioImpl usuarioAlteracao = (UsuarioImpl) controladorUsuario.obterUsuario(chavePrimaria);
				ControleAcessoVO controleAcesso = new ControleAcessoVO();
				popularControleAcesso(controleAcesso, usuarioAlteracao, model);
				model.addAttribute(CONTROLE_DE_ACESSO, controleAcesso);
			} else {
				// carregando unidades organizacionais
				Map<String, Object> filtro = new HashMap<String, Object>();
				ControleAcessoVO controleDeAcesso = (ControleAcessoVO) model.asMap().get(CONTROLE_DE_ACESSO);

				verificaEmpresa(filtro, controleDeAcesso);

				filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
				Collection<UnidadeOrganizacional> unidades = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);
				model.addAttribute(LISTA_UNIDADE_ORGANIZACIONAL, unidades);
			}
			view = "exibirAlterarFunc";
		} else {
			// carregando lista de papeis
			if (!model.containsAttribute(LISTA_PAPEL)) {
				model.addAttribute(LISTA_PAPEL, controladorPapel.consultarPapel(null));
			}
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);
			model.addAttribute(LISTA_UNIDADE_ORGANIZACIONAL, unidades);
			view = "exibirInclusaoFuncionario";
		}

		return view;
	}

	/**
	 * @param filtro
	 * @param controleDeAcesso
	 */
	private void verificaEmpresa(Map<String, Object> filtro, ControleAcessoVO controleDeAcesso) {
		if (controleDeAcesso.getEmpresa() != null) {
			filtro.put(ID_EMPRESA, controleDeAcesso.getEmpresa().getChavePrimaria());
		}
	}

	/**
	 * Método responsável pela inclusão do controle de acesso do usuário.
	 * 
	 * @param controleAcesso {@link ControleAcessoVO}
	 * @param result {@link BindingResult}
	 * @param selecionados {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("static-access")
	@RequestMapping("incluirControleAcessoFunc")
	public String incluirControleAcessoFunc(ControleAcessoVO controleAcesso, BindingResult result,
			@RequestParam(value = "papelSelecionado", required = false) Long[] selecionados, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "";
		Collection<Papel> listaPapelCadastrado = controladorUsuario.consultarPapeisUsuario();
		Collection<Papel> listaPapel = new ArrayList<Papel>();
		Boolean detalharFuncionario = Boolean.parseBoolean(request.getParameter(DETALHAR_FUNCIONARIO));
		Boolean alterarFuncionario = Boolean.parseBoolean(request.getParameter(ALTERAR_FUNCIONARIO));

		model.addAttribute(DETALHAR_FUNCIONARIO, detalharFuncionario);
		model.addAttribute(ALTERAR_FUNCIONARIO, alterarFuncionario);

		model.addAttribute(CONTROLE_DE_ACESSO, controleAcesso);
		FuncionarioImpl funcionariotmp = new FuncionarioImpl();
		UsuarioImpl usuariotmp = new UsuarioImpl();
		try {
			popularFuncionario(controleAcesso, funcionariotmp, usuariotmp, model);
			controladorUsuario.validarIncluirControleAcessoFunc(funcionariotmp);
			boolean alertaEmail = false;
			String email = request.getParameter(EMAIL);
			String login = request.getParameter(FORM_LOGIN);
			Map<String, Object> filtro = new HashMap<String, Object>();

			if (controleAcesso.getFuncionario() != null) {
				filtro.put("funcionario", controleAcesso.getFuncionario().getChavePrimaria());
			}

			Collection<Usuario> usuarios = controladorUsuario.consultarUsuarios(filtro, "papeis");

			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			FuncionarioImpl funcionario = new FuncionarioImpl();
			if (controleAcesso.getFuncionario() != null && controleAcesso.getFuncionario().getChavePrimaria() > 0) {
				funcionario = (FuncionarioImpl) controladorFuncionario.obter(controleAcesso.getFuncionario().getChavePrimaria());
				funcionario.setDadosAuditoria(dadosAuditoria);
				model.addAttribute(DESCRICAO, funcionario.getNome());
			}

			if (usuarios != null && !usuarios.isEmpty()) {
				for (Usuario usuario : usuarios) {
					popularFuncionario(controleAcesso, funcionario, (UsuarioImpl) usuario, model);
					usuario.setDadosAuditoria(getDadosAuditoria(request));
					controladorUsuario.atualizarUsuario(usuario);
					controladorFuncionario.atualizar(funcionario);
				}
			} else {
				ParametroSistema parametroSistema =
						controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

				UsuarioImpl usuario = (UsuarioImpl) controladorUsuario.criar();
				HistoricoSenha historicoSenha = (HistoricoSenha) controladorHistoricoSenha.criar();
				popularFuncionario(controleAcesso, funcionario, usuario, model);
				if (!login.isEmpty()) {
					String senhaCriptografada = Util.criptografarSenha(login, String.valueOf(Calendar.getInstance().getTimeInMillis()),
							Constantes.HASH_CRIPTOGRAFIA);
					String subSenhaCriptografada = senhaCriptografada.substring(0, LIMITE_SENHA);
					String senhaCriptografadaCompleta =
							Util.criptografarSenha(subSenhaCriptografada, subSenhaCriptografada, Constantes.HASH_CRIPTOGRAFIA);
					usuario.setSenha(senhaCriptografadaCompleta);
					usuario.setPrimeiroAcesso(Boolean.TRUE);
					usuario.setDadosAuditoria(dadosAuditoria);
					controladorUsuario.inserir(usuario);
					verificaBloqueio(controleAcesso, usuario);
					controladorFuncionario.atualizar(funcionario);

					alertaEmail = verificarEnvioEmail(alertaEmail, email, login, parametroSistema, usuario, subSenhaCriptografada);

					historicoSenha.setUsuario(usuario);
					historicoSenha.setSenha(usuario.getSenha());
					historicoSenha.setDadosAuditoria(dadosAuditoria);
					controladorHistoricoSenha.inserir(historicoSenha);
				}
			}

			if (alertaEmail) {
				mensagemAlerta(model, Constantes.ERRO_EMAIL_NAO_ENVIADO);
			} else {
				mensagemSucesso(model, MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
			}

			model.addAttribute("retornaTela", true);
			model.addAttribute(HABILITADO, funcionario.isHabilitado());
			view = pesquisarUsuario(funcionario.getNome(), null, usuariotmp.isHabilitado(), request, model);
		} catch (GGASException e) {
			tratandoListaPapeis(selecionados, listaPapel, listaPapelCadastrado, model);
			view = exibirInclusaoControleAcessoFunc(null, request, model);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * @param controleAcesso
	 * @param usuario
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	private void verificaBloqueio(ControleAcessoVO controleAcesso, UsuarioImpl usuario) throws ConcorrenciaException, NegocioException {
		if (controleAcesso.getSenhaExpirada() != null && controleAcesso.getSenhaExpirada()) {
			usuario.setHabilitado(false);
			controladorUsuario.atualizar(usuario);
		}
	}

	/**
	 * Método responsável pela exclusão do controle de acesso do usuário
	 * 
	 * @param descricao {@link String}
	 * @param idPapel {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("excluirControleAcessoFunc")
	public String excluirControleAcessoFunc(@RequestParam(value = DESCRICAO, required = false) String descricao,
			@RequestParam(value = ID_PERFIL, required = false) Long idPapel, @RequestParam("habilitado") Boolean habilitado,
			@RequestParam(value = FORM_CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ID_PERFIL, idPapel);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(DESCRICAO, descricao);
		try {
			validarSelecaoUmOuMais(chavesPrimarias);
			controladorUsuario.removerControleAcessoFunc(chavesPrimarias, getDadosAuditoria(request));
			controladorUsuario.removerUsuario(chavesPrimarias);
		} catch (DataIntegrityViolationException | GGASException e) {
			mensagemErro(model, request, new NegocioException(ERRO_ENTIDADE_EXISTE_REGISTROS_FILHOS, e));
		}

		return "forward:pesquisarUsuario";
	}

	/**
	 * Método responsável por gerar relatório relacionado ao usuário do sistema.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 */
	@RequestMapping("gerarRelatorio")
	public ResponseEntity<byte[]> gerarRelatorio(@RequestParam(FORM_CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			Model model) throws GGASException {

		byte[] relatorioExibicao = null;
		Map<String, Object> filtro = new HashMap<String, Object>();
		ResponseEntity<byte[]> responseEntity = null;

		try {
			FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");
			filtro.put(FORMATO_IMPRESSAO, formatoImpressao);
			Usuario usuario = null;
			if (chavePrimaria != null) {
				usuario = controladorUsuario.obterUsuario(chavePrimaria);
			}
			relatorioExibicao = controladorUsuario.gerarRelatorio(filtro, usuario);
			if (relatorioExibicao != null) {
				MediaType contentType = MediaType.parseMediaType(MEDIA_TYPE);
				String filename = "relatorioUsuario.pdf";

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(contentType);
				headers.setContentDispositionFormData(HEADER_PARAM, filename);

				responseEntity = new ResponseEntity<byte[]>(relatorioExibicao, headers, HttpStatus.OK);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.setAttribute(EXIBIR_MENSAGEM_TELA, false);

		return responseEntity;
	}

	/**
	 * @param alertaEmail {@link boolean}
	 * @param email {@link String}
	 * @param login {@link String}
	 * @param parametroSistema {@link ParametroSistema}
	 * @param usuario {@link UsuarioImpl}
	 * @param subSenhaCriptografada {@link String}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private boolean verificarEnvioEmail(boolean alertaEmail, String email, String login, ParametroSistema parametroSistema,
			UsuarioImpl usuario, String subSenhaCriptografada) throws GGASException {
		if (usuario.getFuncionario() != null && usuario.getFuncionario().getEmail() != null) {

			try {
				controladorUsuario.enviarEmailFunc(parametroSistema.getValor(), email, EMAIL_ASSUNTO,
						"Login: " + login + "\n" + EMAIL_MENSAGEM_CONTEUDO.concat(subSenhaCriptografada) + "\n Sua senha expirará em: "
								+ Util.quantidadeDiasIntervalo(new Date(), usuario.getDataExpirarSenha()) + " dias ");
			} catch (MailSendException e) {
				LOG.error(e.getMessage(), e);
				alertaEmail = true;
			}
		}
		return alertaEmail;
	}

	/**
	 * @param usuario {@link Usuario}
	 * @param listaPapelCadastrado {@link Collection}
	 * @param listaPapel {@link Collection}
	 */
	private void tratarListaPapeisSelecionados(Usuario usuario, Collection<Papel> listaPapelCadastrado, Collection<Papel> listaPapel) {

		if ((usuario.getPapeis() != null && !usuario.getPapeis().isEmpty()) && (listaPapel != null && !listaPapel.isEmpty())) {

			for (Papel papel : listaPapel) {
				if (!usuario.getPapeis().contains(papel)) {
					listaPapelCadastrado.add(papel);
				}
			}
		} else {
			listaPapelCadastrado.addAll(listaPapel);
		}
	}

	/**
	 * @param selecionados {@link Long}
	 * @param papeisCadastrados {@link Collection}
	 * @param listaPapelCadastrado {@link Collection}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void tratandoListaPapeis(Long[] selecionados, Collection<Papel> papeisCadastrados, Collection<Papel> listaPapelCadastrado,
			Model model) throws NegocioException {

		Collection<Papel> listaPapel = new ArrayList<Papel>();

		if (selecionados != null && selecionados.length > 0) {

			for (Long idPapel : selecionados) {
				Papel papelSistema = (Papel) controladorPapel.obter(idPapel);
				listaPapel.add(papelSistema);
			}

			adicionandoPapeisCadastrados(papeisCadastrados, listaPapelCadastrado, listaPapel);

		} else {
			papeisCadastrados.addAll(listaPapelCadastrado);
		}

		model.addAttribute(LISTA_PAPEIS_SELECIONADOS, listaPapel);
		model.addAttribute(LISTA_PAPEL, papeisCadastrados);
	}

	/**
	 * @param papeisCadastrados {@link Collection}olle
	 * @param listaPapelCadastrado {@link Collection}
	 * @param listaPapel {@link Collection}
	 */
	private void adicionandoPapeisCadastrados(Collection<Papel> papeisCadastrados, Collection<Papel> listaPapelCadastrado,
			Collection<Papel> listaPapel) {
		if (listaPapelCadastrado != null && !listaPapelCadastrado.isEmpty()) {
			for (Papel papel : listaPapelCadastrado) {
				if (!listaPapel.contains(papel)) {
					papeisCadastrados.add(papel);
				}
			}
		}
	}

	/**
	 * 
	 * @param controleAcesso {@link ControleAcessoVO}
	 * @param funcionariotmp {@link FuncionarioImpl}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularFuncionario(ControleAcessoVO controleAcesso, FuncionarioImpl funcionariotmp, UsuarioImpl usuario, Model model)
			throws GGASException {

		if (controleAcesso.getChavePrimaria() != null && controleAcesso.getChavePrimaria() > 0 && controleAcesso.getFuncionario() != null) {
			funcionariotmp.setChavePrimaria(controleAcesso.getFuncionario().getChavePrimaria());
		}

		if (controleAcesso.getEmpresa() != null) {
			funcionariotmp.setEmpresa(controleAcesso.getEmpresa());
		}

		if (controleAcesso.getUnidadeOrganizacional() != null) {
			funcionariotmp.setUnidadeOrganizacional(controleAcesso.getUnidadeOrganizacional());
		}

		if (controleAcesso.getEmail() != null) {
			funcionariotmp.setEmail(controleAcesso.getEmail());
		}

		if (controleAcesso.getFuncionario() != null) {
			usuario.setFuncionario(controleAcesso.getFuncionario());
		}

		if (controleAcesso.getLogin() != null) {
			usuario.setLogin(controleAcesso.getLogin());
		}

		if (controleAcesso.getUsuarioDominio() != null) {
			usuario.setUsuarioDominio(controleAcesso.getUsuarioDominio());
		}

		if (controleAcesso.getPeriodicidadeSenha() != null && !controleAcesso.getPeriodicidadeSenha().isEmpty()) {

			usuario.setPeriodicidadeSenha(
					Util.converterCampoStringParaValorInteger("Periodicidade Senha", controleAcesso.getPeriodicidadeSenha()));
			usuario.setDataCriacaoSenha(new Date());
		}

		if (controleAcesso.getAdminAtendimento() != null) {
			usuario.setAdministradorAtendimento(controleAcesso.getAdminAtendimento());
		}

		if (controleAcesso.getSenhaExpirada() == null) {
			usuario.setSenhaExpirada(Boolean.FALSE);
			usuario.setHabilitado(Boolean.TRUE);
		} else if (controleAcesso.getSenhaExpirada()) {
			usuario.setSenhaExpirada(Boolean.TRUE);
			usuario.setHabilitado(Boolean.FALSE);
		}

		if (controleAcesso.getPapelSelecionado() != null && controleAcesso.getPapelSelecionado().length > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(FORM_CHAVES_PRIMARIAS, controleAcesso.getPapelSelecionado());
			Collection<Papel> listaPapeis = controladorUsuario.consultarPapeis(filtro);
			Collection<Papel> papeis = new HashSet<Papel>();
			model.addAttribute(LISTA_PAPEIS_SELECIONADOS, papeis);
			papeis.addAll(listaPapeis);
			usuario.setPapeis(papeis);
		}

		funcionariotmp.setUsuario(usuario);
	}

	/**
	 * @throws NegocioException {@link NegocioException}
	 * 
	 */
	private void popularControleAcesso(ControleAcessoVO controleAcesso, UsuarioImpl usuario, Model model) throws NegocioException {

		if (usuario != null) {
			controleAcesso.setUnidadeOrganizacional(usuario.getFuncionario().getUnidadeOrganizacional());
			controleAcesso.setEmpresa(usuario.getFuncionario().getEmpresa());

			controleAcesso.setChavePrimaria(usuario.getFuncionario().getChavePrimaria());
			controleAcesso.setLogin(usuario.getLogin());

			controleAcesso.setUsuarioDominio(usuario.getUsuarioDominio());

			if (usuario.getPeriodicidadeSenha() != null) {
				controleAcesso.setPeriodicidadeSenha(String.valueOf(usuario.getPeriodicidadeSenha()));
			}

			controleAcesso.setEmail(usuario.getFuncionario().getEmail());
			controleAcesso.setSenhaExpirada(usuario.isSenhaExpirada());
			controleAcesso.setAdminAtendimento(usuario.isAdministradorAtendimento());
			controleAcesso.setFuncionario(usuario.getFuncionario());
			controleAcesso.setEmpresa(usuario.getFuncionario().getEmpresa());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_EMPRESA, usuario.getFuncionario().getEmpresa().getChavePrimaria());
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);
			model.addAttribute(LISTA_UNIDADE_ORGANIZACIONAL, unidades);

			filtro = new HashMap<String, Object>();
			if (usuario.getFuncionario().getUnidadeOrganizacional() != null) {
				filtro.put(ID_UNIDADE_ORGANIZACIONAL, usuario.getFuncionario().getUnidadeOrganizacional().getChavePrimaria());
			}

			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);

			Collection<Funcionario> funcionarios = controladorFuncionario.consultarFuncionarios(filtro);
			model.addAttribute("listaFuncionarios", funcionarios);

			Collection<Papel> listaPapelCadastrado = new ArrayList<Papel>();
			Collection<Papel> listaPapel = controladorUsuario.consultarPapeisUsuario();

			if (usuario.getPapeis() != null && !usuario.getPapeis().isEmpty()) {

				if (listaPapel != null && !listaPapel.isEmpty()) {
					for (Papel papel : listaPapel) {
						if (!usuario.getPapeis().contains(papel)) {
							listaPapelCadastrado.add(papel);
						}
					}
				}

			} else {
				listaPapelCadastrado.addAll(listaPapel);
			}

			model.addAttribute(LISTA_PAPEIS_SELECIONADOS, usuario.getPapeis());
			model.addAttribute(LISTA_PAPEL, listaPapelCadastrado);
		} else {
			model.addAttribute(LISTA_PAPEL, controladorPapel.consultarPapel(null));
		}
	}

}
