/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.controleacesso;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ValidadorAcessoMenuTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2186561140086369936L;

	private String param;

	public String getParam() {

		return param;
	}

	public void setParam(String param) {

		this.param = param;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.BodyTagSupport#doAfterBody()
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public int doAfterBody() throws JspException {

		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		Map<String, Boolean> mapaAcessos = (Map) request.getSession().getAttribute("mapaControleAcesso");
		boolean contains = Boolean.FALSE;
		if (!StringUtils.isEmpty(param) && mapaAcessos != null) {
			String[] params = null;
			if (param.indexOf(',') != -1) {
				params = param.split(",");
			} else {
				params = new String[] {param};
			}

			for (int i = 0; i < params.length; i++) {
				if (mapaAcessos.containsKey(params[i])) {
					contains = Boolean.TRUE;
					break;
				}
			}

			try {
				BodyContent bodycontent = getBodyContent();
				String body = bodycontent.getString();
				JspWriter out = bodycontent.getEnclosingWriter();
				if (!contains || body == null) {
					out.print("");
				} else {
					out.print(body.toString());
				}
			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return SKIP_BODY;
	}
}
