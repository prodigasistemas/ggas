/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.controleacesso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.ModuloFuncOperacoesVO;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.OperacaoItemVO;
import br.com.ggas.controleacesso.impl.PapelImpl;
import br.com.ggas.controleacesso.impl.PermissaoImpl;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;


/**
 * 
 * Classe controladora responsável por gerenciar os eventos e acionar as
 * classes e seus respectivos metodos relacionados as regras de negócio
 * e de modelo para realizar alterações nas informações das telas
 * referentes a Controle de Perfil.
 * 
 * @author bruno silva
 *
 *         
 */
@Controller
public class PapelAction extends GenericAction {
	
	private static final String PAPEL = "papel";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela"; 
	
	private static final String HABILITADO = "habilitado";

	private static final String PERMISSOES = "permissoes";

	private static final String FUNCIONARIO = "funcionario";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_OPERACAO_JSON = "chkOperacao";

	private static final String DESCRICAO = "descricao";

	private static final String RESPONSAVEL = "idResponsavel";

	public static final String ATIVO = "Ativo";

	public static final String INATIVO = "Inativo";

	private static final String LISTA_ADICIONADOS = "listaAdicionados";

	private static final String ARVORE_OPERACAO = "arvoreOperacao";

	private static final String OPERACAO_ITEM_LISTA_JSON = "operacaoItemLista";

	@Autowired
	private ControladorPapel controladorPapel;
	
	@Autowired
	private ControladorUsuario controladorUsuario;
	
	@Autowired
	private ControladorAcesso controladorAcesso;
	
	@Autowired
	private ControladorFuncionario controladorFuncionario;
	
	@Autowired
	private ControladorMenu controladorMenu;
	
	@Autowired
	private ControladorModulo controladorModulo;
	
	/**
	 * Método responsável por exibir a tela de
	 * pasquisar papeis.
	 * 
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaPapel")
	public String exibirPesquisaPapel(Model model) throws GGASException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaPapel";
	}

	/**
	 * Método responsável por pesquisar um papel
	 * do sistema.
	 * 
	 * @param papel {@link papel}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param habilitado {@link Boolean}
	 * @param model {@link Model}
	 * @return exibirPesquisaPapel {@link String} 
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarPapel")
	public String pesquisarPapel(PapelImpl papel, BindingResult result, HttpServletRequest request,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(papel, habilitado, model);

		Collection<Papel> papeis = controladorPapel.consultarPapel(filtro);

		model.addAttribute("listaPapeis", papeis);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(PAPEL, papel);

		return exibirPesquisaPapel(model);
	}
	
	/**
	 * Exibir detalhamento papel.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoPapel")
	public String exibirDetalhamentoPapel(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		String tela = "forward:exibirPesquisaPapel";

		try {
			Papel papel = (Papel) controladorPapel.obter(chavePrimaria, FUNCIONARIO, PERMISSOES);

			model.addAttribute(LISTA_ADICIONADOS, preencherListaPermissoes(papel));

			model.addAttribute(PAPEL, papel);
			model.addAttribute(HABILITADO, habilitado);

			tela = "exibirDetalhamentoPapel";
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return tela;
	}
	
	/**
	 * Método responsável por exibir a tela de incluir papel.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirInclusaoPapel {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping("exibirInclusaoPapel")
	public String exibirInclusaoPapel(HttpServletRequest request, Model model) throws GGASException {

		if (!super.isPostBack(request)) {
			request.getSession().removeAttribute(LISTA_ADICIONADOS);
		}

		List<ModuloFuncOperacoesVO> listaAdicionados = (List) request.getSession().getAttribute(LISTA_ADICIONADOS);
		
		if (listaAdicionados != null) {
			ordenaComparaListaModuloFuncionalidadeOperacoes(listaAdicionados);
		}
		
		request.setAttribute(LISTA_ADICIONADOS, listaAdicionados);

		carregarDados(model);

		saveToken(request);

		return "exibirInclusaoPapel";
	}

	/**
	 * Método responsável por incluir um papel do
	 * sistema.
	 *  
	 * @param papel {@link PapelImpl}
	 * @param result {@link BindingResult}
	 * @param listaOperacaoItens {@link String}
	 * @param idFuncionario {@link Long}
	 * @param arvoreOperacao {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String} 
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirPapel")
	public String incluirPapel(PapelImpl papel, BindingResult result,
			@RequestParam(value = LISTA_OPERACAO_JSON, required = false) String[] listaOperacaoItens,
			@RequestParam(value = RESPONSAVEL, required = false) Long idFuncionario,
			@RequestParam(value = ARVORE_OPERACAO, required = false) String arvoreOperacao, HttpServletRequest request,
			Model model) throws GGASException {

		String tela = null;

		try {

			papel.setHabilitado(Boolean.TRUE);

			if (listaOperacaoItens != null) {
				Gson gson = new Gson();
				
				Set<Permissao> listaPermissoes = new HashSet<Permissao>();
				
				for (int x = 0; x < listaOperacaoItens.length; x++) {
					OperacaoItemVO currentItem = gson.fromJson(listaOperacaoItens[x], OperacaoItemVO.class);

					Permissao permissao = (Permissao) controladorAcesso.criarPermissao();

					Operacao operacao = (Operacao) controladorModulo.criarOperacao();
					operacao.setChavePrimaria(currentItem.getIdOperacaoSistema());

					Menu menu = (Menu) controladorMenu.criar();
					menu.setChavePrimaria(currentItem.getIdMenu());

					permissao.setOperacao(operacao);
					permissao.setMenu(menu);
					permissao.setTipo(0);
					// FIXME : verificar que tipo é esse
					permissao.setUltimaAlteracao(Calendar.getInstance().getTime());
					// FIXME
					permissao.setDadosAuditoria(getDadosAuditoria(request));

					listaPermissoes.add(permissao);
				}

				papel.setPermissoes(listaPermissoes);
			}

			if (idFuncionario != null && idFuncionario > 0) {
				Funcionario responsavel = (Funcionario) controladorFuncionario.obter(idFuncionario);
				papel.setFuncionario(responsavel);
			}

			papel.setDadosAuditoria(getDadosAuditoria(request));

			model.addAttribute(ARVORE_OPERACAO, arvoreOperacao);

			controladorPapel.inserir(papel);

			model.addAttribute(ARVORE_OPERACAO, null);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Papel.ENTIDADE_ROTULO_PAPEL);
			
			request.getSession().removeAttribute(LISTA_ADICIONADOS);

			tela = pesquisarPapel(papel, result, request, papel.isHabilitado(), model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = exibirInclusaoPapel(request, model);
		}

		model.addAttribute(PAPEL, papel);

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de
	 * atualização de papel.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param arvoreOperacoes {@link String}
	 * @param descricao {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoPapel")
	public String exibirAlteracaoPapel(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = ARVORE_OPERACAO, required = false) String arvoreOperacoes,
			@RequestParam(value = DESCRICAO, required = false) String descricao, HttpServletRequest request,
			Model model) throws GGASException {

		String tela = "exibirAlteracaoPapel";

		Boolean postBack = isPostBack(request);

		PapelImpl papel = new PapelImpl();
		model.addAttribute(DESCRICAO, descricao);

		List<OperacaoItemVO> operacaoItemVOList = new ArrayList<OperacaoItemVO>();

		if (!postBack) {

			try {

				papel = (PapelImpl) controladorUsuario.obterPapel(chavePrimaria, FUNCIONARIO, PERMISSOES);

				model.addAttribute(PAPEL, papel);

				popularListaItemOperacao(papel, operacaoItemVOList, postBack);

			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				tela = pesquisarPapel(null, null, request, Boolean.TRUE, model);
			}

		} else {

			popularListaItemOperacao(papel, operacaoItemVOList, postBack);
		}

		carregarDados(model);

		model.addAttribute(OPERACAO_ITEM_LISTA_JSON, new Gson().toJson(operacaoItemVOList.toArray()));

		model.addAttribute(ARVORE_OPERACAO, arvoreOperacoes);

		saveToken(request);

		return tela;
	}
	
	/**
	 * Método responsável por atualizar um papel
	 * do sistema.
	 * 
	 * @param papelParamTela {@link PapelImpl}
	 * @param result {@link BindingResult}
	 * @param chavePrimaria {@link Long}
	 * @param idFuncionario {@link Long}
	 * @param listaAdicionadosJson {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String} 
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("atualizarPapel")
	public String atualizarPapel(PapelImpl papelParamTela, BindingResult result,
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = RESPONSAVEL, required = false) Long idFuncionario,
			@RequestParam(value = LISTA_OPERACAO_JSON, required = false) String[] listaAdicionadosJson,
			HttpServletRequest request, Model model) throws GGASException {

		String tela = "forward:exibirAlteracaoPapel";
		String arvoreOperacoes = request.getParameter(ARVORE_OPERACAO);

		PapelImpl papel = (PapelImpl) controladorUsuario.obterPapel(chavePrimaria, PERMISSOES);
		papel.setDescricao(papelParamTela.getDescricao());
		papel.setComplemento(papelParamTela.getComplemento());
		papel.setHabilitado(papelParamTela.isHabilitado());

		if (idFuncionario != null && idFuncionario > 0) {
			Funcionario responsavel = (Funcionario) controladorFuncionario.obter(idFuncionario);
			papel.setFuncionario(responsavel);
		} else {
			papel.setFuncionario(null);
		}

		model.addAttribute(ARVORE_OPERACAO, arvoreOperacoes);

		Set<Permissao> listaNova = new HashSet<>();

		Map<String, Object> erros = null;

		Gson gson = new Gson();

		try {

			if (listaAdicionadosJson != null && listaAdicionadosJson.length > 0) {
				for (String currentOperacaoItemJson : listaAdicionadosJson) {
					OperacaoItemVO operacaoItemVO = gson.fromJson(currentOperacaoItemJson, OperacaoItemVO.class);

					Permissao permissao = new PermissaoImpl();

					Operacao operacao = (Operacao) controladorModulo.criarOperacao();
					operacao.setChavePrimaria(operacaoItemVO.getIdOperacaoSistema());

					permissao.setOperacao(operacao);
					permissao.setMenu((Menu) controladorMenu.obter(operacaoItemVO.getIdMenu()));
					permissao.setTipo(0);
					permissao.setPapel(papel);

					listaNova.add(permissao);
				}
			}
			
			Operacao operacao = controladorAcesso.obterOperacaoSistemaPelaDescricaoOperacaoEMenu("CONSULTAR", "Cadastro");
			
			Permissao permissao = new PermissaoImpl();
			permissao.setOperacao(operacao);
			permissao.setMenu(operacao.getMenu());
			permissao.setTipo(0);
			permissao.setPapel(papel);
			
			listaNova.add(permissao);

			papel.setPermissoes(listaNova);

			Usuario usuario = (Usuario) request.getSession().getAttribute("usuarioLogado");
			papel.setDadosAuditoria(getDadosAuditoria(request));

			model.addAttribute(PAPEL, papel);

			erros = papel.validarDados();

			if (!erros.isEmpty()) {
				mensagemErroParametrizado(model, request, new NegocioException(erros));
			} else {

				controladorPapel.atualizarPapel(papel, usuario, listaNova);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Papel.ENTIDADE_ROTULO_PAPEL);
				request.getSession().removeAttribute(LISTA_ADICIONADOS);
				tela = pesquisarPapel(papel, result, request, papel.isHabilitado(), model);

			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}
	
	/**
	 * Método responsável por excluir papeis do
	 * sistema.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param descricao {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerPapeis")
	public String removerPapeis(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = DESCRICAO, required = false) String descricao, HttpServletRequest request,
			Model model) throws GGASException {

		getDadosAuditoria(request);

		String tela = "forward:pesquisarPapel";

		model.addAttribute(DESCRICAO, descricao);

		try {
			controladorPapel.removerPapeis(chavesPrimarias);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Papel.ENTIDADE_ROTULO_PAPEL);
			tela = pesquisarPapel(null, null, request, habilitado, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return tela;

	}

	/**
	 * Método responsável por popular atributos da classe operacaoItemVO
	 * 
	 * @param papel {@link PapelImpl}
	 * @param operacaoItemVOList {@link OperacaoItemVO}
	 * @param postBack {@link Boolean}
	 */
	public void popularListaItemOperacao(PapelImpl papel, List<OperacaoItemVO> operacaoItemVOList, Boolean postBack) {

		Map<Menu, Collection<Operacao>> mapaOperacoes = new HashMap<Menu, Collection<Operacao>>();
		Set<Permissao> colecaoPermissao = papel.getPermissoes();

		for (Permissao permissao : colecaoPermissao) {

			if (!postBack) {

				if (mapaOperacoes.containsKey(permissao.getMenu())) {
					mapaOperacoes.get(permissao.getMenu()).add(permissao.getOperacao());
				} else {
					Collection<Operacao> colOp = new ArrayList<Operacao>();
					colOp.add(permissao.getOperacao());
					mapaOperacoes.put(permissao.getMenu(), colOp);
				}

			}

			OperacaoItemVO operacaoItemVO = new OperacaoItemVO();

			operacaoItemVO.setIdMenu(permissao.getMenu().getChavePrimaria());
			operacaoItemVO.setIdOperacaoSistema(permissao.getOperacao().getChavePrimaria());
			operacaoItemVO.setIdPapel(papel.getChavePrimaria());
			operacaoItemVO.setIsSelecionado(true);
			operacaoItemVO.setDescricaoOperacao(permissao.getOperacao().getDescricao());

			operacaoItemVOList.add(operacaoItemVO);

		}
	}

	/**
	 * Carregar dados.
	 * 
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void carregarDados(Model model) throws NegocioException {

		Collection<Funcionario> listaResponsavel = controladorFuncionario.listarFuncionariosEmpresaPrincipal();
		model.addAttribute("listaResponsavel", listaResponsavel);

		Collection<Menu> listaModulos = controladorMenu.listarModulos(new HashMap<String, Object>());
		model.addAttribute("listaModulos", listaModulos);
	}

	/**
	 * Gerar relatorio papel.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param model {@link Model}
	 * @return responseEntity {@link ResponseEntity}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioPapel")
	public ResponseEntity<byte[]> gerarRelatorioPapel(
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria, Model model)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");
		filtro.put(FORMATO_IMPRESSAO, formatoImpressao);

		Papel papel = null;

		if (chavePrimaria != null) {
			papel = controladorUsuario.obterPapel(chavePrimaria, FUNCIONARIO, PERMISSOES);
		}

		ResponseEntity<byte[]> responseEntity = null;

		if (papel != null) {
			byte[] relatorioPerfil = controladorPapel.gerarRelatorioPapel(filtro, papel,
					preencherListaPermissoes(papel));

			if (relatorioPerfil != null) {

				FormatoImpressao formatoImpressao2 = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);

				MediaType contentType = MediaType.parseMediaType(this.obterContentTypeRelatorio(formatoImpressao2));

				String filename = String.format("relatorioPerfil%s",
						this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));

				HttpHeaders headers = new HttpHeaders();

				headers.setContentType(contentType);
				headers.setContentDispositionFormData("attachment", filename);

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				responseEntity = new ResponseEntity<byte[]>(relatorioPerfil, headers, HttpStatus.OK);
			}
		}

		return responseEntity;
	}

	/**
	 * Preencher lista permissoes.
	 * 
	 * @param papel {@link PapelImpl}
	 * @return listaAdicionados {@link ModuloFuncOperacoesVO}
	 */
	public List<ModuloFuncOperacoesVO> preencherListaPermissoes(Papel papel) {

		Map<Menu, Collection<Operacao>> mapaOperacoes = new HashMap<Menu, Collection<Operacao>>();

		Set<Permissao> colecaoPermissao = papel.getPermissoes();
		for (Permissao permissa : colecaoPermissao) {
			if (mapaOperacoes.containsKey(permissa.getMenu())) {
				mapaOperacoes.get(permissa.getMenu()).add(permissa.getOperacao());
			} else {
				Collection<Operacao> colOp = new ArrayList<Operacao>();
				colOp.add(permissa.getOperacao());
				mapaOperacoes.put(permissa.getMenu(), colOp);
			}
		}

		List<ModuloFuncOperacoesVO> listaAdicionados = new ArrayList<ModuloFuncOperacoesVO>();
		for (Map.Entry<Menu, Collection<Operacao>> entry : mapaOperacoes.entrySet()) {
			Menu menu = entry.getKey();
			ModuloFuncOperacoesVO vo = new ModuloFuncOperacoesVO();
			vo.setFuncionalidade(menu);

			Collection<Operacao> colOp = entry.getValue();
			if (colOp != null && !colOp.isEmpty()) {
				vo.setModulo(colOp.iterator().next().getModulo());
			}
			vo.setOperacoes(colOp);
			listaAdicionados.add(vo);
		}

		ordenaComparaListaModuloFuncionalidadeOperacoes(listaAdicionados);

		return listaAdicionados;
	}

	/**
	 * Método responsável por ordenar e comparar lista de ModuloFuncOperacoesVO
	 * 
	 * @param listaAdicionados {@link ModuloFuncOperacoesVO}
	 */
	public void ordenaComparaListaModuloFuncionalidadeOperacoes(List<ModuloFuncOperacoesVO> listaAdicionados) {

		Collections.sort(listaAdicionados, new Comparator<ModuloFuncOperacoesVO>() {

			@Override
			public int compare(ModuloFuncOperacoesVO vo1, ModuloFuncOperacoesVO vo2) {

				int retorno = 0;

				if (vo1.getModulo() != null && vo2.getModulo() != null) {
					retorno = verificarModuloFuncionarioOperacao(vo1, vo2);
				} else if (vo1.getModulo() != null) {
					retorno = 1;
				} else {
					retorno = -1;
				}

				return retorno;
			}

		});
	}
	
	/**
	 * Método responsável por verificar os atributos modulo e funcionalidade da
	 * classe ModuloFuncOperacoesVO antes de invocar o método compareTo
	 * 
	 * @param vo1 {@link ModuloFuncOperacoesVO}
	 * @param vo2 {@link ModuloFuncOperacoesVO}
	 * @return retorno {@link Integer}
	 */
	public int verificarModuloFuncionarioOperacao(ModuloFuncOperacoesVO vo1, ModuloFuncOperacoesVO vo2) {

		int retorno;

		if (vo1.getModulo().equals(vo2.getModulo())) {
			if (vo1.getFuncionalidade() != null && vo2.getFuncionalidade() != null) {
				retorno = vo1.getFuncionalidade().getDescricao().compareTo(vo2.getFuncionalidade().getDescricao());
			} else if (vo1.getFuncionalidade() != null) {
				retorno = 1;
			} else {
				retorno = -1;
			}
		} else {
			retorno = vo1.getModulo().getDescricao().compareTo(vo2.getModulo().getDescricao());
		}
		return retorno;
	}
	
	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param papel {@link PapelImpl}
	 * @param habilitado {@link Boolean}
	 * @param model {@link Model}
	 * @return filtro {@link Map}
	 */
	private Map<String, Object> prepararFiltro(Papel papel, Boolean habilitado, Model model) {

		Map<String, Object> filtro = new HashMap<>();

		if(papel != null) {
			
			if (papel.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, papel.getChavePrimaria());
			}
			
			if (!papel.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, papel.getDescricao());
			}
			
		}else {
			filtro.put(DESCRICAO, model.asMap().get(DESCRICAO));
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, String.valueOf(habilitado));
		}

		return filtro;
	}
	
	/**
	 * Responsável por redefinir o tratamento de {@link String} contendo decimais
	 * separados por vírgula.
	 * 
	 * @param binder
	 *            - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}
	
}
