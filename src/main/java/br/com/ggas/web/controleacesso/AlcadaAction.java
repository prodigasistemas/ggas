package br.com.ggas.web.controleacesso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.AlcadaVO;
import br.com.ggas.controleacesso.AlteracaoAlcadaVO;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

/**
 * 
 * Classe responsável pelas telas e funcionalidades relacionadas ao controle de alçada
 */
@Controller
public class AlcadaAction extends GenericAction {

	private static final String ALCADA = "alcada";

	private static final String MENUS = "menus";

	private static final String LISTA_ALCADAS_ALTERACAO = "listaAlcadasAlteracao";

	private static final String DATA_FINAL = "Data Final";

	private static final String DATA_INICIAL = "Data Inicial";

	private static final String ID_MENU = "idMenu";

	private static final String ID_PAPEL = "idPapel";

	private static final String DATA_INICIAL_ALCADA = "dataInicialAlcada";

	private static final String DATA_FINAL_ALCADA = "dataFinalAlcada";

	private static final String LISTA_ALCADAS_CONSULTADAS = "listaAlcadasConsultadas";

	private static final String DESCRICAO_PAPEL = "descricaoPapel";

	private static final String DESCRICAO_FUNCIONALIDADE = "descricaoFuncionalidade";

	private static final String VALORES_INICIAIS = "listaValoresIniciais";

	private static final String VALORES_FINAIS = "listaValoresFinais";

	private static final String DATA_INICIAL_ALTERACAO = "dataInicialAlteracao";

	private static final String DATA_FINAL_ALTERACAO = "dataFinalAlteracao";

	@Autowired
	private ControladorMenu controladorMenu;

	@Autowired
	private ControladorAlcada controladorAlcada;

	@Autowired
	private ControladorUsuario controladorUsuario;

	/**
	 * Método responsável por exibir a tela de pesquisa de alçada
	 * 
	 * @param model {@link Model}
	 * @return view {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaAlcada")
	public String exibirPesquisaAlcada(Model model) {

		try {
			model.addAttribute(MENUS, controladorMenu.listarFuncionalidadesFilho());
		} catch (NegocioException e) {
			super.mensagemErro(model, e);
		}

		return "exibirPesquisaAlcada";
	}

	/**
	 * Método para pesquisar alçadas.
	 * 
	 * @param model - {@link Model}
	 * @param alcada - {@link AlcadaVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarAlcada")
	public String pesquisarAlcada(Model model, AlcadaVO alcada) {

		try {
			model.addAttribute(MENUS, controladorMenu.listarFuncionalidadesFilho());

			Map<String, Object> filtro = this.prepararFiltro(alcada);

			model.addAttribute(LISTA_ALCADAS_CONSULTADAS, controladorAlcada.obterListaAlcadaVO(filtro));
			model.addAttribute(ALCADA, alcada);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaAlcada";
	}

	/**
	 * Método para processar a exibição do detalhe da alçada.
	 * 
	 * @param model - {@link Model}
	 * @param alcada - {@link AlcadaVO}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalharAlcada")
	public String exibirDetalharAlcada(Model model, AlcadaVO alcada, HttpServletRequest request) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltroExibirAlteracao(alcada, request);

		Collection<AlteracaoAlcadaVO> listaAlcadas = controladorAlcada.obterListaAlteracaoAlcada(filtro);
		AlteracaoAlcadaVO alteracaoAlcadaVO = null;
		for (AlteracaoAlcadaVO alcadaVO : listaAlcadas) {
			if (alcadaVO.getAlcada() != null) {
				alteracaoAlcadaVO = alcadaVO;
			}
		}

		if(alteracaoAlcadaVO != null) {
			model.addAttribute("alteracaoAlcadaVO", alteracaoAlcadaVO);
			model.addAttribute(DATA_INICIAL_ALCADA, DataUtil.converterDataParaString(alteracaoAlcadaVO.getAlcada().getDataInicial()));
			model.addAttribute(DATA_FINAL_ALCADA, DataUtil.converterDataParaString(alteracaoAlcadaVO.getAlcada().getDataFinal()));
		}
		
		request.getSession().setAttribute(LISTA_ALCADAS_ALTERACAO, listaAlcadas);

		return "exibirDetalharAlcada";
	}

	/**
	 * Metodo responsavel por exibir a tela de definição de alçada.
	 * 
	 * @param model {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDefinicaoAlcada")
	public String exibirDefinicaoAlcada(Model model) {

		try {
			model.addAttribute(MENUS, controladorMenu.listarFuncionalidadesFilho());
		} catch (NegocioException e) {
			super.mensagemErro(model, e);
		}

		return "exibirDefinicaoAlcada";
	}

	/**
	 * Método para exibir os campos disponíveis para a funcionalidade selecionada.
	 * 
	 * @param alcada - {@link AlcadaVO}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAlcada")
	public String exibirAlcada(AlcadaVO alcada, Model model) {

		try {
			model.addAttribute(MENUS, controladorMenu.listarFuncionalidadesFilho());
			model.addAttribute("papeis", controladorUsuario.consultarPapeisUsuario());

			Map<String, Object> filtro = this.prepararFiltro(alcada);
			Long idMenu = alcada.getIdMenu();

			if (idMenu != null && idMenu > 0) {
				Collection<Coluna> listaCampos = controladorAlcada.obterListaColunasAlcadaPorMenu(idMenu);
				if (listaCampos != null && !listaCampos.isEmpty()) {
					model.addAttribute("listaCampos", listaCampos);
				} else {
					model.addAttribute("listaCampos", null);
				}

				model.addAttribute("listaAlcadas", controladorAlcada.obterListaAlcadaVO(filtro));
			}
			model.addAttribute(ALCADA, alcada);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return exibirDefinicaoAlcada(model);
	}

	/**
	 * Método para processar a inserção da alçada.
	 * 
	 * @param alcadaVO - {@link AlcadaVO}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirAlcada")
	public String incluirAlcada(AlcadaVO alcadaVO, Model model, HttpServletRequest request) {

		String view = null;
		Long idMenu = alcadaVO.getIdMenu();
		Long idPapel = alcadaVO.getIdPapel();
		Date dataInicial = null;
		Date dataFinal = null;
		String dataInicialAux = alcadaVO.getDataInicialAlcada();
		String dataFinalAux = alcadaVO.getDataFinalAlcada();
		try {
			getDadosAuditoria(request);

			if (!StringUtils.isEmpty(dataInicialAux)) {
				dataInicial = Util.converterCampoStringParaData(DATA_INICIAL, dataInicialAux, Constantes.FORMATO_DATA_BR);
			}

			if (!StringUtils.isEmpty(dataFinalAux)) {
				dataFinal = Util.converterCampoStringParaData(DATA_FINAL, dataFinalAux, Constantes.FORMATO_DATA_BR);
			}

			controladorAlcada.validarAlcadas(idMenu, idPapel, dataInicial, dataFinal);

			Menu menu = (Menu) controladorMenu.obter(idMenu);
			Papel papel = controladorUsuario.obterPapel(idPapel);
			Collection<Alcada> listaAlcadaInsercao = new ArrayList<Alcada>();
			Collection<Coluna> listaCampos = controladorAlcada.obterListaColunasAlcadaPorMenu(idMenu);
			for (Coluna coluna : listaCampos) {
				BigDecimal valorInicial = null;
				BigDecimal valorFinal = null;
				String campoInicial = request.getParameter(coluna.getNomePropriedade() + "Inicial");
				String campoFinal = request.getParameter(coluna.getNomePropriedade() + "Final");
				if (!campoInicial.isEmpty()) {
					valorInicial = Util.converterCampoStringParaValorBigDecimal("Valor Inicial", campoInicial, Constantes.FORMATO_VALOR_BR,
							Constantes.LOCALE_PADRAO);
				}
				if (!campoFinal.isEmpty()) {
					valorFinal = Util.converterCampoStringParaValorBigDecimal("Valor Final", campoFinal, Constantes.FORMATO_VALOR_BR,
							Constantes.LOCALE_PADRAO);
				}
				if (valorInicial != null || valorFinal != null) {
					Alcada alcada = (Alcada) controladorAlcada.criar();
					alcada.setMenu(menu);
					alcada.setDataInicial(dataInicial);
					alcada.setDataFinal(dataFinal);
					alcada.setColuna(coluna);
					alcada.setValorInicial(valorInicial);
					alcada.setValorFinal(valorFinal);
					alcada.setPapel(papel);
					listaAlcadaInsercao.add(alcada);
				}
			}

			controladorAlcada.inserirAlcadas(listaAlcadaInsercao);
			alcadaVO.setHabilitado(Boolean.TRUE);
			model.addAttribute(ALCADA, alcadaVO);
			view = exibirPesquisaAlcada(model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = exibirAlcada(alcadaVO, model);
		}
		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Alcada.ALCADA_CAMPO_STRING);

		return view;
	}

	/**
	 * Método responsável por exibir a tela de alteração da alçada.
	 * 
	 * @param alcada - {@link AlcadaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAlterarAlcada")
	public String exibirAlterarAlcada(AlcadaVO alcada, BindingResult bindingResult, Model model, HttpServletRequest request) {

		String view = "exibirAlterarAlcada";

		try {
			Map<String, Object> filtro = this.prepararFiltroExibirAlteracao(alcada, request);

			Collection<AlteracaoAlcadaVO> listaAlcadas = controladorAlcada.obterListaAlteracaoAlcada(filtro);
			AlteracaoAlcadaVO alteracaoAlcadaVO = null;
			for (AlteracaoAlcadaVO alcadaVO : listaAlcadas) {
				if (alcadaVO.getAlcada() != null) {
					alteracaoAlcadaVO = alcadaVO;
				}
			}

			if(alteracaoAlcadaVO != null) {
				alcada.setDescricaoPapel(alteracaoAlcadaVO.getAlcada().getPapel().getDescricao());
				alcada.setDescricaoFuncionalidade(alteracaoAlcadaVO.getAlcada().getMenu().getDescricao());
				alcada.setDataInicial(alteracaoAlcadaVO.getAlcada().getDataInicial());
				alcada.setDataFinal(alteracaoAlcadaVO.getAlcada().getDataFinal());
				alcada.setHabilitado(alteracaoAlcadaVO.getAlcada().isHabilitado());
			}
			
			this.tratarRetornoErroAlteracao(request, alcada, listaAlcadas);

			this.setDadosSessao(filtro, request);
			request.getSession().setAttribute(LISTA_ALCADAS_ALTERACAO, listaAlcadas);
			model.addAttribute(ALCADA, alcada);
			model.addAttribute("listaAlcadas", controladorAlcada.obterListaAlcadaVO(filtro));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/pesquisarAlcada";
		}

		return view;
	}

	/**
	 * Método para processar a alteraração alçada.
	 * 
	 * @param model - {@link Model}
	 * @param alcada - {@link AlcadaVO}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarAlcada")
	public String alterarAlcada(Model model, AlcadaVO alcada, HttpServletRequest request) {

		String view = null;
		try {
			getDadosAuditoria(request);
			String[] listaValoresIniciais = alcada.getListaValoresIniciais();
			String[] listaValoresFinais = alcada.getListaValoresFinais();
			Boolean habilitado = alcada.getHabilitado();
			Date dataInicial = null;
			Date dataFinal = null;

			String dataInicialAux = alcada.getDataInicialAlcada();
			String dataFinalAux = alcada.getDataFinalAlcada();

			if (!StringUtils.isEmpty(dataInicialAux)) {
				dataInicial = Util.converterCampoStringParaData(DATA_INICIAL, dataInicialAux, Constantes.FORMATO_DATA_BR);
			}

			if (!StringUtils.isEmpty(dataFinalAux)) {
				dataFinal = Util.converterCampoStringParaData(DATA_FINAL, dataFinalAux, Constantes.FORMATO_DATA_BR);
			}

			request.getSession().setAttribute(VALORES_INICIAIS, listaValoresIniciais);
			request.getSession().setAttribute(VALORES_FINAIS, listaValoresFinais);
			request.getSession().setAttribute(DATA_INICIAL_ALTERACAO, dataInicial);
			request.getSession().setAttribute(DATA_FINAL_ALTERACAO, dataFinal);

			Collection<AlteracaoAlcadaVO> listaAlcadas =
					(Collection<AlteracaoAlcadaVO>) request.getSession().getAttribute(LISTA_ALCADAS_ALTERACAO);

			controladorAlcada.alterarAlcada(listaAlcadas, listaValoresIniciais, listaValoresFinais, dataInicial, dataFinal, habilitado);
			model.addAttribute(ALCADA, alcada);
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Alcada.ALCADA_CAMPO_STRING);
			view = exibirPesquisaAlcada(model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirAlterarAlcada";
		}

		return view;
	}

	/**
	 * Método responsável por remover as alçadas.
	 * 
	 * @param model - {@link Model}
	 * @param alcada - {@link AlcadaVO}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("removerAlcada")
	public String removerAlcada(Model model, AlcadaVO alcada, HttpServletRequest request) {

		try {
			getDadosAuditoria(request);

			String alcadasRemocao = alcada.getListaAlcadasRemocao();

			controladorAlcada.removerAlcada(alcadasRemocao);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Alcada.ALCADA_CAMPO_STRING);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return pesquisarAlcada(model, alcada);
	}

	/**
	 * Tratar retorno erro alteracao.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param alcada - {@link AlcadaVO}
	 * @param listaAlcadas - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	private void tratarRetornoErroAlteracao(HttpServletRequest request, AlcadaVO alcada, Collection<AlteracaoAlcadaVO> listaAlcadas)
			throws GGASException {

		if (request.getSession().getAttribute(DATA_INICIAL_ALTERACAO) != null) {
			Date dataInicial = (Date) request.getSession().getAttribute(DATA_INICIAL_ALTERACAO);
			Date dataFinal = (Date) request.getSession().getAttribute(DATA_FINAL_ALTERACAO);
			String[] listaValoresIniciais = (String[]) request.getSession().getAttribute(VALORES_INICIAIS);
			String[] listaValoresFinais = (String[]) request.getSession().getAttribute(VALORES_FINAIS);

			alcada.setDataInicial(dataInicial);
			alcada.setDataFinal(dataFinal);
			int i = 0;
			for (AlteracaoAlcadaVO alteracaoAlcadaVO : listaAlcadas) {

				if (!StringUtils.isEmpty(listaValoresIniciais[i])) {
					BigDecimal valorInicial = Util.converterCampoStringParaValorBigDecimal("Valor Inicial", listaValoresIniciais[i],
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

					alteracaoAlcadaVO.setValorInicial(valorInicial);
				}
				if (!StringUtils.isEmpty(listaValoresFinais[i])) {
					BigDecimal valorFinal = Util.converterCampoStringParaValorBigDecimal("Valor Final", listaValoresFinais[i],
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

					alteracaoAlcadaVO.setValorFinal(valorFinal);
				}

				i++;
			}
		}
		this.limparDadosSecao(request);

	}

	private void setDadosSessao(Map<String, Object> filtro, HttpServletRequest request) throws GGASException {

		Long idMenu = (Long) filtro.get(ID_MENU);
		if ((idMenu != null) && (idMenu > 0)) {
			request.getSession().setAttribute(ID_MENU, idMenu);
		}

		Long idPapel = (Long) filtro.get(ID_PAPEL);
		if ((idPapel != null) && (idPapel > 0)) {
			request.getSession().setAttribute(ID_PAPEL, idPapel);
		}

		Date dataInicial = (Date) filtro.get(DATA_INICIAL_ALCADA);
		if (dataInicial != null) {
			request.getSession().setAttribute(DATA_INICIAL_ALCADA, dataInicial);
		}

		Date dataFinal = (Date) filtro.get(DATA_FINAL_ALCADA);
		if (dataFinal != null) {
			request.getSession().setAttribute(DATA_FINAL_ALCADA, dataFinal);
		}

	}

	private void limparDadosSecao(HttpServletRequest request) throws GGASException {

		request.getSession().removeAttribute(ID_MENU);
		request.getSession().removeAttribute(ID_PAPEL);
		request.getSession().removeAttribute(DATA_INICIAL_ALCADA);
		request.getSession().removeAttribute(DATA_FINAL_ALCADA);

		request.getSession().removeAttribute(VALORES_INICIAIS);
		request.getSession().removeAttribute(VALORES_FINAIS);
		request.getSession().removeAttribute(DATA_INICIAL_ALTERACAO);
		request.getSession().removeAttribute(DATA_FINAL_ALTERACAO);

	}

	private Map<String, Object> prepararFiltro(AlcadaVO alcada) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long idMenu = alcada.getIdMenu();
		if ((idMenu != null) && (idMenu > 0)) {
			filtro.put(ID_MENU, idMenu);
		}

		String descricaoFuncionalidade = alcada.getDescricaoFuncionalidade();
		if (!StringUtils.isEmpty(descricaoFuncionalidade)) {
			filtro.put(DESCRICAO_FUNCIONALIDADE, descricaoFuncionalidade);
		}

		String descricaoPapel = alcada.getDescricaoPapel();
		if (!StringUtils.isEmpty(descricaoPapel)) {
			filtro.put(DESCRICAO_PAPEL, descricaoPapel);
		}

		if (alcada.getDataInicial() != null) {
			filtro.put(DATA_INICIAL_ALCADA, alcada.getDataInicial());
		}

		if (alcada.getDataFinal() != null) {
			filtro.put(DATA_FINAL_ALCADA, alcada.getDataFinal());
		}

		return filtro;
	}

	private Map<String, Object> prepararFiltroExibirAlteracao(AlcadaVO alcada, HttpServletRequest request) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (super.isPostBack(request)) {
			Long idMenu = (Long) request.getSession().getAttribute(ID_MENU);
			Long idPapel = (Long) request.getSession().getAttribute(ID_PAPEL);
			Date dataInicial = (Date) request.getSession().getAttribute(DATA_INICIAL_ALCADA);
			Date dataFinal = (Date) request.getSession().getAttribute(DATA_FINAL_ALCADA);

			filtro.put(ID_MENU, idMenu);
			filtro.put(ID_PAPEL, idPapel);
			filtro.put(DATA_INICIAL_ALCADA, dataInicial);
			filtro.put(DATA_FINAL_ALCADA, dataFinal);

		} else {

			if (alcada.getIsDetalhar() != null && alcada.getIsDetalhar()) {

				prepararFiltroFluxoDetalhamento(alcada, filtro);
			} else {
				prepararFiltroFluxoAlteracao(alcada, filtro);
			}
		}

		return filtro;
	}

	private void prepararFiltroFluxoAlteracao(AlcadaVO alcada, Map<String, Object> filtro) throws GGASException {
		String alcadasRemocao = alcada.getListaAlcadasRemocao();
		String[] listaDadosAlcadas = alcadasRemocao.split(",");
		for (String dadosAlcada : listaDadosAlcadas) {
			String[] listaDados = dadosAlcada.split("_");

			Long idMenu = Util.converterCampoStringParaValorLong("Menu", listaDados[0]);
			Long idPapel = Util.converterCampoStringParaValorLong("Papel", listaDados[1]);
			String dataInicialAux = listaDados[2].replace("-", "/");
			Date dataInicial = Util.converterCampoStringParaData(DATA_INICIAL, dataInicialAux, Constantes.FORMATO_DATA_BR);
			String dataFinalAux = listaDados[3].replace("-", "/");
			Date dataFinal = Util.converterCampoStringParaData(DATA_FINAL, dataFinalAux, Constantes.FORMATO_DATA_BR);

			if ((idMenu != null) && (idMenu > 0)) {
				filtro.put(ID_MENU, idMenu);
			}

			if ((idPapel != null) && (idPapel > 0)) {
				filtro.put(ID_PAPEL, idPapel);
			}

			if (dataInicial != null) {
				filtro.put(DATA_INICIAL_ALCADA, dataInicial);
			}

			if (dataFinal != null) {
				filtro.put(DATA_FINAL_ALCADA, dataFinal);
			}

		}
	}

	private void prepararFiltroFluxoDetalhamento(AlcadaVO alcada, Map<String, Object> filtro) {
		Long idMenu = alcada.getIdMenu();
		if (idMenu != null && idMenu > 0) {
			filtro.put(ID_MENU, idMenu);
		}
		Long idPapel = alcada.getIdPapel();
		if (idPapel != null && idPapel > 0) {
			filtro.put(ID_PAPEL, idPapel);
		}
		if (alcada.getDataInicial() != null) {
			filtro.put(DATA_INICIAL_ALCADA, alcada.getDataInicial());
		}

		if (alcada.getDataFinal() != null) {
			filtro.put(DATA_FINAL_ALCADA, alcada.getDataFinal());
		}
	}

	/**
	 * Responsável por redefinir o tratamento de {@link String} contendo decimais separados por vírgula.
	 * 
	 * @param binder - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}
}
