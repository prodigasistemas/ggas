/*
 Copyright (C) <2011> GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este arquivo Ã© parte do GGAS, um sistema de gestÃ£o comercial de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este programa Ã© um software livre; vocÃª pode redistribuÃ­-lo e/ou
 modificÃ¡-lo sob os termos de LicenÃ§a PÃºblica Geral GNU, conforme
 publicada pela Free Software Foundation; versÃ£o 2 da LicenÃ§a.

 O GGAS Ã© distribuÃ­do na expectativa de ser Ãºtil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implÃ­cita de
 COMERCIALIZAÃ‡ÃƒO ou de ADEQUAÃ‡ÃƒO A QUALQUER PROPÃ“SITO EM PARTICULAR.
 Consulte a LicenÃ§a PÃºblica Geral GNU para obter mais detalhes.

 VocÃª deve ter recebido uma cÃ³pia da LicenÃ§a PÃºblica Geral GNU
 junto com este programa; se nÃ£o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place â€“ Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.integracao.geral;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.geral.AcompanhamentoIntegracaoVO;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoFuncaoTabela;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Acompanhar Integração.
 */
@Controller
public class AcompanharIntegracaoAction extends GenericAction {
	
	private static final String HABILITADO = "habilitado";

	private static final Logger LOG = Logger.getLogger(AcompanharIntegracaoAction.class);

	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	
	private static final String VERSAO = "versao";

	private static final String LISTA_TABELA = "listaTabela";
	
	private static final String REGISTRO_ALTERADOS = "registrosAlterados";
	
	private static final String LISTA_FILTROS = "listaFiltros";

	private static final String ULTIMA_ALTERACAO = "ultimaAlteracao";
	
	private static final String IS_SALVO = "isSalvo";
	
	private static final String REGISTRO_SALVOS = "registrosSalvos";
	
	@Autowired
	private ControladorIntegracao controladorIntegracao;
	
	@Autowired
	private ControladorAuditoria controladorAuditoria;
	
	/**
	 * Método responsável por exibir a tela de
	 * acompanhamento da integracao
	 * 
	 * @param integracaoVO					-	{@link IntegracaoVO}
	 * @param result						-	{@link BindingResult}
	 * @param model							-	{@link Model}
	 * @return exibirAcompanharIntegracao	-	{@link String}
	 * @throws NegocioException				-	{@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaAcompanharIntegracao")
	public String exibirPesquisaAcompanharIntegracao(IntegracaoVO integracaoVO, BindingResult result, Model model)
			throws NegocioException {

		carregarCombos(integracaoVO, model);
		return "exibirAcompanharIntegracao";
	}

	/**
	 * Pesquisa acompanhar integracao.
	 * 
	 * @param integracaoVO							-	{@link IntegracaoVO}
	 * @param result								-	{@link BindingResult}
	 * @param request								-	{@link HttpServletRequest}
	 * @param model									-	{@link Model}
	 * @return	exibirPesquisaAcompanharIntegracao	-	{@link String}
	 * @throws GGASException						-	{@link GGASException}
	 */
	@RequestMapping("pesquisaAcompanharIntegracao")
	public String pesquisaAcompanharIntegracao(IntegracaoVO integracaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		try {

			if (integracaoVO.getIdIntegracaoSistema() != null && integracaoVO.getIdIntegracaoSistema() != -1L) {

				IntegracaoSistema integracaoSistema = controladorIntegracao
						.obterIntegracaoSistema(integracaoVO.getIdIntegracaoSistema());

				Map<String, Object> mapa = new HashMap<String, Object>();
				mapa.put("idIntegracaoSistemas", new Long[] { integracaoSistema.getChavePrimaria() });
				Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao = controladorIntegracao
						.consultarIntegracaoSistemaFuncao(mapa);

				if (listaIntegracaoSistemaFuncao != null && !listaIntegracaoSistemaFuncao.isEmpty()) {

					listaIntegracaoSistemaFuncao = validarIndicadorFuncao(integracaoVO, listaIntegracaoSistemaFuncao);

					Collection<IntegracaoFuncaoTabela> listaIntFuncTabela = controladorIntegracao
							.listarIntegracaoFuncaoTabela(listaIntegracaoSistemaFuncao,
									integracaoVO.getIdIntegracaoFuncao());

					verificarIndicadorAmbos(integracaoVO, listaIntFuncTabela);

					model.addAttribute(LISTA_TABELA, listaIntFuncTabela);

				}
			} else {
				mensagemErroParametrizado(model, request, new GGASException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
						ControladorIntegracao.INTEGRACAO_SISTEMA));
			}
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaAcompanharIntegracao(integracaoVO, result, model);
	}

	/**
	 * Validar Indicador Funcao
	 * 
	 * @param integracaoVO					-	{@link IntegracaoVO}
	 * @param listaIntegracaoSistemaFuncao	- 	{@link Collection}
	 * @return listaIntegracaoSistemaFuncao	-	{@link Collection}
	 * @throws NegocioException				-	{@link NegocioException}
	 */
	private Collection<IntegracaoSistemaFuncao> validarIndicadorFuncao(IntegracaoVO integracaoVO,
			Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao) throws NegocioException {

		if (integracaoVO.getIdFuncao() != null && integracaoVO.getIdFuncao() != -1L) {
			listaIntegracaoSistemaFuncao = new ArrayList<IntegracaoSistemaFuncao>();
			listaIntegracaoSistemaFuncao
					.add(controladorIntegracao.obterIntegracaoSistemaFuncao(integracaoVO.getIdFuncao()));
		}

		return listaIntegracaoSistemaFuncao;
	}

	/**
	 * Verificar Indicador
	 * 
	 * @param integracaoVO			-	{@link IntegracaoVO}
	 * @param listaIntFuncTabela	- 	{@link Collection}
	 * @throws GGASException		-	{@link GGASException}
	 */
	private void verificarIndicadorAmbos(IntegracaoVO integracaoVO,
			Collection<IntegracaoFuncaoTabela> listaIntFuncTabela) throws GGASException {

		if (integracaoVO.isIndicadorCondominioAmbos()) {

			Collection<IntegracaoFuncaoTabela> listaIntFuncTabelaErro = new ArrayList<IntegracaoFuncaoTabela>();

			for (IntegracaoFuncaoTabela intFunTabela : listaIntFuncTabela) {
				if (controladorIntegracao.obterIntegracaoSistemaFuncaoSituacaoErro(intFunTabela)) {
					listaIntFuncTabelaErro.add(intFunTabela);
				}
			}

			listaIntFuncTabela.clear();
			listaIntFuncTabela.addAll(listaIntFuncTabelaErro);
		}
	}

	/**
	 * Filtrar tabela.
	 * 
	 * @param integracaoVO								-	{@link IntegracaoVO}
	 * @param result									-	{@link BindingResult}
	 * @param request									-	{@link HttpServletRequest}
	 * @param model										-	{@link Model}
	 * @return exibirDetalhamentoAcompanharIntegracao	-	{@link String}
	 * @throws Exception								-	{@link Exception}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("filtrarTabela")
	public String filtrarTabela(IntegracaoVO integracaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws Exception {

		String view = "exibirDetalhamentoAcompanharIntegracao";

		// Cria a sessão
		HttpSession sessao = request.getSession(false);
		sessao.setAttribute(REGISTRO_ALTERADOS, new ArrayList<Long>());

		List<AcompanhamentoIntegracaoVO> listaFiltros = (List<AcompanhamentoIntegracaoVO>) sessao
				.getAttribute(LISTA_FILTROS);

		Tabela tabela = controladorAuditoria.obterTabela(integracaoVO.getChavePrimaria());
		Collection<Coluna> listarColunasTabela = controladorIntegracao
				.listarColunasTabela(integracaoVO.getChavePrimaria());
		List<Object> listaConteudoTabelaColuna = null;

		try {
			listaConteudoTabelaColuna = controladorIntegracao.listarConteudoTabelaColuna(tabela, listaFiltros);
		} catch (NegocioException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
			mensagemErroParametrizado(model, request, e);
			
			if (("!java.lang.ClassNotFoundException: br.com.ggas.integracao.notafiscal.IntegracaoNotaFiscal!").equals(model.asMap().get("msg"))) {
				mensagemErro(model, e.getMessage());
			}
			
			return view;
		}

		List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO = new ArrayList<AcompanhamentoIntegracaoVO[]>();
		for (Object conteudoTabela : listaConteudoTabelaColuna) {
			AcompanhamentoIntegracaoVO[] acompanhamentoList = new AcompanhamentoIntegracaoVO[listarColunasTabela
					.size()];
			int j = 0;
			for (int i = 0; i < listarColunasTabela.size(); i++) {
				AcompanhamentoIntegracaoVO acompanhamento = new AcompanhamentoIntegracaoVO();
				Coluna coluna = (Coluna) listarColunasTabela.toArray()[i];
				Object valorMetodoObj = null;
				Class<?> interfacePendente = null;
				String nomeMetodo = "";
				try {
					nomeMetodo = Util.montarNomeGet(coluna.getNomePropriedade());
					interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
					valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
				} catch (NoSuchMethodException e) {
					LOG.error(e.getMessage(), e);
					nomeMetodo = Util.montarNomeIs(coluna.getNomePropriedade());
					interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
					try {
						valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
					} catch (NoSuchMethodException e2) {
						LOG.error(e2.getMessage(), e2);
						nomeMetodo = Util.montarNomeGet(coluna.getNomePropriedade());
						interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
						try {
							valorMetodoObj = conteudoTabela.getClass().getSuperclass().getDeclaredMethod(nomeMetodo)
									.invoke(conteudoTabela);
						} catch (NoSuchMethodException e3) {
							LOG.error(e3.getMessage(), e3);
							try {
								valorMetodoObj = conteudoTabela.getClass().getSuperclass().getSuperclass()
										.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
							} catch (NoSuchMethodException e4) {
								LOG.error(e4.getMessage(), e4);
							}
						}
					}
				}
				acompanhamento.setClasse(listaConteudoTabelaColuna.get(0).getClass());
				acompanhamento.setNomeColuna(coluna.getNome());
				if (valorMetodoObj instanceof EntidadeNegocio) {
					acompanhamento.setValorColuna(((EntidadeNegocio) valorMetodoObj).getChavePrimaria());
				} else {
					acompanhamento.setValorColuna(valorMetodoObj);
				}
				acompanhamento.setNomePropriedade(coluna.getNomePropriedade());
				try {
					acompanhamento
							.setTipoColuna(interfacePendente.getDeclaredMethod(nomeMetodo).getGenericReturnType());
				} catch (NoSuchMethodException e3) {
					LOG.error(e3.getMessage(), e3);
					try {
						acompanhamento.setTipoColuna(conteudoTabela.getClass().getSuperclass()
								.getDeclaredMethod(nomeMetodo).getGenericReturnType());
					} catch (NoSuchMethodException e4) {
						LOG.error(e4.getMessage(), e4);
						try {
							acompanhamento.setTipoColuna(conteudoTabela.getClass().getSuperclass().getSuperclass()
									.getDeclaredMethod(nomeMetodo).getGenericReturnType());
						} catch (NoSuchMethodException e5) {
							LOG.error(e5.getMessage(), e5);
						}
					}
				}
				acompanhamentoList[j] = acompanhamento;
				j = j + 1;
			}
			listaAcompanhamentoIntegracaoVO.add(acompanhamentoList);
		}

		request.setAttribute("listarColunasTabela", listarColunasTabela);
		request.getSession().setAttribute(LISTA_TABELA, listaAcompanhamentoIntegracaoVO);

		if (!Boolean.TRUE.equals(sessao.getAttribute(IS_SALVO))) {
			sessao.setAttribute(REGISTRO_SALVOS, new ArrayList<Long>());
		} else {
			sessao.setAttribute(IS_SALVO, Boolean.FALSE);
		}

		return view;
	}

	/**
	 * Exibir detalhamento acompanhar integracao.
	 * 
	 * @param integracaoVO								-	{@link IntegracaoVO}
	 * @param result									-	{@link BindingResult}
	 * @param request									-	{@link HttpServletRequest}
	 * @param model										-	{@link Model}
	 * @return exibirDetalhamentoAcompanharIntegracao	-	{@link String}
	 * @throws Exception								-	{@link Exception}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirDetalhamentoAcompanharIntegracao")
	public String exibirDetalhamentoAcompanharIntegracao(IntegracaoVO integracaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws Exception {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);
		sessao.setAttribute(REGISTRO_ALTERADOS, new ArrayList<Long>());

		Tabela tabela = controladorAuditoria.obterTabela(integracaoVO.getChavePrimaria());
		Collection<Coluna> listarColunasTabela = controladorIntegracao
				.listarColunasTabela(integracaoVO.getChavePrimaria());
		List<Object> listaConteudoTabelaColuna = controladorIntegracao.listarConteudoTabelaColunaSituacaoErro(tabela,
				null, verificarSituacaoErro(integracaoVO));
		List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO = new ArrayList<AcompanhamentoIntegracaoVO[]>();

		boolean tamanhoZero = false;
		if (listaConteudoTabelaColuna.isEmpty()) {
			listaConteudoTabelaColuna = controladorIntegracao.listarConteudoTabelaColuna(tabela, null);
			tamanhoZero = true;
		}

		iteraListaConteudoTabelaColuna(listarColunasTabela, listaConteudoTabelaColuna, listaAcompanhamentoIntegracaoVO);

		request.setAttribute("listarColunasTabela", listarColunasTabela);

		if (!Boolean.TRUE.equals(sessao.getAttribute(IS_SALVO))) {
			sessao.setAttribute(REGISTRO_SALVOS, new ArrayList<Long>());
		} else {
			sessao.setAttribute(IS_SALVO, Boolean.FALSE);
		}

		List<AcompanhamentoIntegracaoVO> listaFiltros = (List<AcompanhamentoIntegracaoVO>) sessao
				.getAttribute(LISTA_FILTROS);
		if (listaAcompanhamentoIntegracaoVO != null && !listaAcompanhamentoIntegracaoVO.isEmpty()) {
			listaFiltros = new ArrayList<AcompanhamentoIntegracaoVO>();
			for (int i = 0; i < listaAcompanhamentoIntegracaoVO.get(0).length; i++) {
				AcompanhamentoIntegracaoVO filtroAcompanhamentoIntegracaoVO = new AcompanhamentoIntegracaoVO();
				filtroAcompanhamentoIntegracaoVO
						.setNomeColuna(listaAcompanhamentoIntegracaoVO.get(0)[i].getNomeColuna());
				filtroAcompanhamentoIntegracaoVO
						.setNomePropriedade(listaAcompanhamentoIntegracaoVO.get(0)[i].getNomePropriedade());

				if (listaAcompanhamentoIntegracaoVO.get(0)[i].getValorColuna() == null) {
					filtroAcompanhamentoIntegracaoVO
							.setTipoColuna(listaAcompanhamentoIntegracaoVO.get(0)[i].getTipoColuna());
				} else {
					filtroAcompanhamentoIntegracaoVO
							.setTipoColuna(listaAcompanhamentoIntegracaoVO.get(0)[i].getValorColuna().getClass());
				}

				filtroAcompanhamentoIntegracaoVO.setValorColuna("");
				filtroAcompanhamentoIntegracaoVO.setValorColuna2("");
				listaFiltros.add(filtroAcompanhamentoIntegracaoVO);
			}
		}

		if (tamanhoZero) {
			listaAcompanhamentoIntegracaoVO = new ArrayList<AcompanhamentoIntegracaoVO[]>();
		}

		request.getSession().setAttribute(LISTA_TABELA, listaAcompanhamentoIntegracaoVO);

		request.getSession().setAttribute(LISTA_FILTROS, listaFiltros);

		return "exibirDetalhamentoAcompanharIntegracao";
	}

	/**
	 * @param integracaoVO	-	{@link IntegracaoVO}
	 * @return situacao		-	{@link Long}
	 */
	private Long verificarSituacaoErro(IntegracaoVO integracaoVO) {
		Long situacao = 0L;
		if (integracaoVO.isIndicadorCondominioAmbos()) {
			situacao = 1L;
		}
		return situacao;
	}
	
	private void iteraListaConteudoTabelaColuna(Collection<Coluna> listarColunasTabela,
			List<Object> listaConteudoTabelaColuna, List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO)
			throws IllegalAccessException, InvocationTargetException {
		for (Object conteudoTabela : listaConteudoTabelaColuna) {
			AcompanhamentoIntegracaoVO[] acompanhamentoList = new AcompanhamentoIntegracaoVO[listarColunasTabela
					.size()];
			int j = 0;
			for (int i = 0; i < listarColunasTabela.size(); i++) {
				AcompanhamentoIntegracaoVO acompanhamento = new AcompanhamentoIntegracaoVO();
				Coluna coluna = (Coluna) listarColunasTabela.toArray()[i];
				Object valorMetodoObj = null;
				Class<?> interfacePendente = null;
				String nomeMetodo = "";
				try {
					nomeMetodo = Util.montarNomeGet(coluna.getNomePropriedade());
					
					interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
					
					valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
					
				} catch (NoSuchMethodException e) {
					LOG.error(e.getMessage(), e);
					nomeMetodo = Util.montarNomeIs(coluna.getNomePropriedade());
					interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
					try {
						valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
					} catch (NoSuchMethodException e2) {
						LOG.error(e2.getMessage(), e2);
						nomeMetodo = Util.montarNomeGet(coluna.getNomePropriedade());
						interfacePendente = verificarChavePrimaria(conteudoTabela, coluna);
						try {
							valorMetodoObj = conteudoTabela.getClass().getSuperclass().getDeclaredMethod(nomeMetodo)
									.invoke(conteudoTabela);
						} catch (NoSuchMethodException e3) {
							LOG.error(e3.getMessage(), e3);
							try {
								valorMetodoObj = conteudoTabela.getClass().getSuperclass().getSuperclass()
										.getDeclaredMethod(nomeMetodo).invoke(conteudoTabela);
							} catch (NoSuchMethodException e4) {
								LOG.error(e4.getMessage(), e4);
							}
						}
					}
				}
				acompanhamento.setClasse(listaConteudoTabelaColuna.get(0).getClass());
				acompanhamento.setNomeColuna(coluna.getNome());
				if (valorMetodoObj instanceof EntidadeNegocio) {
					acompanhamento.setValorColuna(((EntidadeNegocio) valorMetodoObj).getChavePrimaria());
				} else {
					acompanhamento.setValorColuna(valorMetodoObj);
				}
				acompanhamento.setNomePropriedade(coluna.getNomePropriedade());
				try {
					acompanhamento
							.setTipoColuna(interfacePendente.getDeclaredMethod(nomeMetodo).getGenericReturnType());
				} catch (NoSuchMethodException e3) {
					LOG.error(e3.getMessage(), e3);
					try {
						acompanhamento.setTipoColuna(conteudoTabela.getClass().getSuperclass()
								.getDeclaredMethod(nomeMetodo).getGenericReturnType());
					} catch (NoSuchMethodException e4) {
						LOG.error(e4.getMessage(), e4);
						try {
							acompanhamento.setTipoColuna(conteudoTabela.getClass().getSuperclass().getSuperclass()
									.getDeclaredMethod(nomeMetodo).getGenericReturnType());
						} catch (NoSuchMethodException e5) {
							LOG.error(e5.getMessage(), e5);
						}
					}
				}
				acompanhamentoList[j] = acompanhamento;
				j = j + 1;
			}
			listaAcompanhamentoIntegracaoVO.add(acompanhamentoList);
		}
	}

	private Class<?> verificarChavePrimaria(Object conteudoTabela, Coluna coluna) {
		
		Class<?> interfacePendente = Util.buscarInterface(EntidadeNegocio.class);

		if (!(CHAVE_PRIMARIA.equals(coluna.getNomePropriedade())
				|| VERSAO.equals(coluna.getNomePropriedade())
				|| ULTIMA_ALTERACAO.equals(coluna.getNomePropriedade())
				|| HABILITADO.equals(coluna.getNomePropriedade()))) {
			
			interfacePendente = Util.buscarInterface(conteudoTabela.getClass());
			
		}
		
		return interfacePendente;
		
	}

	/**
	 * Método responsável por salvar as alterações da tela.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param result			-	{@link BindingResult}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 * @return filtrarTabela	-	{@link String}
	 * @throws Exception		-	{@link Exception}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("salvarAlteracao")
	public String salvarAlteracao(IntegracaoVO integracaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws Exception {

		HttpSession sessao = request.getSession(true);
		sessao.getAttribute(LISTA_TABELA);
		List<Long> registrosAlterados = (List<Long>) sessao.getAttribute(REGISTRO_ALTERADOS);
		List<Long> registrosSalvos = new ArrayList<Long>();

		List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO = (List<AcompanhamentoIntegracaoVO[]>) sessao
				.getAttribute(LISTA_TABELA);

		for (AcompanhamentoIntegracaoVO[] acompanhamento : listaAcompanhamentoIntegracaoVO) {
			if (registrosAlterados != null && !registrosAlterados.isEmpty()
					&& registrosAlterados.contains(acompanhamento[0].getValorColuna())) {
				Class<?> type = null;
				Object obj = null;
				type = acompanhamento[0].getClasse();
				obj = type.newInstance();
				for (int i = 0; i < acompanhamento.length; i++) {
					String nomePropriedade = acompanhamento[i].getNomePropriedade();
					if (!(CHAVE_PRIMARIA.equals(nomePropriedade) || VERSAO.equals(nomePropriedade)
							|| ULTIMA_ALTERACAO.equals(nomePropriedade) || HABILITADO.equals(nomePropriedade))) {
						Method m = null;
						try {// IntegracaoBemMedidorImpl/IntegracaoBemVazaoCorretorImpl
							m = type.getMethod(Util.montarNomeSet(nomePropriedade),
									(Class) acompanhamento[i].getTipoColuna());
						} catch (Exception e) {
							LOG.error(e.getMessage(), e);
							try {// IntegracaoBemImpl
								m = type.getSuperclass().getMethod(Util.montarNomeSet(nomePropriedade),
										(Class) acompanhamento[i].getTipoColuna());
							} catch (Exception e1) {
								LOG.error(e1.getMessage(), e1);
								try {// Integracao
									m = type.getSuperclass().getSuperclass().getMethod(
											Util.montarNomeSet(nomePropriedade),
											(Class) acompanhamento[i].getTipoColuna());
								} catch (Exception e2) {
									LOG.error(e2.getMessage(), e2);
								}
							}
						}
						if (m != null) {
							try {
								m.invoke(obj, acompanhamento[i].getValorColuna());
							} catch (IllegalArgumentException e) {
								LOG.error(e.getMessage(), e);
								Class<?> paiImpl = Class.forName(acompanhamento[i].getClasse().getName()
										.substring(0, acompanhamento[i].getClasse().getName().lastIndexOf('.') + 1)
										.concat(((Class<?>) acompanhamento[i].getTipoColuna()).getSimpleName()
												.concat("Impl")));
								Class.forName(acompanhamento[i].getClasse().getName()
										.substring(0, acompanhamento[i].getClasse().getName().lastIndexOf("impl"))
										.concat(((Class<?>) acompanhamento[i].getTipoColuna()).getSimpleName()));
								Object objPai = paiImpl.newInstance();
								try {
									m = paiImpl.getSuperclass().getMethod(Util.montarNomeSet(CHAVE_PRIMARIA),
											long.class);
									m.invoke(objPai, acompanhamento[i].getValorColuna());

									m = type.getMethod(Util.montarNomeSet(nomePropriedade),
											(Class) acompanhamento[i].getTipoColuna());
									m.invoke(obj, objPai);

								} catch (Exception e1) {
									LOG.error(e1.getMessage(), e1);
									continue;
								}
							}
						}
					} else {
						Method m = type.getSuperclass().getMethod(Util.montarNomeSet(nomePropriedade),
								(Class) acompanhamento[i].getTipoColuna());
						m.invoke(obj, acompanhamento[i].getValorColuna());
					}
				}
				try {
					controladorIntegracao.atualizarAcompanharIntegracao((EntidadeNegocio) obj, type);
				} catch (DataException e) {
					LOG.error(e.getStackTrace(), e);
					Logger.getLogger("ERROR").debug(e.getStackTrace());
					mensagemErroParametrizado(model, new NegocioException(
							ControladorIntegracao.ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO, true));
					return exibirDetalhamentoAcompanharIntegracao(integracaoVO, result, request, model);
				} catch (Exception e) {
					LOG.error(e.getStackTrace(), e);
					Logger.getLogger("ERROR").debug(e.getStackTrace());
					mensagemErroParametrizado(model, new NegocioException(
							ControladorIntegracao.ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO, true));
					return exibirDetalhamentoAcompanharIntegracao(integracaoVO, result, request, model);
				}

				if (!registrosSalvos.contains(acompanhamento[0].getValorColuna())) {
					registrosSalvos.add((Long) acompanhamento[0].getValorColuna());
				}
			}
		}
		sessao.setAttribute(REGISTRO_SALVOS, registrosSalvos);
		sessao.setAttribute(IS_SALVO, Boolean.TRUE);
		return filtrarTabela(integracaoVO, result, request, model);
	}

	/**
	 * Carregar combos.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param model				-	{@link Model}
	 * @throws NegocioException	-	{@link NegocioException}
	 */
	private void carregarCombos(IntegracaoVO integracaoVO, Model model) throws NegocioException {

		model.addAttribute("listarIntegracaoSistema", controladorIntegracao.listarIntegracaoSistema());

		if (integracaoVO.getIdIntegracaoSistema() != null && integracaoVO.getIdIntegracaoSistema() != -1L) {
			model.addAttribute("listarIntegracaoSistemaFuncao",
					controladorIntegracao.listarFuncaoPorSistema(integracaoVO.getIdIntegracaoSistema()));
		}
	}

	/**
	 * Exportar tabela.
	 * 
	 * @param request		-	{@link HttpServletResponse}
	 * @param response		-	{@link HttpServletResponse}
	 * @return null
	 * @throws Exception	-	{@link Exception}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exportarTabela")
	public String exportarTabela(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String separador = ";";
		List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO = (List<AcompanhamentoIntegracaoVO[]>) request
				.getSession().getAttribute(LISTA_TABELA);

		if (listaAcompanhamentoIntegracaoVO.isEmpty()) {

			ResourceBundle mensagens = ResourceBundle.getBundle("mensagens");
			String msg = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_EXPORTAR_DADOS,
					MensagemUtil.obterMensagem(mensagens, ControladorIntegracao.INTEGRACAO_SISTEMA));
			throw new GGASException(msg);
		}

		byte[] arquivo = controladorIntegracao.exportarTabelaIntegracaoParaCSV(listaAcompanhamentoIntegracaoVO,
				separador);

		if (arquivo != null) {

			this.exibirDownload("exportar_tabela", arquivo, response);
		}

		return null;
	}

	/**
	 * Exibir download.
	 * 
	 * @param nomeConsulta		-	{@link String}
	 * @param arquivo			-	{@link Byte}
	 * @param response			-	{@link HttpServletResponse}
	 * @throws NegocioException	-	{@link NegocioException}
	 */
	private void exibirDownload(String nomeConsulta, byte[] arquivo, HttpServletResponse response)
			throws NegocioException {

		response.setContentType("application/text");
		response.setContentLength(arquivo.length);
		response.addHeader("Content-Disposition", "attachment; filename="
				+ Util.removerCaracteresEspeciais(nomeConsulta.replace(" ", "_").toUpperCase()) + ".CSV");

		try {
			response.getOutputStream().write(arquivo);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e.getMessage());
		}
	}
}

