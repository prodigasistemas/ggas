/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.integracao.geral;

/**
 * Classe responsável pela representação de valores referentes as funcionalidades do módulo Integração.
 * 
 * @author bruno silva
 * 
 * @see - ManterIntegracaoAction, AcompanharIntegracaoAction, SistemaIntegranteAction
 */
public class IntegracaoVO {
	
	private String sigla;
	private String nome;
	private String operacao;
	private String acao;
	private String descricao;
	private String destinatariosEmail;
	private String mapeamentoExclusao;
	private String sistemaIntegrante;
	private String exibirTabela;

	private String[] valor;
	private String[] codigo;
	private String[] descricaoParametro;
	private String[] codigoEntidadeSistemaGgas;
	private String[] codigoEntidadeSistemaIntegrante;
	
	private Boolean habilitado = true;
	private boolean indicadorCondominioAmbos = true;
	private Integer versao;

	private Long indicadorEnviaEmailErro = 1L;
	private Long codigoEntidadeSistemaIntegranteRemocao;
	private Long codigoEntidadeSistemaGgasRemocao;
	private Long chavePrimariaItemMapeamento;
	private Long chavePrimariaItemMapeamentoAnterior;
	private Long integracaoSistema;
	private Long integracaoFuncao;
	private Long idFuncao;
	private Long integracaoSistemaFuncao;
	private Long idIntegracaoSistema;
	private Long idIntegracaoFuncao;
	private Long integracaoFuncaoTabela;
	private Long chavePrimaria;
	private Long chavePrimariaTabela;
	private Long chavePrimariaMapeamento;

	private Long[] chavesPrimarias;
	private Long[] chavePrimariaParametro;
	private Long[] chaveParametro;
	private Long[] chaveMapeamento;
	private Long[] chaveParametroAlteracao;
	
	
	/**
	 * @return sigla - {@link String}
	 */
	public String getSigla() {
		return sigla;
	}
	/**
	 * @param sigla - {@link String}
	 */
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	/**
	 * @return nome	- {@link String}
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome - {@link String}
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return operacao - {@link String}
	 */
	public String getOperacao() {
		return operacao;
	}
	/**
	 * @param operacao - {@link String}
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	/**
	 * @return acao - {@link String}
	 */
	public String getAcao() {
		return acao;
	}
	/**
	 * @param acao - {@link String}
	 */
	public void setAcao(String acao) {
		this.acao = acao;
	}
	/**
	 * @return descricao	- {@link String}
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao - {@link String}
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return destinatariosEmail	- {@link String}
	 */
	public String getDestinatariosEmail() {
		return destinatariosEmail;
	}
	/**
	 * @param destinatariosEmail - {@link String}
	 */
	public void setDestinatariosEmail(String destinatariosEmail) {
		this.destinatariosEmail = destinatariosEmail;
	}
	/**
	 * @return mapeamentoExclusao	- {@link String}
	 */
	public String getMapeamentoExclusao() {
		return mapeamentoExclusao;
	}
	/**
	 * @param mapeamentoExclusao - {@link String}
	 */
	public void setMapeamentoExclusao(String mapeamentoExclusao) {
		this.mapeamentoExclusao = mapeamentoExclusao;
	}
	/**
	 * @return sistemaIntegrante	- {@link String}
	 */
	public String getSistemaIntegrante() {
		return sistemaIntegrante;
	}
	/**
	 * @param sistemaIntegrante - {@link String}
	 */
	public void setSistemaIntegrante(String sistemaIntegrante) {
		this.sistemaIntegrante = sistemaIntegrante;
	}
	/**
	 * @return exibirTabela	- {@link String}
	 */
	public String getExibirTabela() {
		return exibirTabela;
	}
	/**
	 * @param exibirTabela - {@link String}
	 */
	public void setExibirTabela(String exibirTabela) {
		this.exibirTabela = exibirTabela;
	}
	
	
	/**
	 * @return valor - {@link String}
	 */
	public String[] getValor() {
		return validarClonarAtributoTipoStringArray(valor);
	}
	/**
	 * @param valor - {@link String}
	 */
	public void setValor(String[] valor) {
		this.valor = validarClonarAtributoTipoStringArray(valor);
	}
	/**
	 * @return codigo - {@link String}
	 */
	public String[] getCodigo() {
		return validarClonarAtributoTipoStringArray(codigo);
	}
	/**
	 * @param codigo - {@link String}
	 */
	public void setCodigo(String[] codigo) {
		this.codigo = validarClonarAtributoTipoStringArray(codigo);
	}
	/**
	 * @return descricaoParametro - {@link String}
	 */
	public String[] getDescricaoParametro() {
		return validarClonarAtributoTipoStringArray(descricaoParametro);
	}
	/**
	 * @param descricaoParametro - {@link String}
	 */
	public void setDescricaoParametro(String[] descricaoParametro) {
		this.descricaoParametro = validarClonarAtributoTipoStringArray(descricaoParametro);
	}
	/**
	 * @return codigoEntidadeSistemaGgas - {@link String}
	 */
	public String[] getCodigoEntidadeSistemaGgas() {
		return validarClonarAtributoTipoStringArray(codigoEntidadeSistemaGgas);
	}
	/**
	 * @param codigoEntidadeSistemaGgas - {@link String}
	 */
	public void setCodigoEntidadeSistemaGgas(String[] codigoEntidadeSistemaGgas) {
		this.codigoEntidadeSistemaGgas = (codigoEntidadeSistemaGgas);
	}
	/**
	 * @return codigoEntidadeSistemaIntegrante - {@link String}
	 */
	public String[] getCodigoEntidadeSistemaIntegrante() {
		return validarClonarAtributoTipoStringArray(codigoEntidadeSistemaIntegrante);
	}
	/**
	 * @param codigoEntidadeSistemaIntegrante - {@link String}
	 */
	public void setCodigoEntidadeSistemaIntegrante(String[] codigoEntidadeSistemaIntegrante) {
		this.codigoEntidadeSistemaIntegrante = validarClonarAtributoTipoStringArray(codigoEntidadeSistemaIntegrante);
	}
	
	
	/**
	 * @return habilitado - {@link Boolean}
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}
	/**
	 * @param habilitado - {@link Boolean}
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * @return indicadorCondominio - {@link boolean}
	 */
	public boolean isIndicadorCondominioAmbos() {
		return indicadorCondominioAmbos;
	}
	/**
	 * @param indicadorCondominioAmbos - {@link boolean}
	 */
	public void setIndicadorCondominioAmbos(boolean indicadorCondominioAmbos) {
		this.indicadorCondominioAmbos = indicadorCondominioAmbos;
	}
	/**
	 * @return versao - {@link Integer}
	 */
	public Integer getVersao() {
		return versao;
	}
	/**
	 * @param versao - {@link Integer}
	 */
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	
	
	
	/**
	 * @return indicadorEnviaEmailErro - {@link Long}
	 */
	public Long getIndicadorEnviaEmailErro() {
		return indicadorEnviaEmailErro;
	}
	/**
	 * @param indicadorEnviaEmailErro - {@link Long}
	 */
	public void setIndicadorEnviaEmailErro(Long indicadorEnviaEmailErro) {
		this.indicadorEnviaEmailErro = indicadorEnviaEmailErro;
	}
	/**
	 * @return codigoEntidadeSistemaIntegranteRemocao - {@link Long}
	 */
	public Long getCodigoEntidadeSistemaIntegranteRemocao() {
		return codigoEntidadeSistemaIntegranteRemocao;
	}
	/**
	 * @param codigoEntidadeSistemaIntegranteRemocao - {@link Long}
	 */
	public void setCodigoEntidadeSistemaIntegranteRemocao(Long codigoEntidadeSistemaIntegranteRemocao) {
		this.codigoEntidadeSistemaIntegranteRemocao = codigoEntidadeSistemaIntegranteRemocao;
	}
	/**
	 * @return codigoEntidadeSistemaGgasRemocao - {@link Long}
	 */
	public Long getCodigoEntidadeSistemaGgasRemocao() {
		return codigoEntidadeSistemaGgasRemocao;
	}
	/**
	 * @param codigoEntidadeSistemaGgasRemocao - {@link Long}
	 */
	public void setCodigoEntidadeSistemaGgasRemocao(Long codigoEntidadeSistemaGgasRemocao) {
		this.codigoEntidadeSistemaGgasRemocao = codigoEntidadeSistemaGgasRemocao;
	}
	/**
	 * @return chavePrimariaItemMapeamento - {@link Long}
	 */
	public Long getChavePrimariaItemMapeamento() {
		return chavePrimariaItemMapeamento;
	}
	/**
	 * @param chavePrimariaItemMapeamento - {@link Long}
	 */
	public void setChavePrimariaItemMapeamento(Long chavePrimariaItemMapeamento) {
		this.chavePrimariaItemMapeamento = chavePrimariaItemMapeamento;
	}
	/**
	 * @return chavePrimariaItemMapeamentoAnterior - {@link Long}
	 */
	public Long getChavePrimariaItemMapeamentoAnterior() {
		return chavePrimariaItemMapeamentoAnterior;
	}
	/**
	 * @param chavePrimariaItemMapeamentoAnterior - {@link Long}
	 */
	public void setChavePrimariaItemMapeamentoAnterior(Long chavePrimariaItemMapeamentoAnterior) {
		this.chavePrimariaItemMapeamentoAnterior = chavePrimariaItemMapeamentoAnterior;
	}
	/**
	 * @return integracaoSistema - {@link Long}
	 */
	public Long getIntegracaoSistema() {
		return integracaoSistema;
	}
	/**
	 * @param integracaoSistema - {@link Long}
	 */
	public void setIntegracaoSistema(Long integracaoSistema) {
		this.integracaoSistema = integracaoSistema;
	}
	/**
	 * @return integracaoFuncao - {@link Long}
	 */
	public Long getIntegracaoFuncao() {
		return integracaoFuncao;
	}
	/**
	 * @param integracaoFuncao - {@link Long}
	 */
	public void setIntegracaoFuncao(Long integracaoFuncao) {
		this.integracaoFuncao = integracaoFuncao;
	}
	/**
	 * @return idFuncao - {@link Long}
	 */
	public Long getIdFuncao() {
		return idFuncao;
	}
	/**
	 * @param idFuncao - {@link Long}
	 */
	public void setIdFuncao(Long idFuncao) {
		this.idFuncao = idFuncao;
	}
	/**
	 * @return integracaoSistemaFuncao - {@link Long}
	 */
	public Long getIntegracaoSistemaFuncao() {
		return integracaoSistemaFuncao;
	}
	/**
	 * @param integracaoSistemaFuncao - {@link Long}
	 */
	public void setIntegracaoSistemaFuncao(Long integracaoSistemaFuncao) {
		this.integracaoSistemaFuncao = integracaoSistemaFuncao;
	}
	/**
	 * @return idIntegracaoSistema - {@link Long}
	 */
	public Long getIdIntegracaoSistema() {
		return idIntegracaoSistema;
	}
	/**
	 * @param idIntegracaoSistema - {@link Long}
	 */
	public void setIdIntegracaoSistema(Long idIntegracaoSistema) {
		this.idIntegracaoSistema = idIntegracaoSistema;
	}
	/**
	 * @return idIntegracaoFuncao - {@link Long}
	 */
	public Long getIdIntegracaoFuncao() {
		return idIntegracaoFuncao;
	}
	/**
	 * @param idIntegracaoFuncao - {@link Long}
	 */
	public void setIdIntegracaoFuncao(Long idIntegracaoFuncao) {
		this.idIntegracaoFuncao = idIntegracaoFuncao;
	}
	/**
	 * @return integracaoFuncaoTabela - {@link Long}
	 */
	public Long getIntegracaoFuncaoTabela() {
		return integracaoFuncaoTabela;
	}
	/**
	 * @param integracaoFuncaoTabela - {@link Long}
	 */
	public void setIntegracaoFuncaoTabela(Long integracaoFuncaoTabela) {
		this.integracaoFuncaoTabela = integracaoFuncaoTabela;
	}
	/**
	 * @return chavePrimaria - {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria - {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	/**
	 * @return chavePrimariaTabela - {@link Long}
	 */
	public Long getChavePrimariaTabela() {
		return chavePrimariaTabela;
	}
	/**
	 * @param chavePrimariaTabela - {@link Long}
	 */
	public void setChavePrimariaTabela(Long chavePrimariaTabela) {
		this.chavePrimariaTabela = chavePrimariaTabela;
	}
	/**
	 * @return chavePrimariaMapeamento - {@link Long}
	 */
	public Long getChavePrimariaMapeamento() {
		return chavePrimariaMapeamento;
	}
	/**
	 * @param chavePrimariaMapeamento - {@link Long}
	 */
	public void setChavePrimariaMapeamento(Long chavePrimariaMapeamento) {
		this.chavePrimariaMapeamento = chavePrimariaMapeamento;
	}
	
	
	
	/**
	 * @return chavesPrimarias - {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		return validarClonarAtributoTipoLongArray(chavesPrimarias);
	}
	/**
	 * @param chavesPrimarias - {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		this.chavesPrimarias = validarClonarAtributoTipoLongArray(chavesPrimarias);
	}
	/**
	 * @return chavePrimariaParametro - {@link Long}
	 */
	public Long[] getChavePrimariaParametro() {
		return validarClonarAtributoTipoLongArray(chavePrimariaParametro);
	}
	/**
	 * @param chavePrimariaParametro - {@link Long}
	 */
	public void setChavePrimariaParametro(Long[] chavePrimariaParametro) {
		this.chavePrimariaParametro = validarClonarAtributoTipoLongArray(chavePrimariaParametro);
	}
	/**
	 * @return chaveParametro - {@link Long}
	 */
	public Long[] getChaveParametro() {
		return validarClonarAtributoTipoLongArray(chaveParametro);
	}
	/**
	 * @param chaveParametro - {@link Long}
	 */
	public void setChaveParametro(Long[] chaveParametro) {
		this.chaveParametro = validarClonarAtributoTipoLongArray(chaveParametro);
	}
	/**
	 * @return chaveMapeamento - {@link Long}
	 */
	public Long[] getChaveMapeamento() {
		return validarClonarAtributoTipoLongArray(chaveMapeamento);
	}
	/**
	 * @param chaveMapeamento - {@link Long}
	 */
	public void setChaveMapeamento(Long[] chaveMapeamento) {
		this.chaveMapeamento = validarClonarAtributoTipoLongArray(chaveMapeamento);
	}
	/**
	 * @return chaveParametroAlteracao - {@link Long}
	 */
	public Long[] getChaveParametroAlteracao() {
		return validarClonarAtributoTipoLongArray(chaveParametroAlteracao);
	}
	/**
	 * @param chaveParametroAlteracao - {@link Long}
	 */
	public void setChaveParametroAlteracao(Long[] chaveParametroAlteracao) {
		this.chaveParametroAlteracao = validarClonarAtributoTipoLongArray(chaveParametroAlteracao);
	}
	
	
	
	/**
	 * Método responsável em verificar se uma variável é nula, retornando uma cópia da mesma.
	 * 
	 * @param tmp - {@link Long}
	 * @return tmp variável temporária. - {@link Long}
	 */
	private Long[] validarClonarAtributoTipoLongArray(Long[] tmp) {
		if (tmp != null) {
			return tmp.clone();
		} else {
			return new Long[0];
		}
	}
	
	
	/**
	 * Método responsável em verificar se uma variável é nula, retornando uma cópia da mesma.
	 * 
	 * @param tmp - {@link Long}
	 * @return tmp variável temporária. - {@link Long}
	 */
	private String[] validarClonarAtributoTipoStringArray(String[] tmp) {
		if (tmp != null) {
			return tmp.clone();
		} else {
			return new String[0];
		}
	}
	
}
