/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.integracao.supervisorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.integracao.supervisorio.horaria.impl.SupervisorioMedicaoHorariaImpl;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Validação Medições do Supervisório.
 */
@Controller
public class ValidaMedicaoSupervisorioAction extends GenericAction {
	
	private static final String PARAMETRO_EXIBE_COLUNAS_SUPERVISORIO = "parametroExibeColunasSupervisorio";

	private static final String PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS = "parametroTipoIntegracaoSupervisorios";

	private static final String EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO = "exibirDetalhamentoMedicaoSupervisorioDiario";

	private static final String EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_HORARIO = "exibirDetalhamentoMedicaoSupervisorioHorario";

	private static final Long VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE = 242L;

	private static final String VALIDA_MEDICAO_SUPERVISORIO_VO = "validaMedicaoSupervisorioVO";

	private static final String ULTIMO_USUARIO_ALTERACAO = "ultimoUsuarioAlteracao";

	private static final String LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO = "listaSupervisorioMedicaoDiariaVO";

	private static final String REGISTROS_INDICADOR_PROCESSADO = "registrosIndicadorProcessado";

	private static final String REGISTROS_REMOCAO_LOGICA = "registrosRemocaoLogica";

	private static final String REGISTROS_SOMENTE_LEITURA = "registrosSomenteLeitura";

	private static final String CLIENTE_PRINCIPAL = "clientePrincipal";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String SITUACAO_CONSUMO = "situacaoConsumo";

	private static final String SEGMENTO = "segmento";

	private static final String IMOVEL = "imovel";

	private static final String REGISTROS_INDICADOR_PROCESSADO_HORARIA = "registrosIndicadorProcessadoHoraria";

	private static final String SUPERVISORIO_MEDICAO_ANORMALIDADE = "supervisorioMedicaoAnormalidade";

	private static final String DESC = " desc";

	private static final String ROTA_GRUPO_FATURAMENTO_TIPO_LEITURA = "rota.grupoFaturamento.tipoLeitura";

	private static final String REGISTROS_SALVOS = "registrosSalvos";

	private static final String REGISTROS_ALTERADOS = "registrosAlterados";

	private static final String REGISTROS_REMOCAO_LOGICA_HORARIA = "registrosRemocaoLogicaHoraria";

	private static final String SUPERVISORIO_MEDICAO_HORARIA = "supervisorioMedicaoHoraria";

	private static final String LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO = "listaSupervisorioMedicaoHorariaVO";

	private static final String USUARIO_FUNCIONARIO = "usuario.funcionario";

	private static final String USUARIO = "usuario";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String MEDIDOR_INDEPENDENTE = "medidorIndependente";

	private static final String FILTRO = "filtro";

	private static final String LISTA_SUPERVISORIO_MEDICAO_DIARIA = "listaSupervisorioMedicaoDiaria";

	private static final String LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI = "listaSupervisorioMedicaoDiariaMI";

	private static final String IS_PESQUISA = "isPesquisa";

	private static final Logger LOG = Logger.getLogger(ValidaMedicaoSupervisorioAction.class);

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVE_PRIMARIA_DIARIA = "chavePrimariaDiaria";

	private static final String EXIGE_COMENTARIO = "exigeComentario";

	private Long contadorAuxiliar = Long.valueOf(-1);
	
	// Cliente

	private static final String ID_CLIENTE = "idCliente";

	// Imovel

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String MATRICULA_IMOVEL = "matriculaImovel";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";

	private static final String NOME_IMOVEL = "nomeImovel";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	// Geral

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String IDS_ROTA = "idsRota";

	private static final String OCORRENCIA = "ocorrencia";

	private static final String IDS_OCORRENCIA_MEDICAO = "idsOcorrenciaMedicao";

	private static final String ANALISADA = "analisada";
	
	private static final String INDICADOR_CONSOLIDADA = "indicadorConsolidada";

	private static final String TRANSFERENCIA = "transferencia";

	private static final String ATIVO = "ativo";

	private static final String HABILITADO = "habilitado";

	private static final String ANO_MES_REFERENCIA_LIDO = "anoMesReferenciaLido";

	private static final String ID_CICLO = "idCiclo";

	private static final String ENDERECO_REMOTO_LIDO = "enderecoRemotoLido";

	private static final String DATA_LEITURA_INICIAL = "dataLeituraInicial";

	private static final String DATA_REALIZACAO_LEITURA = "dataRealizacaoLeitura";

	private static final String DATA_INICIAL_REALIZACAO_LEITURA = "dataInicialRealizacaoLeitura";

	private static final String DATA_FINAL_REALIZACAO_LEITURA = "dataFinalRealizacaoLeitura";

	private static final String DATA_LEITURA_FINAL = "dataLeituraFinal";

	private static final String DATA_INCLUSAO_INICIAL = "dataInclusaoInicial";

	private static final String DATA_INCLUSAO_FINAL = "dataInclusaoFinal";

	private static final String SEQUENCIA_COMENTARIO = "sequenciaComentario";

	private static final String STATUS = "status";

	private static final String CODIGO_PONTO_CONSUMO_SUPERVISORIO = "codigoPontoConsumoSupervisorio";

	private static final String CODIGO_MEDIDOR_SUPERVISORIO = "codigoMedidorSupervisorio";

	private static final String SUPERVISORIO_MEDICAO_DIARIA = "supervisorioMedicaoDiaria";

	private static final String COMENTARIO = "comentario";

	private static final String ULTIMA_ALTERACAO = "ultimaAlteracao";

	private static final String ANO_MES_REFERENCIA_PARAMETRO = "anoMesReferenciaParametro";

	private static final String ENDERECO_REMOTO_PARAMETRO = "enderecoRemotoParametro";

	private static final int ESCALA_SUPERVISORIO = 8;

	private static final String STATUS_AUTORIZADO = "statusAutorizado";

	private static final String INDICADOR_TIPO_MEDICAO = "indicadorTipoMedicao";

	private static final String ERRO_MEDICAO_DIARIA_DUPLICADA = "ERRO_MEDICAO_DIARIA_DUPLICADA";

	private static final String ERRO_MEDICAO_HORARIA_DUPLICADA = "ERRO_MEDICAO_HORARIA_DUPLICADA";

	private static final String ERRO_MEDICAO_HORARIA_FORA_CONTRATO = "ERRO_MEDICAO_HORARIA_FORA_CONTRATO";

	private static final String ERRO_MEDICAO_DIARIA_FORA_CONTRATO = "ERRO_MEDICAO_DIARIA_FORA_CONTRATO";

	private static final String DATA_INTEGRACAO_INICIAL = "dataIntegracaoInicial";

	private static final String DATA_INTEGRACAO_FINAL = "dataIntegracaoFinal";

	private static final String INDICADOR_PESQUISA = "indicadorPesquisa";
	
	@Autowired
	private ControladorLocalidade controladorLocalidade;
	
	@Autowired
	private ControladorSetorComercial controladorSetorComercial;
	
	@Autowired
	private ControladorRota controladorRota;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorSupervisorio controladorSupervisorio;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorAlcada controladorAlcada;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	// auxiliar a visualização do supervisório quando for pendente para o usuario na alçada
	private boolean pesquisaPendentes = false;

	private String tipoPendentes;

	private String chavesPrimarias;

	// auxiliar a visualização do supervisório quando for pendente para o usuario na alçada

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa para medição do supervisório.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaMedicaoSupervisorio")
	public String exibirPesquisaMedicaoSupervisorio(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		carregarCampos(validaMedicaoSupervisorioVO, model);

		model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);

		if (!Boolean.TRUE.equals(model.asMap().get(IS_PESQUISA))) {
			request.getSession().setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI, null);
			request.getSession().setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA, null);
		}
		return "exibirPesquisaMedicaoSupervisorio";
	}

	/**
	 * Método responsável por verificar se foi passado algum parâmetro que precisa acessar um ponto consumo.
	 * 
	 * @param filtro {@link Map}
	 * @param model {@link Model}
	 * @return boolean {@link Boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	private Boolean verificarPreenchimentoFiltroPorPontoConsumo(Map<String, Object> filtro, Model model) throws NegocioException {

		Long idCliente = (Long) filtro.get("idCliente");
		String cepImovel = (String) filtro.get("cepImovel");
		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		Long matriculaCondominio = (Long) filtro.get("matriculaCondominio");
		String nomeImovel = (String) filtro.get("nomeImovel");
		String complementoImovel = (String) filtro.get("complementoImovel");
		String numeroImovel = (String) filtro.get("numeroImovel");
		Long idLocalidade = (Long) filtro.get("idLocalidade");
		Long idSetorComercial = (Long) filtro.get("idSetorComercial");
		Long idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
		Long[] idsRota = (Long[]) filtro.get("idsRota");

		if (idCliente != null && idCliente > 0) {
			model.addAttribute("cliente", controladorCliente.obter(idCliente));
		}

		if (((idCliente != null) && (idCliente > 0)) || StringUtils.isNotEmpty(cepImovel)
				|| ((matriculaImovel != null) && (matriculaImovel > 0)) || indicadorCondominio != null
				|| ((matriculaCondominio != null) && (matriculaCondominio > 0)) || StringUtils.isNotEmpty(nomeImovel)
				|| StringUtils.isNotEmpty(complementoImovel) || StringUtils.isNotEmpty(numeroImovel)
				|| (idLocalidade != null && idLocalidade > 0) || (idSetorComercial != null && idSetorComercial > 0)
				|| (idGrupoFaturamento != null && idGrupoFaturamento > 0) || (idsRota != null && idsRota.length > 0)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * Método responsável por realizar a pesquisa
	 * das medições do supervisório.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarMedicaoSupervisorio")
	public String pesquisarMedicaoSupervisorio(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		if (PONTO_CONSUMO.equals(validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
			if (isPesquisaPendentes() && validaMedicaoSupervisorioVO.getStatusPC() == null
					|| (VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE).equals(validaMedicaoSupervisorioVO.getStatusPC())) {
				pesquisarMedicaoSupervisorioPendentes(validaMedicaoSupervisorioVO, bindingResult, request, model);
			}
		} else {
			if (isPesquisaPendentes() && validaMedicaoSupervisorioVO.getStatusMI() == null
					|| (VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE).equals(validaMedicaoSupervisorioVO.getStatusPC())) {
				pesquisarMedicaoSupervisorioPendentes(validaMedicaoSupervisorioVO, bindingResult, request, model);
			}
		}

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {

			this.prepararFiltro(filtro, validaMedicaoSupervisorioVO, model);

			// Cria a sessão
			HttpSession sessao = request.getSession(false);

			Map<String, Object> filtroAux = new HashMap<String, Object>();
			this.prepararFiltro(filtroAux, validaMedicaoSupervisorioVO, model);
			sessao.setAttribute(FILTRO, filtroAux);

			Boolean isConsultaPorPontoConsumo = this.verificarPreenchimentoFiltroPorPontoConsumo(filtro, model);
			Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO = new ArrayList<SupervisorioMedicaoVO>();

			if (filtroAux.get(STATUS) != null) {

				Long status = (Long) filtroAux.get(STATUS);
				filtro.put(STATUS, status);

				Map<Long, Long> chavePrimariaDiaria = new HashMap<Long, Long>();
				for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : controladorAlcada
						.obterSupervisorioMedicaoDiariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
					chavePrimariaDiaria.put(supervisorioMedicaoDiaria.getChavePrimaria(), supervisorioMedicaoDiaria.getChavePrimaria());
				}

				for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : controladorAlcada
						.obterSupervisorioMedicaoHorariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
					chavePrimariaDiaria.put(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria(),
							supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
				}

				filtro.put(CHAVE_PRIMARIA_DIARIA, chavePrimariaDiaria);
				filtro.put(STATUS, null);

				if (!chavePrimariaDiaria.isEmpty()) {
					if (MEDIDOR_INDEPENDENTE.equals((String) validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
						listaSupervisorioMedicaoDiariaVO = controladorSupervisorio.consultarSupervisorioMedicaoDiariaPorMedidor(filtro);
					} else {
						listaSupervisorioMedicaoDiariaVO =
								controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, isConsultaPorPontoConsumo);
					}
				}

				filtro.put(STATUS, status);
				filtro.put(CHAVE_PRIMARIA_DIARIA, null);

			} else {

				if (MEDIDOR_INDEPENDENTE.equals((String) validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
					listaSupervisorioMedicaoDiariaVO = controladorSupervisorio.consultarSupervisorioMedicaoDiariaPorMedidor(filtro);
				} else {
					listaSupervisorioMedicaoDiariaVO =
							controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, isConsultaPorPontoConsumo);
				}

			}

			if (MEDIDOR_INDEPENDENTE.equals((String) validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
				if (!listaSupervisorioMedicaoDiariaVO.isEmpty()) {
					model.addAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI, listaSupervisorioMedicaoDiariaVO);
				} else {
					model.addAttribute("pageSize", 0);
				}
				sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI, listaSupervisorioMedicaoDiariaVO);
				sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA, null);
			} else {
				if (!listaSupervisorioMedicaoDiariaVO.isEmpty()) {
					model.addAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA, listaSupervisorioMedicaoDiariaVO);
				} else {
					model.addAttribute("pageSize", 0);
				}
				sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA, listaSupervisorioMedicaoDiariaVO);
				sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI, null);
			}

			model.addAttribute(IS_PESQUISA, Boolean.TRUE);

			setPesquisaPendentes(false);
			setTipoPendentes(null);
			setChavesPrimarias(null);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Pesquisar medicao supervisorio pendentes.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	public String pesquisarMedicaoSupervisorioPendentes(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (PONTO_CONSUMO.equals(validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
			validaMedicaoSupervisorioVO.setStatusPC(VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE);
		} else {
			validaMedicaoSupervisorioVO.setStatusMI(VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE);
		}
		model.addAttribute("statusPendente", VALOR_CAMPO_RADIO_ALCADA_ID_STATUS_PENDENTE);

		prepararFiltro(filtro, validaMedicaoSupervisorioVO, model);
		super.adicionarFiltroPaginacao(request, filtro);

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		Map<String, Object> filtroAux = new HashMap<String, Object>();
		prepararFiltro(filtroAux, validaMedicaoSupervisorioVO, model);
		sessao.setAttribute(FILTRO, filtroAux);

		String tipo;
		if (request.getParameter("tipoSupervisorio") == null) {
			tipo = getTipoPendentes();
		} else {
			tipo = request.getParameter("tipoSupervisorio");
		}
		String chavesPrimarias;
		if (request.getParameter(CHAVES_PRIMARIAS) == null) {
			chavesPrimarias = getChavesPrimarias();
		} else {
			chavesPrimarias = request.getParameter(CHAVES_PRIMARIAS);
		}

		Boolean isConsultaPorPontoConsumo = this.verificarPreenchimentoFiltroPorPontoConsumo(filtro, model);
		Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO = new ArrayList<SupervisorioMedicaoVO>();

		boolean isPesquisaMedidor = MEDIDOR_INDEPENDENTE.equals((String) validaMedicaoSupervisorioVO.getIndicadorPesquisa());

		if ("diaria".equals(tipo)) {
			filtro.put("chavesPrimariasPendentes", chavesPrimarias);
			if (isPesquisaMedidor) {
				listaSupervisorioMedicaoDiariaVO = controladorSupervisorio.consultarSupervisorioMedicaoDiariaPorMedidor(filtro);
			} else {
				listaSupervisorioMedicaoDiariaVO =
						controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, isConsultaPorPontoConsumo);
			}
		} else {

			Map<Long, Long> chavePrimariaDiaria = new HashMap<Long, Long>();

			for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : controladorAlcada
					.obterSupervisorioMedicaoHorariaPendentesPorUsuario(super.obterUsuario(request), Boolean.FALSE)) {
				chavePrimariaDiaria.put(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria(),
						supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
			}

			filtro.put(CHAVE_PRIMARIA_DIARIA, chavePrimariaDiaria);
			filtro.put(STATUS, null);

			if (!chavePrimariaDiaria.isEmpty()) {
				if (isPesquisaMedidor) {
					listaSupervisorioMedicaoDiariaVO = controladorSupervisorio.consultarSupervisorioMedicaoDiariaPorMedidor(filtro);
				} else {
					listaSupervisorioMedicaoDiariaVO =
							controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, isConsultaPorPontoConsumo);
				}
			}

		}

		if (isPesquisaMedidor) {
			model.addAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI,
					super.criarColecaoPaginada(request, filtro, listaSupervisorioMedicaoDiariaVO));

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI, listaSupervisorioMedicaoDiariaVO);
		} else {
			model.addAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA,
					super.criarColecaoPaginada(request, filtro, listaSupervisorioMedicaoDiariaVO));

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA, listaSupervisorioMedicaoDiariaVO);
		}

		model.addAttribute(IS_PESQUISA, Boolean.TRUE);

		setPesquisaPendentes(true);
		setTipoPendentes(tipo);
		setChavesPrimarias(chavesPrimarias);

		return exibirPesquisaMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Método responsável por executar a ação de
	 * transferir a medição do supervisório diário.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("transferirMedicaoSupervisorio")
	public String transferirMedicaoSupervisorio(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO;
		if (MEDIDOR_INDEPENDENTE.equals(validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {
			listaSupervisorioMedicaoDiariaVO = (List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI);
		} else {
			listaSupervisorioMedicaoDiariaVO = (List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA);
		}

		Collection<SupervisorioMedicaoVO> listaTransferir = new ArrayList<SupervisorioMedicaoVO>();

		for (SupervisorioMedicaoVO medicao : listaSupervisorioMedicaoDiariaVO) {
			for (Long chave : chavesPrimarias) {
				if (chave.equals(medicao.getChavePrimaria())) {
					listaTransferir.add(medicao);
					break;
				}
			}
		}
		
		Map<String, Object> filtro = (Map<String, Object>) sessao.getAttribute(FILTRO);

		this.prepararFiltro(filtro, validaMedicaoSupervisorioVO, model);

		try {
			if (!controladorSupervisorio.transferirSupervisorioMedicoesDiaria(listaTransferir, filtro)) {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_PRONTA_PARA_TRANSFERENCIA, request,
						SupervisorioMedicaoDiaria.SUPERVISORIO_MEDICAO_DIARIA_ROTULO);
			} else {
				mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_REGISTROS_INCOMPLETOS));
			}

		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, request, e.getChaveErro());
		}
		validaMedicaoSupervisorioVO.setDataIntegracaoInicial(null);
		validaMedicaoSupervisorioVO.setDataIntegracaoFinal(null);

		return pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Desfazer supervisorio.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("desfazerSupervisorio")
	public String desfazerSupervisorio(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO = this.obterListaSupervisorioMedicaoVO(chavesPrimarias, request);

		try {
			controladorSupervisorio.desfazerSupervisorio(listaSupervisorioMedicaoVO);
			mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Desfazer consolidar supervisorio.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("desfazerConsolidarSupervisorio")
	public String desfazerConsolidarSupervisorio(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO = this.obterListaSupervisorioMedicaoVO(chavesPrimarias, request);

			controladorSupervisorio.desfazerConsolidarSupervisorio(listaSupervisorioMedicaoVO);

			mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);

		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			try {
				throw new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, true);
			} catch (GGASException ex) {
				mensagemErroParametrizado(model, request, ex);
			}
		}

		return pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Obter lista supervisorio medicao vo.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @return listaSupervisorioRetorno {@link Collection}
	 */
	@SuppressWarnings("unchecked")
	private Collection<SupervisorioMedicaoVO> obterListaSupervisorioMedicaoVO(Long[] chavesPrimarias, HttpServletRequest request) {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO = new ArrayList<SupervisorioMedicaoVO>();

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVOPC =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVOMI =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_MI);

		if (listaSupervisorioMedicaoDiariaVOPC != null && !listaSupervisorioMedicaoDiariaVOPC.isEmpty()) {
			listaSupervisorioMedicaoDiariaVO.addAll(listaSupervisorioMedicaoDiariaVOPC);
		}
		if (listaSupervisorioMedicaoDiariaVOMI != null && !listaSupervisorioMedicaoDiariaVOMI.isEmpty()) {
			listaSupervisorioMedicaoDiariaVO.addAll(listaSupervisorioMedicaoDiariaVOMI);
		}
		Collection<SupervisorioMedicaoVO> listaSupervisorioRetorno = new ArrayList<SupervisorioMedicaoVO>();

		for (SupervisorioMedicaoVO medicao : listaSupervisorioMedicaoDiariaVO) {
			for (Long chave : chavesPrimarias) {
				if (chave.equals(medicao.getChavePrimaria())) {
					listaSupervisorioRetorno.add(medicao);
					break;
				}
			}
		}

		return listaSupervisorioRetorno;
	}

	/**
	 * Exibir detalhamento supervisorio medicao diaria comentario.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioComentario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoSupervisorioMedicaoDiariaComentario")
	public String exibirDetalhamentoSupervisorioMedicaoDiariaComentario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(SUPERVISORIO_MEDICAO_DIARIA, validaMedicaoSupervisorioVO.getChavePrimaria());

		// Monta a ordenação da consulta
		String ordenacao = SEQUENCIA_COMENTARIO + " desc ";

		try {

			Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario = controladorSupervisorio
					.consultarSupervisorioMedicaoComentario(filtro, ordenacao, SUPERVISORIO_MEDICAO_DIARIA, USUARIO, USUARIO_FUNCIONARIO);

			model.addAttribute("listaSupervisorioMedicaoComentario", listaSupervisorioMedicaoComentario);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirDetalhamentoMedicaoSupervisorioComentario";
	}

	/**
	 * Exibir detalhamento supervisorio medicao horaria comentario.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoSupervisorioMedicaoHorariaComentario")
	public String exibirDetalhamentoSupervisorioMedicaoHorariaComentario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		String tela = "exibirDetalhamentoMedicaoHorariaSupervisorioComentario";

		try {
			Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario =
					pesquisarSupervisorioMedicaoComentario(validaMedicaoSupervisorioVO.getChavePrimaria(), request);
			model.addAttribute("listaSupervisorioMedicaoHorariaComentario", listaSupervisorioMedicaoComentario);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
		}

		return tela;
	}

	/**
	 * Pesquisar supervisorio medicao comentario.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @return collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	@SuppressWarnings("unchecked")
	private Collection<SupervisorioMedicaoComentario> pesquisarSupervisorioMedicaoComentario(Long chavePrimaria, HttpServletRequest request)
			throws NegocioException {

		HttpSession sessao = request.getSession(false);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);
		SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
				controladorSupervisorio.obterSupervisorioMedicaoHoraria(listaSupervisorioMedicaoHorariaVO.get(0).getChavePrimaria());

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(SUPERVISORIO_MEDICAO_HORARIA, chavePrimaria);

		if (supervisorioMedicaoHoraria != null && supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria() != null
				&& supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria() > 0) {
			filtro.put(SUPERVISORIO_MEDICAO_DIARIA, supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
		}

		// Monta a ordenação da consulta
		String ordenacao = SEQUENCIA_COMENTARIO + " desc ";

		return controladorSupervisorio.consultarSupervisorioMedicaoComentario(filtro, ordenacao, SUPERVISORIO_MEDICAO_HORARIA,
				SUPERVISORIO_MEDICAO_DIARIA, USUARIO, USUARIO_FUNCIONARIO);
	}

	/**
	 * Pesquisar supervisorio medicao comentario.
	 * 
	 * @param idSupervisorioMedicaoHoraria {@link Long}
	 * @param idSupervisorioMedicaoDiaria {@link Long}
	 * @return collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	private Collection<SupervisorioMedicaoComentario> pesquisarSupervisorioMedicaoComentario(Long idSupervisorioMedicaoHoraria,
			Long idSupervisorioMedicaoDiaria) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (idSupervisorioMedicaoHoraria != null && idSupervisorioMedicaoHoraria > 0) {
			filtro.put(SUPERVISORIO_MEDICAO_HORARIA, idSupervisorioMedicaoHoraria);
		}
		if (idSupervisorioMedicaoDiaria != null && idSupervisorioMedicaoDiaria > 0) {
			filtro.put(SUPERVISORIO_MEDICAO_DIARIA, idSupervisorioMedicaoDiaria);
		}

		// Monta a ordenação da consulta
		String ordenacao = SEQUENCIA_COMENTARIO + " asc ";

		return controladorSupervisorio.consultarSupervisorioMedicaoComentario(filtro, ordenacao, SUPERVISORIO_MEDICAO_HORARIA,
				SUPERVISORIO_MEDICAO_DIARIA, USUARIO, USUARIO_FUNCIONARIO);
	}

	/**
	 * Método responsável por remover uma medição horária da medição diária.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param supervisorioMedicaoDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioHorario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerRegistroSupervisorioHorario")
	public String removerRegistroSupervisorioHorario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long supervisorioMedicaoDiaria, HttpServletRequest request,
			Model model) throws GGASException {

		Long chavePrimaria = validaMedicaoSupervisorioVO.getChavePrimaria();
		model.addAttribute(CHAVE_PRIMARIA_DIARIA, supervisorioMedicaoDiaria);

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		model.addAttribute(EXIGE_COMENTARIO, validaMedicaoSupervisorioVO.getExigeComentario());

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);

		SupervisorioMedicaoVO supervisorioMedicaoHorariaVOAux = null;

		for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {

			if (supervisorioMedicaoHorariaVO.getChavePrimaria() != null
					&& supervisorioMedicaoHorariaVO.getChavePrimaria().equals(validaMedicaoSupervisorioVO.getChavePrimaria())) {

				supervisorioMedicaoHorariaVOAux = supervisorioMedicaoHorariaVO;
			}
		}

		if (supervisorioMedicaoHorariaVOAux != null && supervisorioMedicaoHorariaVOAux.getChavePrimaria() < 0) {

			listaSupervisorioMedicaoHorariaVO.remove(supervisorioMedicaoHorariaVOAux);
			validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() - 1L);
		
		} else {

			List<Long> registrosRemocaoLogica = (List<Long>) sessao.getAttribute(REGISTROS_REMOCAO_LOGICA_HORARIA);
			List<Long> registrosAlterados = (List<Long>) sessao.getAttribute(REGISTROS_ALTERADOS);
			List<Long> registrosSalvos = (List<Long>) sessao.getAttribute(REGISTROS_SALVOS);

			if (registrosSalvos != null && registrosSalvos.contains(chavePrimaria)) {
				registrosSalvos.remove(chavePrimaria);
				sessao.setAttribute(REGISTROS_SALVOS, registrosSalvos);
			}

			if (registrosAlterados != null && registrosAlterados.contains(chavePrimaria)) {
				registrosAlterados.remove(chavePrimaria);
				sessao.setAttribute(REGISTROS_ALTERADOS, registrosAlterados);
			}
			verificaChavePrimariaExistente(chavePrimaria, registrosRemocaoLogica);

			sessao.setAttribute(REGISTROS_REMOCAO_LOGICA_HORARIA, registrosRemocaoLogica);
		}

		sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO, listaSupervisorioMedicaoHorariaVO);

		return EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_HORARIO;
	}

	/**
	 * Método responsável por adicionar uma medição horária na medição diária.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param supervisorioMedicaoDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioHorario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({"unchecked"})
	@RequestMapping("adicionarRegistroSupervisorioHorario")
	public String adicionarRegistroSupervisorioHorario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long supervisorioMedicaoDiaria, HttpServletRequest request,
			Model model) throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVOAux = new ArrayList<SupervisorioMedicaoVO>();

		for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {

			listaSupervisorioMedicaoHorariaVOAux.add(supervisorioMedicaoHorariaVO);

			if (supervisorioMedicaoHorariaVO.getChavePrimaria() != null
					&& supervisorioMedicaoHorariaVO.getChavePrimaria().equals(validaMedicaoSupervisorioVO.getChavePrimaria())) {

				SupervisorioMedicaoVO supervisorioMedicaoHorariaVOAux = new SupervisorioMedicaoVO();
				supervisorioMedicaoHorariaVOAux.setChavePrimaria(contadorAuxiliar);
				supervisorioMedicaoHorariaVOAux
						.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoHorariaVO.getCodigoPontoConsumoSupervisorio());
				supervisorioMedicaoHorariaVOAux.setHabilitado(Boolean.TRUE);
				listaSupervisorioMedicaoHorariaVOAux.add(supervisorioMedicaoHorariaVOAux);

				contadorAuxiliar += -1;
			}
		}

		validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() + 1L);
		model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);
		model.addAttribute(CHAVE_PRIMARIA_DIARIA ,supervisorioMedicaoDiaria);
		sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO, listaSupervisorioMedicaoHorariaVOAux);

		return EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_HORARIO;
	}

	/**
	 * Obter supervisorio medicao vo.
	 * 
	 * @param listaSupervisorioMedicaoVO {@link List}
	 * @param chavePrimaria {@link Long}
	 * @return retorno {@link SupervisorioMedicaoVO}
	 */
	private SupervisorioMedicaoVO obterSupervisorioMedicaoVO(List<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO, Long chavePrimaria) {

		SupervisorioMedicaoVO retorno = null;

		if (listaSupervisorioMedicaoVO != null && !listaSupervisorioMedicaoVO.isEmpty() && chavePrimaria != null) {
			for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoVO) {
				if (chavePrimaria.equals(supervisorioMedicaoVO.getChavePrimaria())) {
					retorno = supervisorioMedicaoVO;
					break;
				}
			}
		}

		return retorno;
	}

	/**
	 * Salvar registro supervisorio horario.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavePrimariaDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioHorario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({"unchecked"})
	@RequestMapping("salvarRegistroSupervisorioHorario")
	public String salvarRegistroSupervisorioHorario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long chavePrimariaDiaria, HttpServletRequest request,
			Model model) throws GGASException {

		try {

			ParametroSistema parametroTabelaSuperMedicaoHoraria =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_HORARIA);

			// Cria a sessão
			HttpSession sessao = request.getSession(false);
			Boolean algumaHorariaAlterada = Boolean.FALSE;

			List<Long> registrosRemocaoLogica = (List<Long>) sessao.getAttribute(REGISTROS_REMOCAO_LOGICA_HORARIA);
			List<Long> registrosAlterados = (List<Long>) sessao.getAttribute(REGISTROS_ALTERADOS);
			List<Long> registrosSalvos = new ArrayList<Long>();

			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
					(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);

			verificaRegistrosRemocaoLogica(request, chavePrimariaDiaria, registrosRemocaoLogica, registrosSalvos,
					listaSupervisorioMedicaoHorariaVO, model);

			for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {

				if (supervisorioMedicaoHorariaVO.getChavePrimaria() == null || supervisorioMedicaoHorariaVO.getChavePrimaria() < 0) {

					Long idSupervisorioMedicaoDiaria = chavePrimariaDiaria;
					String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();

					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
							preencherSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaVO);
					supervisorioMedicaoHoraria.setDataRegistroLeitura(Calendar.getInstance().getTime());
					if (listaSupervisorioMedicaoHorariaVO.get(0).getChavePrimaria() == null) {
						continue;
					}
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAux = controladorSupervisorio
							.obterSupervisorioMedicaoHoraria(listaSupervisorioMedicaoHorariaVO.get(0).getChavePrimaria());
					supervisorioMedicaoHoraria.setSupervisorioMedicaoDiaria(supervisorioMedicaoHorariaAux.getSupervisorioMedicaoDiaria());

					controladorSupervisorio.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
					verificarDataHoraContrato(idSupervisorioMedicaoDiaria, supervisorioMedicaoHorariaVO);
					this.verificarMedicoesHorariasDuplicadas(supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura(),
							codigoPontoConsumoSupervisorio, null);
					verificarMedicoesDuplicadas(listaSupervisorioMedicaoHorariaVO, supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura(),
							Boolean.FALSE);

					supervisorioMedicaoHoraria = controladorSupervisorio.inserirSupervisorioMedicaoHoraria(
							parametroTabelaSuperMedicaoHoraria, supervisorioMedicaoHoraria, getDadosAuditoria(request),

							criarNovoSupervisorioMedicaoComentario(request, supervisorioMedicaoHoraria, null, Integer.valueOf(1), model));

					if (!registrosSalvos.contains(supervisorioMedicaoHoraria.getChavePrimaria())) {
						registrosSalvos.add(supervisorioMedicaoHoraria.getChavePrimaria());
					}
					algumaHorariaAlterada = Boolean.TRUE;
				} else if (registrosAlterados != null && !registrosAlterados.isEmpty()
						&& registrosAlterados.contains(supervisorioMedicaoHorariaVO.getChavePrimaria())) {

					Long idSupervisorioMedicaoDiaria = chavePrimariaDiaria;
					String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();

					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria = controladorSupervisorio
							.obterSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaVO.getChavePrimaria(), STATUS);
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo =
							this.preencherSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaVO);
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario =
							pesquisarSupervisorioMedicaoComentario(supervisorioMedicaoHoraria.getChavePrimaria(),
									supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
					controladorSupervisorio.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
					controladorSupervisorio.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaNovo);
					verificarDataHoraContrato(idSupervisorioMedicaoDiaria, supervisorioMedicaoHorariaVO);
					this.verificarMedicoesHorariasDuplicadas(supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura(),
							codigoPontoConsumoSupervisorio, supervisorioMedicaoHorariaVO.getChavePrimaria());
					verificarMedicoesDuplicadas(listaSupervisorioMedicaoHorariaVO, supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura(),
							Boolean.FALSE);

					supervisorioMedicaoHorariaNovo =
							controladorSupervisorio.atualizarSupervisorioMedicaoHoraria(listaSupervisorioMedicaoComentario,
									getDadosAuditoria(request), supervisorioMedicaoHorariaNovo,
									criarNovoSupervisorioMedicaoComentario(request, supervisorioMedicaoHorariaNovo, null,
											Integer.valueOf(1), model),
									parametroTabelaSuperMedicaoHoraria, supervisorioMedicaoHorariaVO, supervisorioMedicaoHoraria);
					if (!registrosSalvos.contains(supervisorioMedicaoHoraria.getChavePrimaria())) {

						registrosSalvos.add(supervisorioMedicaoHoraria.getChavePrimaria());
						registrosSalvos.add(supervisorioMedicaoHorariaNovo.getChavePrimaria());
					}
					algumaHorariaAlterada = Boolean.TRUE;
				}
			}
			if (algumaHorariaAlterada) {
				for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {
					if (supervisorioMedicaoHorariaVO.getChavePrimaria() != null && supervisorioMedicaoHorariaVO.getChavePrimaria() > 0
							&& supervisorioMedicaoHorariaVO.getHabilitado()
							&& !registrosRemocaoLogica.contains(supervisorioMedicaoHorariaVO.getChavePrimaria())) {

						SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
								controladorSupervisorio.obterSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaVO.getChavePrimaria());
						supervisorioMedicaoHoraria.setIndicadorConsolidada(Boolean.FALSE);
						controladorSupervisorio.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);
					}
				}
			}

			if (listaSupervisorioMedicaoHorariaVO.size() > 1) {

				listaSupervisorioMedicaoHorariaVO =
						this.ordenarSupervisoriMedicaoVOPorDataRealizacaoLeitura(listaSupervisorioMedicaoHorariaVO);
			}

			validaMedicaoSupervisorioVO.setConsultaBanco(Boolean.TRUE);

			// chama o processo de consolidar dados do supervisório
			SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoHoraria =
					Util.primeiroElemento(listaSupervisorioMedicaoHorariaVO);
			SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoHoraria =
					listaSupervisorioMedicaoHorariaVO.get(listaSupervisorioMedicaoHorariaVO.size() - 1);

			Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
			Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
			String codigoPontoConsumoSupervisorio = primeiroRegistroListaSupervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio();

			this.tratarMedicoesHorarias(dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio);

			// insere na lista registrosSalvos todos os registros que foram salvos no banco de acordo com o filtro da tela de pesquisa
			sessao.setAttribute(REGISTROS_SALVOS, registrosSalvos);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoMedicaoSupervisorioHorario(validaMedicaoSupervisorioVO, bindingResult, chavePrimariaDiaria, request,
				model);
	}

	/**
	 * Verifica os Registros Removidos Logicamente antes de Salvar
	 * 
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param chavePrimariaSupervisorioMedicaoDiaria {@link Long}
	 * @param registrosRemocaoLogica {@link Lista}
	 * @param registrosSalvos {@link List}
	 * @param listaSupervisorioMedicaoHorariaVO {@link List}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void verificaRegistrosRemocaoLogica(HttpServletRequest request, Long chavePrimariaSupervisorioMedicaoDiaria,
			List<Long> registrosRemocaoLogica, List<Long> registrosSalvos, List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO,
			Model model) throws GGASException {

		if (registrosRemocaoLogica != null && !registrosRemocaoLogica.isEmpty()) {
			for (Long chavePrimaria : registrosRemocaoLogica) {
				SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
						controladorSupervisorio.obterSupervisorioMedicaoHoraria(chavePrimaria);
				SupervisorioMedicaoVO supervisorioMedicaoVO = obterSupervisorioMedicaoVO(listaSupervisorioMedicaoHorariaVO, chavePrimaria);
				if (supervisorioMedicaoVO != null) {
					supervisorioMedicaoVO.setHabilitado(Boolean.FALSE);
				}
				controladorSupervisorio.removerSupervisorioMedicaoHoraria(
						criarNovoSupervisorioMedicaoComentario(request, supervisorioMedicaoHoraria, null, Integer.valueOf(1), model),
						supervisorioMedicaoHoraria);

				Map<String, Object> filtroPesquisa = new HashMap<String, Object>();
				filtroPesquisa.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaSupervisorioMedicaoDiaria);
				filtroPesquisa.put(HABILITADO, Boolean.TRUE);

				Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria =
						controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtroPesquisa, null, "");

				for (SupervisorioMedicaoHoraria supervisorioMedicaoHorariaLocal : listaSupervisorioMedicaoHoraria) {

					supervisorioMedicaoHorariaLocal.setIndicadorConsolidada(Boolean.FALSE);
					controladorSupervisorio.atualizar(supervisorioMedicaoHorariaLocal, SupervisorioMedicaoHorariaImpl.class);
				}

				if (!registrosSalvos.contains(supervisorioMedicaoHoraria.getChavePrimaria())) {
					registrosSalvos.add(supervisorioMedicaoHoraria.getChavePrimaria());
				}
			}
		}
	}

	/**
	 * Tratar medicoes horarias.
	 * 
	 * @param dataInicialRealizacaoLeitura {@link Date}
	 * @param dataFinalRealizacaoLeitura {@link Date}
	 * @param codigoPontoConsumoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	public void tratarMedicoesHorarias(Date dataInicialRealizacaoLeitura, Date dataFinalRealizacaoLeitura,
			String codigoPontoConsumoSupervisorio) throws GGASException {

		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoHoraria = new LinkedHashMap<String, List<Object[]>>();
		ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO = new ResumoSupervisorioMedicaoVO();
		StringBuilder logProcessamento = new StringBuilder();

		Map<String, Object> filtroPesquisa = new HashMap<String, Object>();
		filtroPesquisa.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
		Collection<PontoConsumo> listaPontoConsumo =
				controladorPontoConsumo.consultarPontosConsumo(filtroPesquisa, ROTA_GRUPO_FATURAMENTO_TIPO_LEITURA);
		PontoConsumo pontoConsumo = null;
		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		mapaAnormalidadeSupervisorioMedicaoHoraria =
				controladorSupervisorio.tratarMedicoesHorarias(pontoConsumo, dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura,
						logProcessamento, mapaAnormalidadeSupervisorioMedicaoHoraria, resumoSupervisorioMedicaoVO, null, null);

		controladorSupervisorio.tratarMedicoesDiarias(pontoConsumo, dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura,
				logProcessamento, mapaAnormalidadeSupervisorioMedicaoHoraria, resumoSupervisorioMedicaoVO, null);

	}

	/**
	 * Tratar medicoes diarias.
	 * 
	 * @param dataInicialRealizacaoLeitura {@link Date}
	 * @param dataFinalRealizacaoLeitura {@link Date}
	 * @param codigoPontoConsumoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	public void tratarMedicoesDiarias(Date dataInicialRealizacaoLeitura, Date dataFinalRealizacaoLeitura,
			String codigoPontoConsumoSupervisorio) throws GGASException {

		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoHoraria = new LinkedHashMap<String, List<Object[]>>();
		ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO = new ResumoSupervisorioMedicaoVO();
		StringBuilder logProcessamento = new StringBuilder();

		Map<String, Object> filtroPesquisa = new HashMap<String, Object>();
		filtroPesquisa.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
		Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.consultarPontosConsumo(filtroPesquisa);
		PontoConsumo pontoConsumo = null;
		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		controladorSupervisorio.tratarMedicoesDiarias(pontoConsumo, dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura,
				logProcessamento, mapaAnormalidadeSupervisorioMedicaoHoraria, resumoSupervisorioMedicaoVO, null);
	}

	/**
	 * Verificar data hora contrato.
	 * 
	 * @param idSupervisorioMedicaoDiaria {@link Long}
	 * @param supervisorioMedicaoHorariaVO {@link SupervisorioMedicaoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void verificarDataHoraContrato(Long idSupervisorioMedicaoDiaria, SupervisorioMedicaoVO supervisorioMedicaoHorariaVO)
			throws GGASException {

		SupervisorioMedicaoDiaria diaria = controladorSupervisorio.obterSupervisorioMedicaoDiaria(idSupervisorioMedicaoDiaria);
		diaria.getCodigoPontoConsumoSupervisorio();
		String horaInicioDia = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_HORA_INICIO_DIA);
		Integer numeroHoraInicial = Integer.valueOf(horaInicioDia);
		Map<String, Object> filtroPesquisa = new HashMap<String, Object>();
		filtroPesquisa.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, diaria.getCodigoPontoConsumoSupervisorio());
		Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.consultarPontosConsumo(filtroPesquisa);
		PontoConsumo pontoConsumo = null;
		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}
		ContratoPontoConsumo contratoPontoConsumo = null;
		if (pontoConsumo != null) {
			contratoPontoConsumo = controladorContrato.obterContratoFaturavelPorPontoConsumo(pontoConsumo.getChavePrimaria(), Boolean.TRUE);
		}

		// valida se tem contrato ativo para aquele Ponto de Consumo
		Map<String, Object> mapaContratoAtivo = controladorContrato.validarContratoAtivoPorDataLeitura(diaria.getDataRealizacaoLeitura(),
				contratoPontoConsumo, pontoConsumo);

		if (mapaContratoAtivo != null && !mapaContratoAtivo.isEmpty()) {

			Boolean possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

			// Pega a hora inicial do contrato ativo se tiver
			if (possuiContrato && mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO) != null) {

				numeroHoraInicial = (Integer) mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO);
			}
		}
		Map<String, Date> listaDatas = Util.incrementarDataComHoraEQuantidadeDias(diaria.getDataRealizacaoLeitura(), numeroHoraInicial,
				Constantes.NUMERO_DIAS_INCREMENTAR_DATA);
		Date dataInicial = listaDatas.get(Constantes.DATA_INICIAL);
		Date dataFinal = listaDatas.get(Constantes.DATA_FINAL);

		if (!(supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura().compareTo(dataInicial) >= 0
				&& supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura().compareTo(dataFinal) <= 0)) {
			throw new NegocioException(ERRO_MEDICAO_HORARIA_FORA_CONTRATO, true);
		}

	}

	/**
	 * Criar novo supervisorio medicao comentario.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param supervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param supervisorioMedicaoDiaria {@link SupervisorioMedicaoDiaria}
	 * @param numeroSequenciaComentario {@link Integer}
	 * @param model {@link Model}
	 * @return supervisorioMedicaoComentario {@link SupervisorioMedicaoComentario}
	 * @throws GGASException {@link GGASException}
	 */
	private SupervisorioMedicaoComentario criarNovoSupervisorioMedicaoComentario(HttpServletRequest request,
			SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			Integer numeroSequenciaComentario, Model model) throws GGASException {

		String comentario = request.getParameter(COMENTARIO).trim();
		model.addAttribute(COMENTARIO, comentario);

		SupervisorioMedicaoComentario supervisorioMedicaoComentario =
				(SupervisorioMedicaoComentario) controladorSupervisorio.criarSupervisorioMedicaoComentario();

		if (supervisorioMedicaoDiaria != null) {

			supervisorioMedicaoComentario.setSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);

		} else if (supervisorioMedicaoHoraria != null) {

			supervisorioMedicaoComentario.setSupervisorioMedicaoDiaria(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria());
			supervisorioMedicaoComentario.setSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
		}

		supervisorioMedicaoComentario.setDescricao(comentario);
		supervisorioMedicaoComentario.setSequenciaComentario(numeroSequenciaComentario);
		Usuario usuario = super.obterUsuario(request);
		supervisorioMedicaoComentario.setUsuario(usuario);
		supervisorioMedicaoComentario.setDadosAuditoria(getDadosAuditoria(request));
		return supervisorioMedicaoComentario;

	}

	/**
	 * Método responsável por exibir a tela de
	 * detalhamento para medição do supervisório horário.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param supervisorioMedicaoDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_HORARIO)
	public String exibirDetalhamentoMedicaoSupervisorioHorario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, @RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long supervisorioMedicaoDiaria,
			HttpServletRequest request, Model model) throws GGASException {

		Boolean indicadorProcessado = validaMedicaoSupervisorioVO.getIndicadorProcessado();
		Boolean indicadorIntegrado = validaMedicaoSupervisorioVO.getIndicadorIntegrado();
		String tela = EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_HORARIO;

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		Map<String, Object> filtroDiaria = (Map<String, Object>) sessao.getAttribute(FILTRO);
		Map<String, Object> filtro = new HashMap<String, Object>();

		if (filtroDiaria != null && filtroDiaria.get(ATIVO) != null) {
			filtro.put(HABILITADO, filtroDiaria.get(ATIVO));
		}

		Long pendente = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE));

		Long naoAutorizado =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO));

		Long autorizado = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		if (filtroDiaria != null && filtroDiaria.get(STATUS) != null) {

			if (pendente.equals(filtroDiaria.get(STATUS)) || naoAutorizado.equals(filtroDiaria.get(STATUS))) {
				filtro.put("codigoStatusAlcada", filtroDiaria.get(STATUS));
			} else if (autorizado.equals(filtroDiaria.get(STATUS))) {
				filtro.put(STATUS_AUTORIZADO, filtroDiaria.get(STATUS));
			}
		}

		filtro.put(SUPERVISORIO_MEDICAO_DIARIA, supervisorioMedicaoDiaria);

		String ordenacao = DATA_REALIZACAO_LEITURA + " asc" + ", " + ULTIMA_ALTERACAO + DESC;

		List<Long> registrosIndicadorProcessado = new ArrayList<Long>();
		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO = null;
		SupervisorioMedicaoVO supervisorioMedicaoHorariaVO = null;

		try {

			if (validaMedicaoSupervisorioVO.getConsultaBanco() != null && validaMedicaoSupervisorioVO.getConsultaBanco()) {

				Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria =
						controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtro, ordenacao, SUPERVISORIO_MEDICAO_ANORMALIDADE,
								STATUS, ULTIMO_USUARIO_ALTERACAO);

				listaSupervisorioMedicaoHorariaVO = new ArrayList<SupervisorioMedicaoVO>();

				if (listaSupervisorioMedicaoHoraria != null && !listaSupervisorioMedicaoHoraria.isEmpty()) {
					for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : listaSupervisorioMedicaoHoraria) {

						String parametroCodigoStatusNaoAutorizado =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);

						if ((!supervisorioMedicaoHoraria.isHabilitado()
								&& !registrosIndicadorProcessado.contains(supervisorioMedicaoHoraria.getChavePrimaria()))
								|| (supervisorioMedicaoHoraria.getStatus() != null && Long.valueOf(parametroCodigoStatusNaoAutorizado)
										.equals(supervisorioMedicaoHoraria.getStatus().getChavePrimaria()))
								|| indicadorProcessado || indicadorIntegrado) {
							registrosIndicadorProcessado.add(supervisorioMedicaoHoraria.getChavePrimaria());
						}

						supervisorioMedicaoHorariaVO = this.preencherSupervisorioMedicaoHorariaVO(supervisorioMedicaoHoraria);
						supervisorioMedicaoHorariaVO.setChavePrimaria(supervisorioMedicaoHoraria.getChavePrimaria());
						supervisorioMedicaoHorariaVO.setHabilitado(supervisorioMedicaoHoraria.isHabilitado());
						supervisorioMedicaoHorariaVO.setVersao(supervisorioMedicaoHoraria.getVersao());

						supervisorioMedicaoHorariaVO.setStatus(supervisorioMedicaoHoraria.getStatus());

						if (supervisorioMedicaoHoraria.getStatus() != null) {

							if (supervisorioMedicaoHoraria.getStatus().getChavePrimaria() == pendente) {
								supervisorioMedicaoHorariaVO.setIsStatusPendente(Boolean.TRUE);
							} else {
								supervisorioMedicaoHorariaVO.setIsStatusPendente(Boolean.FALSE);
							}
						}

						DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

						supervisorioMedicaoHorariaVO.setPossuiAutorizacaoAlcada(
								controladorAlcada.possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(dadosAuditoria.getUsuario(),
										supervisorioMedicaoHoraria));

						if (supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade() != null) {
							supervisorioMedicaoHorariaVO
									.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade());
						}

						listaSupervisorioMedicaoHorariaVO.add(supervisorioMedicaoHorariaVO);

					}
				}
				sessao.setAttribute(REGISTROS_REMOCAO_LOGICA_HORARIA, new ArrayList<Long>());
				sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO_HORARIA, registrosIndicadorProcessado);
				sessao.setAttribute(REGISTROS_ALTERADOS, new ArrayList<Long>());
				validaMedicaoSupervisorioVO.setConsultaBanco(Boolean.FALSE);

			} else {
				listaSupervisorioMedicaoHorariaVO =
						(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);
				validaMedicaoSupervisorioVO.setConsultaBanco(Boolean.FALSE);
			}

			model.addAttribute("supervisorioMedicaoHorariaVO", supervisorioMedicaoHorariaVO);

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO, listaSupervisorioMedicaoHorariaVO);
			Long quantidadeRegistrosAtivos = 0L;
			for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoHorariaVO) {
				if (supervisorioMedicaoVO.getHabilitado()) {
					quantidadeRegistrosAtivos++;
				}
			}

			validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(quantidadeRegistrosAtivos);

			ParametroSistema parametroExigeComentarioMedicaoDiariaHorariaSupervisorio = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_EXIGE_COMENTARIO_MEDICAO_DIARIA_HORARIA_SUPERVISORIO);

			Long exigeComentario = Long.valueOf(parametroExigeComentarioMedicaoDiariaHorariaSupervisorio.getValor());

			validaMedicaoSupervisorioVO.setExigeComentario(exigeComentario);
			validaMedicaoSupervisorioVO.setIndicadorProcessado(indicadorProcessado);
			validaMedicaoSupervisorioVO.setIndicadorIntegrado(indicadorIntegrado);

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
		}
		
		model.addAttribute(CHAVE_PRIMARIA_DIARIA, supervisorioMedicaoDiaria);

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de
	 * detalhamento para medição do supervisório diário.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO)
	public String exibirDetalhamentoMedicaoSupervisorioDiario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		String tela = EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO;
		String anoMesReferencia = request.getParameter(ANO_MES_REFERENCIA_PARAMETRO);
		if ("".equals(anoMesReferencia)) {
			anoMesReferencia = "0";
		}
		model.addAttribute(ANO_MES_REFERENCIA_PARAMETRO, anoMesReferencia);

		try {

			ParametroSistema parametroTipoIntegracaoSupervisorios =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

			ParametroSistema parametroExibeColunasSupervisorio =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_MODULO_MEDICAO_EXIBE_COLUNAS_SUPERVISORIO);

			Long pendente = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE));

			// Cria a sessão
			HttpSession sessao = request.getSession(false);

			List<Long> registrosSomenteLeitura = new ArrayList<Long>();

			Boolean consultaBancoTelaSupervisorioDiario = validaMedicaoSupervisorioVO.getConsultaBancoTelaSupervisorioDiario();

			Map<String, Object> filtro = (Map<String, Object>) sessao.getAttribute(FILTRO);

			String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();
			filtro.put("colecaoPaginada", null);
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			if (anoMesReferencia != null && Long.parseLong(anoMesReferencia) > 0) {
				filtro.put(ANO_MES_REFERENCIA_LIDO, Long.parseLong(anoMesReferencia));
			} else {
				filtro.put("isReferenciaNull", Boolean.TRUE);
			}
			if ((validaMedicaoSupervisorioVO.getIdCicloParametro() != null) && (validaMedicaoSupervisorioVO.getIdCicloParametro() > 0)) {
				filtro.put(ID_CICLO, validaMedicaoSupervisorioVO.getIdCicloParametro());
			} else {
				filtro.put("isCicloNull", Boolean.TRUE);
			}

			Collection<PontoConsumo> listaPontoConsumo =
					controladorPontoConsumo.consultarPontosConsumo(filtro, SITUACAO_CONSUMO, SEGMENTO, IMOVEL);
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();

				if (pontoConsumo != null) {

					model.addAttribute(PONTO_CONSUMO, pontoConsumo);
					model.addAttribute(CLIENTE_PRINCIPAL, controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));
				}
			}

			List<Long> registrosIndicadorProcessado = new ArrayList<Long>();
			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO = null;

			if (consultaBancoTelaSupervisorioDiario != null && consultaBancoTelaSupervisorioDiario) {
				String ordenacao = "smd." + DATA_REALIZACAO_LEITURA + " asc" + ", " + "smd." + ULTIMA_ALTERACAO + DESC;

				Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = new ArrayList<SupervisorioMedicaoDiaria>();

				listaSupervisorioMedicaoDiaria = controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao,
						SUPERVISORIO_MEDICAO_ANORMALIDADE, STATUS, ULTIMO_USUARIO_ALTERACAO);

				if (filtro.get(STATUS) != null) {

					filtro.put(STATUS, validaMedicaoSupervisorioVO.getStatus());
					Map<Long, Long> chavePrimariaDiaria = new HashMap<Long, Long>();

					if (isPesquisaPendentes()) {
						if ("diaria".equals(getTipoPendentes())) {
							for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : controladorAlcada
									.obterSupervisorioMedicaoDiariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
								chavePrimariaDiaria.put(supervisorioMedicaoDiaria.getChavePrimaria(),
										supervisorioMedicaoDiaria.getChavePrimaria());
							}
						} else if ("horaria".equals(getTipoPendentes())) {
							for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : controladorAlcada
									.obterSupervisorioMedicaoHorariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
								chavePrimariaDiaria.put(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria(),
										supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
							}
						}
					} else {
						for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : controladorAlcada
								.obterSupervisorioMedicaoDiariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
							chavePrimariaDiaria.put(supervisorioMedicaoDiaria.getChavePrimaria(),
									supervisorioMedicaoDiaria.getChavePrimaria());
						}

						for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : controladorAlcada
								.obterSupervisorioMedicaoHorariaPendentesPorUsuario(filtro, super.obterUsuario(request))) {
							if (supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria() != null) {
								chavePrimariaDiaria.put(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria(),
										supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria().getChavePrimaria());
							}
						}
					}

					filtro.put(CHAVE_PRIMARIA_DIARIA, chavePrimariaDiaria);
					filtro.put(STATUS, null);
					filtro.put("codigoStatusAlcada", null);
					listaSupervisorioMedicaoDiaria = controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao,
							SUPERVISORIO_MEDICAO_ANORMALIDADE, STATUS, ULTIMO_USUARIO_ALTERACAO);

					filtro.put(STATUS, validaMedicaoSupervisorioVO.getStatus());
					filtro.put(CHAVE_PRIMARIA_DIARIA, null);
				}

				listaSupervisorioMedicaoDiariaVO = new ArrayList<SupervisorioMedicaoVO>();

				String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

				if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
					for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
						if (parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
							registrosSomenteLeitura.add(supervisorioMedicaoDiaria.getChavePrimaria());
						}

						if ((supervisorioMedicaoDiaria.getIndicadorProcessado()
								|| Boolean.FALSE.equals(supervisorioMedicaoDiaria.isHabilitado()))
								&& !registrosIndicadorProcessado.contains(supervisorioMedicaoDiaria.getChavePrimaria())) {
							registrosIndicadorProcessado.add(supervisorioMedicaoDiaria.getChavePrimaria());
						}

						SupervisorioMedicaoVO supervisorioMedicaoDiariaVO =
								this.preencherSupervisorioMedicaoDiariaVO(supervisorioMedicaoDiaria);

						supervisorioMedicaoDiariaVO.setChavePrimaria(supervisorioMedicaoDiaria.getChavePrimaria());
						supervisorioMedicaoDiariaVO.setHabilitado(supervisorioMedicaoDiaria.isHabilitado());
						supervisorioMedicaoDiariaVO.setVersao(supervisorioMedicaoDiaria.getVersao());

						supervisorioMedicaoDiariaVO.setStatus(supervisorioMedicaoDiaria.getStatus());

						if (Boolean.TRUE.equals(supervisorioMedicaoDiaria.isHabilitado())) {
							if (supervisorioMedicaoDiaria.getStatus() != null) {

								if (supervisorioMedicaoDiaria.getStatus().getChavePrimaria() == pendente) {
									supervisorioMedicaoDiariaVO.setIsStatusPendente(Boolean.TRUE);
								} else {
									supervisorioMedicaoDiariaVO.setIsStatusPendente(Boolean.FALSE);
								}
							} else {
								supervisorioMedicaoDiariaVO.setIsStatusPendente(Boolean.FALSE);
							}
						}

						DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

						supervisorioMedicaoDiariaVO.setPossuiAutorizacaoAlcada(controladorAlcada
								.possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(dadosAuditoria.getUsuario(), supervisorioMedicaoDiaria));

						if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null) {
							supervisorioMedicaoDiariaVO
									.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade());
						}

						int quantidadeSupervisorioMedicaoHoraria =
								this.obterQuantidadeSupervisorioMedicaoHoraria(supervisorioMedicaoDiaria.getChavePrimaria(), true, filtro);

						supervisorioMedicaoDiariaVO.setQuantidadeSupervisorioMedicaoHoraria(quantidadeSupervisorioMedicaoHoraria);
						listaSupervisorioMedicaoDiariaVO.add(supervisorioMedicaoDiariaVO);

						model.addAttribute("supervisorioMedicaoDiariaVO", supervisorioMedicaoDiariaVO);
					}
				}

				if (parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
					sessao.setAttribute(PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS, true);
				} else {
					sessao.setAttribute(PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS, false);
				}

				if ("1".equals(parametroExibeColunasSupervisorio.getValor())) {
					sessao.setAttribute(PARAMETRO_EXIBE_COLUNAS_SUPERVISORIO, true);
				} else {
					sessao.setAttribute(PARAMETRO_EXIBE_COLUNAS_SUPERVISORIO, false);
				}

				sessao.setAttribute(REGISTROS_SOMENTE_LEITURA, registrosSomenteLeitura);
				sessao.setAttribute(REGISTROS_REMOCAO_LOGICA, new ArrayList<Long>());
				sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO, registrosIndicadorProcessado);
				sessao.setAttribute(REGISTROS_ALTERADOS, new ArrayList<Long>());
				validaMedicaoSupervisorioVO.setConsultaBancoTelaSupervisorioDiario(Boolean.FALSE);
			} else {
				listaSupervisorioMedicaoDiariaVO = (List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);
				validaMedicaoSupervisorioVO.setConsultaBancoTelaSupervisorioDiario(Boolean.FALSE);
				String ordenacao = "smd." + DATA_REALIZACAO_LEITURA + " asc" + ", " + "smd." + ULTIMA_ALTERACAO + DESC;

				Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria =
						controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, SUPERVISORIO_MEDICAO_ANORMALIDADE,
								STATUS, ULTIMO_USUARIO_ALTERACAO);

				String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

				if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
					for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
						if (parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
							registrosSomenteLeitura.add(supervisorioMedicaoDiaria.getChavePrimaria());
						}
					}
				}
				sessao.setAttribute(REGISTROS_SOMENTE_LEITURA, registrosSomenteLeitura);
			}

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO, listaSupervisorioMedicaoDiariaVO);
			Long quantidadeRegistrosAtivos = 0L;
			BigDecimal totalVolumesCorrigidosAtivos = new BigDecimal("0");
			Long totalVolumesNaoCorrigidosAtivos = 0L;

			for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoDiariaVO) {
				if (supervisorioMedicaoVO.getHabilitado()) {
					quantidadeRegistrosAtivos++;
					if (supervisorioMedicaoVO.getConsumoComCorrecaoFatorPTZ() != null) {
						totalVolumesCorrigidosAtivos = totalVolumesCorrigidosAtivos
								.add(new BigDecimal(supervisorioMedicaoVO.getConsumoComCorrecaoFatorPTZ().toString()));
					}
					if (supervisorioMedicaoVO.getConsumoSemCorrecaoFatorPTZ() != null) {
						totalVolumesNaoCorrigidosAtivos += supervisorioMedicaoVO.getConsumoSemCorrecaoFatorPTZ().longValue();
					}
				}
			}

			validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(quantidadeRegistrosAtivos);
			model.addAttribute("totalVolumesCorrigidosAtivos",
					Long.valueOf(totalVolumesCorrigidosAtivos.longValue()).toString().replace(".", ","));
			model.addAttribute("totalVolumesNaoCorrigidosAtivos", totalVolumesNaoCorrigidosAtivos);

			ParametroSistema parametroExigeComentarioMedicaoDiariaHorariaSupervisorio = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_EXIGE_COMENTARIO_MEDICAO_DIARIA_HORARIA_SUPERVISORIO);

			Long exigeComentario = Long.valueOf(parametroExigeComentarioMedicaoDiariaHorariaSupervisorio.getValor());

			validaMedicaoSupervisorioVO.setExigeComentario(exigeComentario);

			model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarMedicaoSupervisorio(validaMedicaoSupervisorioVO, bindingResult, request, model);
			model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);
		}

		return tela;
	}

	/**
	 * Obter quantidade supervisorio medicao horaria.
	 * 
	 * @param chavePrimariaSupervisorioMedicaoDiaria {@link Long}
	 * @param habilitado {@link Boolean}
	 * @return int {@link Integer}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	public int obterQuantidadeSupervisorioMedicaoHoraria(Long chavePrimariaSupervisorioMedicaoDiaria, Boolean habilitado)
			throws GGASException {

		// monta o filtro para fazer a pesquisa
		Map<String, Object> filtroAux = new HashMap<String, Object>();
		filtroAux.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaSupervisorioMedicaoDiaria);
		filtroAux.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);
		filtroAux.put(HABILITADO, habilitado);

		String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		filtroAux.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

		// consulta a medições horárias de acordo com o filtro
		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria =
				controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtroAux, null);

		return listaSupervisorioMedicaoHoraria.size();

	}

	/**
	 * Obter quantidade supervisorio medicao horaria.
	 * 
	 * @param chavePrimariaSupervisorioMedicaoDiaria {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param filtro {@link Map}
	 * @return int {@link Integer}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	public int obterQuantidadeSupervisorioMedicaoHoraria(Long chavePrimariaSupervisorioMedicaoDiaria, Boolean habilitado,
			Map<String, Object> filtro) throws GGASException {

		// monta o filtro para fazer a pesquisa
		Map<String, Object> filtroAux = new HashMap<String, Object>();
		filtroAux.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaSupervisorioMedicaoDiaria);
		filtroAux.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);

		if (filtro.get(ATIVO) != null) {
			if ("true".equals(filtro.get(ATIVO).toString())) {
				filtroAux.put(HABILITADO, true);
			} else {
				filtroAux.put(HABILITADO, false);
			}
		} else {
			filtroAux.put(HABILITADO, null);
		}

		filtroAux.put(STATUS, filtro.get(STATUS));

		// consulta a medições horárias de acordo com o filtro
		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria =
				controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtroAux, null);

		return listaSupervisorioMedicaoHoraria.size();

	}

	/**
	 * Preencher supervisorio medicao diaria vo.
	 * 
	 * @param supervisorioMedicaoDiaria {@link SupervisorioMedicaoDiaria}
	 * @return supervisorioMedicaoDiariaVO {@link SupervisorioMedicaoVO}
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 */
	public SupervisorioMedicaoVO preencherSupervisorioMedicaoDiariaVO(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
			throws FormatoInvalidoException {

		SupervisorioMedicaoVO supervisorioMedicaoDiariaVO = new SupervisorioMedicaoVO();

		supervisorioMedicaoDiariaVO.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ());
		supervisorioMedicaoDiariaVO.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ());

		if (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() != null) {

			if (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setConsumoSemCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ());
			} else {

				supervisorioMedicaoDiariaVO.setConsumoSemCorrecaoFatorPTZ(
						supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}

		}

		if (supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() != null) {

			if (supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setConsumoComCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ());

			} else {

				supervisorioMedicaoDiariaVO.setConsumoComCorrecaoFatorPTZ(
						supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}
		}

		if (supervisorioMedicaoDiaria.getPressao() != null) {

			if (supervisorioMedicaoDiaria.getPressao().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setPressao(supervisorioMedicaoDiaria.getPressao());
			} else {

				supervisorioMedicaoDiariaVO
						.setPressao(supervisorioMedicaoDiaria.getPressao().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}
		}

		if (supervisorioMedicaoDiaria.getTemperatura() != null) {

			if (supervisorioMedicaoDiaria.getTemperatura().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setTemperatura(supervisorioMedicaoDiaria.getTemperatura());
			} else {

				supervisorioMedicaoDiariaVO
						.setTemperatura(supervisorioMedicaoDiaria.getTemperatura().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}
		}

		if (supervisorioMedicaoDiaria.getFatorZ() != null) {

			if (supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setFatorZ(supervisorioMedicaoDiaria.getFatorZ());
			} else {

				supervisorioMedicaoDiariaVO
						.setFatorZ(supervisorioMedicaoDiaria.getFatorZ().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}
		}

		if (supervisorioMedicaoDiaria.getFatorPTZ() != null) {

			if (supervisorioMedicaoDiaria.getFatorPTZ().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoDiariaVO.setFatorPTZ(supervisorioMedicaoDiaria.getFatorPTZ());
			} else {

				supervisorioMedicaoDiariaVO
						.setFatorPTZ(supervisorioMedicaoDiaria.getFatorPTZ().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}

		}

		supervisorioMedicaoDiariaVO.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio());
		supervisorioMedicaoDiariaVO.setIndicadorIntegrado(supervisorioMedicaoDiaria.getIndicadorIntegrado());
		supervisorioMedicaoDiariaVO.setIndicadorProcessado(supervisorioMedicaoDiaria.getIndicadorProcessado());
		supervisorioMedicaoDiariaVO.setIndicadorConsolidada(supervisorioMedicaoDiaria.getIndicadorConsolidada());
		supervisorioMedicaoDiariaVO.setNumeroCiclo(supervisorioMedicaoDiaria.getNumeroCiclo());

		supervisorioMedicaoDiariaVO.setDataReferencia(supervisorioMedicaoDiaria.getDataReferencia());
		supervisorioMedicaoDiariaVO.setDataReferenciaFormatado(supervisorioMedicaoDiaria.getDataReferenciaFormatado());
		supervisorioMedicaoDiariaVO.setDataRealizacaoLeitura(supervisorioMedicaoDiaria.getDataRealizacaoLeitura());
		supervisorioMedicaoDiariaVO.setDataRegistroLeitura(supervisorioMedicaoDiaria.getDataRegistroLeitura());

		supervisorioMedicaoDiariaVO.setUltimaAlteracao(supervisorioMedicaoDiaria.getUltimaAlteracao());

		supervisorioMedicaoDiariaVO.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade());

		return supervisorioMedicaoDiariaVO;
	}

	/**
	 * Preencher supervisorio medicao horaria vo.
	 * 
	 * @param supervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @return supervisorioMedicaoHorariaVO {@link SupervisorioMedicaoVO}
	 */
	public SupervisorioMedicaoVO preencherSupervisorioMedicaoHorariaVO(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) {

		SupervisorioMedicaoVO supervisorioMedicaoHorariaVO = new SupervisorioMedicaoVO();
		supervisorioMedicaoHorariaVO.setDataRealizacaoLeitura(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
		supervisorioMedicaoHorariaVO.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ());
		supervisorioMedicaoHorariaVO.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ());

		if (supervisorioMedicaoHoraria.getPressao() != null) {

			if (supervisorioMedicaoHoraria.getPressao().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoHorariaVO.setPressao(supervisorioMedicaoHoraria.getPressao());
			} else {

				supervisorioMedicaoHorariaVO
						.setPressao(supervisorioMedicaoHoraria.getPressao().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}

		}

		if (supervisorioMedicaoHoraria.getTemperatura() != null) {

			if (supervisorioMedicaoHoraria.getTemperatura().compareTo(BigDecimal.ZERO) == 0) {

				supervisorioMedicaoHorariaVO.setTemperatura(supervisorioMedicaoHoraria.getTemperatura());
			} else {

				supervisorioMedicaoHorariaVO.setTemperatura(
						supervisorioMedicaoHoraria.getTemperatura().setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
			}

		}

		supervisorioMedicaoHorariaVO.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio());
		supervisorioMedicaoHorariaVO.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade());
		supervisorioMedicaoHorariaVO.setDataRegistroLeitura(supervisorioMedicaoHoraria.getDataRegistroLeitura());

		return supervisorioMedicaoHorariaVO;
	}

	/**
	 * Adicionar registro supervisorio diario.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioDiario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({"unchecked"})
	@RequestMapping("adicionarRegistroSupervisorioDiario")
	public String adicionarRegistroSupervisorioDiario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO = null;
		ParametroSistema parametroTipoIntegracaoSupervisorios = null;
		List<Long> registrosSomenteLeitura = null;
		
		// Cria a sessão
		HttpSession sessao = request.getSession(false);
		
		try {

			parametroTipoIntegracaoSupervisorios =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

			registrosSomenteLeitura = (List<Long>) sessao.getAttribute(REGISTROS_SOMENTE_LEITURA);

			model.addAttribute(EXIGE_COMENTARIO, validaMedicaoSupervisorioVO.getExigeComentario());

			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);

			Collection<PontoConsumo> listaPontoConsumo =
					controladorPontoConsumo.consultarPontosConsumo(filtro, SITUACAO_CONSUMO, SEGMENTO, IMOVEL);

			PontoConsumo pontoConsumo = null;
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				pontoConsumo = listaPontoConsumo.iterator().next();
			}

			if (pontoConsumo != null) {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				model.addAttribute(CLIENTE_PRINCIPAL, controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		listaSupervisorioMedicaoDiariaVO = (List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVOAux = new ArrayList<SupervisorioMedicaoVO>();

		String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

		for (SupervisorioMedicaoVO supervisorioMedicaoDiariaVO : listaSupervisorioMedicaoDiariaVO) {

			listaSupervisorioMedicaoDiariaVOAux.add(supervisorioMedicaoDiariaVO);

			if (supervisorioMedicaoDiariaVO.getChavePrimaria() != null
					&& supervisorioMedicaoDiariaVO.getChavePrimaria().equals(validaMedicaoSupervisorioVO.getChavePrimaria())) {

				SupervisorioMedicaoVO supervisorioMedicaoDiariaVOAux = new SupervisorioMedicaoVO();
				supervisorioMedicaoDiariaVOAux.setChavePrimaria(contadorAuxiliar);

				if (parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
					registrosSomenteLeitura.add(contadorAuxiliar);
				}

				supervisorioMedicaoDiariaVOAux
						.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiariaVO.getCodigoPontoConsumoSupervisorio());
				supervisorioMedicaoDiariaVOAux.setHabilitado(Boolean.TRUE);
				listaSupervisorioMedicaoDiariaVOAux.add(supervisorioMedicaoDiariaVOAux);

				contadorAuxiliar += -1;

			} else if (supervisorioMedicaoDiariaVO.getChavePrimaria() == null || supervisorioMedicaoDiariaVO.getChavePrimaria() < 0
					&& parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
				registrosSomenteLeitura.add(supervisorioMedicaoDiariaVO.getChavePrimaria());
			}

			model.addAttribute("supervisorioMedicaoDiariaVO", supervisorioMedicaoDiariaVO);
		}

		if (validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() != null) {
			validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() + 1L);
		}
		sessao.setAttribute(REGISTROS_SOMENTE_LEITURA, registrosSomenteLeitura);
		sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO, listaSupervisorioMedicaoDiariaVOAux);
		model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);

		return EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO;
	}

	/**
	 * Remover registro supervisorio diario.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioDiario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerRegistroSupervisorioDiario")
	public String removerRegistroSupervisorioDiario(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		ParametroSistema parametroTipoIntegracaoSupervisorios = null;
		List<Long> registrosSomenteLeitura = null;
		SupervisorioMedicaoVO supervisorioMedicaoDiariaVOAux = null;
		Long chavePrimaria = validaMedicaoSupervisorioVO.getChavePrimaria();

		try {

			parametroTipoIntegracaoSupervisorios =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

			registrosSomenteLeitura = (List<Long>) sessao.getAttribute(REGISTROS_SOMENTE_LEITURA);

			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, validaMedicaoSupervisorioVO.getEnderecoRemotoParametro());

			Collection<PontoConsumo> listaPontoConsumo =
					controladorPontoConsumo.consultarPontosConsumo(filtro, SITUACAO_CONSUMO, SEGMENTO, IMOVEL);

			PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();

			if (pontoConsumo != null) {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				model.addAttribute(CLIENTE_PRINCIPAL, controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);

		for (SupervisorioMedicaoVO supervisorioMedicaoDiariaVO : listaSupervisorioMedicaoDiariaVO) {

			if (supervisorioMedicaoDiariaVO.getChavePrimaria() != null
					&& supervisorioMedicaoDiariaVO.getChavePrimaria().equals(chavePrimaria)) {

				supervisorioMedicaoDiariaVOAux = supervisorioMedicaoDiariaVO;
			}

		}

		String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

		List<Long> registrosRemocaoLogica = (List<Long>) sessao.getAttribute(REGISTROS_REMOCAO_LOGICA);
		List<Long> registrosAlterados = (List<Long>) sessao.getAttribute(REGISTROS_ALTERADOS);

		if (supervisorioMedicaoDiariaVOAux != null && supervisorioMedicaoDiariaVOAux.getChavePrimaria() < 0) {

			listaSupervisorioMedicaoDiariaVO.remove(supervisorioMedicaoDiariaVOAux);

			if (parametroTipoIntegracaoSupervisorios.getValor().equals(tipoIntegracaoHoraria)) {
				registrosSomenteLeitura.remove(supervisorioMedicaoDiariaVOAux.getChavePrimaria());
			}

			if (registrosAlterados.contains(chavePrimaria)) {
				registrosAlterados.remove(chavePrimaria);
				sessao.setAttribute(REGISTROS_ALTERADOS, registrosAlterados);
			}

			if (validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() != null) {
				validaMedicaoSupervisorioVO.setQuantidadeRegistrosAtivos(validaMedicaoSupervisorioVO.getQuantidadeRegistrosAtivos() - 1L);
			}
			model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);
		} else {

			registrosRemocaoLogica = (List<Long>) sessao.getAttribute(REGISTROS_REMOCAO_LOGICA);
			registrosAlterados = (List<Long>) sessao.getAttribute(REGISTROS_ALTERADOS);

			if (registrosAlterados.contains(chavePrimaria)) {
				registrosAlterados.remove(chavePrimaria);
				sessao.setAttribute(REGISTROS_ALTERADOS, registrosAlterados);
			}

			verificaChavePrimariaExistente(chavePrimaria, registrosRemocaoLogica);

			sessao.setAttribute(REGISTROS_REMOCAO_LOGICA, registrosRemocaoLogica);
		}

		sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO, listaSupervisorioMedicaoDiariaVO);
		sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());

		return EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO;
	}

	/**
	 * Salvar supervisorio medicao diaria.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping("salvarSupervisorioMedicaoDiaria")
	public String salvarSupervisorioMedicaoDiaria(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String tela = null;
		model.addAttribute("clientePrincipalNome", request.getParameter("clientePrincipalNome"));
		model.addAttribute(COMENTARIO, request.getParameter(COMENTARIO).trim());

		try {

			// FIXME: Substituir por consulta à tabela pela classe
			ParametroSistema parametroTabelaSuperMedicaoDiaria =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_DIARIA);

			// Cria a sessão
			HttpSession sessao = request.getSession(false);
			Map<String, Object> filtroPesquisa = (Map<String, Object>) sessao.getAttribute(FILTRO);
			Boolean isFiltroPesquisaHabilitado = (Boolean) filtroPesquisa.get(ATIVO);

			List<Long> registrosRemocaoLogica = (List<Long>) sessao.getAttribute(REGISTROS_REMOCAO_LOGICA);
			List<Long> registrosAlterados = (List<Long>) sessao.getAttribute(REGISTROS_ALTERADOS);
			List<Long> registrosSalvos = new ArrayList<Long>();
			PontoConsumo pontoConsumo = null;
			HistoricoConsumo ultimoHistoricoConsumo = null;
			Periodicidade periodicidade = null;

			String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO =
					(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);

			EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusAutorizado));
			if (listaSupervisorioMedicaoDiariaVO != null) {
				consultarPrimeiraDataRealizacaoLeitura(listaSupervisorioMedicaoDiariaVO, status);
			}

			if (listaSupervisorioMedicaoDiariaVO != null && !listaSupervisorioMedicaoDiariaVO.isEmpty()) {

				// Monta filtro para a consulta
				Map<String, Object> filtro = new HashMap<String, Object>();
				String codigoSupervisorio = Util.primeiroElemento(listaSupervisorioMedicaoDiariaVO).getCodigoPontoConsumoSupervisorio();
				filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoSupervisorio);

				// Recupera o pontoConsumo pelo codigoPontoConsumoSupervisorio
				Collection<PontoConsumo> listaPontoConsumo =
						controladorPontoConsumo.consultarPontosConsumo(filtro, ROTA_GRUPO_FATURAMENTO_TIPO_LEITURA);
				if (!listaPontoConsumo.isEmpty()) {
					pontoConsumo = listaPontoConsumo.iterator().next();
				} else {
					// Se for medidor independente, pega o ponto de consumo de um dos medidores virtuais que o medidor independente faz
					// parte.
					filtro = new HashMap<String, Object>();
					filtro.put(CODIGO_MEDIDOR_SUPERVISORIO, codigoSupervisorio);
					Collection<Medidor> listaMedidor = controladorMedidor.consultarMedidor(filtro);
					if (!listaMedidor.isEmpty()) {
						Medidor medidor = listaMedidor.iterator().next();
						Collection<Medidor> listaMedidorVirtual = controladorMedidor.consultarMedidorVirtualPorMedidorIndependente(medidor);
						if (!listaMedidor.isEmpty()) {
							Medidor medidorVirtual = listaMedidorVirtual.iterator().next();
							InstalacaoMedidor instalacaoMV = medidorVirtual.getInstalacaoMedidor();
							if (instalacaoMV != null) {
								pontoConsumo = instalacaoMV.getPontoConsumo();
							}
						}
					}
				}

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				
				// obter o HistoricoConsumo
				if (pontoConsumo != null) {
					ultimoHistoricoConsumo =
							controladorHistoricoConsumo.obterUltimoHistoricoConsumoFaturado(pontoConsumo.getChavePrimaria());
				}

			}

			if (registrosRemocaoLogica != null && !registrosRemocaoLogica.isEmpty()) {

				for (Long chavePrimaria : registrosRemocaoLogica) {
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = null;
					supervisorioMedicaoDiaria = controladorSupervisorio.obterSupervisorioMedicaoDiaria(chavePrimaria);
					supervisorioMedicaoDiaria.setHabilitado(Boolean.FALSE);
					supervisorioMedicaoDiaria.setDadosAuditoria(getDadosAuditoria(request));
					SupervisorioMedicaoVO supervisorioMedicaoVO =
							obterSupervisorioMedicaoVO(listaSupervisorioMedicaoDiariaVO, chavePrimaria);
					if (supervisorioMedicaoVO != null) {
						supervisorioMedicaoVO.setHabilitado(Boolean.FALSE);
					}

					controladorSupervisorio.atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria,
							chavePrimaria, Boolean.TRUE, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);

					if (isFiltroPesquisaHabilitado == null || !isFiltroPesquisaHabilitado && !registrosSalvos.contains(chavePrimaria)) {

						registrosSalvos.add(supervisorioMedicaoDiaria.getChavePrimaria());
					}
				}

			}

			if (listaSupervisorioMedicaoDiariaVO != null) {

				for (Iterator<SupervisorioMedicaoVO> iterator = listaSupervisorioMedicaoDiariaVO.iterator(); iterator.hasNext();) {

					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO = new SupervisorioMedicaoVO();
					supervisorioMedicaoDiariaVO = iterator.next();
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = null;

					controladorSupervisorio.validarDadosObrigatoriosSupervisorioMedicaoDiaria(null, supervisorioMedicaoDiariaVO);

					periodicidade = controladorContrato.obterPeriodicidadePorPontoConsumo(pontoConsumo,
							supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura());

					if (supervisorioMedicaoDiariaVO.getChavePrimaria() == null || supervisorioMedicaoDiariaVO.getChavePrimaria() < 0) {

						String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();
						supervisorioMedicaoDiaria = this.preencherSupervisorioMedicaoDiaria(supervisorioMedicaoDiariaVO);

						supervisorioMedicaoDiaria.setIndicadorIntegrado(Boolean.FALSE);
						supervisorioMedicaoDiaria.setIndicadorProcessado(Boolean.FALSE);
						supervisorioMedicaoDiaria.setDataRegistroLeitura(Calendar.getInstance().getTime());
						supervisorioMedicaoDiaria.setDadosAuditoria(getDadosAuditoria(request));
						supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.FALSE);
						SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = controladorSupervisorio
								.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE);
						supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

						SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo = this.criarNovoSupervisorioMedicaoComentario(
								request, null, supervisorioMedicaoDiaria, Integer.valueOf(1), model);

						controladorSupervisorio.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
						controladorSupervisorio.validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);

						this.verificarDataRealizacaoLeitura(supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(),
								codigoPontoConsumoSupervisorio);
						this.verificarMedicoesDiariasDuplicadas(supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(),
								codigoPontoConsumoSupervisorio, null);

						verificarMedicoesDuplicadas(listaSupervisorioMedicaoDiariaVO,
								supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(), Boolean.TRUE);

						Boolean isAnoMesReferenciaCicloValido = controladorSupervisorio.validarAnoMesReferenciaCiclo(
								supervisorioMedicaoDiaria.getDataReferencia(), supervisorioMedicaoDiaria.getNumeroCiclo(), pontoConsumo,
								supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), paramStatusAutorizado, ultimoHistoricoConsumo,
								periodicidade);

						if (!isAnoMesReferenciaCicloValido) {

							sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());
							throw new NegocioException(ControladorSupervisorio.ERRO_NEGOCIO_ANO_MES_REFERENCIA_CICLO, true);
						}

						controladorSupervisorio.inserirSupervisorioMedicaoDiaria(getDadosAuditoria(request), supervisorioMedicaoDiaria,
								parametroTabelaSuperMedicaoDiaria, supervisorioMedicaoComentarioNovo, null, Boolean.FALSE, null,
								Boolean.TRUE);

						if (isFiltroPesquisaHabilitado == null
								|| isFiltroPesquisaHabilitado && !registrosSalvos.contains(supervisorioMedicaoDiaria.getChavePrimaria())) {

							registrosSalvos.add(supervisorioMedicaoDiaria.getChavePrimaria());
						}

					} else if (registrosAlterados != null && !registrosAlterados.isEmpty()
							&& registrosAlterados.contains(supervisorioMedicaoDiariaVO.getChavePrimaria())) {

						String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();
						supervisorioMedicaoDiaria = controladorSupervisorio
								.obterSupervisorioMedicaoDiaria(supervisorioMedicaoDiariaVO.getChavePrimaria(), STATUS);
						supervisorioMedicaoDiaria.setHabilitado(Boolean.FALSE);
						supervisorioMedicaoDiaria.setDadosAuditoria(getDadosAuditoria(request));

						SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo =
								this.preencherSupervisorioMedicaoDiaria(supervisorioMedicaoDiariaVO);

						supervisorioMedicaoDiariaNovo.setDataRegistroLeitura(Calendar.getInstance().getTime());
						supervisorioMedicaoDiariaNovo.setDadosAuditoria(getDadosAuditoria(request));
						supervisorioMedicaoDiariaNovo.setIndicadorConsolidada(Boolean.FALSE);

						this.verificarAnormalidadeDiaria(supervisorioMedicaoDiariaNovo, supervisorioMedicaoDiariaVO,
								supervisorioMedicaoDiaria, iterator);

						Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario =
								this.pesquisarSupervisorioMedicaoComentario(null, supervisorioMedicaoDiaria.getChavePrimaria());

						SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo = this.criarNovoSupervisorioMedicaoComentario(
								request, null, supervisorioMedicaoDiariaNovo, Integer.valueOf(1), model);

						controladorSupervisorio.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
						controladorSupervisorio.validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
						this.verificarDataRealizacaoLeitura(supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(),
								codigoPontoConsumoSupervisorio);
						this.verificarMedicoesDiariasDuplicadas(supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(),
								codigoPontoConsumoSupervisorio, supervisorioMedicaoDiariaVO.getChavePrimaria());

						verificarMedicoesDuplicadas(listaSupervisorioMedicaoDiariaVO,
								supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura(), Boolean.TRUE);

						Boolean isAnoMesReferenciaCicloValido = controladorSupervisorio.validarAnoMesReferenciaCiclo(
								supervisorioMedicaoDiariaNovo.getDataReferencia(), supervisorioMedicaoDiariaNovo.getNumeroCiclo(),
								pontoConsumo, supervisorioMedicaoDiariaNovo.getDataRealizacaoLeitura(), paramStatusAutorizado,
								ultimoHistoricoConsumo, periodicidade);

						if (!isAnoMesReferenciaCicloValido) {

							sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());
							throw new NegocioException(ControladorSupervisorio.ERRO_NEGOCIO_ANO_MES_REFERENCIA_CICLO, true);
						}

						controladorSupervisorio.atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(getDadosAuditoria(request),
								supervisorioMedicaoDiaria, parametroTabelaSuperMedicaoDiaria, supervisorioMedicaoDiaria.getChavePrimaria(),
								Boolean.TRUE, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA, supervisorioMedicaoDiariaNovo,
								listaSupervisorioMedicaoComentario, supervisorioMedicaoComentarioNovo, Boolean.TRUE);

						if (!registrosSalvos.contains(supervisorioMedicaoDiaria.getChavePrimaria())) {

							if (isFiltroPesquisaHabilitado == null) {
								registrosSalvos.add(supervisorioMedicaoDiaria.getChavePrimaria());
								registrosSalvos.add(supervisorioMedicaoDiariaNovo.getChavePrimaria());

							} else {
								if (isFiltroPesquisaHabilitado) {
									registrosSalvos.add(supervisorioMedicaoDiariaNovo.getChavePrimaria());
								} else {
									registrosSalvos.add(supervisorioMedicaoDiaria.getChavePrimaria());
								}

							}

						}

					}

				}
			}
			if (listaSupervisorioMedicaoDiariaVO != null && listaSupervisorioMedicaoDiariaVO.size() > 1) {

				listaSupervisorioMedicaoDiariaVO =
						this.ordenarSupervisoriMedicaoVOPorDataRealizacaoLeitura(listaSupervisorioMedicaoDiariaVO);
			}

			String codigoPontoConsumoSupervisorio = null;

			if (listaSupervisorioMedicaoDiariaVO != null && !listaSupervisorioMedicaoDiariaVO.isEmpty()) {

				// chama o processo de consolidar dados do supervisório
				SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoDiaria =
						Util.primeiroElemento(listaSupervisorioMedicaoDiariaVO);
				SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoDiaria =
						listaSupervisorioMedicaoDiariaVO.get(listaSupervisorioMedicaoDiariaVO.size() - 1);

				Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoDiaria.getDataRealizacaoLeitura();
				Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoDiaria.getDataRealizacaoLeitura();

				Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria = new LinkedHashMap<String, List<Object[]>>();
				ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO = new ResumoSupervisorioMedicaoVO();

				StringBuilder logProcessamento = new StringBuilder();
				codigoPontoConsumoSupervisorio = primeiroRegistroListaSupervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio();

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
				Collection<PontoConsumo> listaPontoConsumo =
						controladorPontoConsumo.consultarPontosConsumo(filtro, ROTA_GRUPO_FATURAMENTO_TIPO_LEITURA);
				pontoConsumo = null;
				if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
					pontoConsumo = listaPontoConsumo.iterator().next();
				}

				controladorSupervisorio.tratarMedicoesDiarias(pontoConsumo, dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura,
						logProcessamento, mapaAnormalidadeSupervisorioMedicaoDiaria, resumoSupervisorioMedicaoVO, null);
			}

			// seta o atributo consultaBanco para TRUE para que a lista que é exibida na tela de validação do supervisório diário
			// traga registros do banco
			validaMedicaoSupervisorioVO.setConsultaBancoTelaSupervisorioDiario(Boolean.TRUE);

			if (codigoPontoConsumoSupervisorio != null) {

				model.addAttribute(ENDERECO_REMOTO_PARAMETRO, codigoPontoConsumoSupervisorio);
			}

			// insere na lista registrosSalvos todos os registros que foram salvos no banco de acordo com o filtro da tela de pesquisa
			sessao.setAttribute(REGISTROS_SALVOS, registrosSalvos);

			tela = exibirDetalhamentoMedicaoSupervisorioDiario(validaMedicaoSupervisorioVO, bindingResult, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = EXIBIR_DETALHAMENTO_MEDICAO_SUPERVISORIO_DIARIO;
		}

		model.addAttribute(VALIDA_MEDICAO_SUPERVISORIO_VO, validaMedicaoSupervisorioVO);
		

		return tela;
	}

	/**
	 * Consultar primeira data realizacao leitura.
	 * 
	 * @param listaSupervisorioMedicaoDiariaVO {@link List}
	 * @param status {@link EntidadeConteudo}
	 * @return primeiraDataRealizacaoLeitura {@link Date}
	 */
	private Date consultarPrimeiraDataRealizacaoLeitura(List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO,
			EntidadeConteudo status) {

		Date primeiraDataRealizacaoLeitura = null;
		for (SupervisorioMedicaoVO supervisorio : listaSupervisorioMedicaoDiariaVO) {
			if ((supervisorio.getStatus() == null || supervisorio.getStatus().equals(status)) && supervisorio.getIsStatusPendente() != null
					&& !supervisorio.getIsStatusPendente() && supervisorio.getHabilitado()) {
				primeiraDataRealizacaoLeitura = supervisorio.getDataRealizacaoLeitura();
				break;
			}
		}
		return primeiraDataRealizacaoLeitura;
	}

	/**
	 * Verificar data realizacao leitura.
	 * 
	 * @param dataRealizacaoLeitura {@link Date}
	 * @param codigoPontoConsumoSupervisorio {@link codigoPontoConsumoSupervisorio}
	 * @throws GGASException {@link GGASException}
	 */
	public void verificarDataRealizacaoLeitura(Date dataRealizacaoLeitura, String codigoPontoConsumoSupervisorio) throws GGASException {

		Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura = new LinkedHashMap<Long, List<Map<String, Object>>>();
		Boolean possuiContrato = Boolean.FALSE;
		Periodicidade periodicidade = null;

		Map<String, Object> filtroPesquisa = new HashMap<String, Object>();
		filtroPesquisa.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);

		Collection<PontoConsumo> listaPontoConsumo =
				controladorPontoConsumo.consultarPontosConsumo(filtroPesquisa, ROTA_GRUPO_FATURAMENTO_TIPO_LEITURA);
		PontoConsumo pontoConsumo = null;

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		if (pontoConsumo != null) {

			Map<String, Object> mapaContratoAtivo = controladorSupervisorio.preencherMapaComListaReferenciaLeitura(pontoConsumo, null,
					dataRealizacaoLeitura, mapaComListaReferenciaLeitura);

			possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

			// obtém a hora inicial e a periodicidade do contrato se tiver
			if (possuiContrato != null && possuiContrato && mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO) != null) {
				// Recupera a periodicidade de faturamento do contrato ativo
				periodicidade = (Periodicidade) mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO);

			} else {
				// Recupera a periodicidade de faturamento do ramo de atividade se o PC não tiver contrato ativo
				if (pontoConsumo.getRamoAtividade().getPeriodicidade() != null) {
					periodicidade = pontoConsumo.getRamoAtividade().getPeriodicidade();

				} else if (pontoConsumo.getSegmento().getPeriodicidade() != null) {

					// Recupera a periodicidade de faturamento do segmento se o ramo de atividade não for informado
					periodicidade = pontoConsumo.getSegmento().getPeriodicidade();
				}
			}

			Map<String, Object> mapaAnoMesReferenciaCiclo = controladorSupervisorio.obterAnoMesReferenciaCiclo(
					mapaComListaReferenciaLeitura, dataRealizacaoLeitura, pontoConsumo, periodicidade, Boolean.TRUE);

			Date dataFim = (Date) mapaAnoMesReferenciaCiclo.get("dataFim");

			if (dataFim == null) {
				throw new NegocioException(Constantes.ERRO_CRONOGRAMA_DE_FATURAMENTO_NAO_ENCONTRADO, true);
			}

			if (dataFim.compareTo(dataRealizacaoLeitura) < 0) {
				throw new NegocioException(ERRO_MEDICAO_DIARIA_FORA_CONTRATO, true);
			}
		}
	}

	/**
	 * Ordenar supervisori medicao vo por data realizacao leitura.
	 * 
	 * @param listaSupervisorioMedicaoDiariaVO {@link List}
	 * @return listaSupervisorioMedicaoDiariaVO {@link List}
	 */
	public List<SupervisorioMedicaoVO> ordenarSupervisoriMedicaoVOPorDataRealizacaoLeitura(
			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO) {

		// ordena as listas por codigoPontoConsumoSupervisorio e por dataRealizacaoLeitura
		Collections.sort(listaSupervisorioMedicaoDiariaVO, new Comparator<SupervisorioMedicaoVO>() {

			@Override
			public int compare(SupervisorioMedicaoVO supervisorioMedicaoHoraria1, SupervisorioMedicaoVO supervisorioMedicaoHoraria2) {

				// compara os registro por dataRealizacaoLeitura
				return supervisorioMedicaoHoraria1.getDataRealizacaoLeitura()
						.compareTo(supervisorioMedicaoHoraria2.getDataRealizacaoLeitura());
			}
		});

		return listaSupervisorioMedicaoDiariaVO;
	}

	/**
	 * Verificar medicoes duplicadas.
	 * 
	 * @param listaSupervisorioMedicaoVO {@link List}
	 * @param dataRealizacaoLeitura {@link Date}
	 * @param isDiaria {@link Boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void verificarMedicoesDuplicadas(List<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO, Date dataRealizacaoLeitura,
			Boolean isDiaria) throws NegocioException {

		int contador = 0;
		if (dataRealizacaoLeitura != null) {
			for (SupervisorioMedicaoVO medicao : listaSupervisorioMedicaoVO) {
				if (isDiaria) {
					if (medicao.getDataRealizacaoLeitura() != null
							&& DataUtil.converterDataParaString(medicao.getDataRealizacaoLeitura())
									.equals(DataUtil.converterDataParaString(dataRealizacaoLeitura))
							&& Boolean.TRUE.equals(medicao.getHabilitado())) {
						contador++;
					}
				} else {
					if (medicao.getDataRealizacaoLeitura() != null
							&& DataUtil.converterDataParaString(medicao.getDataRealizacaoLeitura(), true)
									.equals(DataUtil.converterDataParaString(dataRealizacaoLeitura, true))
							&& Boolean.TRUE.equals(medicao.getHabilitado())) {
						contador++;
					}
				}
			}
			if (isDiaria) {
				if (contador > 1) {
					throw new NegocioException(ERRO_MEDICAO_DIARIA_DUPLICADA, true);
				}
			} else {
				if (contador > 1) {
					throw new NegocioException(ERRO_MEDICAO_HORARIA_DUPLICADA, true);
				}
			}
		}
	}

	/**
	 * Verificar medicoes diarias duplicadas.
	 * 
	 * @param dataRealizacaoLeitura {@link Date}
	 * @param codigoPontoConsumoSupervisorio {@link String}
	 * @param chavePrimaria {@link Long}
	 * @throws GGASException {@link GGASException}
	 */
	private void verificarMedicoesDiariasDuplicadas(Date dataRealizacaoLeitura, String codigoPontoConsumoSupervisorio, Long chavePrimaria)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
		filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);
		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		String ordenacao = "smd." + DATA_REALIZACAO_LEITURA + " asc" + ", " + "smd." + ULTIMA_ALTERACAO + DESC;

		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria =
				controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, "");
		if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
			throw new NegocioException(ERRO_MEDICAO_DIARIA_DUPLICADA, true);
		}
	}

	/**
	 * Verificar medicoes horarias duplicadas.
	 * 
	 * @param dataRealizacaoLeitura {@link Date}
	 * @param codigoPontoConsumoSupervisorio {@link String}
	 * @param chavePrimaria {@link Long}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	private void verificarMedicoesHorariasDuplicadas(Date dataRealizacaoLeitura, String codigoPontoConsumoSupervisorio, Long chavePrimaria)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
		filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);
		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		filtro.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);
		String ordenacao = "smh." + DATA_REALIZACAO_LEITURA + " asc" + ", " + "smh." + ULTIMA_ALTERACAO + DESC;

		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria =
				controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtro, ordenacao);
		if (listaSupervisorioMedicaoHoraria != null && !listaSupervisorioMedicaoHoraria.isEmpty()) {
			throw new NegocioException(ERRO_MEDICAO_HORARIA_DUPLICADA, true);
		}
	}

	/**
	 * Verificar anormalidade diaria.
	 * 
	 * @param supervisorioMedicaoDiariaNovo {@link SupervisorioMedicaoDiaria}
	 * @param supervisorioMedicaoDiariaVO {@link SupervisorioMedicaoVO}
	 * @param supervisorioMedicaoDiaria {@link SupervisorioMedicaoDiaria}
	 * @param iterator {@link Iterator}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	public void verificarAnormalidadeDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
			SupervisorioMedicaoVO supervisorioMedicaoDiariaVO, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			Iterator<SupervisorioMedicaoVO> iterator) throws GGASException {

		BigDecimal consumoComCorrecaoFatorPTZNovo = supervisorioMedicaoDiariaNovo.getConsumoComCorrecaoFatorPTZ();
		BigDecimal consumoComCorrecaoFatorPTZAntigo = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ();
		Boolean possuiAlteracaoConsumo = Boolean.FALSE;

		if ((consumoComCorrecaoFatorPTZAntigo != null) && (consumoComCorrecaoFatorPTZNovo != null)) {

			consumoComCorrecaoFatorPTZAntigo = consumoComCorrecaoFatorPTZAntigo.setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP);

			if (!(consumoComCorrecaoFatorPTZNovo.compareTo(consumoComCorrecaoFatorPTZAntigo) == 0)) {

				if (supervisorioMedicaoDiariaVO.getSupervisorioMedicaoAnormalidade() != null) {

					Long chaveAnormalidade = supervisorioMedicaoDiariaVO.getSupervisorioMedicaoAnormalidade().getChavePrimaria();

					if ((chaveAnormalidade.equals(Constantes.ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MAIOR_24))
							|| (chaveAnormalidade.equals(Constantes.ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MENOR_24))
							|| (chaveAnormalidade.equals(Constantes.ANORMALIDADE_SUPERVISORIO_REGISTRO_DIARIO_NAO_ENCONTRADO))
							|| (chaveAnormalidade.equals(Constantes.ANORMALIDADE_SUPERVISORIO_VOLUME_DIVERGE))) {

						supervisorioMedicaoDiariaNovo.setSupervisorioMedicaoAnormalidade(controladorSupervisorio
								.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE));

						possuiAlteracaoConsumo = Boolean.TRUE;
					}
				} else {
					supervisorioMedicaoDiariaNovo.setSupervisorioMedicaoAnormalidade(controladorSupervisorio
							.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE));

					possuiAlteracaoConsumo = Boolean.TRUE;
				}
			}

		}

		if (possuiAlteracaoConsumo) {

			iterator.remove();
		}
	}

	/**
	 * Preencher supervisorio medicao diaria.
	 * 
	 * @param supervisorioMedicaoDiaria {@link SupervisorioMedicaoDiaria}
	 * @return supervisorioMedicaoDiariaNovo {@link SupervisorioMedicaoDiaria}
	 * @throws GGASException {@link GGASException}
	 */
	public SupervisorioMedicaoDiaria preencherSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
			throws GGASException {

		SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo =
				(SupervisorioMedicaoDiaria) controladorSupervisorio.criarSupervisorioMedicaoDiaria();

		supervisorioMedicaoDiariaNovo.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio());
		supervisorioMedicaoDiariaNovo.setConsumoComCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ());
		supervisorioMedicaoDiariaNovo.setConsumoSemCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ());

		supervisorioMedicaoDiariaNovo.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ());
		supervisorioMedicaoDiariaNovo.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ());

		supervisorioMedicaoDiariaNovo.setDataReferencia(supervisorioMedicaoDiaria.getDataReferencia());
		supervisorioMedicaoDiariaNovo.setDataRealizacaoLeitura(supervisorioMedicaoDiaria.getDataRealizacaoLeitura());

		supervisorioMedicaoDiariaNovo.setFatorPTZ(supervisorioMedicaoDiaria.getFatorPTZ());
		supervisorioMedicaoDiariaNovo.setFatorZ(supervisorioMedicaoDiaria.getFatorZ());

		supervisorioMedicaoDiariaNovo.setNumeroCiclo(supervisorioMedicaoDiaria.getNumeroCiclo());
		supervisorioMedicaoDiariaNovo.setPressao(supervisorioMedicaoDiaria.getPressao());
		supervisorioMedicaoDiariaNovo.setTemperatura(supervisorioMedicaoDiaria.getTemperatura());

		supervisorioMedicaoDiariaNovo.setIndicadorIntegrado(supervisorioMedicaoDiaria.getIndicadorIntegrado());
		supervisorioMedicaoDiariaNovo.setIndicadorProcessado(supervisorioMedicaoDiaria.getIndicadorProcessado());
		supervisorioMedicaoDiariaNovo.setIndicadorConsolidada(supervisorioMedicaoDiaria.getIndicadorConsolidada());

		supervisorioMedicaoDiariaNovo.setUltimaAlteracao(supervisorioMedicaoDiaria.getUltimaAlteracao());
		supervisorioMedicaoDiariaNovo.setUltimoUsuarioAlteracao(supervisorioMedicaoDiaria.getUltimoUsuarioAlteracao());
		supervisorioMedicaoDiariaNovo.setStatus(supervisorioMedicaoDiaria.getStatus());

		return supervisorioMedicaoDiariaNovo;
	}

	/**
	 * Preencher supervisorio medicao horaria.
	 * 
	 * @param supervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @return supervisorioMedicaoHorariaNovo {@link SupervisorioMedicaoHoraria}
	 * @throws GGASException {@link GGASException}
	 */
	public SupervisorioMedicaoHoraria preencherSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria)
			throws GGASException {

		SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo =
				(SupervisorioMedicaoHoraria) controladorSupervisorio.criarSupervisorioMedicaoHoraria();

		supervisorioMedicaoHorariaNovo.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio());
		supervisorioMedicaoHorariaNovo.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ());
		supervisorioMedicaoHorariaNovo.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ());
		supervisorioMedicaoHorariaNovo.setDataRealizacaoLeitura(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
		supervisorioMedicaoHorariaNovo.setPressao(supervisorioMedicaoHoraria.getPressao());
		supervisorioMedicaoHorariaNovo.setTemperatura(supervisorioMedicaoHoraria.getTemperatura());
		supervisorioMedicaoHorariaNovo.setUltimaAlteracao(supervisorioMedicaoHoraria.getUltimaAlteracao());
		supervisorioMedicaoHorariaNovo.setSupervisorioMedicaoDiaria(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria());
		supervisorioMedicaoHorariaNovo.setIndicadorTipoMedicao(supervisorioMedicaoHoraria.getIndicadorTipoMedicao());
		supervisorioMedicaoHorariaNovo.setIndicadorConsolidada(supervisorioMedicaoHoraria.getIndicadorConsolidada());
		supervisorioMedicaoHorariaNovo.setStatus(supervisorioMedicaoHoraria.getStatus());
		supervisorioMedicaoHorariaNovo.setUltimoUsuarioAlteracao(supervisorioMedicaoHoraria.getUltimoUsuarioAlteracao());
		return supervisorioMedicaoHorariaNovo;
	}

	/**
	 * Preencher supervisorio medicao diaria.
	 * 
	 * @param supervisorioMedicaoDiariaVO {@link SupervisorioMedicaoVO}
	 * @return supervisorioMedicaoDiaria {@link SupervisorioMedicaoDiaria}
	 * @throws GGASException {@link GGASException}
	 */
	public SupervisorioMedicaoDiaria preencherSupervisorioMedicaoDiaria(SupervisorioMedicaoVO supervisorioMedicaoDiariaVO)
			throws GGASException {

		SupervisorioMedicaoDiaria supervisorioMedicaoDiaria =
				(SupervisorioMedicaoDiaria) controladorSupervisorio.criarSupervisorioMedicaoDiaria();

		supervisorioMedicaoDiaria.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiariaVO.getCodigoPontoConsumoSupervisorio());
		if (supervisorioMedicaoDiariaVO.getConsumoComCorrecaoFatorPTZ() != null) {
			supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(supervisorioMedicaoDiariaVO.getConsumoComCorrecaoFatorPTZ());
		} else {
			supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(BigDecimal.ZERO);
		}
		if (supervisorioMedicaoDiariaVO.getConsumoSemCorrecaoFatorPTZ() != null) {
			supervisorioMedicaoDiaria.setConsumoSemCorrecaoFatorPTZ(supervisorioMedicaoDiariaVO.getConsumoSemCorrecaoFatorPTZ());
		} else {
			supervisorioMedicaoDiaria.setConsumoSemCorrecaoFatorPTZ(BigDecimal.ZERO);
		}
		if (supervisorioMedicaoDiariaVO.getLeituraSemCorrecaoFatorPTZ() == null) {
			supervisorioMedicaoDiaria.setLeituraSemCorrecaoFatorPTZ(BigDecimal.ZERO);
		} else {
			supervisorioMedicaoDiaria.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoDiariaVO.getLeituraSemCorrecaoFatorPTZ());
		}
		if (supervisorioMedicaoDiariaVO.getLeituraComCorrecaoFatorPTZ() == null) {
			supervisorioMedicaoDiaria.setLeituraComCorrecaoFatorPTZ(BigDecimal.ZERO);
		} else {
			supervisorioMedicaoDiaria.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoDiariaVO.getLeituraComCorrecaoFatorPTZ());
		}

		supervisorioMedicaoDiaria.setDataReferencia(supervisorioMedicaoDiariaVO.getDataReferencia());
		supervisorioMedicaoDiaria.setDataRealizacaoLeitura(supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura());

		if (supervisorioMedicaoDiariaVO.getFatorPTZ() != null) {
			supervisorioMedicaoDiaria.setFatorPTZ(supervisorioMedicaoDiariaVO.getFatorPTZ());
		} else {
			supervisorioMedicaoDiaria.setFatorPTZ(BigDecimal.ZERO);
		}
		if (supervisorioMedicaoDiariaVO.getFatorZ() != null) {
			supervisorioMedicaoDiaria.setFatorZ(supervisorioMedicaoDiariaVO.getFatorZ());
		} else {
			supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ZERO);
		}

		supervisorioMedicaoDiaria.setNumeroCiclo(supervisorioMedicaoDiariaVO.getNumeroCiclo());
		if (supervisorioMedicaoDiariaVO.getPressao() != null) {
			supervisorioMedicaoDiaria.setPressao(supervisorioMedicaoDiariaVO.getPressao());
		}
		if (supervisorioMedicaoDiariaVO.getTemperatura() != null) {
			supervisorioMedicaoDiaria.setTemperatura(supervisorioMedicaoDiariaVO.getTemperatura());
		}
		supervisorioMedicaoDiaria.setIndicadorIntegrado(supervisorioMedicaoDiariaVO.getIndicadorIntegrado());
		supervisorioMedicaoDiaria.setIndicadorProcessado(supervisorioMedicaoDiariaVO.getIndicadorProcessado());
		supervisorioMedicaoDiaria.setIndicadorConsolidada(supervisorioMedicaoDiariaVO.getIndicadorConsolidada());

		supervisorioMedicaoDiaria.setUltimaAlteracao(supervisorioMedicaoDiariaVO.getUltimaAlteracao());

		supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoDiariaVO.getSupervisorioMedicaoAnormalidade());

		return supervisorioMedicaoDiaria;
	}

	/**
	 * Preencher supervisorio medicao horaria.
	 * 
	 * @param supervisorioMedicaoHorariaVO {@link SupervisorioMedicaoVO}
	 * @return supervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	public SupervisorioMedicaoHoraria preencherSupervisorioMedicaoHoraria(SupervisorioMedicaoVO supervisorioMedicaoHorariaVO)
			throws GGASException {

		SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
				(SupervisorioMedicaoHoraria) controladorSupervisorio.criarSupervisorioMedicaoHoraria();

		supervisorioMedicaoHoraria.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoHorariaVO.getCodigoPontoConsumoSupervisorio());

		supervisorioMedicaoHoraria.setLeituraComCorrecaoFatorPTZ(supervisorioMedicaoHorariaVO.getLeituraComCorrecaoFatorPTZ());
		supervisorioMedicaoHoraria.setLeituraSemCorrecaoFatorPTZ(supervisorioMedicaoHorariaVO.getLeituraSemCorrecaoFatorPTZ());

		supervisorioMedicaoHoraria.setDataRealizacaoLeitura(supervisorioMedicaoHorariaVO.getDataRealizacaoLeitura());

		supervisorioMedicaoHoraria.setPressao(supervisorioMedicaoHorariaVO.getPressao());
		supervisorioMedicaoHoraria.setTemperatura(supervisorioMedicaoHorariaVO.getTemperatura());
		supervisorioMedicaoHoraria.setIndicadorTipoMedicao(Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);
		supervisorioMedicaoHoraria.setIndicadorConsolidada(Boolean.FALSE);
		supervisorioMedicaoHoraria.setDataRegistroLeitura(Calendar.getInstance().getTime());
		return supervisorioMedicaoHoraria;
	}

	/**
	 * Método responsável por preparar os filtros para realizar a pesquisa
	 * das medições do supervisório.
	 * 
	 * @param filtro {@link Map}
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, Model model)
			throws GGASException {

		Boolean ocorrencia = null;
		Boolean analisada = null;
		Boolean transferencia = null;
		Boolean ativo = null;
		Long status = null;
		String anoMesReferenciaLido = null;
		String enderecoRemotoLido = null;
		String dataLeituraInicial = null;
		String dataLeituraFinal = null;
		String dataInclusaoInicial = null;
		String dataInclusaoFinal = null;
		Long idCiclo = null;
		Long[] idsOcorrenciaMedicao = null;

		String cepImovel = validaMedicaoSupervisorioVO.getCepImovel();
		String matriculaImovel = validaMedicaoSupervisorioVO.getMatriculaImovel();
		String indicadorCondominio = validaMedicaoSupervisorioVO.getIndicadorCondominio();
		String nomeImovel = validaMedicaoSupervisorioVO.getNomeImovel();
		String numeroImovel = validaMedicaoSupervisorioVO.getNumeroImovel();
		String complementoImovel = validaMedicaoSupervisorioVO.getComplementoImovel();
		String dataIntegracaoInicial = validaMedicaoSupervisorioVO.getDataIntegracaoInicial();
		String dataIntegracaoFinal = validaMedicaoSupervisorioVO.getDataIntegracaoFinal();
		String indicadorPesquisa = validaMedicaoSupervisorioVO.getIndicadorPesquisa();
		Long idCliente = validaMedicaoSupervisorioVO.getIdCliente();
		Long idLocalidade = validaMedicaoSupervisorioVO.getIdLocalidade();
		Long idSetorComercial = validaMedicaoSupervisorioVO.getIdSetorComercial();
		Long idGrupoFaturamento = validaMedicaoSupervisorioVO.getIdGrupoFaturamento();
		Long[] idsRota = validaMedicaoSupervisorioVO.getIdsRota();
		Date fimLeitura = null;
		Date fimInclusao = null;

		if (PONTO_CONSUMO.equals(validaMedicaoSupervisorioVO.getIndicadorPesquisa())) {

			ocorrencia = validaMedicaoSupervisorioVO.getOcorrenciaPC();
			analisada = validaMedicaoSupervisorioVO.getAnalisadaPC();
			transferencia = validaMedicaoSupervisorioVO.getTransferenciaPC();
			ativo = validaMedicaoSupervisorioVO.getAtivoPC();
			status = validaMedicaoSupervisorioVO.getStatusPC();

			anoMesReferenciaLido = validaMedicaoSupervisorioVO.getAnoMesReferenciaLidoPC();
			enderecoRemotoLido = validaMedicaoSupervisorioVO.getEnderecoRemotoLidoPC();
			dataLeituraInicial = validaMedicaoSupervisorioVO.getDataLeituraInicialPC();
			dataLeituraFinal = validaMedicaoSupervisorioVO.getDataLeituraFinalPC();
			dataInclusaoInicial = validaMedicaoSupervisorioVO.getDataInclusaoInicialPC();
			dataInclusaoFinal = validaMedicaoSupervisorioVO.getDataInclusaoFinalPC();

			idCiclo = validaMedicaoSupervisorioVO.getIdCicloPC();
			idsOcorrenciaMedicao = validaMedicaoSupervisorioVO.getIdsOcorrenciaMedicaoPC();

		} else {

			ocorrencia = validaMedicaoSupervisorioVO.getOcorrenciaMI();
			analisada = validaMedicaoSupervisorioVO.getAnalisadaMI();
			transferencia = validaMedicaoSupervisorioVO.getTransferenciaMI();
			ativo = validaMedicaoSupervisorioVO.getAtivoMI();
			status = validaMedicaoSupervisorioVO.getStatusMI();

			anoMesReferenciaLido = validaMedicaoSupervisorioVO.getAnoMesReferenciaLidoMI();
			enderecoRemotoLido = validaMedicaoSupervisorioVO.getEnderecoRemotoLidoMI();
			dataLeituraInicial = validaMedicaoSupervisorioVO.getDataLeituraInicialMI();
			dataLeituraFinal = validaMedicaoSupervisorioVO.getDataLeituraFinalMI();
			dataInclusaoInicial = validaMedicaoSupervisorioVO.getDataInclusaoInicialMI();
			dataInclusaoFinal = validaMedicaoSupervisorioVO.getDataInclusaoFinalMI();

			idCiclo = validaMedicaoSupervisorioVO.getIdCicloMI();
			idsOcorrenciaMedicao = validaMedicaoSupervisorioVO.getIdsOcorrenciaMedicaoMI();

		}

		filtro.put(INDICADOR_CONSOLIDADA, Boolean.TRUE);

		if (ocorrencia != null) {
			filtro.put(OCORRENCIA, ocorrencia);
		}
		if (analisada != null) {
			filtro.put(ANALISADA, analisada);
		}
		if (transferencia != null) {
			filtro.put(TRANSFERENCIA, transferencia);
		}
		if (ativo != null) {
			filtro.put(ATIVO, ativo);
		}
		if (status != null && status > 0) {
			filtro.put(STATUS, status);
		}
		anoMesReferenciaLido = anoMesReferenciaLido.replaceAll("/", "").replaceAll("-", "");
		if ("".equals(anoMesReferenciaLido)) {
			anoMesReferenciaLido = "0";
		}
		if (StringUtils.isNotEmpty(enderecoRemotoLido)) {
			filtro.put(ENDERECO_REMOTO_LIDO, enderecoRemotoLido);
		}

		if (!StringUtils.isEmpty(dataLeituraInicial)) {
			Date inicioLeitura = Util.converterCampoStringParaData("Data Leitura Inicial", dataLeituraInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_LEITURA_INICIAL, inicioLeitura);

			Date dataAtual = Calendar.getInstance().getTime();
			if (DataUtil.gerarDataHmsZerados(inicioLeitura).compareTo(DataUtil.gerarDataHmsZerados(dataAtual)) <= 0
					&& dataLeituraFinal == null || StringUtils.isEmpty(dataLeituraFinal)) {
				fimLeitura = dataAtual;
				filtro.put(DATA_LEITURA_FINAL, fimLeitura);
				model.addAttribute("dtFinal", DataUtil.converterDataParaString(dataAtual));
			}
		}

		if (fimLeitura == null && !StringUtils.isEmpty(dataLeituraFinal)) {
			filtro.put(DATA_LEITURA_FINAL,
					Util.converterCampoStringParaData("Data Leitura Final", dataLeituraFinal, Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(dataInclusaoInicial)) {
			Date inicioInclusao =
					Util.converterCampoStringParaData("Data Inclusão Inicial", dataInclusaoInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_INCLUSAO_INICIAL, inicioInclusao);

			Date dataAtual = Calendar.getInstance().getTime();
			if (DataUtil.gerarDataHmsZerados(inicioInclusao).compareTo(DataUtil.gerarDataHmsZerados(dataAtual)) <= 0
					&& dataInclusaoFinal == null || StringUtils.isEmpty(dataInclusaoFinal)) {
				fimInclusao = dataAtual;
				filtro.put(DATA_INCLUSAO_FINAL, fimInclusao);
				model.addAttribute("dtFinal", DataUtil.converterDataParaString(dataAtual));
			}
		}
		if ((fimInclusao == null) && (!StringUtils.isEmpty(dataInclusaoFinal))) {
			filtro.put(DATA_INCLUSAO_FINAL,
					Util.converterCampoStringParaData("Data Inclusão Final", dataInclusaoFinal, Constantes.FORMATO_DATA_BR));
		}

		if ((idCiclo != null) && (idCiclo > 0)) {
			filtro.put(ID_CICLO, idCiclo);
		}
		if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > 0) {
			filtro.put(IDS_OCORRENCIA_MEDICAO, idsOcorrenciaMedicao);
		}

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}
		if (StringUtils.isNotEmpty(cepImovel)) {
			filtro.put(CEP_IMOVEL, cepImovel);
		}
		if (StringUtils.isNotEmpty(matriculaImovel)) {
			controladorImovel.existeImovelPorMatricula(Util.converterCampoStringParaValorLong("Matrícula do imóvel", matriculaImovel));
			filtro.put(MATRICULA_IMOVEL, Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matriculaImovel));
		}
		if (StringUtils.isNotEmpty(indicadorCondominio)) {
			filtro.put(INDICADOR_CONDOMINIO, Boolean.valueOf(indicadorCondominio));
		}

		if (StringUtils.isNotEmpty(nomeImovel)) {
			filtro.put(NOME_IMOVEL, nomeImovel);
		}

		if (StringUtils.isNotEmpty(numeroImovel)) {
			filtro.put(NUMERO_IMOVEL, numeroImovel);
		}

		if (StringUtils.isNotEmpty(complementoImovel)) {
			filtro.put(COMPLEMENTO_IMOVEL, Util.formatarTextoConsulta(complementoImovel));
		}

		if (idLocalidade != null && idLocalidade > 0) {
			filtro.put(ID_LOCALIDADE, idLocalidade);
		}
		if (idSetorComercial != null && idSetorComercial > 0) {
			filtro.put(ID_SETOR_COMERCIAL, idSetorComercial);
		}
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
		}
		if (idsRota != null && idsRota.length > 0) {
			filtro.put(IDS_ROTA, idsRota);
		}

		Long anoMesReferenciaLidoNumber = Long.valueOf(anoMesReferenciaLido);
		if (anoMesReferenciaLidoNumber != null && anoMesReferenciaLidoNumber > 0) {
			filtro.put(ANO_MES_REFERENCIA_LIDO, anoMesReferenciaLidoNumber);
		}

		if (StringUtils.isNotEmpty(dataIntegracaoInicial)) {
			Date inicioIntegracao =
					Util.converterCampoStringParaData("Data Integracação Inicial", dataIntegracaoInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_INTEGRACAO_INICIAL, inicioIntegracao);
		}

		if (StringUtils.isNotEmpty(dataIntegracaoFinal)) {
			Date finalIntegracao =
					Util.converterCampoStringParaData("Data Integracação Final", dataIntegracaoFinal, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_INTEGRACAO_FINAL, finalIntegracao);
		}

		if (StringUtils.isNotEmpty(indicadorPesquisa)) {
			filtro.put(INDICADOR_PESQUISA, indicadorPesquisa);
		} else {
			filtro.put(INDICADOR_PESQUISA, PONTO_CONSUMO);
		}

	}

	/**
	 * Método responsável por carregar os valores default dos campos da tela de pesquisa.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCampos(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, Model model) throws GGASException {

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		model.addAttribute("listaLocalidade", controladorLocalidade.listarLocalidades());
		model.addAttribute("listaSetorComercial", controladorSetorComercial.listarSetoresComerciais());
		model.addAttribute("listaGrupoFaturamento", controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaOcorrenciaMedicao", controladorSupervisorio
				.listarSupervisorioMedicaoAnormalidade(String.valueOf(validaMedicaoSupervisorioVO.getOcorrenciaPC())));
		model.addAttribute("listaCiclo", controladorCronogramaFaturamento.listarCiclosCronograma());

		if (validaMedicaoSupervisorioVO.getIdGrupoFaturamento() != null && validaMedicaoSupervisorioVO.getIdGrupoFaturamento() > 0) {
			model.addAttribute("listaRota",
					controladorRota.listarRotasPorGrupoFaturamento(validaMedicaoSupervisorioVO.getIdGrupoFaturamento()));
		}

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
		model.addAttribute("statusNaoAutorizado", Long.valueOf(statusNaoAutorizado));
		model.addAttribute("statusPendente", Long.valueOf(statusPendente));

	}

	/**
	 * Autorizar supervisorio medicao diaria.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioDiario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("autorizarSupervisorioMedicaoDiaria")
	public String autorizarSupervisorioMedicaoDiaria(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);
		Long chavePrimaria = validaMedicaoSupervisorioVO.getChavePrimaria();

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);
		List<Long> registrosIndicadorProcessado = (List<Long>) sessao.getAttribute(REGISTROS_INDICADOR_PROCESSADO);

		try {

			String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);

			Collection<PontoConsumo> listaPontoConsumo =
					controladorPontoConsumo.consultarPontosConsumo(filtro, SITUACAO_CONSUMO, SEGMENTO, IMOVEL);

			PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();

			if (pontoConsumo != null) {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				model.addAttribute(CLIENTE_PRINCIPAL, controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));
			}

			String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
			EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusAutorizada));

			if (status != null) {
				SupervisorioMedicaoDiaria supervisorioMedicaoDiaria =
						controladorSupervisorio.obterSupervisorioMedicaoDiaria(chavePrimaria, STATUS);

				if (supervisorioMedicaoDiaria != null) {
					supervisorioMedicaoDiaria.setStatus(status);
					supervisorioMedicaoDiaria.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
					supervisorioMedicaoDiaria.setDadosAuditoria(getDadosAuditoria(request));
					supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.FALSE);

					controladorSupervisorio.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
					controladorSupervisorio.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);

				}

				// chama o processo de consolidar dados do supervisório
				SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoHoraria =
						Util.primeiroElemento(listaSupervisorioMedicaoDiariaVO);
				SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoHoraria =
						listaSupervisorioMedicaoDiariaVO.get(listaSupervisorioMedicaoDiariaVO.size() - 1);

				Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
				Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();

				this.tratarMedicoesDiarias(dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio);

				for (Iterator<SupervisorioMedicaoVO> iterator = listaSupervisorioMedicaoDiariaVO.iterator(); iterator.hasNext();) {
					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO = iterator.next();

					if (chavePrimaria.equals(supervisorioMedicaoDiariaVO.getChavePrimaria())) {
						if (registrosIndicadorProcessado.contains(chavePrimaria)) {
							registrosIndicadorProcessado.remove(chavePrimaria);
						}
						supervisorioMedicaoDiaria = controladorSupervisorio.obterSupervisorioMedicaoDiaria(chavePrimaria, STATUS,
								SUPERVISORIO_MEDICAO_ANORMALIDADE);

						SupervisorioMedicaoVO supervisorioMedicaoDiariaVOAux =
								this.preencherSupervisorioMedicaoDiariaVO(supervisorioMedicaoDiaria);

						supervisorioMedicaoDiariaVOAux.setChavePrimaria(supervisorioMedicaoDiaria.getChavePrimaria());
						supervisorioMedicaoDiariaVOAux.setHabilitado(Boolean.TRUE);
						supervisorioMedicaoDiariaVOAux.setStatus(status);
						supervisorioMedicaoDiariaVOAux.setIsStatusPendente(Boolean.FALSE);
						supervisorioMedicaoDiariaVOAux.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());

						listaSupervisorioMedicaoDiariaVO.add(supervisorioMedicaoDiariaVOAux);
						listaSupervisorioMedicaoDiariaVO.remove(supervisorioMedicaoDiariaVO);

						break;
					}
				}

				this.ordenarListaSupervisorioMedicaoDiariaVO(listaSupervisorioMedicaoDiariaVO);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, SupervisorioMedicaoDiaria.SUPERVISORIO_MEDICAO_DIARIA);
			}

			sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoMedicaoSupervisorioDiario(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Nao autorizar supervisorio medicao diaria.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavePrimaria {@link Long}
	 * @param dataRealizacaoLeitura {@link String}
	 * @param anoMesReferencia {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioDiario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping("naoAutorizarSupervisorioMedicaoDiaria")
	public String naoAutorizarSupervisorioMedicaoDiaria(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, @RequestParam(value = "CHAVE_PRIMARIA", required = false) Long chavePrimaria,
			@RequestParam(value = "DATA_REALIZACAO_LEITURA", required = false) String dataRealizacaoLeitura,
			@RequestParam(value = "ANO_MES_REFERENCIA_PARAMETRO", required = false) String anoMesReferencia, HttpServletRequest request,
			Model model) throws GGASException {

		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo statusAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusAutorizada));

		String paramStatusNaoAutorizada =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		EntidadeConteudo statusNaoAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusNaoAutorizada));

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusPendente));

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		String codigoPontoConsumoSupervisorio = validaMedicaoSupervisorioVO.getEnderecoRemotoParametro();
		Long idCiclo = validaMedicaoSupervisorioVO.getIdCicloParametro();

		if ("".equals(anoMesReferencia)) {
			anoMesReferencia = "0";
		}
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);

		try {

			Collection<PontoConsumo> listaPontoConsumo =
					controladorPontoConsumo.consultarPontosConsumo(filtro, SITUACAO_CONSUMO, SEGMENTO, IMOVEL);

			PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();

			if (pontoConsumo != null) {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				model.addAttribute(CLIENTE_PRINCIPAL, controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));
			}

			// desabilita o registro pendente e atualiza o status para não autorizado
			SupervisorioMedicaoDiaria supervisorioMedicaoDiariaPendente =
					controladorSupervisorio.obterSupervisorioMedicaoDiaria(chavePrimaria);
			supervisorioMedicaoDiariaPendente.setStatus(statusNaoAutorizada);
			supervisorioMedicaoDiariaPendente.setHabilitado(Boolean.FALSE);
			supervisorioMedicaoDiariaPendente.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
			supervisorioMedicaoDiariaPendente.setDadosAuditoria(getDadosAuditoria(request));
			controladorSupervisorio.atualizar(supervisorioMedicaoDiariaPendente, SupervisorioMedicaoDiariaImpl.class);

			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO =
					(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO);

			filtro = new HashMap<String, Object>();

			if (statusAutorizada != null) {

				filtro.put(STATUS, statusAutorizada.getChavePrimaria());
			}

			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			filtro.put(DATA_REALIZACAO_LEITURA,
					Util.converterCampoStringParaData("Data", dataRealizacaoLeitura, Constantes.FORMATO_DATA_BR));
			filtro.put("codigoStatusAlcadaDiferente", statusNaoAutorizada.getChavePrimaria());
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
			if (anoMesReferencia != null && Long.parseLong(anoMesReferencia) > 0) {
				filtro.put(ANO_MES_REFERENCIA_LIDO, Long.valueOf(anoMesReferencia));
			} else {
				filtro.put("isReferenciaNull", Boolean.TRUE);
			}
			if ((idCiclo != null) && (idCiclo > 0)) {
				filtro.put(ID_CICLO, idCiclo);
			} else {
				filtro.put("isCicloNull", Boolean.TRUE);
			}

			String ordenacao = "ultimaAlteracao desc";

			// FIXME: Substituir por consulta à tabela pela classe
			ParametroSistema parametroTabelaSuperMedicaoDiaria =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_DIARIA);

			// consulta no banco se existe algum resgitro anterior com status autorizado
			Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = controladorSupervisorio
					.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, STATUS, SUPERVISORIO_MEDICAO_ANORMALIDADE);

			if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {

				SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAntigoAutorizado = listaSupervisorioMedicaoDiaria.iterator().next();
				SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo = null;

				if (statusPendente.equals(supervisorioMedicaoDiariaAntigoAutorizado.getStatus())) {
					supervisorioMedicaoDiariaNovo = supervisorioMedicaoDiariaAntigoAutorizado;

				} else {
					supervisorioMedicaoDiariaNovo = this.preencherSupervisorioMedicaoDiaria(supervisorioMedicaoDiariaAntigoAutorizado);
				}

				supervisorioMedicaoDiariaNovo.setHabilitado(Boolean.TRUE);
				supervisorioMedicaoDiariaNovo.setDadosAuditoria(getDadosAuditoria(request));
				supervisorioMedicaoDiariaNovo.setDataRegistroLeitura(Calendar.getInstance().getTime());
				supervisorioMedicaoDiariaNovo.setIndicadorConsolidada(Boolean.FALSE);

				Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario =
						this.pesquisarSupervisorioMedicaoComentario(null, supervisorioMedicaoDiariaAntigoAutorizado.getChavePrimaria());

				SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo = this.criarNovoSupervisorioMedicaoComentario(request, null,
						supervisorioMedicaoDiariaNovo, Integer.valueOf(1), model);

				if (statusPendente.equals(supervisorioMedicaoDiariaAntigoAutorizado.getStatus())) {
					controladorSupervisorio.atualizar(supervisorioMedicaoDiariaNovo, SupervisorioMedicaoDiariaImpl.class);
				} else {
					controladorSupervisorio.inserirSupervisorioMedicaoDiaria(getDadosAuditoria(request), supervisorioMedicaoDiariaNovo,
							parametroTabelaSuperMedicaoDiaria, supervisorioMedicaoComentarioNovo, listaSupervisorioMedicaoComentario,
							Boolean.TRUE, null, Boolean.FALSE);
				}

				// Desabilita todos os registro horários que estão relacionados o registro diário que foi alterado e
				// atualiza os registros horários com o novo registro diário
				Map<String, Object> filtroHoraria = new HashMap<String, Object>();
				filtroHoraria.put(SUPERVISORIO_MEDICAO_DIARIA, supervisorioMedicaoDiariaPendente.getChavePrimaria());
				filtroHoraria.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);
				filtroHoraria.put(ATIVO, Boolean.TRUE);
				controladorSupervisorio.atualizarSupervisorioMedicaoHoraria(filtroHoraria, supervisorioMedicaoDiariaNovo);

				SupervisorioMedicaoVO supervisorioMedicaoDiariaVONovo = null;
				if (statusPendente.equals(supervisorioMedicaoDiariaAntigoAutorizado.getStatus())) {
					for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoDiariaVO) {
						if (supervisorioMedicaoDiariaAntigoAutorizado.getChavePrimaria() == supervisorioMedicaoVO.getChavePrimaria()) {
							supervisorioMedicaoDiariaVONovo = supervisorioMedicaoVO;
							break;
						}
					}
				}

				// chama o processo de consolidar dados do supervisório
				SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoHoraria =
						Util.primeiroElemento(listaSupervisorioMedicaoDiariaVO);
				SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoHoraria =
						listaSupervisorioMedicaoDiariaVO.get(listaSupervisorioMedicaoDiariaVO.size() - 1);

				Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
				Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();

				this.tratarMedicoesDiarias(dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio);

				if (supervisorioMedicaoDiariaVONovo == null) {

					supervisorioMedicaoDiariaNovo = controladorSupervisorio.obterSupervisorioMedicaoDiaria(
							supervisorioMedicaoDiariaNovo.getChavePrimaria(), STATUS, SUPERVISORIO_MEDICAO_ANORMALIDADE);

					supervisorioMedicaoDiariaVONovo = this.preencherSupervisorioMedicaoDiariaVO(supervisorioMedicaoDiariaNovo);

				}
				supervisorioMedicaoDiariaVONovo.setHabilitado(Boolean.TRUE);
				supervisorioMedicaoDiariaVONovo.setStatus(supervisorioMedicaoDiariaNovo.getStatus());
				supervisorioMedicaoDiariaVONovo.setIsStatusPendente(statusPendente.equals(supervisorioMedicaoDiariaNovo.getStatus()));
				supervisorioMedicaoDiariaVONovo
						.setPossuiAutorizacaoAlcada(statusPendente.equals(supervisorioMedicaoDiariaNovo.getStatus()));

				supervisorioMedicaoDiariaVONovo.setUltimoUsuarioAlteracao(supervisorioMedicaoDiariaNovo.getUltimoUsuarioAlteracao());
				supervisorioMedicaoDiariaVONovo.setChavePrimaria(supervisorioMedicaoDiariaNovo.getChavePrimaria());

				if (!statusPendente.equals(supervisorioMedicaoDiariaAntigoAutorizado.getStatus())) {
					listaSupervisorioMedicaoDiariaVO.add(supervisorioMedicaoDiariaVONovo);
				}
				for (SupervisorioMedicaoVO supervisorioMedicaoDiariaVO : listaSupervisorioMedicaoDiariaVO) {
					if (supervisorioMedicaoDiariaVO.getChavePrimaria().equals(chavePrimaria)) {
						supervisorioMedicaoDiariaVO.setStatus(statusNaoAutorizada);
						supervisorioMedicaoDiariaVO.setIsStatusPendente(Boolean.FALSE);
						supervisorioMedicaoDiariaVO.setHabilitado(Boolean.FALSE);
						supervisorioMedicaoDiariaVO.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
						List<Long> registrosIndicadorProcessado = (List<Long>) sessao.getAttribute(REGISTROS_INDICADOR_PROCESSADO);

						verificaChavePrimariaExistente(chavePrimaria, registrosIndicadorProcessado);
						sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO, registrosIndicadorProcessado);
						break;
					}
				}

				this.ordenarListaSupervisorioMedicaoDiariaVO(listaSupervisorioMedicaoDiariaVO);

			} else {
				for (SupervisorioMedicaoVO supervisorioMedicaoDiariaVO : listaSupervisorioMedicaoDiariaVO) {
					if (supervisorioMedicaoDiariaVO.getChavePrimaria().equals(chavePrimaria)) {
						supervisorioMedicaoDiariaVO.setStatus(statusNaoAutorizada);
						supervisorioMedicaoDiariaVO.setIsStatusPendente(Boolean.FALSE);
						supervisorioMedicaoDiariaVO.setHabilitado(Boolean.FALSE);
						supervisorioMedicaoDiariaVO.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
						List<Long> registrosIndicadorProcessado = (List<Long>) sessao.getAttribute(REGISTROS_INDICADOR_PROCESSADO);

						verificaChavePrimariaExistente(chavePrimaria, registrosIndicadorProcessado);
						sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO, registrosIndicadorProcessado);
						break;
					}
				}
			}

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_DIARIA_VO, listaSupervisorioMedicaoDiariaVO);
			sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, SupervisorioMedicaoDiaria.SUPERVISORIO_MEDICAO_DIARIA);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoMedicaoSupervisorioDiario(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}

	/**
	 * Ordenar lista supervisorio medicao diaria vo.
	 * 
	 * @param listaSupervisorioMedicaoDiariaVO {@link Collection}
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private void ordenarListaSupervisorioMedicaoDiariaVO(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO) {

		Collections.sort((List) listaSupervisorioMedicaoDiariaVO, new Comparator<SupervisorioMedicaoVO>() {

			@Override
			public int compare(SupervisorioMedicaoVO supervisorioMedicaoVO1, SupervisorioMedicaoVO supervisorioMedicaoVO2) {

				int resultado =
						supervisorioMedicaoVO1.getDataRealizacaoLeitura().compareTo(supervisorioMedicaoVO2.getDataRealizacaoLeitura());

				if (resultado == 0) {

					resultado = supervisorioMedicaoVO2.getUltimaAlteracao().compareTo(supervisorioMedicaoVO1.getUltimaAlteracao());

				}

				return resultado;
			}
		});
	}

	/**
	 * Ordenar lista supervisorio medicao horaria vo.
	 * 
	 * @param listaSupervisorioMedicaoHorariaVO {@link Collection}
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private void ordenarListaSupervisorioMedicaoHorariaVO(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO) {

		Collections.sort((List) listaSupervisorioMedicaoHorariaVO, new Comparator<SupervisorioMedicaoVO>() {

			@Override
			public int compare(SupervisorioMedicaoVO supervisorioMedicaoVO1, SupervisorioMedicaoVO supervisorioMedicaoVO2) {

				int resultado =
						supervisorioMedicaoVO1.getDataRealizacaoLeitura().compareTo(supervisorioMedicaoVO2.getDataRealizacaoLeitura());

				if (resultado == 0 && supervisorioMedicaoVO2.getUltimaAlteracao() != null
						&& supervisorioMedicaoVO1.getUltimaAlteracao() != null) {

					resultado = supervisorioMedicaoVO2.getUltimaAlteracao().compareTo(supervisorioMedicaoVO1.getUltimaAlteracao());

				}

				return resultado;
			}
		});
	}

	/**
	 * Autorizar supervisorio medicao horaria.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavePrimariaDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioHorario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("autorizarSupervisorioMedicaoHoraria")
	public String autorizarSupervisorioMedicaoHoraria(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long chavePrimariaDiaria, HttpServletRequest request,
			Model model) throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
				(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);

		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusAutorizada));

		try {

			if (status != null) {
				SupervisorioMedicaoHoraria supervisorioMedicaoHoraria =
						controladorSupervisorio.obterSupervisorioMedicaoHoraria(validaMedicaoSupervisorioVO.getChavePrimaria(), STATUS);

				if (supervisorioMedicaoHoraria != null) {
					supervisorioMedicaoHoraria.setStatus(status);
					supervisorioMedicaoHoraria.setIndicadorConsolidada(Boolean.FALSE);

					supervisorioMedicaoHoraria.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
					supervisorioMedicaoHoraria.setDadosAuditoria(getDadosAuditoria(request));

					controladorSupervisorio.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
					controladorSupervisorio.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);

				}

				// chama o processo de consolidar dados do supervisório
				SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoHoraria =
						Util.primeiroElemento(listaSupervisorioMedicaoHorariaVO);
				SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoHoraria =
						listaSupervisorioMedicaoHorariaVO.get(listaSupervisorioMedicaoHorariaVO.size() - 1);

				Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
				Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
				String codigoPontoConsumoSupervisorio = primeiroRegistroListaSupervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio();

				this.tratarMedicoesHorarias(dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio);

				for (Iterator<SupervisorioMedicaoVO> iterator = listaSupervisorioMedicaoHorariaVO.iterator(); iterator.hasNext();) {
					SupervisorioMedicaoVO supervisorioMedicaoHorariaVO = iterator.next();

					if (validaMedicaoSupervisorioVO.getChavePrimaria().equals(supervisorioMedicaoHorariaVO.getChavePrimaria())) {

						supervisorioMedicaoHoraria = controladorSupervisorio.obterSupervisorioMedicaoHoraria(
								validaMedicaoSupervisorioVO.getChavePrimaria(), STATUS, SUPERVISORIO_MEDICAO_ANORMALIDADE);

						SupervisorioMedicaoVO supervisorioMedicaoHorariaVOAux =
								this.preencherSupervisorioMedicaoHorariaVO(supervisorioMedicaoHoraria);

						supervisorioMedicaoHorariaVOAux.setHabilitado(Boolean.TRUE);
						supervisorioMedicaoHorariaVOAux.setStatus(status);
						supervisorioMedicaoHorariaVOAux.setIsStatusPendente(Boolean.FALSE);
						supervisorioMedicaoHorariaVOAux.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());

						listaSupervisorioMedicaoHorariaVO.add(supervisorioMedicaoHorariaVOAux);
						listaSupervisorioMedicaoHorariaVO.remove(supervisorioMedicaoHorariaVO);

						break;
					}

				}

				this.ordenarListaSupervisorioMedicaoHorariaVO(listaSupervisorioMedicaoHorariaVO);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, SupervisorioMedicaoHoraria.SUPERVISORIO_MEDICAO_HORARIA);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoMedicaoSupervisorioHorario(validaMedicaoSupervisorioVO, bindingResult, chavePrimariaDiaria, request,
				model);
	}

	/**
	 * Nao autorizar supervisorio medicao horaria.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavePrimariaDiaria {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoMedicaoSupervisorioHorario {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	public String naoAutorizarSupervisorioMedicaoHoraria(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO,
			BindingResult bindingResult, @RequestParam(value = CHAVE_PRIMARIA_DIARIA, required = false) Long chavePrimariaDiaria,
			HttpServletRequest request, Model model) throws GGASException {

		Long chavePrimaria = validaMedicaoSupervisorioVO.getChavePrimaria();

		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo statusAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusAutorizada));

		String paramStatusNaoAutorizada =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		EntidadeConteudo statusNaoAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusNaoAutorizada));

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(paramStatusPendente));

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		String dataRealizacaoLeitura = validaMedicaoSupervisorioVO.getDataRealizacaoLeitura();
		String horaRealizacaoLeitura = validaMedicaoSupervisorioVO.getHoraRealizacaoLeitura();

		try {

			// desabilita o registro pendente e atualiza o status para não autorizado
			SupervisorioMedicaoHoraria supervisorioMedicaoHorariaPendente =
					controladorSupervisorio.obterSupervisorioMedicaoHoraria(chavePrimaria);
			supervisorioMedicaoHorariaPendente.setStatus(statusNaoAutorizada);
			supervisorioMedicaoHorariaPendente.setHabilitado(Boolean.FALSE);
			supervisorioMedicaoHorariaPendente.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
			supervisorioMedicaoHorariaPendente.setDadosAuditoria(getDadosAuditoria(request));
			controladorSupervisorio.atualizar(supervisorioMedicaoHorariaPendente, SupervisorioMedicaoHorariaImpl.class);

			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
					(List<SupervisorioMedicaoVO>) sessao.getAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO);

			Map<String, Object> filtro = new HashMap<String, Object>();

			if (statusAutorizada != null) {

				filtro.put(STATUS_AUTORIZADO, statusAutorizada.getChavePrimaria());
			}

			filtro.put(DATA_REALIZACAO_LEITURA, Util.converterCampoStringParaData("Data",
					dataRealizacaoLeitura + " " + horaRealizacaoLeitura, Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR));
			filtro.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaDiaria);
			filtro.put("codigoStatusAlcadaDiferente", statusNaoAutorizada.getChavePrimaria());
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);

			String ordenacao = "ultimaAlteracao desc";

			ParametroSistema parametroTabelaSuperMedicaoHoraria =
					controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_HORARIA);

			// consulta no banco se existe algum resgitro anterior com status autorizado
			Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = controladorSupervisorio
					.consultarSupervisorioMedicaoHoraria(filtro, ordenacao, STATUS, SUPERVISORIO_MEDICAO_ANORMALIDADE);

			iteraListaSupervisorioMedicaoHoraria(request, model, chavePrimaria, statusNaoAutorizada, statusPendente, sessao,
					listaSupervisorioMedicaoHorariaVO, parametroTabelaSuperMedicaoHoraria, listaSupervisorioMedicaoHoraria);

			sessao.setAttribute(LISTA_SUPERVISORIO_MEDICAO_HORARIA_VO, listaSupervisorioMedicaoHorariaVO);
			sessao.setAttribute(REGISTROS_SALVOS, new ArrayList<Long>());

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, SupervisorioMedicaoHoraria.SUPERVISORIO_MEDICAO_HORARIA);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoMedicaoSupervisorioHorario(validaMedicaoSupervisorioVO, bindingResult, chavePrimariaDiaria, request,
				model);
	}

	private void iteraListaSupervisorioMedicaoHoraria(HttpServletRequest request, Model model, Long chavePrimaria,
			EntidadeConteudo statusNaoAutorizada, EntidadeConteudo statusPendente, HttpSession sessao,
			List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO, ParametroSistema parametroTabelaSuperMedicaoHoraria,
			Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria) throws GGASException {
		if (listaSupervisorioMedicaoHoraria != null && !listaSupervisorioMedicaoHoraria.isEmpty()) {

			SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAntigaAutorizada = listaSupervisorioMedicaoHoraria.iterator().next();
			SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo = null;

			if (statusPendente.equals(supervisorioMedicaoHorariaAntigaAutorizada.getStatus())) {
				supervisorioMedicaoHorariaNovo = supervisorioMedicaoHorariaAntigaAutorizada;
			} else {
				supervisorioMedicaoHorariaNovo = this.preencherSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaAntigaAutorizada);
			}

			supervisorioMedicaoHorariaNovo.setHabilitado(Boolean.TRUE);
			supervisorioMedicaoHorariaNovo.setDadosAuditoria(getDadosAuditoria(request));
			supervisorioMedicaoHorariaNovo.setDataRegistroLeitura(Calendar.getInstance().getTime());
			supervisorioMedicaoHorariaNovo.setIndicadorConsolidada(Boolean.FALSE);

			Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario =
					this.pesquisarSupervisorioMedicaoComentario(null, supervisorioMedicaoHorariaAntigaAutorizada.getChavePrimaria());

			SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo =
					this.criarNovoSupervisorioMedicaoComentario(request, supervisorioMedicaoHorariaNovo,
							supervisorioMedicaoHorariaNovo.getSupervisorioMedicaoDiaria(), Integer.valueOf(1), model);

			if (statusPendente.equals(supervisorioMedicaoHorariaAntigaAutorizada.getStatus())) {
				controladorSupervisorio.atualizar(supervisorioMedicaoHorariaNovo, SupervisorioMedicaoHorariaImpl.class);
			} else {
				controladorSupervisorio.inserirSupervisorioMedicaoHoraria(getDadosAuditoria(request), supervisorioMedicaoHorariaNovo,
						parametroTabelaSuperMedicaoHoraria, supervisorioMedicaoComentarioNovo, listaSupervisorioMedicaoComentario,
						Boolean.TRUE, null, Boolean.FALSE);
			}

			// chama o processo de consolidar dados do supervisório
			SupervisorioMedicaoVO primeiroRegistroListaSupervisorioMedicaoHoraria =
					Util.primeiroElemento(listaSupervisorioMedicaoHorariaVO);
			SupervisorioMedicaoVO ultimoRegistroListaSupervisorioMedicaoHoraria =
					listaSupervisorioMedicaoHorariaVO.get(listaSupervisorioMedicaoHorariaVO.size() - 1);

			Date dataInicialRealizacaoLeitura = primeiroRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
			Date dataFinalRealizacaoLeitura = ultimoRegistroListaSupervisorioMedicaoHoraria.getDataRealizacaoLeitura();
			String codigoPontoConsumoSupervisorio = primeiroRegistroListaSupervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio();

			this.tratarMedicoesHorarias(dataInicialRealizacaoLeitura, dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio);

			SupervisorioMedicaoVO supervisorioMedicaoHorariaVONovo = null;
			if (statusPendente.equals(supervisorioMedicaoHorariaAntigaAutorizada.getStatus())) {
				supervisorioMedicaoHorariaVONovo = iteraListaSupervisorioMedicaoHoraria(listaSupervisorioMedicaoHorariaVO,
						supervisorioMedicaoHorariaAntigaAutorizada, supervisorioMedicaoHorariaVONovo);
			}

			if (supervisorioMedicaoHorariaVONovo == null) {

				supervisorioMedicaoHorariaNovo = controladorSupervisorio.obterSupervisorioMedicaoHoraria(
						supervisorioMedicaoHorariaNovo.getChavePrimaria(), STATUS, SUPERVISORIO_MEDICAO_ANORMALIDADE);

				supervisorioMedicaoHorariaVONovo = this.preencherSupervisorioMedicaoHorariaVO(supervisorioMedicaoHorariaNovo);
			}
			supervisorioMedicaoHorariaVONovo.setHabilitado(Boolean.TRUE);
			supervisorioMedicaoHorariaVONovo.setStatus(supervisorioMedicaoHorariaNovo.getStatus());
			supervisorioMedicaoHorariaVONovo.setIsStatusPendente(statusPendente.equals(supervisorioMedicaoHorariaNovo.getStatus()));
			supervisorioMedicaoHorariaVONovo
					.setPossuiAutorizacaoAlcada(statusPendente.equals(supervisorioMedicaoHorariaNovo.getStatus()));

			supervisorioMedicaoHorariaVONovo.setUltimaAlteracao(supervisorioMedicaoHorariaNovo.getUltimaAlteracao());
			supervisorioMedicaoHorariaVONovo.setUltimoUsuarioAlteracao(supervisorioMedicaoHorariaNovo.getUltimoUsuarioAlteracao());
			supervisorioMedicaoHorariaVONovo.setChavePrimaria(supervisorioMedicaoHorariaNovo.getChavePrimaria());

			if (!statusPendente.equals(supervisorioMedicaoHorariaAntigaAutorizada.getStatus())) {
				listaSupervisorioMedicaoHorariaVO.add(supervisorioMedicaoHorariaVONovo);
			}

			iteraSobreListaSupervisorio(request, statusNaoAutorizada, sessao, chavePrimaria, listaSupervisorioMedicaoHorariaVO);

			this.ordenarListaSupervisorioMedicaoHorariaVO(listaSupervisorioMedicaoHorariaVO);

		} else {
			for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {
				if (supervisorioMedicaoHorariaVO.getChavePrimaria().equals(chavePrimaria)) {
					supervisorioMedicaoHorariaVO.setStatus(statusNaoAutorizada);
					supervisorioMedicaoHorariaVO.setIsStatusPendente(Boolean.FALSE);
					supervisorioMedicaoHorariaVO.setHabilitado(Boolean.FALSE);
					supervisorioMedicaoHorariaVO.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
					List<Long> registrosIndicadorProcessado = (List<Long>) sessao.getAttribute(REGISTROS_INDICADOR_PROCESSADO_HORARIA);

					verificaChavePrimariaExistente(chavePrimaria, registrosIndicadorProcessado);
					sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO_HORARIA, registrosIndicadorProcessado);
					break;
				}
			}
		}
	}

	private SupervisorioMedicaoVO iteraListaSupervisorioMedicaoHoraria(List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO,
			SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAntigaAutorizada, SupervisorioMedicaoVO supervisorioMedicaoHorariaVONovo) {
		for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoHorariaVO) {
			if (supervisorioMedicaoHorariaAntigaAutorizada.getChavePrimaria() == supervisorioMedicaoVO.getChavePrimaria()) {
				supervisorioMedicaoHorariaVONovo = supervisorioMedicaoVO;
				break;
			}
		}
		return supervisorioMedicaoHorariaVONovo;
	}

	private void verificaChavePrimariaExistente(Long chavePrimaria, List<Long> registrosIndicadorProcessado) {
		if (!registrosIndicadorProcessado.contains(chavePrimaria)) {
			registrosIndicadorProcessado.add(chavePrimaria);
		}
	}

	/**
	 * @param request {@link HttpServletRequest}
	 * @param statusNaoAutorizada {@link EntidadeConteudo}
	 * @param sessao {@link HttpSession}
	 * @param chavePrimaria {@link Long}
	 * @param listaSupervisorioMedicaoHorariaVO {@link List}
	 * @throws NegocioException {@link NegocioException}
	 */
	@SuppressWarnings("unchecked")
	private void iteraSobreListaSupervisorio(HttpServletRequest request, EntidadeConteudo statusNaoAutorizada, HttpSession sessao,
			Long chavePrimaria, List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO) throws NegocioException {

		for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {

			if (chavePrimaria.equals(supervisorioMedicaoHorariaVO.getChavePrimaria())) {
				supervisorioMedicaoHorariaVO.setStatus(statusNaoAutorizada);
				supervisorioMedicaoHorariaVO.setIsStatusPendente(Boolean.FALSE);
				supervisorioMedicaoHorariaVO.setHabilitado(Boolean.FALSE);
				supervisorioMedicaoHorariaVO.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
				List<Long> registrosIndicadorProcessado = (List<Long>) sessao.getAttribute(REGISTROS_INDICADOR_PROCESSADO_HORARIA);

				verificaChavePrimariaExistente(chavePrimaria, registrosIndicadorProcessado);
				sessao.setAttribute(REGISTROS_INDICADOR_PROCESSADO_HORARIA, registrosIndicadorProcessado);
				break;
			}
		}
	}

	public void setPesquisaPendentes(boolean pesquisaPendentes) {

		this.pesquisaPendentes = pesquisaPendentes;
	}

	public boolean isPesquisaPendentes() {

		return pesquisaPendentes;
	}

	public void setTipoPendentes(String tipoPendentes) {

		this.tipoPendentes = tipoPendentes;
	}

	public String getTipoPendentes() {

		return tipoPendentes;
	}

	public void setChavesPrimarias(String chavesPrimarias) {

		this.chavesPrimarias = chavesPrimarias;
	}

	public String getChavesPrimarias() {

		return chavesPrimarias;
	}
	
	/**
	 * Método responsável por executar a ação de
	 * transferir a medição do supervisório diário.
	 * 
	 * @param validaMedicaoSupervisorioVO {@link ValidaMedicaoSupervisorioVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarMedicaoSupervisorio {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("transferirMedicaoDiariaPorDia")
	public String transferirMedicaoDiariaPorDia(ValidaMedicaoSupervisorioVO validaMedicaoSupervisorioVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		// Cria a sessão
		HttpSession sessao = request.getSession(false);

		Map<String, Object> filtro = new HashMap<>();

		filtro.put("habilitado", Boolean.TRUE);
		filtro.put("indicadorIntegrado", "0");
		filtro.put(INDICADOR_CONSOLIDADA, Boolean.TRUE);
		filtro.put(TRANSFERENCIA, Boolean.FALSE);
		filtro.put("chavesSupervisorioMedicaoDiaria", chavesPrimarias);

		try {
			controladorSupervisorio.transferirSupervisorioMedicoesDiariaPorDia(filtro);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_PRONTA_PARA_TRANSFERENCIA, request,
					SupervisorioMedicaoDiaria.SUPERVISORIO_MEDICAO_DIARIA_ROTULO);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, request, e.getChaveErro());
		}
		validaMedicaoSupervisorioVO.setDataIntegracaoInicial(null);
		validaMedicaoSupervisorioVO.setDataIntegracaoFinal(null);
		validaMedicaoSupervisorioVO.setConsultaBancoTelaSupervisorioDiario(Boolean.TRUE);

		Map<String, Object> filtroAux = (Map<String, Object>) sessao.getAttribute(FILTRO);
		filtroAux.put("indicadorIntegrado", "0");
		sessao.setAttribute(FILTRO, filtroAux);	
		
		return exibirDetalhamentoMedicaoSupervisorioDiario(validaMedicaoSupervisorioVO, bindingResult, request, model);
	}
	
}
