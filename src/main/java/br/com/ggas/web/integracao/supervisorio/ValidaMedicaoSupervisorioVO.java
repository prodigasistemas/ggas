package br.com.ggas.web.integracao.supervisorio;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Validação Medições do Supervisório..
 * 
 * @author bruno silva
 * 
 * @see ValidaMedicaoSupervisorioAction
 * 
 */
public class ValidaMedicaoSupervisorioVO {
	
	private Boolean ocorrenciaPC;
	private Boolean ocorrenciaMI;
	private Boolean analisadaPC = false;
	private Boolean analisadaMI = false;
	private Boolean transferenciaPC = false;
	private Boolean transferenciaMI = false;
	private Boolean ativoPC = true;
	private Boolean ativoMI = true;
	private Boolean consultaBanco;
	private Boolean indicadorProcessado;
	private Boolean indicadorIntegrado;
	private Boolean consultaBancoTelaSupervisorioDiario;
	
	private String anoMesReferenciaLidoPC;
	private String anoMesReferenciaLidoMI;
	private String anoMesReferenciaParametro;
	private String enderecoRemotoParametro;
	private String enderecoRemotoLidoPC;
	private String enderecoRemotoLidoMI;
	private String dataRealizacaoLeitura;
	private String dataReferencia;
	private String horaRealizacaoLeitura;
	private String dataLeituraInicialPC;
	private String dataLeituraInicialMI;
	private String dataLeituraFinalPC;
	private String dataLeituraFinalMI;
	private String dataInclusaoInicialPC;
	private String dataInclusaoInicialMI;
	private String dataInclusaoFinalPC;
	private String dataInclusaoFinalMI;
	private String dataIntegracaoInicial;
	private String dataIntegracaoFinal;
	private String volumeCorrigido;
	private String cepImovel;
	private String matriculaImovel;
	private String indicadorCondominio;
	private String nomeImovel ;
	private String numeroImovel;
	private String complementoImovel;
	private String indicadorPesquisa = "pontoConsumo";
	
	private Long quantidadeRegistrosAtivos;
	private Long chavePrimaria;
	private Long status;
	private Long statusPC;
	private Long statusMI;
	private Long idImovel;
	private Long idCliente;
	private Long idCicloPC;
	private Long idCicloMI;
	private Long idCicloParametro;
	private Long idGrupoFaturamento;
	private Long idLocalidade;
	private Long idSetorComercial;
	private Long exigeComentario;
	private Long[] idsOcorrenciaMedicaoPC;
	private Long[] idsOcorrenciaMedicaoMI;
	private Long[] idsRota;
	
	/**
	 * @return ocorrenciaPC {@link Boolean}
	 */
	public Boolean getOcorrenciaPC() {
		return ocorrenciaPC;
	}
	/**
	 * @param ocorrenciaPC {@link Boolean}
	 */
	public void setOcorrenciaPC(Boolean ocorrenciaPC) {
		this.ocorrenciaPC = ocorrenciaPC;
	}
	/**
	 * @return ocorrenciaMI {@link Boolean}
	 */
	public Boolean getOcorrenciaMI() {
		return ocorrenciaMI;
	}
	/**
	 * @param ocorrenciaMI {@link Boolean}
	 */
	public void setOcorrenciaMI(Boolean ocorrenciaMI) {
		this.ocorrenciaMI = ocorrenciaMI;
	}
	/**
	 * @return analisadaPC {@link Boolean}
	 */
	public Boolean getAnalisadaPC() {
		return analisadaPC;
	}
	/**
	 * @param analisadaPC {@link Boolean}
	 */
	public void setAnalisadaPC(Boolean analisadaPC) {
		this.analisadaPC = analisadaPC;
	}
	/**
	 * @return analisadaMI {@link Boolean}
	 */
	public Boolean getAnalisadaMI() {
		return analisadaMI;
	}
	/**
	 * @param analisadaMI {@link Boolean}
	 */
	public void setAnalisadaMI(Boolean analisadaMI) {
		this.analisadaMI = analisadaMI;
	}
	/**
	 * @return transferenciaPC {@link Boolean}
	 */
	public Boolean getTransferenciaPC() {
		return transferenciaPC;
	}
	/**
	 * @param transferenciaPC {@link Boolean}
	 */
	public void setTransferenciaPC(Boolean transferenciaPC) {
		this.transferenciaPC = transferenciaPC;
	}
	/**
	 * @return transferenciaMI {@link Boolean}
	 */
	public Boolean getTransferenciaMI() {
		return transferenciaMI;
	}
	/**
	 * @param transferenciaMI {@link Boolean}
	 */
	public void setTransferenciaMI(Boolean transferenciaMI) {
		this.transferenciaMI = transferenciaMI;
	}
	/**
	 * @return ativoPC {@link Boolean}
	 */
	public Boolean getAtivoPC() {
		return ativoPC;
	}
	/**
	 * @param ativoPC {@link Boolean}
	 */
	public void setAtivoPC(Boolean ativoPC) {
		this.ativoPC = ativoPC;
	}
	/**
	 * @return ativoMI {@link Boolean}
	 */
	public Boolean getAtivoMI() {
		return ativoMI;
	}
	/**
	 * @param ativoMI {@link Boolean}
	 */
	public void setAtivoMI(Boolean ativoMI) {
		this.ativoMI = ativoMI;
	}
	/**
	 * @return consultaBanco {@link Boolean}
	 */
	public Boolean getConsultaBanco() {
		return consultaBanco;
	}
	/**
	 * @param consultaBanco {@link Boolean}
	 */
	public void setConsultaBanco(Boolean consultaBanco) {
		this.consultaBanco = consultaBanco;
	}
	/**
	 * @return indicadorProcessado {@link Boolean}
	 */
	public Boolean getIndicadorProcessado() {
		return indicadorProcessado;
	}
	/**
	 * @param indicadorProcessado {@link Boolean}
	 */
	public void setIndicadorProcessado(Boolean indicadorProcessado) {
		this.indicadorProcessado = indicadorProcessado;
	}
	/**
	 * @return indicadorIntegrado {@link Boolean}
	 */
	public Boolean getIndicadorIntegrado() {
		return indicadorIntegrado;
	}
	/**
	 * @param indicadorIntegrado {@link Boolean}
	 */
	public void setIndicadorIntegrado(Boolean indicadorIntegrado) {
		this.indicadorIntegrado = indicadorIntegrado;
	}
	/**
	 * @return consultaBancoTelaSupervisorioDiario {@link Boolean}
	 */
	public Boolean getConsultaBancoTelaSupervisorioDiario() {
		return consultaBancoTelaSupervisorioDiario;
	}
	/**
	 * @param consultaBancoTelaSupervisorioDiario {@link Boolean}
	 */
	public void setConsultaBancoTelaSupervisorioDiario(Boolean consultaBancoTelaSupervisorioDiario) {
		this.consultaBancoTelaSupervisorioDiario = consultaBancoTelaSupervisorioDiario;
	}
	/**
	 * @return anoMesReferenciaLidoPC {@link String}
	 */
	public String getAnoMesReferenciaLidoPC() {
		return anoMesReferenciaLidoPC;
	}
	/**
	 * @param anoMesReferenciaLidoPC {@link String}
	 */
	public void setAnoMesReferenciaLidoPC(String anoMesReferenciaLidoPC) {
		this.anoMesReferenciaLidoPC = anoMesReferenciaLidoPC;
	}
	/**
	 * @return anoMesReferenciaLidoMI {@link String}
	 */
	public String getAnoMesReferenciaLidoMI() {
		return anoMesReferenciaLidoMI;
	}
	/**
	 * @param anoMesReferenciaLidoMI {@link String}
	 */
	public void setAnoMesReferenciaLidoMI(String anoMesReferenciaLidoMI) {
		this.anoMesReferenciaLidoMI = anoMesReferenciaLidoMI;
	}
	/**
	 * @return anoMesReferenciaParametro {@link String}
	 */
	public String getAnoMesReferenciaParametro() {
		return anoMesReferenciaParametro;
	}
	/**
	 * @param anoMesReferenciaParametro {@link String}
	 */
	public void setAnoMesReferenciaParametro(String anoMesReferenciaParametro) {
		this.anoMesReferenciaParametro = anoMesReferenciaParametro;
	}
	/**
	 * @return enderecoRemotoParametro {@link String}
	 */
	public String getEnderecoRemotoParametro() {
		return enderecoRemotoParametro;
	}
	/**
	 * @param enderecoRemotoParametro {@link String}
	 */
	public void setEnderecoRemotoParametro(String enderecoRemotoParametro) {
		this.enderecoRemotoParametro = enderecoRemotoParametro;
	}
	/**
	 * @return enderecoRemotoLidoPC {@link String}
	 */
	public String getEnderecoRemotoLidoPC() {
		return enderecoRemotoLidoPC;
	}
	/**
	 * @param enderecoRemotoLidoPC {@link String}
	 */
	public void setEnderecoRemotoLidoPC(String enderecoRemotoLidoPC) {
		this.enderecoRemotoLidoPC = enderecoRemotoLidoPC;
	}
	/**
	 * @return enderecoRemotoLidoMI {@link String}
	 */
	public String getEnderecoRemotoLidoMI() {
		return enderecoRemotoLidoMI;
	}
	/**
	 * @param enderecoRemotoLidoMI {@link String}
	 */
	public void setEnderecoRemotoLidoMI(String enderecoRemotoLidoMI) {
		this.enderecoRemotoLidoMI = enderecoRemotoLidoMI;
	}
	/**
	 * @return dataRealizacaoLeitura {@link String}
	 */
	public String getDataRealizacaoLeitura() {
		return dataRealizacaoLeitura;
	}
	/**
	 * @param dataRealizacaoLeitura {@link String}
	 */
	public void setDataRealizacaoLeitura(String dataRealizacaoLeitura) {
		this.dataRealizacaoLeitura = dataRealizacaoLeitura;
	}
	/**
	 * @return dataReferencia {@link String}
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}
	/**
	 * @param dataReferencia {@link String}
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	/**
	 * @return horaRealizacaoLeitura {@link String}
	 */
	public String getHoraRealizacaoLeitura() {
		return horaRealizacaoLeitura;
	}
	/**
	 * @param horaRealizacaoLeitura {@link String}
	 */
	public void setHoraRealizacaoLeitura(String horaRealizacaoLeitura) {
		this.horaRealizacaoLeitura = horaRealizacaoLeitura;
	}
	/**
	 * @return dataLeituraInicialPC {@link String}
	 */
	public String getDataLeituraInicialPC() {
		return dataLeituraInicialPC;
	}
	/**
	 * @param dataLeituraInicialPC {@link String}
	 */
	public void setDataLeituraInicialPC(String dataLeituraInicialPC) {
		this.dataLeituraInicialPC = dataLeituraInicialPC;
	}
	/**
	 * @return dataLeituraInicialMI {@link String}
	 */
	public String getDataLeituraInicialMI() {
		return dataLeituraInicialMI;
	}
	/**
	 * @param dataLeituraInicialMI {@link String}
	 */
	public void setDataLeituraInicialMI(String dataLeituraInicialMI) {
		this.dataLeituraInicialMI = dataLeituraInicialMI;
	}
	/**
	 * @return dataLeituraFinalPC {@link String}
	 */
	public String getDataLeituraFinalPC() {
		return dataLeituraFinalPC;
	}
	/**
	 * @param dataLeituraFinalPC {@link String}
	 */
	public void setDataLeituraFinalPC(String dataLeituraFinalPC) {
		this.dataLeituraFinalPC = dataLeituraFinalPC;
	}
	
	public String getDataLeituraFinalMI() {
		return dataLeituraFinalMI;
	}
	public void setDataLeituraFinalMI(String dataLeituraFinalMI) {
		this.dataLeituraFinalMI = dataLeituraFinalMI;
	}
	public String getDataInclusaoInicialPC() {
		return dataInclusaoInicialPC;
	}
	public void setDataInclusaoInicialPC(String dataInclusaoInicialPC) {
		this.dataInclusaoInicialPC = dataInclusaoInicialPC;
	}
	public String getDataInclusaoInicialMI() {
		return dataInclusaoInicialMI;
	}
	public void setDataInclusaoInicialMI(String dataInclusaoInicialMI) {
		this.dataInclusaoInicialMI = dataInclusaoInicialMI;
	}
	public String getDataInclusaoFinalPC() {
		return dataInclusaoFinalPC;
	}
	public void setDataInclusaoFinalPC(String dataInclusaoFinalPC) {
		this.dataInclusaoFinalPC = dataInclusaoFinalPC;
	}
	public String getDataInclusaoFinalMI() {
		return dataInclusaoFinalMI;
	}
	public void setDataInclusaoFinalMI(String dataInclusaoFinalMI) {
		this.dataInclusaoFinalMI = dataInclusaoFinalMI;
	}
	public String getDataIntegracaoInicial() {
		return dataIntegracaoInicial;
	}
	public void setDataIntegracaoInicial(String dataIntegracaoInicial) {
		this.dataIntegracaoInicial = dataIntegracaoInicial;
	}
	public String getDataIntegracaoFinal() {
		return dataIntegracaoFinal;
	}
	public void setDataIntegracaoFinal(String dataIntegracaoFinal) {
		this.dataIntegracaoFinal = dataIntegracaoFinal;
	}
	public String getVolumeCorrigido() {
		return volumeCorrigido;
	}
	public void setVolumeCorrigido(String volumeCorrigido) {
		this.volumeCorrigido = volumeCorrigido;
	}
	public String getCepImovel() {
		return cepImovel;
	}
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	public String getMatriculaImovel() {
		return matriculaImovel;
	}
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}
	public String getIndicadorCondominio() {
		return indicadorCondominio;
	}
	public void setIndicadorCondominio(String indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}
	public String getNomeImovel() {
		return nomeImovel;
	}
	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}
	public String getNumeroImovel() {
		return numeroImovel;
	}
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	public String getComplementoImovel() {
		return complementoImovel;
	}
	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	
	public Long getQuantidadeRegistrosAtivos() {
		return quantidadeRegistrosAtivos;
	}
	public void setQuantidadeRegistrosAtivos(Long quantidadeRegistrosAtivos) {
		this.quantidadeRegistrosAtivos = quantidadeRegistrosAtivos;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public Long getStatusPC() {
		return statusPC;
	}
	public void setStatusPC(Long statusPC) {
		this.statusPC = statusPC;
	}
	public Long getStatusMI() {
		return statusMI;
	}
	public void setStatusMI(Long statusMI) {
		this.statusMI = statusMI;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdCicloPC() {
		return idCicloPC;
	}
	public void setIdCicloPC(Long idCicloPC) {
		this.idCicloPC = idCicloPC;
	}
	public Long getIdCicloMI() {
		return idCicloMI;
	}
	public void setIdCicloMI(Long idCicloMI) {
		this.idCicloMI = idCicloMI;
	}
	public Long getIdCicloParametro() {
		return idCicloParametro;
	}
	public void setIdCicloParametro(Long idCicloParametro) {
		this.idCicloParametro = idCicloParametro;
	}
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	public Long getIdLocalidade() {
		return idLocalidade;
	}
	public void setIdLocalidade(Long idLocalidade) {
		this.idLocalidade = idLocalidade;
	}
	public Long getIdSetorComercial() {
		return idSetorComercial;
	}
	public void setIdSetorComercial(Long idSetorComercial) {
		this.idSetorComercial = idSetorComercial;
	}
	public Long getExigeComentario() {
		return exigeComentario;
	}
	public void setExigeComentario(Long exigeComentario) {
		this.exigeComentario = exigeComentario;
	}
	
	
	/**
	 * @return idsOcorrenciaMedicaoPC {@link Long}
	 */
	public Long[] getIdsOcorrenciaMedicaoPC() {
		Long[] idsOcorrenciaMedicaoPCTmp = null;
		if (idsOcorrenciaMedicaoPC != null) {
			idsOcorrenciaMedicaoPCTmp = idsOcorrenciaMedicaoPC.clone();
		}
		return idsOcorrenciaMedicaoPCTmp;
	}
	/**
	 * @param idsOcorrenciaMedicaoPC {@link Long}
	 */
	public void setIdsOcorrenciaMedicaoPC(Long[] idsOcorrenciaMedicaoPC) {
		if (idsOcorrenciaMedicaoPC != null) {
			this.idsOcorrenciaMedicaoPC = idsOcorrenciaMedicaoPC.clone();
		} else {
			this.idsOcorrenciaMedicaoPC = null;
		}
	}
	
	/**
	 * @return idsOcorrenciaMedicaoMI {@link Long}
	 */
	public Long[] getIdsOcorrenciaMedicaoMI() {
		Long[] idsOcorrenciaMedicaoMITmp = null;
		if (idsOcorrenciaMedicaoMI != null) {
			idsOcorrenciaMedicaoMITmp = idsOcorrenciaMedicaoMI.clone();
		}
		return idsOcorrenciaMedicaoMITmp;
	}
	/**
	 * @param idsOcorrenciaMedicaoMI {@link Long}
	 */
	public void setIdsOcorrenciaMedicaoMI(Long[] idsOcorrenciaMedicaoMI) {
		if (idsOcorrenciaMedicaoMI != null) {
			this.idsOcorrenciaMedicaoMI = idsOcorrenciaMedicaoMI.clone();
		} else {
			this.idsOcorrenciaMedicaoMI = null;
		}
	}
	
	/**
	 * @return idsRota {@link Long}
	 */
	public Long[] getIdsRota() {
		Long[] idsRotaTmp = null;
		if (idsRota != null) {
			idsRotaTmp = idsRota.clone();
		}
		return idsRotaTmp;
	}
	/**
	 * @param idsRota {@link Long}
	 */
	public void setIdsRota(Long[] idsRota) {
		if (idsRota != null) {
			this.idsRota = idsRota.clone();
		} else {
			this.idsRota = null;
		}
	}
	
}
