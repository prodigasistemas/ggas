/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.integracao.geral;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Sistema Integrante.
 * 
 * @author vtavares
 */
@Controller
public class SistemaIntegranteAction extends GenericAction {

	private static final String EXIBIR_INCLUSAO_SISTEMA_INTEGRANTE = "exibirInclusaoSistemaIntegrante";

	private static final String EXIBIR_PESQUISA_SISTEMA_INTEGRANTE = "exibirPesquisaSistemaIntegrante";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME = "nome";

	private static final String SIGLA = "sigla";

	private static final String HABILITADO = "habilitado";
	
	@Autowired
	private ControladorIntegracao controladorIntegracao;

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa do sistema integrante.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @return exibirPesquisaSistemaIntegrante	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping(EXIBIR_PESQUISA_SISTEMA_INTEGRANTE)
	public String exibirPesquisaSistemaIntegrante(IntegracaoVO integracaoVO) throws GGASException {

		return EXIBIR_PESQUISA_SISTEMA_INTEGRANTE;
	}
	
	/**
	 * Método responsável por exibir o resultado da
	 * pesquisa do sistema integrante.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @param result							-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirPesquisaSistemaIntegrante	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("pesquisarSistemaIntegrante")
	public String pesquisarSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			Map<String, Object> filtro = prepararFiltro(integracaoVO);

			adicionarFiltroPaginacao(request, filtro);
			Collection<IntegracaoSistema> listaIntegracaoSistema = controladorIntegracao
					.consultarIntegracaoSistema(filtro);
			model.addAttribute("sistemasIntegrantes",
					super.criarColecaoPaginada(request, filtro, listaIntegracaoSistema));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return EXIBIR_PESQUISA_SISTEMA_INTEGRANTE;
	}
	
	/**
	 * Método responsável por exibir a tela de
	 * inclusao do sistema integrante.
	 * 
	 * @return exibirInclusaoSistemaIntegrante	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping(EXIBIR_INCLUSAO_SISTEMA_INTEGRANTE)
	public String exibirInclusaoSistemaIntegrante() throws GGASException {

		return EXIBIR_INCLUSAO_SISTEMA_INTEGRANTE;
	}
	
	/**
	 * Método responsável por incluir
	 * um sistema integrante.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @param result							-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirInclusaoSistemaIntegrante	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("incluirSistemaIntegrante")
	public String incluirSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = EXIBIR_INCLUSAO_SISTEMA_INTEGRANTE;

		try {

			IntegracaoSistema integracaoSistema = (IntegracaoSistema) controladorIntegracao.criarIntegracaoSistema();
			popularSistemaIntegrante(integracaoSistema, integracaoVO);
			controladorIntegracao.validarDadosSistemaIntegrante(integracaoSistema);
			integracaoSistema.setDadosAuditoria(getDadosAuditoria(request));
			integracaoSistema.setHabilitado(true);

			Map<String, Object> filtro = prepararFiltro(integracaoVO);
			controladorIntegracao.verificarSistemaIntegranteExistente(filtro);

			Long idIntegracaoSistema = controladorIntegracao.inserir(integracaoSistema);
			model.addAttribute(CHAVE_PRIMARIA, idIntegracaoSistema);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, IntegracaoSistema.INTEGRACAO_SISTEMA);

			view = pesquisarSistemaIntegrante(integracaoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;

	}
	
	/**
	 * Método responsável por exibir a tela de atualizacao do sistema integrante.
	 * 
	 * @param integracaoVO 														-	{@link IntegracaoVO}
	 * @param result      														-	{@link BindingResult}
	 * @param request      														-	{@link HttpServletRequest}
	 * @param model       														-	{@link Model}
	 * @return exibirAlteracaoSistemaIntegrante | pesquisarSistemaIntegrante	-	{@link String}
	 * @throws GGASException													-	{@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoSistemaIntegrante")
	public String exibirAlteracaoSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoSistemaIntegrante";

		try {
			Long chavePrimaria = integracaoVO.getChavePrimaria();
			IntegracaoSistema integracaoSistema = controladorIntegracao.obterIntegracaoSistema(chavePrimaria);
			popularVO(integracaoSistema, integracaoVO);
			model.addAttribute("integracaoVO", integracaoVO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			integracaoVO.setChavePrimaria(-1L);
			view = pesquisarSistemaIntegrante(integracaoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por alterar(atualizar)
	 * o sistema integrante.
	 * 
	 * @param integracaoVO														-	{@link IntegracaoVO}
	 * @param result															-	{@link BindingResult}
	 * @param request															-	{@link HttpServletRequest}
	 * @param model																-	{@link Model}
	 * @return pesquisarSistemaIntegrante | exibirAlteracaoSistemaIntegrante	-	{@link String}
	 * @throws GGASException													-	{@link GGASException}
	 */
	@RequestMapping("alterarSistemaIntegrante")
	public String alterarSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = null;

		Long chavePrimaria = integracaoVO.getChavePrimaria();

		try {

			IntegracaoSistema integracaoSistema = controladorIntegracao.obterIntegracaoSistema(chavePrimaria);
			popularSistemaIntegrante(integracaoSistema, integracaoVO);
			controladorIntegracao.validarDadosSistemaIntegrante(integracaoSistema);
			integracaoSistema.setDadosAuditoria(getDadosAuditoria(request));

			Map<String, Object> filtro = prepararFiltro(integracaoVO);
			filtro.put("idIntegracaoSistema", chavePrimaria);

			controladorIntegracao.verificarSistemaIntegranteExistente(filtro);

			if (!integracaoSistema.isHabilitado()) {
				controladorIntegracao.verificarExistenciaIntegracoesAtivas(filtro);
			}

			controladorIntegracao.atualizarIntegracaoSistema(integracaoSistema);

			model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, IntegracaoSistema.INTEGRACAO_SISTEMA);

			view = pesquisarSistemaIntegrante(integracaoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoSistemaIntegrante(integracaoVO, result, request, model);
		}

		return view;
	}
	
	/**
	 * Método responsável por exibir a tela de
	 * detalhamendo de cada sistema integrante.
	 * 
	 * @param integracaoVO														-	{@link IntegracaoVO}
	 * @param result															-	{@link BindingResult}
	 * @param request															-	{@link HttpServletRequest}
	 * @param model																-	{@link Model}		
	 * @return exibirDetalhamentoSistemaIntegrante | pesquisarSistemaIntegrante	-	{@link String}
	 * @throws GGASException													-	{@link GGASException}			
	 */
	@RequestMapping("exibirDetalhamentoSistemaIntegrante")
	public String exibirDetalhamentoSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoSistemaIntegrante";

		try {

			IntegracaoSistema integracaoSistema = controladorIntegracao
					.obterIntegracaoSistema(integracaoVO.getChavePrimaria());
			popularVO(integracaoSistema, integracaoVO);
			model.addAttribute("integracaoVO", integracaoVO);
			Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao = controladorIntegracao
					.listarIntegracaoSistemaFuncao(integracaoSistema);
			model.addAttribute("listaIntegracoes", listarIntegracaoSistemaFuncao);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			integracaoVO.setChavePrimaria(-1L);
			view = pesquisarSistemaIntegrante(integracaoVO, result, request, model);
		}

		return view;
	}
	
	/**
	 * Método responsável por excluir
	 * o sistema integrante do sistema.
	 * 
	 * @param integracaoVO					-	{@link IntegracaoVO}
	 * @param result						-	{@link BindingResult}
	 * @param request						-	{@link HttpServletRequest}
	 * @param model							-	{@link Model}
	 * @return pesquisarSistemaIntegrante	-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	@RequestMapping("removerSistemaIntegrante")
	public String removerSistemaIntegrante(IntegracaoVO integracaoVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Long[] chavesPrimarias = integracaoVO.getChavesPrimarias();

		try {

			validarSelecaoUmOuMais(chavesPrimarias);
			Map<String, Object> filtro = prepararFiltro(integracaoVO);
			filtro.put("idIntegracaoSistemas", chavesPrimarias);
			controladorIntegracao.verificarExistenciaIntegracoesRelacionadas(filtro);
			controladorIntegracao.removerIntegracaoSistema(chavesPrimarias, getDadosAuditoria(request));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, IntegracaoSistema.INTEGRACAO_SISTEMA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarSistemaIntegrante(integracaoVO, result, request, model);
	}

	/**
	 * Popular sistema integrante.
	 * 
	 * @param integracaoSistema	-	{@link IntegracaoSistema}
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 */
	private void popularSistemaIntegrante(IntegracaoSistema integracaoSistema, IntegracaoVO integracaoVO) {

		Long chavePrimaria = integracaoVO.getChavePrimaria();
		Integer versao = integracaoVO.getVersao();
		Boolean habilitado = integracaoVO.getHabilitado();

		if (chavePrimaria != null && chavePrimaria.longValue() > 0) {
			integracaoSistema.setChavePrimaria(chavePrimaria);
		}
		if (versao != null && versao.intValue() > 0) {
			integracaoSistema.setVersao(versao);
		}
		if (habilitado != null) {
			integracaoSistema.setHabilitado(habilitado);
		}
		
		
		popularSistemaIntegranteDois(integracaoSistema, integracaoVO);

	}

	/**
	 * Popular sistema integrante.
	 * 
	 * @param integracaoSistema	-	{@link IntegracaoSistema}
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 */
	private void popularSistemaIntegranteDois(IntegracaoSistema integracaoSistema, IntegracaoVO integracaoVO) {

		String nome = integracaoVO.getNome();
		String sigla = integracaoVO.getSigla();
		String descricao = integracaoVO.getDescricao();

		if (StringUtils.isNotEmpty(nome)) {
			integracaoSistema.setNome(nome);
		} else {
			integracaoSistema.setNome(null);
		}
		if (StringUtils.isNotEmpty(sigla)) {
			integracaoSistema.setSigla(sigla);
		} else {
			integracaoSistema.setSigla(null);
		}
		if (StringUtils.isNotEmpty(descricao)) {
			integracaoSistema.setDescricao(descricao);
		} else {
			integracaoSistema.setDescricao(null);
		}

	}
	
	/**
	 * Método Responsável por popular a classe IntegracaoVO.
	 * 
	 * @param integracaoSistema	-	{@link IntegracaoSistema}
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 */
	private void popularVO(IntegracaoSistema integracaoSistema, IntegracaoVO integracaoVO) {

		integracaoVO.setChavePrimaria(integracaoSistema.getChavePrimaria());
		integracaoVO.setNome(integracaoSistema.getNome());
		integracaoVO.setSigla(integracaoSistema.getSigla());
		integracaoVO.setDescricao(integracaoSistema.getDescricao());
		integracaoVO.setHabilitado(integracaoSistema.isHabilitado());

	}

	/**
	 * Preparar filtro.
	 * 
	 * @param integracaoVO	-	{@link IntegracaoVO}
	 * @return filtro		-	{@link Map}
	 */
	private Map<String, Object> prepararFiltro(IntegracaoVO integracaoVO) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long chavePrimaria = integracaoVO.getChavePrimaria();
		String nome = integracaoVO.getNome();
		String sigla = integracaoVO.getSigla();
		Boolean habilitado = integracaoVO.getHabilitado();

		if (chavePrimaria != null && chavePrimaria > 0) {
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		} else {
			filtro.put(CHAVE_PRIMARIA, 0L);
		}

		if (StringUtils.isNotEmpty(nome)) {
			filtro.put(NOME, nome);
		}

		if (StringUtils.isNotEmpty(sigla)) {
			filtro.put(SIGLA, sigla);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;

	}

}
