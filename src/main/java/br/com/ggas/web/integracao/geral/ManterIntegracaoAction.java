/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.integracao.geral;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoFuncao;
import br.com.ggas.integracao.geral.IntegracaoFuncaoTabela;
import br.com.ggas.integracao.geral.IntegracaoMapeamento;
import br.com.ggas.integracao.geral.IntegracaoParametro;
import br.com.ggas.integracao.geral.IntegracaoParametroVO;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncaoVO;
import br.com.ggas.integracao.geral.IntegracaoSistemaParametro;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Integração.
 */
@Controller
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ManterIntegracaoAction extends GenericAction {

	private static final String EXIBIR_ALTERAR_ASSOCIACAO_FUNCOES_INTEGRACAO = "exibirAlterarAssociacaoFuncoesIntegracao";

	private static final String EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO = "exibirAssociacaoFuncaoIntegracao";

	private static final String LISTA_INTEGRACAO_FUNCAO = "listaIntegracaoFuncao";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String INTEGRACAO_SISTEMA = "integracaoSistema";

	private static final String INTEGRACAO_SISTEMA_FUNCAO = "integracaoSistemaFuncao";

	private static final String INTEGRACAO_FUNCAO = "integracaoFuncao";

	private static final String ID_INTEGRACAO_SISTEMA = "idIntegracaoSistema";

	private static final String INDICADOR_ENVIA_EMAIL_ERRO = "indicadorEnviaEmailErro";

	private static final String DESTINATARIOS_EMAIL = "destinatariosEmail";
	
	private static final String LISTA_INTEGRACAO_SISTEMA_FUNCAO = "listaIntegracaoSistemaFuncao";
	
	private static final String OPERACAO = "operacao";
	
	private static final String CHAVE_PRIMARIA_ITEM_MAPEAMENTO = "chavePrimariaItemMapeamento";
	
	private static final String LISTA_PARAMENTROS = "listaParametros";
	
	private static final String LISTA_MAPEAMENTOS = "listaMapeamentos";
	
	private static final String LISTA_MAPEAMENTO = "listaMapeamento";
	
	private static final String MAPEAMENTOS = "mapeamentos";
	
	private static final String ID_INTEGRACAO_SISTEMAS = "idIntegracaoSistemaFuncao";
	
	private static final String EXIBIR = "exibir";

	private static final String HABILITADO = "habilitado";
	
	@Autowired
	private ControladorIntegracao controladorIntegracao;
	
	@Autowired
	private ControladorAuditoria controladorAuditoria;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Metódo responsável por exibir a tela de
	 * pesquisar integracao.
	 * 
	 * @param integracaoVO				-	{@link IntegracaoVO}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						- 	{@link Model}
	 * @return exibirPesquisaIntegracao	-	{@link String}
	 * @throws GGASException			-	{@link GGASException} 
	 */
	@RequestMapping("exibirPesquisaIntegracao")
	public String exibirPesquisaIntegracao(IntegracaoVO integracaoVO, HttpServletRequest request, Model model)
			throws GGASException {

		carregarCombos(model);
		limparVariaveisSessao(request, model);

		model.addAttribute("integracaoVO", integracaoVO);

		return "exibirPesquisaIntegracao";

	}

	/**
	 * Metódo responsável por pesquisar na tela de
	 * pesquisar integracao.
	 * 
	 * @param integracaoVO				-	{@link IntegracaoVO}
	 * @param bindingResult				-	{@link BindingResult}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @return exibirPesquisaIntegracao	-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@RequestMapping("pesquisarIntegracao")
	public String pesquisarIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		try {

			Map<String, Object> filtro = prepararFiltro(integracaoVO);

			Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao = controladorIntegracao
					.listarIntegracaoSistemaFuncao(filtro);

			filtro.put("integracaoFuncaoTabela", "S");

			Collection<IntegracaoSistemaFuncao> listarFuncoesSistemaFuncaoTabela = controladorIntegracao
					.listarIntegracaoSistemaFuncao(filtro);

			IntegracaoSistemaFuncaoVO integracaoSistemaFuncaoVO;
			Collection<IntegracaoSistemaFuncaoVO> integracaoSistemaFuncaoVOList = new ArrayList<IntegracaoSistemaFuncaoVO>();

			for (IntegracaoSistemaFuncao integracaoSistemaFuncao : listarIntegracaoSistemaFuncao) {
				integracaoSistemaFuncaoVO = new IntegracaoSistemaFuncaoVO();
				integracaoSistemaFuncaoVO.setChavePrimaria(integracaoSistemaFuncao.getChavePrimaria());
				integracaoSistemaFuncaoVO.setHabilitado(integracaoSistemaFuncao.isHabilitado());
				integracaoSistemaFuncaoVO.setIntegracaoFuncao(integracaoSistemaFuncao.getIntegracaoFuncao());
				integracaoSistemaFuncaoVO.setIntegracaoSistema(integracaoSistemaFuncao.getIntegracaoSistema());

				if (listarFuncoesSistemaFuncaoTabela.contains(integracaoSistemaFuncao)) {
					integracaoSistemaFuncaoVO.setPendencia("Sim");
				} else {
					integracaoSistemaFuncaoVO.setPendencia("Não");
				}

				integracaoSistemaFuncaoVOList.add(integracaoSistemaFuncaoVO);
			}

			model.addAttribute(LISTA_INTEGRACAO_SISTEMA_FUNCAO, listarIntegracaoSistemaFuncao);
			model.addAttribute("listaIntegracaoSistemaFuncaoGrid", integracaoSistemaFuncaoVOList);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaIntegracao(integracaoVO, request, model);
	}
	
	/**
	 * Exibir detalhamento associacao funcoes integracao.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param bindingResult		-	{@link BindingResult}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 * @return view				-	{@link String}
	 * @throws GGASException	-	{@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoAssociacaoFuncoesIntegracao")
	public String exibirDetalhamentoAssociacaoFuncoesIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		request.getSession().removeAttribute(MAPEAMENTOS);

		Long idIntegracaoFuncao = integracaoVO.getIdIntegracaoFuncao();
		Long idIntegracaoSistema = integracaoVO.getIdIntegracaoSistema();

		model.addAttribute(CHAVE_PRIMARIA_ITEM_MAPEAMENTO, 0L);

		try {

			if (idIntegracaoFuncao != null) {

				IntegracaoSistemaFuncao integracaoSistemaFuncao = controladorIntegracao
						.obterIntegracaoSistemaFuncao(idIntegracaoSistema, idIntegracaoFuncao);

				if (integracaoSistemaFuncao == null) {
					throw new GGASException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, true);
				}

				model.addAttribute(INTEGRACAO_SISTEMA_FUNCAO, integracaoSistemaFuncao);

				Long chavePrimaria = integracaoSistemaFuncao.getChavePrimaria();
				model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);

				this.exibirDetalhamentoAbaParametro(integracaoVO, model);
				this.exibirDadosAbaMapeamento(integracaoSistemaFuncao.getIntegracaoFuncao(), model);

				model.addAttribute("integracaoSistemaFuncao", integracaoSistemaFuncao);
			}

			model.addAttribute("detalhamento", 1L);

			view = "exibirDetalhamentoAssociacaoFuncoesIntegracao";

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarIntegracao(integracaoVO, bindingResult, request, model);
		}

		saveToken(request);

		return view;
	}

	/**
	 * Metódo responsável por pesquisar na tela de
	 * pesquisar integracao.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @param bindingResult						-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirAssociacaoFuncaoIntegracao	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping(EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO)
	public String exibirAssociacaoFuncaoIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		carregarCombos(model);

		String operacao = integracaoVO.getOperacao();
		Long idIntegracaoFuncao = integracaoVO.getIntegracaoFuncao();

		model.addAttribute(CHAVE_PRIMARIA_ITEM_MAPEAMENTO, 0L);
		model.addAttribute(LISTA_INTEGRACAO_FUNCAO, controladorIntegracao.listarIntegracaoFuncao());

		if (("limpar").equalsIgnoreCase(operacao)) {
			limparVariaveisSessao(request, model);
		} else if (idIntegracaoFuncao != null && idIntegracaoFuncao > 0) {
			IntegracaoFuncao integracaoFuncao = (IntegracaoFuncao) controladorIntegracao.criarIntegracaoFuncao();
			integracaoFuncao.setChavePrimaria(idIntegracaoFuncao);
			this.exibirDadosAbaParametro(integracaoVO, integracaoFuncao, model);
			this.exibirDadosAbaMapeamento(integracaoFuncao, model);
		}

		saveToken(request);

		return EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO;

	}
	
	/**
	 * Salvar integracao.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param bindingResult		-	{@link BindingResult}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 * @return view				-	{@link String}
	 * @throws GGASException	-	{@link GGASException}
	 */
	@RequestMapping("salvarIntegracao")
	public String salvarIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException{

		String view = null;

		try {

			validarToken(request);

			// recupera as informações de mapeamento que foram substituidas na tela sem ter
			// ação de botão
			atualizarListaMapeamentoPorMudancaTela(integracaoVO, request, model);

			Long idIntegracaoSistema = integracaoVO.getIntegracaoSistema();
			Long idIntegracaoFuncao = integracaoVO.getIntegracaoFuncao();

			if (idIntegracaoSistema == null || idIntegracaoFuncao == null || idIntegracaoSistema.equals(-1L)
					|| idIntegracaoFuncao.equals(-1L)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_DADOS_NAO_INFORMADO,
						false);
			}

			IntegracaoSistema intSistema = (IntegracaoSistema) controladorIntegracao.criarIntegracaoSistema();
			intSistema.setChavePrimaria(idIntegracaoSistema);

			IntegracaoFuncao intFuncao = (IntegracaoFuncao) controladorIntegracao.criarIntegracaoFuncao();
			intFuncao.setChavePrimaria(idIntegracaoFuncao);

			IntegracaoSistemaFuncao intSistemaFuncao = (IntegracaoSistemaFuncao) controladorIntegracao
					.criarIntegracaoSistemaFuncao();
			intSistemaFuncao.setIntegracaoSistema(intSistema);
			intSistemaFuncao.setIntegracaoFuncao(intFuncao);

			intSistemaFuncao.setHabilitado(Boolean.TRUE);
			intSistemaFuncao.setUltimaAlteracao(new Date());

			if (controladorIntegracao.obterIntegracaoSistemaFuncao(idIntegracaoSistema, idIntegracaoFuncao) != null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_FUNCAO_SISTEMA_EXISTENTE, false);
			}

			Long indicadorEnviaEmailErro = integracaoVO.getIndicadorEnviaEmailErro();

			intSistemaFuncao.setIndicadorEnviaEmailErro(indicadorEnviaEmailErro);
			if (indicadorEnviaEmailErro.equals(1L)) {
				if (integracaoVO.getDestinatariosEmail() == null || ("").equals(integracaoVO.getDestinatariosEmail())) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_INFORMAR_EMAIL, false);
				} else {
					intSistemaFuncao.setDestinatariosEmail(integracaoVO.getDestinatariosEmail());
					controladorIntegracao.validarDestinatariosEmail(intSistemaFuncao);
				}
			}

			// carrega os dados dos parametros informados na tela.
			Collection<IntegracaoParametro> listaIntegracaoParametro = controladorIntegracao
					.listarParametrosAssociados(intFuncao);

			Long[] chavesParametros = integracaoVO.getChaveParametro();
			String[] valores = integracaoVO.getValor();

			verificaListaIntegracaoParametro(listaIntegracaoParametro, chavesParametros, valores, model);

			controladorIntegracao.inserir(intSistemaFuncao);

			IntegracaoSistemaFuncao integracaoSistemaFuncaoSalva = controladorIntegracao
					.obterIntegracaoSistemaFuncao(idIntegracaoSistema, idIntegracaoFuncao);

			controladorIntegracao.desabilitarRecursosSistema(integracaoSistemaFuncaoSalva.isHabilitado(),
					integracaoSistemaFuncaoSalva.getChavePrimaria());

			if (listaIntegracaoParametro != null) {

				// insere os dados dos parametros
				for (IntegracaoParametro intParametro : listaIntegracaoParametro) {
					IntegracaoSistemaParametro integracaoSistemaParametro = (IntegracaoSistemaParametro) controladorIntegracao
							.criarIntegracaoSistemaParametro();
					integracaoSistemaParametro.setIntegracaoParametro(intParametro);
					integracaoSistemaParametro.setIntegracaoSistema(intSistema);
					integracaoSistemaParametro.setIntegracaoSistemaFuncao(integracaoSistemaFuncaoSalva);
					integracaoSistemaParametro.setValor(intParametro.getParametroSistema().getValor());
					controladorIntegracao.inserirIntegracaoSistemaParametro(integracaoSistemaParametro);
				}
			}

			// carrega e insere os dados dos mapeamentos
			Map<Long, List> mapeamentos = getMapMapeamentos(request, idIntegracaoFuncao);
			Iterator iterator = mapeamentos.entrySet().iterator();

			while (iterator.hasNext()) {

				Map.Entry mapeamento = (Map.Entry) iterator.next();
				List<IntegracaoMapeamento> integracaoMapeamentoList = (List<IntegracaoMapeamento>) mapeamento
						.getValue();
				Tabela tabela = controladorAuditoria.obterTabela((Long) mapeamento.getKey());
				carregarInserirDadosMapeamentos(intSistema, integracaoMapeamentoList, tabela);

			}

			limparVariaveisSessao(request, model);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					IntegracaoSistemaFuncao.INTEGRACAO_SISTEMA_FUNCAO);

			view = exibirPesquisaIntegracao(integracaoVO, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAssociacaoFuncaoIntegracao(integracaoVO, bindingResult, request, model);
		}

		return view;

	}

	/**
	 * Carrega e insere os dados dos mapeamentos
	 * 
	 * @param intSistema				-	{@link IntegracaoSistema}
	 * @param integracaoMapeamentoList	-	{@link List}
	 * @param tabela					-	{@link Tabela}
	 * @throws NegocioException			-	{@link NegocioException}
	 */
	private void carregarInserirDadosMapeamentos(IntegracaoSistema intSistema,
			List<IntegracaoMapeamento> integracaoMapeamentoList, Tabela tabela) throws NegocioException {
		for (IntegracaoMapeamento integracaoMapeamento : integracaoMapeamentoList) {

			if (integracaoMapeamento.getCodigoEntidadeSistemaGgas() != null) {
				integracaoMapeamento.setIntegracaoSistema(intSistema);
				integracaoMapeamento.setTabela(tabela);
				controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);
			}

		}
	}

	/**
	 * Metódo responsável por pesquisar na tela de pesquisar integracao.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @param bindingResult						-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirAssociacaoFuncaoIntegracao	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("pesquisarAssociacaoFuncaoIntegracao")
	public String pesquisarAssociacaoFuncaoIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		try {

			Map<String, Object> filtro = prepararFiltro(integracaoVO);

			Collection<IntegracaoSistemaFuncao> listarFuncoesSistema = controladorIntegracao
					.listarIntegracaoSistemaFuncao(filtro);

			model.addAttribute(LISTA_INTEGRACAO_SISTEMA_FUNCAO, listarFuncoesSistema);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirAssociacaoFuncaoIntegracao(integracaoVO, bindingResult, request, model);

	}

	/**
	 * Metódo responsável por pesquisar na tela de
	 * pesquisar integracao.
	 * 
	 * @param integracaoVO				-	{@link IntegracaoVO}
	 * @param bindingResult				-	{@link BindingResult}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @return exibirPesquisaIntegracao	-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@RequestMapping("removerSistemaFuncao")
	public String removerSistemaFuncao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Long[] chavesPrimarias = integracaoVO.getChavesPrimarias();

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {

			validarSelecaoUmOuMais(chavesPrimarias);

			for (Long chave : chavesPrimarias) {

				IntegracaoSistemaFuncao integracao = controladorIntegracao.obterIntegracaoSistemaFuncao(chave);

				filtro.put("idIntegracaoFuncao", integracao.getIntegracaoFuncao().getChavePrimaria());
				filtro.put(HABILITADO, Boolean.TRUE);

				Collection<IntegracaoSistemaFuncao> lista = controladorIntegracao
						.consultarIntegracaoSistemaFuncao(filtro);

				if (lista.size() == 1) {
					controladorIntegracao.desabilitarRecursosSistema(Boolean.FALSE, integracao.getChavePrimaria());
				}

				controladorIntegracao.removerSistemaFuncao(new Long[] { chave }, getDadosAuditoria(request));
			}

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					IntegracaoSistemaFuncao.INTEGRACAO_SISTEMA_FUNCAO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaIntegracao(integracaoVO, request, model);

	}

	/**
	 * Carregar combos.
	 * 
	 * @param model	-	{@link Model}
	 */
	private void carregarCombos(Model model) {

		model.addAttribute("listaIntegracaoSistema", controladorIntegracao.listarIntegracaoSistema());
		model.addAttribute(LISTA_INTEGRACAO_FUNCAO, controladorIntegracao.listarIntegracaoFuncao());

	}

	/**
	 * Exibir alterar associacao funcoes integracao.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}	
	 * @param bindingResult		-	{@link BindingResult}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 * @return view				-	{@link String}
	 * @throws GGASException	-	{@link GGASException}
	 */
	@RequestMapping(EXIBIR_ALTERAR_ASSOCIACAO_FUNCOES_INTEGRACAO)
	public String exibirAlterarAssociacaoFuncoesIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = EXIBIR_ALTERAR_ASSOCIACAO_FUNCOES_INTEGRACAO;
		String operacao = integracaoVO.getOperacao();
		Long chavePrimaria = integracaoVO.getChavePrimaria();

		if (("limpar").equalsIgnoreCase(operacao)) {
			limparVariaveisSessao(request, model);
		}

		try {

			if (chavePrimaria != null && chavePrimaria > 0) {

				IntegracaoSistemaFuncao integracaoSistemaFuncao = controladorIntegracao
						.obterIntegracaoSistemaFuncao(chavePrimaria);
				model.addAttribute(INTEGRACAO_SISTEMA_FUNCAO, integracaoSistemaFuncao);

				integracaoVO.setIntegracaoSistemaFuncao(chavePrimaria);
				integracaoVO.setIntegracaoFuncao(integracaoSistemaFuncao.getIntegracaoFuncao().getChavePrimaria());
				integracaoVO.setIntegracaoSistema(integracaoSistemaFuncao.getIntegracaoSistema().getChavePrimaria());
				integracaoVO.setHabilitado(integracaoSistemaFuncao.isHabilitado());
				integracaoVO.setIndicadorEnviaEmailErro(integracaoSistemaFuncao.getIndicadorEnviaEmailErro());
				integracaoVO.setDestinatariosEmail(integracaoSistemaFuncao.getDestinatariosEmail());

				if (integracaoVO.getIntegracaoFuncao() != null) {
					this.exibirDetalhamentoAbaParametro(integracaoVO, model);
					this.exibirDadosAbaMapeamento(integracaoSistemaFuncao.getIntegracaoFuncao(), model);
				}

				carregarTodosMapeamentosEmSessao(request, integracaoSistemaFuncao.getIntegracaoFuncao(), chavePrimaria);

				model.addAttribute("integracaoVO", integracaoVO);

			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarIntegracao(integracaoVO, bindingResult, request, model);
		}

		saveToken(request);

		return view;

	}

	/**
	 * Exibir detalhamento aba parametro.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void exibirDetalhamentoAbaParametro(IntegracaoVO integracaoVO, Model model) throws GGASException {

		// bloco para carregar os valores informado na tela nos parametros
		Long[] chavesParametros = integracaoVO.getChaveParametro();
		String[] valores = integracaoVO.getValor();

		IntegracaoSistemaFuncao integracaoSistemaFuncao = controladorIntegracao.obterIntegracaoSistemaFuncao(
				integracaoVO.getIdIntegracaoSistema(), integracaoVO.getIdIntegracaoFuncao());

		List<IntegracaoParametroVO> parametroVOList = new ArrayList<IntegracaoParametroVO>();
		IntegracaoParametroVO parametroVO = null;

		for (IntegracaoSistemaParametro parametro : integracaoSistemaFuncao.getListaIntegracaoSistemaParametro()) {

			parametroVO = new IntegracaoParametroVO();
			parametroVO.setChavePrimaria(parametro.getChavePrimaria());
			parametroVO.setCodigo(parametro.getIntegracaoParametro().getParametroSistema().getCodigo());
			parametroVO.setDescricao(parametro.getIntegracaoParametro().getParametroSistema().getDescricao());
			parametroVO.setValor(parametro.getValor());
			parametroVO.setVersao(parametro.getVersao());
			parametroVOList.add(parametroVO);

		}

		if ((valores != null && chavesParametros != null) && (!parametroVOList.isEmpty())) {
			for (int i = 0; i < valores.length; i++) {
				if (valores[i] != null) {
					popularIntegracaoParametroVO(chavesParametros, valores, parametroVOList, i);
				}
			}
		}

		model.addAttribute("listaParametrosAlteracao", parametroVOList);

	}

	/**
	 * Carregar valores
	 * 
	 * @param chavesParametros	-	{@link Long}
	 * @param valores			-	{@link String}
	 * @param parametroVOList	-	{@link List}
	 * @param indice			-	{@link Integer}
	 */
	private void popularIntegracaoParametroVO(Long[] chavesParametros, String[] valores,
			List<IntegracaoParametroVO> parametroVOList, int indice) {

		Iterator<IntegracaoParametroVO> iterator = parametroVOList.iterator();

		while (iterator.hasNext()) {
			IntegracaoParametroVO integracaoParametroVOIterator = iterator.next();
			if (integracaoParametroVOIterator.getChavePrimaria().equals(chavesParametros[indice])) {
				integracaoParametroVOIterator.setValor(valores[indice]);
			}
		}
	}

	/**
	 * Atualizar lista mapeamento.
	 * 
	 * @param sistemasGGAS			-	{@link String}
	 * @param sistemasIntegrantes	-	{@link String}
	 * @param tabelaMapeamento		-	{@link Long}
	 * @param mapeamento			-	{@link Map}
	 * @param operacao				-	{@link String}
	 * @param model					-	{@link Model}
	 * @return listaMapeamentoNovo	-	{@link List}
	 * @throws GGASException		-	{@link GGASException}
	 */
	private List<IntegracaoMapeamento> atualizarListaMapeamento(String[] sistemasGGAS, String[] sistemasIntegrantes,
			Long tabelaMapeamento, Map<Long, List> mapeamento, String operacao, HttpServletRequest request, Model model) throws GGASException {
		
		List<IntegracaoMapeamento> listaMapeamentoNovo = new ArrayList<IntegracaoMapeamento>();
		Boolean isDadoNumerico = null;
		Boolean isMapeamentoTrocado = Boolean.valueOf(request.getParameter("isMapeamentoTrocado"));

		if (!isMapeamentoTrocado && sistemasGGAS.length > 0 && sistemasIntegrantes.length > 0) {

			for (int i = 0; i < sistemasGGAS.length; i++) {

				isDadoNumerico = true;

				try {

					validadarDadosInformadosTabela(sistemasGGAS, sistemasIntegrantes, operacao, i);

				} catch (FormatoInvalidoException e) {

					mensagemErroParametrizado(model, e);
					isDadoNumerico = false;

				} finally {

					validarItensTabelaAbaMapeamento(sistemasGGAS, sistemasIntegrantes, listaMapeamentoNovo,
							isDadoNumerico, i);

				}

			}

		} else {

			// se esta for a primeira chamada, carregar o array que é exibido na tela com o
			// mapeamento recuperado do banco
			if (mapeamento != null && mapeamento.containsKey(tabelaMapeamento)) {
				listaMapeamentoNovo.addAll(mapeamento.get(tabelaMapeamento));
			}

		}

		removerItensVazios(listaMapeamentoNovo);

		// adiciona um item em branco que será exibido na tela como um novo item a ser
		// preenchido
		listaMapeamentoNovo.add((IntegracaoMapeamento) ServiceLocator.getInstancia()
				.getBeanPorID(IntegracaoMapeamento.BEAN_ID_INTEGRACAO_MAPEAMENTO));

		if (mapeamento != null) {
			mapeamento.put(tabelaMapeamento, listaMapeamentoNovo);
		}

		return listaMapeamentoNovo;

	}

	/**
	 * Verifica se os dados informados nas colunas "GGAS" e "Sistemas Integrantes" são números
	 * 
	 * @param sistemasGGAS				-	{@link String}
	 * @param sistemasIntegrantes		-	{@link String}
	 * @param operacao					-	{@link String}
	 * @param indice					-	{@link String}
	 * @throws FormatoInvalidoException	-	{@link FormatoInvalidoException}
	 */
	private void validadarDadosInformadosTabela(String[] sistemasGGAS, String[] sistemasIntegrantes, String operacao,
			int indice) throws FormatoInvalidoException {

		if (operacao != null && !("removerMapeamento").equals(operacao)) {

			Util.converterCampoStringParaValorLong("da coluna GGAS", sistemasGGAS[indice]);
			Util.converterCampoStringParaValorLong("da coluna Sistema Integrado", sistemasIntegrantes[indice]);

		}
	}

	/**
	 * Verifica se os dados prenchidos na tabela da Aba Mapeamento são vazios
	 * 
	 * @param sistemasGGAS			-	{@link String}
	 * @param sistemasIntegrantes	-	{@link String}
	 * @param listaMapeamentoNovo	-	{@link List}
	 * @param isDadosInvalidos		-	{@link Boolean}
	 * @param indice				-	{@link Integer}
	 */
	private void validarItensTabelaAbaMapeamento(String[] sistemasGGAS, String[] sistemasIntegrantes,
			List<IntegracaoMapeamento> listaMapeamentoNovo, Boolean isDadoNumerico, int indice) {

		if (sistemasGGAS.length > 0 && sistemasIntegrantes.length > 0 && !"".equals(sistemasGGAS[indice])
				&& !"".equals(sistemasIntegrantes[indice])) {

			removerItensDuplicados(sistemasGGAS, sistemasIntegrantes, listaMapeamentoNovo, indice);

			adicionarNovoItem(sistemasGGAS, sistemasIntegrantes, listaMapeamentoNovo, isDadoNumerico, indice);

		}
	}

	/**
	 * Adicionar novo item preenchido na tela para a lista
	 * 
	 * @param sistemasGGAS			-	{@link String}
	 * @param sistemasIntegrantes	-	{@link sistemasIntegrantes}
	 * @param listaMapeamentoNovo	-	{@link List}
	 * @param isDadosInvalidos		-	{@link Boolean}
	 * @param indice				-	{@link Integer}
	 */
	private void adicionarNovoItem(String[] sistemasGGAS, String[] sistemasIntegrantes,
			List<IntegracaoMapeamento> listaMapeamentoNovo, Boolean isDadoNumerico, int indice) {
		if (isDadoNumerico) {

			IntegracaoMapeamento integracao = (IntegracaoMapeamento) ServiceLocator.getInstancia()
					.getBeanPorID(IntegracaoMapeamento.BEAN_ID_INTEGRACAO_MAPEAMENTO);

			integracao.setChavePrimaria(-1);
			integracao.setCodigoEntidadeSistemaGgas(sistemasGGAS[indice]);
			integracao.setCodigoEntidadeSistemaIntegrante(sistemasIntegrantes[indice]);
			listaMapeamentoNovo.add(integracao);
		}
	}

	
	/**
	 * Remove item já adicionado para evitar duplicidade
	 * 
	 * @param sistemasGGAS			-	{@link String}
	 * @param sistemasIntegrantes	-	{@link String}
	 * @param listaMapeamentoNovo	-	{@link List}
	 * @param indice				-	{@link Integer}
	 */
	private void removerItensDuplicados(String[] sistemasGGAS, String[] sistemasIntegrantes,
			List<IntegracaoMapeamento> listaMapeamentoNovo, int indice) {

		Iterator<IntegracaoMapeamento> iterator = listaMapeamentoNovo.iterator();

		while (iterator.hasNext()) {

			IntegracaoMapeamento im = iterator.next();

			if (im.getCodigoEntidadeSistemaGgas() == null || im.getCodigoEntidadeSistemaIntegrante() == null
					|| "".equals(im.getCodigoEntidadeSistemaGgas())
					|| "".equals(im.getCodigoEntidadeSistemaIntegrante())
					|| (im.getCodigoEntidadeSistemaGgas().equals(sistemasGGAS[indice])
							&& im.getCodigoEntidadeSistemaIntegrante().equals(sistemasIntegrantes[indice]))) {
				iterator.remove();
			}
		}
	}
	
	/**
	 * Remove qualquer item da lista que esteja com valores vazios
	 * 
	 * @param listaMapeamentoNovo - {@link List}
	 */
	private void removerItensVazios(List<IntegracaoMapeamento> listaMapeamentoNovo) {

		Iterator<IntegracaoMapeamento> iterator = listaMapeamentoNovo.iterator();

		while (iterator.hasNext()) {
			IntegracaoMapeamento im = iterator.next();
			if (im.getCodigoEntidadeSistemaGgas() == null || "0".equals(im.getCodigoEntidadeSistemaGgas())
					|| im.getCodigoEntidadeSistemaIntegrante() == null
					|| "0".equals(im.getCodigoEntidadeSistemaIntegrante())) {
				iterator.remove();
			}
		}
	}

	/**
	 * Gets the map mapeamentos.
	 * 
	 * @param request				-	{@link HttpServletRequest}
	 * @param idIntegracaoFuncao	-	{@link Long}
	 * @return mapeamento			-	{@link Map}
	 * @throws NegocioException		-	{@link NegocioException}
	 */
	private Map<Long, List> getMapMapeamentos(HttpServletRequest request, Long idIntegracaoFuncao)
			throws NegocioException {

		if ((HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS) != null) {
			return (HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS);
		} else {

			List<IntegracaoMapeamento> listaIntegracaoMapeamento;
			Map<Long, List> mapeamento = new HashMap<Long, List>();

			if (idIntegracaoFuncao != null && idIntegracaoFuncao > 0) {

				listaIntegracaoMapeamento = (List<IntegracaoMapeamento>) controladorIntegracao
						.localizarIntegracaoMapeamento(idIntegracaoFuncao);

				popularMapa(listaIntegracaoMapeamento, mapeamento);

			}

			request.getSession().setAttribute(MAPEAMENTOS, mapeamento);
			saveToken(request);

			return mapeamento;

		}

	}

	/**
	 * Popula mapa com lista de Integracao Mapeamento
	 * 
	 * @param listaIntegracaoMapeamento	-	{@link List}
	 * @param mapeamento				-	{@link Map}
	 */
	private void popularMapa(List<IntegracaoMapeamento> listaIntegracaoMapeamento, Map<Long, List> mapeamento) {

		for (IntegracaoMapeamento integracaoMapeamento : listaIntegracaoMapeamento) {

			if (mapeamento.containsKey(integracaoMapeamento.getTabela().getChavePrimaria())) {

				List<IntegracaoMapeamento> lista = mapeamento.get(integracaoMapeamento.getTabela().getChavePrimaria());
				lista.add(integracaoMapeamento);
				mapeamento.put(integracaoMapeamento.getTabela().getChavePrimaria(), lista);
			} else {
				List<IntegracaoMapeamento> lista = new ArrayList<IntegracaoMapeamento>();
				lista.add(integracaoMapeamento);
				mapeamento.put(integracaoMapeamento.getTabela().getChavePrimaria(), lista);
			}

		}
	}

	/**
	 * Verifica Lista Integração Parametro
	 * 
	 * @param listaIntegracaoParametro	-	{@link Collection}
	 * @param chavesParametros			-	{@link Long}
	 * @param valores					-	{@link String}
	 * @param model						-	{@link Model}
	 * @throws GGASException			-	{@link GGASException}
	 */
	private void verificaListaIntegracaoParametro(Collection<IntegracaoParametro> listaIntegracaoParametro,
			Long[] chavesParametros, String[] valores, Model model) throws GGASException {

		if ((valores != null && chavesParametros != null)
				&& (listaIntegracaoParametro != null && !listaIntegracaoParametro.isEmpty())) {
			for (int i = 0; i < valores.length; i++) {
				if (valores[i] != null) {
					iterarListaIntegracaoParametro(listaIntegracaoParametro, chavesParametros, valores, model, i);
				}
			}
		}
	}

	/**
	 * Percorrer lista de Integracao Parametro
	 * 
	 * @param listaIntegracaoParametro	-	{@link Collection}
	 * @param chavesParametros			-	{@link Long}
	 * @param valores					-	{@link String}
	 * @param model						-	{@link Model}
	 * @param indice					-	{@link Integer}
	 * @throws GGASException			-	{@link GGASException}
	 */
	private void iterarListaIntegracaoParametro(Collection<IntegracaoParametro> listaIntegracaoParametro,
			Long[] chavesParametros, String[] valores, Model model, int indice) throws GGASException {

		Iterator<IntegracaoParametro> iterator = listaIntegracaoParametro.iterator();

		while (iterator.hasNext()) {
			IntegracaoParametro integracaoParametroIterator = iterator.next();
			if (integracaoParametroIterator.getChavePrimaria() == chavesParametros[indice]) {

				validarValoresTabelaAbaParametro(valores, indice, integracaoParametroIterator, model);
			}
		}
	}

	/**
	 * Verifica se o valor do parametro informado é válido
	 * 
	 * @param valores						-	{@link String}
	 * @param indice						-	{@link Integer}
	 * @param integracaoParametroIterator	-	{@link IntegracaoParametro}
	 * @param model							-	{@link Model}
	 * @throws GGASException				-	{@link GGASException}
	 */
	private void validarValoresTabelaAbaParametro(String[] valores, int indice,
			IntegracaoParametro integracaoParametroIterator, Model model) throws GGASException {

		try {

			if (integracaoParametroIterator.getParametroSistema().getTabela() == null) {
				integracaoParametroIterator.getParametroSistema().setValor(valores[indice]);
			} else {

				Tabela tabela = controladorAuditoria
						.obterTabela(integracaoParametroIterator.getParametroSistema().getTabela().getChavePrimaria());

				String nomeClasse = tabela.getNomeClasse();
				nomeClasse = nomeClasse.substring(0, nomeClasse.lastIndexOf('.')) + ".impl"
						+ nomeClasse.substring(nomeClasse.lastIndexOf('.'), nomeClasse.length()) + "Impl";

				verificarObjetoClasseAssociada(valores, indice, integracaoParametroIterator, nomeClasse);
			}

		} catch (NumberFormatException e) {
			mensagemErroParametrizado(model, new GGASException(e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

	}

	/**
	 * Verifica objeto classe associado com o nome da string especificada
	 * 
	 * @param valores						-	{@link String}
	 * @param indice						-	{@link Integer}
	 * @param integracaoParametroIterator	-	{@link IntegracaoParametro}
	 * @param nomeClasse					-	{@link String}
	 * @throws GGASException				-	{@link GGASException}
	 */
	private void verificarObjetoClasseAssociada(String[] valores, int indice,
			IntegracaoParametro integracaoParametroIterator, String nomeClasse) throws GGASException {

		try {

			if (Class.forName(nomeClasse).equals(EntidadeConteudoImpl.class)) {

				Long idClasseEntidade = 0L;

				if (integracaoParametroIterator.getParametroSistema().getClasseEntidade() != null) {
					idClasseEntidade = Long
							.valueOf(integracaoParametroIterator.getParametroSistema().getClasseEntidade());
				}

				if (controladorEntidadeConteudo.obterEntidadeConteudoPorChave(Long.valueOf(valores[indice]),
						idClasseEntidade) == null) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO,
							integracaoParametroIterator.getParametroSistema().getCodigo());
				}

			} else {

				if (controladorIntegracao.obterEntidadePorClasse(Long.valueOf(valores[indice]),
						Class.forName(nomeClasse)) == null) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO,
							integracaoParametroIterator.getParametroSistema().getCodigo());
				}
			}

		} catch (ClassNotFoundException e) {
			throw new GGASException(e);
		}
	}

	/**
	 * Procurar mapeamentos por tabela.
	 * 
	 * @param chavePrimariaTabela			-	{@link Long}
	 * @param idSistemaFuncao				-	{@link Long}
	 * @param request						-	{@link HttpServletRequest}
	 * @return integracaoMapeamentoTabela	-	{@link List}
	 * @throws NegocioException				-	{@link NegocioException}
	 */
	public List<IntegracaoMapeamento> procurarMapeamentosPorTabela(Long chavePrimariaTabela, Long idSistemaFuncao,
			HttpServletRequest request) throws NegocioException {

		List<IntegracaoMapeamento> listaIntegracaoMapeamento;

		if ((HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS) == null) {

			Map<Long, List> mapeamento = new HashMap<Long, List>();

			if (idSistemaFuncao != null && idSistemaFuncao > 0) {

				listaIntegracaoMapeamento = (List<IntegracaoMapeamento>) controladorIntegracao
						.localizarIntegracaoMapeamento(idSistemaFuncao);

				popularMapa(listaIntegracaoMapeamento, mapeamento);

			}

			request.getSession().setAttribute(MAPEAMENTOS, mapeamento);

		}

		listaIntegracaoMapeamento = (List<IntegracaoMapeamento>) controladorIntegracao
				.localizarIntegracaoMapeamento(idSistemaFuncao);

		List<IntegracaoMapeamento> integracaoMapeamentoTabela = new ArrayList<IntegracaoMapeamento>();

		if (listaIntegracaoMapeamento != null) {
			for (IntegracaoMapeamento integracaoMapeamento : listaIntegracaoMapeamento) {
				if (integracaoMapeamento.getTabela().getChavePrimaria() == chavePrimariaTabela) {
					integracaoMapeamentoTabela.add(integracaoMapeamento);
				}
			}
		}

		saveToken(request);

		return integracaoMapeamentoTabela;

	}

	/**
	 * Exibir aba mapeamento.
	 * 
	 * @param integracaoVO						-	{@link IntegracaoVO}
	 * @param bindingResult						-	{@link BindingResult}
	 * @param request							-	{@link HttpServletRequest}
	 * @param model								-	{@link Model}
	 * @return exibirAssociacaoFuncaoIntegracao	-	{@link String}
	 * @throws GGASException					-	{@link GGASException}
	 */
	@RequestMapping("exibirAbaMapeamento")
	public String exibirAbaMapeamento(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		String operacao = integracaoVO.getOperacao();
		Long chavePrimariaMapeamento = integracaoVO.getChavePrimariaItemMapeamento();
		Long idIntegracaoFuncao = integracaoVO.getIdIntegracaoFuncao();

		if (EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO.equals(integracaoVO.getAcao())) {
			carregarCombos(model);
			idIntegracaoFuncao = integracaoVO.getIntegracaoFuncao();
		}

		try {

			// recupera as informações de mapeamento que foram substituidas na tela sem ter
			// ação de botão
			atualizarListaMapeamentoPorMudancaTela(integracaoVO, request, model);

			model.addAttribute(CHAVE_PRIMARIA_ITEM_MAPEAMENTO, chavePrimariaMapeamento);

			if (idIntegracaoFuncao != null) {
				IntegracaoFuncao integracaoFuncao = (IntegracaoFuncao) controladorIntegracao.criarIntegracaoFuncao();
				integracaoFuncao.setChavePrimaria(idIntegracaoFuncao);

				if (EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO.equals(integracaoVO.getAcao())) {
					this.exibirDadosAbaParametro(integracaoVO, integracaoFuncao, model);
				} else {
					this.exibirDetalhamentoAbaParametro(integracaoVO, model);
				}

				this.exibirDadosAbaMapeamento(integracaoFuncao, model);
			}

			String[] sistemasGGAS = integracaoVO.getCodigoEntidadeSistemaGgas();
			String[] sistemasIntegrantes = integracaoVO.getCodigoEntidadeSistemaIntegrante();
			
			if (operacao != null && EXIBIR.equals(operacao)) {
				sistemasGGAS = new String[] {};
				sistemasIntegrantes = new String[] {};
			}

			if (chavePrimariaMapeamento != null && chavePrimariaMapeamento > 0) {

				Map<Long, List> mapeamento = getMapMapeamentos(request, idIntegracaoFuncao);

				if (EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO.equals(integracaoVO.getAcao()) && operacao != null
						&& EXIBIR.equals(operacao) && mapeamento.containsKey(chavePrimariaMapeamento)
						&& !(mapeamento.get(chavePrimariaMapeamento).isEmpty())) {

					model.addAttribute(LISTA_MAPEAMENTO, mapeamento.get(chavePrimariaMapeamento));

				} else if (operacao != null && EXIBIR.equals(operacao)
						&& EXIBIR_ALTERAR_ASSOCIACAO_FUNCOES_INTEGRACAO.equals(integracaoVO.getAcao())) {

					List<IntegracaoMapeamento> integracaoMapeamentoList = procurarMapeamentosPorTabela(
							chavePrimariaMapeamento, integracaoVO.getChavePrimaria(), request);

					mapeamento = (HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS);

					if (mapeamento != null) {
						if (mapeamento.containsKey(chavePrimariaMapeamento)) {
							integracaoMapeamentoList = mapeamento.get(chavePrimariaMapeamento);
						} else {
							mapeamento.put(chavePrimariaMapeamento, integracaoMapeamentoList);
						}
					}

					if (mapeamento != null) {
						Iterator<IntegracaoMapeamento> it = mapeamento.get(chavePrimariaMapeamento).iterator();
						while (it.hasNext()) {
							IntegracaoMapeamento im = it.next();
							if (im.getCodigoEntidadeSistemaGgas() == null
									|| "".equals(im.getCodigoEntidadeSistemaGgas())
									|| im.getCodigoEntidadeSistemaIntegrante() == null
									|| "".equals(im.getCodigoEntidadeSistemaIntegrante())) {
								it.remove();
							}
						}

						mapeamento.get(chavePrimariaMapeamento).add(ServiceLocator.getInstancia()
								.getBeanPorID(IntegracaoMapeamento.BEAN_ID_INTEGRACAO_MAPEAMENTO));
					}

					request.getSession().setAttribute(MAPEAMENTOS, mapeamento);
					model.addAttribute(LISTA_MAPEAMENTO, integracaoMapeamentoList);

				} else if (operacao != null && "removerMapeamento".equals(operacao)) {

					removerMapeamento(integracaoVO, request, model, chavePrimariaMapeamento, mapeamento);

				} else {

					List<IntegracaoMapeamento> listaMapeamento = atualizarListaMapeamento(sistemasGGAS,
							sistemasIntegrantes, chavePrimariaMapeamento, mapeamento, operacao, request, model);

					model.addAttribute(LISTA_MAPEAMENTO, listaMapeamento);
					request.getSession().setAttribute(MAPEAMENTOS, mapeamento);

				}

			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		saveToken(request);

		if (EXIBIR_ASSOCIACAO_FUNCAO_INTEGRACAO.equals(integracaoVO.getAcao())) {
			view = exibirAssociacaoFuncaoIntegracao(integracaoVO, bindingResult, request, model);
		} else {
			view = exibirAlterarAssociacaoFuncoesIntegracao(integracaoVO, bindingResult, request, model);
		}

		return view;

	}

	/**
	 * Remove Mapeamento
	 * 
	 * @param integracaoVO				-	{@link IntegracaoVO}
	 * @param request					-	{@link HttpServletRequest}
	 * @param model						-	{@link Model}
	 * @param chavePrimariaMapeamento	-	{@link Long}
	 * @param mapeamento				-	{@link List}
	 */
	private void removerMapeamento(IntegracaoVO integracaoVO, HttpServletRequest request, Model model,
			Long chavePrimariaMapeamento, Map<Long, List> mapeamento) {

		Long codigoEntidadeSistemaIntegrante = integracaoVO.getCodigoEntidadeSistemaIntegranteRemocao();
		Long codigoEntidadeSistemaGgas = integracaoVO.getCodigoEntidadeSistemaGgasRemocao();

		List<IntegracaoMapeamento> listaMapeamento = mapeamento.get(chavePrimariaMapeamento);

		Iterator<IntegracaoMapeamento> iterator = listaMapeamento.iterator();

		while (iterator.hasNext()) {

			IntegracaoMapeamento im = iterator.next();

			if (codigoEntidadeSistemaGgas != null && codigoEntidadeSistemaIntegrante != null
					&& im.getCodigoEntidadeSistemaGgas() != null && im.getCodigoEntidadeSistemaIntegrante() != null
					&& codigoEntidadeSistemaGgas.equals(Long.valueOf(im.getCodigoEntidadeSistemaGgas()))
					&& codigoEntidadeSistemaIntegrante.equals(Long.valueOf(im.getCodigoEntidadeSistemaIntegrante()))) {
				iterator.remove();
			}

		}

		request.getSession().setAttribute(MAPEAMENTOS, mapeamento);
		model.addAttribute(LISTA_MAPEAMENTO, listaMapeamento);
	}

	/**
	 * Salvar alteracao integracao.
	 * 
	 * @param integracaoVO								-	{@link IntegracaoVO}
	 * @param bindingResult								-	{@link BindingResult}
	 * @param request									-	{@link HttpServletRequest}
	 * @param model										-	{@link Model}
	 * @return exibirAlterarAssociacaoFuncoesIntegracao	-	{@link String}
	 * @throws GGASException							-	{@link GGASException}
	 */
	@RequestMapping("salvarAlteracaoIntegracao")
	public String salvarAlteracaoIntegracao(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		// carrega os dados de parametros do banco e atualiza os valores
		Long[] chavesParametros = integracaoVO.getChaveParametro();
		String[] valores = integracaoVO.getValor();

		try {

			validarToken(request);

			// recupera as informações de mapeamento que foram substituidas na tela sem ter
			// ação de botão
			atualizarListaMapeamentoPorMudancaTela(integracaoVO, request, model);

			request.getParameter(OPERACAO);

			Long chavePrimaria = integracaoVO.getChavePrimaria();
			model.addAttribute(ID_INTEGRACAO_SISTEMAS, chavePrimaria);

			IntegracaoSistemaFuncao integracaoSistemaFuncao = controladorIntegracao
					.obterIntegracaoSistemaFuncao(chavePrimaria);
			model.addAttribute(INTEGRACAO_SISTEMA_FUNCAO, integracaoSistemaFuncao);

			integracaoSistemaFuncao.setHabilitado(integracaoVO.getHabilitado());
			integracaoSistemaFuncao.setUltimaAlteracao(new Date());

			Long indicadorEnviaEmailErro = integracaoVO.getIndicadorEnviaEmailErro();

			integracaoSistemaFuncao.setIndicadorEnviaEmailErro(indicadorEnviaEmailErro);

			if (indicadorEnviaEmailErro.equals(1L)) {

				if (StringUtils.isEmpty(integracaoVO.getDestinatariosEmail())) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_INFORMAR_EMAIL, true);
				} else {
					integracaoSistemaFuncao.setDestinatariosEmail(integracaoVO.getDestinatariosEmail());
					controladorIntegracao.validarDestinatariosEmail(integracaoSistemaFuncao);
				}

			} else {
				integracaoSistemaFuncao.setDestinatariosEmail(null);
			}

			controladorIntegracao.atualizar(integracaoSistemaFuncao);
			controladorIntegracao.desabilitarRecursosSistema(integracaoSistemaFuncao.isHabilitado(),
					integracaoSistemaFuncao.getChavePrimaria());

			Collection<IntegracaoSistemaParametro> listaIntegracaoSistemaParametro = integracaoSistemaFuncao
					.getListaIntegracaoSistemaParametro();

			if ((valores != null && chavesParametros != null)
					&& (listaIntegracaoSistemaParametro != null && !listaIntegracaoSistemaParametro.isEmpty())) {

				for (int i = 0; i < valores.length; i++) {
					if (valores[i] != null) {
						iterarListaIntegracaoSistemaParametroFluxoAlterar(chavesParametros, valores,
								listaIntegracaoSistemaParametro, i, model);
					}
				}
			}

			// carrega e insere os dados dos mapeamentos
			Collection<IntegracaoMapeamento> listaIntegracaoMapeamento = controladorIntegracao
					.localizarIntegracaoMapeamento(integracaoSistemaFuncao.getChavePrimaria());
			controladorIntegracao.removerIntegracaoMapeamento(listaIntegracaoMapeamento);

			Map<Long, List> mapeamentos = getMapMapeamentos(request,
					integracaoSistemaFuncao.getIntegracaoFuncao().getChavePrimaria());
			Iterator iterator = mapeamentos.entrySet().iterator();

			while (iterator.hasNext()) {
				Map.Entry mapeamento = (Map.Entry) iterator.next();
				List<IntegracaoMapeamento> integracaoMapeamentoList = (List<IntegracaoMapeamento>) mapeamento
						.getValue();
				Tabela tabela = controladorAuditoria.obterTabela((Long) mapeamento.getKey());
				carregarInsereDadosMapeamentosFluxoAlterar(integracaoSistemaFuncao, integracaoMapeamentoList, tabela);
			}

			limparVariaveisSessao(request, model);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					IntegracaoSistemaFuncao.INTEGRACAO_SISTEMA_FUNCAO);

			model.addAttribute(HABILITADO, "");

			view = exibirPesquisaIntegracao(integracaoVO, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlterarAssociacaoFuncoesIntegracao(integracaoVO, bindingResult, request, model);

		}

		return view;

	}

	/**
	 * @param integracaoSistemaFuncao	-	{@link IntegracaoSistemaFuncao}
	 * @param integracaoMapeamentoList	-	{@link List}
	 * @param tabela					-	{@link Tabela}
	 * @throws NegocioException			-	{@link NegocioException}
	 */
	private void carregarInsereDadosMapeamentosFluxoAlterar(IntegracaoSistemaFuncao integracaoSistemaFuncao,
			List<IntegracaoMapeamento> integracaoMapeamentoList, Tabela tabela) throws NegocioException {

		for (IntegracaoMapeamento integracaoMapeamento : integracaoMapeamentoList) {
			if (integracaoMapeamento.getCodigoEntidadeSistemaGgas() != null) {
				integracaoMapeamento.setIntegracaoSistema(integracaoSistemaFuncao.getIntegracaoSistema());
				integracaoMapeamento.setTabela(tabela);
				controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);
			}
		}
	}

	/**
	 * Percorre lista de Integracao Parametro
	 * 
	 * @param chavesParametros	
	 * @param valores
	 * @param listaIntegracaoSistemaParametro	-	{@link Collection}
	 * @param indice							-	{@link Integer}
	 * @param model								-	{@link Model}
	 * @throws GGASException					-	{@link GGASException}
	 */
	private void iterarListaIntegracaoSistemaParametroFluxoAlterar(Long[] chavesParametros, String[] valores,
			Collection<IntegracaoSistemaParametro> listaIntegracaoSistemaParametro, int indice, Model model)
			throws GGASException {

		Iterator<IntegracaoSistemaParametro> iterator = listaIntegracaoSistemaParametro.iterator();

		while (iterator.hasNext()) {
			IntegracaoSistemaParametro integracaoSistemaParametroIterator = iterator.next();
			if (integracaoSistemaParametroIterator.getChavePrimaria() == chavesParametros[indice]) {

				validarValoresTabelaAbaParametroFluxoAlterar(valores, indice, integracaoSistemaParametroIterator,
						model);

				integracaoSistemaParametroIterator.setValor(valores[indice]);
				controladorIntegracao.atualizarIntegracaoSistemaParametro(integracaoSistemaParametroIterator);

			}
		}
	}

	/**
	 * Verifica se o valor do parametro informado é válido
	 * 
	 * @param valores
	 * @param indice
	 * @param integracaoSistemaParametroIterator
	 * @throws GGASException
	 */
	private void validarValoresTabelaAbaParametroFluxoAlterar(String[] valores, int indice,
			IntegracaoSistemaParametro integracaoSistemaParametroIterator, Model model) throws GGASException {

		try {

			if (integracaoSistemaParametroIterator.getIntegracaoParametro().getParametroSistema().getTabela() != null) {

				Tabela tabela = controladorAuditoria.obterTabela(integracaoSistemaParametroIterator
						.getIntegracaoParametro().getParametroSistema().getTabela().getChavePrimaria());

				String nomeClasse = tabela.getNomeClasse();
				nomeClasse = nomeClasse.substring(0, nomeClasse.lastIndexOf('.')) + ".impl"
						+ nomeClasse.substring(nomeClasse.lastIndexOf('.'), nomeClasse.length()) + "Impl";

				if (Class.forName(nomeClasse).equals(EntidadeConteudoImpl.class)) {

					Long idClasseEntidade = 0L;
					if (integracaoSistemaParametroIterator.getIntegracaoParametro().getParametroSistema()
							.getClasseEntidade() != null) {
						idClasseEntidade = Long.valueOf(integracaoSistemaParametroIterator.getIntegracaoParametro()
								.getParametroSistema().getClasseEntidade());
					}

					if (controladorEntidadeConteudo.obterEntidadeConteudoPorChave(Long.valueOf(valores[indice]),
							idClasseEntidade) == null) {
						throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO,
								integracaoSistemaParametroIterator.getIntegracaoParametro().getParametroSistema()
										.getCodigo());
					}

				} else {

					if (controladorIntegracao.obterEntidadePorClasse(Long.valueOf(valores[indice]),
							Class.forName(nomeClasse)) == null) {

						throw new NegocioException(Constantes.ERRO_NEGOCIO_INTEGRACAO_ASSOCIACAO_PARAMETRO_INVALIDO,
								integracaoSistemaParametroIterator.getIntegracaoParametro().getParametroSistema()
										.getCodigo());
					}
				}
			}

		} catch (NumberFormatException e) {
			mensagemErroParametrizado(model, new GGASException(e));
		} catch (ClassNotFoundException e) {
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

	}

	/**
	 * Exibir aba mapeamento detalhamento.
	 * 
	 * @param integracaoVO										-	{@link IntegracaoVO}
	 * @param bindingResult										-	{@link BindingResult}
	 * @param request											-	{@link HttpServletRequest}
	 * @param model												-	{@link Model}
	 * @return exibirDetalhamentoAssociacaoFuncoesIntegracao	-	{@link String}
	 * @throws GGASException									-	{@link GGASException}
	 */
	@RequestMapping("exibirAbaMapeamentoDetalhamento")
	public String exibirAbaMapeamentoDetalhamento(IntegracaoVO integracaoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		request.getParameter(OPERACAO);

		Long chavePrimaria = integracaoVO.getChavePrimaria();
		model.addAttribute(ID_INTEGRACAO_SISTEMAS, chavePrimaria);
		model.addAttribute("detalhamento", 1L);

		try {

			IntegracaoSistemaFuncao integracaoSistemaFuncao = controladorIntegracao
					.obterIntegracaoSistemaFuncao(chavePrimaria);
			model.addAttribute(INTEGRACAO_SISTEMA_FUNCAO, integracaoSistemaFuncao);

			integracaoVO.setIntegracaoFuncao(integracaoSistemaFuncao.getIntegracaoFuncao().getChavePrimaria());
			integracaoVO.setIntegracaoSistema(integracaoSistemaFuncao.getIntegracaoSistema().getChavePrimaria());

			if (integracaoVO.getIntegracaoFuncao() != null) {
				this.exibirDetalhamentoAbaParametro(integracaoVO, model);
				this.exibirDadosAbaMapeamento(integracaoSistemaFuncao.getIntegracaoFuncao(), model);
			}

			if (integracaoSistemaFuncao.isHabilitado()) {
				model.addAttribute(HABILITADO, true);
			} else {
				model.addAttribute(HABILITADO, false);
			}

			model.addAttribute(DESTINATARIOS_EMAIL, integracaoSistemaFuncao.getDestinatariosEmail());
			model.addAttribute(INDICADOR_ENVIA_EMAIL_ERRO, integracaoSistemaFuncao.getIndicadorEnviaEmailErro());

			Long chavePrimariaMapeamento = integracaoVO.getChavePrimariaItemMapeamento();
			model.addAttribute(CHAVE_PRIMARIA_ITEM_MAPEAMENTO, chavePrimariaMapeamento);

			if (chavePrimariaMapeamento != null && chavePrimariaMapeamento > 0) {

				List<IntegracaoMapeamento> integracaoMapeamentoList = procurarMapeamentosPorTabela(
						chavePrimariaMapeamento, chavePrimaria, request);

				Map<Long, List> mapeamento = (HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS);

				if (mapeamento != null) {
					if (mapeamento.containsKey(chavePrimariaMapeamento)) {
						integracaoMapeamentoList = mapeamento.get(chavePrimariaMapeamento);
					} else {
						mapeamento.put(chavePrimariaMapeamento, integracaoMapeamentoList);
					}
				}

				request.getSession().setAttribute(MAPEAMENTOS, mapeamento);
				model.addAttribute(LISTA_MAPEAMENTO, integracaoMapeamentoList);

			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		saveToken(request);

		return exibirDetalhamentoAssociacaoFuncoesIntegracao(integracaoVO, bindingResult, request, model);

	}

	/**
	 * Limpar variaveis sessao.
	 * 
	 * @param request	-	{@link HttpServletRequest}
	 * @param model		-	{@link Model}
	 */
	public void limparVariaveisSessao(HttpServletRequest request, Model model) {

		// remove as listas de mapeamento e parametros, só serão carregadas no ato da
		// seleção de uma função na tela.

		model.asMap().remove(LISTA_INTEGRACAO_SISTEMA_FUNCAO);
		model.asMap().remove(ID_INTEGRACAO_SISTEMAS);

		model.asMap().remove(LISTA_MAPEAMENTOS);
		model.asMap().remove(LISTA_PARAMENTROS);

		request.getSession().removeAttribute(MAPEAMENTOS);

	}

	/**
	 * Carregar todos mapeamentos em sessao.
	 * 
	 * @param request					-	{@link HttpServletRequest}
	 * @param integracaoFuncao			-	{@link IntegracaoFuncao}
	 * @param idIntegracaoSistemaFuncao	-	{@link Long}
	 * @throws GGASException			-	{@link GGASException}
	 */
	private void carregarTodosMapeamentosEmSessao(HttpServletRequest request, IntegracaoFuncao integracaoFuncao,
			Long idIntegracaoSistemaFuncao) throws GGASException {

		Collection<IntegracaoFuncaoTabela> listaIntegracaoFuncaoTabela = controladorIntegracao
				.listarMapeamentosAssociados(integracaoFuncao);

		for (IntegracaoFuncaoTabela intFunTabela : listaIntegracaoFuncaoTabela) {

			Long chavePrimariaMapeamento = intFunTabela.getTabela().getChavePrimaria();

			List<IntegracaoMapeamento> integracaoMapeamentoList = procurarMapeamentosPorTabela(chavePrimariaMapeamento,
					idIntegracaoSistemaFuncao, request);

			Map<Long, List> mapeamento = (HashMap<Long, List>) request.getSession().getAttribute(MAPEAMENTOS);

			if (mapeamento != null) {
				if (mapeamento.containsKey(chavePrimariaMapeamento)) {
					integracaoMapeamentoList = mapeamento.get(chavePrimariaMapeamento);
				} else {
					mapeamento.put(chavePrimariaMapeamento, integracaoMapeamentoList);
				}
			}

			request.getSession().setAttribute(MAPEAMENTOS, mapeamento);

		}

	}

	/**
	 * Atualizar lista mapeamento por mudanca tela.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param request			-	{@link HttpServletRequest}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void atualizarListaMapeamentoPorMudancaTela(IntegracaoVO integracaoVO, HttpServletRequest request,
			Model model) throws GGASException {

		Long idIntegracaoFuncao = integracaoVO.getIdIntegracaoFuncao();
		Long chavePrimariaMapeamento = integracaoVO.getChavePrimariaItemMapeamentoAnterior();

		String[] sistemasGGAS = integracaoVO.getCodigoEntidadeSistemaGgas();
		String[] sistemasIntegrantes = integracaoVO.getCodigoEntidadeSistemaIntegrante();
		
		if (chavePrimariaMapeamento != null && chavePrimariaMapeamento > 0) {
			Map<Long, List> mapeamento = getMapMapeamentos(request, idIntegracaoFuncao);

			List<IntegracaoMapeamento> listaMapeamento = atualizarListaMapeamento(sistemasGGAS, sistemasIntegrantes,
					chavePrimariaMapeamento, mapeamento, integracaoVO.getOperacao(), request, model);

			model.addAttribute(LISTA_MAPEAMENTO, listaMapeamento);
			request.getSession().setAttribute(MAPEAMENTOS, mapeamento);

		}

	}
	
	/**
	 * Exibir dados aba parametro.
	 * 
	 * @param integracaoVO		-	{@link IntegracaoVO}
	 * @param integracaoFuncao	-	{@link IntegracaoFuncao}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void exibirDadosAbaParametro(IntegracaoVO integracaoVO, IntegracaoFuncao integracaoFuncao, Model model)
			throws GGASException {

		Collection<IntegracaoParametro> listaIntegracaoParametro = controladorIntegracao
				.listarParametrosAssociados(integracaoFuncao);

		Long[] chavesParametros = integracaoVO.getChaveParametro();
		String[] valores = integracaoVO.getValor();

		if ((valores != null && chavesParametros != null)
				&& (listaIntegracaoParametro != null && !listaIntegracaoParametro.isEmpty())) {

			for (int i = 0; i < valores.length; i++) {

				if (valores[i] != null) {

					Iterator<IntegracaoParametro> iterator = listaIntegracaoParametro.iterator();
					while (iterator.hasNext()) {
						IntegracaoParametro integracaoParametro = iterator.next();
						if (integracaoParametro.getChavePrimaria() == chavesParametros[i]) {
							integracaoParametro.getParametroSistema().setValor(valores[i]);
						}
					}
				}
			}
		}

		model.addAttribute(LISTA_PARAMENTROS, listaIntegracaoParametro);

	}

	/**
	 * Exibir dados aba mapeamento.
	 * 
	 * @param integracaoFuncao	-	{@link IntegracaoFuncao}
	 * @param model				-	{@link Model}
	 * @throws GGASException	-	{@link GGASException}
	 */
	private void exibirDadosAbaMapeamento(IntegracaoFuncao integracaoFuncao, Model model) throws GGASException {

		Collection<IntegracaoFuncaoTabela> listaIntegracaoFuncaoTabela = controladorIntegracao
				.listarMapeamentosAssociados(integracaoFuncao);
		model.addAttribute(LISTA_MAPEAMENTOS, listaIntegracaoFuncaoTabela);

	}
	
	/**
	 * Metódo responsável por tratar os filtors da tela de
	 * pesquisar integracao.
	 * 
	 * @param integracaoVO				-	{@link IntegracaoVO}
	 * @return filtro					-	{@link Map}
	 * @throws FormatoInvalidoException	-	{@link FormatoInvalidoException}
	 */
	private Map<String, Object> prepararFiltro(IntegracaoVO integracaoVO) throws FormatoInvalidoException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Boolean habilitado = integracaoVO.getHabilitado();
		String destinatariosEmail = integracaoVO.getDestinatariosEmail();

		Long chavePrimaria = integracaoVO.getChavePrimaria();
		Long integracaoSistema = integracaoVO.getIntegracaoSistema();
		Long integracaoFuncao = integracaoVO.getIntegracaoFuncao();
		Long integracaoSistemaFuncao = integracaoVO.getIntegracaoSistemaFuncao();
		Long indicadorEnviaEmailErro = integracaoVO.getIndicadorEnviaEmailErro();
		Long idItegracaoSistema = integracaoVO.getIdIntegracaoSistema();

		if (chavePrimaria != null) {
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		}

		if (integracaoSistema != null) {
			filtro.put(INTEGRACAO_SISTEMA, integracaoSistema);
		}

		if (integracaoFuncao != null) {
			filtro.put(INTEGRACAO_FUNCAO, integracaoFuncao);
		}

		if (integracaoSistemaFuncao != null) {
			filtro.put(INTEGRACAO_SISTEMA_FUNCAO, integracaoSistemaFuncao);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		filtro.put(INDICADOR_ENVIA_EMAIL_ERRO, indicadorEnviaEmailErro);

		if (!StringUtils.isEmpty(destinatariosEmail)) {
			filtro.put(DESTINATARIOS_EMAIL, destinatariosEmail);
		}

		if (integracaoSistema != null) {
			filtro.put(ID_INTEGRACAO_SISTEMA, idItegracaoSistema);
		}

		return filtro;

	}

}
