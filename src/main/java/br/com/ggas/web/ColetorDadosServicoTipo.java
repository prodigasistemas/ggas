package br.com.ggas.web;

import java.util.Map;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

import com.google.common.base.Function;

/**
 * Classe reponsável por extrair um {@link Map} de dados a partir 
 * do {@link ServicoTipo}. Os nomes e os valores dos atributos
 * presentes em {@link ServicoTipo} são usados no preenchimento.
 * A operação desta classe está sujeita a {@link NullPointerException}
 * caso um valor nulo seja passado como argumento.
 */
public class ColetorDadosServicoTipo implements Function<ServicoTipo, Map<String, String>> {

	@Override
	public Map<String, String> apply(ServicoTipo servicoTipo) {
		ConstrutorMapaServicoTipo construtor = new ConstrutorMapaServicoTipo(servicoTipo);
		return construtor.preencherChavePrimaria()
						.preencherDescricao()
						.preencherPrioridade()
						.preencherIndicadorRegulamentacao()
						.construir();
	}

}
