/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.auditoria;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.TrilhaAuditoriaUtil;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a Auditoria de Tabelas.
 */
@Controller
public class AuditoriaAction extends GenericAction {

	private static final String TABELA_SEM_COLUNAS = "tabelaSemColunas";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String ID_TABELA = "idTabela";
	
	@Autowired
	private ControladorAuditoria controladorAuditoria;

	@Autowired
	private ControladorMenu controladorMenu;

	/**
	 * Método responsável por exibir o relatorio
	 * resumo volume.
	 *
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirTabelas")
	public String exibirTabelas(Model model) throws GGASException {

		model.addAttribute("listaTabelas", controladorAuditoria.listarTabelas());

		return "exibirTabelas";
	}

	/**
	 * Método responsável por exibir o relatorio 
	 * resumo volume.
	 * 
	 * @param idTabela
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirTabelasSelecionadas")
	public String exibirTabelasSelecionadas(@RequestParam(value = ID_TABELA, required = false) Long idTabela, Model model)
					throws GGASException {

		if (idTabela != null && idTabela != -1) {
			model.addAttribute("listaTabelasSelecionadas", controladorAuditoria.obterTabelaSelecionada(idTabela));
		} else {
			model.addAttribute("listaTabelasSelecionadas", controladorAuditoria.listarTabelas());
		}

		model.addAttribute("listaTabelas", controladorAuditoria.listarTabelas());
		model.addAttribute(ID_TABELA, idTabela);

		return exibirTabelas(model);
	}

	/**
	 * Método responsável por exibir o relatorio
	 * resumo volume.
	 * 
	 * @param chavePrimaria
	 * @param idTabela
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhesTabela")
	public String exibirDetalhesTabela(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
					@RequestParam(value = ID_TABELA, required = false) Long idTabela, HttpServletRequest request, Model model)
					throws GGASException {

		String view = "forward:exibirTabelasSelecionadas";

		model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);
		model.addAttribute(ID_TABELA, idTabela);

		try {

			Collection<Coluna> listaColunaTabela = controladorAuditoria.listarColunasTabelaSelecionada(chavePrimaria);

			if (chavePrimaria != null && chavePrimaria > 0) {
				model.addAttribute("listaColunasTabelaSelecionada", listaColunaTabela);
				model.addAttribute("nomeTabelaSelecionada", listaColunaTabela.iterator().next().getTabela().getNome());
			}

			view = "exibirDetalhesTabela";

		} catch (GGASException e) {
			model.addAttribute(TABELA_SEM_COLUNAS, TABELA_SEM_COLUNAS);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir o relatorio
	 * resumo volume.
	 * 
	 * @param chavePrimaria
	 * @param idTabela
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirAlterarTabela")
	public String exibirAlterarTabela(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
					@RequestParam(value = ID_TABELA, required = false) Long idTabela, HttpServletRequest request, Model model)
					throws GGASException {

		exibirDetalhesTabela(chavePrimaria, idTabela, request, model);

		if (model.containsAttribute(TABELA_SEM_COLUNAS)) {
			return "forward:exibirTabelasSelecionadas";
		}

		return "exibirAlterarTabela";
	}

	/**
	 * Método responsável por exibir o relatorio
	 * resumo volume.
	 * 
	 * @param chavePrimaria
	 * @param idTabela
	 * @param colunasAuditaveis
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("alterarTabela")
	public String alterarTabela(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
								@RequestParam(value = ID_TABELA, required = false) Long idTabela,
								@RequestParam(value = "auditavel", required = false) String[] colunasAuditaveis, HttpServletRequest request,
								Model model) throws GGASException {

		try {

			Tabela tabela = controladorAuditoria.verificarColunasAuditaveis(chavePrimaria, colunasAuditaveis, getDadosAuditoria(request));

			tabela.setDadosAuditoria(getDadosAuditoria(request));

			tabela.setMenu(controladorMenu.consultarMenu(tabela.getChavePrimaria()));

			model.addAttribute("tabela", tabela);

			controladorAuditoria.atualizar(tabela);

			exibirTabelasSelecionadas(idTabela, model);

			TrilhaAuditoriaUtil.limparCache();

			mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);

			return exibirTabelas(model);

		} catch (GGASException e) {

			mensagemErroParametrizado(model, request, e);

		}

		return "forward:exibirAlterarTabela";

	}

}
