/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.batch;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

/**
 * Action Rest responsável por gerenciar o fluxo de acesso às chamadas de processo
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
@Controller
public class ProcessoActionRest extends GenericActionRest {

	private static final Logger LOG = Logger.getLogger(ProcessoActionRest.class);
	private final Fachada fachada = Fachada.getInstancia();

	@Autowired
	private ControladorRota controladorRota;

	/**
	 * Dispara a execução de um processo do sistema para
	 * @param idOperacao identificador da operação
	 * @param idGrupoFaturamento identificador do grupo de faturamento
	 * @param idRota identificador da rota
	 * @param request requisição http
	 * @return retorna status de sucesso (200)
	 * @throws GGASException lançada caso ocorra alguma falha durante o processamento
	 */
	@RequestMapping(value = "processo/executarProcesso", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String> executarProcesso(@RequestParam Long idOperacao, @RequestParam Long idGrupoFaturamento,
			@RequestParam Long idRota, @RequestParam Long[] leituras , HttpServletRequest request) throws GGASException {

		LOG.info(MessageFormat.format("Disparando execução do processo de 'Registrar Leituras' para operacao={0}, "
				+ "grupoFaturamento {1}, rota = {2}, leituras = {3}", idOperacao, idGrupoFaturamento, idRota, leituras));

		ResponseEntity<String> resposta;
		try {
			final Processo processo = criarProcesso(idOperacao, idGrupoFaturamento, idRota, leituras, request);
			fachada.inserirProcesso(processo);
			resposta = new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Falha ao executar processo", e);
			resposta = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return resposta;
	}

	/**
	 * Cria uma instâcia de processo
	 * @param idOperacao identificador da operação
	 * @param idGrupoFaturamento identificador do grupo de faturamento
	 * @param idRota identificador da rota
	 * @param request requisição http
	 * @return retorna a instancia do processo criada
	 * @throws GGASException exceção lançada caso haja falha na criação do processo
	 */
	private Processo criarProcesso(@RequestParam Long idOperacao, @RequestParam Long idGrupoFaturamento, @RequestParam Long idRota,
			@RequestParam Long[] leituras, HttpServletRequest request) throws GGASException {
		final Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);

		final Processo processo = fachada.criarProcesso();
		processo.setAgendado(false);
		processo.setVersao(0);
		processo.setDataInicioAgendamento(DataUtil.toDate(LocalDateTime.now()));
		processo.setHabilitado(true);
		processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
		processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
		processo.setUsuario(usuario);
		processo.setDiaNaoUtil(true);

		final DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(usuario,
				(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
		processo.setDadosAuditoria(dadosAuditoria);
		final GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
		final Operacao operacao = fachada.buscarOperacao(idOperacao);
		final Rota rota = controladorRota.buscarRota(idRota);

		processo.setOperacao(operacao);
		processo.setDescricao(operacao.getDescricao()
				+ " Grupo Faturamento: " + grupoFaturamento.getDescricaoAbreviada()
				+ " Rota: " + rota.getNumeroRota());

		Map<String, String> parametros = new HashMap<>();
		parametros.put("idGrupoFaturamento", String.valueOf(idGrupoFaturamento));
		parametros.put("idRota", String.valueOf(idRota));
		parametros.put("leituras", Arrays.toString(leituras));
		processo.setParametros(parametros);

		return processo;
	}

}
