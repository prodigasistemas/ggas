/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.batch;

import br.com.ggas.batch.*;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.batch.impl.PesquisaProcesso;
import br.com.ggas.batch.impl.ProcessoDTO;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.batch.impl.ProcessoVO;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.impl.ControladorFaturaImpl;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.controleacesso.ControleAcessoAction;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe responsável pelas telas relacionadas ao Controle de Processos
 *
 */
@Controller
public class ProcessoAction extends GenericAction {

	private static final String INDICADOR_AGENDAMENTO = "indicadorAgendamento";

	private static final String ID_MODULO = "idModulo";

	private static final String REQUEST_MODULOS = "modulos";

	private static final String DATA_INICIO_AGENDADAMENTO = "dataInicioAgendamento";

	private static final String DATA_FINAL_AGENDADAMENTO = "dataFinalAgendamento";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String HORA_DIA = "hora";

	private static final String ID_SITUACAO = "idSituacao";

	private static final String ID_PERIODICIDADE = "idPeriodicidade";

	private static final String ID_OPERACAO = "idOperacao";

	private static final String VERSAO = "versao";

	private static final String LOG_ERRO = "logErro";

	private static final String REQUEST_SITUACOES = "situacoes";

	private static final String REQUEST_PERIODICIDADES = "periodicidades";

	private static final String LISTA_PROCESSOS = "processos";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVE_PRIMARIA_DOCUMENTO = "chavePrimariaDocumento";

	private static final String IS_POPUP = "isPopup";

	private static final String PROCESSO = "processo";

	private static final String GRUPOS_FATURAMENTO = "gruposFaturamento";

	public static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_ROTA = "idRota";

	private static final String PERIODO_EMISSAO = "periodoEmissao";

	private static final String TIPO_DATA_RETROATIVA_INVALIDA = "tipoDataRetroativaInvalida";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ID_ACAO = "idAcao";

	private static final String ACAO_LIMPAR_SESSAO = "1";

	private static final String ID_COMANDO = "idComando";

	private static final String LISTA_PROCESSO_DOCUMENTO = "listaProcessoDocumento";

	private static final String STATUS_NFE = "idStatusNFE";

	private static final String DATA_EMISSAO = "dataEmissao";

	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";

	private static final String CICLO = "ciclo";

	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	public static final String ROTAS_SELECIONADAS = "rotas_selecionadas";

	public static final String OFFSET = "offset";

	public static final String QTD_REGISTROS = "qtd_registros";
	public static final String PESQUISA_AUTOMATICA = "pesquisaAutomatica";
	public static final String COLUNA_ORDENACAO = "colunaOrdenacao";
	public static final String DIRECAO_ORDENACAO = "direcaoOrdenacao";

	public static final int QUANTIDADE_CARACTERES_EXTENSAO_ARQUIVO = 4;
	
	String ERRO_NEGOCIO_ACAO_COMANDO = "ERRO_NEGOCIO_ACAO_COMANDO";

	@Autowired
	private ControladorModulo controladorModulo;

	@Autowired
	private ControladorProcesso controladorProcesso;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	@Qualifier("controladorHistoricoConsumo")
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorFatura controladorFatura;

	/**
	 * Método responsável por exibir a tela pesquisa de processos.
	 * 
	 * @param idAcao
	 *            {@link String}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaProcesso")
	public String exibirPesquisaProcesso(@RequestParam(value = ID_ACAO, required = false) String idAcao,
			HttpServletRequest request, Model model) throws GGASException {

		this.carregarCampos(model);

		if (!StringUtils.isEmpty(idAcao) && StringUtils.contains(idAcao, ACAO_LIMPAR_SESSAO)) {
			limparSessao(request);
		}
		model.addAttribute(GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		String tempoAtualizacao = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEMPO_ATUALIZACAO_TELA_CONTROLE_PROCESSO);
		model.addAttribute("tempoAtualizacao", tempoAtualizacao);
		return "exibirPesquisaProcesso";
	}

	/**
	 * Request responsável por realizar a pesquisa paginada para o datatable dos processos
	 * @param pesquisa Os parametros da pesquisa
	 * @return Um json representando os resultados da pesquisa
	 * @throws GGASException o GGASException
	 */
	@RequestMapping(value = "pesquisarProcessoDatatable", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> pesquisarProcessoDatatable(@RequestBody PesquisaProcesso pesquisa) throws GGASException {
		DatatablesServerSideDTO<ProcessoDTO> datatable;
		Map<String, Object> filtro = new HashMap<>();
		try {
			prepararFiltro(filtro, pesquisa);
			Pair<Long, List<Processo>> resultados = controladorProcesso.consultarProcessos(filtro);
			List<ProcessoDTO> dados = resultados.getSecond().stream().map(l -> preencherProcessoDTO(l)).collect(Collectors.toList());
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), resultados.getFirst(), dados);
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), 0L, Collections.emptyList());
		}
		return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
	}

	/**
	 * Cria um objeto ProcessoDTO a partir de um Processo
	 * @param processo O processo de referencia
	 * @return O ProcessoDTO preenchido
	 */
	private ProcessoDTO preencherProcessoDTO(Processo processo) {
		final ProcessoDTO retorno = new ProcessoDTO();

		retorno.setChavePrimaria(processo.getChavePrimaria());
		retorno.setDescricao(processo.getDescricao());
		retorno.setPeriodicidade(processo.getPeriodicidade());
		retorno.setSituacao(processo.getSituacao());
		retorno.setInicioAgendamento(processo.getDataInicioAgendamento());
		if (processo.getDataFinalAgendamento() != null) {
			retorno.setFimAgendamento(processo.getDataFinalAgendamento());
		}
		if (processo.getInicio() != null) {
			retorno.setInicioExecucao(processo.getInicio());
		}
		if (processo.getFim() !=  null) {
			retorno.setFimExecucao(processo.getFim());
		}
		if (processo.getTempoExecucao() != null) {
			retorno.setTempoExecucao(processo.getTempoExecucao());
		}
		if (processo.getLogErro() != null && processo.getLogErro().length > 0) {
			retorno.setLogErro(true);
		}
		if (processo.getLogExecucao() != null && processo.getLogExecucao().length > 0) {
			retorno.setLogExecucao(true);
		}
		if (processo.getListaProcessoDocumento() != null && !processo.getListaProcessoDocumento().isEmpty()) {
			retorno.setQuantidadeDocumento(processo.getListaProcessoDocumento().size());
			ProcessoDocumento documento = processo.getListaProcessoDocumento().iterator().next();
			if (documento.getNomeDocumento().endsWith(".zip")) {
				retorno.setIndicadorDocumentoZip(true);
			}
			if (processo.getListaProcessoDocumento().size() == 1) {
				retorno.setChaveDocumento(documento.getChavePrimaria());
			}
		}
		retorno.setVersao(processo.getVersao());

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela pesquisa de processos.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("pesquisarProcesso")
	public String pesquisarProcesso(ProcessoImpl processo, BindingResult result, ProcessoVO processoVO,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		this.carregarCampos(model);
		String listaIdProcessos = request.getParameter("listaIdProcesso");
		model.addAttribute("processo", processo);
		model.addAttribute("processoVO", processoVO);
		try {
			if (listaIdProcessos != null && !"".equals(listaIdProcessos)) {
				String[] ids = listaIdProcessos.split(",");
				Collection<Long> idProcessos = new ArrayList<Long>();
				for (String id : ids) {
					idProcessos.add(Long.parseLong(id));
				}
				filtro.put("idProcessos", idProcessos);
			}
			model.addAttribute(GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
			model.addAttribute(PESQUISA_AUTOMATICA, true);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaProcesso(null, request, model);
	}

	/**
	 * Método responsável por exibir o log.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param logErro
	 *            {@link Boolean}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@RequestMapping("exibirLog")
	public ResponseEntity exibirLog(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam(LOG_ERRO) Boolean logErro, HttpServletRequest request, Model model)
			throws GGASException, IOException {

		ResponseEntity<byte[]> responseEntity = null;
		Processo processo = null;
		try {
			processo = (Processo) controladorProcesso.obter(chavePrimaria);

			if (processo.getOperacao() != null && processo.getOperacao().getModulo() != null
					&& processo.getOperacao().getModulo().getChavePrimaria() > 0) {
				verificarPermissaoProcesso(processo.getOperacao().getDescricao(), "CONSULTAR",
						processo.getOperacao().getModulo().getChavePrimaria(), Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_CONSULTAR, request);
			}
		} catch (NegocioException e1) {
			mensagemErro(model, e1);
		}

		byte[] log = null;

		if (Boolean.TRUE.equals(logErro) && (processo != null)) {
			log = processo.getLogErro();
		} else if (processo != null) {
			log = processo.getLogExecucao();
		}

		if (log != null) {

			try {

				ByteArrayInputStream in = new ByteArrayInputStream(log);

				int index = 0;

				byte[] bytearray = new byte[in.available()];
				MediaType contentType = MediaType.parseMediaType("application/download");
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(contentType);
				headers.setContentDispositionFormData("Content-Disposition",
						"attachment; filename=" + "Processo" + processo.getChavePrimaria() + ".log");

				String value = "";
				while ((index = in.read(bytearray)) != -1) {
					value = new String(bytearray, 0, index);
				}

				if (!value.isEmpty()) {
					value = value.replaceAll("(\n)", "\r\n");
				}
				
				responseEntity = new ResponseEntity<byte[]>(value.getBytes(), headers, HttpStatus.OK);
				in.close();

			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return responseEntity;

	}

	/**
	 * Método responsável por iniciar o processo.
	 * 
	 * @param processoTmp
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param versao
	 *            {@link Integer}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("reiniciarProcesso")
	public String reiniciarProcesso(ProcessoImpl processoTmp, BindingResult result, ProcessoVO processoVO,
			BindingResult results, @RequestParam(VERSAO) Integer versao, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			Processo processo = (Processo) controladorProcesso.obter(processoTmp.getChavePrimaria());

			if (processo != null && processo.getOperacao() != null && processo.getOperacao().getModulo() != null) {
				verificarPermissaoProcesso(processo.getOperacao().getDescricao(), "EXECUTAR",
						processo.getOperacao().getModulo().getChavePrimaria(), Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_REEXECUTAR,
						request);
			}
			if (processo != null) {
				processo.setVersao(versao);
				processo.setInicio(Calendar.getInstance().getTime());
				processo.setFim(null);
				processo.setLogErro(null);
				processo.setLogExecucao(null);
				processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
				Usuario usuarioLogado = (Usuario) request.getSession().getAttribute("usuarioLogado");
				if (usuarioLogado != null) {
					processo.setUsuario(usuarioLogado);
				}
				processo.setDadosAuditoria(getDadosAuditoria(request));

				controladorProcesso.atualizar(processo);
			}

			mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_REINICIADO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarProcesso(processoTmp, result, processoVO, results, request, model);
	}

	/**
	 * Método responsável por excluir o processo.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("excluirProcesso")
	public String excluirProcesso(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			Model model) throws GGASException {

		try {

			Processo processo = (Processo) controladorProcesso.obter(chavePrimaria);
			if (processo != null && processo.getOperacao() != null && processo.getOperacao().getModulo() != null) {
				verificarPermissaoProcesso(processo.getOperacao().getDescricao(), "EXCLUIR",
						processo.getOperacao().getModulo().getChavePrimaria(), Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_EXCLUIR,
						request);
			}
			if (processo != null) {
				processo.setDadosAuditoria(getDadosAuditoria(request));
				controladorProcesso.remover(processo);
				mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_EXCLUIDO);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}
		model.addAttribute(PESQUISA_AUTOMATICA, true);
		return exibirPesquisaProcesso(null, request, model);
	}

	private void verificarPermissaoProcesso(String processo, String operacao, Long modulo, String chaveMensagemPermissao,
			HttpServletRequest request) throws NegocioException {
		Usuario usuario = (Usuario) request.getSession().getAttribute(ControleAcessoAction.ATRIBUTO_USUARIO_LOGADO);
		List<Long> papeis = usuario.getPapeis().stream().map(papel -> papel.getChavePrimaria()).collect(Collectors.toList());
		Optional<Menu> menu = controladorModulo.buscarMenu(processo, operacao, modulo, papeis);
		if (!menu.isPresent()) {
			throw new NegocioException(chaveMensagemPermissao, true);
		}
	}

	/**
	 * Método responsável por exibir o agendamento de processo.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoProcessoAgendamento") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String exibirInclusaoProcessoAgendamento(HttpServletRequest request, Model model) throws GGASException {

		carregarCampos(model);
		saveToken(request);
		model.addAttribute(GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		return "exibirInclusaoProcessoAgendamento";
	}

	/**
	 * Método responsável por agendar um processo.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping(value = "incluirProcessoAgendamento", method = RequestMethod.POST)
	public String incluirProcessoAgendamento(ProcessoImpl processo, BindingResult result, ProcessoVO processoVO,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		String view;
		model.addAttribute("processo", processo);
		model.addAttribute("processoVO", processoVO);
		try {
			Integer totalQtdRotas = 0;
			Integer contadorRotas = 0;
			validarToken(request);
			if (processoVO.getRotasSelecionadas() != null && processoVO.getRotasSelecionadas().length > 0) {
				totalQtdRotas = processoVO.getRotasSelecionadas().length;
			}
			do {
				ProcessoImpl processoIncluir = new ProcessoImpl();
				popularProcesso(processoIncluir, processoVO, processo, request, model);
				Map<String, Object> erros = processoIncluir.validarDados();
				if (!erros.isEmpty()) {
					mensagemErro(model, request, new NegocioException(erros));
					return exibirInclusaoProcessoAgendamento(request, model);
				} else {
					if (processoVO.getIdGrupoFaturamento() != null && processoVO.getIdGrupoFaturamento() > 0) {
						GrupoFaturamento grupoFaturamento = controladorRota.obterDescricaoGrupoFaturamento(processoVO.getIdGrupoFaturamento());
						if (grupoFaturamento != null) {
							processoIncluir.setDescricao(processoIncluir.getDescricao() + " Grupo Faturamento: " + grupoFaturamento.getDescricaoAbreviada());
						}
						if (totalQtdRotas > 0) {
							Long rotaAtual = processoVO.getRotasSelecionadas()[contadorRotas];
							processoVO.setIdRota(rotaAtual);
							if (rotaAtual != null && rotaAtual >= 0) {
								Rota rota = controladorRota.buscarNumeroRota(processoVO.getIdRota());
								if (rota != null) {
									processoIncluir.setDescricao(processoIncluir.getDescricao() + " Rota: " + rota.getNumeroRota());
								}
							}
						}
						carregarParametrosProcesso(processoIncluir, processoIncluir.getOperacao(), processoVO);
					}

					controladorProcesso.validarDataAgendamento(processoIncluir, processoVO.getHoraInicio(), processoVO.getHoraFim());
					processoIncluir.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
					processoIncluir.setDadosAuditoria(getDadosAuditoria(request));
					controladorProcesso.inserir(processoIncluir);
					++contadorRotas;
				}
			} while (contadorRotas < totalQtdRotas);
			mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_AGENDADO);
			model.addAttribute(PESQUISA_AUTOMATICA, true);
			return exibirPesquisaProcesso(null, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			return exibirInclusaoProcessoAgendamento(request, model);
		}
	}

	/**
	 * Método responsável por realizar a execução de um processo.
	 * 
	 * @param processoTmp
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("processarExecucaoProcesso") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String processarExecucaoProcesso(ProcessoImpl processoTmp, BindingResult result, ProcessoVO processoVO,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {
		
		String view = "";
		Long operacao;
		model.addAttribute("processo", processoTmp);
		model.addAttribute("processoVO", processoVO);
		try {

			if (processoTmp != null && processoTmp.getOperacao() != null && processoTmp.getOperacao().getModulo() != null) {
				verificarPermissaoProcesso(processoTmp.getOperacao().getDescricao(), "EXECUTAR",
						processoTmp.getOperacao().getModulo().getChavePrimaria(), Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_EXECUTAR,
						request);
			}

			if (processoTmp != null) {

				validarToken(request);
				Integer totalQtdRotas = 0;
				Integer contadorRotas = 0;

				if (processoVO.getRotasSelecionadas() != null && processoVO.getRotasSelecionadas().length > 0) {
					totalQtdRotas = processoVO.getRotasSelecionadas().length;
				}
				do {
					Operacao operacaoObj = null;

					if (totalQtdRotas > 0) {
						Long rotaAtual = processoVO.getRotasSelecionadas()[contadorRotas];
						processoVO.setIdRota(rotaAtual);
					}
					ProcessoImpl processo = (ProcessoImpl) controladorProcesso.criar();
					processo.setAgendado(false);
					processo.setVersao(0);
					processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
					processo.setDataFinalAgendamento(null);
					processo.setHabilitado(true);
					processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
					processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
					processo.setUsuario((Usuario) request.getSession().getAttribute("usuarioLogado"));
					if (processo.getUsuario() != null && processo.getUsuario().getFuncionario() != null) {
						Funcionario funcionario = processo.getUsuario().getFuncionario();
						processo.setUsuarioExecucao(funcionario.getNome());
						if (funcionario.getUnidadeOrganizacional() != null) {
							processo.setUnidadeOrganizacionalExecucao(
									funcionario.getUnidadeOrganizacional().getDescricao());
						}
					}
					processo.setDiaNaoUtil(true);
					processo.setDadosAuditoria(getDadosAuditoria(request));

					if (processoTmp.getOperacao() == null || processoTmp.getOperacao().getChavePrimaria() <= 0) {
						processo.setOperacao(null);
					} else {
						operacaoObj = controladorModulo.buscarOperacaoPorChave(processoTmp.getOperacao().getChavePrimaria());
						processo.setOperacao(operacaoObj);
						processo.setDescricao(operacaoObj.getDescricao());

						if (processoVO.getIdGrupoFaturamento() != null && processoVO.getIdGrupoFaturamento() > 0) {
							GrupoFaturamento grupoFaturamento = controladorRota
									.obterGrupoFaturamento(processoVO.getIdGrupoFaturamento());
							if (grupoFaturamento != null) {
								processo.setDescricao(processo.getDescricao() + " Grupo Faturamento: "
										+ grupoFaturamento.getDescricaoAbreviada());
							}
						}

						if (processoVO.getIdRota() != null && processoVO.getIdRota() >= 0) {
							Rota rota = controladorRota.buscarRota(processoVO.getIdRota());
							if (rota != null) {
								processo.setDescricao(processo.getDescricao() + " Rota: " + rota.getNumeroRota());
							}
						}

					}

					if(processo.getDescricao().contains("Corte") && processoVO.getIdComando() < 0) {
						throw new NegocioException(ERRO_NEGOCIO_ACAO_COMANDO, true);
					}
					
					if (processoVO.getIdComando() != null && processoVO.getIdComando() > 0) {

						ControladorAcaoComando controladorAcaoComando = ServiceLocator.getInstancia()
								.getControladorAcaoComando();
						AcaoComando acaoComando = null;
						acaoComando = controladorAcaoComando.obterAcaoComando(processoVO.getIdComando(), "acao",
								"acao.servicoTipo.listaEquipamentoEspecial", "acao.servicoTipo.listaMateriais",
								"acao.servicoTipo.indicadorGeraLote",
								"acao.servicoTipoPesquisa.indicadorPesquisaSatisfacao",
								"acao.chamadoAssuntoPesquisa.indicadorGeraServicoAutorizacao",
								"acao.chamadoAssuntoPesquisa");
						if (acaoComando != null && acaoComando.getAcao().getServicoTipo() != null
								&& acaoComando.getAcao().getServicoTipo().getDescricao() != null) {
							processo.setDescricao(processo.getDescricao() + " Tipo Servico: "
									+ acaoComando.getAcao().getServicoTipo().getDescricao());
						}
					}

					if (!StringUtils.isEmpty(processoTmp.getEmailResponsavel())) {
						processo.setEmailResponsavel(processoTmp.getEmailResponsavel());
					} else {
						processo.setEmailResponsavel(null);
					}
					controladorProcesso.validarEmailObrigatorioAtividadeSelecionada(processo);

					carregarParametrosProcesso(processo, operacaoObj, processoVO);

					controladorProcesso.inserir(processo);
					++contadorRotas;
				} while (contadorRotas < totalQtdRotas);

				mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_EXECUTADO);
				processoTmp.setSituacao(-1);
				processoTmp.setPeriodicidade(-1);
				view = pesquisarProcesso(processoTmp, result, processoVO, results, request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			operacao = verificarOperacao(processoTmp);
			view = exibirExecutarProcesso(processoVO.getModulo(), processoVO.getIdGrupoFaturamento(), operacao, request, model);
		}

		return view;
	}

	private Long verificarOperacao(ProcessoImpl processoTmp) {
		if(processoTmp.getOperacao() != null) {
			return processoTmp.getOperacao().getChavePrimaria();
		}
		return -1L;
	}

	/**
	 * Carregar parametros processo.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param operacao
	 *            {@link Operacao}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void carregarParametrosProcesso(ProcessoImpl processo, Operacao operacao, ProcessoVO processoVO)
			throws GGASException {

		if (operacao != null) {
			Map<String, String> parametros = new HashMap<String, String>();

			Collection<AtividadeSistema> listaAtividadeSistema = controladorCronogramaFaturamento
					.listarAtividadeSistemaPorOperacao(operacao.getChavePrimaria());

			if ((listaAtividadeSistema != null) && (!listaAtividadeSistema.isEmpty())) {

				AtividadeSistema atividadeSistema = ((List<AtividadeSistema>) listaAtividadeSistema).get(0);

				if (atividadeSistema.isCronograma()) {

					controladorHistoricoConsumo.validarSelecaoGrupoFaturamento(processoVO.getIdGrupoFaturamento());

					parametros.put(ID_GRUPO_FATURAMENTO, String.valueOf(processoVO.getIdGrupoFaturamento()));
					if (processoVO.getIdRota() != null && processoVO.getIdRota() > 0) {
						parametros.put(ID_ROTA, String.valueOf(processoVO.getIdRota()));
					}

					if (processoVO.getPeriodoEmissao() != null
							&& Util.isDataValida(processoVO.getPeriodoEmissao(), Constantes.FORMATO_DATA_BR)) {
						if (new Date().before(Util.converterCampoStringParaData("data", processoVO.getPeriodoEmissao(),
								Constantes.FORMATO_DATA_BR))) {
							parametros.put(PERIODO_EMISSAO, null);
						} else if (processoVO.getPeriodoEmissao()
								.equals(Util.converterDataParaString(new Date()))) {
							parametros.put(PERIODO_EMISSAO, null);
						} else {
							parametros.put(PERIODO_EMISSAO, processoVO.getPeriodoEmissao());
							parametros.put(TIPO_DATA_RETROATIVA_INVALIDA, processoVO.getTipoDataRetroativaInvalida());
						}
					}
				}
			}
			if (processoVO.getIdComando() != null) {
				parametros.put(ID_COMANDO, String.valueOf(processoVO.getIdComando()));
			}

			if (processoVO.getIdStatusNFE() != null && processoVO.getIdStatusNFE() > 0) {
				parametros.put(STATUS_NFE, String.valueOf(processoVO.getIdStatusNFE()));
			}

			if (processoVO.getDataEmissao() != null
					&& Util.isDataValida(processoVO.getDataEmissao(), Constantes.FORMATO_DATA_BR)) {
				parametros.put(DATA_EMISSAO, processoVO.getDataEmissao());
			}

			if (processoVO.getAnoMesFaturamento() != null && !"".equals(processoVO.getAnoMesFaturamento())) {
				parametros.put(ANO_MES_FATURAMENTO, String.valueOf(processoVO.getAnoMesFaturamento()));
			}
			
			if (processoVO.getAnoMesReferencia() != null && !"".equals(processoVO.getAnoMesReferencia())) {
				parametros.put(ANO_MES_REFERENCIA, String.valueOf(processoVO.getAnoMesReferencia()));
			}

			if (processoVO.getCiclo() != null && processoVO.getCiclo() > 0) {
				parametros.put(CICLO, String.valueOf(processoVO.getCiclo()));
			}
			processo.setParametros(parametros);
		}
	}

	/**
	 * Método responsável por exibir o agendamento de processo.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoProcessoAgendamento") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String exibirAlteracaoProcessoAgendamento(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			controladorProcesso.validarAlteracaoAgendamentoProcesso(chavePrimaria);
			ProcessoImpl processo = (ProcessoImpl) this.controladorProcesso.obter(chavePrimaria);
			model.addAttribute("processo", processo);
			popularForm(processo, model);
			carregarCampos(model);
			saveToken(request);
			return "exibirAlteracaoProcessoAgendamento";
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			return exibirPesquisaProcesso(null, request, model);
		}
	}

	/**
	 * Método responsável por alterar um processo agendado.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("alterarProcessoAgendamento") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String alterarProcessoAgendamento(ProcessoImpl processo, BindingResult result, ProcessoVO processoVO,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoProcessoAgendamento";
		try {
			validarToken(request);
			model.addAttribute("processo", processo);
			model.addAttribute("processo", processoVO);
			ProcessoImpl processoAtual = (ProcessoImpl) controladorProcesso.obter(processoVO.getChavePrimaria());
			popularProcesso(processoAtual, processoVO, processo, request, model);
			controladorProcesso.validarDataAgendamento(processoAtual, processoVO.getHoraInicio(),
					processoVO.getHoraFim());
			controladorProcesso.atualizarAgendamento(processoAtual);
			mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_ALTERADO);
			saveToken(request);
			view = pesquisarProcesso(processoAtual, results, processoVO, results, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de executar atividade de faturamento.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param idModulo
	 *            {@link Long}
	 * @param idGrupoFaturamento
	 *            {@link Long}
	 * @param idOperacao
	 *            {@link Long}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirExecutarProcesso") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String exibirExecutarProcesso(@RequestParam(value = "idModulo", required = false) Long idModulo,
			@RequestParam(value = "idGrupoFaturamento", required = false) Long idGrupoFaturamento,
			@RequestParam(value = "idOperacao", required = false) Long idOperacao, HttpServletRequest request, Model model) throws GGASException {

		if (isPostBack(request)) {
			model.addAttribute("isPostBack", true);
		}

		execucaoCronograma(idModulo, idGrupoFaturamento, idOperacao, model);
		
		carregarCampos(model);
		model.addAttribute(GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		saveToken(request);
		return "exibirExecutarProcesso";
	}

	/**
	 * Método responsável por tratar os dados enviados pela tela de cronograma
	 * 
	 * @param idModulo {@link Long}
	 * @param idGrupoFaturamento {@link Long}
	 * @param idOperacao {@link Long}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void execucaoCronograma(Long idModulo, Long idGrupoFaturamento, Long idOperacao, Model model)
			throws NegocioException {
		
		ProcessoVO processoVO = new ProcessoVO();
		processoVO.setModulo(idModulo);
		processoVO.setIdGrupoFaturamento(idGrupoFaturamento);
		model.addAttribute("processoVO", processoVO);
		ProcessoImpl processo = new ProcessoImpl();
		processo.setOperacao(controladorModulo.buscarOperacaoPorChave(idOperacao));
		model.addAttribute("processo", processo);
	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param filtro {@link Map}
	 * @param pesquisaProcesso {@link ProcessoVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, PesquisaProcesso pesquisaProcesso) throws GGASException {

		if (pesquisaProcesso != null) {

			if (pesquisaProcesso.getColunaOrdenacao() != null && !pesquisaProcesso.getColunaOrdenacao().isEmpty()) {
				filtro.put(COLUNA_ORDENACAO, pesquisaProcesso.getColunaOrdenacao());
				filtro.put(DIRECAO_ORDENACAO, pesquisaProcesso.getDirecaoOrdenacao());
			}

			if (pesquisaProcesso.getModulo() != null && pesquisaProcesso.getModulo() > 0) {
				filtro.put(ID_MODULO, pesquisaProcesso.getModulo());
			}

			if (pesquisaProcesso.getOperacao() != null && pesquisaProcesso.getOperacao() > 0) {
				filtro.put(ID_OPERACAO, pesquisaProcesso.getOperacao());
			}

			if (pesquisaProcesso.getPeriodicidade() > -1) {
				filtro.put(ID_PERIODICIDADE, pesquisaProcesso.getPeriodicidade());
			}

			if (pesquisaProcesso.getSituacao() > -1) {
				filtro.put(ID_SITUACAO, pesquisaProcesso.getSituacao());
			}

			if (pesquisaProcesso.getIdGrupoFaturamento() != null && pesquisaProcesso.getIdGrupoFaturamento() > 0) {
				filtro.put(ID_GRUPO_FATURAMENTO, pesquisaProcesso.getIdGrupoFaturamento());
			}

			if (pesquisaProcesso.getRotasSelecionadas() != null && pesquisaProcesso.getRotasSelecionadas().length > 0) {
				filtro.put(ROTAS_SELECIONADAS, pesquisaProcesso.getRotasSelecionadas());
			}

			if (!StringUtils.isEmpty(pesquisaProcesso.getDataInicio())) {
				filtro.put("inicio", Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_INICIO,
						pesquisaProcesso.getDataInicio(), Constantes.FORMATO_DATA_BR));
			}

			if (!StringUtils.isEmpty(pesquisaProcesso.getDataFim())) {
				filtro.put("fim", Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_FIM,
						pesquisaProcesso.getDataFim(), Constantes.FORMATO_DATA_BR));
			}

			if ((!StringUtils.isEmpty(pesquisaProcesso.getDataInicio())) && (!StringUtils.isEmpty(pesquisaProcesso.getDataFim()))) {
				controladorProcesso.validarDatasPesquisaProcesso(pesquisaProcesso.getDataInicio(), pesquisaProcesso.getDataFim());
			}

			if (!StringUtils.isEmpty(pesquisaProcesso.getDataInicioAgendamento())) {
				filtro.put(DATA_INICIO_AGENDADAMENTO,
						Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO,
								pesquisaProcesso.getDataInicioAgendamento(), Constantes.FORMATO_DATA_BR));
			}

			if (!StringUtils.isEmpty(pesquisaProcesso.getDataFinalAgendamento())) {
				Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_DATA_FINAL_AGENDAMENTO,
						pesquisaProcesso.getDataFinalAgendamento(), Constantes.FORMATO_DATA_BR);
			}

			if ((!StringUtils.isEmpty(pesquisaProcesso.getDataInicioAgendamento()))
					&& (!StringUtils.isEmpty(pesquisaProcesso.getDataFinalAgendamento()))) {
				controladorProcesso.validarDatasPesquisaProcesso(pesquisaProcesso.getDataInicioAgendamento(),
						pesquisaProcesso.getDataFinalAgendamento());
			}

			if (pesquisaProcesso.getIndicadorAgendamento() != null) {
				filtro.put(INDICADOR_AGENDAMENTO, pesquisaProcesso.getIndicadorAgendamento());
			}
			filtro.put(OFFSET, pesquisaProcesso.getOffset());
			filtro.put(QTD_REGISTROS, pesquisaProcesso.getQtdRegistros());
		}
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(REQUEST_MODULOS, controladorModulo.consultarModulos(new HashMap<String, String>()));
		model.addAttribute(REQUEST_PERIODICIDADES, controladorProcesso.listarPeriodicidadeProcesso());
		model.addAttribute(REQUEST_SITUACOES, controladorProcesso.listarSituacoesAlteracaoAgendamentoProcesso());
		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(PERIODO_EMISSAO, Util.converterDataParaString(controladorFatura.obterDataEmissaoMaxima()));
	}

	/**
	 * Intervalo anos data.
	 * 
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	/**
	 * Popular processo.
	 * 
	 * @param processoAtual
	 *            {@link ProcessoImpl}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularProcesso(ProcessoImpl processoAtual, ProcessoVO processoVO, ProcessoImpl processo,
			HttpServletRequest request, Model model) throws GGASException {

		processoAtual.setAgendado(true);
		if (processo.getVersao() > 0) {
			processoAtual.setVersao(processo.getVersao());
		}

		if (processo.getOperacao() == null || processo.getOperacao().getChavePrimaria() <= 0) {
			processoAtual.setOperacao(null);
		} else {
			processoAtual.setOperacao(processo.getOperacao());
			processoAtual.setDescricao(processo.getOperacao().getDescricao());
		}

		if (processo.getPeriodicidade() < 0) {
			processoAtual.setPeriodicidade(-1);
		} else {
			processoAtual.setPeriodicidade(processo.getPeriodicidade());
		}

		if (processo.getSituacao() < 0) {
			processoAtual.setSituacao(-1);
		} else {
			processoAtual.setSituacao(processo.getSituacao());
		}

		if (StringUtils.isEmpty(processoVO.getDataInicioAgendamento())) {
			processoAtual.setDataInicioAgendamento(null);
		} else {
			DateTime data = new DateTime(
					Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO,
							processoVO.getDataInicioAgendamento(), Constantes.FORMATO_DATA_BR));
			if (processoVO.getHora() != null && processoVO.getHora() > 0) {
				data = data.withHourOfDay(processoVO.getHora());
				data = data.withMinuteOfHour(0);
				data = data.withSecondOfMinute(0);
				data = data.withMillisOfSecond(0);
			}
			processoAtual.setDataInicioAgendamento(data.toDate());
		}

		if (StringUtils.isEmpty(processoVO.getDataFinalAgendamento())) {
			processoAtual.setDataFinalAgendamento(null);
		} else {
			DateTime data = new DateTime(
					Util.converterCampoStringParaData(Processo.PROCESSO_ROTULO_DATA_FINAL_AGENDAMENTO,
							processoVO.getDataFinalAgendamento(), Constantes.FORMATO_DATA_BR));
			if (processoVO.getHora() != null && processoVO.getHora() > 0) {
				data = data.withHourOfDay(processoVO.getHora());
				data = data.withMinuteOfHour(0);
				data = data.withSecondOfMinute(0);
				data = data.withMillisOfSecond(0);
			}
			processoAtual.setDataFinalAgendamento(data.toDate());
		}

		processoAtual.setUsuario((Usuario) request.getSession().getAttribute("usuarioLogado"));

		if (!processo.isDiaNaoUtil()) {
			processoAtual.setDiaNaoUtil(false);
		} else {
			processoAtual.setDiaNaoUtil(true);
		}

		if (!StringUtils.isEmpty(processo.getEmailResponsavel())) {
			processoAtual.setEmailResponsavel(processo.getEmailResponsavel());
		} else {
			processoAtual.setEmailResponsavel(null);
		}

		processoAtual.setDadosAuditoria(getDadosAuditoria(request));

		controladorProcesso.validarEmailObrigatorioAtividadeSelecionada(processoAtual);
	}

	/**
	 * Popular form.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param model
	 *            {@link Model}
	 */
	private void popularForm(ProcessoImpl processo, Model model) {

		model.addAttribute(ID_MODULO, processo.getOperacao().getModulo().getChavePrimaria());
		model.addAttribute(DATA_INICIO_AGENDADAMENTO,
				Util.converterDataParaStringSemHora(processo.getDataInicioAgendamento(), Constantes.FORMATO_DATA_BR));
		model.addAttribute(DATA_FINAL_AGENDADAMENTO,
				Util.converterDataParaStringSemHora(processo.getDataFinalAgendamento(), Constantes.FORMATO_DATA_BR));

		DateTime dataAgendada = new DateTime(processo.getDataInicioAgendamento());
		model.addAttribute(HORA_DIA, dataAgendada.getHourOfDay());
	}

	/**
	 * Método responsável por cancelar um agendamento.
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param chavesPrimarias
	 *            {@link }
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("cancelarAgendamento") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String cancelarAgendamento(ProcessoImpl processo, BindingResult result, ProcessoVO processoVO,
			BindingResult results, @RequestParam(value=CHAVES_PRIMARIAS, required=false) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute("processo", processo);
		model.addAttribute("processoVO", processoVO);
		try {
			if (processo != null && processo.getOperacao() != null && processo.getOperacao().getModulo() != null) {
				verificarPermissaoProcesso(processo.getOperacao().getDescricao(), "EXECUTAR",
						processo.getOperacao().getModulo().getChavePrimaria(), Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_CANCELAR,
						request);
			}

			getDadosAuditoria(request);
			controladorProcesso.cancelarAgendamento(chavesPrimarias);
			mensagemSucesso(model, ControladorProcesso.SUCESSO_PROCESSO_CANCELADO);
			model.addAttribute(PESQUISA_AUTOMATICA, true);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return exibirPesquisaProcesso(null, request, model);
	}

	/**
	 * Limpar sessao.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 */
	private void limparSessao(HttpServletRequest request) {

		request.getSession().removeAttribute(LISTA_PROCESSOS);
	}

	/**
	 * Exibir Relatório
	 * 
	 * @param processo
	 *            {@link ProcessoImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param processoVO
	 *            {@link ProcessoVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param isPopup
	 *            {@link Boolean}
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws IOException
	 *             {@link IOException}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("imprimirRelatorio") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String imprimirRelatorio(ProcessoImpl processo, BindingResult result, ProcessoVO processoVO,
			BindingResult results, @RequestParam(value = IS_POPUP, required = false) Boolean isPopup,
			@RequestParam(CHAVE_PRIMARIA_DOCUMENTO) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response, Model model) throws IOException, GGASException {

		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
				.getControladorProcessoDocumento();

		ProcessoDocumento processoDocumento = (ProcessoDocumento) controladorProcessoDocumento.obter(chavePrimaria);

		try {
			verificarPermissaoProcesso(processoDocumento.getProcesso().getOperacao().getDescricao(), "CONSULTAR",
					processoDocumento.getProcesso().getOperacao().getModulo().getChavePrimaria(),
					Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_CONSULTAR, request);
			File arquivo = Util.getFile(processoDocumento.getDiretorioDocumento());

			if (arquivo.exists()) {
				if (".zip".equals(arquivo.getName().substring(arquivo.getName().length()- QUANTIDADE_CARACTERES_EXTENSAO_ARQUIVO))) {
					baixarZip(processoDocumento.getNomeDocumento(), Util.converterFileParaByte(arquivo), response);
				} else {
					imprimirPDFRelatorio(processoDocumento.getNomeDocumento(), Util.converterFileParaByte(arquivo), response);
				}
			} else {
				mensagemErroParametrizado(model, Constantes.ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO_EM_DIRETORIO, request,
						new Object[] { processoDocumento.getDiretorioDocumento() });
			}

			if (isPopup != null && isPopup) {
				return exibirRelatorioPopup(chavePrimaria, request, model);
			} else {
				model.addAttribute(PESQUISA_AUTOMATICA, true);
				return exibirPesquisaProcesso(null, request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			model.addAttribute(PESQUISA_AUTOMATICA, true);
			return exibirPesquisaProcesso(null, request, model);
		}
	}

	/**
	 * Exibe os relatorios em uma popup.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirRelatorioPopup") //NOSONAR Será resolvido pela Orube na próxima entrega
	public String exibirRelatorioPopup(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			Model model) {

		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
				.getControladorProcessoDocumento();

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(PROCESSO, chavePrimaria);
		List<ProcessoDocumento> lista = (ArrayList) controladorProcessoDocumento.consultarProcessoDocumento(filtro);
		ordenarListaDeDocumentos(lista);
		Map<String, ProcessoDocumento> mapaAux = new HashMap<>();
		List<ProcessoDocumento> listaRemover = new ArrayList<>();
		for(ProcessoDocumento processoAux : lista) {
			if(mapaAux.containsKey(processoAux.getNomeDocumento())) {
				listaRemover.add(mapaAux.get(processoAux.getNomeDocumento()));
			} else {
				mapaAux.put(processoAux.getNomeDocumento(), processoAux);
			}
		}
		
		lista.removeAll(listaRemover);
		
		model.addAttribute(LISTA_PROCESSO_DOCUMENTO, lista);

		return "exibirRelatorioPopup";
	}

	private void ordenarListaDeDocumentos(List<ProcessoDocumento> lista) {
		lista.sort((o1, o2) -> {
			int comparacao = 0;
			if (o1.getNomeDocumento().contains(ControladorFaturaImpl.TODAS_AS_FATURAS)) {
				if (!o2.getNomeDocumento().contains(ControladorFaturaImpl.TODAS_AS_FATURAS)) {
					comparacao = -1;
				}
			} else if (o1.getNomeDocumento().contains(ControladorFaturaImpl.RESUMO_DE_IMPRESSAO_DAS_FATURAS)) {
				if (o2.getNomeDocumento().contains(ControladorFaturaImpl.TODAS_AS_FATURAS)) {
					comparacao = 1;
				} else if (!o2.getNomeDocumento().contains(ControladorFaturaImpl.RESUMO_DE_IMPRESSAO_DAS_FATURAS)) {
					comparacao = -1;
				}
			} else if (o1.getNomeDocumento().contains("Faturas Canceladas")) {
				if (o2.getNomeDocumento().contains(ControladorFaturaImpl.TODAS_AS_FATURAS)) {
					comparacao = 1;
				} else if (!o2.getNomeDocumento().contains("Faturas Canceladas")) {
					comparacao = -1;
				}
			} else {
				if (!o2.getNomeDocumento().contains(ControladorFaturaImpl.TODAS_AS_FATURAS)
						&& !o2.getNomeDocumento().contains(ControladorFaturaImpl.RESUMO_DE_IMPRESSAO_DAS_FATURAS)
						&& !o2.getNomeDocumento().contains("Faturas Canceladas")) {
					comparacao = o1.getNomeDocumento().compareTo(o2.getNomeDocumento());
				} else {
					comparacao = 1;
				}
			}

			return comparacao;
		});
	}
}
