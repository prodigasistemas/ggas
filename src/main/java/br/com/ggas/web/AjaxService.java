/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
import static br.com.ggas.util.Constantes.FORMATO_DATA_BR;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.FluentIterable;
import com.google.gson.Gson;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.impl.ItemLoteComunicacaoImpl;
import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.dominio.FuncionarioEquipeVO;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipeTurno;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.atendimento.exception.AutorizacaoServicoImovelCicloLeituraException;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.impl.ServicoAutorizacaoRegistroLocalImpl;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.contrato.contrato.impl.ControladorContratoImpl;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.AutorizacoesPendentesVO;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.FuncionalidadeVO;
import br.com.ggas.controleacesso.impl.ModuloVO;
import br.com.ggas.controleacesso.impl.OperacaoItemVO;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaValorRegulamentado;
import br.com.ggas.faturamento.simulacao.ConstrutorSerieTemporal;
import br.com.ggas.faturamento.tarifa.AcaoSincroniaDesconto;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.GGASRuntimeException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.integracao.geral.AcompanhamentoIntegracaoVO;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ColetorConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.impl.ColetorConsumoApurado;
import br.com.ggas.medicao.consumo.impl.ColetorConsumoPuro;
import br.com.ggas.medicao.cromatografia.negocio.FatorZ;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.RotaHistorico;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.StringUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.contrato.CalculoIndenizacaoVO;
import br.com.ggas.web.contrato.contrato.ImovelPontoConsumoVO;
import br.com.ggas.web.faturamento.cronograma.CronogramaAction;
import br.com.ggas.web.medicao.consumo.TemperaturaPressaoMedicaoVO;
import br.com.ggas.web.medicao.rota.RotaAction;

/**
 * The Class AjaxService.
 */
public class AjaxService {
	
	private static final int CONSTANTE_NUMERO_ZERO = 0;
	
	private static final int CONSTANTE_NUMERO_UM = 1;
	
	private static final int CONSTANTE_NUMERO_DOIS = 2;
	
	private static final int CONSTANTE_NUMERO_TRES = 3;
	
	private static final int CONSTANTE_NUMERO_QUATRO = 4;
	
	private static final int CONSTANTE_NUMERO_SEIS = 6;
	
	private static final int CONSTANTE_NUMERO_TRINTA = 30;

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String EMAIL_CONTATO = "emailContato";

	private static final String AREA_CONTATO = "areaContato";

	private static final String RAMAL_CONTATO2 = "ramalContato2";

	private static final String TELEFONE_CONTATO2 = "telefoneContato2";

	private static final String DDD_CONTATO2 = "dddContato2";

	private static final String RAMAL_CONTATO = "ramalContato";

	private static final String TELEFONE_CONTATO = "telefoneContato";

	private static final String DDD_CONTATO = "dddContato";

	private static final String NOME_CONTATO = "nomeContato";

	private static final String ID_PROFISSAO = "idProfissao";

	private static final String ID_TIPO_CONTATO = "idTipoContato";

	private static final Logger LOG = Logger.getLogger(AjaxService.class);

	private static final Map<String, Class<?>> PRIMITIVE_CLASSES = new HashMap<String, Class<?>>();

	/** The Constant VALOR_MEDIO_TARIFA. */
	private static final String VALOR_MEDIO_TARIFA = "valorMedioTarifa";

	/** The Constant VALOR_MENSAL_GASTO. */
	private static final String VALOR_MENSAL_GASTO = "valorMensalGasto";

	/** The Constant ID_LOCALIDADE. */
	private static final String ID_LOCALIDADE = "idLocalidade";

	/** A Constante ID_CITY_GATE. */
	private static final String ID_CITY_GATE = "idCityGate";

	/** The Constant NIVEL. */
	private static final String NIVEL = "nivelTipoUnidade";

	/** The Constant HABILITADO. */
	private static final String HABILITADO = "habilitado";
	
	/** The Constant OBRIGATORIO. */
	private static final String OBRIGATORIO = "obrigatorio";

	/** The Constant OBSERVACAO_VIGENCIA. */
	private static final String OBSERVACAO_VIGENCIA = "observacaoVigencia";

	/** The Constant DESCRICAO_VIGENCIA_TIPO_CALCULO. */
	private static final String DESCRICAO_VIGENCIA_TIPO_CALCULO = "descricaoVigenciaTipoCalculo";

	/** The Constant DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO. */
	private static final String DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO = "descricaoVigenciaVolumeBaseCalculo";

	/** The Constant DESCRICAO_VIGENCIA_UNIDADE_MONETARIA. */
	private static final String DESCRICAO_VIGENCIA_UNIDADE_MONETARIA = "descricaoVigenciaUnidadeMonetaria";

	/** The Constant OBSERVACAO_VIGENCIA_DESCONTO. */
	private static final String OBSERVACAO_VIGENCIA_DESCONTO = "descricaoVigenciaDesconto";

	/** The Constant DATA_PERIODO_INICIAL. */
	private static final String DATA_PERIODO_INICIAL = "dataPeriodoInicial";

	/** The Constant DATA_PERIODO_FINAL. */
	private static final String DATA_PERIODO_FINAL = "dataPeriodoFinal";

	/** The Constant DIAS_CONSUMO. */
	private static final String DIAS_CONSUMO = "diasConsumo";

	/** The Constant PERIODICIDADE_SENHA. */
	private static final String PERIODICIDADE_SENHA = "periodicidadeSenha";

	/** The Constant STATUS_PENDENTE. */
	private static final String STATUS_PENDENTE = "statusPendente";

	/** The Constant BASE_CALCULO_ICMS_SUBSTITUIDO. */
	private static final String BASE_CALCULO_ICMS_SUBSTITUIDO = "baseCalculoIcms";

	/** The Constant ICMS_SUBSTITUIDO_METRO_CUBICO. */
	private static final String ICMS_SUBSTITUIDO_METRO_CUBICO = "icmsSubstituido";

	/** The Constant MENSAGEM_ICMS_SUBSTITUIDO. */
	private static final String MENSAGEM_ICMS_SUBSTITUIDO = "mensagemIcmsSubstituido";

	/** The Constant STATUS. */
	private static final String STATUS = "status";

	/** The Constant TIPO_SUBSTITUTO. */
	private static final String TIPO_SUBSTITUTO = "TIPO_SUBSTITUTO";

	/** The Constant FATURAR_GRUPO. */
	private static final String FATURAR_GRUPO = "FATURAR GRUPO";

	/** The Constant INTEGRAR NOTAS FISCAIS. */
	private static final String INTEGRAR_NOTAS_FISCAIS = "Integração Portal NFE";

	/** The Constant GERAR ARQUIVOS SEFAZ. */
	private static final String GERAR_ARQUIVOS_SEFAZ = "Gerar Arquivos SEFAZ";

	/** The Constant INTEGRAR RETORNO NFE. */
	private static final String RETORNO_NFE = "RETORNO NFE";

	/** The Constant USUARIO RESPONSAVEL. */
	private static final String USUARIO_RESPONSAVEl_PROCESSO = "usuarioResponsavel";

	/** The Constant UNIDADE ORGANIZACIONAL. */
	private static final String UNIDADE_ORGANIZACIONAL_PROCESSO = "unidadeOrganizacional";

	/** The Constant DATA INICIO DE EXECUCAO DO PROCESSO. */
	private static final String DATA_INICIO_EXECUCAO_PROCESSO = "dataInicioExecucaoProcesso";

	/** The Constant DATA FIM DE EXECUCAO DO PROCESSO. */
	private static final String DATA_FIM_EXECUCAO_PROCESSO = "dataFimExecucaoProcesso";

	/** The Constant DESCRICAO DO PROCESSO. */
	private static final String DESCRICAO_PROCESSO = "descricaoProcesso";

	/** The Constant TEMPO DE EXECUCAO. */
	private static final String TEMPO_EXECUCAO_PROCESSO = "tempoExecucao";

	private static final String TELEFONE = "telefone";

	private static final String NOME_FANTASIA = "nomeFantasia";

	private static final String STATUS_VIGENCIA_NAO_AUTORIZADA = "Aguardando Autorizacao";

	/** status não informado */
	private static final String STATUS_NAO_INFORMADO = "Não Informado";

	/** The Constant CEM. */
	private static final Integer CEM = 100;

	/** The Constant DUAS_CASAS_DECIMAIS. */
	private static final Integer DUAS_CASAS_DECIMAIS = 2;

	/** The Constant COMPARATOR_ANO_DESCENDENTE. */
	private static final Comparator<Integer> COMPARATOR_ANO_DESCENDENTE = new Comparator<Integer>(){

		@Override
		public int compare(Integer i1, Integer i2) {

			return -i1.compareTo(i2);
		}
	};

	/** Constantes utilizadas para exibir condições de medição na listagem de imóvel */
	private static final String ROTA_GRUPO_FATURAMENTO_DESCRICAO = "rota.grupoFaturamento.descricao";
	private static final String CITY_GATES_DESCRICAO = "cityGates.descricao";
	private static final String CITY_GATES_VALIDO = "cityGates.valido";
	private static final String ROTA_GRUPO_FATURAMENTO_VALIDO = "rota.grupoFaturamento.valido";
	private static final String ROTA_PERIODICIDADE_DESCRICAO = "rota.periodicidade.descricao";
	private static final String ROTA_PERIODICIDADE_VALIDO = "rota.periodicidade.valido";
	private static final String ROTA_TIPO_LEITURA_DESCRICAO = "rota.tipoLeitura.descricao";
	private static final String ROTA_TIPO_LEITURA_VALIDO = "rota.tipoLeitura.valido";
	private static final String PONTO_CONSUMO_DESCRICAO = "pontoConsumo.descricao";

	/** The fachada. */
	private final Fachada fachada;

	/** The mensagens. */
	private final Properties mensagens;

	private static final String TIPO_CONVENIO = "tipoConvenio";

	private static final String CPF = "cpf";
	private static final String CNPJ = "cnpj";
	private static final String EMAIL = "email";
	private static final String NOME = "nome";
	private static final String NUMERO_PASSAPORTE = "numeroPassaporte";
	private static final String RG = "rg";

	/**
	 * Construtor padrão.
	 *
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public AjaxService() throws GGASException {

		fachada = Fachada.getInstancia();
		mensagens = Util.lerArquivoPropriedades("mensagens.properties");
	}

	/**
	 * Método responsável por consultar as unidades organizacionais filtradas
	 * pela empresa selecionada.
	 *
	 * @param idEmpresa
	 *            A chave primária da empresa selecionada.
	 * @return Um mapa de String
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, String> consultarUnidadesOrganizacionais(Long idEmpresa) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa unidades organizacionais pela
		// chave da empresa
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idEmpresa", idEmpresa);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<UnidadeOrganizacional> unidades = fachada.consultarUnidadeOrganizacional(filtro);

		if (unidades != null && !unidades.isEmpty()) {
			for (UnidadeOrganizacional unidadeOrganizacional : unidades) {
				dados.put(String.valueOf(unidadeOrganizacional.getChavePrimaria()), unidadeOrganizacional.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais filtradas pela
	 * empresa
	 * selecionada.
	 *
	 * @param idUnidadeOrganizacional the id unidade organizacional
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarFuncionarios(Long idUnidadeOrganizacional) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa unidades organizacionais pela
		// chave da empresa
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idUnidadeOrganizacional", idUnidadeOrganizacional);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Funcionario> funcionarios = fachada.consultarFuncionarios(filtro);

		if (funcionarios != null && !funcionarios.isEmpty()) {
			for (Funcionario funcionario : funcionarios) {
				dados.put(String.valueOf(funcionario.getChavePrimaria()), funcionario.getNome());
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais filtradas pela
	 * empresa
	 * selecionada.
	 *
	 * @param idFuncionario the id funcionario
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object[]> consultarFuncionario(Long idFuncionario) throws GGASException {

		Map<String, Object[]> dados = new LinkedHashMap<String, Object[]>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idFuncionario);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Funcionario> funcionarios = fachada.consultarFuncionarios(filtro);

		if (funcionarios != null && !funcionarios.isEmpty()) {
			for (Funcionario funcionario : funcionarios) {
				dados.put("email", new Object[] {funcionario.getEmail()});

			}

		}

		filtro = new HashMap<String, Object>();
		filtro.put("funcionario", idFuncionario);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Usuario> usuarios = fachada.consultarUsuarios(filtro, "papeis");
		Collection<Papel> listaPapelCadastrado = new ArrayList<Papel>();
		Object[] arrayPapelCadastrado = null;
		Object[] arrayPapelSelecionado = null;

		if (usuarios != null && !usuarios.isEmpty()) {
			for (Usuario usuario : usuarios) {
				dados.put("login", new Object[] {usuario.getLogin()});
				dados.put("usuarioDominio", new Object[] {usuario.getUsuarioDominio()});
				dados.put("senhaExpirada", new Object[] {usuario.isSenhaExpirada()});
				dados.put(PERIODICIDADE_SENHA, new Object[] {String.valueOf(usuario.getPeriodicidadeSenha())});

				if (usuario.getPapeis() != null && !usuario.getPapeis().isEmpty()) {
					Collection<Papel> listaPapel = fachada.consultarPapeisUsuario();

					if (listaPapel != null && !listaPapel.isEmpty()) {
						for (Papel papel : listaPapel) {
							if (!usuario.getPapeis().contains(papel)) {
								listaPapelCadastrado.add(papel);
							}

						}

					}
				}
				int count = CONSTANTE_NUMERO_ZERO;
				if (!listaPapelCadastrado.isEmpty()) {
					arrayPapelCadastrado = new Object[listaPapelCadastrado.size() * 2];

					for (Papel papel : listaPapelCadastrado) {
						arrayPapelCadastrado[count] = papel.getDescricao();
						++count;
						arrayPapelCadastrado[count] = papel.getChavePrimaria();
						count += 1;
					}
				}

				count = CONSTANTE_NUMERO_ZERO;
				if (!usuario.getPapeis().isEmpty()) {
					arrayPapelSelecionado = new Object[usuario.getPapeis().size() * 2];

					for (Papel papel : usuario.getPapeis()) {
						arrayPapelSelecionado[count] = papel.getDescricao();
						++count;
						arrayPapelSelecionado[count] = papel.getChavePrimaria();
						count += 1;
					}
				}
				dados.put("listaPapel", arrayPapelCadastrado);
				dados.put("listaPapeisSelecionados", arrayPapelSelecionado);
			}
		} else {
			int count = CONSTANTE_NUMERO_ZERO;
			arrayPapelCadastrado = new Object[fachada.consultarPapeisUsuario().size() * CONSTANTE_NUMERO_DOIS];

			for (Papel papel : fachada.consultarPapeisUsuario()) {
				arrayPapelCadastrado[count] = papel.getDescricao();
				++count;
				arrayPapelCadastrado[count] = papel.getChavePrimaria();
				count += 1;
			}

			dados.put("listaPapel", arrayPapelCadastrado);
			dados.put("listaPapeisSelecionados", arrayPapelSelecionado);
		}

		return dados;
	}

	/**
	 * Obter grupo faturamento.
	 *
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @param idCronogramaFaturamento the id cronograma faturamento
	 * @param mesAnoPartida the mes ano partida
	 * @param cicloPartida the ciclo partida
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterGrupoFaturamento(Long idGrupoFaturamento, Long idCronogramaFaturamento, String mesAnoPartida,
					String cicloPartida) throws GGASException {

		Map<String, String> dados;

		try {
			dados = fachada.mapearCamposGrupoFaturamento(idGrupoFaturamento, idCronogramaFaturamento, mesAnoPartida, cicloPartida);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			dados = null;
		}

		return dados;

	}

	/**
	 * Calcular desconto parcelamento.
	 *
	 * @param valor the valor
	 * @param desconto the desconto
	 * @param juros the juros
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> calcularDescontoParcelamento(String valor, String desconto, String juros) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		BigDecimal valorTotalLiquido = BigDecimal.ZERO;
		BigDecimal valorTotalDesconto = BigDecimal.ZERO;
		BigDecimal valorTaxaJuros = BigDecimal.ZERO;

		if (valor != null && !valor.isEmpty()) {
			BigDecimal valorTotal = new BigDecimal(valor).setScale(CONSTANTE_NUMERO_SEIS, RoundingMode.HALF_UP);

			// aplica juros
			if (juros != null && !juros.isEmpty()) {
				valorTaxaJuros = new BigDecimal(juros.replace(",", "."));
				valorTaxaJuros = valorTaxaJuros.divide(new BigDecimal(CEM)).add(BigDecimal.ONE);
				valorTotal = valorTotal.multiply(valorTaxaJuros);
			}

			// aplica descontos
			if ((desconto != null && !desconto.isEmpty()) && valorTotal.compareTo(BigDecimal.ZERO) >= 1) {
				BigDecimal valorDesc = new BigDecimal(desconto.replace(",", "."));
				valorTotalDesconto = valorDesc.divide(new BigDecimal(CEM)).multiply(valorTotal);
				valorTotal = valorTotal.subtract(valorTotalDesconto);
				valorTotalDesconto = valorTotalDesconto.setScale(6, RoundingMode.HALF_UP);
			}

			valorTotalLiquido = valorTotal.setScale(CONSTANTE_NUMERO_SEIS, RoundingMode.UP);
		}

		dados.put("valorTotalLiquido", valorTotalLiquido.toString());
		dados.put("valorTotalDesconto", valorTotalDesconto.toString());

		return dados;
	}

	/**
	 * Aplicar correcao monetaria.
	 *
	 * @param valor the valor
	 * @param chaveIndiceFinanceiro the chave indice financeiro
	 * @param dataVencimento the data vencimento
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> aplicarCorrecaoMonetaria(String valor, Long chaveIndiceFinanceiro, String dataVencimento)
					throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		BigDecimal valorCorrigido = new BigDecimal(valor).setScale(CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP);

		// aplica correção monetária
		if (!dataVencimento.isEmpty() && !"0".equals(valor) && chaveIndiceFinanceiro != null && chaveIndiceFinanceiro > 0) {
			Date dataInicio = Util.converterCampoStringParaData("Data", dataVencimento, FORMATO_DATA_BR);
			valorCorrigido =
							fachada.calcularJurosEntreDatasIndiceFinanceiro(dataInicio, Calendar.getInstance().getTime(),
											chaveIndiceFinanceiro, valorCorrigido);
		}

		dados.put("valorCorrigido", valorCorrigido.toString());

		return dados;
	}

	/**
	 * Aplicar debito credito.
	 *
	 * @param valor the valor
	 * @param creditoOrigem the credito origem
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> aplicarDebitoCredito(String valor, String creditoOrigem) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		BigDecimal valorTotal = new BigDecimal(valor);

		// aplica débito ou crédito
		if (!creditoOrigem.isEmpty()) {
			valorTotal = valorTotal.multiply(new BigDecimal(-1));
		}

		dados.put("valorTotal", valorTotal.toString());

		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * localidades filtradas pela gerência
	 * regional selecionada.
	 *
	 * @param idGerenciaRegional A chave primária da gerência
	 *            regional selecionada.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarLocalidadesPorGerenciaRegional(Long idGerenciaRegional) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa localidades pela chave da
		// gerência regional
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idGerenciaRegional", idGerenciaRegional);
		filtro.put(HABILITADO, Boolean.TRUE);

		Collection<Localidade> localidades = fachada.consultarLocalidades(filtro);
		if (localidades != null && !localidades.isEmpty()) {
			for (Localidade localidade : localidades) {
				dados.put(String.valueOf(localidade.getChavePrimaria()), localidade.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar os
	 * municípios filtrados pela unidade
	 * federativa selecionada.
	 *
	 * @param idUnidadeFederacao A chave primária da unidade
	 *            federativa selecionada.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMunicipiosPorUnidadeFederativa(Long idUnidadeFederacao) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idUnidadeFederacao", idUnidadeFederacao);
		filtro.put(HABILITADO, Boolean.TRUE);

		Collection<Municipio> listaMunicipio = fachada.consultarMunicipios(filtro);

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Municipio municipio : listaMunicipio) {
			sortedMap.put(municipio.getDescricao(), String.valueOf(municipio.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getKey(), ordenar.getValue());
		}

		// Collection<Municipio> listaMunicipio = fachada.consultarMunicipios(filtro);
		// if (listaMunicipio != null && !listaMunicipio.isEmpty()) {
		// for (Municipio municipio : listaMunicipio) {
		// dados.put(String.valueOf(municipio.getChavePrimaria()), municipio.getDescricao());
		// }
		// }
		return dados;
	}

	/**
	 * Método responsável por consultar os
	 * municípios filtrados pela unidade
	 * federativa selecionada.
	 *
	 * @param siglaUnidadeFederacaoPontoConsumo A chave primária da unidade
	 *            federativa selecionada.
	 * @return Um mapa de Strings
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMunicipiosPorUnidadeFederativaPorSigla(String siglaUnidadeFederacaoPontoConsumo) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("siglaUnidadeFederacao", siglaUnidadeFederacaoPontoConsumo);
		filtro.put(HABILITADO, Boolean.TRUE);

		Collection<Municipio> listaMunicipio = fachada.consultarMunicipios(filtro);

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Municipio municipio : listaMunicipio) {
			sortedMap.put(municipio.getDescricao(), String.valueOf(municipio.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getKey(), ordenar.getValue());
		}
		return dados;
	}

	/**
	 * Método responsável por consultar os
	 * municípios filtrados pela unidade
	 * federativa selecionada.
	 *
	 * @param siglaUnidadeFederacaoPontoConsumoChamado A chave primária da unidade
	 *            federativa selecionada.
	 * @return Um mapa de Strings
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMunicipiosPorUnidadeFederativaPorSiglaChamado(String siglaUnidadeFederacaoPontoConsumoChamado)
			throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("siglaUnidadeFederacao", siglaUnidadeFederacaoPontoConsumoChamado);
		filtro.put(HABILITADO, Boolean.TRUE);

		Collection<Municipio> listaMunicipio = fachada.consultarMunicipios(filtro);

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Municipio municipio : listaMunicipio) {
			sortedMap.put(municipio.getDescricao(), String.valueOf(municipio.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getKey(), ordenar.getValue());
		}
		return dados;
	}

	/**
	 * Método responsável por consultar os
	 * bairros filtrados pelo município selecionado.
	 *
	 * @param nomeMunicipioPontoConsumo A chave primária do município selecionado.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */

	public Map<String, String> consultarBairrosPorMunicipio(String nomeMunicipioPontoConsumo) {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		ArrayList<String> listaBairro = (ArrayList<String>) fachada.consultarBairros(nomeMunicipioPontoConsumo);

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (String bairro : listaBairro) {
			sortedMap.put(bairro, bairro);
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getKey(), ordenar.getValue());
		}
		return dados;
	}



	/**
	 * Obter gerencia regional.
	 *
	 * @param idGerenciaRegional the id gerencia regional
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterGerenciaRegional(Long idGerenciaRegional) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		GerenciaRegional gerenciaRegional = fachada.buscarGerenciaRegionalPorChave(idGerenciaRegional);
		if (gerenciaRegional != null) {
			dados.put("nome", gerenciaRegional.getNome());
			dados.put("sigla", gerenciaRegional.getNomeAbreviado());
			dados.put(gerenciaRegional.getNome(), gerenciaRegional.getNomeAbreviado());
		}
		return dados;

	}

	/**
	 * Método responsável por consultar os
	 * municípios filtrados pela unidade
	 * federação selecionado.
	 *
	 * @param codigoUnidadeFederacao o código da unidade federação
	 *            selecionado.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMunicipiosPorUnidadeFederacao(Integer codigoUnidadeFederacao) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		// Pesquisa municípios pelo código da
		// unidade federação
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("codigoUnidadeFederacao", codigoUnidadeFederacao);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Municipio> municipios = fachada.consultarMunicipios(filtro);

		if (municipios != null && !municipios.isEmpty()) {
			for (Municipio municipio : municipios) {
				dados.put(String.valueOf(municipio.getChavePrimaria()), municipio.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por consultar os setores
	 * comerciais a partir da localização
	 * informada.
	 *
	 * @param idLocalidade a chave da Localização.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarSetorComercialPorLocalidade(Long idLocalidade) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		// Pesquisa os setores comerciais pela
		// localização.
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_LOCALIDADE, idLocalidade);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<SetorComercial> setores = fachada.consultarSetorComercial(filtro);

		if (setores != null && !setores.isEmpty()) {
			for (SetorComercial setorComercial : setores) {
				dados.put(String.valueOf(setorComercial.getChavePrimaria()), setorComercial.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Verificar numero maximo pontos consumo.
	 *
	 * @param idRotaRemanejamento the id rota remanejamento
	 * @param numeroPontosConsumo the numero pontos consumo
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	public boolean verificarNumeroMaximoPontosConsumo(Long idRotaRemanejamento, Integer numeroPontosConsumo) throws GGASException {

		Map<Long, Integer> mapaQuantidadePontos = RotaAction.getMapaQuantidadePontos();

		Integer somaTotalPontos;
		Boolean isExcedeuLimite = Boolean.FALSE;
		Rota rota = fachada.buscarRota(idRotaRemanejamento);
		Collection<PontoConsumo> lista = fachada.listarPontosConsumoPorChaveRota(idRotaRemanejamento);
		Integer tamanhoPontosConsumo = lista.size();
		Integer numeroMaximoPontosConsumo = rota.getNumeroMaximoPontosConsumo();

		if (!mapaQuantidadePontos.containsKey(idRotaRemanejamento)) {
			somaTotalPontos = tamanhoPontosConsumo + numeroPontosConsumo;
			mapaQuantidadePontos.put(idRotaRemanejamento, somaTotalPontos);
		} else {
			Integer quantidade = mapaQuantidadePontos.get(idRotaRemanejamento);
			somaTotalPontos = quantidade + numeroPontosConsumo;
			mapaQuantidadePontos.put(idRotaRemanejamento, somaTotalPontos);
		}

		if (rota.getNumeroMaximoPontosConsumo() != null && numeroMaximoPontosConsumo < somaTotalPontos) {
			isExcedeuLimite = Boolean.TRUE;
		}

		return isExcedeuLimite;

	}

	/**
	 * Gets the quantidade dias vencimento documento complementar.
	 *
	 * @return the quantidade dias vencimento documento complementar
	 * @throws GGASException the GGAS exception
	 */
	public String getQuantidadeDiasVencimentoDocumentoComplementar() throws GGASException {

		String retorno = null;
		String valor = fachada.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_DIAS_VENCIMENTO_DOCUMENTO_COMPLEMENTAR).getValor();
		if (valor != null) {
			retorno = valor;
		}
		return retorno;
	}

	/**
	 * Inits the mapa contador.
	 */
	public void initMapaContador() {

		RotaAction.initMapaControleTamanhoPontosConsumo();
	}

	/**
	 * Consultar cep.
	 *
	 * @param logradouro the logradouro
	 * @param municipio the municipio
	 * @param uf the uf
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarCep(String logradouro, String municipio, String uf) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Collection<Cep> ceps = fachada.consultarCeps(uf, municipio, logradouro);

		for (Iterator<Cep> iterator = ceps.iterator(); iterator.hasNext();) {
			Cep cep = iterator.next();
			StringBuilder enderecoCompleto = new StringBuilder();
			if (cep.getTipoLogradouro() != null) {
				enderecoCompleto.append(cep.getTipoLogradouro());
			}
			if (cep.getLogradouro() != null) {
				enderecoCompleto.append(" ").append(cep.getLogradouro());
			}
			if (cep.getBairro() != null) {
				enderecoCompleto.append(", ").append(cep.getBairro());
			}
			if (cep.getCep() != null) {
				enderecoCompleto.append(", ").append(cep.getCep());
			}
			if (cep.getIntervaloNumeracao() != null) {
				enderecoCompleto.append(", ").append(cep.getIntervaloNumeracao());
			}
			dados.put(String.valueOf(cep.getChavePrimaria()), enderecoCompleto.toString());
		}

		return dados;
	}

	/**
	 * Consultar endereco.
	 *
	 * @param cep the cep
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<Long, String[]> consultarEndereco(String cep) throws GGASException {

		Collection<Cep> listaCeps = null;

		if (!StringUtils.isEmpty(cep)) {
			try {
				listaCeps = fachada.obterCeps(cep, true);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				return null;
			}
		}

		Map<Long, String[]> cepsEncontrados = new HashMap<Long, String[]>();

		if (listaCeps != null) {

			for (Cep cepEncontrado : listaCeps) {

				String tipoLogradouroCep = "";
				if (cepEncontrado.getTipoLogradouro() != null) {
					tipoLogradouroCep = cepEncontrado.getTipoLogradouro();
				}
				String logradouroCep = "";
				if (cepEncontrado.getLogradouro() != null) {
					logradouroCep = cepEncontrado.getLogradouro();
				}
				String bairroCep = "";
				if (cepEncontrado.getBairro() != null) {
					bairroCep = cepEncontrado.getBairro();
				}
				String nomeMunicipioCep = "";
				if (cepEncontrado.getNomeMunicipio() != null) {
					nomeMunicipioCep = cepEncontrado.getNomeMunicipio();
				}
				String ufCep = "";
				if (cepEncontrado.getUf() != null) {
					ufCep = cepEncontrado.getUf();
				}
				String[] endereco =
								{
									cepEncontrado.getCep(), (tipoLogradouroCep) + " " + logradouroCep, bairroCep, nomeMunicipioCep, ufCep};

				cepsEncontrados.put(cepEncontrado.getChavePrimaria(), endereco);
			}
		}

		return cepsEncontrados;
	}

	/**
	 * Consultar cep por id.
	 *
	 * @param idCep the id cep
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] consultarCepPorId(String idCep) throws GGASException {

		Cep cepEncontrado = null;

		if (!StringUtils.isEmpty(idCep)) {
			try {
				cepEncontrado = fachada.obterCepPorChave(Util.converterCampoStringParaValorLong("CEP", idCep));
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				return null;
			}
		}

		String[] endereco = new String[CONSTANTE_NUMERO_QUATRO];

		if (cepEncontrado != null) {
			endereco[CONSTANTE_NUMERO_ZERO] = cepEncontrado.getTipoLogradouro() + " " + cepEncontrado.getLogradouro();
			endereco[CONSTANTE_NUMERO_UM] = cepEncontrado.getBairro();
			endereco[CONSTANTE_NUMERO_DOIS] = cepEncontrado.getNomeMunicipio();
			endereco[CONSTANTE_NUMERO_TRES] = cepEncontrado.getUf();
		}

		return endereco;
	}

	/**
	 * Método responsável por consultar as
	 * unidades de negócio filtradas pela
	 * gerência regional selecionada.
	 *
	 * @param idGerenciaRegional A chave primária da gerência
	 *            regional
	 *            selecionada.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarUnidadeNegocioPorGerenciaRegional(Long idGerenciaRegional) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		// Pesquisa localidades pela chave da
		// gerência regional
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idGerenciaRegional", idGerenciaRegional);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<UnidadeNegocio> unidades = fachada.consultarUnidadesNegocio(filtro);

		if (unidades != null && !unidades.isEmpty()) {
			for (UnidadeNegocio unidadeNegocio : unidades) {
				dados.put(String.valueOf(unidadeNegocio.getChavePrimaria()), unidadeNegocio.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Consultar cliente por id.
	 *
	 * @param idCliente the id cliente
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String consultarClientePorId(Long idCliente) throws GGASException {

		String retorno = "";

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idCliente);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Cliente> listaCliente = fachada.consultarClientes(filtro);

		if ((listaCliente != null) && (!listaCliente.isEmpty())) {
			for (Cliente cliente : listaCliente) {
				retorno = cliente.getNome();
				break;
			}
		}

		return retorno;
	}

	/**
	 * Recupera o cliente com a chave primária
	 * informada.
	 *
	 * @param idCliente Chave primária do cliente a ser
	 *            obtido.
	 * @return Um cliente.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterClientePorChave(Long idCliente) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idCliente == null || idCliente == 0) {
			dados = null;
		} else {
			Cliente cliente = fachada.obterCliente(idCliente, Cliente.PROPRIEDADES_LAZY);

			if (cliente != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(cliente.getChavePrimaria()).toString());
				dados.put("nome", cliente.getNome());
				if (!StringUtils.isEmpty(cliente.getCnpj())) {
					dados.put("cnpj", cliente.getCnpjFormatado());
				}
				if (!StringUtils.isEmpty(cliente.getCpf())) {
					dados.put("cpf", cliente.getCpfFormatado());
				}

				if (cliente.getEmailPrincipal() != null) {
					dados.put("email", cliente.getEmailPrincipal());
				} else {
					dados.put("email", "");
				}

				ClienteEndereco enderecoPrincipal = null;
				if (cliente.getEnderecos() != null && !cliente.getEnderecos().isEmpty()) {
					for (ClienteEndereco endereco : cliente.getEnderecos()) {
						if (endereco.getCorrespondencia() != null && endereco.getCorrespondencia()) {
							enderecoPrincipal = endereco;
							break;
						}
					}
				}
				if (enderecoPrincipal == null && cliente.getEnderecos() != null && !cliente.getEnderecos().isEmpty()) {
					enderecoPrincipal = cliente.getEnderecos().iterator().next();
				}
				if (enderecoPrincipal != null) {
					dados.put("enderecoFormatado", enderecoPrincipal.getEnderecoFormatado());
				} else {
					dados.put("enderecoFormatado", "");
				}

				if (cliente.getNomeFantasia() != null && !cliente.getNomeFantasia().isEmpty()) {
					dados.put(NOME_FANTASIA, cliente.getNomeFantasia());
				} else {
					dados.put(NOME_FANTASIA, "");
				}

				if (cliente.getFones() != null && !cliente.getFones().isEmpty()) {
					ClienteFone fonePrincipal = null;
					for (ClienteFone fone : cliente.getFones()) {
						if (fone.getIndicadorPrincipal()) {
							fonePrincipal = fone;
							break;
						}
					}
					if (fonePrincipal != null) {
						dados.put(TELEFONE, "(" + fonePrincipal.getCodigoDDD() + ") " + fonePrincipal.getNumero());
					} else {
						dados.put(TELEFONE, "");
					}
				} else {
					dados.put(TELEFONE, "");
				}

				if (cliente.getNumeroPassaporte() != null && !cliente.getNumeroPassaporte().isEmpty()) {
					dados.put("numeroPassaporte", cliente.getNumeroPassaporte());
				} else {
					dados.put("numeroPassaporte", "");
				}

				if (StringUtils.isNotBlank(cliente.getRg())) {
					dados.put("rg", cliente.getRg());
				} else {
					dados.put("rg", "");
				}
				
				if (cliente.getTipoCliente() != null
						&& StringUtils.isNotBlank(cliente.getTipoCliente().getDescricao())) {
					dados.put("tipoCliente", cliente.getTipoCliente().getDescricao());
				}
			}
		}

		return dados;
	}

	/**
	 * Recupera o cliente com a chave primária
	 * informada.
	 *
	 * @param cpfCnpj - {@link String}
	 * @return Um cliente.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> carregarClientePorCpfCnpj(String cpfCnpj) throws GGASException {
		Map<String, String> dados = new HashMap<String, String>();

			if (cpfCnpj == null) {
				dados = null;
			} else {
				try{
					Cliente cliente = fachada.obterClientePorCpfCnpj(StringUtil.retirarMascara(cpfCnpj));
		
					if (cliente != null) {
						dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(cliente.getChavePrimaria()).toString());
						dados.put(EMAIL, "");
						dados.put(RG, "");
						dados.put(NOME, "");
						dados.put(NUMERO_PASSAPORTE, "");
						dados.put(TELEFONE, "");
						dados.put(NOME_FANTASIA, "");
						dados.put(EMAIL, "");

						if(!StringUtils.isEmpty(cliente.getNome())){
							dados.put(NOME, cliente.getNome());
						}
						if (!StringUtils.isEmpty(cliente.getCnpj())) {
							dados.put(CNPJ, cliente.getCnpjFormatado());
						}
						if (!StringUtils.isEmpty(cliente.getCpf())) {
							dados.put(CPF, cliente.getCpfFormatado());
						}
		
						if (cliente.getEmailPrincipal() != null) {
							dados.put(EMAIL, cliente.getEmailPrincipal());
						} 
						
						if (cliente.getNomeFantasia() != null && !cliente.getNomeFantasia().isEmpty()) {
							dados.put(NOME_FANTASIA, cliente.getNomeFantasia());
						} 
		
						if (cliente.getFones() != null && !cliente.getFones().isEmpty()) {
							ClienteFone fonePrincipal = null;
							for (ClienteFone fone : cliente.getFones()) {
								if (fone.getIndicadorPrincipal()) {
									fonePrincipal = fone;
									break;
								}
							}
							if (fonePrincipal != null) {
								dados.put(TELEFONE, "(" + fonePrincipal.getCodigoDDD() + ") " + fonePrincipal.getNumero());
							}
						} 
		
						if (cliente.getNumeroPassaporte() != null && !cliente.getNumeroPassaporte().isEmpty()) {
							dados.put(NUMERO_PASSAPORTE, cliente.getNumeroPassaporte());
						} 
		
						if (StringUtils.isNotBlank(cliente.getRg())) {
							dados.put(RG, cliente.getRg());
						}
					}else{
						dados.put(EMAIL, "");
						dados.put(RG, "");
						dados.put(NUMERO_PASSAPORTE, "");
						dados.put(TELEFONE, "");
						dados.put(NOME_FANTASIA, "");
						dados.put(NOME, "");
					}
				}catch(NegocioException e){
					LOG.error(e.getMessage(), e);
				}
			}
		return dados;
	}
	
	/**
	 * Obter tipo cliente por id tipo cliente.
	 *
	 * @param idTipoCliente
	 *            the id tipo cliente
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, String> obterTipoClientePorIdTipoCliente(Long idTipoCliente) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idTipoCliente == null || idTipoCliente == 0) {
			dados = null;
		} else {
			TipoCliente tipoCliente = fachada.obterTipoClientePorIdTipoCliente(idTipoCliente);

			if (tipoCliente != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(tipoCliente.getChavePrimaria()).toString());
				dados.put("cdTipoPessoa", String.valueOf(tipoCliente.getTipoPessoa().getCodigo()));
			}
		}

		return dados;
	}

	/**
	 * Recupera o medidor com a chave primária
	 * informada.
	 *
	 * @param idMedidor Chave primária do cliente a ser
	 *            obtido.
	 * @return Um Medidor.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterMedidorPorChave(Long idMedidor) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idMedidor == null) {
			dados = null;
		} else {
			Medidor medidor = fachada.obterMedidor(idMedidor, Cliente.PROPRIEDADES_LAZY);

			if (medidor != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(medidor.getChavePrimaria()).toString());
				dados.put("numeroSerie", String.valueOf(medidor.getNumeroSerie()));
				if (medidor.getModelo() != null) {
					dados.put("descricaoModelo", medidor.getModelo().getDescricao());
				}
				dados.put("tipo", medidor.getTipoMedidor().getDescricao());

				dados.put("marca", medidor.getMarcaMedidor().getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Obter atividade cronograma faturamento.
	 *
	 * @param idOperacao the id operacao
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterAtividadeCronogramaFaturamento(Long idOperacao) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idOperacao != null) {
			Collection<AtividadeSistema> listaAtividadeSistema = fachada.listarAtividadeSistemaPorOperacao(idOperacao);

			Operacao operacao = fachada.buscarOperacaoPorChave(idOperacao);

			if ((listaAtividadeSistema != null) && (!listaAtividadeSistema.isEmpty())) {
				AtividadeSistema atividadeSistema = ((List<AtividadeSistema>) listaAtividadeSistema).get(0);
				dados.put("ehAtividadeCronograma", String.valueOf(atividadeSistema.isCronograma()));

				if (atividadeSistema.isEnviaEmail() && !StringUtils.isEmpty(atividadeSistema.getEmail())) {
					dados.put("emailExecucao", atividadeSistema.getEmail());
				}
				if (atividadeSistema.getHoraInicial() != null) {
					dados.put("horaInicio", String.valueOf(atividadeSistema.getHoraInicial()));
				}
				if (atividadeSistema.getHoraFinal() != null) {
					dados.put("horaFim", String.valueOf(atividadeSistema.getHoraFinal()));
				}
				if (FATURAR_GRUPO.equals(atividadeSistema.getDescricao())) {
					dados.put("isFaturarGrupo", "true");
				}
				if(RETORNO_NFE.equals(atividadeSistema.getDescricao())) {
					dados.put("isRetornoNfe", "true");
				}
			}

			if(INTEGRAR_NOTAS_FISCAIS.equals(operacao.getDescricao())) {
				dados.put("isIntegrarNFE", "true");
			}

			if(GERAR_ARQUIVOS_SEFAZ.equals(operacao.getDescricao())) {
				dados.put("isGerarArquivosSEFAZ", "true");
			}
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades superiores pela localidade e nível
	 * hierárquico.
	 *
	 * @param idUnidadeTipo a chave do tipo da unidade.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarUnidadeSuperiorPorLocalidadeNivel(Long idUnidadeTipo) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		UnidadeTipo unidadeTipo = null;
		if (idUnidadeTipo != null && idUnidadeTipo > CONSTANTE_NUMERO_ZERO) {
			unidadeTipo = fachada.obterUnidadeTipo(idUnidadeTipo);
		}

		boolean temUnidadeTipo = false;

		if (unidadeTipo != null) {
			temUnidadeTipo = true;
		}

		if (/* temLocalidade && */temUnidadeTipo) {
			// A unidade superior tem que estar na
			// mesma localidade da unidade que
			// está sendo
			// inserida no momento.
			Map<String, Object> filtro = new HashMap<String, Object>();

			// A unidade superior tem que ser um
			// nível acima do tipo da unidade que
			// está sendo
			// inserida no momento.
			if (unidadeTipo.getNivel() != null) {
				if (unidadeTipo.getNivel() > 1) {
					filtro.put(NIVEL, unidadeTipo.getNivel() - 1);
				} else {
					filtro.put(NIVEL, unidadeTipo.getNivel());
				}
			}

			filtro.put(HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = fachada.consultarUnidadeOrganizacional(filtro);

			if (unidades != null && !unidades.isEmpty()) {
				for (UnidadeOrganizacional unidadeOrganizacional : unidades) {
					dados.put(String.valueOf(unidadeOrganizacional.getChavePrimaria()), unidadeOrganizacional.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Consultar preco gas complementar.
	 *
	 * @param idContratoCompra the id contrato compra
	 * @param dataInicioFornecimento the data inicio fornecimento
	 * @param dataFimFornecimento the data fim fornecimento
	 * @param idPrecoGas the id preco gas
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarPrecoGasComplementar(Long idContratoCompra, String dataInicioFornecimento,
					String dataFimFornecimento, Long idPrecoGas) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();
		Map<String, Object> filtro = new HashMap<String, Object>();

		Boolean bFlagDataInvalida = false;

		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put("idContratoCompra", idContratoCompra);

		try {
			filtro.put("dataInicioFornecimento", Util.converterCampoStringParaData(PrecoGas.DATA_FORNECIMENTO_INICIO,
							dataInicioFornecimento, FORMATO_DATA_BR));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			bFlagDataInvalida = true;
		}

		try {
			filtro.put("dataFimFornecimento", Util.converterCampoStringParaData(PrecoGas.DATA_FORNECIMENTO_FIM, dataFimFornecimento,
							FORMATO_DATA_BR));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			bFlagDataInvalida = true;
		}

		if (!bFlagDataInvalida) {

			filtro.put("indicadorComplementar", false);

			if (idPrecoGas != null && idPrecoGas > 0) {
				filtro.put("idPrecoGasReverso", idPrecoGas);
			}

			Collection<PrecoGas> precosGas = fachada.consultarPrecoGas(filtro);
			if (precosGas != null && !precosGas.isEmpty()) {
				for (PrecoGas precoGas : precosGas) {
					dados.put(String.valueOf(precoGas.getChavePrimaria()), precoGas.getNumeroNotaFiscal().toString());
				}
			}
		}

		return dados;
	}

	/**
	 * Consultar item preco gas complementar.
	 *
	 * @param idPrecoGas the id preco gas
	 * @param idItemFatura the id item fatura
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarItemPrecoGasComplementar(Long idPrecoGas, Long idItemFatura) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put("idPrecoGas", idPrecoGas);
		filtro.put("idItemFatura", idItemFatura);

		Collection<PrecoGasItem> precosGasItem = fachada.consultarPrecoGasItem(filtro);
		if (precosGasItem != null && !precosGasItem.isEmpty()) {
			for (PrecoGasItem precoGasItem : precosGasItem) {
				dados.put(String.valueOf(precoGasItem.getItemFatura().getChavePrimaria()), precoGasItem.getVolume().toString());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades centralizadoras pelo tipo.
	 *
	 * @param idUnidadeTipo a chave do tipo da unidade.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarUnidadeCentralizadora(Long idUnidadeTipo) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		UnidadeTipo unidadeTipo = null;
		boolean isCentralizadora = false;

		if (idUnidadeTipo != null && idUnidadeTipo > CONSTANTE_NUMERO_ZERO) {
			unidadeTipo = fachada.obterUnidadeTipo(idUnidadeTipo);
			isCentralizadora = UnidadeTipo.CENTRALIZADORA.equals(unidadeTipo.getTipo().trim());
		}

		boolean temUnidadeTipo = false;

		if (unidadeTipo != null) {
			temUnidadeTipo = true;
		}

		if (temUnidadeTipo && isCentralizadora) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			// A unidade superior tem que ser um
			// nível acima do tipo da unidade que
			// está sendo
			// inserida no momento.
			filtro.put("tipoUnidadeTipo", unidadeTipo.getTipo().trim());
			filtro.put(HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = fachada.consultarUnidadeOrganizacional(filtro);

			if (unidades != null && !unidades.isEmpty()) {
				for (UnidadeOrganizacional unidadeOrganizacional : unidades) {
					dados.put(String.valueOf(unidadeOrganizacional.getChavePrimaria()), unidadeOrganizacional.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades repavimentadoras pela localidade.
	 *
	 * @param idLocalidade a chave da localidade.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarUnidadeRepavimentadora(Long idLocalidade) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idLocalidade != null && idLocalidade > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			// A unidade repavimentadora tem que
			// ser na mesma localidade da unidade
			// que está sendo
			// inserida no momento.
			filtro.put(ID_LOCALIDADE, idLocalidade);
			filtro.put(HABILITADO, Boolean.TRUE);
			Collection<UnidadeOrganizacional> unidades = fachada.consultarUnidadeOrganizacional(filtro);

			if (unidades != null && !unidades.isEmpty()) {
				for (UnidadeOrganizacional unidadeOrganizacional : unidades) {
					dados.put(String.valueOf(unidadeOrganizacional.getChavePrimaria()), unidadeOrganizacional.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades repavimentadoras pela localidade.
	 *
	 * @param idUnidadeTipo the id unidade tipo
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarEmpresasPorTipo(Long idUnidadeTipo) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		UnidadeTipo unidadeTipo = null;
		if (idUnidadeTipo != null && idUnidadeTipo > 0) {
			unidadeTipo = fachada.obterUnidadeTipo(idUnidadeTipo);
		}

		if (unidadeTipo != null) {
			if (UnidadeTipo.TERCEIRIZADA.equals(unidadeTipo.getTipo().trim())) {

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(HABILITADO, Boolean.TRUE);
				filtro.put("principal", Boolean.FALSE);
				Collection<Empresa> empresas = fachada.consultarEmpresas(filtro);

				adicionarEmpresasNoMapa(dados, empresas);
			} else {
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("principal", Boolean.TRUE);
				filtro.put(HABILITADO, Boolean.TRUE);
				Collection<Empresa> empresas = fachada.consultarEmpresas(filtro);

				adicionarEmpresasNoMapa(dados, empresas);

			}
		}
		return dados;
	}

	/**
	 * Adicionar empresas no mapa.
	 *
	 * @param dados the dados
	 * @param empresas the empresas
	 */
	private void adicionarEmpresasNoMapa(Map<String, String> dados, Collection<Empresa> empresas) {

		if (empresas != null && !empresas.isEmpty()) {
			for (Empresa empresa : empresas) {
				dados.put(String.valueOf(empresa.getChavePrimaria()), empresa.getCliente().getNome());
			}
		}
	}

	/**
	 * Método responsável por obter o tipo da
	 * unidade tipo.
	 *
	 * @param idUnidadeTipo a chave da unidade tipo.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterUnidadeTipo(Long idUnidadeTipo) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		UnidadeTipo unidadeTipo = null;
		if (idUnidadeTipo != null && idUnidadeTipo > 0) {
			unidadeTipo = fachada.obterUnidadeTipo(idUnidadeTipo);
		}
		if (unidadeTipo != null) {
			dados.put("tipo", String.valueOf(unidadeTipo.getTipo()));
		}
		return dados;
	}

	/**
	 * Método responsável por obter a localidade
	 * por chave.
	 *
	 * @param idLocalidade a chave da localidade.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterLocalidade(Long idLocalidade) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Localidade localidade = null;
		if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
			localidade = fachada.buscarLocalidadeChave(idLocalidade);
		}
		if (localidade != null) {
			dados.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, String.valueOf(localidade.getDescricao()));
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as quadras
	 * pelo cep passado por parâmetro.
	 *
	 * @param cep O número do cep.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarQuadrasPorCep(String cep) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (cep != null) {
			Collection<Quadra> listaQuadra = fachada.listarQuadraPorCep(cep);
			if ((listaQuadra != null) && (!listaQuadra.isEmpty())) {
				Util.ordenarColecaoPorAtributo(listaQuadra, "numeroQuadra", true);
				for (Quadra quadra : listaQuadra) {
					dados.put(String.valueOf(quadra.getChavePrimaria()), String.valueOf(quadra.getNumeroQuadra()));
				}
			}
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as faces
	 * de quadra pela quadra.
	 *
	 * @param chaveQuadra the chave quadra
	 * @param cep the cep
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarFacesQuadraPorQuadra(String chaveQuadra, String cep) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		List<QuadraFace> faces = fachada.obterQuadraFacePorQuadra(Long.valueOf(chaveQuadra), cep);

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (QuadraFace face : faces) {
			sortedMap.put(face.getQuadraFaceFormatada(), String.valueOf(face.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as faces
	 * de quadra pela quadra.
	 *
	 * @param chaveQuadraFace the chave quadra face
	 * @param chavePrimariaSelect the select chave primaria
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarSitucaoImovelPorFacesQuadra(String chaveQuadraFace, String chavePrimariaSelect)
					throws GGASException {

		String selectChavePrimaria = chavePrimariaSelect;
		Map<String, String> dados = new LinkedHashMap<String, String>();
		Collection<SituacaoImovel> listaSituacoesImovel = null;
		if (selectChavePrimaria.contains(";")) {
			selectChavePrimaria = selectChavePrimaria.replace(";", "");
			selectChavePrimaria =
							String.valueOf(fachada.buscarPropostaPorChave(Long.valueOf(selectChavePrimaria)).getImovel().getChavePrimaria());
		}
		listaSituacoesImovel =
						fachada.verificarSitucaoImovelPorRedeIndicador(
										"null".equals(chaveQuadraFace) || StringUtils.isEmpty(chaveQuadraFace)
														|| "0".equals(chaveQuadraFace) ? null : chaveQuadraFace,
										"null".equals(selectChavePrimaria) || StringUtils.isEmpty(selectChavePrimaria)
														|| "0".equals(selectChavePrimaria) ? null : selectChavePrimaria);

		for (SituacaoImovel situacaoImovel : listaSituacoesImovel) {
			dados.put(String.valueOf(situacaoImovel.getChavePrimaria()), situacaoImovel.getDescricao());
		}

		return dados;
	}

	/**
	 * Método responsável por listar os ramos de
	 * atividades associados a um
	 * segmento informado.
	 *
	 * @param chavePrimariaSegmento Chave primária de um segmento.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public List<Map<String, String>> listarRamoAtividadePorSegmento(Long chavePrimariaSegmento) throws GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();
		if (chavePrimariaSegmento != null) {
			Collection<RamoAtividade> ramosAtividade = fachada.listarRamoAtividadePorSegmento(chavePrimariaSegmento);
			if (ramosAtividade != null && !ramosAtividade.isEmpty()) {
				for (RamoAtividade ramoAtividade : ramosAtividade) {
					Map<String, String> dados = new HashMap<String, String>();
					dados.put(String.valueOf(ramoAtividade.getChavePrimaria()), ramoAtividade.getDescricao());
					lista.add(dados);
				}
			}
		}
		return lista;
	}

	/**
	 * Método responsável por listar os ramos de atividades associados a um segmento informado no Imóvel.
	 *
	 * @param idRamoAtividadePontoConsumo Id ramo atividade do ponto consumo.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public List<Map<String, String>> listarRamoAtividadePorSegmentoImovel(Long idRamoAtividadePontoConsumo) throws GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();
		if (idRamoAtividadePontoConsumo != null) {
			Collection<RamoAtividade> ramosAtividade = fachada.listarRamoAtividadePorSegmentoImovel(idRamoAtividadePontoConsumo);
			if (ramosAtividade != null && !ramosAtividade.isEmpty()) {
				for (RamoAtividade ramoAtividade : ramosAtividade) {
					Map<String, String> dados = new HashMap<String, String>();
					dados.put(String.valueOf(ramoAtividade.getChavePrimaria()), ramoAtividade.getDescricao());
					lista.add(dados);
				}
			}
		}
		return lista;
	}

	/**
	 * Método responsável por listar os ramos de
	 * atividades associados a um
	 * segmento informado.
	 *
	 * @param chavePeriodicidade the chave periodicidade
	 * @param chaveTipoLeitura the chave tipo leitura
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarGrupoFaturamento(Long chavePeriodicidade, Long chaveTipoLeitura) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		if (chavePeriodicidade != null) {
			Collection<GrupoFaturamento> grupoFaturamento = fachada.listarGrupoFaturamento(chavePeriodicidade, chaveTipoLeitura);
			if (grupoFaturamento != null && !grupoFaturamento.isEmpty()) {
				for (GrupoFaturamento grupo : grupoFaturamento) {
					dados.put(String.valueOf(grupo.getChavePrimaria()), grupo.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar todas as
	 * modalidades de medição de imóvel do
	 * sistema.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarModalidadeMedicaoImovel() throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<ModalidadeMedicaoImovel> modalidades = fachada.listarModalidadeMedicaoImovel();
		if (modalidades != null && !modalidades.isEmpty()) {
			for (ModalidadeMedicaoImovel modalidadeMedicaoImovel : modalidades) {
				dados.put(String.valueOf(modalidadeMedicaoImovel.getCodigo()), modalidadeMedicaoImovel.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar os tipos de
	 * botijão do sistema.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarTipoBotijao() throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		Collection<TipoBotijao> tiposBotijao = fachada.listarTipoBotijao();
		if (tiposBotijao != null && !tiposBotijao.isEmpty()) {
			for (TipoBotijao tipoBotijao : tiposBotijao) {
				dados.put(String.valueOf(tipoBotijao.getChavePrimaria()), tipoBotijao.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as empresas
	 * classificadas como
	 * construtoras.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarEmpresasConstrutoras() throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		Collection<Empresa> empresas = fachada.listarEmpresasConstrutoras();
		if (empresas != null && !empresas.isEmpty()) {
			for (Empresa empresa : empresas) {
				dados.put(String.valueOf(empresa.getChavePrimaria()), empresa.getCliente().getNome());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar os diâmetros
	 * de rede.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarRedeDiametro() throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		Collection<RedeDiametro> diametros = fachada.listarRedeDiametro();
		if (diametros != null && !diametros.isEmpty()) {
			for (RedeDiametro redeDiametro : diametros) {
				dados.put(String.valueOf(redeDiametro.getChavePrimaria()), redeDiametro.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar os materiais
	 * de rede.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarRedeMaterial() throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		Collection<RedeMaterial> materiais = fachada.listarRedeMaterial();
		if (materiais != null && !materiais.isEmpty()) {
			for (RedeMaterial redeMaterial : materiais) {
				dados.put(String.valueOf(redeMaterial.getChavePrimaria()), redeMaterial.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por consultar os troncos
	 * a partir do CityGate informada.
	 *
	 * @param idCityGate a chave da Localização.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarTroncosPorCityGate(Long idCityGate) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa os troncos pela localização.
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_CITY_GATE, idCityGate);
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Tronco> listaTronco = fachada.consultarTronco(filtro);

		if (listaTronco != null && !listaTronco.isEmpty()) {
			for (Tronco tronco : listaTronco) {
				dados.put(String.valueOf(tronco.getChavePrimaria()), tronco.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar as rotas
	 * associadas a um setor comercial
	 * informado.
	 *
	 * @param chavePrimariaSetorComercial Chave primária de um setor
	 *            comercial.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarRotasGeracaoDadosLeituraPorSetorComercial(Long chavePrimariaSetorComercial) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<Rota> rotas = fachada.listarRotasGeracaoDadosLeituraPorSetorComercial(chavePrimariaSetorComercial);
		if (rotas != null && !rotas.isEmpty()) {
			for (Rota rota : rotas) {
				dados.put(String.valueOf(rota.getChavePrimaria()), rota.getNumeroRota());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as rotas para
	 * a geração de dados de leitura
	 * associadas a um Grupo Faturamento e uma
	 * Periodicidade.
	 *
	 * @param idGrupoFaturamento Chave primária do Grupo
	 *            Faturamento
	 * @param idPeriodicidade Chave primária da Periodicidade
	 * @param dataPrevistaFiltro the data prevista filtro
	 * @return Um mapa de String
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Object[] listarRotasGeracaoDadosLeitura(Long idGrupoFaturamento, Long idPeriodicidade, String dataPrevistaFiltro)
					throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();
		Map<String, String> dadosRotasSemCronograma = new TreeMap<String, String>();
		Map<String, String> dadosRotasCronogramaEmAtraso = new TreeMap<String, String>();
		Date dataPrevista = null;

		if (!StringUtils.isEmpty(dataPrevistaFiltro)) {
			try {
				dataPrevista =
								Util.converterCampoStringParaData("CRONOGRAMA_ROTA_DATA_PREVISTA", dataPrevistaFiltro,
												FORMATO_DATA_BR);
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getStackTrace(), e);
				dataPrevista = null;
			}
		}
		Collection<Rota> rotas = fachada.listarRotasGeracaoDadosLeitura(idGrupoFaturamento, idPeriodicidade, dataPrevista);
		Collection<Rota> rotasCronogramaEmAtraso = fachada.listarRotasGeracaoDadosLeituraCronogramaEmAtraso(rotas);
		Collection<Rota> rotasSemCronograma =
						fachada.listarRotasGeracaoDadosLeituraSemCronograma(idGrupoFaturamento, idPeriodicidade, dataPrevista);

		if (rotas != null && rotasSemCronograma != null) {
			rotas.removeAll(rotasSemCronograma);
		}
		if (rotas != null && rotasCronogramaEmAtraso != null) {
			rotas.removeAll(rotasCronogramaEmAtraso);
		}

		if (rotas != null && !rotas.isEmpty()) {
			for (Rota rota : rotas) {
				dados.put(rota.getNumeroRota(), String.valueOf(rota.getChavePrimaria()));
			}
		}

		if ((rotasSemCronograma != null) && (!rotasSemCronograma.isEmpty())) {
			for (Rota rotaSemCronograma : rotasSemCronograma) {
				dadosRotasSemCronograma.put(rotaSemCronograma.getNumeroRota(), String.valueOf(rotaSemCronograma.getChavePrimaria()));
			}
		}

		if ((rotasCronogramaEmAtraso != null) && (!rotasCronogramaEmAtraso.isEmpty())) {
			for (Rota rotaCronogramaEmAtraso : rotasCronogramaEmAtraso) {
				dadosRotasCronogramaEmAtraso.put(rotaCronogramaEmAtraso.getNumeroRota(),
								String.valueOf(rotaCronogramaEmAtraso.getChavePrimaria()));
			}
		}

		return new ArrayList<Object>(Arrays.asList(dados, dadosRotasSemCronograma, dadosRotasCronogramaEmAtraso)).toArray();
	}

	/**
	 * Método responsável por listar as rotas
	 * associadas a um grupo de
	 * faturamento.
	 *
	 * @param idGrupoFaturamento
	 *            Chave primária de um grupo de
	 *            faturamento.
	 * @return Um mapa de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarRotaPorGrupoFaturamento(Long idGrupoFaturamento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<Rota> rotas = fachada.listarRotasPorGrupoFaturamento(idGrupoFaturamento);
		if (rotas != null && !rotas.isEmpty()) {
			for (Rota rota : rotas) {
				dados.put(String.valueOf(rota.getChavePrimaria()), rota.getNumeroRota());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as ocorrências das medições.
	 *
	 * @param ocorrencia the ocorrencia
	 * @return Um mapa de String
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarSupervisorioMedicaoAnormalidade(String ocorrencia) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<SupervisorioMedicaoAnormalidade> medicaoAnormalidade = fachada.listarSupervisorioMedicaoAnormalidade(ocorrencia);
		if (medicaoAnormalidade != null && !medicaoAnormalidade.isEmpty()) {
			for (SupervisorioMedicaoAnormalidade anormalidade : medicaoAnormalidade) {
				dados.put(String.valueOf(anormalidade.getChavePrimaria()), anormalidade.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as rotas de
	 * acordo com a periodicidade.
	 *
	 * @param idRota atual Chave primária da rota que
	 *            está sendo alterada.
	 * @param idPeriodicidade the id periodicidade
	 * @return Um mapa de String
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarRotaPorPeriodicidade(Long idRota, Long idPeriodicidade) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<Rota> listaRotas = fachada.listarRotasRemanejamentoPeriodicidade(idRota, idPeriodicidade);

		if (listaRotas != null && !listaRotas.isEmpty()) {
			for (Rota rota : listaRotas) {
				dados.put(String.valueOf(rota.getChavePrimaria()), rota.getNumeroRota());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as rotas
	 * associadas a um grupo de
	 * faturamento.
	 *
	 * @param idGrupoFaturamento Chave primária de um grupo de
	 *            faturamento.
	 * @return Um mapa de String
	 * @throws GGASException the exception
	 */
	public Map<String, String> listarPontoConsumoPorGrupoFaturamento(Long idGrupoFaturamento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		GrupoFaturamento grupoFaturamento = fachada.obterGrupoFaturamento(idGrupoFaturamento);

		Collection<PontoConsumo> pontoConsumo = fachada.listarPontosConsumoNaoSequenciadas(grupoFaturamento);
		if (pontoConsumo != null && !pontoConsumo.isEmpty()) {
			for (PontoConsumo ponto : pontoConsumo) {
				dados.put(String.valueOf(ponto.getChavePrimaria()), descricaoFormatada(ponto));
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar as rotas
	 * associadas a um grupo de
	 * faturamento.
	 *
	 * @param idPeriodicidade the id periodicidade
	 * @param idTipoLeitura the id tipo leitura
	 * @return Um mapa de String
	 * @throws GGASException the exception
	 */
	public Map<String, String> listarPontoConsumo(Long idPeriodicidade, Long idTipoLeitura) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<PontoConsumo> collPontoConsumoTemp = fachada.listarPontosConsumoNaoSequenciadas(idPeriodicidade, idTipoLeitura);
		List<PontoConsumo> collPontoConsumo = Util.ordenarColecaoPorAtributo(collPontoConsumoTemp, "numeroSequenciaLeitura", true);

		if (collPontoConsumo != null && !collPontoConsumo.isEmpty()) {

			List<Object> lista = new ArrayList<Object>();

			for (PontoConsumo pontoConsumo : collPontoConsumo) {
				lista.add(new Object[] {
										pontoConsumo.getChavePrimaria(), this.descricaoFormatada(pontoConsumo)});
			}

			this.ordernarListaPontoConsumoPorDescricao(lista);

			for (Object obj : lista) {

				Object[] dadosPontoConsumo = (Object[]) obj;

				dados.put(String.valueOf(dadosPontoConsumo[CONSTANTE_NUMERO_ZERO]), String.valueOf(dadosPontoConsumo[1]));

			}

		}

		return dados;
	}

	/**
	 * Descricao formatada.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the string
	 * @throws GGASException the exception
	 */
	public String descricaoFormatada(PontoConsumo pontoConsumo) throws GGASException {

		StringBuilder descricaoFormatada = new StringBuilder();
		String separador = "";
		String nomeImovel = "";
		if (pontoConsumo.getImovel() != null && pontoConsumo.getImovel().getNome() != null) {
			nomeImovel = pontoConsumo.getImovel().getNome().trim();
		}
		if (nomeImovel.length() > 0) {
			descricaoFormatada.append(pontoConsumo.getDescricaoFormatada());
		} else {
			Cliente cliente = fachada.obterClientePrincipal(pontoConsumo.getChavePrimaria());
			if (cliente != null && !StringUtils.isEmpty(cliente.getNome())) {
				descricaoFormatada.append(cliente.getNome());
				separador = " - ";
			}
			if (!StringUtils.isEmpty(pontoConsumo.getDescricao())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getDescricao());
				separador = " - ";
			}

			Cep cepEndereco = pontoConsumo.getCep();
			if (cepEndereco == null) {
				cepEndereco = pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep();
			}
			if (!StringUtils.isEmpty(cepEndereco.getTipoLogradouro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getTipoLogradouro());
				if (StringUtils.isEmpty(cepEndereco.getLogradouro())) {
					separador = ", ";
				} else {
					separador = " ";
				}
			}
			if (!StringUtils.isEmpty(cepEndereco.getLogradouro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getLogradouro());
				separador = ", ";
			}

			if (!StringUtils.isEmpty(pontoConsumo.getNumeroImovel())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getNumeroImovel());
				separador = ", ";
			} else {
				descricaoFormatada.append(separador).append(pontoConsumo.getImovel().getNumeroImovel());
				separador = ", ";
			}
			if (!StringUtils.isEmpty(pontoConsumo.getDescricaoComplemento())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getDescricaoComplemento());
				separador = ", ";
			}
			if (!StringUtils.isEmpty(cepEndereco.getBairro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getBairro());
			}
		}

		return descricaoFormatada.toString().toUpperCase();

	}

	/**
	 * Ordernar lista ponto consumo por descricao.
	 *
	 * @param lista the lista
	 */
	private void ordernarListaPontoConsumoPorDescricao(List<Object> lista) {

		Collections.sort(lista, (o1, o2) -> {

			String descricaoPontoConsumo1 = (String) ((Object[]) o1)[1];
			String descricaoPontoConsumo2 = (String) ((Object[]) o2)[1];
			return descricaoPontoConsumo1.compareTo(descricaoPontoConsumo2);
		});

	}

	/**
	 * Método responsável por listar os tipos de
	 * convênio de acordo com o tipo do
	 * arrecadador (banco
	 * ou cliente).
	 *
	 * @param idArrecadador
	 *            Chave Primária do Arrecadador.
	 * @return Um mapa de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarTiposConvenioPorArrecadador(Long idArrecadador) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();
		if (idArrecadador != null && idArrecadador > CONSTANTE_NUMERO_ZERO) {
			Collection<ArrecadadorContratoConvenio> listaArrecadadorContratoConvenio =
							fachada.consultarACConvenioPorArrecadador(idArrecadador, null);
			if (listaArrecadadorContratoConvenio != null && !listaArrecadadorContratoConvenio.isEmpty()) {
				Collection<EntidadeConteudo> listaFormaArrecadacao = new ArrayList<EntidadeConteudo>();
				for (ArrecadadorContratoConvenio contratoConvenio : listaArrecadadorContratoConvenio) {
					listaFormaArrecadacao.add(contratoConvenio.getTipoConvenio());
				}
				if (!listaFormaArrecadacao.isEmpty()) {
					for (EntidadeConteudo formaArrecadacao : listaFormaArrecadacao) {
						dados.put(String.valueOf(formaArrecadacao.getChavePrimaria()), formaArrecadacao.getDescricao());
					}
				}
			}
		}
		return dados;
	}

	/**
	 * Recupera o funcionário com a chave primária
	 * informada.
	 *
	 * @param idFuncionario Chave primária do funcionario a
	 *            ser obtido.
	 * @return Um funcionario.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterFuncionarioPorChave(Long idFuncionario) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idFuncionario == null) {
			dados = null;
		} else {
			Funcionario funcionario = fachada.buscarFuncionarioPorChave(idFuncionario);

			if (funcionario != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(funcionario.getChavePrimaria()).toString());
				dados.put("nome", funcionario.getNome());
				dados.put("matricula", funcionario.getMatricula());
				dados.put("nomeEmpresa", funcionario.getEmpresa().getCliente().getNome());
				if (funcionario.getEmail() != null) {
					dados.put("email", funcionario.getEmail());
				} else {
					dados.put("email", "");
				}
			}

		}
		return dados;

	}

	/**
	 * Recupera a atividade econômica com a chave
	 * primária informada.
	 *
	 * @param idAtividadeEconomica Chave primária da atividade
	 *            econômica a ser obtida.
	 * @return Uma atividade econômica.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterAtividadeEconomicaPorChave(Long idAtividadeEconomica) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idAtividadeEconomica == null) {
			dados = null;
		} else {
			AtividadeEconomica atividadeEconomica = fachada.obterAtividadeEconomica(idAtividadeEconomica);

			if (atividadeEconomica != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(atividadeEconomica.getChavePrimaria()).toString());
				dados.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, atividadeEconomica.getDescricao());

			}
		}

		return dados;
	}

	/**
	 * Lista os anos disponíveis para manutenção
	 * de dados de compra de city
	 * gate.
	 *
	 * @param idCityGate Chave primária do city gate.
	 * @return Coleção de inteiros que representam
	 *         os anos.
	 * @throws GGASException the GGAS exception
	 */
	public Map<Integer, Integer> listarAnosDisponiveisCityGateMedicao(Long idCityGate) throws GGASException {

		Map<Integer, Integer> dados = new TreeMap<Integer, Integer>(COMPARATOR_ANO_DESCENDENTE);

		if (idCityGate != null && idCityGate > CONSTANTE_NUMERO_ZERO) {
			CityGate cityGate = fachada.buscarCityGatePorChave(idCityGate);
			Collection<Integer> listaAnosDisponiveis = fachada.listarAnosDisponiveisCityGateMedicao(cityGate);

			if (listaAnosDisponiveis != null) {
				for (Integer ano : listaAnosDisponiveis) {
					dados.put(ano, ano);
				}
			}
		}

		return dados;
	}

	/**
	 * Obter lista veiculos por transportadora.
	 *
	 * @param idTransportadora the id transportadora
	 * @return the map
	 */
	public Map<String, String> obterListaVeiculosPorTransportadora(Long idTransportadora) {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		ControladorTransportadora controladorTransportadora = ServiceLocator.getInstancia().getControladorTransportadora();
		Collection<Veiculo> listaVeiculos = controladorTransportadora.consultarVeiculosPorTransportadora(idTransportadora);

		for (Veiculo veiculo : listaVeiculos) {
			retorno.put(String.valueOf(veiculo.getChavePrimaria()), veiculo.getPlaca());
		}

		return retorno;

	}

	/**
	 * Obter lista motoristas por transportadora.
	 *
	 * @param idTransportadora the id transportadora
	 * @return the map
	 */
	public Map<String, String> obterListaMotoristasPorTransportadora(Long idTransportadora) {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		ControladorTransportadora controladorTransportadora = ServiceLocator.getInstancia().getControladorTransportadora();
		Collection<Motorista> listaMotoristas = controladorTransportadora.consultarMotoristasPorTransportadora(idTransportadora);

		for (Motorista motorista : listaMotoristas) {
			retorno.put(String.valueOf(motorista.getChavePrimaria()), motorista.getNome());
		}

		return retorno;

	}

	/**
	 * Lista as áreas de abrangência de acordo com
	 * o tipo de abrangência
	 * informado. gate.
	 *
	 * @param codigoTipoAbrangencia Código respondente ao tipo de
	 *            abrangência.
	 * @return Coleção de áreas de abrangência.
	 * @throws GGASException the GGAS exception
	 */
	public List<Map<String, Object>> listarAreasAbrangenciaTemperaturaPressaoMedicao(String codigoTipoAbrangencia) throws GGASException {

		List<Map<String, Object>> dados = new ArrayList<Map<String, Object>>();
		Map<String, Object> abrangencia = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(codigoTipoAbrangencia)) {

			if (TemperaturaPressaoMedicaoVO.CODIGO_UNIDADE_FEDERACAO.equals(codigoTipoAbrangencia)) {
				Collection<UnidadeFederacao> listarAreaAbrangencia = fachada.listarTodasUnidadeFederacao();
				if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
					for (UnidadeFederacao entidade : listarAreaAbrangencia) {
						abrangencia = new HashMap<String, Object>();
						abrangencia.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, entidade.getChavePrimaria());
						abrangencia.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, entidade.getDescricao());
						dados.add(abrangencia);
					}
				}
			} else if (TemperaturaPressaoMedicaoVO.CODIGO_REGIAO.equals(codigoTipoAbrangencia)) {
				Collection<Regiao> listarAreaAbrangencia = fachada.listarRegioes();
				if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
					for (Regiao entidade : listarAreaAbrangencia) {
						abrangencia = new HashMap<String, Object>();
						abrangencia.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, entidade.getChavePrimaria());
						abrangencia.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, entidade.getDescricao());
						dados.add(abrangencia);
					}
				}
			} else if (TemperaturaPressaoMedicaoVO.CODIGO_MUNICIPIO.equals(codigoTipoAbrangencia)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				String idUnidadeFederacao =
								(String) fachada.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_UNIDADE_FEDERACAO_IMPLANTACAO);
				filtro.put("idUnidadeFederacao", Long.valueOf(idUnidadeFederacao));

				Collection<Municipio> listarAreaAbrangencia = fachada.consultarMunicipios(filtro);
				if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
					for (Municipio entidade : listarAreaAbrangencia) {
						abrangencia = new HashMap<String, Object>();
						abrangencia.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, entidade.getChavePrimaria());
						abrangencia.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, entidade.getDescricao());
						dados.add(abrangencia);
					}
				}
			}

		}

		return dados;
	}

	/**
	 * Recupera o histórico de medição com a chave
	 * primária informada.
	 *
	 * @param idHistoricoMedicao Chave primária do cliente a ser
	 *            obtido.
	 * @return Um histórico de medição.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterHistoricoMedicaoPorChave(Long idHistoricoMedicao) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idHistoricoMedicao == null) {
			dados = null;
		} else {
			HistoricoMedicao historicoMedicao =
							fachada.buscarHistoricoMedicaoPorChave(idHistoricoMedicao, "leiturista", "anormalidadeLeituraInformada",
											"anormalidadeLeituraFaturada", "anormalidadeLeituraAnterior", "situacaoLeitura",
											"historicoInstalacaoMedidor.medidor");

			if (historicoMedicao != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(historicoMedicao.getChavePrimaria()).toString());
				dados.put("anoMesLeituraFormatado", historicoMedicao.getAnoMesLeituraFormatado());
				if (historicoMedicao.getNumeroLeituraFaturada() != null) {
					dados.put("numeroLeituraFaturada", Util.formatarValorNumerico(historicoMedicao.getNumeroLeituraFaturada()));
				}
				if (historicoMedicao.getDataLeituraFaturada() != null) {
					dados.put("dataLeituraFaturada", Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraFaturada(),
									FORMATO_DATA_BR));
				}
				if (historicoMedicao.getAnormalidadeLeituraFaturada() != null) {
					dados.put("descricaoAnormalidadeLeituraFaturada", historicoMedicao.getAnormalidadeLeituraFaturada().getDescricao());
				}
				if (historicoMedicao.getNumeroLeituraInformada() != null) {
					dados.put("numeroLeituraInformada", Util.formatarValorNumerico(historicoMedicao.getNumeroLeituraInformada()).replace(",", "."));
				}
				if (historicoMedicao.getDataLeituraInformada() != null) {
					dados.put("dataLeituraInformada", Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraInformada(),
									FORMATO_DATA_BR));
				}
				if (historicoMedicao.getAnormalidadeLeituraInformada() != null) {
					dados.put("idAnormalidadeLeituraInformada",
									Long.toString(historicoMedicao.getAnormalidadeLeituraInformada().getChavePrimaria()));
				}
				if (historicoMedicao.getNumeroLeituraAnterior() != null) {
					dados.put("numeroLeituraAnterior", Util.formatarValorNumerico(historicoMedicao.getNumeroLeituraAnterior()).replace(",", "."));
				}
				if (historicoMedicao.getDataLeituraAnterior() != null) {
					dados.put("dataLeituraAnterior", Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraAnterior(),
									FORMATO_DATA_BR));
				}
				if (historicoMedicao.getAnormalidadeLeituraAnterior() != null) {
					dados.put("idAnormalidadeLeituraAnterior",
									Long.toString(historicoMedicao.getAnormalidadeLeituraAnterior().getChavePrimaria()));
				}
				if (historicoMedicao.getLeiturista() != null) {
					dados.put("idLeiturista", Long.toString(historicoMedicao.getLeiturista().getChavePrimaria()));
				}
				if (historicoMedicao.getConsumoInformado() != null) {
					dados.put("consumoInformado", Util.formatarValorNumerico(historicoMedicao.getConsumoInformado()));
				}
				if (historicoMedicao.getSituacaoLeitura() != null) {
					dados.put("descricaoSituacao", historicoMedicao.getSituacaoLeitura().getDescricao());
				}
				if (historicoMedicao.getHistoricoInstalacaoMedidor() != null) {
					dados.put("numeroMedidor", historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor().getNumeroSerie());
				}

				if (historicoMedicao.getHistoricoInstalacaoMedidor() != null
								&& historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() != null) {
					dados.put("vazaoCorretor", historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor().toString());
				}
			}
		}

		return dados;
	}



	/**
	 * Recupera o Imóvel com a chave primária
	 * informada.
	 *
	 * @param idImovel Chave primária do imovel a ser
	 *            obtido.
	 * @return Um imovel.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterImovelPorChave(Long idImovel) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idImovel == null) {
			dados = null;
		} else {
			Imovel imovel = fachada.buscarImovelPorChave(idImovel);

			if (imovel != null) {
				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(imovel.getChavePrimaria()));
				dados.put("matriculaImovel", String.valueOf(imovel.getChavePrimaria()));

				dados.put(NOME_FANTASIA, "");
				if (imovel.getNome() != null) {
					dados.put(NOME_FANTASIA, imovel.getNome());
				}

				dados.put("numeroImovel", "");
				if (imovel.getNumeroImovel() != null) {
					dados.put("numeroImovel", imovel.getNumeroImovel());
				}

				dados.put("cidadeImovel", "");
				if (imovel.getQuadraFace().getEndereco().getCep().getNomeMunicipio() != null) {
					dados.put("cidadeImovel", imovel.getQuadraFace().getEndereco().getCep().getNomeMunicipio());
				}

				dados.put("indicadorCondominioAmbos", String.valueOf(imovel.getCondominio()));
				dados.put("indicadorUso", String.valueOf(imovel.isHabilitado()));
				if (imovel.getCep() != null) {
					dados.put("cepImovel", imovel.getCep());
					} else {
					dados.put("cepImovel", imovel.getQuadraFace().getEndereco().getCep().getCep());
				}
				dados.put("enderecoImovel", String.valueOf(imovel.getEnderecoFormatado()));
				if (imovel.getCondominio() != null && imovel.getCondominio()) {
					if (imovel.getModalidadeMedicaoImovel() != null) {
						dados.put("codigoModalidadeMedicao", String.valueOf(imovel.getModalidadeMedicaoImovel().getCodigo()));
					}
					if ((imovel.getQuantidadeBloco() != null && imovel.getQuantidadeBloco() > CONSTANTE_NUMERO_ZERO)
									&& (imovel.getQuantidadeAndar() != null && imovel.getQuantidadeBloco() > CONSTANTE_NUMERO_ZERO)) {
						int blocos = imovel.getQuantidadeBloco();
						int andares = imovel.getQuantidadeAndar();
						int unidadesAndar = imovel.getQuantidadeApartamentoAndar();
						int total = blocos * andares * unidadesAndar;

						dados.put("numeroAptoLojas", String.valueOf(total));
					}

				}
				preencheDadosQuadraImovel(dados, imovel);
				if (imovel.getSituacaoImovel()!=null) {
					dados.put("idSituacao", String.valueOf(imovel.getSituacaoImovel().getChavePrimaria()));
				}
			}
		}
		return dados;
	}

	/**
	 * Metodo para preencher os dados da quadra imovel
	 * @param dados Mapa com os filtros a ser usados
	 * @param imovel Imovel corrente
	 */
	private void preencheDadosQuadraImovel(Map<String, String> dados, Imovel imovel) {
		try {
			dados.put("idQuadraFace", String.valueOf(imovel.getQuadraFace().getChavePrimaria()));
			dados.put("idQuadraImovel", String.valueOf(imovel.getQuadraFace().getQuadra().getChavePrimaria()));
			dados.put("ïdCepImovel", String.valueOf(imovel.getQuadraFace().getEndereco().getCep().getChavePrimaria()));
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Método responsável por verificar a concessão de crédito.
	 *
	 * @param idHistoricoMedicao the id historico medicao
	 * @param leituraAnteriorModificado the leitura anterior modificado
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> verificarConcessaoCredito(Long idHistoricoMedicao, String leituraAnteriorModificado) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		BigDecimal valorLeituraAnteriorModificado =
						Util.converterCampoStringParaValorBigDecimal(HistoricoMedicao.NUMERO_LEITURA_ANTERIOR, leituraAnteriorModificado,
										Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

		BigDecimal[] valores = fachada.verificarConcessaoCredito(idHistoricoMedicao, valorLeituraAnteriorModificado);

		dados.put("houveCredito", Boolean.valueOf(valores[1] != null).toString());
		if (valores[CONSTANTE_NUMERO_ZERO] != null) {
			dados.put("diferenca",
							Util.converterCampoValorParaString(valores[CONSTANTE_NUMERO_ZERO], Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			dados.put("diferenca", StringUtils.EMPTY);
		}
		if (valores[1] != null) {
			dados.put("credito", Util.converterCampoValorParaString(valores[CONSTANTE_NUMERO_UM],
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			dados.put("credito", StringUtils.EMPTY);
		}

		return dados;
	}

	/**
	 * Método responsável por listar as versões
	 * aprovadas de uma proposta a partir do
	 * número
	 * informado.
	 *
	 * @param numeroProposta O número da proposta.
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarVersoesAprovadasPropostaPorNumero(String numeroProposta) throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		if (!StringUtils.isEmpty(numeroProposta)) {
			Collection<Proposta> propostas = fachada.listarPropostasAprovadasPorNumero(numeroProposta);

			if (!propostas.isEmpty()) {
				for (Proposta proposta : propostas) {
					dados.put(Long.toString(proposta.getChavePrimaria()), Integer.toString(proposta.getVersaoProposta()));
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar uma
	 * proposta pela chave primária.
	 *
	 * @param chavePrimaria A chave primária da proposta.
	 * @return Um mapa com as informações da
	 *         proposta.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterPropostaPorChave(String chavePrimaria) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (!StringUtils.isEmpty(chavePrimaria)) {

			Proposta proposta =
							fachada.buscarPropostaPorChave(Util.converterCampoStringParaValorLong("Número proposta", chavePrimaria),
											"tarifa");

			dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, Long.toString(proposta.getChavePrimaria()));
			dados.put("indicadorParticipacaoCliente", Boolean.toString(proposta.isIndicadorParticipacao()));
			if (proposta.getValorGastoMensal() != null) {
				dados.put("valorGastoMensal", Util.converterCampoValorDecimalParaString("economiaAnualGN", proposta.getValorGastoMensal(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}
			if (proposta.getNumeroCompletoProposta() != null) {
				dados.put("numeroCompletoProposta", proposta.getNumeroCompletoProposta());
			}
			if (proposta.getEconomiaMensalGN() != null) {
				dados.put("economiaMensalGN", Util.converterCampoValorDecimalParaString("economiaAnualGN", proposta.getEconomiaMensalGN(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}
			if (proposta.getEconomiaAnualGN() != null) {
				dados.put("economiaAnualGN", Util.converterCampoValorDecimalParaString("economiaAnualGN", proposta.getEconomiaAnualGN(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}
			if (proposta.getPercentualEconomia() != null) {
				dados.put("percentualEconomia", Util.converterCampoPercentualParaString("percentualEconomia", proposta
								.getPercentualEconomia().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorCliente() != null) {
				dados.put("valorCliente", Util.converterCampoValorDecimalParaString("valorCliente", proposta.getValorCliente(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}
			if (proposta.getQuantidadeParcela() != null) {
				dados.put("quantidadeParcelas", proposta.getQuantidadeParcela().toString());
			}
			if (proposta.getPercentualJuros() != null) {
				dados.put("percentualJuros",
								Util.converterCampoPercentualParaString("percentualJuros",
												proposta.getPercentualJuros().multiply(BigDecimal.valueOf(CEM)), Constantes.LOCALE_PADRAO));
			}
			if (proposta.getValorInvestimento() != null) {
				dados.put("valorInvestimento", Util.converterCampoValorDecimalParaString("valorInsvestimento",
								proposta.getValorInvestimento(), Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}
			if (proposta.getDataEmissao() != null) {
				dados.put("dataInvestimento", Util.converterDataParaString(proposta.getDataEmissao()));
			}
			if (proposta.getImovel() != null) {
				dados.put("idImovel", String.valueOf(proposta.getImovel().getChavePrimaria()));
			}
			if (proposta.getCliente() != null) {
				dados.put("idCliente", String.valueOf(proposta.getCliente().getChavePrimaria()));
			}

		}

		return dados;
	}

	/**
	 * Obter medidor por numero serie.
	 *
	 * @param numeroSerie the numero serie
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterMedidorPorNumeroSerie(String numeroSerie) throws GGASException {

		List<Medidor> medidores = fachada.obterMedidorPorNumeroSerie(numeroSerie);

		Map<String, Object> dados = null;

		if (medidores != null && !medidores.isEmpty()) {
			int count = 1;

			dados = new HashMap<String, Object>();

			for (Medidor medidor : medidores) {

				dados.put(String.valueOf(count), this.popularDadosMedidor(medidor));
				count++;

			}

		}

		return dados;

	}

	/**
	 * Popular dados medidor.
	 *
	 * @param medidor the medidor
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	private Map<String, String> popularDadosMedidor(Medidor medidor) throws GGASException {

		long codigoMedidorDisponivel =
						Long.parseLong(Fachada.getInstancia().obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL));

		Map<String, String> dados = new HashMap<String, String>();

		dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(medidor.getChavePrimaria()));
		if (medidor.getTipoMedidor() != null) {
			dados.put("tipo", medidor.getTipoMedidor().getDescricao());
		} else {
			dados.put("tipo", "");
		}
		if (medidor.getModelo() != null) {
			dados.put("modelo", medidor.getModelo().getDescricao());
		} else {
			dados.put("modelo", "");
		}
		if (medidor.getMarcaMedidor() != null) {
			dados.put("marca", medidor.getMarcaMedidor().getDescricao());
		} else {
			dados.put("marca", "");
		}
		if (medidor.getDigito() != null) {
			dados.put("numeroDigitos", String.valueOf(medidor.getDigito()));
		} else {
			dados.put("numeroDigitos", "0");
		}

		String chaveModoUsoIndependente = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE);
		String chaveModoUsoVirtual = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL);
		if (medidor.getModoUso() != null) {
			if (chaveModoUsoIndependente.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
				dados.put("modoUso", "Independente");
			} else if (chaveModoUsoVirtual.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
				dados.put("modoUso", "Virtual");
			} else {
				dados.put("modoUso", "Normal");
			}
		} else {
			dados.put("modoUso", "Normal");
		}

		if (medidor.getSituacaoMedidor().getChavePrimaria() == codigoMedidorDisponivel) {
			dados.put("disponivel", Boolean.TRUE.toString());
		} else {
			dados.put("disponivel", Boolean.FALSE.toString());
		}

		return dados;
	}

	/**
	 * Obter corretor vazao por numero serie.
	 *
	 * @param numeroSerie the numero serie
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterCorretorVazaoPorNumeroSerie(String numeroSerie) throws GGASException {

		long codigoMedidorDisponivel =
						Long.parseLong(Fachada.getInstancia().obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL));

		List<VazaoCorretor> vazaoCorretorList = fachada.obterVazaoCorretorPorNumeroSerie(numeroSerie);

		Map<String, Object> dados = null;

		if (vazaoCorretorList != null && !vazaoCorretorList.isEmpty()) {
			int count = 1;

			dados = new HashMap<String, Object>();
			for (VazaoCorretor vazaoCorretor : vazaoCorretorList) {

				dados.put(String.valueOf(count), this.popularVazaoCorretor(codigoMedidorDisponivel, vazaoCorretor));
				count++;

			}
		}

		return dados;

	}

	/**
	 * Popular vazao corretor.
	 *
	 * @param codigoMedidorDisponivel the codigo medidor disponivel
	 * @param vazaoCorretor the vazao corretor
	 * @return the map
	 */
	private Map<String, String> popularVazaoCorretor(long codigoMedidorDisponivel, VazaoCorretor vazaoCorretor) {

		Map<String, String> dados;
		dados = new HashMap<String, String>();

		dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(vazaoCorretor.getChavePrimaria()));
		if (vazaoCorretor.getMarcaCorretor() != null) {
			dados.put("modelo", vazaoCorretor.getMarcaCorretor().getDescricao());
		} else {
			dados.put("modelo", "");
		}
		if (vazaoCorretor.getMarcaCorretor() != null) {
			dados.put("marca", vazaoCorretor.getMarcaCorretor().getDescricao());
		} else {
			dados.put("marca", "");
		}
		if (vazaoCorretor.getSituacaoMedidor().getChavePrimaria() == codigoMedidorDisponivel) {
			dados.put("disponivel", Boolean.TRUE.toString());
		} else {
			dados.put("disponivel", Boolean.FALSE.toString());
		}

		if (vazaoCorretor.getNumeroDigitos() != null) {
			dados.put("numeroDigitos", String.valueOf(vazaoCorretor.getNumeroDigitos()));
		} else {
			dados.put("numeroDigitos", "0");
		}

		return dados;
	}

	/**
	 * Método responsável por adicionar um
	 * quantidade de dias a uma data informada.
	 *
	 * @param dataTexto String data no formato
	 *            dd/MM/yyyy.
	 * @param numeroDias String número de dias
	 * @return String com a nova data no formato
	 *         dd/MM/yyyy.
	 * @throws GGASException the GGAS exception
	 */
	public String adicionarDiasData(String dataTexto, String numeroDias) throws GGASException {

		DateTime date = new DateTime(Util.converterCampoStringParaData("Data", dataTexto, FORMATO_DATA_BR));
		Integer quantidadeDias = Util.converterCampoStringParaValorInteger(numeroDias);
		date = date.plusDays(quantidadeDias);

		return Util.converterDataParaStringSemHora(date.toDate(), FORMATO_DATA_BR);
	}

	/**
	 * Método repsonsável por validar se é
	 * possível informar uma fase de vencimento a
	 * partir uma
	 * opção de vencimento
	 * informada.
	 *
	 * @param chaveOpcaoVencimento A chave primária da opção de
	 *            vencimento.
	 * @return true, permita
	 * @throws GGASException the GGAS exception
	 */
	public boolean permiteFaseReferencialVencimentoPorOpcaoVencimento(Long chaveOpcaoVencimento) throws GGASException {

		if(chaveOpcaoVencimento != null) {
			return fachada.permiteFaseReferencialVencimentoPorOpcaoVencimento(chaveOpcaoVencimento);
		}
		
		return false;
	}

	/**
	 * Método responsável por consultar as faces
	 * de quadra pela quadra.
	 *
	 * @param chaveQuadra the chave quadra
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarQuadraPorFaceQuadra(String chaveQuadra) throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		if (!StringUtils.isEmpty(chaveQuadra)) {
			Quadra quadra = fachada.buscarQuadraPorChave(Long.valueOf(chaveQuadra));
			Collection<QuadraFace> faces = quadra.getQuadrasFace();
			if (faces != null && !faces.isEmpty()) {
				for (QuadraFace face : faces) {
					dados.put(String.valueOf(face.getChavePrimaria()), face.getQuadraFaceFormatada());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por obter a quantidade
	 * de propostas aprovadas.
	 *
	 * @param numeroProposta
	 *            Número da proposta.
	 * @return Quantidade de propostas aprovadas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Integer obterQtdPropostasAprovadas(String numeroProposta) throws GGASException {

		Integer qtdProposta = null;
		if (!StringUtils.isEmpty(numeroProposta)) {
			qtdProposta = fachada.obterQtdPropostasAprovadasPorNumero(numeroProposta);
		}

		return qtdProposta;
	}

	/**
	 * Método responsável por verificar se uma
	 * data em formato texto é válida.
	 *
	 * @param strData
	 *            Data no formato texto.
	 * @return true caso válida
	 */
	public boolean isDataValida(String strData) {

		boolean retorno = true;
		try {
			Util.converterCampoStringParaData("Data", strData, FORMATO_DATA_BR);
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			retorno = false;
		}

		return retorno;
	}

	/**
	 * Metodo utilizado para obter a data no servidor
	 * evita a tulizacao do new Date() no JavaScript
	 * pois ete procedimento obtem a data do usuario
	 * e pode acarretar erros.
	 *
	 * @return a data atual no servidor
	 */
	public String getDataHoje() {

		return DataUtil.converterDataParaString(new Date());
	}

	/**
	 * Método responsável por obter uma quadra a
	 * partir da chave primaria da quadraFace
	 * informada.
	 *
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Um Mapa de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> consultarQuadraPorQuadraFace(Long idQuadraFace) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Quadra quadra = fachada.obterQuadraPorQuadraFace(idQuadraFace);
		if (quadra != null) {
			dados.put(String.valueOf(quadra.getNumeroQuadra()), String.valueOf(quadra.getChavePrimaria()));
		}
		return dados;
	}

	/**
	 * Método responsável por obter um
	 * PontoConsumoCityGate a partir da chave
	 * primaria do Ponto
	 * Consumo informada.
	 *
	 * @param idPontoConsumo
	 *            A chave primária do Ponto
	 *            Consumo
	 * @return Uma Lista de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public List<String[]> obterPontoConsumoCityGate(Long idPontoConsumo) throws GGASException {

		List<String[]> dados = new ArrayList<String[]>();
		Collection<PontoConsumoCityGate> listaPontoConsumoCityGate = fachada.obterPontoConsumoCityGate(idPontoConsumo);
		if (listaPontoConsumoCityGate != null && !listaPontoConsumoCityGate.isEmpty()) {
			for (PontoConsumoCityGate pontoConsumoCityGate : listaPontoConsumoCityGate) {
				dados.add(new String[] {
										pontoConsumoCityGate.getCityGate().getDescricao(),
										String.valueOf(pontoConsumoCityGate.getPercentualParticipacao()),
										DataUtil.converterDataParaString(pontoConsumoCityGate.getDataVigencia())});
			}
		}
		return dados;
	}

	/**
	 * Obter ponto consumo tributo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String[]> obterPontoConsumoTributo(Long idPontoConsumo) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idPontoConsumo", idPontoConsumo);
		filtro.put(HABILITADO, Boolean.TRUE);

		Map<String, String[]> dados = new HashMap<String, String[]>();

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota = fachada.consultarPontosConsumoTributoAliquota(filtro);
		if (listaPontoConsumoTributoAliquota != null && !listaPontoConsumoTributoAliquota.isEmpty()) {
			for (PontoConsumoTributoAliquota pontoConsumoTributoAliquota : listaPontoConsumoTributoAliquota) {
				String dataInicioVigencia = "";
				String dataFimVigencia = "";

				if (pontoConsumoTributoAliquota.getDataInicioVigencia() != null) {
					dataInicioVigencia =
									Util.converterDataParaStringSemHora(pontoConsumoTributoAliquota.getDataInicioVigencia(),
													FORMATO_DATA_BR);
				}

				if (pontoConsumoTributoAliquota.getDataFimVigencia() != null) {
					dataFimVigencia =
									Util.converterDataParaStringSemHora(pontoConsumoTributoAliquota.getDataFimVigencia(),
													FORMATO_DATA_BR);
				}

				dados.put(String.valueOf(pontoConsumoTributoAliquota.getChavePrimaria()),
								new String[] {
												String.valueOf(pontoConsumoTributoAliquota.getTributo().getDescricao()),
												String.valueOf(pontoConsumoTributoAliquota.getIndicadorIsencao()),
												String.valueOf(pontoConsumoTributoAliquota.getPorcentagemAliquota()), dataInicioVigencia,
												dataFimVigencia});
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar as faixas de
	 * pressão por segmento.
	 *
	 * @param chaveSegmento A chave primária do segmento.
	 * @return the map
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarFaixasPressaoFornecimentoPorSegmento(Long chaveSegmento) throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		if (chaveSegmento != null && chaveSegmento > CONSTANTE_NUMERO_ZERO) {
			Segmento segmento = fachada.buscarSegmentoChave(chaveSegmento);
			Collection<FaixaPressaoFornecimento> faixas = fachada.listarFaixaPressaoFornecimentoPorSegmento(segmento);

			if (!faixas.isEmpty()) {
				for (FaixaPressaoFornecimento faixa : faixas) {
					dados.put(String.valueOf(faixa.getChavePrimaria()), faixa.getDescricaoFormatada());
				}
			}
		}
		return dados;
	}

	/**
	 * Obter fator correcao ptzpc spor faixa pressao.
	 *
	 * @param chaveFaixaPressaoFornecimento the chave faixa pressao fornecimento
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String obterFatorCorrecaoPTZPCSporFaixaPressao(Long chaveFaixaPressaoFornecimento) throws GGASException {

		String fatorCorrecao = null;

		if (chaveFaixaPressaoFornecimento != null && chaveFaixaPressaoFornecimento > CONSTANTE_NUMERO_ZERO) {
			FaixaPressaoFornecimento faixaPressaoFornecimento =
							fachada.obterFaixaPressaoFornecimentoPorChave(chaveFaixaPressaoFornecimento);
			if (faixaPressaoFornecimento != null) {
				if (faixaPressaoFornecimento.getNumeroFatorCorrecaoPTZPCS() == null) {
					fatorCorrecao =
									Util.converterCampoValorDecimalParaString("fatorCorrecao", new BigDecimal("0"),
													Constantes.LOCALE_PADRAO, Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
				} else {
					fatorCorrecao =
									Util.converterCampoValorDecimalParaString("fatorCorrecao",
													faixaPressaoFornecimento.getNumeroFatorCorrecaoPTZPCS(), Constantes.LOCALE_PADRAO,
													Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL);
				}

			}
		}

		return fatorCorrecao;
	}

	/**
	 * Método responsável por listar as
	 * amostragens de PCS por ramo de atividade.
	 *
	 * @param chaveRamoAtividade A chave primária do ramo de
	 *            atividade.
	 * @return the map
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarAmostragensPCSPorRamoAtividade(Long chaveRamoAtividade) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chaveRamoAtividade != null && chaveRamoAtividade > 0) {
			RamoAtividade ramoAtividade = fachada.buscarRamoAtividadeChave(chaveRamoAtividade);
			Collection<EntidadeConteudo> amostragens = fachada.listarAmostragensPCSPorRamoAtividade(ramoAtividade);

			if (!amostragens.isEmpty()) {
				for (EntidadeConteudo amostragem : amostragens) {
					dados.put(String.valueOf(amostragem.getChavePrimaria()), amostragem.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar as
	 * amostragens de PCS por segmento.
	 *
	 * @param chaveSegmento A chave primária do segmento.
	 * @return the map
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarAmostragensPCSPorSegmento(Long chaveSegmento) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chaveSegmento != null && chaveSegmento > 0) {
			Segmento segmento = fachada.buscarSegmentoChave(chaveSegmento);
			Collection<EntidadeConteudo> amostragens = fachada.listarAmostragensPCSPorSegmento(segmento);

			if (!amostragens.isEmpty()) {
				for (EntidadeConteudo amostragem : amostragens) {
					dados.put(String.valueOf(amostragem.getChavePrimaria()), amostragem.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar as intervalos
	 * de PCS por ramo de atividade.
	 *
	 * @param chaveRamoAtividade A chave primária do ramo de
	 *            atividade.
	 * @return the map
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarIntervalosPCSPorRamoAtividade(Long chaveRamoAtividade) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chaveRamoAtividade != null && chaveRamoAtividade > CONSTANTE_NUMERO_ZERO) {
			RamoAtividade ramoAtividade = fachada.buscarRamoAtividadeChave(chaveRamoAtividade);
			Collection<IntervaloPCS> intervalos = fachada.listarIntervalosPCSPorRamoAtividade(ramoAtividade);

			if (!intervalos.isEmpty()) {
				for (IntervaloPCS intervalo : intervalos) {
					dados.put(String.valueOf(intervalo.getChavePrimaria()), intervalo.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar as intervalos
	 * de PCS por segmento.
	 *
	 * @param chaveSegmento A chave primária do segmento.
	 * @return the map
	 * @throws GGASException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarIntervalosPCSPorSegmento(Long chaveSegmento) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chaveSegmento != null && chaveSegmento > 0) {
			Segmento segmento = fachada.buscarSegmentoChave(chaveSegmento);
			Collection<IntervaloPCS> intervalos = fachada.listarIntervalosPCSPorSegmento(segmento);

			if (!intervalos.isEmpty()) {
				for (IntervaloPCS intervalo : intervalos) {
					dados.put(String.valueOf(intervalo.getChavePrimaria()), intervalo.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Recupera o Ponto Consumo com a chave
	 * primária informada.
	 *
	 * @param idImovel the id imovel
	 * @return Um ponto consumo.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterPontoConsumoPorChave(Long idImovel) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idImovel == null) {
			dados = null;
		} else {
			PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(idImovel, "cep", "imovel", "quadraFace");

			dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(pontoConsumo.getChavePrimaria()));
			dados.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, String.valueOf(pontoConsumo.getDescricao()));
			dados.put("endereco", pontoConsumo.getEnderecoFormatado());
			dados.put("cep", pontoConsumo.getQuadraFace().getEndereco().getCep().getCep());
			String complementoPontoConsumo = pontoConsumo.getDescricaoComplemento();
			if (complementoPontoConsumo == null) {
				dados.put("complemento", "");
			} else {
				dados.put("complemento", complementoPontoConsumo);
			}
		}

		return dados;

	}

	/**
	 * Recupera os Pontos Consumo com a chave
	 * primária ou com a chave primária da Fatura.
	 *
	 * @param idFatura Chave primária da fatura.
	 * @return Lista de pontos de consumo.
	 * @throws GGASException the GGAS exception
	 */
	public List<Object> obterListaPontoConsumoPorFatura(Long idFatura) throws GGASException {

		List<Object> retorno = new ArrayList<Object>();
		Long[] chavesPontoConsumo = null;

		if (idFatura != null) {
			chavesPontoConsumo = fachada.obterChavesPontosConsumoFatura(idFatura);
			for (Long idPontoConsumo : chavesPontoConsumo) {
				PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(idPontoConsumo, "cep", "imovel", "quadraFace");

				Map<String, String> dados = new HashMap<String, String>();
				dados.put("matriculaImovel", String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));
				dados.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, String.valueOf(pontoConsumo.getDescricao()));
				if (pontoConsumo.getEnderecoFormatado() != null) {
					dados.put("endereco", pontoConsumo.getEnderecoFormatado());
				} else {
					dados.put("endereco", "");
				}
				if (pontoConsumo.getQuadraFace().getEndereco().getCep().getCep() != null) {
					dados.put("cep", pontoConsumo.getQuadraFace().getEndereco().getCep().getCep());
				} else {
					dados.put("cep", "");
				}
				if (pontoConsumo.getSegmento().getDescricao() != null) {
					dados.put("segmento", pontoConsumo.getSegmento().getDescricao());
				} else {
					dados.put("segmento", "");
				}
				retorno.add(dados);
			}
		}
		return retorno;
	}

	/**
	 * listar as contas relacionadas a forma de
	 * arrecadação selecionada.
	 *
	 * @param idArrecadador the id arrecadador
	 * @param idFormaArrecadacao the id forma arrecadacao
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarContaArrecadadorSelecionado(Long idArrecadador, Long idFormaArrecadacao) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idArrecadador != null && idArrecadador > CONSTANTE_NUMERO_ZERO) {
			Collection<ArrecadadorContratoConvenio> listaACConvenio =
							fachada.consultarACConvenioPorArrecadador(idArrecadador, idFormaArrecadacao);
			Collection<ContaBancaria> contaBancaria = new HashSet<ContaBancaria>();
			Agencia agencia = null;
			for (ArrecadadorContratoConvenio arrecadadorContratoConvenio : listaACConvenio) {
				if (arrecadadorContratoConvenio.getContaCredito() != null) {
					contaBancaria.add(arrecadadorContratoConvenio.getContaCredito());
				}
			}
			if (!contaBancaria.isEmpty()) {
				for (ContaBancaria cBancaria : contaBancaria) {
					// só existe um unico elemento na "lista" conta bancaria
					agencia = fachada.obterAgenciaPorChave(cBancaria.getAgencia().getChavePrimaria());
					if (agencia.getBanco() != null) {
						dados.put("contaBancaria",
										agencia.getCodigo() + " / " + cBancaria.getNumeroConta() + " - " + cBancaria.getNumeroDigito());
						dados.put("nomeBanco", agencia.getBanco().getCodigoBanco() + " - " + agencia.getBanco().getNome());
					}
				}
			}
		}
		if (dados.isEmpty()) {
			return null;
		}
		return dados;
	}

	/**
	 * Obter perfil parcelamento.
	 *
	 * @param idPerfil the id perfil
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterPerfilParcelamento(Long idPerfil) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		ParcelamentoPerfil perfil = fachada.obterParcelamentoPerfil(idPerfil);
		if (perfil != null) {
			dados.put("indicadorDesconto", String.valueOf(perfil.isIndicadorDesconto()));
			dados.put("indicadorJuros", String.valueOf(perfil.isIndicadorJuros()));
			dados.put("indicadorCorrecaoMonetaria", String.valueOf(perfil.isIndicadorCorrecaoMonetaria()));
			if (perfil.isIndicadorDesconto()) {
				dados.put("percentualDesconto", Util.converterCampoValorDecimalParaString("percentualDesconto", perfil.getValorDesconto(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
				dados.put("numeroParcelas", String.valueOf(perfil.getMaximoParcelas()));
			}

			if (perfil.isIndicadorJuros()) {
				dados.put("percentualJuros", Util.converterCampoValorDecimalParaString("percentualJuros", perfil.getTaxaJuros(),
								Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
			}

			if (perfil.isIndicadorCorrecaoMonetaria() && perfil.getIndiceFinanceiro() != null) {
				dados.put("idIndiceFinanceiro", String.valueOf(perfil.getIndiceFinanceiro().getChavePrimaria()));
			}

		}
		return dados;

	}

	/**
	 * Recupera o valor da fatura.
	 *
	 * @param chavesFatura As chaves primárias da fatura
	 * @return mapa contendo o valor da fatura
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterValorFatura(Long[] chavesFatura) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		BigDecimal valorTotal = null;
		if (chavesFatura != null && chavesFatura.length > CONSTANTE_NUMERO_ZERO) {
			valorTotal = BigDecimal.ZERO;
			for (Long chave : chavesFatura) {
				BigDecimal valor = fachada.obterValorFatura(chave);
				if (valor != null) {
					valorTotal = valorTotal.add(valor);
				}
			}

			dados.put("valorFatura", Util.converterCampoValorDecimalParaString("Valor Fatura", valorTotal, Constantes.LOCALE_PADRAO,
							DUAS_CASAS_DECIMAIS));
		}
		return dados;

	}

	/**
	 * Obter lista fatura dialog gerar boleto.
	 *
	 * @param chavesFatura the chaves fatura
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<Object> obterListaFaturaDialogGerarBoleto(Long[] chavesFatura) throws GGASException {

		List<Object> retorno = new ArrayList<Object>();

		if (chavesFatura != null) {

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesFatura);
			filtro.put(ControladorFatura.ORDENACAO_NUMERO_FATURA_DESC, true);
			Collection<Fatura> listaFatura = fachada.consultarFatura(filtro);
			DocumentoFiscal documentoFiscal = null;

			for (Fatura fatura : listaFatura) {

				BigDecimal valorTotalDocumentoCobranca = fatura.getValorTotal();

				documentoFiscal = fachada.obterDocumentoFiscalporNumeroFatura(fatura.getChavePrimaria());

				String valorTotalDocumentoCobrancaString =
								NumeroUtil.converterCampoValorDecimalParaString(valorTotalDocumentoCobranca, DUAS_CASAS_DECIMAIS);

				String valorConciliado =
						NumeroUtil.converterCampoValorDecimalParaString(fatura.getValorConciliado(), DUAS_CASAS_DECIMAIS);

				
				BigDecimal valorRecebimentoPelaFatura =
								Util.coalescenciaNula(fachada.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria()), BigDecimal.ZERO);
				String valorRecebimentoPelaFaturaString =
								NumeroUtil.converterCampoValorDecimalParaString(valorRecebimentoPelaFatura, DUAS_CASAS_DECIMAIS);

				BigDecimal saldo = fachada.calcularSaldoFatura(fatura, true, true).getSecond();

				String saldoString = NumeroUtil.converterCampoValorDecimalParaString(saldo, DUAS_CASAS_DECIMAIS);

				Map<String, String> dados = new HashMap<String, String>();
				dados.put("numeroFatura", String.valueOf(fatura.getChavePrimaria()));
				dados.put("descricaoFaturaPontoConsumo", fatura.getDescricaoPontoConsumo());
				dados.put("dataEmissaoFatura", fatura.getDataEmissaoFormatada());
				dados.put("dataVencimentoFatura", fatura.getDataVencimentoFormatada());
				dados.put("faturaVencida", String.valueOf(fatura.isVencida()));
				dados.put("valorDocumentoCobranca", valorTotalDocumentoCobrancaString);
				dados.put("valorRecebimento", valorRecebimentoPelaFaturaString);
				dados.put("valorConciliado", valorConciliado);
				dados.put("saldo", saldoString);

				retorno.add(dados);
			}
		}
		return retorno;
	}

	/**
	 * Obter tarifa vigencia.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */

	public Map<String, Object> obterTarifaVigencia(Long chavePrimaria) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();

		if (chavePrimaria != null && chavePrimaria > CONSTANTE_NUMERO_ZERO) {

			TarifaVigencia tarifaVigencia =
							fachada.obterTarifaVigencia(chavePrimaria, "tipoCalculo", "unidadeMonetaria", "baseApuracao", STATUS,
											"tributos");
			if (tarifaVigencia != null) {

				if (tarifaVigencia.getTipoCalculo().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_TIPO_CALCULO, tarifaVigencia.getTipoCalculo().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_TIPO_CALCULO, "");
				}

				if (tarifaVigencia.getUnidadeMonetaria().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_UNIDADE_MONETARIA, tarifaVigencia.getUnidadeMonetaria().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_UNIDADE_MONETARIA, "");
				}

				if (tarifaVigencia.getComentario() != null) {
					dados.put(OBSERVACAO_VIGENCIA, tarifaVigencia.getComentario());
				} else {
					dados.put(OBSERVACAO_VIGENCIA, "");
				}

				if (tarifaVigencia.getBaseApuracao().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO, tarifaVigencia.getBaseApuracao().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO, "");
				}

				if (tarifaVigencia.getInformarMsgIcmsSubstituido() != null) {
					if (tarifaVigencia.getInformarMsgIcmsSubstituido().equals(Boolean.TRUE)) {
						dados.put(HABILITADO, "Sim");
					} else {
						dados.put(HABILITADO, "Não");
					}
					if (tarifaVigencia.getBaseCalculoIcmsSubstituido() != null) {
						dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, tarifaVigencia.getBaseCalculoIcmsSubstituido());
					} else {
						dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, "");
					}
					if (tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
						dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, tarifaVigencia.getIcmsSubstituidoMetroCubico());
					} else {
						dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, "");
					}
					if (tarifaVigencia.getMensagemIcmsSubstituto() != null) {
						dados.put(MENSAGEM_ICMS_SUBSTITUIDO, tarifaVigencia.getMensagemIcmsSubstituto());
					} else {
						dados.put(MENSAGEM_ICMS_SUBSTITUIDO, "");
					}
				} else {
					dados.put(HABILITADO, "Não");
					dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, "");
					dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, "");
					dados.put(MENSAGEM_ICMS_SUBSTITUIDO, "");
				}

				String paramStatusPendente = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
				if (tarifaVigencia.getStatus() != null && paramStatusPendente != null) {
					dados.put(STATUS, tarifaVigencia.getStatus().getDescricao());
					if (tarifaVigencia.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
						dados.put(STATUS_PENDENTE, Boolean.TRUE);
					}
				}
				StringBuilder tributos = new StringBuilder();

				for (TarifaVigenciaTributo tarifaTributo : tarifaVigencia.getTributos()) {
					tributos.append(" ").append(tarifaTributo.getTributo().getDescricao());
				}

				dados.put("tributos", tributos.toString());
			}
		}
		return dados;
	}

	/**
	 * Obter vigencia.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	@SuppressWarnings({
						"unchecked", "rawtypes"})
	public List<String[]> obterVigencia(Long chavePrimaria) throws GGASException {

		List<String[]> dados = new ArrayList<String[]>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimariaTarifa", chavePrimaria);

		String paramStatusAutorizada = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		filtro.put(STATUS, Long.parseLong(paramStatusAutorizada));

		List<TarifaVigencia> tarifaVigencia = new ArrayList(fachada.consultarTarifasVigencia(filtro));

		if (!tarifaVigencia.isEmpty()) {
			for (TarifaVigencia vigencia : tarifaVigencia) {

				dados.add(new String[] {String.valueOf(vigencia.getChavePrimaria()),
								Util.converterDataParaStringSemHora(vigencia.getDataVigencia(), FORMATO_DATA_BR)});
			}
		}

		return dados;
	}

	/**
	 * Obter tarifa vigencia desconto.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterTarifaVigenciaDesconto(Long chavePrimaria) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (chavePrimaria != null && chavePrimaria > CONSTANTE_NUMERO_ZERO) {
			TarifaVigenciaDesconto tarifaVigenciaDesconto = fachada.obterTarifaVigenciaDesconto(chavePrimaria);

			if (tarifaVigenciaDesconto.getDescricaoDescontoVigencia() != null) {
				dados.put(OBSERVACAO_VIGENCIA_DESCONTO, tarifaVigenciaDesconto.getDescricaoDescontoVigencia());
			} else {
				dados.put(OBSERVACAO_VIGENCIA_DESCONTO, "");
			}
		}

		return dados;

	}

	/**
	 * Método responsável por obter o histórico
	 * consumo.
	 *
	 * @param idHistoricoConsumo Chave do histórico de consumo.
	 * @param idContrato Chave do contrato.
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterHistoricoConsumo(Long idHistoricoConsumo, Long idContrato) throws GGASException {

		Map<String, String> dados = new HashMap<>();

		if (idHistoricoConsumo != null) {

			HistoricoConsumo historico =
							fachada.obterHistoricoConsumoParaSimulacao(idHistoricoConsumo, "historicoAnterior", "historicoAtual");

			if (historico != null) {

				// Verifica se existem pontos de
				// consumo agrupados
				@SuppressWarnings("deprecation")
				EntidadeConteudo tipoAgrupamento = fachada.obterEntidadeConteudo(EntidadeConteudo.CHAVE_AGRUPAMENTO_POR_VOLUME);
				Collection<PontoConsumo> listaPontoConsumoAgrupados =
								fachada.listarPontoConsumoFaturamentoAgrupado(idContrato, tipoAgrupamento);

				preencherConsumo(dados, historico, listaPontoConsumoAgrupados, new ColetorConsumoPuro());
				preencherConsumo(dados, historico, listaPontoConsumoAgrupados, new ColetorConsumoApurado());
				Date dataInicial = null;
				DateTime dateTimeInicial = null;
				Date dataFinal = historico.getHistoricoAtual().getDataLeituraFaturada();

				Collection<HistoricoConsumo> listaHistoricoConsumo =
								fachada.consultarHistoricoConsumoPorHistoricoConsumoSintetizador(historico);
				if (!listaHistoricoConsumo.isEmpty()) {

					dataInicial = listaHistoricoConsumo.iterator().next().getHistoricoAtual().getDataLeituraInformada();
					dateTimeInicial = new DateTime(dataInicial);
					 
				} else { 
					if (historico.getHistoricoAnterior() != null && historico.getHistoricoAnterior().getDataLeituraFaturada() != null) {

						dataInicial = historico.getHistoricoAnterior().getDataLeituraFaturada();
						dateTimeInicial = new DateTime(dataInicial);
						
					} else {

						Date dataLeitura =
								fachada.setarDataLeituraAnterior(null, historico.getHistoricoAtual().getPontoConsumo(), historico
													.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor());
						dataInicial = dataLeitura;
						dateTimeInicial = new DateTime(dataInicial);
					}
					// Só adicionar mais 1 dia à data
					// inicial caso ela seja menor que
					// a final.
					if (dataInicial.compareTo(dataFinal) < CONSTANTE_NUMERO_ZERO ) {

						dateTimeInicial = dateTimeInicial.plusDays(1);
					}
					
				}

				DateTime dateTimeFinal = new DateTime(dataFinal);

				// Calcula a quantidade de dias de consumo
				int diasConsumo = 0;
				DateTime dateTimeIteracao = dateTimeInicial;
				while (!dateTimeIteracao.isAfter(dateTimeFinal)) {
					diasConsumo++;
					dateTimeIteracao = dateTimeIteracao.plusDays(1);
				}

				dados.put(DATA_PERIODO_INICIAL, DataUtil.converterDataParaString(dateTimeInicial.toDate()));
				dados.put(DATA_PERIODO_FINAL, DataUtil.converterDataParaString(dataFinal));
				dados.put(DIAS_CONSUMO, String.valueOf(diasConsumo));

			}
		}

		return dados;
	}

	private void preencherConsumo(Map<String, String> dados, HistoricoConsumo historico,
					Collection<PontoConsumo> listaPontoConsumoAgrupados, ColetorConsumo coletorConsumo) throws GGASException {
		
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String escalaParametro =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);
		int escala = Integer.parseInt(escalaParametro);


		BigDecimal consumo = BigDecimal.ZERO;
		if (listaPontoConsumoAgrupados != null && !listaPontoConsumoAgrupados.isEmpty()) {
			int anoMesFaturamento = historico.getAnoMesFaturamento();
			int ciclo = historico.getNumeroCiclo();
			consumo = coletorConsumo.coletarPorConsulta(anoMesFaturamento, ciclo, listaPontoConsumoAgrupados);
		} else {
			if (historico.getConsumoApurado() != null) {
				consumo = coletorConsumo.coletarPorPropriedade(historico);
			}
		}

		dados.put(coletorConsumo.getChaveMapaDados(), Util.converterCampoValorDecimalParaString("", consumo, Constantes.LOCALE_PADRAO, escala));
	}

	/**
	 * Método responsável por consultar a rubrica
	 * e obter o valor máximo.
	 *
	 * @param idRubrica A chave primária da rubrica.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterValorRubrica(Long idRubrica) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Rubrica rubrica = fachada.obterRubrica(idRubrica, "amortizacao", "valorRegulamentados");
		if (rubrica != null) {
			if (!rubrica.getIndicadorRegulamentada()) {
				if (rubrica.getValorReferencia() != null) {
					dados.put("valor", Util.converterCampoValorDecimalParaString("valor", rubrica.getValorReferencia(),
									Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
				}
				dados.put("indicadorAlteracaoValor", String.valueOf(!rubrica.getIndicadorRegulamentada()));
			} else {
				// nova regra
				Date dataAtual = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());
				RubricaValorRegulamentado rubricaValorRegulamentadoSelecionado = null;
				for (RubricaValorRegulamentado rubricaValorRegulamentado : rubrica.getValorRegulamentados()) {
					if (rubricaValorRegulamentado != null
									&& Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataInicioVigencia()) >= CONSTANTE_NUMERO_ZERO
									&& rubricaValorRegulamentado.getDataFimVigencia() == null) {
						rubricaValorRegulamentadoSelecionado = rubricaValorRegulamentado;
						break;
					} else if (rubricaValorRegulamentado != null
									&& Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataInicioVigencia()) >= CONSTANTE_NUMERO_ZERO
									&& Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataFimVigencia()) <= CONSTANTE_NUMERO_ZERO) {
						rubricaValorRegulamentadoSelecionado = rubricaValorRegulamentado;
						break;
					}
				}

				if (rubricaValorRegulamentadoSelecionado != null) {

					dados.put("valor", Util.converterCampoValorDecimalParaString("valor",
									rubricaValorRegulamentadoSelecionado.getValorReferencia(), Constantes.LOCALE_PADRAO,
									DUAS_CASAS_DECIMAIS));
				}

				dados.put("indicadorAlteracaoValor", String.valueOf(!rubrica.getIndicadorRegulamentada()));
			}
			if (rubrica.getAmortizacao() != null) {
				dados.put("indicadorTaxaJuros", "true");
			} else {
				dados.put("indicadorTaxaJuros", "false");
			}

			if (rubrica.getIndicadorRegulamentada() != null) {
				dados.put("indicadorRegulamentada", String.valueOf(rubrica.getIndicadorRegulamentada()));
			} else {
				dados.put("indicadorRegulamentada", "false");
			}

			if (rubrica.getValorReferencia() != null) {
				dados.put("valorReferencia", Util.converterCampoCurrencyParaString(rubrica.getValorReferencia(), Constantes.LOCALE_PADRAO));
			}

			dados.put("indicadorDescricaoComplementar", String.valueOf(rubrica.getPermiteDescricaoComplementar()));

			dados.put("indicadorEntradaObrigatoria", String.valueOf(rubrica.getIndicadorEntradaObrigatoria()));

		}
		return dados;
	}

	/**
	 * Método responsável por consultar um
	 * Lançamento Item Contábil e retornar seu
	 * tipo, caso seja
	 * débito ou crédito.
	 *
	 * @param idLancamentoItemContabil the id lancamento item contabil
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterIndicadorTipoLAIC(Long idLancamentoItemContabil) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		LancamentoItemContabil lancamentoItemContabil = fachada.obterLancamentoItemContabil(idLancamentoItemContabil);
		if (lancamentoItemContabil != null) {

			String chaveTipoDebito = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);

			String chaveTipoCredito = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);

			if (lancamentoItemContabil.getTipoCreditoDebito().getChavePrimaria() == Integer.parseInt(chaveTipoDebito)) {
				dados.put("indicadorTipoLAIC", String.valueOf(Boolean.TRUE));
			} else if (lancamentoItemContabil.getTipoCreditoDebito().getChavePrimaria() == Integer.parseInt(chaveTipoCredito)) {
				dados.put("indicadorTipoLAIC", String.valueOf(Boolean.FALSE));
			}
		}
		return dados;
	}

	/**
	 * Método responsável por listar rubricas
	 * levando em consideração o tipo de
	 * Crédito/Débito.
	 *
	 * @param tipoDebito the tipo debito
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<String, String> listarRubricasPorTipoCreditoDebito(String tipoDebito) throws NegocioException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (!StringUtils.isEmpty(tipoDebito)) {
			Boolean isTipoDebito = Boolean.valueOf(tipoDebito);
			Collection<Rubrica> listaRubrica = fachada.listarRubricasPorTipoCreditoDebito(isTipoDebito);

			if (listaRubrica != null) {

				for (Rubrica rubrica : listaRubrica) {
					dados.put(String.valueOf(rubrica.getChavePrimaria()), rubrica.getDescricao());
				}
			}

		}
		return dados;
	}

	/**
	 * Intervalo datas.
	 *
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @return the int
	 * @throws GGASException the GGAS exception
	 */
	public int intervaloDatas(String dataInicio, String dataFim) throws GGASException {

		Date inicio = Util.converterCampoStringParaData("Data", dataInicio, FORMATO_DATA_BR);
		Date fim = Util.converterCampoStringParaData("Data", dataFim, FORMATO_DATA_BR);

		return Util.intervaloDatas(inicio, fim);
	}

	/**
	 * Obter entidade conteudo pela chave.
	 *
	 * @param idEntidadeConteudo the id entidade conteudo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterEntidadeConteudoPelaChave(Long idEntidadeConteudo) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();
		if ((idEntidadeConteudo != null) && (idEntidadeConteudo > CONSTANTE_NUMERO_ZERO)) {
			EntidadeConteudo entidadeConteudo = fachada.obterEntidadeConteudo(idEntidadeConteudo);
			dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, entidadeConteudo.getChavePrimaria());
			dados.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, entidadeConteudo.getDescricao());
			if (entidadeConteudo.getDescricao() != null) {
				dados.put("descricaoAbreviada", entidadeConteudo.getDescricaoAbreviada());
			}
			dados.put("indicadorPadrao", entidadeConteudo.isIndicadorPadrao());
		} else {
			dados = null;
		}

		return dados;
	}

	/**
	 * Obter segmento ponto consumo imovel.
	 *
	 * @param chavePrimariaImovel the chave primaria imovel
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterSegmentoPontoConsumoImovel(Long chavePrimariaImovel) throws GGASException {

		List<Segmento> listaSegmento = new ArrayList<Segmento>();
		Map<String, String> dados = null;

		Collection<PontoConsumo> listaPontoConsumo = fachada.listarPontoConsumoPorChaveImovel(chavePrimariaImovel);
		if (!listaPontoConsumo.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				if (listaSegmento.isEmpty()) {
					listaSegmento.add(pontoConsumo.getSegmento());
					dados = new HashMap<String, String>();
					dados.put(ID_SEGMENTO, String.valueOf(pontoConsumo.getSegmento().getChavePrimaria()));
					continue;
				}
				if (!listaSegmento.contains(pontoConsumo.getSegmento())) {
					break;
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por verificar se uma
	 * tarifa tem associação ao dolar.
	 *
	 * @param chavePrimariaTarifa chave primária da tarifa
	 * @return true caso válida
	 * @throws NegocioException the negocio exception
	 */
	public boolean verificaUnidadeMonetariaTarifaVigencia(Long chavePrimariaTarifa) throws NegocioException {

		if(chavePrimariaTarifa != null) {
			return fachada.verificarUnidadeMonetariaTarifaVigencia(chavePrimariaTarifa);
		}
		
		return false;
	}

	/**
	 * Método responsável por consultar as
	 * operações batchs.
	 *
	 * @param idModulo
	 *            A chave primária de um módulo do
	 *            sistema
	 * @return Uma coleção de operações
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> consultarOperacoesBatchPorModulo(long idModulo) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<Operacao> listaOperacoes = fachada.consultarOperacoesBatchPorModulo(idModulo);
		if (listaOperacoes != null && !listaOperacoes.isEmpty()) {
			for (Operacao operacao : listaOperacoes) {
				dados.put(String.valueOf(operacao.getChavePrimaria()), operacao.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * *
	 * Método responsável por listar as tarifa
	 * usando o item de fatura.
	 *
	 * @param idItemFatura the id item fatura
	 * @param idPontoConsumo the id ponto consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarTarifaVigenciaItemFatura(Long idItemFatura, Long idPontoConsumo) throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFatura =
						fachada.obterTarifaPontoConsumoItemFaturamento(idPontoConsumo, idItemFatura);
		PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(idPontoConsumo);
		ContratoPontoConsumo contratoPontoConsumo = fachada.obterContratoAtivoPontoConsumo(pontoConsumo);
		// TKT 3332
		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = fachada.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
		}
		Long idSegmento = contratoPontoConsumo.getPontoConsumo().getSegmento().getChavePrimaria();
		Collection<TarifaVigencia> listaTarifaVigencia = fachada.listarTarifaVigenciaItemFatura(idItemFatura, idSegmento);

		if (listaTarifaVigencia != null && !listaTarifaVigencia.isEmpty()) {
			for (TarifaVigencia tarifaVigencia : listaTarifaVigencia) {
				if (contratoPontoConsumoItemFatura.getTarifa().getChavePrimaria() != tarifaVigencia.getTarifa().getChavePrimaria()) {
					dados.put(String.valueOf(tarifaVigencia.getTarifa().getChavePrimaria()), tarifaVigencia.getTarifa().getDescricao());
				}
			}
		}

		return dados;
	}

	/**
	 * Listar tarifa vigencia por tarifa.
	 *
	 * @param idTarifa the id tarifa
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> listarTarifaVigenciaPorTarifaNaoAutorizada(Long idTarifa) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		EntidadeConteudo entidadeConteudo = fachada.obterEntidadeConteudoLikeDescricao(STATUS_VIGENCIA_NAO_AUTORIZADA);

		if(entidadeConteudo != null){
			filtro.put("status", entidadeConteudo.getChavePrimaria());
		}

		filtro.put("idTarifa", idTarifa);

		List<TarifaVigencia> listaTarifaVigencia = fachada.consultarTarifasVigencia(filtro);

		List<String[]> dados = new ArrayList<String[]>();
		if (listaTarifaVigencia != null && !listaTarifaVigencia.isEmpty()) {
			for (TarifaVigencia tarifaVigencia : listaTarifaVigencia) {
				dados.add(new String[] {String.valueOf(tarifaVigencia.getChavePrimaria()),
								Util.converterDataParaStringSemHora(tarifaVigencia.getDataVigencia(), FORMATO_DATA_BR)});
			}
		}

		return dados;
	}


	/**
	 * Listar tarifa vigencia por tarifa.
	 *
	 * @param idTarifa the id tarifa
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> listarTarifaVigenciaPorTarifa(Long idTarifa) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idTarifa", idTarifa);

		List<TarifaVigencia> listaTarifaVigencia = fachada.consultarTarifasVigencia(filtro);

		List<String[]> dados = new ArrayList<String[]>();
		if (listaTarifaVigencia != null && !listaTarifaVigencia.isEmpty()) {
			for (TarifaVigencia tarifaVigencia : listaTarifaVigencia) {
				dados.add(new String[] {String.valueOf(tarifaVigencia.getChavePrimaria()),
								Util.converterDataParaStringSemHora(tarifaVigencia.getDataVigencia(), FORMATO_DATA_BR)});
			}
		}

		return dados;
	}

	/**
	 * *
	 * Método responsável por listar os itens de
	 * fatura de um ponto de consumo associados a
	 * um
	 * contrato.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarItemFaturaTarifaPontoConsumo(Long idPontoConsumo) throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();

		PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(idPontoConsumo);
		ContratoPontoConsumo contratoPontoConsumo = fachada.obterContratoAtivoPontoConsumo(pontoConsumo);
		// TKT 3332
		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = fachada.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
		}

		Collection<EntidadeConteudo> itensFatura = fachada.listarItemFaturaPontoConsumo(contratoPontoConsumo);

		if (itensFatura != null && !itensFatura.isEmpty()) {
			for (EntidadeConteudo itemFatura : itensFatura) {
				dados.put(String.valueOf(itemFatura.getChavePrimaria()), itemFatura.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por obter um
	 * IndiceFinanceiro.
	 *
	 * @param chavePrimaria
	 *            Chave primária do índice
	 *            finaceiro.
	 * @return Um IndiceFinanceiro.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> obterIndiceFinanceiro(Long chavePrimaria) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if ((chavePrimaria != null) && (chavePrimaria > CONSTANTE_NUMERO_ZERO)) {
			IndiceFinanceiro indiceFinanceiro = fachada.obterIndiceFinanceiro(chavePrimaria);

			if (indiceFinanceiro != null) {

				dados.put("indicadorMensal", String.valueOf(indiceFinanceiro.isIndicadorMensal()));
				dados.put("indicadorPermiteNegativo", String.valueOf(indiceFinanceiro.isIndicadorPermiteNegativo()));
				dados.put("indicadorValorPercentual", String.valueOf(indiceFinanceiro.isIndicadorValorPercentual()));
			}
		}

		return dados;
	}

	/**
	 * Atualizar valores corrigidos.
	 *
	 * @param chavesFatura the chaves fatura
	 * @param stringDataFinal the string data final
	 * @param adicionarImpontualidade the adicionar impontualidade
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<Object> atualizarValoresCorrigidos(Long[] chavesFatura, String stringDataFinal, boolean adicionarImpontualidade)
					throws GGASException {

		List<Object> retorno = new ArrayList<Object>();

		if (chavesFatura != null) {

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesFatura);
			Collection<Fatura> listaFatura = fachada.consultarFatura(filtro);

			for (Fatura fatura : listaFatura) {

				BigDecimal saldoCorrigido =
								fachada.calcularSaldoFatura(fatura, DataUtil.parse(stringDataFinal), true, adicionarImpontualidade)
												.getSecond();
				String stringSaldoCorrigido = NumeroUtil.converterCampoValorDecimalParaString(saldoCorrigido, DUAS_CASAS_DECIMAIS);

				Map<String, String> dados = new HashMap<String, String>();
				dados.put(Fatura.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(fatura.getChavePrimaria()));
				dados.put("saldoCorrigido", stringSaldoCorrigido);

				retorno.add(dados);
			}
		}
		return retorno;
	}

	/**
	 * Obter tarifa vigencia desconto por tarifa vigencia.
	 *
	 * @param chavePrimariaTarifaVigencia the chave primaria tarifa vigencia
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
 	public Map<String, String> obterTarifaVigenciaDescontoPorTarifaVigencia(Long chavePrimariaTarifaVigencia) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chavePrimariaTarifaVigencia != null && chavePrimariaTarifaVigencia > CONSTANTE_NUMERO_ZERO) {

			Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto =
							fachada.obterTarifaVigenciaDescontoPorTarifaVigencia(chavePrimariaTarifaVigencia);
			if (listaTarifaVigenciaDesconto != null && !listaTarifaVigenciaDesconto.isEmpty()) {

				for (TarifaVigenciaDesconto tarifaVigenciaDesconto : listaTarifaVigenciaDesconto) {
					dados.put(String.valueOf(tarifaVigenciaDesconto.getChavePrimaria()),
									Util.formatarDataTarifa(tarifaVigenciaDesconto.getInicioVigencia(),
													tarifaVigenciaDesconto.getFimVigencia()));
				}
			}
			return dados;
		}
		return dados;
	}

	/**
	 * Obter tarifa vigencia desconto por tarifa vigencia ordenada.
	 *
	 * @param chavePrimariaTarifaVigencia the chave primaria tarifa vigencia
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> obterTarifaVigenciaDescontoPorTarifaVigenciaOrdenada(Long chavePrimariaTarifaVigencia) throws GGASException {

		Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = fachada.
				obterTarifaVigenciaDescontoPorTarifaVigencia(chavePrimariaTarifaVigencia);

		List<String[]> dados = new ArrayList<String[]>();

		if (listaTarifaVigenciaDesconto != null && !listaTarifaVigenciaDesconto.isEmpty()) {
			for (TarifaVigenciaDesconto tarifaVigenciaDesconto : listaTarifaVigenciaDesconto) {

				dados.add(new String[] {String.valueOf(tarifaVigenciaDesconto.getChavePrimaria()),
						Util.formatarDataTarifa(tarifaVigenciaDesconto.getInicioVigencia(),
								tarifaVigenciaDesconto.getFimVigencia())});

			}
		}

		return dados;
	}

	/**
	 * Obter multiplicacao valores.
	 *
	 * @param valorQuantidade the valor quantidade
	 * @param valorUnitario the valor unitario
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterMultiplicacaoValores(String valorQuantidade, String valorUnitario) throws GGASException {

		return fachada.obterMultiplicacaoValorUnitario(valorQuantidade, valorUnitario);
	}

	/**
	 * Listar unidades organizacionais.
	 *
	 * @param descricao the descricao
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] listarUnidadesOrganizacionais(String descricao) throws GGASException {

		String[] descricoesUnidadesOrganizacionais;

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao);

		Collection<UnidadeOrganizacional> unidadesOrganizacionais = fachada.consultarUnidadeOrganizacional(filtro);
		descricoesUnidadesOrganizacionais = new String[unidadesOrganizacionais.size()];

		int i = 0;
		for (UnidadeOrganizacional unidadeOrganizacional : unidadesOrganizacionais) {
			descricoesUnidadesOrganizacionais[i] = unidadeOrganizacional.getDescricao();
			i++;
		}

		return descricoesUnidadesOrganizacionais;
	}

	/**
	 * Listar funcionarios.
	 *
	 * @param nome the nome
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] listarFuncionarios(String nome) throws GGASException {

		String[] nomeFuncionario;

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", nome);

		Collection<Funcionario> funcionarios = fachada.consultarFuncionarios(filtro);
		nomeFuncionario = new String[funcionarios.size()];

		int i = 0;
		for (Funcionario funcionario : funcionarios) {
			nomeFuncionario[i] = funcionario.getNome();
			i++;
		}

		return nomeFuncionario;
	}

	/**
	 * Método responsável por verificar se a
	 * periodicidade é do tipo 'mensal'.
	 *
	 * @param idPeriodicidade id da periodicidade.
	 * @return true caso seja mensal.
	 * @throws GGASException the GGAS exception
	 */
	public boolean isPeriodicidadeMensal(long idPeriodicidade) throws GGASException {

		boolean retorno = false;
		String periodicidadeMensal = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_MENSAL);

		if (idPeriodicidade == Long.parseLong(periodicidadeMensal)) {
			retorno = true;
		}

		return retorno;
	}

	/**
	 * Obter ramos atividade por segmentos.
	 *
	 * @param chavesSegmentos the chaves segmentos
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterRamosAtividadePorSegmentos(Long[] chavesSegmentos) throws GGASException {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		if (chavesSegmentos != null && chavesSegmentos.length > CONSTANTE_NUMERO_ZERO) {

			Collection<RamoAtividade> listaRamosAtividade = fachada.consultarRamoAtividadePorSegmentos(chavesSegmentos);
			if (listaRamosAtividade != null && !listaRamosAtividade.isEmpty()) {
				for (RamoAtividade ramoAtividade : listaRamosAtividade) {
					retorno.put(String.valueOf(ramoAtividade.getChavePrimaria()), ramoAtividade.getDescricao() + "		- Segmento: "
									+ ramoAtividade.getSegmento().getDescricao());
				}
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por listar as tarifas
	 * relacionadas a um ponto de consumo e a um
	 * item de
	 * fatura.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chaveItemFatura the chave item fatura
	 * @return Map<String, String>
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterTarifas(Long chavePontoConsumo, Long chaveItemFatura) throws GGASException {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		List<Tarifa> listaTarifa = fachada.obterTarifasPorRamoAtividade(chavePontoConsumo, chaveItemFatura);

		if (listaTarifa == null || listaTarifa.isEmpty()) {
			listaTarifa = fachada.obterTarifas(chavePontoConsumo, chaveItemFatura);
		} else {
			List<Tarifa> listaTarifaSegmento = fachada.obterTarifas(chavePontoConsumo, chaveItemFatura);
			if (listaTarifaSegmento != null && !listaTarifaSegmento.isEmpty()) {
				for (Tarifa tarifa : listaTarifaSegmento) {
					if (!listaTarifa.contains(tarifa)) {
						listaTarifa.add(tarifa);
					}
				}
			}
		}

		if (listaTarifa != null) {

			for (Tarifa tarifa : listaTarifa) {
				retorno.put(String.valueOf(tarifa.getChavePrimaria()), tarifa.getDescricao());
			}

		}

		return retorno;
	}

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de
	 * consumo e medidor / vazaoCorretor.
	 *
	 * @param tipoAssociacao the tipo associacao
	 * @param status the status
	 * @param medidor the medidor
	 * @param corretorVazao the corretor vazao
	 * @param isAssociacaoMedidorIndependente the is associacao medidor independente
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterAcoesAssociacao(Integer tipoAssociacao, String status, String medidor, String corretorVazao,
					boolean isAssociacaoMedidorIndependente) throws GGASException {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		List<OperacaoMedidor> listaOperacoes = null;
		if (isAssociacaoMedidorIndependente) {
			listaOperacoes = fachada.obterOperacoesMedidorIndependentePorTipoAssociacao(tipoAssociacao, status, medidor, corretorVazao);
		} else {
			listaOperacoes = fachada.obterOperacoesMedidorPorTipoAssociacao(tipoAssociacao, status, medidor, corretorVazao);
		}

		for (OperacaoMedidor operacaoMedidor : listaOperacoes) {
			retorno.put(String.valueOf(operacaoMedidor.getChavePrimaria()), operacaoMedidor.getDescricao());
		}

		return retorno;
	}

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * imóvel e medidor / vazaoCorretor.
	 *
	 * @param tipoAssociacao the tipo associacao
	 * @param idImovel a chave primária do imóvel
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterAcoesAssociacaoLote(Integer tipoAssociacao, Long idImovel) throws GGASException {
		Map<String, String> retorno = new LinkedHashMap<>();
		List<OperacaoMedidor> listaOperacoes = fachada.obterOperacoesMedidorPorImovelCondonimioIndividual(tipoAssociacao, idImovel);
		for (OperacaoMedidor operacaoMedidor : listaOperacoes) {
			retorno.put(String.valueOf(operacaoMedidor.getChavePrimaria()), operacaoMedidor.getDescricao());
		}
		return retorno;
	}

	/**
	 * Calcula o valor total da indenização de
	 * encerramento/recisão do contrato.
	 *
	 * @param idContrato Identificador do contrato alvo.
	 * @param tipoMulta the tipo multa
	 * @param valorInvestimento the valor investimento
	 * @param periodoInvestimento the periodo investimento
	 * @param igpmInicial the igpm inicial
	 * @param periodoRecisao the periodo recisao
	 * @param igpmRecisao the igpm recisao
	 * @param periodoConsumo the periodo consumo
	 * @param valorAdicionalMensal the valor adicional mensal
	 * @param qtdDiariaContratada the qtd diaria contratada
	 * @param qtdConsumida the qtd consumida
	 * @param qtdMinimaContratada the qtd minima contratada
	 * @param diasRestantes the dias restantes
	 * @param tarifaGas the tarifa gas
	 * @return mapResultados
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> calcularValorIndenizacaoEncerrarRescindir(Long idContrato, Long tipoMulta, BigDecimal valorInvestimento,
					String periodoInvestimento, BigDecimal igpmInicial, String periodoRecisao, BigDecimal igpmRecisao,
					BigDecimal periodoConsumo, BigDecimal valorAdicionalMensal, BigDecimal qtdDiariaContratada, BigDecimal qtdConsumida,
					BigDecimal qtdMinimaContratada, BigDecimal diasRestantes, BigDecimal tarifaGas) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		CalculoIndenizacaoVO dadosCalculo = new CalculoIndenizacaoVO();
		dadosCalculo.setTipoMulta(tipoMulta);

		dadosCalculo.setValorInvestimento(valorInvestimento);

		try {
			if (periodoInvestimento != null && !"".equals(periodoInvestimento)) {
				dadosCalculo.setDataInvestimento(new DateTime(Util.converterCampoStringParaData("Período do investimento",
								periodoInvestimento, FORMATO_DATA_BR)));
			}
			dadosCalculo.setIgpmInvestimento(igpmInicial);

			if (periodoRecisao != null && !"".equals(periodoRecisao)) {
				dadosCalculo.setDataRecisao(new DateTime(Util.converterCampoStringParaData("Período da Recisão", periodoRecisao,
								FORMATO_DATA_BR)));
			}
		} catch (GGASException ex) {
			LOG.error(ex.getMessage(), ex);
			String msg;
			if (mensagens != null) {
				msg = "Dados inválidos. Verifique o Período do investimento e da Recisão";
			} else {
				msg = ex.getChaveErro();
			}
			dados.put("erro", msg);
		}

		dadosCalculo.setIgpmRecisao(igpmRecisao);

		dadosCalculo.setMesesPeriodoConsumo(periodoConsumo);
		dadosCalculo.setValorAdicionalMensal(valorAdicionalMensal);

		dadosCalculo.setQtdDiariaContratada(qtdDiariaContratada);
		dadosCalculo.setQtdConsumida(qtdConsumida);
		dadosCalculo.setQtdMinimaContratada(qtdMinimaContratada);

		dadosCalculo.setDiasRestantes(diasRestantes);
		dadosCalculo.setTarifaGas(tarifaGas);

		try {
			fachada.calcularIndenizacaoRescisao(dadosCalculo);

			dados.put("valorIndenizacao", dadosCalculo.getValorTotal().toPlainString());
			dados.put("valorIndenizacaoFormatado",
							Util.converterCampoCurrencyParaString(dadosCalculo.getValorTotal(), Constantes.LOCALE_PADRAO));

			if (dadosCalculo.getInvestimentoCorrigido() != null) {
				dados.put("investimentoCorrigido",
								Util.converterCampoCurrencyParaString(dadosCalculo.getInvestimentoCorrigido(), Constantes.LOCALE_PADRAO));
			}

		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			String msg;
			if (mensagens != null) {
				msg = mensagens.getProperty(e.getChaveErro());
			} else {
				msg = e.getChaveErro();
			}
			dados.put("erroCalculoIndenizacao", msg);
		}

		return dados;
	}

	/**
	 * Valida a data de recisão informada pelo
	 * usuário para o cálculo da recisão e
	 * calcula o IGPM de recisão e o período de
	 * consumo.
	 *
	 * @param periodoInvestimento the periodo investimento
	 * @param periodoRecisao the periodo recisao
	 * @return mapResultados
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> validarPeriodoRecisao(String periodoInvestimento, String periodoRecisao) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		DateTime dataInvestimento = null;
		if (periodoInvestimento != null && !"".equals(periodoInvestimento)) {
			dataInvestimento = new DateTime(Util.converterCampoStringParaData("", periodoInvestimento, FORMATO_DATA_BR));
		}

		DateTime dataRecisao = null;
		if (periodoRecisao != null && !"".equals(periodoRecisao)) {
			dataRecisao = new DateTime(Util.converterCampoStringParaData("", periodoRecisao, FORMATO_DATA_BR));
		}

		try {
			BigDecimal periodoConsumo = fachada.calcularMesesPeriodoConsumo(dataInvestimento, dataRecisao);
			BigDecimal igpmRecisao = null;
			try {
				igpmRecisao =
								fachada.buscarValorIndiceFinanceiroNominal(dataRecisao,
												ControladorContratoImpl.INDICE_FINANCEIRO_IGPM_ABREVIADO);
			} catch (NegocioException e) {
				LOG.error(e.getStackTrace(), e);
				DateTime mesAnteriorRecisao = dataRecisao.minusMonths(1);
				igpmRecisao =
								fachada.buscarValorIndiceFinanceiroNominal(mesAnteriorRecisao,
												ControladorContratoImpl.INDICE_FINANCEIRO_IGPM_ABREVIADO);
			}

			String igpmRecisaoLabel;
			if (igpmRecisao == null) {
				igpmRecisaoLabel = "";
			} else {
				igpmRecisaoLabel = igpmRecisao.toPlainString();
			}
			String periodoConsumoLabel;
			if (periodoConsumo == null) {
				periodoConsumoLabel = "";
			} else {
				periodoConsumoLabel = periodoConsumo.toPlainString();
			}

			dados.put("igpmRecisao", igpmRecisaoLabel);
			dados.put("periodoConsumo", periodoConsumoLabel);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			String msg;
			if (mensagens != null) {
				msg = mensagens.getProperty(e.getChaveErro());
			} else {
				msg = e.getChaveErro();
			}
			dados.put("erro", msg);
		}

		return dados;
	}

	/**
	 * Método responsável por calcular o valor de
	 * desconto sobre o valor calculado.
	 *
	 * @param valorTotalFormatado the valor total formatado
	 * @param percentualDescontoFormatado the percentual desconto formatado
	 * @param valorFinalFormatado the valor final formatado
	 * @param totalValorFinalFormatado the total valor final formatado
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] aplicarPercentualDescontoSobreValorTotal(String valorTotalFormatado, String percentualDescontoFormatado,
					String valorFinalFormatado, String totalValorFinalFormatado) throws GGASException {

		return fachada.aplicarPercentualDescontoSobreValorTotal(valorTotalFormatado, percentualDescontoFormatado, valorFinalFormatado,
						totalValorFinalFormatado);

	}

	/**
	 * Método responsável por calcular o
	 * percentual de desconto sobre o valor
	 * calculado.
	 *
	 * @param valorTotalFormatado the valor total formatado
	 * @param valorDescontoFormatado the valor desconto formatado
	 * @param valorFinalFormatado the valor final formatado
	 * @param totalValorFinalFormatado the total valor final formatado
	 * @return the string[]
	 * @throws GGASException the GGAS exception
	 */
	public String[] aplicarValorDescontoSobreValorTotal(String valorTotalFormatado, String valorDescontoFormatado,
					String valorFinalFormatado, String totalValorFinalFormatado) throws GGASException {

		return fachada.aplicarValorDescontoSobreValorTotal(valorTotalFormatado, valorDescontoFormatado, valorFinalFormatado,
						totalValorFinalFormatado);

	}

	/**
	 * Método responsável por calcular o valor
	 * total.
	 *
	 * @param valorTotalFormatado the valor total formatado
	 * @param valorFormatado the valor formatado
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String aplicarValorSobreValorTotal(String valorTotalFormatado, String valorFormatado) throws GGASException {

		BigDecimal valorTotal =
						Util.converterCampoStringParaValorBigDecimal("Valor Total", valorTotalFormatado, Constantes.FORMATO_VALOR_BR,
										Constantes.LOCALE_PADRAO);
		BigDecimal valorFinal =
						Util.converterCampoStringParaValorBigDecimal("Valor Final", valorFormatado, Constantes.FORMATO_VALOR_BR,
										Constantes.LOCALE_PADRAO);

		valorTotal = valorTotal.subtract(valorFinal);

		return Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO);
	}

	/**
	 * Método responsável por atualizar os dados
	 * das solicitacoes de consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param mapa the mapa
	 * @return the map
	 * @throws GGASException the exception
	 */
	public Map<String, Map<String, String>> atualizarDadosSolicitacoesConsumo(Long idPontoConsumo, Map<String, Map<String, String>> mapa)
			throws GGASException {

		return fachada.atualizarDadosSolicitacoesConsumo(idPontoConsumo, mapa);

	}

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais filtradas pela
	 * empresa
	 * selecionada.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarEntidadeClasse() throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa unidades organizacionais pela
		// chave da empresa
		Collection<EntidadeClasse> entidades = fachada.listarEntidadeClasse();

		if (entidades != null && !entidades.isEmpty()) {
			for (EntidadeClasse entidade : entidades) {
				dados.put(String.valueOf(entidade.getChavePrimaria()), entidade.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais filtradas pela
	 * empresa
	 * selecionada.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarClasseUnidade() throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		// Pesquisa unidades organizacionais pela
		// chave da empresa
		Collection<ClasseUnidade> entidades = fachada.listarClasseUnidade();

		if (entidades != null && !entidades.isEmpty()) {
			for (ClasseUnidade entidade : entidades) {
				dados.put(String.valueOf(entidade.getChavePrimaria()), entidade.getDescricao());
			}
		}
		return dados;
	}

	/**
	 * Listar funcionalidades por modulos.
	 *
	 * @param idModulo the id modulo
	 * @return the map
	 * @throws NegocioException the exception
	 */
	public Map<String, String[]> listarFuncionalidadesPorModulos(Long idModulo) throws NegocioException {

		Map<String, String[]> dados = new LinkedHashMap<String, String[]>();

		Collection<Menu> colecaoMenu = fachada.listarFuncionalidadesPorModulo(idModulo);

		if (colecaoMenu != null && !colecaoMenu.isEmpty()) {

			List<Object> lista = new ArrayList<Object>();

			for (Menu menu : colecaoMenu) {

				Collection<Menu> colecaoSubFuncionalidades = fachada.listarFuncionalidadesPorModulo(menu.getChavePrimaria());
				if (colecaoSubFuncionalidades != null && !colecaoSubFuncionalidades.isEmpty()) {
					lista.add(new Object[] {
											menu.getChavePrimaria(), menu.getDescricao(), Boolean.TRUE});
					for (Menu subMenu : colecaoSubFuncionalidades) {
						lista.add(new Object[] {
												subMenu.getChavePrimaria(), subMenu.getDescricao(), null});
					}
				} else {
					lista.add(new Object[] {
											menu.getChavePrimaria(), menu.getDescricao(), Boolean.FALSE});
				}
			}

			for (Object obj : lista) {
				Object[] dadosMenu = (Object[]) obj;
				dados.put(String.valueOf(dadosMenu[CONSTANTE_NUMERO_ZERO]), new String[] {
																		String.valueOf(dadosMenu[1]), String.valueOf(dadosMenu[2])});
			}
		}

		return dados;
	}

	/**
	 * Método responsável por carregar as
	 * operações de uma funcionalidade.
	 *
	 * @param idFuncionalidade chave da funcionalidade
	 * @return coleção de operações da
	 *         funcionalidade
	 * @throws NegocioException caso ocorra algum erro
	 */
	public Map<String, String> carregarOperacoes(Long idFuncionalidade) throws NegocioException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (idFuncionalidade != null) {
			Collection<Operacao> colecaoOperacao = fachada.listarOperacoesPorFuncionalidade(idFuncionalidade);

			if (colecaoOperacao != null && !colecaoOperacao.isEmpty()) {

				List<Object> lista = new ArrayList<Object>();

				for (Operacao operacao : colecaoOperacao) {
					lista.add(new Object[] {
											operacao.getChavePrimaria(), operacao.getDescricao()});
				}

				for (Object obj : lista) {
					Object[] dadosMenu = (Object[]) obj;
					dados.put(String.valueOf(dadosMenu[CONSTANTE_NUMERO_ZERO]), String.valueOf(dadosMenu[1]));
				}
			}
		}
		return dados;
	}

	/**
	 * Método responsável por obter as informaçoes
	 * do medidor instalado no ponto de consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Object> obterMedidorPontoConsumo(Long idPontoConsumo) throws GGASException {

		return fachada.obterInformacoesMedidor(idPontoConsumo);

	}

	/**
	 * Método responsável por carregar as
	 * informações da alçada.
	 *
	 * @param descricaoPapel the descricao papel
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> obterDetalhesAlcada(String descricaoPapel, String dataInicial, String dataFinal) throws GGASException {

		List<String[]> retorno = new ArrayList<String[]>();
		Date dataInicialAlcada = Util.converterCampoStringParaData("Data Inicial", dataInicial, FORMATO_DATA_BR);
		Date dataFinalAlcada = Util.converterCampoStringParaData("Data Final", dataFinal, FORMATO_DATA_BR);

		Map<String, Object> filtro = this.prepararFiltroAlcada(descricaoPapel, dataInicialAlcada, dataFinalAlcada);

		Collection<Alcada> listaAlcadas = fachada.obterlistaAlcada(filtro);

		if (listaAlcadas != null) {
			for (Alcada alcada : listaAlcadas) {
				retorno.add(new String[] {
											alcada.getColuna().getDescricao(), String.valueOf(alcada.getValorInicialFormatado()),
											String.valueOf(alcada.getValorFinalFormatado())});
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por montar o filtro a
	 * ser usado na busca da alçada.
	 *
	 * @param descricaoPapel the descricao papel
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the map
	 */
	private Map<String, Object> prepararFiltroAlcada(String descricaoPapel, Date dataInicial, Date dataFinal) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(descricaoPapel)) {
			filtro.put("descricaoPapel", descricaoPapel);
		}

		if (dataInicial != null) {
			filtro.put("dataInicialAlcada", dataInicial);
		}

		if (dataFinal != null) {
			filtro.put("dataFinalAlcada", dataFinal);
		}

		return filtro;
	}

	/**
	 * Obter lista autorizacoes pendentes.
	 *
	 * @param idUsuario the id usuario
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> obterListaAutorizacoesPendentes(Long idUsuario) throws GGASException {

		List<String[]> retorno = new ArrayList<String[]>();

		Usuario usuario = fachada.obterUsuario(idUsuario);
		AutorizacoesPendentesVO pendentes = fachada.obterRegistrosPendentesPorUsuario(usuario, Boolean.FALSE);

		// Montando item para os créditos e
		// débitos pendentes
		if (pendentes.getCreditoDebitos() != null && !pendentes.getCreditoDebitos().isEmpty()) {
			String chaves = Util.collectionParaStringChavesPrimarias(pendentes.getCreditoDebitos(), ",");
			retorno.add(new String[] {
										String.valueOf(pendentes.getCreditoDebitos().size()), "Crédito(s) e Débito(s)",
										"exibirCreditoDebito", chaves, "", ""});
		}

		// Montando item para os parcelamentos
		// pendentes
		if (pendentes.getParcelamentos() != null && !pendentes.getParcelamentos().isEmpty()) {
			Date dataInicio = Calendar.getInstance().getTime();
			Date dataFim = Calendar.getInstance().getTime();

			for (Parcelamento pendente : pendentes.getParcelamentos()) {
				if (dataInicio.after(pendente.getDataParcelamento())) {
					dataInicio = pendente.getDataParcelamento();
				}
				if (dataFim.before(pendente.getDataParcelamento())) {
					dataFim = pendente.getDataParcelamento();
				}
			}

			String chaves = Util.collectionParaStringChavesPrimarias(pendentes.getParcelamentos(), ",");
			retorno.add(new String[] {
										String.valueOf(pendentes.getParcelamentos().size()), "Parcelamento(s)", "exibirParcelamento",
										chaves, Util.converterDataParaStringSemHora(dataInicio, FORMATO_DATA_BR),
										Util.converterDataParaStringSemHora(dataFim, FORMATO_DATA_BR)});
		}

		// Montando item para as tarifas
		if (pendentes.getTarifa() != null && !pendentes.getTarifa().isEmpty()) {
			String chaves = Util.collectionParaStringChavesPrimarias(pendentes.getTarifa(), ",");
			retorno.add(new String[] {
										String.valueOf(pendentes.getTarifa().size()), "Tarifa(s)", "exibirTarifa", chaves, "", ""});
		}

		// Montando item para as medições diárias do supervisório
		if (pendentes.getSupervisorioMedicoesDiaria() != null && !pendentes.getSupervisorioMedicoesDiaria().isEmpty()) {
			String chaves = Util.collectionParaStringChavesPrimarias(pendentes.getSupervisorioMedicoesDiaria(), ",");
			retorno.add(new String[] {
										String.valueOf(pendentes.getSupervisorioMedicoesDiaria().size()),
										"Medição(ões) diária(s) do supervisório", "exibirSupervisorioMedicaoDiaria", chaves, "", ""});
		}

		// Montando item para as medições diárias do supervisório
		if (pendentes.getSupervisorioMedicoesHoraria() != null && !pendentes.getSupervisorioMedicoesHoraria().isEmpty()) {
			String chaves = Util.collectionParaStringChavesPrimarias(pendentes.getSupervisorioMedicoesHoraria(), ",");
			retorno.add(new String[] {
										String.valueOf(pendentes.getSupervisorioMedicoesHoraria().size()),
										"Medição(ões) horária(s) do supervisório", "exibirSupervisorioMedicaoHoraria", chaves, "", ""});
		}

		if (pendentes.getSolicitacaoConsumoPontoConsumo() != null && !pendentes.getSolicitacaoConsumoPontoConsumo().isEmpty()) {
			String chaves = null;
			retorno.add(new String[] {
										String.valueOf(pendentes.getSolicitacaoConsumoPontoConsumo().size()), "Solicitacões de Consumo",
										"exibirSolicitacaoConsumo", chaves, "", ""});
		}

		return retorno;
	}

	/**
	 * Método responsável por verificar se houve substituicao de medidor.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param leitura the leitura
	 * @param data the data
	 * @return the boolean
	 * @throws ParseException {@link ParseException}
	 * @throws GGASException {@link GGASException}
	 * @throws NumberFormatException {@link NumberFormatException}
	 */
	public Boolean verificarViradaMedidor(Long idPontoConsumo, String leitura, String data) 
				throws NumberFormatException, GGASException, ParseException {

		SimpleDateFormat df = new SimpleDateFormat(FORMATO_DATA_BR);

		return fachada.verificarViradaMedidor(idPontoConsumo, BigDecimal.valueOf(Double.valueOf(leitura)), df.parse(data));
	}

	/**
	 * Possui alcada autorizacao tarifa vigencia.
	 *
	 * @param idUsuario the id usuario
	 * @param idTarifaVigencia the id tarifa vigencia
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, Boolean> possuiAlcadaAutorizacaoTarifaVigencia(Long idUsuario, Long idTarifaVigencia) throws GGASException {

		Map<String, Boolean> dados = new LinkedHashMap<String, Boolean>();

		if (idUsuario != null && idTarifaVigencia != null) {
			dados.put("isUsuarioAutorizado",
							fachada.possuiAlcadaAutorizacaoTarifaVigencia(fachada.obterUsuario(idUsuario),
											fachada.obterTarifaVigencia(idTarifaVigencia)));
		}

		return dados;
	}

	/**
	 * Consultar ponto consumo por rota.
	 *
	 * @param idRota the id rota
	 * @return the map
	 * @throws GGASException - {@link GGASException}
	 */
	public Map<String, String> consultarPontoConsumoPorRota(Long idRota) throws GGASException {

		Rota rota = fachada.criarRota();
		rota.setChavePrimaria(idRota);

		Map<String, String> dados = new HashMap<String, String>();
		Collection<PontoConsumo> listaPontoConsumo = fachada.consultarPontoConsumoPorRota(rota);

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			for (PontoConsumo ponto : listaPontoConsumo) {
				dados.put(String.valueOf(ponto.getChavePrimaria()), ponto.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Método responsável por listar os setores comerciais a partir da localidade selecionada.
	 *
	 * @param idLocalidade
	 *            Chave primária de uma localidade
	 * @return Um mapa de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarSetoresComerciaisPorLocalidade(Long idLocalidade) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_LOCALIDADE, idLocalidade);

		Collection<SetorComercial> setoresComerciais = fachada.listarSetoresComerciaisPorLocalidade(filtro);
		if (setoresComerciais != null && !setoresComerciais.isEmpty()) {
			for (SetorComercial setor : setoresComerciais) {
				dados.put(String.valueOf(setor.getChavePrimaria()), setor.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Alterar status registro supervisorio diario.
	 *
	 * @param idSupervisorioMedicaoDiaria the id supervisorio medicao diaria
	 * @param nomeCampoAlterado the nome campo alterado
	 * @param valorCampoAlterado the valor campo alterado
	 * @param codigoPontoConsumoSupervisorio the codigo ponto consumo supervisorio
	 * @return the map
	 * @throws Exception the exception
	 */
	public Map<String, String> alterarStatusRegistroSupervisorioDiario(Long idSupervisorioMedicaoDiaria, String nomeCampoAlterado,
					String valorCampoAlterado, Boolean codigoPontoConsumoSupervisorio) throws GGASException {

		HttpSession sessao = WebContextFactory.get().getSession();

		Map<String, String> dados = new HashMap<String, String>();
		dados.put("nomeCampoAlterado", nomeCampoAlterado);
		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoDiariaVO =
						(List<SupervisorioMedicaoVO>) sessao.getAttribute("listaSupervisorioMedicaoDiariaVO");
		try {
			for (SupervisorioMedicaoVO supervisorioMedicaoDiariaVO : listaSupervisorioMedicaoDiariaVO) {

				if (supervisorioMedicaoDiariaVO.getChavePrimaria() != null
								&& supervisorioMedicaoDiariaVO.getChavePrimaria().equals(idSupervisorioMedicaoDiaria)) {

					Field field = supervisorioMedicaoDiariaVO.getClass().getDeclaredField(nomeCampoAlterado);
					field.setAccessible(true);

					if (valorCampoAlterado != null && "dataRealizacaoLeitura".equalsIgnoreCase(nomeCampoAlterado)) {
						try {
							Date data = Util.converterCampoStringParaData("Data", valorCampoAlterado, FORMATO_DATA_BR);
							field.set(supervisorioMedicaoDiariaVO, data);
						} catch (GGASException e) {
							LOG.error(e.getStackTrace(), e);
							field.set(supervisorioMedicaoDiariaVO, null);
						}

					} else if (valorCampoAlterado != null && "dataReferencia".equalsIgnoreCase(nomeCampoAlterado)) {
						if (valorCampoAlterado.length() == 9 && !"__/____-_".equals(valorCampoAlterado)) {

							try {
								String anoMes = Util.converterMesAnoEmAnoMes(valorCampoAlterado.substring(CONSTANTE_NUMERO_ZERO, valorCampoAlterado.length() - 2));
								String ciclo = valorCampoAlterado.substring(valorCampoAlterado.length() - 1);
								Integer anoMesInteger = Util.converterCampoStringParaValorAnoMes("dataRealizacaoLeitura", anoMes);
								field.set(supervisorioMedicaoDiariaVO, anoMesInteger);

								Field fieldAux = supervisorioMedicaoDiariaVO.getClass().getDeclaredField("numeroCiclo");
								fieldAux.setAccessible(true);
								fieldAux.set(supervisorioMedicaoDiariaVO, Integer.valueOf(ciclo));
								fieldAux.setAccessible(false);

							} catch (Exception e) {
								LOG.error(e.getStackTrace(), e);
								field.set(supervisorioMedicaoDiariaVO, null);

								Field fieldAux = supervisorioMedicaoDiariaVO.getClass().getDeclaredField("numeroCiclo");
								fieldAux.setAccessible(true);
								fieldAux.set(supervisorioMedicaoDiariaVO, null);
								fieldAux.setAccessible(false);
							}
						} else if ("__/____-_".equals(valorCampoAlterado)) {

							field.set(supervisorioMedicaoDiariaVO, null);

							Field fieldAux = supervisorioMedicaoDiariaVO.getClass().getDeclaredField("numeroCiclo");
							fieldAux.setAccessible(true);
							fieldAux.set(supervisorioMedicaoDiariaVO, null);
							fieldAux.setAccessible(false);
						}

					} else {

						BigDecimal valor = null;

						if (valorCampoAlterado != null && !valorCampoAlterado.isEmpty()) {
							try {
								valor = Util.converterCampoStringParaValorBigDecimal(nomeCampoAlterado,
										valorCampoAlterado, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
								valor = valor.setScale(8, BigDecimal.ROUND_HALF_UP);
							} catch (GGASException e) {
								LOG.error(e.getStackTrace(), e);
							}
						}

						field.set(supervisorioMedicaoDiariaVO, valor);
					}

					field.setAccessible(false);

					List<Long> registrosAlterados = (List<Long>) sessao.getAttribute("registrosAlterados");
					if (!registrosAlterados.contains(idSupervisorioMedicaoDiaria)) {
						registrosAlterados.add(idSupervisorioMedicaoDiaria);
					}
					sessao.setAttribute("registrosAlterados", registrosAlterados);
					List<Long> registrosSalvos = (List<Long>) sessao.getAttribute("registrosSalvos");
					if (registrosSalvos != null && registrosSalvos.contains(idSupervisorioMedicaoDiaria)) {
						registrosSalvos.remove(idSupervisorioMedicaoDiaria);
						sessao.setAttribute("registrosSalvos", registrosSalvos);
					}
					break;
				}

			}
		}catch (IllegalAccessException  e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(e);
		}catch (NoSuchFieldException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(e);
		}
		return dados;
	}

	/**
	 * Alterar status registro supervisorio horario.
	 *
	 * @param idSupervisorioMedicaoDiaria the id supervisorio medicao diaria
	 * @param nomeCampoAlterado the nome campo alterado
	 * @param valorCampoAlterado the valor campo alterado
	 * @param dataHoraRealizacaoLeitura the data hora realizacao leitura
	 * @return the map
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> alterarStatusRegistroSupervisorioHorario(Long idSupervisorioMedicaoDiaria, String nomeCampoAlterado,
					String valorCampoAlterado, String dataHoraRealizacaoLeitura) throws Exception{

		HttpSession sessao = WebContextFactory.get().getSession();

		Map<String, String> dados = new HashMap<String, String>();
		dados.put("nomeCampoAlterado", nomeCampoAlterado);

		List<SupervisorioMedicaoVO> listaSupervisorioMedicaoHorariaVO =
						(List<SupervisorioMedicaoVO>) sessao.getAttribute("listaSupervisorioMedicaoHorariaVO");

		for (SupervisorioMedicaoVO supervisorioMedicaoHorariaVO : listaSupervisorioMedicaoHorariaVO) {

			if (supervisorioMedicaoHorariaVO.getChavePrimaria() != null
							&& supervisorioMedicaoHorariaVO.getChavePrimaria().equals(idSupervisorioMedicaoDiaria)) {

				Field field = null;

				if (valorCampoAlterado != null && "dataRealizacaoLeitura".equalsIgnoreCase(nomeCampoAlterado)) {
					field = supervisorioMedicaoHorariaVO.getClass().getDeclaredField(nomeCampoAlterado);
					field.setAccessible(true);
					Date data = null;
					try {
						if (dataHoraRealizacaoLeitura == null || dataHoraRealizacaoLeitura.isEmpty()
										|| "__:__".equals(dataHoraRealizacaoLeitura)) {
							data = Util.converterCampoStringParaData("Data", valorCampoAlterado, FORMATO_DATA_BR);
						} else if (!dataHoraRealizacaoLeitura.isEmpty()) {
							data =
											Util.converterCampoStringParaData("Data", valorCampoAlterado + " " + dataHoraRealizacaoLeitura,
															Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
						} else {
							data = Util.converterCampoStringParaData("Data", valorCampoAlterado, FORMATO_DATA_BR);
						}
						field.set(supervisorioMedicaoHorariaVO, data);
					} catch (GGASException e) {
						LOG.error(e.getStackTrace(), e);
						field.set(supervisorioMedicaoHorariaVO, null);
					}
				} else if (valorCampoAlterado != null && "horaRealizacaoLeitura".equalsIgnoreCase(nomeCampoAlterado)) {

					try {

						Util.validarHora(valorCampoAlterado, "Hora");
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
						throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS_SUPERVISORIO);
					}

					field = supervisorioMedicaoHorariaVO.getClass().getDeclaredField("dataRealizacaoLeitura");
					field.setAccessible(true);
					Date data =
									Util.converterCampoStringParaData("Data", dataHoraRealizacaoLeitura + " " + valorCampoAlterado,
													Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
					field.set(supervisorioMedicaoHorariaVO, data);
				} else if (valorCampoAlterado != null) {
					field = supervisorioMedicaoHorariaVO.getClass().getDeclaredField(nomeCampoAlterado);
					field.setAccessible(true);
					BigDecimal valor = BigDecimal.ZERO;

					if (!valorCampoAlterado.isEmpty()) {
						try {
							valor =
											Util.converterCampoStringParaValorBigDecimal(nomeCampoAlterado, valorCampoAlterado,
															Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
						} catch (GGASException e) {
							LOG.error(e.getStackTrace(), e);
						}
					}

					field.set(supervisorioMedicaoHorariaVO, valor.setScale(8, BigDecimal.ROUND_HALF_UP));
				}

				if (field != null) {
					field.setAccessible(false);
				}
				List<Long> registrosHorarioAlterados = (List<Long>) sessao.getAttribute("registrosAlterados");
				registrosHorarioAlterados.add(idSupervisorioMedicaoDiaria);
				sessao.setAttribute("registrosAlterados", registrosHorarioAlterados);
				List<Long> registrosSalvos = (List<Long>) sessao.getAttribute("registrosSalvos");
				if (registrosSalvos != null && registrosSalvos.contains(idSupervisorioMedicaoDiaria)) {
					registrosSalvos.remove(idSupervisorioMedicaoDiaria);
					sessao.setAttribute("registrosSalvos", registrosSalvos);
				}

				break;
			}

		}

		return dados;
	}

	/**
	 * Método responsável por listar as funcoes
	 * associadas a um sistema integrante.
	 *
	 * @param idIntegracaoSistema
	 *            Chave primária de um Sistema Integrante
	 * @return Um mapa de String
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> listarFuncaoPorSistema(Long idIntegracaoSistema) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<IntegracaoSistemaFuncao> funcoes = fachada.listarFuncaoPorSistema(idIntegracaoSistema);
		if (funcoes != null && !funcoes.isEmpty()) {
			for (IntegracaoSistemaFuncao funcao : funcoes) {
				dados.put(String.valueOf(funcao.getChavePrimaria()), funcao.getIntegracaoFuncao().getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Alterar acompanhamento integracao.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param nomeCampoAlterado the nome campo alterado
	 * @param valorCampoAlterado the valor campo alterado
	 * @param tipoColuna the tipo coluna
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void alterarAcompanhamentoIntegracao(Long chavePrimaria, String nomeCampoAlterado, String valorCampoAlterado, String tipoColuna)
					throws Exception {

		HttpSession sessao = WebContextFactory.get().getSession();

		List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO =
						(List<AcompanhamentoIntegracaoVO[]>) sessao.getAttribute("listaTabela");
		Class<?> tipo = null;
		if (tipoColuna != null && tipoColuna.contains("class ")) {
			tipo = Class.forName(tipoColuna.substring(CONSTANTE_NUMERO_SEIS));
		} else {
			tipo = loadIt(tipoColuna);
		}
		for (AcompanhamentoIntegracaoVO[] acompanhamentoIntegracaoVO : listaAcompanhamentoIntegracaoVO) {

			if (acompanhamentoIntegracaoVO[0].getValorColuna() != null
							&& acompanhamentoIntegracaoVO[0].getValorColuna().equals(chavePrimaria)) {
				for (int i = 0; i < acompanhamentoIntegracaoVO.length; i++) {
					if (acompanhamentoIntegracaoVO[i].getNomePropriedade().equals(nomeCampoAlterado)) {
						acompanhamentoIntegracaoVO[i].setValorColuna(castDinamico(nomeCampoAlterado, valorCampoAlterado, tipo));
					}
				}

				List<Long> registrosAlterados = (List<Long>) sessao.getAttribute("registrosAlterados");
				if (registrosAlterados != null && !registrosAlterados.contains(chavePrimaria)) {
					registrosAlterados.add(chavePrimaria);
					sessao.setAttribute("registrosAlterados", registrosAlterados);
				}
				List<Long> registrosSalvos = (List<Long>) sessao.getAttribute("registrosSalvos");
				if (registrosSalvos != null && registrosSalvos.contains(chavePrimaria)) {
					registrosSalvos.remove(chavePrimaria);
					sessao.setAttribute("registrosSalvos", registrosSalvos);
				}

				break;
			}
		}
	}

	/** The Constant primitiveClasses. */

	static {
		PRIMITIVE_CLASSES.put("byte", byte.class);
		PRIMITIVE_CLASSES.put("short", short.class);
		PRIMITIVE_CLASSES.put("char", char.class);
		PRIMITIVE_CLASSES.put("int", int.class);
		PRIMITIVE_CLASSES.put("long", long.class);
		PRIMITIVE_CLASSES.put("float", float.class);
		PRIMITIVE_CLASSES.put("double", double.class);
		PRIMITIVE_CLASSES.put("boolean", boolean.class);
	}

	/**
	 * Load it.
	 *
	 * @param name the name
	 * @return the class
	 * @throws ClassNotFoundException
	 */
	private Class<?> loadIt(String name) throws ClassNotFoundException {

		if (PRIMITIVE_CLASSES.containsKey(name)) {
			return PRIMITIVE_CLASSES.get(name);
		} else {
			return Class.forName(name);
		}
	}

	/**
	 * Cast dinamico.
	 *
	 * @param nomeCampoAlterado the nome campo alterado
	 * @param valorCampoAlterado the valor campo alterado
	 * @param tipo the tipo
	 * @return the object
	 * @throws GGASException the GGAS exception
	 */
	private Object castDinamico(String nomeCampoAlterado, String valorCampoAlterado, Class<?> tipo) throws GGASException {

		Object retorno = null;
		try {
			if (tipo.equals(Long.class)) {
				retorno = Long.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Date.class)) {
				retorno =
								Util.converterCampoStringParaData(nomeCampoAlterado, valorCampoAlterado,
												Constantes.FORMATO_DATA_HORA_US_COMPLETA);
			} else if (tipo.equals(Boolean.class)) {
				retorno = Boolean.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Short.class)) {
				retorno = Short.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Byte.class)) {
				retorno = Byte.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Integer.class)) {
				retorno = Integer.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Double.class)) {
				retorno = Double.valueOf(valorCampoAlterado);
			} else if (tipo.equals(Float.class)) {
				retorno = Float.valueOf(valorCampoAlterado);
			} else if (tipo.equals(BigDecimal.class)) {
				retorno =
								Util.converterCampoStringParaValorBigDecimal(nomeCampoAlterado, valorCampoAlterado,
												Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			} else if (tipo.equals(int.class)) {
				retorno = Integer.valueOf(valorCampoAlterado);
			} else if (tipo.equals(long.class)) {
				retorno = Long.valueOf(valorCampoAlterado);
			} else if (tipo.equals(double.class)) {
				retorno = Double.valueOf(valorCampoAlterado);
			} else if (tipo.equals(float.class)) {
				retorno = Float.valueOf(valorCampoAlterado);
			} else if (tipo.equals(short.class)) {
				retorno = Short.valueOf(valorCampoAlterado);
			} else if (tipo.equals(byte.class)) {
				retorno = Byte.valueOf(valorCampoAlterado);
			} else if (tipo.equals(boolean.class)) {
				retorno = Boolean.valueOf(valorCampoAlterado);
			} else {
				retorno = valorCampoAlterado;
			}
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
		}
		return retorno;
	}

	/**
	 * Atualizar filtro pesquisa acompanhemento tabela integracao.
	 *
	 * @param campo the campo
	 * @param valor the valor
	 * @param valor2 the valor2
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public void atualizarFiltroPesquisaAcompanhementoTabelaIntegracao(String campo, String valor, String valor2) {

		HttpSession sessao = WebContextFactory.get().getSession();

		List<AcompanhamentoIntegracaoVO> listaFiltros = (List<AcompanhamentoIntegracaoVO>) sessao.getAttribute("listaFiltros");

		for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : listaFiltros) {

			if (acompanhamentoIntegracaoVO.getNomeColuna() != null && acompanhamentoIntegracaoVO.getNomeColuna().equals(campo)) {

				if (valor != null) {
					acompanhamentoIntegracaoVO.setValorColuna(valor);
				}
				if (valor2 != null) {
					acompanhamentoIntegracaoVO.setValorColuna2(valor2);
				}

				break;
			}
		}

		WebContextFactory.get().getSession().setAttribute("listaFiltros", listaFiltros);
	}

	/**
	 * Listar colunas tabela.
	 *
	 * @param idTabela the id tabela
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarColunasTabela(Long idTabela) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<Coluna> colunas = fachada.listarColunasTabelaVariavel(idTabela);
		if (colunas != null && !colunas.isEmpty()) {
			for (Coluna coluna : colunas) {
				dados.put(String.valueOf(coluna.getChavePrimaria()), coluna.getCodigoVariavel());
			}
		}

		return dados;
	}

	/**
	 * Listar rotas por setor comercial da quadra.
	 *
	 * @param idQuadra the id quadra
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<Long, String> listarRotasPorSetorComercialDaQuadra(Long idQuadra) throws NegocioException {

		Map<Long, String> dados = new HashMap<Long, String>();

		Collection<Rota> rotas = fachada.listarRotasPorSetorComercialDaQuadra(idQuadra);

		if (rotas != null && !rotas.isEmpty()) {
			for (Rota rota : rotas) {
				dados.put(rota.getChavePrimaria(), rota.getNumeroRota());
			}
		}

		return dados;
	}

	/**
	 * Listar modulos.
	 *
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<Long, String> listarModulos() throws NegocioException {

		Map<Long, String> dados = new HashMap<Long, String>();

		Collection<Menu> modulos = fachada.listarModulos(new HashMap<String, Object>());

		if (modulos != null && !modulos.isEmpty()) {
			for (Menu modulo : modulos) {
				dados.put(modulo.getChavePrimaria(), modulo.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Listar funcionalidades por modulo.
	 *
	 * @param idModuloPai the id modulo pai
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<Long, String> listarFuncionalidadesPorModulo(Long idModuloPai) throws NegocioException {

		Map<Long, String> dados = new HashMap<Long, String>();

		Collection<Menu> modulos = fachada.listarFuncionalidadesPorModulo(idModuloPai);

		if (modulos != null && !modulos.isEmpty()) {
			for (Menu modulo : modulos) {
				dados.put(modulo.getChavePrimaria(), modulo.getDescricao());
			}
		}

		return dados;
	}

	/**
	 * Listar operacoes por funcionalidade.
	 *
	 * @param idFuncionalidade the id funcionalidade
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	public String listarOperacoesPorFuncionalidade(Long idFuncionalidade) throws NegocioException {

		Gson gson = new Gson();

		Collection<Operacao> operacoes = fachada.listarOperacoesPorFuncionalidade(idFuncionalidade);

		if (operacoes != null && !operacoes.isEmpty()) {
			OperacaoItemVO[] dados = new OperacaoItemVO[operacoes.size()];
			int x = CONSTANTE_NUMERO_ZERO;
			for (Operacao operacao : operacoes) {

				OperacaoItemVO operacaoItemVO = new OperacaoItemVO();

				operacaoItemVO.setIdMenu(operacao.getMenu().getChavePrimaria());
				operacaoItemVO.setIdOperacaoSistema(operacao.getChavePrimaria());
				operacaoItemVO.setDescricaoOperacao(operacao.getDescricao());
				operacaoItemVO.setIsSelecionado(false);

				dados[x] = operacaoItemVO;
				x++;
			}
			return gson.toJson(dados);
		}
		return "";

	}

	/**
	 * Preencher operacoes em funcionalidade arvore permissao.
	 *
	 * @param funcionalidades the funcionalidades
	 * @throws NegocioException the negocio exception
	 */
	private void preencherOperacoesEmFuncionalidadeArvorePermissao(FuncionalidadeVO[] funcionalidades) throws NegocioException {

		for (int x = 0; x < funcionalidades.length; x++) {
			FuncionalidadeVO currentFuncionalidade = funcionalidades[x];

			List<Operacao> operacaoList = (List<Operacao>) fachada.listarOperacoesPorFuncionalidade(currentFuncionalidade.getId());

			OperacaoItemVO[] operacoesVO = new OperacaoItemVO[operacaoList.size()];
			for (int y = 0; y < operacaoList.size(); y++) {
				Operacao currentOperacao = operacaoList.get(y);

				OperacaoItemVO operacaoItemVO = new OperacaoItemVO();

				operacaoItemVO.setDescricaoOperacao(currentOperacao.getDescricao());
				operacaoItemVO.setIdMenu(currentOperacao.getMenu().getChavePrimaria());
				operacaoItemVO.setIdOperacaoSistema(currentOperacao.getChavePrimaria());
				operacaoItemVO.setIdPapel(0L);
				operacaoItemVO.setIsSelecionado(false);

				operacoesVO[y] = operacaoItemVO;
			}

			currentFuncionalidade.setOperacoes(operacoesVO);
		}
	}

	/**
	 * Listar arvore permissao funcionalidades.
	 *
	 * @param idFuncionalidadePai the id funcionalidade pai
	 * @param isRetornaRegistroFilho the is retorna registro filho
	 * @return the funcionalidade v o[]
	 * @throws NegocioException the negocio exception
	 */
	private FuncionalidadeVO[] listarArvorePermissaoFuncionalidades(Long idFuncionalidadePai, boolean isRetornaRegistroFilho)
					throws NegocioException {

		List<Menu> fucionalidades = (List<Menu>) fachada.listarFuncionalidadesPorModulo(idFuncionalidadePai);
		FuncionalidadeVO[] funcionalidadeListVO = new FuncionalidadeVO[fucionalidades.size()];

		for (int x = 0; x < fucionalidades.size(); x++) {
			FuncionalidadeVO funcionalidadeVO = new FuncionalidadeVO();

			funcionalidadeVO.setId(fucionalidades.get(x).getChavePrimaria());
			funcionalidadeVO.setDescricao(fucionalidades.get(x).getDescricao());

			FuncionalidadeVO[] subItens = new FuncionalidadeVO[0];

			if (Boolean.TRUE.equals(isRetornaRegistroFilho)) {
				subItens = listarArvorePermissaoFuncionalidades(fucionalidades.get(x).getChavePrimaria(), false);
			}

			funcionalidadeVO.setOperacoes(new OperacaoItemVO[0]);
			funcionalidadeVO.setSubFuncionalidades(subItens);

			if (subItens.length > 0) {
				preencherOperacoesEmFuncionalidadeArvorePermissao(subItens);
			} else {
				funcionalidadeVO.setDescricao(fucionalidades.get(x).getDescricao());
				funcionalidadeVO.setId(fucionalidades.get(x).getChavePrimaria());

				preencherOperacoesEmFuncionalidadeArvorePermissao(new FuncionalidadeVO[] {funcionalidadeVO});
			}

			funcionalidadeListVO[x] = funcionalidadeVO;
		}

		return funcionalidadeListVO;
	}

	/**
	 * Listar arvore permissao completa.
	 *
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	public String listarArvorePermissaoCompleta() throws NegocioException {

		Gson gson = new Gson();

		List<Menu> modulos = (List<Menu>) fachada.listarModulos(new HashMap<String, Object>());

		if (modulos != null && !modulos.isEmpty()) {
			ModuloVO[] listaModulosVO = new ModuloVO[modulos.size()];
			for (int x = 0; x < modulos.size(); x++) {
				Menu currentItem = modulos.get(x);
				ModuloVO moduloVO = new ModuloVO();

				moduloVO.setId(currentItem.getChavePrimaria());
				moduloVO.setDescricao(currentItem.getDescricao());
				moduloVO.setListaFuncionalidade(listarArvorePermissaoFuncionalidades(currentItem.getChavePrimaria(), true));

				listaModulosVO[x] = moduloVO;
			}
			return gson.toJson(listaModulosVO);
		}
		return "";

	}

	/**
	 * Consultar aliquota rubrica.
	 *
	 * @param chaveRubrica the chave rubrica
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean consultarAliquotaRubrica(Long chaveRubrica) throws NegocioException {

		return fachada.tributoPossuiAliquota(chaveRubrica);
	}

	/**
	 * Obter id constante icms.
	 *
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	public Long obterIdConstanteIcms() throws GGASException {

		ConstanteSistema constanteSistema = fachada.obterConstantePorCodigo(Constantes.C_ICMS);

		if (constanteSistema == null) {
			throw new GGASException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, Constantes.C_ICMS);
		}

		return Long.parseLong(constanteSistema.getValor());
	}

	/**
	 * Remove um item da lista de favoritos do usuário.
	 *
	 * @param idFavorito A chave primária do item favorito.
	 * @throws GGASException the GGAS exception
	 */
	public void removerFavorito(Long idFavorito) throws GGASException {

		Favoritos favorito = fachada.obterFavorito(idFavorito);
		fachada.removerFavorito(favorito);
	}

	/**
	 * Adiciona um item na lista de favoritos do usuário.
	 *
	 * @param idMenu the id menu
	 * @param request the request
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public String adicionarFavorito(Long idMenu, HttpServletRequest request) throws GGASException {

		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		Favoritos favorito = fachada.criarFavoritos();
		Menu menu = fachada.obterMenu(idMenu);
		favorito.setUsuario(usuario);
		favorito.setMenu(menu);

		return fachada.adicionarFavorito(favorito);

	}

	/**
	 * Validar duracao dias.
	 *
	 * @param idAtividade the id atividade
	 * @param duracao the duracao
	 * @param listaDatasAtividadesIniciais the lista datas atividades iniciais
	 * @param dataLimite the data limite
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<String, Boolean>
					validarDuracaoDias(Long idAtividade, String duracao, String listaDatasAtividadesIniciais, String dataLimite)
									throws NegocioException {

		Map<String, Boolean> dados = new HashMap<String, Boolean>();
		Boolean lancouExcecao = Boolean.FALSE;

		if (!StringUtils.isEmpty(duracao) && !StringUtils.isEmpty(dataLimite)) {

			String dataInicio = null;
			if (listaDatasAtividadesIniciais.contains(",")) {

				String[] arrayDataInicio = listaDatasAtividadesIniciais.split(",");
				dataInicio = arrayDataInicio[(int) (idAtividade - 1)];
			} else {

				dataInicio = listaDatasAtividadesIniciais;
			}

			try {

				fachada.validarDuracaoDias(Integer.parseInt(duracao),
								Util.converterCampoStringParaData("dataInicio", dataInicio, FORMATO_DATA_BR),
								Util.converterCampoStringParaData("dataLimite", dataLimite, FORMATO_DATA_BR));
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				lancouExcecao = Boolean.TRUE;
			}
		}

		dados.put("lancouExcecao", lancouExcecao);

		return dados;
	}

	/**
	 * Listar rotas por imovel por setor comercial da quadra.
	 *
	 * @param idImovel the id imovel
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<Long, String> listarRotasPorImovelPorSetorComercialDaQuadra(Long idImovel) throws GGASException {

		Imovel imovel = fachada.buscarImovelPorChave(idImovel);

		Collection<Rota> rotas = fachada.listarRotasPorSetorComercialDaQuadra(imovel.getQuadraFace().getQuadra().getChavePrimaria());

		Map<Long, String> resultado = new HashMap<Long, String>();

		for (Rota currentRota : rotas) {
			resultado.put(currentRota.getChavePrimaria(), currentRota.getNumeroRota());
		}

		return resultado;
	}

	/**
	 * Obter lista atualizacao cadastral.
	 *
	 * @param atualizaoCadastral the atualizao cadastral
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterListaAtualizacaoCadastral(String atualizaoCadastral) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		ConstanteSistema constanteSistema = fachada.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TELA_ATUALIZACAO);
		Collection<EntidadeConteudo> listaEntidadeConteudo = fachada.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));

		String cliente = atualizaoCadastral.split(";")[0];
		String contrato = atualizaoCadastral.split(";")[1];

		List<Object> lista = new ArrayList<Object>();

		for (EntidadeConteudo e : listaEntidadeConteudo) {
			if (e.getDescricao().contains("Tela Cliente")) {
				if (!"false".equals(cliente)) {
					lista.add(new Object[] {
											e.getChavePrimaria(), e.getDescricao()});
				}
			} else if (e.getDescricao().contains("Tela Contrato")) {
				if (!"false".equals(contrato)) {
					lista.add(new Object[] {
											e.getChavePrimaria(), e.getDescricao()});
				}
			} else {
				lista.add(new Object[] {
										e.getChavePrimaria(), e.getDescricao()});
			}
		}

		for (Object obj : lista) {
			Object[] dadosMenu = (Object[]) obj;
			dados.put(String.valueOf(dadosMenu[0]), String.valueOf(dadosMenu[1]));
		}

		return dados;
	}

	/**
	 * Obter lista servicos.
	 *
	 * @param listaServicos the lista servicos
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterListaServicos(String listaServicos) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		String cliente = listaServicos.split(";")[0];
		String imovel = listaServicos.split(";")[1];

		ServicoTipo servicoTipo = new ServicoTipo();
		if ("true".equals(cliente)) {
			servicoTipo.setIndicadorClienteObrigatorio(true);
		} else {
			servicoTipo.setIndicadorClienteObrigatorio(false);
		}
		if ("true".equals(imovel)) {
			servicoTipo.setIndicadorImovelObrigatorio(true);
		} else {
			servicoTipo.setIndicadorImovelObrigatorio(false);
		}

		ControladorServicoTipo controladorServicoTipo = ServiceLocator.getInstancia().getControladorServicoTipo();
		Collection<EntidadeNegocio> listaServicoTipo = controladorServicoTipo.consultarServicoTipo(servicoTipo, true);

		for (EntidadeNegocio servico : listaServicoTipo) {
			dados.put(String.valueOf(servico.getChavePrimaria()), servico.getLabel());
		}

		return dados;
	}

	/**
	 * Calcula Cromatografia
	 * informada.
	 *
	 * @param dados the dados
	 * @param nomeComponentes the nome componentes
	 * @return componentes mais o fator de compressibilidade.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> calcularCromatografia(String dados, String nomeComponentes) throws GGASException {

		String[] componentesArray = new String[dados.split(";").length];

		// Separa a String em um array
		for (int i = 0; i < dados.split(";").length; i++) {
			componentesArray[i] = dados.split(";")[i];
		}

		// Recupera a pressao e a temperatura do array montado no javaScript
		double pressao = 0;
		double temperatura = 0;
		if (componentesArray[21].contains(",")) {
			pressao = Double.parseDouble(componentesArray[21].replace(',', '.'));
		} else {
			pressao = Double.parseDouble(componentesArray[21]);
		}
		if (componentesArray[22].contains(",")) {
			temperatura = Double.parseDouble(componentesArray[22].replace(',', '.'));
		} else {
			temperatura = Double.parseDouble(componentesArray[22]);
		}

		// Coloca inteiramente o conteudo array de string para um array double tratando o campo trocando a virgula pelo ponto
		double[] componentes = new double[24];
		int count = 0;
		for (String dado : componentesArray) {
			componentes[count] =
							Util.converterCampoStringParaValorBigDecimal(nomeComponentes.split(",")[count], dado,
											Constantes.FORMATO_VALOR_4_DECIMAIS, Constantes.LOCALE_PADRAO).doubleValue();
			count++;
		}

		// calcula o fator e coloca no array
		double fatorCompressibilidade =
						FatorZ.calcular(Util.converterCelciusParaKelvin(temperatura),
										Util.converterQuilogramaCentimetroQuadradoParaPascal(pressao), componentes);
		double fatorCompressibilidadeFormatado = converteValorFatorCrompressibilidade(fatorCompressibilidade);
		componentesArray[23] = String.valueOf(fatorCompressibilidadeFormatado).replace('.', ',');

		// coloca tudo num mapa
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < nomeComponentes.split(",").length; i++) {
			map.put(nomeComponentes.split(",")[i], componentesArray[i]);
		}
		return map;
	}

	/**
	 * Converte valor fator crompressibilidade.
	 *
	 * @param fatorCompressibilidade the fator compressibilidade
	 * @return the double
	 */
	private static double converteValorFatorCrompressibilidade(double fatorCompressibilidade) {

		String stringFator = String.valueOf(fatorCompressibilidade);
		int posicao = stringFator.indexOf('.');
		String decimaisString = stringFator.substring(posicao);
		int qtdDecimais = 0;
		if (decimaisString.length() < 7) {
			qtdDecimais = decimaisString.length();
		} else {
			qtdDecimais = 7;
		}
		String valorDecimal = stringFator.substring(0, posicao) + stringFator.substring(posicao, posicao + qtdDecimais);

		return Double.valueOf(valorDecimal);
	}

	/**
	 * Recupera os tipo de contato de acordo com o tipo da pessoa(Física, Jurídica).
	 *
	 * @param idTipoCliente the id tipo cliente
	 * @return the map
	 * @throws GGASException the GGAS exception
	 * @throws NumberFormatException the number format exception
	 */
	public Map<String, String> obterListaTipoContatoPorTipoPessoa(String idTipoCliente) throws GGASException {

		if ("-1".equals(idTipoCliente)) {
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		Collection<TipoContato> listaTipoContato = fachada.listarTipoContadoPorTipoPessoaDoCliente(idTipoCliente);
		for (TipoContato tipoContato : listaTipoContato) {
			map.put(String.valueOf(tipoContato.getChavePrimaria()), tipoContato.getDescricao());
		}
		return map;
	}

	/**
	 * Calcula Cromatografia
	 * informada.
	 *
	 * @param dataString the data string
	 * @param idCityGate the id city gate
	 * @return componentes mais o fator de compressibilidade.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterPcs(String dataString, String idCityGate) throws GGASException {

		Date data = null;
		if (dataString != null && isDataValida(dataString)) {
			data = Util.converterCampoStringParaData("data", dataString, FORMATO_DATA_BR);
		}

		Map<String, String> map = new HashMap<String, String>();

		CityGate cityGate = fachada.obterCityGate(idCityGate);

		List<Integer> listaPcs = obtemListaPcs(data, cityGate);
		map.put("dataString", dataString);
		map.put(ID_CITY_GATE, idCityGate);
		for (Integer pcs : listaPcs) {
			map.put("pcs", ObjectUtils.toString(pcs));
			return map;
		}
		map.put("pcs", "");
		return map;
	}

	/**
	 * Obtem lista pcs.
	 *
	 * @param data the data
	 * @param cityGate the city gate
	 * @return the list
	 */
	
	private List<Integer> obtemListaPcs(Date data, CityGate cityGate) {

		List<Integer> listaPcs = new ArrayList<Integer>();
		if (cityGate != null) {
			List<CityGateMedicao> listaCityGateMedicao = fachada.consultarCityGateMedicaoPorCityGateEData(cityGate, data);
			for (CityGateMedicao cityGateMedicao : listaCityGateMedicao) {
				listaPcs.add(cityGateMedicao.getMedidaPCSSupridora());
			}
		}
		return listaPcs;
	}

	/**
	 * Listar modelo contrato por tipo contrato.
	 *
	 * @param tipoModelo the tipo modelo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarModeloContratoPorTipoContrato(Long tipoModelo) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (tipoModelo > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(HABILITADO, Boolean.TRUE);
			filtro.put("tipoModelo", tipoModelo);

			Collection<ModeloContrato> modelos = fachada.consultarModelosContrato(filtro);
			if (modelos != null && !modelos.isEmpty()) {
				for (ModeloContrato modelo : modelos) {
					dados.put(String.valueOf(modelo.getChavePrimaria()), modelo.getDescricao());
				}
			}
		}
		return dados;
	}

	/**
	 * Obter tarifas.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chaveItemFatura the chave item fatura
	 * @param chaveModeloContrato the chave modelo contrato
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterTarifas(Long chavePontoConsumo, Long chaveItemFatura, Long chaveModeloContrato) throws GGASException {

		Map<String, String> retorno = new LinkedHashMap<String, String>();

		List<Tarifa> listaTarifa = fachada.obterTarifasPorRamoAtividade(chavePontoConsumo, chaveItemFatura);

		if (listaTarifa == null || listaTarifa.isEmpty()) {
			listaTarifa = fachada.obterTarifas(chavePontoConsumo, chaveItemFatura);
		} else {
			List<Tarifa> listaTarifaSegmento = fachada.obterTarifas(chavePontoConsumo, chaveItemFatura);
			if (listaTarifaSegmento != null && !listaTarifaSegmento.isEmpty()) {
				for (Tarifa tarifa : listaTarifaSegmento) {
					if (!listaTarifa.contains(tarifa)) {
						listaTarifa.add(tarifa);
					}
				}
			}
		}

		ModeloContrato modeloContrato = fachada.obterModeloContrato(chaveModeloContrato);

		if (listaTarifa != null) {

			for (Tarifa tarifa : listaTarifa) {
				if (modeloContrato.getTipoModelo().getChavePrimaria() == tarifa.getTipoContrato().getChavePrimaria()) {
					retorno.put(String.valueOf(tarifa.getChavePrimaria()), tarifa.getDescricao());
				}
			}

		}

		return retorno;
	}

	/**
	 * Checks if is tipo substituto petrobras.
	 *
	 * @return the boolean
	 * @throws GGASException the GGAS exception
	 */
	public Boolean isTipoSubstitutoPetrobras() throws GGASException {

		Boolean isPetrobras = Boolean.FALSE;

		String substituto = (String) fachada.obterValorDoParametroPorCodigo(TIPO_SUBSTITUTO);

		long idSubstituto = 0;
		if (substituto != null) {
			idSubstituto = Long.parseLong(substituto);
		}

		if (idSubstituto == EntidadeConteudo.CHAVE_PETROBRAS) {
			isPetrobras = Boolean.TRUE;
		}
		return isPetrobras;
	}

	/**
	 * Calcular valor mensal gasto.
	 *
	 * @param idTarifa the id tarifa
	 * @param consumo the consumo
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> calcularValorMensalGasto(Long idTarifa, String consumo) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		Long chaveBaseApuracaoDiaria =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TARIFA_BASE_APURACAO_DIARIA));
		Long chaveBaseApuracaoPeriodica =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TARIFA_BASE_APURACAO_PERIODICA));

		Map<String, String> dados = new HashMap<String, String>();
		Collection<TarifaVigencia> listaTarifaVigencia = null;

		Tarifa tarifaSelecionada = null;
		if (idTarifa != null && idTarifa > 0) {
			tarifaSelecionada = fachada.obterTarifa(idTarifa, "itemFatura");
			String consumoFormatado = "";
			StringBuilder builder = new StringBuilder();
			if (!StringUtils.isEmpty(consumo)) {
				for (int i = 0; i < consumo.length(); i++) {
					if (!",".equals(consumo.substring(i, i + 1))) {
						builder.append(consumo.substring(i, i + 1));
					} else {
						break;
					}
				}
				consumoFormatado = builder.toString();
			}
			EntidadeConteudo baseApuracao = null;
			if (tarifaSelecionada != null) {
				listaTarifaVigencia = fachada.listarTarifaVigenciaPorTarifa(tarifaSelecionada.getChavePrimaria());
				if (!CollectionUtils.isEmpty(listaTarifaVigencia)) {
					baseApuracao = listaTarifaVigencia.iterator().next().getBaseApuracao();
				}

				Calendar dataAtual = Calendar.getInstance();
				Calendar ultimaDataMes = Calendar.getInstance();
				BigDecimal consumoMedioMensalOuDiario = new BigDecimal(consumoFormatado);
				if (baseApuracao != null && chaveBaseApuracaoDiaria != null && chaveBaseApuracaoDiaria > 0
								&& chaveBaseApuracaoPeriodica != null && chaveBaseApuracaoPeriodica > 0) {
					if (chaveBaseApuracaoPeriodica == baseApuracao.getChavePrimaria()) {
						// Data primeiro dia do mês
						int primeiroDiaMes = dataAtual.getActualMinimum(Calendar.DAY_OF_MONTH);
						// Data último dia do mês
						int ultimoDiaMes = dataAtual.getActualMaximum(Calendar.DAY_OF_MONTH);
						// data atual passará a ser a primeira data do mês
						dataAtual.set(Calendar.DAY_OF_MONTH, primeiroDiaMes);
						// última data do mês
						ultimaDataMes.set(Calendar.DAY_OF_MONTH, ultimoDiaMes);

					} else if (chaveBaseApuracaoDiaria == baseApuracao.getChavePrimaria()) {
						// Consumo passa a ser diário
						consumoMedioMensalOuDiario = consumoMedioMensalOuDiario.divide(
								new BigDecimal(CONSTANTE_NUMERO_TRINTA), CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_UP);
					}

				}

				DadosFornecimentoGasVO dadosFornecimentoGasSimulado =
								fachada.calcularValorFornecimentoGas(tarifaSelecionada.getItemFatura(), dataAtual.getTime(),
												ultimaDataMes.getTime(), consumoMedioMensalOuDiario, tarifaSelecionada, Boolean.TRUE,
												false, false);

				if (dadosFornecimentoGasSimulado != null) {

					if (chaveBaseApuracaoPeriodica == baseApuracao.getChavePrimaria()) {
						BigDecimal valorTotal = dadosFornecimentoGasSimulado.getValorTotalGas().setScale(CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_UP);
						String valorTotalString = valorTotal.toString();
						valorTotalString = valorTotalString.replace(".", ",");
						dados.put(VALOR_MENSAL_GASTO, valorTotalString);
						dados.put(VALOR_MEDIO_TARIFA, valorTotal.divide(consumoMedioMensalOuDiario, 4, BigDecimal.ROUND_HALF_UP).toString());

					} else if (chaveBaseApuracaoDiaria == baseApuracao.getChavePrimaria()) {
						BigDecimal valorTotal = dadosFornecimentoGasSimulado.getValorTotalGas().setScale(4, RoundingMode.HALF_UP);
						valorTotal = valorTotal.multiply(new BigDecimal(30));
						String valorTotalString = valorTotal.toString();
						consumoMedioMensalOuDiario = consumoMedioMensalOuDiario.multiply(new BigDecimal(30));
						valorTotalString = valorTotalString.replace(".", ",");
						dados.put(VALOR_MENSAL_GASTO, valorTotalString);
						dados.put(VALOR_MEDIO_TARIFA, valorTotal.divide(consumoMedioMensalOuDiario, 4, BigDecimal.ROUND_HALF_UP).toString());
					}
				}
			}
		}
		return dados;
	}

	/**
	 * Obter dados historico rota cronograma.
	 *
	 * @param chaveRota the chave rota
	 * @param chaveCronograma the chave cronograma
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	public List<String[]> obterDadosHistoricoRotaCronograma(Long chaveRota, Long chaveCronograma) throws GGASException {

		ControladorRota controladorRota =
						(ControladorRota) ServiceLocator.getInstancia().getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		List<String[]> dadosHistorico = new ArrayList<String[]>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chaveRota", chaveRota);
		filtro.put("chaveCronograma", chaveCronograma);
		List<RotaHistorico> listaHistorico =
						(ArrayList<RotaHistorico>) controladorRota.obterHistoricoRotaMaisRecente(filtro, Boolean.FALSE);

		Date dataCadastro = Util.primeiroElemento(listaHistorico).getDataCadastro();
		filtro.put("dataCadastro", dataCadastro);
		filtro.remove("chaveCronograma");
		List<RotaHistorico> listaHistoricoRotaSelecionada =
						(ArrayList<RotaHistorico>) controladorRota.obterHistoricoRotaMaisRecente(filtro, null);

		for (RotaHistorico historico : listaHistoricoRotaSelecionada) {
			String[] dadosLinha = new String[CONSTANTE_NUMERO_TRES];
			if (historico.getRota() != null) {
				dadosLinha[0] = historico.getRota().getNumeroRota();
			}

			if (historico.getLeiturista() != null) {
				dadosLinha[1] = historico.getLeiturista().getFuncionario().getNome();
			}

			if (historico.getLeituristaSuplente() != null) {
				dadosLinha[2] = historico.getLeituristaSuplente().getFuncionario().getNome();
			}

			dadosHistorico.add(dadosLinha);
		}

		return dadosHistorico;
	}

	/**
	 * Obter soma qdc contrato.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param dataAvisoCorte the data aviso corte
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	public BigDecimal obterSomaQdcContrato(Long chavePontoConsumo, String dataAvisoCorte) throws GGASException {

		ContratoPontoConsumo contratoPontoConsumo = fachada.consultarContratoPontoConsumoPorPontoConsumo(chavePontoConsumo);
		return fachada.obterSomaQdcContrato(contratoPontoConsumo, DataUtil.converterParaData(dataAvisoCorte));
	}

	/**
	 * Recupera os descontos cujos períodos serão sobrepostos por uma vigência.
	 *
	 * @param chavePrimariaTarifa the chave primaria tarifa
	 * @param chavePrimariaVigencia the chave primaria vigencia
	 * @param dataInicioVigencia the data inicio vigencia
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> montarListaTarifaVigenciaDesconto(Long chavePrimariaTarifa, Long chavePrimariaVigencia,
			String dataInicioVigencia) throws GGASException {

		TarifaVigencia tarifaVigencia = fachada.criarTarifaVigencia();
		Tarifa tarifa = fachada.criarTarifa();

		if (chavePrimariaVigencia != null) {
			tarifaVigencia.setChavePrimaria(chavePrimariaVigencia);
		} else {
			tarifaVigencia.setChavePrimaria(Long.valueOf(0));
		}
		if (chavePrimariaTarifa != null) {
			tarifa.setChavePrimaria(chavePrimariaTarifa);
		} else {
			tarifa.setChavePrimaria(Long.valueOf(0));
		}

		tarifaVigencia.setTarifa(tarifa);
		tarifaVigencia.setDataVigencia(DataUtil.converterParaData(dataInicioVigencia));

		Collection<AcaoSincroniaDesconto> acoes = fachada.acoesParaSincronizacao(tarifaVigencia);

		return CollectionUtils.collect(acoes, input -> {

			AcaoSincroniaDesconto acaoSincroniaDesconto = (AcaoSincroniaDesconto) input;
			TarifaVigenciaDesconto desconto = acaoSincroniaDesconto.getDesconto();
			return desconto.getDescricaoDescontoVigencia() + " [" + desconto.getInicioVigenciaFormatado() + " - "
					+ desconto.getFimVigenciaFormatado() + "]";
		});
	}

	/**
	 *
	 * @param idHistoricoConsumo
	 * @return ConstrutorSerieTemporal
	 * @throws GGASException
	 */
	public Map<Long, BigDecimal> obterSeriePCS(Long idHistoricoConsumo) throws GGASException {

		return ConstrutorSerieTemporal.novoConstrutorSerieTemporalPCS().consultarHistorico(idHistoricoConsumo).criarMapa();
	}

	/**
	 *
	 * @param idHistoricoConsumo
	 * @return ConstrutorSerieTemporal
	 * @throws GGASException
	 */
	public Map<Long, BigDecimal> obterSeriePTZ(Long idHistoricoConsumo) throws GGASException {

		return ConstrutorSerieTemporal.novoConstrutorSerieTemporalPTZ().consultarHistorico(idHistoricoConsumo).criarMapa();
	}

	/**
	 * Método responsável por consultar as microrregiões.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarMicrorregioes() throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<Microrregiao> microrregioes = fachada.listarMicrorregioes();

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Microrregiao microrregiao : microrregioes) {
			sortedMap.put(microrregiao.getDescricao(), String.valueOf(microrregiao.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as microrregiões de uma região.
	 *
	 * @param chaveRegiao the chave região
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMicrorregioesPorRegiao(String chaveRegiao) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		List<Microrregiao> microrregioes = fachada.obterMicrorregioesPorRegiao(Long.valueOf(chaveRegiao));

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Microrregiao microrregiao : microrregioes) {
			sortedMap.put(microrregiao.getDescricao(), String.valueOf(microrregiao.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as regiões de uma unidade federação.
	 *
	 * @param chaveUnidadeFederacao the chave unidade federação
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarMicrorregioesPorUnidadeFederacao(String chaveUnidadeFederacao) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		List<Microrregiao> microrregioes = fachada.obterMicrorregioesPorUnidadeFederacao(Long.valueOf(chaveUnidadeFederacao));

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Microrregiao microrregiao : microrregioes) {
			sortedMap.put(microrregiao.getDescricao(), String.valueOf(microrregiao.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as regiões.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarRegioes() throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<Regiao> regioes = fachada.listarRegioes();

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Regiao regiao : regioes) {
			sortedMap.put(regiao.getDescricao(), String.valueOf(regiao.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as regiões de uma unidade federação.
	 *
	 * @param chaveUnidadeFederacao the chave unidade federação
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> consultarRegioesPorUnidadeFederacao(String chaveUnidadeFederacao) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		List<Regiao> regioes = fachada.obterRegioesPorUnidadeFederacao(Long.valueOf(chaveUnidadeFederacao));

		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (Regiao regiao : regioes) {
			sortedMap.put(regiao.getDescricao(), String.valueOf(regiao.getChavePrimaria()));
		}

		for (Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método obtem uma lista de Tipo de serviço através do chamado.
	 *
	 * @param chaveChamado
	 * @param chaveAssunto
	 * @return Uma {@link Collection} de {@link Map} de {@link String} contendo dados para exibição
	 * @throws GGASException
	 * @throws ParseException
	 */
	public List<Map<String, String>> obterListaServicoTipoPorChamado(Long chaveChamado, Long chaveAssunto) throws GGASException {

		Collection<ChamadoAssuntoServicoTipo> servicos = new HashSet<>();

		if (chaveChamado != null && chaveChamado != 0L) {
			Chamado chamado = ServiceLocator.getInstancia().getControladorChamado().obterChamado(chaveChamado);
			servicos = chamado.getChamadoAssunto().getListaServicos();
			return FluentIterable.from(servicos).transform(new ColetorServicoTipo()).filter(new FiltroServicoNaoAutorizado(chamado))
					.transform(new ColetorDadosServicoTipo()).toList();
		} else if (chaveAssunto != null && chaveAssunto != 0L) {
			ChamadoAssunto chamadoAssunto =
					ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chaveAssunto);
			servicos = chamadoAssunto.getListaServicos().stream().sorted(Comparator.comparing(p -> p.getNumeroOrdem())).collect(Collectors.toList());
		}
		
		if(servicos != null && !servicos.isEmpty()) {
			servicos = servicos.stream().filter(p -> p.getServicoTipo().getIndicadorAgendamento()).collect(Collectors.toList());
		}

		return FluentIterable.from(servicos).transform(new ColetorServicoTipo()).transform(new ColetorDadosServicoTipo()).toList();
	}

	/**
	 * Método responsável por consultar tronco por city gate.
	 *
	 * @param chavePrimariaCityGate Chave primária de um City Gate.
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public List<Map<String, String>> listarTroncoPorCityGate(Long chavePrimariaCityGate) throws GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_CITY_GATE, chavePrimariaCityGate);
		if (chavePrimariaCityGate != null) {
			Collection<Tronco> troncos = fachada.consultarTronco(filtro);
			if (troncos != null && !troncos.isEmpty()) {
				for (Tronco tronco : troncos) {
					Map<String, String> dados = new HashMap<String, String>();
					dados.put(String.valueOf(tronco.getChavePrimaria()), tronco.getDescricao());
					lista.add(dados);
				}
			}
		}
		return lista;
	}

	/**
	 * Método responsável por carregar uma lista de agendamentos para preenchimento do calendario.
	 *
	 * @param mes
	 * @param ano
	 * @param chaveServicoTipo
	 * @return List<Map<String, String>> - lista
	 * @throws GGASException 
	 * @throws NumberFormatException 
	 */
	public List<Map<String, String>> carregaListaAgendamentos(int mes, int ano, Long chaveServicoTipo) throws NumberFormatException, GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();

		ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO = new ServicoAutorizacaoAgendaPesquisaVO();
		servicoAutorizacaoAgendaPesquisaVO.setMes(mes);
		servicoAutorizacaoAgendaPesquisaVO.setAno(ano);

		List<ServicoAutorizacaoAgendaVO> agendamentos =
						ServiceLocator.getInstancia().getControladorServicoAutorizacao()
										.consultarAgendamentos(servicoAutorizacaoAgendaPesquisaVO, chaveServicoTipo);
		for (ServicoAutorizacaoAgendaVO servicoAutorizacaoAgendaVO : agendamentos) {
			Map<String, String> dados = new HashMap<String, String>();
			dados.put("manha", String.valueOf(servicoAutorizacaoAgendaVO.getTurnoManha()));
			dados.put("tarde", String.valueOf(servicoAutorizacaoAgendaVO.getTurnoTarde()));
			dados.put("noite", String.valueOf(servicoAutorizacaoAgendaVO.getTurnoNoite()));
			lista.add(dados);
		}
		return lista;
	}

	/**
	 * Verifica a existencia de Autorizacao de Serviço por Tipo de Serviço.
	 *
	 * @param chaveTipoServico
	 * @return Map<String, String> - dados
	 * @throws GGASException
	 */
	public Map<String, String> existeAutorizacaoServico(Long chaveTipoServico) throws GGASException {

		ControladorServicoTipo controladorTipoServico = ServiceLocator.getInstancia().getControladorServicoTipo();
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia().getControladorServicoAutorizacao();

		ServicoTipo servicoTipo = controladorTipoServico.obterServicoTipo(chaveTipoServico);
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		servicoAutorizacaoVO.setServicoTipo(servicoTipo);
		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (controladorServicoAutorizacao.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.TRUE).isEmpty()) {
			dados.put("existeErro", "true");
		} else {
			dados.put("existeErro", "false");
		}
		return dados;
	}

	/**
	 * Recupera o Equipamento com a chave primária informada.
	 *
	 * @param idEquipamento Chave primária do equipamento a ser obtido.
	 * @return Map<String, String> - dados
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> obterEquipamentoPorChave(Long idEquipamento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (idEquipamento == null) {
			dados = null;
		} else {
			Equipamento equipamento = fachada.buscarEquipamentoPorChave(idEquipamento);

			dados.put(ID_SEGMENTO, "");
			dados.put("indPotenciaFixa", "false");
			dados.put("descricao", "");
			dados.put("potenciaFixaBaixa", "");
			dados.put("potenciaFixaMedia", "");
			dados.put("potenciaFixaAlta", "");
			dados.put("potenciaPadrao", "");

			if (equipamento != null) {
				Segmento segmentoEqpa = equipamento.getSegmento();

				dados.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, String.valueOf(equipamento.getChavePrimaria()));

				if (equipamento.getDescricao() != null) {
					dados.put("descricao", equipamento.getDescricao());
				}

				if (segmentoEqpa != null) {
					popularDadosEquipamento(dados, equipamento, segmentoEqpa);
				}
			}
		}
		return dados;
	}

	/**
	 * @param dados
	 * @param equipamento
	 * @param segmentoEqpa
	 */
	private void popularDadosEquipamento(Map<String, String> dados, Equipamento equipamento, Segmento segmentoEqpa) {

		dados.put(ID_SEGMENTO, String.valueOf(segmentoEqpa.getChavePrimaria()));

		if (segmentoEqpa.getindicadorEquipamentoPotenciaFixa() != null) {
			dados.put("indPotenciaFixa", segmentoEqpa.getindicadorEquipamentoPotenciaFixa().toString());
		}

		dados.put("segmentoDescricao", "");
		if (segmentoEqpa.getDescricao() != null) {
			dados.put("segmentoDescricao", segmentoEqpa.getDescricao());
		}

		if (segmentoEqpa.getindicadorEquipamentoPotenciaFixa() != null && segmentoEqpa.getindicadorEquipamentoPotenciaFixa()) {

			dados.put("potenciaFixaBaixa",
							Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_BAIXA,
											equipamento.getPotenciaFixaBaixa(), Constantes.LOCALE_PADRAO));

			dados.put("potenciaFixaMedia",
							Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_MEDIA,
											equipamento.getPotenciaFixaMedia(), Constantes.LOCALE_PADRAO));

			dados.put("potenciaFixaAlta",
							Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_BAIXA,
											equipamento.getPotenciaFixaAlta(), Constantes.LOCALE_PADRAO));
		} else {
			dados.put("potenciaPadrao", Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_PADRAO,
							equipamento.getPotenciaPadrao(), Constantes.LOCALE_PADRAO));
		}
	}

	/**
	 * Método responsável por consultar as equipes.
	 *
	 * @return Um mapa de String
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> listarEquipes() throws GGASException {

		Map<String, String> dados = new TreeMap<String, String>();
		ControladorEquipe controladorEquipe = ServiceLocator.getInstancia().getControladorEquipe();


		Collection<Equipe> listaEquipe = controladorEquipe.listarEquipe();

		for (EntidadeNegocio equipe : listaEquipe) {
			dados.put(String.valueOf(equipe.getChavePrimaria()), equipe.getLabel());
		}

		return dados;
	}

	/**
	 * Método responsável por carregar unidades visualizadoras do assunto do chamado
	 *
	 * @param chaveAssuntoChamado Long - chave do assunto do chamado
	 * @return Um mapa de String
	 */
	public Map<String, String> carregarUnidadesVisualizadorasPadrao(Long chaveAssuntoChamado) {

		Map<String, String> dados = new HashMap<String, String>();

		ControladorChamadoAssunto controladorChamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto();
		ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.carregarChamadoAssuntoProjetado(chaveAssuntoChamado);

		Boolean indicadorUnidadesOrganizacionalVisualizadora = chamadoAssunto.getIndicadorUnidadesOrganizacionalVisualizadora();
		dados.put("indicadorUnidadeVisualizadora", String.valueOf(indicadorUnidadesOrganizacionalVisualizadora));

		Collection<UnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora = controladorChamadoAssunto.
				obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(chamadoAssunto.getChavePrimaria());

		if (indicadorUnidadesOrganizacionalVisualizadora
						&& !Util.isNullOrEmpty(listaUnidadeOrganizacionalVisualizadora)) {
			String idsUnidades = "";
			for (UnidadeOrganizacional unidadeVisualizadora : listaUnidadeOrganizacionalVisualizadora) {
				idsUnidades = idsUnidades.concat(String.valueOf(unidadeVisualizadora.getChavePrimaria())).concat(",");
			}
			idsUnidades = idsUnidades.substring(0, idsUnidades.length() - 1);
			dados.put("idsUnidadesVisualizadoras", idsUnidades);
		}

		return dados;
	}

	/**
	 * Recupera o Equipamento com a chave primária
	 * informada.
	 *
	 * @param idEquipamento Chave primária do equipamento a ser
	 *            obtido.
	 * @param idSegmento Chave primária do
	 * 			segmento para filtrar equipamaento
	 * @return Um equipamento.
	 * @throws GGASException the GGAS exception
	 */
	public Map<String, String> carregarOpcoesPotenciaParaEquipamento(Long idSegmento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();
		Segmento segmento = fachada.buscarSegmentoChave(idSegmento);

		dados.put("indicadorEquipamentoPotenciaFixa", "false");

		if (segmento != null) {
			if (segmento.getindicadorEquipamentoPotenciaFixa() != null) {
				dados.put("indicadorEquipamentoPotenciaFixa", segmento.getindicadorEquipamentoPotenciaFixa().toString());
			}
		} else {
			dados = null;
		}

		return dados;
	}

	/**
	 * Obtem o valor da potencia
	 *
	 * @param idPotenciaFixa
	 * @param idEquipamento
	 * @return valor potencia
	 * @throws GGASException
	 */
	public Map<String, String> obterValorPotenciaFixaPorChave(Long idPotenciaFixa, Long idEquipamento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Equipamento equipamento = fachada.obterEquipamento(idEquipamento);

		Long codPotenciaAlta = fachada.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_ALTA).getValorLong();
		Long codPotenciaMedia = fachada.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_MEDIA).getValorLong();
		Long codPotenciaBaixa = fachada.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_BAIXA).getValorLong();

		dados.put("valor", "");

		if (idPotenciaFixa != null && equipamento != null) {
			if (idPotenciaFixa.equals(codPotenciaAlta) && equipamento.getPotenciaFixaAlta() != null) {

				dados.put("valor", equipamento.getPotenciaFixaAltaFormatada());

			} else if (idPotenciaFixa.equals(codPotenciaMedia) && equipamento.getPotenciaFixaMedia() != null) {

				dados.put("valor", equipamento.getPotenciaFixaMediaFormatada());

			} else if (idPotenciaFixa.equals(codPotenciaBaixa) && equipamento.getPotenciaFixaBaixa() != null) {

				dados.put("valor", equipamento.getPotenciaFixaBaixaFormatada());
			}
		}

		return dados;
	}

	/**
	 * Verifica restrição imovel
	 *
	 * @param chaveAssunto
	 * @param chaveImovel
	 * @return indicador restrição
	 * @throws GGASException
	 */
	public Boolean verificarGeracaoAutomaticaASChamado(Long chaveAssunto) throws GGASException {


		if (chaveAssunto != null) {
			ChamadoAssunto chamadoAssunto =
					ServiceLocator.getInstancia().getControladorChamadoAssunto().carregarChamadoAssuntoProjetado(chaveAssunto);

			if (Util.isTrue(chamadoAssunto.getIndicadorGeraServicoAutorizacao())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica restrição imovel
	 *
	 * @param chaveAssunto
	 * @param chaveImovel
	 * @return indicador restrição
	 * @throws GGASException
	 */
	public Map<String, Boolean> verificarRestricaoServicoChamadoImovel(Long chaveAssunto, Long chaveImovel) throws GGASException {

		Map<String, Boolean> dados = new HashMap<String, Boolean>();

		ChamadoAssunto assunto = ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chaveAssunto);

		Imovel imovel = fachada.buscarImovelPorChave(chaveImovel);

		if (assunto != null && imovel != null && Util.isTrue(assunto.getIndicadorVerificaRestServ())
						&& Util.isTrue(imovel.getIndicadorRestricaoServico())) {
			dados.put("valor", true);
		} else {
			dados.put("valor", false);
		}

		return dados;
	}

	/**
	 * Carrega orientação do assunto do chamado
	 *
	 * @param chaveAssunto
	 * @return orientacao
	 */
	public String carregarOrientacaoAssunto(Long chaveAssunto) {

		if (chaveAssunto != null) {
			ChamadoAssunto chamadoAssunto =
							ServiceLocator.getInstancia().getControladorChamadoAssunto().carregarChamadoAssuntoProjetado(chaveAssunto);
			if (chamadoAssunto != null && chamadoAssunto.getOrientacao() != null) {
				return chamadoAssunto.getOrientacao();
			}
		}
		return "";
	}

	/**
	 * Verifica se o assunto do chamado está habilitado para informação de acionamento de gasista ou plantonista
	 *
	 * @param chaveAssunto Long - Chave do assunto
	 * @return indicador acionamento gasista
	 */
	public Boolean possuiInformacaoAcionamentoGasista(Long chaveAssunto) {

		Boolean retorno = false;

		if (chaveAssunto != null) {
			ChamadoAssunto chamadoAssunto =
							ServiceLocator.getInstancia().getControladorChamadoAssunto().carregarChamadoAssuntoProjetado(chaveAssunto);
			if (chamadoAssunto != null && chamadoAssunto.getIndicadorAcionamentoGasista() != null
							&& chamadoAssunto.getIndicadorAcionamentoGasista()) {
				retorno = true;
			}
		}

		return retorno;
	}

	/**
	 * Obter convenios.
	 *
	 * @param chavePrimariaTipoConvenio the chave primaria tipo convenio
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	public Map<String, String> obterConvenios(Long chavePrimariaTipoConvenio) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(TIPO_CONVENIO, chavePrimariaTipoConvenio);
		filtro.put(HABILITADO, "true");

		Map<String, String> map = new HashMap<String, String>();

		for (ArrecadadorContratoConvenio convenio : fachada.consultarArrecadadorConvenio(filtro)) {

			StringBuilder construtor = new StringBuilder();
			construtor.append(convenio.getCodigoConvenio());
			if (convenio.getArrecadadorCarteiraCobranca() != null) {
				construtor.append(" - ");
				if ("SEM REGISTRO".equalsIgnoreCase(convenio.getArrecadadorCarteiraCobranca().getTipoCarteira().getDescricao())) {
					construtor.append("CN");
				} else {
					construtor.append("CR");
				}
			}
			construtor.append(" - ");
			construtor.append(convenio.getArrecadadorContrato().getArrecadador().getBanco().getNomeAbreviado());

			map.put(String.valueOf(convenio.getChavePrimaria()), construtor.toString());
		}

		return map;

	}

	/**
	 * Alterar data previsão de encerramento
	 *
	 * @param chavePrimaria
	 * @param descricao
	 * @param idMotivo
	 * @param dataPrevisaoEncerramento
	 * @param request
	 * @return retorno - Boolean
	 * @throws GGASException
	 */
	public Boolean alterarDataPrevisaoEncerramento(Long chavePrimaria, String descricao, String idMotivo,
			String dataPrevisaoEncerramento, HttpServletRequest request) throws GGASException {

		ControladorChamado controladorChamado = ServiceLocator.getInstancia().getControladorChamado();
		Chamado chamadoExistente = controladorChamado.obterChamado(chavePrimaria);
		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);

		Boolean retorno = true;

		if (chamadoExistente.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
			return false;
		}

		try {
			@SuppressWarnings("unchecked")
			List<ContatoCliente> listaChamadoContato = (List<ContatoCliente>) request.getSession().getAttribute("listaChamadoContato");

			Chamado chamado = chamadoExistente.clone();

			chamado.setDescricao(descricao);
			chamadoExistente.setDescricao(descricao);

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			chamado.setDataPrevisaoEncerramento(format.parse(dataPrevisaoEncerramento));

			controladorChamado.atualizarChamado(chamado, listaChamadoContato);

			Map<String, Object> dados = new HashMap<>();
			dados.put("usuario", usuario);
			dados.put("idMotivo", idMotivo);

			controladorChamado.incluirChamadoHistorico(chamadoExistente, Constantes.C_OPERACAO_CHAMADO_ALTERADO, dados);

		} catch (CloneNotSupportedException e) {
			LOG.info(e);
			LOG.error(e.getMessage());
			retorno = false;
		} catch (ParseException ex) {
			LOG.error(ex.getMessage());
			retorno = false;
		}

		return retorno;
	}

	/**
	 * Obtem contato imovel por id
	 *
	 * @param chavePrimaria
	 * @return contato imovel
	 * @throws GGASException
	 */
	public Map<String, String> obterContatoImovelPorId(Long chavePrimaria) throws GGASException {
		Map<String, String> dados = new HashMap<String, String>();

		ContatoImovel contato = fachada.obterContatoImovel(chavePrimaria);

		if (contato.getTipoContato() != null) {
			dados.put(ID_TIPO_CONTATO, String.valueOf(contato.getTipoContato().getChavePrimaria()));
		} else {
			dados.put(ID_TIPO_CONTATO, "");
		}

		if (contato.getProfissao() != null) {
			dados.put(ID_PROFISSAO, String.valueOf(contato.getProfissao().getChavePrimaria()));
		} else {
			dados.put(ID_PROFISSAO, "");
		}

		if (contato.getNome() != null) {
			dados.put(NOME_CONTATO, contato.getNome());
		} else {
			dados.put(NOME_CONTATO, "");
		}

		if (contato.getCodigoDDD() != null) {
			dados.put(DDD_CONTATO,  String.valueOf(contato.getCodigoDDD()));
		} else {
			dados.put(DDD_CONTATO, "");
		}

		if (contato.getFone() != null) {
			dados.put(TELEFONE_CONTATO, contato.getFone());
		} else {
			dados.put(TELEFONE_CONTATO, "");
		}

		if (contato.getRamal() != null) {
			dados.put(RAMAL_CONTATO, contato.getRamal());
		} else {
			dados.put(RAMAL_CONTATO, "");
		}

		if (contato.getCodigoDDD2() != null) {
			dados.put(DDD_CONTATO2, String.valueOf(contato.getCodigoDDD2()));
		} else {
			dados.put(DDD_CONTATO2, "");
		}

		if (contato.getFone2() != null) {
			dados.put(TELEFONE_CONTATO2, contato.getFone2());
		} else {
			dados.put(TELEFONE_CONTATO2, "");
		}

		if (contato.getRamal2() != null) {
			dados.put(RAMAL_CONTATO2, contato.getRamal2());
		} else {
			dados.put(RAMAL_CONTATO2, "");
		}

		if (contato.getDescricaoArea() != null) {
			dados.put(AREA_CONTATO, contato.getDescricaoArea());
		} else {
			dados.put(AREA_CONTATO, "");
		}

		if (contato.getEmail() != null) {
			dados.put(EMAIL_CONTATO, contato.getEmail());
		} else {
			dados.put(EMAIL_CONTATO, "");
		}

		if (contato.getFone() != null) {
			dados.put("fone1Formatado", contato.getTelefone1Formatado());
		} else {
			dados.put("fone1Formatado", "");
		}
		if (contato.getFone2() != null) {
			dados.put("fone2Formatado", contato.getTelefone2Formatado());
		} else {
			dados.put("fone2Formatado", "");
		}

		return dados;
	}

	/**
	 * Verifica restrição de servico
	 *
	 * @param chaveServico
	 * @param chaveChamado
	 * @param chaveImovel
	 * @param chaveAssunto
	 * @return
	 * @throws GGASException
	 */
	public Boolean verificarRestricaoServico(Long chaveServico, Long chaveChamado, Long chaveImovel, Long chaveAssunto)
			throws GGASException {

		Imovel imovel = null;
		ServicoTipo servicoTipo = null;
		ChamadoAssunto chamadoAssunto = null;

		if (!Util.isEmpty(chaveChamado)) {
			Chamado chamado = ServiceLocator.getInstancia().getControladorChamado().obterChamado(chaveChamado);
			imovel = chamado.getImovel();
			chamadoAssunto = chamado.getChamadoAssunto();
		}

		if (imovel == null && !Util.isEmpty(chaveImovel)) {
			imovel = fachada.buscarImovelPorChave(chaveImovel);
		}

		if (chamadoAssunto == null && !Util.isEmpty(chaveAssunto)) {
			chamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chaveAssunto);
		}

		if (!Util.isEmpty(chaveServico)) {
			servicoTipo = ServiceLocator.getInstancia().getControladorServicoTipo().consultarServicoTipo(chaveServico);
		}

		if (imovel != null && servicoTipo != null && chamadoAssunto != null && Util.isTrue(chamadoAssunto.getIndicadorVerificaRestServ())) {

			if (!Util.isNullOrEmpty(imovel.getListaImovelServicoTipoRestricao())) {
				for (ImovelServicoTipoRestricao imovelServicoTipoRestricao : imovel.getListaImovelServicoTipoRestricao()) {
					if (servicoTipo.getChavePrimaria() == imovelServicoTipoRestricao.getServicoTipo().getChavePrimaria()) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Obtem contato imovel por id
	 *
	 * @param chavePrimaria
	 * @return contato imovel
	 * @throws GGASException
	 */
	public Map<String, String> obterContatoClientePorId(Long chavePrimaria) throws GGASException {
		Map<String, String> dados = new HashMap<String, String>();

		ContatoCliente contato = fachada.obterContatoCliente(chavePrimaria);

		if (contato.getTipoContato() != null) {
			dados.put(ID_TIPO_CONTATO, String.valueOf(contato.getTipoContato().getChavePrimaria()));
		} else {
			dados.put(ID_TIPO_CONTATO, "");
		}

		if (contato.getProfissao() != null) {
			dados.put(ID_PROFISSAO, String.valueOf(contato.getProfissao().getChavePrimaria()));
		} else {
			dados.put(ID_PROFISSAO, "");
		}

		if (contato.getNome() != null) {
			dados.put(NOME_CONTATO, contato.getNome());
		} else {
			dados.put(NOME_CONTATO, "");
		}

		if (contato.getCodigoDDD() != null) {
			dados.put(DDD_CONTATO,  String.valueOf(contato.getCodigoDDD()));
		} else {
			dados.put(DDD_CONTATO, "");
		}

		if (contato.getFone() != null) {
			dados.put(TELEFONE_CONTATO, String.valueOf(contato.getFone()));
		} else {
			dados.put(TELEFONE_CONTATO, "");
		}

		if (contato.getRamal() != null) {
			dados.put(RAMAL_CONTATO, String.valueOf(contato.getRamal()));
		} else {
			dados.put(RAMAL_CONTATO, "");
		}

		if (contato.getCodigoDDD2() != null) {
			dados.put(DDD_CONTATO2, String.valueOf(contato.getCodigoDDD2()));
		} else {
			dados.put(DDD_CONTATO2, "");
		}

		if (contato.getFone2() != null) {
			dados.put(TELEFONE_CONTATO2, String.valueOf(contato.getFone2()));
		} else {
			dados.put(TELEFONE_CONTATO2, "");
		}

		if (contato.getRamal2() != null) {
			dados.put(RAMAL_CONTATO2, String.valueOf(contato.getRamal2()));
		} else {
			dados.put(RAMAL_CONTATO2, "");
		}

		if (contato.getDescricaoArea() != null) {
			dados.put(AREA_CONTATO, contato.getDescricaoArea());
		} else {
			dados.put(AREA_CONTATO, "");
		}

		if (contato.getEmail() != null) {
			dados.put(EMAIL_CONTATO, contato.getEmail());
		} else {
			dados.put(EMAIL_CONTATO, "");
		}

		if (StringUtils.isNotBlank(contato.getTelefone1Formatado())) {
			dados.put("telefone1Formatado", contato.getTelefone1Formatado());
		} else {
			dados.put("telefone1Formatado", "");
		}

		if (StringUtils.isNotBlank(contato.getTelefone2Formatado())) {
			dados.put("telefone2Formatado", contato.getTelefone2Formatado());
		} else {
			dados.put("telefone2Formatado", "");
		}

		if (StringUtils.isNotBlank(contato.getRg())) {
			dados.put("rg", contato.getRg());
		} else {
			dados.put("rg", "");
		}

		if (StringUtils.isNotBlank(contato.getCpf())) {
			dados.put("cpf", contato.getCpf());
		} else {
			dados.put("cpf", "");
		}

		return dados;
	}

	/**
	 * Retorna o canal de atendimento
	 *
	 * @param chaveAssunto
	 * @return canalAtendimento
	 */
	public String obterCanalAtendimetno(Long chaveAssunto) {

		if (chaveAssunto != null) {
			ChamadoAssunto chamadoAssunto =
					ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chaveAssunto);
			if (chamadoAssunto != null && chamadoAssunto.getCanalAtendimento() != null) {
				return String.valueOf(chamadoAssunto.getCanalAtendimento().getChavePrimaria());
			}
		}
		return "";
	}

	/**
	 * Carrega orientação do assunto do chamado
	 *
	 * @param chaveServicoTipo
	 * @return orientacao
	 */
	public String carregarOrientacaoServicoTipo(Long chaveServicoTipo) {

		if (chaveServicoTipo != null) {
			ServicoTipo servicoTipo = ServiceLocator.getInstancia().getControladorServicoTipo().consultarServicoTipo(chaveServicoTipo);
			if (servicoTipo != null && servicoTipo.getOrientacao() != null) {
				return servicoTipo.getOrientacao();
			}
		}
		return "";
	}

	/**
	 * Carrega prazo previsto para atendimento do chamado
	 *
	 * @param chavePrimaria
	 * @return quantidadeHorasPrevistaAtendimento
	 */
	public String carregarPrazoPrevisto(Long chavePrimaria) {

		if (chavePrimaria != null) {
			ChamadoAssunto chamadoAssunto =
					ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chavePrimaria);
			if (chamadoAssunto != null && chamadoAssunto.getQuantidadeHorasPrevistaAtendimento() != null) {
				return String.valueOf(chamadoAssunto.getQuantidadeHorasPrevistaAtendimento());
			}
		}
		return "";
	}

	/**
	 * Método responsável por carregar uma lista de perguntas do questionario
	 *
	 * @param chaveQuestionario
	 * @return perguntas
	 * @throws GGASException
	 */
	public List<Map<String, String>> carregaListaPerguntas(Long chaveQuestionario) throws GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();

		ControladorQuestionario controladorQuestionario = ServiceLocator.getInstancia().getControladorQuestionario();

		if (chaveQuestionario != null && chaveQuestionario > 0) {
			Questionario questionario = controladorQuestionario.consultarPorCodico(chaveQuestionario, "listaPerguntas");
			Collection<QuestionarioPergunta> perguntas = questionario.getListaPerguntas();

			for (QuestionarioPergunta pergunta : perguntas) {
				Map<String, String> dados = new HashMap<String, String>();
				dados.put("id", String.valueOf(pergunta.getChavePrimaria()));
				dados.put("descricao", pergunta.getNomePergunta());
				lista.add(dados);
			}
		}

		return lista;
	}

	/**
	 * Consulta as informações de id e desscrição de chamado assunto a partir de um
	 * id do chamado
	 * @param id do chamado
	 * @return retorna a lsita de valores
	 */
	public List<Map<String, String>> consultarValores(Long id) {
		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();

		try {
			Collection<ChamadoAssunto> chamadoAssuntos = fachada.getControladorChamadoAssunto().consultarChamado(id);
			chamadoAssuntos.forEach(chamado -> {
				Map<String, String> dados = new HashMap<String, String>();
				dados.put("id", String.valueOf(chamado.getChavePrimaria()));
				dados.put("descricao", chamado.getDescricao());
				lista.add(dados);
			});

		} catch (Exception e) {
			LOG.error("Falha ao consultar o valor para o chamado " + id, e);
		}

		return lista;
	}
	/**
	 * Método responsável por verificar Ponto Consumo Ativo
	 *
	 * @param idContrato
	 * @return TRUE ou FALSE
	 * @throws GGASException
	 */
	public boolean verificarPontoConsumoAtivo(Long idContrato) throws GGASException {

		Contrato selecionado = fachada.carregarContratoEncerramentoRescisao(idContrato);
		String situacaoConsumo = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_PONTO_CONSUMO_AGUARDANDO_ATIVACAO);
		SituacaoConsumo situacao = fachada.obterSituacaoConsumo(Long.parseLong(situacaoConsumo));

		int count = 0;

		for (ContratoPontoConsumo contratoPontoConsumo : selecionado.getListaContratoPontoConsumo()) {
			ContratoPontoConsumo contratoPontoConsumoAux = fachada.obterContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
			if (contratoPontoConsumoAux.getPontoConsumo().getSituacaoConsumo().equals(situacao)) {
				count++;
			}
		}
		return !(!Util.isNullOrEmpty(selecionado.getListaContratoPontoConsumo()) && selecionado.getListaContratoPontoConsumo().size() == count);
	}

	/**
	 * Método chamado pela JSP para exibição do log do processo em um modal
	 * @param chavePrimaria
	 * @param logErro
	 * @return
	 * @throws GGASException
	 */
	public List<Map<String, String>> exibirLog(Long chavePrimaria, Boolean logErro)
			throws GGASException {

		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();
		Processo processo = fachada.obterProcesso(chavePrimaria);

		byte[] log = null;

		HttpSession request = WebContextFactory.get().getSession();
		Usuario usuario = (Usuario) request.getAttribute(ATRIBUTO_USUARIO_LOGADO);

		if (processo.getOperacao() != null && processo.getOperacao().getModulo() != null &&
				processo.getOperacao().getModulo().getChavePrimaria() > 0 && usuario.getPapeis() != null) {
			List<Long> papeis = usuario.getPapeis().stream().map(papel -> papel.getChavePrimaria()).collect(Collectors.toList());
			Optional<Menu> menu = fachada.getControladorModulo().buscarMenu(processo.getOperacao().getDescricao(),
					"CONSULTAR", processo.getOperacao().getModulo().getChavePrimaria(), papeis);
			if (!menu.isPresent()) {
				Map<String, String> dados = new HashMap<String, String>();
				dados.put("error", GenericAction.obterMensagem(Constantes.ERRO_NEGOCIO_SEM_PERMISSAO_CONSULTAR));
				lista.add(dados);
				return lista;
			}
		}

		if(Boolean.TRUE.equals(logErro)) {
			log = processo.getLogErro();
		} else {
			log = processo.getLogExecucao();
		}

		if(log != null) {

			try {

				ByteArrayInputStream in = new ByteArrayInputStream(log);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				String value = null;
				while((index = in.read(bytearray)) != -1) {
					value = new String(bytearray, 0, index);
				}

				if (value != null) {
					value = value.replaceAll("(\n)", "\r\n");
				}

				Map<String, String> dados = new HashMap<String, String>();
				dados.put("log", value);
				lista.add(dados);

				in.close();

			} catch(IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return lista;

	}

	/**
	 * Consulta as condições necessárias para realizar o faturamento de um imóvel.
	 *
	 * Este recurso é consultado atráves do menu: “Cadastro > Imóvel > imóvel”. (exibirPesquisaImovel.jsp)
	 *
	 * A validação de medição é realizada para as seguintes atividades:
	 *
	 * City Gate;
	 * Grupo de Faturamento;
	 * Rota;
	 * Periodicidade;
	 * Meio de leitura;
	 * Número de série do Medidor;
	 * Datas de instalação e ativação do medidor.
	 *
	 * @param chavePrimaria
	 * 		-{@link Long}
	 * @return List<Map<String, String>> lista
	 * 		-{@link List }
	 */
	public List<Map<String, String>> exibirCondicoesParaMedicao(Long chavePrimaria) {

		Imovel imovel;
		List<Map<String, String>> lista = new ArrayList<>();
		try {

			imovel = fachada.buscarImovelPorChave(chavePrimaria);

			Collection<Imovel> imoveis;
			if (imovel.getCondominio()) {
				imoveis = new ArrayList<>();
				imoveis.add(imovel);
				imoveis.addAll(fachada.obterImoveisFilhosDoCondominio(imovel));
			} else {
				imoveis = Collections.singletonList(imovel);
			}

			for (Imovel imovelIndividual : imoveis) {
				Collection<PontoConsumo> pontosConsumo = imovelIndividual.getListaPontoConsumo()
						.stream()
						.sorted(Comparator.comparing(PontoConsumo::getDescricao))
						.collect(Collectors.toList());

				for (PontoConsumo pontoConsumo : pontosConsumo) {
					Map<String, String> dados = new HashMap<>();

					dados.put("imovelPai", String.valueOf(imovelIndividual.getCondominio()));
					dados.put(PONTO_CONSUMO_DESCRICAO, pontoConsumo.getDescricao());
					dados.put("pontoConsumo.chavePrimaria", String.valueOf(pontoConsumo.getChavePrimaria()));
					dados.put("latitude", pontoConsumo.getLatitudeGrau().toString());
					dados.put("longitude", pontoConsumo.getLongitudeGrau().toString());
					dados.put("codigoLegado", pontoConsumo.getCodigoLegado());

					Collection<PontoConsumoCityGate> pontoConsumoCityGates = fachada.obterPontoConsumoCityGate(pontoConsumo.getChavePrimaria());

					verificarPreRequisitoCityGate(dados, pontoConsumoCityGates);

					verificarPreRequisitoMedidor(pontoConsumo, dados);

					verificarPreRequisitoRota(pontoConsumo.getRota(), dados);
					
					verificarContratoPonto(pontoConsumo, dados);

					lista.add(dados);
				}
			}
		} catch (GGASException e) {
			LOG.error("Erro ao exibirCondicoesParaMedicao para a chave primaria: "+chavePrimaria, e);
			Map<String, String> dados = new HashMap<>();
			dados.put("erro", "true");
			lista.clear();
			lista.add(dados);
		}
		return lista.stream()
				.sorted(Comparator.comparing(p -> p.getOrDefault(PONTO_CONSUMO_DESCRICAO, "")))
				.collect(Collectors.toList());

	}

	private void verificarContratoPonto(PontoConsumo pontoConsumo, Map<String, String> dados) {
		
		String pressao = "";
		String vazao = "";
		
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		
		if (contratoPontoConsumo != null) {
			pressao = contratoPontoConsumo.getMedidaPressao() != null ? contratoPontoConsumo.getMedidaPressao() + " "
					+ contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada() : "";

			vazao = contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null
					? contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() + " "
							+ contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada()
					: "";
		}
		
		dados.put("pressaoContrato", pressao);
		dados.put("vazaoContrato", vazao);
		
	}

	/**
	 * Verificar os pré-requisitos para o city gate
	 *
	 * @param dados
	 * @param pontoConsumoCityGates
	 */
	private void verificarPreRequisitoCityGate(Map<String, String> dados, Collection<PontoConsumoCityGate> pontoConsumoCityGates) {
		if (pontoConsumoCityGates != null && !pontoConsumoCityGates.isEmpty()) {
			pontoConsumoCityGates.forEach(pontoConsumoCityGate -> {
				dados.put("cityGates.dataVigencia", Util.converterDataParaString(pontoConsumoCityGate.getDataVigencia()));
				if (pontoConsumoCityGate.getCityGate() == null) {
					dados.put(CITY_GATES_DESCRICAO, "");
					dados.put(CITY_GATES_VALIDO, "false");
				} else {
					dados.put(CITY_GATES_DESCRICAO, pontoConsumoCityGate.getCityGate().getDescricao());
					dados.put("cityGates.chavePrimaria", String.valueOf(pontoConsumoCityGate.getCityGate().getChavePrimaria()));
					dados.put(CITY_GATES_VALIDO, "true");
				}

			});
		} else {
			dados.put(CITY_GATES_DESCRICAO, "");
			dados.put(CITY_GATES_VALIDO, "false");
		}
	}
	/**
	 * Verificar os pré-requisitos do medidor
	 *
	 * @param pontoConsumo ponto de consumo do medidor
	 * @param dados mapa de dados a ser verificado
	 * @throws GGASException exceção lançada caso haja falha
	 */
	private void verificarPreRequisitoMedidor(PontoConsumo pontoConsumo, Map<String, String> dados) throws GGASException {
		InstalacaoMedidor instalacaoMedidor = fachada.obterInstalacaoMedidorPontoConsumo(pontoConsumo.getChavePrimaria());
		String dataInstalacao = "";
		String dataAtivacao = "";
		String numeroSerieMedidor = "";
		String valido = "false";
		String tipoMedidor = "";

		if (instalacaoMedidor != null && instalacaoMedidor.getMedidor() != null) {

			if (instalacaoMedidor.getData() != null) {
				dataInstalacao = Util.converterDataParaString(instalacaoMedidor.getData());
			}

			if (instalacaoMedidor.getDataAtivacao() != null) {
				dataAtivacao = Util.converterDataParaString(instalacaoMedidor.getDataAtivacao());
			}

			numeroSerieMedidor = instalacaoMedidor.getMedidor().getNumeroSerie();
			valido = "true";
			
			if(instalacaoMedidor.getMedidor().getTipoMedidor() != null) {
				tipoMedidor = instalacaoMedidor.getMedidor().getTipoMedidor().getDescricao();
			}
		}

		dados.put("dataInstalacao", dataInstalacao);
		dados.put("dataAtivacao", dataAtivacao);
		dados.put("numeroSerieMedidor", numeroSerieMedidor);
		dados.put("valido", valido);
		dados.put("tipoMedidor",tipoMedidor);
	}

	/**
	 * Verificar os pré-requisitos de leitura para rota (grupo de faturamento, periodicidade e tipo de leitura)
	 * @param rota
	 * @param dados
	 */
	private void verificarPreRequisitoRota(Rota rota, Map<String, String> dados) {
		if (rota == null) {
			dados.put("rota.descricao", "");
			dados.put("rota.valido", "false");

			dados.put(ROTA_GRUPO_FATURAMENTO_DESCRICAO, "");
			dados.put(ROTA_GRUPO_FATURAMENTO_VALIDO, "false");

			dados.put(ROTA_PERIODICIDADE_DESCRICAO, "");
			dados.put(ROTA_PERIODICIDADE_VALIDO, "false");

			dados.put(ROTA_TIPO_LEITURA_DESCRICAO, "");
			dados.put(ROTA_TIPO_LEITURA_VALIDO, "false");
		} else {
			dados.put("rota.valido", "true");
			dados.put("rota.descricao", rota.getNumeroRota());
			verificarPreRequisitoGrupoFaturamento(rota, dados);
			verificarPreRequisitoPeriodicidade(rota, dados);
			verificarPreRequisitoTipoLeitura(rota, dados);
		}
	}

	/**
	 * Verificar os pré-requisitos para grupo de faturamento
	 *
	 * @param rota
	 * @param dados
	 */
	private void verificarPreRequisitoGrupoFaturamento(Rota rota, Map<String, String> dados) {
		if (rota.getGrupoFaturamento() != null) {
			dados.put(ROTA_GRUPO_FATURAMENTO_DESCRICAO, rota.getGrupoFaturamento().getDescricao());
			dados.put(ROTA_GRUPO_FATURAMENTO_VALIDO, "true");
		} else {
			dados.put(ROTA_GRUPO_FATURAMENTO_DESCRICAO, "");
			dados.put(ROTA_GRUPO_FATURAMENTO_VALIDO, "false");
		}
	}

	/**
	 * Verificar os pré-requisitos de periodicidade
	 *
	 * @param imovel
	 * @param dados
	 */
	private void verificarPreRequisitoPeriodicidade(Rota rota, Map<String, String> dados) {
		if (rota.getPeriodicidade() != null) {
			dados.put(ROTA_PERIODICIDADE_DESCRICAO, rota.getPeriodicidade().getDescricao());
			dados.put(ROTA_PERIODICIDADE_VALIDO, "true");
		} else {
			dados.put(ROTA_PERIODICIDADE_DESCRICAO, "");
			dados.put(ROTA_PERIODICIDADE_VALIDO, "false");
		}
	}

	/**
	 * Verificar os pré-requisitos de tipo de leitura
	 *
	 * @param rota
	 * @param dados
	 */
	private void verificarPreRequisitoTipoLeitura(Rota rota, Map<String, String> dados) {
		if (rota.getTipoLeitura() != null) {
			dados.put(ROTA_TIPO_LEITURA_DESCRICAO, rota.getTipoLeitura().getDescricao());
			dados.put(ROTA_TIPO_LEITURA_VALIDO, "true");
		} else {
			dados.put(ROTA_TIPO_LEITURA_DESCRICAO, "");
			dados.put(ROTA_TIPO_LEITURA_VALIDO, "false");
		}
	}

	/**
	 * Método chamado na inclusão de chamado para verificar a garantia do tipo de serviço que está associado ao tipo do assunto de chamado
	 *
	 * A garantia é baseada em cima da data de execução do serviço ou se ainda tiver data de exec
	 *
	 * @param chavePrimariaCliente identificador do cliente
	 * @param chaveAssunto identificado do chamado assunto
	 * @param chaveImovel identificador do imóvel
	 * @return retorna se a garantia está vigente ou não
	 */
	public Boolean garantiaVigente(Long chavePrimariaCliente, Long chaveAssunto, Long chaveImovel) {
		if (chavePrimariaCliente != null && chavePrimariaCliente > 0 && chaveAssunto != null && chaveAssunto > 0 &&
				chaveImovel != null && chaveImovel > 0) {
			ChamadoAssunto chamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto()
					.consultarChamadoAssunto(chaveAssunto);
			Collection<ChamadoAssuntoServicoTipo> listaServicos = chamadoAssunto.getListaServicos();

			ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia().getControladorServicoAutorizacao();
			Collection<ServicoTipo> servicosTipos = Collections.EMPTY_LIST;
			if (listaServicos != null) {
				servicosTipos = listaServicos.stream().map(chamadoAssuntoServicoTipo -> chamadoAssuntoServicoTipo.getServicoTipo())
						.collect(Collectors.toList());
			}

			return isExistePeloMenosUmServicoLiberado(chavePrimariaCliente, chaveImovel, servicosTipos, controladorServicoAutorizacao);
		}
		return true;
	}

	/**
	 * Verifica se as garantias de todos os pontos de consumo informados estão válidas para o assunto informado
	 * @param chaveAssunto chave do assunto
	 * @param pontosConsumo lista de pontos de consumo
	 * @return retorna se todas as garantias estão válidas
	 */
	public Boolean isGarantiaVigentePontoConsumo(Long chaveAssunto, List<Long> pontosConsumo) {
		return pontosConsumo.stream().allMatch(chave -> {

			final PontoConsumo ponto = fachada.obterPontoConsumo(chave);
			final Cliente cliente;

			try {
				cliente = fachada.obterClientePorPontoConsumo(chave);
			} catch (GGASException e) {
				LOG.error("Falha ao obter cliente por ponto de consumo", e);
				throw new GGASRuntimeException(e);
			}

			return Optional.ofNullable(cliente)
					.map(c -> garantiaVigente(c.getChavePrimaria(), chaveAssunto, ponto.getImovel().getChavePrimaria())).orElse(true);

		});
	}

	private Boolean isExistePeloMenosUmServicoLiberado(Long chavePrimariaCliente, Long chaveImovel,
			Collection<ServicoTipo> listaServicos, ControladorServicoAutorizacao controladorServicoAutorizacao) {
		if (listaServicos.isEmpty()) {
			return true;
		} else {
			return listaServicos.stream().anyMatch(servicoTipo -> {
				Long numeroMaximoExecucoes = servicoTipo.getNumeroMaximoExecucoes();
				Long prazoGarantia = servicoTipo.getPrazoGarantia();

				if (numeroMaximoExecucoes != null) {
					Optional<ServicoAutorizacao> servicoAutorizacao = controladorServicoAutorizacao
							.consultarServicoAutorizacaoReferenciaGarantia(chavePrimariaCliente, servicoTipo.getChavePrimaria(), chaveImovel);
					if (servicoAutorizacao.isPresent()) {
						LocalDate agora = LocalDate.now();

						Optional<LocalDate> dataLimiteGarantia = buildDataGarantia(prazoGarantia, servicoAutorizacao);

						if (dataLimiteGarantia.isPresent() && dataLimiteGarantia.get().isBefore(agora) || prazoGarantia == null) {
							Long numeroExecucoes = controladorServicoAutorizacao.consultarNumeroExecucoesPorClienteEServicoTipo(chavePrimariaCliente,
									servicoTipo.getChavePrimaria(), chaveImovel,
									AjaxService.asDateFinalDia(dataLimiteGarantia.get()));

							if (numeroExecucoes < numeroMaximoExecucoes) {
								return true;
							}
							return false;
						} else {
							return true;
						}
					}
				}
				return true;
			});
		}
	}

	public static Optional<LocalDate> buildDataGarantia(Long prazoGarantia, Optional<ServicoAutorizacao> servicoAutorizacao) {
		if (servicoAutorizacao.isPresent()) {
			LocalDate dataLimiteGarantia = AjaxService.asLocalDate(servicoAutorizacao.get().getDataExecucao());

			return Optional.of(buildDataGarantiaComBaseNoPrazoEmDias(prazoGarantia, dataLimiteGarantia));
		}
		return Optional.empty();
	}

	private static LocalDate buildDataGarantiaComBaseNoPrazoEmDias(Long prazoGarantia, LocalDate dataLimiteGarantia) {
		if (prazoGarantia != null) {
			dataLimiteGarantia = dataLimiteGarantia.plus(prazoGarantia, ChronoUnit.DAYS);
		}
		return dataLimiteGarantia;
	}

	/**
	 * Obtém a lista de garantias de serviço com chamados em lote
	 * @param chaveAssunto chave do assunto do chamado
	 * @param pontosConsumo lista de pontos de consumo
	 * @return retorna a lista de garantia de servico para chamados em lote
	 * @throws GGASException caso ocorra alguma falha na busca
	 */
	public List<Map<String, Object>> servicosChamadoComPrazosEmLote(Long chaveAssunto, List<Long> pontosConsumo) throws GGASException {

		List<Map<String, Object>> garantiaServicoDtos = new ArrayList<>();

		for (Long chave : pontosConsumo) {
			final PontoConsumo ponto = fachada.obterPontoConsumo(chave);
			final Cliente cliente = fachada.obterClientePorPontoConsumo(chave);

			final List<Map<String, Object>> dados = servicosChamadoAssuntoComPrazos(
					Optional.ofNullable(cliente).map(Cliente::getChavePrimaria).orElse(null),
					chaveAssunto, ponto.getImovel().getChavePrimaria()
			);

			dados.forEach(m -> {
				m.put("cliente", Optional.ofNullable(cliente).map(Cliente::getNome).orElse(null));
				m.put("pontoConsumo", ponto.getDescricao());
			});

			garantiaServicoDtos.addAll(dados);

		}

		return garantiaServicoDtos;
	}

	/**
	 * Obtém serviços de chamado assunto com prazos
	 * 
	 * @param chavePrimariaCliente id do cliente - {@link Long}
	 * @param chaveAssunto id do assunto - {@link Long}
	 * @param chaveImovel id do imóvel - {@link Long}
	 * @return retorna os chamados assuntos com prazos - {@link List}
	 * @throws GGASException - {@link GGASException}
	 */
	public List<Map<String, Object>> servicosChamadoAssuntoComPrazos(Long chavePrimariaCliente, Long chaveAssunto, Long chaveImovel)
			throws GGASException {
		List<Map<String, Object>> lista = new ArrayList<>();

		ChamadoAssunto chamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto().consultarChamadoAssunto(chaveAssunto);
		Collection<ChamadoAssuntoServicoTipo> listaServicos = chamadoAssunto.getListaServicos();

		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia().getControladorServicoAutorizacao();

		try {

			listaServicos.forEach(chamadoAssuntoServicoTipo -> {

				Map<String, Object> dados = new LinkedHashMap<>();
				dados.put("chavePrimaria", chamadoAssuntoServicoTipo.getServicoTipo().getChavePrimaria());
				dados.put("descricao", chamadoAssuntoServicoTipo.getServicoTipo().getDescricao());

				Long numeroMaximoExecucoes = chamadoAssuntoServicoTipo.getServicoTipo().getNumeroMaximoExecucoes();
				Long prazoGarantia = chamadoAssuntoServicoTipo.getServicoTipo().getPrazoGarantia();

				dados.put("numeroMaximoExecucoes", numeroMaximoExecucoes);
				dados.put("prazoGarantia", prazoGarantia);
				Long numeroExecucoesDisponiveis = numeroMaximoExecucoes;

				if (numeroMaximoExecucoes != null && chavePrimariaCliente != null) {
					Optional<ServicoAutorizacao> servicoAutorizacao =
							controladorServicoAutorizacao.consultarServicoAutorizacaoReferenciaGarantia(chavePrimariaCliente,
									chamadoAssuntoServicoTipo.getServicoTipo().getChavePrimaria(), chaveImovel);
					if (servicoAutorizacao.isPresent()) {
						LocalDate agora = LocalDate.now();

						LocalDate dataLimiteGarantia = AjaxService.asLocalDate(servicoAutorizacao.get().getDataExecucao());
						dados.put("dataExecucacao", dataLimiteGarantia.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

						dataLimiteGarantia = buildDataGarantiaComBaseNoPrazoEmDias(prazoGarantia, dataLimiteGarantia);
						dados.put("dataLimiteGarantia", dataLimiteGarantia.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

						Long numeroExecucoes = controladorServicoAutorizacao.consultarNumeroExecucoesPorClienteEServicoTipo(
								chavePrimariaCliente, chamadoAssuntoServicoTipo.getServicoTipo().getChavePrimaria(), chaveImovel,
								AjaxService.asDateFinalDia(dataLimiteGarantia));

						numeroExecucoesDisponiveis = numeroMaximoExecucoes - numeroExecucoes;
						if ((dataLimiteGarantia.isBefore(agora) && numeroExecucoesDisponiveis <= 0)
								|| (prazoGarantia == null && numeroExecucoesDisponiveis <= 0)) {
							dados.put("naGarantia", Boolean.FALSE);
						} else {
							dados.put("naGarantia", Boolean.TRUE);
						}
					}
				}
				if (numeroExecucoesDisponiveis != null && numeroExecucoesDisponiveis < 0) {
					numeroExecucoesDisponiveis = 0L;
				}
				dados.put("numeroExecucoesDisponiveis", numeroExecucoesDisponiveis);
				lista.add(dados);
			});
		} catch (Exception e) {
			LOG.error("Não á serviços registrados para esse chamado.", e);
			throw new GGASRuntimeException(e);
		}
		return lista;
	}

	/**
	 *
	 * @param date
	 * @return Localdate
	 */
	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/**
	 *
	 * @param localDate
	 * @return data - Date
	 */
	public static Date asDateFinalDia(LocalDate localDate) {
		Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant.plus(1, ChronoUnit.DAYS));
	}

	/**
	 * Listar Status de NFE
	 * @return status da nota fiscal
	 * @throws GGASException caso ocorra falha na operação
	 */
	public Map<String, String> listarStatusNfe() throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Collection<EntidadeConteudo> entidades = fachada.listarStatusNFEDocumentoFiscal();
		if (entidades != null && !entidades.isEmpty()) {
			for (EntidadeConteudo entidade : entidades) {
				dados.put(String.valueOf(entidade.getChavePrimaria()), entidade.getDescricao());
			}
		}

		return dados;
	}
	/**
	 * Método responsável por consultar as faces de quadra pela quadra.
	 *
	 * @param chaveQuadra
	 *            the chave quadra
	 * @param idCep
	 *            the cep
	 * @return Um mapa de String
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, String> consultarFacesQuadraPorQuadraIdCep(final String chaveQuadra, final String idCep)
			throws GGASException {

		final Map<String, String> dados = new LinkedHashMap<String, String>();

		final List<QuadraFace> faces = fachada.obterQuadraFacePorQuadraId(Long.valueOf(chaveQuadra), idCep);

		final SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		for (final QuadraFace face : faces) {
			sortedMap.put(face.getQuadraFaceFormatada(), String.valueOf(face.getChavePrimaria()));
		}

		for (final Map.Entry<String, String> ordenar : sortedMap.entrySet()) {
			dados.put(ordenar.getValue(), ordenar.getKey());
		}

		return dados;
	}

	/**
	 * Método responsável por consultar as quadras pelo cep passado por parâmetro.
	 *
	 * @param idCep
	 *            O número do cep.
	 * @return Um mapa de String
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, String> consultarQuadrasPorIdCep(final String idCep) throws GGASException {

		final Map<String, String> dados = new LinkedHashMap<String, String>();

		if (idCep != null) {
			final Collection<Quadra> listaQuadra = fachada.listarQuadraPorIdCep(Long.valueOf(idCep));
			if ((listaQuadra != null) && (!listaQuadra.isEmpty())) {
				Util.ordenarColecaoPorAtributo(listaQuadra, "numeroQuadra", true);
				for (final Quadra quadra : listaQuadra) {
					dados.put(String.valueOf(quadra.getChavePrimaria()), String.valueOf(quadra.getNumeroQuadra()));
				}
			}
		}

		return dados;
	}


	/**
	 * Envia e-mail com o detalhamento do chamado
	 * 
	 * @param protocolo {@link String}
	 * 			protocolo do chamado
	 * @param emailCliente {@link String}
	 * 			email do cliente
	 * @param chamadoAssunto {@link String}
	 * 			assunto do email
	 * @param dataInicioAbertura {@link String}
	 * 			data de abertura do chamado
	 * @param dataPrevisaoEncerramento {@link String}
	 * 			data prevista de encerramento
	 * @param status {@link String}
	 * 			situação do chamado
	 * @param nomeCliente {@link String}
	 * 			nome do cliente
	 * @return true para email enviado com sucesso
	 *
	 * @throws GGASException - {@link GGASException}
	 */
	public boolean enviarEmailChamado(String protocolo, String emailCliente, String chamadoAssunto, String dataInicioAbertura,
			String dataPrevisaoEncerramento, String status, String nomeCliente) throws GGASException {
			return enviarEmailChamadoComRemetente(protocolo, emailCliente, chamadoAssunto, dataInicioAbertura,
					dataPrevisaoEncerramento, status, nomeCliente, null);
	}

	/**
	 * Envia e-mail com o detalhamento do chamado
	 * 
	 * @param protocolo {@link String}
	 * 			protocolo do chamado
	 * @param emailCliente {@link String} 
	 * 			email do cliente
	 * @param chamadoAssunto {@link String}
	 * 			assunto do email
	 * @param dataInicioAbertura {@link String}
	 * 			data de abertura do chamado
	 * @param dataPrevisaoEncerramento {@link String}
	 * 			data prevista de encerramento
	 * @param status {@link String}
	 * 			situação do chamado
	 * @param nomeCliente {@link String}
	 * 			nome do cliente
	 * @param remetente {@link String}
	 * 			email do remetente
	 * @return true para email enviado com sucesso
	 * 
	 * @throws GGASException - {@link GGASException}
	 */
	public boolean enviarEmailChamadoComRemetente(String protocolo, String emailCliente, String chamadoAssunto, String dataInicioAbertura,
			String dataPrevisaoEncerramento, String status, String nomeCliente, String remetente) throws GGASException {

		String dataAberturaFormatada = DataUtil.formatarDataString(dataInicioAbertura,
				Constantes.FORMATO_DATA_HORA_US_COMPLETA, Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);

		String dataPrevisaoEncerramentoFormatada =  DataUtil.formatarDataString(dataPrevisaoEncerramento,
				Constantes.FORMATO_DATA_HORA_US_COMPLETA, Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);

		String[] listaInformacoes = new String[] {nomeCliente, protocolo, dataAberturaFormatada,
				dataPrevisaoEncerramentoFormatada, chamadoAssunto, status, "-"};

		String conteudoEmail = StringUtils.join(listaInformacoes, ";");
		String assuntoEmail = ("[GGAS] - Protocolo de Chamado");

		JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia()
				.getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		try {
			javaMailUtil.enviarChamado(remetente, emailCliente, assuntoEmail, conteudoEmail, true);
			return true;
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			return false;
		}
	}

	/**
	 * Envia e-mail com o histórico do chamado
	 *
	 * @param idChamado
	 * 			chave primária do chamado
	 * @return true para email enviado com sucesso
	 *
	 */
	public boolean enviarEmailHistoricoChamado(String idChamado) {

		if (StringUtils.isBlank(idChamado) && !StringUtils.isNumeric(idChamado)) {
			LOG.error("idChamado - Identificador vazio ou inválido");
			return false;
		}
		try {
			fachada.enviarEmailHistoricoChamado(Long.valueOf(idChamado));
			return true;
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			return false;
		}
	}
	
	/**
	 * Método responsável por consultar os
	 * ramos de atividade.
	 *
	 * @param idSegmento
	 *            A chave primária de um segmento do
	 *            sistema
	 * @return Uma coleção de ramo de atividades
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Map<String, String> consultarRamoAtividadePorSegmento(long idSegmento) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<RamoAtividade> listaRamoAtividade = fachada.listarRamoAtividadePorSegmento(idSegmento);
		if (listaRamoAtividade != null && !listaRamoAtividade.isEmpty()) {
			for (RamoAtividade ramoAtividade : listaRamoAtividade) {
				dados.put(String.valueOf(ramoAtividade.getChavePrimaria()), ramoAtividade.getDescricao());
			}
		}

		return dados;
	}
	
    /**
	 * Método responsável por obter o tamanho
	 * da lista de quadras por cep
	 * 
	 * @param cep - {@link String}
	 * @return tamanho da lista de quadras 
	 *@throws GGASException the GGAS exception
	 */
	
	public Integer obterTamanhoListaQuadraPorCep(String cep) throws GGASException{
		Collection<Quadra> listaQuadra = null;
		
		if (cep != null) {
			listaQuadra = fachada.listarQuadraPorCep(cep);
		}
		if (listaQuadra != null && !listaQuadra.isEmpty()) {
			return listaQuadra.size();
		}
		
		return 0;
	}
	
	/**
	 * Método responsável por obter o tamanho de
	 * uma lista de faces de quadra pela quadra.
	 * 
	 * @param chaveQuadra - {@link String}
	 * @param cep - {@link String}
	 * @return listaQuadraFaces - {@link Integer}
	 * @throws GGASException - {@link GGASException}
	 */
	public Integer obterTamanhoListaFacesQuadraPorQuadra(String chaveQuadra, String cep)
			throws GGASException {

		List<QuadraFace> listaQuadraFaces = null;
		
		if(chaveQuadra != null && cep != null){
			listaQuadraFaces = fachada.obterQuadraFacePorQuadra(Long.valueOf(chaveQuadra), cep);
			
			return listaQuadraFaces.size();
		}
		
		return null;
	}

	/**
	 * Consulta o processo a partir do chave primaria
	 *
	 * @param chavePrimaria
	 * 			chave primária do processo sistema
	 * @return o processo
	 * @throws GGASException - {@link GGASException}
	 *
	 */
	public Map<String, Object> exibirDialogInfoExecucaoProcesso(Long chavePrimaria) throws GGASException {

		Processo processo = fachada.obterProcesso(chavePrimaria);

		Map<String, Object> mapa = new HashMap<>();
		mapa.put(USUARIO_RESPONSAVEl_PROCESSO, STATUS_NAO_INFORMADO);
		mapa.put(UNIDADE_ORGANIZACIONAL_PROCESSO, STATUS_NAO_INFORMADO);
		mapa.put(DATA_INICIO_EXECUCAO_PROCESSO, STATUS_NAO_INFORMADO);
		mapa.put(DATA_FIM_EXECUCAO_PROCESSO, STATUS_NAO_INFORMADO);
		mapa.put(TEMPO_EXECUCAO_PROCESSO, STATUS_NAO_INFORMADO);
		mapa.put(DESCRICAO_PROCESSO, STATUS_NAO_INFORMADO);

		if (processo != null) {
			if (processo.getUsuarioExecucao() != null && !processo.getUsuarioExecucao().isEmpty()) {
				mapa.put(USUARIO_RESPONSAVEl_PROCESSO, processo.getUsuarioExecucao());
			}
			if (processo.getUnidadeOrganizacionalExecucao() != null &&
					!processo.getUnidadeOrganizacionalExecucao().isEmpty()) {
				mapa.put(UNIDADE_ORGANIZACIONAL_PROCESSO, processo.getUnidadeOrganizacionalExecucao());
			}
			if (processo.getInicio() != null) {
				String dataInicio = DataUtil.formatarDataString(processo.getInicio().toString(),
						Constantes.FORMATO_DATA_HORA_US_COMPLETA, Constantes.FORMATO_DATA_HORA_BR);
				mapa.put(DATA_INICIO_EXECUCAO_PROCESSO, dataInicio);
			}
			if (processo.getFim() != null) {
				String dataFim = DataUtil.formatarDataString(processo.getFim().toString(),
						Constantes.FORMATO_DATA_HORA_US_COMPLETA, Constantes.FORMATO_DATA_HORA_BR);
				mapa.put(DATA_FIM_EXECUCAO_PROCESSO, dataFim);
			}
			if (processo.getOperacao() != null && processo.getOperacao().getDescricao() != null
					&& !processo.getOperacao().getDescricao().isEmpty()) {
				mapa.put(DESCRICAO_PROCESSO, processo.getOperacao().getDescricao());
			}
			if (!processo.getTempoExecucao().isEmpty()) {
				mapa.put(TEMPO_EXECUCAO_PROCESSO, processo.getTempoExecucao());
			}
		}
		return mapa;
	}
	
	/**
	 * Verifica se houve virada de mes com a data informada
	 * @param periodo - {@link DateTime}
	 * @return true se houve, false se não houve virada de mes
	 */
	public boolean verificaViradaDeMes(DateTime periodo) {
		return Util.obterUltimoDiaDoMesParaData(periodo).compareTo(periodo) == 0;
	}
	
	public boolean verificaInicioDeMes(DateTime periodo, Integer periodicidade) {
		DateTime ultimoDiaMesDataMenosPeriodicidade = Util.obterUltimoDiaDoMesParaData(periodo.minus(periodicidade));
		DateTime ultimoDiaMesDataAtual = Util.obterUltimoDiaDoMesParaData(periodo);
		
		
		return Util.compararDatas(ultimoDiaMesDataMenosPeriodicidade.toDate(), ultimoDiaMesDataAtual.toDate()) == 0;
	}
	

	/**
	 * Função que valida as datas.
	 * 
	 * @param data
	 * 			the data
	 * @param dataAnterior
	 * 			the dataAnterior
	 * @return int
	 * @throws GGASException
	 * 					the GGAS Exception
	 */
	public int validarDatas(String data, String dataAnterior) throws GGASException{

			int valor = 0;
			Date dataAtual = Calendar.getInstance().getTime();
			Date date = Util.converterCampoStringParaData("Data da Leitura", data, Constantes.FORMATO_DATA_BR);
			Date datedataAnterior = Util.converterCampoStringParaData("Data da Leitura", dataAnterior, Constantes.FORMATO_DATA_BR);
			

			if(date != null && date.compareTo(dataAtual) > 0) {
				valor = 1;
				LOG.error("é igual ao dia atual.");
			}
			if(date != null && datedataAnterior != null && date.compareTo(datedataAnterior) < 0) {
				valor = 2;
				LOG.error("é menor que a data anterior.");
			}
			if(date == null){
				valor = 3;
			}
			return valor;
	}
	
	/**
	 * Carregar lista de datas disponiveis vencimento.
	 *
	 * @param chave dias corridos
	 * @throws NegocioException the negocio exception
	 * @return retorno
	 */
	public Long[] carregarListaDatasDisponiveisVencimento(Long chave) throws NegocioException {
		Long[] listaComboDias;
		Long[] valor = {1L,2L,3L,4L,5L,6L,7L,8L,9L,10L,11L,12L,13L,14L,15L,16L,17L,18L,19L,20L,21L,22L,23L,24L,25L,26L,27L,28L,29L,30L,31L};
		
		if (chave == 29L || chave == 30L) {
			listaComboDias = valor;
		} else {
			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ConstanteSistema constanteDiasVencimento =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_DIAS_POSSIVEIS_VENCIMENTO);

			ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			List<Long> listaDiasVencimentoPossiveis = controladorEntidadeConteudo.listarDiasVencimento(constanteDiasVencimento.getClasse());

			listaComboDias = new Long[listaDiasVencimentoPossiveis.size()];
			for (int i = 0; i < listaDiasVencimentoPossiveis.size(); i++) {
				listaComboDias[i] = listaDiasVencimentoPossiveis.get(i);
			}
		}
		return listaComboDias;
	}

	public int validarDatas(String data, String dataAnterior, Long anormalidadeLeitura) throws GGASException{
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));
			int valor = 0;
			Date dataAtual = Calendar.getInstance().getTime();
			Date date = null;
			if(!"".equals(data)) {
				date = Util.converterCampoStringParaData("Data da Leitura", data, Constantes.FORMATO_DATA_BR);
			}
			Date datedataAnterior = Util.converterCampoStringParaData("Data da Leitura", dataAnterior, Constantes.FORMATO_DATA_BR);
			if(date != null && date.compareTo(dataAtual) > 0) {
				valor = 1;
				LOG.error("é igual ao dia atual.");
			}
			if(date != null && datedataAnterior != null && date.compareTo(datedataAnterior) < 0) {
				valor = 2;
				LOG.error("é menor que a data anterior.");
			}
			if(date == null && anormalidadeLeitura.compareTo(codigoAnormalidadeLeitura) != 0){
				valor = 3;
			}
			return valor;
	}
	
	/**
	 * Função que valida as Tarefas Obrigatorias.
	 * 
	 * @param listaChavesPrimariasMarcadas - {@link Long}
	 * @return int - {@link int}
	 * @throws GGASException - {@link GGASException}
	 */
	public int validarAtividadesObrigatorias(List<Long> listaChavesPrimariasMarcadas) throws GGASException {
		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		List<Long> listaChavesObrigatorias = controladorCronogramaFaturamento.listaChavesObrigatorias();

		for (int i = 0; i < listaChavesObrigatorias.size(); i++) {
			if (!listaChavesPrimariasMarcadas.contains(listaChavesObrigatorias.get(i))) {
				return 2;
			}
		}
		return 1;
	}
	
	
	
	/**
	 * Função que tras os dado da medicao para buscar a foto que ve do coletor.
	 * 
	 * @param idRota
	 * 			the idRota
	 *  @param anoMesFaturamento
	 * 			the anoMesFaturamento
	 *  @param idLeituraMovimento
	 * 			the idLeituraMovimento
	 * @return String
	 * @throws GGASException
	 * 					the GGAS Exception
	 *  @throws IOException
	 * 					the IO Exception
	 */
	public String obterFotoPorIdDadosMedicao(Long idRota, Integer anoMesFaturamento, Long idLeituraMovimento) throws NegocioException, IOException{
		ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getInstancia().getControladorLeituraMovimento();
		Collection<LeituraMovimento> listaLeituraMovimento = 
							controladorLeituraMovimento.consultarLeituraMovimentoPorRota(idRota, anoMesFaturamento, null, null, null);
		String caminhoUrlFoto = "";

		for(LeituraMovimento leituraMovimento : listaLeituraMovimento){
			if(leituraMovimento.getFotoColetor() != null){
				if(idLeituraMovimento == leituraMovimento.getChavePrimaria()){
					caminhoUrlFoto =  Util.converterUrl(leituraMovimento.getFotoColetor(),idLeituraMovimento, leituraMovimento.getChavePrimaria());
					if(caminhoUrlFoto != null && caminhoUrlFoto != ""){
						return caminhoUrlFoto;
					}
				}
			}
		}
		return caminhoUrlFoto;
	}
	
	
	public String checarOperacaoMedidorCorreta (String idOperacaoMedidor, String idPontoConsumo) {
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getInstancia().getControladorMedidor();

		FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
		filtro.setIdPontoConsumo(Long.valueOf(idPontoConsumo));
		filtro.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
		Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
				.obterHistoricoOperacaoMedidor(filtro);
		
		if(!listaHistoricoOperacaoMedidor.isEmpty() && listaHistoricoOperacaoMedidor.iterator().hasNext()) {
			HistoricoOperacaoMedidor historicoOperacaoMedidor = listaHistoricoOperacaoMedidor.iterator().next();
			
			Long codigoOperacao = historicoOperacaoMedidor.getOperacaoMedidor().getChavePrimaria();
			
			if (codigoOperacao == OperacaoMedidor.CODIGO_INSTALACAO) {
				return String.valueOf(OperacaoMedidor.CODIGO_ATIVACAO);
			} else if (codigoOperacao == OperacaoMedidor.CODIGO_BLOQUEIO) {
				return String.valueOf(OperacaoMedidor.CODIGO_REATIVACAO);
			} else if (codigoOperacao == OperacaoMedidor.CODIGO_ATIVACAO) {
				return null;
			}
			
		}
		
		return idOperacaoMedidor;
	}
	
	public boolean checarEncerrarRotasCronogramaAberto(String anoMesReferencia, String grupoFaturamento) {
		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
		
		GrupoFaturamento grupo = controladorCronogramaFaturamento.obterGrupoFaturamentoPorDescricao(grupoFaturamento);
		
		String grupoReferenciaFormatado = Util.formatarAnoMes(grupo.getAnoMesReferencia()) + "-"
				+ grupo.getNumeroCiclo();

		
		
		if(grupoReferenciaFormatado.equals(anoMesReferencia)) {
			return true;
		}
		
		
		return false;
	}
	
	
	public String obterProximoCodigoLegadoPontoContumo() {
		
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		String codigoLegado = controladorPontoConsumo.obterProximoCodigoLegadoPontoContumo();
		
		if(codigoLegado != null) {
			return codigoLegado;
		}
		
		return "";
		
	}
	
	
	public String obterRotaProximaCepNumero(String chaveCep, String numero) {
		if(chaveCep != null && !chaveCep.equals("")) {
			ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
			Map<Integer, Long> mapaNumeroRota = controladorPontoConsumo.listarPontoConsumoPorCep(chaveCep);
			if(mapaNumeroRota != null && !mapaNumeroRota.isEmpty()) {
				if(numero != null && !numero.equals("") && numero.matches("[0-9]+")) {

					Integer numerosCep[] = mapaNumeroRota.keySet().toArray(new Integer[mapaNumeroRota.size()]);
					
					Integer numeroCep = Util.acharMaisProximoArrayInteiro(numerosCep, 800);
					
					return String.valueOf(mapaNumeroRota.get(numeroCep));
					
				} else {
					return String.valueOf(mapaNumeroRota.entrySet().iterator().next().getValue());
				}
			
			}
		}
		
		
		return "";
	}
	
	public Boolean checarUltimaOperacaoHistorico(HttpServletRequest request) {
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getInstancia().getControladorMedidor();
		Boolean retorno = Boolean.FALSE;
		
		
		Collection<ImovelPontoConsumoVO> listaImovelPontoConsumoVO =
				(Collection<ImovelPontoConsumoVO>) request.getSession().getAttribute("listaImovelPontoConsumoVO");
		
		for(ImovelPontoConsumoVO imovelPontoConsumo : listaImovelPontoConsumoVO) {
			Collection<PontoConsumo> listaPontoConsumo = imovelPontoConsumo.getListaPontoConsumo();
			for(PontoConsumo pontoConsumo : listaPontoConsumo) {
				FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
				filtro.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
				filtro.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
				filtro.setLimiteResultados(1);
				HistoricoOperacaoMedidor historico = controladorMedidor.obterUnicoHistoricoOperacaoMedidor(filtro);
				
				if(historico != null && historico.getOperacaoMedidor().getChavePrimaria() == OperacaoMedidor.CODIGO_BLOQUEIO && historico.getMotivoOperacaoMedidor().getChavePrimaria() == 5l) {
					if(retorno == Boolean.FALSE) {
						retorno = Boolean.TRUE;
					}
				} else {
					listaPontoConsumo.remove(pontoConsumo);
				}
			}
		}
		return retorno;
	}
	
	public Boolean consultarServicoAgendamentoItemLote(Long chaveServicoAgendamento) {
		ControladorComunicacao controladorComunicacao = ServiceLocator.getInstancia().getControladorComunicacao();
		
		return controladorComunicacao.verificaServicoTipoAgendamento(chaveServicoAgendamento) > 0;
	}
	
	public String[] consultarInformacoesProtocolo(Long chaveProtocolo) throws NegocioException {
		
		String[] retorno = new String[2];
		
		EntregaDocumento protocolo = fachada.obterEntregaDocumento(chaveProtocolo);
		
		retorno[0] = protocolo.getDescricaoObservacaoRetorno();
		retorno[1] = (protocolo.getFuncionario() != null) ? protocolo.getFuncionario().getNome() : "";
		
		return retorno;
	}
	
	public String consultarMesmaObservacaoProtocolo(Long[] chavesProtocolos) {
		String retorno = "";
		
		ControladorEntregaDocumento controladorEntregaDocumento = (ControladorEntregaDocumento) ServiceLocator.getInstancia().getControladorNegocio(ControladorEntregaDocumento.BEAN_ID_CONTROLADOR_ENTRAGA_DOCUMENTO);
		
		Collection<EntregaDocumento> listaEntregaDocumento = controladorEntregaDocumento.consultarProtocolosPorListaChaves(chavesProtocolos);
		
		String observacaoRetornoComparar = listaEntregaDocumento.iterator().next().getDescricaoObservacaoRetorno();
		
		if(observacaoRetornoComparar != null && !observacaoRetornoComparar.isEmpty()) {
			if(listaEntregaDocumento.stream().allMatch(p ->  p.getDescricaoObservacaoRetorno() != null && p.getDescricaoObservacaoRetorno().equals(observacaoRetornoComparar))) {
				retorno = observacaoRetornoComparar;
			}
		}
		
		return retorno;
	}
	
	public Boolean verificarNecessidadeNovoMedidor (Long chaveServicoTipo) throws NegocioException {
		ControladorServicoTipo controladorServicoTipo = ServiceLocator.getInstancia().getControladorServicoTipo();
		
		if(chaveServicoTipo == null || chaveServicoTipo <= 0) {
			return false;
		}
		ServicoTipo servicoTipo = (ServicoTipo) controladorServicoTipo.obterServicoTipo(chaveServicoTipo);
		
		return servicoTipo != null && servicoTipo.getIndicadorNovoMedidor() != null && servicoTipo.getIndicadorNovoMedidor();
	}
	
	public Long carregarChaveEquipePrioritaria(Long chavePrimaria)
					throws NegocioException, ParseException {

		ControladorServicoTipo controladorServicoTipo = (ControladorServicoTipo) ServiceLocator.getInstancia().getControladorServicoTipo();
		
		Equipe equipePrioritaria =  controladorServicoTipo.obterEquipePrioritaria(chavePrimaria);
		
		return equipePrioritaria != null ? equipePrioritaria.getChavePrimaria() : -1l;
	}
	
	
	public Map<String, String> listaTurnoPorEquipe(Long chaveEquipe) {
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia().getControladorServicoAutorizacao();

		Collection<ServicoTipoAgendamentoTurno> listaServicoTipoAgendamento = controladorServicoAutorizacao.listaTurnoPorEquipeServicoTipo(chaveEquipe);
		
		Collection<EntidadeConteudo> listaTurno = listaServicoTipoAgendamento.stream().map(p -> p.getTurno()).distinct()
				.sorted(Comparator.comparing(EntidadeConteudo::getDescricao)).collect(Collectors.toList());
		
		Map<String, String> dados = new LinkedHashMap<String, String>();
		for(EntidadeConteudo turno : listaTurno) {
			dados.put(String.valueOf(turno.getChavePrimaria()), turno.getDescricao());
		}
		
		return dados;
	}
	
	
	/**
	 * Consultar cliente por id.
	 *
	 * @param idCliente the id cliente
	 * @return the string
	 * @throws GGASException the GGAS exception
	 */
	public Long consultarClientePorCPFCNPJ(String cpfCnpj) throws GGASException {

		ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getControladorCliente();
		
		Cliente cliente = controladorCliente.obterClientePorCpfCnpj(cpfCnpj);
		
		return cliente != null ? cliente.getChavePrimaria() : 0l;
	}
	
	
	public Map<String, String> obterFuncionariosPorEquipe(Long idEquipe) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		Collection<EquipeComponente> listaEquipeComponente = ServiceLocator.getInstancia().getControladorEquipe()
				.listarEquipeComponente(Long.valueOf(idEquipe));

		Collection<Funcionario> listaFuncionario = listaEquipeComponente.stream().map(p -> p.getFuncionario())
				.collect(Collectors.toList());

		for (Funcionario funcionario : listaFuncionario) {
			dados.put(String.valueOf(funcionario.getChavePrimaria()), funcionario.getNome());
		}

		return dados;
	}
	
	
	public Boolean consultarEscalaDisponivel(String dataSelecionada, Long chaveEquipe)
			throws ParseException, GGASException {
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();
		Fachada fachada = Fachada.getInstancia();
		Equipe equipe = fachada.obterEquipe(chaveEquipe);

		Date data = Util.converterCampoStringParaData("data", dataSelecionada, Constantes.FORMATO_DATA_BR);

		Collection<EquipeTurnoItem> lista = controladorServicoAutorizacao.consultarEscalaDisponivel(equipe, null,
				DataUtil.converterDataParaString(data));

		for (EquipeTurnoItem equipeTurnoItem : lista) {
			Collection<ServicoAutorizacaoAgenda> listaAgendamento = controladorServicoAutorizacao
					.obterListaAgendamentosPorDiaturnoServicoTipoEquipe(data, equipeTurnoItem.getTurno(), null, equipe);

			if (!listaAgendamento.isEmpty()) {
				Date dataMinimaAgendamento = listaAgendamento.stream().map(ServicoAutorizacaoAgenda::getDataAgenda)
						.min(Date::compareTo).get();
				Date dataMaximaAgendamento = listaAgendamento.stream().map(ServicoAutorizacaoAgenda::getDataAgenda)
						.max(Date::compareTo).get();

				Date dataInicioEscala = equipeTurnoItem.getDataInicio();
				Date dataFimEscala = equipeTurnoItem.getDataFim();

				Long diferencaMinutosAgendamento = DataUtil.obterDiferencaMinutosEntreDatas(dataMinimaAgendamento,
						dataMaximaAgendamento);
				Long diferencaMinutosEscala = DataUtil.obterDiferencaMinutosEntreDatas(dataInicioEscala, dataFimEscala);

				if (diferencaMinutosAgendamento.compareTo(diferencaMinutosEscala) < 0) {
					if (DataUtil.menorOuIgualQue(dataFimEscala, data, false)) {
						continue;
					} else {
						return Boolean.TRUE;
					}
				}

			} else {
				return Boolean.TRUE;
			}

		}

		return Boolean.FALSE;
	}
	
	public Map<String, Boolean> carregaListaEscala(int mes, int ano, Long chaveEquipe) throws NumberFormatException, GGASException {
		
		Map<String, Boolean> lista = new HashMap<String, Boolean>();

		Collection<EquipeTurnoItem>  listaEscala =
				fachada.obterListaEquipeTurnoItem(mes, ano, chaveEquipe);
		
		for (EquipeTurnoItem equipeTurnoItem : listaEscala) {
			lista.put(DataUtil.converterDataParaString(equipeTurnoItem.getDataInicio()), Boolean.TRUE);
		}
		
		return lista;
	}
	
	
	public List<Map<String, String>> consultarLocalizacaoFuncionario(Long chaveFuncionario, Long chaveEquipe) throws NegocioException, IOException, ConcorrenciaException {
		
		List<Map<String, String>> lista = new ArrayList<Map<String, String>>();
		
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();
		
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia()
				.getControladorEntidadeConteudo();
		
		Long periodicidadeLocalizacao = Long.valueOf(controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Periodicidade Registro Localização Autorização Serviço").iterator().next().getDescricao());
		
		List<Object[]> listaLocalizacao = controladorServicoAutorizacao.consultarLocalizacaoOperadorMobile(chaveFuncionario);
		
		List<FuncionarioEquipeVO> equipeFuncionario = fachada.obterEquipeTurnoItemPorChaveEquipe(chaveEquipe);
				
		for(Object[] localizacao : listaLocalizacao) {
			Map<String, String> dados = new HashMap<String, String>();
			String nomeFuncionario = (String) localizacao[0];
			String coordenada = (String) localizacao[1];
			String popup = nomeFuncionario;
			String horario = (String) localizacao[4];
			Boolean isLogoff = Boolean.FALSE;
			String tipoLocalizacao = (String) localizacao[5];
			popup = popup + " - <b>"+horario+"h</b>";
			String numeroAS = localizacao[2] != null ? String.valueOf((Long)localizacao[2]) : "";
			isLogoff = DataUtil.verificarLogoffMobile((Date) localizacao[8],periodicidadeLocalizacao);
			Long chaveFunc = (Long) localizacao[9];
			Long previsao = (Long) localizacao [11];
			
			if(!isLogoff) {
				if(((Long)localizacao[10]) == 929) {
					String numeroASDeslocalmento = localizacao[6] != null ? String.valueOf((Long)localizacao[6]) : "";

					popup = popup + "<br> <b> Em Exec. AS: </b>"+numeroASDeslocalmento;
					
					String pontoConsumo = localizacao[7] != null ? String.valueOf(localizacao[7]) : "";
					
					if(!StringUtils.isEmpty(pontoConsumo)) {
						popup = popup + "<br>" +pontoConsumo;
					}
				} else if (((Long)localizacao[10]) == 928){
					
					String numeroASDeslocalmento = localizacao[6] != null ? String.valueOf((Long)localizacao[6]) : "";
					if(!StringUtils.isEmpty(numeroASDeslocalmento)) {
						
						String tipoMensagem = "Em Desl. AS:";
						if(tipoLocalizacao.equals("Fim do Deslocamento")) {
							tipoMensagem = "Em Exec. AS:";
						}
						
						popup = popup + "<br> <b> "+tipoMensagem+" </b>"+numeroASDeslocalmento;
						String pontoConsumo = localizacao[7] != null ? String.valueOf(localizacao[7]) : "";
						
						if(!StringUtils.isEmpty(pontoConsumo)) {
							popup = popup + "<br>"+pontoConsumo;
						}
						
					}
				}
			}
			
			//Início do fluxo de previsão de chegada
			ServicoAutorizacao sa = null;

			if((Long) localizacao[6] != null && previsao == null && ((Long)localizacao[10] == 928)) {
				
				sa = controladorServicoAutorizacao.obterServicoAutorizacao((Long) localizacao[6]);
				
				if(sa.getPontoConsumo() != null) {
					List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
					listaPontoConsumo.add(sa.getPontoConsumo());
					Double latitudeComparar = Double.valueOf(coordenada.split(",")[0]);
					Double longitudeComparar = Double.valueOf(coordenada.split(",")[1]);
					Double menorDistancia = -1.0;
					Long tempoDistancia = 0l;
					
					LOG.info("Iniciando chamada da API do Google (Acompanhamento AS) ");
					
					Map<String, Object> mapaRetornado = Util.obterMenorDistanciaTempoEntrePontosConsumo(listaPontoConsumo, latitudeComparar, longitudeComparar, menorDistancia, sa.getPontoConsumo(), tempoDistancia);
					
					LOG.info("Finalizando chamada da API do Google (Acompanhamento AS) ");
					tempoDistancia = (Long) mapaRetornado.get("tempoDistancia");
					menorDistancia = (Double) mapaRetornado.get("menorDistancia");
					
					ServicoAutorizacaoRegistroLocalImpl servicoAutorizacaoRegistroLocal = controladorServicoAutorizacao.consultarRegistroServicoAutorizacaoRegLocal((Long) localizacao[12]);
					
					if(servicoAutorizacaoRegistroLocal != null) {
						BigDecimal tempoDistanciaCalculo = BigDecimal.valueOf(tempoDistancia).divide(new BigDecimal(60),RoundingMode.UP);
						Long tempoDistanciaMinutos = Long.valueOf(String.valueOf(tempoDistanciaCalculo));
						servicoAutorizacaoRegistroLocal.setPrevisaoChegada(tempoDistanciaMinutos);
						controladorServicoAutorizacao.atualizar(servicoAutorizacaoRegistroLocal);
					}
					
				}
				
			}
			ServicoAutorizacaoRegistroLocalImpl servicoAutorizacaoRegistroLocal = controladorServicoAutorizacao.consultarRegistroServicoAutorizacaoRegLocal((Long) localizacao[12]);

			if(servicoAutorizacaoRegistroLocal != null) {
				if(servicoAutorizacaoRegistroLocal.getTipoLocalizacao() != null) {
					if(servicoAutorizacaoRegistroLocal.getTipoLocalizacao().getChavePrimaria() == 928) {
						if(servicoAutorizacaoRegistroLocal.getPrevisaoChegada() != null && localizacao[6] != null) {
							sa = controladorServicoAutorizacao.obterServicoAutorizacao((Long) localizacao[6]);
							if(sa != null) {
								if(sa.getPontoConsumo() != null) {
									popup += "<br>" + "Prev. Chegada: "+ servicoAutorizacaoRegistroLocal.getPrevisaoChegada() + " minuto(s)" ;
								}else {
									popup += "<br>" + "Sem Previsão de Chegada";
								}
							}
						}
					}
				}
			}//Fim de previsão de chegada

			dados.put("nomeFuncionario", nomeFuncionario);
			dados.put("coordenada", coordenada);
			dados.put("numeroAS", numeroAS);
			dados.put("popup", popup);
			dados.put("tipoLocalizacao", tipoLocalizacao);
			dados.put("isLogoff", String.valueOf(isLogoff));

			if(chaveEquipe == -1) {
				lista.add(dados);
			}else {
				if(equipeFuncionario.stream().anyMatch(ef -> ef.getChavePrimariaFuncionario() == chaveFunc)) {
					lista.add(dados);
				}
			}
			
		}
		
		return lista;
	}
	
	public Map<String, String> consultarFuncionariosAtualizados(Long equipe) throws NegocioException {

		
		Map<String, String> dados = new LinkedHashMap<String, String>();
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();

		ControladorFuncionario controladorFuncionario = ServiceLocator.getInstancia().getControladorFuncionario();

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("habilitado", true);

		List<Object[]> listaLocalizacao = controladorServicoAutorizacao.consultarLocalizacaoOperadorMobile(null);

		Collection<Funcionario> listaFuncionarios = controladorFuncionario.consultarFuncionarios(filtro);
		
		List<FuncionarioEquipeVO> equipeFuncionario = fachada.obterEquipeTurnoItemPorChaveEquipe(equipe);
		
		List<String> nomes = listaLocalizacao.stream().sorted(Comparator.comparing(array -> (String) array[0])).map(array -> (String) array[0]).collect(Collectors.toList());

		listaFuncionarios = listaFuncionarios.stream().filter(funcionario -> nomes.contains(funcionario.getNome()))
				.sorted(Comparator.comparing(p -> p.getNome()))
				.collect(Collectors.toList());

		if (listaFuncionarios != null && !listaFuncionarios.isEmpty()) {
			for (Funcionario funcionario : listaFuncionarios) {
				if(equipeFuncionario.size() > 0) {
					if(equipeFuncionario.stream().anyMatch(ef -> ef.getChavePrimariaFuncionario() == funcionario.getChavePrimaria())) {
						dados.put(String.valueOf(funcionario.getChavePrimaria()), funcionario.getNome());
					}
				}
				if (equipe == -1 && equipeFuncionario.size() < 1){
					dados.put(String.valueOf(funcionario.getChavePrimaria()), funcionario.getNome());
				}
			}
		}
		return dados;
	}
	
	public Map<String, String> carregarUnidadesVisualizadorasPadraoDescricao(Long chaveAssuntoChamado) {

		Map<String, String> dados = new HashMap<String, String>();

		ControladorChamadoAssunto controladorChamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto();
		ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.carregarChamadoAssuntoProjetado(chaveAssuntoChamado);

		Boolean indicadorUnidadesOrganizacionalVisualizadora = chamadoAssunto.getIndicadorUnidadesOrganizacionalVisualizadora();
		dados.put("indicadorUnidadeVisualizadora", String.valueOf(indicadorUnidadesOrganizacionalVisualizadora));

		Collection<UnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora = controladorChamadoAssunto.
				obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(chamadoAssunto.getChavePrimaria());

		if (indicadorUnidadesOrganizacionalVisualizadora
						&& !Util.isNullOrEmpty(listaUnidadeOrganizacionalVisualizadora)) {
			String descricaoUnidadesVisualizadoras = "";
			for (UnidadeOrganizacional unidadeVisualizadora : listaUnidadeOrganizacionalVisualizadora) {
				descricaoUnidadesVisualizadoras = descricaoUnidadesVisualizadoras.concat(String.valueOf(unidadeVisualizadora.getDescricao())).concat(",");
			}
			descricaoUnidadesVisualizadoras = descricaoUnidadesVisualizadoras.substring(0, descricaoUnidadesVisualizadoras.length() - 1);
			dados.put("descricaoUnidadesVisualizadoras", descricaoUnidadesVisualizadoras);
		}

		return dados;
	}

	public Boolean consultarMesmaOperacaoServicoTipo(Long idPontoConsumo, Long idServicoTipo) {
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();
		
		Boolean retorno = controladorServicoAutorizacao.verificarAsAbertaMesmaOperacao(idPontoConsumo, idServicoTipo);
		
		
		return retorno;
	}
	
	public Boolean consultarAssuntoChamadoMesmaOperacaoTipoServico(Long idChamadoAssunto, Long idPontoConsumo)
			throws NegocioException {

		ControladorChamadoAssunto controladorChamadoAssunto = ServiceLocator.getInstancia()
				.getControladorChamadoAssunto();

		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(idChamadoAssunto);

		if (!chamadoAssunto.getIndicadorGeraServicoAutorizacao()) {
			return Boolean.FALSE;
		}

		for (ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo : chamadoAssunto.getListaServicos()) {
			ServicoTipo servicoTipo = chamadoAssuntoServicoTipo.getServicoTipo();

			if (consultarMesmaOperacaoServicoTipo(idPontoConsumo, servicoTipo.getChavePrimaria())) {
				return Boolean.TRUE;
			}
		}

		return Boolean.FALSE;
	}
	
	
	public Boolean verificarImovelCicloLeitura(Long idPontoConsumo, Long idServicoTipo ) throws NegocioException {
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();
		
		ServicoTipo servicoTipo = fachada.consultarServicoTipo(idServicoTipo);
		PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(idPontoConsumo);
		
		if (controladorServicoAutorizacao.isServicoTipoSubstituicaoMedidor(servicoTipo) && pontoConsumo.temGrupoFaturamento()) {
			GrupoFaturamento grupo = pontoConsumo.getRota().getGrupoFaturamento();

			LeituraMovimento leituraMovimento = controladorServicoAutorizacao.pesquisarLeituraMovimentoCicloMedicao(pontoConsumo, grupo);
			
			return leituraMovimento != null;
		}
		
		return false;
	}

	public Boolean verificarRetornoDocumento(Long chavePrimaria) {
		
		ControladorComunicacao controladorComunicacao = ServiceLocator.getInstancia().getControladorComunicacao();
		Long entregaDocumento = controladorComunicacao.consultarEntregaDocumento(chavePrimaria);
		
		return entregaDocumento != null ? true : false;
	}
	
	public Boolean alterarFuncionarioAgenda(Long chaveAgendamento, Long chaveNovoFuncionario) throws GGASException {
		
		Funcionario novoFuncionario = chaveNovoFuncionario > 0l ? (Funcionario) Fachada.getInstancia().obterFuncionario(chaveNovoFuncionario) : null;
		
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia()
				.getControladorServicoAutorizacao();
		
        ServicoAutorizacaoAgenda agenda = controladorServicoAutorizacao.obterServicoAutorizacaoAgenda(chaveAgendamento);
		
        agenda.setFuncionario(novoFuncionario);
        
        controladorServicoAutorizacao.atualizarServicoAutorizacaoAgenda(agenda);
		
		return Boolean.TRUE;
	}
	
}
