
package br.com.ggas.web.medicao.consumo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.impl.FaixaConsumoVariacaoImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
/**
 * Classe responsável pelas telas relacionadas as faixas de variação de consumo 
 *
 */
@Controller
public class FaixaConsumoVariacaoAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_FAIXA_CONSUMO_VARIACAO = "exibirPesquisaFaixaConsumoVariacao";

	private static final String FAIXA_CONSUMO_VARIACAO = "faixaConsumoVariacao";

	private static final String TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO = "exibirInclusaoFaixaConsumoVariacao";

	private static final String INCLUIR = "incluir";

	private static final String TELA_ACAO = "telaAcao";

	private static final Logger LOG = Logger.getLogger(FaixaConsumoVariacaoAction.class);

	public static final String DESCRICAO = "descricao";

	public static final String CHAVE_PRIMARIA = "chavePrimaria";

	public static final String ID_SEGMENTO = "idSegmento";

	public static final String ID_SEGMENTO_FILTRO = "idSegmentoFiltro";

	public static final String DESCRICAO_FILTRO = "descricaoFiltro";

	public static final String LISTA_FAIXA_CONSUMO_VARIACAO = "listaFaixaVariacao";

	public static final String LISTA_SEGMENTO_CONSUMO_VARIACAO = "listaSegmentoVariacao";

	public static final String LISTA_SEGMENTO_FAIXA_VARIACAO_COMBO_BOX_PESQUISA = "listaSegmentoFaixaVariacao";

	public static final String GRID_IDS = "faixasHabilitadas";

	public static final String GRID_ABILITADOS = "abilitados";

	public static final String GRID_MAIORES_FAIXAS = "faixaSuperior";

	public static final String GRID_MENORES_FAIXAS = "faixaInferior";

	public static final String GRID_PERCENTUAIS_INFERIOR = "percentualInferior";

	public static final String GRID_PERCENTUAIS_SUPERIOR = "percentualSuperior";

	public static final String GRID_ALTOS_CONSUMO = "altoConsumo";

	public static final String GRID_BAIXOS_CONSUMO = "baixoConsumo";

	public static final String GRID_ESTOURO_CONSUMO = "estouroConsumo";

	public static final String FAIXA = "faixa";

	public static final String HABILITADO = "habilitado";

	public static final int MENOR_VALOR_FAIXA_INFERIOR = 0;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao;

	/**
	 * Exibir pesquisa faixa consumo variacao.
	 * 
	 * @param model
	 *            -{@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @return String -{@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_FAIXA_CONSUMO_VARIACAO)
	public String exibirPesquisaFaixaConsumoVariacao(HttpServletRequest request, Model model) throws GGASException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, null);
		}
		model.addAttribute(LISTA_SEGMENTO_FAIXA_VARIACAO_COMBO_BOX_PESQUISA,
				controladorSegmento.consultarSegmento(null));
		request.removeAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO);
		request.getSession().removeAttribute(ID_SEGMENTO);
		return TELA_EXIBIR_PESQUISA_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * Consultar faixa variacao consumo.
	 * 
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param habilitado
	 *            -{@link Boolean}
	 * @return Collection -{@link Collection}
	 * @throws NegocioException
	 *             -{@link NegocioException}
	 */
	private Collection<FaixaConsumoVariacao> consultarFaixaVariacaoConsumo(
			FaixaConsumoVariacaoImpl faixaConsumoVariacao, Boolean habilitado) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (faixaConsumoVariacao.getSegmento() != null) {
			filtro.put(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());
		}

		if (faixaConsumoVariacao.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, faixaConsumoVariacao.getChavePrimaria());
		}

		if (!"".equals(faixaConsumoVariacao.getDescricao())) {
			filtro.put("descricao", faixaConsumoVariacao.getDescricao());
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return controladorFaixaConsumoVariacao.consultarFaixasConsumoVariacao(filtro);
	}

	/**
	 * Pesquisar faixa consumo variacao.
	 * 
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param result
	 *            -{@link HttpServletRequest}
	 * @param habilitado
	 *            -{@link Boolean}
	 * @param model
	 *            -{@link Model}
	 * @return String -{@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping("pesquisarFaixaConsumoVariacao")
	public String pesquisarFaixaConsumoVariacao(FaixaConsumoVariacaoImpl faixaConsumoVariacao, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Collection<FaixaConsumoVariacao> listaFaixaVariacaoConsumoAux = pesquisarFaixaConsumoVariacao(
				faixaConsumoVariacao, habilitado);
		model.addAttribute(LISTA_SEGMENTO_FAIXA_VARIACAO_COMBO_BOX_PESQUISA,
				controladorSegmento.consultarSegmento(null));
		model.addAttribute(HABILITADO, habilitado);
		verificarObjeto(faixaConsumoVariacao, model);

		if (listaFaixaVariacaoConsumoAux.isEmpty()) {
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, null);
		} else {
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, listaFaixaVariacaoConsumoAux);
		}

		return TELA_EXIBIR_PESQUISA_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param model
	 *            -{@link Model}
	 */
	private void verificarObjeto(FaixaConsumoVariacaoImpl faixaConsumoVariacao, Model model) {
		if (!model.containsAttribute("manter")) {
			if (faixaConsumoVariacao.getSegmento() != null) {
				model.addAttribute(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());
			}
			model.addAttribute(DESCRICAO, faixaConsumoVariacao.getDescricao());
		}
	}

	/**
	 * Pesquisar faixa consumo variacao.
	 * 
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param habilitado
	 *            -{@link Boolean}
	 * @return Collection -{@link Colleaction}
	 * @throws NegocioException
	 *             -{@link NegocioException}
	 */
	private Collection<FaixaConsumoVariacao> pesquisarFaixaConsumoVariacao(
			FaixaConsumoVariacaoImpl faixaConsumoVariacao, Boolean habilitado) throws NegocioException {

		Collection<FaixaConsumoVariacao> listaFaixaVariacaoConsumoAux = new ArrayList<FaixaConsumoVariacao>();

		Collection<FaixaConsumoVariacao> listaFaixaVariacaoConsumo = consultarFaixaVariacaoConsumo(faixaConsumoVariacao,
				habilitado);
		Map<Long, Long> mapa = new HashMap<Long, Long>();

		for (FaixaConsumoVariacao faixa : listaFaixaVariacaoConsumo) {
			if (mapa.containsKey(faixa.getSegmento().getChavePrimaria())) {
				continue;
			} else {
				mapa.put(faixa.getSegmento().getChavePrimaria(), 1L);
				listaFaixaVariacaoConsumoAux.add(faixa);
			}
		}

		return listaFaixaVariacaoConsumoAux;
	}

	/**
	 * Exibir inclusao faixa consumo variacao.
	 * 
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param result
	 *            -{@link BindingResult}
	 * @param request
	 *            -{@link HttpServletRequest}
	 * @param model
	 *            -{@link Model}
	 * @param idSegmentoFiltro
	 *            - {@link Long}
	 * 
	 * @param habilitado
	 *            -{@link Boolean}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO)
	public String exibirInclusaoFaixaConsumoVariacao(FaixaConsumoVariacaoImpl faixaConsumoVariacao,
			BindingResult result, HttpServletRequest request, Model model,
			@RequestParam(value = "idSegmentoFiltro", required = false) Long idSegmentoFiltro,
			@RequestParam("habilitado") Boolean habilitado) throws GGASException {

		if (idSegmentoFiltro != null) {
			faixaConsumoVariacao.setSegmento(controladorSegmento.obterSegmento(idSegmentoFiltro));
		}

		Collection<FaixaConsumoVariacao> listaFaixaVariacaoConsumo = consultarFaixaVariacaoConsumo(faixaConsumoVariacao,
				habilitado);

		if (!listaFaixaVariacaoConsumo.isEmpty() && (idSegmentoFiltro != null && (idSegmentoFiltro > 0))) {
			model.addAttribute(DESCRICAO, listaFaixaVariacaoConsumo.iterator().next().getDescricao());
			faixaConsumoVariacao.setDescricao(listaFaixaVariacaoConsumo.iterator().next().getDescricao());
			faixaConsumoVariacao.setSegmento(listaFaixaVariacaoConsumo.iterator().next().getSegmento());
			model.addAttribute(FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacao);
			if (listaFaixaVariacaoConsumo.iterator().next().isHabilitado()) {
				model.addAttribute(HABILITADO, "true");
			} else {
				model.addAttribute(HABILITADO, "false");
			}

			request.getSession().setAttribute(TELA_ACAO, "alterar");
			model.addAttribute(ID_SEGMENTO,
					listaFaixaVariacaoConsumo.iterator().next().getSegmento().getChavePrimaria());
			/**
			 * request.getSession().setAttribute(ID_SEGMENTO,
			 * listaFaixaVariacaoConsumo.iterator().next().getSegmento().getChavePrimaria());
			 */
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
		} else {

			listaFaixaVariacaoConsumo = new ArrayList<FaixaConsumoVariacao>();
			faixaConsumoVariacao.setFaixaInferior(new BigDecimal(MENOR_VALOR_FAIXA_INFERIOR));
			faixaConsumoVariacao.setHabilitado(true);

			listaFaixaVariacaoConsumo.add(faixaConsumoVariacao);

			request.getSession().setAttribute(TELA_ACAO, INCLUIR);
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO,
					controladorSegmento.consultarSegmentoInclusaoFaixa(null));
		}

		model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, listaFaixaVariacaoConsumo);

		obterListaDadosFaixaOrdenada(listaFaixaVariacaoConsumo);
		request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, listaFaixaVariacaoConsumo);

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * Obter lista dados faixa ordenada.
	 * 
	 * @param listaNova
	 *            -{@link Collection}
	 */
	private void obterListaDadosFaixaOrdenada(Collection<FaixaConsumoVariacao> listaNova) {

		if (listaNova != null && !listaNova.isEmpty()) {

			Collections.sort((List<FaixaConsumoVariacao>) listaNova, new Comparator<FaixaConsumoVariacao>() {

				@Override
				public int compare(FaixaConsumoVariacao dados1, FaixaConsumoVariacao dados2) {

					if (dados1.getFaixaInferior().compareTo(dados2.getFaixaInferior()) < 0) {
						return -1;
					} else if (dados1.getFaixaInferior().compareTo(dados2.getFaixaInferior()) > 0) {
						return 1;
					} else {
						return 0;
					}
				}
			});
		}
	}

	/**
	 * Incluir faixa variacao consumo.
	 * 
	 * @param faixaConsumoVariacaoTmp
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param result
	 *            -{@link BindingResult}
	 * @param faixaConsumoVariacaoVO
	 *            -{@link FaixaConsumoVariacaoVO}
	 * @param results
	 *            -{@link BindingResult}
	 * @param request
	 *            -{@link HttpServletRequest}
	 * @param model
	 *            -{@link Model}
	 * @return String -{@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping("incluirFaixaVariacaoConsumo")
	public String incluirFaixaVariacaoConsumo(FaixaConsumoVariacaoImpl faixaConsumoVariacaoTmp, BindingResult result,
			FaixaConsumoVariacaoVO faixaConsumoVariacaoVO, BindingResult results, HttpServletRequest request,
			Model model) throws GGASException {

		getDadosAuditoria(request);
		model.addAttribute(FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoTmp);
		model.addAttribute(HABILITADO, faixaConsumoVariacaoTmp.isHabilitado());
		
		Long idSegmento = null;
		if (faixaConsumoVariacaoTmp.getSegmento() != null) {
			idSegmento = faixaConsumoVariacaoTmp.getSegmento().getChavePrimaria();
		}

		List<FaixaConsumoVariacao> faixaConsumoVariacaoList = null;
		if (request.getSession().getAttribute(LISTA_FAIXA_CONSUMO_VARIACAO) != null
				&& !((List<FaixaConsumoVariacao>) request.getSession().getAttribute(LISTA_FAIXA_CONSUMO_VARIACAO))
						.isEmpty()) {
			faixaConsumoVariacaoList = (List<FaixaConsumoVariacao>) request.getSession()
					.getAttribute(LISTA_FAIXA_CONSUMO_VARIACAO);
		} else {
			faixaConsumoVariacaoList = (List<FaixaConsumoVariacao>) consultarFaixaVariacaoConsumo(
					faixaConsumoVariacaoTmp, null);
		}

		FaixaConsumoVariacao ultimoItem = faixaConsumoVariacaoList.get(faixaConsumoVariacaoList.size() - 1);

		ajustarItemRemovidosLista(faixaConsumoVariacaoList, faixaConsumoVariacaoVO.getFaixaInferior());

		montarNovaListaFaixaConsumoVariacao(faixaConsumoVariacaoVO, faixaConsumoVariacaoList);

		BigDecimal ultimaFaixaSuperior = ultimoItem.getFaixaSuperior();
		BigDecimal ultimaFaixaInferior = ultimoItem.getFaixaInferior();

		if (ultimaFaixaSuperior != null
				&& ultimaFaixaSuperior.compareTo(new BigDecimal(FaixaConsumoVariacao.VALOR_MAXIMO_FAIXA)) >= 0) {
			mensagemErro(model, request, FaixaConsumoVariacao.ERRO_VALOR_MAXIMO_DA_FAIXA);

			request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
			request.setAttribute(ID_SEGMENTO, idSegmento);
			model.addAttribute(ID_SEGMENTO, idSegmento);

			return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
		}

		if (ultimaFaixaSuperior == null || ultimaFaixaSuperior.compareTo(new BigDecimal("0")) == 0) {
			mensagemErro(model, request, FaixaConsumoVariacao.ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO);

			request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			request.setAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
			request.setAttribute(ID_SEGMENTO, idSegmento);
			model.addAttribute(ID_SEGMENTO, idSegmento);

			return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
		}

		if (ultimaFaixaInferior != null && ultimaFaixaSuperior.compareTo(ultimaFaixaInferior) > 0) {
			FaixaConsumoVariacao faixaConsumoVariacao = (FaixaConsumoVariacao) controladorFaixaConsumoVariacao.criar();

			verificaListaFaixaConsumoVariacao(faixaConsumoVariacaoList, faixaConsumoVariacao);

			if (ultimoItem.getFaixaSuperior().compareTo(ultimoItem.getFaixaInferior()) < 0) {

				mensagemErro(model, request, FaixaConsumoVariacao.FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR);

				request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
				request.setAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
				request.setAttribute(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());
				model.addAttribute(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());

				return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
			}

			faixaConsumoVariacaoList.add(faixaConsumoVariacao);

		} else if (ultimaFaixaInferior != null && ultimaFaixaSuperior.compareTo(ultimaFaixaInferior) < 0) {
			FaixaConsumoVariacao faixaConsumoVariacao = (FaixaConsumoVariacao) controladorFaixaConsumoVariacao.criar();

			faixaConsumoVariacao.setHabilitado(true);
			faixaConsumoVariacao.setFaixaInferior(ultimoItem.getFaixaSuperior().add(new BigDecimal("0.1")));
			faixaConsumoVariacao.setFaixaSuperior(ultimoItem.getFaixaInferior());
			faixaConsumoVariacao.setAltoConsumo(ultimoItem.getAltoConsumo());
			faixaConsumoVariacao.setBaixoConsumo(ultimoItem.getBaixoConsumo());
			faixaConsumoVariacao.setDescricao(ultimoItem.getDescricao());
			faixaConsumoVariacao.setEstouroConsumo(ultimoItem.getEstouroConsumo());
			faixaConsumoVariacao.setPercentualInferior(ultimoItem.getPercentualInferior());
			faixaConsumoVariacao.setPercentualSuperior(ultimoItem.getPercentualSuperior());
			faixaConsumoVariacao.setSegmento(ultimoItem.getSegmento());

			if (ultimoItem.getFaixaSuperior().compareTo(ultimoItem.getFaixaInferior()) < 0) {

				mensagemErro(model, request, FaixaConsumoVariacao.FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR);

				request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
				model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
				model.addAttribute(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());
				model.addAttribute(ID_SEGMENTO, faixaConsumoVariacao.getSegmento().getChavePrimaria());

				return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;

			}

			faixaConsumoVariacaoList.add(faixaConsumoVariacao);

			obterListaDadosFaixaOrdenada(faixaConsumoVariacaoList);

			ultimoItem.setFaixaSuperior(null);
			ultimoItem.setPercentualInferior(null);
			ultimoItem.setPercentualSuperior(null);
			ultimoItem.setBaixoConsumo(null);
			ultimoItem.setAltoConsumo(null);
			ultimoItem.setEstouroConsumo(null);

			// realizando ajuste em lista após adicionar item no meio do intervalo

			ajustarValoresEmLista(faixaConsumoVariacaoList);

			int indexFaixaConsumoNovo = faixaConsumoVariacaoList.indexOf(faixaConsumoVariacao);

			if (indexFaixaConsumoNovo > 0) {
				FaixaConsumoVariacao itemAnterior = faixaConsumoVariacaoList.get(indexFaixaConsumoNovo - 1);

				FaixaConsumoVariacao itemAux = null;
				try {
					itemAux = (FaixaConsumoVariacao) faixaConsumoVariacao.clone();
				} catch (CloneNotSupportedException e) {
					LOG.error(e);
					throw new GGASException(e);
				}

				copiarDadosFaixaConsumo(itemAnterior, faixaConsumoVariacao);
				copiarDadosFaixaConsumo(itemAux, itemAnterior);
			}
		} else {

			return comparaFaixaFinalInicial(request, model, idSegmento, faixaConsumoVariacaoList);
		}

		model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
		model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
		request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
		model.addAttribute(ID_SEGMENTO, idSegmento);
		model.addAttribute(ID_SEGMENTO, idSegmento);

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param idSegmento - {@link Long}
	 * @param faixaConsumoVariacaoList - {@link List}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private String comparaFaixaFinalInicial(HttpServletRequest request, Model model, Long idSegmento,
			List<FaixaConsumoVariacao> faixaConsumoVariacaoList) throws NegocioException {

		mensagemErro(model, request, FaixaConsumoVariacao.FAIXA_SUPERIOR_IGUAL_FAIXA_INFERIOR);
		request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
		model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
		model.addAttribute(ID_SEGMENTO, idSegmento);
		model.addAttribute(ID_SEGMENTO, idSegmento);

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * 
	 * @param faixaConsumoVariacaoList
	 *            -{@link List}
	 * @param faixaConsumoVariacao
	 *            -{@link FaixaConsumoVariacao}
	 */
	private void verificaListaFaixaConsumoVariacao(List<FaixaConsumoVariacao> faixaConsumoVariacaoList,
			FaixaConsumoVariacao faixaConsumoVariacao) {
		if (!faixaConsumoVariacaoList.isEmpty()) {

			FaixaConsumoVariacao ultimaFaixaConsumo = faixaConsumoVariacaoList.get(faixaConsumoVariacaoList.size() - 1);

			BigDecimal faixaInferiorNovaFaixaConsumo = ultimaFaixaConsumo.getFaixaSuperior();

			faixaConsumoVariacao.setFaixaInferior(faixaInferiorNovaFaixaConsumo.add(new BigDecimal("0.1")));
			faixaConsumoVariacao.setHabilitado(true);
			faixaConsumoVariacao.setDescricao(ultimaFaixaConsumo.getDescricao());
			faixaConsumoVariacao.setSegmento(ultimaFaixaConsumo.getSegmento());
		}
	}

	/**
	 * Copiar dados faixa consumo.
	 * 
	 * @param faixa2
	 *            -{@link FaixaConsumoVariacao}
	 * @param paraFaixa1
	 *            -{@link FaixaConsumoVariacao}
	 */
	private void copiarDadosFaixaConsumo(FaixaConsumoVariacao faixa2, FaixaConsumoVariacao paraFaixa1) {

		if (faixa2.getAltoConsumo() != null) {
			paraFaixa1.setAltoConsumo(new BigDecimal(faixa2.getAltoConsumo().toString()));
		} else {
			paraFaixa1.setAltoConsumo(null);
		}

		if (faixa2.getBaixoConsumo() != null) {
			paraFaixa1.setBaixoConsumo(new BigDecimal(faixa2.getBaixoConsumo().toString()));
		} else {
			paraFaixa1.setBaixoConsumo(null);
		}

		if (faixa2.getEstouroConsumo() != null) {
			paraFaixa1.setEstouroConsumo(new BigDecimal(faixa2.getEstouroConsumo().toString()));
		} else {
			paraFaixa1.setEstouroConsumo(null);
		}

		if (faixa2.getPercentualInferior() != null) {
			paraFaixa1.setPercentualInferior(new BigDecimal(faixa2.getPercentualInferior().toString()));
		} else {
			paraFaixa1.setPercentualInferior(null);
		}

		if (faixa2.getPercentualSuperior() != null) {
			paraFaixa1.setPercentualSuperior(new BigDecimal(faixa2.getPercentualSuperior().toString()));
		} else {
			paraFaixa1.setPercentualSuperior(null);
		}
	}

	/**
	 * Ajustar valores em lista.
	 * 
	 * @param lista
	 *            -{@link List}
	 */
	public void ajustarValoresEmLista(List<FaixaConsumoVariacao> lista) {

		for (int x = 0; x < lista.size(); x++) {
			FaixaConsumoVariacao itemAtual = lista.get(x);

			if (x + 1 != lista.size()) {
				FaixaConsumoVariacao proximoItem = lista.get(x + 1);

				proximoItem.setFaixaInferior(itemAtual.getFaixaSuperior().add(new BigDecimal("0.1")));

			}
		}

		// o primeiro item da lista sempre começa com zero
		if (!lista.isEmpty() && lista.get(0) != null) {
			lista.get(0).setFaixaInferior(new BigDecimal(0));
		}
	}

	/**
	 * Remover faixa consumo variacao.
	 * 
	 * @param faixaConsumoVariacaoTmp
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param result
	 *            -{@link BindingResult}
	 * @param faixaConsumoVariacaoVO
	 *            -{@link FaixaConsumoVariacaoVO}
	 * @param request
	 *            -{@link HttpServletRequest}
	 * @param idSegmentoFiltro
	 *            -{@link Long}
	 * @param model
	 *            -{@link Model}
	 * @return String -{@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping("removerFaixaConsumoVariacao")
	public String removerFaixaConsumoVariacao(FaixaConsumoVariacaoImpl faixaConsumoVariacaoTmp, BindingResult result,
			FaixaConsumoVariacaoVO faixaConsumoVariacaoVO, HttpServletRequest request,
			@RequestParam(ID_SEGMENTO_FILTRO) Long idSegmentoFiltro, Model model) throws GGASException {

		String faixaInferiorExcluir = request.getParameter("faixaInicialExclusao").toString();
		model.addAttribute(FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoTmp);
		List<FaixaConsumoVariacao> faixaConsumoVariacaoList = (ArrayList<FaixaConsumoVariacao>) request.getSession()
				.getAttribute(LISTA_FAIXA_CONSUMO_VARIACAO);

		montarNovaListaFaixaConsumoVariacao(faixaConsumoVariacaoVO, faixaConsumoVariacaoList);

		for (int x = 0; x < faixaConsumoVariacaoList.size(); x++) {
			FaixaConsumoVariacao faixaConsumoVariacao = faixaConsumoVariacaoList.get(x);

			if (faixaConsumoVariacao.getFaixaInferior().compareTo(new BigDecimal(faixaInferiorExcluir)) == 0) {
				faixaConsumoVariacaoList.remove(x);
				break;
			}
		}

		if (faixaConsumoVariacaoList.size() == 1) {
			faixaConsumoVariacaoList.get(0).setFaixaInferior(new BigDecimal(0));
		}

		request.setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
		request.setAttribute(ID_SEGMENTO, idSegmentoFiltro);
		request.setAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
		model.addAttribute(ID_SEGMENTO, idSegmentoFiltro);

		obterListaDadosFaixaOrdenada(faixaConsumoVariacaoList);
		ajustarValoresEmLista(faixaConsumoVariacaoList);

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * Salvar faixa variacao consumo.
	 * 
	 * @param faixaConsumoVariacaoTmp
	 *            -{@link FaixaConsumoVariacaoImpl}
	 * @param results
	 *            -{@link BindingResult}
	 * @param faixaConsumoVariacaoVO
	 *            -{@link FaixaConsumoVariacaoVO}
	 * @param result
	 *            -{@link BindingResult}
	 * @param request
	 *            -{@link HttpServletRequest}
	 * @param model
	 *            -{@link Model}
	 * @param idSegmentoFiltro
	 *            -{@link Long}
	 * @return String -{@link String}
	 * @throws GGASException
	 *             -{@link GGASException}
	 */
	@RequestMapping("salvarFaixaVariacaoConsumo")
	public String salvarFaixaVariacaoConsumo(FaixaConsumoVariacaoImpl faixaConsumoVariacaoTmp, BindingResult results,
			FaixaConsumoVariacaoVO faixaConsumoVariacaoVO, BindingResult result,
			@RequestParam(ID_SEGMENTO_FILTRO) Long idSegmentoFiltro, HttpServletRequest request, Model model)
			throws GGASException {

		String view;
		Long idSegmento = null;
		if (faixaConsumoVariacaoTmp.getSegmento() != null) {
			idSegmento = faixaConsumoVariacaoTmp.getSegmento().getChavePrimaria();
		}
		
		String inclusaoAlteracao = request.getParameter("inclusaoAlteracao");
		model.addAttribute(FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoTmp);
		List<FaixaConsumoVariacao> faixaConsumoVariacaoList = (List<FaixaConsumoVariacao>) request.getSession()
				.getAttribute(LISTA_FAIXA_CONSUMO_VARIACAO);

		try {
			ajustarItemRemovidosLista(faixaConsumoVariacaoList, faixaConsumoVariacaoVO.getFaixaInferior());

			String erroNegocioString = montarNovaListaFaixaConsumoVariacao(faixaConsumoVariacaoVO,
					faixaConsumoVariacaoList);

			Segmento segmento = null;

			if (faixaConsumoVariacaoTmp.getSegmento() != null
					&& faixaConsumoVariacaoTmp.getSegmento().getChavePrimaria() != -1) {
				segmento = controladorSegmento.obterSegmento(faixaConsumoVariacaoTmp.getSegmento().getChavePrimaria());
			}

			if (!"".equals(erroNegocioString)) {
				mensagemErroParametrizado(model, erroNegocioString, request);

				model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);

				throw new NegocioException();
			}

			Map<String, Object> filtro2 = new HashMap<String, Object>();

			if (faixaConsumoVariacaoTmp.getSegmento() != null) {
				filtro2.put(ID_SEGMENTO, faixaConsumoVariacaoTmp.getSegmento().getChavePrimaria());
			}

			if (!controladorFaixaConsumoVariacao.consultarFaixasConsumoVariacao(filtro2).isEmpty()
					&& "-1".equalsIgnoreCase(inclusaoAlteracao)) {
				mensagemErro(model, request, FaixaConsumoVariacao.ERRO_FAIXA_VARIACAO_CONSUMO_JA_EXISTENTE);

				model.addAttribute(DESCRICAO, "");

				return pesquisarFaixaConsumoVariacao(faixaConsumoVariacaoTmp, result,
						faixaConsumoVariacaoTmp.isHabilitado(), model);
			}

			Map<Long, Long> mapa = new HashMap<Long, Long>();
			for (int x = 0; x < faixaConsumoVariacaoList.size(); x++) {
				FaixaConsumoVariacao faixaConsumoVariacao = faixaConsumoVariacaoList.get(x);

				faixaConsumoVariacao.setSegmento(segmento);
				faixaConsumoVariacao.setDescricao(faixaConsumoVariacaoTmp.getDescricao());
				if (INCLUIR.equalsIgnoreCase(request.getSession().getAttribute(TELA_ACAO).toString())) {
					faixaConsumoVariacao.setHabilitado(true);
				} else {
					faixaConsumoVariacao.setHabilitado(faixaConsumoVariacaoTmp.isHabilitado());
				}

				Map<String, Object> erros = faixaConsumoVariacao.validarDados();

				if (!erros.isEmpty()) {
					String parametrosMensagemErro = MensagemUtil.obterMensagemErro(
							erros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS).toString(),
							Constantes.LOCALE_PADRAO);

					view = verificaSuperiorMenorInferior(faixaConsumoVariacaoVO, request, model, idSegmento,
							faixaConsumoVariacaoList, faixaConsumoVariacao, erros);

					view = verificaValorMaximoFaixa(faixaConsumoVariacaoVO, request, model, idSegmento,
							faixaConsumoVariacaoList, faixaConsumoVariacao, erros);

					view = verificaInferiorIgualSuperior(idSegmentoFiltro, request, model, faixaConsumoVariacaoList,
							faixaConsumoVariacao);

					if (!model.containsAttribute("msg")) {
						mensagemErroParametrizado(model, Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, request,
								new Object[] { parametrosMensagemErro });
					}

					model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
					model.addAttribute(DESCRICAO, faixaConsumoVariacao.getDescricao());
					model.addAttribute(ID_SEGMENTO, idSegmento);
					model.addAttribute(GRID_MENORES_FAIXAS, faixaConsumoVariacaoVO.getFaixaInferior());
					request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
					request.getSession().setAttribute(ID_SEGMENTO, idSegmento);
					model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));

					return view;
				}

				if (faixaConsumoVariacao.getChavePrimaria() > 0) {
					controladorFaixaConsumoVariacao.atualizar(faixaConsumoVariacao);
				} else {
					controladorFaixaConsumoVariacao.inserir(faixaConsumoVariacao);
				}

				mapa.put(faixaConsumoVariacao.getChavePrimaria(), faixaConsumoVariacao.getChavePrimaria());
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavePrimaria", idSegmento);

			model.addAttribute(LISTA_SEGMENTO_FAIXA_VARIACAO_COMBO_BOX_PESQUISA,
					controladorSegmento.consultarSegmento(null));
			request.removeAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO);

			if (request.getSession().getAttribute(TELA_ACAO) != null
					&& INCLUIR.equalsIgnoreCase(request.getSession().getAttribute(TELA_ACAO).toString())) {
				mensagemSucesso(model, FaixaConsumoVariacao.MSG_SALVAR_SUCESSO_FAIXA_CONSUMO_VARIACAO);
				faixaConsumoVariacaoTmp.setHabilitado(true);
			} else {
				mensagemSucesso(model, FaixaConsumoVariacao.MSG_ATUALIZAR_SUCESSO_FAIXA_CONSUMO_VARIACAO);
			}

			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO,
					pesquisarFaixaConsumoVariacao(faixaConsumoVariacaoTmp, faixaConsumoVariacaoTmp.isHabilitado()));
			model.addAttribute(ID_SEGMENTO, -1L);

			removerFaixas(mapa, segmento);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("manter", false);
		return TELA_EXIBIR_PESQUISA_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * @param faixaConsumoVariacaoVO
	 * @param request
	 * @param model
	 * @param idSegmento
	 * @param faixaConsumoVariacaoList
	 * @param faixaConsumoVariacao
	 * @param erros
	 * @throws NegocioException
	 */
	private String verificaSuperiorMenorInferior(FaixaConsumoVariacaoVO faixaConsumoVariacaoVO,
			HttpServletRequest request, Model model, Long idSegmento,
			List<FaixaConsumoVariacao> faixaConsumoVariacaoList, FaixaConsumoVariacao faixaConsumoVariacao,
			Map<String, Object> erros) throws NegocioException {

		if (erros.containsValue(FaixaConsumoVariacao.FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR)) {
			mensagemErro(model, request, FaixaConsumoVariacao.FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR);

			model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			model.addAttribute(DESCRICAO, faixaConsumoVariacao.getDescricao());
			model.addAttribute(ID_SEGMENTO, idSegmento);
			model.addAttribute(GRID_MENORES_FAIXAS, faixaConsumoVariacaoVO.getFaixaInferior());
			request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			request.getSession().setAttribute(ID_SEGMENTO, idSegmento);
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));

		}
		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * @param faixaConsumoVariacaoVO
	 * @param request
	 * @param model
	 * @param idSegmento
	 * @param faixaConsumoVariacaoList
	 * @param faixaConsumoVariacao
	 * @param erros
	 * @throws NegocioException
	 */
	private String verificaValorMaximoFaixa(FaixaConsumoVariacaoVO faixaConsumoVariacaoVO, HttpServletRequest request,
			Model model, Long idSegmento, List<FaixaConsumoVariacao> faixaConsumoVariacaoList,
			FaixaConsumoVariacao faixaConsumoVariacao, Map<String, Object> erros) throws NegocioException {

		if (erros.containsValue(FaixaConsumoVariacao.ERRO_VALOR_MAXIMO_DA_FAIXA) && (!model.containsAttribute("msg"))) {
			mensagemErro(model, request, FaixaConsumoVariacao.ERRO_VALOR_MAXIMO_DA_FAIXA);

			model.addAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			model.addAttribute(DESCRICAO, faixaConsumoVariacao.getDescricao());
			model.addAttribute(ID_SEGMENTO, idSegmento);
			model.addAttribute(GRID_MENORES_FAIXAS, faixaConsumoVariacaoVO.getFaixaInferior());
			request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));

		}

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * @param idSegmentoFiltro
	 * @param request
	 * @param model
	 * @param faixaConsumoVariacaoList
	 * @param faixaConsumoVariacao
	 * @throws NegocioException
	 */
	private String verificaInferiorIgualSuperior(Long idSegmentoFiltro, HttpServletRequest request, Model model,
			List<FaixaConsumoVariacao> faixaConsumoVariacaoList, FaixaConsumoVariacao faixaConsumoVariacao)
			throws NegocioException {

		if (faixaConsumoVariacao.getFaixaInferior() != null && faixaConsumoVariacao.getFaixaSuperior() != null
				&& faixaConsumoVariacao.getFaixaInferior().compareTo(faixaConsumoVariacao.getFaixaSuperior()) == 0 && !model.containsAttribute("msg")) {

			mensagemErro(model, request, FaixaConsumoVariacao.FAIXA_SUPERIOR_IGUAL_FAIXA_INFERIOR);

			request.getSession().setAttribute(LISTA_FAIXA_CONSUMO_VARIACAO, faixaConsumoVariacaoList);
			model.addAttribute(LISTA_SEGMENTO_CONSUMO_VARIACAO, controladorSegmento.consultarSegmento(null));
			request.setAttribute(ID_SEGMENTO, idSegmentoFiltro);
			model.addAttribute(ID_SEGMENTO, idSegmentoFiltro);

		}

		return TELA_EXIBIR_INCLUSAO_FAIXA_CONSUMO_VARIACAO;
	}

	/**
	 * Remover faixas.
	 * 
	 * @param mapa
	 *            -{@link Map}
	 * @param segmento
	 *            -{@link Segmento}
	 * @throws NegocioException
	 *             -{@link NegocioException}
	 */
	private void removerFaixas(Map<Long, Long> mapa, Segmento segmento) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_SEGMENTO, segmento.getChavePrimaria());

		Collection<FaixaConsumoVariacao> listaOriginal = controladorFaixaConsumoVariacao
				.consultarFaixasConsumoVariacao(filtro);

		for (FaixaConsumoVariacao faixa : listaOriginal) {
			if (!mapa.containsKey(faixa.getChavePrimaria())) {
				controladorFaixaConsumoVariacao.remover(faixa);
			}
		}

	}

	/**
	 * sincronizar dados que vem da tela com registros que ja foram removidos
	 * 
	 * @param faixaConsumoList
	 *            -{@link List}
	 * @param ids
	 *            -{@link String}
	 */
	private void ajustarItemRemovidosLista(List<FaixaConsumoVariacao> faixaConsumoList, String[] ids) {

		for (int x = 0; x < faixaConsumoList.size(); x++) {
			FaixaConsumoVariacao faixaConsumoVariacaoCurrent = faixaConsumoList.get(x);

			Boolean itemExiste = existeIdListaIds(faixaConsumoVariacaoCurrent.getFaixaInferior().toString(), ids);

			if (!itemExiste) {
				faixaConsumoList.remove(faixaConsumoVariacaoCurrent);
			}
		}
	}

	/**
	 * Existe id lista ids.
	 * 
	 * @param id
	 *            -{@link String}
	 * @param ids
	 *            -{@link String}
	 * @return Boolean -{@link Boolean}
	 */
	private Boolean existeIdListaIds(String id, String[] ids) {

		if (ids.length == 0) {
			return true;
		}

		for (String currentId : ids) {
			if (currentId.equals(id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Montar nova lista faixa consumo variacao.
	 * 
	 * @param faixaConsumoVariacaoVO
	 *            -{@link FaixaConsumoVariacaoVO}
	 * @param faixaConsumoVariacaoList
	 *            -{@link List}
	 * @return String -{@link String}
	 */
	private String montarNovaListaFaixaConsumoVariacao(FaixaConsumoVariacaoVO faixaConsumoVariacaoVO,
			List<FaixaConsumoVariacao> faixaConsumoVariacaoList) {

		String erroString = "";

		int count = 0;
		for (FaixaConsumoVariacao faixaConsumoVariacao : faixaConsumoVariacaoList) {

			if (faixaConsumoVariacaoVO.getAltoConsumo().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getAltoConsumo()[count])) {
				faixaConsumoVariacao.setAltoConsumo(new BigDecimal(faixaConsumoVariacaoVO.getAltoConsumo()[count]));
			} else {
				faixaConsumoVariacao.setAltoConsumo(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getBaixoConsumo().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getBaixoConsumo()[count])) {
				faixaConsumoVariacao.setBaixoConsumo(new BigDecimal(faixaConsumoVariacaoVO.getBaixoConsumo()[count]));
			} else {
				faixaConsumoVariacao.setBaixoConsumo(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getEstouroConsumo().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getEstouroConsumo()[count])) {
				faixaConsumoVariacao
						.setEstouroConsumo(new BigDecimal(faixaConsumoVariacaoVO.getEstouroConsumo()[count]));
			} else {
				faixaConsumoVariacao.setEstouroConsumo(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getFaixaSuperior().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getFaixaSuperior()[count])) {
				faixaConsumoVariacao.setFaixaSuperior(
						new BigDecimal(faixaConsumoVariacaoVO.getFaixaSuperior()[count].toString().replace(",", ".")));
			} else {
				faixaConsumoVariacao.setFaixaSuperior(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getFaixaInferior().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getFaixaInferior()[count])) {
				faixaConsumoVariacao.setFaixaInferior(
						new BigDecimal(faixaConsumoVariacaoVO.getFaixaInferior()[count].toString().replace(",", ".")));
			} else {
				faixaConsumoVariacao.setFaixaInferior(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getPercentualInferior().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getPercentualInferior()[count])) {
				faixaConsumoVariacao
						.setPercentualInferior(new BigDecimal(faixaConsumoVariacaoVO.getPercentualInferior()[count]));
			} else {
				faixaConsumoVariacao.setPercentualInferior(BigDecimal.ZERO);
			}

			if (faixaConsumoVariacaoVO.getPercentualSuperior().length > 0
					&& !"".equals(faixaConsumoVariacaoVO.getPercentualSuperior()[count])) {
				faixaConsumoVariacao
						.setPercentualSuperior(new BigDecimal(faixaConsumoVariacaoVO.getPercentualSuperior()[count]));
			} else {
				faixaConsumoVariacao.setPercentualSuperior(BigDecimal.ZERO);
			}

			count++;
		}

		return erroString;
	}
	/**
	 * método responsável pela reotorno a tela de pesquisa
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("cancelar")
	public String cancelar(HttpServletRequest request) {
		request.getSession().removeAttribute(ID_SEGMENTO);
		
		return "forward:pesquisarFaixaConsumoVariacao";
	}

}
