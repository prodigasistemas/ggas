/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.vazaocorretor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoMovimentacao;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.leitura.PontoConsumoVO;

/**
 * 
 * A classe VazaoCorretorHistoricoAction representa uma VazaoCorretorHistoricoAction no sistema.
 *
 * @since 22/09/2009
 *
 */
@Controller
public class HistoricoVazaoCorretorAction extends GenericAction {

	private static final String NUMERO_SERIE_PARCIAL = "numeroSerieParcial";

	private static final String EXIBIR_DETALHAMENTO_HISTORICO_VAZAO_IMOVEL = "exibirDetalhamentoHistoricoVazaoImovel";

	private static final String INDICADOR_PESQUISA = "indicadorPesquisa";

	private static final String DADOS_PESQUISA = "dadosPesquisa";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_HISTORICO_CORRETOR_VAZAO = "listaVazaoCorretorHistorico";

	private static final String ID_MARCA = "idMarca";

	private static final String DESCRICAO_MODELO = "descricaoModelo";

	private static final String LISTA_VAZAO = "listaVazao";

	private static final String LISTA_MARCA = "listaMarca";

	private static final String LISTA_MODELO = "listaModelo";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String ID_MODELO = "idModelo";

	private static final String NOME = "nome";

	private static final String MATRICULA = "matricula";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";

	private static final String CORRETOR_VAZAO = "vazao";

	private static final String LISTA_VAZAO_CORRETOR_HISTORICO_MOVIMENTACAO = "listaVazaoCorretorHistoricoMovimentacao";

	private static final String LAZY_ROTA_GRUPO_FATURAMENTO = "rota.grupoFaturamento";

	private static final String LAZY_ROTA_EMPRESA = "rota.empresa";

	private static final String IMOVEL = "imovel";

	private static final String CHAVES_PRIMARIAS_IMOVEIS = "chavesPrimariasImoveis";

	private static final String LISTA_PONTOS_CONSUMO = "listaPontosConsumo";

	private static final String LAZY_LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String PONTO_CONSUMO = "pontoConsumo";


	@Autowired
	@Qualifier("controladorVazaoCorretor")
	private ControladorVazaoCorretor controladorVazaoCorretor;
	
	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;
	
	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;
	
	/**
	 * 
	 * Método responsável por exibir a tela de pesquisa de vazão corretor.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link Exception}
	 * 
	 */
	@RequestMapping("exibirPesquisaHistoricoVazaoCorretor")
	public String exibirPesquisaHistoricoVazaoCorretor(Model model, HttpServletRequest request)
			throws NegocioException {

		request.getSession().removeAttribute(DADOS_PESQUISA);

		if (!model.containsAttribute(INDICADOR_PESQUISA)) {
			model.addAttribute(INDICADOR_PESQUISA, "indicadorPesquisaVazao");
		}
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}
		if (!model.containsAttribute(INDICADOR_CONDOMINIO_AMBOS)) {
			model.addAttribute(INDICADOR_CONDOMINIO_AMBOS, null);
		}

		model.addAttribute(LISTA_MARCA, controladorVazaoCorretor.listarMarcaVazaoCorretor());
		model.addAttribute(LISTA_MODELO, controladorVazaoCorretor.listarModeloVazaoCorretor());

		return "exibirPesquisaHistoricoVazaoCorretor";
	}

	/**
	 * 
	 * Pesquisar historico vazao corretor.
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@ling Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * 
	 */
	@RequestMapping("pesquisarHistoricoVazaoCorretor")
	public String pesquisarHistoricoVazaoCorretor(HistoricoVazaoCorretorVO historicoVazaoCorretorVO,
			BindingResult results, HttpServletRequest request, Model model) throws NegocioException {

		Map<String, Object> filtro = null;
		
		if(historicoVazaoCorretorVO != null) {
			model.addAttribute(INDICADOR_PESQUISA, historicoVazaoCorretorVO.getIndicadorPesquisa());
			filtro = prepararFiltro(historicoVazaoCorretorVO);
		}
		
		Collection<VazaoCorretor> listaVazao = controladorVazaoCorretor.consultarVazaoCorretor(filtro);

		model.addAttribute(LISTA_VAZAO, listaVazao);
		model.addAttribute(CORRETOR_VAZAO, historicoVazaoCorretorVO);

		return exibirPesquisaHistoricoVazaoCorretor(model, request);
	}

	/**
	 * 
	 * Exibir detalhamento historico vazao corretor.
	 * 
	 * @param vazaoCorretorTmp
	 *            {@link VazaoCorretorImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * 
	 */
	@RequestMapping("exibirDetalhamentoHistoricoVazaoCorretor")
	public String exibirDetalhamentoHistoricoVazaoCorretor(VazaoCorretorImpl vazaoCorretorTmp, BindingResult result,
			HistoricoVazaoCorretorVO historicoVazaoCorretorVO, BindingResult results, HttpServletRequest request,
			Model model) throws NegocioException {

		salvarPesquisa(request, historicoVazaoCorretorVO, DADOS_PESQUISA);
		VazaoCorretor vazao = controladorMedidor.obterVazaoCorretor(historicoVazaoCorretorVO.getChavePrimaria());

		Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretorHistorico = null;
		Collection<VazaoCorretorHistoricoMovimentacao> listaVazaoCorretorHistoricoMovimentacao = null;

		listaVazaoCorretorHistorico = controladorVazaoCorretor
				.listarHistoricoCorretorVazao(historicoVazaoCorretorVO.getChavePrimaria());
		listaVazaoCorretorHistoricoMovimentacao = controladorVazaoCorretor
				.listarVazaoCorretorHistoricoMovimentacao(historicoVazaoCorretorVO.getChavePrimaria());

		model.addAttribute(LISTA_HISTORICO_CORRETOR_VAZAO, listaVazaoCorretorHistorico);
		model.addAttribute(LISTA_VAZAO_CORRETOR_HISTORICO_MOVIMENTACAO, listaVazaoCorretorHistoricoMovimentacao);
		model.addAttribute(CORRETOR_VAZAO, vazao);

		return "exibirDetalhamentoHistoricoVazaoCorretor";
	}

	/**
	 * 
	 * Exibir detalhamento historico operacao vazao corretor.
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * 
	 */
	@RequestMapping("exibirDetalhamentoHistoricoOperacaoVazaoCorretor")
	public String exibirDetalhamentoHistoricoOperacaoVazaoCorretor(HistoricoVazaoCorretorVO historicoVazaoCorretorVO,
			BindingResult result, HttpServletRequest request, Model model) throws NegocioException {

		VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor
				.obter(historicoVazaoCorretorVO.getChavePrimaria());

		Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretorHistorico = null;

		listaVazaoCorretorHistorico = controladorVazaoCorretor
				.listarHistoricoCorretorVazao(vazaoCorretor.getChavePrimaria());

		model.addAttribute(LISTA_HISTORICO_CORRETOR_VAZAO, listaVazaoCorretorHistorico);
		model.addAttribute(CORRETOR_VAZAO, vazaoCorretor);

		return "exibirDetalhamentoHistoricoOperacaoVazaoCorretor";
	}

	/**
	 * 
	 * Pesquisar historico vazao imovel.
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * 
	 */
	@RequestMapping("pesquisarHistoricoVazaoImovel")
	public String pesquisarHistoricoVazaoImovel(HistoricoVazaoCorretorVO historicoVazaoCorretorVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			validarCampos(historicoVazaoCorretorVO, historicoVazaoCorretorVO.getMatricula());

			Map<String, Object> filtro = prepararFiltroImovel(historicoVazaoCorretorVO);
			adicionarFiltroPaginacao(request, filtro);
			Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);
			model.addAttribute("listaImoveis", criarColecaoPaginada(request, filtro, listaImoveis));

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(INDICADOR_PESQUISA, historicoVazaoCorretorVO.getIndicadorPesquisa());
		model.addAttribute(MATRICULA, historicoVazaoCorretorVO.getMatricula());
		model.addAttribute("imovel", historicoVazaoCorretorVO);

		model.addAttribute(INDICADOR_CONDOMINIO_AMBOS, historicoVazaoCorretorVO.getCondominio());
		model.addAttribute(HABILITADO, historicoVazaoCorretorVO.getHabilitado());

		return exibirPesquisaHistoricoVazaoCorretor(model, request);
	}

	/**
	 * 
	 * Método responsável por verificar se os campos Matrícula e Número do Imóvel
	 * possuem caracteres especiais
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param matricula
	 *            {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * 
	 */
	private void validarCampos(HistoricoVazaoCorretorVO historicoVazaoCorretorVO, String matricula)
			throws GGASException {

		if (!matricula.isEmpty()) {
			historicoVazaoCorretorVO
					.setChavePrimaria(Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matricula));
		}
		if (!historicoVazaoCorretorVO.getNumeroImovel().isEmpty()) {
			Util.converterCampoStringParaValorLong(Imovel.NUMERO_IMOVEL, historicoVazaoCorretorVO.getNumeroImovel());
		}

	}

	/**
	 * 
	 * Exibir detalhamento historico vazao imovel.
	 * 
	 * @param imovelTmp
	 *            {@link ImovelImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return {@link String}
	 * @throws GGASException
	 *             {@link NegocioException}
	 * 
	 */
	@RequestMapping(EXIBIR_DETALHAMENTO_HISTORICO_VAZAO_IMOVEL)
	public String exibirDetalhamentoHistoricoVazaoImovel(ImovelImpl imovelTmp, BindingResult result,
			HistoricoVazaoCorretorVO historicoVazaoCorretorVO, BindingResult results, HttpServletRequest request,
			Model model) throws GGASException {

		Imovel imovel = null;
		Collection<PontoConsumoVO> listaPontosConsumoVO = null;
		String view = "forward:pesquisarHistoricoVazaoImovel";
		salvarPesquisa(request, historicoVazaoCorretorVO, DADOS_PESQUISA);

		if (historicoVazaoCorretorVO.getIdImovel() != null && historicoVazaoCorretorVO.getIdImovel() > 0) {

			try {
				imovel = (Imovel) controladorImovel.obter(historicoVazaoCorretorVO.getIdImovel(),
						LAZY_LISTA_PONTO_CONSUMO);
				Collection<PontoConsumo> listaPontosConsumo = consultarPontosConsumoImovel(imovel);
				controladorImovel.validarImovelSemPontoConsumo(listaPontosConsumo);
				listaPontosConsumoVO = montarListaPontosConsumoVo(listaPontosConsumo);

				model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
				model.addAttribute(IMOVEL, imovel);

				view = EXIBIR_DETALHAMENTO_HISTORICO_VAZAO_IMOVEL;

			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
			}
		}

		return view;
	}

	/**
	 * 
	 * Exibir detalhamento historico vazao corretor imovel.
	 * 
	 * @param imovelTmp
	 *            {@link ImovelImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * 
	 */
	@RequestMapping("exibirDetalhamentoHistoricoVazaoCorretorImovel")
	public String exibirDetalhamentoHistoricoVazaoCorretorImovel(ImovelImpl imovelTmp, BindingResult result,
			HistoricoVazaoCorretorVO historicoVazaoCorretorVO, BindingResult results, HttpServletRequest request,
			Model model) throws GGASException {

		salvarPesquisa(request, historicoVazaoCorretorVO, DADOS_PESQUISA);
		PontoConsumo pontoConsumo = null;

		if (historicoVazaoCorretorVO.getIdPontoConsumo() != null && historicoVazaoCorretorVO.getIdPontoConsumo() > 0) {

			pontoConsumo = controladorPontoConsumo.buscarPontoConsumo(historicoVazaoCorretorVO.getIdPontoConsumo());
			model.addAttribute(PONTO_CONSUMO, pontoConsumo);

			Collection<VazaoCorretorHistoricoOperacao> listaHistoricoOperacaoVazaoCorretor = controladorVazaoCorretor
					.consultarHistoricoOperacaoVazaoCorretor(null, historicoVazaoCorretorVO.getIdPontoConsumo());

			model.addAttribute(LISTA_HISTORICO_CORRETOR_VAZAO, listaHistoricoOperacaoVazaoCorretor);

		}

		Imovel imovel = null;

		if (historicoVazaoCorretorVO.getIdImovel() != null && historicoVazaoCorretorVO.getIdImovel() > 0) {
			imovel = (Imovel) controladorImovel.obter(historicoVazaoCorretorVO.getIdImovel(), LAZY_LISTA_PONTO_CONSUMO);
			model.addAttribute(IMOVEL, imovel);
			Collection<PontoConsumo> listaPontosConsumo = consultarPontosConsumoImovel(imovel);
			Collection<PontoConsumoVO> listaPontosConsumoVO = montarListaPontosConsumoVo(listaPontosConsumo);

			if (listaPontosConsumoVO != null && !listaPontosConsumoVO.isEmpty()) {
				model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
			}
		}

		return EXIBIR_DETALHAMENTO_HISTORICO_VAZAO_IMOVEL;
	}

	/**
	 * 
	 * Consultar pontos consumo imovel.
	 * 
	 * @param imovel
	 *            {@link Imovel}
	 * @return listaPontosConsumo {@link Collection}
	 * @throws GGASException
	 *             {@link GGASException}
	 * 
	 */
	private Collection<PontoConsumo> consultarPontosConsumoImovel(Imovel imovel) throws GGASException {

		Collection<PontoConsumo> listaPontosConsumo = new ArrayList<PontoConsumo>();

		if (imovel != null) {
			Collection<PontoConsumo> pontosDeConsumo = controladorImovel
					.listarPontoConsumoPorChaveImovel(imovel.getChavePrimaria());
			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}
			if (imovel.getCondominio()) {
				Collection<Imovel> listaImoveisCondominio = controladorImovel.obterImoveisFilhosDoCondominio(imovel);

				verificaListaCondominioAdicionaPontoConsumo(listaPontosConsumo, listaImoveisCondominio);

			}
		}

		return listaPontosConsumo;
	}

	/**
	 * 
	 * Método responsável por vericar uma lista de Codominios e adicionar pontos de
	 * Consumos realcionados a mesma.
	 * 
	 * @param listaPontosConsumo
	 *            {@link Collection}
	 * @param listaImoveisCondominio
	 *            {@link Collection}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * 
	 */
	public void verificaListaCondominioAdicionaPontoConsumo(Collection<PontoConsumo> listaPontosConsumo,
			Collection<Imovel> listaImoveisCondominio) throws NegocioException {

		Collection<PontoConsumo> pontosDeConsumo;

		if (listaImoveisCondominio != null && !listaImoveisCondominio.isEmpty()) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS_IMOVEIS, Util.collectionParaArrayChavesPrimarias(listaImoveisCondominio));
			pontosDeConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);

			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}

		}
	}

	/**
	 * 
	 * Montar lista pontos consumo VO.
	 * 
	 * @param listaPontosConsumo
	 *            {@link Collection}
	 * @return listaPontosConsumoVO {@link Collection}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * 
	 */
	private Collection<PontoConsumoVO> montarListaPontosConsumoVo(Collection<PontoConsumo> listaPontosConsumo)
			throws NegocioException {

		Collection<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			pontoVO = new PontoConsumoVO();

			PontoConsumo pontoConsumoCompleto = (PontoConsumo) controladorPontoConsumo.obter(
					pontoConsumo.getChavePrimaria(), LAZY_ROTA_GRUPO_FATURAMENTO, LAZY_ROTA_EMPRESA,
					"instalacaoMedidor.medidor", "situacaoConsumo");
			pontoVO.setPontoConsumo(pontoConsumoCompleto);

			Map<String, Integer> referenciaCiclo = controladorPontoConsumo
					.obterReferenciaCicloAtual(pontoConsumoCompleto);

			if (referenciaCiclo != null) {
				pontoVO.setCiclo(String.valueOf(referenciaCiclo.get("ciclo")));
				pontoVO.setReferencia(Util.formatarAnoMes(referenciaCiclo.get("referencia")));
			}

			listaPontosConsumoVO.add(pontoVO);
		}
		return listaPontosConsumoVO;
	}
	
	/**
	 * 
	 * Método responsável por manter os dados de uma pesquisa após voltar de uma
	 * ação de detalhamento
	 *
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @param result
	 *            {@linkBindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * 
	 */
	@RequestMapping("voltarHistorico")
	public String voltar(HistoricoVazaoCorretorVO historicoVazaoCorretorVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		HistoricoVazaoCorretorVO historicoVazaoCorretor = null;

		if (historicoVazaoCorretorVO != null) {
			historicoVazaoCorretor = (HistoricoVazaoCorretorVO) retornaPesquisa(request, DADOS_PESQUISA);
		}
		if (historicoVazaoCorretor != null
				&& "indicadorPesquisaImovel".equals(historicoVazaoCorretor.getIndicadorPesquisa())) {
			return pesquisarHistoricoVazaoImovel(historicoVazaoCorretor, result, request, model);
		} else {
			return pesquisarHistoricoVazaoCorretor(historicoVazaoCorretor, result, request, model);
		}
	}

	/**
	 * 
	 * Preparar filtro.
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @return filtro {@link Map}
	 * 
	 */
	private Map<String, Object> prepararFiltro(HistoricoVazaoCorretorVO historicoVazaoCorretorVO) {

		Map<String, Object> filtro = new HashMap<>();

		if (historicoVazaoCorretorVO.getNumeroSerie() != null && !historicoVazaoCorretorVO.getNumeroSerie().isEmpty()) {
			filtro.put(NUMERO_SERIE_PARCIAL, historicoVazaoCorretorVO.getNumeroSerie());
		}

		if (historicoVazaoCorretorVO.getModelo() != null && !historicoVazaoCorretorVO.getModelo().isEmpty()) {
			filtro.put(DESCRICAO_MODELO, historicoVazaoCorretorVO.getModelo());
		}

		if (historicoVazaoCorretorVO.getIdModelo() != null && historicoVazaoCorretorVO.getIdModelo() > 0) {
			filtro.put(ID_MODELO, historicoVazaoCorretorVO.getIdModelo());
		}

		if (historicoVazaoCorretorVO.getIdMarca() != null && historicoVazaoCorretorVO.getIdMarca() > 0) {
			filtro.put(ID_MARCA, historicoVazaoCorretorVO.getIdMarca());
		}

		if (historicoVazaoCorretorVO.getHabilitado() != null) {
			filtro.put(HABILITADO, historicoVazaoCorretorVO.getHabilitado());
		}

		return filtro;
	}

	/**
	 * 
	 * Preparar filtro imovel.
	 * 
	 * @param historicoVazaoCorretorVO
	 *            {@link HistoricoVazaoCorretorVO}
	 * @return filtro {@link Map}
	 * 
	 */
	private Map<String, Object> prepararFiltroImovel(HistoricoVazaoCorretorVO historicoVazaoCorretorVO) {

		Map<String, Object> filtro = new HashMap<>();

		if (historicoVazaoCorretorVO.getChavePrimaria() != null && historicoVazaoCorretorVO.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, historicoVazaoCorretorVO.getChavePrimaria());
		}

		if (historicoVazaoCorretorVO.getDescricaoComplemento() != null
				&& !historicoVazaoCorretorVO.getDescricaoComplemento().isEmpty()) {
			filtro.put(COMPLEMENTO_IMOVEL, historicoVazaoCorretorVO.getDescricaoComplemento());
		}

		if (!historicoVazaoCorretorVO.getNumeroImovel().isEmpty()) {
			filtro.put(NUMERO_IMOVEL, historicoVazaoCorretorVO.getNumeroImovel());
		}

		if (!historicoVazaoCorretorVO.getCep().isEmpty()) {
			filtro.put(CEP_IMOVEL, historicoVazaoCorretorVO.getCep());
		}

		if (historicoVazaoCorretorVO.getCondominio() != null) {
			filtro.put(INDICADOR_CONDOMINIO_AMBOS, String.valueOf(historicoVazaoCorretorVO.getCondominio()));
		}

		if (!historicoVazaoCorretorVO.getNome().isEmpty()) {
			filtro.put(NOME, historicoVazaoCorretorVO.getNome());
		}

		if (historicoVazaoCorretorVO.getHabilitado() != null) {
			filtro.put(HABILITADO, historicoVazaoCorretorVO.getHabilitado());
		}

		return filtro;
	}
	
}
