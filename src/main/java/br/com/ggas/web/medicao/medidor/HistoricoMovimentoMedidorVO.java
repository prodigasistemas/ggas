/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.medicao.medidor;

import java.io.Serializable;
import java.util.Date;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;

/**
 * 
 *
 */
public class HistoricoMovimentoMedidorVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5782208551518286529L;

	private Date dataMovimento;

	private MotivoMovimentacaoMedidor motivoMovimentacaoMedidor;

	private String descricaoParecer;

	private MedidorLocalArmazenagem localArmazenagemOrigem;

	private MedidorLocalArmazenagem localArmazenagemDestino;

	public Date getDataMovimento() {
		Date data = null;
		if(this.dataMovimento != null) {
			data = (Date) dataMovimento.clone();
		}
		return data;
	}

	public void setDataMovimento(Date dataMovimento) {

		this.dataMovimento = dataMovimento;
	}

	public MotivoMovimentacaoMedidor getMotivoMovimentacaoMedidor() {

		return motivoMovimentacaoMedidor;
	}

	public void setMotivoMovimentacaoMedidor(MotivoMovimentacaoMedidor motivoMovimentacaoMedidor) {

		this.motivoMovimentacaoMedidor = motivoMovimentacaoMedidor;
	}

	public String getDescricaoParecer() {

		return descricaoParecer;
	}

	public void setDescricaoParecer(String descricaoParecer) {

		this.descricaoParecer = descricaoParecer;
	}

	public MedidorLocalArmazenagem getLocalArmazenagemOrigem() {

		return localArmazenagemOrigem;
	}

	public void setLocalArmazenagemOrigem(MedidorLocalArmazenagem localArmazenagemOrigem) {

		this.localArmazenagemOrigem = localArmazenagemOrigem;
	}

	public MedidorLocalArmazenagem getLocalArmazenagemDestino() {

		return localArmazenagemDestino;
	}

	public void setLocalArmazenagemDestino(MedidorLocalArmazenagem localArmazenagemDestino) {

		this.localArmazenagemDestino = localArmazenagemDestino;
	}
}
