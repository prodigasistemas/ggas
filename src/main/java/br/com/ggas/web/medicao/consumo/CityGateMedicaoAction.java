package br.com.ggas.web.medicao.consumo;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.impl.PlanilhaVolumePcsLinhaVO;
import br.com.ggas.medicao.consumo.impl.PlanilhaVolumePcsVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Classe responsável pelas ações referentes ao Modelo do Medidor.
 */
@Controller
public class CityGateMedicaoAction extends GenericAction {

	private static final String ID_CITY_GATE = "idCityGate";

	private static final String LISTA_CITY_GATES = "listaCityGates";

	private static final String LISTA_MESES_ANO = "listaMesesAno";

	private static final String LISTA_ANOS_DISPONIVEIS_CITY_GATE_MEDICAO = "listaAnosDisponiveisCityGateMedicao";

	private static final String LISTA_CITY_GATE_MEDICAO_VO = "listaCityGateMedicaoVO";
	
	private static final String PLANILHA_VOLPSC = "arquivo";

	@Autowired
	private ControladorTronco controladorTronco;

	@Autowired
	private ControladorCityGateMedicao controladorCityGateMedicao;
	
	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();

	/**
	 * Método responsável por exibir a tela de Manutenção de PCS do City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param idCityGate - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirManutencaoPCSCityGate")
	public String exibirManutencaoPCSCityGate(HttpServletRequest request, Model model,
			@RequestParam(value = ID_CITY_GATE, required = false) Long idCityGate) throws GGASException {

		carregarCampos(model, idCityGate);
		model.addAttribute(ID_CITY_GATE, idCityGate);

		saveToken(request);

		return "exibirManutencaoPCSCityGate";
	}

	/**
	 * Método responsável por exibir os dados de Manutenção de PCS do City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param idCityGateParam - {@link Long}
	 * @param mesParam - {@link Integer}
	 * @param anoParam - {@link Integer}
	 * @return view - {@link String }
	 */
	@RequestMapping("exibirDadosPCSCityGate")
	public String exibirDadosPCSCityGate(Model model, HttpServletRequest request,
			@RequestParam(value = ID_CITY_GATE, required = false) Long idCityGateParam,
			@RequestParam(value = "ano", required = false) Integer anoParam, @RequestParam(value = "mes", required = false) Integer mesParam)
			throws GGASException {

		Long idCityGate = idCityGateParam;
		Integer ano = anoParam;
		Integer mes = mesParam;

		if (ano == null || ano < 1) {
			ano = null;
		}

		if (mes == null || mes < 1) {
			mes = null;
		}

		CityGate cityGate = null;

		try {
			if (idCityGate != null && idCityGate > 0) {
				cityGate = controladorTronco.obterCityGate(idCityGate);
			}
			Date dataLeituraFaturada = null;
			Collection<CityGateMedicao> listaCityGateMedicao =
					controladorCityGateMedicao.listarCityGateMedicaoManutencao(cityGate, ano, mes);
			HistoricoConsumo historicoConsumo = controladorCityGateMedicao.obterUltimoConsumoHistorico(cityGate, true);
			if (historicoConsumo != null && historicoConsumo.getHistoricoAtual() != null) {
				dataLeituraFaturada = historicoConsumo.getHistoricoAtual().getDataLeituraFaturada();
			}
			Collection<CityGateMedicaoVO> listaCityGateMedicaoVO = converterCityGateMedicao(listaCityGateMedicao, dataLeituraFaturada);
			model.addAttribute(LISTA_CITY_GATE_MEDICAO_VO, listaCityGateMedicaoVO);

		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirManutencaoPCSCityGate";
		}
		model.addAttribute("ano", ano);
		model.addAttribute("mes", mes);
		model.addAttribute(ID_CITY_GATE, idCityGate);
		return exibirManutencaoPCSCityGate(request, model, idCityGate);
	}


	/**
	 * Método responsável por manter e salvar os dados de Manutenção de PCS do City Gate.
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param faixa
	 * @param response
	 * @param idCityGateParam
	 * @param ano
	 * @param mes
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("manterPCSCityGate")
	public String manterPCSCityGate(HttpServletRequest request, Model model, FaixaCityGateMedicaoVo faixa, HttpServletResponse response,
			@RequestParam(value = ID_CITY_GATE, required = false) Long idCityGateParam,
			@RequestParam(value = "ano", required = false) Integer ano, @RequestParam(value = "mes", required = false) Integer mes)
			throws GGASException {

		Long idCityGate = idCityGateParam;

		try {

			validarToken(request);

			// Obtém dados de compra de PCS de city
			// gate do form e coloca no request
			// para exibição
			Collection<CityGateMedicaoVO> listaCityGateMedicaoVO = converterCityGateMedicao(faixa);
			request.setAttribute(LISTA_CITY_GATE_MEDICAO_VO, listaCityGateMedicaoVO);

			// Obtém dados de compra de PCS de city
			// gate do form e persiste
			Collection<CityGateMedicao> listaCityGateMedicao = new ArrayList<CityGateMedicao>();
			popularListaCityGateMedicao(faixa, request, listaCityGateMedicao, idCityGate);

			boolean exibirMsgProcessarConsistencia = controladorCityGateMedicao.atualizarListaCityGateMedicao(listaCityGateMedicao);

			if (exibirMsgProcessarConsistencia) {

				super.mensagemAlerta(model, Constantes.NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA);

			} else {

				super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(CityGateMedicao.CITY_GATE_MEDICOES));

			}

			model.addAttribute("inputModificado", false);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			return "forward:exibirManutencaoPCSCityGate";
		}
		model.addAttribute("ano", ano);
		model.addAttribute("mes", mes);
		model.addAttribute(ID_CITY_GATE, idCityGate);

		return exibirManutencaoPCSCityGate(request, model, idCityGate);
	}

	/**
	 * Método responsável por popular a lista em dados de PCS do City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param faixa - {@link FaixaCityGateMedicaoVo}
	 * @param listaCityGateMedicao - {@link CityGateMedicao}
	 * @param idCityGate - {@link Long}
	 * @return view - {@link String}
	 */
	private void popularListaCityGateMedicao(FaixaCityGateMedicaoVo faixa, HttpServletRequest request,
			Collection<CityGateMedicao> listaCityGateMedicao, Long idCityGate) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		String[] dataMedicao = faixa.getDataMedicao();
		String[] supridora = faixa.getValorSupridora();
		String[] distribuidora = faixa.getValorDistribuidora();
		String[] fixo = faixa.getValorFixo();
		String[] volumeSupridora = faixa.getVolumeSupridora();
		String[] volumeDistribuidora = faixa.getVolumeDistribuidora();
		Boolean[] habilitado = faixa.getHabilitado();

		CityGate cityGate = null;
		if (idCityGate != null && idCityGate > 0) {
			cityGate = controladorTronco.obterCityGate(idCityGate);
		}

		if (dataMedicao != null && dataMedicao.length > 0) {
			for (int i = 0; i < dataMedicao.length; i++) {
				CityGateMedicao cityGateMedicao = (CityGateMedicao) controladorCityGateMedicao.criar();
				if (habilitado != null && habilitado[i]) {
					cityGateMedicao.setCityGate(cityGate);
					if (!StringUtils.isEmpty(dataMedicao[i])) {
						cityGateMedicao.setDataMedicao(Util
								.converterCampoStringParaData("Data", dataMedicao[i], Constantes.FORMATO_DATA_BR));
					}
					if (supridora != null && !StringUtils.isEmpty(supridora[i])) {
						cityGateMedicao.setMedidaPCSSupridora(Util.converterCampoStringParaValorInteger("Supridora", supridora[i]));
					}
					if (distribuidora != null && !StringUtils.isEmpty(distribuidora[i])) {
						cityGateMedicao.setMedidaPCSDistribuidora(Util.converterCampoStringParaValorInteger("Distribuidora",
								distribuidora[i]));
					}
					if (fixo != null && !StringUtils.isEmpty(fixo[i])) {
						cityGateMedicao.setMedidaPCSFixo(Util.converterCampoStringParaValorInteger("Fixo", fixo[i]));
					}
					if (volumeSupridora != null && !StringUtils.isEmpty(volumeSupridora[i])) {
						cityGateMedicao.setMedidaVolumeSupridora(Util.converterCampoStringParaValorInteger("Volume da Supridora",
								volumeSupridora[i]));
					}
					if (volumeDistribuidora != null && !StringUtils.isEmpty(volumeDistribuidora[i])) {
						cityGateMedicao.setMedidaVolumeDistribuidora(Util.converterCampoStringParaValorInteger("Volume da Distribuidora",
								volumeDistribuidora[i]));
					}

					cityGateMedicao.setDadosAuditoria(dadosAuditoria);
					listaCityGateMedicao.add(cityGateMedicao);
				}
			}
		}
	}

	/**
	 * Método responsável por carregar os campos para exibição de dados em PCS City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param idCityGate - {@link Long}
	 */
	private void carregarCampos(Model model, Long idCityGate) throws GGASException {
		carregarCampos(model, idCityGate, null, null, null, null);
	}
	
	/**
	 * Método responsável por carregar os campos para exibição de dados em PCS City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param idCityGate - {@link Long}
	 */
	private void carregarCampos(Model model, Long idCityGate, Integer mes, Integer ano, 
			Integer diaInicial, Integer diaFinal) throws GGASException {

		model.addAttribute(LISTA_CITY_GATES, controladorTronco.listarCityGateExistente());
		if (idCityGate != null && idCityGate > 0) {
			CityGate cityGate = controladorTronco.obterCityGate(idCityGate);
			model.addAttribute(LISTA_ANOS_DISPONIVEIS_CITY_GATE_MEDICAO,
					controladorCityGateMedicao.listarAnosDisponiveisCityGateMedicao(cityGate));
		}
		model.addAttribute(LISTA_MESES_ANO, Mes.MESES_ANO);
		
		if (ano!=null) {
			model.addAttribute("ano", ano);
		}
		if (mes!=null) {
			model.addAttribute("mes", mes);
		}
		if (diaInicial!=null) {
			model.addAttribute("diaInicial", diaInicial);
		}
		if (diaFinal!=null) {
			model.addAttribute("diaFinal", diaFinal);
		}
		if (idCityGate!=null) {
			model.addAttribute(ID_CITY_GATE, idCityGate);
		}
	}

	/**
	 * Método responsável por carregar os campos para exibição de dados em PCS City Gate.
	 * 
	 * @param listaCityGateMedicao
	 * @param dataUltimaLeitura
	 * @return list
	 */

	private Collection<CityGateMedicaoVO> converterCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicao, Date dataUltimaLeitura)
			throws GGASException {

		Collection<CityGateMedicaoVO> listaCityGateMedicaoVO = new ArrayList<CityGateMedicaoVO>();

		if (listaCityGateMedicao != null && !listaCityGateMedicao.isEmpty()) {
			for (CityGateMedicao cityGateMedicao : listaCityGateMedicao) {
				CityGateMedicaoVO cityGateMedicaoVO = new CityGateMedicaoVO();
				cityGateMedicaoVO.setData(cityGateMedicao.getDataMedicao());
				if (cityGateMedicao.getMedidaPCSSupridora() != null) {
					cityGateMedicaoVO.setValorSupridora(cityGateMedicao.getMedidaPCSSupridora().toString());
				}
				if (cityGateMedicao.getMedidaPCSDistribuidora() != null) {
					cityGateMedicaoVO.setValorDistribuidora(cityGateMedicao.getMedidaPCSDistribuidora().toString());
				}
				if (cityGateMedicao.getMedidaPCSFixo() != null) {
					cityGateMedicaoVO.setValorFixo(cityGateMedicao.getMedidaPCSFixo().toString());
				}
				if (cityGateMedicao.getMedidaVolumeDistribuidora() != null) {
					cityGateMedicaoVO.setVolumeDistribuidora(cityGateMedicao.getMedidaVolumeDistribuidora().toString());
				}
				if (cityGateMedicao.getMedidaVolumeSupridora() != null) {
					cityGateMedicaoVO.setVolumeSupridora(cityGateMedicao.getMedidaVolumeSupridora().toString());
				}

				cityGateMedicaoVO.setHabilitado(controladorCityGateMedicao.permitirAtualizacaoCityGateMedicao(dataUltimaLeitura,
						cityGateMedicao.getDataMedicao()));

				listaCityGateMedicaoVO.add(cityGateMedicaoVO);
			}
		}

		return listaCityGateMedicaoVO;
	}

	/**
	 * Método responsável por carregar os campos para exibição de dados em PCS City Gate.
	 * 
	 * @param faixa
	 * @return list
	 */
	private Collection<CityGateMedicaoVO> converterCityGateMedicao(FaixaCityGateMedicaoVo faixa) throws GGASException {

		Collection<CityGateMedicaoVO> listaCityGateMedicaoVO = new ArrayList<CityGateMedicaoVO>();

		String[] dataMedicao = faixa.getDataMedicao();
		String[] supridora = faixa.getValorSupridora();
		String[] distribuidora = faixa.getValorDistribuidora();
		String[] fixo = faixa.getValorFixo();
		String[] volumeSupridora = faixa.getVolumeSupridora();
		String[] volumeDistribuidora = faixa.getVolumeDistribuidora();
		Boolean[] habilitado = faixa.getHabilitado();

		if (dataMedicao != null && dataMedicao.length > 0) {
			for (int i = 0; i < dataMedicao.length; i++) {
				CityGateMedicaoVO cityGateMedicaoVO = new CityGateMedicaoVO();
				if (!StringUtils.isEmpty(dataMedicao[i])) {
					cityGateMedicaoVO.setData(Util.converterCampoStringParaData("Data", dataMedicao[i], Constantes.FORMATO_DATA_BR));
				}
				if (supridora != null && !StringUtils.isEmpty(supridora[i])) {
					cityGateMedicaoVO.setValorSupridora(supridora[i]);
				}
				if (distribuidora != null && !StringUtils.isEmpty(distribuidora[i])) {
					cityGateMedicaoVO.setValorDistribuidora(distribuidora[i]);
				}
				if (fixo != null && !StringUtils.isEmpty(fixo[i])) {
					cityGateMedicaoVO.setValorFixo(fixo[i]);
				}
				if (volumeDistribuidora != null && !StringUtils.isEmpty(volumeDistribuidora[i])) {
					cityGateMedicaoVO.setVolumeDistribuidora(volumeDistribuidora[i]);
				}
				if (volumeSupridora != null && !StringUtils.isEmpty(volumeSupridora[i])) {
					cityGateMedicaoVO.setVolumeSupridora(volumeSupridora[i]);
				}
				if (habilitado != null) {
					cityGateMedicaoVO.setHabilitado(habilitado[i]);
				}

				listaCityGateMedicaoVO.add(cityGateMedicaoVO);

			}
		}

		return listaCityGateMedicaoVO;
	}

	/**
	 * Método responsável por importar planilha PCS do City Gate.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param idCityGateParam - {@link Long}
	 * @param anoParam - {@link Integer}
	 * @param mesParam - {@link Integer}
	 * @param planilha - {@link MultipartFile}
	 * @param diaInicialParam - {@link Integer}
	 * @param diaFinalParam - {@link Integer}
	 * @return view - {@link String}
	 */
	@RequestMapping("importarPlanilhaPCSCityGate")
	public String importarPlanilhaPCSCityGate(HttpServletRequest request,
			@RequestParam(value = ID_CITY_GATE, required = false) Long idCityGate, 
			@RequestParam(value = "ano", required = false) Integer anoParam, 
			@RequestParam(value = "mes", required = false) Integer mesParam,
			@RequestParam(value = PLANILHA_VOLPSC, required = false) MultipartFile planilha,
			@RequestParam(value = "diaInicial", required = false) Integer diaInicialParam, 
			@RequestParam(value = "diaFinal", required = false) Integer diaFinalParam,
			Model model)
			throws GGASException {
		try {
			validarImportarCityGateMedicao(anoParam, mesParam, planilha, diaInicialParam, diaFinalParam);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			carregarCampos(model, idCityGate, mesParam, anoParam, diaInicialParam, diaFinalParam);
			return exibirManutencaoPCSCityGate(request, model, idCityGate);
		}
		
		try {
			CityGate cityGate = fachada.buscarCityGatePorChave(idCityGate);

			Date dataLeituraFaturada = null;
			HistoricoConsumo historicoConsumo = controladorCityGateMedicao.obterUltimoConsumoHistorico(cityGate, true);
			if (historicoConsumo != null && historicoConsumo.getHistoricoAtual() != null) {
				dataLeituraFaturada = historicoConsumo.getHistoricoAtual().getDataLeituraFaturada();
			}
			
			PlanilhaVolumePcsVO dadosImportados = fachada.importarVolPSCCityGateExcel(cityGate, planilha.getBytes(),
					planilha.getContentType(), planilha.getName(), diaInicialParam, diaFinalParam, dataLeituraFaturada);

			List<CityGateMedicao> listaCityGateMedicoes = toCityGateMedicoes(idCityGate, dadosImportados);

			Calendar calendar = controladorCityGateMedicao.extrairPeriodoCalendar(dadosImportados);
			Collection<CityGateMedicao> listaCityGateBancoDeDados = 
					fachada.listarCityGateMedicaoManutencao(cityGate, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)+1);
			
			listaCityGateMedicoes = atualizarListaVindaDoBanco(listaCityGateBancoDeDados, listaCityGateMedicoes);
			
			fachada.atualizarImportacaoPlanilhaCityGateMedicao(listaCityGateMedicoes);

			mensagemSucesso(model, Constantes.SUCESSO_PLANILHA_CITY_GATE_IMPORTADA);
			carregarCampos(model, idCityGate, mesParam, anoParam, diaInicialParam, diaFinalParam);
			return exibirDadosPCSCityGate(model, request, idCityGate, anoParam, mesParam);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_PLANILHA_CITY_GATE_IMPORTADA, true));
			carregarCampos(model, idCityGate, mesParam, anoParam, diaInicialParam, diaFinalParam);
			return exibirManutencaoPCSCityGate(request, model, idCityGate);
		}
	}

	private void validarImportarCityGateMedicao(Integer anoParam, Integer mesParam, MultipartFile planilha,
			Integer diaInicialParam, Integer diaFinalParam) throws NegocioException {
		
		//Valida Dia Inicial e DiaFinal
		if ( ((diaInicialParam == null) ^ (diaFinalParam == null)) ) {
			throw new NegocioException(Constantes.CITY_GATE_IMPORTACAO_ERRO_DIAS_INCOMPLETOS);
		} else {
			if ( (diaInicialParam != null) &&
				 (diaFinalParam < diaInicialParam) ) {
				throw new NegocioException(Constantes.CITY_GATE_IMPORTACAO_ERRO_DIA_FINAL_MAIOR);
			}
		}
	}
	
	private CityGateMedicao criarCityGateMedicao(CityGate cityGate, PlanilhaVolumePcsVO planilha,
			Integer dia, Calendar periodoPlanilha) throws GGASException {
		CityGateMedicao medicao = null;
		
		CityGateMedicao cityGateMedicao = fachada.criarCityGateMedicao();
		cityGateMedicao.setCityGate(cityGate);
		
		if (planilha.getLinha().get(dia) != null) {
			PlanilhaVolumePcsLinhaVO linha = planilha.getLinha().get(dia);
								
			if (StringUtils.isNotBlank(linha.getData())) {
				periodoPlanilha.set(Calendar.DAY_OF_MONTH, dia.intValue());
				cityGateMedicao.setDataMedicao(periodoPlanilha.getTime());
			}
			if (StringUtils.isNotBlank(linha.getVolume())) {
				cityGateMedicao.setMedidaVolumeSupridora( formatarValorDecimalParaInteger( linha.getVolume()) );
			}
			if (StringUtils.isNotBlank(linha.getPcs())) {
				cityGateMedicao.setMedidaPCSSupridora( formatarValorDecimalParaInteger( linha.getPcs()) );
			}
			cityGateMedicao.setHabilitado(true);
			cityGateMedicao.setMedidaPCSFixo(null);
			cityGateMedicao.setMedidaPCSDistribuidora(null);
			cityGateMedicao.setMedidaVolumeDistribuidora(null);
			medicao = cityGateMedicao;
		}
		
		return medicao;
	}
	
	private List<CityGateMedicao> toCityGateMedicoes(Long idCityGate, PlanilhaVolumePcsVO planilha) 
			throws GGASException {
		List<CityGateMedicao> cityGateMedicaoList = new ArrayList<>();
		CityGate cityGate = null;
		if(idCityGate != null && idCityGate > 0) {
			cityGate = fachada.buscarCityGatePorChave(idCityGate);
		}

		if(StringUtils.isNotBlank(planilha.getMes()) && StringUtils.isNotBlank(planilha.getAno())) {
			Calendar periodoPlanilha = controladorCityGateMedicao.extrairPeriodoCalendar(planilha);
			Integer ultimoDiaDoMes =  periodoPlanilha.getActualMaximum(Calendar.DAY_OF_MONTH);
			for (Integer dia = 1; dia <= ultimoDiaDoMes; dia++) {
				CityGateMedicao cityGateMedicao = criarCityGateMedicao(cityGate, planilha, dia, periodoPlanilha);
				if (cityGateMedicao != null) {			
					cityGateMedicaoList.add(cityGateMedicao);
				}
			}
		}
		return cityGateMedicaoList;
	}
	
	private static Integer formatarValorDecimalParaInteger(String str) {
		Double doubleValor = Double.parseDouble( str );
		return (int) Math.round(doubleValor);
	}
	
	private List<CityGateMedicao> atualizarListaVindaDoBanco(Collection<CityGateMedicao> listaBancoDeDados, 
			Collection<CityGateMedicao> listaPlanilha) {
		List<CityGateMedicao> retorno = new ArrayList<>();
		for (CityGateMedicao medicaoPlanilha : listaPlanilha) {
			
			CityGateMedicao medicaoParaSalvar = null;
			for (CityGateMedicao item : listaBancoDeDados) {
				Integer diaExistente = extrairDia(item.getDataMedicao());
				Integer diaPlanilha = extrairDia(medicaoPlanilha.getDataMedicao());
				if (diaExistente.equals(diaPlanilha)) {
					medicaoParaSalvar = item;
					medicaoParaSalvar.setMedidaPCSSupridora(medicaoPlanilha.getMedidaPCSSupridora());
					medicaoParaSalvar.setMedidaVolumeSupridora(medicaoPlanilha.getMedidaVolumeSupridora());
				}
			}
			if (medicaoParaSalvar == null) {
				medicaoParaSalvar = medicaoPlanilha;
			}
			retorno.add(medicaoParaSalvar);
		}
		return retorno;
	}
	
	private static int extrairDia(Date data) {
		Calendar calExistente = Calendar.getInstance();
		calExistente.setTime(data);
		return calExistente.get(Calendar.DAY_OF_MONTH);
	}
}
