/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import java.io.Serializable;

/**
 * Classe responsável pela representação de valores de Dados de Compra de Gas - Imóvel
 * 
 * @author bruno silva
 */
public class ImovelDadosCompraGasVO implements Serializable {

	private static final long serialVersionUID = -1000116031744736235L;

	private Long chavePrimaria;

	private String matricula;

	private String nome;

	private String[] dataVigencia;

	private String[] medidaPCS;

	private String[] fatorZ;

	private Boolean[] habilitado;

	private Boolean[] permiteAlteracaoFatorZ;

	private Integer mes;

	private Integer ano;

	/**
	 * @return the chavePrimaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            the chavePrimaria to set
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return the dataVigencia
	 */
	public String[] getDataVigencia() {

		if (dataVigencia != null) {
			return dataVigencia.clone();
		}

		return new String[0];

	}

	/**
	 * @param dataVigencia
	 *            the dataVigencia to set
	 */
	public void setDataVigencia(String[] dataVigencia) {

		if (dataVigencia != null) {
			this.dataVigencia = dataVigencia.clone();
		}

	}

	/**
	 * @return the medidaPCS
	 */
	public String[] getMedidaPCS() {

		if (medidaPCS != null) {
			return medidaPCS.clone();
		}

		return new String[0];
	}

	/**
	 * @param medidaPCS
	 *            the medidaPCS to set
	 */
	public void setMedidaPCS(String[] medidaPCS) {

		if (medidaPCS != null) {
			this.medidaPCS = medidaPCS.clone();
		}

	}

	/**
	 * @return the fatorZ
	 */
	public String[] getFatorZ() {

		if (fatorZ != null) {
			return fatorZ.clone();
		}

		return new String[0];

	}

	/**
	 * @param fatorZ
	 *            the fatorZ to set
	 */
	public void setFatorZ(String[] fatorZ) {

		if (fatorZ != null) {
			this.fatorZ = fatorZ.clone();
		}

	}

	/**
	 * @return the habilitado
	 */
	public Boolean[] isHabilitado() {

		if (habilitado != null) {
			return habilitado.clone();
		}

		return new Boolean[0];

	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(Boolean[] habilitado) {

		if (habilitado != null) {
			this.habilitado = habilitado.clone();
		}

	}

	/**
	 * @return the permiteAlteracaoFatorZ
	 */
	public Boolean[] isPermiteAlteracaoFatorZ() {

		if (permiteAlteracaoFatorZ != null) {
			return permiteAlteracaoFatorZ.clone();
		}

		return new Boolean[0];

	}

	/**
	 * @param permiteAlteracaoFatorZ
	 *            the permiteAlteracaoFatorZ to
	 *            set
	 */
	public void setPermiteAlteracaoFatorZ(Boolean[] permiteAlteracaoFatorZ) {

		if (permiteAlteracaoFatorZ != null) {
			this.permiteAlteracaoFatorZ = permiteAlteracaoFatorZ.clone();
		}

	}

	/**
	 * @return the mes
	 */
	public Integer getMes() {

		return mes;
	}

	/**
	 * @param mes
	 *            the mes to
	 *            set
	 */
	public void setMes(Integer mes) {

		this.mes = mes;
	}

	/**
	 * @return the ano
	 */
	public Integer getAno() {

		return ano;
	}

	/**
	 * @param ano
	 *            the mes to
	 *            set
	 */
	public void setAno(Integer ano) {

		this.ano = ano;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {

		return matricula;
	}

	/**
	 * @param ano
	 *            the matricula to
	 *            set
	 */
	public void setMatricula(String matricula) {

		this.matricula = matricula;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * @param ano
	 *            the nome to
	 *            set
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

}
