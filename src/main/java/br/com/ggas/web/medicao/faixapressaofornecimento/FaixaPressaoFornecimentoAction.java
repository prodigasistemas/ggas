/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 12/08/2013 15:18:13
 @author vtavares
 */

package br.com.ggas.web.medicao.faixapressaofornecimento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimentoVO;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.FaixaPressaoFornecimentoImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * Classe responsável pelas telas relacionadas às faixas de pressão de
 * fornecimento
 *
 */
@Controller
public class FaixaPressaoFornecimentoAction extends GenericAction {

	private static final String FORWARD_PESQUISAR_FAIXA_PRESSAO_FORNECIMENTO = "forward:pesquisarFaixaPressaoFornecimento";

	private static final String INCLUIR = "incluir";

	private static final String ALTERAR = "alterar";

	private static final String FILTRO_FAIXA = "filtroFaixa";

	private static final String MEDIDA_MINIMO = "MedidaMinimo";

	private static final String INDEX_LISTA_FAIXA_PRESSAO_FORNECIMENTO = "indexListaFaixaPressaoFornecimento";

	private static final String LISTA_REMOVER = "listaRemover";

	private static final String LISTA_FAIXA_PRESSAO_APENAS_VINCULADAS = "listaFaixaPressaoApenasVinculadas";

	private static final String LISTA_FAIXA_PRESSAO_FORNECIMENTO = "listaFaixaPressaoFornecimento";

	private static final String ID_UNIDADE_PRESSAO = "idUnidadePressao";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String FAIXAS_PRESSAO_FORNECIMENTO = "faixasPressaoFornecimento";

	private static final String FAIXA_PRESSAO_FORNECIMENTO = "faixaPressaoFornecimento";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier("controladorFaixaPressaoFornecimento")
	private ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorUnidade")
	private ControladorUnidade controladorUnidade;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Exibir pesquisa faixa pressao fornecimento.
	 * 
	 * @param habilitado
	 * @param model
	 * @param session
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaFaixaPressaoFornecimento")
	public String exibirPesquisaFaixaPressaoFornecimento(
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model, HttpSession session)
			throws GGASException {

		this.carregarCampos(model);

		session.removeAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);
		session.removeAttribute(LISTA_FAIXA_PRESSAO_APENAS_VINCULADAS);
		session.removeAttribute(LISTA_REMOVER);
		session.removeAttribute(INDEX_LISTA_FAIXA_PRESSAO_FORNECIMENTO);

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaFaixaPressaoFornecimento";
	}

	/**
	 * Pesquisar faixa pressao fornecimento.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param habilitado
	 * @param fluxo
	 * @param voltar
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("pesquisarFaixaPressaoFornecimento")
	public String pesquisarFaixaPressaoFornecimento(FaixaPressaoFornecimentoImpl faixaPressao, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = "fluxo", required = false) String fluxo,
			@RequestParam(value = "voltar", required = false) boolean voltar, Model model, HttpServletRequest request)
			throws Exception {

		Map<String, Object> filtro = null;
		if (voltar) {
			FaixaPressaoFornecimento faixa = (FaixaPressaoFornecimento) request.getSession().getAttribute(FILTRO_FAIXA);
			filtro = prepararFiltro(faixa, habilitado);
			model.addAttribute(FAIXA_PRESSAO_FORNECIMENTO, faixa);
			request.getSession().setAttribute(FILTRO_FAIXA, faixa);

		} else {
			filtro = prepararFiltro(faixaPressao, habilitado);
			model.addAttribute(FAIXA_PRESSAO_FORNECIMENTO, faixaPressao);
			request.getSession().setAttribute(FILTRO_FAIXA, faixaPressao);
		}

		super.adicionarFiltroPaginacao(request, filtro);

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = controladorFaixaPressaoFornecimento
				.listarFaixaPressaoFornecimento(filtro);

		request.getSession().setAttribute(FAIXAS_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimentoVO);

		model.addAttribute("listaFaixaPressaoFornecimentoVO", listaFaixaPressaoFornecimentoVO);
		if (INCLUIR.equals(fluxo) || ALTERAR.equals(fluxo)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		} else {
			model.addAttribute(HABILITADO, habilitado);
		}

		return exibirPesquisaFaixaPressaoFornecimento(habilitado, model, request.getSession());
	}

	/**
	 * Alterar faixa pressao fornecimento.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param faixaPressaoVO
	 * @param results
	 * @param fluxo
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirAlterarFaixaPressaoFornecimento")
	public String incluirAlterarFaixaPressaoFornecimento(FaixaPressaoFornecimentoImpl faixaPressao,
			BindingResult result, FaixaPressaoFornecimentoVO faixaPressaoVO, BindingResult results,
			@RequestParam("fluxo") String fluxo, HttpServletRequest request, Model model) throws GGASException {

		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = (List<FaixaPressaoFornecimento>) request
				.getSession().getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);

		if (faixaPressao.validarDados().size() > 0) {
			model.addAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);
			mensagemErroParametrizado(model, request, new NegocioException(faixaPressao.validarDados()));
			return verifcaFluxo(fluxo);
		}

		try {
			validarFaixaPressaoAdicionada(faixaPressao, faixaPressaoVO, listaFaixaPressaoFornecimento);
			Segmento segmento = faixaPressao.getSegmento();
			Unidade unidade = faixaPressao.getUnidadePressao();

			for (FaixaPressaoFornecimento faixaPressaoFornecimento : listaFaixaPressaoFornecimento) {

				if (faixaPressaoFornecimento.getMedidaMaximo() != null) {
					faixaPressaoFornecimento.setSegmento(segmento);
					faixaPressaoFornecimento.setUnidadePressao(unidade);
					controladorFaixaPressaoFornecimento.verificarFaixaPressaoExistente(faixaPressaoFornecimento);

					verificarInsercaoAlteracao(fluxo, faixaPressaoFornecimento);
				}
			}

			if (ALTERAR.equals(fluxo)) {
				DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
				removerFaixas(listaFaixaPressaoFornecimento, segmento, unidade, dadosAuditoria);
				incluirFaixasNovas(listaFaixaPressaoFornecimento);

			}

			if (INCLUIR.equals(fluxo)) {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA,
						FaixaPressaoFornecimentoImpl.FAIXA_PRESSAO_FORNECIMENTO);
			} else {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA,
						FaixaPressaoFornecimento.FAIXA_PRESSAO_FORNECIMENTO);
			}

			return FORWARD_PESQUISAR_FAIXA_PRESSAO_FORNECIMENTO;

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
		}

		return verifcaFluxo(fluxo);
	}

	/**
	 * @param fluxo
	 * @param faixaPressaoFornecimento
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	private void verificarInsercaoAlteracao(String fluxo, FaixaPressaoFornecimento faixaPressaoFornecimento)
			throws NegocioException, ConcorrenciaException {
		if (INCLUIR.equals(fluxo)) {
			controladorFaixaPressaoFornecimento.inserirFaixaPressaoFornecimento(faixaPressaoFornecimento);
		} else {
			controladorFaixaPressaoFornecimento.atualizarFaixasPressaoFornecimento(faixaPressaoFornecimento);
		}
	}

	/**
	 * @param listaFaixaPressaoFornecimento
	 * @throws NegocioException
	 */
	private void incluirFaixasNovas(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento)
			throws NegocioException {

		for (FaixaPressaoFornecimento faixa : listaFaixaPressaoFornecimento) {
			if (faixa.getChavePrimaria() == 0) {
				controladorFaixaPressaoFornecimento.inserirFaixaPressaoFornecimento(faixa);
			}
		}
	}

	/**
	 * @param listaFaixaPressaoFornecimento
	 * @param segmento
	 * @param unidade
	 * @param dadosAuditoria
	 * @throws NegocioException
	 */
	private void removerFaixas(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento, Segmento segmento,
			Unidade unidade, DadosAuditoria dadosAuditoria) throws NegocioException {

		Collection<FaixaPressaoFornecimento> lista = controladorFaixaPressaoFornecimento
				.obterFaixasPressaoFornecimento(segmento, unidade);

		Long[] faixasPersistidas = Util.collectionParaArrayChavesPrimarias(lista);
		Long[] faixasSessao = Util.collectionParaArrayChavesPrimarias(listaFaixaPressaoFornecimento);

		for (Long chave : faixasSessao) {
			faixasPersistidas = (Long[]) ArrayUtils.removeElement(faixasPersistidas, chave);
		}
		controladorFaixaPressaoFornecimento.removerFaixasPressaoFornecimento(faixasPersistidas, dadosAuditoria);
	}

	/**
	 * Excluir faixa pressao fornecimento.
	 * 
	 * @param chavesPrimarias
	 * @param model
	 * @param request
	 * @return String
	 * @throws NegocioException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("excluirFaixaPressaoFornecimento")
	public String excluirFaixaPressaoFornecimento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, Model model,
			HttpServletRequest request) throws NegocioException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = (List<FaixaPressaoFornecimentoVO>) request
				.getSession().getAttribute(FAIXAS_PRESSAO_FORNECIMENTO);

		Collection<FaixaPressaoFornecimento> faixas = new ArrayList<>();

		FaixaPressaoFornecimentoVO faixaPressaoVOremover = null;

		try {
			for (Long id : chavesPrimarias) {

				faixaPressaoVOremover = listaFaixaPressaoFornecimentoVO.get(id.intValue());

				this.validarVinculoFaixaPressaoComContrato(faixaPressaoVOremover.getSegmento(),
						faixaPressaoVOremover.getUnidadePressao());

				faixas.addAll(controladorFaixaPressaoFornecimento.obterFaixasPressaoFornecimento(
						faixaPressaoVOremover.getSegmento(), faixaPressaoVOremover.getUnidadePressao()));
			}

			controladorFaixaPressaoFornecimento.remover(Util.collectionParaArrayChavesPrimarias(faixas),
					dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA,
					FaixaPressaoFornecimento.FAIXA_PRESSAO_FORNECIMENTO);

		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model,
					new NegocioException(Constantes.MENSAGEM_PRESSAO_FORNECIMENTO_VINCULADA, e));
			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			LOG.error(e.getMessage(), e);
		} catch (IndexOutOfBoundsException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		} catch (NullPointerException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		}

		return FORWARD_PESQUISAR_FAIXA_PRESSAO_FORNECIMENTO;

	}

	/**
	 * @param segmento
	 * @param unidade
	 * @throws GGASException
	 */
	private void validarVinculoFaixaPressaoComContrato(Segmento segmento, Unidade unidade) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();

		if (segmento != null) {
			filtro.put(ID_SEGMENTO, segmento.getChavePrimaria());
		}

		if (unidade != null) {
			filtro.put(ID_UNIDADE_PRESSAO, unidade.getChavePrimaria());
		}

		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoRelacionadas = controladorFaixaPressaoFornecimento
				.listarFaixasPressaoFaturamentoRelacionadas(filtro);

		if (!listaFaixaPressaoFornecimentoRelacionadas.isEmpty()) {
			throw new GGASException(Constantes.MENSAGEM_PRESSAO_FORNECIMENTO_VINCULADA);
		}
	}

	/**
	 * Exibir inclusao faixa pressao fornecimento.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirInclusaoFaixaPressaoFornecimento")
	public String exibirInclusaoFaixaPressaoFornecimento(FaixaPressaoFornecimentoImpl faixaPressao,
			BindingResult result, @RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model,
			HttpServletRequest request) throws GGASException {

		carregarCampos(model);

		List<FaixaPressaoFornecimento> listaFaixaPressaoSessao = (List<FaixaPressaoFornecimento>) request.getSession()
				.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);

		if (listaFaixaPressaoSessao != null) {
			request.getSession().setAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoSessao);
			model.addAttribute(FAIXA_PRESSAO_FORNECIMENTO, faixaPressao);
		} else {
			criarFaixaPressaoFornecimento(request);
		}

		model.addAttribute(HABILITADO, habilitado);
		request.getSession().removeAttribute(FAIXAS_PRESSAO_FORNECIMENTO);

		return "exibirInclusaoFaixaPressaoFornecimento";
	}

	/**
	 * Criar faixa pressao fornecimento.
	 * 
	 * @param request
	 * @return faixaPressaoFornecimento
	 * @throws GGASException
	 */
	private FaixaPressaoFornecimento criarFaixaPressaoFornecimento(HttpServletRequest request) throws GGASException {

		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = new ArrayList<FaixaPressaoFornecimento>();
		FaixaPressaoFornecimento faixaPressaoFornecimento = (FaixaPressaoFornecimento) controladorFaixaPressaoFornecimento
				.criar();

		faixaPressaoFornecimento.setMedidaMinimo(new BigDecimal(0));
		listaFaixaPressaoFornecimento.add(faixaPressaoFornecimento);
		request.getSession().setAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);

		return faixaPressaoFornecimento;
	}

	/**
	 * Exibir atualizacao faixa pressao fornecimento.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param habilitado
	 * @param chavePrimaria
	 * @param model
	 * @param session
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirAtualizacaoFaixaPressaoFornecimento")
	public String exibirAtualizacaoFaixaPressaoFornecimento(FaixaPressaoFornecimentoImpl faixaPressao,
			BindingResult result, @RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			Long chavePrimaria, Model model, HttpSession session) throws GGASException {

		this.carregarCampos(model);

		Map<String, Object> filtro = new HashMap<String, Object>();

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = (List<FaixaPressaoFornecimentoVO>) session
				.getAttribute(FAIXAS_PRESSAO_FORNECIMENTO);

		try {

			FaixaPressaoFornecimentoVO faixaPressaoVO = listaFaixaPressaoFornecimentoVO.get(chavePrimaria.intValue());

			if (faixaPressaoVO.getSegmento() != null) {
				filtro.put(ID_SEGMENTO, faixaPressaoVO.getSegmento().getChavePrimaria());
			}

			filtro.put(ID_UNIDADE_PRESSAO, faixaPressaoVO.getUnidadePressao().getChavePrimaria());

			List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoSessao = (List<FaixaPressaoFornecimento>) session
					.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);

			Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = null;

			if (listaFaixaPressaoFornecimentoSessao != null) {
				listaFaixaPressaoFornecimento = listaFaixaPressaoFornecimentoSessao;
			} else {
				listaFaixaPressaoFornecimento = controladorFaixaPressaoFornecimento
						.consultarFaixaPressaoFornecimento(filtro);
				session.setAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);
			}

			List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoRelacionadas = controladorFaixaPressaoFornecimento
					.listarFaixasPressaoFaturamentoRelacionadas(filtro);

			for (FaixaPressaoFornecimento faixa : listaFaixaPressaoFornecimento) {
				for (FaixaPressaoFornecimento faixaPressaoRelacionada : listaFaixaPressaoFornecimentoRelacionadas) {
					if (faixa.getChavePrimaria() == faixaPressaoRelacionada.getChavePrimaria()) {
						faixa.setIndicadorFaixaVinculada(true);
						break;
					}
				}
			}

			model.addAttribute("faixaPressaoVO", faixaPressaoVO);
			model.addAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);
			model.addAttribute("chavePrimaria", chavePrimaria);
			model.addAttribute(HABILITADO, habilitado);

			return "exibirAtualizacaoFaixaPressaoFornecimento";
		} catch (IndexOutOfBoundsException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		} catch (NullPointerException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		}

		return FORWARD_PESQUISAR_FAIXA_PRESSAO_FORNECIMENTO;
	}

	/**
	 * Limpar pressoes sem vinculo.
	 * 
	 * @param fluxo
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("limparPressoesSemVinculo")
	public String limparPressoesSemVinculo(@RequestParam("fluxo") String fluxo, HttpServletRequest request)
			throws Exception {

		List<FaixaPressaoFornecimento> listaFaixaPressaoSessao = (List<FaixaPressaoFornecimento>) request.getSession()
				.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);
		List<FaixaPressaoFornecimento> lista = new ArrayList<FaixaPressaoFornecimento>();
		List<FaixaPressaoFornecimento> listaRemover = new ArrayList<FaixaPressaoFornecimento>();

		for (FaixaPressaoFornecimento faixaPressaoFornecimento : listaFaixaPressaoSessao) {
			if (faixaPressaoFornecimento.isIndicadorFaixaVinculada()) {
				lista.add(faixaPressaoFornecimento);
			} else {
				listaRemover.add(faixaPressaoFornecimento);
			}
		}

		if (lista.isEmpty()) {
			FaixaPressaoFornecimento faixaPressaoFornecimento = criarFaixaPressaoFornecimento(request);
			faixaPressaoFornecimento.setHabilitado(true);

			if (INCLUIR.equals(fluxo)) {
				request.getSession().removeAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);
			}

			lista.add(faixaPressaoFornecimento);
		}

		request.getSession().setAttribute(LISTA_FAIXA_PRESSAO_APENAS_VINCULADAS, lista);
		request.getSession().setAttribute(LISTA_REMOVER, listaRemover);
		request.getSession().getAttribute(INDEX_LISTA_FAIXA_PRESSAO_FORNECIMENTO);

		return verifcaFluxo(fluxo);
	}

	/**
	 * Exibir detalhamento faixa pressao fornecimento.
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws NegocioException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirDetalhamentoFaixaPressaoFornecimento")
	public String exibirDetalhamentoFaixaPressaoFornecimento(Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model,
			HttpServletRequest request) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = (List<FaixaPressaoFornecimentoVO>) request
				.getSession().getAttribute(FAIXAS_PRESSAO_FORNECIMENTO);

		FaixaPressaoFornecimentoVO faixaPressaoVO = null;

		try {

			faixaPressaoVO = listaFaixaPressaoFornecimentoVO.get(chavePrimaria.intValue());

			if (faixaPressaoVO.getSegmento() != null) {
				filtro.put(ID_SEGMENTO, faixaPressaoVO.getSegmento().getChavePrimaria());
			}

			filtro.put(ID_UNIDADE_PRESSAO, faixaPressaoVO.getUnidadePressao().getChavePrimaria());

			Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = controladorFaixaPressaoFornecimento
					.consultarFaixaPressaoFornecimento(filtro);

			model.addAttribute("faixaPressaoVO", faixaPressaoVO);
			model.addAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);
			model.addAttribute("chavePrimaria", chavePrimaria);
			model.addAttribute(HABILITADO, habilitado);

			return "exibirDetalhamentoFaixaPressaoFornecimento";

		} catch (IndexOutOfBoundsException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		} catch (NullPointerException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA));
			LOG.error(e.getMessage(), e);
		}

		return FORWARD_PESQUISAR_FAIXA_PRESSAO_FORNECIMENTO;
	}

	/**
	 * Adicionar faixa pressao.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param faixaPressaoVO
	 * @param results
	 * @param model
	 * @param session
	 * @param fluxo
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarFaixaPressao")
	public String adicionarFaixaPressao(FaixaPressaoFornecimentoImpl faixaPressao, BindingResult result,
			FaixaPressaoFornecimentoVO faixaPressaoVO, BindingResult results, Model model, HttpSession session,
			@RequestParam("fluxo") String fluxo) throws GGASException {

		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = null;
		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoSessao = null;

		if (INCLUIR.equals(fluxo)) {
			listaFaixaPressaoFornecimento = (List<FaixaPressaoFornecimento>) session
					.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);
		} else {
			listaFaixaPressaoFornecimentoSessao = (List<FaixaPressaoFornecimento>) session
					.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);

			if (listaFaixaPressaoFornecimentoSessao != null) {
				listaFaixaPressaoFornecimento = listaFaixaPressaoFornecimentoSessao;
			} else {
				listaFaixaPressaoFornecimento = (List<FaixaPressaoFornecimento>) controladorFaixaPressaoFornecimento
						.obterFaixasPressaoFornecimento(faixaPressaoVO.getSegmento(),
								faixaPressaoVO.getUnidadePressao());
			}
		}

		if (listaFaixaPressaoFornecimento != null && !listaFaixaPressaoFornecimento.isEmpty()) {

			try {
				validarFaixaPressaoAdicionada(faixaPressao, faixaPressaoVO, listaFaixaPressaoFornecimento);

				FaixaPressaoFornecimento ultimaMedidaMaximoPressaoFornecimento = listaFaixaPressaoFornecimento
						.get(listaFaixaPressaoFornecimento.size() - 1);

				BigDecimal faixaInferiorNovaFaixaConsumo = ultimaMedidaMaximoPressaoFornecimento.getMedidaMaximo();

				FaixaPressaoFornecimento faixaPressaoFornecimento = (FaixaPressaoFornecimento) controladorFaixaPressaoFornecimento
						.criar();
				faixaPressaoFornecimento.setHabilitado(true);

				faixaPressaoFornecimento.setMedidaMinimo(somarUltimaCasaDecimal(faixaInferiorNovaFaixaConsumo));
				listaFaixaPressaoFornecimento.add(faixaPressaoFornecimento);

			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		session.setAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);

		return verifcaFluxo(fluxo);
	}

	/**
	 * 
	 * @param fluxo
	 * @return String
	 */
	private String verifcaFluxo(String fluxo) {

		if (INCLUIR.equals(fluxo)) {
			return "forward:exibirInclusaoFaixaPressaoFornecimento";
		}
		return "forward:exibirAtualizacaoFaixaPressaoFornecimento";
	}

	/**
	 * Somar ultima casa decimal.
	 * 
	 * @param faixaInferiorNovaFaixaConsumo
	 * @return BigDecimal
	 */
	private BigDecimal somarUltimaCasaDecimal(BigDecimal faixaInferiorNovaFaixaConsumo) {

		BigDecimal retorno = null;

		String faixaInferiorNovaFaixaConsumoString = faixaInferiorNovaFaixaConsumo.toString();
		if (faixaInferiorNovaFaixaConsumoString.indexOf('.') == -1) {
			faixaInferiorNovaFaixaConsumoString = faixaInferiorNovaFaixaConsumoString + ".0";
		}

		retorno = new BigDecimal(faixaInferiorNovaFaixaConsumoString);

		StringBuilder valorASerAdicionadoSB = new StringBuilder();

		for (int i = 0; i < retorno.scale(); i++) {
			if (i == 0) {
				valorASerAdicionadoSB.append("0.");
			} else {
				valorASerAdicionadoSB.append("0");
			}
		}
		valorASerAdicionadoSB.append("1");

		String valorASerAdicionado = valorASerAdicionadoSB.toString();
		retorno = retorno.add(new BigDecimal(valorASerAdicionado));

		return retorno;

	}

	/**
	 * Validar faixa pressao adicionada.
	 * 
	 * @param faixaPressao
	 * @param faixaPressaoVO
	 * @param listaFaixaPressaoFornecimento
	 * @throws NegocioException
	 * @throws FormatoInvalidoException
	 */
	private void validarFaixaPressaoAdicionada(FaixaPressaoFornecimentoImpl faixaPressao,
			FaixaPressaoFornecimentoVO faixaPressaoVO, List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento)
			throws NegocioException, FormatoInvalidoException {

		this.montarListaFaixaPressao(faixaPressao, faixaPressaoVO, listaFaixaPressaoFornecimento);
		this.excluirFaixasDuplicadas(listaFaixaPressaoFornecimento);

		controladorFaixaPressaoFornecimento.validarFaixasPressaoFornecimento(listaFaixaPressaoFornecimento);
	}

	/**
	 * Remover faixa pressao.
	 * 
	 * @param faixaPressao
	 * @param result
	 * @param faixaPressaoVO
	 * @param indexFaixaPressaoExlcusao
	 * @param fluxo
	 * @param session
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerFaixaPressao")
	public String removerFaixaPressao(FaixaPressaoFornecimentoImpl faixaPressao, BindingResult result,
			FaixaPressaoFornecimentoVO faixaPressaoVO,
			@RequestParam(value = "indexFaixaPressaoExlcusao", required = false) Long indexFaixaPressaoExlcusao,
			@RequestParam("fluxo") String fluxo, HttpSession session, Model model) throws Exception {

		List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = null;

		listaFaixaPressaoFornecimento = (List<FaixaPressaoFornecimento>) session
				.getAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO);

		listaFaixaPressaoFornecimento.remove(indexFaixaPressaoExlcusao.intValue());

		session.setAttribute(LISTA_FAIXA_PRESSAO_FORNECIMENTO, listaFaixaPressaoFornecimento);

		return verifcaFluxo(fluxo);
	}

	/**
	 * Excluir faixas duplicadas.
	 * 
	 * @param listaFaixaPressaoFornecimento
	 */
	private void excluirFaixasDuplicadas(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento) {

		for (int i = 0; i < listaFaixaPressaoFornecimento.size(); i++) {
			for (int j = i + 1; j < listaFaixaPressaoFornecimento.size(); j++) {
				if (listaFaixaPressaoFornecimento.get(i).getMedidaMinimo()
						.compareTo(listaFaixaPressaoFornecimento.get(j).getMedidaMinimo()) == 0
						&& listaFaixaPressaoFornecimento.get(i).getIndicadorCorrecaoPT()
								.getChavePrimaria() == listaFaixaPressaoFornecimento.get(j).getIndicadorCorrecaoPT()
										.getChavePrimaria()
						&& listaFaixaPressaoFornecimento.get(i).getIndicadorCorrecaoZ()
								.getChavePrimaria() == listaFaixaPressaoFornecimento.get(j).getIndicadorCorrecaoZ()
										.getChavePrimaria()) {
					listaFaixaPressaoFornecimento.remove(j);
				}
			}
		}
	}

	/**
	 * Montar lista faixa pressao.
	 * 
	 * @param faixaPressao
	 * @param faixaPressaoVO
	 * @param listaNova
	 * @throws FormatoInvalidoException
	 */
	private void montarListaFaixaPressao(FaixaPressaoFornecimentoImpl faixaPressao,
			FaixaPressaoFornecimentoVO faixaPressaoVO, List<FaixaPressaoFornecimento> listaNova)
			throws FormatoInvalidoException {

		int i = 0;

		for (FaixaPressaoFornecimento faixa : listaNova) {

			if (faixaPressaoVO.getMedidaMinimo().length != 0 && faixaPressaoVO.getMedidaMaximo().length != 0) {
				if (faixaPressaoVO.getMedidaMinimo()[i].length() != 0
						&& faixaPressaoVO.getMedidaMaximo()[i].length() != 0) {

					BigDecimal medidaMinimo = Util.converterCampoStringParaValorBigDecimal(MEDIDA_MINIMO,
							faixaPressaoVO.getMedidaMinimo()[i].replace(".", ","), Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);

					BigDecimal medidaMaximo = Util.converterCampoStringParaValorBigDecimal("Medida Maximo",
							faixaPressaoVO.getMedidaMaximo()[i].replace(".", ","), Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);

					if (faixaPressaoVO.getNumeroFatorCorrecaoPTZPCS().length != 0) {
						if (faixaPressaoVO.getNumeroFatorCorrecaoPTZPCS()[i].length() != 0) {
							BigDecimal numeroFatorCorrecaoPTZPCS = Util.converterCampoStringParaValorBigDecimal(
									MEDIDA_MINIMO, faixaPressaoVO.getNumeroFatorCorrecaoPTZPCS()[i].replace(".", ","),
									Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
							faixa.setNumeroFatorCorrecaoPTZPCS(numeroFatorCorrecaoPTZPCS);
						} else {
							faixa.setNumeroFatorCorrecaoPTZPCS(null);
						}
					} else {
						faixa.setNumeroFatorCorrecaoPTZPCS(null);
					}

					if (faixaPressaoVO.getNumeroFatorZ().length != 0) {
						if (faixaPressaoVO.getNumeroFatorZ()[i].length() != 0) {
							BigDecimal numeroFatorZ = Util.converterCampoStringParaValorBigDecimal(MEDIDA_MINIMO,
									faixaPressaoVO.getNumeroFatorZ()[i].replace(".", ","),
									Constantes.FORMATO_VALOR_FATOR_Z, Constantes.LOCALE_PADRAO);
							faixa.setNumeroFatorZ(numeroFatorZ);
						} else {
							faixa.setNumeroFatorZ(null);
						}
					} else {
						faixa.setNumeroFatorZ(null);
					}

					EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo
							.obterEntidadeConteudo(Long.valueOf(faixaPressaoVO.getEntidadeConteudo()[i]));
					faixa.setEntidadeConteudo(entidadeConteudo);

					EntidadeConteudo entidadeConteudoCorrigiPT = controladorEntidadeConteudo
							.obterEntidadeConteudo(Long.valueOf(faixaPressaoVO.getIndicadorCorrecaoPT()[i]));
					faixa.setIndicadorCorrecaoPT(entidadeConteudoCorrigiPT);

					EntidadeConteudo entidadeConteudoCorrigiZ = controladorEntidadeConteudo
							.obterEntidadeConteudo(Long.valueOf(faixaPressaoVO.getIndicadorCorrecaoZ()[i]));
					faixa.setIndicadorCorrecaoZ(entidadeConteudoCorrigiZ);

					faixa.setMedidaMinimo(medidaMinimo);
					faixa.setMedidaMaximo(medidaMaximo);

					i++;
				}
			}
		}

	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCampos(Model model) throws GGASException {

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		Collection<Unidade> listaUnidadePressao = controladorUnidade.listarUnidadesPressao();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_CLASSE_PRESSAO_FORNECIMENTO);
		String codigoEntidadeClasse = constanteSistema.getValor();

		Collection<EntidadeConteudo> listaClasse = controladorEntidadeConteudo
				.listarEntidadeConteudo(Long.parseLong(codigoEntidadeClasse));

		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_CORRIGI_PT);
		codigoEntidadeClasse = constanteSistema.getValor();
		Collection<EntidadeConteudo> listaCorrigiPT = controladorEntidadeConteudo
				.listarEntidadeConteudo(Long.parseLong(codigoEntidadeClasse));

		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_CORRIGI_Z);
		codigoEntidadeClasse = constanteSistema.getValor();
		Collection<EntidadeConteudo> listaCorrigiZ = controladorEntidadeConteudo
				.listarEntidadeConteudo(Long.parseLong(codigoEntidadeClasse));

		constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR);
		String codigoEntidadeConteudoCorrigiZSimValor = constanteSistema.getValor();

		ConstanteSistema constanteCorrigePTNao = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO);
		String codigoEntidadeConteudoCorrigiPTNao = constanteCorrigePTNao.getValor();

		ConstanteSistema constanteCorrigeZNao = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO);
		String codigoEntidadeConteudoCorrigiZNao = constanteCorrigeZNao.getValor();

		ConstanteSistema constanteCorrigeZCromatografia = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA);
		String codigoEntidadeConteudoCorrigiZCromatografia = constanteCorrigeZCromatografia.getValor();

		model.addAttribute("listaSegmento", listaSegmento);
		model.addAttribute("listaUnidadePressao", listaUnidadePressao);
		model.addAttribute("listaClasse", listaClasse);
		model.addAttribute("listaCorrigiPT", listaCorrigiPT);
		model.addAttribute("listaCorrigiZ", listaCorrigiZ);
		model.addAttribute("codigoEntidadeConteudoCorrigiZSimValor", codigoEntidadeConteudoCorrigiZSimValor);
		model.addAttribute("codigoEntidadeConteudoCorrigiPTNao", codigoEntidadeConteudoCorrigiPTNao);
		model.addAttribute("codigoEntidadeConteudoCorrigiZNao", codigoEntidadeConteudoCorrigiZNao);
		model.addAttribute("codigoEntidadeConteudoCorrigiZCromatografia", codigoEntidadeConteudoCorrigiZCromatografia);

	}

	/**
	 * Preparar filtro.
	 * 
	 * @param faixaPressao
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(FaixaPressaoFornecimento faixaPressao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (faixaPressao.getSegmento() != null && faixaPressao.getSegmento().getChavePrimaria() > 0) {
			filtro.put(ID_SEGMENTO, faixaPressao.getSegmento().getChavePrimaria());
		}

		if (faixaPressao.getUnidadePressao() != null && faixaPressao.getUnidadePressao().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_PRESSAO, faixaPressao.getUnidadePressao().getChavePrimaria());
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
