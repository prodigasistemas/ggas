package br.com.ggas.web.medicao.medidor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.impl.ModeloMedidorImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes ao Modelo do Medidor.
 */
@Controller
public class ModeloMedidorAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String MEDIDOR_MODELO = "medidorModelo";

	private static final String EXIBIR_ATUALIZAR_MEDIDOR_MODELO = "exibirAtualizarMedidorModelo";

	private static final String EXIBIR_INSERIR_MEDIDOR_MODELO = "exibirInserirMedidorModelo";

	private static final Class<ModeloMedidorImpl> CLASSE = ModeloMedidorImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.medicao.medidor.impl.ModeloMedidorImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_MEDIDOR_MODELO = "listaMedidorModelo";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa Modelo do Medidor.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaMedidorModelo")
	public String exibirPesquisaMedidorModelo(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaMedidorModelo";
	}

	/**
	 * Método responsável por pesquisar um Modelo do Medidor.
	 * 
	 * @param medidorModelo - {@link ModeloMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarMedidorModelo")
	public String pesquisarMedidorModelo(ModeloMedidorImpl medidorModelo, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(medidorModelo, habilitado);

		Collection<TabelaAuxiliar> listaMedidorModelo = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_MEDIDOR_MODELO, listaMedidorModelo);
		model.addAttribute(MEDIDOR_MODELO, medidorModelo);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaMedidorModelo(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de um Modelo do Medidor.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoMedidorModelo")
	public String exibirDetalhamentoMedidorModelo(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			ModeloMedidor medidorModelo = (ModeloMedidor) controladorTabelaAuxiliar.obter(chavePrimaria, ModeloMedidorImpl.class);
			model.addAttribute(MEDIDOR_MODELO, medidorModelo);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMedidorModelo";
		}

		return "exibirDetalhamentoMedidorModelo";
	}

	/**
	 * Método responsável por exibir a tela de inserir Modelo do Medidor.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_MEDIDOR_MODELO)
	public String exibirInserirMedidorModelo() {

		return EXIBIR_INSERIR_MEDIDOR_MODELO;
	}

	/**
	 * Método responsável por inserir um Modelo do Medidor.
	 * 
	 * @param medidorModelo - {@link ModeloMedidorImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirMedidorModelo")
	private String inserirMedidorModelo(ModeloMedidorImpl medidorModelo, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;

		try {
			if (medidorModelo.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (medidorModelo.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(medidorModelo);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(medidorModelo, "Medidor Modelo");
			controladorTabelaAuxiliar.inserir(medidorModelo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(ModeloMedidor.MEDIDOR_MODELO));
			retorno = pesquisarMedidorModelo(medidorModelo, result, model, medidorModelo.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(MEDIDOR_MODELO, medidorModelo);
			retorno = EXIBIR_INSERIR_MEDIDOR_MODELO;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar o Modelo do Medidor.
	 * 
	 * @param meidodrModelo- {@link ModeloMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param medidorModeloParam - {@link ModeloMedidorImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_MEDIDOR_MODELO)
	public String exibirAtualizarMedidorModelo(ModeloMedidorImpl medidorModeloParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		ModeloMedidor medidorModelo = medidorModeloParam;

		try {
			medidorModelo = (ModeloMedidor) controladorTabelaAuxiliar.obter(medidorModelo.getChavePrimaria(), CLASSE);
			model.addAttribute(MEDIDOR_MODELO, medidorModelo);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMedidorModelo";
		}

		return EXIBIR_ATUALIZAR_MEDIDOR_MODELO;
	}

	/**
	 * Método responsável por atualizar um Modelo do Medidor.
	 * 
	 * @param medidorModelo - {@link ModeloMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarMedidorModelo")
	private String atualizarMedidorModelo(ModeloMedidorImpl medidorModelo, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (medidorModelo.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (medidorModelo.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(medidorModelo);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(medidorModelo, "Modelo do Medidor");
			controladorTabelaAuxiliar.atualizar(medidorModelo, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(ModeloMedidor.MEDIDOR_MODELO));
			retorno = pesquisarMedidorModelo(medidorModelo, result, model, medidorModelo.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(MEDIDOR_MODELO, medidorModelo);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_MEDIDOR_MODELO;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover um Modelo do Medidor.
	 * 
	 * @param medidorModelo {@link MedidorModeloImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerMedidorModelo")
	public String removerMedidorModelo(ModeloMedidorImpl medidorModelo, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(ModeloMedidor.MEDIDOR_MODELO));

			return pesquisarMedidorModelo(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarMedidorModelo";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa do Modelo do Medidor.
	 * 
	 * @param medidorModelo - {@link ModeloMedidor}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(ModeloMedidor medidorModelo, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (medidorModelo != null) {
			if (medidorModelo.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, medidorModelo.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(medidorModelo.getDescricao())) {
				filtro.put(DESCRICAO, medidorModelo.getDescricao());
			}

			if (StringUtils.isNotEmpty(medidorModelo.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, medidorModelo.getDescricaoAbreviada());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
