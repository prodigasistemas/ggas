/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Transient;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class TemperaturaPressaoMedicaoVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 4185823262929252169L;

	public static final String CODIGO_LOCALIDADE = "LO";

	public static final String CODIGO_MICRORREGIAO = "MR";

	public static final String CODIGO_MUNICIPIO = "3MU";

	public static final String CODIGO_REGIAO = "2RE";

	public static final String CODIGO_UNIDADE_FEDERACAO = "1UF";
	
	@Transient
	public final Map<String, String> TIPOS_ABRANGENCIA = new TreeMap<String, String>(){

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(CODIGO_UNIDADE_FEDERACAO, "Estado");
			put(CODIGO_REGIAO, "Região");
			put(CODIGO_MUNICIPIO, "Município");
		}

	};

	private Long chavePrimaria;

	private Date dataVigencia;

	private String temperatura;

	private Long idUnidadeTemperatura;

	private String pressao;

	private Long idUnidadePressao;

	private Boolean habilitado = true;

	/**
	 * @return the chavePrimaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            the chavePrimaria to set
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return the dataVigencia
	 */
	public Date getDataVigencia() {
		Date data = null;
		if(dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	/**
	 * @param dataVigencia
	 *            the dataVigencia to set
	 */
	public void setDataVigencia(Date dataVigencia) {

		this.dataVigencia = dataVigencia;
	}

	/**
	 * @return the temperatura
	 */
	public String getTemperatura() {

		return temperatura;
	}

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	public void setTemperatura(String temperatura) {

		this.temperatura = temperatura;
	}

	/**
	 * @return the idUnidadeTemperatura
	 */
	public Long getIdUnidadeTemperatura() {

		return idUnidadeTemperatura;
	}

	/**
	 * @param idUnidadeTemperatura
	 *            the idUnidadeTemperatura to set
	 */
	public void setIdUnidadeTemperatura(Long idUnidadeTemperatura) {

		this.idUnidadeTemperatura = idUnidadeTemperatura;
	}

	/**
	 * @return the pressao
	 */
	public String getPressao() {

		return pressao;
	}

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	public void setPressao(String pressao) {

		this.pressao = pressao;
	}

	/**
	 * @return the idUnidadePressao
	 */
	public Long getIdUnidadePressao() {

		return idUnidadePressao;
	}

	/**
	 * @param idUnidadePressao
	 *            the idUnidadePressao to set
	 */
	public void setIdUnidadePressao(Long idUnidadePressao) {

		this.idUnidadePressao = idUnidadePressao;
	}

	/**
	 * Método responsável por retornar a data no
	 * formato dd/MM/yyyy
	 * 
	 * @return
	 */
	public String getDataVigenciaFormatada() {

		String retorno = "";
		if(this.getDataVigencia() != null) {
			retorno = Util.converterDataParaStringSemHora(this.getDataVigencia(), Constantes.FORMATO_DATA_BR);
		}
		return retorno;
	}

	/**
	 * @return the habilitado
	 */
	public Boolean getHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(Boolean habilitado) {

		this.habilitado = habilitado;
	}

}
