/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing)
 de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial
 de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing)
 de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.vazaocorretor;

import java.io.Serializable;

/**
 * 
 * @author bruno silva
 * 
 * Classe responsável pela transição dos valores entre as telas
 * de Pesquisar e Detalhar Histórico Corretor de Vazão
 *
 */
public class HistoricoVazaoCorretorVO implements Serializable{

	private static final long serialVersionUID = 3233488680972171996L;
	
	private Long chavePrimaria;
	private String indicadorPesquisa;
	private Long idPontoConsumo;
	private Long idImovel;
	private String nome;
	private String descricaoComplemento;
	private String matricula;
	private String numeroImovel;
	private Boolean condominio;
	private Boolean habilitado;
	private String cep;
	private String numeroSerie;
	private String marcaCorretor;
	private String modelo;
	private Long idModelo;
	private Long idMarca;
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricaoComplemento() {
		return descricaoComplemento;
	}
	public void setDescricaoComplemento(String descricaoComplemento) {
		this.descricaoComplemento = descricaoComplemento;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getNumeroImovel() {
		return numeroImovel;
	}
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	public Boolean getCondominio() {
		return condominio;
	}
	public void setCondominio(Boolean condominio) {
		this.condominio = condominio;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getMarcaCorretor() {
		return marcaCorretor;
	}
	public void setMarcaCorretor(String marcaCorretor) {
		this.marcaCorretor = marcaCorretor;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Long getIdModelo() {
		return idModelo;
	}
	public void setIdModelo(Long idModelo) {
		this.idModelo = idModelo;
	}
	public Long getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}

}
