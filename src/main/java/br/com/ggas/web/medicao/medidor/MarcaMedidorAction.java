package br.com.ggas.web.medicao.medidor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.impl.MarcaMedidorImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes a Marca do Medidor.
 */
@Controller
public class MarcaMedidorAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String MEDIDOR_MARCA = "medidorMarca";

	private static final String EXIBIR_ATUALIZAR_MEDIDOR_MARCA = "exibirAtualizarMedidorMarca";

	private static final String EXIBIR_INSERIR_MEDIDOR_MARCA = "exibirInserirMedidorMarca";

	private static final Class<MarcaMedidorImpl> CLASSE = MarcaMedidorImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.medicao.medidor.impl.MarcaMedidorImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_MEDIDOR_MARCA = "listaMedidorMarca";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa Marca do Medidor.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaMedidorMarca")
	public String exibirPesquisaMedidorMarca(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaMedidorMarca";
	}

	/**
	 * Método responsável por pesquisar uma Marca do Medidor.
	 * 
	 * @param medidorMarca- {@link MarcaMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarMedidorMarca")
	public String pesquisarMedidorMarca(MarcaMedidorImpl medidorMarca, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(medidorMarca, habilitado);

		Collection<TabelaAuxiliar> listaMedidorMarca = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_MEDIDOR_MARCA, listaMedidorMarca);
		model.addAttribute(MEDIDOR_MARCA, medidorMarca);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaMedidorMarca(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de uma Marca do Medidor.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoMedidorMarca")
	public String exibirDetalhamentoMedidorMarca(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			MarcaMedidor medidorMarca = (MarcaMedidor) controladorTabelaAuxiliar.obter(chavePrimaria, MarcaMedidorImpl.class);
			model.addAttribute(MEDIDOR_MARCA, medidorMarca);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMedidorMarca";
		}

		return "exibirDetalhamentoMedidorMarca";
	}

	/**
	 * Método responsável por exibir a tela de inserir Marca do Medidor.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_MEDIDOR_MARCA)
	public String exibirInserirMedidorMarca() {

		return EXIBIR_INSERIR_MEDIDOR_MARCA;
	}

	/**
	 * Método responsável por inserir um Marca do Medidor.
	 * 
	 * @param medidorMarca- {@link MarcaMedidorImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirMedidorMarca")
	private String inserirMedidorMarca(MarcaMedidorImpl medidorMarca, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;

		try {
			if (medidorMarca.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(medidorMarca);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(medidorMarca, "Medidor Marca");
			controladorTabelaAuxiliar.inserir(medidorMarca);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(MarcaMedidor.MEDIDOR_MARCA));
			retorno = pesquisarMedidorMarca(medidorMarca, result, model, medidorMarca.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(MEDIDOR_MARCA, medidorMarca);
			retorno = EXIBIR_INSERIR_MEDIDOR_MARCA;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar o Marca do Medidor.
	 * 
	 * @param meidodrMarca- {@link MarcaMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param medidorMarcaParam - {@link MarcaMedidorImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_MEDIDOR_MARCA)
	public String exibirAtualizarMedidorMarca(MarcaMedidorImpl medidorMarcaParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		MarcaMedidor medidorMarca = medidorMarcaParam;

		try {
			medidorMarca = (MarcaMedidor) controladorTabelaAuxiliar.obter(medidorMarca.getChavePrimaria(), CLASSE);
			model.addAttribute(MEDIDOR_MARCA, medidorMarca);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMedidorMarca";
		}

		return EXIBIR_ATUALIZAR_MEDIDOR_MARCA;
	}

	/**
	 * Método responsável por atualizar um Marca do Medidor.
	 * 
	 * @param medidorMarca- {@link MarcaMedidorImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarMedidorMarca")
	private String atualizarMedidorMarca(MarcaMedidorImpl medidorMarca, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (medidorMarca.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(medidorMarca);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(medidorMarca, "Marca do Medidor");
			controladorTabelaAuxiliar.atualizar(medidorMarca, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(MarcaMedidor.MEDIDOR_MARCA));
			retorno = pesquisarMedidorMarca(medidorMarca, result, model, medidorMarca.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(MEDIDOR_MARCA, medidorMarca);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_MEDIDOR_MARCA;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover uma Marca do Medidor.
	 * 
	 * @param medidorMarca {@link MedidorMarcaImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerMedidorMarca")
	public String removerMedidorMarca(MarcaMedidorImpl medidorMarca, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(MarcaMedidor.MEDIDOR_MARCA));

			return pesquisarMedidorMarca(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarMedidorMarca";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa da Marca do Medidor.
	 * 
	 * @param medidorMarca - {@link MarcaMedidor}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(MarcaMedidor medidorMarca, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (medidorMarca != null) {
			if (medidorMarca.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, medidorMarca.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(medidorMarca.getDescricao())) {
				filtro.put(DESCRICAO, medidorMarca.getDescricao());
			}

			if (StringUtils.isNotEmpty(medidorMarca.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, medidorMarca.getDescricaoAbreviada());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
