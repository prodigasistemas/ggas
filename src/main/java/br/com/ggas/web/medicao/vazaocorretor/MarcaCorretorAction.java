package br.com.ggas.web.medicao.vazaocorretor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes a Marca Corretor.
 */

@Controller
public class MarcaCorretorAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String CORRETOR_MARCA = "corretorMarca";

	private static final String EXIBIR_ATUALIZAR_CORRETOR_MARCA = "exibirAtualizarCorretorMarca";

	private static final String EXIBIR_INSERIR_CORRETOR_MARCA = "exibirInserirCorretorMarca";

	private static final Class<MarcaCorretorImpl> CLASSE = MarcaCorretorImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_CORRETOR_MARCA = "listaCorretorMarca";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa da Marca do Corretor de Vazão.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaCorretorMarca")
	public String exibirPesquisaCorretorMarca(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaCorretorMarca";
	}

	/**
	 * Método responsável por pesquisar uma Marca de Corretor de Vazão.
	 * 
	 * @param corretorMarca - {@link MarcaCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarCorretorMarca")
	public String pesquisarCorretorMarca(MarcaCorretorImpl corretorMarca, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(corretorMarca, habilitado);

		Collection<TabelaAuxiliar> listaCorretorMarca = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_CORRETOR_MARCA, listaCorretorMarca);
		model.addAttribute(CORRETOR_MARCA, corretorMarca);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaCorretorMarca(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de uma Marca do Corretor de Vazão.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoCorretorMarca")
	public String exibirDetalhamentoCorretorMarca(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			MarcaCorretor corretorMarca = (MarcaCorretor) controladorTabelaAuxiliar.obter(chavePrimaria, MarcaCorretorImpl.class);
			model.addAttribute(CORRETOR_MARCA, corretorMarca);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCorretorMarca";
		}

		return "exibirDetalhamentoCorretorMarca";
	}

	/**
	 * Método responsável por exibir a tela de inserir Marca do Corretor de Vazão.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_CORRETOR_MARCA)
	public String exibirInserirCorretorMarca() {

		return EXIBIR_INSERIR_CORRETOR_MARCA;
	}

	/**
	 * Método responsável por inserir uma Marca do Corretor de Vazão .
	 * 
	 * @param corretorMarca - {@link MarcaCorretorImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirCorretorMarca")
	private String inserirCorretorMarca(MarcaCorretorImpl corretorMarca, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;

		try {
			if (corretorMarca.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (corretorMarca.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(corretorMarca);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(corretorMarca, "Corretor Marca");
			controladorTabelaAuxiliar.inserir(corretorMarca);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(MarcaCorretor.CORRETOR_MARCA));
			retorno = pesquisarCorretorMarca(corretorMarca, result, model, corretorMarca.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CORRETOR_MARCA, corretorMarca);
			retorno = EXIBIR_INSERIR_CORRETOR_MARCA;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar a Marca do Corretor de Vazão.
	 * 
	 * @param corretorMarca - {@link MarcaCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param corretorMarcaParam - {@link MarcaCorretorImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_CORRETOR_MARCA)
	public String exibirAtualizarCorretorMarca(MarcaCorretorImpl corretorMarcaParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		MarcaCorretor corretorMarca = corretorMarcaParam;

		try {
			corretorMarca = (MarcaCorretor) controladorTabelaAuxiliar.obter(corretorMarca.getChavePrimaria(), CLASSE);
			model.addAttribute(CORRETOR_MARCA, corretorMarca);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCorretorMarca";
		}

		return EXIBIR_ATUALIZAR_CORRETOR_MARCA;
	}

	/**
	 * Método responsável por atualizar uma Marca do Corretor de Vazão.
	 * 
	 * @param corretorMarca - {@link MarcaCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarCorretorMarca")
	private String atualizarZonaBloqueio(MarcaCorretorImpl corretorMarca, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (corretorMarca.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (corretorMarca.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(corretorMarca);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(corretorMarca, "Marca do Corretor");
			controladorTabelaAuxiliar.atualizar(corretorMarca, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(MarcaCorretor.CORRETOR_MARCA));
			retorno = pesquisarCorretorMarca(corretorMarca, result, model, corretorMarca.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CORRETOR_MARCA, corretorMarca);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_CORRETOR_MARCA;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover uma Marca do Corretor de Vazão.
	 * 
	 * @param corretorMarca {@link CorretorMarcaImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerCorretorMarca")
	public String removerCorretorMarca(MarcaCorretorImpl corretorMarca, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(MarcaCorretor.CORRETOR_MARCA));

			return pesquisarCorretorMarca(null, null, model, Boolean.TRUE);

		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarCorretorMarca";

	}

	/**
	 * Método responsável por preparar o filtro para pesquisa da Marca do Corretor de Vazão.
	 * 
	 * @param corretorMarca - {@link MarcaCorretor}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(MarcaCorretor corretorMarca, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (corretorMarca != null) {

			if (corretorMarca.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, corretorMarca.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(corretorMarca.getDescricao())) {
				filtro.put(DESCRICAO, corretorMarca.getDescricao());
			}

			if (StringUtils.isNotEmpty(corretorMarca.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, corretorMarca.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
