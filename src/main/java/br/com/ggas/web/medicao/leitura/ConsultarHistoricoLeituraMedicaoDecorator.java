/*
 Copyright (C) <2011> GGAS ï¿½ Sistema de Gestï¿½o Comercial (Billing) de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 Este arquivo ï¿½ parte do GGAS, um sistema de gestï¿½o comercial de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 Este programa ï¿½ um software livre; vocï¿½ pode redistribuï¿½-lo e/ou
 modificï¿½-lo sob os termos de Licenï¿½a Pï¿½blica Geral GNU, conforme
 publicada pela Free Software Foundation; versï¿½o 2 da Licenï¿½a.

 O GGAS ï¿½ distribuï¿½do na expectativa de ser ï¿½til,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implï¿½cita de
 COMERCIALIZAï¿½ï¿½O ou de ADEQUAï¿½ï¿½O A QUALQUER PROPï¿½SITO EM PARTICULAR.
 Consulte a Licenï¿½a Pï¿½blica Geral GNU para obter mais detalhes.

 Vocï¿½ deve ter recebido uma cï¿½pia da Licenï¿½a Pï¿½blica Geral GNU
 junto com este programa; se nï¿½o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ï¿½ Sistema de Gestï¿½o Comercial (Billing) de Serviï¿½os de Distribuiï¿½ï¿½o de Gï¿½s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ï¿½ Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.util.DisplayTagGenericoDecorator;

/**
 * 
 *
 */
public class ConsultarHistoricoLeituraMedicaoDecorator extends DisplayTagGenericoDecorator {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.util.DisplayTagGenericoDecorator#addRowClass()
	 */
	@Override
	public String addRowClass() {

		String rowClass = super.addRowClass();

		PontoConsumo pontoConsumoSelecionado = (PontoConsumo) getPageContext().getRequest().getAttribute("pontoConsumo");
		if(pontoConsumoSelecionado != null
						&& pontoConsumoSelecionado.getChavePrimaria() == ((PontoConsumoVO) getCurrentRowObject()).getPontoConsumo()
										.getChavePrimaria()) {
			rowClass = "clickedRow";
		}

		return rowClass;
	}

	/*
	 * (non-Javadoc)
	 * @see org.displaytag.decorator.TableDecorator#addRowId()
	 */
	@Override
	public String addRowId() {

		return "PontoConsumo" + (((PontoConsumoVO) getCurrentRowObject()).getPontoConsumo().getChavePrimaria());
	}

}
