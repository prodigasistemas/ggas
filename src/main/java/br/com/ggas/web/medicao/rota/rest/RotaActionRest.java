/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.rota.rest;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.geral.apresentacao.EntidadeActionRest;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.cadastro.imovel.rest.ImovelActionRest;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.ImovelDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaPontoConsumo;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Action padrão de {@link Rota}.
 * Contém métodos para CRUD e acesso a etidade
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Controller
@RequestMapping("rest/rota")
@SuppressWarnings({"common-java:InsufficientCommentDensity"})
public class RotaActionRest extends EntidadeActionRest<Rota> {

	/**
	 * Construtor padrão de Rota
	 * @param controladorRota controlador de rota
	 */
	@Autowired
	public RotaActionRest(ControladorRota controladorRota) {
		super(controladorRota);
	}

	/**
	 * Obtém a lista de rota de um grupo de faturamento
	 * @param grupoFaturamento id da entidade de grupo faturamento
	 * @return retorna uma resposta json com a lista de rotas
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/listaPorGrupoFaturamento", produces = "application/json; charset=utf-8",
			method = { RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> listaPorGrupoFaturamento(@RequestParam Long grupoFaturamento) throws GGASException {

		final GrupoFaturamento grupo = ((ControladorRota) super.controlador).obterGrupoFaturamento(grupoFaturamento);
		final Collection<Rota> rotas = ((ControladorRota) super.controlador).consultarRotaPorGrupoFaturamento(grupo);
		final String json = serializarJson(rotas);

		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Busca os pontos de consumo por sua descrição de forma paginada
	 * @param pesquisa O objeto contendo os dados de pesquisa do ponto de consumo
	 * @return retorna a lista de pontos de consumos
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "buscarPontoConsumoPaginado", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarPontoConsumoPaginado(@RequestBody PesquisaPontoConsumo pesquisa) throws GGASException {
		DatatablesServerSideDTO<ImovelDTO> datatable;
		try {
			ControladorImovel controladorImovel =
					(ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
			final Pair<List<ImovelDTO>, Long> resultados = controladorImovel.listarImovelRotaComPontosDeConsumoFilho(pesquisa);
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), resultados.getSecond(), resultados.getFirst());
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
		} catch (Exception e) {
			final Logger log = Logger.getLogger(ImovelActionRest.class);
			log.error(e.getMessage(), e);
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), 0L, Collections.emptyList());
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder()
				.put(Rota.class, new RotaSimplesSerializador())
				.build();
	}
}
