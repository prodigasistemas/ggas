/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.*;
import br.com.ggas.cadastro.imovel.impl.ComentarioPontoConsumo;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.impl.HistoricoConsumoVORelatorioFaturaVerso;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.*;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
import br.com.ggas.medicao.leitura.impl.SituacaoLeituraMovimentoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.cronograma.CronogramaVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Classe action para interacao com a view de analise de incossistencias
 * 
 * @author orube
 *
 */
@Controller
public class AnaliseExcecaoLeituraAction extends GenericAction {

	private static final String ANALISE_EXCECAO_LEITURA_PESQUISA = "analiseExcecaoLeituraPesquisa";

	private static final int STR_MAX_LEN = 1000;

	private static final String ROTA_LEITURISTA_FUNCIONARIO = "rota.leiturista.funcionario";

	private static final String LAZY_INSTALACAO_MEDIDOR_MEDIDOR = "instalacaoMedidor.medidor";

	private static final String ANALISADA = "analisada";

	private static final String ID_TIPO_CONSUMO = "idTipoConsumo";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String IDS_ROTA = "idsRota";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME_IMOVEL = "nomeImovel";

	private static final String PERCENTUAL_VARIACAO_CONSUMO = "percentualVariacaoConsumo";

	private static final String CONSUMO_LIDO = "consumoLido";

	private static final String MATRICULA_IMOVEL = "matriculaImovel";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";

	private static final String PONTO_SEM_CONSUMO = "pontoSemConsumo";

	private static final String IDS_ANORMALIDADE_CONSUMO = "idsAnormalidadeConsumo";

	private static final String IDS_ANORMALIDADE_LEITURA = "idsAnormalidadeLeitura";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ID_SITUACAO_PONTO_CONSUMO = "idSituacaoPontoConsumo";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_CLIENTE = "idCliente";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LAZY_ROTA_EMPRESA = "rota.empresa";

	private static final String LAZY_ROTA_GRUPO_FATURAMENTO = "rota.grupoFaturamento";

	private static final String LISTA_PONTOS_CONSUMO = "listaPontosConsumo";

	private static final String PONTO_CONSUMO_LEGADO = "pontoConsumoLegado";

	private static final String INDICADOR_IMPEDIMENTO_FATURAMENTO = "indicadorImpedeFaturamento";

	private static final String DATA_LEITURA_INICIO = "dataLeituraInicio";

	private static final String DATA_LEITURA_FIM = "dataLeituraFim";
	
	private static final String DATA_ANO_MES_REFERENCIA = "dataLeituraFim";

	private static final String SCALA_CONSUMO_APURADO = "escalaConsumoApurado";

	@Autowired
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	private ControladorSetorComercial controladorSetorComercial;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	/**
	 * Método responsável por exibir a tela de pesquisa para análise de exceção de
	 * leitura de consumo.
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param bindingResult
	 *            {@link BindingResult}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaExcecaoLeituraConsumo")
	public String exibirPesquisaExcecaoLeituraConsumo(HttpServletRequest request, Model model,
			AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, BindingResult bindingResult) {

		if (!model.containsAttribute("analiseExcecaoLeituraVO")) {
			analiseExcecaoLeituraVO = new AnaliseExcecaoLeituraVO();
			analiseExcecaoLeituraVO.setComOcorrencia(true);
			analiseExcecaoLeituraVO.setAnalisada(false);
			model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		}

		try {
			carregarCampos(analiseExcecaoLeituraVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaExcecaoLeituraConsumo";
	}

	/**
	 * Abre a tela de análise de anormalidade de leitura/consumo com alguns filtros já preenchidos
	 * @param model O model
	 * @param cronogramaVO O CronogramaVO com os filtros preenchidos
	 * @param request A request
	 * @param bindingResult O bindingResult
	 * @return A tela de análise de anormalidade
	 * @throws NegocioException O NegocioException
	 */
	@RequestMapping("exibirAnaliseExcecaoLeitura")
	public String exibirAnaliseExcecaoLeitura(Model model, CronogramaVO cronogramaVO, HttpServletRequest request,
			BindingResult bindingResult) throws NegocioException {

		AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO = new AnaliseExcecaoLeituraVO();
		analiseExcecaoLeituraVO.setPontoConsumoLegado(cronogramaVO.getIdPontoConsumo());
		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(cronogramaVO.getIdPontoConsumo());

		LeituraMovimentoImpl pesquisaLeitura = new LeituraMovimentoImpl();
		pontoConsumo.setChavePrimaria(cronogramaVO.getIdPontoConsumo());
		Rota rota = pontoConsumo.getRota();
		GrupoFaturamento grupo = rota.getGrupoFaturamento();
		Long[] rotas = new Long[1];
		rotas[0] = rota.getChavePrimaria();
		analiseExcecaoLeituraVO.setIdsRota(rotas);
		analiseExcecaoLeituraVO.setIdGrupoFaturamento(grupo.getChavePrimaria());
		CronogramaFaturamento cronogramaFaturamento =
				controladorCronogramaFaturamento.obterCronogramaFaturamento(cronogramaVO.getChavePrimaria());
		pesquisaLeitura.setPontoConsumo(pontoConsumo);
		pesquisaLeitura.setAnoMesFaturamento(cronogramaFaturamento.getAnoMesFaturamento());
		pesquisaLeitura.setCiclo(cronogramaFaturamento.getNumeroCiclo());
		SituacaoLeituraMovimentoImpl situacaoLeituraMovimento = new SituacaoLeituraMovimentoImpl();
		situacaoLeituraMovimento.setChavePrimaria(SituacaoLeituraMovimento.PROCESSADO);
		pesquisaLeitura.setSituacaoLeitura(situacaoLeituraMovimento);

		LeituraMovimento leituraMovimento = controladorLeituraMovimento.consultarPorSituacaoAnoMesCicloPontoConsumo(pesquisaLeitura);
		if (leituraMovimento != null) {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String dataLeitura = dateFormat.format(leituraMovimento.getDataLeitura());
			analiseExcecaoLeituraVO.setDataLeituraInicio(dataLeitura);
			analiseExcecaoLeituraVO.setDataLeituraFim(dataLeitura);
		}

		model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		return exibirPesquisaExcecaoLeituraConsumo(request, model, analiseExcecaoLeituraVO, bindingResult);
	}
	
	private static final String ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS_ANALISE = "ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS_ANALISE";
	/**
	 * Método responsável por realizar a validação da referencia
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private Boolean validarReferenciaFiltro(Map<String, Object> filtro, AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO) throws GGASException {
		Integer anoMesLeitura = null;
		String numeroCiclo = null;
		if (analiseExcecaoLeituraVO.getAnoMesLeitura() != null
				&& StringUtils.isNotEmpty(analiseExcecaoLeituraVO.getAnoMesLeitura())) {
			anoMesLeitura = Integer
					.parseInt(analiseExcecaoLeituraVO.getAnoMesLeitura().replace("/", "").replaceAll("_", ""));
			filtro.put("anoMesReferencia", anoMesLeitura);
		}
		if (analiseExcecaoLeituraVO.getNumeroCiclo() != null) {
			numeroCiclo = analiseExcecaoLeituraVO.getNumeroCiclo();
			filtro.put("numeroCiclo", numeroCiclo);		
		}
		
		return numeroCiclo != null && anoMesLeitura != null;
	}


	/**
	 * Método responsável por realizar a pesquisa para análise de exceção de leitura
	 * de consumo.
	 * 
	 * @throws NegocioException
	 *
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("pesquisarExcecaoLeituraConsumo")
	public String pesquisarExcecaoLeituraConsumo(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<HistoricoConsumo> listaHistoricoConsumo = null;
		String tela = String.valueOf(request.getParameter("tela"));
		if (request.getSession().getAttribute(ANALISE_EXCECAO_LEITURA_PESQUISA) != null
				&& "detalhamento".equals(tela)) {
			analiseExcecaoLeituraVO = (AnaliseExcecaoLeituraVO) super.retornaPesquisa(request,
					ANALISE_EXCECAO_LEITURA_PESQUISA);
		}

		model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		
		if(validarReferenciaFiltro(filtro, analiseExcecaoLeituraVO) ||  verificaDataInicioDataFim(filtro, analiseExcecaoLeituraVO)) {

		try {
			if (analiseExcecaoLeituraVO.getIdCliente() != null) {
				ClienteImpl cliente = (ClienteImpl) controladorCliente.obter(analiseExcecaoLeituraVO.getIdCliente());
				model.addAttribute("cliente", cliente);
			}
			this.prepararFiltro(filtro, analiseExcecaoLeituraVO);
			listaHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumoAnaliseExcecoesLeitura(filtro);
		} catch (NegocioException e) {
			super.mensagemErro(model, e.getMessage());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}
		request.getSession().setAttribute("listaHistoricoConsumo", listaHistoricoConsumo);
		request.getSession().setAttribute(ANALISE_EXCECAO_LEITURA_PESQUISA, null);
		model.addAttribute("listaHistoricoConsumo", listaHistoricoConsumo);
		model.addAttribute(SCALA_CONSUMO_APURADO, buscarParametroEscalaConsumoApurado());

		} else {
			try {
				throw new NegocioException(ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS_ANALISE, true);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		} return exibirPesquisaExcecaoLeituraConsumo(request, model, analiseExcecaoLeituraVO, result);

		
	}
	
	/**
	 * Método responsável pela geração do relatório de análise de consumo.
	 *
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param chavesPrimarias
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return ResponseEntity {@link ResponseEntity}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@RequestMapping("gerarRelatorioAnaliseExcecaoConsumo")
	public String gerarRelatorioAnaliseExcecaoConsumo(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO,
			BindingResult result, @RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException, IOException {

		String view = null;
		model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		try {
			String[] chavesPrimariasAux = new String[chavesPrimarias.length];
			for (int i = 0; i < chavesPrimarias.length; i++) {
				chavesPrimariasAux[i] = String.valueOf(chavesPrimarias[i]);
			}

			if(analiseExcecaoLeituraVO.getIdsRota() == null) {	
				model.addAttribute("listaHistoricoConsumo", request.getSession().getAttribute("listaHistoricoConsumo"));
				throw new NegocioException(Constantes.ERRO_ROTA_NAO_INFORMADA, true);
			}
			
			if (chavesPrimariasAux != null && chavesPrimariasAux.length > 0) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				this.prepararFiltro(filtro, analiseExcecaoLeituraVO);
				
				byte[] bytes = controladorHistoricoConsumo.gerarRelatorioAnaliseConsumo(chavesPrimariasAux, filtro);

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				response.addHeader("Content-Disposition", "attachment; filename=relatorioAnaliseExcecao.pdf");
				servletOutputStream.write(bytes, 0, bytes.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			return exibirPesquisaExcecaoLeituraConsumo(request, model, analiseExcecaoLeituraVO, result);	
		} 
		return view;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento para análise de exceção
	 * de leitura de consumo.
	 *
	 * @param pontoConsumo
	 *            {@link PontoConsumo}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	public String descricaoFormatada(PontoConsumo pontoConsumo) throws Exception {

		StringBuilder descricaoFormatada = new StringBuilder();
		String separador = "";
		String nomeImovel;

		if (pontoConsumo.getImovel().getNome() != null) {
			nomeImovel = pontoConsumo.getImovel().getNome().trim();
		} else {
			nomeImovel = "";
		}

		if (nomeImovel.length() > 0) {
			descricaoFormatada.append(pontoConsumo.getDescricaoFormatada());
		} else {
			Cliente cliente = controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria());
			if (cliente != null && !StringUtils.isEmpty(cliente.getNome())) {
				descricaoFormatada.append(cliente.getNome());
				separador = " - ";
			}
			if (!StringUtils.isEmpty(pontoConsumo.getDescricao())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getDescricao());
				separador = " - ";
			}

			Cep cepEndereco = pontoConsumo.getCep();
			if (cepEndereco == null) {
				cepEndereco = pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep();
			}
			if (!StringUtils.isEmpty(cepEndereco.getTipoLogradouro())) {
				descricaoFormatada.append(separador);
				descricaoFormatada.append(cepEndereco.getTipoLogradouro());
				if (StringUtils.isEmpty(cepEndereco.getLogradouro())) {
					separador = ", ";
				} else {
					separador = " ";
				}

			}
			if (!StringUtils.isEmpty(cepEndereco.getLogradouro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getLogradouro());
				separador = ", ";
			}

			if (!StringUtils.isEmpty(pontoConsumo.getNumeroImovel())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getNumeroImovel());
				separador = ", ";
			} else {
				descricaoFormatada.append(separador).append(pontoConsumo.getImovel().getNumeroImovel());
				separador = ", ";
			}
			if (!StringUtils.isEmpty(pontoConsumo.getDescricaoComplemento())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getDescricaoComplemento());
				separador = ", ";
			}
			if (!StringUtils.isEmpty(cepEndereco.getBairro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getBairro());
			}
		}

		return descricaoFormatada.toString();

	}

	/**
	 * Exibir detalhamento exceção leitura consumo.
	 * 
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param chavePrimariaPontoConsumo
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirDetalhamentoExcecaoLeituraConsumoAnalise")
	public String exibirDetalhamentoExcecaoLeituraConsumo(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO,
			BindingResult result, @RequestParam(CHAVE_PRIMARIA) Long chavePrimariaPontoConsumo,
			HttpServletRequest request, Model model) {

		String view = "exibirDetalhamentoExcecaoLeituraConsumo";

		super.salvarPesquisa(request, analiseExcecaoLeituraVO, ANALISE_EXCECAO_LEITURA_PESQUISA);
		try {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavePrimariaPontoConsumo,
					"situacaoConsumo", "segmento", "rota", LAZY_ROTA_GRUPO_FATURAMENTO, "imovel",
					"imovel.listaPontoConsumo", "instalacaoMedidor", "imovel.quadraFace.endereco.cep");
			Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
					.consultarHistoricoConsumo(chavePrimariaPontoConsumo, true, null, null);

			Collection<HistoricoConsumo> listaHistoricoConsumoParaMedicao = controladorHistoricoConsumo
					.consultarHistoricoConsumoParaAnaliseExcecoes(chavePrimariaPontoConsumo, true, null, null);
			
			String anoMesReferencia;
			String ciclo;
			if (pontoConsumo.getRota() != null) {
				anoMesReferencia = String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getAnoMesReferencia());
				ciclo = String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getNumeroCiclo());
			} else {
				anoMesReferencia = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
				ciclo = String.valueOf(1);

			}

			String anoMesCiclo = anoMesReferencia + ciclo;

			// Montar um VO para exibição com
			// indicação se existe comentário para a
			// linha
			Collection<HistoricoMedicaoAgrupadoVO> listaHistoricoMedicaoAgrupadoVO = new ArrayList<HistoricoMedicaoAgrupadoVO>();

			if (listaHistoricoConsumoParaMedicao != null && !listaHistoricoConsumoParaMedicao.isEmpty()) {
				for (HistoricoConsumo historicoConsumo : listaHistoricoConsumoParaMedicao) {

						
					HistoricoMedicaoAgrupadoVO historico = new HistoricoMedicaoAgrupadoVO();
					historico.setHistoricoMedicao(historicoConsumo.getHistoricoAtual());
					historico.setExisteComentario(historicoConsumo.getHistoricoAtual().getExisteComentario());
					historico.setDataLeituraUltimoDiaCicloAnterior(controladorHistoricoMedicao.obterDataMedicaoAnterior(historicoConsumo));
					listaHistoricoMedicaoAgrupadoVO.add(historico);
						
					String ano = anoMesReferencia.substring(0, 4);
					String mes = anoMesReferencia.substring(4, 6);
					String anoMesAnterior = String.valueOf((Integer.parseInt(ano) - 1)) + mes;
						
					if (historicoConsumo.getHistoricoAtual().getAnoMesCicloFormatado() >= Integer.valueOf(anoMesCiclo)) {
						Collection<HistoricoConsumo> historicoConsumosAnoAnterior = controladorHistoricoConsumo
								.consultarHistoricoConsumo(chavePrimariaPontoConsumo, Integer.parseInt(anoMesAnterior),
										Integer.parseInt(ciclo), true, null, null);
						
						if (CollectionUtils.isNotEmpty(historicoConsumosAnoAnterior)) {
							HistoricoConsumo historicoConsumoAnoAnterior = historicoConsumosAnoAnterior.iterator().next();
							historico.setConsumoAnoAnterior(historicoConsumoAnoAnterior.getConsumo());
						}
						
						historico.setConsumoAnoAtual(historicoConsumo.getConsumo());
					}
				}
			}

			model.addAttribute("clientePrincipal",
					controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimariasImoveis", new Long[] { pontoConsumo.getImovel().getChavePrimaria() });
			Collection<PontoConsumo> listaPontosConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);

			listaPontosConsumo.remove(pontoConsumo);
			Collection<PontoConsumo> listaPontosConsumoAux = new ArrayList<PontoConsumo>();

			for (PontoConsumo pontoConsumoAux : listaPontosConsumo) {
				if (pontoConsumoAux.getRota() != null && pontoConsumoAux.getRota().getGrupoFaturamento() != null) {
					listaPontosConsumoAux.add(pontoConsumoAux);
				}
			}

			Collection<PontoConsumoVO> listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumoAux);
			model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);

			model.addAttribute("pontoConsumo", pontoConsumo);
			model.addAttribute("listaHistoricoConsumo", listaHistoricoConsumo);
			model.addAttribute("listaHistoricoMedicao", listaHistoricoConsumoParaMedicao);
			model.addAttribute("listaHistoricoMedicaoVO", listaHistoricoMedicaoAgrupadoVO);
			model.addAttribute("listaAnormalidadeLeitura", controladorAnormalidade.listarAnormalidadesLeitura());
			model.addAttribute("listaLeiturista", controladorRota.listarLeituristas());
			model.addAttribute("comentarioObrigatorio", controladorHistoricoMedicao.exigeComentarioAnaliseExcecao());
			model.addAttribute("anoMesReferencia",
					Util.converterCampoStringParaValorInteger("Parâmetro Ano/Mês de Faturamento", anoMesCiclo));

			model.addAttribute("intervaloAnosData", intervaloAnosData());
			model.addAttribute(SCALA_CONSUMO_APURADO, buscarParametroEscalaConsumoApurado());

			String gerarCreditoVolumeAutomatico = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_GERA_CREDITO_VOLUME_AUTOMATICAMENTE);
			model.addAttribute("gerarCreditoVolumeAutomatico", "1".equals(gerarCreditoVolumeAutomatico));

			saveToken(request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaExcecaoLeituraConsumo(request, model, analiseExcecaoLeituraVO, result);
		}

		return view;
	}

	/**
	 * Montar lista pontos consumo vo.
	 *
	 * @param listaPontosConsumo
	 *            {@link Collection}
	 * @return Collection {@link Collection}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private Collection<PontoConsumoVO> montarListaPontosConsumoVo(Collection<PontoConsumo> listaPontosConsumo)
			throws GGASException {

		List<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			pontoVO = new PontoConsumoVO();
			PontoConsumo pontoConsumoCompleto = (PontoConsumo) controladorPontoConsumo.obter(
					pontoConsumo.getChavePrimaria(), LAZY_ROTA_GRUPO_FATURAMENTO, LAZY_ROTA_EMPRESA,
					LAZY_INSTALACAO_MEDIDOR_MEDIDOR, ROTA_LEITURISTA_FUNCIONARIO);
			pontoVO.setPontoConsumo(pontoConsumoCompleto);

			Map<String, Integer> referenciaCiclo = controladorPontoConsumo
					.obterReferenciaCicloAtual(pontoConsumoCompleto);

			if (referenciaCiclo != null) {
				pontoVO.setCiclo(String.valueOf(referenciaCiclo.get("ciclo")));
				pontoVO.setReferencia(Util.formatarAnoMes(referenciaCiclo.get("referencia")));
			}

			listaPontosConsumoVO.add(pontoVO);

		}

		Collections.sort(listaPontosConsumoVO, new Comparator<PontoConsumoVO>() {

			@Override
			public int compare(PontoConsumoVO o1, PontoConsumoVO o2) {

				int retorno = 0;
				if (o2 == null || o2.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
					retorno = -1;
				} else {
					if (o1 == null || o1.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
						retorno = 1;
					} else {
						retorno = o1.getPontoConsumo().getNumeroSequenciaLeitura()
								.compareTo(o2.getPontoConsumo().getNumeroSequenciaLeitura());
					}
				}
				return retorno;
			}
		});

		return listaPontosConsumoVO;
	}

	/**
	 * Intervalo anos data.
	 *
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Método responsável por alterar os dados de faturamento de um histórico de
	 * leitura de um ponto de consumo.
	 * 
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 * @throws IOException
	 *             {@link IOException}
	 */
	@RequestMapping("alterarDadosFaturamento")
	public String alterarDadosFaturamento(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException, IOException {

		String view = null;
		model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		try {
			getDadosAuditoria(request);
			validarToken(request);
			MedicaoHistoricoComentario medicaoHistoricoComentario = (MedicaoHistoricoComentario) controladorHistoricoMedicao
					.criarMedicaoHistoricoComentario();
			popularMedicaoHistoricoComentario(medicaoHistoricoComentario, analiseExcecaoLeituraVO, request);

			HistoricoMedicao historicoMedicao = null;
			if (analiseExcecaoLeituraVO.getIdHistoricoMedicao() != null
					&& analiseExcecaoLeituraVO.getIdHistoricoMedicao() > 0) {
				historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao
						.obter(analiseExcecaoLeituraVO.getIdHistoricoMedicao());
				
				popularHistoricoMedicao(historicoMedicao, analiseExcecaoLeituraVO, request);
			}
			
			

			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
					.obter(analiseExcecaoLeituraVO.getChavePrimaria(), "imovel", "instalacaoMedidor");
	
			controladorHistoricoMedicao.atualizarDadosFaturamentoLeitura(pontoConsumo, historicoMedicao,
					medicaoHistoricoComentario, analiseExcecaoLeituraVO.getExecutarConsistir());
					
			
			if (analiseExcecaoLeituraVO.getGerarLog() != null
					&& !"".equals(analiseExcecaoLeituraVO.getGerarLog().trim())) {

				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
						LogAnaliseLeituraConsumoTemp.getInstance().getSb().length());
				BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(byteArrayOutputStream));

				bufferedWriter.write(LogAnaliseLeituraConsumoTemp.getInstance().getSb().toString());
				bufferedWriter.close();
				byteArrayOutputStream.close();

				byte[] bytes = byteArrayOutputStream.toByteArray();
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType("text/plain");
				response.setContentLength(bytes.length);
				response.addHeader("Content-Disposition", "attachment; filename=LOG_CALCULO_PCS.txt");
				servletOutputStream.write(bytes, 0, bytes.length);
				servletOutputStream.flush();
				servletOutputStream.close();

				LogAnaliseLeituraConsumoTemp.getInstance().limparInfo();

			} else {
				if (analiseExcecaoLeituraVO.getIdAnormalidadeLeituraIdentificada() != null && analiseExcecaoLeituraVO
						.getIdAnormalidadeLeituraIdentificada() == codigoAnormalidadeLeitura) {
					mensagemSucesso(model, "Adicionado com sucesso a anormalidade para não faturar o ponto");
					view = pesquisarExcecaoLeituraConsumo(analiseExcecaoLeituraVO, result, request, model);
				} else {

					view = exibirDetalhamentoExcecaoLeituraConsumo(analiseExcecaoLeituraVO, result,
							analiseExcecaoLeituraVO.getIdPontoConsumo(), request, model);
				}
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);

			view = exibirDetalhamentoExcecaoLeituraConsumo(analiseExcecaoLeituraVO, result,
					analiseExcecaoLeituraVO.getIdPontoConsumo(), request, model);

		}

		return view;
	}

	/**
	 * Popular medicao historico comentario.
	 *
	 * @param medicaoHistoricoComentario
	 *            {@link MedicaoHistoricoComentario}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularMedicaoHistoricoComentario(MedicaoHistoricoComentario medicaoHistoricoComentario,
			AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, HttpServletRequest request) throws GGASException {

		if (analiseExcecaoLeituraVO.getIdHistoricoMedicao() == null
				|| analiseExcecaoLeituraVO.getIdHistoricoMedicao() <= 0) {
			medicaoHistoricoComentario.setHistoricoMedicao(null);
		} else {
			HistoricoMedicao historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao
					.obter(analiseExcecaoLeituraVO.getIdHistoricoMedicao());
			medicaoHistoricoComentario.setHistoricoMedicao(historicoMedicao);
		}

		if (analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura())) {
			medicaoHistoricoComentario.setDescricao(null);
		} else {
			analiseExcecaoLeituraVO
					.setComentarioAlteracaoLeitura(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().trim());
			if (analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().length() > STR_MAX_LEN) {
				analiseExcecaoLeituraVO.setComentarioAlteracaoLeitura(
						analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().substring(STR_MAX_LEN));
			}
			medicaoHistoricoComentario.setDescricao(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura());
		}

		Usuario usuario = super.obterUsuario(request);
		medicaoHistoricoComentario.setUsuario(usuario);
		medicaoHistoricoComentario.setUltimaAlteracao(Calendar.getInstance().getTime());
		medicaoHistoricoComentario.setVersao(1);
		medicaoHistoricoComentario.setDadosAuditoria(getDadosAuditoria(request));
	}

	/**
	 * Método responsável por alterar os dados de um histórico de consumo de um
	 * ponto de consumo.
	 *
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param result
	 *            {@link BindingResult} {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("alterarDadosConsumo")
	public String alterarDadosConsumo(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("analiseExcecaoLeituraVO", analiseExcecaoLeituraVO);
		ComentarioPontoConsumo comentarioPontoConsumo = (ComentarioPontoConsumo) controladorPontoConsumo
				.criarComentarioPontoConsumo();
		popularComentarioPontoConsumo(comentarioPontoConsumo, analiseExcecaoLeituraVO, request);
		

		return exibirDetalhamentoExcecaoLeituraConsumo(analiseExcecaoLeituraVO, result,
				analiseExcecaoLeituraVO.getIdPontoConsumo(), request, model);
	}

	/**
	 * Preparar filtro.
	 *
	 * @param filtro
	 *            {@link Map}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO)
			throws GGASException {

		consultaDadosPontoConsumo(filtro, analiseExcecaoLeituraVO);

		if (analiseExcecaoLeituraVO.getIdsRota() != null && analiseExcecaoLeituraVO.getIdsRota().length > 0) {
			filtro.put(IDS_ROTA, analiseExcecaoLeituraVO.getIdsRota());
		}
		if (analiseExcecaoLeituraVO.getIdsAnormalidadeConsumo() != null
				&& analiseExcecaoLeituraVO.getIdsAnormalidadeConsumo().length > 0) {
			filtro.put(IDS_ANORMALIDADE_CONSUMO, analiseExcecaoLeituraVO.getIdsAnormalidadeConsumo());
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getNumeroOcorrenciaAnormalidadeConsumo())) {
			filtro.put("quantidadeAnormalidadeConsumoConsecutivas", Util.converterCampoStringParaValorInteger(
					"Número de Ocorrencias", analiseExcecaoLeituraVO.getNumeroOcorrenciaAnormalidadeConsumo()));
		}
		if (analiseExcecaoLeituraVO.getPontoSemConsumo() != null) {
			filtro.put(PONTO_SEM_CONSUMO, analiseExcecaoLeituraVO.getPontoSemConsumo());
		}
		if ((analiseExcecaoLeituraVO.getIdTipoConsumo() != null) && (analiseExcecaoLeituraVO.getIdTipoConsumo() > 0)) {
			filtro.put(ID_TIPO_CONSUMO, analiseExcecaoLeituraVO.getIdTipoConsumo());
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getConsumoLido())) {
			filtro.put(CONSUMO_LIDO,
					Util.converterCampoStringParaValorBigDecimal("Consumo Lido",
							analiseExcecaoLeituraVO.getConsumoLido(), Constantes.FORMATO_VALOR_FATOR_K,
							Constantes.LOCALE_PADRAO));
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getPercentualVariacaoConsumo())) {
			filtro.put(PERCENTUAL_VARIACAO_CONSUMO,
					Util.converterCampoStringParaValorBigDecimal("Percentual de Variação do Consumo em Relação à Média",
							analiseExcecaoLeituraVO.getPercentualVariacaoConsumo(), Constantes.FORMATO_VALOR_FATOR_K,
							Constantes.LOCALE_PADRAO));
		}
		if (analiseExcecaoLeituraVO.getAnalisada() != null) {
			filtro.put(ANALISADA, analiseExcecaoLeituraVO.getAnalisada());
		}
		if (analiseExcecaoLeituraVO.getComOcorrencia() != null) {
			filtro.put("comOcorrencia", analiseExcecaoLeituraVO.getComOcorrencia());
		}
		if ((analiseExcecaoLeituraVO.getPontoConsumoLegado() != null)
				&& (analiseExcecaoLeituraVO.getPontoConsumoLegado() > 0)) {
			filtro.put(PONTO_CONSUMO_LEGADO, String.valueOf(analiseExcecaoLeituraVO.getPontoConsumoLegado()));
		}
		if (analiseExcecaoLeituraVO.getIndicadorImpedeFaturamento() != null) {
			filtro.put(INDICADOR_IMPEDIMENTO_FATURAMENTO, analiseExcecaoLeituraVO.getIndicadorImpedeFaturamento());
		}

		verificaDataInicioDataFim(filtro, analiseExcecaoLeituraVO);
	}

	/**
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void consultaDadosPontoConsumo(Map<String, Object> filtro, AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO)
			throws GGASException {

		if (analiseExcecaoLeituraVO.getIdCliente() != null && analiseExcecaoLeituraVO.getIdCliente() > 0) {
			filtro.put(ID_CLIENTE, analiseExcecaoLeituraVO.getIdCliente());
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getCepImovel())) {
			filtro.put(CEP_IMOVEL, analiseExcecaoLeituraVO.getCepImovel());
		}
		if (analiseExcecaoLeituraVO.getMatriculaImovel() != null
				&& !StringUtils.isEmpty(analiseExcecaoLeituraVO.getMatriculaImovel())) {
			controladorImovel.existeImovelPorMatricula(Util.converterCampoStringParaValorLong(
					"Matrícula do imóvel condomínio", analiseExcecaoLeituraVO.getMatriculaImovel()));
			filtro.put(MATRICULA_IMOVEL, Util.converterCampoStringParaValorLong(Imovel.MATRICULA,
					analiseExcecaoLeituraVO.getMatriculaImovel()));
		}
		if (analiseExcecaoLeituraVO.getIndicadorCondominio() != null) {
			filtro.put(INDICADOR_CONDOMINIO, analiseExcecaoLeituraVO.getIndicadorCondominio());
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getNomeImovel())) {
			filtro.put(NOME_IMOVEL, analiseExcecaoLeituraVO.getNomeImovel());
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getComplementoImovel())) {
			filtro.put(COMPLEMENTO_IMOVEL, Util.formatarTextoConsulta(analiseExcecaoLeituraVO.getComplementoImovel()));
		}
		if (!StringUtils.isEmpty(analiseExcecaoLeituraVO.getNumeroImovel())) {
			filtro.put(NUMERO_IMOVEL, analiseExcecaoLeituraVO.getNumeroImovel());
		}
		if (analiseExcecaoLeituraVO.getIdLocalidade() != null && analiseExcecaoLeituraVO.getIdLocalidade() > 0) {
			filtro.put(ID_LOCALIDADE, analiseExcecaoLeituraVO.getIdLocalidade());
		}
		if (analiseExcecaoLeituraVO.getIdSetorComercial() != null
				&& analiseExcecaoLeituraVO.getIdSetorComercial() > 0) {
			filtro.put(ID_SETOR_COMERCIAL, analiseExcecaoLeituraVO.getIdSetorComercial());
		}
		if (analiseExcecaoLeituraVO.getIdGrupoFaturamento() != null
				&& analiseExcecaoLeituraVO.getIdGrupoFaturamento() > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, analiseExcecaoLeituraVO.getIdGrupoFaturamento());
		}
		if (analiseExcecaoLeituraVO.getIdsAnormalidadeLeitura() != null
				&& analiseExcecaoLeituraVO.getIdsAnormalidadeLeitura().length > 0) {
			filtro.put(IDS_ANORMALIDADE_LEITURA, analiseExcecaoLeituraVO.getIdsAnormalidadeLeitura());
		}
		if (analiseExcecaoLeituraVO.getIdSegmento() != null && analiseExcecaoLeituraVO.getIdSegmento() > 0) {
			filtro.put(ID_SEGMENTO, analiseExcecaoLeituraVO.getIdSegmento());
		}
		if (analiseExcecaoLeituraVO.getIdSituacaoPontoConsumo() != null
				&& analiseExcecaoLeituraVO.getIdSituacaoPontoConsumo() > 0) {
			filtro.put(ID_SITUACAO_PONTO_CONSUMO, analiseExcecaoLeituraVO.getIdSituacaoPontoConsumo());
		}
	}

	/**
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private boolean verificaDataInicioDataFim(Map<String, Object> filtro, AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO)
			throws GGASException {
		Date dataLeituraInicio = null;
		Date dataLeituraFim = null;
		if (analiseExcecaoLeituraVO.getDataLeituraInicio() != null
				&& StringUtils.isNotEmpty(analiseExcecaoLeituraVO.getDataLeituraInicio())) {
			dataLeituraInicio = Util.converterCampoStringParaData("Período inicial da leitura",
					analiseExcecaoLeituraVO.getDataLeituraInicio(), Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_LEITURA_INICIO, dataLeituraInicio);
		}
		if (analiseExcecaoLeituraVO.getDataLeituraFim() != null
				&& StringUtils.isNotEmpty(analiseExcecaoLeituraVO.getDataLeituraFim())) {
			dataLeituraFim = Util.converterCampoStringParaData("Período final da leitura",
					analiseExcecaoLeituraVO.getDataLeituraFim(), Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_LEITURA_FIM, dataLeituraFim);
		}
		
		return dataLeituraInicio != null && dataLeituraFim != null;
		
	}

	/**
	 * Carregar campos.
	 * 
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void carregarCampos(AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, Model model) throws GGASException {

		model.addAttribute("listaLocalidade", controladorLocalidade.listarLocalidades());
		model.addAttribute("listaSetorComercial", controladorSetorComercial.listarSetoresComerciais());
		model.addAttribute("listaGrupoFaturamento", controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaAnormalidadeLeitura", controladorAnormalidade.listarAnormalidadesLeitura());
		model.addAttribute("listaAnormalidadeConsumo", controladorAnormalidade.listarAnormalidadesConsumo());
		model.addAttribute("listaTiposConsumo", controladorHistoricoConsumo.listarTiposConsumo());
		model.addAttribute("listaSituacaoPontoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
		model.addAttribute("listaCiclo", controladorCronogramaFaturamento.listarCiclosCronograma());


		Long idGrupoFaturamento = analiseExcecaoLeituraVO.getIdGrupoFaturamento();
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			model.addAttribute("listaRota", controladorRota.listarRotasPorGrupoFaturamento(idGrupoFaturamento));
		}
	}

	/**
	 * Popular comentario ponto consumo.
	 * 
	 * @param comentarioPontoConsumo
	 *            {@link ComentarioPontoConsumo}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @throws Exception
	 *             {@link Exception}
	 */
	private void popularComentarioPontoConsumo(ComentarioPontoConsumo comentarioPontoConsumo,
			AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, HttpServletRequest request) throws Exception {

		if (analiseExcecaoLeituraVO.getIdPontoConsumo() == null || analiseExcecaoLeituraVO.getIdPontoConsumo() <= 0) {
			comentarioPontoConsumo.setPontoConsumo(null);
		} else {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
					.obter(analiseExcecaoLeituraVO.getIdPontoConsumo());
			comentarioPontoConsumo.setPontoConsumo(pontoConsumo);
		}

		if (analiseExcecaoLeituraVO.getIdHistoricoMedicao() == null
				|| analiseExcecaoLeituraVO.getIdHistoricoMedicao() <= 0) {
			comentarioPontoConsumo.setNumero(null);
		} else {
			comentarioPontoConsumo.setNumero(analiseExcecaoLeituraVO.getIdHistoricoMedicao().intValue());
		}

		if (analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura())) {
			comentarioPontoConsumo.setDescricao(null);
		} else {
			analiseExcecaoLeituraVO
					.setComentarioAlteracaoLeitura(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().trim());
			if (analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().length() > STR_MAX_LEN) {
				analiseExcecaoLeituraVO.setComentarioAlteracaoLeitura(
						analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura().substring(STR_MAX_LEN));
			}
			comentarioPontoConsumo.setDescricao(analiseExcecaoLeituraVO.getComentarioAlteracaoLeitura());
		}

		Usuario usuario = super.obterUsuario(request);
		comentarioPontoConsumo.setUsuario(usuario);

		Operacao operacao = super.obterOperacao(request);
		comentarioPontoConsumo.setOperacao(operacao);

		comentarioPontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
	}

	/**
	 * Popular historico medicao.
	 *
	 * @param historicoMedicao
	 *            {@link HistoricoMedicao}
	 * @param analiseExcecaoLeituraVO
	 *            {@link AnaliseExcecaoLeituraVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void popularHistoricoMedicao(HistoricoMedicao historicoMedicao,
			AnaliseExcecaoLeituraVO analiseExcecaoLeituraVO, HttpServletRequest request) throws GGASException {

		if (analiseExcecaoLeituraVO.getValorLeituraMedida() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getValorLeituraMedida())) {
			historicoMedicao.setNumeroLeituraInformada(null);
		} else {
			historicoMedicao.setNumeroLeituraInformada(Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.NUMERO_LEITURA_INFORMADA, analiseExcecaoLeituraVO.getValorLeituraMedida(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		if (analiseExcecaoLeituraVO.getDataRealizacaoLeitura() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getDataRealizacaoLeitura())) {
			historicoMedicao.setDataLeituraInformada(null);
		} else {

			historicoMedicao
					.setDataLeituraInformada(Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_INFORMADA,
							analiseExcecaoLeituraVO.getDataRealizacaoLeitura(), Constantes.FORMATO_DATA_BR));
		}
		if (analiseExcecaoLeituraVO.getIdAnormalidadeLeituraIdentificada() == null
				|| analiseExcecaoLeituraVO.getIdAnormalidadeLeituraIdentificada() <= 0) {
			historicoMedicao.setAnormalidadeLeituraInformada(null);
			historicoMedicao.setAnormalidadeLeituraFaturada(null);
		} else {
			AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade
					.obterAnormalidadeLeitura(analiseExcecaoLeituraVO.getIdAnormalidadeLeituraIdentificada());
			historicoMedicao.setAnormalidadeLeituraInformada(anormalidadeLeitura);
		}

		if (analiseExcecaoLeituraVO.getValorLeituraAnterior() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getValorLeituraAnterior())) {
			historicoMedicao.setNumeroLeituraAnterior(null);
		} else {
			historicoMedicao.setNumeroLeituraAnterior(Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.NUMERO_LEITURA_ANTERIOR, analiseExcecaoLeituraVO.getValorLeituraAnterior(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		if (analiseExcecaoLeituraVO.getDataLeituraAnterior() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getDataLeituraAnterior())) {
			historicoMedicao.setDataLeituraAnterior(null);
		} else {
			historicoMedicao
					.setDataLeituraAnterior(Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_ANTERIOR,
							analiseExcecaoLeituraVO.getDataLeituraAnterior(), Constantes.FORMATO_DATA_BR));
		}

		if (analiseExcecaoLeituraVO.getIdLeiturista() == null || analiseExcecaoLeituraVO.getIdLeiturista() <= 0) {
			historicoMedicao.setLeiturista(null);
		} else {
			Leiturista leiturista = controladorRota.obterLeiturista(analiseExcecaoLeituraVO.getIdLeiturista());
			historicoMedicao.setLeiturista(leiturista);
		}

		if (analiseExcecaoLeituraVO.getVolumeApurado() == null
				|| StringUtils.isEmpty(analiseExcecaoLeituraVO.getVolumeApurado())) {
			historicoMedicao.setConsumoInformado(null);
		} else {
			historicoMedicao.setConsumoInformado(Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.CONSUMO_INFORMADO, analiseExcecaoLeituraVO.getVolumeApurado(),
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (analiseExcecaoLeituraVO.getCreditoVolume() != null
				&& !StringUtils.isEmpty(analiseExcecaoLeituraVO.getCreditoVolume())) {
			BigDecimal valorCreditoVolume = Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.CREDITO_GERADO, analiseExcecaoLeituraVO.getCreditoVolume(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO);
			controladorHistoricoMedicao.calcularCreditoGerado(historicoMedicao, valorCreditoVolume);
		} else {
			historicoMedicao.setCreditoGerado(null);
			historicoMedicao.setCreditoSaldo(null);
		}

		if (StringUtils.isEmpty(analiseExcecaoLeituraVO.getInformadoCliente())) {
			historicoMedicao.setIndicadorInformadoCliente(Boolean.FALSE);
		} else {
			historicoMedicao
					.setIndicadorInformadoCliente(Boolean.valueOf(analiseExcecaoLeituraVO.getInformadoCliente()));
		}
		
		historicoMedicao.setConsumoCorretor(historicoMedicao.getNumeroLeituraInformada().subtract(historicoMedicao.getNumeroLeituraAnterior()));	
		
		historicoMedicao.setDadosAuditoria(getDadosAuditoria(request));
	}

	private String buscarParametroEscalaConsumoApurado() throws NegocioException {
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		return controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO)
				.getValor();
	}

}
