package br.com.ggas.web.medicao.vazaocorretor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.ModeloCorretorImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes ao Modelo do Corretor.
 */
@Controller
public class ModeloCorretorAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String CORRETOR_MODELO = "corretorModelo";

	private static final String EXIBIR_ATUALIZAR_CORRETOR_MODELO = "exibirAtualizarCorretorModelo";

	private static final String EXIBIR_INSERIR_CORRETOR_MODELO = "exibirInserirCorretorModelo";

	private static final Class<ModeloCorretorImpl> CLASSE = ModeloCorretorImpl.class;

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_CORRETOR_MODELO = "listaCorretorModelo";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa Modelo do Corretor de Vazão.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaCorretorModelo")
	public String exibirPesquisaCorretorModelo(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaCorretorModelo";
	}

	/**
	 * Método responsável por pesquisar um Modelo de Corretor de Vazão.
	 * 
	 * @param corretorModelo - {@link ModeloCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarCorretorModelo")
	public String pesquisarCorretorModelo(ModeloCorretorImpl corretorModelo, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(corretorModelo, habilitado);

		Collection<TabelaAuxiliar> listaCorretorModelo =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, corretorModelo.getClass().getName());

		model.addAttribute(LISTA_CORRETOR_MODELO, listaCorretorModelo);
		model.addAttribute(CORRETOR_MODELO, corretorModelo);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaCorretorModelo(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de um Modelo do Corretor de Vazão.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoCorretorModelo")
	public String exibirDetalhamentoCorretorModelo(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			ModeloCorretor corretorModelo = (ModeloCorretor) controladorTabelaAuxiliar.obter(chavePrimaria, ModeloCorretorImpl.class);
			model.addAttribute(CORRETOR_MODELO, corretorModelo);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCorretorModelo";
		}

		return "exibirDetalhamentoCorretorModelo";
	}

	/**
	 * Método responsável por exibir a tela de inserir Modelo do Corretor de Vazão.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_CORRETOR_MODELO)
	public String exibirInserirCorretorModelo() {

		return EXIBIR_INSERIR_CORRETOR_MODELO;
	}

	/**
	 * Método responsável por inserir um Modelo do Corretor de Vazão .
	 * 
	 * @param corretorModelo - {@link ModeloCorretorImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirCorretorModelo")
	private String inserirCorretorModelo(ModeloCorretorImpl corretorModelo, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;
	
		try {
			if (corretorModelo.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (corretorModelo.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(corretorModelo);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(corretorModelo, "Corretor Marca");
			controladorTabelaAuxiliar.inserir(corretorModelo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(ModeloCorretor.CORRETOR_MODELO));
			retorno = pesquisarCorretorModelo(corretorModelo, result, model, corretorModelo.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CORRETOR_MODELO, corretorModelo);
			retorno = EXIBIR_INSERIR_CORRETOR_MODELO;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar o Modelo do Corretor de Vazão.
	 * 
	 * @param corretorModelo- {@link ModeloCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param corretorModeloParam - {@link ModeloCorretorImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_CORRETOR_MODELO)
	public String exibirAtualizarCorretorModelo(ModeloCorretorImpl corretorModeloParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		ModeloCorretor corretorModelo = corretorModeloParam;

		try {
			corretorModelo = (ModeloCorretor) controladorTabelaAuxiliar.obter(corretorModelo.getChavePrimaria(), CLASSE);
			model.addAttribute(CORRETOR_MODELO, corretorModelo);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCorretorModelo";
		}

		return EXIBIR_ATUALIZAR_CORRETOR_MODELO;
	}

	/**
	 * Método responsável por atualizar um Modelo do Corretor de Vazão.
	 * 
	 * @param corretorModelo - {@link ModeloCorretorImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarCorretorModelo")
	private String atualizarCorretorModelo(ModeloCorretorImpl corretorModelo, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (corretorModelo.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (corretorModelo.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(corretorModelo);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(corretorModelo, "Modelo do Corretor");
			controladorTabelaAuxiliar.atualizar(corretorModelo, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(ModeloCorretor.CORRETOR_MODELO));
			retorno = pesquisarCorretorModelo(corretorModelo, result, model, corretorModelo.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CORRETOR_MODELO, corretorModelo);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_CORRETOR_MODELO;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover um Modelo do Corretor de Vazão.
	 * 
	 * @param corretorModelo {@link CorretorModeloImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerCorretorModelo")
	public String removerCorretorModelo(ModeloCorretorImpl corretorModelo, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(ModeloCorretor.CORRETOR_MODELO));
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarCorretorModelo(corretorModelo, result, model, Boolean.TRUE);
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa do Modelo do Corretor de Vazão.
	 * 
	 * @param corretorModelo - {@link ModeloCorretor}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(ModeloCorretor corretorModelo, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (corretorModelo.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, corretorModelo.getChavePrimaria());
		}

		if (StringUtils.isNotEmpty(corretorModelo.getDescricao())) {
			filtro.put(DESCRICAO, corretorModelo.getDescricao());
		}

		if (StringUtils.isNotEmpty(corretorModelo.getDescricaoAbreviada())) {
			filtro.put(DESCRICAO_ABREVIADA, corretorModelo.getDescricaoAbreviada());
		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
