/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe RotaAction representa uma RotaAction no sistema.
 *
 * @since 25/09/2009
 * 
 */

package br.com.ggas.web.medicao.rota;

import static br.com.ggas.medicao.rota.ControladorRota.ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.RegrasOrdenacaoPontoConsumoRota;
import br.com.ggas.medicao.rota.impl.RotaHistorico;
import br.com.ggas.medicao.rota.impl.RotaImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Controller responsável pelos métodos relacionados as telas de Rota
 *
 */
@Controller
public class RotaAction extends GenericAction {

	private static final String ROTA = "rota";

	private static final String FORWARD_PESQUISAR_ROTA = "forward:pesquisarRota";

	private static final String FSETOR_COMERCIAL = "setorComercial";

	private static final String FGRUPO_FATURAMENTO = "grupoFaturamento";

	private static final String FPERIODICIDADE = "periodicidade";

	private static final String FLEITURISTA = "leiturista";

	private static final String FEMPRESA = "empresa";

	private static final String FTIPO_LEITURA = "tipoLeitura";

	private static final String LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO = "listaPontoConsumoRemoverAssociacao";

	private static final String LISTA_CHAVES_PONTO_CONSUMO = "listaChavesPontoConsumo";

	private static final String LISTA_PONTO_CONSUMO_ROTA = "listaPontoConsumoRota";
	
	private static final String LISTA_PONTO_CONSUMO_ROTA_EXIBICAO = "listaPontoConsumoRotaExibicao";

	private static final String ID_ROTA_REMANEJAMENTO = "idRotaRemanejamento";

	private static final String PONTOS_CONSUMO_ASSOCIADOS = "pontosConsumoAssociados";

	private static final String ID_PONTOS_CONSUMO_ASSOCIADOS = "idPontosConsumoAssociados";

	private static final String LISTA_ROTA = "listaRota";

	private static final String LISTA_ROTA_REMANEJAMENTO = "listaRotaRemanejamento";

	private static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	private static final String LISTA_SETOR_COMERCIAL = "listaSetorComercial";

	private static final String LISTA_PERIODICIDADE = "listaPeriodicidade";

	private static final String LISTA_LEITURISTA = "listaLeiturista";

	private static final String LISTA_EMPRESA = "listaEmpresa";

	private static final String LISTA_TIPO_LEITURA = "listaTipoLeitura";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String TIPO_LEITURA = "idTipoLeitura";

	private static final String ID_TIPO_LEITURA = TIPO_LEITURA;

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String NUMERO_ROTA = "numeroRota";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_PERIODICIDADE = "idPeriodicidade";

	private static final String ID_LEITURISTA = "idLeiturista";

	private static final String ID_EMPRESA = "idEmpresa";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_PONTO_CONSUMO_SEQUENCIADOS = "listaPontoConsumoSequenciados";
	
	private static final String LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS = "listaPontoConsumoSequenciadosAgrupados";

	private static final String PONTOS_CONSUMO_REMANEJADOS = "pontosConsumoRemanejados";

	private static final String POSICAO = "posicao";

	private static final String DADOS_LATITUDE_LONGITUDE = "dadosLatitudeLongitude";

	private static final String EXIBIR_GOOGLE_MAPS = "exibirGoogleMaps";

	private static final String CHAVES_PRIMARIAS_ORDENACAO_PONTOS_CONSUMO = "chavesPrimariasOrdenacaoPontoConsumo";

	private static  Map<Long, Integer> mapaQuantidadePontos;

	private static final String POSICAO_ORDENACAO = "posicaoOrdenacao";

	private static final String EXIBIR_INCLUSAO = "exibirInclusao";

	private static final String ORDENAR_TODOS = "ordenarTodos";

	private static final String SELECIONE_UM_PONTO_CONSUMO = "SELECIONE_UM_PONTO_CONSUMO";
	
	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	private ControladorSetorComercial controladorSetorComercial;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorLeiturista controladorLeiturista;

	@Autowired
	private ControladorImovel controladorImovel;

	/**
	 * Método responsável por exibir a tela de pesquisa de Rota.
	 * 
	 * @param rota
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaRota")
	public String exibirPesquisaRota(RotaImpl rota, BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS);
		

		initMapaControleTamanhoPontosConsumo();
		carregarCampos(rota, request);
		model.addAttribute(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());

		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_ROTA);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO);
		request.getSession().removeAttribute(LISTA_CHAVES_PONTO_CONSUMO);
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		return "exibirPesquisaRota";
	}

	/**
	 * Método responsável pesquisar a rota.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 *
	 * @return retorna o valor da rota
	 */
	@RequestMapping("pesquisarRota")
	public String pesquisarRota(RotaImpl rota, BindingResult result, HttpServletRequest request, Model model) {

		model.addAttribute(ROTA, rota);
		request.getSession().removeAttribute(ROTA);
		Map<String, Object> filtro = new HashMap<String, Object>();

		try {
			prepararFiltro(filtro, rota);
			Collection<Rota> listaRota = controladorRota.consultarRota(filtro);
			model.addAttribute(LISTA_ROTA, listaRota);
			request.getSession().setAttribute(PONTOS_CONSUMO_REMANEJADOS, null);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "forward:exibirPesquisaRota";
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro - {@link Map}
	 * @param rota - {@link RotaImpl}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, RotaImpl rota) throws FormatoInvalidoException {

		if (rota != null) {
			if (rota.getTipoLeitura() != null && rota.getTipoLeitura().getChavePrimaria() > 0) {
				filtro.put(ID_TIPO_LEITURA, rota.getTipoLeitura().getChavePrimaria());
			}
			if (rota.getEmpresa() != null && rota.getEmpresa().getChavePrimaria() > 0) {
				filtro.put(ID_EMPRESA, rota.getEmpresa().getChavePrimaria());
			}
			if (rota.getLeiturista() != null && rota.getLeiturista().getChavePrimaria() > 0) {
				filtro.put(ID_LEITURISTA, rota.getLeiturista().getChavePrimaria());
			}
			if (rota.getPeriodicidade() != null && rota.getPeriodicidade().getChavePrimaria() > 0) {
				filtro.put(ID_PERIODICIDADE, rota.getPeriodicidade().getChavePrimaria());
			}
			if (rota.getSetorComercial() != null && rota.getSetorComercial().getChavePrimaria() > 0) {
				filtro.put(ID_SETOR_COMERCIAL, rota.getSetorComercial().getChavePrimaria());
			}
			if (!StringUtils.isEmpty(rota.getNumeroRota())) {
				String numero = rota.getNumeroRota().replaceAll(" ", "");
				Util.validarDominioCaracteres("Código da Rota", numero, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES_ALFA_SEM_ACENTO);
				filtro.put(NUMERO_ROTA, numero);
			}
			if (rota.getGrupoFaturamento() != null && rota.getGrupoFaturamento().getChavePrimaria() > 0) {
				filtro.put(ID_GRUPO_FATURAMENTO, rota.getGrupoFaturamento().getChavePrimaria());
			}
		}
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Rota.
	 * 
	 * @param rota
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirInclusaoRota")
	public String exibirInclusaoRota(RotaImpl rota, BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		if (!super.isPostBack(request) && request.getSession().getAttribute(PONTOS_CONSUMO_ASSOCIADOS) != null) {
			request.getSession().removeAttribute(PONTOS_CONSUMO_ASSOCIADOS);
		}

		carregarCampos(rota, request);
		carregarPontosConsumo(request, null);
		saveToken(request);

		return "exibirInclusaoRota";
	}

	/**
	 * Método responsável por incluir uma Rota.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param idPontosConsumoAssociados - {@link Long}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws IllegalAccessException - {@link IllegalAccessException}
	 * @throws InvocationTargetException  - {@link InvocationTargetException}
	 * @throws GGASException  - {@link GGASException}
	 */
	@RequestMapping("incluirRota")
	public String incluirRota(RotaImpl rota, BindingResult result, HttpServletRequest request,
			@RequestParam(value = ID_PONTOS_CONSUMO_ASSOCIADOS, required = false) Long[] idPontosConsumoAssociados, Model model)
			throws IllegalAccessException, InvocationTargetException, GGASException {

		String view = "forward:exibirInclusaoRota";
		model.addAttribute(ROTA, rota);
		try {
			 
			RegrasOrdenacaoPontoConsumoRota regrasOrdenacao = new RegrasOrdenacaoPontoConsumoRota();

			Map<Long, Object> pontosConsumoAssociados =	new HashMap<Long, Object>();
			
			pontosConsumoAssociados.put(null, request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO));

			// Ordena os pontos de consumo com a ordem
			// de leitura que o usuário escolheu
			regrasOrdenacao.insereNumeroSequenciaPontoConsumo(idPontosConsumoAssociados, pontosConsumoAssociados);

			// Atualiza a lista de pontos associados
			// já ordenados
			request.getSession().setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
			model.addAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
			validarToken(request);
			Long idRota = null;

			popularRota(null, rota, request);
			controladorRota.validarDadosEntidade(rota);
			validarRotaPontosConsumo(rota, (Collection<Object>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS));
			rota.setHabilitado(true);
			idRota = controladorRota.inserirRota(rota, pontosConsumoAssociados);
			
			model.addAttribute(CHAVE_PRIMARIA, idRota);
			atualizaPontosConsumoAssociadosInclusao(request, idRota);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Rota.ROTA_CAMPO_STRING);
			view = FORWARD_PESQUISAR_ROTA;
			request.getSession().removeAttribute(ROTA);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, null);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, null);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA, null);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO, null);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	private void validarRotaPontosConsumo(Rota rota, Collection<Object> pontos) throws NegocioException {
		if (rota.getNumeroMaximoPontosConsumo() != null && pontos != null && rota.getNumeroMaximoPontosConsumo() < pontos.size()) {
			throw new NegocioException(ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO);
		}
	}

	/**
	 * Método responsável por atualizar os pontos de consumo associados
	 * 
	 * @param request
	 * @param idRota
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	private void atualizaPontosConsumoAssociadosInclusao(HttpServletRequest request, Long idRota) 
			throws ConcorrenciaException, NegocioException {
		List<PontoConsumo> listaPontoConsumo = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			adicionaNumeroSequenciaLeitura(listaPontoConsumo);
			controladorRota.atualizarAssociacaoPontoConsumoRota(listaPontoConsumo, null, idRota);
		}
	}
	
	/**
	 * Método responsável por atualizar os pontos de consumo associados
	 * 
	 * @param request
	 * @param idRota
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	private void atualizaPontosConsumoAssociadosAlteracao(HttpServletRequest request, Long idRota)
			throws ConcorrenciaException, NegocioException {
		List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		if (request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS) != null) {
			if( request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS) instanceof HashSet ) {
				listaPontoConsumo.addAll((Set<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS));
			}else {
				listaPontoConsumo.addAll((List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS));
			}
			if (!listaPontoConsumo.isEmpty()) {
				adicionaNumeroSequenciaLeitura(listaPontoConsumo);
				controladorRota.atualizarAssociacaoPontoConsumoRota(listaPontoConsumo, null, idRota);
			}
		}
		
	}

	/**
	 * 
	 * @param listaPontoConsumo
	 */
	private void adicionaNumeroSequenciaLeitura(List<PontoConsumo> listaPontoConsumo) {
		int contador = 1;
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			pontoConsumo.setNumeroSequenciaLeitura(contador);
			contador++;
		}
	}

	/**
	 * Mover ponto consumo.
	 * 
	 * @param list
	 * @param idPontoConsumoAlterar
	 * @param posicao
	 * @return List<PontoConsumo>
	 */
	private List<PontoConsumo> moverPontoConsumo(List<PontoConsumo> list, Long idPontoConsumoAlterar, Integer posicao) {

		List<PontoConsumo> listaOrdenada = new LinkedList<PontoConsumo>();

		Iterator<PontoConsumo> listaPontoConsumoIterator = list.iterator();
		PontoConsumo pontoConsumoMudarPosicao = null;

		while (listaPontoConsumoIterator.hasNext()) {
			PontoConsumo pontoConsumo = listaPontoConsumoIterator.next();
			if (pontoConsumo.getChavePrimaria() == idPontoConsumoAlterar) {
				pontoConsumoMudarPosicao = pontoConsumo;
				list.remove(pontoConsumo);
				break;
			}
		}

		if (!CollectionUtils.isEmpty(list)) {
			if (list.size() < posicao) {
				listaOrdenada.addAll(list);
				listaOrdenada.add(pontoConsumoMudarPosicao);
			} else if (1 == posicao) {
				listaOrdenada.add(pontoConsumoMudarPosicao);
				listaOrdenada.addAll(list);
			} else {
				listaOrdenada.addAll(list.subList(0, posicao - 1));
				listaOrdenada.add(pontoConsumoMudarPosicao);
				listaOrdenada.addAll(list.subList(posicao - 1, list.size()));
			}
		}

		return listaOrdenada;
	}

	/**
	 * Carregar campos.
	 * 
	 * @param rota
	 * @param request
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void carregarCampos(RotaImpl rota, HttpServletRequest request) throws GGASException {

		request.setAttribute(LISTA_TIPO_LEITURA, controladorRota.listarTiposLeitura());
		request.setAttribute(LISTA_EMPRESA, controladorEmpresa.listarEmpresasMedicao());
		request.setAttribute(LISTA_LEITURISTA, controladorRota.listarLeituristas());
		request.setAttribute(LISTA_PERIODICIDADE, controladorRota.listarPeriodicidades());
		request.setAttribute(LISTA_SETOR_COMERCIAL, controladorSetorComercial.listarSetoresComerciais());
		request.setAttribute(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());

		Long idTipoLeitura = null;
		Long idPeriodicidade = null;

		if (rota.getTipoLeitura() != null) {
			idTipoLeitura = rota.getTipoLeitura().getChavePrimaria();
		}
		if (rota.getPeriodicidade() != null) {
			idPeriodicidade = rota.getPeriodicidade().getChavePrimaria();
		}

		if (idPeriodicidade != null) {
			request.setAttribute(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGrupoFaturamento(idPeriodicidade, idTipoLeitura));
		}

		String aceitaInclusaoPontoConsumo = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA);

		request.setAttribute("aceitaInclusaoPontoConsumo", aceitaInclusaoPontoConsumo);

		List<PontoConsumo> list = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
		if (CollectionUtils.isNotEmpty(list)) {
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, list);
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, prepararListaParaExibicao(list, false, true));
		}
	}

	/**
	 * Método responsável por Mover ponto consumo.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param idPontoConsumoAlterar - {@link Long}
	 * @param posicao - {@link Integer}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param posicaoTmp - posicaoTmp
	 * @return String - {@link String}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("moverPontoConsumo")
	public String moverPontoConsumo(RotaImpl rota, BindingResult result,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] idPontoConsumoAlterar,
			@RequestParam(value = POSICAO, required = false) String posicaoTmp, HttpServletRequest request, Model model) {

		String view = "";

		try {
			Integer posicao = this.converterPosicaoParaInteger(posicaoTmp);
			
			Boolean exibirInclusao = verificarFluxo(request);
			if (exibirInclusao != null && exibirInclusao) {
				view = exibirInclusaoRota(rota, result, request, model);
			} else {
				view = exibirAlteracaoRota(rota, result, request, model);
			}

			model.addAttribute(ROTA, rota);
			
			List<PontoConsumo> list = null;
			
			Object listaSessao = request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
						
			if (listaSessao != null) {
				if (listaSessao instanceof LinkedList) {
					list =  (LinkedList<PontoConsumo>)  listaSessao;
				}else if (listaSessao instanceof ArrayList) {
					list =  (ArrayList<PontoConsumo>)  listaSessao;
				}
			}
			if(list == null){
				list = new ArrayList<>();
			}
			validarParametros(idPontoConsumoAlterar, posicao, list.size());

			List<PontoConsumo> listaOrdenada = moverPontoConsumo(list, idPontoConsumoAlterar[0], posicao);

			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, listaOrdenada);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, prepararListaParaExibicao(listaOrdenada, false, true));
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, listaOrdenada);
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, prepararListaParaExibicao(listaOrdenada, false, true));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}catch (Exception e2) {
			super.mensagemErroParametrizado(model, request, new GGASException(e2));
		}

		return view;
	}
	
	private Integer converterPosicaoParaInteger(String posicao) {
		Integer valorPosicao = null;
		
		try {
			valorPosicao = Integer.valueOf(posicao);
		} catch(NumberFormatException e) {
			valorPosicao = 0;
		}
		
		return valorPosicao;
	}

	/**
	 * Método responsável por verificar o fluxo da tela
	 * 
	 * @param request
	 * @return
	 */
	private Boolean verificarFluxo(HttpServletRequest request) {
		Boolean exibirInclusao = null;
		if (request.getParameter(EXIBIR_INCLUSAO) != null) {
			exibirInclusao = Boolean.parseBoolean(request.getParameter(EXIBIR_INCLUSAO));
		}
		return exibirInclusao;
	}

	/**
	 * Validar parametros.
	 * 
	 * @param idPontoConsumoAlterar
	 * @param posicao
	 * @param tamanhoLista
	 * @throws GGASException
	 */
	private void validarParametros(Long[] idPontoConsumoAlterar, Integer posicao, int tamanhoLista) throws GGASException {

		if (idPontoConsumoAlterar != null && idPontoConsumoAlterar.length > 1) {
			throw new GGASException(Constantes.ESCOLHA_UM_PONTO_PARA_MOVER, true);
		}

		if (idPontoConsumoAlterar == null || idPontoConsumoAlterar.length == 0 ) {
			throw new GGASException(Constantes.ERRO_SELECIONE_UM_PONTO_PARA_SER_ORDENADO, true);
		}
		
		boolean achouItem = false;
		for (int i = 0; i < idPontoConsumoAlterar.length; i++) {
			if (idPontoConsumoAlterar[i] != 0) {
				achouItem = true;
			}
		}
		
		if (!achouItem) {
			throw new GGASException(Constantes.ERRO_SELECIONE_UM_PONTO_PARA_SER_ORDENADO, true);
		}
		
		if (posicao == null || posicao.equals(0)) {
			throw new GGASException(Constantes.DIGITE_A_POSICAO_PARA_MOVER, true);

		}
		if (posicao > tamanhoLista) {
			throw new GGASException(Constantes.ERRO_NAO_EXISTE_ESTA_POSICAO, true);
		}
	}


	/**
	 * Método responsável por remanejar pontos de consumo na alteração de uma Rota.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param idRotaDestino - {@link Long}
	 * @param idPontosConsumoASeremRemanejados
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("remanejarPontosConsumoRota")
	public String remanejarPontosConsumoRota(RotaImpl rota, BindingResult result, @RequestParam(ID_ROTA_REMANEJAMENTO) Long idRotaDestino,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] idPontosConsumoASeremRemanejados, HttpServletRequest request,
			Model model) throws GGASException {

		try {

			model.addAttribute(ROTA, rota);

			Map<Long, Object> pontosConsumoAssociados =
					(HashMap<Long, Object>) request.getSession().getAttribute(PONTOS_CONSUMO_ASSOCIADOS);
			rota.setDadosAuditoria(getDadosAuditoria(request));
			Collection<PontoConsumo> pontosSequenciados = 
					(Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);

			validarRemanejamento(idRotaDestino, idPontosConsumoASeremRemanejados);

			// remove os pontos remanejados dos pontos associados (apenas pontos salvos)
			atualizarPontosConsumo(idPontosConsumoASeremRemanejados, pontosConsumoAssociados);
			
			// remove os pontos remanejados dos pontos sequenciados (pontos salvos e não salvos)
			atualizarPontosConsumoSequenciados(idPontosConsumoASeremRemanejados, pontosSequenciados);

			Collection<PontoConsumo> pontosConsumoRemanejados =
					controladorPontoConsumo.remanejarPontoConsumo(idPontosConsumoASeremRemanejados, idRotaDestino);

			Collection<PontoConsumo> pontosConsumoReman =
					(Collection<PontoConsumo>) request.getSession().getAttribute(PONTOS_CONSUMO_REMANEJADOS);

			if (pontosConsumoReman != null && !pontosConsumoReman.isEmpty()) {
				pontosConsumoRemanejados.addAll(pontosConsumoReman);
			}

			// seta na session os pontos de consumo que foram remanejados
			request.getSession().setAttribute(PONTOS_CONSUMO_REMANEJADOS, pontosConsumoRemanejados);
			request.setAttribute(PONTOS_CONSUMO_REMANEJADOS, pontosConsumoRemanejados);

			// atualiza a lista de pontos de consumo da rota selecionada
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, pontosSequenciados);
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, pontosSequenciados);
			
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, 
					prepararListaParaExibicao(pontosSequenciados, false, true));
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS,  prepararListaParaExibicao(pontosSequenciados, false, true));

			mensagemSucesso(model, Constantes.REMANEJADO_COM_SUCESSO, request, Rota.ROTA_CAMPO_STRING);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (Exception e2) {
			super.mensagemErroParametrizado(model, request, new GGASException(e2));
		}

		return exibirAlteracaoRota(rota, result, request, model);

	}

	/**
	 * Validar remanejamento.
	 * 
	 * @param idRotaDestino - {@link Long}
	 * @param idPontosConsumoASeremRemanejados - {@link Long}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarRemanejamento(Long idRotaDestino, Long[] idPontosConsumoASeremRemanejados) throws GGASException {

		if (idPontosConsumoASeremRemanejados == null || idPontosConsumoASeremRemanejados.length == 0) {
			throw new GGASException(SELECIONE_UM_PONTO_CONSUMO);
		}
		boolean achouItem = false;
		for (int i = 0; i < idPontosConsumoASeremRemanejados.length; i++) {
			if (idPontosConsumoASeremRemanejados[i] != 0) {
				achouItem = true;
			}
		}
		if (!achouItem) {
			throw new GGASException(SELECIONE_UM_PONTO_CONSUMO, true);
		}
		if (idRotaDestino <= 0) {
			throw new GGASException("Selecione uma rota para remanejar");
		}
	}

	/**
	 * Converter ponto consumo v0 para ponto consumo.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param idsPontosConsumoAssociados - {@link Long}
	 * @param pontosConsumoAssoc - {@link Map}
	 * @return Map - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<Long, Object> converterPontoConsumoV0ParaPontoConsumo(HttpServletRequest request, Long[] idsPontosConsumoAssociados,
			Map<Long, Object> pontosConsumoAssoc) throws GGASException {

		Map<Long, Object> pontosConsumoAssociados = pontosConsumoAssoc;
		for (Object obj : pontosConsumoAssociados.values()) {
			if (obj instanceof PontoConsumoRotaVO) {
				Map<Long, Object> pontosConsumo = new HashMap<Long, Object>();
				for (Long id : idsPontosConsumoAssociados) {
					PontoConsumoRotaVO p = (PontoConsumoRotaVO) pontosConsumoAssociados.get(id);
					if (p != null) {
						PontoConsumo ponto = controladorPontoConsumo.obterPontoConsumoImovel(p.getIdPontoConsumo());
						pontosConsumo.put(ponto.getChavePrimaria(), ponto);
					}
				}
				pontosConsumoAssociados.clear();

				pontosConsumoAssociados = pontosConsumo;

				request.getSession().setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
				request.setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
			}
			break;
		}
		return pontosConsumoAssociados;
	}

	/**
	 * Converter map collection.
	 * 
	 * @param pontosConsumoAssociados - {@link Map}
	 * @param idsPontosConsumoAssociados - {@link Long}
	 * @return Collection - {@link Collection}
	 */
	public Collection<PontoConsumo> converterMapCollection(Map<Long, Object> pontosConsumoAssociados, Long[] idsPontosConsumoAssociados) {

		Collection<PontoConsumo> pontos = new LinkedList<PontoConsumo>();

		for (int i = 0; i < idsPontosConsumoAssociados.length; i++) {
			Long idponto = idsPontosConsumoAssociados[i];
			PontoConsumo ponto = (PontoConsumo) pontosConsumoAssociados.get(idponto);
			if (ponto != null) {
				pontos.add(ponto);
			}
		}
		return pontos;
	}

	/**
	 * Atualizar pontos consumo associados (map).
	 * 
	 * @param idPontosConsumoASeremRemanejados - {@link Long}
	 * @param pontosConsumoAssociados - {@link Map}
	 */
	private void atualizarPontosConsumo(Long[] idPontosConsumoASeremRemanejados, Map<Long, Object> pontosConsumoAssociados) {

		for (Long id : idPontosConsumoASeremRemanejados) {
			pontosConsumoAssociados.remove(id);
		}

	}
	
	/**
	 * Atualizar pontos consumo sequenciados (linkedlist).
	 * 
	 * @param idPontosConsumoASeremRemanejados - {@link Long}
	 * @param pontosConsumoSequenciados - {@link Collection}
	 */
	private void atualizarPontosConsumoSequenciados(Long[] idPontosConsumoASeremRemanejados, 
			Collection<PontoConsumo> pontosConsumoSequenciados) {
		for (Long id : idPontosConsumoASeremRemanejados) {			
			for(Iterator<PontoConsumo> iterator = pontosConsumoSequenciados.iterator(); iterator.hasNext();) {
				PontoConsumo pontoSequenciado = iterator.next();
				if (id.equals(pontoSequenciado.getChavePrimaria())) {
					iterator.remove();
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por carregar os pontos de consumo disponíveis, associados, remanejáveis e remanejados na tela.
	 *
	 * @param request - {@link HttpServletRequest}
	 * @param rota - {@link RotaImpl}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void carregarPontosConsumo(HttpServletRequest request, Rota rota) throws GGASException {

		List<PontoConsumo> list = (List<PontoConsumo>) request.getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
		Map<Long, Object> pontosConsumoAssociados = (HashMap<Long, Object>) request.getAttribute(PONTOS_CONSUMO_ASSOCIADOS);

		Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarPontosConsumoAssociados =
				controladorPontoConsumo.carregarAtributosDePontosConsumoAssociados(rota, list, pontosConsumoAssociados);

		list = (List<PontoConsumo>) carregarPontosConsumoAssociados.getFirst();
		pontosConsumoAssociados = carregarPontosConsumoAssociados.getSecond();

		request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, list);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, list);
		
		request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, prepararListaParaExibicao(list, false, true));
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, prepararListaParaExibicao(list, false, true));
		request.getSession().setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
	}

	/**
	 * Método responsável por converter uma lista de pontos de consumo em uma lista de VO's de ponto de consumo. A rota só deve ser
	 * informada no caso de exibição da alteração de rota.
	 * 
	 * @param listaPontoConsumo - {@link Collection}
	 * @param rota - {@link RotaImpl}
	 * @return List - {@link List}
	 */
	private List<PontoConsumoRotaVO> converterListaPontoConsumoRemanejados(Collection<PontoConsumo> listaPontoConsumo, Rota rota) {

		List<PontoConsumoRotaVO> listaVOPontoConsumoRota = new ArrayList<PontoConsumoRotaVO>();
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			PontoConsumoRotaVO voPontoConsumoRota = null;
			voPontoConsumoRota = new PontoConsumoRotaVO();
			voPontoConsumoRota.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
			if (rota != null) {
				voPontoConsumoRota.setIdRotaOrigem(rota.getChavePrimaria());
			}
			voPontoConsumoRota.setNumeroSequenciaLeitura(pontoConsumo.getNumeroSequenciaLeitura());
			voPontoConsumoRota.setDescricaoPontoConsumoFormatada(
					ServiceLocator.getInstancia().getControladorPontoConsumo().descricaoFormatada(pontoConsumo));
			voPontoConsumoRota.definirEndereco(pontoConsumo);
			if (pontoConsumo.getRota() != null) {
				voPontoConsumoRota.setIdRotaDestino(pontoConsumo.getRota().getChavePrimaria());
			}
			if (voPontoConsumoRota != null) {
				listaVOPontoConsumoRota.add(voPontoConsumoRota);
			}
		}

		return listaVOPontoConsumoRota;
	}

	/**
	 * Converter lista ponto consumo.
	 * 
	 * @param listaPontoConsumo - {@link Collection}
	 * @param pontosConsumoAssociados - {@link Map}
	 * @param rota - {@link RotaImpl}
	 * @return List - {@link List}
	 */
	private List<PontoConsumoRotaVO> converterListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo,
			Map<Long, Object> pontosConsumoAssociados, Rota rota) {

		List<PontoConsumoRotaVO> listaVOPontoConsumoRota = new ArrayList<PontoConsumoRotaVO>();

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				PontoConsumoRotaVO voPontoConsumoRota = null;

				if (pontosConsumoAssociados != null && !pontosConsumoAssociados.isEmpty()) {

					voPontoConsumoRota = (PontoConsumoRotaVO) pontosConsumoAssociados.get(pontoConsumo.getChavePrimaria());

				} else {

					voPontoConsumoRota = new PontoConsumoRotaVO();
					voPontoConsumoRota.setIdPontoConsumo(pontoConsumo.getChavePrimaria());

					if (pontoConsumo.getRota() != null) {
						voPontoConsumoRota.setIdRotaOrigem(pontoConsumo.getRota().getChavePrimaria());
					}

					voPontoConsumoRota.setNumeroSequenciaLeitura(pontoConsumo.getNumeroSequenciaLeitura());
					voPontoConsumoRota.setDescricaoPontoConsumoFormatada(
							ServiceLocator.getInstancia().getControladorPontoConsumo().descricaoFormatada(pontoConsumo));
					voPontoConsumoRota.definirEndereco(pontoConsumo);

					if (rota != null) {
						voPontoConsumoRota.setIdRotaDestino(rota.getChavePrimaria());
					}

				}
				if (voPontoConsumoRota != null) {

					listaVOPontoConsumoRota.add(voPontoConsumoRota);

				}
			}
		}

		return listaVOPontoConsumoRota;

	}

	/**
	 * Popular rota.
	 * 
	 * @param rotaAnterior - {@link RotaImpl}
	 * @param rota - {@link RotaImpl}
	 * @param request - {@link FormatoInvalidoException}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void popularRota(RotaImpl rotaAnteriorTmp, RotaImpl rota, HttpServletRequest request)
			throws FormatoInvalidoException, NegocioException {

		RotaImpl rotaAnterior = rotaAnteriorTmp;
		if (rotaAnterior != null) {

			rotaAnterior.setChavePrimaria(rota.getChavePrimaria());
			rotaAnterior.setVersao(rota.getVersao());
			rotaAnterior.setHabilitado(rota.isHabilitado());
			rotaAnterior.setNumeroRota(rota.getNumeroRota());
			rotaAnterior.setTipoLeitura(rota.getTipoLeitura());
			rotaAnterior.setEmpresa(rota.getEmpresa());
			rotaAnterior.setGrupoFaturamento(rota.getGrupoFaturamento());
			rotaAnterior.setSetorComercial(rota.getSetorComercial());
			rotaAnterior.setLeiturista(rota.getLeiturista());
			rotaAnterior.setPeriodicidade(rota.getPeriodicidade());
			rotaAnterior.setNumeroMaximoPontosConsumo(rota.getNumeroMaximoPontosConsumo());
		} else {
			rotaAnterior = rota;
		}

		if (!StringUtils.isEmpty(rota.getNumeroRota())) {
			String numero = rota.getNumeroRota().replaceAll(" ", "");
			Util.validarDominioCaracteres("Código da Rota", numero, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES_ALFA_SEM_ACENTO);
			rotaAnterior.setNumeroRota(numero);
		} else {
			rotaAnterior.setNumeroRota(null);
		}

		rotaAnterior.setDadosAuditoria(getDadosAuditoria(request));
	}

	/**
	 * Método responsável por excluir uma Rota.
	 * 
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("excluirRota")
	public String excluirRota(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) {

		try {
			getDadosAuditoria(request);
			controladorRota.validarRemoverRotas(chavesPrimarias);
			controladorRota.removerRotas(chavesPrimarias);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Rota.ROTAS);
			request.getSession().removeAttribute(ROTA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarRota(null, null, request, model);
	}

	/**
	 * Método responsável por detalhar a rota.
	 * 
	 * @param rotaTmp - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoRota")
	public String exibirDetalhamentoRota(RotaImpl rotaTmp, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {
		String view = "exibirDetalhamentoRota";
		RotaImpl rota = rotaTmp;
		salvarPesquisa(request, rotaTmp, ROTA);
		try {
			rota = (RotaImpl) controladorRota.obter(rotaTmp.getChavePrimaria(), FTIPO_LEITURA, FEMPRESA, FLEITURISTA, FPERIODICIDADE,
					FGRUPO_FATURAMENTO, FSETOR_COMERCIAL);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_PESQUISAR_ROTA;
		}
		model.addAttribute(ROTA, rota);
		carregarPontosConsumo(request, rota);

		Collection<Imovel> listaImovel = controladorImovel.listarImovelPorChaveRota(rotaTmp.getChavePrimaria());
		request.setAttribute("listaImovel", listaImovel);

		this.carregarPontoConsumoGoogleMaps(request);

		return view;
	}

	/**
	 * Carregar ponto consumo google maps.
	 * 
	 * @param request - {@link HttpServletRequest}
	 */
	private void carregarPontoConsumoGoogleMaps(HttpServletRequest request) {

		boolean exibirGoogleMaps = false;
		@SuppressWarnings("unchecked")
		Collection<PontoConsumo> listaPontoConsumo =
				(Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);

		StringBuilder stringBuilder = new StringBuilder();
		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				BigDecimal latitude = pontoConsumo.getLatitudeGrau();
				BigDecimal longitude = pontoConsumo.getLongitudeGrau();
				String descricao = pontoConsumo.getDescricao();

				if (latitude != null && longitude != null) {
					stringBuilder.append(latitude.toString());
					stringBuilder.append(";");
					stringBuilder.append(longitude.toString());
					stringBuilder.append(";");
					stringBuilder.append(descricao);
					stringBuilder.append(",");
				}
			}
			if (!stringBuilder.toString().isEmpty()) {
				exibirGoogleMaps = true;
			}
		}

		request.setAttribute(DADOS_LATITUDE_LONGITUDE, stringBuilder.toString());
		request.setAttribute(EXIBIR_GOOGLE_MAPS, exibirGoogleMaps);
	}

	/**
	 * Adicionar ponto consumo rota inclusao.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param listaIdsPontoConsumoRota - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("adicionarPontoConsumoRotaInclusao")
	public String adicionarPontoConsumoRotaInclusao(RotaImpl rota, BindingResult result,
			@RequestParam("listaIdsPontoConsumoRota") String listaIdsPontoConsumoRota, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ROTA, rota);

		adicionarPontoConsumoRota(listaIdsPontoConsumoRota, request);
		rota.setDadosAuditoria(getDadosAuditoria(request));

		return exibirInclusaoRota(rota, result, request, model);
	}
	
	/**
	 * Adicionar ponto consumo rota inclusao.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param listaIdsPontoConsumoRota - {@link String}
	 * @param listaIdsImoveisPais - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("adicionarPontoConsumoRotaInclusaoIncluiFilhos")
	public String adicionarPontoConsumoRotaInclusaoIncluiFilhos(RotaImpl rota, BindingResult result,
			@RequestParam("listaIdsPontoConsumoRota") String listaIdsPontoConsumoRota, 
			@RequestParam("listaIdsImoveisPais") String listaIdsImoveisPais,
			HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ROTA, rota);

		adicionarPontoConsumoRota(listaIdsPontoConsumoRota, request);
		adicionarPontoImoveisFilhos(listaIdsImoveisPais, request);
		rota.setDadosAuditoria(getDadosAuditoria(request));

		return exibirInclusaoRota(rota, result, request, model);
	}
	

	/**
	 * Adicionar ponto consumo rota alteracao.
	 * 
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param listaIdsPontoConsumoRota - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("adicionarPontoConsumoRotaAlteracao")
	public String adicionarPontoConsumoRotaAlteracao(RotaImpl rota, BindingResult result,
			@RequestParam("listaIdsPontoConsumoRota") String listaIdsPontoConsumoRota, HttpServletRequest request, Model model)
			throws NegocioException {

		adicionarPontoConsumoRota(listaIdsPontoConsumoRota, request);

		@SuppressWarnings("unchecked")
		List<PontoConsumo> listaPontoConsumoRemoverAssociacao =
				(List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		if (listaPontoConsumoRemoverAssociacao != null) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRemoverAssociacao) {
				pontoConsumo.setRota(null);
			}
		}

		return exibirAlteracaoRota(rota, result, request, model);
	}
	
	/**
	 * Adicionar ponto consumo rota alteracao.
	 * 
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param listaIdsPontoConsumoRota - {@link String}
	 * @param listaIdsImoveisPais - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("adicionarPontoConsumoRotaAlteracaoIncluiFilhos")
	public String adicionarPontoConsumoRotaAlteracaoIncluiFilhos(RotaImpl rota, BindingResult result,
			@RequestParam("listaIdsPontoConsumoRota") String listaIdsPontoConsumoRota,
			@RequestParam("listaIdsImoveisPais") String listaIdsImoveisPais,
			HttpServletRequest request, Model model)
			throws NegocioException {

		adicionarPontoConsumoRota(listaIdsPontoConsumoRota, request);

		adicionarPontoImoveisFilhos(listaIdsImoveisPais, request);
		@SuppressWarnings("unchecked")
		List<PontoConsumo> listaPontoConsumoRemoverAssociacao =
				(List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		if (listaPontoConsumoRemoverAssociacao != null) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRemoverAssociacao) {
				pontoConsumo.setRota(null);
			}
		}

		return exibirAlteracaoRota(rota, result, request, model);
	}

	/**
	 * Adicionar ponto consumo rota.
	 * 
	 * @param listaIds - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void adicionarPontoConsumoRota(String listaIds, HttpServletRequest request) throws NegocioException {

		RegrasOrdenacaoPontoConsumoRota regrasOrdenacao = new RegrasOrdenacaoPontoConsumoRota();

		String[] listaIdPontoConsumoRota = listaIds.split(",");
		List<Long> listaChavesPontoConsumo = (List<Long>) request.getSession().getAttribute(LISTA_CHAVES_PONTO_CONSUMO);
		if (listaChavesPontoConsumo == null) {
			listaChavesPontoConsumo = new ArrayList<>();
		}
		List<PontoConsumo> listaPontoConsumoRota = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA);

		if (listaPontoConsumoRota == null) {
			listaPontoConsumoRota = new ArrayList<PontoConsumo>();
		}
		Object[] listaIdPontoConsumoRotaFiltrado = new HashSet<String>(Arrays.asList(listaIdPontoConsumoRota)).toArray(); 
		for (Object chavePontoConsumo : listaIdPontoConsumoRotaFiltrado) {
			Long chavePrimaria = Long.parseLong(chavePontoConsumo.toString());
			PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumoImovel(chavePrimaria);
			if (!verificaPontoNaLista(listaPontoConsumoRota, pontoConsumo)) {
				listaPontoConsumoRota.add(pontoConsumo);
			}
			
			listaChavesPontoConsumo.add(chavePrimaria);
		}

		listaPontoConsumoRota = regrasOrdenacao.ordenaListaPontoConsumo(listaPontoConsumoRota);
		regrasOrdenacao.ordenaImoveisLote(listaPontoConsumoRota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA, listaPontoConsumoRota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO, 
				prepararListaParaExibicao(listaPontoConsumoRota, true, false));
		request.getSession().setAttribute(LISTA_CHAVES_PONTO_CONSUMO, listaChavesPontoConsumo);

	}
	
	/**
	 * Verifica se o ponto ja esta na lista
	 * @param listaPontoConsumoRota Lista atual de ponto de consumo
	 * @param pontoConsumo Ponto de consumo correte
	 * @return Se o ponto esta na lista ou nao
	 */
	private boolean verificaPontoNaLista(List<PontoConsumo> listaPontoConsumoRota, PontoConsumo pontoConsumo) {
		for (PontoConsumo pontoConsumoExistente : listaPontoConsumoRota) {
			if (pontoConsumoExistente.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adicionar ponto consumo rota.
	 * 
	 * @param listaIdsImoveisPais - {@link String}
	 * @param request - {@link HttpServletRequest}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void adicionarPontoImoveisFilhos(String listaIdsImoveisPais, HttpServletRequest request) throws NegocioException {

		RegrasOrdenacaoPontoConsumoRota regrasOrdenacao = new RegrasOrdenacaoPontoConsumoRota();

		String[] listaIdaImoveisPais = listaIdsImoveisPais.split(",");
		List<Long> listaChavesPontoConsumo = (List<Long>) request.getSession().getAttribute(LISTA_CHAVES_PONTO_CONSUMO);
		if (listaChavesPontoConsumo == null) {
			listaChavesPontoConsumo = new ArrayList<>();
		}
		List<PontoConsumo> listaPontoConsumoRota = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA);

		if (listaPontoConsumoRota == null) {
			listaPontoConsumoRota = new ArrayList<PontoConsumo>();
		}

		for (String idMovelPai : listaIdaImoveisPais) {
			Long idMovel = Long.parseLong(idMovelPai);
			
			List<PontoConsumo> pontosConsumo = controladorPontoConsumo.obterPontosConsumoImoveisFilhos(idMovel);
			listaPontoConsumoRota.addAll(pontosConsumo);
			for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
				listaChavesPontoConsumo.add(pontoConsumo.getChavePrimaria());
			}
			
		}

		listaPontoConsumoRota = regrasOrdenacao.ordenaListaPontoConsumo(listaPontoConsumoRota);
		regrasOrdenacao.ordenaImoveisLote(listaPontoConsumoRota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA, listaPontoConsumoRota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO, prepararListaParaExibicao(listaPontoConsumoRota, true, false));
		request.getSession().setAttribute(LISTA_CHAVES_PONTO_CONSUMO, listaChavesPontoConsumo);

	}

	/**
	 * Remover ponto consumo rota inclusao.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param chavePontoRemover - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("removerPontoConsumoRotaInclusao")
	public String removerPontoConsumoRotaInclusao(RotaImpl rota, BindingResult result,
			@RequestParam("chavePontoRemover") Long chavePontoRemover, HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(ROTA, rota);

		removerPontoConsumoRota(chavePontoRemover, request);

		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		return exibirInclusaoRota(rota, result, request, model);
	}

	/**
	 * 	 * Remover ponto consumo rota inclusao.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param chavePontoRemover - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param listaChavePontoRemover - {@link String}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("removerPontoConsumoRotaInclusaoAgrupado")
	public String removerPontoConsumoRotaInclusaoAgrupado(RotaImpl rota, BindingResult result,
			@RequestParam("listaChavePontoRemover") String[] listaChavePontoRemover, HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(ROTA, rota);

		for (String chavePontoRemover : listaChavePontoRemover) {
			removerPontoConsumoRota(Long.valueOf(chavePontoRemover), request);
		}

		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		return exibirInclusaoRota(rota, result, request, model);
	}
	
	
	/**
	 * Remover ponto consumo rota alteracao.
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param chavePontoRemover - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("removerPontoConsumoRotaAlteracao")
	public String removerPontoConsumoRotaAlteracao(RotaImpl rota, BindingResult result,
			@RequestParam("chavePontoRemover") Long chavePontoRemover, HttpServletRequest request, Model model) {

		model.addAttribute(ROTA, rota);

		removerPontoConsumoRota(chavePontoRemover, request);

		return exibirAlteracaoRota(rota, result, request, model);
	}

	/**
	 * Remover ponto consumo rota.
	 * 
	 * @param chavePontoRemover -{@link Long}
	 * @param request -{@link HttpServletRequest}
	 */
	@SuppressWarnings("unchecked")
	private void removerPontoConsumoRota(@RequestParam("chavePontoRemover") Long chavePontoRemover, HttpServletRequest request) {

		List<PontoConsumo> listaPontoConsumoRota = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA);
		List<PontoConsumo> listaPontoConsumoRemoverAssociacao =
				(List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);

		if (listaPontoConsumoRemoverAssociacao == null) {
			listaPontoConsumoRemoverAssociacao = new ArrayList<PontoConsumo>();
		}

		if (listaPontoConsumoRota != null) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
				if (pontoConsumo.getChavePrimaria() == chavePontoRemover) {
					listaPontoConsumoRota.remove(pontoConsumo);
					listaPontoConsumoRemoverAssociacao.add(pontoConsumo);
					break;
				}
			}
		}

		List<Long> chavesPontoConsumo = (List<Long>) request.getSession().getAttribute(LISTA_CHAVES_PONTO_CONSUMO);
		if (chavesPontoConsumo != null) {
			chavesPontoConsumo.remove(chavePontoRemover);
		}
		request.getSession().setAttribute(LISTA_CHAVES_PONTO_CONSUMO, chavesPontoConsumo);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA, listaPontoConsumoRota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO, prepararListaParaExibicao(listaPontoConsumoRota, true, false));
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO, listaPontoConsumoRemoverAssociacao);
	}

	/**
	 * Inits the mapa controle tamanho pontos consumo.
	 * 
	 * @return map - {@link Map}
	 */
	public static Map<Long, Integer> initMapaControleTamanhoPontosConsumo() {

		Map<Long, Integer> mapa = new HashMap<Long, Integer>();
		setMapaQuantidadePontos(mapa);

		return mapa;
	}

	/**
	 * Método responsável por exibir a tela de alteração da rota.
	 * 
	 * @param rotaTmp - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirAlteracaoRota")
	public String exibirAlteracaoRota(RotaImpl rotaTmp, BindingResult result, HttpServletRequest request, Model model) {

		String view = "exibirAlteracaoRota";

		RotaImpl rota = rotaTmp;
		salvarPesquisa(request, rotaTmp, ROTA);
		try {
			rota = (RotaImpl) controladorRota.obter(rotaTmp.getChavePrimaria(), FTIPO_LEITURA, FEMPRESA, FLEITURISTA, FPERIODICIDADE,
					FGRUPO_FATURAMENTO, FSETOR_COMERCIAL, "pontosConsumo");

			model.addAttribute(ROTA, rota);
			this.carregarCampos(rota, request);

			if (request.getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS) == null) {
				if (!super.isPostBack(request)) {
					if (request.getSession().getAttribute(PONTOS_CONSUMO_ASSOCIADOS) != null) {
						request.getSession().removeAttribute(PONTOS_CONSUMO_ASSOCIADOS);
					}
					carregarPontosConsumo(request, rota);
				} else {
					carregarPontosConsumo(request, rota);
				}
			}
			if (rota.getPeriodicidade() != null && rota.getPeriodicidade().getChavePrimaria() > 0) {
				request.setAttribute(LISTA_ROTA_REMANEJAMENTO, controladorRota.listarRotasRemanejamentoPeriodicidade(rotaTmp.getChavePrimaria(),
						rota.getPeriodicidade().getChavePrimaria()));
			} else {
				Collection<Rota> rotas = controladorRota.listarRotasRemanejamento(rota);
				request.setAttribute(LISTA_ROTA_REMANEJAMENTO, rotas);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_PESQUISAR_ROTA;
		}

		saveToken(request);

		return view;
	}

	/**
	 * Método responsável por alterar uma rota.
	 * 
	 * @param rotaTmp - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 * @throws IllegalAccessException - {@link IllegalAccessException}
	 * @throws InvocationTargetException - {@link InvocationTargetException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("alterarRota")
	public String alterarRota(RotaImpl rotaTmp, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		String view = "exibirAlteracaoRota";
		RegrasOrdenacaoPontoConsumoRota regrasOrdenacao = new RegrasOrdenacaoPontoConsumoRota();
		RotaImpl rota = rotaTmp;
		
		model.addAttribute(ROTA, rota);

		initMapaControleTamanhoPontosConsumo();
		try {
			List<PontoConsumo> list = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
			Collection<Object> pontosConsumoValidacaoQuantidade = 
					(Collection<Object>)request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
			List<PontoConsumo> listaPontoConsumoRota = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA);
			if(listaPontoConsumoRota != null) {
				pontosConsumoValidacaoQuantidade.addAll(listaPontoConsumoRota);
			}
			
			validarRotaPontosConsumo(rota, pontosConsumoValidacaoQuantidade);
			
			Collection<PontoConsumo> listAux = (Collection<PontoConsumo>) request.getSession().getAttribute(PONTOS_CONSUMO_REMANEJADOS);
			RotaImpl rotaAnterior = (RotaImpl) controladorRota.obter(rotaTmp.getChavePrimaria(), FTIPO_LEITURA, FEMPRESA, FLEITURISTA,
					FPERIODICIDADE, FGRUPO_FATURAMENTO, FSETOR_COMERCIAL, "pontosConsumo");
			
			validarToken(request);
			this.carregarCampos(rota, request);
			popularListaPontoConsumo(request, list, rota);

			popularRota(rotaAnterior, rota, request);

			controladorRota.validarDadosEntidade(rotaAnterior);

			Map<Long, Object> pontosConsumoAssociados = new HashMap<Long, Object>();

			List<PontoConsumoRotaVO> listaPontoConsumo = converterListaPontoConsumo(list, pontosConsumoAssociados, rota);
			regrasOrdenacao.insereNumeroSequenciaPontoConsumoVO(listaPontoConsumo);
			for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumo) {
				pontosConsumoAssociados.put(pontoConsumoRotaVO.getIdPontoConsumo(), pontoConsumoRotaVO);
			}
			
			Map<Long, PontoConsumo> pontosPorChavePrimaria = list.stream()
					.collect(Collectors.toMap(PontoConsumo::getChavePrimaria, Function.identity()));

			if (listAux != null) {
				List<PontoConsumoRotaVO> listaPontoConsumoRemanejados = converterListaPontoConsumoRemanejados(listAux, rota);
				for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumoRemanejados) {
					pontosConsumoAssociados.put(pontoConsumoRotaVO.getIdPontoConsumo(), pontoConsumoRotaVO);
				}
				
				pontosPorChavePrimaria.putAll(listAux.stream()
						.collect(Collectors.toMap(PontoConsumo::getChavePrimaria, Function.identity())));
			}

			request.setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);
			request.getSession().setAttribute(PONTOS_CONSUMO_ASSOCIADOS, pontosConsumoAssociados);

			Collection<PontoConsumo> pontosRemanejados = new ArrayList<PontoConsumo>();
			if (pontosConsumoAssociados == null || pontosConsumoAssociados.isEmpty()) {
				rotaAnterior.getPontosConsumo().clear();
			} else {
				rotaAnterior.getPontosConsumo().clear();
				Iterator pontosConsumoVO = pontosConsumoAssociados.entrySet().iterator();
				// Itera a lista de pontos de consumo
				// associados e remanejados
				while (pontosConsumoVO.hasNext()) {
					Map.Entry entryPontoConsumo = (Map.Entry) pontosConsumoVO.next();
					PontoConsumoRotaVO pontoConsumoRotaVO = (PontoConsumoRotaVO) entryPontoConsumo.getValue();
					PontoConsumo pontoConsumo = pontosPorChavePrimaria.get(pontoConsumoRotaVO.getIdPontoConsumo());

					// Se o destino do ponto de
					// consumo for igual a rota
					// alterada, adicionar na lista de
					// pontos de consumo desta
					if (((Long) rotaTmp.getChavePrimaria()).equals(pontoConsumoRotaVO.getIdRotaDestino())) {
						pontoConsumo.setRota(rotaAnterior);
						pontoConsumo.setNumeroSequenciaLeitura(pontoConsumoRotaVO.getNumeroSequenciaLeitura());
						rotaAnterior.getPontosConsumo().add(pontoConsumo);
					} else {
						// Caso contrário,
						// adicionar a lista de
						// pontos de consumo
						// remanejados
						pontoConsumo.setNumeroSequenciaLeitura(pontoConsumoRotaVO.getNumeroSequenciaLeitura());
						if (pontoConsumoRotaVO.getIdRotaDestino() == null || pontoConsumoRotaVO.getIdRotaDestino() <= 0) {
							pontoConsumo.setRota(null);
							pontoConsumo.setNumeroSequenciaLeitura(null);
						} else {
							pontoConsumo.setRota((Rota) controladorRota.obter(pontoConsumoRotaVO.getIdRotaDestino()));
						}
						pontosRemanejados.add(pontoConsumo);
					}
				}
			}

			validarRotaPontosConsumo(rotaAnterior, pontosConsumoAssociados.values());
			controladorRota.atualizar(rotaAnterior, pontosRemanejados);
			model.addAttribute(CHAVE_PRIMARIA, rotaTmp.getChavePrimaria());

			List<PontoConsumo> listaPontoConsumoRemoverAssociacao =
					(List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_REMOVER_ASSOCIACAO);
			listaPontoConsumoRota = regrasOrdenacao.ordenaListaPontoConsumo(listaPontoConsumoRota);
			regrasOrdenacao.ordenaImoveisLote(listaPontoConsumoRota);
			list.addAll(listaPontoConsumoRota);
			adicionaNumeroSequenciaLeitura(list);
			
			this.controladorPontoConsumo.atualizarSequencialLeituraDePontosConsumoPorRota(list, rotaTmp);
			this.controladorPontoConsumo.atualizarSequencialLeituraDePontosConsumoPorRota(listaPontoConsumoRemoverAssociacao, null);

			request.getSession().setAttribute(PONTOS_CONSUMO_REMANEJADOS, null);

			// Atualiza o histórico de escalonamento de Leituristas
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("chaveRota", rotaTmp.getChavePrimaria());
			Collection<RotaHistorico> listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro, Boolean.FALSE);

			Long idLeiturista = null;
			if (request.getParameter(ID_LEITURISTA) != null) {
				idLeiturista = Long.parseLong(request.getParameter(ID_LEITURISTA));
			}
			if (!CollectionUtils.isEmpty(listaRotaHistorico) && idLeiturista != null && idLeiturista > 0) {
				listaRotaHistorico.iterator().next().setLeiturista((Leiturista) controladorLeiturista.obter(idLeiturista));
				controladorRota.atualizarRotaHistorico(listaRotaHistorico.iterator().next());
			}
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Rota.ROTA_CAMPO_STRING);
			view = FORWARD_PESQUISAR_ROTA;
			request.getSession().removeAttribute(ROTA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;

		// Ordena os pontos de consumo com a ordem
		// de leitura que o usuário remanejou
		// TODO verificar se esse método é necessário
		// ordernarPontosConsumo(idPontosConsumoRemanejados, pontosConsumoAssociados);

		// Atualiza a lista de pontos associados
		// já ordenados
	}

	/**
	 * Popular lista ponto consumo.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param list - {@link List}
	 * @param rota - {@link RotaImpl}
	 */
	private void popularListaPontoConsumo(HttpServletRequest request, Collection<PontoConsumo> list, Rota rota) {

		if (list == null || list.isEmpty()) {
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, rota.getPontosConsumo());
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, rota.getPontosConsumo());
		} else {
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, list);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, list);
		}
	}

	/**
	 * Ordena pontos de consumo
	 * 
	 * @param rota - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @param isOrdenarTodos - {@link Boolean}
	 * @param posicaoOrdenacao - {@link Integer}
	 * @param chavesPrimariasPontosConsumo - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("ordenarPontosConsumo")
	public String ordenarPontosConsumo(RotaImpl rota, BindingResult result, @RequestParam(ORDENAR_TODOS) Boolean isOrdenarTodos,
			@RequestParam(POSICAO_ORDENACAO) Integer posicaoOrdenacao,
			@RequestParam(CHAVES_PRIMARIAS_ORDENACAO_PONTOS_CONSUMO) Long[] chavesPrimariasPontosConsumo,
			HttpServletRequest request, Model model) {
 
		
		
		String view = null;

		try {
			model.addAttribute(ROTA, rota);

			RegrasOrdenacaoPontoConsumoRota regrasOrdenacao = new RegrasOrdenacaoPontoConsumoRota();

			Boolean exibirInclusao = verificarFluxo(request);
			if (exibirInclusao != null && exibirInclusao) {
				view = exibirInclusaoRota(rota, result, request, model);
			} else {
				view = exibirAlteracaoRota(rota, result, request, model);
			}

			List<PontoConsumo> listaPontoConsumoRota = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_ROTA);

			// Lista de pontos de consumo que serão adicionados aos pontos de consumo já
			// ordenados.
			List<PontoConsumo> listaPontoConsumoParaOrdenacao = null;

			if (isOrdenarTodos != null && isOrdenarTodos) {
				listaPontoConsumoParaOrdenacao = listaPontoConsumoRota;
			} else {
				listaPontoConsumoParaOrdenacao = (List<PontoConsumo>) consultarPontosConsumo(chavesPrimariasPontosConsumo);
			}

			List<PontoConsumoRotaVO> listaPontoConsumoVO =
					controladorPontoConsumo.converterListaPontoConsumo(listaPontoConsumoParaOrdenacao, null, null);
			controladorPontoConsumo.ordernarListaPontoConsumoRotaVOPorDescricao(listaPontoConsumoVO);

			List<PontoConsumo> listaPontoConsumoOrdenada =
					regrasOrdenacao.converteListaPontoConsumoVOOrdenada(listaPontoConsumoParaOrdenacao, listaPontoConsumoVO);

			regrasOrdenacao.ordenaImoveisLote(listaPontoConsumoOrdenada);

			controladorPontoConsumo.removerPontosConsumoAssociados(listaPontoConsumoRota, listaPontoConsumoVO);

			List<PontoConsumo> listaPontoConsumoJaAssociados =
					(List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS);
			if (listaPontoConsumoJaAssociados == null) {
				listaPontoConsumoJaAssociados = new ArrayList<PontoConsumo>();
			}
			if (posicaoOrdenacao != null && !posicaoOrdenacao.equals(0)) {

				verificaPosicao(posicaoOrdenacao, listaPontoConsumoOrdenada, listaPontoConsumoJaAssociados);

			} else {
				listaPontoConsumoJaAssociados.addAll(listaPontoConsumoOrdenada);
			}
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, listaPontoConsumoJaAssociados);
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS, listaPontoConsumoJaAssociados);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, 
					prepararListaParaExibicao(listaPontoConsumoJaAssociados, false, true));
			request.setAttribute(LISTA_PONTO_CONSUMO_SEQUENCIADOS_AGRUPADOS, 
					prepararListaParaExibicao(listaPontoConsumoJaAssociados, false, true));
				
			

			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA, listaPontoConsumoRota);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_ROTA_EXIBICAO, 
					prepararListaParaExibicao(listaPontoConsumoRota, true, false));

			carregarCampos(rota, request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (Exception e2) {
			super.mensagemErroParametrizado(model, request, new GGASException(e2));
		}

		return view;
	}

	/**
	 * @param posicaoOrdenacao - {@link Integer}
	 * @param listaPontoConsumoOrdenada - {@link List}
	 * @param listaPontoConsumoJaAssociados - {@link List}
	 */
	private void verificaPosicao(Integer posicaoOrdenacao, List<PontoConsumo> listaPontoConsumoOrdenada,
			List<PontoConsumo> listaPontoConsumoJaAssociados) {
		try{		
			listaPontoConsumoJaAssociados.addAll(posicaoOrdenacao - 1, listaPontoConsumoOrdenada);
		}catch(Exception e){
			listaPontoConsumoJaAssociados.addAll(listaPontoConsumoOrdenada);
			LOG.error(e);
		}
	}

	/**
	 * 
	 * @param chavesPrimariasPontosConsumo - {@link Long}
	 * @return ListaPontoConsumo - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@SuppressWarnings("unused")
	private Collection<PontoConsumo> consultarPontosConsumo(Long[] chavesPrimariasPontosConsumo) throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumo = null;
		if (chavesPrimariasPontosConsumo != null && chavesPrimariasPontosConsumo.length > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, chavesPrimariasPontosConsumo);
			listaPontoConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);
		}
		return listaPontoConsumo;
	}

	/**
	 * Paginacao
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @return String - {@link String}
	 */
	@RequestMapping("paginacaoRota")
	public String paginacaoRota(HttpServletRequest request) {

		return "paginacaoRota";
	}

	/**
	 * Paginacao sequenciados
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @return String - {@link String}
	 */
	@RequestMapping("paginacaoRotaSequenciados")
	public String paginacaoRotaSequenciados(HttpServletRequest request) {

		return "paginacaoRotaSequenciados";
	}

	/**
	 * Método responsável pelo retorno dos dados a tela de pesquisa.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param rotaTmp - {@link RotaImpl}
	 * @param result - {@link BindingResult}
	 * @return String - {@link String}
	 */
	@RequestMapping("voltarRota")
	public String voltar(RotaImpl rotaTmp, BindingResult result, HttpServletRequest request, Model model) {

		RotaImpl rota = null;
		if(rotaTmp != null) {
			rota = (RotaImpl) retornaPesquisa(request, ROTA);
		}
		return pesquisarRota(rota, result, request, model);
	}

	public static Map<Long, Integer> getMapaQuantidadePontos() {
		return mapaQuantidadePontos;
	}

	public static void setMapaQuantidadePontos(Map<Long, Integer> mapaQuantidadePontos) {
		RotaAction.mapaQuantidadePontos = mapaQuantidadePontos;
	}
	
	/**
	 * Metodo para Agrupar os pontos de consumo pelo imovel pai
	 * @param listaPontoConsumoRota Lista com os pontos para exibicao
	 * @param exibeDescricao Se exibe ou nao a descricao ou nao do ponto de consumo
	 * @param exibeSequenciado Parametro para informar se o agrupamento esta na lista sequenciada
	 * @return Lista grupada
	 */
	public Collection<PontoConsumo> prepararListaParaExibicao(Collection<PontoConsumo> listaPontoConsumoRota, boolean exibeDescricao,
			boolean exibeSequenciado) {
		List<PontoConsumo> listaRetorno = new ArrayList<PontoConsumo>();
		Collection<PontoConsumo> listaPais = new ArrayList<PontoConsumo>();
		Collection<PontoConsumo> listaFilhos = new ArrayList<PontoConsumo>();
		if (listaPontoConsumoRota!=null && !listaPontoConsumoRota.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
				if (pontoConsumo.getImovel().getCondominio()) {
					listaPais.add(pontoConsumo);
				}else {
					listaFilhos.add(pontoConsumo);
				}
			}
			if (!exibeSequenciado) {
				alimentaListaPai(listaPais, listaFilhos, listaRetorno, exibeDescricao);
				alimentaListaFilhos(listaFilhos, listaRetorno, exibeDescricao);
			}else {
				alimentaListaSequenciada(listaPontoConsumoRota, listaRetorno, exibeDescricao);
				preencheNumeroSequencia(listaRetorno);
			}
			
		}
		return listaRetorno;
	}
	
	/**
	 * Preenche o sequencial para ser exibido na tela
	 * @param listaRetorno Lista que vai para tela
	 */
	private void preencheNumeroSequencia(Collection<PontoConsumo> listaRetorno) {
		if (listaRetorno!=null && !listaRetorno.isEmpty()) {
			int contador = 1;
			for (PontoConsumo pontoConsumo : listaRetorno) {
				pontoConsumo.setNumeroSequenciaLeitura(contador);
				if (pontoConsumo.getListaPontosConsumoImoveisFilho()!=null &&
						!pontoConsumo.getListaPontosConsumoImoveisFilho().isEmpty()) {
					contador = preencheSequenciaLista(pontoConsumo, contador);
				}else {
					contador = contador + 1;
				}
			}
		}
		
	}

	/**
	 * Metodo criado por causa do sonar
	 * @param pontoConsumo Ponto de consumo a ser sequenciado
	 * @param contador Numero da ordem do item
	 * @return Contador do proximo item
	 */
	private int preencheSequenciaLista(PontoConsumo pontoConsumo, int contador) {
		for (PontoConsumo pontoConsumoFilho : pontoConsumo.getListaPontosConsumoImoveisFilho()) {
			pontoConsumoFilho.setNumeroSequenciaLeitura(contador);
			contador = contador + 1;
		}
		return contador;
	}

	/**
	 * Alimenta Lista de pontos de consumo sequenciado
	 * @param listaRetorno Lista de retorno final
	 * @param exibeDescricao Se a descricao sera exibida ou nao.
	 */
	private void alimentaListaSequenciada(Collection<PontoConsumo> listaCompleta, List<PontoConsumo> listaRetorno, 
			boolean exibeDescricao) {
		PontoConsumo pontoConsumoAnterior = null;
		for (PontoConsumo pontoConsumo : listaCompleta) {
			if (pontoConsumoAnterior == null) {
				if (pontoConsumo.getImovel().getCondominio() || pontoConsumo.getImovel().getIdImovelCondominio() != null) {
					PontoConsumoImpl pontoConsumoImovelPai = 
							criaImovelPai(pontoConsumo, exibeDescricao, pontoConsumo.getImovel().getCondominio());
					pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
					listaRetorno.add(pontoConsumoImovelPai);
				}else {
					listaRetorno.add(pontoConsumo);
					}
			}else {
				alimentaPontoConsumoAtual(pontoConsumo, pontoConsumoAnterior, listaRetorno, exibeDescricao);
			}
			pontoConsumoAnterior = pontoConsumo;
		}
		
	}

	/**
	 * Metodo criado para alimetar o ponto de consumo Atual
	 * @param pontoConsumo Ponto de consumo atual
	 * @param pontoConsumoAnterior Ponto de consumo anterior
	 * @param listaRetorno Lista final que vai para a tela
	 * @param exibeDescricao Se é para exibir ou nao a descricao do agrupamento
	 */
	private void alimentaPontoConsumoAtual(PontoConsumo pontoConsumo, PontoConsumo pontoConsumoAnterior, 
			List<PontoConsumo> listaRetorno, boolean exibeDescricao ) {
		if (pontoConsumo.getImovel().getCondominio()) {
			if (pontoConsumo.getImovel().getChavePrimaria() == pontoConsumoAnterior.getImovel().getChavePrimaria()) {
				PontoConsumo pontoConsumoUltimoPai = listaRetorno.get(listaRetorno.size() - 1);
				pontoConsumoUltimoPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
			}else {
				PontoConsumoImpl pontoConsumoImovelPai = criaImovelPai(pontoConsumo, exibeDescricao,
						pontoConsumo.getImovel().getCondominio());
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
				listaRetorno.add(pontoConsumoImovelPai);
			}
		}else {
			if (pontoConsumoAnterior.getImovel().getImovelCondominio() != null) {
				alimentaPontoConsumoAtualImovelAnteriorCondominio(pontoConsumo, pontoConsumoAnterior, listaRetorno, exibeDescricao);
			}else if (pontoConsumo.getImovel().getImovelCondominio() != null){
				if (pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria() == 
						pontoConsumoAnterior.getImovel().getChavePrimaria()) {
					PontoConsumo pontoConsumoUltimoPai = ((ArrayList<PontoConsumo>) listaRetorno).get(listaRetorno.size() - 1);
					pontoConsumoUltimoPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
				}else {
					PontoConsumoImpl pontoConsumoImovelPai = criaImovelPai(pontoConsumo, exibeDescricao, 
							pontoConsumo.getImovel().getCondominio());
					pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
					listaRetorno.add(pontoConsumoImovelPai);
				}	
			}else{
				listaRetorno.add(pontoConsumo);
			}
			
		}
		
	}

	/**
	 * Metodo criado para tratar o agrupamento quando o imovel anterior eh um imovel de condominio
	 * @param pontoConsumo Ponto de consumo atual
	 * @param pontoConsumoAnterior Ponto de consumo anterior
	 * @param listaRetorno Lista final que vai para a tela
	 * @param exibeDescricao Se é para exibir ou nao a descricao do agrupamento
	 */
	private void alimentaPontoConsumoAtualImovelAnteriorCondominio(PontoConsumo pontoConsumo, PontoConsumo pontoConsumoAnterior, 
			List<PontoConsumo> listaRetorno, boolean exibeDescricao ) {
		if (pontoConsumo.getImovel().getImovelCondominio() != null){
			if (pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria() == 
					pontoConsumoAnterior.getImovel().getImovelCondominio().getChavePrimaria()) {
				PontoConsumo pontoConsumoUltimoPai = ((ArrayList<PontoConsumo>) listaRetorno).get(listaRetorno.size() - 1);
				pontoConsumoUltimoPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
			}else{
				PontoConsumoImpl pontoConsumoImovelPai = criaImovelPai(pontoConsumo, exibeDescricao,
						pontoConsumo.getImovel().getCondominio());
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
				listaRetorno.add(pontoConsumoImovelPai);
			}
		}else{
			listaRetorno.add(pontoConsumo);
		}
		
	}

	/**
	 * Metodo para criar o agrupamento do imovel Pai
	 * @param pontoConsumo Ponto de consumo que sera usado de base para a criacao do imovel pai
	 * @param exibeDescricao Se vai exibir a descricao do Ponto de consumo ou nao
	 * @param imovelCondominio Se o imovel passado é o proprio imovel Condominio
	 * @return Novo agrupamento de imovel pai
	 */
	private PontoConsumoImpl criaImovelPai(PontoConsumo pontoConsumo, boolean exibeDescricao, Boolean imovelCondominio) {
		PontoConsumoImpl pontoConsumoImovelPai = new PontoConsumoImpl();
		if (imovelCondominio) {
			pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel());
		}else {
			pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel().getImovelCondominio());
		}
		
		pontoConsumoImovelPai.setListaPontosConsumoImoveisFilho(new ArrayList<>());
		if (exibeDescricao) {
			pontoConsumoImovelPai.setDescricao(pontoConsumo.getImovel().getNome());
		}
		return pontoConsumoImovelPai;
	}

	/**
	 * Metodo criado para alimentar a lista de imoveis filhos
	 * @param listaPais Lista com os imoveis pai
	 * @param listaFilhos Lsita com os imoveis filhos
	 * @param listaRetorno Lista retorno com os pais e filhos rganizados
	 * @param exibeDescricao Se vai exibir a descricao do imovel ou nao.
	 */
	private void alimentaListaFilhos(Collection<PontoConsumo> listaFilhos,
			Collection<PontoConsumo> listaRetorno, boolean exibeDescricao) {
		if (!listaFilhos.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaFilhos) {
				if (!verificaPontoConsumoNaLista(pontoConsumo, listaRetorno)) {
					organizaPontoConsumo(pontoConsumo, listaRetorno, exibeDescricao, listaFilhos);
				}
			}
		}
	}

	/**
	 * Metodo criado para organizao o ponto de consumo entre pai e filho
	 * @param pontoConsumo Ponto de consumo a ser organizado
	 * @param listaRetorno Lista de retorno final com os pontos de consumo
	 * @param exibeDescricao Se exibe a descricao ou nao 
	 * @param listaFilhos Lista com os imoveis filhos.
	 */
	private void organizaPontoConsumo(PontoConsumo pontoConsumo, Collection<PontoConsumo> listaRetorno, boolean exibeDescricao, 
			Collection<PontoConsumo> listaFilhos) {
		if (pontoConsumo.getImovel().getIdImovelCondominio()!= null) {
			PontoConsumo pontoConsumoImovelPai = recuperaAgrupamentoPaiPontoConsumo(
					pontoConsumo.getImovel().getIdImovelCondominio(), listaRetorno);
			if (pontoConsumoImovelPai == null) {
				pontoConsumoImovelPai = new PontoConsumoImpl();
				pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel().getImovelCondominio());
				pontoConsumoImovelPai.setListaPontosConsumoImoveisFilho(new ArrayList<>());
				if (exibeDescricao) {
					pontoConsumoImovelPai.setDescricao(pontoConsumo.getImovel().getNome());
				}
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().addAll(preencheImoveisFilhos(
					pontoConsumo.getImovel().getIdImovelCondominio(), listaFilhos));
				listaRetorno.add(pontoConsumoImovelPai);
			}else {
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
			}
		}else {
			listaRetorno.add(pontoConsumo);
		}
		
	}

	/**
	 * Metodo criado para alimentar a lista de imoveis pai
	 * @param listaPais Lista com os imoveis pai
	 * @param listaFilhos Lsita com os imoveis filhos
	 * @param listaRetorno Lista retorno com os pais e filhos rganizados
	 * @param exibeDescricao Se vai exibir a descricao do imovel ou nao.
	 */
	private void alimentaListaPai(Collection<PontoConsumo> listaPais, Collection<PontoConsumo> listaFilhos,
			Collection<PontoConsumo> listaRetorno, boolean exibeDescricao) {
		if (!listaPais.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPais) {
				PontoConsumo pontoConsumoImovelPai = recuperaAgrupamentoPaiPontoConsumo(pontoConsumo.getImovel().getChavePrimaria(),
						listaRetorno);
				if (pontoConsumoImovelPai == null) {
					pontoConsumoImovelPai = criaImovelPai(pontoConsumo, exibeDescricao, listaFilhos);
					listaRetorno.add(pontoConsumoImovelPai);
				}else {
					pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
				}
				
			}
		}
		
	}

	/**
	 * Metodo criado para adicionar os dados no imovel pai
	 * @param pontoConsumo Ponto de consumo corrente
	 * @param exibeDescricao Se no campo descricao, vamos motrar a descricao do imovel
	 * @param listaFilhos lista com os imoveis de condominio definidos ate o momnoto
	 * @return Ponto de consumo pronto para a lista
	 */
	private PontoConsumo criaImovelPai(PontoConsumo pontoConsumo, boolean exibeDescricao, Collection<PontoConsumo> listaFilhos) {
		PontoConsumo pontoConsumoImovelPai = new PontoConsumoImpl();
		pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel());
		pontoConsumoImovelPai.setListaPontosConsumoImoveisFilho(new ArrayList<>());
		pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
		if (exibeDescricao) {
			pontoConsumoImovelPai.setDescricao(pontoConsumo.getImovel().getNome());
		}
		pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().addAll(preencheImoveisFilhos(
			pontoConsumo.getImovel().getChavePrimaria(), listaFilhos));
		return pontoConsumoImovelPai;
	}

	/**
	 * Verifica se o Imovel Pai ja esta na lista de retorno para nao duplicar
	 * @param pontoConsumo Ponto de consumo pai
	 * @param listaRetorno Lista Final
	 * @return Objeto existente
	 */
	private PontoConsumo recuperaAgrupamentoPaiPontoConsumo(long chavePrimariaImovel,
			Collection<PontoConsumo> listaRetorno) {
		for (PontoConsumo pontoConsumoExistente : listaRetorno) {
			if (pontoConsumoExistente.getImovel().getChavePrimaria() == chavePrimariaImovel) {
				return pontoConsumoExistente;
			}
		}
		return null;
	}

	
	/**
	 * Verifica se o item esta na lista de retorno
	 * @param pontoConsumo Ponto de Consumo de referencia para ser verificado
	 * @param listaRetorno Lista de retorno para a tela
	 * @return Se o ponto ja esta na lista de retono ou nao
	 */
	private boolean verificaPontoConsumoNaLista(PontoConsumo pontoConsumo, Collection<PontoConsumo> listaRetorno) {
		for (PontoConsumo pontoConsumoExistente : listaRetorno) {
			if (pontoConsumoExistente.getChavePrimaria() != 0) {
				if (pontoConsumo.getChavePrimaria() == pontoConsumoExistente.getChavePrimaria()) {
					return true;
				}
			}else {
				if (verificaPontoConsumoListaImovelPai(pontoConsumoExistente, pontoConsumo)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Metodo para verificar se o ponto de consumo ja esta na lista de imovel filho
	 * @param pontoConsumoExistente Lista com os pontos de conusmos ja existentes
	 * @param pontoConsumo Ponto de consumo a ser verificado
	 * @return Se existe o ponto de consumo ou nao
	 */
	private boolean verificaPontoConsumoListaImovelPai(PontoConsumo pontoConsumoExistente, PontoConsumo pontoConsumo) {
		for (PontoConsumo pontoConsumoFilho : pontoConsumoExistente.getListaPontosConsumoImoveisFilho()) {
			if (pontoConsumoFilho.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Metodo para percorrer a lista e procurar um imovel filho para agrupar
	 * @param chavePrimaria Identificaxdor do imovel pai
	 * @param listaPontoConsumoRota Lista completa com os resultados
	 * @return Lista agrupada com os imoveis filhos
	 */
	private List<PontoConsumo> preencheImoveisFilhos(long chavePrimaria,
			Collection<PontoConsumo> listaPontoConsumoRota) {
		List<PontoConsumo> retorno = new ArrayList<PontoConsumo>();
		for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
			if (pontoConsumo.getImovel().getIdImovelCondominio()!=null
					&& pontoConsumo.getImovel().getIdImovelCondominio() == chavePrimaria) {
				retorno.add(pontoConsumo);
			}
		}
		return retorno;
	}
}
