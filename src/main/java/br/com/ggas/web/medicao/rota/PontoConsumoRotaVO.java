/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.rota;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Pair;

/**
 * Classe responsável pela trasnferência de dados que relaciona a rota ao ponto de consumo
 *
 */
public class PontoConsumoRotaVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 7810982157616301508L;

	private Long idPontoConsumo;

	private Long idRotaOrigem;

	private Long idRotaDestino;

	private String descricaoPontoConsumoFormatada;

	private Integer numeroSequenciaLeitura;
	
	private String cpSetorComercial;
	
	private String bairro;

	private String cep;

	private Long numeroImovel;

	private String nomeImovel;

	private Long descricaoComplementoNumero;

	private String descricaoComplementoNome;

	private Long nomeImovelNumero;

	/**
	 * @return the idPontoConsumo
	 */
	public Long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the idRotaOrigem
	 */
	public Long getIdRotaOrigem() {

		return idRotaOrigem;
	}

	/**
	 * @param idRotaOrigem
	 *            the idRotaOrigem to set
	 */
	public void setIdRotaOrigem(Long idRotaOrigem) {

		this.idRotaOrigem = idRotaOrigem;
	}

	/**
	 * @return the idRotaDestino
	 */
	public Long getIdRotaDestino() {

		return idRotaDestino;
	}

	/**
	 * @param idRotaDestino
	 *            the idRotaDestino to set
	 */
	public void setIdRotaDestino(Long idRotaDestino) {

		this.idRotaDestino = idRotaDestino;
	}

	/**
	 * @return the descricaoPontoConsumoFormatada
	 */
	public String getDescricaoPontoConsumoFormatada() {

		return descricaoPontoConsumoFormatada;
	}

	/**
	 * @param descricaoPontoConsumoFormatada
	 *            the
	 *            descricaoPontoConsumoFormatada
	 *            to set
	 */
	public void setDescricaoPontoConsumoFormatada(String descricaoPontoConsumoFormatada) {

		this.descricaoPontoConsumoFormatada = descricaoPontoConsumoFormatada;
	}

	/**
	 * @return the numeroSequenciaLeitura
	 */
	public Integer getNumeroSequenciaLeitura() {

		return numeroSequenciaLeitura;
	}

	/**
	 * @param numeroSequenciaLeitura
	 *            the numeroSequenciaLeitura to
	 *            set
	 */
	public void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura) {

		this.numeroSequenciaLeitura = numeroSequenciaLeitura;
	}

	
	/**
	 * @return the cpSetorComercial
	 */
	public String getCpSetorComercial() {
	
		return cpSetorComercial;
	}

	
	/**
	 * @param cpSetorComercial the cpSetorComercial to set
	 */
	public void setCpSetorComercial(String cpSetorComercial) {
	
		this.cpSetorComercial = cpSetorComercial;
	}

	
	/**
	 * @return the bairro
	 */
	public String getBairro() {
	
		return bairro;
	}

	
	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
	
		this.bairro = bairro;
	}

	
	/**
	 * @return the cep
	 */
	public String getCep() {
	
		return cep;
	}

	
	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
	
		this.cep = cep;
	}

	
	/**
	 * @return the numeroImovel
	 */
	public Long getNumeroImovel() {
	
		return numeroImovel;
	}

	
	/**
	 * @param numeroImovel the numeroImovel to set
	 */
	public void setNumeroImovel(Long numeroImovel) {
	
		this.numeroImovel = numeroImovel;
	}

	
	/**
	 * @return the nomeImovel
	 */
	public String getNomeImovel() {
	
		return nomeImovel;
	}

	
	/**
	 * @param nomeImovel the nomeImovel to set
	 */
	public void setNomeImovel(String nomeImovel) {
	
		this.nomeImovel = nomeImovel;
	}

	
	/**
	 * @return the descricaoComplementoNumero
	 */
	public Long getDescricaoComplementoNumero() {
	
		return descricaoComplementoNumero;
	}

	
	/**
	 * @param descricaoComplementoNumero the descricaoComplementoNumero to set
	 */
	public void setDescricaoComplementoNumero(Long descricaoComplementoNumero) {
	
		this.descricaoComplementoNumero = descricaoComplementoNumero;
	}

	
	/**
	 * @return the descricaoComplementoNome
	 */
	public String getDescricaoComplementoNome() {
	
		return descricaoComplementoNome;
	}

	
	/**
	 * @param descricaoComplementoNome the descricaoComplementoNome to set
	 */
	public void setDescricaoComplementoNome(String descricaoComplementoNome) {
	
		this.descricaoComplementoNome = descricaoComplementoNome;
	}

	
	/**
	 * @return the nomeImovelNumero
	 */
	public Long getNomeImovelNumero() {
	
		return nomeImovelNumero;
	}

	
	/**
	 * @param nomeImovelNumero the nomeImovelNumero to set
	 */
	public void setNomeImovelNumero(Long nomeImovelNumero) {
	
		this.nomeImovelNumero = nomeImovelNumero;
	}
	
	/**
	 * Definir endereco.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @throws Exception the exception
	 */
	public void definirEndereco(PontoConsumo pontoConsumo) {
		
		if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getSetorComercial() != null
				&& pontoConsumo.getRota().getSetorComercial().getCodigo() != null) {
			String strCpSetorComercial = String.valueOf(pontoConsumo.getRota().getSetorComercial().getCodigo());
			if (StringUtils.isNotEmpty(strCpSetorComercial)) {
				this.cpSetorComercial = strCpSetorComercial;

			}
		}

		if (StringUtils.isNotEmpty(pontoConsumo.getCep().getBairro())) {
			this.bairro = pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep().getBairro();
		}

		if (StringUtils.isNotEmpty(pontoConsumo.getCep().getCep())) {
			this.cep = pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep().getCep();
		}

		String nrImovel = pontoConsumo.getImovel().getNumeroImovel();
		if (StringUtils.isNotEmpty(nrImovel)) {
			if (!NumberUtils.isNumber(nrImovel)) {
				nrImovel = null;
			}
			this.numeroImovel = NumberUtils.toLong(nrImovel);
		}
		
		String nmImovel = pontoConsumo.getImovel().getNome();
		Pair<String, Long> pairNomeImovel = separarPartesNomeNumero(nmImovel);
		
		this.nomeImovel = pairNomeImovel.getFirst();
		this.nomeImovelNumero = pairNomeImovel.getSecond();
		
		String descricaoComplemento = pontoConsumo.getDescricaoComplemento();
		Pair<String, Long> pairDescricaoComplemento = separarPartesNomeNumero(descricaoComplemento);

		this.descricaoComplementoNome = pairDescricaoComplemento.getFirst();
		this.descricaoComplementoNumero = pairDescricaoComplemento.getSecond();	
		
	}
	
	private Pair<String, Long> separarPartesNomeNumero(String str) {
		
		String dsComplementoNome = StringUtils.EMPTY;
		Long dsComplementoNumero = NumeroUtil.LONG_ZERO;	
		
		if (StringUtils.isNotEmpty(str)) {
			int lastIndexOf = str.lastIndexOf(Constantes.STRING_ESPACO);
			if (lastIndexOf > 0) {				
				String numero = str.substring(lastIndexOf).trim();
				if (NumberUtils.isNumber(numero)) {
					dsComplementoNome = str.substring(0, lastIndexOf);
					dsComplementoNumero = NumberUtils.toLong(numero);
				} else {
					dsComplementoNome = str;
				}
			}
		}
		
		return new Pair<String, Long>(
						dsComplementoNome, dsComplementoNumero);
		
	}
	
}
