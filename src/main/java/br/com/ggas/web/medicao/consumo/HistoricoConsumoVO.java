package br.com.ggas.web.medicao.consumo;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.medicao.consumo.TipoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.util.Util;

/**
 * Classe VO relacionada as telas da funcionalidade Contrato
 *
 */
public class HistoricoConsumoVO {
	private Integer anoMesFaturamento;
	private Integer numeroCiclo;
	private String urlFoto;
	private Date dataLeituraAnterior;
	private Date dataLeituraInformada;
	private BigDecimal numeroLeituraAnterior;
	private BigDecimal numeroLeituraInformada;
	private BigDecimal consumo;
	private Integer diasConsumo;
	private BigDecimal fatorPCS;
	private BigDecimal fatorPTZ;
	private BigDecimal consumoMedido;
	private BigDecimal fatorCorrecao;
	private String descricaoTipoConsumo;
	private Long idRota;
	private Long idLeituraMovimento;



	
	public String getUrlFoto() {
		return urlFoto;
	}
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	
	public String getAnoMesFaturamentoFormatado() {

		return Util.formatarAnoMes(anoMesFaturamento);
	}
	
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}
	
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	public BigDecimal getFatorCorrecao() {
		return fatorCorrecao;
	}
	public void setFatorCorrecao(BigDecimal fatorCorrecao) {
		this.fatorCorrecao = fatorCorrecao;
	}
	public BigDecimal getConsumoMedido() {
		return consumoMedido;
	}
	public void setConsumoMedido(BigDecimal consumoMedido) {
		this.consumoMedido = consumoMedido;
	}
	public BigDecimal getFatorPTZ() {
		return fatorPTZ;
	}
	public void setFatorPTZ(BigDecimal fatorPTZ) {
		this.fatorPTZ = fatorPTZ;
	}
	public BigDecimal getFatorPCS() {
		return fatorPCS;
	}
	public void setFatorPCS(BigDecimal fatorPCS) {
		this.fatorPCS = fatorPCS;
	}
	public Integer getDiasConsumo() {
		return diasConsumo;
	}
	public void setDiasConsumo(Integer diasConsumo) {
		this.diasConsumo = diasConsumo;
	}
	public BigDecimal getConsumo() {
		return consumo;
	}
	public void setConsumo(BigDecimal consumo) {
		this.consumo = consumo;
	}
	public Date getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}
	public void setDataLeituraAnterior(Date dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}
	public Date getDataLeituraInformada() {
		return dataLeituraInformada;
	}
	public void setDataLeituraInformada(Date dataLeituraInformada) {
		this.dataLeituraInformada = dataLeituraInformada;
	}
	public BigDecimal getNumeroLeituraAnterior() {
		return numeroLeituraAnterior;
	}
	public void setNumeroLeituraAnterior(BigDecimal numeroLeituraAnterior) {
		this.numeroLeituraAnterior = numeroLeituraAnterior;
	}
	public BigDecimal getNumeroLeituraInformada() {
		return numeroLeituraInformada;
	}
	public void setNumeroLeituraInformada(BigDecimal numeroLeituraInformada) {
		this.numeroLeituraInformada = numeroLeituraInformada;
	}
	public String getDescricaoTipoConsumo() {
		return descricaoTipoConsumo;
	}
	public void setDescricaoTipoConsumo(String descricaoTipoConsumo) {
		this.descricaoTipoConsumo = descricaoTipoConsumo;
	}
	public Integer getNumeroCiclo() {
		return numeroCiclo;
	}
	public void setNumeroCiclo(Integer numeroCiclo) {
		this.numeroCiclo = numeroCiclo;
	}
	public Long getIdRota() {
		return idRota;
	}
	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}
	public Long getIdLeituraMovimento() {
		return idLeituraMovimento;
	}
	public void setIdLeituraMovimento(Long idLeituraMovimento) {
		this.idLeituraMovimento = idLeituraMovimento;
	}
	
	
	
	
}