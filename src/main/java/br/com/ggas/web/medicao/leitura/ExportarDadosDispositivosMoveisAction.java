/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.relatorio.exportacaodados.ControladorConsulta;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.exportacaodados.ExportacaoDadosVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Exportação Dados para Dispositivos Móveis.
 */
@Controller
public class ExportarDadosDispositivosMoveisAction extends GenericAction {
	
	private static final String EXPORTACAO_DADOS_VO = "exportacaoDadosVO";

	private static final String EXIBIR_RELATORIO = "exibirRelatorio";
	
	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	private static final String ARQUIVO_EXPORTACAO_SQL = "arquivoExportacaoSQL";

	private static final String MODULOS = "modulos";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_CONSULTA_PARAMETRO = "listaConsultaParametro";

	private static final String REQUEST_LISTA_COLUNAS = "colunas";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String PARAMETRO_ALTERADO = "parametroAlterado";

	private static final String REQUEST_CONSULTA_SQL_DECLARACAO = "sql";

	private static final String REQUEST_CONSULTA_SQL_CAMPOS = "sqlCampos";

	private static final String NOME_TABELA_LEITURA_MOVIMENTO = "LEITURA_MOVIMENTO";

	private static final String RELATORIO_EXPORTACAO = "relatorioExportacao";

	private static final int INDICE = 6;

	private static final int INDICE_INICIAL = 4;

	@Autowired
	private ControladorConsulta controladorConsulta;
	
	@Autowired
	private ControladorAuditoria controladorAuditoria;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorModulo controladorModulo;
	
	@Autowired
	private ControladorRota controladorRota;	
	
	/**
	 * Método responsável por exibir a tela de exportação de dados de leituras para dispositivos móveis.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirExportacaoDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirExportacaoDispositivosMoveis")
	public String exibirExportacaoDispositivosMoveis(ExportacaoDadosVO exportacaoDadosVO, HttpServletRequest request, Model model)
			throws GGASException {

		try {

			Consulta consulta = controladorConsulta.buscarConsultaExportacaoDispositivosMoveis();
			exportacaoDadosVO.setChavePrimaria(consulta.getChavePrimaria());
			exportacaoDadosVO.setNome(consulta.getNome());
			exportacaoDadosVO.setDescricao(consulta.getDescricao());

			Collection<ConsultaParametro> parametros = new ArrayList<ConsultaParametro>();
			if (consulta.getListaParametro() != null) {
				for (ConsultaParametro p : consulta.getListaParametro()) {
					parametros.add(p);
				}
			}

			request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, parametros);
			model.addAttribute("listaRota",
					controladorRota.listarRotasPorTipoLeitura("COLETOR DE DADOS"));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);

		saveToken(request);

		return "exibirExportacaoDispositivosMoveis";
	}
	
	/**
	 * Método responsável por exibir a tela de alteração da consulta de exportação de dados de leituras para dispositivos móveis.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoExportacaoDispositivosMoveis")
	public String exibirAlteracaoExportacaoDispositivosMoveis(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoExportacaoDispositivosMoveis";

		Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();
		Consulta consulta = null;
		String sql = "";
		StringBuilder camposFormatado = new StringBuilder();

		try {
			if (chavePrimaria == null || chavePrimaria == 0) {
				consulta = controladorConsulta.buscarConsultaExportacaoDispositivosMoveis();
				chavePrimaria = consulta.getChavePrimaria();
				sql = consulta.getSql().toUpperCase();
				String[] camposColunas = sql.substring(INDICE, sql.indexOf("FROM")).replace(" ", "").split(",");
				camposFormatado.append("");
				for (String string : camposColunas) {
					campoFormatado(camposFormatado, string);
				}
				sql = sql.substring(sql.indexOf("1=1 ") + INDICE_INICIAL, sql.indexOf("ORDER"));
			} else {
				chavePrimaria = exportacaoDadosVO.getChavePrimaria();
				sql = exportacaoDadosVO.getSql();
				camposFormatado.append(exportacaoDadosVO.getColunasSelecionadas());
			}

			Collection<Coluna> colunas = controladorConsulta.obterColunasLeituraMovimentoPorNomeTabela(NOME_TABELA_LEITURA_MOVIMENTO);
			model.addAttribute(REQUEST_LISTA_COLUNAS, colunas);
			model.addAttribute(REQUEST_CONSULTA_SQL_DECLARACAO, sql);
			model.addAttribute(REQUEST_CONSULTA_SQL_CAMPOS, camposFormatado);
			carregarListasComboNoRequest(model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirExportacaoDispositivosMoveis(exportacaoDadosVO, request, model);
		}

		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);
		model.addAttribute(CHAVE_PRIMARIA, chavePrimaria);
		model.addAttribute(LISTA_CONSULTA_PARAMETRO, request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO));
		saveToken(request);

		return view;
	}

	private void campoFormatado(StringBuilder camposFormatado, String string) {
		if (camposFormatado.length() == 0) {
			camposFormatado.append(string);
		} else {
			camposFormatado.append(camposFormatado);
			camposFormatado.append(", ");
			camposFormatado.append(string);
		}
	}
	
	/**
	 * Carregar listas combo no request.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarListasComboNoRequest(Model model) throws GGASException {

		model.addAttribute(MODULOS, controladorModulo.consultarTodosModulos());
		model.addAttribute("tipos", controladorEntidadeConteudo.listarTipoParametro());
		model.addAttribute("mascaras_a", controladorConsulta.listarMascaraParametro("221"));
		model.addAttribute("mascaras_n", controladorConsulta.listarMascaraParametro("220"));
		model.addAttribute("mascaras_d", controladorConsulta.listarMascaraParametro("222"));
		model.addAttribute("entidadesReferenciadas", controladorConsulta.listarEntidadeReferenciada());
		model.addAttribute("entidadesClasses", controladorEntidadeConteudo.listarEntidadeClasse());
		model.addAttribute("classeUnidades", controladorEntidadeConteudo.listarClasseUnidade());
	}

	/**
	 * Validar consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param listaConsultaParametro - {@link Collection}
	 * @param indice - {@link Integer}
	 * @throws GGASException - {@link GGASException}
	 */
	private void validarConsultaParametro(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
			int indice) throws GGASException {

		controladorConsulta.validarConsultaParametro(consultaParametro, listaConsultaParametro, indice);
	}
	
	/**
	 * Método responsável por adicionar parametros na consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirAlteracaoExportacaoDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarParametroConsultaDispositivosMoveis")
	public String adicionarParametroConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		Integer indexLista = exportacaoDadosVO.getIndexLista();

		List<ConsultaParametro> listaConsultaParametro =
				(List<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

		try {
			validarToken(request);

			if (indexLista == null || indexLista < 0) {
				ConsultaParametro consultaParametro = (ConsultaParametro) controladorConsulta.criarConsultaParametro();
				consultaParametro.setDadosAuditoria(getDadosAuditoria(request));
				this.popularConsultaParametro(consultaParametro, exportacaoDadosVO, listaConsultaParametro);
				if (indexLista != null) {
					validarConsultaParametro(consultaParametro, listaConsultaParametro, indexLista);
				}
				if (listaConsultaParametro == null) {
					listaConsultaParametro = new ArrayList<ConsultaParametro>();
				}
				listaConsultaParametro.add(consultaParametro);

			} else {
				request.setAttribute(PARAMETRO_ALTERADO, true);
				this.alterarParametroConsulta(indexLista, exportacaoDadosVO, listaConsultaParametro);
			}

			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, listaConsultaParametro);
		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);

		return exibirAlteracaoExportacaoDispositivosMoveis(exportacaoDadosVO, bindingResult, request, model);
	}

	/**
	 * Alterar parametro consulta.
	 * 
	 * @param indexLista - {@link Integer}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param listaConsultaParametro - {@link List}
	 * @throws GGASException - {@link GGASException}
	 */
	private void alterarParametroConsulta(Integer indexLista, ExportacaoDadosVO exportacaoDadosVO,
			List<ConsultaParametro> listaConsultaParametro) throws GGASException {

		if (listaConsultaParametro != null) {
			ConsultaParametro consultaParametro = (ConsultaParametro) controladorConsulta.criarConsultaParametro();
			this.popularConsultaParametro(consultaParametro, exportacaoDadosVO, listaConsultaParametro);
			validarConsultaParametro(consultaParametro, listaConsultaParametro, indexLista);
			ConsultaParametro consultaParametroExistente = null;
			for (int i = 0; i < listaConsultaParametro.size(); i++) {
				consultaParametroExistente = listaConsultaParametro.get(i);
				if (i == indexLista && consultaParametroExistente != null) {
					this.popularConsultaParametro(consultaParametroExistente, exportacaoDadosVO, listaConsultaParametro);
				}
			}
		}

	}
	
	/**
	 * Método responsável por remover parametros na consulta.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirAlteracaoExportacaoDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerParametroConsultaDispositivosMoveis")
	public String removerParametroConsulta(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		Integer indexLista = exportacaoDadosVO.getIndexLista();

		Collection<ConsultaParametro> listaConsultaParametro =
				(Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

		try {

			validarToken(request);

			if (listaConsultaParametro != null && indexLista != null) {
				((List) listaConsultaParametro).remove(indexLista.intValue());
			} else {
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO_EXCLUIR_SELECIONADO);
			}

			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_CONSULTA_PARAMETRO, listaConsultaParametro);

		return exibirAlteracaoExportacaoDispositivosMoveis(exportacaoDadosVO, bindingResult, request, model);
	}

	/**
	 * Popular consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param listaConsultaParametro - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularConsultaParametro(ConsultaParametro consultaParametro, ExportacaoDadosVO exportacaoDadosVO,
			Collection<ConsultaParametro> listaConsultaParametro) throws GGASException {

		String paramCodigo = exportacaoDadosVO.getParamCodigo();
		String paramNome = exportacaoDadosVO.getParamNome();
		Long paramIdTipo = exportacaoDadosVO.getParamIdTipo();
		Integer paramQtdMaxCaracteres = exportacaoDadosVO.getParamQtdMaxCaracteres();
		String paramMascara = exportacaoDadosVO.getParamMascara();

		if (StringUtils.isEmpty(paramCodigo)) {
			consultaParametro.setCodigo(null);
		} else {
			consultaParametro.setCodigo(paramCodigo);
		}

		if (StringUtils.isEmpty(paramNome)) {
			consultaParametro.setNomeCampo(null);
		} else {
			consultaParametro.setNomeCampo(paramNome);
		}

		if (paramIdTipo != null && paramIdTipo > 0) {
			consultaParametro.setEntidadeConteudo(controladorEntidadeConteudo.obterEntidadeConteudo(paramIdTipo));
		} else {
			consultaParametro.setEntidadeConteudo(null);
		}

		if (paramQtdMaxCaracteres != null && paramQtdMaxCaracteres > 0) {
			consultaParametro.setNumeroMaximoCaracteres(paramQtdMaxCaracteres);
		} else {
			consultaParametro.setNumeroMaximoCaracteres(null);
		}

		if (StringUtils.isEmpty(paramMascara) || "-1".equals(paramMascara)) {
			consultaParametro.setMascara(null);
		} else {
			consultaParametro.setMascara(paramMascara);
		}

		popularConsultaParametroDois(consultaParametro, listaConsultaParametro, exportacaoDadosVO);
	}

	/**
	 * Popular consulta parametro.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param listaConsultaParametro - {@link Collection}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void popularConsultaParametroDois(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
			ExportacaoDadosVO exportacaoDadosVO) throws NegocioException {

		Integer paramOrdemExibicao = exportacaoDadosVO.getParamOrdemExibicao();
		Boolean paramIsEntidadeChave = exportacaoDadosVO.getParamIsChaveEntidade();
		Boolean paramIsPermiteMultiplaSelecao = exportacaoDadosVO.getParamIsPermiteMultiplaSelecao();
		Long paramEntidadeReferenciada = exportacaoDadosVO.getParamIdEntidadeReferenciada();
		Long paramClasseChaveEntidade = exportacaoDadosVO.getParamIdClasseAssociada();

		if (paramOrdemExibicao != null && paramOrdemExibicao > 0) {
			consultaParametro.setOrdem(paramOrdemExibicao);
		} else {
			if (listaConsultaParametro == null) {
				consultaParametro.setOrdem(1);
			} else {
				consultaParametro.setOrdem(null);
			}
		}

		if (paramIsEntidadeChave != null) {
			consultaParametro.setFk(paramIsEntidadeChave);
		} else {
			consultaParametro.setFk(false);
		}

		if (paramIsPermiteMultiplaSelecao != null) {
			consultaParametro.setMultivalorado(paramIsPermiteMultiplaSelecao);
		} else {
			consultaParametro.setMultivalorado(false);
		}

		if (paramEntidadeReferenciada != null && paramEntidadeReferenciada > 0) {
			consultaParametro.setTabela(controladorAuditoria.obterTabelaSelecionada(paramEntidadeReferenciada));
		} else {
			consultaParametro.setTabela(null);
		}

		if (paramClasseChaveEntidade != null && paramClasseChaveEntidade > 0) {
			consultaParametro.setChaveEntidadeComplementar(paramClasseChaveEntidade);
		} else {
			consultaParametro.setChaveEntidadeComplementar(null);
		}
	}

	/**
	 * Salva em arquivo os dados das leituras
	 * retornados pela consulta de exportação para
	 * dispositivos móveis,
	 * conforme os valores de filtro informados
	 * (caso haja).
	 *
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param isReexportar - {@link Boolean}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirExportacaoDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exportarDispositivosMoveis")
	public String exportarDispositivosMoveis(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			@RequestParam(value = "isReexportar", required = false) Boolean isReexportar, HttpServletRequest request, Model model)
			throws GGASException {

		String separador = exportacaoDadosVO.getSeparador();

		try {

			Consulta consulta = controladorConsulta.buscarConsultaExportacaoDispositivosMoveis();
			Map<String, Object[]> mapDadosParametro = obterMapExportacaoPopulado(consulta, request);
			Long chaveSituacaoLeitura = null;
			if (isReexportar) {
				chaveSituacaoLeitura = SituacaoLeituraMovimento.EM_LEITURA;
				controladorConsulta.limitarConsultaExportacao(consulta, SituacaoLeituraMovimento.EM_LEITURA);
			} else {
				chaveSituacaoLeitura = SituacaoLeituraMovimento.GERADO;
				controladorConsulta.limitarConsultaExportacao(consulta, SituacaoLeituraMovimento.GERADO);
			}

			byte[] arquivoExportacao = controladorConsulta.obterDadosExecucaoConsulta(consulta, mapDadosParametro, separador);

			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("chaveSituacaoLeitura", chaveSituacaoLeitura);
			byte[] arquivoExportacaoSql = controladorConsulta.exportarDadosMoveis(parametros);

			if (arquivoExportacao != null) {
				request.getSession().setAttribute(RELATORIO_EXPORTACAO, arquivoExportacao);
				request.getSession().setAttribute(ARQUIVO_EXPORTACAO_SQL, arquivoExportacaoSql);
				model.addAttribute(EXIBIR_RELATORIO, Boolean.TRUE);

				if (!isReexportar) {
					controladorConsulta.confirmarExportacao(consulta, mapDadosParametro);
				}
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirExportacaoDispositivosMoveis(exportacaoDadosVO, request, model);
	}

	/**
	 * Exibir exportacao dispositivos moveis relatorio.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null 
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirExportacaoDispositivosMoveisRelatorio")
	public String exibirExportacaoDispositivosMoveisRelatorio(HttpServletRequest request, HttpServletResponse response, Model model)
			throws GGASException {

		byte[] arquivoExportacao = (byte[]) request.getSession().getAttribute(RELATORIO_EXPORTACAO);

		request.getSession().removeAttribute(RELATORIO_EXPORTACAO);

		if (arquivoExportacao != null) {
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			this.imprimirArquivo(response, arquivoExportacao, "arquivo_exportacao.txt");

		}

		return null;
	}

	/** 
	 * Exibir exportacao dispositivos moveis dados leitura.
	 *
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param model - {@link Model}
	 * @return null
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirExportacaoDispositivosMoveisDadosLeitura")
	public String exibirExportacaoDispositivosMoveisDadosLeitura(HttpServletRequest request, HttpServletResponse response, Model model)
			throws GGASException {

		byte[] arquivoExportacao = (byte[]) request.getSession().getAttribute(ARQUIVO_EXPORTACAO_SQL);

		request.getSession().removeAttribute(ARQUIVO_EXPORTACAO_SQL);

		if (arquivoExportacao != null) {
			model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

			this.imprimirArquivo(response, arquivoExportacao, "dadosLeitura.sql");

		}

		return null;
	}

	/**
	 * Imprimir arquivo.
	 * 
	 * @param response - {@link HttpServletResponse}
	 * @param arquivoExportacao - {@link Byte}
	 * @param nomeArquivo - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void imprimirArquivo(HttpServletResponse response, byte[] arquivoExportacao, String nomeArquivo) throws NegocioException {

		response.setContentType("application/text");
		response.setContentLength(arquivoExportacao.length);
		response.addHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);

		try {
			response.getOutputStream().write(arquivoExportacao);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e.getMessage());
		}

	}

	/**
	 * Salva as alterações realizadas na consulta
	 * de exportação para dispositivos móveis.
	 * 
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarExportacaoDispositivosMoveis")
	public String alterarExportacaoDispositivosMoveis(ExportacaoDadosVO exportacaoDadosVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		try {

			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);

			Long chavePrimaria = exportacaoDadosVO.getChavePrimaria();
			Consulta consulta = (Consulta) controladorConsulta.obter(chavePrimaria);
			popularExportacaoArquivo(consulta, exportacaoDadosVO);
			consulta.setDadosAuditoria(getDadosAuditoria(request));

			Collection<ConsultaParametro> listaParametros =
					(Collection<ConsultaParametro>) request.getSession().getAttribute(LISTA_CONSULTA_PARAMETRO);

			controladorConsulta.setarListaParametroConsulta(consulta, listaParametros);
			model.addAttribute(CHAVE_PRIMARIA, null);
			controladorConsulta.atualizar(consulta);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Consulta.CONSULTA_CAMPO_STRING);

			view = exibirExportacaoDispositivosMoveis(exportacaoDadosVO, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoExportacaoDispositivosMoveis(exportacaoDadosVO, bindingResult, request, model);
		}

		model.addAttribute(EXPORTACAO_DADOS_VO, exportacaoDadosVO);

		return view;

	}

	/**
	 * Popular exportacao arquivo.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param exportacaoDadosVO - {@link ExportacaoDadosVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularExportacaoArquivo(Consulta consulta, ExportacaoDadosVO exportacaoDadosVO) throws GGASException {

		String sql = exportacaoDadosVO.getSql();
		String camposFormatados = exportacaoDadosVO.getColunasSelecionadas();

		if (StringUtils.isEmpty(camposFormatados)) {
			consulta.setSql(null);
		} else {
			consulta.setSql("SELECT " + camposFormatados + " " + ControladorConsulta.SQL_FROM_WHERE_FIXO_LEITURA_MOVIMENTO + " " + sql + " "
					+ ControladorConsulta.SQL_ORDER_BY_FIXO_LEITURA_MOVIMENTO);
		}
	}
	
	/**
	 * Obter map exportacao populado.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param request - {@link HttpServletRequest}
	 * @return mapCamposExecucaoConsulta - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object[]> obterMapExportacaoPopulado(Consulta consulta, HttpServletRequest request) throws GGASException {

		Map<String, Object[]> mapCamposExecucaoConsulta = new HashMap<String, Object[]>();
		List<ConsultaParametro> lista = Util.ordenarColecaoPorAtributo(consulta.getListaParametro(), "ordem", true);
		for (ConsultaParametro consultaParametro : lista) {
			Object[] valor = request.getParameterValues(consultaParametro.getChavePrimaria() + "");

			if (valor == null) {
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
			}

			// removendo mascaras
			for (int i = 0; i < valor.length; i++) {
				if (!"DATA".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
					controladorConsulta.tratarParametrosData(consultaParametro, valor, i);
				}
			}

			if (valor.length == 0 && !"BOLEANO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				// campo booleano pode ser null
				// (false)
				throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
			}

			if ("BOLEANO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				valor = new Boolean[1];
				valor[0] = null;

				Boolean[] params = new Boolean[valor.length];
				for (int i = 0; i < valor.length; i++) {
					if (valor[i] != null && "on".equals(valor[i].toString())) {
						params[i] = Boolean.TRUE;
					} else {
						params[i] = Boolean.FALSE;
					}
				}
				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			} else if ("DATA".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				Date[] params = new Date[valor.length];
				for (int i = 0; i < valor.length; i++) {
					params[i] = Util.converterCampoStringParaData(consultaParametro.getNomeCampo(), valor[i].toString(),
							Constantes.FORMATO_DATA_BR);
				}
				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			} else if ("NUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				Object[] params = new Object[valor.length];

				if (valor[0].toString().contains(".")) {
					for (int i = 0; i < valor.length; i++) {
						params[i] = new Double(valor[i].toString().trim());
					}
				} else {
					for (int i = 0; i < valor.length; i++) {
						params[i] = Long.valueOf(valor[i].toString().trim());
					}
				}

				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			} else if ("ALFANUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				String[] params = new String[valor.length];
				for (int i = 0; i < valor.length; i++) {
					params[i] = valor[i].toString();
				}
				mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), params);
			}
		}
		return mapCamposExecucaoConsulta;
	}
	
}
