/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.rota;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
public class CronogramaRotaVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 7082315988723909922L;

	private Date dataSugerida;

	private Date dataMinima;

	private Date dataMaxima;

	private Date dataPrevista;

	private Integer anoMesReferencia;

	private Integer numeroCiclo;

	private boolean emAlerta = false;

	private Collection<Rota> rotasEmAlerta;

	private Integer quantidadeDiasPeriodo;

	/**
	 * @return the dataSugerida
	 */
	public Date getDataSugerida() {
		Date data = null;
		if (this.dataSugerida != null) {
			data = (Date) dataSugerida.clone();
		}
		return data;
	}

	/**
	 * @param dataSugerida
	 *            the dataSugerida to set
	 */
	public void setDataSugerida(Date dataSugerida) {
		if(dataSugerida != null) {
			this.dataSugerida = (Date) dataSugerida.clone();
		} else {
			this.dataSugerida = null;
		}
	}

	/**
	 * @return the dataPrevista
	 */
	public Date getDataPrevista() {
		Date data = null;
		if (this.dataPrevista != null) {
			data = (Date) dataPrevista.clone();
		}
		return data;
	}

	/**
	 * @param dataPrevista
	 *            the dataPrevista to set
	 */
	public void setDataPrevista(Date dataPrevista) {
		if(dataPrevista != null) {
			this.dataPrevista = (Date) dataPrevista.clone();
		} else {
			this.dataPrevista = null;
		}
	}

	/**
	 * @return the dataMinima
	 */
	public Date getDataMinima() {
		Date data = null;
		if (this.dataMinima != null) {
			data = (Date) dataMinima.clone();
		}
		return data;
	}

	/**
	 * @param dataMinima
	 *            the dataMinima to set
	 */
	public void setDataMinima(Date dataMinima) {
		if(dataMinima != null) {
			this.dataMinima = (Date) dataMinima.clone();
		} else {
			this.dataMinima = null;
		}
	}

	/**
	 * @return the dataMaxima
	 */
	public Date getDataMaxima() {
		Date data = null;
		if(this.dataMaxima != null) {
			data = (Date) dataMaxima.clone();
		}
		return data;
	}

	/**
	 * @param dataMaxima
	 *            the dataMaxima to set
	 */
	public void setDataMaxima(Date dataMaxima) {
		if(dataMaxima != null) {
			this.dataMaxima = dataMaxima;
		} else {
			this.dataMaxima = null;
		}
	}

	public String getDataSugeridaFormatada() {

		if(dataSugerida == null) {
			return "";
		} else {
			return Util.converterDataParaStringSemHora(dataSugerida, Constantes.FORMATO_DATA_BR);
		} 
			
	}

	public String getDataPrevistaFormatada() {

		if(dataPrevista == null) {
			return "";
		} else {
			return Util.converterDataParaStringSemHora(dataPrevista, Constantes.FORMATO_DATA_BR);
		} 
	
	}

	public String getDataMinimaFormatadaParametro() {

		String dataFormatadaParametro = "";
		if(dataMinima != null) {
			dataFormatadaParametro = Util.formatarDataParametroComponente(dataMinima);
		}
		return dataFormatadaParametro;
	}

	public String getDataMaximaFormatadaParametro() {

		String dataFormatadaParametro = "";
		if(dataMaxima != null) {
			dataFormatadaParametro = Util.formatarDataParametroComponente(dataMaxima);
		}
		return dataFormatadaParametro;
	}

	/**
	 * @return the numeroCiclo
	 */
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/**
	 * @return the anoMesReferencia
	 */
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	/**
	 * @param anoMesReferencia
	 *            the anoMesReferencia to set
	 */
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	public String getAnoMesReferenciaFormatado() {

		String anoMesReferenciaFormatado = "";
		if(anoMesReferencia != null) {
			anoMesReferenciaFormatado = Util.formatarAnoMes(anoMesReferencia);
		}
		return anoMesReferenciaFormatado;
	}

	/**
	 * @return the emAlerta
	 */
	public boolean isEmAlerta() {

		return emAlerta;
	}

	/**
	 * @param emAlerta
	 *            the emAlerta to set
	 */
	public void setEmAlerta(boolean emAlerta) {

		this.emAlerta = emAlerta;
	}

	public String getEmAlertaFormatado() {

		String emAlertaFormatado = "Não";
		if(isEmAlerta()) {
			emAlertaFormatado = "Sim";
		}
		return emAlertaFormatado;
	}

	/**
	 * @return the rotasEmAlerta
	 */
	public Collection<Rota> getRotasEmAlerta() {

		return rotasEmAlerta;
	}

	/**
	 * @param rotasEmAlerta
	 *            the rotasEmAlerta to set
	 */
	public void setRotasEmAlerta(Collection<Rota> rotasEmAlerta) {

		this.rotasEmAlerta = rotasEmAlerta;
	}

	public String getRotasEmAlertaFormatado() {

		StringBuilder stringBuilder = new StringBuilder("Rota(s):[");
		if (this.rotasEmAlerta != null) {
			for (Rota rota : this.rotasEmAlerta) {
				stringBuilder.append("(");
				stringBuilder.append(rota.getNumeroRota());
				stringBuilder.append(")");
			}
		}
		
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

	/**
	 * @return the quantidadeDiasPeriodo
	 */
	public Integer getQuantidadeDiasPeriodo() {

		return quantidadeDiasPeriodo;
	}

	/**
	 * @param quantidadeDiasPeriodo
	 *            the quantidadeDiasPeriodo to set
	 */
	public void setQuantidadeDiasPeriodo(Integer quantidadeDiasPeriodo) {

		this.quantidadeDiasPeriodo = quantidadeDiasPeriodo;
	}
}
