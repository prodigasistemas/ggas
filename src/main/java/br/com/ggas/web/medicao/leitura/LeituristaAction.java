/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.impl.LeituristaImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas a entidade Leiturista
 * 
 */
@Controller
public class LeituristaAction extends GenericAction {

	private static final String FUNCIONARIO = "funcionario";

	private static final String HABILITADO = "habilitado";

	private static final String ID_FUNCIONARIO = "idFuncionario";

	private static final String VERSAO = "versao";

	@Autowired
	private ControladorLeiturista controladorLeiturista;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorFuncionario controladorFuncionario;

	/**
	 * Método responsável por alterar um leiturista.
	 * 
	 * @param leituristaTmp
	 *            {@link Leiturista}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws Exception
	 *             {@link Exception}
	 */
	@RequestMapping("alterarLeiturista")
	public String alterarLeiturista(LeituristaImpl leituristaTmp, BindingResult result, HttpServletRequest request,
			Model model) {

		String view = "exibirAlteracaoLeiturista";
		model.addAttribute(HABILITADO, leituristaTmp.isHabilitado());
		try {
			if (leituristaTmp.getFuncionario() != null) {
				model.addAttribute(FUNCIONARIO,
						controladorFuncionario.obter(leituristaTmp.getFuncionario().getChavePrimaria()));
			}
			validarToken(request);
			Leiturista leiturista = (Leiturista) controladorLeiturista.obter(leituristaTmp.getChavePrimaria(),
					Leiturista.PROPRIEDADES_LAZY);
			popularLeiturista(leiturista, request, leituristaTmp);
			leiturista.setDadosAuditoria(getDadosAuditoria(request));
			controladorLeiturista.atualizar(leiturista);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Leiturista.LEITURISTA_ROTULO);
			view = pesquisarLeiturista(leiturista.getFuncionario().getChavePrimaria(), leiturista.isHabilitado(),
					request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de inserção de um Leiturista no sistema.
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirInclusaoLeiturista")
	public String exibirInclusaoLeiturista(Model model) {

		return "exibirInclusaoLeiturista";
	}

	/**
	 * Método responsável por incluir um(a) Leiturista.
	 * 
	 * @param leituristaTmp
	 *            {@link Leiturista}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletResquest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 */
	@RequestMapping("incluirLeiturista")
	public String incluirLeiturista(LeituristaImpl leituristaTmp, BindingResult result, HttpServletRequest request,
			Model model) throws FormatoInvalidoException {

		model.addAttribute(FUNCIONARIO, leituristaTmp.getFuncionario());
		String view = "exibirInclusaoLeiturista";
		try {
			LeituristaImpl leiturista = new LeituristaImpl();
			popularLeiturista(leiturista, request, leituristaTmp);
			leiturista.setHabilitado(true);
			leiturista.setDadosAuditoria(getDadosAuditoria(request));

			controladorLeiturista.inserir(leiturista);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Leiturista.LEITURISTA_ROTULO);
			view = pesquisarLeiturista(leiturista.getFuncionario().getChavePrimaria(), true, request, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;

	}

	/**
	 * Método responsável por exibir a tela de pesquisa de um(a) Leiturista no
	 * sistema.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaLeiturista")
	public String exibirPesquisaLeiturista(HttpServletRequest request, Model model) {

		request.getSession().removeAttribute(FUNCIONARIO);
		request.getSession().removeAttribute(HABILITADO);

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaLeiturista";
	}

	/**
	 * Método responsável por pesquisar um(a) Leiturista.
	 * 
	 * @param idFuncionario
	 *            {@link Long}
	 * @param habilitado
	 *            {@link Boolean}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("pesquisarLeiturista")
	public String pesquisarLeiturista(@RequestParam(value = "idFuncionario", required = false) Long idFuncionario,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws FormatoInvalidoException, NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute(HABILITADO, habilitado);
		if (idFuncionario != null) {
			FuncionarioImpl funcionario = (FuncionarioImpl) controladorFuncionario.obter(idFuncionario);
			model.addAttribute(FUNCIONARIO, funcionario);
		}
		this.prepararFiltro(filtro, idFuncionario, habilitado);
		try {
			model.addAttribute("listaLeituristas", controladorLeiturista.consultarLeituristas(filtro));
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}
		return exibirPesquisaLeiturista(request, model);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Leiturista.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param idFuncionario
	 *            {@link Long}
	 * @param habilitado
	 *            {@link Boolean}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("exibirDetalhamentoLeiturista")
	public String exibirDetalhamentoLeiturista(@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = "idFuncionario", required = false) Long idFuncionario,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws NegocioException {

		String view = "exibirDetalhamentoLeiturista";
		try {
			Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chavePrimaria,
					Leiturista.PROPRIEDADES_LAZY);
			model.addAttribute("leiturista", leiturista);
			if (idFuncionario != null) {
				salvarPesquisa(request, controladorFuncionario.obter(idFuncionario), FUNCIONARIO);
			}
			super.salvarPesquisa(request, habilitado, HABILITADO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaLeiturista(request, model);
		}

		return view;
	}

	/**
	 * Método responsável por excluir os leituristas selecionados.
	 * 
	 * @param chavesPrimarias
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws FormatoInvalidoException
	 *             {@link FormatoInvalidoException}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("removerLeiturista")
	public String removerLeiturista(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws FormatoInvalidoException, NegocioException {

		try {
			controladorLeiturista.validarRemoverLeituristas(chavesPrimarias);

			// rotina criada para dar uma mensagem melhor quando tentar excluir um
			// leiturista que possua pelo menos uma rota
			// ao ives de dar a msg padrao do sistema
			Map<String, Object> filtro = new HashMap<String, Object>();
			for (Long l : chavesPrimarias) {
				filtro.put("idLeiturista", l);
				if (!controladorRota.consultarRota(filtro).isEmpty()) {
					throw new NegocioException(
							"O(s) Leiturista(s) está(ão) associado(s) a um Rota e não pode ser excluído.");
				}
			}

			controladorLeiturista.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Leiturista.LEITURISTA_ROTULO);
		} catch (NegocioException e) {
			Log.info(e.getMessage());
			super.mensagemErro(model, e.getMessage());
		}

		return pesquisarLeiturista(null, null, request, model);
	}

	/**
	 * Método responsável por exibir a tela de alterar leiturista.
	 * 
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("exibirAlteracaoLeiturista")
	public String exibirAlteracaoLeiturista(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		String view = "exibirAlteracaoLeiturista";
		try {
			if (!isPostBack(request)) {
				Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chavePrimaria,
						Leiturista.PROPRIEDADES_LAZY);
				model.addAttribute("chavePrimaria", chavePrimaria);
				model.addAttribute(FUNCIONARIO, leiturista.getFuncionario());
				model.addAttribute(HABILITADO, leiturista.isHabilitado());
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaLeiturista(request, model);
		}
		
		saveToken(request);

		return view;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro
	 *            {@link Map}
	 * @param idFuncionario
	 *            {@link Long}
	 * @param habilitado
	 *            {@link Boolean}
	 */
	private void prepararFiltro(Map<String, Object> filtro, Long idFuncionario, Boolean habilitado) {

		if ((idFuncionario != null) && (idFuncionario > 0)) {
			filtro.put(ID_FUNCIONARIO, idFuncionario);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
	}

	/**
	 * Popular leiturista.
	 * 
	 * @param leiturista
	 *            {@link Leiturista}
	 * @param resquest
	 *            {@link HttpServletRequest}
	 * @param leituristaTmp
	 *            {@link Leiturista}
	 */
	private void popularLeiturista(Leiturista leiturista, HttpServletRequest request, LeituristaImpl leituristaTmp) {

		Integer versao = (Integer) request.getAttribute(VERSAO);

		if (versao != null && versao.intValue() > 0) {
			leiturista.setVersao(versao);
		}
		if (leituristaTmp.getFuncionario() != null) {
			leiturista.setFuncionario(leituristaTmp.getFuncionario());
		}

		leiturista.setHabilitado(leituristaTmp.isHabilitado());

	}

	@RequestMapping("cancelarDetalhamentoLeiturista")
	public String cancelarDetalhamentoLeiturista(HttpServletRequest request, Model model)
			throws FormatoInvalidoException, NegocioException {

		Boolean habilitado = (Boolean) retornaPesquisa(request, HABILITADO);

		Long idFuncionario = null;
		if (retornaPesquisa(request, FUNCIONARIO) != null) {
			FuncionarioImpl funcionario = (FuncionarioImpl) retornaPesquisa(request, FUNCIONARIO);
			idFuncionario = funcionario.getChavePrimaria();
		}

		return pesquisarLeiturista(idFuncionario, habilitado, request, model);
	}

}
