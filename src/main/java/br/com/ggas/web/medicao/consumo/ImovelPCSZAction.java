/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.consumo.impl.ControladorImovelPCSZ;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 * alterações nas informações das telas referentes a Manter Dados de Compra de Gás - Imóvel.
 */
@Controller
public class ImovelPCSZAction extends GenericAction {

	private static final String IMOVEL_DADOS_COMPRA_GAS_VO = "imovelDadosCompraGasVO";

	private static final String EXIBIR_MANUTENCAO_PCSZ_IMOVEL = "exibirManutencaoPCSZImovel";

	private static final String LISTA_IMOVEL_PCS_Z_VO = "listaImovelPCSZVO";

	private static final String LISTA_MESES_ANO = "listaMesesAno";

	private static final String LISTA_ANOS = "listaAnos";

	private static final String LISTA_IMOVEIS = "listaImoveis";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME = "nome";

	private static final String IMOVEL = "imovel";
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorImovelPCSZ controladorImovelPCSZ;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa para manutenção de PCS e Fator Z
	 * do Imóvel.
	 * 
	 * @return exibirManutencaoPCSZImovel {@link String}
	 */
	@RequestMapping("exibirPesquisaManutencaoPCSZImovel")
	public String exibirPesquisaManutencaoPCSZImovel() {

		return EXIBIR_MANUTENCAO_PCSZ_IMOVEL;
	}

	/**
	 * Método responsável por realizar a pesquisa
	 * de imóveis para manutenção de PCS e Fator
	 * Z.
	 *
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirManutencaoPCSZImovel {@link String}
	 * @throws GGASException Caso ocorra algum erro
	 */
	@RequestMapping("pesquisarImovelManutencaoPCSZ")
	public String pesquisarImovelManutencaoPCSZ(ImovelDadosCompraGasVO imovelDadosCompraGasVO, BindingResult bindingResult,
					HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = null;

		try {

			if ((imovelDadosCompraGasVO != null) && !StringUtils.isEmpty(imovelDadosCompraGasVO.getMatricula())) {
				Util.converterCampoStringParaValorLong(Imovel.MATRICULA, imovelDadosCompraGasVO.getMatricula());
			}

			filtro = prepararFiltro(imovelDadosCompraGasVO);

			Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);

			model.addAttribute(LISTA_IMOVEIS, listaImoveis);
			model.addAttribute(IMOVEL_DADOS_COMPRA_GAS_VO, imovelDadosCompraGasVO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return EXIBIR_MANUTENCAO_PCSZ_IMOVEL;
	}

	/**
	 * Método responsável por exibir a tela para
	 * manutenção de PCS e FatorZ do imóvel
	 * selecionado.
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisaManutencaoPCSZImovel {@link String}
	 * @throws GGASException Caso ocorra algum erro
	 */
	@RequestMapping(EXIBIR_MANUTENCAO_PCSZ_IMOVEL)
	public String exibirManutencaoPCSZImovel(ImovelDadosCompraGasVO imovelDadosCompraGasVO, BindingResult bindingResult,
					HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(IMOVEL_DADOS_COMPRA_GAS_VO, imovelDadosCompraGasVO);

		Integer ano = null;
		Integer mes = null;
		if (!super.isPostBack(request)) {
			ano = imovelDadosCompraGasVO.getAno();
			mes = imovelDadosCompraGasVO.getMes();
		}

		if (ano == null || ano < 1) {
			ano = null;
		}
		if (mes == null || mes < 1) {
			mes = null;
		}

		Collection<ImovelPCSZ> listaPCSZImovel = null;

		Imovel imovel = null;

		try {

			carregarCampos(imovelDadosCompraGasVO.getChavePrimaria(), imovelDadosCompraGasVO.getAno(), imovelDadosCompraGasVO.getMes(),
							request, model);

			if (imovelDadosCompraGasVO.getChavePrimaria() != null && imovelDadosCompraGasVO.getChavePrimaria() > 0) {

				imovel = (Imovel) controladorImovel.obter(imovelDadosCompraGasVO.getChavePrimaria());

				listaPCSZImovel = controladorImovelPCSZ.listarPCSZImovel(imovel, ano, mes);

				Collection<ImovelPCSZVO> listaImovelPCSZVO = converterImovelPCSZ(listaPCSZImovel);

				model.addAttribute(LISTA_IMOVEL_PCS_Z_VO, listaImovelPCSZVO);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		saveToken(request);

		return "pesquisaManutencaoPCSZImovel";
	}

	/**
	 * Carrega os campos.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param anoParam {@link Integer}
	 * @param mesParam {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException Caso ocorra algum erro
	 */
	private void carregarCampos(Long chavePrimaria, Integer anoParam, Integer mesParam, HttpServletRequest request, Model model)
					throws GGASException {

		Integer ano = null;
		Integer mes = null;

		if (!super.isPostBack(request)) {
			ano = anoParam;
			mes = mesParam;
		}

		if (ano == null || ano < 1) {
			ano = null;
		}
		if (mes == null || mes < 1) {
			mes = null;
		}

		Imovel imovel = null;
		if (chavePrimaria != null && chavePrimaria > 0) {

			imovel = (Imovel) controladorImovel.obter(chavePrimaria);
			model.addAttribute(IMOVEL, imovel);
		}

		model.addAttribute(LISTA_ANOS, listarAnos());
		model.addAttribute(LISTA_MESES_ANO, Mes.MESES_ANO);
	}

	/**
	 * Listar anos.
	 * 
	 * @return listaAnos {@link Collection}
	 * @throws GGASException Caso ocorra algum erro
	 */
	private Collection<String> listarAnos() throws GGASException {

		Collection<String> listaAnos = new ArrayList<String>();

		Integer anoAtual = new DateTime().getYear();

		String valorQuantidadeAnos = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		Integer quantidadeAnos = Integer.valueOf(valorQuantidadeAnos);

		for (int i = 0; i < quantidadeAnos; i++) {
			Integer ano = anoAtual - i;
			listaAnos.add(ano.toString());
		}

		return listaAnos;
	}

	/**
	 * Converter imovel pcsz.
	 * 
	 * @param listaImovelPCSZ {@link Collection}
	 * @return listaImovelPCSZVO {@link Collection}
	 * @throws GGASException Caso ocorra algum erro
	 */
	private Collection<ImovelPCSZVO> converterImovelPCSZ(Collection<ImovelPCSZ> listaImovelPCSZ) throws GGASException {

		Collection<ImovelPCSZVO> listaImovelPCSZVO = new ArrayList<ImovelPCSZVO>();

		if (listaImovelPCSZ != null && !listaImovelPCSZ.isEmpty()) {

			for (ImovelPCSZ imovelPCSZ : listaImovelPCSZ) {

				ImovelPCSZVO imovelPCSZVO = new ImovelPCSZVO();
				imovelPCSZVO.setData(imovelPCSZ.getDataVigencia());

				if (imovelPCSZ.getMedidaPCS() != null) {
					imovelPCSZVO.setMedidaPCS(imovelPCSZ.getMedidaPCS().toString());
				}
				if (imovelPCSZ.getFatorZ() != null) {
					imovelPCSZVO.setFatorZ(Util.converterCampoValorParaString(imovelPCSZ.getFatorZ(), Constantes.FORMATO_VALOR_FATOR_Z,
									Constantes.LOCALE_PADRAO));
				}
				imovelPCSZVO.setHabilitado(controladorImovelPCSZ.permitirAtualizacaoImovelPCSZ(imovelPCSZ));
				imovelPCSZVO.setPermiteAlteracaoFatorZ(
								controladorImovelPCSZ.permitirManutencaoFatorZ(imovelPCSZ.getImovel().getChavePrimaria()));

				listaImovelPCSZVO.add(imovelPCSZVO);
			}
		}

		return listaImovelPCSZVO;
	}

	/**
	 * Método responsável por manter os PCS e
	 * Fator K de um imóvel.
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirManutencaoPCSZImovel Caso ocorra algum erro retornará para exibirManutencaoPCSZImovel
	 * @throws GGASException Caso ocorra algum erro
	 */
	@RequestMapping("manterPCSZImovel")
	public String manterPCSZImovel(ImovelDadosCompraGasVO imovelDadosCompraGasVO, BindingResult bindingResult, HttpServletRequest request,
					Model model) throws GGASException {

		Long chavePrimariaAux = imovelDadosCompraGasVO.getChavePrimaria();

		model.addAttribute(IMOVEL_DADOS_COMPRA_GAS_VO, imovelDadosCompraGasVO);

		try {

			validarToken(request);

			Collection<ImovelPCSZVO> listaImovelPCSZVO = converterImovelPCSZVO(imovelDadosCompraGasVO);
			model.addAttribute(LISTA_IMOVEL_PCS_Z_VO, listaImovelPCSZVO);

			Collection<ImovelPCSZ> listaImovelPCSZ = new ArrayList<ImovelPCSZ>();
			popularListaImovelPCSZ(imovelDadosCompraGasVO, request, listaImovelPCSZ);

			boolean exibirMsgProcessarConsistencia = controladorImovelPCSZ.atualizarListaImovelPCSZ(listaImovelPCSZ);

			if (exibirMsgProcessarConsistencia) {
				mensagemAlerta(model, Constantes.NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA);
			} else {
				mensagemSucesso(model, ControladorImovelPCSZ.SUCESSO_IMOVEL_PCS_Z_ATUALIZADO, ImovelPCSZ.IMOVEL_PCS_Z);
			}

			return pesquisarImovelManutencaoPCSZ(imovelDadosCompraGasVO, bindingResult, request, model);

		} catch (GGASException e) {
			imovelDadosCompraGasVO.setChavePrimaria(chavePrimariaAux);
			model.addAttribute("imovelDados", imovelDadosCompraGasVO);
			model.addAttribute("erroDadosInvalidos", true);
			mensagemErroParametrizado(model, request, e);
		}

		return exibirManutencaoPCSZImovel(imovelDadosCompraGasVO, bindingResult, request, model);

	}

	/**
	 * Converter imovel pcszvo.
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @return listaImovelPCSZVO {@link Collection}
	 * @throws GGASException Caso ocorra algum erro
	 */
	private Collection<ImovelPCSZVO> converterImovelPCSZVO(ImovelDadosCompraGasVO imovelDadosCompraGasVO) throws GGASException {

		Collection<ImovelPCSZVO> listaImovelPCSZVO = new ArrayList<ImovelPCSZVO>();

		if (imovelDadosCompraGasVO.getDataVigencia() != null && imovelDadosCompraGasVO.getDataVigencia().length > 0) {
			for (int i = 0; i < imovelDadosCompraGasVO.getDataVigencia().length; i++) {

				ImovelPCSZVO imovelPCSZVO = new ImovelPCSZVO();

				if (!StringUtils.isEmpty(imovelDadosCompraGasVO.getDataVigencia()[i])) {
					imovelPCSZVO.setData(Util.converterCampoStringParaData("Data", imovelDadosCompraGasVO.getDataVigencia()[i],
									Constantes.FORMATO_DATA_BR));
				}
				if (imovelDadosCompraGasVO.getMedidaPCS() != null && !StringUtils.isEmpty(imovelDadosCompraGasVO.getMedidaPCS()[i])) {
					imovelPCSZVO.setMedidaPCS(imovelDadosCompraGasVO.getMedidaPCS()[i]);
				}
				if (imovelDadosCompraGasVO.getFatorZ() != null && !StringUtils.isEmpty(imovelDadosCompraGasVO.getFatorZ()[i])) {
					imovelPCSZVO.setFatorZ(imovelDadosCompraGasVO.getFatorZ()[i]);
				}
				if (imovelDadosCompraGasVO.isHabilitado() != null) {
					imovelPCSZVO.setHabilitado(imovelDadosCompraGasVO.isHabilitado()[i]);
				}
				if (imovelDadosCompraGasVO.isPermiteAlteracaoFatorZ() != null) {
					imovelPCSZVO.setPermiteAlteracaoFatorZ(imovelDadosCompraGasVO.isPermiteAlteracaoFatorZ()[i]);
				}
				listaImovelPCSZVO.add(imovelPCSZVO);
			}
		}

		return listaImovelPCSZVO;
	}

	/**
	 * Popular lista imovel pcsz.
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @param request {@link HttpServletRequest}
	 * @param listaImovelPCSZ {@link Collection}
	 * @throws GGASException Caso ocorra algum erro
	 */
	private void popularListaImovelPCSZ(ImovelDadosCompraGasVO imovelDadosCompraGasVO, HttpServletRequest request,
					Collection<ImovelPCSZ> listaImovelPCSZ) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		Imovel imovel = null;
		if (imovelDadosCompraGasVO.getChavePrimaria() != null && imovelDadosCompraGasVO.getChavePrimaria() > 0) {
			imovel = (Imovel) controladorImovel.obter(imovelDadosCompraGasVO.getChavePrimaria());
			imovelDadosCompraGasVO.setChavePrimaria(null);
		}

		if (imovelDadosCompraGasVO.getDataVigencia() != null && imovelDadosCompraGasVO.getDataVigencia().length > 0) {
			for (Integer i = 0; i < imovelDadosCompraGasVO.getDataVigencia().length; i++) {
				ImovelPCSZ imovelPCSZ = (ImovelPCSZ) controladorImovelPCSZ.criar();

				verificarParametrosView(imovelDadosCompraGasVO, listaImovelPCSZ, dadosAuditoria, imovel, i, imovelPCSZ);
			}
		}
	}

	/**
	 * Método responsável por verificar se os parametros da view não são nulos antes de converte-los
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @param listaImovelPCSZ {@link Collection}
	 * @param dadosAuditoria {@link DadosAuditoria}
	 * @param imovel {@link Imovel}
	 * @param indice {@link Integer}
	 * @param imovelPCSZ {@link ImovelPCSZ}
	 * @throws GGASException Caso ocorra algum erro
	 */
	public void verificarParametrosView(ImovelDadosCompraGasVO imovelDadosCompraGasVO, Collection<ImovelPCSZ> listaImovelPCSZ,
					DadosAuditoria dadosAuditoria, Imovel imovel, Integer indice, ImovelPCSZ imovelPCSZ) throws GGASException {

		if (imovelDadosCompraGasVO.isHabilitado() != null && imovelDadosCompraGasVO.isHabilitado()[indice]) {

			imovelPCSZ.setImovel(imovel);

			if (!StringUtils.isEmpty(imovelDadosCompraGasVO.getDataVigencia()[indice])) {
				imovelPCSZ.setDataVigencia(Util.converterCampoStringParaData("Data", imovelDadosCompraGasVO.getDataVigencia()[indice],
								Constantes.FORMATO_DATA_BR));
			}
			if (imovelDadosCompraGasVO.getMedidaPCS() != null && !StringUtils.isEmpty(imovelDadosCompraGasVO.getMedidaPCS()[indice])) {
				imovelPCSZ.setMedidaPCS(Util.converterCampoStringParaValorInteger("PCS", imovelDadosCompraGasVO.getMedidaPCS()[indice]));
			}
			if (imovelDadosCompraGasVO.isPermiteAlteracaoFatorZ() != null && imovelDadosCompraGasVO.isPermiteAlteracaoFatorZ()[indice]
							&& imovelDadosCompraGasVO.getFatorZ() != null
							&& !StringUtils.isEmpty(imovelDadosCompraGasVO.getFatorZ()[indice])) {
				imovelPCSZ.setFatorZ(Util.converterCampoStringParaValorBigDecimal("Fator Z", imovelDadosCompraGasVO.getFatorZ()[indice],
								"##.##", Constantes.LOCALE_PADRAO));
			}

			imovelPCSZ.setDadosAuditoria(dadosAuditoria);
			listaImovelPCSZ.add(imovelPCSZ);
		}
	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param imovelDadosCompraGasVO {@link ImovelDadosCompraGasVO}
	 * @return filtro {@link Map}
	 */
	private Map<String, Object> prepararFiltro(ImovelDadosCompraGasVO imovelDadosCompraGasVO) {

		Map<String, Object> filtro = new HashMap<>();

		if (imovelDadosCompraGasVO != null) {

			if (imovelDadosCompraGasVO.getChavePrimaria() != null && imovelDadosCompraGasVO.getChavePrimaria() > 1) {
				filtro.put(CHAVE_PRIMARIA, imovelDadosCompraGasVO.getChavePrimaria());
			}

			if (!StringUtils.isEmpty(imovelDadosCompraGasVO.getMatricula())) {
				filtro.put(CHAVE_PRIMARIA, Long.parseLong(imovelDadosCompraGasVO.getMatricula()));
			}

			if (!StringUtils.isEmpty(imovelDadosCompraGasVO.getNome())) {
				filtro.put(NOME, imovelDadosCompraGasVO.getNome());
			}

		}

		return filtro;
	}
	
}
