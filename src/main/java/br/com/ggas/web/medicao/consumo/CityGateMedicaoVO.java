/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import java.io.Serializable;
import java.util.Date;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela transferência de dados relacionados as telas
 * de CityGateMedicaoAction
 *
 */
public class CityGateMedicaoVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 4114486432184588683L;

	private Date data;

	private String valorSupridora;

	private String valorDistribuidora;

	private String valorFixo;

	private String volumeSupridora;

	private String volumeDistribuidora;

	private boolean habilitado;

	/**
	 * @return the data
	 */
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Date data) {

		this.data = data;
	}

	/**
	 * @return the valorSupridora
	 */
	public String getValorSupridora() {

		return valorSupridora;
	}

	/**
	 * @param valorSupridora
	 *            the valorSupridora to set
	 */
	public void setValorSupridora(String valorSupridora) {

		this.valorSupridora = valorSupridora;
	}

	/**
	 * @return the valorDistribuidora
	 */
	public String getValorDistribuidora() {

		return valorDistribuidora;
	}

	/**
	 * @param valorDistribuidora
	 *            the valorDistribuidora to set
	 */
	public void setValorDistribuidora(String valorDistribuidora) {

		this.valorDistribuidora = valorDistribuidora;
	}

	/**
	 * @return the valorFixo
	 */
	public String getValorFixo() {

		return valorFixo;
	}

	/**
	 * @param valorFixo
	 *            the valorFixo to set
	 */
	public void setValorFixo(String valorFixo) {

		this.valorFixo = valorFixo;
	}

	/**
	 * @return the habilitado
	 */
	public boolean isHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * Método responsável por retornar a data no
	 * formato dd/MM/yyyy
	 * 
	 * @return
	 */
	public String getDataFormatada() {

		return Util.converterDataParaStringSemHora(this.getData(), Constantes.FORMATO_DATA_BR);
	}

	/**
	 * @return the volumeSupridora
	 */
	public String getVolumeSupridora() {

		return volumeSupridora;
	}

	/**
	 * @param volumeSupridora
	 *            the volumeSupridora to set
	 */
	public void setVolumeSupridora(String volumeSupridora) {

		this.volumeSupridora = volumeSupridora;
	}

	/**
	 * @return the volumeDistribuidora
	 */
	public String getVolumeDistribuidora() {

		return volumeDistribuidora;
	}

	/**
	 * @param volumeDistribuidora
	 *            the volumeDistribuidora to set
	 */
	public void setVolumeDistribuidora(String volumeDistribuidora) {

		this.volumeDistribuidora = volumeDistribuidora;
	}
}
