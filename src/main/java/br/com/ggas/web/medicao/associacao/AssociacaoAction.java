/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.associacao;

import br.com.ggas.atendimento.chamado.dominio.AssociacaoVO;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.*;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.FaixaPressaoFornecimentoImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo para
 * realizar alterações nas informações das telas referentes a funcionalidade
 * Associar Ponto de Consumo ao Medidor / Corretor de Vazão.
 */
@Controller
public class AssociacaoAction extends GenericAction {

	private static final String DADOS_REATIVACAO = "dadosReativacao";

	private static final String DADOS_BLOQUEIO = "dadosBloqueio";

	private static final String DADOS_ATIVACAO = "dadosAtivacao";
	
	private static final String DADOS_BLOQUEIO_REATIVACAO = "dadosBloqueioReativacao";

	private static final String EXIBIR_ASSOCIACAO = "exibirAssociacao";

	private static final String URL_RETORNO_ATUALIZACAO_CASTRAL = "urlRetornoAtualizacaoCastral";

	private static final String ASSOCIACAO_VO = "associacaoVO";

	private static final String DADOS_RETIRADA = "dadosRetirada";

	private static final String DADOS_SUBSTITUICAO = "dadosSubstituicao";

	private static final String DADOS_INSTALACAO = "dadosInstalacao";

	private static final String DADOS_INSTALACAO_ATIVACAO = "dadosInstalacaoAtivacao";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String MSG_ERRO_CORRETOR_DATA = "CORRETOR_DATA";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumoAssociacao";

	private static final String LISTA_IMOVEL = "listaImovelAssociacao";

	private static final String LISTA_TIPO_ASSOCIACAO = "listaTipoAssociacao";

	private static final String LISTA_MEDIDOR_LOCAL_INSTALACAO = "listaMedidorLocalInstalacao";

	private static final String LISTA_MEDIDOR_PROTECAO = "listaMedidorProtecao";

	private static final String LISTA_FUNCIONARIO = "listaFuncionario";

	private static final String LISTA_MEDIDOR_MOTIVO_OPERACAO = "listaMedidorMotivoOperacao";

	private static final String ID_MEDIDOR_ANTERIOR = "chaveMedidorAnterior";

	private static final String ID_CORRETOR_VAZAO_ANTERIOR = "chaveCorretorVazaoAnterior";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String NUMERO_MEDIDOR = "numeroMedidor";

	private static final String NUMERO_CORRETOR_VAZAO = "numeroCorretorVazao";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ID_OPERACAO_MEDIDOR = "idOperacaoMedidor";

	private static final String ID_OPERACAO_MEDIDOR_INDEPENDENTE = "idOperacaoMedidorIndependente";

	private static final String TIPO_ASSOCIACAO = "tipoAssociacao";

	private static final String DESCRICAO_PONTO_CONSUMO = "descricaoPontoConsumo";

	private static final String DESCRICAO_IMOVEL = "descricaoImovel";

	private static final String ENDERECO_PONTO_CONSUMO = "enderecoPontoConsumo";

	private static final String ENDERECO_IMOVEL = "enderecoImovel";

	private static final String CEP_PONTO_CONSUMO = "cepPontoConsumo";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String CONTEM_CORRETOR_VAZAO = "contemCorretorVazao";

	private static final String COMPLEMENTO_PONTO_CONSUMO = "complementoPontoConsumo";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String PRESSAO = "pressao";

	private static final String FATOR_CORRECAO = "fatorCorrecao";

	private static final String TITULO_ASSOCIACAO = "tituloAssociacao";

	private static final String NUMERO_SERIE_MEDIDOR_ANTERIOR = "numeroSerieMedidorAnterior";

	private static final String TIPO_MEDIDOR_ANTERIOR = "tipoMedidorAnterior";

	private static final String MODELO_MEDIDOR_ANTERIOR = "modeloMedidorAnterior";

	private static final String MARCA_MEDIDOR_ANTERIOR = "marcaMedidorAnterior";

	private static final String NUMERO_DIGITOS_MEDIDOR_ANTERIOR = "numeroDigitosMedidorAnterior";

	private static final String NUMERO_DIGITOS_CORRETOR_VAZAO_ANTERIOR = "numeroDigitosCorretorVazaoAnterior";

	private static final String NUMERO_SERIE_CORRETOR_VAZAO_ANTERIOR = "numeroSerieCorretorVazaoAnterior";

	private static final String MODELO_CORRETOR_VAZAO_ANTERIOR = "modeloCorretorVazaoAnterior";

	private static final String MARCA_CORRETOR_VAZAO_ANTERIOR = "marcaCorretorVazaoAnterior";

	private static final String TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR = "tipoAssociacaoPontoConsumoMedidor";

	private static final String TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO = "tipoAssociacaoPontoConsumoCorretorVazao";

	private static final String DADOS = "dados";

	private static final String ID_CORRETOR_VAZAO_ATUAL = "chaveCorretorVazaoAtual";

	private static final String ID_MEDIDOR_ATUAL = "chaveMedidorAtual";

	private static final String DATA_MEDIDOR = "dataMedidor";

	private static final String DATA_CORRETOR_VAZAO = "dataCorretorVazao";

	private static final String HORA_CORRETOR_VAZAO = "horaCorretorVazao";

	private static final String LOCAL_INSTALACAO_MEDIDOR = "localInstalacaoMedidor";

	private static final String LOCAL_INSTALACAO_CORRETOR_VAZAO = "localInstalacaoCorretorVazao";

	private static final String MEDIDOR_PROTECAO = "medidorProtecao";

	private static final String LEITURA_ATUAL = "leituraAtual";

	private static final String LEITURA_ATUAL_CORRETOR_VAZAO = "leituraAtualCorretorVazao";

	private static final String LEITURA_ANTERIOR_CORRETOR_VAZAO = "leituraAnteriorCorretorVazao";

	private static final String FUNCIONARIO = "funcionario";

	private static final String FUNCIONARIO_CORRETOR_VAZAO = "funcionarioCorretorVazao";

	private static final String MEDIDOR_MOTIVO_OPERACAO = "medidorMotivoOperacao";

	private static final String INSTALAR_CORRETOR_VAZAO = "instalarCorretorVazao";

	private static final String SUBSTITUIR_CORRETOR_VAZAO = "substituirCorretorVazao";

	private static final String LEITURA_ANTERIOR = "leituraAnterior";

	private static final String FILTRO_ASSOCIACAO_PONTO_CONSUMO = "filtroAssociaoPontoConsumo";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String DOCUMENTO_CLIENTE = "documentoCliente";

	private static final String EMAIL_CLIENTE = "emailCliente";

	private static final String ENDERECO_CLIENTE = "enderecoCliente";

	private static final String INDICADOR_PESQUISA = "indicadorPesquisa";

	private static final String NOME_FANTASIA_IMOVEL = "nomeFantasiaImovel";

	private static final String MATRICULA_IMOVEL = "matriculaImovel";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String CIDADE_IMOVEL = "cidadeImovel";

	private static final String CONDOMINIO_IMOVEL = "condominio";

	private static final String DATA_INICIO_INSTALACAO = "dataInicioInstalacao";

	private static final String DATA_LIMITE_INSTALACAO = "dataLimiteInstalacao";

	private static final String LISTA_MARCA_MEDIDOR = "listaMarcaMedidor";

	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";

	private static final String LISTA_TIPO_MEDIDOR = "listaTipoMedidor";

	private static final String ID_MARCA = "idMarca";

	private static final String ID_MODELO = "idModelo";

	private static final String HABILITADO = "habilitado";

	private static final String ID_TIPO = "idTipo";

	private static final String LACRE = "lacre";
	
	private static final String LACRE_DOIS = "lacreDois";
	
	private static final String LACRE_TRES = "lacreTres";

	private static final String COMPOSICAO_VIRTUAL = "composicaoVirtual";

	private static final String MODO_USO = "modoUso";

	private static final String ID_MEDIDOR_INDEPENDENTE = "idMedidorIndependente";

	private static final String IS_ASSOCIACAO_MEDIDOR_INDEPENDENTE = "isAssociacaoMedidorIndependente";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String QUADRA = "quadra";

	private static final String QUADRA_FACE = "quadraFace";

	private static final String QUADRA_FACE_ENDERECO = "quadraFace.endereco";

	private static final String QUADRA_FACE_ENDERECO_CEP = "quadraFace.endereco.cep";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";
	public static final String EXIBIR_ASSOCIACAO_LOTE = "exibirAssociacaoLote";
	public static final String LISTA_PONTO_CONSUMO_FILHO = "listaPontoConsumoFilho";
	public static final String QUANTIDADE_PONTO_CONSUMO_FILHO = "quantidadePontoConsumoFilho";
	public static final String PONTOS_MARCADOS = "pontosMarcados";
	public static final String LEITURA_MARCADA = "leituraMarcada";
	public static final String ID_MEDIDOR_MARCADO = "idMedidorMarcado";
	public static final String NUM_MEDIDOR_MARCADO = "numMedidorMarcado";
	public static final String NUM_MEDIDOR_CORRETOR_VAZAO_MARCADO = "numMedidorCorretorVazaoMarcado";
	public static final String ID_MEDIDOR_CORRETOR_VAZAO_MARCADO = "idMedidorCorretorVazaoMarcado";
	public static final String CAMPO_DATA = "Data";
	public static final String CAMPO_LOCAL = "Local";
	public static final String CAMPO_FUNCIONARIO = "Funcionário";
	public static final String CAMPO_PROTECAO = "Proteção";
	public static final String CAMPO_MOTIVO = "Motivo";
	public static final String CAMPO_HORA = "Hora";
	public static final String DIGITO_MEDIDOR_MARCADO = "digitoMedidorMarcado";
	public static final String DIGITO_MEDIDOR_CORRETOR_VAZAO_MARCADO = "digitoMedidorCorretorVazaoMarcado";
	public static final String DIGITO_NOVE = "9";

	private static final String LISTA_PRESSAO_FORNECIMENTO_CAMPO = "listaPressaoFornecimentoCampo";
	private static final String CONTRATO_PONTO_CONSUMO = "contratoPontoConsumo";
	private static final String CONTRATO_FORMATADO="contratoFormatado";  
	private static final String PRESSAO_FORNECIMENTO_CAMPO = "pressaoFornecimentoCampo";

	private ContratoPontoConsumo contratoPontoConsumo; 

	@Autowired
	private ControladorMedidor controladorMedidor;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorContrato controladorContrato;

	@Qualifier("controladorVazaoCorretor") @Autowired
	private ControladorVazaoCorretor controladorCorretorVazao;

	/**
	 * Método responsável por exibir a tela de pesquisa de associação entre ponto de
	 * consumo e medidor ou corretor de vazão.
	 *
	 * @param model
	 * @return exibirPesquisaAssociacao {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaAssociacao")
	public String exibirPesquisaAssociacao(Model model) throws GGASException {

		configurarDadosPesquisaAssociacao(model);

		return "exibirPesquisaAssociacao";

	}

	/**
	 * Método responsável por pesquisar associações entre ponto de consumo e medidor
	 * ou corretor de vazão.
	 *
	 * @param associacao
	 * @param bindingResult
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("pesquisarAssociacao")
	public String pesquisarAssociacao(AssociacaoVO associacao, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		bindingResult.getErrorCount();

		if ("indicadorPesquisaMedidor".equals(associacao.getIndicadorPesquisa())) {
			return pesquisarMedidorAssociacao(associacao, request, model);
		} else {
			Boolean exibirDadosFiltroSessao = associacao.getExibirDadosFiltro();

			Map<String, Object> filtro = null;

			if (exibirDadosFiltroSessao != null && exibirDadosFiltroSessao) {

				filtro = (Map<String, Object>) request.getSession().getAttribute(FILTRO_ASSOCIACAO_PONTO_CONSUMO);
				
				if (filtro == null) {
					filtro = new HashMap<String, Object>();
				}
				configurarDadosFiltroPesquisaAssociacao(filtro, model);

			} else {

				request.getSession().removeAttribute(FILTRO_ASSOCIACAO_PONTO_CONSUMO);
				filtro = obterFiltroPesquisaAssociacao(associacao);

			}

			adicionarFiltroPaginacao(request, filtro);

			Boolean associacaoEmLote = associacao.getIndicadorAssociacaoEmLote();
			if (associacaoEmLote!= null && associacaoEmLote) {
				List<Imovel> listaImoveis = new ArrayList<>();
				Consumer<PontoConsumo> pontoConsumoConsumer = ponto -> {
					if (ponto.getImovel() != null && ponto.getImovel().getCondominio() != null && ponto.getImovel().isIndividual() != null
							&& !listaImoveis.contains(ponto.getImovel()) && ponto.getImovel().getCondominio()
							&& ponto.getImovel().isIndividual()) {
						listaImoveis.add(ponto.getImovel());
					}
				};
				if (associacao.getIdCliente() != null && associacao.getIdCliente() > 0) {
					controladorPontoConsumo.listarPontoConsumoPorCliente(associacao.getIdCliente()).forEach(pontoConsumoConsumer);
				} else if (associacao.getIdImovel() != null && associacao.getIdImovel() > 0){
					Imovel imovel = (Imovel) controladorImovel.obter(associacao.getIdImovel());
					if (imovel.getCondominio() != null && imovel.isIndividual() != null
							&& imovel.getCondominio() && imovel.isIndividual()) {
						listaImoveis.add(imovel);
						ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
						colecaoPaginada.setFullListSize(listaImoveis.size());
					}
				}
				model.addAttribute(LISTA_IMOVEL, criarColecaoPaginada(request, filtro, listaImoveis));
			} else {
				List<PontoConsumo> listaPontoConsumo = new ArrayList<>();
				if (associacao.getIdCliente() != null && associacao.getIdCliente() > 0) {
					listaPontoConsumo
							.addAll(controladorPontoConsumo.listarPontoConsumoPorCliente(associacao.getIdCliente()));
				} else {
					listaPontoConsumo.addAll(controladorPontoConsumo.listarPontosConsumo(filtro));
				}
				Iterator<PontoConsumo> iterator = listaPontoConsumo.iterator();
				while (iterator.hasNext()) {
					PontoConsumo pc = iterator.next();
					if (pc.getSituacaoConsumo().getDescricao() == null) {
						SituacaoConsumo sc = controladorImovel
								.obterSituacaoConsumo(pc.getSituacaoConsumo().getChavePrimaria());
						pc.setSituacaoConsumo(sc);
					}
				}
				controladorPontoConsumo.removerImovelFilhoComPaiMedicaoColetiva(listaPontoConsumo);
				model.addAttribute(LISTA_PONTO_CONSUMO, criarColecaoPaginada(request, filtro, listaPontoConsumo));
			}

			request.getSession().setAttribute(FILTRO_ASSOCIACAO_PONTO_CONSUMO, filtro);

			if (associacao.getIdCliente() != null && associacao.getIdCliente() > 0) {

				model.addAttribute("cliente", controladorCliente.obter(associacao.getIdCliente()));
			}

			if (associacao.getIdImovel() != null && associacao.getIdImovel() > 0) {

				model.addAttribute("imovel", controladorImovel.obter(associacao.getIdImovel()));
			}

			model.addAttribute(ASSOCIACAO_VO, associacao);

			if (model.containsAttribute("indicadorAtualizacaoCadastralImediata")) {

				configurarDadosPesquisaAssociacao(model);

				return "forward:" + model.asMap().get(URL_RETORNO_ATUALIZACAO_CASTRAL);
			}

			return exibirPesquisaAssociacao(model);
		}
	}

	/**
	 * Método responsável por salvar uma associação entre ponto de consumo e medidor
	 * ou corretor de vazão.
	 *
	 * @param associacao
	 * @param bindingResult
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("salvarAssociacao")
	public String salvarAssociacao(AssociacaoVO associacao, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {
		
		saveToken(request);
		String tela = null;

		try {

			Map<String, Object> dados = obterDadosAssociacao(associacao);
			dados.put("dadosAuditoria", getDadosAuditoria(request));

			if (dados.containsKey(TIPO_ASSOCIACAO)) {
				obterDadosAssociacaoPontoConsumoMedidor(dados, associacao);
				obterDadosAssociacaoPontoConsumoCorretorVazao(dados, associacao);
				if (controladorMedidor.isAssociacaoPontoConsumoMedidor((Integer) dados.get(TIPO_ASSOCIACAO))) {
					Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);
					if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO
							|| idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO
							|| idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_ATIVACAO) {
						FaixaPressaoFornecimento faixaPressaoFornecimento = null;
						if ((Long) dados.get(PRESSAO_FORNECIMENTO_CAMPO) != null) {
							faixaPressaoFornecimento = (FaixaPressaoFornecimento) controladorMedidor.obter(
									(Long) dados.get(PRESSAO_FORNECIMENTO_CAMPO), FaixaPressaoFornecimentoImpl.class);
							contratoPontoConsumo.setMedidaPressao(faixaPressaoFornecimento.getMedidaMinimo());
							controladorContrato.atualizarContratoPontoConsumo(contratoPontoConsumo);
						}
					}
					PontoConsumo pontoConsumo = controladorMedidor.inserirDadosAssociacaoPontoConsumoMedidor(dados);
					controladorPontoConsumo.atualizarSaveOrUpdate(pontoConsumo, PontoConsumoImpl.class);
					
				} else if (controladorMedidor
						.isAssociacaoPontoConsumoCorretorVazao((Integer) dados.get(TIPO_ASSOCIACAO))) {
					controladorMedidor.validarDadosAssociacaoPontoConsumoCorretorVazao(dados);
					controladorMedidor.inserirDadosAssociacaoPontoConsumoCorretorVazao(dados);
				}
			}
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession()
					.getAttribute("indicadorAtualizacaoCadastral");
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession()
						.getAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL);
				model.addAttribute("indicadorAtualizacaoCadastralImediata", indicadorAtualizacaoCadastralImediata);
				model.addAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL, urlRetornoAtualizacaoCastral);

			}

			mensagemSucesso(model, Constantes.SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_CORRETOR_VAZAO, request);

			tela = pesquisarAssociacao(associacao, bindingResult, request, model);

		} catch (NegocioException e) {
			configurarDadosAssociacao(associacao, model);
			model.addAttribute(ASSOCIACAO_VO, associacao);
			mensagemErroParametrizado(model, request, e);

			if (("O ponto de consumo já possui um medidor ativo.").equals(model.asMap().get("msg"))
					|| ("O ponto de consumo deve possuir um medidor instalado.").equals(model.asMap().get("msg"))) {
				return pesquisarAssociacao(associacao, bindingResult, request, model);
			}

			tela = EXIBIR_ASSOCIACAO;

		} catch (GGASException e) {
			e.printStackTrace();
			mensagemErroParametrizado(model, request, e);

			tela = exibirAssociacao(associacao, bindingResult, request, model);
		}

		return tela;

	}

	/**
	 * Método responsável por salvar uma associação em lote entre ponto de consumo e medidor
	 * ou corretor de vazão.
	 *
	 * @param associacao the AssociacaoVO
	 * @param bindingResult the bindingResult
	 * @param request the request
	 * @param model the model
	 * @return String
	 * @throws GGASException the GGASException
	 */
	@RequestMapping("salvarAssociacaoLote")
	public String salvarAssociacaoLote(AssociacaoVO associacao, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);
		String tela = null;
		try {
			inserirAssociacoes(associacao, request);

			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession()
					.getAttribute("indicadorAtualizacaoCadastral");

			if (indicadorAtualizacaoCadastralImediata != null) {

				String urlRetornoAtualizacaoCastral = (String) request.getSession()
						.getAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL);

				model.addAttribute("indicadorAtualizacaoCadastralImediata", indicadorAtualizacaoCadastralImediata);
				model.addAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL, urlRetornoAtualizacaoCastral);

			}

			mensagemSucesso(model, Constantes.SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_CORRETOR_VAZAO, request);
			associacao.setIndicadorAssociacaoEmLote(true);
			associacao.setExibirDadosFiltro(false);
			tela = pesquisarAssociacao(associacao, bindingResult, request, model);

		} catch (GGASException e) {
			configurarDadosAssociacao(associacao, model);
			List<PontoConsumo> listaFilhos =
					controladorPontoConsumo.listarPontoDeConsumoFilhoDeImovelCondominio(associacao.getIdImovel());
			preencherDadosAssociacaoParaRetorno(associacao, model);
			boolean operacaoCorretorVazao = controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(associacao.getTipoAssociacao());
			if (operacaoCorretorVazao) {
				associacao.setDataCorretorVazao(associacao.getDataMedidor());
				associacao.setLocalInstalacaoCorretorVazao(associacao.getLocalInstalacaoMedidor());
				associacao.setFuncionarioCorretorVazao(associacao.getFuncionario());
				associacao.setLeituraAtualCorretorVazao(associacao.getLeituraAtual());
			}
			Imovel imovel = (Imovel) controladorImovel.obter(associacao.getIdImovel(),
					QUADRA_FACE, QUADRA_FACE_ENDERECO, QUADRA_FACE_ENDERECO_CEP, QUADRA);
			model.addAttribute(DESCRICAO_IMOVEL, imovel.getNome());
			model.addAttribute(ENDERECO_IMOVEL, imovel.getEnderecoFormatado());
			if (imovel.getQuadraFace().getEndereco().getCep().getCep() != null) {
				model.addAttribute(CEP_IMOVEL, imovel.getQuadraFace().getEndereco().getCep().getCep());
			}
			if (imovel.getDescricaoComplemento() != null) {
				model.addAttribute(COMPLEMENTO_IMOVEL, imovel.getDescricaoComplemento());
			}
			model.addAttribute(LISTA_PONTO_CONSUMO_FILHO, listaFilhos);
			model.addAttribute(QUANTIDADE_PONTO_CONSUMO_FILHO, listaFilhos.size());
			model.addAttribute(ASSOCIACAO_VO, associacao);
			mensagemErroParametrizado(model, request, e);
			tela = EXIBIR_ASSOCIACAO_LOTE;
		}
		return tela;
	}

	/**
	 * Insere as associacoes em lote
	 * @param associacao the associacao
	 * @param request the request
	 * @throws GGASException the GGASException
	 */
	public void inserirAssociacoes(AssociacaoVO associacao, HttpServletRequest request) throws GGASException {
		int listaPontoConsumoIndex = 0;
		for (Long idPontoConsumo : associacao.getIdPontoConsumoLote()) {
			associacao.setIdPontoConsumo(idPontoConsumo);
			associacao.setChaveMedidorAtual(associacao.getChaveMedidorLote()[listaPontoConsumoIndex]);
			associacao.setChaveMedidorAnterior(associacao.getChaveMedidorLote()[listaPontoConsumoIndex]);
			String[] leitura = associacao.getLeituraLote();
			if (leitura.length == 0) {
				associacao.setLeituraAtual("");
			} else {
				associacao.setLeituraAtual(leitura[listaPontoConsumoIndex]);
			}
			if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(associacao.getTipoAssociacao())) {
				associacao.setDataCorretorVazao(associacao.getDataMedidor());
				associacao.setLocalInstalacaoCorretorVazao(associacao.getLocalInstalacaoMedidor());
				associacao.setFuncionarioCorretorVazao(associacao.getFuncionario());
				associacao.setLeituraAtualCorretorVazao(associacao.getLeituraAtual());
				Long[] chaveCorretorVazao = associacao.getChaveCorretorVazaoLote();
				if (chaveCorretorVazao != null && chaveCorretorVazao.length > 0) {
					associacao.setChaveCorretorVazaoAtual(chaveCorretorVazao[listaPontoConsumoIndex]);
				}
			}
			listaPontoConsumoIndex++;

			Map<String, Object> dados = obterDadosAssociacao(associacao);
			dados.put("dadosAuditoria", getDadosAuditoria(request));

			if (dados.containsKey(TIPO_ASSOCIACAO)) {

				obterDadosAssociacaoPontoConsumoMedidor(dados, associacao);
				obterDadosAssociacaoPontoConsumoCorretorVazao(dados, associacao);

				if (controladorMedidor.isAssociacaoPontoConsumoMedidor((Integer) dados.get(TIPO_ASSOCIACAO))) {

					controladorMedidor.inserirDadosAssociacaoPontoConsumoMedidor(dados);

				} else if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao((Integer) dados.get(TIPO_ASSOCIACAO))) {

					controladorMedidor.validarDadosAssociacaoPontoConsumoCorretorVazao(dados);
					controladorMedidor.inserirDadosAssociacaoPontoConsumoCorretorVazao(dados);
				}
			}
		}
	}

	/**
	 * Preenche os dados de associacao para retorno
	 * @param associacao the associacao
	 * @param model the model
	 * @throws NegocioException the NegocioException
	 */
	public void preencherDadosAssociacaoParaRetorno(AssociacaoVO associacao, Model model)
			throws NegocioException {
		Map<Long, Long> pontoConsumoMarcado = new HashMap<>();
		Map<Long, Long> idMedidorMarcado = new HashMap<>();
		Map<Long, String> numMedidorMarcado = new HashMap<>();
		Map<Long, String> digitoMedidorMarcado = new HashMap<>();
		Map<Long, String> leituraMarcada = new HashMap<>();
		Map<Long, Long> idMedidorCorretorVazaoMarcado = new HashMap<>();
		Map<Long, String> numMedidorCorretorVazaoMarcado = new HashMap<>();
		Map<Long, String> digitoMedidorCorretorVazaoMarcado = new HashMap<>();
		int indiceDados = 0;
		Long[] idsPontoConsumo = associacao.getIdPontoConsumoLote();
		Long[] idsMedidor = associacao.getChaveMedidorLote();
		String[] leituras = associacao.getLeituraLote();
		Long[] chaveCorretorVazao = associacao.getChaveCorretorVazaoLote();
		boolean operacaoCorretorVazao = controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(associacao.getTipoAssociacao());
		for (Long idPontoConsumo: idsPontoConsumo) {
			pontoConsumoMarcado.putIfAbsent(idPontoConsumo, idPontoConsumo);
			idMedidorMarcado.putIfAbsent(idPontoConsumo, idsMedidor[indiceDados]);
			Medidor medidor = controladorMedidor.obterMedidor(idsMedidor[indiceDados]);
			numMedidorMarcado.putIfAbsent(idPontoConsumo, medidor.getNumeroSerie());
			digitoMedidorMarcado.putIfAbsent(idPontoConsumo, StringUtils.repeat(DIGITO_NOVE,medidor.getDigito()));
			leituraMarcada.putIfAbsent(idPontoConsumo, leituras[indiceDados]);
			if (operacaoCorretorVazao && chaveCorretorVazao != null && chaveCorretorVazao.length > 0) {
				VazaoCorretor corretorVazao = (VazaoCorretor) controladorCorretorVazao.obter(chaveCorretorVazao[indiceDados]);
				idMedidorCorretorVazaoMarcado.putIfAbsent(idPontoConsumo, chaveCorretorVazao[indiceDados]);
				numMedidorCorretorVazaoMarcado.putIfAbsent(idPontoConsumo, corretorVazao.getNumeroSerie());
				digitoMedidorCorretorVazaoMarcado.putIfAbsent(idPontoConsumo, StringUtils.repeat(DIGITO_NOVE,
						corretorVazao.getNumeroDigitos()));
			}
			indiceDados++;
		}
		model.addAttribute(PONTOS_MARCADOS, pontoConsumoMarcado);
		model.addAttribute(LEITURA_MARCADA, leituraMarcada);
		model.addAttribute(ID_MEDIDOR_MARCADO, idMedidorMarcado);
		model.addAttribute(NUM_MEDIDOR_MARCADO, numMedidorMarcado);
		model.addAttribute(DIGITO_MEDIDOR_MARCADO, digitoMedidorMarcado);
		model.addAttribute(ID_MEDIDOR_CORRETOR_VAZAO_MARCADO, idMedidorCorretorVazaoMarcado);
		model.addAttribute(NUM_MEDIDOR_CORRETOR_VAZAO_MARCADO, numMedidorCorretorVazaoMarcado);
		model.addAttribute(DIGITO_MEDIDOR_CORRETOR_VAZAO_MARCADO, digitoMedidorCorretorVazaoMarcado);
	}

	/**
	 * Método responsável por salvar uma associação medidor independente e corretor
	 * de vazão.
	 *
	 * @param associacao
	 * @param bindingResult
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("salvarAssociacaoMedidorIndependente")
	public String salvarAssociacaoMedidorIndependente(AssociacaoVO associacao, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		bindingResult.getObjectName();

		saveToken(request);

		String tela = "forward:exibirAssociacaoIndependente";

		try {

			Map<String, Object> dados = obterDadosAssociacao(associacao);
			dados.put("dadosAuditoria", getDadosAuditoria(request));

			if (dados.containsKey(TIPO_ASSOCIACAO)) {
				obterDadosAssociacaoPontoConsumoMedidor(dados, associacao);
				obterDadosAssociacaoPontoConsumoCorretorVazao(dados, associacao);

				if (controladorMedidor.isAssociacaoPontoConsumoMedidor((Integer) dados.get(TIPO_ASSOCIACAO))) {
					controladorMedidor.inserirDadosAssociacaoMedidorIndependente(dados);
				} else if (controladorMedidor
						.isAssociacaoPontoConsumoCorretorVazao((Integer) dados.get(TIPO_ASSOCIACAO))) {
					controladorMedidor.inserirDadosAssociacaoCorretorVazaoMedidorIndependente(dados);
				}
			}

			mensagemSucesso(model, Constantes.SUCESSO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR_CORRETOR_VAZAO, request);

			model.addAttribute(ID_MEDIDOR_INDEPENDENTE, dados.get(ID_MEDIDOR_INDEPENDENTE));

			tela = pesquisarMedidorAssociacao(associacao, request, model);

		} catch (NegocioException e) {
			model.addAttribute(ASSOCIACAO_VO, associacao);
			mensagemErroParametrizado(model, request, e);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;

	}

	/**
	 * Método responsável por exibir a tela de associação entre ponto de consumo e
	 * medidor ou corretor de vazão.
	 *
	 * @param associacao
	 * @param bindingResult
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_ASSOCIACAO)
	public String exibirAssociacao(AssociacaoVO associacao, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);

		String tela = null;

		try {

			Map<String, Object> dados = obterDadosAssociacao(associacao);

			if (controladorMedidor.isAssociacaoPontoConsumoMedidor((Integer) associacao.getTipoAssociacao())) {
				controladorMedidor.validarDadosAssociacaoMedidor(dados);
			} else if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao((Integer) dados.get(TIPO_ASSOCIACAO))) {
				controladorMedidor.validarDadosAssociacaoCorretorVazao(dados);
			}

			configurarDadosAssociacao(associacao, model);

			model.addAttribute(ASSOCIACAO_VO, associacao);

			tela = EXIBIR_ASSOCIACAO;

		} catch (GGASException e) {

			mensagemErroParametrizado(model, request, e);
			tela = pesquisarAssociacao(associacao, bindingResult, request, model);

		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de associação entre ponto de consumo e
	 * medidor ou corretor de vazão.
	 *
	 * @param associacao the associacao
	 * @param bindingResult the bindingResult
	 * @param request the request
	 * @param model the model
	 * @return String
	 * @throws GGASException the GGASException
	 */
	@RequestMapping(EXIBIR_ASSOCIACAO_LOTE)
	public String exibirAssociacaoLote(AssociacaoVO associacao, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {
		saveToken(request);
		String tela;
		try {
			Map<String, Object> dados = obterDadosAssociacao(associacao);
			controladorMedidor.validarDadosAssociacaoMedidorLote(dados);
			configurarDadosAssociacaoLote(associacao, model);

			List<PontoConsumo> listaFilhos =
					controladorPontoConsumo.listarPontoDeConsumoFilhoDeImovelCondominio(associacao.getIdImovel());
			model.addAttribute(LISTA_PONTO_CONSUMO_FILHO, listaFilhos);
			model.addAttribute(ASSOCIACAO_VO, associacao);
			model.addAttribute(QUANTIDADE_PONTO_CONSUMO_FILHO, listaFilhos.size());

			tela = EXIBIR_ASSOCIACAO_LOTE;

		} catch (GGASException e) {

			mensagemErroParametrizado(model, request, e);
			tela = pesquisarAssociacao(associacao, bindingResult, request, model);

		}

		return tela;
	}

	/**
	 * Método responsável por configurar os dados da associacao.
	 *
	 * @param associacao the associacao
	 * @param model the model
	 * @throws GGASException the GGASException
	 */
	private void configurarDadosAssociacao(AssociacaoVO associacao, Model model)
			throws GGASException {

		Map<Integer, String> tipoAssociacao = controladorMedidor.obterTiposAssociacoes();

		Integer codigoTipoAssociacao = associacao.getTipoAssociacao();
		Long codigoOperacaoMedidor = associacao.getIdOperacaoMedidor();
		OperacaoMedidor operacaoMedidor = controladorMedidor.obterOperacaoMedidor(codigoOperacaoMedidor);

		if (associacao.getIndicadorAssociacaoEmLote() != null && !associacao.getIndicadorAssociacaoEmLote()) {
			configurarDadosPontoConsumo(associacao, model);
		}
		model.addAttribute(TITULO_ASSOCIACAO,
				operacaoMedidor.getDescricao() + " - " + tipoAssociacao.get(codigoTipoAssociacao));

		if (controladorMedidor.isAssociacaoPontoConsumoMedidor(codigoTipoAssociacao)) {
			configurarDadosAssociacaoMedidor(associacao, codigoOperacaoMedidor, model);
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR, true);
		} else if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(codigoTipoAssociacao)) {
			configurarDadosAssociacaoPontoConsumoCorretorVazao(associacao, model);
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO, true);
		}

		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));

	}

	/**
	 * Método responsável por configurar os dados da associacao.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoLote(AssociacaoVO associacao, Model model)
			throws GGASException {

		Map<Integer, String> tipoAssociacao = controladorMedidor.obterTiposAssociacoes();

		Integer codigoTipoAssociacao = associacao.getTipoAssociacao();
		Long codigoOperacaoMedidor = associacao.getIdOperacaoMedidor();
		OperacaoMedidor operacaoMedidor = controladorMedidor.obterOperacaoMedidor(codigoOperacaoMedidor);

		configurarDadosImovel(associacao, model);
		model.addAttribute(TITULO_ASSOCIACAO,
				operacaoMedidor.getDescricao() + " - " + tipoAssociacao.get(codigoTipoAssociacao));

		if (controladorMedidor.isAssociacaoPontoConsumoMedidor(codigoTipoAssociacao)) {
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR, true);
			configurarDadosAssociacaoLoteMedidor(associacao, codigoOperacaoMedidor, model);
		} else if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(codigoTipoAssociacao)) {
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO, true);
		}

		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));

		configurarDadosAssociacaoPontoConsumoCorretorVazao(associacao, model);
	}

	/**
	 * Método responsável por configurar dados associacao lote medidor.
	 *
	 * @param associacao
	 * @param codigoOperacaoMedidor
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoLoteMedidor(AssociacaoVO associacao, Long codigoOperacaoMedidor, Model model)
			throws GGASException {

		if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_INSTALACAO) {
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO);
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_ATIVACAO) {
			configurarDadosMedidorAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(DADOS, DADOS_ATIVACAO);
			configurarDatasIntalacaoMedidor(associacao, model);
		} else {
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO_ATIVACAO);
		}
	}

	/**
	 * Método responsável por configurar dados associacao medidor.
	 *
	 * @param associacao
	 * @param codigoOperacaoMedidor
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoMedidor(AssociacaoVO associacao, Long codigoOperacaoMedidor, Model model)
			throws GGASException {

		 if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_INSTALACAO) {
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO);
			if(associacao.getIdPontoConsumo() != null){
				model.addAttribute(CONTRATO_PONTO_CONSUMO, consultarContratoPontoConsumoPorPontoConsumo(associacao.getIdPontoConsumo()));
				model.addAttribute(CONTRATO_FORMATADO, formatarContrato(associacao));
			}
			if (contratoPontoConsumo != null && associacao.getIdPontoConsumo() != null ) {
				model.addAttribute(LISTA_PRESSAO_FORNECIMENTO_CAMPO, listarPressaoFornecimentoCampo(associacao.getIdPontoConsumo()));
			}
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			configurarDadosMedidorAnterior(associacao, model);
			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_SUBSTITUICAO);
			configurarDatasIntalacaoMedidor(associacao, model);
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_ATIVACAO) {
			configurarDadosMedidorAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(DADOS, DADOS_ATIVACAO);
			configurarDatasIntalacaoMedidor(associacao, model);
			if(associacao.getIdPontoConsumo() != null){
				model.addAttribute(CONTRATO_PONTO_CONSUMO, consultarContratoPontoConsumoPorPontoConsumo(associacao.getIdPontoConsumo()));
				model.addAttribute(CONTRATO_FORMATADO, formatarContrato(associacao));
			}
			if (contratoPontoConsumo != null && associacao.getIdPontoConsumo() != null ) {
				model.addAttribute(LISTA_PRESSAO_FORNECIMENTO_CAMPO, listarPressaoFornecimentoCampo(associacao.getIdPontoConsumo()));
			}
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_RETIRADA) {
			configurarDadosMedidorAnterior(associacao, model);
			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_RETIRADA);
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_BLOQUEIO) {
			configurarDadosMedidorAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_BLOQUEIO);
		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_REATIVACAO) {
			configurarDadosMedidorAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_REATIVACAO);
		}else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO) {
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO_ATIVACAO);
			if(associacao.getIdPontoConsumo() != null){
				model.addAttribute(CONTRATO_PONTO_CONSUMO, consultarContratoPontoConsumoPorPontoConsumo(associacao.getIdPontoConsumo()));
				model.addAttribute(CONTRATO_FORMATADO, formatarContrato(associacao));
			}
			if (contratoPontoConsumo != null && associacao.getIdPontoConsumo() != null ) {
				model.addAttribute(LISTA_PRESSAO_FORNECIMENTO_CAMPO, listarPressaoFornecimentoCampo(associacao.getIdPontoConsumo()));
			}
		} else if(codigoOperacaoMedidor == OperacaoMedidor.CODIGO_BLOQUEIO_REATIVACAO) {
			configurarDadosMedidorAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_BLOQUEIO_REATIVACAO);
		}
	}

	private List<FaixaPressaoFornecimento> listarPressaoFornecimentoCampo(Long idPontoConsumo){
		return controladorMedidor.listarPressaoFornecimentoCampo(idPontoConsumo);
	}
	
	private ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long idPontoConsumo){
		
		  contratoPontoConsumo = controladorMedidor.consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
		return contratoPontoConsumo;
	}
	
	private String formatarContrato(AssociacaoVO associacao) {
		StringBuilder numeroFormatado = new StringBuilder();
		ContratoPontoConsumo cpc = consultarContratoPontoConsumoPorPontoConsumo(associacao.getIdPontoConsumo());
		if (cpc != null) {
			ContratoPontoConsumo valor = controladorMedidor.consultarContratoPorContratoPontoConsumo(associacao.getIdPontoConsumo());
			if (valor != null && valor.getContrato() != null) {
				if (valor.getContrato().getAnoContrato() != null) {
					numeroFormatado.append(valor.getContrato().getAnoContrato().toString());
				}
				if (valor.getContrato().getNumero() != null) {
					numeroFormatado.append(String.format("%05d", valor.getContrato().getNumero()));
					numeroFormatado.append("-");
				}
				if (valor.getContrato().getNumeroAditivo() != null) {
					numeroFormatado.append(String.format("%02d", valor.getContrato().getNumeroAditivo()));
				}
			} else {
				numeroFormatado.append("");
			}
		}
		return numeroFormatado.toString();
	}
	
	/**
	 * Configurar dados associacao ponto consumo corretor vazao.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoPontoConsumoCorretorVazao(AssociacaoVO associacao, Model model)
			throws GGASException {

		long codigoOperacaoMedidor = associacao.getIdOperacaoMedidor();

		if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_INSTALACAO) {

			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO);

		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_SUBSTITUICAO) {

			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_SUBSTITUICAO);

		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_RETIRADA) {

			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_RETIRADA);

		}

	}

	/**
	 * Método responsável por configurar dados associacao corretor vazao medidor
	 * independente.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoCorretorVazaoMedidorIndependente(AssociacaoVO associacao, Model model)
			throws GGASException {

		long codigoOperacaoMedidor = (Long) associacao.getIdOperacaoMedidorIndependente();

		if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_INSTALACAO) {

			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_INSTALACAO);

		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_SUBSTITUICAO) {

			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_MEDIDOR_LOCAL_INSTALACAO, controladorMedidor.listarMedidorLocalInstalacao());
			model.addAttribute(LISTA_MEDIDOR_PROTECAO, controladorMedidor.listarMedidorProtecao());
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_SUBSTITUICAO);

		} else if (codigoOperacaoMedidor == OperacaoMedidor.CODIGO_RETIRADA) {

			configurarDadosCorretorVazaoAnterior(associacao, model);
			model.addAttribute(LISTA_FUNCIONARIO, obterFuncionarios());
			model.addAttribute(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
			model.addAttribute(DADOS, DADOS_RETIRADA);

		}

	}

	/**
	 * Método responsável por configurar dados ponto consumo.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosPontoConsumo(AssociacaoVO associacao, Model model) throws GGASException {

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(associacao.getIdPontoConsumo(),
				"imovel", "cep", "instalacaoMedidor", QUADRA_FACE);

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();

		model.addAttribute(DESCRICAO_PONTO_CONSUMO, pontoConsumo.getDescricao());
		model.addAttribute(ENDERECO_PONTO_CONSUMO, pontoConsumo.getEnderecoFormatado());

		if (pontoConsumo.getCep() != null) {
			model.addAttribute(CEP_PONTO_CONSUMO, pontoConsumo.getCep().getCep());
		} else {
			model.addAttribute(CEP_PONTO_CONSUMO, "");
		}
		if (instalacaoMedidor == null) {
			associacao.setContemCorretorVazao(false);
		} else {
			associacao.setContemCorretorVazao(pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null);
		}
		if (instalacaoMedidor == null) {
			associacao.setContemCorretorVazao(false);
		} else {
			associacao.setContemCorretorVazao(pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null);
		}
		if (pontoConsumo.getDescricaoComplemento() != null) {
			model.addAttribute(COMPLEMENTO_PONTO_CONSUMO, pontoConsumo.getDescricaoComplemento());
		} else {
			model.addAttribute(COMPLEMENTO_PONTO_CONSUMO, "");
		}

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato
				.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

		if (contratoPontoConsumo != null) {
			String pressao = "";

			if (contratoPontoConsumo.getMedidaPressao() != null) {
				pressao = Util.converterCampoCurrencyParaString(contratoPontoConsumo.getMedidaPressao(),
						Constantes.LOCALE_PADRAO);
			}

			if (contratoPontoConsumo.getUnidadePressao() != null) {
				pressao = pressao + " / " + contratoPontoConsumo.getUnidadePressao().getDescricao();
			}

			model.addAttribute(PRESSAO, pressao);

			if (contratoPontoConsumo.getNumeroFatorCorrecao() != null) {
				model.addAttribute(FATOR_CORRECAO, Util.converterCampoCurrencyParaString(
						contratoPontoConsumo.getNumeroFatorCorrecao(), Constantes.LOCALE_PADRAO));
			}
		}
	}

	/**
	 * Método responsável por configurar dados do imóvel.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosImovel(AssociacaoVO associacao, Model model) throws GGASException {

		Imovel imovel = (Imovel) controladorImovel.obter(associacao.getIdImovel(), QUADRA_FACE, QUADRA_FACE_ENDERECO,
				QUADRA_FACE_ENDERECO_CEP, QUADRA);

		model.addAttribute(DESCRICAO_IMOVEL, imovel.getNome());
		model.addAttribute(ENDERECO_IMOVEL, imovel.getEnderecoFormatado());

		if (imovel.getQuadraFace().getEndereco().getCep().getCep() != null) {
			model.addAttribute(CEP_IMOVEL, imovel.getQuadraFace().getEndereco().getCep().getCep());
		} else {
			model.addAttribute(CEP_IMOVEL, "");
		}
		if (imovel.getDescricaoComplemento() != null) {
			model.addAttribute(COMPLEMENTO_IMOVEL, imovel.getDescricaoComplemento());
		} else {
			model.addAttribute(COMPLEMENTO_IMOVEL, "");
		}
	}

	/**
	 * Método responsável por configurar dados pesquisa associacao.
	 *
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosPesquisaAssociacao(Model model) throws GGASException {

		model.addAttribute(LISTA_TIPO_ASSOCIACAO, controladorMedidor.obterTiposAssociacoes());
		model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());

	}

	/**
	 * Método responsável por configurar dados medidor anterior.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosMedidorAnterior(AssociacaoVO associacao, Model model) throws GGASException {

		InstalacaoMedidor instalacaoMedidor = obtemInstalacaoMedidor(associacao);

		if (instalacaoMedidor != null && instalacaoMedidor.getMedidor() != null) {
			Medidor medidor = instalacaoMedidor.getMedidor();

			model.addAttribute(ID_MEDIDOR_ANTERIOR, medidor.getChavePrimaria());
			model.addAttribute(NUMERO_SERIE_MEDIDOR_ANTERIOR, "" + medidor.getNumeroSerie());
			if (medidor.getTipoMedidor() != null) {
				model.addAttribute(TIPO_MEDIDOR_ANTERIOR, medidor.getTipoMedidor().getDescricao());
			} else {
				model.addAttribute(TIPO_MEDIDOR_ANTERIOR, "");
			}
			if (medidor.getModelo() != null) {
				model.addAttribute(MODELO_MEDIDOR_ANTERIOR, medidor.getModelo().getDescricao());
			} else {
				model.addAttribute(MODELO_MEDIDOR_ANTERIOR, "");
			}
			if (medidor.getMarcaMedidor() != null) {
				model.addAttribute(MARCA_MEDIDOR_ANTERIOR, medidor.getMarcaMedidor().getDescricao());
			} else {
				model.addAttribute(MARCA_MEDIDOR_ANTERIOR, "");
			}
			if (medidor.getDigito() != null) {
				model.addAttribute(NUMERO_DIGITOS_MEDIDOR_ANTERIOR, medidor.getDigito());
			} else {
				model.addAttribute(NUMERO_DIGITOS_MEDIDOR_ANTERIOR, 0);
			}

			String chaveModoUsoIndependente = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE);
			String chaveModoUsoVirtual = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL);

			if (medidor.getModoUso() != null) {
				if (chaveModoUsoIndependente.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
					model.addAttribute(MODO_USO, "Independente");
				} else if (chaveModoUsoVirtual.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
					model.addAttribute(MODO_USO, "Virtual");
				} else {
					model.addAttribute(MODO_USO, "Normal");
				}
			} else {
				model.addAttribute(MODO_USO, "Normal");
			}
		}

	}

	/**
	 * Método responsável por configurar dados corretor vazao anterior.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosCorretorVazaoAnterior(AssociacaoVO associacao, Model model) throws GGASException {

		InstalacaoMedidor instalacaoMedidor = obtemInstalacaoMedidor(associacao);

		if (instalacaoMedidor != null && instalacaoMedidor.getVazaoCorretor() != null) {

			associacao.setContemCorretorVazao(true);
			model.addAttribute(ID_CORRETOR_VAZAO_ANTERIOR, instalacaoMedidor.getVazaoCorretor().getChavePrimaria());
			model.addAttribute(NUMERO_SERIE_CORRETOR_VAZAO_ANTERIOR,
					"" + instalacaoMedidor.getVazaoCorretor().getNumeroSerie());
			associacao.setChaveCorretorVazaoAnterior(instalacaoMedidor.getVazaoCorretor().getChavePrimaria());

			if (instalacaoMedidor.getVazaoCorretor().getModelo() != null) {
				model.addAttribute(MODELO_CORRETOR_VAZAO_ANTERIOR,
						instalacaoMedidor.getVazaoCorretor().getModelo().getDescricao());
			} else {
				model.addAttribute(MODELO_CORRETOR_VAZAO_ANTERIOR, "");
			}
			if (instalacaoMedidor.getVazaoCorretor().getMarcaCorretor() != null) {
				model.addAttribute(MARCA_CORRETOR_VAZAO_ANTERIOR,
						instalacaoMedidor.getVazaoCorretor().getMarcaCorretor().getDescricao());
			} else {
				model.addAttribute(MARCA_CORRETOR_VAZAO_ANTERIOR, "");
			}

			if (instalacaoMedidor.getVazaoCorretor().getNumeroDigitos() != null) {
				model.addAttribute(NUMERO_DIGITOS_CORRETOR_VAZAO_ANTERIOR,
						instalacaoMedidor.getVazaoCorretor().getNumeroDigitos());
			} else {
				model.addAttribute(NUMERO_DIGITOS_CORRETOR_VAZAO_ANTERIOR, 0);
			}
		}

	}

	/**
	 * Método responsável por obter instalação medidor.
	 *
	 * @param associacao
	 * @return instalacaoMedidor {@link InstalacaoMedidor}
	 * @throws GGASException
	 */
	private InstalacaoMedidor obtemInstalacaoMedidor(AssociacaoVO associacao) throws GGASException {

		InstalacaoMedidor instalacaoMedidor = null;

		Boolean isAssociacaoIndependente = associacao.getIsAssociacaoMedidorIndependente();

		if (isAssociacaoIndependente != null && isAssociacaoIndependente) {
			Medidor medidor = controladorMedidor.obterMedidor(associacao.getIdMedidorIndependente());
			instalacaoMedidor = medidor.getInstalacaoMedidor();
		} else if (associacao.getIdPontoConsumo() != null) {
			instalacaoMedidor = controladorMedidor
					.obterInstalacaoMedidorPontoConsumo((Long) associacao.getIdPontoConsumo());
		}

		return instalacaoMedidor;
	}

	/**
	 * configurar dados filtro pesquisa associacao.
	 *
	 * @param filtro
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosFiltroPesquisaAssociacao(Map<String, Object> filtro, Model model) throws GGASException {

		if (filtro != null && filtro.get(ID_CLIENTE) != null) {

			Cliente cliente = controladorCliente.obterCliente((Long) filtro.get(ID_CLIENTE), "enderecos");
			model.addAttribute(ID_CLIENTE, cliente.getChavePrimaria());
			model.addAttribute(NOME_CLIENTE, cliente.getNome());
			model.addAttribute(DOCUMENTO_CLIENTE, cliente.getNumeroDocumentoFormatado());
			if (cliente.getEmailPrincipal() != null) {
				model.addAttribute(EMAIL_CLIENTE, cliente.getEmailPrincipal());
			} else {
				model.addAttribute(EMAIL_CLIENTE, "");
			}
			if (cliente.getEnderecoPrincipal() != null) {
				model.addAttribute(ENDERECO_CLIENTE, cliente.getEnderecoPrincipal().getEnderecoFormatado());
			} else {
				model.addAttribute(ENDERECO_CLIENTE, "");
			}
			model.addAttribute(INDICADOR_PESQUISA, "indicadorPesquisaCliente");

		} else if (filtro != null && filtro.get(ID_IMOVEL) != null) {

			Imovel imovel = (Imovel) controladorImovel.obter((Long) filtro.get(ID_IMOVEL), QUADRA_FACE_ENDERECO_CEP);
			model.addAttribute(ID_IMOVEL, imovel.getChavePrimaria());

			if (imovel.getNome() != null) {
				model.addAttribute(NOME_FANTASIA_IMOVEL, imovel.getNome());
			} else {
				model.addAttribute(NOME_FANTASIA_IMOVEL, "");
			}

			if (imovel.getNome() != null) {
				model.addAttribute(NOME_FANTASIA_IMOVEL, imovel.getNome());
			} else {
				model.addAttribute(NOME_FANTASIA_IMOVEL, "");
			}
			model.addAttribute(MATRICULA_IMOVEL, String.valueOf(imovel.getChavePrimaria()));
			if (imovel.getNumeroImovel() != null) {
				model.addAttribute(NUMERO_IMOVEL, imovel.getNumeroImovel());
			} else {
				model.addAttribute(NUMERO_IMOVEL, "");
			}
			if (imovel.getQuadraFace().getEndereco().getCep().getMunicipio() != null) {
				model.addAttribute(CIDADE_IMOVEL,
						imovel.getQuadraFace().getEndereco().getCep().getMunicipio().getDescricao());
			} else {
				model.addAttribute(CIDADE_IMOVEL, "");
			}
			if (imovel.getCondominio() != null) {
				model.addAttribute(CONDOMINIO_IMOVEL, String.valueOf(imovel.getCondominio()));
			} else {
				model.addAttribute(CONDOMINIO_IMOVEL, "");
			}
			model.addAttribute(INDICADOR_PESQUISA, "indicadorPesquisaImovel");

		}

		if (filtro != null && filtro.get(NUMERO_MEDIDOR) != null) {
			model.addAttribute(NUMERO_MEDIDOR, filtro.get(NUMERO_MEDIDOR));
		}

		if (filtro != null && filtro.get(NUMERO_CORRETOR_VAZAO) != null) {
			model.addAttribute(NUMERO_CORRETOR_VAZAO, filtro.get(NUMERO_CORRETOR_VAZAO));
		}

	}

	/**
	 * Obter dados associacao.
	 *
	 * @param associacao
	 * @return dados
	 * @throws GGASException
	 */
	private Map<String, Object> obterDadosAssociacao(AssociacaoVO associacao) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();

		if (associacao.getIdPontoConsumo() != null && associacao.getIdPontoConsumo() > 0) {
			dados.put(ID_PONTO_CONSUMO, associacao.getIdPontoConsumo());
		}

		if (associacao.getIdImovel() != null && associacao.getIdImovel() > 0) {
			dados.put(ID_IMOVEL, associacao.getIdImovel());
		}

		if (associacao.getIdOperacaoMedidor() != null && associacao.getIdOperacaoMedidor() > 0) {
			dados.put(ID_OPERACAO_MEDIDOR, associacao.getIdOperacaoMedidor());
		}

		if (associacao.getIdOperacaoMedidorIndependente() != null
				&& associacao.getIdOperacaoMedidorIndependente() > 0) {
			dados.put(ID_OPERACAO_MEDIDOR_INDEPENDENTE, associacao.getIdOperacaoMedidorIndependente());
		}

		if (associacao.getIdMedidorIndependente() != null && associacao.getIdMedidorIndependente() > 0) {
			dados.put(ID_MEDIDOR_INDEPENDENTE, associacao.getIdMedidorIndependente());
		}

		if (associacao.getTipoAssociacao() != null && associacao.getTipoAssociacao() > 0) {
			dados.put(TIPO_ASSOCIACAO, associacao.getTipoAssociacao());
		}

		if (!StringUtils.isEmpty(associacao.getModoUso())) {
			dados.put(MODO_USO, associacao.getModoUso());
		}

		return dados;
	}

	/**
	 * Obter dados associacao ponto consumo medidor.
	 *
	 * @param dados
	 * @param associacao
	 * @return dados
	 * @throws GGASException
	 */
	private Map<String, Object> obterDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados,
			AssociacaoVO associacao) throws GGASException {

		if (associacao.getChaveMedidorAtual() != null && associacao.getChaveMedidorAtual() > 0) {
			dados.put(ID_MEDIDOR_ATUAL, associacao.getChaveMedidorAtual());
		}

		if (associacao.getIdMedidorIndependente() != null && associacao.getIdMedidorIndependente() > 0) {
			dados.put(ID_MEDIDOR_INDEPENDENTE, associacao.getIdMedidorIndependente());
		}

		if (associacao.getIsAssociacaoMedidorIndependente() != null
				&& associacao.getIsAssociacaoMedidorIndependente()) {
			dados.put(IS_ASSOCIACAO_MEDIDOR_INDEPENDENTE, associacao.getIsAssociacaoMedidorIndependente());
		}

		String dataMedidorCorretorVazao = associacao.getDataMedidor();
		if (!StringUtils.isEmpty(dataMedidorCorretorVazao) && !"__/__/____".equals(dataMedidorCorretorVazao)) {
			Date dataMedidor = Util.converterCampoStringParaData("Data", dataMedidorCorretorVazao,
					Constantes.FORMATO_DATA_BR);
			dados.put(DATA_MEDIDOR, dataMedidor);
		}

		if (associacao.getLocalInstalacaoMedidor() != null && associacao.getLocalInstalacaoMedidor() > 0) {
			dados.put(LOCAL_INSTALACAO_MEDIDOR, associacao.getLocalInstalacaoMedidor());
		}

		if (associacao.getMedidorProtecao() != null && associacao.getMedidorProtecao() > 0) {
			dados.put(MEDIDOR_PROTECAO, associacao.getMedidorProtecao());
		}

		if (!StringUtils.isEmpty(associacao.getLeituraAtual())) {
			dados.put(LEITURA_ATUAL, associacao.getLeituraAtual());
		}

		if (associacao.getFuncionario() != null && associacao.getFuncionario() > 0) {
			dados.put(FUNCIONARIO, associacao.getFuncionario());
		}

		if (associacao.getMedidorMotivoOperacao() != null && associacao.getMedidorMotivoOperacao() > 0) {
			dados.put(MEDIDOR_MOTIVO_OPERACAO, associacao.getMedidorMotivoOperacao());
		}

		if (associacao.getChaveMedidorAnterior() != null && associacao.getChaveMedidorAnterior() > 0) {
			dados.put(ID_MEDIDOR_ANTERIOR, associacao.getChaveMedidorAnterior());
		}

		if (!StringUtils.isEmpty(associacao.getLeituraAnterior())) {
			dados.put(LEITURA_ANTERIOR, associacao.getLeituraAnterior());
		}

		if (!StringUtils.isEmpty(associacao.getLacre())) {
			dados.put(LACRE, associacao.getLacre());
		}
		
		if (!StringUtils.isEmpty(associacao.getLacreDois())) {
			dados.put(LACRE_DOIS, associacao.getLacreDois());
		}
		if (!StringUtils.isEmpty(associacao.getLacreTres())) {
			dados.put(LACRE_TRES, associacao.getLacreTres());
		}

		if (associacao.getPressaoFornecimentoCampo() != null && associacao.getPressaoFornecimentoCampo() > 0) {
			dados.put(PRESSAO_FORNECIMENTO_CAMPO, associacao.getPressaoFornecimentoCampo());
		}
		return dados;
	}

	/**
	 * Método responsável por pesquisar um Medidor.
	 *
	 * @param associacao
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarMedidorAssociacao")
	public String pesquisarMedidorAssociacao(AssociacaoVO associacao, HttpServletRequest request, Model model)
			throws GGASException {

		associacao.setModoUso(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));

		Map<String, Object> filtro = prepararFiltroMedidor(associacao, request);

		Collection<Medidor> listaMedidorIndependente = controladorMedidor.consultarMedidorIndependente(filtro);
		model.addAttribute("listaMedidorIndependente", listaMedidorIndependente);

		return exibirPesquisaAssociacao(model);
	}

	/**
	 * Preparar filtro medidor.
	 *
	 * @param associacao
	 * @param request
	 * @return filtro
	 * @throws FormatoInvalidoException
	 */
	private Map<String, Object> prepararFiltroMedidor(AssociacaoVO associacao, HttpServletRequest request)
			throws FormatoInvalidoException {

		Map<String, Object> filtro = new HashMap<>();

		if (!StringUtils.isEmpty(associacao.getNumeroSerie())) {
			filtro.put("numeroSerieParcial", associacao.getNumeroSerie().trim());
		}

		if (associacao.getIdTipo() != null && associacao.getIdTipo() > 0) {
			filtro.put(ID_TIPO, associacao.getIdTipo());
		}

		if (associacao.getIdMarca() != null && associacao.getIdMarca() > 0) {
			filtro.put(ID_MARCA, associacao.getIdMarca());
		}

		if (associacao.getIdModelo() != null && associacao.getIdModelo() > 0) {
			filtro.put(ID_MODELO, associacao.getIdModelo());
		}

		if (associacao.getHabilitado() != null) {
			filtro.put(HABILITADO, associacao.getHabilitado());
		}

		if (!StringUtils.isEmpty(associacao.getModoUso())) {
			filtro.put(MODO_USO, Long.valueOf(associacao.getModoUso()));
		}

		if (associacao.getIdMedidorIndependente() != null && associacao.getIdMedidorIndependente() > 0) {
			filtro.put(CHAVE_PRIMARIA, associacao.getIdMedidorIndependente());
		}

		Boolean isComposicaoVirtual = (Boolean) request.getAttribute(COMPOSICAO_VIRTUAL);

		if (isComposicaoVirtual != null && !isComposicaoVirtual) {
			filtro.put("InstaladorOuProntoPraInstalar", true);
		}

		return filtro;

	}

	/**
	 * Obter dados associacao ponto consumo corretor vazao.
	 *
	 * @param dados
	 * @param associacao
	 * @return dados
	 * @throws GGASException
	 */
	private Map<String, Object> obterDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados,
			AssociacaoVO associacao) throws GGASException {

		if (associacao.getChaveCorretorVazaoAtual() != null && associacao.getChaveCorretorVazaoAtual() > 0) {
			dados.put(ID_CORRETOR_VAZAO_ATUAL, associacao.getChaveCorretorVazaoAtual());
		}

		if (associacao.getChaveCorretorVazaoAnterior() != null && associacao.getChaveCorretorVazaoAnterior() > 0) {
			dados.put(ID_CORRETOR_VAZAO_ANTERIOR, associacao.getChaveCorretorVazaoAnterior());
		}

		String dataMedidorCorretorVazao = associacao.getDataCorretorVazao();

		if (!StringUtils.isEmpty(dataMedidorCorretorVazao)) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

			try {
				if (Util.isDataValida(dataMedidorCorretorVazao, Constantes.FORMATO_DATA_BR)) {
					dados.put(DATA_CORRETOR_VAZAO, df.parse(dataMedidorCorretorVazao));
				} else {
					dados.put(DATA_CORRETOR_VAZAO, "");

					throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, MSG_ERRO_CORRETOR_DATA);
				}
			} catch (ParseException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		}

		if (associacao.getLocalInstalacaoCorretorVazao() != null && associacao.getLocalInstalacaoCorretorVazao() > 0) {
			dados.put(LOCAL_INSTALACAO_CORRETOR_VAZAO, associacao.getLocalInstalacaoCorretorVazao());
		}

		if (!StringUtils.isEmpty(associacao.getLeituraAtualCorretorVazao())) {
			dados.put(LEITURA_ATUAL_CORRETOR_VAZAO, associacao.getLeituraAtualCorretorVazao());
		}

		if (!StringUtils.isEmpty(associacao.getLeituraAnteriorCorretorVazao())) {
			dados.put(LEITURA_ANTERIOR_CORRETOR_VAZAO, associacao.getLeituraAnteriorCorretorVazao());
		}

		if (!StringUtils.isEmpty(associacao.getHoraCorretorVazao())) {
			dados.put(HORA_CORRETOR_VAZAO, associacao.getHoraCorretorVazao());
		}

		if (associacao.getFuncionarioCorretorVazao() != null && associacao.getFuncionarioCorretorVazao() > 0) {
			dados.put(FUNCIONARIO_CORRETOR_VAZAO, associacao.getFuncionarioCorretorVazao());
		}

		if (associacao.getMedidorMotivoOperacao() != null && associacao.getMedidorMotivoOperacao() > 0) {
			dados.put(MEDIDOR_MOTIVO_OPERACAO, associacao.getMedidorMotivoOperacao());
		}

		Boolean instalarCorretorVazao = associacao.getInstalarCorretorVazao();
		if (associacao.getInstalarCorretorVazao() != null) {
			dados.put(INSTALAR_CORRETOR_VAZAO, instalarCorretorVazao);
		}

		if (associacao.getSubstituirCorretorVazao() != null) {
			dados.put(SUBSTITUIR_CORRETOR_VAZAO, associacao.getSubstituirCorretorVazao());
		}

		if (associacao.getContemCorretorVazao() != null) {
			dados.put(CONTEM_CORRETOR_VAZAO, associacao.getContemCorretorVazao());
		}

		return dados;
	}

	/**
	 * Obter filtro pesquisa associacao.
	 *
	 * @param associacao
	 * @return filtro
	 * @throws GGASException
	 */
	private Map<String, Object> obterFiltroPesquisaAssociacao(AssociacaoVO associacao) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (associacao.getIdCliente() != null && associacao.getIdCliente() > 0) {
			filtro.put(ID_CLIENTE, associacao.getIdCliente());
		}

		if (associacao.getIdImovel() != null && associacao.getIdImovel() > 0) {
			filtro.put(ID_IMOVEL, associacao.getIdImovel());
		}

		if (!StringUtils.isEmpty(associacao.getNumeroMedidor())) {
			filtro.put(NUMERO_MEDIDOR, associacao.getNumeroMedidor());
		}

		if (!StringUtils.isEmpty(associacao.getNumeroCorretorVazao())) {
			filtro.put(NUMERO_CORRETOR_VAZAO, associacao.getNumeroCorretorVazao());
		}

		return filtro;
	}

	/**
	 * Obter funcionarios.
	 *
	 * @return collection
	 * @throws GGASException
	 */
	private Collection<Funcionario> obterFuncionarios() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(HABILITADO, true);

		return controladorFuncionario.consultarFuncionarios(filtro);

	}

	/**
	 * Configurar datas intalacao medidor.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDatasIntalacaoMedidor(AssociacaoVO associacao, Model model) throws GGASException {

		InstalacaoMedidor instalacaoMedidor = obtemInstalacaoMedidor(associacao);

		if (instalacaoMedidor != null && instalacaoMedidor.getData() != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

			String valor = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);
			DateTime dataAtual = new DateTime();

			model.addAttribute(DATA_LIMITE_INSTALACAO, df.format(instalacaoMedidor.getData()));
			model.addAttribute(DATA_INICIO_INSTALACAO,
					df.format(dataAtual.minusYears(Integer.parseInt(valor)).toDate()));

		}

	}

	/**
	 * Método responsável por exibir a tela de associação entre medidor independente
	 * e corretor de vazão.
	 *
	 * @param associacao
	 * @param bindingResult
	 * @param request
	 * @param model
	 * @return exibirPesquisaAssociacao {@link}
	 * @throws GGASException
	 */
	@RequestMapping("exibirAssociacaoIndependente")
	public String exibirAssociacaoIndependente(AssociacaoVO associacao, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		bindingResult.getObjectName();

		saveToken(request);

		String tela = "";

		try {

			Map<String, Object> dados = obterDadosAssociacao(associacao);

			if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao((Integer) dados.get(TIPO_ASSOCIACAO))) {
				controladorMedidor.validarDadosAssociacaoCorretorVazaoMedidorIndependente(dados);
			}
			configurarDadosAssociacaoIndependente(associacao, model);

			model.addAttribute(ASSOCIACAO_VO, associacao);

			tela = EXIBIR_ASSOCIACAO;

		} catch (GGASException e) {
			tela = exibirPesquisaAssociacao(model);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Configurar dados associacao independente.
	 *
	 * @param associacao
	 * @param model
	 * @throws GGASException
	 */
	private void configurarDadosAssociacaoIndependente(AssociacaoVO associacao, Model model)
			throws GGASException {

		associacao.setIsAssociacaoMedidorIndependente(Boolean.TRUE);

		Map<Integer, String> tipoAssociacao = controladorMedidor.obterTiposAssociacoes();

		Integer codigoTipoAssociacao = associacao.getTipoAssociacao();

		Long codigoOperacaoMedidor = associacao.getIdOperacaoMedidorIndependente();
		OperacaoMedidor operacaoMedidor = controladorMedidor.obterOperacaoMedidor(codigoOperacaoMedidor);

		model.addAttribute(TITULO_ASSOCIACAO,
				operacaoMedidor.getDescricao() + " - " + tipoAssociacao.get(codigoTipoAssociacao));

		if (controladorMedidor.isAssociacaoPontoConsumoMedidor(codigoTipoAssociacao)) {

			carregarNumeroDigitoLeitura(associacao, model);
			configurarDadosAssociacaoMedidor(associacao, codigoOperacaoMedidor, model);
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR, true);

		} else if (controladorMedidor.isAssociacaoPontoConsumoCorretorVazao(codigoTipoAssociacao)) {
			model.addAttribute(TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO, true);
		}

		configurarDadosAssociacaoCorretorVazaoMedidorIndependente(associacao, model);

		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		model.addAttribute(ASSOCIACAO_VO, associacao);
		model.addAttribute(ID_MEDIDOR_INDEPENDENTE, associacao.getIdMedidorIndependente());
	}

	/**
	 * Carregar numero digito leitura
	 *
	 * @param associacao
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarNumeroDigitoLeitura(AssociacaoVO associacao, Model model) throws NegocioException {

		Medidor medidorIndependente = (Medidor) controladorMedidor.obter((Long) associacao.getIdMedidorIndependente());
		model.addAttribute("numeroDigitosMedidorAtual", medidorIndependente.getDigito());
	}

}
