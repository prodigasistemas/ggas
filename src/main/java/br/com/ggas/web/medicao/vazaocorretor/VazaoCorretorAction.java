/*
 * 
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 */

package br.com.ggas.web.medicao.vazaocorretor;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 
 * A classe VazaoCorretorAction representa uma VazaoCorretorAction no sistema.
 *
 * @since 22/09/2009
 *
 */
@Controller
public class VazaoCorretorAction extends GenericAction {

	private static final String CNPJ_FORMATADO = "cnpjFormatado";

	private static final String CPF_FORMATADO = "cpfFormatado";

	private static final String LISTA_ANOS_FABRICACAO = "listaAnosFabricacao";

	private static final String EXIBIR_ALTERACAO_VAZAO_CORRETOR = "exibirAlteracaoVazaoCorretor";

	private static final String EXIBIR_INCLUSAO_VAZAO_CORRETOR = "exibirInclusaoVazaoCorretor";

	private static final String FORWARD_PESQUISAR_VAZAO_CORRETOR = "forward:pesquisarVazaoCorretor";

	private static final String CLIENTE = "cliente";

	private static final String INSTALACAO_MEDIDOR = "instalacaoMedidor";

	private static final String VAZAO_CORRETOR = "vazaoCorretor";

	private static final String NUMERO_SERIE_PARCIAL = "numeroSerieParcial";

	private static final String EXISTE_CADASTRO_BENS = "existeCadastroBens";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_HISTORICO_CORRETOR_VAZAO = "listaVazaoCorretorHistorico";

	private static final String LISTA_TIPO_TRANSDUTOR_TEMPERATURA = "listaTipoTransdutorTemperatura";

	private static final String LISTA_TIPO_TRANSDUTOR_PRESSAO = "listaTipoTransdutorPressao";

	private static final String LISTA_PROTOCOLO_COMUNICACAO = "listaProtocoloComunicacao";

	private static final String LISTA_NUMERO_DIGITOS_LEITURA = "listaNumeroDigitosLeitura";

	private static final String ID_MARCA = "idMarca";

	private static final String DESCRICAO_MODELO = "descricaoModelo";

	private static final String LISTA_VAZAO = "listaVazao";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_TIPO_MOSTRADOR = "listaTipoMostrador";

	private static final String LISTA_MARCA = "listaMarca";

	private static final String LISTA_MODELO = "listaModelo";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_TEMPERATURA_MAXIMA_TRANSDUTOR = "listaTemperaturaMaximaTransdutor";

	private static final String LISTA_PRESSAO_MAXIMA_TRANSDUTOR = "listaPressaoMaximaTransdutor";

	private static final String BLOQUEAR_ALTERACAO_NUMERO_SERIE = "bloquearAlteracaoNumeroSerie";

	private static final String BLOQUEAR_ALTERACAO_LOCAL_ARMAZENAGEM = "bloquearAlteracaoLocalArmazenagem";

	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";

	private static final String BLOQUEAR_ALTERACAO_VAZAO_CORRETOR = "bloquearAlteracaoVazaoCorretor";

	private static final String LISTA_LOCAL_ARMAZENAGEM = "listaLocalArmazenagem";

	private static final String DATA_INSTALACAO = "dataInstalacao";

	private static final String HORA_INSTALACAO = "horaInstalacao";

	private static final String LEITURA_INSTALACAO = "leituraInstalacao";

	private static final String LOCAL_INSTALACAO = "localInstalacao";

	private static final String FUNCIONARIO_INSTALACAO = "funcionarioInstalacao";

	private static final String MOTIVO_INSTALACAO = "motivoInstalacao";
	
	private static final String RETORNA_ID = "retornaId";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String LOTE = "lote";

	@Autowired
	@Qualifier("controladorIntegracao")
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	@Qualifier("controladorVazaoCorretor")
	private ControladorVazaoCorretor controladorVazaoCorretor;

	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;

	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * 
	 * Exibir pesquisa vazao corretor.
	 * 
	 * @param model
	 * @param request
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("exibirPesquisaVazaoCorretor")
	public String exibirPesquisaVazaoCorretor(Model model, HttpServletRequest request) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
			request.getSession().removeAttribute(VAZAO_CORRETOR);
		}

		Collection<IntegracaoSistemaFuncao> listaIsfCadastroBens = controladorIntegracao
				.listarIntegracaoSistemaFuncaoCadastroBens();

		model.addAttribute(LISTA_MARCA, controladorVazaoCorretor.listarMarcaVazaoCorretor());
		model.addAttribute(LISTA_MODELO, controladorVazaoCorretor.listarModeloVazaoCorretor());
		model.addAttribute(EXISTE_CADASTRO_BENS, CollectionUtils.isNotEmpty(listaIsfCadastroBens));

		return "exibirPesquisaVazaoCorretor";
	}

	/**
	 * 
	 * Método responsável por exibir a tela de pesquisa de Corretor de vazão.
	 * 
	 * @param vazaoCorretor
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("pesquisarVazaoCorretor")
	public String pesquisarVazaoCorretor(VazaoCorretorImpl vazaoCorretor, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws NegocioException {

		Map<String, Object> filtro = null;

		filtro = prepararFiltro(vazaoCorretor, habilitado);
		Collection<VazaoCorretor> listaVazao = controladorVazaoCorretor.consultarVazaoCorretor(filtro);

		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(LISTA_VAZAO, listaVazao);
		model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);

		saveToken(request);

		return exibirPesquisaVazaoCorretor(model, request);
	}

	/**
	 * 
	 * Método responsável por detalhar o corretor de vazão.
	 * 
	 * @param vazaoCorretorTmp
	 * @param result
	 * @param chavePrimaria
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("exibirDetalhamentoVazaoCorretor")
	public String exibirDetalhamentoVazaoCorretor(VazaoCorretorImpl vazaoCorretorTmp, BindingResult result,
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		carregarCampos(model);
		request.getSession().setAttribute(VAZAO_CORRETOR, vazaoCorretorTmp);

		try {
			VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter(chavePrimaria);
			InstalacaoMedidor instalacaoMedidor = controladorMedidor
					.obterInstalacaoMedidorPorCorretorVazao(chavePrimaria);

			model.addAttribute(EXISTE_CADASTRO_BENS,
					CollectionUtils.isNotEmpty(controladorIntegracao.listarIntegracaoSistemaFuncaoCadastroBens()));

			Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretorHistorico = controladorVazaoCorretor
					.listarHistoricoCorretorVazao(chavePrimaria);

			carregarInformacaoInstalacaoCorretorVazao(listaVazaoCorretorHistorico, model);

			Cliente cliente = null;
			PontoConsumo pontoConsumo = null;

			if (instalacaoMedidor != null) {
				cliente = controladorCliente
						.obterClientePorPontoConsumo(instalacaoMedidor.getPontoConsumo().getChavePrimaria());

				model.addAttribute("clienteEnderecoFormatado", cliente.getEnderecoPrincipal().getEnderecoFormatado());

				pontoConsumo = controladorPontoConsumo
						.obterPontoConsumo(instalacaoMedidor.getPontoConsumo().getChavePrimaria());
			}

			model.addAttribute(HABILITADO, habilitado);
			model.addAttribute(LISTA_HISTORICO_CORRETOR_VAZAO, listaVazaoCorretorHistorico);
			model.addAttribute(INSTALACAO_MEDIDOR, instalacaoMedidor);
			model.addAttribute(CLIENTE, cliente);

			verificarCpfCnpj(model, cliente);

			model.addAttribute(PONTO_CONSUMO, pontoConsumo);
			model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return FORWARD_PESQUISAR_VAZAO_CORRETOR;
		}

		return "exibirDetalhamentoVazaoCorretor";
	}

	/**
	 * 
	 * Verifica se existe CPF ou CNPJ para formatar
	 * 
	 * @param model
	 * @param cliente
	 * 
	 */
	private void verificarCpfCnpj(Model model, Cliente cliente) {

		if (cliente != null) {

			if (cliente.getCpf() != null) {
				model.addAttribute(CPF_FORMATADO, cliente.getCpfFormatado());
			}

			if (cliente.getCnpj() != null) {
				model.addAttribute(CNPJ_FORMATADO, cliente.getCnpjFormatado());
			}

		}

	}

	/**
	 * 
	 * Método responsável por exibir a tela de inclusão de Corretor de Vazão.
	 * 
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping(EXIBIR_INCLUSAO_VAZAO_CORRETOR)
	public String exibirInclusaoVazaoCorretor(HttpServletRequest request, Model model) throws GGASException {
		request.getSession().removeAttribute(VAZAO_CORRETOR);
		carregarCampos(model);

		saveToken(request);

		return EXIBIR_INCLUSAO_VAZAO_CORRETOR;
	}

	/**
	 * 
	 * Método responsável por incluir o Corretor de Vazão.
	 * 
	 * @param vazaoCorretor
	 * @param result
	 * @param request
	 * @param model
	 * @return Sring
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("incluirVazaoCorretor")
	public String incluirVazaoCorretor(VazaoCorretorImpl vazaoCorretor, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		carregarCampos(model);
		String view = EXIBIR_INCLUSAO_VAZAO_CORRETOR;
		model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);

		String codigoSituacaoDisponivel = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL);

		vazaoCorretor
				.setSituacaoMedidor(controladorMedidor.obterSituacaoMedidor(Long.parseLong(codigoSituacaoDisponivel)));

		try {

			validarToken(request);

			verificarNumeroSerieSequencialNumerico(vazaoCorretor);

			controladorVazaoCorretor.inserirCorretorVazao(vazaoCorretor);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, VazaoCorretor.CORRETOR_VAZAO);

			view = pesquisarVazaoCorretor(vazaoCorretor, result, vazaoCorretor.isHabilitado(), request, model);
			request.getSession().removeAttribute(VAZAO_CORRETOR);
		} catch (GGASException e) {
			LOG.error("Falha ao incluir corretor de vazão", e);
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
		}
		return view;
	}

	/**
	 * 
	 * Método responsável por exibir a tela de alteração de Corretor de Vazão.
	 * 
	 * @param vazaoCorretorTmp
	 * @param result
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping(EXIBIR_ALTERACAO_VAZAO_CORRETOR)
	public String exibirAlteracaoVazaoCorretor(VazaoCorretorImpl vazaoCorretorTmp, BindingResult result,
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);
		request.getSession().setAttribute(VAZAO_CORRETOR, vazaoCorretorTmp);
		carregarCampos(model);

		try {
			VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter(chavePrimaria);

			if (!controladorMedidor.permiteAlteracaoSituacaoMedidor(chavePrimaria)) {
				model.addAttribute(BLOQUEAR_ALTERACAO_NUMERO_SERIE, true);
			} else {
				model.addAttribute(BLOQUEAR_ALTERACAO_NUMERO_SERIE, false);
			}

			if (!controladorVazaoCorretor.permiteAlteracaoSituacaoCorretorVazao(chavePrimaria)) {
				Collection<SituacaoMedidor> situacoesMedidor = new ArrayList<SituacaoMedidor>();
				situacoesMedidor.add(vazaoCorretor.getSituacaoMedidor());
				model.addAttribute(LISTA_SITUACAO_MEDIDOR, situacoesMedidor);
				model.addAttribute(BLOQUEAR_ALTERACAO_VAZAO_CORRETOR, true);
			} else {
				model.addAttribute(BLOQUEAR_ALTERACAO_VAZAO_CORRETOR, false);
			}

			MedidorLocalArmazenagem localArmazenagem = vazaoCorretor.getLocalArmazenagem();

			listaLocalArmazenagemMedidor(model, localArmazenagem);

			model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);

			return EXIBIR_ALTERACAO_VAZAO_CORRETOR;

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_PESQUISAR_VAZAO_CORRETOR;
	}

	/**
	 * 
	 * Método responsável por alterar um corretor de vazão.
	 * 
	 * @param vazaoCorretor
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("alterarVazaoCorretor")
	public String alterarVazaoCorretor(VazaoCorretorImpl vazaoCorretor, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);

		carregarCampos(model);
		String view = EXIBIR_ALTERACAO_VAZAO_CORRETOR;
		Map<String, Object> erros = vazaoCorretor.validarDados();

		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
		} else {

			try {

				validarToken(request);

				controladorMedidor.validarNumeroSerie(vazaoCorretor.getNumeroSerie());

				MedidorLocalArmazenagem localArmazenagemAnterior = vazaoCorretor.getLocalArmazenagem();

				if (vazaoCorretor.getLocalArmazenagem() != null && localArmazenagemAnterior != null && vazaoCorretor
						.getLocalArmazenagem().getChavePrimaria() != localArmazenagemAnterior.getChavePrimaria()) {
					vazaoCorretor.getMovimentacoes()
							.add(controladorVazaoCorretor.criarMovimentacaoTransferenciaCorretorVazao(vazaoCorretor,
									localArmazenagemAnterior, vazaoCorretor.getLocalArmazenagem()));
				}

				controladorVazaoCorretor.validarDadosAlteracaoVazaoCorretor(vazaoCorretor);

				controladorVazaoCorretor.atualizar(vazaoCorretor);

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, VazaoCorretor.CORRETOR_VAZAO);

				view = pesquisarVazaoCorretor(vazaoCorretor, result, vazaoCorretor.isHabilitado(), request, model);
				request.getSession().removeAttribute(VAZAO_CORRETOR);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				saveToken(request);
			}
		}

		return view;
	}

	/**
	 * 
	 * Método responsável por excluir os corretores de vazão selecionados.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("excluirVazaoCorretor")
	public String excluirVazaoCorretor(@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws NegocioException {

		try {
			controladorVazaoCorretor.validarExcluirVazaoCorretor(chavesPrimarias);
			controladorVazaoCorretor.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, VazaoCorretor.CORRETORES_VAZAO);
			request.getSession().removeAttribute(VAZAO_CORRETOR);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return pesquisarVazaoCorretor(null, null, true, request, model);
	}

	/**
	 * 
	 * Método responsável por manter os dados de uma pesquisa após cancelar uma ação
	 * de alteração ou voltar da tela de detalhamento
	 * 
	 * @param vazaoCorretor
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("voltar")
	public String voltar(VazaoCorretorImpl vazaoCorretor, BindingResult result,
			@RequestParam("habilitadoPesquisa") Boolean habilitado, HttpServletRequest request, Model model)
			throws NegocioException {

		VazaoCorretorImpl vazaoCorretorSession = vazaoCorretor;

		if (request.getSession().getAttribute(VAZAO_CORRETOR) != null) {
			vazaoCorretorSession = (VazaoCorretorImpl) request.getSession().getAttribute(VAZAO_CORRETOR);
		}

		return pesquisarVazaoCorretor(vazaoCorretorSession, result, habilitado, request, model);
	}

	/**
	 * 
	 * Método responsável por verificar o campo Número de Série e validar o mesmo
	 * 
	 * @param vazaoCorretor
	 * @throws NegocioException
	 * 
	 */
	private void verificarNumeroSerieSequencialNumerico(VazaoCorretor vazaoCorretor) throws NegocioException {

		if (!StringUtils.isEmpty(vazaoCorretor.getNumeroSerie())) {
			controladorMedidor.validarNumeroSerie(vazaoCorretor.getNumeroSerie());
		} else {
			vazaoCorretor.setNumeroSerie(null);
		}

	}

	/**
	 * 
	 * Método responsável por verificar, montar e carregar a lista dos locais de
	 * armazenagens
	 * 
	 * @param model
	 * @param localArmazenagem
	 * @throws NegocioException
	 * 
	 */
	public void listaLocalArmazenagemMedidor(Model model, MedidorLocalArmazenagem localArmazenagem)
			throws NegocioException {

		if (localArmazenagem != null) {

			String codigoLocalArmazenagemCliente = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_CODIGO_LOCAL_ARMAZENAGEM_CLIENTE);

			if (localArmazenagem.getChavePrimaria() == Long.parseLong(codigoLocalArmazenagemCliente)) {

				model.addAttribute(BLOQUEAR_ALTERACAO_LOCAL_ARMAZENAGEM, true);

			} else {

				Collection<MedidorLocalArmazenagem> locaisArmazenagem = new ArrayList<MedidorLocalArmazenagem>();

				for (MedidorLocalArmazenagem medidorLocalArmazenagem : controladorMedidor.listarLocalArmazenagem()) {

					if (medidorLocalArmazenagem.getChavePrimaria() != Long.parseLong(codigoLocalArmazenagemCliente)) {
						locaisArmazenagem.add(medidorLocalArmazenagem);
					}

				}

				model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, locaisArmazenagem);
			}
		}
	}

	/**
	 * 
	 * carrega as informações referente a instalação do corretor de vazão
	 * 
	 * @param listaVazaoCorretorHistorico
	 * @param model
	 * 
	 */
	private void carregarInformacaoInstalacaoCorretorVazao(
			Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretorHistorico, Model model) {

		Iterator<VazaoCorretorHistoricoOperacao> iterator = listaVazaoCorretorHistorico.iterator();

		while (iterator.hasNext()) {

			VazaoCorretorHistoricoOperacao item = iterator.next();

			if (item.getOperacaoMedidor().getChavePrimaria() == OperacaoMedidor.CODIGO_INSTALACAO) {

				if (item.getDataRealizada() != null) {
					model.addAttribute(DATA_INSTALACAO, Util.converterDataParaStringSemHora(item.getDataRealizada(),
							Constantes.FORMATO_DATA_HORA_BR));
				}
				if (item.getLeitura() != null) {
					model.addAttribute(LEITURA_INSTALACAO, item.getLeitura().toString());
				}
				if (item.getLocalInstalacao().getDescricao() != null) {
					model.addAttribute(LOCAL_INSTALACAO, item.getLocalInstalacao().getDescricao());
				}
				if (item.getDataRealizada() != null) {
					model.addAttribute(HORA_INSTALACAO,
							Util.converterDataParaStringHoraMinutoComCaracteresEspeciais(item.getDataRealizada()));
				}
				if (item.getFuncionario() != null && item.getFuncionario().getNome() != null) {
					model.addAttribute(FUNCIONARIO_INSTALACAO, item.getFuncionario().getNome());
				}
				if (item.getMotivoOperacaoMedidor() != null && item.getMotivoOperacaoMedidor().getDescricao() != null) {
					model.addAttribute(MOTIVO_INSTALACAO, item.getMotivoOperacaoMedidor().getDescricao());
				}

			}
		}
	}

	/**
	 * 
	 * Carregar campos.
	 * 
	 * @param model
	 * @throws GGASException
	 * 
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_MARCA, controladorVazaoCorretor.listarMarcaVazaoCorretor());
		model.addAttribute(LISTA_MODELO, controladorVazaoCorretor.listarModeloVazaoCorretor());
		model.addAttribute(LISTA_TIPO_MOSTRADOR, controladorVazaoCorretor.listarTipoMostrador());
		model.addAttribute(LISTA_PROTOCOLO_COMUNICACAO, controladorVazaoCorretor.listarProtocoloComunicacao());
		model.addAttribute(LISTA_NUMERO_DIGITOS_LEITURA, controladorVazaoCorretor.listarNumeroDigitosLeitura());
		model.addAttribute(LISTA_TIPO_TRANSDUTOR_PRESSAO, controladorVazaoCorretor.listarTipoTransdutorPressao());
		model.addAttribute(LISTA_TEMPERATURA_MAXIMA_TRANSDUTOR,
				controladorVazaoCorretor.listarTemperaturaMaximaTransdutor());
		model.addAttribute(LISTA_TIPO_TRANSDUTOR_TEMPERATURA,
				controladorVazaoCorretor.listarTipoTransdutorTemperatura());
		model.addAttribute(LISTA_PRESSAO_MAXIMA_TRANSDUTOR, controladorVazaoCorretor.listarPressaoMaximaTransdutor());
		model.addAttribute(LISTA_ANOS_FABRICACAO, Util.listarAnos((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO)));
		model.addAttribute(LISTA_SITUACAO_MEDIDOR, controladorMedidor.listarSituacaoMedidorCadastro());
		model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
	}

	/**
	 * 
	 * Preparar filtro.
	 * 
	 * @param vazaoCorretor
	 * @param habilitado
	 * @return filtro
	 * 
	 */
	private Map<String, Object> prepararFiltro(VazaoCorretor vazaoCorretor, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (vazaoCorretor != null) {

			if (vazaoCorretor.getModelo() != null) {
				filtro.put(DESCRICAO_MODELO, vazaoCorretor.getModelo().getDescricao());
			}

			if (!vazaoCorretor.getNumeroSerie().isEmpty()) {
				filtro.put(NUMERO_SERIE_PARCIAL, vazaoCorretor.getNumeroSerie());
			}

			if (vazaoCorretor.getMarcaCorretor() != null && vazaoCorretor.getMarcaCorretor().getChavePrimaria() > 0) {
				filtro.put(ID_MARCA, vazaoCorretor.getMarcaCorretor().getChavePrimaria());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;

	}
	
	/**
	 * Método responsável por pesquisar um imóvel atraves do popup de "Pesquisa
	 * Imovel".
	 * 
	 * @param vazaoCorretor
	 *            {@link VazaoCorretorImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param habilitado
	 *            {@link Boolean}
	 * @param pontoConsumo
	 *            {@link String}
	 * @param lote
	 *            {@link Boolean}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("pesquisarCorretorVazaoPopup")
	public String pesquisarCorretorVazaoPopup(VazaoCorretorImpl vazaoCorretor, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam("pontoConsumo") String pontoConsumo, @RequestParam("lote") boolean lote, HttpServletRequest request, Model model)
					throws NegocioException {

		Map<String, Object> filtro = prepararFiltro(vazaoCorretor, habilitado);

		Collection<VazaoCorretor> listaVazao = controladorVazaoCorretor.consultarVazaoCorretorDisponivel(filtro);

		model.addAttribute(VAZAO_CORRETOR, vazaoCorretor);
		model.addAttribute(LISTA_VAZAO, listaVazao);
		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute(LOTE, lote);

		return exibirPesquisaCorretorVazaoPopup(request, model, lote, pontoConsumo);
	}

	/**
	 * Método responsável por exibir a tela do popup de pesquisa imóvel.
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @param lote
	 *            {@link}
	 * @param pontoConsumo
	 *            {@link String}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaCorretorVazaoPopup")
	public String exibirPesquisaCorretorVazaoPopup(HttpServletRequest request, Model model,
			@RequestParam(value = LOTE, required = false) boolean lote,
			@RequestParam(value = PONTO_CONSUMO, required = false) String pontoConsumo) throws NegocioException {

		model.addAttribute(LISTA_MARCA, controladorVazaoCorretor.listarMarcaVazaoCorretor());
		model.addAttribute(LISTA_MODELO, controladorVazaoCorretor.listarModeloVazaoCorretor());
		model.addAttribute(RETORNA_ID, request.getParameter(RETORNA_ID));
		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute(LOTE, lote);

		return "exibirPesquisaCorretorVazaoPopup";
	}

}
