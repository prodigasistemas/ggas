/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import java.io.Serializable;
import java.util.Date;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class ImovelPCSZVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -1000116031744736235L;

	private Date data;

	private String medidaPCS;

	private String fatorZ;

	private boolean habilitado;

	private boolean permiteAlteracaoFatorZ;

	/**
	 * @return the data
	 */
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Date data) {

		this.data = data;
	}

	/**
	 * @return the medidaPCS
	 */
	public String getMedidaPCS() {

		return medidaPCS;
	}

	/**
	 * @param medidaPCS
	 *            the medidaPCS to set
	 */
	public void setMedidaPCS(String medidaPCS) {

		this.medidaPCS = medidaPCS;
	}

	/**
	 * @return the fatorZ
	 */
	public String getFatorZ() {

		return fatorZ;
	}

	/**
	 * @param fatorZ
	 *            the fatorZ to set
	 */
	public void setFatorZ(String fatorZ) {

		this.fatorZ = fatorZ;
	}

	/**
	 * @return the habilitado
	 */
	public boolean isHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * Método responsável por retornar a data no
	 * formato dd/MM/yyyy
	 * 
	 * @return
	 */
	public String getDataFormatada() {

		return Util.converterDataParaStringSemHora(this.getData(), Constantes.FORMATO_DATA_BR);
	}

	/**
	 * @return the permiteAlteracaoFatorZ
	 */
	public boolean isPermiteAlteracaoFatorZ() {

		return permiteAlteracaoFatorZ;
	}

	/**
	 * @param permiteAlteracaoFatorZ
	 *            the permiteAlteracaoFatorZ to
	 *            set
	 */
	public void setPermiteAlteracaoFatorZ(boolean permiteAlteracaoFatorZ) {

		this.permiteAlteracaoFatorZ = permiteAlteracaoFatorZ;
	}

}
