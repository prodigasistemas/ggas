/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.consumo;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável pelas tela relacionada a Dados de Pressão e Temperatura
 *
 */
@Controller
public class TemperaturaPressaoMedicaoAction extends GenericAction {

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String INTERVALO_ANOS_POSTERIORES_DATA_ATUAL = "intervaloAnosPosterioresDataAtual";

	private static final String LISTA_UNIDADE_PRESSAO = "listaUnidadePressao";

	private static final String LISTA_UNIDADE_TEMPERATURA = "listaUnidadeTemperatura";

	private static final String ID_ABRANGENCIA_PESQUISA = "idAbrangenciaPesquisa";

	private static final String DATA_INICIAL = "dataInicial";

	private static final String ID_ABRANGENCIA = "idAbrangencia";

	private static final String CODIGO_TIPO_ABRANGENCIA = "codigoTipoAbrangencia";

	private static final String CODIGO_TIPO_ABRANGENCIA_PESQUISA = "codigoTipoAbrangenciaPesquisa";

	private static final String LISTA_TIPO_ABRANGENCIA = "listaTipoAbrangencia";

	private static final String LISTA_TEMPERATURA_PRESSAO_MEDICAO_VO = "listaTemperaturaPressaoMedicaoVO";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	private ControladorGeografico controladorGeografico;

	@Autowired
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	private ControladorTemperaturaPressaoMedicao controladorTemperaturaPressaoMedicao;

	@Autowired
	private ControladorEndereco controladorEndereco;

	/**
	 * Método responsável por exibir a tela de manutenção de temperatura e pressão
	 * de gás para medição.
	 * 
	 * @param codigoTipoAbrangencia
	 *            - {@link String}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirManutencaoTemperaturaPressaoMedicao")
	public String exibirManutencaoTemperaturaPressaoMedicao(
			@RequestParam(value = CODIGO_TIPO_ABRANGENCIA_PESQUISA, required = false) String codigoTipoAbrangencia,
			HttpServletRequest request, Model model) throws GGASException {

		carregarCampos(codigoTipoAbrangencia, model);

		saveToken(request);

		return "exibirManutencaoTemperaturaPressaoMedicao";
	}

	/**
	 * Método responsável por exibir a tela com os dados para manutenção de
	 * temperatura e pressão de gás para medição.
	 * 
	 * @param dataInicialFormatada
	 *            - {@link String}
	 * @param codigoTipoAbrangencia
	 *            - {@link String}
	 * @param idAbrangencia
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirTemperaturaPressaoMedicao")
	public String exibirTemperaturaPressaoMedicao(@RequestParam(DATA_INICIAL) String dataInicialFormatada,
			@RequestParam(CODIGO_TIPO_ABRANGENCIA_PESQUISA) String codigoTipoAbrangencia,
			@RequestParam(value = ID_ABRANGENCIA_PESQUISA, required = false) Long idAbrangencia,
			HttpServletRequest request, Model model) throws GGASException {

		Long chaveRegiao = null;
		Long chaveMunicipio = null;
		Long chaveLocalidade = null;
		Long chaveMicrorregiao = null;
		Long chaveUnidadeFederacao = null;

		if (!StringUtils.isEmpty(dataInicialFormatada)) {
			model.addAttribute("dataInicial", Util.removerCaracteresEspeciais(dataInicialFormatada));
		}
		model.addAttribute(CODIGO_TIPO_ABRANGENCIA_PESQUISA, codigoTipoAbrangencia);

		if (TemperaturaPressaoMedicaoVO.CODIGO_REGIAO.equals(codigoTipoAbrangencia)) {
			chaveRegiao = idAbrangencia;
		} else if (TemperaturaPressaoMedicaoVO.CODIGO_MUNICIPIO.equals(codigoTipoAbrangencia)) {
			chaveMunicipio = idAbrangencia;
		} else if (TemperaturaPressaoMedicaoVO.CODIGO_UNIDADE_FEDERACAO.equals(codigoTipoAbrangencia)) {
			chaveUnidadeFederacao = idAbrangencia;
		} else if (TemperaturaPressaoMedicaoVO.CODIGO_LOCALIDADE.equals(codigoTipoAbrangencia)) {
			chaveLocalidade = idAbrangencia;
		} else if (TemperaturaPressaoMedicaoVO.CODIGO_MICRORREGIAO.equals(codigoTipoAbrangencia)) {
			chaveMicrorregiao = idAbrangencia;
		}

		try {
			Date dataInicial = null;
			if (!StringUtils.isEmpty(dataInicialFormatada)) {
				dataInicial = Util.converterCampoStringParaData("Data Inicial", dataInicialFormatada,
						Constantes.FORMATO_DATA_BR);
			}
			controladorTemperaturaPressaoMedicao.validarPesquisaManutencaoTemperaturaPressaoMedicao(
					codigoTipoAbrangencia, idAbrangencia, dataInicial);


				Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao = controladorTemperaturaPressaoMedicao
						.listarTemperaturaPressaoMedicaoManutencao(chaveLocalidade, chaveMicrorregiao, chaveMunicipio,
								chaveRegiao, dataInicial, chaveUnidadeFederacao);

				Collection<TemperaturaPressaoMedicaoVO> listaTemperaturaPressaoMedicaoVO = converterTemperaturaPressaoMedicao(
						listaTemperaturaPressaoMedicao);

				listaTemperaturaPressaoMedicaoVO.add(new TemperaturaPressaoMedicaoVO());

				Collections.sort((List<TemperaturaPressaoMedicaoVO>) listaTemperaturaPressaoMedicaoVO,
						new Comparator<TemperaturaPressaoMedicaoVO>() {

							@Override
							public int compare(TemperaturaPressaoMedicaoVO o1, TemperaturaPressaoMedicaoVO o2) {

								int retorno = 0;

								if (o1.getDataVigencia() == null) {
									retorno = 1;
								} else {
									if (o2.getDataVigencia() == null) {
										retorno = -1;
									} else {
										if (o1.getDataVigencia() != null && o2.getDataVigencia() != null) {
											retorno = o1.getDataVigencia().compareTo(o2.getDataVigencia());
										}
									}
								}

								return retorno;
							}
						});

				model.addAttribute(LISTA_TEMPERATURA_PRESSAO_MEDICAO_VO, listaTemperaturaPressaoMedicaoVO);
			
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
		}
		// Atribui ao valor da abrangência o da
		// lista gerada
		model.addAttribute(ID_ABRANGENCIA, idAbrangencia);
		model.addAttribute(CODIGO_TIPO_ABRANGENCIA, codigoTipoAbrangencia);

		saveToken(request);

		return exibirManutencaoTemperaturaPressaoMedicao(codigoTipoAbrangencia, request, model);
	}

	/**
	 * Método responsável por manter de temperatura e pressão de gás para medição.
	 * 
	 * @param temperaturaPressaoListasVO
	 *            - {@link TemperaturaPressaoListasVO}
	 * @param results
	 *            - {@link BindingResult}
	 * @param dataInicialFormatada
	 *            - {@link String}
	 * @param idAbrangencia
	 *            - {@link Long}
	 * @param codigoTipoAbrangencia
	 *            - {@link String}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("manterTemperaturaPressaoMedicao")
	public String manterTemperaturaPressaoMedicao(TemperaturaPressaoListasVO temperaturaPressaoListasVO,
			BindingResult results, @RequestParam(DATA_INICIAL) String dataInicialFormatada,
			@RequestParam(ID_ABRANGENCIA) Long idAbrangencia,
			@RequestParam(CODIGO_TIPO_ABRANGENCIA) String codigoTipoAbrangencia, HttpServletRequest request,
			Model model) throws GGASException {

		try {

			// Passa para os campos de pesquisa a
			// abrangência da persistencia
			model.addAttribute(ID_ABRANGENCIA, idAbrangencia);
			model.addAttribute(CODIGO_TIPO_ABRANGENCIA, codigoTipoAbrangencia);
			model.addAttribute(ID_ABRANGENCIA_PESQUISA, idAbrangencia);
			model.addAttribute(CODIGO_TIPO_ABRANGENCIA_PESQUISA, codigoTipoAbrangencia);
			
			validarToken(request);
			// Obtém dados de compra de PCS de city
			// gate do form e coloca no request
			// para exibição
			Collection<TemperaturaPressaoMedicaoVO> listaTemperaturaPressaoMedicaoVO = converterTemperaturaPressaoMedicao(
					temperaturaPressaoListasVO);
			model.addAttribute(LISTA_TEMPERATURA_PRESSAO_MEDICAO_VO, listaTemperaturaPressaoMedicaoVO);

			// Obtém dados de compra de PCS de city
			// gate do form e persiste
			Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao = new ArrayList<TemperaturaPressaoMedicao>();
			popularListaTemperaturaPressaoMedicao(temperaturaPressaoListasVO, request, listaTemperaturaPressaoMedicao,
					codigoTipoAbrangencia, idAbrangencia);
			boolean exibirMsgProcessarConsistencia = controladorTemperaturaPressaoMedicao
					.atualizarListaTemperaturaPressaoMedicao(listaTemperaturaPressaoMedicao);

			if (exibirMsgProcessarConsistencia) {
				mensagemAlerta(model, Constantes.NECESSARIO_PROCESSAR_CONSISTENCIA_LEITURA);
			} else {
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						TemperaturaPressaoMedicao.TEMPERATURA_PRESSAO_MEDICOES);
			}

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			return exibirManutencaoTemperaturaPressaoMedicao(codigoTipoAbrangencia, request, model);
		} catch (FormatoInvalidoException e) {
			mensagemErroParametrizado(model, e);
			return exibirManutencaoTemperaturaPressaoMedicao(codigoTipoAbrangencia, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return exibirTemperaturaPressaoMedicao(dataInicialFormatada, codigoTipoAbrangencia, idAbrangencia, request,
				model);
	}

	/**
	 * Método responsável por exibir a tela de manutenção de temperatura e pressão
	 * de gás para medição.
	 * 
	 * @param temperaturaPressaoListasVO
	 *            - {@link TemperaturaPressaoListasVO}
	 * @param result
	 *            - {@link BindingResult}
	 * @param temperaturaPressaoMedicaoVOTela
	 *            - {@link TemperaturaPressaoMedicaoVO}
	 * @param results
	 *            - {@link BindingResult}
	 * @param codigoTipoAbrangencia
	 *            - {@link String}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerTemperaturaPressaoMedicao")
	public String removerTemperaturaPressaoMedicao(TemperaturaPressaoListasVO temperaturaPressaoListasVO,
			BindingResult result, TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVOTela, BindingResult results,
			@RequestParam(CODIGO_TIPO_ABRANGENCIA) String codigoTipoAbrangencia, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			String dataInicialFormatada = "";
			Integer indexRemover = Integer.parseInt(request.getParameter("indexLista"));
			Long idAbrangencia = Long.parseLong(request.getParameter(ID_ABRANGENCIA));
			model.addAttribute(ID_ABRANGENCIA, idAbrangencia);
			model.addAttribute(CODIGO_TIPO_ABRANGENCIA, codigoTipoAbrangencia);
			model.addAttribute(ID_ABRANGENCIA_PESQUISA, idAbrangencia);
			model.addAttribute(CODIGO_TIPO_ABRANGENCIA_PESQUISA, codigoTipoAbrangencia);
			dataInicialFormatada = request.getParameter(DATA_INICIAL);
			model.addAttribute(DATA_INICIAL, dataInicialFormatada);
			Date dataInicial = null;
			if (!StringUtils.isEmpty(dataInicialFormatada)) {
				dataInicial = Util.converterCampoStringParaData("Data Inicial", dataInicialFormatada,
						Constantes.FORMATO_DATA_BR);
			}
			validarToken(request);
			
			List<TemperaturaPressaoMedicaoVO> listaTemperaturaPressaoMedicaoVO =
					(List<TemperaturaPressaoMedicaoVO>) converterTemperaturaPressaoMedicao(temperaturaPressaoListasVO);

			TemperaturaPressaoMedicaoVO medicaoRemovida = null;
			if (!listaTemperaturaPressaoMedicaoVO.isEmpty() && indexRemover != null && indexRemover >= 0) {
				medicaoRemovida = listaTemperaturaPressaoMedicaoVO.get(indexRemover);
			}

			if (medicaoRemovida != null && medicaoRemovida.getChavePrimaria() != null
					&& medicaoRemovida.getChavePrimaria() > 0) {

				TemperaturaPressaoMedicao temperaturaPressaoMedicaoRemovida = (TemperaturaPressaoMedicao) controladorTemperaturaPressaoMedicao
						.obter(medicaoRemovida.getChavePrimaria());
				temperaturaPressaoMedicaoRemovida.setDadosAuditoria(getDadosAuditoria(request));

				controladorTemperaturaPressaoMedicao.remover(temperaturaPressaoMedicaoRemovida);

				listaTemperaturaPressaoMedicaoVO.remove(medicaoRemovida);

				// Verifica se ainda existe na lista
				// algum vigente
				if (!listaTemperaturaPressaoMedicaoVO.isEmpty()) {
					boolean existeVigente = false;
					for (TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVO : listaTemperaturaPressaoMedicaoVO) {
						if (temperaturaPressaoMedicaoVO.getDataVigencia() != null
								&& !temperaturaPressaoMedicaoVO.getDataVigencia().after(new Date())) {
							existeVigente = true;
							break;
						}
					}

					// Caso não exista um vigente,
					// consultar o último vigente
					// cadastrado.
					if (!existeVigente) {
						Long chaveLocalidade = null;
						Long chaveMicrorregiao = null;
						Long chaveMunicipio = null;
						Long chaveRegiao = null;

						if (TemperaturaPressaoMedicaoVO.CODIGO_LOCALIDADE.equals(codigoTipoAbrangencia)) {
							chaveLocalidade = idAbrangencia;
						} else {
							if (TemperaturaPressaoMedicaoVO.CODIGO_MICRORREGIAO.equals(codigoTipoAbrangencia)) {
								chaveMicrorregiao = idAbrangencia;
							} else {
								if (TemperaturaPressaoMedicaoVO.CODIGO_MUNICIPIO.equals(codigoTipoAbrangencia)) {
									chaveMunicipio = idAbrangencia;
								} else {
									if (TemperaturaPressaoMedicaoVO.CODIGO_REGIAO.equals(codigoTipoAbrangencia)) {
										chaveRegiao = idAbrangencia;
									}
								}
							}
						}

						TemperaturaPressaoMedicao temperaturaPressaoMedicaoVigente = controladorTemperaturaPressaoMedicao
								.obterTemperaturaPressaoMedicaoVigentePorData(chaveLocalidade, chaveMicrorregiao,
										chaveMunicipio, chaveRegiao, dataInicial, null);

						// Caso exista um vigente,
						// adicionar na lista a ser
						// exibida.
						if (temperaturaPressaoMedicaoVigente != null) {
							TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVO = converterTemperaturaPressaoMedicao(
									temperaturaPressaoMedicaoVigente);
							listaTemperaturaPressaoMedicaoVO.add(temperaturaPressaoMedicaoVO);
						}

						// Ordena lista VO
						Collections.sort((List<TemperaturaPressaoMedicaoVO>) listaTemperaturaPressaoMedicaoVO,
								new Comparator<TemperaturaPressaoMedicaoVO>() {

									@Override
									public int compare(TemperaturaPressaoMedicaoVO o1, TemperaturaPressaoMedicaoVO o2) {

										int retorno = 0;

										if (o1.getDataVigencia() == null) {
											retorno = 1;
										} else {
											if (o2.getDataVigencia() == null) {
												retorno = -1;
											} else {
												if (o1.getDataVigencia() != null && o2.getDataVigencia() != null) {
													retorno = o1.getDataVigencia().compareTo(o2.getDataVigencia());
												}
											}
										}

										return retorno;
									}
								});
					}
				}

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request,
						TemperaturaPressaoMedicao.TEMPERATURA_PRESSAO_MEDICAO);

				model.addAttribute(LISTA_TEMPERATURA_PRESSAO_MEDICAO_VO, listaTemperaturaPressaoMedicaoVO);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return exibirManutencaoTemperaturaPressaoMedicao(codigoTipoAbrangencia, request, model);
	}

	/**
	 * Método responsável por popular a lista de dados de temperatura e pressão a
	 * partir dos dados do formulário.
	 * 
	 */
	private void popularListaTemperaturaPressaoMedicao(TemperaturaPressaoListasVO temperaturaPressaoListas,
			HttpServletRequest request, Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao,
			String codigoTipoAbrangencia, Long idAbrangencia) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		if (temperaturaPressaoListas.getChavePrimaria().length > 0) {
			for (int i = 0; i < temperaturaPressaoListas.getChavePrimaria().length; i++) {
				if (temperaturaPressaoListas.getChavePrimaria()[i] == null) {
					Long[] chaves = temperaturaPressaoListas.getChavePrimaria();
					chaves[i] = 0L;
					temperaturaPressaoListas.setChavePrimaria(chaves);
				}
				if (temperaturaPressaoListas.getHabilitado()[i] != null
						&& temperaturaPressaoListas.getHabilitado()[i]) {

					TemperaturaPressaoMedicao temperaturaPressaoMedicao = null;

					if (temperaturaPressaoListas.getChavePrimaria()[i] > 0) {
						temperaturaPressaoMedicao = (TemperaturaPressaoMedicao) controladorTemperaturaPressaoMedicao
								.obter(temperaturaPressaoListas.getChavePrimaria()[i], "unidadePressaoAtmosferica",
										"unidadeTemperatura");
					} else {
						temperaturaPressaoMedicao = (TemperaturaPressaoMedicao) controladorTemperaturaPressaoMedicao
								.criar();
						preencherAreaAbrangenciaTemperaturaPressaoMedicao(temperaturaPressaoMedicao,
								codigoTipoAbrangencia, idAbrangencia);
					}

					if (temperaturaPressaoListas.getDataVigencia().length > 0
							&& !StringUtils.isEmpty(temperaturaPressaoListas.getDataVigencia()[i])) {
						if (!Util.isDataValida(temperaturaPressaoListas.getDataVigencia()[i],
								Constantes.FORMATO_DATA_BR)) {
							throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS);
						}
						temperaturaPressaoMedicao.setDataInicio(Util.converterCampoStringParaData("Data",
								temperaturaPressaoListas.getDataVigencia()[i], Constantes.FORMATO_DATA_BR));
					} else {
						temperaturaPressaoMedicao.setDataInicio(null);
					}

					if (temperaturaPressaoListas.getTemperatura().length > 0
							&& !StringUtils.isEmpty(temperaturaPressaoListas.getTemperatura()[i])) {
						temperaturaPressaoMedicao.setTemperatura(Util.converterCampoStringParaValorBigDecimal(
								TemperaturaPressaoMedicao.TEMPERATURA, temperaturaPressaoListas.getTemperatura()[i],
								Constantes.FORMATO_VALOR_NUMERO, request.getLocale()));
					} else {
						temperaturaPressaoMedicao.setTemperatura(null);
					}

					if (temperaturaPressaoListas.getIdUnidadeTemperatura().length > 0
							&& temperaturaPressaoListas.getIdUnidadeTemperatura()[i] != null
							&& temperaturaPressaoListas.getIdUnidadeTemperatura()[i] > 0) {
						Unidade unidade = (Unidade) controladorUnidade
								.obter(temperaturaPressaoListas.getIdUnidadeTemperatura()[i]);
						temperaturaPressaoMedicao.setUnidadeTemperatura(unidade);
					} else {
						temperaturaPressaoMedicao.setUnidadeTemperatura(null);
					}

					if (temperaturaPressaoListas.getPressao().length > 0
							&& !StringUtils.isEmpty(temperaturaPressaoListas.getPressao()[i])) {
						temperaturaPressaoMedicao.setPressaoAtmosferica(Util.converterCampoStringParaValorBigDecimal(
								TemperaturaPressaoMedicao.PRESSAO_ATMOSFERICA, temperaturaPressaoListas.getPressao()[i],
								Constantes.FORMATO_VALOR_NUMERO, request.getLocale()));
					} else {
						temperaturaPressaoMedicao.setPressaoAtmosferica(null);
					}

					if (temperaturaPressaoListas.getIdUnidadePressao().length > 0
							&& temperaturaPressaoListas.getIdUnidadePressao()[i] != null
							&& temperaturaPressaoListas.getIdUnidadePressao()[i] > 0) {
						Unidade unidade = (Unidade) controladorUnidade
								.obter(temperaturaPressaoListas.getIdUnidadePressao()[i]);
						temperaturaPressaoMedicao.setUnidadePressaoAtmosferica(unidade);
					} else {
						temperaturaPressaoMedicao.setUnidadePressaoAtmosferica(null);
					}

					// Adicionar a lista apenas os
					// registro com algum dos
					// dados
					// preenchido
					if (temperaturaPressaoMedicao.getDataInicio() != null
							|| temperaturaPressaoMedicao.getTemperatura() != null
							|| temperaturaPressaoMedicao.getUnidadeTemperatura() != null
							|| temperaturaPressaoMedicao.getPressaoAtmosferica() != null
							|| temperaturaPressaoMedicao.getUnidadePressaoAtmosferica() != null) {

						temperaturaPressaoMedicao.setDadosAuditoria(dadosAuditoria);
						listaTemperaturaPressaoMedicao.add(temperaturaPressaoMedicao);
					}
				}
			}
		}
	}

	/**
	 * Método aulixiar que preenche a abrangência de um objeto
	 * TemperaturaPressaoMedicao.
	 * 
	 */
	private void preencherAreaAbrangenciaTemperaturaPressaoMedicao(TemperaturaPressaoMedicao temperaturaPressaoMedicao,
			String codigoTipoAbrangencia, Long idAbrangencia) throws GGASException {

		if (!StringUtils.isEmpty(codigoTipoAbrangencia)) {
			if (TemperaturaPressaoMedicaoVO.CODIGO_LOCALIDADE.equals(codigoTipoAbrangencia)) {
				Localidade localidade = (Localidade) controladorLocalidade.obter(idAbrangencia);
				temperaturaPressaoMedicao.setLocalidade(localidade);
			} else {
				if (TemperaturaPressaoMedicaoVO.CODIGO_MICRORREGIAO.equals(codigoTipoAbrangencia)) {
					Microrregiao microrregiao = controladorGeografico.obterMicrorregiao(idAbrangencia);
					temperaturaPressaoMedicao.setMicrorregiao(microrregiao);
				} else {
					if (TemperaturaPressaoMedicaoVO.CODIGO_MUNICIPIO.equals(codigoTipoAbrangencia)) {
						Municipio municipio = (Municipio) controladorMunicipio.obter(idAbrangencia);
						temperaturaPressaoMedicao.setMunicipio(municipio);
					} else {
						if (TemperaturaPressaoMedicaoVO.CODIGO_REGIAO.equals(codigoTipoAbrangencia)) {
							Regiao regiao = controladorGeografico.obterRegiao(idAbrangencia);
							temperaturaPressaoMedicao.setRegiao(regiao);
						} else {
							if (TemperaturaPressaoMedicaoVO.CODIGO_UNIDADE_FEDERACAO.equals(codigoTipoAbrangencia)) {
								UnidadeFederacao unidadeFederacao = controladorEndereco
										.obterUnidadeFederacao(idAbrangencia);
								temperaturaPressaoMedicao.setUnidadeFederacao(unidadeFederacao);
							}
						}
					}
				}
			}

		}

	}

	/**
	 * Método responsável por obter os dados de temperatura e pressão e conveter
	 * numa coleção de VO's.
	 * 
	 */
	private Collection<TemperaturaPressaoMedicaoVO> converterTemperaturaPressaoMedicao(
			TemperaturaPressaoListasVO temperaturaPressaoListasVO) throws GGASException {

		Collection<TemperaturaPressaoMedicaoVO> listaTemperaturaPressaoMedicaoVO = new ArrayList<TemperaturaPressaoMedicaoVO>();
		if (temperaturaPressaoListasVO.getChavePrimaria().length > 0) {
			for (int i = 0; i < temperaturaPressaoListasVO.getChavePrimaria().length; i++) {
				TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVO = new TemperaturaPressaoMedicaoVO();

				temperaturaPressaoMedicaoVO.setChavePrimaria(temperaturaPressaoListasVO.getChavePrimaria()[i]);

				if (temperaturaPressaoListasVO.getDataVigencia().length > 0
						&& !StringUtils.isEmpty(temperaturaPressaoListasVO.getDataVigencia()[i])) {
					temperaturaPressaoMedicaoVO.setDataVigencia(Util.converterCampoStringParaData("Data",
							temperaturaPressaoListasVO.getDataVigencia()[i], Constantes.FORMATO_DATA_BR));
				}
				if (temperaturaPressaoListasVO.getTemperatura().length > 0
						&& !StringUtils.isEmpty(temperaturaPressaoListasVO.getTemperatura()[i])) {
					temperaturaPressaoMedicaoVO.setTemperatura(temperaturaPressaoListasVO.getTemperatura()[i]);
				}
				if (temperaturaPressaoListasVO.getIdUnidadeTemperatura().length > 0) {
					temperaturaPressaoMedicaoVO
							.setIdUnidadeTemperatura(temperaturaPressaoListasVO.getIdUnidadeTemperatura()[i]);
				}
				if (temperaturaPressaoListasVO.getPressao().length > 0
						&& !StringUtils.isEmpty(temperaturaPressaoListasVO.getPressao()[i])) {
					temperaturaPressaoMedicaoVO.setPressao(temperaturaPressaoListasVO.getPressao()[i]);
				}
				if (temperaturaPressaoListasVO.getIdUnidadePressao().length > 0) {
					temperaturaPressaoMedicaoVO
							.setIdUnidadePressao(temperaturaPressaoListasVO.getIdUnidadePressao()[i]);
				}
				if (temperaturaPressaoListasVO.getHabilitado().length > 0) {
					temperaturaPressaoMedicaoVO.setHabilitado(temperaturaPressaoListasVO.getHabilitado()[i]);
				}

				listaTemperaturaPressaoMedicaoVO.add(temperaturaPressaoMedicaoVO);

			}
		}

		return listaTemperaturaPressaoMedicaoVO;
	}

	/**
	 * Método responsável por transformar uma lista de dados de temperatura e
	 * pressão.
	 * 
	 */
	private Collection<TemperaturaPressaoMedicaoVO> converterTemperaturaPressaoMedicao(
			Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao) throws GGASException {

		Collection<TemperaturaPressaoMedicaoVO> retorno = new ArrayList<TemperaturaPressaoMedicaoVO>();

		if (listaTemperaturaPressaoMedicao != null && !listaTemperaturaPressaoMedicao.isEmpty()) {
			for (TemperaturaPressaoMedicao temperaturaPressaoMedicao : listaTemperaturaPressaoMedicao) {
				TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVO = converterTemperaturaPressaoMedicao(
						temperaturaPressaoMedicao);
				retorno.add(temperaturaPressaoMedicaoVO);
			}
		}

		return retorno;
	}

	/**
	 * Converte um objeto TemperaturaPressaoMedicao em um
	 * TemperaturaPressaoMedicaoVO.
	 * 
	 */
	private TemperaturaPressaoMedicaoVO converterTemperaturaPressaoMedicao(
			TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws GGASException {

		TemperaturaPressaoMedicaoVO temperaturaPressaoMedicaoVO = new TemperaturaPressaoMedicaoVO();
		temperaturaPressaoMedicaoVO.setChavePrimaria(temperaturaPressaoMedicao.getChavePrimaria());
		temperaturaPressaoMedicaoVO.setDataVigencia(temperaturaPressaoMedicao.getDataInicio());

		if (temperaturaPressaoMedicao.getTemperatura() != null) {
			temperaturaPressaoMedicaoVO
					.setTemperatura(Util.formatarValorNumerico(temperaturaPressaoMedicao.getTemperatura()));
		}

		if (temperaturaPressaoMedicao.getUnidadeTemperatura() != null) {
			temperaturaPressaoMedicaoVO
					.setIdUnidadeTemperatura(temperaturaPressaoMedicao.getUnidadeTemperatura().getChavePrimaria());
		}

		if (temperaturaPressaoMedicao.getPressaoAtmosferica() != null) {
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance();
			decimalFormat.setMaximumFractionDigits(5);
			temperaturaPressaoMedicaoVO
					.setPressao(decimalFormat.format(temperaturaPressaoMedicao.getPressaoAtmosferica()).toString());
		}

		if (temperaturaPressaoMedicao.getUnidadePressaoAtmosferica() != null) {
			temperaturaPressaoMedicaoVO
					.setIdUnidadePressao(temperaturaPressaoMedicao.getUnidadePressaoAtmosferica().getChavePrimaria());
		}

		if (controladorTemperaturaPressaoMedicao
				.permitirAtualizacaoTemperaturaPressaoMedicao(temperaturaPressaoMedicao)) {
			temperaturaPressaoMedicaoVO.setHabilitado(true);
		} else {
			temperaturaPressaoMedicaoVO.setHabilitado(false);
		}

		return temperaturaPressaoMedicaoVO;
	}

	/**
	 * Carregar campos.
	 * 
	 */
	private void carregarCampos(String codigoTipoAbrangencia, Model model) throws GGASException {

		model.addAttribute(LISTA_TIPO_ABRANGENCIA, new TemperaturaPressaoMedicaoVO().TIPOS_ABRANGENCIA);
		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(INTERVALO_ANOS_POSTERIORES_DATA_ATUAL, this.intervalorAnosPosterioresDataAtual());

		model.addAttribute(LISTA_UNIDADE_TEMPERATURA, controladorUnidade.listarUnidadesTemperatura());
		model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());

		Collection<Map<String, Object>> listaAreaAbrangencia = new ArrayList<Map<String, Object>>();

		if (!StringUtils.isEmpty(codigoTipoAbrangencia)) {
			Map<String, Object> dados = null;
			if (TemperaturaPressaoMedicaoVO.CODIGO_LOCALIDADE.equals(codigoTipoAbrangencia)) {
				Collection<Localidade> listarAreaAbrangencia = controladorLocalidade.listarLocalidades();
				if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
					for (Localidade entidade : listarAreaAbrangencia) {
						dados = new HashMap<String, Object>();
						dados.put(CHAVE_PRIMARIA, entidade.getChavePrimaria());
						dados.put(DESCRICAO, entidade.getDescricao());
						listaAreaAbrangencia.add(dados);
					}
				}
			} else {
				if (TemperaturaPressaoMedicaoVO.CODIGO_MICRORREGIAO.equals(codigoTipoAbrangencia)) {
					Collection<Microrregiao> listarAreaAbrangencia = controladorGeografico.listarMicrorregioes();
					if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
						for (Microrregiao entidade : listarAreaAbrangencia) {
							dados = new HashMap<String, Object>();
							dados.put(CHAVE_PRIMARIA, entidade.getChavePrimaria());
							dados.put(DESCRICAO, entidade.getDescricao());
							listaAreaAbrangencia.add(dados);
						}
					}
				} else {
					if (TemperaturaPressaoMedicaoVO.CODIGO_MUNICIPIO.equals(codigoTipoAbrangencia)) {
						Collection<Municipio> listarAreaAbrangencia = controladorMunicipio.listarMunicipios();
						if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
							for (Municipio entidade : listarAreaAbrangencia) {
								dados = new HashMap<String, Object>();
								dados.put(CHAVE_PRIMARIA, entidade.getChavePrimaria());
								dados.put(DESCRICAO, entidade.getDescricao());
								listaAreaAbrangencia.add(dados);
							}
						}
					} else {
						if (TemperaturaPressaoMedicaoVO.CODIGO_REGIAO.equals(codigoTipoAbrangencia)) {
							Collection<Regiao> listarAreaAbrangencia = controladorGeografico.listarRegioes();
							if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
								for (Regiao entidade : listarAreaAbrangencia) {
									dados = new HashMap<String, Object>();
									dados.put(CHAVE_PRIMARIA, entidade.getChavePrimaria());
									dados.put(DESCRICAO, entidade.getDescricao());
									listaAreaAbrangencia.add(dados);
								}
							}
						} else {
							if (TemperaturaPressaoMedicaoVO.CODIGO_UNIDADE_FEDERACAO.equals(codigoTipoAbrangencia)) {
								Collection<UnidadeFederacao> listarAreaAbrangencia = controladorMunicipio
										.listarUnidadeFederacao();
								if (listarAreaAbrangencia != null && !listarAreaAbrangencia.isEmpty()) {
									for (UnidadeFederacao entidade : listarAreaAbrangencia) {
										dados = new HashMap<String, Object>();
										dados.put(CHAVE_PRIMARIA, entidade.getChavePrimaria());
										dados.put(DESCRICAO, entidade.getDescricao());
										listaAreaAbrangencia.add(dados);
									}
								}
							}
						}
					}

				}

			}
		}
		model.addAttribute("listaAreasAbrangencia", listaAreaAbrangencia);
	}

	/**
	 * Intervalo anos data.
	 * 
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Intervalor anos posteriores data atual.
	 * 
	 */
	private String intervalorAnosPosterioresDataAtual() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataAtual.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

}
