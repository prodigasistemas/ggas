/*
 * 
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 * 
 */
package br.com.ggas.web.medicao.consumo;

import java.io.Serializable;
/**
 * Classe responsável pela transferência de dados para tela de 
 * Dados de Pressão e Temperatura
 *
 */
public class TemperaturaPressaoListasVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1750978816165659652L;

	private Long[] chavePrimaria;

	private String[] dataVigencia;

	private String[] temperatura;

	private Long[] idUnidadeTemperatura;

	private String[] pressao;

	private Long[] idUnidadePressao;

	private Boolean[] habilitado;

	/**
	 * 
	 * @return chavePrimaria
	 */
	public Long[] getChavePrimaria() {
		return chavePrimaria.clone();
	}
	/**
	 * 
	 * @param chavePrimaria
	 */
	public void setChavePrimaria(Long[] chavePrimaria) {
		this.chavePrimaria = chavePrimaria.clone();
	}
	/**
	 * 
	 * @return dataVigencia
	 */
	public String[] getDataVigencia() {
		return dataVigencia.clone();
	}
	/**
	 * 
	 * @param dataVigencia
	 */
	public void setDataVigencia(String[] dataVigencia) {
		this.dataVigencia = dataVigencia.clone();
	}
	/**
	 * 
	 * @return temperatura
	 */
	public String[] getTemperatura() {
		return temperatura.clone();
	}
	/**
	 * 
	 * @param temperatura
	 */
	public void setTemperatura(String[] temperatura) {
		this.temperatura = temperatura.clone();
	}
	/**
	 * 
	 * @return idUnidadeTemperatura
	 */
	public Long[] getIdUnidadeTemperatura() {
		return idUnidadeTemperatura.clone();
	}
	/**
	 * 
	 * @param idUnidadeTemperatura
	 */
	public void setIdUnidadeTemperatura(Long[] idUnidadeTemperatura) {
		this.idUnidadeTemperatura = idUnidadeTemperatura.clone();
	}
	/**
	 * 
	 * @return pressao
	 */
	public String[] getPressao() {
		return pressao.clone();
	}
	/**
	 * 
	 * @param pressao
	 */
	public void setPressao(String[] pressao) {
		this.pressao = pressao.clone();
	}
	/**
	 * 
	 * @return idUnidadePressao
	 */
	public Long[] getIdUnidadePressao() {
		return idUnidadePressao.clone();
	}
	/**
	 * 
	 * @param idUnidadePressao
	 */
	public void setIdUnidadePressao(Long[] idUnidadePressao) {
		this.idUnidadePressao = idUnidadePressao.clone();
	}
	/**
	 * 
	 * @return habilitado
	 */
	public Boolean[] getHabilitado() {
		return habilitado.clone();
	}
	/**
	 * 
	 * @param habilitado
	 */
	public void setHabilitado(Boolean[] habilitado) {
		this.habilitado = habilitado.clone();
	}
	
}
