/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe Medidor2Action representa uma Medidor2Action no sistema.
 *
 * @since 18/09/2009
 *
 */

package br.com.ggas.web.medicao.medidor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.displaytag.pagination.PaginatedList;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ContratoPontoConsumoMedidorDTO;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MedidorVirtualVO;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * @author Pedro Silva
 *
 *         Classe responsável pelo registro de medidores no sistema
 * 
 */
@Controller
public class MedidorAction extends GenericAction {

	private static final String LISTA_CHAVE_PRIMARIA = "listaChavePrimaria";

	private static final String MEDIDOR = "medidor";

	private static final String LOCAL_ARMAZENAGEM = "idLocalArmazenagem";

	private static final String LISTA_MEDIDOR = "listaMedidor";

	private static final String EXISTE_CADASTRO_BENS = "existeCadastroBens";

	private static final String LISTA_HISTORICO_OPERACAO_MEDIDOR = "listaHistoricoOperacaoMedidor";

	private static final String LISTA_UNIDADE_PRESSAO = "listaUnidadePressao";

	private static final String HABILITADO = "habilitado";

	private static final String MEDIDORES = "medidores";

	private static final String ID_MARCA_CORRETOR = "idMarcaCorretor";

	private static final String ID_MODELO_CORRETOR = "idModeloCorretor";

	private static final String ID_TIPO = "idTipo";

	private static final String NUMERO_SERIE_CORRETOR = "numeroSerieCorretor";

	private static final String LISTA_FAIXA_TEMPERATURA_TRABALHO = "listaFaixaTemperaturaTrabalho";

	private static final String LISTA_LOCAL_ARMAZENAGEM = "listaLocalArmazenagem";

	private static final String LISTA_CAPACIDADE_MEDIDOR = "listaCapacidadeMedidor";

	private static final String LISTA_DIAMETRO_MEDIDOR = "listaDiametroMedidor";

	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";

	private static final String LISTA_MARCA_MEDIDOR = "listaMarcaMedidor";

	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";

	private static final String LISTA_FATOR_K = "listaFatorK";

	private static final String LISTA_TIPO_MEDIDOR = "listaTipoMedidor";

	private static final String REQUEST_SEM_CADASTRAMENTO_EM_LOTE = "comCadastroEmLote";

	private static final String REQUEST_COM_CADASTRAMENTO_EM_LOTE = "semCadastroEmLote";

	private static final String BLOQUEAR_ALTERACAO_MEDIDOR = "bloquearAlteracaoMedidor";

	private static final String BLOQUEAR_ALTERACAO_LOCAL_ARMAZENAGEM = "bloquearAlteracaoLocalArmazenagem";

	private static final String BLOQUEAR_ALTERACAO_NUMERO_SERIE = "bloquearAlteracaoNumeroSerie";

	private static final String LINEARIZACAO_FATOR_K = "linearizacaoFatorK";

	private static final String CONTROLE_VAZAO = "controleVazao";

	private static final String CORRETOR_TEMPERATURA = "corretorTemperatura";

	private static final String CORRETOR_PRESSAO = "corretorPressao";

	private static final String ID_TIPO_TRANSDUTOR_TEMPERATURA = "idTipoTransdutorTemperatura";

	private static final String ID_TIPO_TRANSDUTOR_PRESSAO = "idTipoTransdutorPressao";

	private static final String ID_PROTOCOLO_COMUNICACAO = "idProtocoloComunicacao";

	private static final String NUMERO_ANO_FABRICACAO = "numeroAnoFabricacao";

	private static final String CODIGO_TIPO_MOSTRADOR = "codigoTipoMostrador";

	private static final String ID_MARCA = "idMarca";

	private static final String DESCRICAO_MODELO_CORRETOR = "descricaoModeloCorretor";

	private static final String ID_PRESSAO_MAXIMA_TRANSDUTOR = "idPressaoMaximaTransdutor";

	private static final String ID_TEMPERATURA_MAXIMA_TRANSDUTOR = "idTemperaturaMaximaTransdutor";

	private static final String ID_MODELO = "idModelo";

	private static final String DATA_INSTALACAO = "dataInstalacao";

	private static final String LEITURA_INSTALACAO = "leituraInstalacao";

	private static final String LOCAL_INSTALACAO = "localInstalacao";

	private static final String PROTECAO_INSTALACAO = "protecaoInstalacao";

	private static final String FUNCIONARIO_INSTALACAO = "funcionarioInstalacao";

	private static final String MOTIVO_INSTALACAO = "motivoInstalacao";

	public static final String MODO_USO1 = "modoUso";
	private static final String MODO_USO = "modoUso";

	private static final String LISTA_NUMERO_SERIE = "listaNumeroSerie";

	private static final String LISTA_OPERACAO = "listaOperacao";

	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";
	private static final String RETORNA_ID = "retornaId";
	private static final String COMPOSICAO_VIRTUAL = "composicaoVirtual";
	private static final String PONTO_CONSUMO = "pontoConsumo";
	private static final String LOTE = "lote";

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorMedidor controladorMedidor;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorCliente controladorCliente;

	/**
	 * 
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaMedidor")
	public String exibirPesquisaMedidor(Model model) throws GGASException {
		carregarCampos(model);
		carregarCamposPesquisa(model);
		return "exibirPesquisaMedidor";
	}

	/**
	 * 
	 * @param medidor
	 * @param result
	 * @param request
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarMedidor")
	public String pesquisarMedidor(@ModelAttribute("medidorImpl") MedidorImpl medidor, BindingResult result,
			HttpServletRequest request, @RequestParam(value = "habilitado", required = false) String habilitado,
			Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		carregarCamposPesquisa(model);
		prepararFiltro(filtro, medidor, habilitado);
		adicionarFiltroPaginacao(request, filtro);
		model.addAttribute(MEDIDOR, medidor);

		model.addAttribute("habilitado", habilitado);
		Collection<ContratoPontoConsumoMedidorDTO> contratoPontoConsumoMedidorDTO = controladorMedidor.consultarContratoPontoConsumoMedidor(filtro);
		PaginatedList paginatedList = criarColecaoPaginada(request,filtro,contratoPontoConsumoMedidorDTO);
		
		model.addAttribute(MEDIDORES,paginatedList);

		return "exibirPesquisaMedidor";
	}

	/**
	 * 
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("exibirInclusaoMedidor")
	public String exibirInclusaoMedidor(Model model) throws GGASException {

		carregarCampos(model);
		return "exibirInclusaoMedidor";
	}

	/**
	 * @param medidor
	 * @param result
	 * @param dataMaximaInstalacao
	 * @param dataUltimaCalibracao
	 * @param dataAquisicao
	 * @param listaChavePrimaria
	 * @param listaOperacao
	 * @param listaNumeroSerie
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("incluirMedidor")
	public String incluirMedidor(@ModelAttribute("MedidorImpl") MedidorImpl medidor, BindingResult result,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = LISTA_CHAVE_PRIMARIA, required = false) Long[] listaChavePrimaria,
			@RequestParam(value = "listaOperacao", required = false) String[] listaOperacao,
			@RequestParam(value = "listaNumeroSerie", required = false) String[] listaNumeroSerie,
			@RequestParam(value = "medidaPressao", required = false) String medidaPressao,
			@RequestParam(value = "medidaVazaoInstantanea", required = false) String medidaVazaoInstantanea,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		request.setAttribute(LISTA_CHAVE_PRIMARIA, listaChavePrimaria);
		request.setAttribute(LISTA_OPERACAO, listaOperacao);
		request.setAttribute(LISTA_NUMERO_SERIE, listaNumeroSerie);
		medidor.setDadosAuditoria(getDadosAuditoria(request));

		if(medidaPressao != null && !medidaPressao.isEmpty()) 
			medidor.setMedidaPressao(new BigDecimal(medidaPressao.replace(",", ".")));
		if(medidaVazaoInstantanea != null && !medidaVazaoInstantanea.isEmpty())
			medidor.setMedidaVazaoInstantanea(new BigDecimal(medidaVazaoInstantanea.replace(",", ".")));
		
		retorno = "exibirInclusaoMedidor";

		carregarCampos(model);

		medidor.setComposicaoVirtual(
				montaComposicaoVirtual(listaChavePrimaria, listaNumeroSerie, listaOperacao, model));
		carregaListaMedidorVirtualPorComposicaoVirtual(medidor, model);

		try {
			validarDataAquisicao(medidor, dataAquisicao);
			validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
			validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);
			medidor.setSituacaoMedidor(controladorMedidor.obterSituacaoMedidor(Long.parseLong(
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL))));
			medidor.setHabilitado(true);
			model.addAttribute(MEDIDOR, medidor);
			controladorMedidor.inserirMedidor(medidor);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Medidor.MEDIDOR_CAMPO_STRING);
			retorno = pesquisarMedidor(medidor, result, request, String.valueOf(medidor.isHabilitado()), model);
		} catch (FormatoInvalidoException | NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(MEDIDOR, medidor);
		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("exibirDetalhamentoMedidor")
	public String exibirDetalhamentoMedidor(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		carregarCampos(model);
		MedidorImpl medidor = (MedidorImpl) controladorMedidor.obterMedidor(chavePrimaria, "unidadePressaoMaximaTrabalho");
		if (medidor != null && !(medidor.getListaInstalacaoMedidor().isEmpty())) {			
			popularInstalacaoMedidor(chavePrimaria, retornarUltimoMedidorInstalacao(medidor), model);
		}
		if(!model.containsAttribute(LISTA_MEDIDOR) && medidor.getComposicaoVirtual() != null) {			
			carregaListaMedidorVirtualPorComposicaoVirtual(medidor, model);
		}
		Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = new ArrayList<HistoricoOperacaoMedidor>();

		if (medidor != null) {
			medidor = (MedidorImpl) controladorMedidor.obterMedidor(chavePrimaria, "situacaoMedidor", "tipoMedidor");
			listaHistoricoOperacaoMedidor = controladorMedidor
					.consultarHistoricoOperacaoMedidor(chavePrimaria, null);
		}
		model.addAttribute(MEDIDOR, medidor);
		model.addAttribute("instalacaoMedidor",
				controladorMedidor.obterInstalacaoMedidorPorChave(chavePrimaria));
		model.addAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR, listaHistoricoOperacaoMedidor);
		model.addAttribute(EXISTE_CADASTRO_BENS,
				CollectionUtils.isNotEmpty(controladorIntegracao.listarIntegracaoSistemaFuncaoCadastroBens()));

		return "exibirDetalhamentoMedidor";
	}

	/**
	 * 
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("voltarHistoricoMovimentacaoMedidor")
	public String voltarHistoricoMovimentacaoMedidor(Model model) throws GGASException {

		return "exibirPesquisaHistoricoMedidor";
	}

	/**
	 * 
	 * @param medidor
	 * @param dataAquisicao
	 * @param model
	 * @throws GGASException
	 */
	private void validarDataAquisicao(MedidorImpl medidor, String dataAquisicao) throws GGASException {
		if (!"".equals(dataAquisicao) && !((dataAquisicao.replace("_", "")).length() < 8)) {
			medidor.setDataAquisicao(Util.converterCampoStringParaData(Medidor.DATA_AQUISICAO, dataAquisicao,
					Constantes.FORMATO_DATA_BR));
		}
	}
	
	/**
	 *  Retorna Ultimo Medidor Instalacao
	 * @param medidor - {@link Medidor}
	 * @return InstalacaoMedidor - {@link InstalacaoMedidor}
	 */
	public InstalacaoMedidor retornarUltimoMedidorInstalacao(Medidor medidor){
		InstalacaoMedidor medidorInstalacao = null;
		
		for(InstalacaoMedidor medInsAux : medidor.getListaInstalacaoMedidor()) {
			medidorInstalacao = medInsAux;

			if (medInsAux.isHabilitado()) {
				break;
			}
		}
		
		return medidorInstalacao;
	}

	/**
	 * 
	 * @param medidor
	 * @param dataUltimaCalibracao
	 * @param model
	 * @throws GGASException
	 */
	private void validarDataUltimaCalibracao(MedidorImpl medidor, String dataUltimaCalibracao) throws GGASException {
		if ((dataUltimaCalibracao != null) && (!"".equals(dataUltimaCalibracao))
				&& !((dataUltimaCalibracao.replace("_", "")).length() < 8)) {
			medidor.setDataUltimaCalibracao(Util.converterCampoStringParaData(Medidor.DATA_ULTIMA_CALIBRACAO,
					dataUltimaCalibracao, Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * 
	 * @param medidor
	 * @param dataMaximaInstalacao
	 * @param model
	 * @throws GGASException
	 */
	private void validarDataMaximaInstalacao(MedidorImpl medidor, String dataMaximaInstalacao) throws GGASException {
		if ((dataMaximaInstalacao != null) && (!"".equals(dataMaximaInstalacao))
				&& !((dataMaximaInstalacao.replace("_", "")).length() < 8)) {
			medidor.setDataMaximaInstalacao(Util.converterCampoStringParaData(Medidor.DATA_MAXIMA_INSTALACAO,
					dataMaximaInstalacao, Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param instalacaoMedidor
	 * @param model
	 * @throws NegocioException
	 */
	private void popularInstalacaoMedidor(Long chavePrimaria, InstalacaoMedidor instalacaoMedidor, Model model)
			throws NegocioException {

		if (instalacaoMedidor.getData() != null) {
			model.addAttribute(DATA_INSTALACAO,
					Util.converterDataParaStringSemHora(instalacaoMedidor.getData(), Constantes.FORMATO_DATA_HORA_BR));
		}
		if (instalacaoMedidor.getLeitura() != null) {
			model.addAttribute(LEITURA_INSTALACAO, instalacaoMedidor.getLeitura().toString());
		}
		if (instalacaoMedidor.getLocalInstalacao() != null
				&& instalacaoMedidor.getLocalInstalacao().getDescricao() != null) {
			model.addAttribute(LOCAL_INSTALACAO, instalacaoMedidor.getLocalInstalacao().getDescricao());
		}
		if (instalacaoMedidor.getProtecao() != null && instalacaoMedidor.getProtecao().getDescricao() != null) {
			model.addAttribute(PROTECAO_INSTALACAO, instalacaoMedidor.getProtecao().getDescricao());
		}
		if (instalacaoMedidor.getFuncionario() != null && instalacaoMedidor.getFuncionario().getNome() != null) {
			model.addAttribute(FUNCIONARIO_INSTALACAO, instalacaoMedidor.getFuncionario().getNome());
		}

		Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
				.consultarHistoricoOperacaoMedidor(chavePrimaria, null);

		// carrega a informação do motivo da instalação
		Iterator<HistoricoOperacaoMedidor> iterator = listaHistoricoOperacaoMedidor.iterator();

		while (iterator.hasNext()) {
			HistoricoOperacaoMedidor item = iterator.next();
			if (item.getOperacaoMedidor().getChavePrimaria() == OperacaoMedidor.CODIGO_INSTALACAO
					&& item.getMotivoOperacaoMedidor() != null
					&& item.getMotivoOperacaoMedidor().getDescricao() != null) {
				model.addAttribute(MOTIVO_INSTALACAO, item.getMotivoOperacaoMedidor().getDescricao());

			}
		}

		//Carrega informacoes do0 Corretor de Vazão 
		carregaInformacoesVazaoCorretor(instalacaoMedidor, model);

		// Detalhamento Ponto de consumo
		detalhamentoPontoConsumo(instalacaoMedidor, model);

	}

	private void carregaInformacoesVazaoCorretor(InstalacaoMedidor instalacaoMedidor, Model model) {
		if (instalacaoMedidor.getVazaoCorretor() != null) {

			VazaoCorretor vazaoCorretor = instalacaoMedidor.getVazaoCorretor();
			if (vazaoCorretor != null) {

				if (vazaoCorretor.getNumeroSerie() != null) {
					model.addAttribute(NUMERO_SERIE_CORRETOR, vazaoCorretor.getNumeroSerie());
				}
				if (vazaoCorretor.getMarcaCorretor() != null) {
					model.addAttribute(ID_MARCA_CORRETOR, vazaoCorretor.getMarcaCorretor().getChavePrimaria());
				}

				if (vazaoCorretor.getModelo() != null) {
					model.addAttribute(ID_MODELO_CORRETOR, vazaoCorretor.getModelo().getChavePrimaria());
					model.addAttribute(DESCRICAO_MODELO_CORRETOR, vazaoCorretor.getModelo().getDescricao());
				}

				if (vazaoCorretor.getNumeroAnoFabricacao() != null && vazaoCorretor.getNumeroAnoFabricacao() > 0) {
					model.addAttribute(NUMERO_ANO_FABRICACAO, String.valueOf(vazaoCorretor.getNumeroAnoFabricacao()));
				}
				if (vazaoCorretor.getTipoMostrador() != null) {
					model.addAttribute(CODIGO_TIPO_MOSTRADOR, vazaoCorretor.getTipoMostrador().getCodigo());
				}
				if (vazaoCorretor.getProtocoloComunicacao() != null) {
					model.addAttribute(ID_PROTOCOLO_COMUNICACAO,
							vazaoCorretor.getProtocoloComunicacao().getChavePrimaria());
					model.addAttribute("descricaoProtocoloComunicacao",
							vazaoCorretor.getProtocoloComunicacao().getDescricao());
				}
				if (vazaoCorretor.getTipoTransdutorPressao() != null) {
					model.addAttribute(ID_TIPO_TRANSDUTOR_PRESSAO,
							vazaoCorretor.getTipoTransdutorPressao().getChavePrimaria());
					model.addAttribute("descricaoTipoTransdutorPressao",
							vazaoCorretor.getTipoTransdutorPressao().getDescricao());
				}
				if (vazaoCorretor.getTipoTransdutorTemperatura() != null) {
					model.addAttribute(ID_TIPO_TRANSDUTOR_TEMPERATURA,
							vazaoCorretor.getTipoTransdutorTemperatura().getChavePrimaria());
					model.addAttribute("descricaoTransdutorTemperatura",
							vazaoCorretor.getTipoTransdutorTemperatura().getDescricao());
				}
				if (vazaoCorretor.getCorrecaoPressao() != null) {
					model.addAttribute(CORRETOR_PRESSAO, String.valueOf(vazaoCorretor.getCorrecaoPressao()));
				}
				if (vazaoCorretor.getCorrecaoTemperatura() != null) {
					model.addAttribute(CORRETOR_TEMPERATURA, String.valueOf(vazaoCorretor.getCorrecaoTemperatura()));
				}
				if (vazaoCorretor.getControleVazao() != null) {
					model.addAttribute(CONTROLE_VAZAO, String.valueOf(vazaoCorretor.getControleVazao()));
				}
				if (vazaoCorretor.getLinearizacaoFatorK() != null) {
					model.addAttribute(LINEARIZACAO_FATOR_K, String.valueOf(vazaoCorretor.getLinearizacaoFatorK()));
				}

				if (vazaoCorretor.getTipoMostrador() != null) {
					model.addAttribute("descricaoTipoMostrador", vazaoCorretor.getTipoMostrador().getDescricao());
				}
				if (vazaoCorretor.getMarcaCorretor() != null) {
					model.addAttribute("descricaoMarcaCorretor", vazaoCorretor.getMarcaCorretor().getDescricao());
				}
				if (vazaoCorretor.getPressaoMaximaTransdutor() != null
						&& vazaoCorretor.getPressaoMaximaTransdutor().getPressao() != null) {
					String valorPressaoMaxima = vazaoCorretor.getPressaoMaximaTransdutor().getPressao().toString() + " "
							+ vazaoCorretor.getPressaoMaximaTransdutor().getUnidade().getDescricaoAbreviada();
					model.addAttribute("valorPressaoMaximaTransdutor", valorPressaoMaxima);
					model.addAttribute(ID_PRESSAO_MAXIMA_TRANSDUTOR,
							vazaoCorretor.getPressaoMaximaTransdutor().getChavePrimaria());
				}
				if (vazaoCorretor.getTemperaturaMaximaTransdutor() != null
						&& vazaoCorretor.getTemperaturaMaximaTransdutor().getTemperatura() != null) {
					String valorTemperaturaMaxima = vazaoCorretor.getTemperaturaMaximaTransdutor().getTemperatura()
							.toString() + " "
							+ vazaoCorretor.getTemperaturaMaximaTransdutor().getUnidade().getDescricaoAbreviada();
					model.addAttribute("valorTemperaturaMaximaTransdutor", valorTemperaturaMaxima);
					model.addAttribute(ID_TEMPERATURA_MAXIMA_TRANSDUTOR,
							vazaoCorretor.getTemperaturaMaximaTransdutor().getChavePrimaria());
				}
			}
		}
	}

	/**
	 * 
	 * @param instalacaoMedidor
	 * @param model
	 * @throws NegocioException
	 */
	private void detalhamentoPontoConsumo(InstalacaoMedidor instalacaoMedidor, Model model) throws NegocioException {
		if (instalacaoMedidor.getPontoConsumo() != null) {
			instalacaoMedidor.setPontoConsumo(
					(PontoConsumo) controladorPontoConsumo.obter(instalacaoMedidor.getPontoConsumo().getChavePrimaria(),
							"quadraFace", "quadraFace.endereco", "quadraFace.endereco.cep"));

			model.addAttribute("descricaoPontoConsumo", instalacaoMedidor.getPontoConsumo().getDescricao());
			model.addAttribute("enderecoPontoConsumo", instalacaoMedidor.getPontoConsumo().getEnderecoFormatado());
			model.addAttribute("cepPontoConsumo",
					instalacaoMedidor.getPontoConsumo().getQuadraFace().getEndereco().getCep().getCep());
			model.addAttribute("complementoPontoConsumo",
					instalacaoMedidor.getPontoConsumo().getDescricaoComplemento());
			model.addAttribute("nomeFantasiaImovel", instalacaoMedidor.getPontoConsumo().getImovel().getNome());
			model.addAttribute("matriculaImovel",
					String.valueOf(instalacaoMedidor.getPontoConsumo().getChavePrimaria()));
			model.addAttribute("cidadeImovel", instalacaoMedidor.getPontoConsumo().getImovel().getQuadraFace()
					.getEndereco().getCep().getNomeMunicipio());
			model.addAttribute("enderecoImovel",
					instalacaoMedidor.getPontoConsumo().getImovel().getEnderecoFormatado());
		
			// Detalhamento de cliente
			Cliente cliente = controladorCliente
					.obterClientePorPontoConsumo(instalacaoMedidor.getPontoConsumo().getChavePrimaria());
			
			if (cliente != null) {
				model.addAttribute("nomeCompletoCliente", cliente.getNome());

				if (cliente.getCpf() != null) {
					model.addAttribute("documentoFormatado", cliente.getCpfFormatado());
				}
				if (cliente.getCnpj() != null) {
					model.addAttribute("documentoFormatado", cliente.getCnpjFormatado());
				}
				if (cliente.getEnderecoPrincipal() != null) {
					model.addAttribute("enderecoFormatadoCliente",
							cliente.getEnderecoPrincipal().getEnderecoFormatado());
				}
				if (cliente.getEmailPrincipal() != null) {
					model.addAttribute("emailCliente", cliente.getEmailPrincipal());
				}
			}
		}
	}

	/**
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		model.addAttribute(LISTA_FATOR_K, controladorMedidor.listarFatorK());
		model.addAttribute(LISTA_SITUACAO_MEDIDOR, controladorMedidor.listarSituacaoMedidorCadastro());
		model.addAttribute(LISTA_DIAMETRO_MEDIDOR, controladorMedidor.listarDiametroMedidor());
		model.addAttribute(LISTA_CAPACIDADE_MEDIDOR, controladorMedidor.listarCapacidadeMedidor());
		model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
		model.addAttribute(LISTA_FAIXA_TEMPERATURA_TRABALHO, controladorMedidor.listarFaixaTemperaturaTrabalho());
		model.addAttribute(LISTA_UNIDADE_PRESSAO, controladorUnidade.listarUnidadesPressao());

		model.addAttribute(REQUEST_SEM_CADASTRAMENTO_EM_LOTE, Medidor.SEM_LOTE);
		model.addAttribute(REQUEST_COM_CADASTRAMENTO_EM_LOTE, Medidor.COM_LOTE);

		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));

		model.addAttribute("listaAnosFabricacao", Util.listarAnos((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO)));
		model.addAttribute("intervaloAnosData", intervaloAnosData());
		
	}

	/**
	 * 
	 * @return String
	 * @throws GGASException
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String intervaloAnosData = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		intervaloAnosData = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return intervaloAnosData;
	}

	private String montaComposicaoVirtual(Long[] listaChavePrimaria, String[] listaNumeroSerie,
			String[] listaOperacao, Model model) {

		List<MedidorVirtualVO> listaMedidorVirtual = adicionaMedidoresIndependenteALista(listaChavePrimaria,
				listaNumeroSerie, listaOperacao);
		int i = 1;
		StringBuilder builder = new StringBuilder();
		for (MedidorVirtualVO medidorVirtualVO : listaMedidorVirtual) {
			builder.append(medidorVirtualVO.getOperacao());
			builder.append(medidorVirtualVO.getId());
			if (i < listaMedidorVirtual.size()) {
				builder.append('#');
			}
			i++;
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCamposPesquisa(Model model) throws GGASException {

		Collection<IntegracaoSistemaFuncao> listaIsfCadastroBens = controladorIntegracao
				.listarIntegracaoSistemaFuncaoCadastroBens();

		model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
		model.addAttribute(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		model.addAttribute(EXISTE_CADASTRO_BENS, CollectionUtils.isNotEmpty(listaIsfCadastroBens));
	}

	/**
	 * @param retornaId
	 * @param composicaoVirtual
	 * @param request 
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("exibirPesquisaMedidorPopup")
	public String exibirPesquisaMedidorPopup(@RequestParam(value = "retornaId", required = false) boolean retornaId,
			@RequestParam(value = "lote", required = false) boolean lote,
			@RequestParam(value = "pontoConsumo", required = false) Long pontoConsumo,
			@RequestParam(value = "composicaoVirtual", required = false) String composicaoVirtual, HttpServletRequest request, Model model)
			throws GGASException {
		model.addAttribute(RETORNA_ID, retornaId);
		model.addAttribute(COMPOSICAO_VIRTUAL, composicaoVirtual);
		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute(LOTE, lote);
		carregarCamposPesquisa(model);
		return "/exibirPesquisaMedidorPopup";
	}

	/**
	 * @param medidor
	 * @param result
	 * @param habilitado
	 * @param modoUso
	 * @param retornaId
	 * @param composicaoVirtual
	 * @param listaChavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarMedidorPopup")
	public String pesquisarMedidorPopup(@ModelAttribute("MedidorImpl") MedidorImpl medidor, BindingResult result,
			@RequestParam("habilitado") String habilitado, @RequestParam("modoUso") String modoUso,
			@RequestParam("retornaId") String retornaId, @RequestParam("composicaoVirtual") String composicaoVirtual,
			@RequestParam("pontoConsumo") String pontoConsumo, @RequestParam("lote") boolean lote,
			@RequestParam(value = LISTA_CHAVE_PRIMARIA, required = false) Long[] listaChavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, medidor, habilitado);
		adicionarFiltroPaginacao(request, filtro);
		Collection<Medidor> listaMedidor;
		if (composicaoVirtual != null && ("true".equals(composicaoVirtual) && listaChavePrimaria != null)) {
			listaMedidor = controladorMedidor.consultarMedidor(filtro);
			removerAdicionados(listaMedidor, listaChavePrimaria);
		} else {
			listaMedidor = controladorMedidor.consultarMedidorDisponivel(filtro);
		}
		carregarCamposPesquisa(model);
		model.addAttribute(LISTA_MEDIDOR, super.criarColecaoPaginada(request, filtro, listaMedidor));
		model.addAttribute(MEDIDOR, medidor);
		model.addAttribute(MODO_USO1, modoUso);
		model.addAttribute(RETORNA_ID, retornaId);
		model.addAttribute(COMPOSICAO_VIRTUAL, composicaoVirtual);
		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute(LOTE, lote);
		return "/exibirPesquisaMedidorPopup";
	}

	/**
	 * @param filtro
	 * @param medidor
	 * @param habilitado
	 */
	private void prepararFiltro(Map<String, Object> filtro, MedidorImpl medidor, String habilitado) {

		if (medidor.getNumeroSerie() != null && !"".equals(medidor.getNumeroSerie())) {
			filtro.put("numeroSerieParcial", medidor.getNumeroSerie().trim());
		}
		if (medidor.getTipoMedidor() != null) {
			filtro.put(ID_TIPO, medidor.getTipoMedidor().getChavePrimaria());
		}
		if (medidor.getMarcaMedidor() != null) {
			filtro.put(ID_MARCA, medidor.getMarcaMedidor().getChavePrimaria());
		}
		if (medidor.getModelo() != null) {
			filtro.put(ID_MODELO, medidor.getModelo().getChavePrimaria());
		}
		if (medidor.getLocalArmazenagem() != null) {
			filtro.put(LOCAL_ARMAZENAGEM, medidor.getLocalArmazenagem().getChavePrimaria());
		}
		if (!"null".equals(habilitado) && habilitado != null) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}
		if (medidor.getModoUso() != null) {
			filtro.put(MODO_USO, medidor.getModoUso().getChavePrimaria());
		}
	}

	/**
	 * @param medidor
	 * @param result
	 * @param listaChavePrimaria
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("exibirAtualizarMedidor")
	public String exibirAtualizarMedidor(@ModelAttribute("MedidorImpl") MedidorImpl medidor, BindingResult result,
			@RequestParam(value = LISTA_CHAVE_PRIMARIA, required = false) Long[] listaChavePrimaria, Model model)
			throws GGASException {

		medidor = (MedidorImpl) controladorMedidor.obter(medidor.getChavePrimaria(), "situacaoMedidor", "tipoMedidor");

		Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
				.consultarHistoricoOperacaoMedidor(medidor.getChavePrimaria(), null);

		if (medidor.getSituacaoMedidor() != null) {
			model.addAttribute("idSituacao", medidor.getSituacaoMedidor());
		}

		if(!model.containsAttribute(LISTA_MEDIDOR) && medidor.getComposicaoVirtual() != null) {			
			carregaListaMedidorVirtualPorComposicaoVirtual(medidor, model);
		}

		model.addAttribute(MEDIDOR, medidor);
		model.addAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR, listaHistoricoOperacaoMedidor);
		carregarCampos(model);

		if (!controladorMedidor.permiteAlteracaoSituacaoMedidor(medidor.getChavePrimaria())) {
			Collection<SituacaoMedidor> situacoesMedidor = new ArrayList<SituacaoMedidor>();
			situacoesMedidor.add(medidor.getSituacaoMedidor());
			model.addAttribute(LISTA_SITUACAO_MEDIDOR, situacoesMedidor);
			model.addAttribute(BLOQUEAR_ALTERACAO_MEDIDOR, true);
		}

		if (!controladorMedidor.permiteAlteracaoNumeroSerieMedidor(medidor.getChavePrimaria())) {
			model.addAttribute(BLOQUEAR_ALTERACAO_NUMERO_SERIE, true);
		}

		MedidorLocalArmazenagem localArmazenagem = null;

		if (medidor.getLocalArmazenagem() != null) {
			localArmazenagem = medidor.getLocalArmazenagem();
		}

		if (localArmazenagem != null) {
			String codigoLocalArmazenagemCliente = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_CODIGO_LOCAL_ARMAZENAGEM_CLIENTE);
			if (localArmazenagem.getChavePrimaria() == Long.parseLong(codigoLocalArmazenagemCliente)) {
				model.addAttribute(BLOQUEAR_ALTERACAO_LOCAL_ARMAZENAGEM, true);
			} else {
				Collection<MedidorLocalArmazenagem> locaisArmazenagem = new ArrayList<MedidorLocalArmazenagem>();
				for (MedidorLocalArmazenagem mla : controladorMedidor.listarLocalArmazenagem()) {
					if (mla.getChavePrimaria() != Long.parseLong(codigoLocalArmazenagemCliente)) {
						locaisArmazenagem.add(mla);
					}
				}
				model.addAttribute(LISTA_LOCAL_ARMAZENAGEM, locaisArmazenagem);
			}
		}

		return "exibirAlteracaoMedidor";
	}

	/**
	 * 
	 * @param medidor
	 * @param result
	 * @param dataMaximaInstalacao
	 * @param dataUltimaCalibracao
	 * @param dataAquisicao
	 * @param listaChavePrimaria
	 * @param listaOperacao
	 * @param listaNumeroSerie
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("alterarMedidor")
	public String alterarMedidor(@ModelAttribute("MedidorImpl") MedidorImpl medidor, BindingResult result,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = LISTA_CHAVE_PRIMARIA, required = false) Long[] listaChavePrimaria,
			@RequestParam(value = "listaOperacao", required = false) String[] listaOperacao,
			@RequestParam(value = "listaNumeroSerie", required = false) String[] listaNumeroSerie,
			@RequestParam(value = "medidaPressao", required = false) String medidaPressao,
			@RequestParam(value = "medidaVazaoInstantanea", required = false) String medidaVazaoInstantanea,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		request.setAttribute(LISTA_CHAVE_PRIMARIA, listaChavePrimaria);
		request.setAttribute(LISTA_OPERACAO, listaOperacao);
		request.setAttribute(LISTA_NUMERO_SERIE, listaNumeroSerie);
		medidor.setDadosAuditoria(getDadosAuditoria(request));
		
		if(medidaPressao != null) {
			if(!medidaPressao.isEmpty()) {
				medidor.setMedidaPressao(new BigDecimal(medidaPressao.replace(",", ".")));
			}
		}
		if(medidaVazaoInstantanea != null) {
			if(!medidaVazaoInstantanea.isEmpty()) {
				medidor.setMedidaVazaoInstantanea(new BigDecimal(medidaVazaoInstantanea.replace(",", ".")));
			}
		}
		carregarCampos(model);

		retorno = "exibirAlteracaoMedidor";
		try {
			validarDataAquisicao(medidor, dataAquisicao);
			validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
			validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);
			medidor.setComposicaoVirtual(
					montaComposicaoVirtual(listaChavePrimaria, listaNumeroSerie, listaOperacao, model));
			carregaListaMedidorVirtualPorComposicaoVirtual(medidor, model);
			model.addAttribute(MEDIDOR, medidor);
			medidor.setIndicadorLote(0);
			controladorMedidor.atualizarMedidor(medidor, medidor.getLocalArmazenagem());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Medidor.MEDIDOR_CAMPO_STRING);
			retorno = pesquisarMedidor(medidor, null, request, String.valueOf(medidor.isHabilitado()), model);
		} catch (FormatoInvalidoException | NegocioException e) {
			if (!controladorMedidor.permiteAlteracaoSituacaoMedidor(medidor.getChavePrimaria())) {
				Collection<SituacaoMedidor> situacoesMedidor = new ArrayList<SituacaoMedidor>();
				situacoesMedidor.add(medidor.getSituacaoMedidor());
				model.addAttribute(LISTA_SITUACAO_MEDIDOR, situacoesMedidor);
				model.addAttribute(BLOQUEAR_ALTERACAO_MEDIDOR, true);
			}

			if (!controladorMedidor.permiteAlteracaoNumeroSerieMedidor(medidor.getChavePrimaria())) {
				model.addAttribute(BLOQUEAR_ALTERACAO_NUMERO_SERIE, true);
			}
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("removerMedidor")
	public String removerMedidor(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		getDadosAuditoria(request);
		try {
			controladorMedidor.validarRemoverMedidores(chavesPrimarias);
			controladorMedidor.removerMedidores(chavesPrimarias);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Medidor.MEDIDORES);
		} catch (NegocioException | FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarMedidor";
	}

	/**
	 * @param medidor
	 * @param result
	 * @param chavePrimaria
	 * @param listaChavePrimaria
	 * @param listaNumeroSerie
	 * @param listaOperacao
	 * @param dataAquisicao
	 * @param dataUltimaCalibracao
	 * @param dataMaximaInstalacao
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("carregarListaMedidorDoAlterar")
	public String carregarListaMedidorDoAlterar(@ModelAttribute("MedidorImpl") MedidorImpl medidor,
			BindingResult result, @RequestParam("idMedidorVirtual") Long chavePrimaria, Long[] listaChavePrimaria,
			String[] listaNumeroSerie, String[] listaOperacao, @RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao, Model model)
			throws GGASException {

		validarDataAquisicao(medidor, dataAquisicao);
		validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
		validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);

		model.addAttribute(LISTA_OPERACAO, listaOperacao);
		model.addAttribute(LISTA_NUMERO_SERIE, listaNumeroSerie);
		model.addAttribute(LISTA_CHAVE_PRIMARIA, listaChavePrimaria);
		carregarListaMedidor(chavePrimaria, listaChavePrimaria, listaNumeroSerie, listaOperacao, model);
		model.addAttribute(MEDIDOR, medidor);
		return exibirAtualizarMedidor(medidor, result, listaChavePrimaria, model);
	}

	/**
	 * 
	 * @param medidor
	 * @param result
	 * @param chavePrimaria
	 * @param listaChavePrimaria
	 * @param listaNumeroSerie
	 * @param listaOperacao
	 * @param dataAquisicao
	 * @param dataUltimaCalibracao
	 * @param dataMaximaInstalacao
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerItemListaMedidorDoInserir")
	public String removerItemListaMedidorDoInserir(@ModelAttribute("MedidorImpl") MedidorImpl medidor,
			BindingResult result, @RequestParam(value = "medidorVirtualVO", required = false) Long chavePrimaria,
			Long[] listaChavePrimaria, String[] listaNumeroSerie, String[] listaOperacao,
			@RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao, Model model)
			throws GGASException {

		validarDataAquisicao(medidor, dataAquisicao);
		validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
		validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);

		model.addAttribute("medidor", medidor);
		removerItemListaMedidor(chavePrimaria, listaChavePrimaria, listaNumeroSerie, listaOperacao, model);
		return "forward:exibirInclusaoMedidor";
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param listaChavePrimaria
	 * @param listaNumeroSerie
	 * @param listaOperacao
	 * @param dataAquisicao
	 * @param dataUltimaCalibracao
	 * @param dataMaximaInstalacao
	 * @param model
	 * @param medidor
	 * @param result
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerItemListaMedidorDoAlterar")
	public String removerItemListaMedidorDoAlterar(@ModelAttribute("MedidorImpl") MedidorImpl medidor,
			BindingResult result, @RequestParam(value = "medidorVirtualVO", required = false) Long chavePrimaria,
			Long[] listaChavePrimaria, String[] listaNumeroSerie, String[] listaOperacao,
			@RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao, Model model)
			throws GGASException {

		validarDataAquisicao(medidor, dataAquisicao);
		validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
		validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);

		removerItemListaMedidor(chavePrimaria, listaChavePrimaria, listaNumeroSerie, listaOperacao, model);
		model.addAttribute(MEDIDOR, medidor);
		
		return exibirAtualizarMedidor(medidor, result, listaChavePrimaria, model);
	}

	/**
	 * 
	 * @param medidor
	 * @param result
	 * @param model
	 * @throws GGASException
	 */
	private void carregaListaMedidorVirtualPorComposicaoVirtual(MedidorImpl medidor, Model model) throws GGASException {

		if (medidor.getComposicaoVirtual() != null && !"".equals(medidor.getComposicaoVirtual())) {
			String[] arrayComposicaoVirtual = medidor.getComposicaoVirtual().split("#");
			Long[] arrayIdMedidor = new Long[arrayComposicaoVirtual.length];
			String[] arrayOperadorMedidor = new String[arrayComposicaoVirtual.length];
			String[] arrayNumeroSerie = new String[arrayComposicaoVirtual.length];

			for (int i = 0; i < arrayComposicaoVirtual.length; i++) {
				Long idMedidor = Long.valueOf(arrayComposicaoVirtual[i].substring(1));
				Medidor medidorAux = controladorMedidor.obterMedidor(idMedidor);

				arrayIdMedidor[i] = idMedidor;
				arrayOperadorMedidor[i] = (String.valueOf(arrayComposicaoVirtual[i].charAt(0)));
				arrayNumeroSerie[i] = medidorAux.getNumeroSerie();
			}

			List<MedidorVirtualVO> listaMedidorVirtualVO = criaListaMedidorVirtualVO(arrayIdMedidor, arrayNumeroSerie,
					arrayOperadorMedidor);
			model.addAttribute(LISTA_MEDIDOR, listaMedidorVirtualVO);
		}
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @throws GGASException
	 */
	private void carregarListaMedidor(Long chavePrimaria, Long[] listaChavePrimaria, String[] listaNumeroSerie,
			String[] listaOperacao, Model model) throws GGASException {

		MedidorVirtualVO medidorVirtualVO = populaMedidorVirtual(chavePrimaria);

		List<MedidorVirtualVO> listaMedidorVO = adicionaMedidoresIndependenteALista(listaChavePrimaria,
				listaNumeroSerie, listaOperacao);
		listaMedidorVO.add(medidorVirtualVO);

		model.addAttribute(LISTA_MEDIDOR, listaMedidorVO);
	}

	/**
	 * 
	 * @param request
	 * @return List
	 */
	private List<MedidorVirtualVO> adicionaMedidoresIndependenteALista(Long[] chavesPrimarias,
			String[] listaNumeroSerie, String[] listaOperacao) {

		return criaListaMedidorVirtualVO(chavesPrimarias, listaNumeroSerie, listaOperacao);
	}

	/**
	 * 
	 * @param listaChavePrimaria
	 * @param listaNumeroSerie
	 * @param listaOperacao
	 * @return List
	 */
	private List<MedidorVirtualVO> criaListaMedidorVirtualVO(Long[] listaChavePrimaria, String[] listaNumeroSerie,
			String[] listaOperacao) {

		List<MedidorVirtualVO> listaMedidorVO = new ArrayList<MedidorVirtualVO>();
		if (listaOperacao != null && listaOperacao.length > 0) {
			for (int i = 0; i < listaOperacao.length; i++) {
				MedidorVirtualVO medidorAux = new MedidorVirtualVO();
				medidorAux.setId(listaChavePrimaria[i]);
				medidorAux.setNumeroSerie(listaNumeroSerie[i]);
				medidorAux.setOperacao(listaOperacao[i]);
				listaMedidorVO.add(medidorAux);
			}
		}
		return listaMedidorVO;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @return MedidorVirtualVO
	 * @throws GGASException
	 */
	private MedidorVirtualVO populaMedidorVirtual(Long chavePrimaria) throws GGASException {

		MedidorImpl medidorVirtual = (MedidorImpl) controladorMedidor.obterMedidor(chavePrimaria);
		MedidorVirtualVO medidorVirtualVO = new MedidorVirtualVO();
		medidorVirtualVO.setId(medidorVirtual.getChavePrimaria());
		medidorVirtualVO.setNumeroSerie(medidorVirtual.getNumeroSerie());

		return medidorVirtualVO;
	}

	/**
	 * @param medidor
	 * @param result
	 * @param chavePrimaria
	 * @param listaChavePrimaria
	 * @param listaNumeroSerie
	 * @param listaOperacao
	 * @param dataAquisicao
	 * @param dataUltimaCalibracao
	 * @param dataMaximaInstalacao
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("carregarListaMedidorDoInserir")
	public String carregarListaMedidorDoInserir(@ModelAttribute("MedidorImpl") MedidorImpl medidor,
			BindingResult result, @RequestParam("idMedidorVirtual") Long chavePrimaria,
			@RequestParam(value = LISTA_CHAVE_PRIMARIA, required = false) Long[] listaChavePrimaria,
			@RequestParam(value = "listaNumeroSerie", required = false) String[] listaNumeroSerie,
			@RequestParam(value = "listaOperacao", required = false) String[] listaOperacao,
			@RequestParam("dataAquisicao") String dataAquisicao,
			@RequestParam(value = "dataUltimaCalibracao", required = false) String dataUltimaCalibracao,
			@RequestParam(value = "dataMaximaInstalacao", required = false) String dataMaximaInstalacao,
			HttpServletRequest request, Model model) throws GGASException {

		validarDataAquisicao(medidor, dataAquisicao);
		validarDataMaximaInstalacao(medidor, dataMaximaInstalacao);
		validarDataUltimaCalibracao(medidor, dataUltimaCalibracao);

		model.addAttribute(LISTA_OPERACAO, listaOperacao);
		model.addAttribute(LISTA_NUMERO_SERIE, listaNumeroSerie);
		model.addAttribute(LISTA_CHAVE_PRIMARIA, listaChavePrimaria);
		model.addAttribute(MEDIDOR, medidor);
		carregarListaMedidor(chavePrimaria, listaChavePrimaria, listaNumeroSerie, listaOperacao, model);
		return "forward:exibirInclusaoMedidor";
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @throws NegocioException
	 */
	private void removerItemListaMedidor(Long chavePrimaria, Long[] listaChavePrimaria, String[] listaNumeroSerie,
			String[] listaOperacao, Model model) throws NegocioException {

		List<MedidorVirtualVO> listaMedidorVO = adicionaMedidoresIndependenteALista(listaChavePrimaria,
				listaNumeroSerie, listaOperacao);
		int index = 0;
		for (int i = 0; i < listaMedidorVO.size(); i++) {
			if ((listaMedidorVO.get(i).getId()).equals(chavePrimaria)) {
				index = i;
			}
		}
		listaMedidorVO.remove(index);
		if (listaMedidorVO.isEmpty()) {
			listaMedidorVO = null;
		}
		model.addAttribute(LISTA_MEDIDOR, listaMedidorVO);
	}

	/**
	 * Remove os medidores da listaMedidor que foram adicionados na página.
	 *
	 * @param listaMedidor
	 *            a lista de medidores
	 * @param dynaForm
	 *            o formulário contendo medidores já adicionados
	 */
	private void removerAdicionados(Collection<Medidor> listaMedidor, Long[] listaChavePrimaria) {
		Arrays.sort(listaChavePrimaria);
		Iterator<Medidor> iterador = listaMedidor.iterator();
		while (iterador.hasNext()) {
			Medidor medidor = iterador.next();
			if (Arrays.binarySearch(listaChavePrimaria, medidor.getChavePrimaria()) >= 0) {
				iterador.remove();
			}
		}
	}

}
