package br.com.ggas.web.medicao.consumo;

/**
 * Classe responsável pela representação de valores de lista da faixa de PCS City Gate.
 * 
 * @author mgermano
 *
 */
public class FaixaCityGateMedicaoVo {

	private String[] dataMedicao;
	private String[] valorSupridora;
	private String[] valorDistribuidora;
	private String[] valorFixo;
	private String[] volumeSupridora;
	private String[] volumeDistribuidora;
	private Boolean[] habilitado;

	public String[] getDataMedicao() {
		String[] retorno = null;
		if(dataMedicao != null){
			retorno = dataMedicao.clone();
		}
		return retorno;
	}

	public void setDataMedicao(String[] dataMedicao) {
		if (dataMedicao != null) {
			this.dataMedicao = dataMedicao.clone();
		}
	}

	public String[] getValorSupridora() {
		String[] retorno = null;
		if (this.valorSupridora != null) {
			retorno = this.valorSupridora.clone();
		}
		return retorno;
	}

	public void setValorSupridora(String[] valorSupridora) {
		if (valorSupridora != null) {
			this.valorSupridora = valorSupridora.clone();
		}
	}

	public String[] getValorDistribuidora() {
		String[] retorno = null;
		if (this.valorDistribuidora != null) {
			retorno = this.valorDistribuidora.clone();
		}

		return retorno;
	}

	public void setValorDistribuidora(String[] valorDistribuidora) {
		if (valorDistribuidora != null) {
			this.valorDistribuidora = valorDistribuidora.clone();
		}
	}

	public String[] getValorFixo() {
		String[] retorno = null;
		if (valorFixo != null) {
			retorno = valorFixo.clone();
		}
		return retorno;
	}

	public void setValorFixo(String[] valorFixo) {

		if (valorFixo != null) {
			this.valorFixo = valorFixo.clone();
		}
	}

	public String[] getVolumeSupridora() {
		String[] retorno = null;
		if (volumeSupridora != null) {
			retorno = volumeSupridora.clone();
		}
		return retorno;
	}

	public void setVolumeSupridora(String[] volumeSupridora) {
		if (volumeSupridora != null) {
			this.volumeSupridora = volumeSupridora.clone();
		}
	}

	public String[] getVolumeDistribuidora() {
		String[] retorno = null;
		if (volumeDistribuidora != null) {
			retorno = volumeDistribuidora.clone();
		}
		return retorno;
	}

	public void setVolumeDistribuidora(String[] volumeDistribuidora) {
		if (volumeDistribuidora != null) {
			this.volumeDistribuidora = volumeDistribuidora.clone();
		}
	}

	public Boolean[] getHabilitado() {
		Boolean[] retorno = null;
		if (habilitado != null) {
			retorno = habilitado.clone();
		}
		return retorno;
	}

	public void setHabilitado(Boolean[] habilitado) {
		if (habilitado != null) {
			this.habilitado = habilitado.clone();
		}
	}
}
