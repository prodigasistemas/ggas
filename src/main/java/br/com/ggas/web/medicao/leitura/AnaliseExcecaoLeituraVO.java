package br.com.ggas.web.medicao.leitura;

import java.io.Serializable;

/**
 * 
 * @author pedro Classe VO responsável pela transferência de dados entre os
 *         métodos relacionados as telas de análise de exceção de leitura.
 */
public class AnaliseExcecaoLeituraVO implements Serializable {

	private static final long serialVersionUID = 6378188376316187230L;

	private Long[] idsRota;
	private Long[] idsAnormalidadeConsumo;
	private Long[] idsAnormalidadeLeitura;
	private String numeroOcorrenciaAnormalidadeConsumo;
	private String percentualVariacaoConsumo;
	private Boolean indicadorImpedeFaturamento;
	private Boolean indicadorCondominio;
	private Boolean pontoSemConsumo;
	private Boolean comOcorrencia;
	private Boolean analisada;
	private Boolean modificouLeituraAnterior;
	private Boolean alterarHistoricoMedicaoDiario;
	private String dataLeituraInicio;
	private String complementoImovel;
	private String matriculaImovel;
	private String dataLeituraFim;
	private String consumoLido;
	private String numeroImovel;
	private String nomeImovel;
	private String cepImovel;
	private String gerarLog;
	private String volumeApurado;
	private String creditoVolume;
	private String informadoCliente;
	private String valorLeituraMedida;
	private String dataLeituraAnterior;
	private String valorLeituraAnterior;
	private String dataRealizacaoLeitura;
	private String comentarioAlteracaoLeitura;
	private String descricaoSituacao;
	private String numeroSerieMedidor;
	private String anoMesLeituraCiclo;
	private String dataLeituraFaturada;
	private String valorLeituraFaturada;
	private String descricaoAnormalidadeFaturada;
	private String anoMesLeitura;
	private String numeroCiclo;
	private Long idCliente;
	private Long idTipoConsumo;
	private Long pontoConsumoLegado;
	private Long idGrupoFaturamento;
	private Long idHistoricoMedicao;
	private Long idHistoricoConsumo;
	private Long idSetorComercial;
	private Long chavePrimaria;
	private Long idLocalidade;
	private Long idSegmento;
	private Long idLeiturista;
	private Long idSituacaoPontoConsumo;
	private Long idAnormalidadeLeituraIdentificada;
	private Long idPontoConsumo;
	private Boolean executarConsistir;

	public Long[] getIdsRota() {
		Long[] idsRotaTmp = null;
		if (idsRota != null) {
			idsRotaTmp = idsRota.clone();
		}
		return idsRotaTmp;
	}

	public void setIdsRota(Long[] idsRota) {
		if (idsRota != null) {
			this.idsRota = idsRota.clone();
		} else {
			this.idsRota = null;
		}
	}

	public Long[] getIdsAnormalidadeConsumo() {
		Long[] idsAnormalidadeConsumoTmp = null;
		if (idsAnormalidadeConsumo != null) {
			idsAnormalidadeConsumoTmp = idsAnormalidadeConsumo.clone();
		}
		return idsAnormalidadeConsumoTmp;
	}

	public void setIdsAnormalidadeConsumo(Long[] idsAnormalidadeConsumo) {
		if (idsAnormalidadeConsumo != null) {
			this.idsAnormalidadeConsumo = idsAnormalidadeConsumo.clone();
		} else {
			this.idsAnormalidadeConsumo = null;
		}
	}

	public Long[] getIdsAnormalidadeLeitura() {
		Long[] idsAnormalidadeLeituraTmp = null;
		if (idsAnormalidadeLeitura != null) {
			idsAnormalidadeLeituraTmp = idsAnormalidadeLeitura.clone();
		}
		return idsAnormalidadeLeituraTmp;
	}

	public void setIdsAnormalidadeLeitura(Long[] idsAnormalidadeLeitura) {
		if (idsAnormalidadeLeitura != null) {
			this.idsAnormalidadeLeitura = idsAnormalidadeLeitura.clone();
		} else {
			this.idsAnormalidadeLeitura = null;
		}
	}
	
	/**
	 * @return anoMesLeitura {@link String}
	 */
	public String getAnoMesLeitura() {
		return anoMesLeitura;
	}
	/**
	 * @param anoMesLeitura {@link String}
	 */
	public void setAnoMesLeitura(String anoMesLeitura) {
		this.anoMesLeitura = anoMesLeitura;
	}


	public String getNumeroOcorrenciaAnormalidadeConsumo() {
		return numeroOcorrenciaAnormalidadeConsumo;
	}

	public void setNumeroOcorrenciaAnormalidadeConsumo(String numeroOcorrenciaAnormalidadeConsumo) {
		this.numeroOcorrenciaAnormalidadeConsumo = numeroOcorrenciaAnormalidadeConsumo;
	}

	public String getPercentualVariacaoConsumo() {
		return percentualVariacaoConsumo;
	}

	public void setPercentualVariacaoConsumo(String percentualVariacaoConsumo) {
		this.percentualVariacaoConsumo = percentualVariacaoConsumo;
	}

	public Boolean getIndicadorImpedeFaturamento() {
		return indicadorImpedeFaturamento;
	}

	public void setIndicadorImpedeFaturamento(Boolean indicadorImpedeFaturamento) {
		this.indicadorImpedeFaturamento = indicadorImpedeFaturamento;
	}
	
	public String getNumeroCiclo() {
		return numeroCiclo;
	}
	public void setNumeroCiclo(String numeroCiclo) {
		this.numeroCiclo = numeroCiclo;
	}


	public Boolean getIndicadorCondominio() {
		return indicadorCondominio;
	}

	public void setIndicadorCondominio(Boolean indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}

	public Boolean getPontoSemConsumo() {
		return pontoSemConsumo;
	}

	public void setPontoSemConsumo(Boolean pontoSemConsumo) {
		this.pontoSemConsumo = pontoSemConsumo;
	}

	public Boolean getComOcorrencia() {
		return comOcorrencia;
	}

	public void setComOcorrencia(Boolean comOcorrencia) {
		this.comOcorrencia = comOcorrencia;
	}

	public Boolean getAnalisada() {
		return analisada;
	}

	public void setAnalisada(Boolean analisada) {
		this.analisada = analisada;
	}

	public Boolean getModificouLeituraAnterior() {
		return modificouLeituraAnterior;
	}

	public void setModificouLeituraAnterior(Boolean modificouLeituraAnterior) {
		this.modificouLeituraAnterior = modificouLeituraAnterior;
	}

	public Boolean getAlterarHistoricoMedicaoDiario() {
		return alterarHistoricoMedicaoDiario;
	}

	public void setAlterarHistoricoMedicaoDiario(Boolean alterarHistoricoMedicaoDiario) {
		this.alterarHistoricoMedicaoDiario = alterarHistoricoMedicaoDiario;
	}
	
	public String getDataLeituraInicio() {
		return dataLeituraInicio;
	}

	public void setDataLeituraInicio(String dataLeituraInicio) {
		this.dataLeituraInicio = dataLeituraInicio;
	}

	public String getComplementoImovel() {
		return complementoImovel;
	}

	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}

	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	public String getDataLeituraFim() {
		return dataLeituraFim;
	}

	public void setDataLeituraFim(String dataLeituraFim) {
		this.dataLeituraFim = dataLeituraFim;
	}

	public String getConsumoLido() {
		return consumoLido;
	}

	public void setConsumoLido(String consumoLido) {
		this.consumoLido = consumoLido;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getNomeImovel() {
		return nomeImovel;
	}

	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}

	public String getCepImovel() {
		return cepImovel;
	}

	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}

	public String getGerarLog() {
		return gerarLog;
	}

	public void setGerarLog(String gerarLog) {
		this.gerarLog = gerarLog;
	}

	public String getVolumeApurado() {
		return volumeApurado;
	}

	public void setVolumeApurado(String volumeApurado) {
		this.volumeApurado = volumeApurado;
	}

	public String getCreditoVolume() {
		return creditoVolume;
	}

	public void setCreditoVolume(String creditoVolume) {
		this.creditoVolume = creditoVolume;
	}

	public String getInformadoCliente() {
		return informadoCliente;
	}

	public void setInformadoCliente(String informadoCliente) {
		this.informadoCliente = informadoCliente;
	}

	public String getValorLeituraMedida() {
		return valorLeituraMedida;
	}

	public void setValorLeituraMedida(String valorLeituraMedida) {
		this.valorLeituraMedida = valorLeituraMedida;
	}

	public String getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}

	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}

	public String getValorLeituraAnterior() {
		return valorLeituraAnterior;
	}

	public void setValorLeituraAnterior(String valorLeituraAnterior) {
		this.valorLeituraAnterior = valorLeituraAnterior;
	}

	public String getDataRealizacaoLeitura() {
		return dataRealizacaoLeitura;
	}

	public void setDataRealizacaoLeitura(String dataRealizacaoLeitura) {
		this.dataRealizacaoLeitura = dataRealizacaoLeitura;
	}

	public String getComentarioAlteracaoLeitura() {
		return comentarioAlteracaoLeitura;
	}

	public void setComentarioAlteracaoLeitura(String comentarioAlteracaoLeitura) {
		this.comentarioAlteracaoLeitura = comentarioAlteracaoLeitura;
	}

	public String getDescricaoSituacao() {
		return descricaoSituacao;
	}

	public void setDescricaoSituacao(String descricaoSituacao) {
		this.descricaoSituacao = descricaoSituacao;
	}

	public String getNumeroSerieMedidor() {
		return numeroSerieMedidor;
	}

	public void setNumeroSerieMedidor(String numeroSerieMedidor) {
		this.numeroSerieMedidor = numeroSerieMedidor;
	}

	public String getAnoMesLeituraCiclo() {
		return anoMesLeituraCiclo;
	}

	public void setAnoMesLeituraCiclo(String anoMesLeituraCiclo) {
		this.anoMesLeituraCiclo = anoMesLeituraCiclo;
	}

	public String getDataLeituraFaturada() {
		return dataLeituraFaturada;
	}

	public void setDataLeituraFaturada(String dataLeituraFaturada) {
		this.dataLeituraFaturada = dataLeituraFaturada;
	}

	public String getValorLeituraFaturada() {
		return valorLeituraFaturada;
	}

	public void setValorLeituraFaturada(String valorLeituraFaturada) {
		this.valorLeituraFaturada = valorLeituraFaturada;
	}

	public String getDescricaoAnormalidadeFaturada() {
		return descricaoAnormalidadeFaturada;
	}

	public void setDescricaoAnormalidadeFaturada(String descricaoAnormalidadeFaturada) {
		this.descricaoAnormalidadeFaturada = descricaoAnormalidadeFaturada;
	}
	
	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdTipoConsumo() {
		return idTipoConsumo;
	}

	public void setIdTipoConsumo(Long idTipoConsumo) {
		this.idTipoConsumo = idTipoConsumo;
	}

	public Long getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}

	public void setPontoConsumoLegado(Long pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}

	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	public Long getIdHistoricoMedicao() {
		return idHistoricoMedicao;
	}

	public void setIdHistoricoMedicao(Long idHistoricoMedicao) {
		this.idHistoricoMedicao = idHistoricoMedicao;
	}

	public Long getIdHistoricoConsumo() {
		return idHistoricoConsumo;
	}

	public void setIdHistoricoConsumo(Long idHistoricoConsumo) {
		this.idHistoricoConsumo = idHistoricoConsumo;
	}

	public Long getIdSetorComercial() {
		return idSetorComercial;
	}

	public void setIdSetorComercial(Long idSetorComercial) {
		this.idSetorComercial = idSetorComercial;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Long getIdLocalidade() {
		return idLocalidade;
	}

	public void setIdLocalidade(Long idLocalidade) {
		this.idLocalidade = idLocalidade;
	}

	public Long getIdSegmento() {
		return idSegmento;
	}

	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	public Long getIdLeiturista() {
		return idLeiturista;
	}

	public void setIdLeiturista(Long idLeiturista) {
		this.idLeiturista = idLeiturista;
	}

	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}

	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}

	public Long getIdAnormalidadeLeituraIdentificada() {
		return idAnormalidadeLeituraIdentificada;
	}

	public void setIdAnormalidadeLeituraIdentificada(Long idAnormalidadeLeituraIdentificada) {
		this.idAnormalidadeLeituraIdentificada = idAnormalidadeLeituraIdentificada;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public Boolean getExecutarConsistir() {
		return executarConsistir;
	}

	public void setExecutarConsistir(Boolean executarConsistir) {
		this.executarConsistir = executarConsistir;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
