/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.medidor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MedidorVirtualVO;
import br.com.ggas.medicao.medidor.MovimentacaoMedidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.leitura.PontoConsumoVO;
/**
 * Classe resonsável pelas telas relaciondas as consultas do histórico de instalações de medidor
 *
 */
@Controller
public class ConsultarHistoricoInstalacaoMedidorAction extends GenericAction {

	private static final String EXIBIR_DETALHAMENTO_HISTORICO_MEDIDOR_PONTO_CONSUMO = "exibirDetalhamentoHistoricoMedidorPontoConsumo";

	private static final String CONSULTAR_HISTORICO_INSTALACAO_MEDIDOR_VO = "consultarHistoricoInstalacaoMedidorVO";

	private static final String LISTA_HISTORICO_OPERACAO_MEDIDOR = "listaHistoricoOperacaoMedidor";

	private static final String LISTA_HISTORICO_OPERACAO_MEDIDOR_VO = "listaHistoricoOperacaoMedidorVO";

	private static final String HABILITADO = "habilitado";

	private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String LISTA_MARCA_MEDIDOR = "listaMarcaMedidor";

	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";

	private static final String LISTA_TIPO_MEDIDOR = "listaTipoMedidor";

	private static final String LAZY_ROTA_GRUPO_FATURAMENTO = "rota.grupoFaturamento";

	private static final String LAZY_ROTA_EMPRESA = "rota.empresa";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String IMOVEL = "imovel";

	private static final String CHAVES_PRIMARIAS_IMOVEIS = "chavesPrimariasImoveis";

	private static final String LISTA_PONTOS_CONSUMO = "listaPontosConsumo";

	private static final String[] LAZY_LISTA_PONTO_CONSUMO = 
				{"listaPontoConsumo", "quadraFace", "quadraFace.endereco", "quadraFace.endereco.cep"};

	private static final String ID_MEDIDOR = "idMedidor";

	private static final String NOME = "nome";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";

	private static final String ID_MARCA = "idMarca";

	private static final String ID_MODELO = "idModelo";

	private static final String ID_TIPO = "idTipo";

	private static final String NUMERO_SERIE = "numeroSerie";

	private static final String PONTO_CONSUMO_LEGADO = "pontoConsumoLegado";

	private static final String MEDIDOR_VIRTUAL = "medidorVirtual";	

	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";
	
	private static final String LISTA_MEDIDOR = "listaMedidor";
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	/**
	 * Método responsável por exibir a tela de
	 * Pesquisa de histórico de instalação do
	 * medidor.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param model - {@link Model}
	 * @return exibirPesquisaHistoricoMedidor - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaHistoricoMedidor")
	public String exibirPesquisaHistoricoMedidor(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO, Model model)
			throws GGASException {

		model.addAttribute(CONSULTAR_HISTORICO_INSTALACAO_MEDIDOR_VO, consultarHistoricoInstalacaoMedidorVO);

		try {
			model.addAttribute(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
			model.addAttribute(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
			model.addAttribute(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "exibirPesquisaHistoricoMedidor";
	}

	/**
	 * Pesquisar historico medidor.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaHistoricoMedidor - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarHistoricoMedidor")
	public String pesquisarHistoricoMedidor(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {

			prepararFiltroMedidor(filtro, consultarHistoricoInstalacaoMedidorVO);
			super.adicionarFiltroPaginacao(request, filtro);

			Collection<Medidor> medidores = controladorMedidor.consultarMedidor(filtro);

			model.addAttribute("medidores", super.criarColecaoPaginada(request, filtro, medidores));

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(CONSULTAR_HISTORICO_INSTALACAO_MEDIDOR_VO, consultarHistoricoInstalacaoMedidorVO);

		return exibirPesquisaHistoricoMedidor(consultarHistoricoInstalacaoMedidorVO, model);

	}

	/**
	 * Pesquisar historico medidor imovel.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirPesquisaHistoricoMedidor - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarHistoricoMedidorImovel")
	public String pesquisarHistoricoMedidorImovel(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {

			this.prepararFiltro(filtro, consultarHistoricoInstalacaoMedidorVO);
			super.adicionarFiltroPaginacao(request, filtro);
			Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);
			model.addAttribute("listaImoveis", super.criarColecaoPaginada(request, filtro, listaImoveis));

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		
		model.addAttribute("consultarHistoricoInstalacaoMedidorVO", consultarHistoricoInstalacaoMedidorVO);

		return exibirPesquisaHistoricoMedidor(consultarHistoricoInstalacaoMedidorVO, model);
	}

	/**
	 * Exibir detalhamento historico medidor.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirDetalhamentoHistoricoMedidor - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoHistoricoMedidor")
	public String exibirDetalhamentoHistoricoMedidor(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) {

		Map<String, Object> filtro2 = new HashMap<String, Object>();
		Long idMedidor = consultarHistoricoInstalacaoMedidorVO.getIdMedidor();
		filtro2.put(ID_MEDIDOR, consultarHistoricoInstalacaoMedidorVO.getIdMedidor());

		Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = new ArrayList<HistoricoOperacaoMedidor>();
		Collection<MovimentacaoMedidor> listaHistoricoMovimentoMedidor = new ArrayList<MovimentacaoMedidor>();
		Collection<HistoricoOperacaoMedidorVO> listaHistoricoOperacaoMedidorVO = new ArrayList<HistoricoOperacaoMedidorVO>();
		Collection<HistoricoMovimentoMedidorVO> listaHistoricoMovimentoMedidorVO = new ArrayList<HistoricoMovimentoMedidorVO>();
		Collection<MedidorVirtualVO> listaComposicaoVirtual = new ArrayList<MedidorVirtualVO>();

		Medidor medidor = null;

		try {

			if (idMedidor != null && idMedidor > 0) {

				medidor = controladorMedidor.obterMedidor(idMedidor, "situacaoMedidor", "tipoMedidor", "modoUso");
				listaHistoricoOperacaoMedidor = controladorMedidor.consultarHistoricoOperacaoMedidor(medidor.getChavePrimaria(), null);
				listaHistoricoMovimentoMedidor = controladorMedidor.consultarMovimentacaoMedidor(filtro2);

				if (listaHistoricoOperacaoMedidor != null && !listaHistoricoOperacaoMedidor.isEmpty()) {
					controladorMedidor.verificarListaHistoricoOperacaoMedidor(listaHistoricoOperacaoMedidor);
				}

				if (listaHistoricoOperacaoMedidor != null) {
					for (HistoricoOperacaoMedidor historicoOperacaoMedidor : listaHistoricoOperacaoMedidor) {
						HistoricoOperacaoMedidorVO historico = montarHistoricoOperacaoMedidorVO(historicoOperacaoMedidor);
						listaHistoricoOperacaoMedidorVO.add(historico);
					}
				}

				for (MovimentacaoMedidor movimentoMedidor : listaHistoricoMovimentoMedidor) {
					HistoricoMovimentoMedidorVO movimento = new HistoricoMovimentoMedidorVO();

					movimento.setDataMovimento(movimentoMedidor.getDataMovimento());
					movimento.setDescricaoParecer(movimento.getDescricaoParecer());
					movimento.setLocalArmazenagemDestino(movimentoMedidor.getLocalArmazenagemDestino());
					movimento.setMotivoMovimentacaoMedidor(movimento.getMotivoMovimentacaoMedidor());
					movimento.setLocalArmazenagemOrigem(movimento.getLocalArmazenagemOrigem());

					listaHistoricoMovimentoMedidorVO.add(movimento);
				}

				listaComposicaoVirtual = controladorMedidor.obterComposicaoMedidorVirtual(medidor.getComposicaoVirtual());
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("medidor", medidor);
		model.addAttribute(CONSULTAR_HISTORICO_INSTALACAO_MEDIDOR_VO, consultarHistoricoInstalacaoMedidorVO);
		model.addAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR_VO, listaHistoricoOperacaoMedidorVO);
		model.addAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR, listaHistoricoOperacaoMedidor);

		model.addAttribute("listaHistoricoMovimentoMedidorVO", listaHistoricoMovimentoMedidorVO);
		model.addAttribute("listaHistoricoMovimentoMedidorVO", listaHistoricoMovimentoMedidor);

		model.addAttribute(MODO_USO_INDEPENDENTE,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addAttribute(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addAttribute(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		model.addAttribute(LISTA_MEDIDOR, listaComposicaoVirtual);

		return "exibirDetalhamentoHistoricoMedidor";
	}

	/**
	 * Montar historico operacao medidor vo.
	 *
	 * @param historicoOperacaoMedidor - {@link HistoricoOperacaoMedidor}
	 * @return historico - {@link HistoricoOperacaoMedidorVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private HistoricoOperacaoMedidorVO montarHistoricoOperacaoMedidorVO(HistoricoOperacaoMedidor historicoOperacaoMedidor)
			throws GGASException {

		HistoricoOperacaoMedidorVO historico = new HistoricoOperacaoMedidorVO();

		historico.setOperacaoMedidor(historicoOperacaoMedidor.getOperacaoMedidor());
		historico.setDataPlanejada(historicoOperacaoMedidor.getDataPlanejada());
		historico.setDataRealizada(historicoOperacaoMedidor.getDataRealizada());
		historico.setDescricao(historicoOperacaoMedidor.getDescricao());

		if (historicoOperacaoMedidor.getPontoConsumo() != null) {
			historico.setDescricaoPontoConsumo(descricaoFormatada(historicoOperacaoMedidor.getPontoConsumo()));
			historico.setPontoConsumo(historicoOperacaoMedidor.getPontoConsumo());
		}

		historico.setMedidor(historicoOperacaoMedidor.getMedidor());
		historico.setFuncionario(historicoOperacaoMedidor.getFuncionario());
		historico.setNumeroLeitura(historicoOperacaoMedidor.getNumeroLeitura());
		historico.setUnidadePressaoAnterior(historicoOperacaoMedidor.getUnidadePressaoAnterior());
		historico.setMedidaPressaoAnterior(historicoOperacaoMedidor.getMedidaPressaoAnterior());
		historico.setMedidorLocalInstalacao(historicoOperacaoMedidor.getMedidorLocalInstalacao());
		historico.setMotivoOperacaoMedidor(historicoOperacaoMedidor.getMotivoOperacaoMedidor());

		return historico;
	}

	/**
	 * Exibir detalhamento historico medidor imovel.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoHistoricoMedidorImovel")
	public String exibirDetalhamentoHistoricoMedidorImovel(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		String view = EXIBIR_DETALHAMENTO_HISTORICO_MEDIDOR_PONTO_CONSUMO;

		Long idImovel = consultarHistoricoInstalacaoMedidorVO.getIdImovel();
		Long idMedidor = consultarHistoricoInstalacaoMedidorVO.getIdMedidor();
		Imovel imovel = null;

		Collection<PontoConsumo> listaPontosConsumo;
		Collection<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();

		try {

			if (idImovel != null && idImovel > 0) {
				imovel = (Imovel) controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO);
				listaPontosConsumo = this.consultarPontosConsumoImovel(imovel);
				controladorImovel.validarImovelSemPontoConsumo(listaPontosConsumo);

				if (!listaPontosConsumo.isEmpty()) {
					listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumo);

					if (listaPontosConsumoVO != null && listaPontosConsumoVO.size() == 1) {
						consultarHistoricoInstalacaoMedidorVO
								.setIdPontoConsumo(listaPontosConsumoVO.iterator().next().getPontoConsumo().getChavePrimaria());
						consultarHistoricoInstalacaoMedidorVO.setIdImovel(imovel.getChavePrimaria());
						
						view = this.exibirHistoricoInstalacaoMedidorPontoConsumo(consultarHistoricoInstalacaoMedidorVO, bindingResult,
								request, model);
					}
				}
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = pesquisarHistoricoMedidorImovel(consultarHistoricoInstalacaoMedidorVO, bindingResult, request, model);
		}

		model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
		model.addAttribute("consultarHistoricoInstalacaoMedidorVO", consultarHistoricoInstalacaoMedidorVO);
		consultarHistoricoInstalacaoMedidorVO.setIdMedidor(idMedidor);
		model.addAttribute(IMOVEL, imovel);

		return view;
	}

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de Medidor.
	 * 
	 * @param filtro - {@link Map}
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	private void prepararFiltroMedidor(Map<String, Object> filtro,
			ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO) throws FormatoInvalidoException {

		String numeroSerie = consultarHistoricoInstalacaoMedidorVO.getNumeroSerie();
		if (StringUtils.isNotEmpty(numeroSerie)) {
			filtro.put(NUMERO_SERIE, numeroSerie.trim());
		}
		Long idTipo = consultarHistoricoInstalacaoMedidorVO.getIdTipo();
		if (idTipo != null && idTipo > 0) {
			filtro.put(ID_TIPO, idTipo);
		}
		Long idMarca = consultarHistoricoInstalacaoMedidorVO.getIdMarca();
		if (idMarca != null && idMarca > 0) {
			filtro.put(ID_MARCA, idMarca);
		}
		Long idModelo = consultarHistoricoInstalacaoMedidorVO.getIdModelo();
		if (idModelo != null && idModelo > 0) {
			filtro.put(ID_MODELO, idModelo);
		}
		String medidorVirtual = consultarHistoricoInstalacaoMedidorVO.getMedidorVirtual();
		if (StringUtils.isNotEmpty(medidorVirtual)) {
			filtro.put(MEDIDOR_VIRTUAL, Boolean.parseBoolean(medidorVirtual));
		}
	}

	/**
	 * Método responsável por exibir o resultado
	 * da pesquisa de Imóvel.
	 * 
	 * @param filtro - {@link Map}
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO)
			throws FormatoInvalidoException {

		String matricula = consultarHistoricoInstalacaoMedidorVO.getMatricula();
		if (StringUtils.isNotEmpty(matricula)) {
			filtro.put(CHAVE_PRIMARIA, Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matricula));
		}

		String complementoImovel = consultarHistoricoInstalacaoMedidorVO.getComplementoImovel();
		if (StringUtils.isNotEmpty(complementoImovel)) {
			filtro.put(COMPLEMENTO_IMOVEL, complementoImovel);
		}

		String numeroImovel = consultarHistoricoInstalacaoMedidorVO.getNumeroImovel();
		if (!StringUtils.isEmpty(numeroImovel)) {
			filtro.put(NUMERO_IMOVEL, numeroImovel);
		}

		String cep = consultarHistoricoInstalacaoMedidorVO.getCepImovel();
		if (StringUtils.isNotEmpty(cep)) {
			filtro.put(CEP_IMOVEL, cep);
		}

		String indicadorCondominio = consultarHistoricoInstalacaoMedidorVO.getIndicadorCondominio();
		if (StringUtils.isNotEmpty(indicadorCondominio)) {
			filtro.put(INDICADOR_CONDOMINIO, Boolean.valueOf(indicadorCondominio));
		}

		String indicadorCondominioAmbos = consultarHistoricoInstalacaoMedidorVO.getIndicadorCondominioAmbos();
		if (StringUtils.isNotEmpty(indicadorCondominioAmbos)) {
			filtro.put(INDICADOR_CONDOMINIO_AMBOS, indicadorCondominioAmbos);
		}

		Boolean habilitado = consultarHistoricoInstalacaoMedidorVO.getHabilitado();
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		String nome = consultarHistoricoInstalacaoMedidorVO.getNome();
		if (StringUtils.isNotEmpty(nome)) {
			filtro.put(NOME, nome);
		}

		String idPontoConsumoLegado = consultarHistoricoInstalacaoMedidorVO.getPontoConsumoLegado();
		if (StringUtils.isNotEmpty(idPontoConsumoLegado) && idPontoConsumoLegado != null) {
			filtro.put(PONTO_CONSUMO_LEGADO, idPontoConsumoLegado);
		}
	}

	/**
	 * Método responsável por exibir o histórico
	 * de instalação do medidor.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirDetalhamentoHistoricoMedidorPontoConsumo - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirHistoricoMedidorPontoConsumo")
	public String exibirHistoricoMedidorPontoConsumo(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Long idPontoConsumo = consultarHistoricoInstalacaoMedidorVO.getIdPontoConsumo();
		Long idImovel = consultarHistoricoInstalacaoMedidorVO.getIdImovel();

		PontoConsumo pontoConsumo = null;

		if (idPontoConsumo != null && idPontoConsumo > 0) {

			pontoConsumo = controladorPontoConsumo.buscarPontoConsumo(idPontoConsumo);
			model.addAttribute(PONTO_CONSUMO, pontoConsumo);

			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor =
					controladorMedidor.consultarHistoricoOperacaoMedidor(null, idPontoConsumo);
			request.setAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR, listaHistoricoOperacaoMedidor);

		}

		Imovel imovel = null;
		if (idImovel != null && idImovel > 0) {
			imovel = (Imovel) controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO);
			request.setAttribute(IMOVEL, imovel);
			Collection<PontoConsumo> listaPontosConsumo = this.consultarPontosConsumoImovel(imovel);
			Collection<PontoConsumoVO> listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumo);

			if (listaPontosConsumoVO != null && !listaPontosConsumoVO.isEmpty()) {
				request.setAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
			}
		}

		return EXIBIR_DETALHAMENTO_HISTORICO_MEDIDOR_PONTO_CONSUMO;
	}

	/**
	 * Método responsável por exibir o histórico
	 * de instalacao do medidor do ponto de
	 * consumo.
	 * 
	 * @param consultarHistoricoInstalacaoMedidorVO - {@link ConsultarHistoricoInstalacaoMedidorVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return exibirDetalhamentoHistoricoMedidorPontoConsumo - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirHistoricoInstalacaoMedidorPontoConsumo")
	public String exibirHistoricoInstalacaoMedidorPontoConsumo(ConsultarHistoricoInstalacaoMedidorVO consultarHistoricoInstalacaoMedidorVO,
			BindingResult bindingResult, HttpServletRequest request, Model model) throws GGASException {

		Long idPontoConsumo = consultarHistoricoInstalacaoMedidorVO.getIdPontoConsumo();
		Long idImovel = consultarHistoricoInstalacaoMedidorVO.getIdImovel();

		PontoConsumo pontoConsumo = null;

		if (idPontoConsumo != null && idPontoConsumo > 0) {

			pontoConsumo = controladorPontoConsumo.buscarPontoConsumo(idPontoConsumo);
			model.addAttribute(PONTO_CONSUMO, pontoConsumo);

			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor =
					controladorMedidor.consultarHistoricoOperacaoMedidor(null, idPontoConsumo);
			request.setAttribute(LISTA_HISTORICO_OPERACAO_MEDIDOR, listaHistoricoOperacaoMedidor);

		}

		Imovel imovel = null;
		if (idImovel != null && idImovel > 0) {
			imovel = (Imovel) controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO);
			request.setAttribute(IMOVEL, imovel);
			Collection<PontoConsumo> listaPontosConsumo = this.consultarPontosConsumoImovel(imovel);
			Collection<PontoConsumoVO> listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumo);

			if (listaPontosConsumoVO != null && !listaPontosConsumoVO.isEmpty()) {
				request.setAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
			}
		}

		return EXIBIR_DETALHAMENTO_HISTORICO_MEDIDOR_PONTO_CONSUMO;
	}

	/**
	 * Montar lista pontos consumo vo.
	 * 
	 * @param listaPontosConsumo - {@link Collection}
	 * @return listaPontosConsumoVO - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	private Collection<PontoConsumoVO> montarListaPontosConsumoVo(Collection<PontoConsumo> listaPontosConsumo) throws GGASException {

		Collection<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			pontoVO = new PontoConsumoVO();
			PontoConsumo pontoConsumoCompleto = (PontoConsumo) controladorPontoConsumo.obter(pontoConsumo.getChavePrimaria(),
					LAZY_ROTA_GRUPO_FATURAMENTO, LAZY_ROTA_EMPRESA, "instalacaoMedidor.medidor", "situacaoConsumo");
			pontoVO.setPontoConsumo(pontoConsumoCompleto);

			Map<String, Integer> referenciaCiclo = controladorPontoConsumo.obterReferenciaCicloAtual(pontoConsumoCompleto);

			if (referenciaCiclo != null) {
				pontoVO.setCiclo(String.valueOf(referenciaCiclo.get("ciclo")));
				pontoVO.setReferencia(Util.formatarAnoMes(referenciaCiclo.get("referencia")));
			}

			listaPontosConsumoVO.add(pontoVO);
		}
		return listaPontosConsumoVO;
	}

	/**
	 * Consultar pontos consumo imovel.
	 * 
	 * @param imovel - {@link Imovel}
	 * @return listaPontosConsumo - {@link Collection}
	 * @throws GGASException - {@link GGASException}
	 */
	private Collection<PontoConsumo> consultarPontosConsumoImovel(Imovel imovel) throws GGASException {

		Collection<PontoConsumo> listaPontosConsumo = new ArrayList<PontoConsumo>();

		if (imovel != null) {
			Collection<PontoConsumo> pontosDeConsumo = controladorImovel.listarPontoConsumoPorChaveImovelAtivosInativos(imovel.getChavePrimaria());

			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}
			if (imovel.getCondominio()) {
				Collection<Imovel> listaImoveisCondominio = controladorImovel.obterImoveisFilhosDoCondominio(imovel);

				verificarMontarListaPontoConsumo(listaPontosConsumo, listaImoveisCondominio);

			}
		}

		return listaPontosConsumo;
	}

	/**
	 * Método responsável por verificar e montar Lista de Ponto deConsumo
	 * 
	 * @param listaPontosConsumo - {@link Collection}
	 * @param listaImoveisCondominio - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void verificarMontarListaPontoConsumo(Collection<PontoConsumo> listaPontosConsumo, Collection<Imovel> listaImoveisCondominio)
			throws NegocioException {

		Collection<PontoConsumo> pontosDeConsumo;

		if (listaImoveisCondominio != null && !listaImoveisCondominio.isEmpty()) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS_IMOVEIS, Util.collectionParaArrayChavesPrimarias(listaImoveisCondominio));
			pontosDeConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);
			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}
		}
	}

	/**
	 * Método responsável por exibir o histórico
	 * de instalação do medidor.
	 * 
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @return descricaoFormatada - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	public String descricaoFormatada(PontoConsumo pontoConsumo) throws GGASException {

		StringBuilder descricaoFormatada = new StringBuilder();
		String separador = "";
		String nomeImovel;

		if (pontoConsumo.getImovel().getNome() != null) {
			nomeImovel = pontoConsumo.getImovel().getNome().trim();
		} else {
			nomeImovel = "";
		}

		if (nomeImovel.length() > 0) {
			descricaoFormatada.append(pontoConsumo.getDescricaoFormatada());
		} else {
			Cliente cliente = controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria());
			if (cliente != null && StringUtils.isNotEmpty(cliente.getNome())) {
				descricaoFormatada.append(cliente.getNome());
				separador = " - ";
			}
			if (StringUtils.isNotEmpty(pontoConsumo.getDescricao())) {
				descricaoFormatada.append(separador).append(pontoConsumo.getDescricao());
				separador = " - ";
			}

			Cep cepEndereco = pontoConsumo.getCep();
			if (cepEndereco == null) {
				cepEndereco = pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep();
			}
			if (StringUtils.isNotEmpty(cepEndereco.getTipoLogradouro())) {
				descricaoFormatada.append(separador).append(cepEndereco.getTipoLogradouro());

				if (StringUtils.isEmpty(cepEndereco.getLogradouro())) {
					separador = ", ";
				} else {
					separador = " ";
				}

			}
			
			descricaoFormatadaDois(pontoConsumo, descricaoFormatada, separador, cepEndereco);
			
		}

		return descricaoFormatada.toString();

	}

	/**
	 * Método responsável por exibir o histórico
	 * de instalação do medidor.
	 * 
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @param descricaoFormatada - {@link StringBuilder}
	 * @param separador - {@link String}
	 * @param cepEndereco - {@link Cep}
	 */
	private void descricaoFormatadaDois(PontoConsumo pontoConsumo, StringBuilder descricaoFormatada, String separador, Cep cepEndereco) {

		if (StringUtils.isNotEmpty(cepEndereco.getLogradouro())) {
			descricaoFormatada.append(separador).append(cepEndereco.getLogradouro());
			separador = ", ";
		}
		if (StringUtils.isNotEmpty(pontoConsumo.getNumeroImovel())) {
			descricaoFormatada.append(separador).append(pontoConsumo.getNumeroImovel());
			separador = ", ";
		} else {
			descricaoFormatada.append(separador).append(pontoConsumo.getImovel().getNumeroImovel());
			separador = ", ";
		}
		if (StringUtils.isNotEmpty(pontoConsumo.getDescricaoComplemento())) {
			descricaoFormatada.append(separador).append(pontoConsumo.getDescricaoComplemento());
			separador = ", ";
		}
		if (StringUtils.isNotEmpty(cepEndereco.getBairro())) {
			descricaoFormatada.append(separador).append(cepEndereco.getBairro());
		}
	}
	
}
