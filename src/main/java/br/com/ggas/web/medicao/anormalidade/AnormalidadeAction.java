/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.anormalidade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.impl.ModalidadeMedicaoImovelImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.anormalidade.impl.AnormalidadeSegmentoParametroImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas a Anormalidade
 *
 */
@Controller
public class AnormalidadeAction extends GenericAction {

	private static final String ANORMALIDADE = "anormalidade";

	private static final String HABILITADO = "habilitado";
	
	private static final String ACAO = "acao";

	private static final String ATRIBUTO_FORM_CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ATRIBUTO_FORM_DESCRICAO = "descricao";

	private static final String ATRIBUTO_FORM_ID_ANORMALIDADE_SEM_CONSUMO = "idAcaoAnormalidadeSemConsumo";

	private static final String ATRIBUTO_FORM_ID_ANORMALIDADE_COM_CONSUMO = "idAcaoAnormalidadeComConsumo";

	private static final String ATRIBUTO_FORM_ID_ANORMALIDADE_SEM_LEITURA = "idAcaoAnormalidadeSemLeitura";

	private static final String ATRIBUTO_FORM_ID_ANORMALIDADE_COM_LEITURA = "idAcaoAnormalidadeComLeitura";

	private static final String LISTA_ANORMALIDADES_PARAMETROS = "listaAnormalidadeSegmentoParametros";
	
	private static final String LISTA_SEGMENTOS = "listaSegmentos";
	
	private static final String ANORMALIDADE_PARAMETRO = "anormalidadeParametro";
	
	private static final String PARAMETRO_CODIGO_ANORMALIDADE_CONSUMO_ZERO = "CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO";
	
	@Autowired
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;
	
	
	/**
	 * Método responsável por exibir a tela de pesquisar anormalidade.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirPesquisaAnormalidade")
	public String exibirPesquisaAnormalidade(HttpServletRequest request, Model model) {

		carregarCombos(model);
		request.getSession().removeAttribute(ANORMALIDADE);
		saveToken(request);
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		return "exibirPesquisaAnormalidade";
	}

	/**
	 * Método responsável realizar a pesquisa de anormalidade.
	 * 
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("pesquisarAnormalidade")
	public String pesquisarAnormalidade(AnormalidadeVO anormalidadeVO, BindingResult result,
			@RequestParam(value=HABILITADO, required=false) Boolean habilitado, HttpServletRequest request, Model model)
			throws NegocioException {

		model.addAttribute(ANORMALIDADE, anormalidadeVO);
		model.addAttribute(HABILITADO, habilitado);
		Map<String, Object> filtro = obterFiltroForm(anormalidadeVO, habilitado);

		if (anormalidadeVO.getEhTipoAnormalidadeConsumo() != null) {
			if (anormalidadeVO.getEhTipoAnormalidadeConsumo()) {
				Collection<AnormalidadeConsumo> listaAnormalidadeConsumo = controladorAnormalidade.consultarAnormalidadesConsumo(filtro);
				model.addAttribute("listaAnormalidadeConsumo", listaAnormalidadeConsumo);
			} else {
				Collection<AnormalidadeLeitura> listaAnormalidadeLeitura = controladorAnormalidade.consultarAnormalidadesLeitura(filtro);
				model.addAttribute("listaAnormalidadeLeitura", listaAnormalidadeLeitura);
			}
		}

		return exibirPesquisaAnormalidade(request, model);
	}

	/**
	 * Método responsável a tela de inclusão de anormalidade.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirInclusaoAnormalidade")
	public String exibirInclusaoAnormalidade(HttpServletRequest request, Model model) {

		carregarCombos(model);
		saveToken(request);

		return "exibirInclusaoAnormalidade";
	}

	/**
	 * Método responsável por inserir uma anormalidade.
	 * 
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("incluirAnormalidade")
	public String incluirAnormalidade(AnormalidadeVO anormalidade, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "";
		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);
			if (anormalidade.getEhTipoAnormalidadeConsumo()) {
				AnormalidadeConsumo anormalidadeConsumo = (AnormalidadeConsumo) controladorAnormalidade
						.criarAnormalidadeConsumo();
				popularAnormalidadeConsumo(anormalidadeConsumo, anormalidade);
				anormalidadeConsumo.setDadosAuditoria(getDadosAuditoria(request));
				anormalidadeConsumo.setHabilitado(true);
				controladorAnormalidade.inserir(anormalidadeConsumo);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						AnormalidadeConsumo.ANORMALIDADE_CONSUMO);
			} else {
				AnormalidadeLeitura anormalidadeLeitura = (AnormalidadeLeitura) controladorAnormalidade
						.criarAnormalidadeLeitura();
				popularAnormalidadeLeitura(anormalidadeLeitura, anormalidade);
				anormalidadeLeitura.setDadosAuditoria(getDadosAuditoria(request));
				anormalidadeLeitura.setHabilitado(true);
				controladorAnormalidade.inserir(anormalidadeLeitura);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						AnormalidadeLeitura.ANORMALIDADE_LEITURA);
			}
			view = pesquisarAnormalidade(anormalidade, result, true, request, model);
		} catch (GGASException e) {
			model.addAttribute(ANORMALIDADE, anormalidade);
			view = exibirInclusaoAnormalidade(request, model);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de atualizar anormalidade.
	 * 
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoAnormalidadeConsumo")
	public String exibirAlteracaoAnormalidadeConsumo(AnormalidadeVO anormalidadeVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoAnormalidade";
		salvarPesquisa(request, anormalidadeVO, ANORMALIDADE);
		AnormalidadeConsumo anormalidade = null;
		try {
			anormalidade = controladorAnormalidade.obterAnormalidadeConsumo(anormalidadeVO.getChavePrimaria());
			verificarSeEhAnormalidadeConsumoZeroParametrizada(anormalidade.getChavePrimaria(), request);
			popularFormAnormalidadeConsumo(request,anormalidade, model);
		} catch (GGASException e) {
			view = exibirPesquisaAnormalidade(request, model);
			mensagemErroParametrizado(model, e);
		}

		carregarCombos(model);
		carregarAnormalidadeSegmentoParametros(model, anormalidade);
		carregarSegmentos(request);
		request.setAttribute(ANORMALIDADE_PARAMETRO, new AnormalidadeSegmentoParametroImpl());
		saveToken(request);

		return view;
	}


	/**
	 * Método responsável por exibir adicionar uma paratrizacao a anormalidade.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return String {@link String}
	 */
	@RequestMapping("adicionarOuAlterarParametrizacao")
	public String adicionarOuAlterarParametrizacao(Model model, HttpServletRequest request, AnormalidadeVO anormalidadeVO,
			BindingResult bindingResult) {

		try {
			Long chavePrimaria = anormalidadeVO.getChavePrimaria();
			int indexParametrizacao = anormalidadeVO.getIndexParametrizacao();
			AnormalidadeConsumo anormalidade = controladorAnormalidade.obterAnormalidadeConsumo(chavePrimaria);
			AnormalidadeSegmentoParametro parametrizacao = null;
			if (indexParametrizacao < 0) {
				parametrizacao = new AnormalidadeSegmentoParametroImpl();
				popularAnormalidadeSegmentoParametro(anormalidade, parametrizacao, anormalidadeVO);
				controladorAnormalidade.inserir(parametrizacao);
			} else {
				List<AnormalidadeSegmentoParametro> parametros =
						new ArrayList<>(carregarAnormalidadeSegmentoParametros(null, anormalidade));
				parametrizacao = parametros.get(indexParametrizacao);
				popularAnormalidadeSegmentoParametro(anormalidade, parametrizacao, anormalidadeVO);
				controladorAnormalidade.atualizar(parametrizacao);
			}
			request.setAttribute(ACAO, Boolean.TRUE);
			carregarAnormalidadeSegmentoParametros(model, anormalidade);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "gridParametrizacao";
	}
	
	/**
	 * Método responsável para remover um anormalidadeSegmentoParametro
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return String {@link String}
	 */

	@RequestMapping("removerParametrizacao")
	public String removerParametrizacao(Model model, HttpServletRequest request, AnormalidadeVO anormalidadeVO,
			BindingResult bindingResult) {

		try {
			Long chavePrimaria = anormalidadeVO.getChavePrimaria();
			Long chavePrimariaParametrizacao = anormalidadeVO.getChavePrimariaParametrizacao();
			AnormalidadeConsumo anormalidade = controladorAnormalidade.obterAnormalidadeConsumo(chavePrimaria);
			List<AnormalidadeSegmentoParametro> parametros = new ArrayList<>(carregarAnormalidadeSegmentoParametros(null, anormalidade));
			AnormalidadeSegmentoParametro parametrizacao =
					parametros.stream().filter(p -> p.getChavePrimaria() == chavePrimariaParametrizacao).findFirst().orElse(null);

			if (parametrizacao != null) {
				controladorAnormalidade.remover(parametrizacao);
			}
			request.setAttribute(ACAO, Boolean.TRUE);
			carregarAnormalidadeSegmentoParametros(model, anormalidade);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "gridParametrizacao";
	}


	/**
	 * Método responsável por exibir a tela de atualizar anormalidade.
	 * 
	 * @param chavesPrimarias - {@link Long}
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoAnormalidadeLeitura")
	public String exibirAlteracaoAnormalidadeLeitura(@RequestParam("chavePrimaria") Long[] chavesPrimarias,
			AnormalidadeVO anormalidade, BindingResult result, HttpServletRequest request, Model model) {
		
		for(Long chave : chavesPrimarias){
			if(chave != null){
				anormalidade.setChavePrimaria(chave);
			}
		}

		String view = "exibirAlteracaoAnormalidade";
		salvarPesquisa(request, anormalidade, ANORMALIDADE);
		try {
			popularFormAnormalidadeLeitura(request, controladorAnormalidade.obterAnormalidadeLeitura(anormalidade.getChavePrimaria()), model);
		} catch (GGASException e) {
			view = exibirPesquisaAnormalidade(request, model);
			mensagemErroParametrizado(model, e);
		}
		carregarCombos(model);
		saveToken(request);

		return view;
	}

	/**
	 * Método responsável por atualizar a anormalidade.
	 * 
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarAnormalidadeConsumo")
	public String alterarAnormalidadeConsumo(AnormalidadeVO anormalidade, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "";
		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);
			AnormalidadeConsumo anormalidadeConsumo = controladorAnormalidade
					.obterAnormalidadeConsumo(anormalidade.getChavePrimaria());
			popularAnormalidadeConsumo(anormalidadeConsumo, anormalidade);
			anormalidadeConsumo.setDadosAuditoria(getDadosAuditoria(request));
			controladorAnormalidade.atualizarAnormalidadeConsumo(anormalidadeConsumo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					AnormalidadeConsumo.ANORMALIDADE_CONSUMO);
			view = pesquisarAnormalidade(anormalidade, result, anormalidade.getHabilitado(), request, model);
		} catch (GGASException e) {
			view = exibirAlteracaoAnormalidadeConsumo(anormalidade, result, request, model);
			model.addAttribute(ANORMALIDADE, anormalidade);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por atualizar a anormalidade.
	 * 
	 * @param anormalidade {@link AnormalidadeVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarAnormalidadeLeitura")
	public String alterarAnormalidadeLeitura(AnormalidadeVO anormalidade, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "";
		try {
			// Valida o token para garantir que não
			// aconteça um duplo submit.
			validarToken(request);
			AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade
					.obterAnormalidadeLeitura(anormalidade.getChavePrimaria());
			popularAnormalidadeLeitura(anormalidadeLeitura, anormalidade);
			anormalidadeLeitura.setDadosAuditoria(getDadosAuditoria(request));
			controladorAnormalidade.atualizarAnormalidadeLeitura(anormalidadeLeitura);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					AnormalidadeLeitura.ANORMALIDADE_LEITURA);
			view = pesquisarAnormalidade(anormalidade, result, anormalidade.getHabilitado(), request, model);
		} catch (GGASException e) {
			model.addAttribute(ANORMALIDADE, anormalidade);
			view = exibirAlteracaoAnormalidadeLeitura(new Long[0], anormalidade, result, request, model);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por remover uma anormalidade consumo.
	 * 
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("excluirAnormalidadeConsumo")
	public String excluirAnormalidadeConsumo(AnormalidadeVO anormalidade, BindingResult result,
			@RequestParam(ATRIBUTO_FORM_CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			validarToken(request);
			controladorAnormalidade.removerAnormalidadesConsumo(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					AnormalidadeConsumo.ANORMALIDADE_CONSUMO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarAnormalidade(anormalidade, result, anormalidade.getHabilitado(), request, model);
	}

	/**
	 * Método responsável por remover uma anormalidade leitura.
	 * 
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("excluirAnormalidadeLeitura")
	public String excluirAnormalidadeLeitura(AnormalidadeVO anormalidade, BindingResult result,
			@RequestParam(ATRIBUTO_FORM_CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			validarToken(request);
			controladorAnormalidade.removerAnormalidadesLeitura(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					AnormalidadeLeitura.ANORMALIDADE_LEITURA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarAnormalidade(anormalidade, result, anormalidade.getHabilitado(), request, model);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de anormalidade.
	 * 
	 * @param anormalidadeTmp - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link  Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoAnormalidadeConsumo")
	public String exibirDetalhamentoAnormalidadeConsumo(AnormalidadeVO anormalidadeTmp, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoAnormalidadeConsumo";
		salvarPesquisa(request, anormalidadeTmp, ANORMALIDADE);
		try {
			if (!super.isPostBack(request)) {
				AnormalidadeConsumo anormalidadeConsumo = controladorAnormalidade
						.obterAnormalidadeConsumo(anormalidadeTmp.getChavePrimaria());
				popularFormAnormalidadeConsumo(request, anormalidadeConsumo, model);
				carregarAnormalidadeSegmentoParametros(model, anormalidadeConsumo);
				verificarSeEhAnormalidadeConsumoZeroParametrizada(anormalidadeTmp.getChavePrimaria(), request);
			}
		} catch (GGASException e) {
			view = exibirPesquisaAnormalidade(request, model);
			mensagemErroParametrizado(model, request, e);
		}

		carregarCombos(model);

		saveToken(request);

		return view;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de anormalidade.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param anormalidadeTmp - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoAnormalidadeLeitura")
	public String exibirDetalhamentoAnormalidadeLeitura(@RequestParam("chavePrimaria") Long chavePrimaria,
			AnormalidadeVO anormalidadeTmp, BindingResult result, HttpServletRequest request, Model model) {

		String view = "exibirDetalhamentoAnormalidadeLeitura";
		salvarPesquisa(request, anormalidadeTmp, ANORMALIDADE);
		try {
			if (!super.isPostBack(request)) {
				AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade.obterAnormalidadeLeitura(anormalidadeTmp.getChavePrimaria());
				popularFormAnormalidadeLeitura(request, anormalidadeLeitura, model);
			}
		} catch (GGASException e) {
			view = exibirPesquisaAnormalidade(request, model);
			mensagemErroParametrizado(model, request, e);
		}
		carregarCombos(model);
		saveToken(request);

		return view;
	}

	/**
	 * Popular form anormalidade consumo.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param anormalidadeConsumo - {@link AnormalidadeConsumo}
	 * @param model - {@link Model}
	 */
	private void popularFormAnormalidadeConsumo(HttpServletRequest request,
			AnormalidadeConsumo anormalidadeConsumo, Model model) {

		if (!super.isPostBack(request)) {
			AnormalidadeVO anormalidadeVO = new AnormalidadeVO();
			anormalidadeVO.setChavePrimaria(anormalidadeConsumo.getChavePrimaria());
			anormalidadeVO.setVersao(anormalidadeConsumo.getVersao());
			anormalidadeVO.setDescricao(anormalidadeConsumo.getDescricao());
			anormalidadeVO.setDescricaoAbreviada(anormalidadeConsumo.getDescricaoAbreviada());
			anormalidadeVO.setNumeroOcorrenciaConsecutivasAnormalidade(
					anormalidadeConsumo.getNumeroOcorrenciaConsecutivasAnormalidade());
			anormalidadeVO.setMensagemConsumo(anormalidadeConsumo.getMensagemConsumo());
			anormalidadeVO.setBloquearFaturamento(anormalidadeConsumo.getBloquearFaturamento());
			anormalidadeVO.setEhTipoAnormalidadeConsumo(true);
			anormalidadeVO.setHabilitado(anormalidadeConsumo.isHabilitado());
			anormalidadeVO.setRegra(anormalidadeConsumo.getRegra());
			anormalidadeVO.setIgnorarAcaoAutomatica(anormalidadeConsumo.isIgnorarAcaoAutomatica());
			model.addAttribute(ANORMALIDADE, anormalidadeVO);
			request.setAttribute("tipoAnormalidadeConsumo", true);
		}
	}

	/**
	 * Popular form anormalidade leitura.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param anormalidadeLeitura - {@link AnormalidadeLeitura}
	 * @param model - {@link Model}
	 */
	private void popularFormAnormalidadeLeitura(HttpServletRequest request,
			AnormalidadeLeitura anormalidadeLeitura, Model model) {

		if (!super.isPostBack(request)) {
			AnormalidadeVO anormalidadeVO = new AnormalidadeVO();
			anormalidadeVO.setChavePrimaria(anormalidadeLeitura.getChavePrimaria());
			anormalidadeVO.setVersao(anormalidadeLeitura.getVersao());
			anormalidadeVO.setDescricao(anormalidadeLeitura.getDescricao());
			anormalidadeVO.setDescricaoAbreviada(anormalidadeLeitura.getDescricaoAbreviada());
			anormalidadeVO.setMensagemLeitura(anormalidadeLeitura.getMensagemLeitura());
			anormalidadeVO.setMensagemManutencao(anormalidadeLeitura.getMensagemManutencao());
			anormalidadeVO.setMensagemPrevencao(anormalidadeLeitura.getMensagemPrevencao());
			anormalidadeVO.setAceitaLeitura(anormalidadeLeitura.getAceitaLeitura());
			anormalidadeVO.setRelativoMedidor(anormalidadeLeitura.getRelativoMedidor());
			anormalidadeVO.setBloquearFaturamento(anormalidadeLeitura.getBloquearFaturamento());
			anormalidadeVO.setExigeFoto(anormalidadeLeitura.getExigeFoto());

			if (anormalidadeLeitura.getAcaoAnormalidadeSemConsumo() != null) {
				anormalidadeVO.setAcaoAnormalidadeSemConsumo(
						anormalidadeLeitura.getAcaoAnormalidadeSemConsumo().getChavePrimaria());
				model.addAttribute("descricaoAnormalidadeSemConsumo", anormalidadeLeitura.getAcaoAnormalidadeSemConsumo().getDescricao());
			}
			if (anormalidadeLeitura.getAcaoAnormalidadeComConsumo() != null) {
				anormalidadeVO.setAcaoAnormalidadeComConsumo(
						anormalidadeLeitura.getAcaoAnormalidadeComConsumo().getChavePrimaria());
				model.addAttribute("descricaoAnormalidadeComConsumo", anormalidadeLeitura.getAcaoAnormalidadeComConsumo().getDescricao());
			}
			if (anormalidadeLeitura.getAcaoAnormalidadeSemLeitura() != null) {
				anormalidadeVO.setAcaoAnormalidadeSemLeitura(
						anormalidadeLeitura.getAcaoAnormalidadeSemLeitura().getChavePrimaria());
				model.addAttribute("descricaoAnormalidadeSemLeitura", anormalidadeLeitura.getAcaoAnormalidadeSemLeitura().getDescricao());
			}
			if (anormalidadeLeitura.getAcaoAnormalidadeComLeitura() != null) {
				anormalidadeVO.setAcaoAnormalidadeComLeitura(
						anormalidadeLeitura.getAcaoAnormalidadeComLeitura().getChavePrimaria());
				model.addAttribute("descricaoAnormalidadeComLeitura", anormalidadeLeitura.getAcaoAnormalidadeComLeitura().getDescricao());
			}
			
			anormalidadeVO.setEhTipoAnormalidadeConsumo(false);
			anormalidadeVO.setHabilitado(anormalidadeLeitura.isHabilitado());
			anormalidadeVO.setRegra(anormalidadeLeitura.getRegra());
			model.addAttribute(ANORMALIDADE, anormalidadeVO);
			request.setAttribute("aceitaLeitura", anormalidadeLeitura.getAceitaLeitura());
			request.setAttribute("relativoMedidor", anormalidadeLeitura.getRelativoMedidor());
			request.setAttribute("bloquearFaturamento", anormalidadeLeitura.getBloquearFaturamento());
			request.setAttribute("tipoAnormalidadeConsumo", false);
		}
	}

	/**
	 * Popular anormalidade consumo.
	 * 
	 * @param anormalidadeConsumo - {@link AnormalidadeConsumo}
	 * @param anormalidade - {@link AnormalidadeVO}
	 */
	private void popularAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo, AnormalidadeVO anormalidade) {

		if (StringUtils.isEmpty(anormalidade.getDescricao())) {
			anormalidadeConsumo.setDescricao(null);
		} else {
			anormalidadeConsumo.setDescricao(anormalidade.getDescricao());
		}
		if (StringUtils.isEmpty(anormalidade.getDescricaoAbreviada())) {
			anormalidadeConsumo.setDescricaoAbreviada(null);
		} else {
			anormalidadeConsumo.setDescricaoAbreviada(anormalidade.getDescricaoAbreviada());
		}

		if (anormalidade.getNumeroOcorrenciaConsecutivasAnormalidade() == null) {
			anormalidadeConsumo.setNumeroOcorrenciaConsecutivasAnormalidade(null);
		} else {
			anormalidadeConsumo.setNumeroOcorrenciaConsecutivasAnormalidade(
					anormalidade.getNumeroOcorrenciaConsecutivasAnormalidade());
		}
		anormalidadeConsumo.setMensagemConsumo(anormalidade.getMensagemConsumo());

		if (anormalidade.getBloquearFaturamento() == null) {
			anormalidadeConsumo.setBloquearFaturamento(null);
		} else {
			anormalidadeConsumo.setBloquearFaturamento(anormalidade.getBloquearFaturamento());
		}
		if (anormalidade.getHabilitado() != null) {
			anormalidadeConsumo.setHabilitado(anormalidade.getHabilitado());
		}

		anormalidadeConsumo.setRegra(anormalidade.getRegra());
		anormalidadeConsumo.setIgnorarAcaoAutomatica(anormalidade.isIgnorarAcaoAutomatica());
	}
	
	private void popularAnormalidadeSegmentoParametro(AnormalidadeConsumo anormalidadeConsumo,
			AnormalidadeSegmentoParametro anormalidadeSegmentoParametro, AnormalidadeVO anormalidadeVO) throws GGASException {
		
		Integer modalidadeMedicaoCodigo = anormalidadeVO.getModalidadeMedicao();
		Long chavePrimariaSegmento = anormalidadeVO.getSegmentoAnormalidade();
		Long chaveRamoAtividade = anormalidadeVO.getRamoAtividade();
		Boolean bloqueiaFaturamento = false;
		RamoAtividade ramoAtividade = null;

		if(anormalidadeVO.getBloqueiaFaturamento() == null){
			bloqueiaFaturamento = false;
		}else{
			bloqueiaFaturamento = anormalidadeVO.getBloqueiaFaturamento();
		}
		
		ModalidadeMedicaoImovel modalidade = null;
		
		if(modalidadeMedicaoCodigo != null) {
			modalidade = new ModalidadeMedicaoImovelImpl();
			modalidade.setCodigo(modalidadeMedicaoCodigo);
		}
			
		anormalidadeSegmentoParametro.setAnormalidadeConsumo(anormalidadeConsumo);
		anormalidadeSegmentoParametro.setModalidadeMedicao(modalidade);
		anormalidadeSegmentoParametro.setSegmento(controladorSegmento.obterSegmento(chavePrimariaSegmento));
		anormalidadeSegmentoParametro.setBloqueiaFaturamento(bloqueiaFaturamento);
		if (chaveRamoAtividade > 0) {
			ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(chaveRamoAtividade);
		}
		anormalidadeSegmentoParametro.setRamoAtividade(ramoAtividade);
		
	}

	/**
	 * Popular anormalidade leitura.
	 * 
	 * @param anormalidadeLeitura - {@link AnormalidadeLeitura}
	 * @param anormalidade - {@link AnormalidadeVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura, AnormalidadeVO anormalidade)
			throws GGASException {

		if (anormalidade.getVersao() > 0) {
			anormalidadeLeitura.setVersao(anormalidade.getVersao());
		}
		if (StringUtils.isEmpty(anormalidade.getDescricao())) {
			anormalidadeLeitura.setDescricao(null);
		} else {
			anormalidadeLeitura.setDescricao(anormalidade.getDescricao());
		}
		if (StringUtils.isEmpty(anormalidade.getDescricaoAbreviada())) {
			anormalidadeLeitura.setDescricaoAbreviada(null);
		} else {
			anormalidadeLeitura.setDescricaoAbreviada(anormalidade.getDescricaoAbreviada());
		}
		if (StringUtils.isEmpty(anormalidade.getMensagemLeitura())) {
			anormalidadeLeitura.setMensagemLeitura(null);
		} else {
			anormalidadeLeitura.setMensagemLeitura(anormalidade.getMensagemLeitura());
		}
		if (StringUtils.isEmpty(anormalidade.getMensagemManutencao())) {
			anormalidadeLeitura.setMensagemManutencao(null);
		} else {
			anormalidadeLeitura.setMensagemManutencao(anormalidade.getMensagemManutencao());
		}
		if (StringUtils.isEmpty(anormalidade.getMensagemPrevencao())) {
			anormalidadeLeitura.setMensagemPrevencao(null);
		} else {
			anormalidadeLeitura.setMensagemPrevencao(anormalidade.getMensagemPrevencao());
		}
		if (anormalidade.getAceitaLeitura() == null) {
			anormalidadeLeitura.setAceitaLeitura(null);
		} else {
			anormalidadeLeitura.setAceitaLeitura(anormalidade.getAceitaLeitura());
		}
		if (anormalidade.getExigeFoto() != null) {
			anormalidadeLeitura.setExigeFoto(anormalidade.getExigeFoto());
		}

		if (anormalidade.getRelativoMedidor() == null) {
			anormalidadeLeitura.setRelativoMedidor(null);
		} else {
			anormalidadeLeitura.setRelativoMedidor(anormalidade.getRelativoMedidor());
		}
		if (anormalidade.getBloquearFaturamento() == null) {
			anormalidadeLeitura.setBloquearFaturamento(null);
		} else {
			anormalidadeLeitura.setBloquearFaturamento(anormalidade.getBloquearFaturamento());
		}
		if (anormalidade.getAcaoAnormalidadeSemConsumo() == null || anormalidade.getAcaoAnormalidadeSemConsumo() <= 0) {
			anormalidadeLeitura.setAcaoAnormalidadeSemConsumo(null);
		} else {
			AcaoAnormalidadeConsumo acaoAnormalidadeSemConsumo = controladorAnormalidade
					.obterAcaoAnormalidadeConsumo(anormalidade.getAcaoAnormalidadeSemConsumo());
			anormalidadeLeitura.setAcaoAnormalidadeSemConsumo(acaoAnormalidadeSemConsumo);
		}
		if (anormalidade.getAcaoAnormalidadeComConsumo() == null || anormalidade.getAcaoAnormalidadeComConsumo() <= 0) {
			anormalidadeLeitura.setAcaoAnormalidadeComConsumo(null);
		} else {
			AcaoAnormalidadeConsumo acaoAnormalidadeComConsumo = controladorAnormalidade
					.obterAcaoAnormalidadeConsumo(anormalidade.getAcaoAnormalidadeComConsumo());
			anormalidadeLeitura.setAcaoAnormalidadeComConsumo(acaoAnormalidadeComConsumo);
		}
		if (anormalidade.getAcaoAnormalidadeSemLeitura() == null || anormalidade.getAcaoAnormalidadeSemLeitura() <= 0) {
			anormalidadeLeitura.setAcaoAnormalidadeSemLeitura(null);
		} else {
			AcaoAnormalidadeLeitura acaoAnormalidadeSemLeitura = controladorAnormalidade
					.obterAcaoAnormalidadeLeitura(anormalidade.getAcaoAnormalidadeSemLeitura());
			anormalidadeLeitura.setAcaoAnormalidadeSemLeitura(acaoAnormalidadeSemLeitura);
		}
		if (anormalidade.getAcaoAnormalidadeComLeitura() == null || anormalidade.getAcaoAnormalidadeComLeitura() <= 0) {
			anormalidadeLeitura.setAcaoAnormalidadeComLeitura(null);
		} else {
			AcaoAnormalidadeLeitura acaoAnormalidadeComLeitura = controladorAnormalidade
					.obterAcaoAnormalidadeLeitura(anormalidade.getAcaoAnormalidadeComLeitura());
			anormalidadeLeitura.setAcaoAnormalidadeComLeitura(acaoAnormalidadeComLeitura);
		}
		if (anormalidade.getHabilitado() != null) {
			anormalidadeLeitura.setHabilitado(anormalidade.getHabilitado());
		}

		anormalidadeLeitura.setRegra(anormalidade.getRegra());
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param anormalidadeVO - {@link AnormalidadeVO}
	 * @param habilitado - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> obterFiltroForm(AnormalidadeVO anormalidadeVO, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (!StringUtils.isEmpty(anormalidadeVO.getDescricao())) {
			filtro.put(ATRIBUTO_FORM_DESCRICAO, anormalidadeVO.getDescricao());
		}
		if (anormalidadeVO.getAcaoAnormalidadeSemConsumo() != null
				&& anormalidadeVO.getAcaoAnormalidadeSemConsumo() > 0) {
			filtro.put(ATRIBUTO_FORM_ID_ANORMALIDADE_SEM_CONSUMO, anormalidadeVO.getAcaoAnormalidadeSemConsumo());
		}
		if (anormalidadeVO.getAcaoAnormalidadeComConsumo() != null
				&& anormalidadeVO.getAcaoAnormalidadeComConsumo() > 0) {
			filtro.put(ATRIBUTO_FORM_ID_ANORMALIDADE_COM_CONSUMO, anormalidadeVO.getAcaoAnormalidadeComConsumo());
		}
		if (anormalidadeVO.getAcaoAnormalidadeSemLeitura() != null
				&& anormalidadeVO.getAcaoAnormalidadeSemLeitura() > 0) {
			filtro.put(ATRIBUTO_FORM_ID_ANORMALIDADE_SEM_LEITURA, anormalidadeVO.getAcaoAnormalidadeSemLeitura());
		}
		if (anormalidadeVO.getAcaoAnormalidadeComLeitura() != null
				&& anormalidadeVO.getAcaoAnormalidadeComLeitura() > 0) {
			filtro.put(ATRIBUTO_FORM_ID_ANORMALIDADE_COM_LEITURA, anormalidadeVO.getAcaoAnormalidadeComLeitura());
		}
		
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model - {@link Model}
	 */
	private void carregarCombos(Model model) {

		model.addAttribute("listaAcaoAnormalidadeConsumoComLeitura",
				controladorAnormalidade.listarAcaoAnormalidadeConsumoComLeitura());
		model.addAttribute("listaAcaoAnormalidadeConsumoSemLeitura",
				controladorAnormalidade.listarAcaoAnormalidadeConsumoSemLeitura());
		model.addAttribute("listaAcaoAnormalidadeComLeitura",
				controladorAnormalidade.listarAcaoAnormalidadeComLeitura());
		model.addAttribute("listaAcaoAnormalidadeSemLeitura",
				controladorAnormalidade.listarAcaoAnormalidadeSemLeitura());
	}
	
	/**
	 * método responsável por recuperar filtro de pesquisa anterior
	 * 
	 * @param anormalidadeTmp - {@link AnormalidadeVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("voltaAnormalidade")
	public String voltaAnormalidade(AnormalidadeVO anormalidadeTmp, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		AnormalidadeVO anormalidade = null;
		
		if (retornaPesquisa(request, ANORMALIDADE) != null) {
			anormalidade = (AnormalidadeVO) retornaPesquisa(request, ANORMALIDADE);
			request.getSession().removeAttribute(ANORMALIDADE);
		} else {
			anormalidade = anormalidadeTmp;
		}

		return pesquisarAnormalidade(anormalidade, result, anormalidade.getHabilitado(), request, model);
	}

	private Collection<AnormalidadeSegmentoParametro> carregarAnormalidadeSegmentoParametros(Model model,
			AnormalidadeConsumo anormalidadeConsumo) throws NegocioException {
		Collection<AnormalidadeSegmentoParametro> parametros = null;

		if (anormalidadeConsumo != null) {
			parametros = controladorAnormalidade.listarAnormalidadeSegmentoParametros(anormalidadeConsumo);
			if (model != null) {
				model.addAttribute(LISTA_ANORMALIDADES_PARAMETROS, parametros);
			}
		}

		return parametros;
	}
	
	private void carregarSegmentos(HttpServletRequest request) throws GGASException {
		request.setAttribute(LISTA_SEGMENTOS, controladorSegmento.listarSegmento());
	}
	
	private void verificarSeEhAnormalidadeConsumoZeroParametrizada(Long chavePrimaria, HttpServletRequest request)
			throws  GGASException {
		Long idAnormalidadeParametrizada =
				Long.parseLong(controladorParametroSistema.obterParametroPorCodigo(PARAMETRO_CODIGO_ANORMALIDADE_CONSUMO_ZERO).getValor());
		if (chavePrimaria.equals(idAnormalidadeParametrizada)) {
			request.setAttribute(PARAMETRO_CODIGO_ANORMALIDADE_CONSUMO_ZERO, true);
		} else {
			request.setAttribute(PARAMETRO_CODIGO_ANORMALIDADE_CONSUMO_ZERO, null);
		}
	}
}
