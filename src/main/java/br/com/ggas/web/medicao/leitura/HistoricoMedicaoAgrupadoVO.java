/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.web.medicao.leitura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.medicao.leitura.HistoricoMedicao;

/**
 * Classe que agrupa valores do historicoMedicao e alguns consumos
 * como value object
 * 
 * @author desconhecido
 */
public class HistoricoMedicaoAgrupadoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2633073958996909440L;

	private HistoricoMedicao historicoMedicao;

	private BigDecimal consumoAnoAnterior;

	private BigDecimal consumoAnoAtual;

	private Date dataLeituraUltimoDiaCicloAnterior;

	private boolean existeComentario;

	/**
	 * @return the historicoMedicao
	 */
	public HistoricoMedicao getHistoricoMedicao() {

		return historicoMedicao;
	}

	/**
	 * @param historicoMedicao
	 *            the historicoMedicao to set
	 */
	public void setHistoricoMedicao(HistoricoMedicao historicoMedicao) {

		this.historicoMedicao = historicoMedicao;
	}

	/**
	 * @return the historicoConsumoAnoAnterior
	 */
	public BigDecimal getConsumoAnoAnterior() {

		return consumoAnoAnterior;
	}

	/**
	 * @param historicoConsumoAnoAnterior
	 *            the historicoConsumoAnoAnterior to set
	 */
	public void setConsumoAnoAnterior(BigDecimal consumoAnoAnterior) {

		this.consumoAnoAnterior = consumoAnoAnterior;
	}

	/**
	 * @return the historicoConsumoAnoAtual
	 */
	public BigDecimal getConsumoAnoAtual() {

		return consumoAnoAtual;
	}

	/**
	 * @param historicoConsumoAnoAtual
	 *            the historicoConsumoAnoAtual to set
	 */
	public void setConsumoAnoAtual(BigDecimal consumoAnoAtual) {

		this.consumoAnoAtual = consumoAnoAtual;
	}

	/**
	 * @return the existeComentario
	 */
	public boolean isExisteComentario() {

		return existeComentario;
	}

	/**
	 * @param existeComentario
	 *            the existeComentario to set
	 */
	public void setExisteComentario(boolean existeComentario) {

		this.existeComentario = existeComentario;
	}

	public Date getDataLeituraUltimoDiaCicloAnterior() {
		Date data = null;
		if (this.dataLeituraUltimoDiaCicloAnterior != null) {
			data = (Date) dataLeituraUltimoDiaCicloAnterior.clone();
		}
		return data;
	}

	public void setDataLeituraUltimoDiaCicloAnterior(Date dataLeituraUltimoDiaCicloAnterior) {
		this.dataLeituraUltimoDiaCicloAnterior = dataLeituraUltimoDiaCicloAnterior;
	}

}
