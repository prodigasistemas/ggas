package br.com.ggas.web.medicao.leitura;

public class HistoricoLeituraGSAVO {
	
	private String dataLeituraAtual;
	private String leituraAtual;
	private String dataLeituraAnterior;
	private String leituraAnterior;
	private String volumeFaturado;
	private String volumeMedido;
	private String tipoMedicaoManual;
	
	
	public String getDataLeituraAtual() {
		return dataLeituraAtual;
	}
	public void setDataLeituraAtual(String dataLeituraAtual) {
		this.dataLeituraAtual = dataLeituraAtual;
	}
	public String getLeituraAtual() {
		return leituraAtual;
	}
	public void setLeituraAtual(String leituraAtual) {
		this.leituraAtual = leituraAtual;
	}
	public String getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}
	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}
	public String getLeituraAnterior() {
		return leituraAnterior;
	}
	public void setLeituraAnterior(String leituraAnterior) {
		this.leituraAnterior = leituraAnterior;
	}
	public String getVolumeFaturado() {
		return volumeFaturado;
	}
	public void setVolumeFaturado(String volumeFaturado) {
		this.volumeFaturado = volumeFaturado;
	}
	public String getVolumeMedido() {
		return volumeMedido;
	}
	public void setVolumeMedido(String volumeMedido) {
		this.volumeMedido = volumeMedido;
	}
	public String getTipoMedicaoManual() {
		return tipoMedicaoManual;
	}
	public void setTipoMedicaoManual(String tipoMedicaoManual) {
		this.tipoMedicaoManual = tipoMedicaoManual;
	}

}
