/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.MedicaoHistoricoComentario;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioConsumoClienteMedidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
import br.com.ggas.web.cadastro.cliente.ClientePopupVO;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Histórico de Leitura e Consumo.
 */
@Controller
public class ConsultarHistoricoLeituraMedicaoAction extends GenericAction {
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
	private static final String EXIBIR_HISTORICO_LEITURA_CONSUMO_IMOVEL = "exibirHistoricoLeituraConsumoImovel";

	private static final String CLIENTE_POPUP_VO = "clientePopupVO";

	private static final String HISTORICO_LEITURA_CONSUMO_VO = "historicoLeituraConsumoVO";

	private static final String INFORMAR_FILTRO_CLIENTE_IMOVEL_GRUPO_FATURAMENTO = "INFORMAR_FILTRO_CLIENTE_IMOVEL_GRUPO_FATURAMENTO";

	private static final String ID_MEDIDOR = "idMedidor";

	private static final String LISTA_HIST_CONSUMO_MEDIDOR = "listaHistConsumoMedidor";

	private static final String MEDIDOR = "medidor";

	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";

	private static final String ROTA_LEITURISTA_FUNCIONARIO = "rota.leiturista.funcionario";

	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String LAZY_INSTALACAO_MEDIDOR = "instalacaoMedidor";

	private static final String LAZY_ROTA_EMPRESA = "rota.empresa";

	private static final String LAZY_ROTA_GRUPO_FATURAMENTO = "rota.grupoFaturamento";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String LISTA_HISTORICO_CONSUMO = "listaHistoricoConsumo";

	private static final String LISTA_HISTORICO_MEDICAO = "listaHistoricoMedicao";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String IMOVEL = "imovel";

	private static final String CHAVES_PRIMARIAS_IMOVEIS = "chavesPrimariasImoveis";

	private static final String LISTA_PONTOS_CONSUMO = "listaPontosConsumo";

	private static final String[] LAZY_LISTA_PONTO_CONSUMO = new String[] { "listaPontoConsumo", "quadraFace",
			"quadraFace.endereco", "quadraFace.endereco.cep", "quadra" };

	private static final String LISTA_MESES_ANO = "listaMesesAno";

	private static final String LISTA_ANOS = "listaAnos";

	private static final String LISTA_MEDICAO_HISTORICO_COMENTARIO = "listaMedicaoHistoricoComentario";

	private static final String ID_CLIENTE = "idCliente";

	private static final String NOME = "nome";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String MATRICULA = "matricula";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";

	private static final String HABILITADO = "habilitado";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_ROTA = "idRota";

	private static final String LISTA_IMOVEIS = "listaImoveis";

	private static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	private static final String PONTO_CONSUMO_LEGADO = "pontoConsumoLegado";

	private static final String SCALA_CONSUMO_APURADO = "escalaConsumoApurado";

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorRelatorioConsumoClienteMedidor controladorRelatorioConsumoClienteMedidor;

	/**
	 * Método responsável por exibir a tela de pesquisar imóvel.
	 * 
	 * @param model                     - {@link Model}
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param bindingResult				- {@link BindingResult}
	 * @return view 					- {@link String}
	 * @throws GGASException 			- {@link GGASException}
	 * 
	 */
	@RequestMapping("exibirPesquisaImovelConsultaHistoricoLeituraConsumo")
	public String exibirPesquisaImovelConsultaHistoricoLeituraConsumo(Model model, HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			BindingResult bindingResult) throws GGASException {

		this.configurarPesquisaImovelHistoricoLeituraMedicao(historicoLeituraConsumoVO, model);

		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		return "exibirPesquisaImovelConsultaHistoricoLeituraMedicao";
	}

	/**
	 * Método responsável por pesquisar todos os imoveis que possuem historico de
	 * leitura e medicao.
	 * 
	 * @param model                     - {@link Model}
	 * @param request                   - {@link HttpServletRequest}
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult             - {@link BindingResult}
	 * @return view 					- {@link String}
	 * @throws GGASException 			- {@link GGASException}
	 */
	@RequestMapping("pesquisaImovelConsultaHistoricoLeituraConsumo")
	public String pesquisaImovelConsultaHistoricoLeituraConsumo(Model model, HttpServletRequest request,
			HistoricoLeituraConsumoVO historicoLeituraConsumoVO, ClientePopupVO clientePopupVO, BindingResult bindingResult)
			throws GGASException {

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		try {
			Map<String, Object> filtro = this.obterFiltroPesquisaImovelHistoricoLeituraMedicao(historicoLeituraConsumoVO, clientePopupVO);

			this.validarCampos(historicoLeituraConsumoVO, clientePopupVO);

			super.adicionarFiltroPaginacao(request, filtro);

			Collection<Imovel> imoveis = controladorHistoricoConsumo.pesquisarImovelHistoricoLeituraConsumo(filtro);

			model.addAttribute(LISTA_IMOVEIS, imoveis);

			this.configurarPesquisaImovelHistoricoLeituraMedicao(historicoLeituraConsumoVO, model);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaImovelConsultaHistoricoLeituraConsumo(model, historicoLeituraConsumoVO, bindingResult);
	}

	/**
	 * @param historicoLeituraConsumoVO	- {@link HistoricoLeituraConsumoVO}
	 * @param model						- {@link Model}
	 * @throws GGASException			- {@link GGASException}
	 */
	private void configurarPesquisaImovelHistoricoLeituraMedicao(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, Model model)
			throws GGASException {

		model.addAttribute(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());

		Long idGrupoFaturamento = historicoLeituraConsumoVO.getIdGrupoFaturamento();
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			model.addAttribute("listaRota", controladorRota.listarRotasPorGrupoFaturamento(idGrupoFaturamento));
		}

	}

	/**
	 * Preparar filtro
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO			- {@link ClientePopupVO}
	 * @return filtro					- {@link Map}
	 */
	private Map<String, Object> obterFiltroPesquisaImovelHistoricoLeituraMedicao(HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			ClientePopupVO clientePopupVO) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (clientePopupVO.getIdCliente() != null && clientePopupVO.getIdCliente() > 0) {
			filtro.put(ID_CLIENTE, clientePopupVO.getIdCliente());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getNome())) {
			filtro.put(NOME, historicoLeituraConsumoVO.getNome());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getComplementoImovel())) {
			filtro.put(COMPLEMENTO_IMOVEL, historicoLeituraConsumoVO.getComplementoImovel());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getMatricula())
				&& StringUtils.isNumeric(historicoLeituraConsumoVO.getMatricula())) {
			filtro.put(MATRICULA, historicoLeituraConsumoVO.getMatricula());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getNumeroImovel())) {
			filtro.put(NUMERO_IMOVEL, historicoLeituraConsumoVO.getNumeroImovel());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getIndicadorCondominioAmbos())) {
			filtro.put(INDICADOR_CONDOMINIO_AMBOS, historicoLeituraConsumoVO.getIndicadorCondominioAmbos());
		}

		prepararFiltroDois(historicoLeituraConsumoVO, filtro);

		return filtro;
	}

	/**
	 * Preparar filtro Dois
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param filtro					- {@link Map}
	 */
	private void prepararFiltroDois(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, Map<String, Object> filtro) {
		
		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getHabilitado())) {
			filtro.put(HABILITADO, historicoLeituraConsumoVO.getHabilitado());
		}

		if (!StringUtils.isEmpty(historicoLeituraConsumoVO.getCepImovel())) {
			filtro.put(CEP_IMOVEL, historicoLeituraConsumoVO.getCepImovel());
		}

		if (historicoLeituraConsumoVO.getIdGrupoFaturamento() != null && historicoLeituraConsumoVO.getIdGrupoFaturamento() > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, historicoLeituraConsumoVO.getIdGrupoFaturamento());
		}

		if (historicoLeituraConsumoVO.getIdRota() != null && historicoLeituraConsumoVO.getIdRota() > 0) {
			filtro.put(ID_ROTA, historicoLeituraConsumoVO.getIdRota());
		}

		String idPontoConsumoLegado = historicoLeituraConsumoVO.getPontoConsumoLegado();
		if (!StringUtils.isEmpty(idPontoConsumoLegado) && idPontoConsumoLegado != null) {
			filtro.put(PONTO_CONSUMO_LEGADO, idPontoConsumoLegado);
		}
		
	}

	/**
	 * Método responsável por exibir o histórico leitura e medição do imóvel.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult				- {@link BindingResult}
	 * @param model                     - {@link Model}
	 * @param request                   - {@link HttpServletRequest}
	 * @return view 					- {@link String}
	 */
	@RequestMapping(EXIBIR_HISTORICO_LEITURA_CONSUMO_IMOVEL)
	public String exibirHistoricoLeituraConsumoImovel(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, ClientePopupVO clientePopupVO,
			BindingResult bindingResult, Model model, HttpServletRequest request) {

		Long idImovel = historicoLeituraConsumoVO.getIdImovel();

		String view = EXIBIR_HISTORICO_LEITURA_CONSUMO_IMOVEL;

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		Imovel imovel = null;

		try {
			if (idImovel != null && idImovel > 0) {

				imovel = (Imovel) controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO);
				Collection<PontoConsumo> listaPontosConsumo = this.consultarPontosConsumoImovel(imovel);
				controladorImovel.validarImovelSemPontoConsumo(listaPontosConsumo);
				listaPontosConsumo = controladorHistoricoConsumo.removerPontosConsumoDaListaSemHistoricoConsumo(listaPontosConsumo);
				Collection<PontoConsumoVO> listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumo);

				if (listaPontosConsumoVO != null && !listaPontosConsumoVO.isEmpty()) {
					historicoLeituraConsumoVO
							.setIdPontoConsumo(listaPontosConsumoVO.iterator().next().getPontoConsumo().getChavePrimaria());
					view = this.exibirHistoricoMedicaoConsumoPonto(historicoLeituraConsumoVO, bindingResult, clientePopupVO, model,
							request);
				} else {
					request.setAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
				}

			}

			model.addAttribute(IMOVEL, imovel);
			model.addAttribute(LISTA_ANOS, listarAnos());
			model.addAttribute(LISTA_MESES_ANO, Mes.MESES_ANO);
			model.addAttribute(SCALA_CONSUMO_APURADO, obterParametroEscalaConsumoApurado()); 
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir o histórico leitura e medição do imóvel.
	 * 
	 * @param historicoLeituraConsumoVO	- {@link HistoricoLeituraConsumoVO}
	 * @param bindingResult				- {@link BindingResult}
	 * @param clientePopupVO			- {@link ClientePopupVO}
	 * @param model						- {@link Model}
	 * @param request					- {@link String}
	 * @return view						- {@link String}
	 * @throws GGASException 
	 */
	@RequestMapping("exibirHistoricoMedicaoConsumoPonto")
	public String exibirHistoricoMedicaoConsumoPonto(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, BindingResult bindingResult,
			ClientePopupVO clientePopupVO, Model model, HttpServletRequest request) throws GGASException {

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		String referenciaInicial = null;
		String referenciaFinal = null;
		String mesInicial = historicoLeituraConsumoVO.getMesInicial();
		String anoInicial = historicoLeituraConsumoVO.getAnoInicial();
		String mesFinal = historicoLeituraConsumoVO.getMesFinal();
		String anoFinal = historicoLeituraConsumoVO.getAnoFinal();
		Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
		Long idImovel = historicoLeituraConsumoVO.getIdImovel();
		
		try {
			if (!super.isPostBack(request)) {
				controladorHistoricoConsumo.validarIntervaloDatas(mesInicial, anoInicial, mesFinal, anoFinal);
				referenciaInicial = controladorHistoricoConsumo.obterReferenciaInicial(anoInicial, mesInicial);
				referenciaFinal = controladorHistoricoConsumo.obterReferenciaFinal(anoFinal, mesFinal);
				controladorHistoricoConsumo.validarIntervaloReferencia(referenciaInicial, referenciaFinal);
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}
		
		exibirHistoricoMedicaoDePontoDeConsumo(model, idPontoConsumo, idImovel, referenciaInicial, referenciaFinal);


		return EXIBIR_HISTORICO_LEITURA_CONSUMO_IMOVEL;
	}

	/**
	 * @param model				- {@link Model}
	 * @param idPontoConsumo	- {@link Long}
	 * @param idImovel			- {@link Long}
	 * @param referenciaInicial - {@link String}
	 * @param referenciaFinal	- {@link String}
	 * @throws GGASException	- {@link GGASException}
	 */
	private void exibirHistoricoMedicaoDePontoDeConsumo(Model model, Long idPontoConsumo, Long idImovel, String referenciaInicial,
			String referenciaFinal) throws GGASException {
		PontoConsumo pontoConsumo = null;
		Integer numeroCiclos = null;
		
		if (idPontoConsumo != null && idPontoConsumo > 0) {

			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, LAZY_ROTA_GRUPO_FATURAMENTO);

			// se referencia não foi informada,
			// buscar intervalo
			if (referenciaInicial == null || referenciaFinal == null) {

				String anoMesReferencia = null;

				if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getGrupoFaturamento() != null) {

					anoMesReferencia = String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getAnoMesReferencia());

				}

				if (anoMesReferencia == null || StringUtils.isEmpty(anoMesReferencia) || Integer.parseInt(anoMesReferencia) == 0) {
					anoMesReferencia = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
				}

				numeroCiclos = pontoConsumo.getSegmento().getNumeroCiclos();
				if (numeroCiclos != null) {
					referenciaFinal = anoMesReferencia;
					referenciaInicial = (Util.diminuirMesEmAnoMes(Integer.valueOf(anoMesReferencia), numeroCiclos + 1)).toString();
				}
			}
			if (numeroCiclos != null) {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				Collection<HistoricoMedicao> listaHistoricoMedicao = controladorHistoricoMedicao.consultarHistoricoAgrupadoMedicao(
						pontoConsumo.getChavePrimaria(), referenciaInicial, referenciaFinal, numeroCiclos);
				model.addAttribute(LISTA_HISTORICO_MEDICAO, listaHistoricoMedicao);
				Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
						.consultarHistoricoConsumo(pontoConsumo.getChavePrimaria(), true, referenciaInicial, referenciaFinal, numeroCiclos);
				this.calcularPercentualVariacaoConsumo(listaHistoricoConsumo);
				model.addAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);

			} else {

				model.addAttribute(PONTO_CONSUMO, pontoConsumo);
				Collection<HistoricoMedicao> listaHistoricoMedicao = controladorHistoricoMedicao
						.consultarHistoricoAgrupadoMedicao(pontoConsumo.getChavePrimaria(), referenciaInicial, referenciaFinal);
				model.addAttribute(LISTA_HISTORICO_MEDICAO, listaHistoricoMedicao);
				Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
						.consultarHistoricoConsumo(pontoConsumo.getChavePrimaria(), true, referenciaInicial, referenciaFinal);
				this.calcularPercentualVariacaoConsumo(listaHistoricoConsumo);
				model.addAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);

			}
			model.addAttribute(SCALA_CONSUMO_APURADO, obterParametroEscalaConsumoApurado());
		}

		Imovel imovel = null;
		if (idImovel != null && idImovel > 0) {

			imovel = (Imovel) controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO);
			model.addAttribute(IMOVEL, imovel);
			Collection<PontoConsumo> listaPontosConsumo = this.consultarPontosConsumoImovel(imovel);
			listaPontosConsumo = controladorHistoricoConsumo.removerPontosConsumoDaListaSemHistoricoConsumo(listaPontosConsumo);
			Collection<PontoConsumoVO> listaPontosConsumoVO;

			listaPontosConsumoVO = this.montarListaPontosConsumoVo(listaPontosConsumo);

			if (listaPontosConsumoVO != null && listaPontosConsumoVO.size() > 1) {
				model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);
			}
		}

		model.addAttribute(LISTA_ANOS, listarAnos());
		model.addAttribute(LISTA_MESES_ANO, Mes.MESES_ANO);
	}

	/**
	 * Método responsável por detalhar o histórico de leitura do ponto de consumo.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult             - {@link BindingResult}
	 * @param model                     - {@link Model}
	 * @return view 					- {@link String}
	 */
	@RequestMapping("exibirDetalhamentoHistoricoLeituraPontoConsumo")
	public String exibirDetalhamentoHistoricoLeituraPontoConsumo(HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			ClientePopupVO clientePopupVO, BindingResult bindingResult, Model model) {

		Long idImovel = historicoLeituraConsumoVO.getIdImovel();

		try {

			model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
			model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);
			model.addAttribute(LISTA_MESES_ANO, Mes.MESES_ANO);
			model.addAttribute(LISTA_ANOS, listarAnos());

			if (idImovel != null && idImovel > 0) {

				model.addAttribute(IMOVEL, controladorImovel.obter(idImovel, LAZY_LISTA_PONTO_CONSUMO));
			}

			carregarHistoricosLeitura(historicoLeituraConsumoVO, model);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirDetalhamentoHistoricoLeituraPontoConsumo";
	}

	/**
	 * Método responsável por detalhar o histórico de leitura para Análise Excecao.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult             - {@link BindingResult}
	 * @param model                     - {@link Model}
	 * @return view 					- {@link String}
	 */
	@RequestMapping("exibirDetalhamentoHistoricoLeituraAnaliseExcecao")
	public String exibirDetalhamentoHistoricoLeituraAnaliseExcecao(HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			ClientePopupVO clientePopupVO, BindingResult bindingResult, Model model) {

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		try {
			carregarHistoricosLeitura(historicoLeituraConsumoVO, model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirDetalhamentoHistoricoLeituraAnaliseExcecao";
	}

	/**
	 * @param historicoLeituraConsumoVO	- {@link HistoricoLeituraConsumoVO}
	 * @param model						- {@link Model}
	 * @throws GGASException			- {@link GGASException}
	 */
	private void carregarHistoricosLeitura(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, Model model) throws GGASException {

		Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
		Integer anoMesLeitura = historicoLeituraConsumoVO.getAnoMesLeitura();
		Integer numeroCiclo = historicoLeituraConsumoVO.getNumeroCiclo();
		Collection<HistoricoMedicao> listaHistoricoMedicao =
				controladorHistoricoMedicao.consultarHistoricoMedicao(idPontoConsumo, anoMesLeitura, numeroCiclo);
		model.addAttribute(LISTA_HISTORICO_MEDICAO, listaHistoricoMedicao);
	}

	/**
	 * Método responsável por detalhar o histórico de consumo do ponto de consumo.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult             - {@link BindingResult}
	 * @param model                     - {@link Model}
	 * @return view 					- {@link String}
	 */
	@RequestMapping("exibirDetalhamentoHistoricoConsumoPontoConsumo")
	public String exibirDetalhamentoHistoricoConsumoPontoConsumo(HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			ClientePopupVO clientePopupVO, BindingResult bindingResult, Model model) {

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		try {
			Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
			Integer anoMesFaturamento = historicoLeituraConsumoVO.getAnoMesFaturamento();
			Integer numeroCiclo = historicoLeituraConsumoVO.getNumeroCiclo();
			Long chavePrimariaHistoricoConsumoSintetizador = historicoLeituraConsumoVO.getChavePrimariaHistoricoConsumo();
			Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumo(idPontoConsumo,
					anoMesFaturamento, numeroCiclo, Boolean.FALSE, null, chavePrimariaHistoricoConsumoSintetizador);

			if (idPontoConsumo != null) {
				Medidor medidor = controladorPontoConsumo.obterMedidorPorPontoConsumo(idPontoConsumo);
				if (medidor != null) {
					model.addAttribute(MEDIDOR, medidor);
				}
			}

			model.addAttribute(MODO_USO_INDEPENDENTE,
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
			model.addAttribute(MODO_USO_NORMAL,
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
			model.addAttribute(MODO_USO_VIRTUAL,
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
			model.addAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirDetalhamentoHistoricoConsumoPontoConsumo";
	}

	/**
	 * @param listaPontosConsumo 	- {@link Collection}
	 * @return listaPontosConsumoVO	- {@link List}
	 * @throws NegocioException		- {@link NegocioException}
	 */
	private Collection<PontoConsumoVO> montarListaPontosConsumoVo(Collection<PontoConsumo> listaPontosConsumo) throws NegocioException {

		List<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {

			pontoVO = new PontoConsumoVO();
			PontoConsumo pontoConsumoCompleto = (PontoConsumo) controladorPontoConsumo.obter(pontoConsumo.getChavePrimaria(),
					LAZY_ROTA_GRUPO_FATURAMENTO, LAZY_ROTA_EMPRESA,
					LAZY_INSTALACAO_MEDIDOR, ROTA_LEITURISTA_FUNCIONARIO);
			pontoVO.setPontoConsumo(pontoConsumoCompleto);

			Map<String, Integer> referenciaCiclo = controladorPontoConsumo.obterReferenciaCicloAtual(pontoConsumoCompleto);

			if (referenciaCiclo != null) {
				pontoVO.setCiclo(String.valueOf(referenciaCiclo.get("ciclo")));
				pontoVO.setReferencia(Util.formatarAnoMes(referenciaCiclo.get("referencia")));
			}

			listaPontosConsumoVO.add(pontoVO);

		}

		Collections.sort(listaPontosConsumoVO, new Comparator<PontoConsumoVO>() {

			@Override
			public int compare(PontoConsumoVO o1, PontoConsumoVO o2) {

				int retorno = 0;

				if (o2 == null || o2.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
					retorno = -1;
				} else {
					if (o1 == null || o1.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
						retorno = 1;
					} else {
						retorno = o1.getPontoConsumo().getNumeroSequenciaLeitura()
								.compareTo(o2.getPontoConsumo().getNumeroSequenciaLeitura());
					}
				}
				return retorno;
			}

		});

		return listaPontosConsumoVO;
	}

	/**
	 * @param imovel			- {@link Imovel}
	 * @return					- {@link Collection}
	 * @throws GGASException	- {@link GGASException}
	 */
	private Collection<PontoConsumo> consultarPontosConsumoImovel(Imovel imovel) throws GGASException {

		Collection<PontoConsumo> listaPontosConsumo = new ArrayList<PontoConsumo>();

		if (imovel != null) {
			Collection<PontoConsumo> pontosDeConsumo = imovel.getListaPontoConsumo();

			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}

			if (imovel.getCondominio()) {
				Collection<Imovel> listaImoveisCondominio = controladorImovel.obterImoveisFilhosDoCondominio(imovel);

				if (listaImoveisCondominio != null && !listaImoveisCondominio.isEmpty()) {

					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(CHAVES_PRIMARIAS_IMOVEIS, Util.collectionParaArrayChavesPrimarias(listaImoveisCondominio));
					pontosDeConsumo = controladorPontoConsumo.consultarPontosConsumo(filtro);

					if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
						listaPontosConsumo.addAll(pontosDeConsumo);
					}
				}

			}
		}

		return listaPontosConsumo;
	}

	/**
	 * @return	listaAnos		- {@link Collection}
	 * @throws GGASException 	- {@link GGASException}
	 */
	private Collection<String> listarAnos() throws GGASException {

		Collection<String> listaAnos = new ArrayList<String>();

		Integer anoAtual = new DateTime().getYear();

		String valorQuantidadeAnos =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		Integer quantidadeAnos = Integer.valueOf(valorQuantidadeAnos);
		for (int i = 0; i < quantidadeAnos; i++) {
			Integer ano = anoAtual - i;
			listaAnos.add(ano.toString());
		}

		return listaAnos;
	}

	/**
	 * Método responsável por exibir os comentários do histórico de medição.
	 * 
	 * @param historicoLeituraConsumoVO				- {@link HistoricoLeituraConsumoVO}
	 * @param bindingResult							- {@link BindingResult}
	 * @param model									- {@link Model}
	 * @return exibirMedicaoHistoricoComentarios	- {@link String}
	 */
	@RequestMapping("exibirMedicaoHistoricoComentarios")
	public String exibirMedicaoHistoricoComentarios(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, BindingResult bindingResult,
			Model model) {

		try {
			Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
			Integer anoMesLeitura = historicoLeituraConsumoVO.getAnoMesLeitura();
			Integer numeroCiclo = historicoLeituraConsumoVO.getNumeroCiclo();

			Collection<MedicaoHistoricoComentario> listaMedicaoHistoricoComentario =
					controladorHistoricoMedicao.consultarMedicaoHistoricoComentario(idPontoConsumo, anoMesLeitura, numeroCiclo);
			model.addAttribute(LISTA_MEDICAO_HISTORICO_COMENTARIO, listaMedicaoHistoricoComentario);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirMedicaoHistoricoComentarios";
	}

	/**
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO			- {@link ClientePopupVO}
	 * @throws GGASException			- {@link GGASException}
	 */
	private void validarCampos(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, ClientePopupVO clientePopupVO) throws GGASException {

		if ((clientePopupVO.getIdCliente() == null || clientePopupVO.getIdCliente() == 0)
				&& historicoLeituraConsumoVO.getCepImovel().isEmpty() && historicoLeituraConsumoVO.getIdGrupoFaturamento() == -1
				&& historicoLeituraConsumoVO.getMatricula().isEmpty() && historicoLeituraConsumoVO.getNome().isEmpty()
				&& historicoLeituraConsumoVO.getComplementoImovel().isEmpty()) {
			throw new GGASException(INFORMAR_FILTRO_CLIENTE_IMOVEL_GRUPO_FATURAMENTO);
		}
	}

	/**
	 * Calcular percentual variacao consumo.
	 * 
	 * @param listaHistoricoConsumo - {@link Collection}
	 */
	private void calcularPercentualVariacaoConsumo(Collection<HistoricoConsumo> listaHistoricoConsumo) {

		Map<String, Collection<HistoricoConsumo>> mapaHistoricoConsumoReferencia = new HashMap<String, Collection<HistoricoConsumo>>();
		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			String chave = historicoConsumo.getAnoMesFaturamento() + "-" + historicoConsumo.getNumeroCiclo();
			if (mapaHistoricoConsumoReferencia.containsKey(chave)) {
				Collection<HistoricoConsumo> lista = mapaHistoricoConsumoReferencia.get(chave);
				lista.add(historicoConsumo);
				mapaHistoricoConsumoReferencia.put(chave, lista);
			} else {
				Collection<HistoricoConsumo> lista = new ArrayList<HistoricoConsumo>();
				lista.add(historicoConsumo);
				mapaHistoricoConsumoReferencia.put(chave, lista);
			}
		}

		int y = listaHistoricoConsumo.size();
		BigDecimal[] mediaApuracao = new BigDecimal[y];

		int x = 0;
		for (HistoricoConsumo hc : listaHistoricoConsumo) {
			BigDecimal somaConsumoApuradoPorReferencia = new BigDecimal(0);
			String chave = hc.getAnoMesFaturamento() + "-" + hc.getNumeroCiclo();
			Collection<HistoricoConsumo> lista = mapaHistoricoConsumoReferencia.get(chave);
			for (HistoricoConsumo historicoConsumo : lista) {
				if (historicoConsumo.getConsumoApurado() != null) {
					somaConsumoApuradoPorReferencia = somaConsumoApuradoPorReferencia.add(historicoConsumo.getConsumoApurado());
				}
			}
			mediaApuracao[x] = somaConsumoApuradoPorReferencia;
			x++;
		}

		x = 0;
		for (HistoricoConsumo hc : listaHistoricoConsumo) {
			BigDecimal media = BigDecimal.ZERO;
			for (int i = x; i < y; i++) {
				if (mediaApuracao[i] != null) {
					media = media.add(mediaApuracao[i]);
				}
			}
			if (media.compareTo(BigDecimal.ZERO) > 0 && hc.getConsumoApurado() != null
					&& hc.getConsumoApurado().compareTo(BigDecimal.ZERO) > 0) {
				media = media.divide(new BigDecimal(y - x), 4, RoundingMode.HALF_EVEN);
				media = mediaApuracao[x].divide(media, 4, RoundingMode.HALF_EVEN).subtract(new BigDecimal(1)).multiply(new BigDecimal(100));
				hc.setVariacaoConsumo(media);
			} else {
				hc.setVariacaoConsumo(BigDecimal.ZERO);
			}

			x++;
		}

	}

	/**
	 * Método responsável por detalhar o histórico de consumo dos medidores
	 * independentes.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param clientePopupVO            - {@link ClientePopupVO}
	 * @param bindingResult             - {@link BindingResult}
	 * @param model                     - {@link Model}
	 * @return view 					- {@link String}
	 * 
	 */
	@RequestMapping("exibirDetalhamentoHistoricoConsumoMedidoresIndependentes")
	public String exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(HistoricoLeituraConsumoVO historicoLeituraConsumoVO,
			ClientePopupVO clientePopupVO, BindingResult bindingResult, Model model) {

		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(HISTORICO_LEITURA_CONSUMO_VO, historicoLeituraConsumoVO);

		try {
			Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
			Long idMedidor = historicoLeituraConsumoVO.getIdMedidor();
			Integer anoMesFaturamento = historicoLeituraConsumoVO.getAnoMesFaturamento();
			Integer numeroCiclo = historicoLeituraConsumoVO.getNumeroCiclo();

			Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
					.obterHistoricoConsumoPorMedidoresIndependentes(idPontoConsumo, anoMesFaturamento, numeroCiclo);

			Map<Long, Collection<HistoricoConsumo>> historicoConsumoMedidores =
					controladorHistoricoConsumo.agruparHistoricoConsumoPorMedidorIndependente(listaHistoricoConsumo);

			model.addAttribute(LISTA_HIST_CONSUMO_MEDIDOR, historicoConsumoMedidores);
			model.addAttribute(ID_PONTO_CONSUMO, idPontoConsumo);
			model.addAttribute(ID_MEDIDOR, idMedidor);
			model.addAttribute(ANO_MES_FATURAMENTO, anoMesFaturamento);
			model.addAttribute(NUMERO_CICLO, numeroCiclo);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirDetalhamentoHistoricoConsumoMedidoresIndependentes";
	}
	
	/**
	 * Método responsável por gerar relatorio de consumo por medidor independente.
	 * 
	 * @param historicoLeituraConsumoVO -	{@link HistoricoLeituraConsumoVO}
	 * @param bindingResult				-	{@link BindingResult}
	 * @param response					-	{@link HttpServletResponse}
	 * @param clientePopupVO			-	{@link ClientePopupVO}
	 * @param opcaoExpotacaoSelecionada -	{@link String}
	 * @param exibirFiltros				-	{@link Boolean}
	 * @param model						-	{@link Model}
	 * @return view						-	{@link String}
	 * @throws GGASException			-	{@link GGASException}
	 */
	@RequestMapping("gerarRelatorioConsumoClienteMedidor")
	public String gerarRelatorioConsumoClienteMedidor(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, BindingResult bindingResult,
			HttpServletResponse response, ClientePopupVO clientePopupVO,
			@RequestParam(value = "opcaoExpotacaoSelecionada", required = false) String opcaoExpotacaoSelecionada,
			@RequestParam(value = "exibirFiltros", required = false) Boolean exibirFiltros, Model model) throws GGASException {

		String view = null;

		Map<String, Object> filtro = prepararFiltroGerarRelatorio(historicoLeituraConsumoVO, exibirFiltros);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");

		if (("RTF").equals(opcaoExpotacaoSelecionada)) {
			formatoImpressao = FormatoImpressao.valueOf(opcaoExpotacaoSelecionada);
		} else if (("XLS").equals(opcaoExpotacaoSelecionada)) {
			formatoImpressao = FormatoImpressao.valueOf(opcaoExpotacaoSelecionada);
		}

		try {

			byte[] relatorio = controladorRelatorioConsumoClienteMedidor.gerarRelatorio(filtro, formatoImpressao);

			if (relatorio != null) {
				ServletOutputStream servletOutputStream;
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition",
						"attachment; filename=relatorioConsumoClienteMedidor" + this.obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}

		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = exibirDetalhamentoHistoricoConsumoMedidoresIndependentes(historicoLeituraConsumoVO, clientePopupVO, bindingResult, model);
		}

		return view;

	}

	/**
	 * Metodo responsável por verificar os dados e montar filtro para gerar relatorio de consumo por medidor independente.
	 * 
	 * @param historicoLeituraConsumoVO - {@link HistoricoLeituraConsumoVO}
	 * @param exibirFiltros - {@link Boolean}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Object> prepararFiltroGerarRelatorio(HistoricoLeituraConsumoVO historicoLeituraConsumoVO, Boolean exibirFiltros)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long idPontoConsumo = historicoLeituraConsumoVO.getIdPontoConsumo();
		Long idMedidor = historicoLeituraConsumoVO.getIdMedidor();
		String anoMesFaturamento = String.valueOf(historicoLeituraConsumoVO.getAnoMesFaturamento());
		Integer numeroCiclo = historicoLeituraConsumoVO.getNumeroCiclo();

		filtro.put(EXIBIR_FILTROS, exibirFiltros);

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			filtro.put(ID_PONTO_CONSUMO, idPontoConsumo);
		}

		if (idMedidor != null && idMedidor > 0) {
			filtro.put(ID_MEDIDOR, idMedidor);
		}

		if (StringUtils.isNotBlank(anoMesFaturamento)) {
			filtro.put(ANO_MES_FATURAMENTO, anoMesFaturamento);
		}

		if (numeroCiclo != null && numeroCiclo > 0) {
			filtro.put(NUMERO_CICLO, numeroCiclo);
		}

		return filtro;
	}

	private String obterParametroEscalaConsumoApurado() throws NegocioException {
		return (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO);
	}
}
