package br.com.ggas.web.medicao.leitura;

import static br.com.ggas.medicao.batch.ProcessarArquivoLeituraBatch.PARAMETRO_ARQUIVO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * 
 * @author Orube
 * 
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Importar Dados de Dispositivos Móveis.
 * 
 */
@Controller
public class ImportarDadosDispositivosMoveisAction extends GenericAction {

	private static final String EXTENSAO_XML = ".xml";

	private static final String EXTENSAO_TXT = ".txt";

	private static final String DIRETORIO_IMPORTAR_LEITURA_UPLOAD = "DIRETORIO_IMPORTAR_LEITURA_UPLOAD";

	private static final String LISTA_PROCESSO = "listaProcesso";

	private static final String LISTA_ID_PROCESSO = "listaIdProcesso";
	
	private static final int VALOR_LEITURA = 6; 
	
	private static final int DATA_LEITURA = 7;
	
	private static final int CODIGO_ANORMALIDADE_LEITURA = 8;
	
	private static final int PONTO_CONSUMO = 11;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorProcesso controladorProcesso;

	/**
	 * Método responsável por exibir a tela de importação de arquivos de leituras de dispositivos móveis.
	 * 
	 * @return exibirImportacaoLeituraDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirImportarLeituraDispositivosMoveis")
	public String exibirImportarLeituraDispositivosMoveis() throws GGASException {

		return "exibirImportacaoLeituraDispositivosMoveis";
	}

	/**
	 * Montar arquivo xml.
	 * 
	 * @param file - {@link MultipartFile}
	 * @return arquivoLeituraXML {@link Byte}
	 * @throws IOException - {@link IOException}
	 */
	private byte[] montarArquivoXML(MultipartFile file) throws IOException {

		InputStream is = file.getInputStream();
		Reader reader = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(reader);
		Document document = new Document();
		Element elementImportacao = new Element("importacao");

		String linha = br.readLine();
		while (!StringUtils.isEmpty(linha = br.readLine())) {
			String[] dados = linha.split(";");

			Element elementLeitura = new Element("leitura");
			elementLeitura.setAttribute("valorLeitura", dados[VALOR_LEITURA]);
			elementLeitura.setAttribute("dataLeitura", dados[DATA_LEITURA]);
			elementLeitura.setAttribute("codigoAnormalidadeLeitura", dados[CODIGO_ANORMALIDADE_LEITURA]);
			elementLeitura.setAttribute("pontoConsumo", dados[PONTO_CONSUMO]);

			elementLeitura.setAttribute("indicadorConfirmacaoLeitura", "true");

			elementImportacao.addContent(elementLeitura);
			document.setRootElement(elementImportacao);

		}

		XMLOutputter xout = new XMLOutputter();
		xout.setFormat(Format.getPrettyFormat());
		String arquivoLeituraXML = xout.outputString(document);

		is.close();
		reader.close();
		br.close();

		return arquivoLeituraXML.getBytes();
	}

	/**
	 * Verificar arquivo txt dados importacao.
	 * 
	 * @param nomeArquivo - {@link String}
	 * @param dados - {@link Byte}
	 * @return retorno - {@link Boolean}
	 */
	private boolean verificarArquivoTxtDadosImportacao(String nomeArquivo, byte[] dados) {

		String arquivo = new String(dados);

		boolean retorno = false;

		String cabecalhoArquivoImportacaoAlgas = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_CABECALHO_ARQUIVO_TXT_IMPORTACAO_DADOS_MOVEIS_ALGAS);

		if (nomeArquivo.toLowerCase().endsWith(EXTENSAO_TXT) && arquivo.contains(cabecalhoArquivoImportacaoAlgas)) {
			retorno = true;
		}

		return retorno;
	}

	/**
	 * Recupera na requisição os arquivos de leitura informados pelo usuário e cadastra para cada um deles uma rotina batch para processar
	 * seu conteúdo e atualizar os registros correspondentes na tabela de integração LEITURA_MOVIMENTO.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param files - {@link MultipartFile}
	 * @param model - {@link Model}
	 * @return exibirImportacaoLeituraDispositivosMoveis - {@link String}
	 * @throws GGASException - {@link GGASException}
	 * @throws IOException - {@link IOException}
	 */
	@RequestMapping("importarLeituraDispositivosMoveis")
	public String importarLeituraDispositivosMoveis(HttpServletRequest request,
			@RequestParam(value = "file_1", required = false) MultipartFile[] files, Model model) throws GGASException, IOException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		Collection<Processo> processos = new ArrayList<Processo>();
		StringBuilder listaIdProcesso = new StringBuilder();

		Map<String, byte[]> arquivos = new HashMap<String, byte[]>();
		StringBuilder nomeArquivosInvalidos = new StringBuilder();

		try {

			if (files != null && files[0].getSize() > 0) {
				
				for (MultipartFile file : files) {

					String nomeArquivo = file.getOriginalFilename();
					byte[] dadosArquivo = file.getBytes();
					boolean arquivoTxtValido = this.verificarArquivoTxtDadosImportacao(nomeArquivo, dadosArquivo);
					if (arquivoTxtValido) {
						dadosArquivo = this.montarArquivoXML(file);
						nomeArquivo = nomeArquivo.replace(EXTENSAO_TXT, EXTENSAO_XML);
					}
					arquivos.put(nomeArquivo, dadosArquivo);
					if (!nomeArquivo.toLowerCase().endsWith(EXTENSAO_XML) && !arquivoTxtValido) {
						nomeArquivosInvalidos.append(nomeArquivo).append(", ");
					}
				}
			}

			if (nomeArquivosInvalidos.length() > 0) {

				mensagemErroParametrizado(model, Constantes.ERRO_ARQUIVOS_EXTENSAO_INVALIDA, request,
						new Object[] { nomeArquivosInvalidos.substring(0, nomeArquivosInvalidos.length() - 2),
								EXTENSAO_XML.substring(1).toUpperCase() + ", " + EXTENSAO_TXT.substring(1).toUpperCase() });
			} else if (arquivos.isEmpty()) {

				mensagemErro(model, request, Constantes.ERRO_ARQUIVO_NAO_SELECIONADO);

			} else {

				// Obter o diretorio dos arquivos de
				// leitura
				String diretorioArquivosUpload =
						(String) controladorParametroSistema.obterValorDoParametroPorCodigo(DIRETORIO_IMPORTAR_LEITURA_UPLOAD);
				File diretorioLeitura = Util.getFile(diretorioArquivosUpload);
				if (!diretorioLeitura.exists()) {
					diretorioLeitura.mkdirs();
				}

				Map<String, String> parametros = new HashMap<String, String>();

				String nomeArquivoGravacao = null;

				for (Entry<String, byte[]> mapArquivos : arquivos.entrySet()) {
					String nomeArquivo = mapArquivos.getKey();
					byte[] dadosArquivo = mapArquivos.getValue();

					nomeArquivoGravacao = diretorioArquivosUpload + Calendar.getInstance().getTimeInMillis() + "_" + nomeArquivo;

					FileOutputStream fileOutputStream = new FileOutputStream(nomeArquivoGravacao); // NOSONAR
					fileOutputStream.write(dadosArquivo);
					fileOutputStream.close();

					parametros.put(PARAMETRO_ARQUIVO, nomeArquivoGravacao);

					// Criando processo para
					// importação de arquivo
					Operacao operacao = super.obterOperacao(request);
					Processo processo = (Processo) controladorProcesso.criar();
					processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
					processo.setHabilitado(true);
					processo.setOperacao(operacao);
					processo.setDescricao(operacao.getDescricao() + ": " + nomeArquivo);
					processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
					processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
					processo.setUsuario(super.obterUsuario(request));
					processo.setParametros(parametros);
					processo.setDiaNaoUtil(true);
					processo.setDadosAuditoria(dadosAuditoria);

					controladorProcesso.inserir(processo);

					processos.add(processo);
					listaIdProcesso.append(processo.getChavePrimaria()).append(",");
					parametros.clear();
				}
			}

			if (!processos.isEmpty()) {
				model.addAttribute(LISTA_PROCESSO, processos);
				model.addAttribute(LISTA_ID_PROCESSO, listaIdProcesso.substring(0, listaIdProcesso.length() - 1));
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirImportacaoLeituraDispositivosMoveis";

	}
	
}
