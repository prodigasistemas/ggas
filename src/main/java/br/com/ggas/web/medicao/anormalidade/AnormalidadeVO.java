package br.com.ggas.web.medicao.anormalidade;

import java.io.Serializable;
/**
 * Classe responsável pela transferência de dados relacionados a entidade Anormalidade
 *
 */
public class AnormalidadeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7912094180011512443L;
	
	private Boolean habilitado;
	private Boolean aceitaLeitura;
	private Boolean relativoMedidor;
	private Boolean bloquearFaturamento;
	private Boolean ehTipoAnormalidadeConsumo;
	private String descricao;
	private String mensagemLeitura;
	private String mensagemConsumo;
	private String mensagemPrevencao;
	private String mensagemManutencao;
	private String descricaoAbreviada;
	private Long chavePrimaria; 
	private Long acaoAnormalidadeSemConsumo;
	private Long acaoAnormalidadeComConsumo;
	private Long acaoAnormalidadeSemLeitura;
	private Long acaoAnormalidadeComLeitura;
	private Long numeroOcorrenciaConsecutivasAnormalidade;
	private int versao;
	private Integer modalidadeMedicao;
	private Long segmentoAnormalidade;
	private Boolean habilitadoParametrizacao;
	private Boolean bloqueiaFaturamento;
	private Boolean exigeFoto;
	private Integer indexParametrizacao;
	private Long chavePrimariaParametrizacao;
	private String regra;
	private boolean ignorarAcaoAutomatica;
	private Long ramoAtividade;
	
	public Integer getModalidadeMedicao() {
		return modalidadeMedicao;
	}
	public void setModalidadeMedicao(Integer modalidadeMedicao) {
		this.modalidadeMedicao = modalidadeMedicao;
	}
	public Long getSegmentoAnormalidade() {
		return segmentoAnormalidade;
	}
	public void setSegmentoAnormalidade(Long segmentoAnormalidade) {
		this.segmentoAnormalidade = segmentoAnormalidade;
	}
	public Boolean getHabilitadoParametrizacao() {
		return habilitadoParametrizacao;
	}
	public void setHabilitadoParametrizacao(Boolean habilitadoParametrizacao) {
		this.habilitadoParametrizacao = habilitadoParametrizacao;
	}
	public Boolean getBloqueiaFaturamento() {
		return bloqueiaFaturamento;
	}
	public void setBloqueiaFaturamento(Boolean bloqueiaFaturamento) {
		this.bloqueiaFaturamento = bloqueiaFaturamento;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public Boolean getAceitaLeitura() {
		return aceitaLeitura;
	}
	public void setAceitaLeitura(Boolean aceitaLeitura) {
		this.aceitaLeitura = aceitaLeitura;
	}
	public Boolean getRelativoMedidor() {
		return relativoMedidor;
	}
	public void setRelativoMedidor(Boolean relativoMedidor) {
		this.relativoMedidor = relativoMedidor;
	}
	public Boolean getBloquearFaturamento() {
		return bloquearFaturamento;
	}
	public void setBloquearFaturamento(Boolean bloquearFaturamento) {
		this.bloquearFaturamento = bloquearFaturamento;
	}
	public Boolean getEhTipoAnormalidadeConsumo() {
		return ehTipoAnormalidadeConsumo;
	}
	public void setEhTipoAnormalidadeConsumo(Boolean ehTipoAnormalidadeConsumo) {
		this.ehTipoAnormalidadeConsumo = ehTipoAnormalidadeConsumo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMensagemLeitura() {
		return mensagemLeitura;
	}
	public void setMensagemLeitura(String mensagemLeitura) {
		this.mensagemLeitura = mensagemLeitura;
	}

	public Boolean getExigeFoto() {
		return exigeFoto;
	}

	public void setExigeFoto(Boolean exigeFoto) {
		this.exigeFoto = exigeFoto;
	}

	/**
	 * Retorna a mensagemConsumo
	 * @return mensagemConsumo
	 */
	public String getMensagemConsumo() {
		return mensagemConsumo;
	}

	/**
	 * Altera a mensagemConsumo
	 * @param mensagemConsumo a mensagemConsumo
	 */
	public void setMensagemConsumo(String mensagemConsumo) {
		this.mensagemConsumo = mensagemConsumo;
	}

	public String getMensagemPrevencao() {
		return mensagemPrevencao;
	}
	public void setMensagemPrevencao(String mensagemPrevencao) {
		this.mensagemPrevencao = mensagemPrevencao;
	}
	public String getMensagemManutencao() {
		return mensagemManutencao;
	}
	public void setMensagemManutencao(String mensagemManutencao) {
		this.mensagemManutencao = mensagemManutencao;
	}
	public String getDescricaoAbreviada() {
		return descricaoAbreviada;
	}
	public void setDescricaoAbreviada(String descricaoAbreviada) {
		this.descricaoAbreviada = descricaoAbreviada;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Long getAcaoAnormalidadeSemConsumo() {
		return acaoAnormalidadeSemConsumo;
	}
	public void setAcaoAnormalidadeSemConsumo(Long acaoAnormalidadeSemConsumo) {
		this.acaoAnormalidadeSemConsumo = acaoAnormalidadeSemConsumo;
	}
	public Long getAcaoAnormalidadeComConsumo() {
		return acaoAnormalidadeComConsumo;
	}
	public void setAcaoAnormalidadeComConsumo(Long acaoAnormalidadeComConsumo) {
		this.acaoAnormalidadeComConsumo = acaoAnormalidadeComConsumo;
	}
	public Long getAcaoAnormalidadeSemLeitura() {
		return acaoAnormalidadeSemLeitura;
	}
	public void setAcaoAnormalidadeSemLeitura(Long acaoAnormalidadeSemLeitura) {
		this.acaoAnormalidadeSemLeitura = acaoAnormalidadeSemLeitura;
	}
	public Long getAcaoAnormalidadeComLeitura() {
		return acaoAnormalidadeComLeitura;
	}
	public void setAcaoAnormalidadeComLeitura(Long acaoAnormalidadeComLeitura) {
		this.acaoAnormalidadeComLeitura = acaoAnormalidadeComLeitura;
	}
	public Long getNumeroOcorrenciaConsecutivasAnormalidade() {
		return numeroOcorrenciaConsecutivasAnormalidade;
	}
	public void setNumeroOcorrenciaConsecutivasAnormalidade(Long numeroOcorrenciaConsecutivasAnormalidade) {
		this.numeroOcorrenciaConsecutivasAnormalidade = numeroOcorrenciaConsecutivasAnormalidade;
	}
	public int getVersao() {
		return versao;
	}
	public void setVersao(int versao) {
		this.versao = versao;
	}
	public Integer getIndexParametrizacao() {
		return indexParametrizacao;
	}
	public void setIndexParametrizacao(Integer indexParametrizacao) {
		this.indexParametrizacao = indexParametrizacao;
	}
	public Long getChavePrimariaParametrizacao() {
		return chavePrimariaParametrizacao;
	}
	public void setChavePrimariaParametrizacao(Long chavePrimariaParametrizacao) {
		this.chavePrimariaParametrizacao = chavePrimariaParametrizacao;
	}

	public String getRegra() {
		return regra;
	}

	public void setRegra(String regra) {
		this.regra = regra;
	}

	public boolean isIgnorarAcaoAutomatica() {
		return ignorarAcaoAutomatica;
	}

	public void setIgnorarAcaoAutomatica(boolean ignorarAcaoAutomatica) {
		this.ignorarAcaoAutomatica = ignorarAcaoAutomatica;
	}
	
	public Long getRamoAtividade() {
		return ramoAtividade;
	}
	
	public void setRamoAtividade(Long ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}
}
