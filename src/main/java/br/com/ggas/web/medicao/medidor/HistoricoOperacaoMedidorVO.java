/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.medicao.medidor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe responsável pela transferência de dados relacionados ao histórico de
 * operações do medidor
 *
 */
public class HistoricoOperacaoMedidorVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5782208551518286529L;

	private OperacaoMedidor operacaoMedidor;

	private Date dataPlanejada;

	private Date dataRealizada;

	private Medidor medidor;

	private MedidorLocalInstalacao medidorLocalInstalacao;

	private String descricao;

	private String descricaoPontoConsumo;

	private BigDecimal numeroLeitura;

	private Funcionario funcionario;

	private MotivoOperacaoMedidor motivoOperacaoMedidor;

	private BigDecimal medidaPressaoAnterior;

	private PontoConsumo pontoConsumo;

	private Unidade unidadePressaoAnterior;

	/**
	 * @return the descricaoPontoConsumo
	 */
	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	/**
	 * @param descricaoPontoConsumo
	 *            the descricaoPontoConsumo to set
	 */
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/**
	 * @return the medidaPressaoAnterior
	 */
	public BigDecimal getMedidaPressaoAnterior() {

		return medidaPressaoAnterior;
	}

	/**
	 * @param medidaPressaoAnterior
	 *            the medidaPressaoAnterior to set
	 */
	public void setMedidaPressaoAnterior(BigDecimal medidaPressaoAnterior) {

		this.medidaPressaoAnterior = medidaPressaoAnterior;
	}

	/**
	 * @return the pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the unidadePressaoAnterior
	 */
	public Unidade getUnidadePressaoAnterior() {

		return unidadePressaoAnterior;
	}

	/**
	 * @param unidadePressaoAnterior
	 *            the unidadePressaoAnterior to
	 *            set
	 */
	public void setUnidadePressaoAnterior(Unidade unidadePressaoAnterior) {

		this.unidadePressaoAnterior = unidadePressaoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getOperacaoMedidor()
	 */
	public OperacaoMedidor getOperacaoMedidor() {

		return operacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setOperacaoMedidor(
	 * br.com.ggas.medicao.medidor
	 * .OperacaoMedidor)
	 */
	public void setOperacaoMedidor(OperacaoMedidor operacaoMedidor) {

		this.operacaoMedidor = operacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDataPlanejada()
	 */
	public Date getDataPlanejada() {
		Date data = null;
		if (this.dataPlanejada != null) {
			data = (Date) dataPlanejada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setDataPlanejada(java.util.Date)
	 */
	public void setDataPlanejada(Date dataPlanejada) {

		this.dataPlanejada = dataPlanejada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDataRealizada()
	 */
	public Date getDataRealizada() {
		Date data = null;
		if (this.dataRealizada != null) {
			data = (Date) dataRealizada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setDataRealizada(java.util.Date)
	 */
	public void setDataRealizada(Date dataRealizada) {

		this.dataRealizada = dataRealizada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getMedidor()
	 */
	public Medidor getMedidor() {

		return medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMedidor(br.com.ggas
	 * .medicao.medidor.Medidor)
	 */
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getMedidorLocalInstalacao()
	 */
	public MedidorLocalInstalacao getMedidorLocalInstalacao() {

		return medidorLocalInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMedidorLocalInstalacao
	 * (br.com.ggas.medicao
	 * .leitura.MedidorLocalInstalacao)
	 */
	public void setMedidorLocalInstalacao(MedidorLocalInstalacao medidorLocalInstalacao) {

		this.medidorLocalInstalacao = medidorLocalInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDescricao()
	 */
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setDescricao(java.lang.String)
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getNumeroLeitura()
	 */
	public BigDecimal getNumeroLeitura() {

		return numeroLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setNumeroLeitura(java.math.BigDecimal)
	 */
	public void setNumeroLeitura(BigDecimal numeroLeitura) {

		this.numeroLeitura = numeroLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getFuncionario()
	 */
	public Funcionario getFuncionario() {

		return funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setFuncionario(br.com
	 * .ggas.cadastro.funcionario.Funcionario)
	 */
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getMotivoOperacaoMedidor()
	 */
	public MotivoOperacaoMedidor getMotivoOperacaoMedidor() {

		return motivoOperacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMotivoOperacaoMedidor
	 * (br.com.ggas.medicao
	 * .medidor.impl.MotivoOperacaoMedidor)
	 */
	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor) {

		this.motivoOperacaoMedidor = motivoOperacaoMedidor;
	}

}
