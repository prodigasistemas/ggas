package br.com.ggas.web.medicao.medidor;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Histórico do Medidor.
 * 
 * @author bruno silva
 * 
 * @see ConsultarHistoricoInstalacaoMedidorAction
 * 
 */
public class ConsultarHistoricoInstalacaoMedidorVO {

	private String numeroSerie;
	private String pontoConsumoLegado;
	private String medidorVirtual;
	private String modoUso;
	private String nome;
	private String matriculaCondominio;
	private String indicadorCondominio;
	private String indicadorCondominioAmbos;
	private String matricula;
	private String cepImovel;
	private String numeroImovel;
	private String complementoImovel;
	private String indicadorPesquisa = "indicadorPesquisaMedidor";
	private String descricaoModelo;
	
	private Boolean habilitado = true;
	
	private Long chavePrimaria;
	private Long idMarca;
	private Long idModelo;
	private Long idTipo;
	private Long idImovel;
	private Long idMedidor;
	private Long idPontoConsumo;
	private Long[] chavesPrimarias;
	
	/**
	 * @return numeroSerie - {@link String}
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie - {@link String}
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return pontoConsumoLegado - {@link String}
	 */
	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}
	/**
	 * @param pontoConsumoLegado - {@link String}
	 */
	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}
	/**
	 * @return medidorVirtual - {@link String}
	 */
	public String getMedidorVirtual() {
		return medidorVirtual;
	}
	/**
	 * @param medidorVirtual - {@link String}
	 */
	public void setMedidorVirtual(String medidorVirtual) {
		this.medidorVirtual = medidorVirtual;
	}
	/**
	 * @return modoUso - {@link String}
	 */
	public String getModoUso() {
		return modoUso;
	}
	/**
	 * @param modoUso - {@link String}
	 */
	public void setModoUso(String modoUso) {
		this.modoUso = modoUso;
	}
	/**
	 * @return nome - {@link String}
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome - {@link String}
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return matriculaCondominio - {@link String}
	 */
	public String getMatriculaCondominio() {
		return matriculaCondominio;
	}
	/**
	 * @param matriculaCondominio - {@link String}
	 */
	public void setMatriculaCondominio(String matriculaCondominio) {
		this.matriculaCondominio = matriculaCondominio;
	}
	/**
	 * @return indicadorCondominio - {@link String}
	 */
	public String getIndicadorCondominio() {
		return indicadorCondominio;
	}
	/**
	 * @param indicadorCondominio - {@link String}
	 */
	public void setIndicadorCondominio(String indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}
	/**
	 * @return indicadorCondominioAmbos - {@link String}
	 */
	public String getIndicadorCondominioAmbos() {
		return indicadorCondominioAmbos;
	}
	/**
	 * @param indicadorCondominioAmbos - {@link String}
	 */
	public void setIndicadorCondominioAmbos(String indicadorCondominioAmbos) {
		this.indicadorCondominioAmbos = indicadorCondominioAmbos;
	}
	/**
	 * @return matricula - {@link String}
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula - {@link String}
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return cepImovel - {@link String}
	 */
	public String getCepImovel() {
		return cepImovel;
	}
	/**
	 * @param cepImovel - {@link String}
	 */
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	/**
	 * @return numeroImovel - {@link String}
	 */
	public String getNumeroImovel() {
		return numeroImovel;
	}
	/**
	 * @param numeroImovel - {@link String}
	 */
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	/**
	 * @return complementoImovel - {@link String}
	 */
	public String getComplementoImovel() {
		return complementoImovel;
	}
	/**
	 * @param complementoImovel - {@link String}
	 */
	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}
	/**
	 * @return indicadorPesquisa - {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	/**
	 * @param indicadorPesquisa - {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	/**
	 * @return descricaoModelo - {@link String}
	 */
	public String getDescricaoModelo() {
		return descricaoModelo;
	}
	/**
	 * @param descricaoModelo - {@link String}
	 */
	public void setDescricaoModelo(String descricaoModelo) {
		this.descricaoModelo = descricaoModelo;
	}
	/**
	 * @return habilitado - {@link Boolean}
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}
	/**
	 * @param habilitado - {@link Boolean}
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * @return chavePrimaria - {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria - {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	/**
	 * @return idMarca - {@link Long}
	 */
	public Long getIdMarca() {
		return idMarca;
	}
	/**
	 * @param idMarca - {@link Long}
	 */
	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}
	/**
	 * @return idModelo - {@link Long}
	 */
	public Long getIdModelo() {
		return idModelo;
	}
	/**
	 * @param idModelo - {@link Long}
	 */
	public void setIdModelo(Long idModelo) {
		this.idModelo = idModelo;
	}
	/**
	 * @return idTipo - {@link Long}
	 */
	public Long getIdTipo() {
		return idTipo;
	}
	/**
	 * @param idTipo - {@link Long}
	 */
	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}
	/**
	 * @return idImovel - {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}
	/**
	 * @param idImovel - {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	/**
	 * @return idMedidor - {@link Long}
	 */
	public Long getIdMedidor() {
		return idMedidor;
	}
	/**
	 * @param idMedidor - {@link Long}
	 */
	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}
	/**
	 * @return idPontoConsumo - {@link Long}
	 */
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	/**
	 * @param idPontoConsumo - {@link Long}
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	/**
	 * @return chavesPrimariasTmp {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] chavesPrimariasTmp = null;
		if (chavesPrimarias != null) {
			chavesPrimariasTmp = chavesPrimarias.clone();
		}
		return chavesPrimariasTmp;
	}
	/**
	 * @param chavesPrimarias {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if(chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}else {
			this.chavesPrimarias = null;
		}
	}
	
}
