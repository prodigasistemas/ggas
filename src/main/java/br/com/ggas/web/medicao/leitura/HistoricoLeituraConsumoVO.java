/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.medicao.leitura;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Histórico de Leitura e Consumo.
 * 
 * 
 * @see ConsultarHistoricoLeituraMedicaoAction
 * 
 */
public class HistoricoLeituraConsumoVO {

	private String nome;

	private String complementoImovel;

	private String matricula;

	private String numeroImovel;

	private String indicadorCondominioAmbos;

	private String habilitado = "true";

	private String cepImovel;
	
	private String pontoConsumoLegado;
	
	private String mesInicial = "";
	
	private String anoInicial = "";
	
	private String mesFinal = "";
	
	private String anoFinal = "";
	
	private Integer anoMesLeitura;
	
	private Integer anoMesFaturamento;

	private Integer numeroCiclo;
	
	private Long idGrupoFaturamento;

	private Long idRota;
	
	private Long idPontoConsumo;
	
	private Long chavePrimariaHistoricoConsumo;
	
	private Long idMedidor;
	
	private Long idImovel;
	
	private Boolean permitirAlteracao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getComplementoImovel() {
		return complementoImovel;
	}

	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getIndicadorCondominioAmbos() {
		return indicadorCondominioAmbos;
	}

	public void setIndicadorCondominioAmbos(String indicadorCondominioAmbos) {
		this.indicadorCondominioAmbos = indicadorCondominioAmbos;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public String getCepImovel() {
		return cepImovel;
	}

	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}

	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}

	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public String getMesInicial() {
		return mesInicial;
	}

	public void setMesInicial(String mesInicial) {
		this.mesInicial = mesInicial;
	}

	public String getAnoInicial() {
		return anoInicial;
	}

	public void setAnoInicial(String anoInicial) {
		this.anoInicial = anoInicial;
	}

	public String getMesFinal() {
		return mesFinal;
	}

	public void setMesFinal(String mesFinal) {
		this.mesFinal = mesFinal;
	}

	public String getAnoFinal() {
		return anoFinal;
	}

	public void setAnoFinal(String anoFinal) {
		this.anoFinal = anoFinal;
	}

	public Integer getAnoMesLeitura() {
		return anoMesLeitura;
	}

	public void setAnoMesLeitura(Integer anoMesLeitura) {
		this.anoMesLeitura = anoMesLeitura;
	}

	public Integer getNumeroCiclo() {
		return numeroCiclo;
	}

	public void setNumeroCiclo(Integer numeroCiclo) {
		this.numeroCiclo = numeroCiclo;
	}

	public Integer getAnoMesFaturamento() {
		return anoMesFaturamento;
	}

	public void setAnoMesFaturamento(Integer anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}

	public Long getChavePrimariaHistoricoConsumo() {
		return chavePrimariaHistoricoConsumo;
	}

	public void setChavePrimariaHistoricoConsumo(Long chavePrimariaHistoricoConsumo) {
		this.chavePrimariaHistoricoConsumo = chavePrimariaHistoricoConsumo;
	}

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public Boolean getPermitirAlteracao() {
		return permitirAlteracao;
	}

	public void setPermitirAlteracao(Boolean permitirAlteracao) {
		this.permitirAlteracao = permitirAlteracao;
	}
	
}
