package br.com.ggas.web.cadastro.localidade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.MunicipioImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações que mantém um Município.
 * 
 * @author esantana
 *
 */
@Controller
public class MunicipioAction extends GenericAction {

	private static final String FORWARD_EXIBIR_PESQUISAR_MUNICIPIO = "forward:/exibirPesquisarMunicipio";

	private static final String MUNICIPIO = "municipio";

	private static final String LISTA_UNIDADE_FEDERACAO = "listaUnidadeFederacao";

	private static final String LISTA_REGIAO = "listaRegiao";

	private static final String CODIGO_IBGE = "codigoIBGE";

	private static final String ID_REGIAO = "idRegiao";

	private static final String ID_MICRORREGIAO = "idMicrorregiao";

	private static final String ID_UNI_FED = "idUnidadeFederacao";

	@Autowired
	private ControladorGeografico controladorGeografico;

	@Autowired
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela exibição da tela de pesquisa de municípios.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisarMunicipio")
	public String exibirPesquisarMunicipio(Model model) throws GGASException {

		Collection<Regiao> listaRegiao = controladorGeografico.listarRegioes();
		model.addAttribute(LISTA_REGIAO, listaRegiao);

		Collection<UnidadeFederacao> listaUnidadeFederacao = controladorMunicipio.listarUnidadeFederacao();
		model.addAttribute(LISTA_UNIDADE_FEDERACAO, listaUnidadeFederacao);

		Collection<Microrregiao> listaMicrorregioes = controladorGeografico.listarMicrorregioes();
		model.addAttribute("listaMicrorregioes", listaMicrorregioes);

		if (!model.containsAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO)) {
			model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisarMunicipio";
	}

	/**
	 * Método responsável pela pesquisa de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param municipio - {@link MunicipioImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarMunicipio")
	public String pesquisarMunicipio(Model model, MunicipioImpl municipio, BindingResult bindingResult,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(municipio, habilitado);

		Collection<TabelaAuxiliar> listaMunicipio =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, MunicipioImpl.class.getName());

		model.addAttribute("listaMunicipio", listaMunicipio);
		model.addAttribute(MUNICIPIO, municipio);
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);

		return this.exibirPesquisarMunicipio(model);
	}

	/**
	 * Método responsável pela exibição da tela de inclusão de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param municipio - {@link MunicipioImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirInserirMunicipio")
	public String exibirInserirMunicipio(Model model, HttpServletRequest request, MunicipioImpl municipio,
			BindingResult bindingResult) {

		String view = "exibirInserirMunicipio";

		try {

			Collection<UnidadeFederacao> listaUnidadeFederacao = controladorMunicipio.listarUnidadeFederacao();
			model.addAttribute(LISTA_UNIDADE_FEDERACAO, listaUnidadeFederacao);
			
			this.popularListasDeMunicipio(municipio, model);
			model.addAttribute(MUNICIPIO, municipio);
			
			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_PESQUISAR_MUNICIPIO;
		}

		return view;
	}

	/**
	 * Método responsável pela inclusão de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param municipio - {@link MunicipioImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("inserirMunicipio")
	public String inserirMunicipio(Model model, HttpServletRequest request, MunicipioImpl municipio, BindingResult bindingResult) {

		String view = "forward:/exibirInserirMunicipio";

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(municipio.getDescricao(), municipio.getClass().getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(municipio);
			controladorTabelaAuxiliar.pesquisarMunicipioTabelaAuxiliar(municipio, "Município");
			controladorTabelaAuxiliar.inserir(municipio);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Municipio.MUNICIPIO_ROTULO);
			view = this.pesquisarMunicipio(model, municipio, bindingResult, municipio.isHabilitado());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de atualização de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param municipioParam - {@link MunicipioImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAtualizarMunicipio")
	public String exibirAtualizarMunicipio(Model model, HttpServletRequest request, MunicipioImpl municipioParam,
			BindingResult bindingResult) {

		String view = "exibirAtualizarMunicipio";
		Municipio municipio = municipioParam;
		try {
			if (!super.isPostBack(request)) {
				municipio = (Municipio) controladorTabelaAuxiliar.obter(municipio.getChavePrimaria(), MunicipioImpl.class);

			}
			this.popularListasDeMunicipio(municipio, model);

			Collection<UnidadeFederacao> listaUnidadeFederacao = controladorMunicipio.listarUnidadeFederacao();
			model.addAttribute(LISTA_UNIDADE_FEDERACAO, listaUnidadeFederacao);
			model.addAttribute(MUNICIPIO, municipio);

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_PESQUISAR_MUNICIPIO;
		}

		return view;
	}

	private void popularListasDeMunicipio(Municipio municipio, Model model) throws GGASException {
		if(municipio != null) {
			if (municipio.getUnidadeFederacao() != null) {
				Collection<Regiao> listaRegioes =
						controladorGeografico.obterRegioesPorUnidadeFederacao(municipio.getUnidadeFederacao().getChavePrimaria());
				model.addAttribute(LISTA_REGIAO, listaRegioes);
			}

			if (municipio.getRegiao() != null) {
				Collection<Microrregiao> listaMicrorregioes =
						controladorGeografico.obterMicrorregioesPorRegiao(municipio.getRegiao().getChavePrimaria());
				model.addAttribute("listaMicrorregioes", listaMicrorregioes);
			}
		}
	}

	/**
	 * Método responsável pela atualização de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param municipio - {@link MunicipioImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("atualizarMunicipio")
	public String atualizarMunicipio(Model model, HttpServletRequest request, MunicipioImpl municipio, BindingResult bindingResult) {

		String view = "forward:/exibirAtualizarMunicipio";
		model.addAttribute(MUNICIPIO, municipio);

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(municipio.getDescricao(), municipio.getClass().getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(municipio);
			controladorTabelaAuxiliar.pesquisarMunicipioTabelaAuxiliar(municipio, "Município");
			controladorTabelaAuxiliar.atualizar(municipio, municipio.getClass());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Municipio.MUNICIPIO_ROTULO);
			view = this.pesquisarMunicipio(model, municipio, bindingResult, municipio.isHabilitado());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela remoção de municípios.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param chavesPrimarias
	 * @return view - {@link String}
	 */
	@RequestMapping("removerMunicipio")
	public String removerMunicipio(Model model, HttpServletRequest request,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) {

		String view = "forward:/pesquisarMunicipio";

		try {
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, MunicipioImpl.class, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Municipio.MUNICIPIO_ROTULO);
			view = this.pesquisarMunicipio(model, null, null, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável pelo detalhamento de um município.
	 * 
	 * @param model - {@link Model}
	 * @param chavePrimaria - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoMunicipio")
	public String exibirDetalhamentoMunicipio(Model model, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		String view = "exibirDetalhamentoMunicipio";

		try {
			Municipio municipio = (Municipio) controladorTabelaAuxiliar.obter(chavePrimaria, MunicipioImpl.class);
			model.addAttribute(MUNICIPIO, municipio);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_EXIBIR_PESQUISAR_MUNICIPIO;
		}

		return view;
	}

	private Map<String, Object> prepararFiltro(Municipio municipio, Boolean habilitado) {
		Map<String, Object> filtro = new HashMap<>();

		if (municipio != null) {
			if (municipio.getDescricao() != null && !StringUtils.isEmpty(municipio.getDescricao())) {
				filtro.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, municipio.getDescricao());
			}

			if (municipio.getChavePrimaria() > 0) {
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, municipio.getChavePrimaria());
			}

			this.prepararFiltroRegioesGeograficas(municipio, filtro);

			Long codigoIBGE = municipio.getUnidadeFederativaIBGE();
			if (codigoIBGE != null) {
				filtro.put(CODIGO_IBGE, codigoIBGE);
			}
		}

		if (habilitado != null) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		}

		return filtro;
	}

	private void prepararFiltroRegioesGeograficas(Municipio municipio, Map<String, Object> filtro) {

		Microrregiao microrregiao = municipio.getMicrorregiao();
		if (microrregiao != null && microrregiao.getChavePrimaria() != -1) {
			filtro.put(ID_MICRORREGIAO, microrregiao.getChavePrimaria());
		}

		Regiao regiao = municipio.getRegiao();
		if (regiao != null && regiao.getChavePrimaria() != -1) {
			filtro.put(ID_REGIAO, regiao.getChavePrimaria());
		}

		UnidadeFederacao unidadeFederacao = municipio.getUnidadeFederacao();
		if (unidadeFederacao != null && unidadeFederacao.getChavePrimaria() != -1) {
			filtro.put(ID_UNI_FED, unidadeFederacao.getChavePrimaria());
		}
	}
}
