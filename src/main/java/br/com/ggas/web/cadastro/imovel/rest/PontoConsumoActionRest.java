/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.imovel.rest;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.Map;

/**
 * Action de requisições rest do ponto de consumo
 * @author jose.victor@logiquesistemas.com.br
 */
@Controller
@RequestMapping("rest/pontoConsumo")
@SuppressWarnings("common-java:InsufficientCommentDensity")
public class PontoConsumoActionRest extends GenericActionRest {

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Busca os pontos de consumo por sua descrição de forma paginada
	 * @param quantidadeMax quantidade máxima de registros que a consulta irá trazer
	 * @param pagina número do offset de registros
	 * @param pontoConsumo descrição do ponto de consumo que será utilziado para filtroar
	 * @return retorna a lista de pontos de consumos
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/buscarPaginado", produces = "application/json; charset=utf-8",
			method = { RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> buscarPontoConsumoPaginado(@RequestParam(value = "quantidadeMax") int quantidadeMax,
			@RequestParam(value = "pagina") int pagina, @RequestParam(value = "pontoConsumo") String pontoConsumo) throws GGASException {
		final Collection<PontoConsumo> pontos = controladorPontoConsumo.listarPontosConsumoDescricao(pontoConsumo, quantidadeMax, pagina);
		final String json = serializarJson(pontos);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Busca a listade pontos de consumos associados a um imóvel
	 * @param imovel chave primaria do imovel
	 * @return retorna a lista de pontos de consumo em json
	 * @throws GGASException exceção lançada caso ocorra falha
	 */
	@RequestMapping(value = "/buscarPorImovel", produces = "application/json; charset=utf-8",
			method = { RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> buscarPontosConsumoPorImovel(@RequestParam(value = "imovel") Long imovel) throws GGASException {
		final Collection<PontoConsumo> pontos = controladorPontoConsumo.consultarPontosConsumoFilhosImovelCondominio(imovel);
		final String json = serializarJson(pontos);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder()
				.put(PontoConsumo.class, new PontoConsumoSerializador())
				.build();
	}

}
