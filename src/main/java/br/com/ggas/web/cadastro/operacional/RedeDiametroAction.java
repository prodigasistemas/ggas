/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.operacional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.impl.RedeDiametroImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes ao manter Diâmetro de Rede.
 */
@Controller
public class RedeDiametroAction extends GenericAction {

	private static final String EXIBIR_PESQUISAR_DIAMETRO_REDE = "exibirPesquisarRedeDiametro";

	private static final String REDE_DIAMETRO = "redeDiametro";

	private static final String EXIBIR_ATUALIZAR_REDE_DIAMETRO = "exibirAtualizarRedeDiametro";

	private static final String EXIBIR_INSERIR_REDE_DIAMETRO = "exibirInserirRedeDiametro";

	private static final Class<RedeDiametroImpl> CLASSE = RedeDiametroImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.operacional.impl.RedeDiametroImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_REDE_DIAMETRO = "listaRedeDiametro";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de Diamêtro de Rede
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISAR_DIAMETRO_REDE)
	public String exibirPesquisaDiametroRede(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISAR_DIAMETRO_REDE;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Diamêtro de Rede.
	 * 
	 * @param redeDiametro - {@link RedeDiametroImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarRedeDiametro")
	public String pesquisarDiametroRede(RedeDiametroImpl redeDiametro, BindingResult bindingResult,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(redeDiametro, habilitado);

		Collection<TabelaAuxiliar> listaDiametroRede = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_REDE_DIAMETRO, listaDiametroRede);
		model.addAttribute(REDE_DIAMETRO, redeDiametro);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISAR_DIAMETRO_REDE;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Diâmetro de Rede.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoRedeDiametro")
	public String exibirDetalhamentoDiametroRede(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			RedeDiametro redeDiametro = (RedeDiametro) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(REDE_DIAMETRO, redeDiametro);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisarRedeDiametro";
		}

		return "exibirDetalhamentoRedeDiametro";
	}

	/**
	 * Método responsável por exibir a tela de inserir Diâmetro de Rede.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_REDE_DIAMETRO)
	public String exibirInserirDiametroRede() {

		return EXIBIR_INSERIR_REDE_DIAMETRO;
	}

	/**
	 * Método responsável por inserir um Diâmero de Rede.
	 * 
	 * @param redeDiametro - {@link RedeDiametroImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirRedeDiametro")
	private String inserirDiametroRede(RedeDiametroImpl redeDiametro, BindingResult bindingResult,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = null;

		model.addAttribute(REDE_DIAMETRO, redeDiametro);

		if (!redeDiametro.validarDados().isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(redeDiametro.validarDados()));
			return EXIBIR_INSERIR_REDE_DIAMETRO;
		}

		try {
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(redeDiametro, "Diâmetro da Rede");
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(redeDiametro);
			controladorTabelaAuxiliar.inserir(redeDiametro);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, RedeDiametro.REDE_DIAMETRO);
			retorno = pesquisarDiametroRede(redeDiametro, bindingResult, redeDiametro.isHabilitado(), model);
		} catch (NegocioException e) {
			retorno = EXIBIR_INSERIR_REDE_DIAMETRO;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Diâmetro de Rede.
	 * 
	 * @param redeDiametro - {@link RedeDiametroImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param redeDiametroParam - {@link RedeDiametroImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_REDE_DIAMETRO)
	public String exibirAtualizarDiametroRede(RedeDiametroImpl redeDiametroParam, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		RedeDiametro redeDiametro = redeDiametroParam;

		try {
			redeDiametro = (RedeDiametro) controladorTabelaAuxiliar.obter(redeDiametro.getChavePrimaria(), CLASSE);
			model.addAttribute(REDE_DIAMETRO, redeDiametro);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisarRedeDiametro";
		}

		return EXIBIR_ATUALIZAR_REDE_DIAMETRO;
	}

	/**
	 * Método responsável por atualizar um Diâmetro de Rede.
	 * 
	 * @param redeDiametro - {@link RedeDiametroImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarRedeDiametro")
	private String atualizarDiametroRede(RedeDiametroImpl redeDiametro, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(redeDiametro);
			controladorTabelaAuxiliar.atualizar(redeDiametro, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, RedeDiametro.REDE_DIAMETRO);
			retorno = pesquisarDiametroRede(redeDiametro, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(REDE_DIAMETRO, redeDiametro);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_REDE_DIAMETRO;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover um Diâmetro da Rede.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerRedeDiametro")
	public String removerDiametroRede(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
					HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, RedeDiametro.REDE_DIAMETRO);
			return pesquisarDiametroRede(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarRedeDiametro";

	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Diâmetro da Rede.
	 * 
	 * @param redeDiametro - {@link RedeDiametroImpl}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(RedeDiametroImpl redeDiametro, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (redeDiametro != null) {

			if (redeDiametro.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, redeDiametro.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(redeDiametro.getDescricao())) {
				filtro.put(DESCRICAO, redeDiametro.getDescricao());
			}

		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
