/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.localidade;

/**
 * @author enascimento
 * 
 * Classe responsável por realizar
 * a interpolação de dados entre
 * o formulário de quadra e o QuadraAction
 */
public class QuadraFaceVO {

	private Integer indexListas;

	private Integer redeIndicador;

	private Long idRede;

	private String numeroFace;

	private String cep;

	private String chaveCep;

	/**
	 * @return Integer
	 */
	public Integer getIndexListas() {
		return indexListas;
	}

	/**
	 * @param indexListas
	 */
	public void setIndexListas(Integer indexListas) {
		this.indexListas = indexListas;
	}

	/**
	 * @return Integer
	 */
	public Integer getRedeIndicador() {
		return redeIndicador;
	}

	/**
	 * @param redeIndicador
	 */
	public void setRedeIndicador(Integer redeIndicador) {
		this.redeIndicador = redeIndicador;
	}

	/**
	 * @return Long
	 */
	public Long getIdRede() {
		return idRede;
	}

	/**
	 * @param idRede 
	 */
	public void setIdRede(Long idRede) {
		this.idRede = idRede;
	}

	/**
	 * @return String
	 */
	public String getNumeroFace() {
		return numeroFace;
	}

	/**
	 * @param numeroFace 
	 */
	public void setNumeroFace(String numeroFace) {
		this.numeroFace = numeroFace;
	}

	/**
	 * @return String
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * @param cep
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * @return String
	 */
	public String getChaveCep() {
		return chaveCep;
	}

	/**
	 * @param chaveCep
	 */
	public void setChaveCep(String chaveCep) {
		this.chaveCep = chaveCep;
	}

}
