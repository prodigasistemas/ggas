package br.com.ggas.web.cadastro.cliente.rest;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.geral.apresentacao.serializadorjson.AbstractGGASSerializador;
import br.com.ggas.geral.apresentacao.serializadorjson.GenericSafeJsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Serializador simples da classe Cliente.
 * Serializa apenas os atributos mais simples da classe.
 * @author jose.victor@logiquesistemas.com.br
 */
public class ClienteSimplesSerializador extends AbstractGGASSerializador<Cliente> {

	@Override
	public void serializar(Cliente value, GenericSafeJsonGenerator jgen, SerializerProvider provider) throws IOException {
		jgen.writeStartObject();
		jgen.writeNumberField("chavePrimaria", value.getChavePrimaria());
		jgen.writeStringField("nome", value.getNome());
		jgen.writeStringField("nomeFantasia", value.getNomeFantasia());
		jgen.writeStringField("cnpj", value.getCnpj());
		jgen.writeStringField("cpf", value.getCpf());
		jgen.writeStringField("rg", value.getRg());
		jgen.writeStringField("emailPrincipal", value.getEmailPrincipal());
		jgen.writeObjectField("fones", value.getFones());
		jgen.writeEndObject();
	}

}
