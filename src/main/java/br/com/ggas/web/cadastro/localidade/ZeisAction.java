
package br.com.ggas.web.cadastro.localidade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.localidade.Zeis;
import br.com.ggas.cadastro.localidade.impl.ZeisImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;


/**
 * Classe Responsável por Ações referentes 
 * a Funcionalidade "Zeis"
 * 
 */
@Controller
public class ZeisAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String ZEIS = "zeis";

	private static final String EXIBIR_ALTERAR_ZEIS = "exibirAlterarZeis";

	private static final String EXIBIR_INCLUIR_ZEIS = "exibirIncluirZeis";

	private static final Class<ZeisImpl> CLASSE = ZeisImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.cadastro.localidade.impl.ZeisImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_ZEIS = "listaZeis";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de 
	 * pesquisa da Funcionalidade "Zeis".
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaZeis")
	public String exibirPesquisaZeis(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaZeis";
	}

	/**
	 * Método responsável pela ação de 
	 * pesquisar uma Zeis.
	 * 
	 * @param zeis - {@link ZeisImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarZeis")
	public String pesquisarZeis(ZeisImpl zeis, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(zeis, habilitado);

		Collection<TabelaAuxiliar> listaZeis= controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_ZEIS, listaZeis);
		model.addAttribute(ZEIS, zeis);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaZeis(model);
	}

	/**
	 * Método responsável por exibir o a tela de
	 * detalhamento da funcionalidade "Zeis".
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoZeis")
	public String exibirDetalhamentoZeis(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			Zeis zeis = (Zeis) controladorTabelaAuxiliar.obter(chavePrimaria, ZeisImpl.class);
			model.addAttribute(ZEIS, zeis);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaZeis";
		}

		return "exibirDetalhamentoZeis";
	}

	/**
	 * Método responsável por exibir a tela de 
	 * incluir da funcionalidade "Zeis".
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INCLUIR_ZEIS)
	public String exibirIncluirZeis() {

		return EXIBIR_INCLUIR_ZEIS;
	}

	/**
	 * Método responsável pela ação de 
	 * incluir uma Zeis.
	 * 
	 * @param zeis - {@link ZeisImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("incluirZeis")
	private String incluirZeis(ZeisImpl zeis, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;

		try {
			if (zeis.getDescricao() != null && zeis.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (zeis.getDescricaoAbreviada() != null && zeis.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(zeis);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(zeis, "Zeis");
			controladorTabelaAuxiliar.inserir(zeis);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(Zeis.ZEIS_MENSAGEM));
			retorno = pesquisarZeis(zeis, result, model, zeis.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(ZEIS, zeis);
			retorno = EXIBIR_INCLUIR_ZEIS;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de 
	 * alteração da funcionalidade "zeis".
	 * 
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param zeisParam - {@link zeisImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ALTERAR_ZEIS)
	public String exibirAlterarZeis(ZeisImpl zeisParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Zeis zeis = zeisParam;

		try {
			zeis = (Zeis) controladorTabelaAuxiliar.obter(zeis.getChavePrimaria(), CLASSE);
			model.addAttribute(ZEIS, zeis);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaZeis";
		}

		return EXIBIR_ALTERAR_ZEIS;
	}

	/**
	 * Método responsável pela ação de 
	 * alterar uma Zeis.
	 * 
	 * @param zeis - {@link ZeisImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarZeis")
	private String alterarZeis(ZeisImpl zeis, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (zeis.getDescricao() != null && zeis.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (zeis.getDescricaoAbreviada() != null && zeis.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(zeis);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(zeis, "Zeis");
			controladorTabelaAuxiliar.atualizar(zeis, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Zeis.ZEIS_MENSAGEM));
			retorno = pesquisarZeis(zeis, result, model, zeis.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(ZEIS, zeis);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ALTERAR_ZEIS;
		}

		return retorno;
	}

	/**
	 * Método responsável pela ação de 
	 * remover uma Zeis.
	 * 
	 * @param zeis {@link ZeisImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerZeis")
	public String removerZeis(ZeisImpl zeis, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(Zeis.ZEIS_MENSAGEM));

			return pesquisarZeis(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarZeis";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa da 
	 * funcionalidade "Zeis".
	 * 
	 * @param zeis - {@link Zeis}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(Zeis zeis, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (zeis != null) {
			if (zeis.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, zeis.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(zeis.getDescricao())) {
				filtro.put(DESCRICAO, zeis.getDescricao());
			}

			if (StringUtils.isNotEmpty(zeis.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, zeis.getDescricaoAbreviada());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
	
}
