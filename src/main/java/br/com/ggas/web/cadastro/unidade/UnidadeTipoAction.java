package br.com.ggas.web.cadastro.unidade;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Constantes.TipoUnidadeTipo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe responsável pelos metodos de ações referentes ao Tipo de Unidade.
 */
@Controller
public class UnidadeTipoAction extends GenericAction {

	private static final String DESCRICAO = "descricao";
	private static final String DESC_ABREVIADA = "descricaoAbreviada";
	private static final String TIPO_UNIDADE = "tipoUnidade";
	private static final String HABILITADO = "habilitado";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String NIVEL = "nivel";
	private static final String TIPO = "tipo";
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl";


	@Autowired
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa Tipo de Unidade.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaTipoUnidade")
	public String exibirPesquisaTipoUnidade(Model model) throws NegocioException {

		Collection<String> lista = controladorUnidadeOrganizacional.listarNivelUnidadeTipo();
		model.addAttribute("listaNivelUnidadeTipo", lista);

		popularListaTipo(model);
		
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}
		
		return "exibirPesquisaTipoUnidade";
	}

	/**
	 * Método responsável por efetuar a pesquisa dos Tipo de Unidade.
	 * 
	 * @param tipoUnidade - {@link UnidadeTipoImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarTipoUnidade")
	public String pesquisarTipoUnidade(Model model, UnidadeTipoImpl tipoUnidade, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(tipoUnidade, habilitado);

		Collection<TabelaAuxiliar> listaTipoUnidade = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);
		model.addAttribute(TIPO_UNIDADE, tipoUnidade);
		model.addAttribute("listaTipoUnidade", listaTipoUnidade);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaTipoUnidade(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de um Tipo de Unidade.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoTipoUnidade")
	public String exibirDetalhamentoTipoUnidade(Model model, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) {

		try {
			UnidadeTipo tipoUnidade = (UnidadeTipo) controladorTabelaAuxiliar.obter(chavePrimaria, UnidadeTipoImpl.class);
			model.addAttribute(TIPO_UNIDADE, tipoUnidade);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaTipoUnidade";
		}

		return "exibirDetalhamentoTipoUnidade";
	}

	/**
	 * Método responsável por exibir a tela de inserir Tipo de Unidade.
	 * 
	 * @param model - {@link Model}
	 * @param tipoUnidade - {@link UnidadeTipoImpl}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirInserirTipoUnidade")
	public String exibirInserirTipoUnidade(Model model, UnidadeTipoImpl tipoUnidade, HttpServletRequest request) {

		popularListaTipo(model);
		
		if (isPostBack(request)) {
			model.addAttribute(TIPO_UNIDADE, tipoUnidade);
		}

		saveToken(request);

		return "exibirInserirTipoUnidade";
	}

	/**
	 * Método responsável por executar a inserção de um Tipo de Unidade.
	 * 
	 * @param tipoUnidade - {@link UnidadeTipoImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirTipoUnidade")
	public String inserirTipoUnidade(Model model, UnidadeTipoImpl tipoUnidade, BindingResult result, HttpServletRequest request)
			throws GGASException {

		String retorno = null;
		Collection<String> listaTipo = null;

		try {
			preencherAtributos(tipoUnidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoUnidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoUnidade, "Tipo de Unidade");
			controladorTabelaAuxiliar.inserir(tipoUnidade);

			Collection<String> lista = controladorUnidadeOrganizacional.listarNivelUnidadeTipo();
			model.addAttribute("listaNivelUnidadeTipo", lista);

			listaTipo = controladorUnidadeOrganizacional.listarTipoUnidadeTipo();
			model.addAttribute("listaTipoUnidadeTipo", listaTipo);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(UnidadeTipo.TIPO_UNIDADE));
			retorno = pesquisarTipoUnidade(model, tipoUnidade, result, tipoUnidade.isHabilitado());

		} catch (GGASException e) {

			popularListaTipo(model);
			model.addAttribute(TIPO_UNIDADE, tipoUnidade);

			super.mensagemErroParametrizado(model, request, e);
			retorno = "exibirInserirTipoUnidade";

		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de alterar o Tipo de Unidade.
	 * 
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param tipoUnidadeParam - {@link UnidadeTipoImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlterarTipoUnidade")
	public String exibirAlterarTipoUnidade(Model model, UnidadeTipoImpl tipoUnidadeParam, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) {

		UnidadeTipoImpl tipoUnidade = tipoUnidadeParam;
		try {
			popularListaTipo(model);
			tipoUnidade = (UnidadeTipoImpl) controladorTabelaAuxiliar.obter(tipoUnidade.getChavePrimaria(), UnidadeTipoImpl.class);
			model.addAttribute(TIPO_UNIDADE, tipoUnidade);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaTipoUnidade";
		}

		return "exibirAlterarTipoUnidade";
	}

	/**
	 * Método responsável por efetuar a alteração de um Tipo de Unidade.
	 * 
	 * @param tipoUnidade - {@link UnidadeTipoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */

	@RequestMapping("alterarTipoUnidade")
	public String alterarTipoUnidade(Model model, UnidadeTipoImpl tipoUnidade, BindingResult result, HttpServletRequest request)
			throws GGASException {

		
		String retorno = null;

		try {

			preencherAtributos(tipoUnidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoUnidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoUnidade, "Tipo de Unidade");
			controladorTabelaAuxiliar.atualizar(tipoUnidade, UnidadeTipoImpl.class);
			popularListaTipo(model);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(UnidadeTipo.TIPO_UNIDADE));
			retorno = pesquisarTipoUnidade(model, tipoUnidade, result, tipoUnidade.isHabilitado());
		} catch (NegocioException e) {
			
			popularListaTipo(model);
			model.addAttribute(TIPO_UNIDADE, tipoUnidade);
			super.mensagemErroParametrizado(model, request, e);
			retorno = "exibirAlterarTipoUnidade";
		}

		return retorno;
	}

	/**
	 * Método responsável por executar a remoção um Tipo de Unidade.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerTipoUnidade")
	public String removerTipoUnidade(Model model, HttpServletRequest request,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) throws NegocioException {

	
		try {
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, UnidadeTipoImpl.class, dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, super.obterMensagem(UnidadeTipo.TIPO_UNIDADE));

			return pesquisarTipoUnidade(model, null, null, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarTipoUnidade";
	}

	private Map<String, Object> prepararFiltro(UnidadeTipoImpl tipoUnidade, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();
		if (tipoUnidade != null) {
			if (tipoUnidade.getDescricao() != null && !tipoUnidade.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, tipoUnidade.getDescricao());
			}

			if (tipoUnidade.getDescricaoAbreviada() != null && !tipoUnidade.getDescricaoAbreviada().isEmpty()) {
				filtro.put(DESC_ABREVIADA, tipoUnidade.getDescricaoAbreviada());
			}

			if (tipoUnidade.getChavePrimaria() != 0 && tipoUnidade.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, tipoUnidade.getChavePrimaria());
			}
			filtro = prepararListaFiltro(tipoUnidade, filtro);
			
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	private Map<String, Object> prepararListaFiltro(UnidadeTipoImpl tipoUnidade, Map<String, Object> filtro ){
		
		String nivel = String.valueOf(tipoUnidade.getNivel());
		
		if (!"-1".equals(nivel)) {
			filtro.put(NIVEL, String.valueOf(tipoUnidade.getNivel()));
		}

		if (!"-1".equals(tipoUnidade.getTipo())) {
			filtro.put(TIPO, tipoUnidade.getTipo());
		}
		return filtro;
	}
	
	private void popularListaTipo(Model model) {

		TipoUnidadeTipo[] tipoUnidadeTipo = TipoUnidadeTipo.values();
		Collection<String> listaTipo = new ArrayList<String>();
		for (int i = 0; i < tipoUnidadeTipo.length; i++) {
			listaTipo.add(tipoUnidadeTipo[i].getValor());
		}
		

		model.addAttribute("arrayTipoUnidadeTipo", listaTipo);

	}

	private void preencherAtributos(UnidadeTipoImpl tipoUnidade) throws NegocioException {
		
		String descricao = tipoUnidade.getDescricao();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(descricao, UnidadeTipoImpl.class.getName());
			tipoUnidade.setDescricao(descricao);

		}
		
		String descricaoAbreviada = tipoUnidade.getDescricaoAbreviada();
		if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(descricaoAbreviada, UnidadeTipoImpl.class.getName());
			tipoUnidade.setDescricaoAbreviada(descricaoAbreviada);
		}

		Integer nivel = tipoUnidade.getNivel();
		if (nivel != null) {

			tipoUnidade.setNivel(nivel);
		}

		String tipo = tipoUnidade.getTipo();
		if ("-1".equals(tipo)) {

			tipoUnidade.setTipo(null);
		}
	}
}
