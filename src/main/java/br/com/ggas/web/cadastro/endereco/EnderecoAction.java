package br.com.ggas.web.cadastro.endereco;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
/**
 * 
 * Classe responsável pela tela relacionada ao cep do endereço
 *
 */
@Controller
public class EnderecoAction extends GenericAction {

	@Autowired
	private ControladorEndereco controladorEndereco;

	/**
	 * Método responsável por exibir a tela de importar cep.
	 * 
	 * @return view
	 */
	@RequestMapping("exibirImportarCep")
	public String exibirImportarCep() {

		return "exibirImportarCep";
	}

	/**
	 * Método responsável pela funcionalidade de importar cep.
	 * 
	 * @param mapping the mapping
	 * @param form O formulário
	 * @param request O objeto request
	 * @param response O objeto response
	 * @param model
	 * @param arquivoForm
	 * @return ActionForward O retorno da ação
	 * @throws GGASException Caso ocorra algum erro
	 */
	@RequestMapping("importarCep")
	public String importarCep(Model model, @RequestParam(value = "arquivo") MultipartFile arquivoForm, HttpServletRequest request)
			throws GGASException {

		int qtdIncluidas = 0;
		int qtdAlteradas = 0;
		int qtdRejeitadas = 0;

		getDadosAuditoria(request);

		boolean sucessoImportacao = Boolean.FALSE;
		if ((arquivoForm == null) || (StringUtils.isEmpty(arquivoForm.getOriginalFilename()))) {
			super.mensagemErro(model, request, Constantes.ERRO_ARQUIVO_NAO_SELECIONADO);
		} else {
			Map<String, Integer> resumoImportacao;
			try {
				resumoImportacao = controladorEndereco.importarCep(arquivoForm.getInputStream(), arquivoForm.getContentType());
			} catch (IOException e) {
				LOG.error(e);
				throw new GGASException(e);
			}

			if (resumoImportacao.containsKey(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_INCLUIDAS)) {
				qtdIncluidas = resumoImportacao.get(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_INCLUIDAS);
			}
			if (resumoImportacao.containsKey(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_ALTERADAS)) {
				qtdAlteradas = resumoImportacao.get(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_ALTERADAS);
			}
			if (resumoImportacao.containsKey(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS)) {
				qtdRejeitadas = resumoImportacao.get(Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
			}

			if ((qtdIncluidas == 0) && (qtdAlteradas == 0) && (qtdRejeitadas == 0)) {
				super.mensagemErro(model, request, Constantes.FALHA_FORMATO_IMPORTACAO);
			} else {
				if (Objects.equals(qtdRejeitadas, resumoImportacao.get("qtdRejeitadas"))) {
					super.mensagemAlerta(model, Constantes.SUCESSO_IMPORTACAO_TODOS_REJEITADOS);
				} else {
					super.mensagemSucesso(model, Constantes.SUCESSO_IMPORTACAO);
				}
				model.addAttribute("qtdIncluidas", qtdIncluidas);
				model.addAttribute("qtdAlteradas", qtdAlteradas);
				model.addAttribute("qtdRejeitadas", qtdRejeitadas);
				sucessoImportacao = Boolean.TRUE;
			}
		}

		model.addAttribute("sucessoImportacao", sucessoImportacao);

		return "exibirImportarCep";
	}
}
