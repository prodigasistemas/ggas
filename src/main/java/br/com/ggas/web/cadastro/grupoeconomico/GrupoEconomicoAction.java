/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.grupoeconomico;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.impl.GrupoEconomicoImpl;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * 
 * @author Orube
 *
 */
@Controller
public class GrupoEconomicoAction extends GenericAction {

	private static final String EXIBIR_PESQUISA_GRUPO_ECONOMICO = "exibirPesquisaGrupoEconomico";

	private static final String TELA_EXIBIR_ALTERAR_GRUPO_ECONOMICO = "exibirAlterarGrupoEconomico";

	private static final String TELA_EXIBIR_INCLUIR_GRUPO_ECONOMICO = "exibirIncluirGrupoEconomico";

	protected static final String FORWARD_SUCESSO = "sucesso";

	private static final String LISTA_GRUPO_ECONOMICO = "listaGrupoEconomico";

	private static final String GRUPO_ECONOMICO = "grupoEconomico";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String ID_CLIENTE = "idCliente";

	private static final String LISTA_ID_CLIENTE = "listaIdCliente";

	private static final String LISTA_CLIENTE = "listaCliente";

	protected static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String SEGMENTO = "segmento";
	
	private String retorno;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorGrupoEconomico")
	private ControladorGrupoEconomico controladorGrupoEconomico;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	/**
	 * 
	 * @param request
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping(EXIBIR_PESQUISA_GRUPO_ECONOMICO)
	public String exibirPesquisaGrupoEconomico(HttpServletRequest request, Model model) throws NegocioException {

		HttpSession session = request.getSession(false);
		session.removeAttribute(LISTA_CLIENTE);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		return EXIBIR_PESQUISA_GRUPO_ECONOMICO;
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param habilitado
	 * @param idCliente
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping("pesquisarGrupoEconomico")
	public String pesquisarGrupoEconomico(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico, BindingResult result,
			@RequestParam(HABILITADO) String habilitado, @RequestParam("idCliente") Long idCliente, Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		prepararFiltro(filtro, grupoEconomico, habilitado, idCliente, null);
		if (idCliente != null) {
			ClienteImpl cliente = (ClienteImpl) controladorCliente.obter(idCliente);
			model.addAttribute("cliente", cliente);
			if(!cliente.getEnderecos().isEmpty()) {				
				model.addAttribute("endereco", cliente.getEnderecoPrincipal().getEnderecoFormatado());
			}
		}
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(LISTA_GRUPO_ECONOMICO, controladorGrupoEconomico.consultarGrupoEconomico(filtro));
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		return EXIBIR_PESQUISA_GRUPO_ECONOMICO;
	}

	/**
	 * 
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping(TELA_EXIBIR_INCLUIR_GRUPO_ECONOMICO)
	public String exibirIncluirGrupoEconomico(Model model) throws NegocioException {

		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		return TELA_EXIBIR_INCLUIR_GRUPO_ECONOMICO;
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping(value = "incluirGrupoEconomico", method = RequestMethod.POST)
	public String incluirGrupoEconomico(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico, BindingResult result,
			@RequestParam(HABILITADO) String habilitado, HttpServletRequest request, Model model) throws NegocioException   {

		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		retorno = TELA_EXIBIR_INCLUIR_GRUPO_ECONOMICO;

		Map<String, Object> erros = grupoEconomico.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				@SuppressWarnings("unchecked")
				Collection<Cliente> listaCliente = (Collection<Cliente>) request.getSession().getAttribute(LISTA_CLIENTE);

				if (listaCliente != null && !listaCliente.isEmpty()) {
					Long[] listaIdCliente = new Long[listaCliente.size()];
					int i = 0;
					adicionaIdsClientes(listaCliente, listaIdCliente, i);
					listaCliente = null;
					controladorGrupoEconomico.incluirGrupoEconomico(grupoEconomico);
					controladorGrupoEconomico.vincularClienteGrupoEconomico(listaIdCliente, grupoEconomico);
				} else {
					controladorGrupoEconomico.incluirGrupoEconomico(grupoEconomico);
				}
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, GrupoEconomico.GRUPO_ECONOMICO);
				retorno = pesquisarGrupoEconomico(grupoEconomico, result, habilitado, null, model);
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		HttpSession session = request.getSession(false);
		session.removeAttribute(LISTA_CLIENTE);

		return retorno;
	}

	private void adicionaIdsClientes(Collection<Cliente> listaCliente, Long[] listaIdCliente, int indice) {
		int i = indice;
		for (Cliente cliente : listaCliente) {
			listaIdCliente[i] = cliente.getChavePrimaria();
			i++;
		}
	}

	/**
	 * 
	 * @param request
	 * @param chavePrimaria
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping(TELA_EXIBIR_ALTERAR_GRUPO_ECONOMICO)
	public String exibirAlterarGrupoEconomico(HttpServletRequest request,
			@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws NegocioException {
		retorno = EXIBIR_PESQUISA_GRUPO_ECONOMICO;
		
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		
		GrupoEconomicoImpl grupoEconomico;
		
		try {
			grupoEconomico = (GrupoEconomicoImpl) controladorGrupoEconomico.obter(chavePrimaria, "segmento");
			
			if (grupoEconomico != null) {
				model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
				@SuppressWarnings("unchecked")
				Collection<Cliente> listaCliente = (Collection<Cliente>) request.getSession()
						.getAttribute(LISTA_CLIENTE);
				if (listaCliente == null) {
					listaCliente = controladorCliente.listarClientePorGrupoEconomico(chavePrimaria);
					request.getSession().setAttribute(LISTA_CLIENTE, listaCliente);
				}
				Segmento segmento = grupoEconomico.getSegmento();
				if (segmento != null) {
					request.setAttribute(SEGMENTO, segmento);
					model.addAttribute(ID_SEGMENTO, segmento.getChavePrimaria());
				}
			}
			retorno = TELA_EXIBIR_ALTERAR_GRUPO_ECONOMICO;
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping(value = "alterarGrupoEconomico", method = RequestMethod.POST)
	public String alterarGrupoEconomico(@ModelAttribute("chavePrimaria") GrupoEconomicoImpl grupoEconomico, BindingResult result,
			@RequestParam(HABILITADO) String habilitado, HttpServletRequest request, Model model) throws NegocioException {

		retorno = TELA_EXIBIR_ALTERAR_GRUPO_ECONOMICO;
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		Map<String, Object> erros = grupoEconomico.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				@SuppressWarnings("unchecked")
				Collection<Cliente> listaCliente = (Collection<Cliente>) request.getSession().getAttribute(LISTA_CLIENTE);

				if (listaCliente != null && !listaCliente.isEmpty()) {
					Long[] listaIdCliente = new Long[listaCliente.size()];
					int i = 0;
					adicionaIdsClientes(listaCliente, listaIdCliente, i);
					listaCliente = null;
					controladorGrupoEconomico.vincularClienteGrupoEconomico(listaIdCliente, grupoEconomico);
				} else {
					controladorGrupoEconomico.vincularClienteGrupoEconomico(null, grupoEconomico);
				}

				controladorGrupoEconomico.atualizarGrupoEconomico(grupoEconomico);

				HttpSession session = request.getSession(false);
				session.removeAttribute(LISTA_CLIENTE);

				if (grupoEconomico.getSegmento() != null) {
					Long idSegmento = grupoEconomico.getSegmento().getChavePrimaria();
					if (idSegmento != null && idSegmento > 0) {
						model.addAttribute(ID_SEGMENTO, idSegmento);
					}
				}
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GrupoEconomico.GRUPO_ECONOMICO);
				retorno = pesquisarGrupoEconomico(grupoEconomico, result, habilitado, null, model);
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping("exibirDetalharGrupoEconomico")
	public String exibirDetalharGrupoEconomico(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws NegocioException {

		retorno = EXIBIR_PESQUISA_GRUPO_ECONOMICO;
		try {
			GrupoEconomicoImpl grupoEconomico = (GrupoEconomicoImpl) controladorGrupoEconomico.obter(chavePrimaria);
			
			if (grupoEconomico != null) {
				model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
				Collection<Cliente> listaCliente = controladorCliente.listarClientePorGrupoEconomico(chavePrimaria);
				if (listaCliente != null && !listaCliente.isEmpty()) {
					model.addAttribute(LISTA_CLIENTE, listaCliente);
				}
				Segmento segmento = grupoEconomico.getSegmento();
				if (segmento != null) {
					Long idSegmento = segmento.getChavePrimaria();
					Segmento segmentoVinculado = controladorSegmento.obterSegmento(idSegmento);
					model.addAttribute(SEGMENTO, segmentoVinculado);
				}
			}
			retorno = "exibirDetalharGrupoEconomico";
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}


		return retorno;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("removerGrupoEconomico")
	public String removerGrupoEconomico(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			controladorGrupoEconomico.removerGrupoEconomico(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, GrupoEconomico.GRUPO_ECONOMICO);
			retorno = "forward:pesquisarGrupoEconomico";
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param request
	 * @param listaIds
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("incluirClienteGrupoEconomicoIncluir")
	public String incluirClienteGrupoEconomicoIncluir(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico,
			BindingResult result, HttpServletRequest request, @RequestParam("listaIdCliente") String listaIds, Model model)
			throws GGASException {

		incluirClienteGrupoEconomico(grupoEconomico, String.valueOf(grupoEconomico.isHabilitado()), listaIds, request);
		model.addAttribute(LISTA_ID_CLIENTE, listaIds);
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		return exibirIncluirGrupoEconomico(model);
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param listaIds
	 * @param request
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("incluirClienteGrupoEconomicoAlterar")
	public String incluirClienteGrupoEconomicoAlterar(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico,
			BindingResult result, @RequestParam("listaIdCliente") String listaIds, HttpServletRequest request, Model model)
			throws GGASException {

		incluirClienteGrupoEconomico(grupoEconomico, String.valueOf(grupoEconomico.isHabilitado()), listaIds, request);
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		return exibirAlterarGrupoEconomico(request, grupoEconomico.getChavePrimaria(), model);
	}

	/**
	 * 
	 * @param idCliente
	 * @param request
	 * @param model
	 * @param grupoEconomico
	 * @param result
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerClienteGrupoEconomicoIncluir")
	public String removerClienteGrupoEconomicoIncluir(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico,
			BindingResult result, @RequestParam("idCliente") Long idCliente, HttpServletRequest request, Model model)
			throws GGASException {

		removerClienteGrupoEconomico(idCliente, request);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		return TELA_EXIBIR_INCLUIR_GRUPO_ECONOMICO;
	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param result
	 * @param idCliente
	 * @param request
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("removerClienteGrupoEconomicoAlterar")
	public String removerClienteGrupoEconomicoAlterar(@ModelAttribute("GrupoEconomicoImpl") GrupoEconomicoImpl grupoEconomico,
			BindingResult result, @RequestParam("idCliente") Long idCliente, HttpServletRequest request, Model model)
			throws GGASException {

		removerClienteGrupoEconomico(idCliente, request);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(GRUPO_ECONOMICO, grupoEconomico);
		return TELA_EXIBIR_ALTERAR_GRUPO_ECONOMICO;
	}

	/**
	 * 
	 * @param idCliente
	 * @param request
	 * @throws GGASException
	 */
	@SuppressWarnings({ "unchecked" })
	private void removerClienteGrupoEconomico(@RequestParam("idCliente") Long idCliente, HttpServletRequest request) throws GGASException {

		Collection<Cliente> listaCliente = (Collection<Cliente>) request.getSession().getAttribute(LISTA_CLIENTE);

		if (listaCliente != null && !listaCliente.isEmpty()) {
			Iterator<Cliente> iterator = listaCliente.iterator();
			while (iterator.hasNext()) {
				Cliente cliente = iterator.next();
				if (cliente.getChavePrimaria() == idCliente) {
					iterator.remove();
				}
			}
		}
		request.getSession().setAttribute(LISTA_CLIENTE, listaCliente);

	}

	/**
	 * 
	 * @param filtro
	 * @param grupoEconomico
	 * @param habilitado
	 * @param idCliente
	 * @param listaIdCliente
	 */
	private void prepararFiltro(Map<String, Object> filtro, GrupoEconomicoImpl grupoEconomico, String habilitado, Long idCliente,
			String listaIdCliente) {

		if (grupoEconomico.getDescricao() != null || !StringUtils.isEmpty(grupoEconomico.getDescricao())) {
			filtro.put(DESCRICAO, grupoEconomico.getDescricao());
		}

		if (grupoEconomico.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, grupoEconomico.getChavePrimaria());
		}

		if (idCliente != null && idCliente > 0) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		if (listaIdCliente != null && !listaIdCliente.isEmpty()) {
			String[] ids = listaIdCliente.split(",");
			Long[] listaCliente = new Long[ids.length];
			int i = 0;
			for (String string : ids) {
				listaCliente[i] = Long.parseLong(string);
				i++;
			}
			filtro.put(LISTA_ID_CLIENTE, listaCliente);
		}

		if (grupoEconomico.getSegmento() != null) {
			filtro.put(ID_SEGMENTO, grupoEconomico.getSegmento().getChavePrimaria());
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}

	}

	/**
	 * 
	 * @param grupoEconomico
	 * @param habilitado
	 * @param listaIds
	 * @param request
	 * @throws NegocioException
	 */
	private void incluirClienteGrupoEconomico(GrupoEconomicoImpl grupoEconomico, String habilitado, String listaIds,
			HttpServletRequest request) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		prepararFiltro(filtro, grupoEconomico, habilitado, null, listaIds);

		Long[] listaIdCliente = (Long[]) filtro.get(LISTA_ID_CLIENTE);
		@SuppressWarnings("unchecked")
		Collection<Cliente> listaCliente = (Collection<Cliente>) request.getSession().getAttribute(LISTA_CLIENTE);
		if (listaCliente != null && !listaCliente.isEmpty()) {
			if (listaIdCliente != null && listaIdCliente.length > 0) {
				Collection<Cliente> listaClienteAdc = controladorCliente.consultarCliente(listaIdCliente);
				listaCliente.removeAll(listaClienteAdc);
				listaCliente.addAll(listaClienteAdc);
			}
			request.getSession().setAttribute(LISTA_CLIENTE, listaCliente);
		} else if (listaIdCliente != null && listaIdCliente.length > 0) {
			Collection<Cliente> listaClienteAdc = controladorCliente.consultarCliente(listaIdCliente);
			listaCliente = listaClienteAdc;
		}
		request.getSession().setAttribute(LISTA_CLIENTE, listaCliente);

	}
}
