/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.funcionario;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.FuncionarioAfastamento;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;

/**
 * 
 * @author Pedro Silva
 *
 */
@Controller
public class FuncionarioAction extends GenericAction {

	private static final String FUNCIONARIO = "funcionario";

	private static final String TELA_EXIBIR_INSERIR_FUNCIONARIO = "exibirInserirFuncionario";

	private static final Logger LOG = Logger.getLogger(FuncionarioAction.class);

	private static final String LISTA_UNIDADE_ORGANIZACIONAL = "listaUnidadeOrganizacional";

	private static final String EMPRESAS = "empresas";

	private static final String HABILITADO = "habilitado";

	private static final String ID_UNIDADE_ORGANIZACIONAL = "idUnidadeOrganizacional";

	private static final String ID_EMPRESA = "idEmpresa";

	private static final String DESCRICAO_CARGO = "descricaoCargo";

	private static final String NOME = "nome";

	private static final String MATRICULA = "matricula";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String EXIGE_MATRICULA_FUNCIONARIO = "exigeMatriculaFuncionario";

	private static final String TELA_GRID_AFASTAMENTO = "gridAfastamentoFuncionario";

	private static final String LISTA_AFASTAMENTO = "listaAfastamento";

	private static final String LISTA_REMOVIDA = "listaRemovida";

	private String retorno;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;
	
	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * 
	 * @param funcionario
	 * @param habilitado
	 * @param model
	 * @return exibirPesquisaFuncionario
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("exibirPesquisaFuncionario")
	public String exibirPesquisaFuncionario(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario,
			@RequestParam(value = "habilitado", required = false) String habilitado, Model model, HttpSession sessao) throws GGASException, IOException {

		carregarCampos(model, funcionario, null);
		model.addAttribute("habiltado", habilitado);
		model.addAttribute(FUNCIONARIO, funcionario);
		sessao.removeAttribute(LISTA_AFASTAMENTO);
		sessao.removeAttribute(LISTA_REMOVIDA);
		sessao.removeAttribute("fotoFuncionarioSessao");


		return "exibirPesquisaFuncionario";
	}

	/**
	 * 
	 * @param funcionario
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return exibirPesquisaFuncionario
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("pesquisarFuncionarios")
	public String pesquisarFuncionarios(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, BindingResult result,
			@RequestParam(value = "habilitado", required = false) String habilitado, Model model, HttpServletRequest request) throws GGASException, IOException  {

		carregarCampos(model, funcionario, null);
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, funcionario, habilitado);
		model.addAttribute("habilitado", habilitado);
		model.addAttribute("funcionarios", controladorFuncionario.consultarFuncionarios(filtro));
		model.addAttribute(FUNCIONARIO, funcionario);
		
		request.getSession().removeAttribute(LISTA_REMOVIDA);
		request.getSession().removeAttribute("fotoFuncionarioSessao");

		return "exibirPesquisaFuncionario";
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return exibirDetalhamentoFuncionario
	 * @throws Exception
	 */
	@RequestMapping("exibirDetalhamentoFuncionario")
	public String exibirDetalhamentoFuncionario(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
		throws NegocioException	 {
		FuncionarioImpl funcionario = (FuncionarioImpl) controladorFuncionario.obter(chavePrimaria);
		model.addAttribute(FUNCIONARIO, funcionario);
		model.addAttribute("fluxoDetalhamento", Boolean.TRUE);
		
		request.getSession().setAttribute(LISTA_AFASTAMENTO, funcionario.getListaAfastamento());
		
		return "exibirDetalhamentoFuncionario";
	}

	/**
	 * 
	 * @param funcionario
	 * @param model
	 * @return exibirInserirFuncionario
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping(TELA_EXIBIR_INSERIR_FUNCIONARIO)
	public String exibirInserirFuncionario(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, Model model, HttpServletRequest request)
			throws GGASException, IOException {

		model.addAttribute(EXIGE_MATRICULA_FUNCIONARIO, controladorFuncionario.exigeMatriculaFuncionario());
		model.addAttribute("funcionario", funcionario);
		model.addAttribute("formatosAceitosFotoFuncionario", formatosAceitos());
		model.addAttribute("motivosAfastamento", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Motivos Afastamento"));
		request.getSession().removeAttribute(LISTA_AFASTAMENTO);
		request.getSession().removeAttribute(LISTA_REMOVIDA);
		request.getSession().removeAttribute("fotoFuncionarioSessao");


		carregarCampos(model, funcionario, null);
		return TELA_EXIBIR_INSERIR_FUNCIONARIO;
	}

	/**
	 * 
	 * @param funcionario
	 * @param result
	 * @param request
	 * @param model
	 * @return pesquisarFuncionarios
	 * @throws GGASException 
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("inserirFuncionario")
	public String inserirFuncionario(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, BindingResult result,
			@RequestParam("fotoFuncionario") MultipartFile fotoFuncionario,
			HttpServletRequest request, Model model) throws GGASException, IOException {
		retorno = TELA_EXIBIR_INSERIR_FUNCIONARIO;
		try {
			model.addAttribute(EXIGE_MATRICULA_FUNCIONARIO, controladorFuncionario.exigeMatriculaFuncionario());
			model.addAttribute(FUNCIONARIO, funcionario);
			carregarCampos(model, funcionario, fotoFuncionario);
			Map<String, Object> erros = funcionario.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, new NegocioException(erros));
			} else {
				
				Collection<FuncionarioAfastamento> listaAfastamento = null;
				if(request.getSession().getAttribute(LISTA_AFASTAMENTO) != null) {
					listaAfastamento = new HashSet<>((Collection<FuncionarioAfastamento>) request.getSession().getAttribute(LISTA_AFASTAMENTO));
					listaAfastamento.stream().forEach(p -> p.setFuncionario(funcionario));
				}
				
				funcionario.setListaAfastamento(listaAfastamento);

				funcionario.setDadosAuditoria(getDadosAuditoria(request));
				controladorFuncionario.verificarOrigatoriedadeMatricula(funcionario.getMatricula());
				model.addAttribute(CHAVE_PRIMARIA, controladorFuncionario.inserir(funcionario));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Funcionario.FUNCIONARIO_CAMPO_STRING);
				retorno = pesquisarFuncionarios(funcionario, result, String.valueOf(funcionario.isHabilitado()), model, request);

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return exibirAtualizarFuncionario
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("exibirAtualizarFuncionario")
	public String exibirAtualizarFuncionario(@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = "removaFotoFuncionario", required = false) Boolean removaFotoFuncionario, Model model, HttpServletRequest request)
			throws GGASException, IOException {
		FuncionarioImpl funcionario = (FuncionarioImpl) controladorFuncionario.obter(chavePrimaria);
		carregarCampos(model, funcionario, null);
		
		if(funcionario.getFotoFuncionario() != null) {
			request.getSession().setAttribute("fotoFuncionarioSessao", funcionario.getFotoFuncionario());
		}
		
		if (removaFotoFuncionario != null && removaFotoFuncionario) {
			funcionario.setFotoFuncionario(null);
			request.getSession().removeAttribute("fotoFuncionarioSessao");
		}
		
		model.addAttribute(EXIGE_MATRICULA_FUNCIONARIO, controladorFuncionario.exigeMatriculaFuncionario());
		model.addAttribute(FUNCIONARIO, funcionario);
		model.addAttribute("formatosAceitosFotoFuncionario", formatosAceitos());
		model.addAttribute("motivosAfastamento", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Motivos Afastamento"));
		
		request.getSession().setAttribute(LISTA_AFASTAMENTO, funcionario.getListaAfastamento());

		return "exibirAtualizarFuncionario";
	}

	/**
	 * 
	 * @param funcionario
	 * @param result
	 * @param request
	 * @param model
	 * @return pesquisarFuncionarios
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("atualizarFuncionario")
	public String atualizarFuncionario(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, BindingResult result,
			@RequestParam("fotoFuncionario") MultipartFile fotoFuncionario,
			HttpServletRequest request, Model model) throws GGASException, IOException {

		retorno = "exibirAtualizarFuncionario";
		try {
			if (request.getSession().getAttribute("fotoFuncionarioSessao") != null) {
				funcionario.setFotoFuncionario((byte[]) request.getSession().getAttribute("fotoFuncionarioSessao"));
			}
			
			model.addAttribute(EXIGE_MATRICULA_FUNCIONARIO, controladorFuncionario.exigeMatriculaFuncionario());
			model.addAttribute(FUNCIONARIO, funcionario);
			carregarCampos(model, funcionario, fotoFuncionario);
			Map<String, Object> erros = funcionario.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, new NegocioException(erros));
			} else {
				
				Collection<FuncionarioAfastamento> listaAfastamento = null;
				if(request.getSession().getAttribute(LISTA_AFASTAMENTO) != null) {
					listaAfastamento = new HashSet<>((Collection<FuncionarioAfastamento>) request.getSession().getAttribute(LISTA_AFASTAMENTO));
					listaAfastamento.stream().forEach(p -> p.setFuncionario(funcionario));
				}
				
				
				controladorFuncionario.removerListaAfastamento((List<Long>) request.getSession().getAttribute(LISTA_REMOVIDA));

				funcionario.setListaAfastamento(listaAfastamento);
				funcionario.setDadosAuditoria(getDadosAuditoria(request));
				controladorFuncionario.verificarOrigatoriedadeMatricula(funcionario.getMatricula());
				controladorFuncionario.atualizar(funcionario);
				
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Funcionario.FUNCIONARIO_CAMPO_STRING);
				retorno = pesquisarFuncionarios(funcionario, null, String.valueOf(funcionario.isHabilitado()), model, request);
			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}
		
		return retorno;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return forward:pesquisarFuncionarios
	 * @throws Exception
	 */
	@RequestMapping("removerFuncionario")
	public String removerFuncionario(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws NegocioException {

		try {
			controladorFuncionario.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Funcionario.FUNCIONARIO_CAMPO_STRING);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_REMOVER_FUNCIONARIO, e));
		}

		return "forward:pesquisarFuncionarios";
	}

	/**
	 * 
	 * @param funcionario
	 * @param model
	 * @return exibirPesquisaFuncionarioPopup
	 * @throws Exception
	 */
	@RequestMapping("exibirPesquisaFuncionarioPopup")
	public String exibirPesquisaFuncionarioPopup(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, Model model)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltroPopup(filtro, funcionario);
		model.addAttribute("listaEmpresas", controladorEmpresa.consultarEmpresas(filtro));

		return "exibirPesquisaFuncionarioPopup";
	}

	/**
	 * 
	 * @param funcionario
	 * @param model
	 * @param result
	 * @return exibirPesquisaFuncionarioPopup
	 * @throws NegocioException 
	 * @throws Exception
	 */
	@RequestMapping("pesquisarFuncionarioPopup")
	public String pesquisarFuncionarioPopup(@ModelAttribute("FuncionarioImpl") FuncionarioImpl funcionario, BindingResult result,
			Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltroPopup(filtro, funcionario);
		model.addAttribute("listaEmpresas", controladorEmpresa.consultarEmpresas(filtro));
		model.addAttribute("funcionariosEncontrados", controladorFuncionario.consultarFuncionarios(filtro));

		return "exibirPesquisaFuncionarioPopup";
	}

	/**
	 * 
	 * @param model
	 * @param funcionario
	 * @throws GGASException
	 * @throws IOException 
	 */
	private void carregarCampos(Model model, FuncionarioImpl funcionario, MultipartFile fotoFuncionario) throws GGASException, IOException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);
		model.addAttribute(EMPRESAS, controladorEmpresa.consultarEmpresas(filtro));

		Collection<UnidadeOrganizacional> listaUnidadeOrganizacional = new ArrayList<UnidadeOrganizacional>();

		if (funcionario.getEmpresa() != null && funcionario.getEmpresa().getChavePrimaria() > 0) {
			filtro.put(ID_EMPRESA, funcionario.getEmpresa().getChavePrimaria());
			listaUnidadeOrganizacional = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);
		}
		
		if (fotoFuncionario != null && controladorEmpresa.validarImagemDoArquivo(fotoFuncionario, funcionario.getExtensoesArquivoFotoFuncionario(),
				Funcionario.TAMANHO_MAXIMO_ARQUIVO_FOTO_FUNCIONARIO, Empresa.ERRO_NEGOCIO_TIPO_ARQUIVO,
				Empresa.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO, Boolean.FALSE)) {
			funcionario.setFotoFuncionario(fotoFuncionario.getBytes());
		}
		
		
		model.addAttribute(LISTA_UNIDADE_ORGANIZACIONAL, listaUnidadeOrganizacional);
	}

	/**
	 * 
	 * @param filtro
	 * @param funcionario
	 * @throws FormatoInvalidoException
	 */
	private void prepararFiltroPopup(Map<String, Object> filtro, FuncionarioImpl funcionario) {

		if (funcionario.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, funcionario.getChavePrimaria());
		}

		if (funcionario.getMatricula() != null) {
			filtro.put(MATRICULA, funcionario.getMatricula());
		}

		if (funcionario.getNome() != null) {
			filtro.put(NOME, funcionario.getNome());
		}

		if (funcionario.getEmpresa() != null && funcionario.getEmpresa().getChavePrimaria() > 0) {
			filtro.put(ID_EMPRESA, funcionario.getEmpresa().getChavePrimaria());
		}

		filtro.put(HABILITADO, Boolean.TRUE);
	}

	/**
	 * 
	 * @param filtro
	 * @param funcionario
	 * @param habilitado
	 * @throws FormatoInvalidoException
	 */
	private void prepararFiltro(Map<String, Object> filtro, FuncionarioImpl funcionario, String habilitado) {

		if (funcionario.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, funcionario.getChavePrimaria());
		}

		if (funcionario.getMatricula() != null) {
			filtro.put(MATRICULA, funcionario.getMatricula());
		}

		if (funcionario.getNome() != null) {
			filtro.put(NOME, funcionario.getNome());
		}

		if (funcionario.getDescricaoCargo() != null) {
			filtro.put(DESCRICAO_CARGO, funcionario.getDescricaoCargo());
		}

		if (funcionario.getEmpresa() != null && funcionario.getEmpresa().getChavePrimaria() > 0) {
			filtro.put(ID_EMPRESA, funcionario.getEmpresa().getChavePrimaria());
		}

		if (funcionario.getUnidadeOrganizacional() != null && funcionario.getUnidadeOrganizacional().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_ORGANIZACIONAL, funcionario.getUnidadeOrganizacional().getChavePrimaria());
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}
	}
	
	public String formatosAceitos() {

		StringBuilder formatosAceitosSb = new StringBuilder();

		for (int i = 0; i < Funcionario.EXTENSOES_ARQUIVO_FOTO_FUNCIONARIO.length; i++) {

			if (i == Funcionario.EXTENSOES_ARQUIVO_FOTO_FUNCIONARIO.length - 1) {
				formatosAceitosSb.append(" e ");
			} else if (i != 0) {
				formatosAceitosSb.append(", ");
			}
			formatosAceitosSb.append(Funcionario.EXTENSOES_ARQUIVO_FOTO_FUNCIONARIO[i]);
		}

		return formatosAceitosSb.toString();
	}
	
	
	/**
	 * Método responsável por exibir na tela a foto do funcionario.
	 *
	 * @param mapping the mapping
	 * @param form O formulário
	 * @param request O objeto request
	 * @param response O objeto response
	 * @param chavePrimaria
	 * @return ActionForward O retorno da ação
	 * @throws NegocioException 
	 * @throws IOException 
	 * @throws Exception Caso ocorra algum erro
	 */

	@RequestMapping(value = "exibirFotoFuncionario/{chavePrimaria}")
	public void exibirFotoFuncionario(@PathVariable(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response) throws NegocioException, IOException  {

		Funcionario funcionario = (Funcionario) controladorFuncionario.obter(chavePrimaria);

		byte[] fotoFuncionario = funcionario.getFotoFuncionario();

		if (fotoFuncionario != null) {

			try {
				ByteArrayInputStream in = new ByteArrayInputStream(fotoFuncionario);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				response.reset();
				response.setContentType("image/png");

				while ((index = in.read(bytearray)) != -1) {
					response.getOutputStream().write(bytearray, 0, index);
				}
				response.flushBuffer();
				in.close();

			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			} finally {
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
		}
	}
	
	

	/**
	 * Carregar adicionar afastamento funcionario
	 * 
	 * @param motivoAfastamento
	 * @param dataInicio
	 * @param dataFim
	 * @param observacao
	 * @param indexList
	 * @param sessao
	 * @param request
	 * @return
	 * @throws NegocioException
	 */
	@RequestMapping("carregarAdicionarAfastamentoFuncionario")
	public ModelAndView carregarAdicionarAfastamentoFuncionario(
			@RequestParam("motivoAfastamento") EntidadeConteudo motivoAfastamento,
			@RequestParam(value = "dataInicio") String dataInicio,
			@RequestParam(value = "dataFim", required = false) String dataFim,
			@RequestParam(value = "observacao", required = false) String observacao,
			@RequestParam("indexList") Integer indexList, HttpSession sessao, HttpServletRequest request) throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_GRID_AFASTAMENTO);

		FuncionarioAfastamento funcionarioAfastamento = (FuncionarioAfastamento) controladorFuncionario
				.criarFuncionarioAfastamento();
		
		funcionarioAfastamento.setMotivoAfastamento(motivoAfastamento);
		funcionarioAfastamento.setDataInicioAfastamento(DataUtil.converterParaData(dataInicio));
		funcionarioAfastamento.setDataFimAfastamento(!StringUtils.isBlank(dataFim) ? DataUtil.converterParaData(dataFim) : null);
		funcionarioAfastamento.setObservacao(observacao);
		funcionarioAfastamento.setHabilitado(true);
		funcionarioAfastamento.setVersao(1);
		funcionarioAfastamento.setUltimaAlteracao(new Date());
		
		Collection<FuncionarioAfastamento> listaAtual = (Collection<FuncionarioAfastamento>) sessao
				.getAttribute(LISTA_AFASTAMENTO);
		if (indexList != null) {
			List<FuncionarioAfastamento> lista = new ArrayList<>(listaAtual);

			lista.get(indexList).setMotivoAfastamento(motivoAfastamento);
			lista.get(indexList).setDataInicioAfastamento(DataUtil.converterParaData(dataInicio));
			lista.get(indexList).setDataFimAfastamento(!StringUtils.isBlank(dataFim) ? DataUtil.converterParaData(dataFim) : null);
			lista.get(indexList).setObservacao(observacao);
			listaAtual.clear();
			listaAtual.addAll(lista);
		} else {
			if (listaAtual == null || listaAtual.isEmpty()) {
				listaAtual = new HashSet<>();
			}
			listaAtual.add(funcionarioAfastamento);
		}

		if ( listaAtual != null) {
			sessao.setAttribute(LISTA_AFASTAMENTO, listaAtual);
			model.addObject(LISTA_AFASTAMENTO, listaAtual);
		}

		return model;

	}
	
	
	/**
	 * Remover afastamento.
	 * 
	 * @param indexAfastamento
	 *            the index afastamento
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerAfastamento")
	public ModelAndView removerAfastamento(@RequestParam("indexAfastamento") int indexAfastamento, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_AFASTAMENTO);
		
		List<Long> removida = (List<Long>) sessao.getAttribute(LISTA_REMOVIDA);
		if(removida == null) {
			removida = new ArrayList<>();
		}
		
		Collection<FuncionarioAfastamento> lista = (Collection<FuncionarioAfastamento>) sessao.getAttribute(LISTA_AFASTAMENTO);
		if(lista != null && !lista.isEmpty()) {
			List<FuncionarioAfastamento> lista1 = new ArrayList<>(lista);
			FuncionarioAfastamento funcionarioAfastamento = lista1.get(indexAfastamento);
			
			lista.remove(funcionarioAfastamento);
			
			if(funcionarioAfastamento.getChavePrimaria() > 0) {
				removida.add(funcionarioAfastamento.getChavePrimaria());
			}
				
		}
		sessao.setAttribute(LISTA_AFASTAMENTO, lista);
		sessao.setAttribute(LISTA_REMOVIDA, removida);

		return model;
	}
}
