package br.com.ggas.web.cadastro.localidade;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.impl.MicrorregiaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas a entidade Microrregião
 *
 */
@Controller
public class MicrorregiaoAction extends GenericAction {

	private static final String MICRORREGIAO = "microrregiao";
	private static final String LISTA_REGIAO = "listaRegiao";
	private static final String DESCRICAO = "descricao";
	private static final String HABILITADO = "habilitado";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String ID_REGIAO = "idRegiao";
	private static final Class<MicrorregiaoImpl> CLASSE = MicrorregiaoImpl.class;

	@Autowired
	private ControladorGeografico controladorGeografico;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela exibição da tela de pesquisa de microrregião
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaMicrorregiao")
	public String exibirPesquisaMicrorregiao(Model model) throws NegocioException {

		model.addAttribute(LISTA_REGIAO, controladorGeografico.listarRegioes());
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaMicrorregiao";
	}

	/**
	 * Método responsável pela consulta de uma microrregião
	 * 
	 * @param microrregiao
	 *            - {@link MicrorregiaoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarMicrorregiao")
	public String pesquisarMicrorregiao(MicrorregiaoImpl microrregiao, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(MICRORREGIAO, microrregiao);
		Map<String, Object> filtro = montarFiltro(microrregiao, habilitado);
		model.addAttribute(LISTA_REGIAO, controladorGeografico.listarRegioes());
		model.addAttribute("listaMicrorregiao",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, MicrorregiaoImpl.class.getName()));

		return "exibirPesquisaMicrorregiao";
	}

	/**
	 * Método responsável pela exibição da tela de inclusão de microrregião
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirInclusaoMicrorregiao")
	public String exibirInclusaoMicrorregiao(Model model) throws NegocioException {

		model.addAttribute(LISTA_REGIAO, controladorGeografico.listarRegioes());
		return "exibirInclusaoMicrorregiao";
	}

	/**
	 * Método responsável pela inclusão de uma microrregião
	 * 
	 * @param microrregiao
	 *            - {@link MicrorregiaoImpl}
	 * @param result
	 *            - {@link BindinR}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirMicrorregiao")
	public String incluirMicrorregiao(MicrorregiaoImpl microrregiao, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		String view = "";

		model.addAttribute(MICRORREGIAO, microrregiao);
		Map<String, Object> erros = microrregiao.validarDados();
		if (!erros.isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(erros));
			view = exibirInclusaoMicrorregiao(model);
		} else {
			try {
				getDadosAuditoria(request);
				controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(microrregiao);
				controladorTabelaAuxiliar.pesquisarMicrorregiaoTabelaAuxiliar(microrregiao, MICRORREGIAO);
				controladorTabelaAuxiliar.inserir(microrregiao);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						MicrorregiaoImpl.MICRORREGIAO_ENTIDADE);
				view = pesquisarMicrorregiao(microrregiao, result, true, model);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				view = exibirInclusaoMicrorregiao(model);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração de microrregião
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirAlteracaoMicrorregiao")
	public String exibirAlteracaoMicrorregiao(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		if (!isPostBack(request)) {
			try {
				MicrorregiaoImpl microrregiao = (MicrorregiaoImpl) controladorTabelaAuxiliar.obter(chavePrimaria,
						CLASSE);
				model.addAttribute(MICRORREGIAO, microrregiao);
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
			}
		}
		model.addAttribute(LISTA_REGIAO, controladorGeografico.listarRegioes());
		return "exibirAlteracaoMicrorregiao";
	}

	/**
	 * Método responsável pela alteração de uma microrregião
	 * 
	 * @param microrregiao
	 *            - {@link MicrorregiaoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("alterarMicrorregiao")
	public String alterarMicrorregiao(MicrorregiaoImpl microrregiao, BindingResult result, HttpServletRequest request,
			Model model) throws NegocioException {
		String view = "";

		
		model.addAttribute(MICRORREGIAO, microrregiao);
		Map<String, Object> erros = microrregiao.validarDados();
		if (!erros.isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(erros));
			view = exibirAlteracaoMicrorregiao(null, request, model);
		} else {
			try {
				getDadosAuditoria(request);
				controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(microrregiao);
				controladorTabelaAuxiliar.pesquisarMicrorregiaoTabelaAuxiliar(microrregiao, MICRORREGIAO);
				controladorTabelaAuxiliar.atualizar(microrregiao);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						MicrorregiaoImpl.MICRORREGIAO_ENTIDADE);
				view = pesquisarMicrorregiao(microrregiao, result, microrregiao.isHabilitado(), model);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				view = exibirAlteracaoMicrorregiao(null, request, model);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela relacionada ao detalhamento de uma
	 * microrregião
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirDetalharMicrorregiao")
	public String exibirDetalharMicrorregiao(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {
		String view = "exibirDetalharMicrorregiao";

		try {
			MicrorregiaoImpl microrregiao = (MicrorregiaoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(MICRORREGIAO, microrregiao);
		} catch (GGASException e) {
			view = exibirPesquisaMicrorregiao(model);
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável pela remoção de uma microrregião
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerMicrorregiao")
	public String removerMicrorregiao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					MicrorregiaoImpl.MICRORREGIAO_ENTIDADE);
			return pesquisarMicrorregiao(null, null, Boolean.TRUE, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarMicrorregiao";
	}

	/**
	 * Método responsável por montar o filtro de pesquisa para consulta de
	 * microrregião
	 * 
	 * @param microrregiao
	 *            - {@link MicrorregiaoImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return String - {@link String}
	 */
	private Map<String, Object> montarFiltro(MicrorregiaoImpl microrregiao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (microrregiao != null) {
			if (!microrregiao.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, microrregiao.getDescricao());
			}
			if (microrregiao.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, microrregiao.getChavePrimaria());
			}
			if (microrregiao.getRegiao() != null && microrregiao.getRegiao().getChavePrimaria() > 0) {
				filtro.put(ID_REGIAO, microrregiao.getRegiao().getChavePrimaria());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
