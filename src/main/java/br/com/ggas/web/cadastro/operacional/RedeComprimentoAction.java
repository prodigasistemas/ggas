package br.com.ggas.web.cadastro.operacional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.RedeComprimento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;

@Controller
public class RedeComprimentoAction extends GenericAction{

	@Autowired
	private ControladorRede controladorRede;
	
	
	@RequestMapping("exibirIncluirRedeComprimento")
	public String exibirIncluirRedeComprimento(Model model) throws GGASException {

		return "exibirIncluirRedeComprimento";
	}
	
	
	@RequestMapping("gerarRedeComprimento")
	public String gerarRedeComprimento(Model model, @RequestParam(value = "ano", required = false) String ano) throws GGASException {

		Collection<RedeComprimento> listaComprimentoRede = controladorRede.consultarRedeComprimentoPorAno(ano);
		
		this.conferirTodosMesesAno(listaComprimentoRede, ano);
		
		model.addAttribute("listaComprimentoRede", listaComprimentoRede);
		model.addAttribute("ano", ano);
		
		return "exibirIncluirRedeComprimento";
	}
	
	private void conferirTodosMesesAno(Collection<RedeComprimento> listaComprimentoRede, String ano) throws NegocioException {
	    Set<Integer> referenciasExistentes = listaComprimentoRede.stream()
	        .map(RedeComprimento::getReferencia)
	        .collect(Collectors.toSet());

	    for (int i = 1; i <= 12; i++) {
	        String referenciaStr = ano + String.format("%02d", i);
	        Integer referencia = Integer.valueOf(referenciaStr);

	        if (!referenciasExistentes.contains(referencia)) {
	            RedeComprimento redeComprimento = Fachada.getInstancia().criarRedeComprimento();
	            redeComprimento.setReferencia(referencia);
	            listaComprimentoRede.add(redeComprimento);
	        }
	    }
	}

	@RequestMapping("salvarRedeComprimento")
	public String salvarRedeComprimento(Model model, @RequestParam("comprimento") List<String> comprimentos, @RequestParam(value = "anoGerar", required = false) String ano) throws GGASException {

		controladorRede.incluirRedeComprimento(comprimentos, ano);
		
		mensagemSucesso(model, "Comprimento de Rede Gerado com Sucesso");
		
		return gerarRedeComprimento(model, ano);

	}
	
}
