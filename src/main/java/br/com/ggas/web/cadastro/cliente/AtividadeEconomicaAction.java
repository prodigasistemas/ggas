/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.cliente;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das views referentes a Atividade Econômica.
 */
@Controller
public class AtividadeEconomicaAction extends GenericAction {

	private static final String CODIGO_CNAE = "codigoCnae";

	private static final String CODIGO = "codigo";

	private static final String ATIVIDADE_ECONOMICA_TITULO = "Atividade Econômica";

	private static final String DESCRICAO = "descricao";

	private static final String EXIBIR_PESQUISA_ATIVIDADE_ECONOMICA = "exibirPesquisaAtividadeEconomica";

	private static final String ATIVIDADE_ECONOMICA = "atividadeEconomica";

	private static final String EXIBIR_ATUALIZAR_ATIVIDADE_ECONOMICA = "exibirAtualizarAtividadeEconomica";

	private static final String EXIBIR_INSERIR_ATIVIDADE_ECONOMICA = "exibirInserirAtividadeEconomica";

	private static final Class<AtividadeEconomicaImpl> CLASSE = AtividadeEconomicaImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_ATIVIDADE_ECONOMICA = "listaAtividadeEconomica";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	/**
	 * Método responsável por exibir a tela de pesquisa de Atividade Econômica
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISA_ATIVIDADE_ECONOMICA)
	public String exibirPesquisaAtividadeEconomica(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_ATIVIDADE_ECONOMICA;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Atividade Econômica.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarAtividadeEconomica")
	public String pesquisarAtividadeEconomica(AtividadeEconomicaImpl atividadeEconomica, BindingResult bindingResult,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(atividadeEconomica, habilitado);

		Collection<TabelaAuxiliar> listaAtividadeEconomica = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_ATIVIDADE_ECONOMICA, listaAtividadeEconomica);
		model.addAttribute(ATIVIDADE_ECONOMICA, atividadeEconomica);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_ATIVIDADE_ECONOMICA;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Atividade Econômica.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoAtividadeEconomica")
	public String exibirDetalhamentoAtividadeEconomica(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			AtividadeEconomica atividadeEconomica = (AtividadeEconomica) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(ATIVIDADE_ECONOMICA, atividadeEconomica);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaAtividadeEconomica";
		}

		return "exibirDetalhamentoAtividadeEconomica";
	}

	/**
	 * Método responsável por exibir a tela de inserir Atividade Econômica.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_ATIVIDADE_ECONOMICA)
	public String exibirInserirAtividadeEconomica() {

		return EXIBIR_INSERIR_ATIVIDADE_ECONOMICA;
	}

	/**
	 * Método responsável por inserir uma Atividade Econômica.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirAtividadeEconomica")
	private String inserirAtividadeEconomica(AtividadeEconomicaImpl atividadeEconomica, BindingResult bindingResult, Model model,
					HttpServletRequest request) throws GGASException {

		String view = EXIBIR_INSERIR_ATIVIDADE_ECONOMICA;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(atividadeEconomica);
			controladorTabelaAuxiliar.pesquisaCodigoTabelaAuxiliar(atividadeEconomica, ATIVIDADE_ECONOMICA_TITULO);
			controladorTabelaAuxiliar.pesquisaCodigoCnaeTabelaAuxiliar(atividadeEconomica, ATIVIDADE_ECONOMICA_TITULO);
			controladorTabelaAuxiliar.verificarCodigoOriginalCodigoCNAE(String.valueOf(atividadeEconomica.getCodigoOriginal()),
							String.valueOf(atividadeEconomica.getCodigo()));
			controladorTabelaAuxiliar.inserir(atividadeEconomica);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, AtividadeEconomica.ATIVIDADE_ECONOMICA);
			view = pesquisarAtividadeEconomica(atividadeEconomica, bindingResult, atividadeEconomica.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(ATIVIDADE_ECONOMICA, atividadeEconomica);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Atividade Econômica.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param atividadeEconomicaParam - {@link AtividadeEconomicaImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_ATIVIDADE_ECONOMICA)
	public String exibirAtualizarAtividadeEconomica(AtividadeEconomicaImpl atividadeEconomicaParam, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		AtividadeEconomica atividadeEconomica = atividadeEconomicaParam;

		try {
			atividadeEconomica = (AtividadeEconomica) controladorTabelaAuxiliar.obter(atividadeEconomica.getChavePrimaria(), CLASSE);
			model.addAttribute(ATIVIDADE_ECONOMICA, atividadeEconomica);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaAtividadeEconomica";
		}

		return EXIBIR_ATUALIZAR_ATIVIDADE_ECONOMICA;
	}

	/**
	 * Método responsável por atualizar uma Atividade Econômica.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarAtividadeEconomica")
	private String atualizarAtividadeEconomica(AtividadeEconomicaImpl atividadeEconomica, BindingResult result, HttpServletRequest request,
					Model model) throws GGASException {

		String view = EXIBIR_ATUALIZAR_ATIVIDADE_ECONOMICA;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(atividadeEconomica);
			controladorTabelaAuxiliar.pesquisaCodigoTabelaAuxiliar(atividadeEconomica, ATIVIDADE_ECONOMICA_TITULO);
			controladorTabelaAuxiliar.pesquisaCodigoCnaeTabelaAuxiliar(atividadeEconomica, ATIVIDADE_ECONOMICA_TITULO);
			controladorTabelaAuxiliar.verificarCodigoOriginalCodigoCNAE(String.valueOf(atividadeEconomica.getCodigoOriginal()),
							String.valueOf(atividadeEconomica.getCodigo()));
			controladorTabelaAuxiliar.atualizar(atividadeEconomica, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, AtividadeEconomica.ATIVIDADE_ECONOMICA);
			view = pesquisarAtividadeEconomica(atividadeEconomica, result, atividadeEconomica.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(ATIVIDADE_ECONOMICA, atividadeEconomica);
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por remover uma Atividade Econômica.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerAtividadeEconomica")
	public String removerAtividadeEconomica(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
					Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, AtividadeEconomica.ATIVIDADE_ECONOMICA);
			return pesquisarAtividadeEconomica(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarAtividadeEconomica";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Atividade Econômica.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(AtividadeEconomica atividadeEconomica, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (atividadeEconomica != null) {

			if (atividadeEconomica.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, atividadeEconomica.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(atividadeEconomica.getDescricao())) {
				filtro.put(DESCRICAO, atividadeEconomica.getDescricao());
			}

			if (atividadeEconomica.getCodigoOriginal() != null && atividadeEconomica.getCodigoOriginal() > 0) {
				filtro.put(CODIGO, atividadeEconomica.getCodigoOriginal());
			}

			if (StringUtils.isNotEmpty(atividadeEconomica.getCodigo())) {
				filtro.put(CODIGO_CNAE, atividadeEconomica.getCodigo().toUpperCase());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
	
	/**
	 * Método responsável por preparar o filtro para pesquisa no popup.
	 * 
	 * @param atividadeEconomica - {@link AtividadeEconomicaImpl}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltroPopup(AtividadeEconomica atividadeEconomica) {

		Map<String, Object> filtro = new HashMap<>();

		if (StringUtils.isNotEmpty(atividadeEconomica.getDescricao())) {
			filtro.put(DESCRICAO, atividadeEconomica.getDescricao());
		}

		if (atividadeEconomica.getCodigoOriginal() != null && atividadeEconomica.getCodigoOriginal() > 0) {
			filtro.put("codigoCNAE", atividadeEconomica.getCodigoOriginal());
		}

		if (StringUtils.isNotEmpty(atividadeEconomica.getCodigo())) {
			filtro.put(CODIGO, atividadeEconomica.getCodigo().toUpperCase());
		}

		return filtro;
	}

	/**
	 * Método responsável por exibir a tela do
	 * popup de pesquisa de atividade
	 * econômica.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaAtividadeEconomicaPopup")
	public String exibirPesquisaAtividadeEconomicaPopup(HttpServletRequest request) throws GGASException {

		return "exibirPesquisaAtividadeEconomicaPopup";
	}

	/**
	 * Método responsável por realizar a consulta
	 * do popup de pesquisa de
	 * atividade econômica.
	 * 
	 * @param atividadeEconomica
	 * @param bindingResult {@link BindingResult}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarAtividadeEconomicaPopup")
	public String pesquisarAtividadeEconomicaPopup(AtividadeEconomicaImpl atividadeEconomica, BindingResult bindingResult, Model model)
					throws GGASException {

		Map<String, Object> filtro = prepararFiltroPopup(atividadeEconomica);

		Collection<AtividadeEconomica> listaAtividadesEconomicas = controladorCliente.consultarAtividadeEconomica(filtro);

		model.addAttribute("listaAtividadesEconomicas", listaAtividadesEconomicas);
		model.addAttribute("atividadeEconomica", atividadeEconomica);
		return "forward:exibirPesquisaAtividadeEconomicaPopup";
	}
}
