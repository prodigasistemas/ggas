
package br.com.ggas.web.cadastro.operacional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.impl.RedeMaterialImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes ao manter Material da Rede.
 */
@Controller
public class RedeMaterialAction extends GenericAction {

	private static final String EXIBIR_PESQUISAR_REDE_MATERIAL = "exibirPesquisarRedeMaterial";

	private static final String REDE_MATERIAL = "redeMaterial";

	private static final String EXIBIR_ATUALIZAR_REDE_MATERIAL = "exibirAtualizarRedeMaterial";

	private static final String EXIBIR_INSERIR_REDE_MATERIAL = "exibirInserirRedeMaterial";

	private static final Class<RedeMaterialImpl> CLASSE = RedeMaterialImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.operacional.impl.RedeMaterialImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_REDE_MATERIAL = "listaRedeMaterial";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de Material da Rede
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISAR_REDE_MATERIAL)
	public String exibirPesquisaMaterialRede(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISAR_REDE_MATERIAL;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Material da Rede.
	 * 
	 * @param redeMaterial - {@link RedeMaterialImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarRedeMaterial")
	public String pesquisarMaterialRede(RedeMaterialImpl redeMaterial, BindingResult bindingResult,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(redeMaterial, habilitado);

		Collection<TabelaAuxiliar> listaMaterialRede = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_REDE_MATERIAL, listaMaterialRede);
		model.addAttribute(REDE_MATERIAL, redeMaterial);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISAR_REDE_MATERIAL;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Material da Rede.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoRedeMaterial")
	public String exibirDetalhamentoMaterialRede(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			RedeMaterial redeMaterial = (RedeMaterial) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(REDE_MATERIAL, redeMaterial);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisarRedeMaterial";
		}

		return "exibirDetalhamentoRedeMaterial";
	}

	/**
	 * Método responsável por exibir a tela de inserir Material da Rede.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_REDE_MATERIAL)
	public String exibirInserirMaterialRede() {

		return EXIBIR_INSERIR_REDE_MATERIAL;
	}

	/**
	 * Método responsável por inserir um Material da Rede.
	 * 
	 * @param redeMaterial - {@link RedeMaterialImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirRedeMaterial")
	private String inserirMaterialRede(RedeMaterialImpl redeMaterial, BindingResult bindingResult, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(redeMaterial);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(redeMaterial, "Material da Rede");
			controladorTabelaAuxiliar.inserir(redeMaterial);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, RedeMaterial.REDE_MATERIAL);
			retorno = pesquisarMaterialRede(redeMaterial, bindingResult, redeMaterial.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(REDE_MATERIAL, redeMaterial);
			retorno = EXIBIR_INSERIR_REDE_MATERIAL;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Material da Rede.
	 * 
	 * @param redeMaterial - {@link RedeMaterialImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param redeMaterialParam - {@link RedeMaterialImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_REDE_MATERIAL)
	public String exibirAtualizarMaterialRede(RedeMaterialImpl redeMaterialParam, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		RedeMaterial redeMaterial = redeMaterialParam;

		try {
			redeMaterial = (RedeMaterial) controladorTabelaAuxiliar.obter(redeMaterial.getChavePrimaria(), CLASSE);
			model.addAttribute(REDE_MATERIAL, redeMaterial);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisarRedeMaterial";
		}

		return EXIBIR_ATUALIZAR_REDE_MATERIAL;
	}

	/**
	 * Método responsável por atualizar um Material da Rede.
	 * 
	 * @param redeMaterial - {@link RedeMaterialImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarRedeMaterial")
	private String atualizarMaterialRede(RedeMaterialImpl redeMaterial, BindingResult result, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(redeMaterial);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(redeMaterial, "Material da Rede");
			controladorTabelaAuxiliar.atualizar(redeMaterial, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, RedeMaterial.REDE_MATERIAL);
			retorno = pesquisarMaterialRede(redeMaterial, result, redeMaterial.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(REDE_MATERIAL, redeMaterial);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_REDE_MATERIAL;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover um Material da Rede.
	 * 
	 * @param redeMaterial {@link RedeMaterialImpl}
	 * @param result {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerRedeMaterial")
	public String removerMaterialRede(RedeMaterialImpl redeMaterial, BindingResult result,
					@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, RedeMaterial.REDE_MATERIAL);
			return pesquisarMaterialRede(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarRedeMaterial";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Material da Rede.
	 * 
	 * @param redeMaterial - {@link RedeMaterialImpl}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(RedeMaterialImpl redeMaterial, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (redeMaterial != null) {

			if (redeMaterial.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, redeMaterial.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(redeMaterial.getDescricao())) {
				filtro.put(DESCRICAO, redeMaterial.getDescricao());
			}

		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
