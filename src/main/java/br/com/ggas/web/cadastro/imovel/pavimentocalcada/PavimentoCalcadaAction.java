/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.web.cadastro.imovel.pavimentocalcada;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.impl.PavimentoCalcadaImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas a Entidade Pavimento da Calçada
 *
 */
@Controller
public class PavimentoCalcadaAction extends GenericAction {

	private static final String PAVIMENTO_DA_CALCADA = "Pavimento da Calçada";

	private static final String PAVIMENTO_CALCADA = "pavimentoCalcada";

	private static final String HABILITADO = "habilitado";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela exibição da tela de pesquisa da entidade Pavimento da Calçada
	 * 
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@RequestMapping("exibirPesquisaPavimentoCalcada")
	public String exibirPesquisaPavimentoCalcada(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaPavimentoCalcada";
	}

	/**
	 * Método responsável pela pesquisa da entidde Pavimento de Calçada
	 * 
	 * @param pavimento - {@link PavimentoCalcadaImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 * 
	 */
	@RequestMapping("pesquisarPavimentoCalcada")
	public String pesquisarPavimentoCalcada(PavimentoCalcadaImpl pavimento, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(pavimento, habilitado);
		model.addAttribute(PAVIMENTO_CALCADA, pavimento);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("listaPavimentoCalcada",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, PavimentoCalcadaImpl.class.getName()));

		return "exibirPesquisaPavimentoCalcada";
	}

	/**
	 * Método responsável pela exibição da tela de inclusão da entidade Pavimento de Calçada
	 * 
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@RequestMapping("exibirIncluirPavimentoCalcada")
	public String exibirIncluirPavimentoCalcada(Model model) {

		return "exibirIncluirPavimentoCalcada";
	}

	/**
	 * Método responsável pela inclusão da entidade Pavimento de Calçada
	 * 
	 * @param pavimento - {@link PavimentoCalcadaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@RequestMapping("incluirPavimentoCalcada")
	public String incluirPavimentoCalcada(PavimentoCalcadaImpl pavimento, BindingResult result, HttpServletRequest request, Model model) {

		String view = "exibirIncluirPavimentoCalcada";
		model.addAttribute(PAVIMENTO_CALCADA, pavimento);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(pavimento.getDescricao(),  PavimentoCalcadaImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(pavimento.getDescricaoAbreviada(), PavimentoCalcadaImpl.class.getName());
			if("".equals(pavimento.getDescricaoAbreviada())) {
				pavimento.setDescricaoAbreviada(null);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(pavimento);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(pavimento, PAVIMENTO_DA_CALCADA);
			controladorTabelaAuxiliar.inserir(pavimento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, PAVIMENTO_DA_CALCADA);
			view = pesquisarPavimentoCalcada(pavimento, result, pavimento.isHabilitado(), model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela para alteração da entidade Pavimento de Calçada.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@RequestMapping("exibirAlterarPavimentoCalcada")
	public String exibirAlterarPavimentoCalcada(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model) {

		String view = "forward:exibirPesquisaPavimentoCalcada";
		try {
			PavimentoCalcadaImpl pavimento = (PavimentoCalcadaImpl) controladorTabelaAuxiliar.obter(chavePrimaria, PavimentoCalcadaImpl.class);
			model.addAttribute(PAVIMENTO_CALCADA, pavimento);
			view = "exibirAlterarPavimentoCalcada";
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por realizar a alteração da entidade Pavimento de Calçada.
	 * 
	 * @param pavimento - {@link PavimentoCalcadaImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * 
	 */
	@RequestMapping("alterarPavimentoCalcada")
	public String alterarPavimentoCalcada(PavimentoCalcadaImpl pavimento, BindingResult result, HttpServletRequest request, Model model) {

		String view = "exibirAlterarPavimentoCalcada";
		model.addAttribute(PAVIMENTO_CALCADA, pavimento);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(pavimento.getDescricao(),  PavimentoCalcadaImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(pavimento.getDescricaoAbreviada(), PavimentoCalcadaImpl.class.getName());
			if("".equals(pavimento.getDescricaoAbreviada())) {
				pavimento.setDescricaoAbreviada(null);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(pavimento);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(pavimento, PAVIMENTO_DA_CALCADA);
			controladorTabelaAuxiliar.atualizar(pavimento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, PAVIMENTO_DA_CALCADA);
			view = pesquisarPavimentoCalcada(pavimento, result, pavimento.isHabilitado(), model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por detalhar a entidade Pavimento de Calçada
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalharPavimentoCalcada")
	public String exibirDetalharPavimentoCalcada(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model) {

		String view = "forward:exibirPesquisaPavimentoCalcada";
		try {
			PavimentoCalcadaImpl pavimento = (PavimentoCalcadaImpl) controladorTabelaAuxiliar.obter(chavePrimaria, PavimentoCalcadaImpl.class);
			model.addAttribute(PAVIMENTO_CALCADA, pavimento);
			view = "exibirDetalharPavimentoCalcada";
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por realizar a remoção da entidade Pavimento de Calçada
	 * 
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("removerPavimentoCalcada")
	public String removerPavimentoCalcada(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) {

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, PavimentoCalcadaImpl.class, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, PAVIMENTO_DA_CALCADA);
			return pesquisarPavimentoCalcada(null, null, Boolean.TRUE, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarPavimentoCalcada";
	}

	/**
	 * Método responsável por montar o filtro de pesquisa.
	 * 
	 * @param pavimento - {@link PavimentoCalcadaImpl}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> popularFiltro(PavimentoCalcadaImpl pavimento, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (pavimento != null) {

			if (pavimento.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", pavimento.getChavePrimaria());
			}
			if (!StringUtils.isEmpty(pavimento.getDescricao())) {
				filtro.put(DESCRICAO, pavimento.getDescricao());
			}

			if (!StringUtils.isEmpty(pavimento.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, pavimento.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
