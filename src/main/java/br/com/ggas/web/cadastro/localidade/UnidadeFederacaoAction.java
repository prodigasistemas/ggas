package br.com.ggas.web.cadastro.localidade;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas a Unidade Federativa
 *
 */
@Controller
public class UnidadeFederacaoAction extends GenericAction {

	private static final String SIGLA = "sigla";
	private static final String ID_PAIS = "idPais";
	private static final String DESCRICAO = "descricao";
	private static final String HABILITADO = "habilitado";
	private static final String CODIGO_IBGE = "codigoIBGE";
	private static final String LISTA_PAISES = "listaPaises";
	private static final String UNIDADE_FEDERACAO = "unidadeFederacao";

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	@Qualifier("controladorGeografico")
	private ControladorGeografico controladorGeografico;

	/**
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaUnidadeFederacao")
	public String exibirPesquisaUnidadeFederacao(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());

		return "exibirPesquisaUnidadeFederacao";
	}

	/**
	 * 
	 * @param unidadeFederacao
	 *            - {@link UnidadeFederacaoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarUnidadeFederacao")
	public String pesquisarUnidadeFederacao(UnidadeFederacaoImpl unidadeFederacao, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = montarFiltro(unidadeFederacao, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
		model.addAttribute("listaUnidadeFederacao",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, UnidadeFederacaoImpl.class.getName()));
		model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());
		return "exibirPesquisaUnidadeFederacao";
	}

	/**
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirIncluirUnidadeFederacao")
	public String exibirIncluirUnidadeFederacao(Model model) throws NegocioException {

		model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());
		return "exibirIncluirUnidadeFederacao";
	}

	/**
	 * 
	 * @param unidadeFederacao
	 *            - {@link UnidadeFederacaoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirUnidadeFederacao")
	public String incluirUnidadeFederacao(UnidadeFederacaoImpl unidadeFederacao, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeFederacao);
			controladorTabelaAuxiliar.pesquisarUnidadeFederacaoTabelaAuxiliar(unidadeFederacao,
					UnidadeFederacaoImpl.UNIDADE_FEDERACAO_ENTIDADE);
			controladorTabelaAuxiliar.inserir(unidadeFederacao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					UnidadeFederacaoImpl.UNIDADE_FEDERACAO_ENTIDADE);
			view = pesquisarUnidadeFederacao(unidadeFederacao, result, unidadeFederacao.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirIncluirUnidadeFederacao(model);
		}
		return view;
	}

	/**
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirAlterarUnidadeFederacao")
	public String exibirAlterarUnidadeFederacao(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		String view = "exibirAlterarUnidadeFederacao";
		model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());
		if (!isPostBack(request)) {
			try {
				UnidadeFederacaoImpl unidadeFederacao = (UnidadeFederacaoImpl) controladorTabelaAuxiliar
						.obter(chavePrimaria, UnidadeFederacaoImpl.class, "pais");
				model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
				view = exibirPesquisaUnidadeFederacao(model);
			}
		}

		return view;
	}

	/**
	 * 
	 * @param unidadeFederacao
	 *            - {@link UnidadeFederacaoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarUnidadeFederacao")
	public String alterarUnidadeFederacao(UnidadeFederacaoImpl unidadeFederacao, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeFederacao);
			controladorTabelaAuxiliar.pesquisarUnidadeFederacaoTabelaAuxiliar(unidadeFederacao,
					UnidadeFederacaoImpl.UNIDADE_FEDERACAO_ENTIDADE);
			controladorTabelaAuxiliar.atualizar(unidadeFederacao, UnidadeFederacaoImpl.class);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					UnidadeFederacaoImpl.UNIDADE_FEDERACAO_ENTIDADE);
			view = pesquisarUnidadeFederacao(unidadeFederacao, result, unidadeFederacao.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());
			view = "exibirAlterarUnidadeFederacao";
		}

		return view;
	}

	/**
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirDetalharUnidadeFederacao")
	public String exibirDetalharUnidadeFederacao(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		String view = "exibirDetalharUnidadeFederacao";
		model.addAttribute(LISTA_PAISES, controladorGeografico.listarPaises());
		if (!isPostBack(request)) {
			try {
				UnidadeFederacaoImpl unidadeFederacao = (UnidadeFederacaoImpl) controladorTabelaAuxiliar
						.obter(chavePrimaria, UnidadeFederacaoImpl.class, "pais");
				model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
				view = exibirPesquisaUnidadeFederacao(model);
			}
		}

		return view;
	}

	/**
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerUnidadeFederacao")
	public String removerUnidadeFederacao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, UnidadeFederacaoImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					UnidadeFederacaoImpl.UNIDADE_FEDERACAO_ENTIDADE);
			return pesquisarUnidadeFederacao(null, null, Boolean.TRUE, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarUnidadeFederacao";
	}

	/**
	 * 
	 * @param unidadeFederacao
	 *            - {@link UnidadeFederacaoImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> montarFiltro(UnidadeFederacaoImpl unidadeFederacao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (unidadeFederacao != null) {

			if (!StringUtils.isEmpty(unidadeFederacao.getDescricao())) {
				filtro.put(DESCRICAO, unidadeFederacao.getDescricao());
			}

			if (unidadeFederacao.getPais() != null) {
				filtro.put(ID_PAIS, unidadeFederacao.getPais().getChavePrimaria());
			}

			if (!StringUtils.isEmpty(unidadeFederacao.getSigla())) {
				filtro.put(SIGLA, unidadeFederacao.getSigla());
			}

			if (unidadeFederacao.getUnidadeFederativaIBGE() != null
					&& unidadeFederacao.getUnidadeFederativaIBGE() > 0) {
				filtro.put(CODIGO_IBGE, unidadeFederacao.getUnidadeFederativaIBGE());
			}
		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
		return filtro;
	}
	
	private String setUnidadeFederacaoESalvaToken(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
			Model model, String view) throws NegocioException {
		if (!isPostBack(request)) {
			try {
				UnidadeFederacaoImpl unidadeFederacao = (UnidadeFederacaoImpl) controladorTabelaAuxiliar
						.obter(chavePrimaria, UnidadeFederacaoImpl.class, "pais");
				model.addAttribute(UNIDADE_FEDERACAO, unidadeFederacao);
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
				view = exibirPesquisaUnidadeFederacao(model);
			}
		}
		return view;
	}
}
