package br.com.ggas.web.cadastro.imovel.tipocilindro;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.imovel.impl.TipoBotijaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe responsável pelas telas relacionadas aos tipos de cilindro.
 */
@Controller
public class TipoCilindroAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_TIPO_CILINDRO = "exibirPesquisaTipoCilindro";
	private static final String TIPO_CILINDRO = "tipoCilindro";
	private static final String DESCRICAO = "descricao";
	private static final String HABILITADO = "habilitado";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela tela de pesquisa do tipo de cilindro
	 * 
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_TIPO_CILINDRO)
	public String exibirPesquisaTipoCilindro(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return TELA_EXIBIR_PESQUISA_TIPO_CILINDRO;
	}

	/**
	 * Método responsável pela pesquisa dos tipos de cilindro
	 * 
	 * @param tipoBotijao - {@link TipoBotijaoImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarTipoCilindro")
	public String pesquisarTipoCilindro(TipoBotijaoImpl tipoBotijao, BindingResult result, @RequestParam("habilitado") Boolean habilitado,
			Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(tipoBotijao, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(TIPO_CILINDRO, tipoBotijao);
		model.addAttribute("listaTipoCilindro", controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, TipoBotijaoImpl.class.getName()));

		return TELA_EXIBIR_PESQUISA_TIPO_CILINDRO;
	}

	/**
	 * Método responsável pela exibição da tela de inclusão dos tipos de cilindro
	 * 
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirInclusaoTipoCilindro")
	public String exibirInclusaoTipoCilindro(Model model) {

		return "exibirInclusaoTipoCilindro";
	}

	/**
	 * Método responsável pela inserção dos tipos de cilindro
	 * 
	 * @param tipoBotijao - {@link TipoBotijaoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("inserirTipoCilindro")
	public String inserirTipoCilindro(TipoBotijaoImpl tipoBotijao, BindingResult result, HttpServletRequest request, Model model) {
		String view = "exibirInclusaoTipoCilindro";

		model.addAttribute(TIPO_CILINDRO, tipoBotijao);

		try {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoBotijao.getDescricao(), TipoBotijaoImpl.class.getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoBotijao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoBotijao, TipoBotijao.ROTULO_TIPO_BOTIJAO);
			controladorTabelaAuxiliar.inserir(tipoBotijao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TipoBotijao.ROTULO_TIPO_BOTIJAO);
			view = pesquisarTipoCilindro(tipoBotijao, result, tipoBotijao.isHabilitado(), model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirInclusaoTipoCilindro(model);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração dos tipos de cilindro
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoTipoCilindro")
	public String exibirAlteracaoTipoCilindro(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {
		String view = "exibirAlteracaoTipoCilindro";
		try {
			TipoBotijaoImpl tipoBotijao = (TipoBotijaoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, TipoBotijaoImpl.class);
			model.addAttribute(TIPO_CILINDRO, tipoBotijao);
		} catch (GGASException e) {
			model.addAttribute(HABILITADO, true);
			mensagemErroParametrizado(model, request, e);
			view = "exibirPesquisaTipoCilindro";
		}

		return view;
	}

	/**
	 * Método responsável pela alteração dos tipos de cilindro
	 * 
	 * @param tipoBotijao - {@link TipoBotijaoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("alterarTipoCilindro")
	public String alterarTipoCilindro(TipoBotijaoImpl tipoBotijao, BindingResult result, HttpServletRequest request, Model model) {

		String view = "exibirAlteracaoTipoCilindro";

		model.addAttribute(TIPO_CILINDRO, tipoBotijao);

		try {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoBotijao.getDescricao(), TipoBotijaoImpl.class.getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoBotijao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoBotijao, TipoBotijaoImpl.ROTULO_TIPO_BOTIJAO);
			controladorTabelaAuxiliar.atualizar(tipoBotijao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TipoBotijaoImpl.ROTULO_TIPO_BOTIJAO);
			view = pesquisarTipoCilindro(tipoBotijao, result, tipoBotijao.isHabilitado(), model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de detalhamento dos tipos de cilindro
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalharTipoCilindro")
	public String exibirDetalharTipoCilindro(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model) {
		String view = "exibirDetalharTipoCilindro";

		try {
			model.addAttribute(TIPO_CILINDRO, controladorTabelaAuxiliar.obter(chavePrimaria, TipoBotijaoImpl.class));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirPesquisaTipoCilindro";
		}

		return view;
	}

	/**
	 * Método responsável pela remoção dos tipos de cilindro
	 * 
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param tipoBotijao - {@link TipoBotijaoImpl}
	 * @param result - {@link BindingResult}
	 * @return String - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerTipoCilindro")
	public String removerTipoCilindro(TipoBotijaoImpl tipoBotijao, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TipoBotijaoImpl.class, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, TipoBotijaoImpl.ROTULO_TIPO_BOTIJAO);
			return pesquisarTipoCilindro(null, null, true, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarTipoCilindro(tipoBotijao, result, true, model);
	}

	/**
	 * Método responsável por popular o filtro pra pesquisa dos tipos de cilindro
	 * 
	 * @param tipoBotijao - {@link TipoBotijaoImpl}
	 * @param habilitado - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> popularFiltro(TipoBotijaoImpl tipoBotijao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (tipoBotijao != null) {

			if (tipoBotijao.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", tipoBotijao.getChavePrimaria());
			}

			if (!StringUtils.isEmpty(tipoBotijao.getDescricao())) {
				filtro.put(DESCRICAO, tipoBotijao.getDescricao());
			}

			if (!StringUtils.isEmpty(tipoBotijao.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", tipoBotijao.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
