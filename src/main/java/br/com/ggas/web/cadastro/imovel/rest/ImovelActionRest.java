/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.imovel.rest;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Pair;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.ImovelDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaPontoConsumo;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Action de requisições rest do ponto de consumo
 * @author jose.victor@logiquesistemas.com.br
 */
@Controller
@RequestMapping("rest/imovel")
@SuppressWarnings("common-java:InsufficientCommentDensity")
public class ImovelActionRest extends GenericActionRest {

	@Autowired
	private ControladorImovel controladorImovel;

	/**
	 * Busca os pontos de consumo por sua descrição de forma paginada
	 * @param pesquisa O objeto contendo os dados de pesquisa do ponto de consumo
	 * @return retorna a lista de pontos de consumos
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "buscarPaginado", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarPontoConsumoPaginado(@RequestBody PesquisaPontoConsumo pesquisa) throws GGASException {
		DatatablesServerSideDTO<ImovelDTO> datatable;
		try {
			final Pair<List<ImovelDTO>, Long> resultados = controladorImovel.listarImovelComPontosDeConsumoFilho(pesquisa);
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), resultados.getSecond(), resultados.getFirst());
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
		} catch (Exception e) {
			final Logger LOG = Logger.getLogger(ImovelActionRest.class);
			LOG.error(e.getMessage(), e);
			datatable = new DatatablesServerSideDTO<>(pesquisa.getOffset(), 0L, Collections.emptyList());
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Consulta imóveis por seus atributos
	 * @param nome nome do imóvel
	 * @param complemento complemento do imóvel
	 * @param matricula matrícula do imóvel
	 * @param numeroImovel número do endereço do imóvel
	 * @param numeroCep cep do imóvel
	 * @param indicadorCondominio indica se o móvel é do tipo condomínio ou não
	 * @return retorna um json com a lista de imóveis
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/consultarImovel", produces = "application/json; charset=utf-8",
			method = { RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> consultarImovel(@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "complemento", required = false) String complemento,
			@RequestParam(value = "matricula", required = false) String matricula,
			@RequestParam(value = "numeroImovel", required = false) String numeroImovel,
			@RequestParam(value = "numeroCep", required = false) String numeroCep,
			@RequestParam(value = "indicadorCondominio", required = false) Boolean indicadorCondominio) throws GGASException {

		final Map<String, Object> filtros = criarFiltroImovel(nome, complemento, matricula, numeroImovel, numeroCep, indicadorCondominio);
		final Collection<Imovel> imoveis = controladorImovel.consultarImoveis(filtros);

		final String json = serializarJson(imoveis);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	private Map<String, Object> criarFiltroImovel(String nome, String complemento, String matricula, String numeroImovel, String numeroCep,
			Boolean indicadorCondominio) throws FormatoInvalidoException {

		Map<String, Object> filtro = new HashMap<>();

		if(!StringUtils.isEmpty(matricula)) {
			filtro.put(FiltroImovel.CHAVE_PRIMARIA.getChave(), Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matricula));
		}

		inserirCasoExista(FiltroImovel.CEP_IMOVEL.getChave(), numeroCep, filtro);
		inserirCasoExista(FiltroImovel.NOME.getChave(), nome, filtro);
		inserirCasoExista(FiltroImovel.COMPLEMENTO_IMOVEL.getChave(), complemento, filtro);
		inserirCasoExista(FiltroImovel.NUMERO_IMOVEL.getChave(), numeroImovel, filtro);

		if (BooleanUtils.isTrue(indicadorCondominio)) {
			filtro.put(FiltroImovel.INDICADOR_CONOMINIO.getChave(), indicadorCondominio);
		}

		return filtro;
	}

	private void inserirCasoExista(String chave, String valor, Map<String, Object> filtro) {
		if(!StringUtils.isEmpty(valor)) {
			filtro.put(chave, valor);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder().put(Imovel.class, new ImovelSerializador()).build();
	}

}
