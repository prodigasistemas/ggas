package br.com.ggas.web.cadastro.operacional;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.cadastro.operacional.impl.TroncoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author pedro 
 * Controlador responsável pelas telas relacionadas ao Tronco
 *
 */
@Controller
public class TroncoAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_TRONCO = "exibirPesquisaTronco";
	private static final String TRONCO = "tronco";
	private static final String DESCRICAO = "descricao";
	private static final String ID_CITY_GATE = "idCityGate";
	private static final String HABILITADO = "habilitado";
	private static final Class<TroncoImpl> CLASSE = TroncoImpl.class;
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.operacional.impl.TroncoImpl";

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa relacionada a entidade tronco
	 * 
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirPesquisaTronco")
	public String exibirPesquisaTronco(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		carregarCityGates(model);
		return TELA_EXIBIR_PESQUISA_TRONCO;
	}

	/**
	 * Método responável por realizar a consulta relacionada a entidade tronco.
	 * 
	 * @param tronco
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarTronco")
	public String pesquisarTronco(TroncoImpl tronco, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(tronco, habilitado);

		model.addAttribute(TRONCO, tronco);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("listaTronco", controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING));
		carregarCityGates(model);

		return TELA_EXIBIR_PESQUISA_TRONCO;
	}

	/**
	 * Método responsável por exibir a tela de inclusão relacionada a entidade tronco.
	 * 
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirInclusaoTronco")
	public String exibirInclusaoTronco(Model model) throws NegocioException {

		carregarCityGates(model);

		return "exibirInclusaoTronco";
	}

	/**
	 * Método responsável pela inclusão da entidade tronco.
	 * 
	 * @param tronco
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("incluirTronco")
	public String incluirTronco(TroncoImpl tronco, BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		model.addAttribute(TRONCO, tronco);
		Map<String, Object> erros = tronco.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
			view = "forward:exibirInclusaoTronco";
		} else {
			try {
				controladorTabelaAuxiliar.pesquisarTroncoTabelaAuxiliar(tronco, TRONCO);
				controladorTabelaAuxiliar.inserir(tronco);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TroncoImpl.TRONCO_CAMPO_STRING);
				view = pesquisarTronco(tronco, result, tronco.isHabilitado(), model);
			} catch (NegocioException e) {
				view = "forward:exibirInclusaoTronco";
				mensagemErroParametrizado(model, request, e);
			}
		}
		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração relacionada a entidade tronco
	 * 
	 * @param troncoTmp
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirAlteracaoTronco")
	public String exibirAlteracaoTronco(TroncoImpl troncoTmp, BindingResult result, HttpServletRequest request, Model model)
					throws NegocioException {

		String view = null;
		carregarCityGates(model);
		TroncoImpl tronco = troncoTmp;
		model.addAttribute(TRONCO, tronco);
		if (!isPostBack(request)) {
			try {
				tronco = (TroncoImpl) controladorTabelaAuxiliar.obter(tronco.getChavePrimaria(), CLASSE);
				model.addAttribute(TRONCO, tronco);
				view = "exibirAlteracaoTronco";
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				view = TELA_EXIBIR_PESQUISA_TRONCO;
			}
		}

		return view;
	}

	/**
	 * Método responsável por atualizar os dados relacionados a entidade tronco
	 * 
	 * @param tronco
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("alterarTronco")
	public String alterarTronco(TroncoImpl tronco, BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		model.addAttribute(TRONCO, tronco);
		Map<String, Object> erros = tronco.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
			view = "forward:exibirAlteracaoTronco";
		} else {
			try {
				controladorTabelaAuxiliar.pesquisarTroncoTabelaAuxiliar(tronco, TRONCO);
				controladorTabelaAuxiliar.atualizar(tronco);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TroncoImpl.TRONCO_CAMPO_STRING);
				view = pesquisarTronco(tronco, result, tronco.isHabilitado(), model);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
				view = "forward:exibirAlteracaoTronco";
			}
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento relacionada a entidade tronco
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirDetalharTronco")
	public String exibirDetalharTronco(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model) {

		String view = null;
		try {
			model.addAttribute(TRONCO, controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE));
			view = "exibirDetalharTronco";
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:exibirPesquisaTronco";
		}
		return view;
	}

	/**
	 * Método responsável por realizar a remoção de uma entidade do tipo tronco
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerTronco")
	public String removerTronco(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
					HttpServletRequest request, Model model) throws GGASException {

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, TroncoImpl.TRONCO_CAMPO_STRING);
			return pesquisarTronco(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarTronco";
	}

	/**
	 * Método responsável por popular um filtro com dados relacionados a entidade tronco
	 * 
	 * @param tronco
	 * @param habilitado
	 * @return Map
	 */
	private Map<String, Object> popularFiltro(TroncoImpl tronco, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (tronco != null) {

			if (tronco.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", tronco.getChavePrimaria());
			}
			if (!StringUtils.isEmpty(tronco.getDescricao())) {
				filtro.put(DESCRICAO, tronco.getDescricao());
			}
			if (tronco.getCityGate() != null) {
				filtro.put(ID_CITY_GATE, tronco.getCityGate().getChavePrimaria());
			}

		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * Método responsável por carregar uma lista de cityGates disponíveis
	 * 
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarCityGates(Model model) throws NegocioException {

		model.addAttribute("listaCityGate", controladorTabelaAuxiliar.obterTodas(CityGateImpl.class, true));
	}
}
