/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe EmpresaAction representa uma EmpresaAction no sistema.
 *
 * @since 20/07/2009
 * 
 */

package br.com.ggas.web.cadastro.empresa;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.ControladorServicoPrestado;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.empresa.ServicoPrestado;
import br.com.ggas.cadastro.empresa.impl.EmpresaImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Controller responsável pelas telas relacionadas
 * a Empresa
 * 
 */
@Controller
public class EmpresaAction extends GenericAction {

	@Autowired
	@Qualifier("controladorServicoPrestado")
	private ControladorServicoPrestado controladorServicoPrestado;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	private static final String EXIBIR_PESQUISAR_EMPRESA = "exibirPesquisaEmpresa";
	//
	private static final String ID_SERVICO_PRESTADO = "idServicoPrestado";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String LISTA_SERVICOS_PRESTADOS = "listaServicoPrestado";

	private static final String EXIBIR_INCLUSAO_EMPRESA = "exibirInclusaoEmpresa";

	private static final String SERVICOS_PRESTADOS = "idServicosPrestados";

	private static final String EXIBIR_ATUALIZAR_EMPRESA = "exibirAtualizarEmpresa";
	
	private static final String EXIBIR_DETALHAR_EMPRESA = "exibirDetalhamentoEmpresa";
	
	private static final String CNPJ_FORMATADO = "cnpjFormatado";

	private static final String LOGO_EMPRESA = "logoEmpresa";

	private static final String EMPRESAS = "empresas";
	
	private static final String EMPRESA = "empresa";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String HABILITADO = "habilitado";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private String retorno;

	/**
	 * Método responsável por exibir a tela de pesquisar Empresa.
	 * 
	 * @param model {@link - Model}
	 * @return {@link String}
	 * @throws NegocioException
	 */
	@RequestMapping(EXIBIR_PESQUISAR_EMPRESA)
	public String exibirPesquisaEmpresa(Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		model.addAttribute(LISTA_SERVICOS_PRESTADOS, controladorServicoPrestado.consultarServicoPrestado(filtro));

		return EXIBIR_PESQUISAR_EMPRESA;
	}

	/**
	 * Método responsável por trazer empresas de acordo com um filtro passado pelo usuário.
	 * 
	 * @param empresa - {@link - EmpresaImpl}
	 * @param result - {@link - BindingResult}
	 * @param habilitado - {@link - String}
	 * @param model - {@link - Model}
	 * @param nomeCliente - {@link - String}
	 * @param cnpjFormatado - {@link - String}
	 * @param idServicoPrestado - {@link - Long}
	 * @return {@link String}
	 * @throws FormatoInvalidoException - {@link - FormatoInvalidoException}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	@RequestMapping("pesquisarEmpresa")
	public String pesquisarEmpresa(@ModelAttribute("EmpresaImpl") EmpresaImpl empresa, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) String habilitado, Model model,
			@RequestParam(value = NOME_CLIENTE, required = false) String nomeCliente,
			@RequestParam(value = CNPJ_FORMATADO, required = false) String cnpjFormatado,
			@RequestParam(value = ID_SERVICO_PRESTADO, required = false) Long idServicoPrestado) throws FormatoInvalidoException,
			NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		model.addAttribute("listaServicoPrestado", controladorServicoPrestado.consultarServicoPrestado(filtro));

		prepararFiltro(filtro, nomeCliente, cnpjFormatado, habilitado, idServicoPrestado);
		model.addAttribute(EMPRESAS, controladorEmpresa.consultarEmpresas(filtro));
		carregarModelPesquisa(model, idServicoPrestado, empresa, nomeCliente, cnpjFormatado, habilitado);

		return EXIBIR_PESQUISAR_EMPRESA;
	}

	/**
	 * Recebe os objetos que serão usados para pesquisa e os devolve já incluindo no model.
	 * 
	 * @param model - {@link - Model}
	 * @param idServicoPrestado - {@link - Long}
	 * @param empresa - {@link - EmpresaImpl}
	 * @param nomeCliente - {@link - String}
	 * @param cnpjFormatado - {@link - String}
	 * @param habilitado - {@link - String}
	 */
	private void carregarModelPesquisa(Model model, Long idServicoPrestado, EmpresaImpl empresa, String nomeCliente, String cnpjFormatado,
			String habilitado) {

		model.addAttribute(ID_SERVICO_PRESTADO, idServicoPrestado);
		model.addAttribute(EMPRESA, empresa);
		model.addAttribute(NOME_CLIENTE, nomeCliente);
		model.addAttribute(CNPJ_FORMATADO, cnpjFormatado);
		model.addAttribute(HABILITADO, habilitado);

	}

	/**
	 * Prepara o filtro para ser utilizado na consulta de empresas.
	 * 
	 * @param filtro - {@link - Map}
	 * @param nomeCliente - {@link - String}
	 * @param cnpjFormatado - {@link - String}
	 * @param habilitado - {@link - String}
	 * @param idServicoPrestado - {@link - Long}
	 * @throws FormatoInvalidoException - {@link - FormatoInvalidoException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, String nomeCliente, String cnpjFormatado, String habilitado,
			Long idServicoPrestado) throws FormatoInvalidoException {

		if (nomeCliente != null && !StringUtils.isEmpty(nomeCliente)) {
			filtro.put(NOME_CLIENTE, nomeCliente);
		}

		if (cnpjFormatado != null && !StringUtils.isEmpty(cnpjFormatado)) {
			String cnpjSemFormatacao = Util.removerCaracteresEspeciais(cnpjFormatado);
			filtro.put("cnpjCliente", cnpjSemFormatacao);
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}

		if (idServicoPrestado != null && idServicoPrestado > 0) {
			filtro.put(ID_SERVICO_PRESTADO, idServicoPrestado);
		}

	}

	/**
	 * Método responsável pela remoções das empresas.
	 * 
	 * @param model - {@link - Model}
	 * @param chavesPrimarias - {@link - Long}
	 * @param request - {@link - HttpServletRequest}
	 * @return {@link String}
	 * @throws FormatoInvalidoException - {@link - FormatoInvalidoException}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	@RequestMapping("excluirEmpresa")
	public String excluirEmpresa(Model model, @RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request)
			throws FormatoInvalidoException, NegocioException {

		try {
			controladorEmpresa.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, obterMensagem(Empresa.EMPRESAS));

			return pesquisarEmpresa(null, null, null, model, null, null, null);
		} catch (GGASException e) {

			mensagemErroParametrizado(model, (GGASException) e);

		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new GGASException(ControladorEmpresa.ERRO_NEGOCIO_EMPRESA_VINCULADA, e));
		}
		return pesquisarEmpresa(null, null, null, model, null, null, null);
	}

	/**
	 * Método responsável pela exibição da tela de Inclusão de Empresa.
	 * 
	 * @param model - {@link - Model}
	 * @return {@link String}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	@RequestMapping(EXIBIR_INCLUSAO_EMPRESA)
	public String exibirInclusaoEmpresa(Model model) throws NegocioException {
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute(LISTA_SERVICOS_PRESTADOS, controladorServicoPrestado.consultarServicoPrestado(filtro));
		model.addAttribute("formatosAceitosLogoEmpresa", formatosAceitos());

		return EXIBIR_INCLUSAO_EMPRESA;
	}

	/**
	 * Método responsável por realizar a inclusão de uma empresa.
	 * 
	 * @param model - {@link - Model}
	 * @param empresa - {@link - EmpresaImpl}
	 * @param result - {@link - BindingResult}
	 * @param idServicosPrestados - {@link - Long}
	 * @param request - {@link - HttpServletRequest}
	 * @param logoEmpresa - {@link - MultipartFile}
	 * @return {@link String}
	 * @throws NegocioException - {@link - NegocioException}
	 * @throws IOException - {@link - IOException}
	 * @throws FormatoInvalidoException - {@link - FormatoInvalidoException}
	 */
	@RequestMapping("incluirEmpresa")
	public String incluirEmpresa(Model model, EmpresaImpl empresa, BindingResult result,
			@RequestParam(value = SERVICOS_PRESTADOS, required = false) Long[] idServicosPrestados, HttpServletRequest request,
			@RequestParam(LOGO_EMPRESA) MultipartFile logoEmpresa) throws NegocioException, IOException, FormatoInvalidoException {

		retorno = EXIBIR_INCLUSAO_EMPRESA;

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<ServicoPrestado> listaServico = controladorServicoPrestado.consultarServicoPrestado(filtro);
		carregarCampos(empresa, request, logoEmpresa, idServicosPrestados);

		Map<String, Object> erros = empresa.validarDados();
		if (!erros.isEmpty()) {
			montarCamposExibicaoFalha(model, empresa, listaServico);
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorEmpresa.inserir(empresa);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, obterMensagem(Empresa.EMPRESA_ROTULO));
				retorno = pesquisarEmpresa(empresa, result, String.valueOf(empresa.isHabilitado()), model, null, null, null);
			} catch (NegocioException e) {
				montarCamposExibicaoFalha(model, empresa, listaServico);
				mensagemErroParametrizado(model, e);
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por montar os erros caso falhe a inclusão e setar no model.
	 * 
	 * @param model - {@link - Model}
	 * @param empresa - {@link - EmpresaImpl}
	 * @param listaServico - {@link - Collection}
	 * @return
	 */
	private Model montarCamposExibicaoFalha(Model model, EmpresaImpl empresa, Collection<ServicoPrestado> listaServico) {

		model.addAttribute("listaServicoPrestadoSelecionados", empresa.getServicosPrestados());

		if (empresa.getServicosPrestados() != null && empresa.getServicosPrestados().size() > 0) {
			listaServico.removeAll(empresa.getServicosPrestados());
		}
		model.addAttribute(LISTA_SERVICOS_PRESTADOS, listaServico);
		model.addAttribute(EMPRESA, empresa);

		return model;
	}

	/**
	 * Método responsável por capturar dados que precisam ser pre-configurados antes do cadastro da empresa.
	 * 
	 * @param empresa - {@link - EmpresaImpl}
	 * @param request - {@link - HttpServletRequest}
	 * @param logoEmpresa - {@link - MultipartFile}
	 * @param idServicosPrestados - {@link - Long}
	 * @throws NegocioException - {@link - NegocioException}
	 * @throws IOException - {@link - IOException}
	 */
	public void carregarCampos(EmpresaImpl empresa, HttpServletRequest request, MultipartFile logoEmpresa, Long[] idServicosPrestados)
			throws NegocioException, IOException {

		empresa.setHabilitado(true);
		empresa.setDadosAuditoria(getDadosAuditoria(request));

		if (idServicosPrestados != null && idServicosPrestados.length > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, idServicosPrestados);
			Collection<ServicoPrestado> listaServicoPrestadoSelecionados = controladorServicoPrestado.consultarServicoPrestado(filtro);

			empresa.setServicosPrestados(new HashSet(listaServicoPrestadoSelecionados));

		} else {
			empresa.setServicosPrestados(null);
		}

		if (controladorEmpresa.validarImagemDoArquivo(logoEmpresa, empresa.getExtensoesArquivoLogoEmpresa(),
				Empresa.TAMANHO_MAXIMO_ARQUIVO_LOGO_EMPRESA, Empresa.ERRO_NEGOCIO_TIPO_ARQUIVO,
				Empresa.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO, Boolean.FALSE)) {
			empresa.setLogoEmpresa(logoEmpresa.getBytes());
		}

	}

	/**
	 * Método responsável por trazer os tipos dos arquivos permitidos para cadastro da Logo da empresa.
	 * 
	 * @return {@link String}
	 */
	public String formatosAceitos() {

		StringBuilder formatosAceitosSb = new StringBuilder();

		for (int i = 0; i < Empresa.EXTENSOES_ARQUIVO_LOGO_EMPRESA.length; i++) {

			if (i == Empresa.EXTENSOES_ARQUIVO_LOGO_EMPRESA.length - 1) {
				formatosAceitosSb.append(" e ");
			} else if (i != 0) {
				formatosAceitosSb.append(", ");
			}
			formatosAceitosSb.append(Empresa.EXTENSOES_ARQUIVO_LOGO_EMPRESA[i]);
		}

		return formatosAceitosSb.toString();
	}

	/**
	 * Método responsável por exibir a tela Atualizar Empresa.
	 * 
	 * @param chavePrimaria - {@link - Long}
	 * @param removaLogo - {@link - Boolean}
	 * @param model - {@link - Model}
	 * @return {@link String}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_EMPRESA)
	public String exibirAtualizarEmpresa(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam(value = "removaLogo", required = false) Boolean removaLogo, Model model) throws NegocioException {

		EmpresaImpl empresa = (EmpresaImpl) controladorEmpresa.obter(chavePrimaria);
		Map<String, Object> filtro = new HashMap<String, Object>();

		Collection<ServicoPrestado> listaServico = controladorServicoPrestado.consultarServicoPrestado(filtro);
		listaServico.removeAll(empresa.getServicosPrestados());

		if (removaLogo != null && removaLogo) {
			empresa.setLogoEmpresa(null);
		}

		model.addAttribute(EMPRESA, empresa);
		model.addAttribute(LISTA_SERVICOS_PRESTADOS, listaServico);
		model.addAttribute("listaServicoPrestadoSelecionados", empresa.getServicosPrestados());
		model.addAttribute("formatosAceitosLogoEmpresa", formatosAceitos());

		return EXIBIR_ATUALIZAR_EMPRESA;
	}
	
	
	/**
	 * Método responsável por atualizar uma empresa.
	 * 
	 * @param empresa - {@link - EmpresaImpl}
	 * @param result - {@link - BindingResult}
	 * @param idServicosPrestados - {@link - Long}
	 * @param model - {@link - Model}
	 * @param request  - {@link - HttpServletRequest}
	 * @param logoEmpresa - {@link - MultipartFile}
	 * @param habilitado - {@link - String}
	 * @return {@link String}
	 * @throws NegocioException - {@link - NegocioException}
	 * @throws IOException - {@link - IOException}
	 */
	@RequestMapping("atualizarEmpresa")
	public String atualizarEmpresa(@ModelAttribute("EmpresaImpl") EmpresaImpl empresa, BindingResult result,
			@RequestParam(SERVICOS_PRESTADOS) Long[] idServicosPrestados, Model model, HttpServletRequest request,
			@RequestParam(LOGO_EMPRESA) MultipartFile logoEmpresa, @RequestParam(HABILITADO) String habilitado) throws NegocioException,
			IOException {

		retorno = EXIBIR_ATUALIZAR_EMPRESA;

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<ServicoPrestado> listaServico = controladorServicoPrestado.consultarServicoPrestado(filtro);
		carregarCampos(empresa, request, logoEmpresa, idServicosPrestados);
		empresa.setHabilitado(Boolean.valueOf(habilitado));

		Map<String, Object> erros = empresa.validarDados();
		if (!erros.isEmpty()) {
			montarCamposExibicaoFalha(model, empresa, listaServico);
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorEmpresa.atualizar(empresa);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, obterMensagem(Empresa.EMPRESA_ROTULO));
				retorno = pesquisarEmpresa(empresa, null, String.valueOf(empresa.isHabilitado()), model, null, null, null);
			} catch (GGASException e) {
				montarCamposExibicaoFalha(model, empresa, listaServico);
				mensagemErroParametrizado(model, e);
			}

		}
		return retorno;
	}
	
	/**
	 * Método responsável por exibir a tela Detalhar Empresa.
	 * 
	 * @param chavePrimaria - {@link - Long}
	 * @param model - {@link - Model}
	 * @return String {@link String}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	@RequestMapping(EXIBIR_DETALHAR_EMPRESA)
	public String exibirDetalhamentoEmpresa(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute("listaServicoPrestadoSelecionados", controladorServicoPrestado.consultarServicoPrestado(filtro));
		model.addAttribute(EMPRESA, controladorEmpresa.obter(chavePrimaria));

		return EXIBIR_DETALHAR_EMPRESA;
	}

	/**
	 * Método responsável por exibir na tela o logomarca da empresa.
	 *
	 * @param mapping the mapping
	 * @param form O formulário
	 * @param request O objeto request
	 * @param response O objeto response
	 * @param chavePrimaria
	 * @return ActionForward O retorno da ação
	 * @throws NegocioException 
	 * @throws IOException 
	 * @throws Exception Caso ocorra algum erro
	 */

	@RequestMapping(value = "exibirLogoEmpresa/{chavePrimaria}")
	public void exibirLogoMarcaEmpresa(@PathVariable(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response) throws NegocioException, IOException  {

		EmpresaImpl empresa = (EmpresaImpl) controladorEmpresa.obter(chavePrimaria);

		byte[] logo = empresa.getLogoEmpresa();

		if (logo != null) {

			try {
				ByteArrayInputStream in = new ByteArrayInputStream(logo);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				response.reset();
				response.setContentType("image/jpeg");

				while ((index = in.read(bytearray)) != -1) {
					response.getOutputStream().write(bytearray, 0, index);
				}
				response.flushBuffer();
				in.close();

			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			} finally {
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
		}
	}
}
