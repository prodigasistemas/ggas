/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.cadastro.imovel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.impl.PadraoConstrucaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a Padrão de Construção.
 */
@Controller
public class PadraoConstrucaoAction extends GenericAction {

	private static final String EXIBIR_PESQUISA_PADRAO_CONSTRUCAO = "exibirPesquisaPadraoConstrucao";

	private static final String PADRAO_CONSTRUCAO = "padraoConstrucao";

	private static final String EXIBIR_ATUALIZAR_PADRAO_CONSTRUCAO = "exibirAtualizarPadraoConstrucao";

	private static final String EXIBIR_INSERIR_PADRAO_CONSTRUCAO = "exibirInserirPadraoConstrucao";

	private static final Class<PadraoConstrucaoImpl> CLASSE = PadraoConstrucaoImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.imovel.impl.PadraoConstrucaoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_PADRAO_CONSTRUCAO = "listaPadraoConstrucao";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa padrão de construção.
	 * 
	 * @param model
	 * @param session
	 * @return String
	 */
	@RequestMapping(EXIBIR_PESQUISA_PADRAO_CONSTRUCAO)
	public String exibirPesquisaPadraoConstrucao(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_PADRAO_CONSTRUCAO;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de padrão de construção.
	 * 
	 * @param padraoConstrucao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarPadraoConstrucao")
	public String pesquisarPadraoConstrucao(PadraoConstrucaoImpl padraoConstrucao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(padraoConstrucao, habilitado);

		Collection<TabelaAuxiliar> listaPadraoConstrucao = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_PADRAO_CONSTRUCAO, listaPadraoConstrucao);
		model.addAttribute(PADRAO_CONSTRUCAO, padraoConstrucao);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_PADRAO_CONSTRUCAO;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento padrão de construção.
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirDetalhamentoPadraoConstrucao")
	public String exibirDetalhamentoPadraoConstrucao(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaPadraoConstrucao";
		
		try {
			PadraoConstrucaoImpl padraoConstrucao = (PadraoConstrucaoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(PADRAO_CONSTRUCAO, padraoConstrucao);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoPadraoConstrucao";
		} catch (NegocioException e) {
			mensagemErro(model, e);
			
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir padrão de construção.
	 * 
	 * @return String
	 */
	@RequestMapping(EXIBIR_INSERIR_PADRAO_CONSTRUCAO)
	public String exibirInserirEquipamento() {

		return EXIBIR_INSERIR_PADRAO_CONSTRUCAO;
	}

	/**
	 * Método responsável por inserir padrão de construção.
	 * 
	 * @param padraoConstrucao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirPadraoConstrucao")
	private String inserirPadraoConstrucao(PadraoConstrucaoImpl padraoConstrucao, BindingResult result, Model model,
					HttpServletRequest request) throws GGASException {

		String retorno = EXIBIR_INSERIR_PADRAO_CONSTRUCAO;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(padraoConstrucao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(padraoConstrucao, "Padrão de Construção");
			controladorTabelaAuxiliar.inserir(padraoConstrucao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, PadraoConstrucao.PADRAO_CONSTRUCAO);
			retorno = pesquisarPadraoConstrucao(padraoConstrucao, result, padraoConstrucao.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute("padraoConstrucao", padraoConstrucao);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar padrão de construção.
	 * 
	 * @param construcao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_PADRAO_CONSTRUCAO)
	public String exibirAtualizarPadraoConstrucao(PadraoConstrucaoImpl construcao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		String retorno = "forward:exibirPesquisaPadraoConstrucao";
		
		PadraoConstrucaoImpl padraoConstrucao = construcao;

		try {
			padraoConstrucao = (PadraoConstrucaoImpl) controladorTabelaAuxiliar.obter(padraoConstrucao.getChavePrimaria(), CLASSE);
			model.addAttribute(PADRAO_CONSTRUCAO, padraoConstrucao);
			model.addAttribute(HABILITADO, habilitado);
			retorno = EXIBIR_ATUALIZAR_PADRAO_CONSTRUCAO;
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o padrão de construção.
	 * 
	 * @param padraoConstrucao
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * @throws ConcorrenciaException
	 */
	@RequestMapping("atualizarPadraoConstrucao")
	private String atualizarPadraoConstrucao(PadraoConstrucaoImpl padraoConstrucao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = EXIBIR_ATUALIZAR_PADRAO_CONSTRUCAO;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(padraoConstrucao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(padraoConstrucao, "Padrão de Construção");
			controladorTabelaAuxiliar.atualizar(padraoConstrucao, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, PadraoConstrucao.PADRAO_CONSTRUCAO);
			retorno = pesquisarPadraoConstrucao(padraoConstrucao, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(PADRAO_CONSTRUCAO, padraoConstrucao);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o padrão de construção.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerPadraoConstrucao")
	public String removerPadraoConstrucao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = "forward:pesquisarPadraoConstrucao";
		
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, PadraoConstrucao.PADRAO_CONSTRUCAO);
			retorno = pesquisarPadraoConstrucao(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param padraoConstrucao
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(PadraoConstrucaoImpl padraoConstrucao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (padraoConstrucao != null) {

			if (padraoConstrucao.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, padraoConstrucao.getChavePrimaria());
			}

			if (padraoConstrucao.getDescricao() != null && !padraoConstrucao.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, padraoConstrucao.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
