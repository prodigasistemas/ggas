package br.com.ggas.web.cadastro.setorcensitario;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.dadocensitario.impl.SetorCensitarioImpl;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe respopnsável pelas telas relacionadas ao Setor Censitario
 *
 */
@Controller
public class SetorCensitarioAction extends GenericAction {

	private static final String HABILITADO = "habilitado";

	private static final String TELA_EXIBIR_PESQUISA_SETOR_CENSITARIO = "exibirPesquisaSetorCensitario";

	private static final String SETOR_CENSITARIO = "setorCensitario";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	private ControladorMunicipio controladorMunicipio;

	/**
	 * Método responsável pela exibição da tela de pesquisa do Setor Censitário
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_SETOR_CENSITARIO)
	public String exibirPesquisaSetorCensitario(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		carregarMunicipios(model);

		return TELA_EXIBIR_PESQUISA_SETOR_CENSITARIO;
	}

	/**
	 * @param model
	 *            - {@link Model}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	private void carregarMunicipios(Model model) throws NegocioException {
		model.addAttribute("listaMunicipios", controladorMunicipio.listarMunicipios());
	}

	/**
	 * Método responsável pela pesquisa de um Setor Censitário
	 * 
	 * @param setorCensitario
	 *            - {@link SetorCensitarioImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarSetorCensitario")
	public String pesquisarSetorCensitario(SetorCensitarioImpl setorCensitario, BindingResult result,
			@RequestParam(HABILITADO) Boolean habilitado, Model model) throws GGASException {
		carregarMunicipios(model);
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(setorCensitario, filtro, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("setorCensitario", setorCensitario);
		model.addAttribute("listaSetorCensitario",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, SetorCensitarioImpl.class.getName()));

		return "exibirPesquisaSetorCensitario";
	}

	/**
	 * @param setorCensitario
	 *            - {@link SetorCensitarioImpl}
	 * @param filtro
	 *            - {@link Map}
	 * @param habilitado
	 *            - {@link Boolean}
	 */
	private void prepararFiltro(SetorCensitarioImpl setorCensitario, Map<String, Object> filtro, Boolean habilitado) {

		if (setorCensitario != null) {
			if (setorCensitario.getDescricao() != null && !StringUtils.isEmpty(setorCensitario.getDescricao())) {
				filtro.put("descricao", setorCensitario.getDescricao());
			}

			if (setorCensitario.getDescricaoAbreviada() != null
					&& !StringUtils.isEmpty(setorCensitario.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", setorCensitario.getDescricaoAbreviada());
			}

			if (setorCensitario.getMunicipio() != null) {
				filtro.put("idMunicipio", setorCensitario.getMunicipio().getChavePrimaria());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
	}

	/**
	 * Método responsável pela exibição da tela de inclusão de um Setor Censitario
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirIncluirSetorCensitario")
	public String exibirIncluirSetorCensitario(Model model) throws NegocioException {

		carregarMunicipios(model);
		return "exibirIncluirSetorCensitario";
	}

	/**
	 * Método responsável pela inclusão de um Setor Censitário
	 * 
	 * @param setorCensitario
	 *            - {@link SetorCensitarioImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirSetorCensitario")
	public String incluirSetorCensitario(SetorCensitarioImpl setorCensitario, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "forward:exibirIncluirSetorCensitario";
		model.addAttribute(SETOR_CENSITARIO, setorCensitario);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(setorCensitario, "SetorCensitario");
			controladorTabelaAuxiliar.inserir(setorCensitario);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					SetorCensitarioImpl.SETOR_CENSITARIO_TITULO);
			view = pesquisarSetorCensitario(setorCensitario, result, setorCensitario.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração de um Setor Censitário
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirAlterarSetorCensitario")
	public String exibirAlterarSetorCensitario(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {
		carregarMunicipios(model);
		String view = "exibirAlterarSetorCensitario";

		if (!isPostBack(request)) {
			try {
				SetorCensitarioImpl setorCensitario = (SetorCensitarioImpl) controladorTabelaAuxiliar
						.obter(chavePrimaria, SetorCensitarioImpl.class);
				model.addAttribute(SETOR_CENSITARIO, setorCensitario);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				view = exibirPesquisaSetorCensitario(model);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela alteração de um Setor Censitário
	 * 
	 * @param setorCensitario
	 *            - {@link SetorCensitarioImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarSetorCensitario")
	public String alterarSetorCensitario(SetorCensitarioImpl setorCensitario, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "forward:exibirAlterarSetorCensitario";
		model.addAttribute(SETOR_CENSITARIO, setorCensitario);
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(setorCensitario, "SetorCensitario");
			controladorTabelaAuxiliar.atualizar(setorCensitario, SetorCensitarioImpl.class);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					SetorCensitarioImpl.SETOR_CENSITARIO_TITULO);
			view = pesquisarSetorCensitario(setorCensitario, result, setorCensitario.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de detalhamento de um Setor
	 * Censitário
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link Sting}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("exibirDetalharSetorCensitario")
	public String exibirDetalharSetorCensitario(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {
		String view = "exibirDetalharSetorCensitario";

		try {
			SetorCensitarioImpl setorCensitario = (SetorCensitarioImpl) controladorTabelaAuxiliar.obter(chavePrimaria,
					SetorCensitarioImpl.class);
			model.addAttribute(SETOR_CENSITARIO, setorCensitario);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPesquisaSetorCensitario(model);
		}

		return view;
	}

	/**
	 * Método responsável pela remoção de um Setor Censitário
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("removerSetorCensitario")
	public String removerSetorCensitario(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) {

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, SetorCensitarioImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request,
					SetorCensitarioImpl.SETOR_CENSITARIO_TITULO);
			return pesquisarSetorCensitario(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarSetorCensitario";
	}
}
