/*

Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.cadastro.imovel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.impl.PerfilImovelImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo para
 * realizar alterações nas informações das telas referentes a Perfil de Imóvel.
 * 
 * @author bruno silva
 * 
 */
@Controller
public class PerfilImovelAction extends GenericAction {
	
	private static final int NUMERO_MAXIMO_DESCRICAO = 50;

	private static final String EXIBIR_PESQUISA_PERFIL_IMOVEL = "exibirPesquisaPerfilImovel";

	private static final String PERFIL_IMOVEL = "perfilImovel";

	private static final String EXIBIR_ATUALIZAR_PERFIL_IMOVEL = "exibirAtualizarPerfilImovel";

	private static final String EXIBIR_INSERIR_PERFIL_IMOVEL = "exibirInserirPerfilImovel";

	private static final Class<PerfilImovelImpl> CLASSE = PerfilImovelImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.imovel.impl.PerfilImovelImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_PERFIL_IMOVEL = "listaPerfilImovel";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa perfil de imóvel.
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISA_PERFIL_IMOVEL)
	public String exibirPesquisaPerfilImovel(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_PERFIL_IMOVEL;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de perfil de imóvel.
	 * 
	 * @param perfilImovel - {@link PerfilImovelImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarPerfilImovel")
	public String pesquisarPerfilImovel(PerfilImovelImpl perfilImovel, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(perfilImovel, habilitado);

		Collection<TabelaAuxiliar> listaPerfilImovel = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_PERFIL_IMOVEL, listaPerfilImovel);
		model.addAttribute(PERFIL_IMOVEL, perfilImovel);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_PERFIL_IMOVEL;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento perfil de imóvel.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoPerfilImovel")
	public String exibirDetalhamentoPerfilImovel(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaPerfilImovel";
		
		try {
			PerfilImovelImpl perfilImovel = (PerfilImovelImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(PERFIL_IMOVEL, perfilImovel);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoPerfilImovel";
		} catch (NegocioException e) {
			mensagemErro(model, e);
			
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir perfil de imóvel.
	 * 
	 * @return String
	 */
	@RequestMapping(EXIBIR_INSERIR_PERFIL_IMOVEL)
	public String exibirInserirEquipamento() {

		return EXIBIR_INSERIR_PERFIL_IMOVEL;
	}

	/**
	 * Método responsável por inserir perfil de imóvel.
	 * 
	 * @param perfilImovel
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirPerfilImovel")
	private String inserirPerfilImovel(PerfilImovelImpl perfilImovel, BindingResult result, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = EXIBIR_INSERIR_PERFIL_IMOVEL;

		try {

			if (perfilImovel.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(perfilImovel);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(perfilImovel, "Perfil de Imóvel");
			controladorTabelaAuxiliar.inserir(perfilImovel);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, PerfilImovel.PERFIL_IMOVEL);
			retorno = pesquisarPerfilImovel(perfilImovel, result, perfilImovel.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute("perfilImovel", perfilImovel);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar perfil de imóvel.
	 * 
	 * @param construcao - {@link PerfilImovelImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String} 
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_PERFIL_IMOVEL)
	public String exibirAtualizarPerfilImovel(PerfilImovelImpl construcao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		String retorno = "forward:exibirPesquisaPerfilImovel";
		
		PerfilImovelImpl perfilImovel = construcao;

		try {
			perfilImovel = (PerfilImovelImpl) controladorTabelaAuxiliar.obter(perfilImovel.getChavePrimaria(), CLASSE);
			model.addAttribute(PERFIL_IMOVEL, perfilImovel);
			model.addAttribute(HABILITADO, habilitado);
			retorno = EXIBIR_ATUALIZAR_PERFIL_IMOVEL;
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o perfil de imóvel.
	 * 
	 * @param perfilImovel
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * @throws ConcorrenciaException
	 */
	@RequestMapping("atualizarPerfilImovel")
	private String atualizarPerfilImovel(PerfilImovelImpl perfilImovel, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = EXIBIR_ATUALIZAR_PERFIL_IMOVEL;

		try {

			if (perfilImovel.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(perfilImovel);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(perfilImovel, "Perfil de Imóvel");
			controladorTabelaAuxiliar.atualizar(perfilImovel, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, PerfilImovel.PERFIL_IMOVEL);
			retorno = pesquisarPerfilImovel(perfilImovel, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(PERFIL_IMOVEL, perfilImovel);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o perfil de imóvel.
	 * 
	 * @param chavesPrimarias - {@link Long} Array
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("removerPerfilImovel")
	public String removerPerfilImovel(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = "forward:pesquisarPerfilImovel";
		
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, PerfilImovel.PERFIL_IMOVEL);
			retorno = pesquisarPerfilImovel(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param perfilImovel
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(PerfilImovelImpl perfilImovel, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (perfilImovel != null) {

			if (perfilImovel.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, perfilImovel.getChavePrimaria());
			}

			if (perfilImovel.getDescricao() != null && !perfilImovel.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, perfilImovel.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
