package br.com.ggas.web.cadastro.veiculo;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.cadastro.veiculo.ControladorVeiculo;
import br.com.ggas.cadastro.veiculo.impl.VeiculoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;

@Controller
public class VeiculoAction extends GenericAction{
	
	private static final String UNIDADES_FEDERACAO = "unidadesFederacao";
	private static final String ALTERACAO_SUCESSO = "Veículo alterado com sucesso";
	private static final String INCLUSAO_SUCESSO = "Veículo alterado com sucesso";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	
	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;
	
	@Autowired
	@Qualifier("controladorVeiculo")
	private ControladorVeiculo controladorVeiculo;

	@RequestMapping("exibirPesquisaVeiculo")
	public String exibirPesquisaComunicacao(HttpSession session, HttpServletResponse response,Model model) throws GGASException {

		model.addAttribute(UNIDADES_FEDERACAO,controladorEndereco.listarTodasUnidadeFederacao());
		return "exibirPesquisaVeiculo";
	}
	
	@RequestMapping("pesquisarVeiculo")
	public String pesquisarVeiculo(@ModelAttribute("Veiculo") VeiculoImpl veiculo, 
			BindingResult result, @RequestParam(value = "habilitado", required = false) String habilitado, HttpSession session, 
			HttpServletResponse response, Model model) throws GGASException {

		model.addAttribute("habilitado",habilitado);
		model.addAttribute("veiculo",veiculo);
		model.addAttribute(UNIDADES_FEDERACAO,controladorEndereco.listarTodasUnidadeFederacao());
		model.addAttribute("listaVeiculos",controladorVeiculo.consultarVeiculo(veiculo,habilitado));
		return "exibirPesquisaVeiculo";

	}
	
	@RequestMapping("exibirIncluirVeiculo")
	public ModelAndView exibirIncluirVeiculo(HttpSession session, HttpServletResponse response) throws GGASException {
		ModelAndView model = new ModelAndView("exibirIncluirVeiculo");
		model.addObject("fluxoInclusao", true);
		model.addObject(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		
		return model;
	}
	
	@RequestMapping("incluirVeiculo")
	public String incluirVeiculo(@ModelAttribute("Veiculo") VeiculoImpl veiculo, BindingResult result, 
			@RequestParam(value = "habilitado", required = false) String habilitado, Model model, HttpSession session, 
			HttpServletResponse response) throws GGASException {

		model.addAttribute("veiculo",veiculo);
		model.addAttribute(UNIDADES_FEDERACAO,controladorEndereco.listarTodasUnidadeFederacao());
		controladorVeiculo.inserir(veiculo);
		
		veiculo.setHabilitado(Boolean.parseBoolean(habilitado));
		controladorVeiculo.atualizar(veiculo);
		model.addAttribute("listaVeiculos",controladorVeiculo.consultarVeiculo(veiculo,habilitado));
		
		
		return "exibirPesquisaVeiculo";
	}
	
	
	@RequestMapping("exibirAlterarVeiculo")
	public ModelAndView exibirAlterarVeiculo(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpSession session, HttpServletResponse response) throws GGASException {
		ModelAndView model = new ModelAndView("exibirIncluirVeiculo");
		model.addObject("fluxoAlteracao", true);
		model.addObject(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		
		VeiculoImpl veiculo = (VeiculoImpl) controladorVeiculo.obter(chavePrimaria);
		model.addObject("veiculo", veiculo);
		return model;
	}
	
	@RequestMapping("alterarVeiculo")
	public String alterarVeiculo(@ModelAttribute("Veiculo") VeiculoImpl veiculo, BindingResult result, 
			@RequestParam(value = "habilitado", required = false) String habilitado, Model model, 
			HttpSession session, HttpServletResponse response) throws GGASException {
		
		model.addAttribute("veiculo",veiculo);
		model.addAttribute(UNIDADES_FEDERACAO,controladorEndereco.listarTodasUnidadeFederacao());
		controladorVeiculo.atualizar(veiculo);
		model.addAttribute("listaVeiculos",controladorVeiculo.consultarVeiculo(veiculo, habilitado));
		mensagemSucesso(model,ALTERACAO_SUCESSO);
		
		return "exibirPesquisaVeiculo";
	}
}
