/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 */

package br.com.ggas.web.cadastro.cliente.rest;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.Map;

/**
 * Classe responsável por gerenciar as respostas rest relacionadas a clientes
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Controller
public class ClienteActionRest extends GenericActionRest {

	@Autowired
	@Qualifier("clienteRestNegocio")
	private ClienteRestNegocio negocio;

	/**
	 * Realiza a busca de clientes.
	 * A busca é realizada através do {@link PesquisaClienteDTO}
	 * Utilize essa classe em conjunto com telas de pesquisa via
	 * rest do front end.
	 *
	 * @param pesquisa busca clientes a partir de um ou mais campos
	 *                    - {@link - PesquisaClienteDTO}
	 * @return retorna a resposta da requisição em json
	 * @throws GGASException lançada quando ocorre alguma falha durante
	 * o procedimento da requisição - {@link - GGASException}
	 */
	@RequestMapping(value = "rest/cliente/buscar", produces = "application/json; charset=utf-8",
			method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarClientes(@RequestBody PesquisaClienteDTO pesquisa) throws GGASException {
		final Collection<Cliente> clientes = negocio.buscarClientes(pesquisa);
		final String json = serializarJson(clientes);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Busca a lista de cliientes de forma paginada
	 * @param quantidadeMax quantidade máxima de clientes por página
	 * @param pagina página de clientes
	 * @param nomeCliente nome do cliente
	 * @return retorna um json com as resposta
	 * @throws GGASException lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "rest/cliente/buscarPaginado", produces = "application/json; charset=utf-8",
			method = {RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> buscarClientesPaginados(@RequestParam(value = "quantidadeMax") int quantidadeMax,
			@RequestParam(value = "pagina") int pagina, @RequestParam(value = "nomeCliente") String nomeCliente) throws GGASException {
		final PesquisaClienteDTO pesquisa = new PesquisaClienteDTO();
		pesquisa.setNomeGenerico(nomeCliente);
		final Collection<Cliente> clientes = negocio.buscarClientesPaginados(pesquisa, quantidadeMax, pagina);
		final String json = serializarJson(clientes);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Obtém o mapa de serializadores customizáveis
	 *
	 * @return retorna uma mapa de class e a implementação
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder().put(Cliente.class, new ClienteSimplesSerializador())
				.put(ClienteFone.class, new ClienteFoneSimplesSerializador()).build();
	}

}
