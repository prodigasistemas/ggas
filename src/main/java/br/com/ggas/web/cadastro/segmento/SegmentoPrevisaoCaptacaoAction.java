/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 30/12/2013 vpessoa 11:15:44
 */

package br.com.ggas.web.cadastro.segmento;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.impl.SegmentoPrevisaoCaptacao;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * @author vpessoa
 */
@Controller
public class SegmentoPrevisaoCaptacaoAction extends GenericAction {

	private static final String SEGMENTO_PREVISAO_CAPTACAO = "segmentoPrevisaoCaptacao";

	private static final String TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO = "exibirIncluirSegmentoPrevisaoCaptacao";

	@Autowired
	private ControladorPrevisaoCaptacao controladorPrevisaoCaptacao;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	/**
	 * Exibir incluir segmento previsao captacao.
	 * 
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping(TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO)
	public ModelAndView exibirIncluirSegmentoPrevisaoCaptacao() throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO);
		carregarCombos(model);
		model.addObject("fluxoInclusao", Boolean.TRUE);

		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model the model
	 * @throws NegocioException the negocio exception
	 */
	private void carregarCombos(ModelAndView model) throws NegocioException {

		List<String> listaAnoReferencia = new ArrayList<String>();
		Calendar calendario = GregorianCalendar.getInstance();
		int anoAtual = calendario.get(Calendar.YEAR);
		for (int anoIFinal = anoAtual + 3; anoAtual <= anoIFinal; anoAtual++) {
			listaAnoReferencia.add(String.valueOf(anoAtual));
		}
		model.addObject("listaAnoReferencia", listaAnoReferencia);
		model.addObject("listaSegmento", controladorSegmento.listarSegmento());
	}

	/**
	 * Incluir segmento previsao captacao.
	 * 
	 * @param segmentoPrevisaoCaptacao the segmento previsao captacao
	 * @param result the result
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("incluirSegmentoPrevisaoCaptacao")
	public ModelAndView incluirSegmentoPrevisaoCaptacao(
			@ModelAttribute("SegmentoPrevisaoCaptacao") SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao, BindingResult result)
			throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/pesquisarSegmentoPrevisaoCaptacao");

		try {

			if (segmentoPrevisaoCaptacao.getChavePrimaria() == 0) {
				controladorPrevisaoCaptacao.inserir(segmentoPrevisaoCaptacao);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, GenericAction.obterMensagem(Constantes.PREVISAO_CAPTACAO));
			} else {
				controladorPrevisaoCaptacao.atualizar(segmentoPrevisaoCaptacao);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.PREVISAO_CAPTACAO));
			}

		} catch (ConcorrenciaException e) {
			model = new ModelAndView(TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO);
			carregarCombos(model);
			model.addObject(SEGMENTO_PREVISAO_CAPTACAO, segmentoPrevisaoCaptacao);
			mensagemErroParametrizado(model, e);
		} catch (NegocioException e1) {
			model = new ModelAndView(TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO);
			carregarCombos(model);
			model.addObject(SEGMENTO_PREVISAO_CAPTACAO, segmentoPrevisaoCaptacao);
			mensagemErroParametrizado(model, e1);
		}

		return model;
	}

	/**
	 * Exibir pesquisa segmento previsao captacao.
	 * 
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirPesquisaSegmentoPrevisaoCaptacao")
	public ModelAndView exibirPesquisaSegmentoPrevisaoCaptacao() throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaSegmentoPrevisaoCaptacao");
		carregarCombos(model);

		return model;
	}

	/**
	 * Pesquisar segmento previsao captacao.
	 * 
	 * @param segmentoPrevisaoCaptacao the segmento previsao captacao
	 * @param result the result
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("pesquisarSegmentoPrevisaoCaptacao")
	public ModelAndView pesquisarSegmentoPrevisaoCaptacao(
			@ModelAttribute("SegmentoPrevisaoCaptacao") SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao, BindingResult result)
			throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaSegmentoPrevisaoCaptacao");
		try {
			carregarCombos(model);
			model.addObject(SEGMENTO_PREVISAO_CAPTACAO, segmentoPrevisaoCaptacao);
			model.addObject("listaPrevisaoCaptacao",
					controladorPrevisaoCaptacao.consultarSegmentoPrevisaoCaptacao(segmentoPrevisaoCaptacao));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir detalhamento segmento previsao captacao.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoSegmentoPrevisaoCaptacao")
	public ModelAndView exibirDetalhamentoSegmentoPrevisaoCaptacao(@RequestParam("chavesPrimarias") Long chavePrimaria)
			throws GGASException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoSegmentoPrevisaoCaptacao");
		SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao = controladorPrevisaoCaptacao.obterPrevisaoCaptacao(chavePrimaria, "");
		model.addObject(SEGMENTO_PREVISAO_CAPTACAO, segmentoPrevisaoCaptacao);

		return model;
	}

	/**
	 * Exibir alterar segmento previsao captacao.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirAlterarSegmentoPrevisaoCaptacao")
	public ModelAndView exibirAlterarSegmentoPrevisaoCaptacao(@RequestParam("chavesPrimaria") Long chavePrimaria,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUIR_SEGMENTO_PREVISAO_CAPTACAO);

		SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao = controladorPrevisaoCaptacao.obterPrevisaoCaptacao(chavePrimaria, "");
		model.addObject(SEGMENTO_PREVISAO_CAPTACAO, segmentoPrevisaoCaptacao);
		carregarCombos(model);
		model.addObject("fluxoAlteracao", Boolean.TRUE);

		return model;
	}

	/**
	 * Remover segmento previsao captacao.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param request the request
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("removerSegmentoPrevisaoCaptacao")
	public ModelAndView removerSegmentoPrevisaoCaptacao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
			throws NegocioException {

		return new ModelAndView("forward:/pesquisarAcao");
	}

}
