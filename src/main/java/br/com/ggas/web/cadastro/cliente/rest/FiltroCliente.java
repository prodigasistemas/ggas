/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.cliente.rest;

/**
 * Lista as opções de filtros do cliente
 * @author jose.victor@logiquesistemas.com.br.
 */
public enum FiltroCliente {

	CHAVES_PRIMARIAS("chavesPrimarias"),
	CODIGO_TIPO_PESSOA("codigoTipoPessoa"),
	CHAVE_PRIMARIA("chavePrimaria"),
	NOME("nome"),
	NOME_FANTASIA("nomeFantasia"),
	CNPJ("cnpj"),
	CPF("cpf"),
	PASSAPORTE("passaporte"),
	INSCRICAO_ESTADUAL("inscricaoEstadual"),
	INSCRICAO_MUNICIPAL("inscricaoMunicipal"),
	INSCRICAO_RURAL("inscricaoRural"),
	ID_ATIVIDADE_ECONOMIA("idAtividadeEconomia"),
	CEP("cep"),
	HABILITADO("habilitado"),
	LISTA_ID_CLIENTE("listaIdCliente"),
	COM_GRUPO_ECONOMICO("comGrupoEconomico"),
	NOME_GENERICO("qualquerNome");

	private String chave;

	FiltroCliente(String chave) {
		this.chave = chave;
	}

	/**
	 * Obtém a chave do filtro
	 * @return string com a chave do filtro
	 */
	public String getChave() {
		return chave;
	}
}
