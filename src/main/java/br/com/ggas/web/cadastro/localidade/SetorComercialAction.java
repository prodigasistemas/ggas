/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe SetorComercialAction representa um SetorComercialAction no sistema.
 *
 * @since 30/072009
 *
 */

package br.com.ggas.web.cadastro.localidade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.impl.SetorComercialImpl;
import br.com.ggas.cadastro.localidade.impl.SetorComercialVO;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas ao Setor Comercial
 *
 */
@Controller
public class SetorComercialAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_SETOR_COMERCIAL = "exibirPesquisaSetorComercial";

	private static final String TELA_EXIBIR_INSERCAO_SETOR_COMERCIAL = "exibirInsercaoSetorComercial";

	private static final String SETOR_COMERCIAL_FORM = "setorComercialForm";

	private static final String HABILITADO = "habilitado";

	private static final String ID_UNIDADE_FEDERACAO = "idUnidadeFederacao";

	private static final String ID_MUNICIPIO = "idMunicipio";

	private static final String DESCRICAO = "descricao";

	private static final String CODIGO = "codigo";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String GERENCIA_REGIONAL = "idGerenciaRegional";

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorGerenciaRegional")
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	@Qualifier("controladorSetorComercial")
	private ControladorSetorComercial controladorSetorComercial;

	/**
	 * Metodo responsavel por exibir a tela de detalhamento de um setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhamentoSetorComercial")
	public String exibirDetalhamentoSetorComercial(SetorComercialVO setorComercialVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		
		model.addAttribute("setorComercialVO", setorComercialVO);
		try {
			SetorComercialImpl setorComercial = (SetorComercialImpl) controladorSetorComercial
					.obter(setorComercialVO.getChavePrimaria());
			model.addAttribute(SETOR_COMERCIAL_FORM, setorComercial);
			retorno = "exibirDetalhamentoSetorComercial";
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
			retorno = TELA_EXIBIR_PESQUISA_SETOR_COMERCIAL;
		}

		carregarCampos(setorComercialVO, model);
		
		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(TELA_EXIBIR_INSERCAO_SETOR_COMERCIAL)
	public String exibirInsercaoSetorComercial(SetorComercialVO setorComercialVO, BindingResult result, Model model)
			throws GGASException {

		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);
		carregarCampos(setorComercialVO, model);

		return TELA_EXIBIR_INSERCAO_SETOR_COMERCIAL;
	}

	/**
	 * Método responsável por inserir o setor comercial no sistema.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("inserirSetorComercial")
	public String inserirSetorComercial(SetorComercialVO setorComercialVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		
		SetorComercialImpl setorComercial = (SetorComercialImpl) controladorSetorComercial.criar();
		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);

		try {
			popularSetorComercial(setorComercialVO, setorComercial);
			Map<String, Object> erros = setorComercial.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
				retorno = exibirInsercaoSetorComercial(setorComercialVO, result, model);
			} else {

				setorComercial.setHabilitado(true);
				setorComercialVO.setHabilitado(true);
				model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);
				setorComercial.setDadosAuditoria(getDadosAuditoria(request));
				controladorSetorComercial.inserir(setorComercial);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, SetorComercial.SETOR_COMERCIAL);
				retorno = "pesquisarSetoresComerciais";
			}
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = TELA_EXIBIR_INSERCAO_SETOR_COMERCIAL;
		} catch (FormatoInvalidoException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = TELA_EXIBIR_INSERCAO_SETOR_COMERCIAL;
		}
		return "forward:" + retorno;
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param model
	 * @param request
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_SETOR_COMERCIAL)
	public String exibirPesquisaSetorComercial(SetorComercialVO setorComercialVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		if (!this.isPostBack(request) && (request.getParameter(HABILITADO) == null)) {
			setorComercialVO.setHabilitado(true);
		}
		carregarCampos(setorComercialVO, model);
		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);

		return TELA_EXIBIR_PESQUISA_SETOR_COMERCIAL;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de setor comercial no
	 * sistema.
	 *
	 * @param setorComercialVO
	 * @param result
	 * @param model
	 * @return String
	 * @param request
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarSetoresComerciais")
	public String pesquisarSetoresComerciais(SetorComercialVO setorComercialVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		Map<String, Object> filtro = obterFiltroForm(setorComercialVO);
		Collection<SetorComercial> listaSetorComercial = controladorSetorComercial.consultarSetorComercial(filtro);

		model.addAttribute("listaSetorComercial", listaSetorComercial);
		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);

		return "forward:exibirPesquisaSetorComercial";
	}

	/**
	 * Método responsável por exibir a tela de atualizar setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirAtualizacaoSetorComercial")
	public String exibirAtualizacaoSetorComercial(SetorComercialVO setorComercialVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		
		SetorComercialImpl setorComercial = (SetorComercialImpl) controladorSetorComercial.criar();
		model.addAttribute("indicador", setorComercialVO.getHabilitado());
		Map<String, Object> filtroMunicipio = new HashMap<String, Object>();
		if (!super.isPostBack(request)) {
			try {
				setorComercial = (SetorComercialImpl) controladorSetorComercial.obter(setorComercialVO.getChavePrimaria());
				popularSetorComercialVO(setorComercialVO, setorComercial);
				retorno = "exibirAtualizacaoSetorComercial";
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
				retorno = TELA_EXIBIR_PESQUISA_SETOR_COMERCIAL;
			}
		}

		carregarCampos(setorComercialVO, model);
		filtroMunicipio.put(HABILITADO, Boolean.TRUE);
		model.addAttribute("listaMunicipio", controladorMunicipio.consultarMunicipios(filtroMunicipio));
		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);

		return retorno;
	}

	/**
	 * Carrega os dados do Setor Comercial para um VO
	 * que representa os campos do Setor Comercial.
	 * 
	 * @param setorComercialVO
	 * @param setorComercial
	 */
	private void popularSetorComercialVO(SetorComercialVO setorComercialVO, SetorComercialImpl setorComercial) {
		
		setorComercialVO.setChavePrimaria(setorComercial.getChavePrimaria());
		setorComercialVO.setVersao(setorComercial.getVersao());
		setorComercialVO.setCodigo(setorComercial.getCodigo());
		setorComercialVO.setDescricao(setorComercial.getDescricao());
		setorComercialVO.setGerenciaRegional(setorComercial.getMunicipio().getGerenciaRegional());
		setorComercialVO.setHabilitado(setorComercial.isHabilitado());
		setorComercialVO.setLocalidade(setorComercial.getLocalidade());
		setorComercialVO.setMunicipio(setorComercial.getMunicipio());
		setorComercialVO.setUnidadeFederativa(setorComercial.getMunicipio().getUnidadeFederacao());
	}

	/**
	 * Método responsável por atualizar o setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("atualizarSetorComercial")
	public String atualizarSetorComercial(SetorComercialVO setorComercialVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String retorno = null;
		SetorComercialImpl setorComercial = (SetorComercialImpl) controladorSetorComercial.criar();
		model.addAttribute(SETOR_COMERCIAL_FORM, setorComercialVO);

		try {
			popularSetorComercial(setorComercialVO, setorComercial);
			Map<String, Object> erros = setorComercial.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
				retorno = exibirAtualizacaoSetorComercial(setorComercialVO, result, request, model);
			} else {
				setorComercial.setDadosAuditoria(getDadosAuditoria(request));
				controladorSetorComercial.atualizar(setorComercial);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, SetorComercial.SETOR_COMERCIAL);
				retorno = pesquisarSetoresComerciais(setorComercialVO, result, request, model);
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
			retorno = exibirAtualizacaoSetorComercial(setorComercialVO, result, request, model);
		} catch (FormatoInvalidoException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = exibirAtualizacaoSetorComercial(setorComercialVO, result, request, model);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover setor comercial.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("removerSetorComercial")
	public String removerSetorComercial(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws NegocioException {

		try {
			controladorSetorComercial.validarRemoverSetoresComerciais(chavesPrimarias);
			controladorSetorComercial.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, SetorComercial.SETORES_COMERCIAIS);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarSetoresComerciais";
	}

	/**
	 * Carregar campos.
	 * 
	 * @param setorComercial
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCampos(SetorComercialVO setorComercialVO, Model model) throws GGASException {

		Collection<GerenciaRegional> listaGerenciaRegional = null;
		Collection<Localidade> listaLocalidade = null;
		Collection<UnidadeFederacao> listaUnidadeFederacao = null;
		Collection<Municipio> listaMunicipio = new ArrayList<Municipio>();
		Map<String, Object> filtroLocalidade = new HashMap<String, Object>();

		if (setorComercialVO.getGerenciaRegional() != null) {
			filtroLocalidade.put(GERENCIA_REGIONAL,
					setorComercialVO.getGerenciaRegional().getChavePrimaria());
		}
		filtroLocalidade.put(HABILITADO, Boolean.TRUE);
		listaLocalidade = controladorLocalidade.consultarLocalidades(filtroLocalidade);

		listaGerenciaRegional = controladorGerenciaRegional.consultarGerenciaRegional(null);
		listaUnidadeFederacao = controladorMunicipio.listarUnidadeFederacao();

		// ////Pega o município que foi
		// selecionado
		if (setorComercialVO.getUnidadeFederativa() != null) {

			Map<String, Object> filtroMunicipio = new HashMap<String, Object>();
			filtroMunicipio.put(ID_UNIDADE_FEDERACAO,
					setorComercialVO.getUnidadeFederativa().getChavePrimaria());
			filtroMunicipio.put(HABILITADO, Boolean.TRUE);
			listaMunicipio = controladorMunicipio.consultarMunicipios(filtroMunicipio);
		}

		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);
		model.addAttribute("listaLocalidade", listaLocalidade);
		model.addAttribute("listaUnidadeFederacao", listaUnidadeFederacao);
		model.addAttribute("listaMunicipio", listaMunicipio);
	}

	/**
	 * Popular setor comercial.
	 * 
	 * @param setorComercialVO
	 * @param setorComercial
	 * @throws GGASException
	 */
	private void popularSetorComercial(SetorComercialVO setorComercialVO, SetorComercialImpl setorComercial)
			throws GGASException {

		if (setorComercialVO.getChavePrimaria() != null && setorComercialVO.getChavePrimaria() > 0) {
			setorComercial.setChavePrimaria(setorComercialVO.getChavePrimaria());
		}

		if (setorComercialVO.getLocalidade() != null && setorComercialVO.getLocalidade().getChavePrimaria() > 0) {
			setorComercial.setLocalidade(
					(Localidade) controladorLocalidade.obter(setorComercialVO.getLocalidade().getChavePrimaria()));
		} else {
			setorComercial.setLocalidade(null);
		}

		if (StringUtils.isEmpty(setorComercialVO.getCodigo())) {
			setorComercial.setCodigo(null);
		} else {
			setorComercial.setCodigo(setorComercialVO.getCodigo());
		}

		if (setorComercialVO.getDescricao() == null) {
			setorComercial.setDescricao("");
		} else {
			Util.validarDominioCaracteres(SetorComercial.DESCRICAO, setorComercialVO.getDescricao(),
					Constantes.EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME_SETOR_COMERCIAL);
			setorComercial.setDescricao(setorComercialVO.getDescricao());
		}

		if (setorComercialVO.getMunicipio() != null && setorComercialVO.getMunicipio().getChavePrimaria() > 0) {
			setorComercial.setMunicipio(
					(Municipio) controladorMunicipio.obter(setorComercialVO.getMunicipio().getChavePrimaria()));
		} else {
			setorComercial.setMunicipio(null);
		}

		if (setorComercialVO.getHabilitado() != null) {
			setorComercial.setHabilitado(setorComercialVO.getHabilitado());
		}
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param setorComercial
	 * @return filtro
	 * @throws GGASException
	 */
	private Map<String, Object> obterFiltroForm(SetorComercialVO setorComercial) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (setorComercial.getLocalidade() != null) {
			filtro.put(ID_LOCALIDADE, setorComercial.getLocalidade().getChavePrimaria());
		}

		if (!StringUtils.isEmpty(setorComercial.getCodigo())) {
			filtro.put(CODIGO, setorComercial.getCodigo());
		}

		if (!StringUtils.isEmpty(setorComercial.getDescricao())) {
			Util.validarDominioCaracteres(SetorComercial.DESCRICAO, setorComercial.getDescricao(),
					Constantes.EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME_SETOR_COMERCIAL);
			filtro.put(DESCRICAO, setorComercial.getDescricao());
		}

		if ((setorComercial.getUnidadeFederativa() != null)
				&& setorComercial.getUnidadeFederativa().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_FEDERACAO, setorComercial.getUnidadeFederativa().getChavePrimaria());
		}

		if (setorComercial.getMunicipio() != null && setorComercial.getMunicipio().getChavePrimaria() > 0) {
			filtro.put(ID_MUNICIPIO, setorComercial.getMunicipio().getChavePrimaria());
		}

		if ((setorComercial.getGerenciaRegional() != null)
				&& setorComercial.getGerenciaRegional().getChavePrimaria() > 0) {
			filtro.put(GERENCIA_REGIONAL, setorComercial.getGerenciaRegional().getChavePrimaria());
		}

		if (setorComercial.getHabilitado() != null) {
			filtro.put(HABILITADO, setorComercial.getHabilitado());
		}

		return filtro;
	}
}
