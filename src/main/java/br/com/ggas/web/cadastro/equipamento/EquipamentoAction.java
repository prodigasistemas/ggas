package br.com.ggas.web.cadastro.equipamento;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ggas.cadastro.equipamento.ControladorEquipamento;
import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.equipamento.impl.EquipamentoImpl;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * 
 * Controlador responsável pelas telas relacionadas a entidade Equipamento
 */
@Controller
public class EquipamentoAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_EQUIPAMENTO = "exibirPesquisaEquipamento";

	private static final String EQUIPAMENTO = "equipamento";

	private static final Logger LOG = Logger.getLogger(EquipamentoAction.class);

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ID_SEGMENTO_RESIDENCIAL = "idSegmentoResidencial";

	private static final String TELA_EXIBIR_INSERIR_EQUIPAMENTO = "exibirInserirEquipamento";

	private static final String TELA_EXIBIR_ATUALIZAR_EQUIPAMENTO = "exibirAtualizarEquipamento";

	private static final String QUANTIDADE_DIGITO_INVALIDO = "QUANTIDADE_DIGITO_INVALIDO";

	private String retorno;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorEquipamento")
	private ControladorEquipamento controladorEquipamento;

	/**
	 * Método responsável por exibir a tela de pesquisa de equipamento.
	 * 
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_EQUIPAMENTO)
	public String exibirPesquisaEquipamento(Model model) throws GGASException {

		carregarCampos(model);

		return TELA_EXIBIR_PESQUISA_EQUIPAMENTO;
	}

	/**
	 * Método responsável pela pesquisa de equipamentos
	 * 
	 * @param equipamento
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarEquipamentos")
	public String pesquisarEquipamentos(@ModelAttribute("EquipamentoImpl") EquipamentoImpl equipamento,
			BindingResult result, @RequestParam(value = HABILITADO, required = false) String habilitado,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		carregarCampos(model);
		prepararFiltro(filtro, equipamento, habilitado);

		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(EQUIPAMENTO, equipamento);
		model.addAttribute("equipamentos", controladorEquipamento.consultarEquipamentos(filtro));

		return TELA_EXIBIR_PESQUISA_EQUIPAMENTO;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento Equipamento.
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhamentoEquipamento")
	public String exibirDetalhamentoEquipamento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model)
			throws GGASException {

		model.addAttribute(EQUIPAMENTO, controladorEquipamento.obterEquipamento(chavePrimaria));

		carregarCampos(model);

		return "exibirDetalhamentoEquipamento";
	}

	/**
	 * Carregar campos.
	 * 
	 * @param form
	 *            the form
	 * @param request
	 *            the request
	 * @throws GGASException
	 *             the GGAS exception
	 */

	private void carregarCampos(Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.consultarSegmento(filtro));
		model.addAttribute(ID_SEGMENTO_RESIDENCIAL, SegmentoImpl.RESIDENCIAL);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param dynaForm
	 *            the dyna form
	 */

	private void prepararFiltro(Map<String, Object> filtro, EquipamentoImpl equipamento, String habilitado) {

		if (equipamento.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, equipamento.getChavePrimaria());
		}
		if (equipamento.getDescricao() != null && !equipamento.getDescricao().isEmpty()) {
			filtro.put(DESCRICAO, equipamento.getDescricao());
		}
		if (equipamento.getSegmento() != null) {
			filtro.put(ID_SEGMENTO, equipamento.getSegmento().getChavePrimaria());
		}
		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}
	}

	/**
	 * Método responsável por exibir a tela de inserir equipamento
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            O formulário
	 * @param request
	 *            O objeto request
	 * @param response
	 *            O objeto response
	 * @param model
	 * @return ActionForward O retorno da ação
	 * @throws Exception
	 *             Caso ocorra algum erro
	 */

	@RequestMapping("exibirInserirEquipamento")
	public String exibirInserirEquipamento(Model model) throws GGASException {

		carregarCampos(model);

		return "exibirInserirEquipamento";
	}

	/**
	 * Método responsável por inserir o equipamento
	 * 
	 * @param equipamento
	 * @param result
	 * @param habilitado
	 * @param potenciaFixaAlta
	 * @param potenciaFixaMedia
	 * @param potenciaFixaBaixa
	 * @param request
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirEquipamento")
	public String inserirEquipamento(@ModelAttribute("EquipamentoImpl") EquipamentoImpl equipamento,
			BindingResult result, @RequestParam(value = "habilitado", required = false) String habilitado,
			String potenciaFixaAlta, String potenciaFixaMedia, String potenciaFixaBaixa, HttpServletRequest request,
			Model model) throws GGASException {

		retorno = TELA_EXIBIR_INSERIR_EQUIPAMENTO;

		model.addAttribute(EQUIPAMENTO, equipamento);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		Map<String, Object> erros = equipamento.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				equipamento.setHabilitado(true);
				equipamento.setDadosAuditoria(getDadosAuditoria(request));
				controladorEquipamento.inserir(equipamento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Equipamento.EQUIPAMENTO_CAMPO_STRING);
				retorno = pesquisarEquipamentos(equipamento, result, String.valueOf(equipamento.isHabilitado()),
						request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			} catch (DataException e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(QUANTIDADE_DIGITO_INVALIDO, e));
			}
		}

		return retorno;
	}

	private void tratarPotencias(Model model, Equipamento equipamento, String potenciaFixaAlta,
			String potenciaFixaMedia, String potenciaFixaBaixa) {

		try {

			if (!StringUtils.isEmpty(potenciaFixaAlta) || !StringUtils.isEmpty(potenciaFixaMedia)
					|| !StringUtils.isEmpty(potenciaFixaBaixa)) {

				if (!StringUtils.isEmpty(potenciaFixaAlta)) {
					equipamento.setPotenciaFixaAlta(
							Util.converterCampoStringParaValorBigDecimal(Equipamento.POTENCIA_FIXA_ALTA,
									potenciaFixaAlta, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
					if (potenciaFixaAlta.length() < 9) {
						model.addAttribute("potenciaFixaAlta", potenciaFixaAlta);
					}

				}

				if (!StringUtils.isEmpty(potenciaFixaMedia)) {
					equipamento.setPotenciaFixaMedia(
							Util.converterCampoStringParaValorBigDecimal(Equipamento.POTENCIA_FIXA_MEDIA,
									potenciaFixaMedia, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
					if (potenciaFixaMedia.length() < 9) {
						model.addAttribute("potenciaFixaMedia", potenciaFixaMedia);
					}

				}

				if (!StringUtils.isEmpty(potenciaFixaBaixa)) {
					equipamento.setPotenciaFixaBaixa(
							Util.converterCampoStringParaValorBigDecimal(Equipamento.POTENCIA_FIXA_BAIXA,
									potenciaFixaBaixa, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
					if (potenciaFixaBaixa.length() < 9) {
						model.addAttribute("potenciaFixaBaixa", potenciaFixaBaixa);
					}
				}

			} else {
				equipamento.setPotenciaFixaAlta(null);
				equipamento.setPotenciaFixaMedia(null);
				equipamento.setPotenciaFixaBaixa(null);
			}

		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
		}

	}

	/**
	 * Método responsável por remover equipamento.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "removerEquipamento", params = "chavesPrimarias")
	public String removerEquipamento(Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		retorno = TELA_EXIBIR_PESQUISA_EQUIPAMENTO;

		try {
			controladorEquipamento.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Equipamento.EQUIPAMENTO_CAMPO_STRING);
			retorno = pesquisarEquipamentos(new EquipamentoImpl(), null, null, request, model);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_REMOVER_EQUIPAMENTO, true));

		}

		return retorno;
	}

	/**
	 * Método responsável por atualizar o equipamento.
	 * 
	 * @param equipamento
	 * @param result
	 * @param habilitado
	 * @param potenciaFixaAlta
	 * @param potenciaFixaMedia
	 * @param potenciaFixaBaixa
	 * @param request
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("atualizarEquipamento")
	public String atualizarEquipamento(@ModelAttribute("EquipamentoImpl") EquipamentoImpl equipamento,
			BindingResult result, @RequestParam(value = "habilitado", required = false) String habilitado,
			String potenciaFixaAlta, String potenciaFixaMedia, String potenciaFixaBaixa, HttpServletRequest request,
			Model model) throws GGASException {

		retorno = TELA_EXIBIR_ATUALIZAR_EQUIPAMENTO;

		carregarCampos(model);
		model.addAttribute(EQUIPAMENTO, equipamento);
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		Map<String, Object> erros = equipamento.validarDados();

		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				equipamento.setHabilitado(true);
				equipamento.setDadosAuditoria(getDadosAuditoria(request));
				controladorEquipamento.atualizar(equipamento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Equipamento.EQUIPAMENTO_CAMPO_STRING);
				retorno = pesquisarEquipamentos(equipamento, result, String.valueOf(equipamento.isHabilitado()),
						request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			} catch (DataException e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(QUANTIDADE_DIGITO_INVALIDO, e));
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar equipamento.
	 * 
	 * @param mapping
	 *            the mapping
	 * @param form
	 *            O formulário
	 * @param request
	 *            O objeto request
	 * @param response
	 *            O objeto response
	 * @param chavePrimaria
	 * @param model
	 * @return ActionForward O retorno da ação
	 * @throws Exception
	 *             Caso ocorra algum erro
	 */

	@RequestMapping("exibirAtualizarEquipamento")
	public String exibirAtualizarEquipamento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model)
			throws GGASException {

		EquipamentoImpl equipamento = (EquipamentoImpl) controladorEquipamento.obterEquipamento(chavePrimaria);
		model.addAttribute(EQUIPAMENTO, equipamento);
		carregarCampos(model);

		return "exibirAtualizarEquipamento";
	}

	/**
	 * Método responsável por exibir a tela pesquisa de equipamento.
	 * 
	 * @param model
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaEquipamentoPopup")
	public String exibirPesquisaEquipamentoPopup(Model model) throws GGASException {

		carregarCampos(model);

		return "exibirPesquisaEquipamentoPopup";
	}

	/**
	 * 
	 * @param equipamento
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("pesquisarEquipamentoPopup")
	public String pesquisarEquipamentoPopup(@ModelAttribute("EquipamentoImpl") EquipamentoImpl equipamento,
			BindingResult result, @RequestParam(value = HABILITADO, required = false) String habilitado,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		prepararFiltro(filtro, equipamento, habilitado);

		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(EQUIPAMENTO, equipamento);
		model.addAttribute("equipamentos", controladorEquipamento.consultarEquipamentos(filtro));

		return "exibirPesquisaEquipamentoPopup";
	}

	/**
	 * Recupera o Equipamento com a chave primária informada.
	 * 
	 * @param idSegmento
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping(value = "/equipamento/opcoes", produces = "application/json")
	public @ResponseBody Map<String, String> carregarOpcoesPotenciaParaEquipamento(
			@RequestParam("idSegmento") Long idSegmento) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		Segmento segmento = controladorSegmento.obterSegmento(idSegmento);

		dados.put("indicadorEquipamentoPotenciaFixa", "false");

		if (segmento != null) {
			if (segmento.getindicadorEquipamentoPotenciaFixa() != null) {
				dados.put("indicadorEquipamentoPotenciaFixa",
						segmento.getindicadorEquipamentoPotenciaFixa().toString());
			}
		} else {
			dados = null;
		}

		return dados;
	}

}
