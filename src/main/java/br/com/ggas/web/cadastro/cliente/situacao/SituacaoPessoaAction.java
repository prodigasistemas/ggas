package br.com.ggas.web.cadastro.cliente.situacao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas operações referentes ao manter situação de pessoa.
 *
 */
@Controller
public class SituacaoPessoaAction extends GenericAction {

	private static final String FORWARD_EXIBIR_PESQUISAR_SITUACAO_PESSOA = "forward:/exibirPesquisarSituacaoPessoa";

	private static final String EXIBIR_ALTERACAO_SITUACAO_PESSOA = "exibirAlteracaoSituacaoPessoa";

	private static final String EXIBIR_INCLUSAO_SITUACAO_PESSOA = "exibirInclusaoSituacaoPessoa";

	private static final String SITUACAO_PESSOA = "situacaoPessoa";

	private static final String LISTA_SITUACAO_PESSOA = "listaSituacaoPessoa";

	private static final String EXIBIR_PESQUISAR_SITUACAO_PESSOA = "exibirPesquisarSituacaoPessoa";

	private static final String TITULO = "Situação da Pessoa";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a view referente a pesquisa de situação de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISAR_SITUACAO_PESSOA)
	public String exibirPesquisarSituacaoPessoa(Model model) {

		model.addAttribute(TabelaAuxiliar.ATRIBUTO_HABILITADO, "true");

		return EXIBIR_PESQUISAR_SITUACAO_PESSOA;
	}

	/**
	 * Método responsável por realizar uma pesquisa por situações de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param clienteSituacao - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param habilitado - {@link String}
	 * @return view - {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarSituacaoPessoa")
	public String pesquisarSituacaoPessoa(Model model, ClienteSituacaoImpl clienteSituacao, BindingResult bindingResult,
			HttpServletRequest request, @RequestParam(value = "habilitado", required = false) String habilitado) throws GGASException {

		model.addAttribute(TabelaAuxiliar.ATRIBUTO_CHAVES_PRIMARIAS, null);

		Map<String, Object> filtro = prepararFiltro(clienteSituacao, habilitado);

		Collection<TabelaAuxiliar> listaTabelaAuxiliar =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, ClienteSituacaoImpl.class.getName());

		model.addAttribute(LISTA_SITUACAO_PESSOA, listaTabelaAuxiliar);
		model.addAttribute(SITUACAO_PESSOA, clienteSituacao);
		model.addAttribute(TabelaAuxiliar.ATRIBUTO_HABILITADO, habilitado);
		
		return EXIBIR_PESQUISAR_SITUACAO_PESSOA;
	}

	private Map<String, Object> prepararFiltro(ClienteSituacaoImpl clienteSituacao, String habilitado) {
		Map<String, Object> filtro = new HashMap<String, Object>();

		String descricao = clienteSituacao.getDescricao();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			filtro.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao);
		}
		
		if (habilitado != null && !StringUtils.isEmpty(habilitado)) {
			filtro.put(TabelaAuxiliar.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado));
		}

		Long chavePrimaria = clienteSituacao.getChavePrimaria();
		if (chavePrimaria != null && chavePrimaria > 0) {
			filtro.put(TabelaAuxiliar.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);
		}
		return filtro;
	}

	private void limparFormulario(Model model) {
		model.addAttribute(TabelaAuxiliar.ATRIBUTO_HABILITADO, "true");
		model.addAttribute(new ClienteSituacaoImpl());
	}

	/**
	 * Método responsável por remover uma situação de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param clienteSituacao - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param habilitado - {@link String}
	 * @param chavesPrimarias {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("removerSituacaoPessoa")
	public String removerSituacaoPessoa(Model model, ClienteSituacaoImpl clienteSituacao, BindingResult bindingResult,
			HttpServletRequest request, @RequestParam(value = "habilitado", required = false) String habilitado,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) {

		String view = "forward:/pesquisarSituacaoPessoa";

		try {

			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, ClienteSituacaoImpl.class, dadosAuditoria);
			super.mensagemSucesso(model, Util.getRemoveMsgTabelaAuxiliar(ClienteSituacaoImpl.class), request, TITULO);
			view = this.pesquisarSituacaoPessoa(model, new ClienteSituacaoImpl(), 
					bindingResult, request, String.valueOf(Boolean.TRUE));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			super.mensagemErroParametrizado(model, request, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de inclusão de situação de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param clienteSituacao - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INCLUSAO_SITUACAO_PESSOA)
	public String exibirInclusaoSituacaoPessoa(Model model, HttpServletRequest request,
			ClienteSituacaoImpl clienteSituacao, BindingResult bindingResult) {

		if (!isPostBack(request)) {
			limparFormulario(model);
		}

		model.addAttribute(SITUACAO_PESSOA, clienteSituacao);
		
		saveToken(request);

		return EXIBIR_INCLUSAO_SITUACAO_PESSOA;
	}

	/**
	 * Método responsável por incluir uma situação de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param clienteSituacao - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirSituacaoPessoa")
	public String incluirSituacaoPessoa(Model model, HttpServletRequest request, ClienteSituacaoImpl clienteSituacao,
			BindingResult bindingResult) {
		String view = "forward:/exibirInclusaoSituacaoPessoa";

		try {

			getDadosAuditoria(request);

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(clienteSituacao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(clienteSituacao, TITULO);
			controladorTabelaAuxiliar.inserir(clienteSituacao);
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TITULO);
			view = this.pesquisarSituacaoPessoa(model, clienteSituacao, bindingResult, request, String.valueOf(Boolean.TRUE));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}
	
	/**
	 * Método responsável exibir a tela de alteração de situação da pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param clienteSituacaoParam - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ALTERACAO_SITUACAO_PESSOA)
	public String exibirAlteracaoSituacaoPessoa(Model model, HttpServletRequest request, ClienteSituacaoImpl clienteSituacaoParam,
			BindingResult  bindingResult) throws GGASException {

		ClienteSituacao clienteSituacao = clienteSituacaoParam;
		String view = "exibirAlteracaoSituacaoPessoa";
		try {
			
			if (!super.isPostBack(request)) {
				clienteSituacao = (ClienteSituacao) controladorTabelaAuxiliar.obter(clienteSituacao.getChavePrimaria(), 
						ClienteSituacaoImpl.class);	
			}
			model.addAttribute(SITUACAO_PESSOA, clienteSituacao);
			saveToken(request);
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_PESQUISAR_SITUACAO_PESSOA;
		}
		
		return view;
	}
	
	/**
	 * Método responsável por alterar uma situação de pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param clienteSituacao - {@link ClienteSituacaoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarSituacaoPessoa")
	public String alterarSituacaoPessoa(Model model, HttpServletRequest request, ClienteSituacaoImpl clienteSituacao,
			BindingResult bindingResult) {

		String view = "forward:/exibirAlteracaoSituacaoPessoa";

		try {
			getDadosAuditoria(request);

			controladorTabelaAuxiliar.verificarTamanhoDescricao(clienteSituacao.getDescricao(),
					clienteSituacao.getClass().getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(clienteSituacao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(clienteSituacao, TITULO);
			controladorTabelaAuxiliar.atualizar(clienteSituacao, clienteSituacao.getClass());
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TITULO);
			view = this.pesquisarSituacaoPessoa(model, clienteSituacao, bindingResult, request,
					String.valueOf(clienteSituacao.isHabilitado()));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}
	
	/**
	 * Método responsável por exibir o detalhamento de uma situação de um cliente.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param chavePrimaria - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoSituacaoPessoa")
	public String exibirDetalhamentoSituacaoPessoa(Model model, HttpServletRequest request,
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		String view = "exibirDetalhamentoSituacaoPessoa";

		try {
			ClienteSituacao situacaoPessoa = (ClienteSituacao) controladorTabelaAuxiliar.obter(chavePrimaria, ClienteSituacaoImpl.class);
			model.addAttribute(SITUACAO_PESSOA, situacaoPessoa);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_PESQUISAR_SITUACAO_PESSOA;
		}

		return view;
	}
}
