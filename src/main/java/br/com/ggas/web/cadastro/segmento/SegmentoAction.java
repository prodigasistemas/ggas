/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.segmento;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RamoAtividadeAmostragemPCS;
import br.com.ggas.cadastro.imovel.RamoAtividadeAuxVO;
import br.com.ggas.cadastro.imovel.RamoAtividadeIntervaloPCS;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SegmentoAmostragemPCS;
import br.com.ggas.cadastro.imovel.SegmentoIntervaloPCS;
import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.cadastro.imovel.impl.RamoAtividadeImpl;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.contabil.ControladorContaContabil;
import br.com.ggas.contabil.impl.ContaContabilImpl;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.faturamento.tributo.impl.RamoAtividadeSubstituicaoTributariaImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas a Segmento
 *
 */
@Controller
public class SegmentoAction extends GenericAction {

	private static final String OPERACAO_EXECUTADA = "operacaoExecutada";

	private static final String OPERACAO_RAMO_ATIVIDADE = "operacaoRamoAtividade";

	private static final String NOME_CONTA = "nomeConta";

	private static final String VO_AUXILIAR_RAMO_ATIVIDADE = "ramoAtividadeAuxVO";

	private static final String RAMO_ATIVIDADE = "ramoAtividade";

	private static final String SEGMENTO_FORM = "segmentoForm";

	private static final String VALOR = "Valor";

	private static final String LISTA_SEGMENTO_INTERVALO_PCS_SELECIONADOS = "listaSegmentoIntervaloPCSSelecionados";

	private static final String LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS = "listaSegmentoAmostragemPCSSelecionados";

	private static final String NUMERO_CONTA = "numeroConta";

	private static final String DE_0_A_9 = "[^0-9]";

	private static final String LISTA_TIPO_SEGMENTO = "listaTipoSegmento";

	private static final String DETALHAR_SEGMENTO = "detalharSegmento";

	private static final String ALTERAR = "alterar";

	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";

	private static final String MASCARA_NUMERO_CONTA2 = "MASCARA_NUMERO_CONTA";

	private static final String INCLUIR = "incluir";

	private static final String HABILITADO = "habilitado";

	private static final String ID_TIPO_SEGMENTO = "idTipoSegmento";

	private static final String DESCRICAO = "descricao";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String REQUEST_LISTA_RAMO_ATIVIDADE = "listaRamoAtividades";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String CONTA_CONTABIL = "contaContabil";

	private static final String MASCARA_NUMERO_CONTA = "mascaraNumeroConta";

	private static final String LOCAL_AMOSTRAGEM_PCS = "Local de amostragem do PCS";

	private static final String TAMANHO_REDUCAO = "tamanhoReducao";

	private static final String LISTA_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA = "listaRamoAtividadeSubstituicaoTributaria";

	private static final String LISTA_RAMO_ATIVIDADE_AMOSTRAGEM_PCS = "listaRamoAtividadeAmostragemPCSSelecionados";

	private static final String LISTA_RAMO_ATIVIDADE_INTERVALO_PCS = "listaRamoAtividadeIntervaloPCSSelecionados";

	private static final String C_TIPO_SUBS_TRIBUTARIA = "Tipo de Subst. Tributaria";

	private static final String SUBS_TRIBUT_MVA = "subsTributMVA";

	private static final String SUBS_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL = "subsTributPrecoMedioPondAConsFinal";

	private static final String REQUEST_LISTA_SUBS_TRIBUTARIA = "listaSubsTributaria";

	private static final String OPERACAO = "operacao";

	private static final String LISTA_TIPO_CONSUMO_FATURAMENTO = "listaTipoConsumoFaturamento";

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier("controladorRamoAtividade")
	private ControladorRamoAtividade controladorRamoAtividade;

	@Autowired
	@Qualifier("controladorRamoAtividadeSubstituicaoTributaria")
	private ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstituicaoTributaria;

	@Autowired
	@Qualifier("controladorRota")
	private ControladorRota controladorRota;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorHistoricoConsumo")
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	@Qualifier("controladorContaContabil")
	private ControladorContaContabil controladorContaContabil;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Método responsável por exibir a tela de pesquisa de segmento.
	 * 
	 * @param segmento
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaSegmento")
	public String exibirPesquisaSegmento(SegmentoImpl segmento, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}
		limparListaRamoAtividade(request);
		model.addAttribute(LISTA_TIPO_SEGMENTO, controladorSegmento.listarTodosTipoSegmento());
		model.addAttribute(DESCRICAO, null);
		limparSessionRamoAtividade(request);

		return "exibirPesquisaSegmento";
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de segmentos. no sistema.
	 *
	 * @param segmento
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarSegmentos")
	public String pesquisarSegmentos(SegmentoImpl segmento, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, HttpServletRequest request, Model model)
			throws GGASException {

		limparListaRamoAtividade(request);
		Map<String, Object> filtro = obterFiltroForm(segmento, habilitado);
		Collection<Segmento> listaSegmento = controladorSegmento.consultarSegmento(filtro);
		model.addAttribute(LISTA_TIPO_SEGMENTO, controladorSegmento.listarTodosTipoSegmento());
		model.addAttribute("listaSegmento", listaSegmento);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(SEGMENTO_FORM, segmento);

		return exibirPesquisaSegmento(segmento, result, request, model);
	}

	/**
	 * Método responsável por exibir a tela de inserir segmento.
	 * 
	 * @param segmento
	 * @param result
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param resultRa
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirInsercaoSegmento")
	public String exibirInsercaoSegmento(SegmentoImpl segmento, BindingResult result, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRa, Model model) throws GGASException {

		this.carregarCamposSegmento(ramoAtividadeAuxVO.getListaSegmentoAmostragemPCSSelecionados(),
				ramoAtividadeAuxVO.getListaSegmentoIntervaloPCSSelecionados(), ramoAtividadeAuxVO.getOperacao(), request, segmento, model);
		model.addAttribute(MASCARA_NUMERO_CONTA, controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2));
		model.addAttribute(SEGMENTO_FORM, segmento);

		return "exibirInsercaoSegmento";
	}

	/**
	 * Método responsável por inserir o segmento no sistema.
	 * 
	 * @param segmento
	 * @param results
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param resultRa
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("inserirSegmento")
	public String inserirSegmento(SegmentoImpl segmento, BindingResult results, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRa, Model model) throws GGASException {

		String retorno = null;

		segmento.setVolumeMedioEstouro(ramoAtividadeAuxVO.getVolumeMedioEstouroRamo());
		segmento.setTamanhoReducao(ramoAtividadeAuxVO.getTamanhoReducaoRamo());

		Map<String, Object> erros = segmento.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			retorno = exibirInsercaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
		} else {
			try {
				popularSegmento(ramoAtividadeAuxVO.getListaSegmentoAmostragemPCSSelecionados(),
						ramoAtividadeAuxVO.getListaSegmentoIntervaloPCSSelecionados(), segmento, request, ramoAtividadeAuxVO.getOperacao());
				controladorSegmento.inserirSegmentoRamoAtividade(segmento, getDadosAuditoria(request));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Constantes.SEGMENTO_ROTULO);
				retorno = pesquisarSegmentos(segmento, resultRa, segmento.isHabilitado(), request, model);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
				retorno = exibirInsercaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			} catch (ConcorrenciaException e) {
				mensagemErroParametrizado(model, request, e);
				retorno = exibirInsercaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			} catch (ConstraintViolationException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
				retorno = exibirInsercaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável pela transferência de dados entre telas
	 * 
	 * @param segmento
	 * @param results
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param resultRa
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("retornarTela")
	public String retornarTela(SegmentoImpl segmento, BindingResult results, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRa, Model model) throws GGASException {
		String retorno = null;

		Collection<RamoAtividade> listaRamoAtividade = getListaRamoAtividade(request);
		if (listaRamoAtividade != null) {
			request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividade);
		}
		String contaContabilNumero = "";
		segmento.setVolumeMedioEstouro(ramoAtividadeAuxVO.getVolumeMedioEstouroRamo());
		segmento.setTamanhoReducao(ramoAtividadeAuxVO.getTamanhoReducaoRamo());
		if (segmento.getContaContabil() != null && !"".equals(segmento.getContaContabil().getNumeroConta())) {
			contaContabilNumero = Util.removerCaracteresEspeciais(segmento.getContaContabil().getNumeroConta());
		}
		if (ramoAtividadeAuxVO.isRetornou()) {
			model.addAttribute(SEGMENTO_FORM, segmento);
			model.addAttribute(OPERACAO, ramoAtividadeAuxVO.getOperacao());
			model.addAttribute("numeroContaContabil", contaContabilNumero);
			model.addAttribute("listaSegmentoAmostragemPCSSelecionados", ramoAtividadeAuxVO.getListaSegmentoAmostragemPCSSelecionados());
			model.addAttribute("listaSegmentoIntervaloPCSSelecionados", ramoAtividadeAuxVO.getListaSegmentoIntervaloPCSSelecionados());

			if (INCLUIR.equals(ramoAtividadeAuxVO.getOperacao())) {
				limparSessionSubsTritutaria(request);
				retorno = exibirInsercaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			} else {
				limparSessionSubsTritutaria(request);
				retorno = exibirAtualizacaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por inserir o segmento no sistema.
	 * 
	 * @param segmento
	 * @param result
	 * @param request
	 * @param ramoAtividade
	 * @param results
	 * @param ramoAtividadeAuxVO
	 * @param resultList
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarRamoAtividadeInserir")
	public String adicionarRamoAtividadeInserir(SegmentoImpl segmento, BindingResult result, HttpServletRequest request,
			RamoAtividadeImpl ramoAtividade, BindingResult results, RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultList,
			Model model) throws GGASException {

		String retorno = null;

		Collection<RamoAtividade> listaRamoAtividade = getListaRamoAtividade(request);
		if (ramoAtividadeAuxVO != null) {
			popularRamoAtividadeAmostragemPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), request);
			popularRamoAtividadeIntervaloPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), request);
		}
		Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias =
				(Collection<RamoAtividadeSubstituicaoTributaria>) request.getSession().getAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);

		if (ramoAtividadeAuxVO != null && ramoAtividadeAuxVO.getChavePrimaria() > 0) {

			ramoAtividade.setChavePrimaria(ramoAtividadeAuxVO.getChavePrimaria());
			ramoAtividade.setVersao(-1);

		} else if (ramoAtividade != null) {

			int chaveTemp = listaRamoAtividade.size() + 1;
			if (!listaRamoAtividade.isEmpty()) {
				boolean achou = true;
				while (achou) {
					for (RamoAtividade ra : listaRamoAtividade) {
						if (ra.getChavePrimaria() == chaveTemp) {
							chaveTemp++;
							achou = true;
							break;
						} else {
							achou = false;
						}
					}
				}
			}

			ramoAtividade.setChavePrimaria(chaveTemp);
			ramoAtividade.setVersao(-1);

		}

		Iterator<RamoAtividade> iterator = listaRamoAtividade.iterator();

		while (iterator.hasNext()) {
			if (ramoAtividade != null && iterator.next().getChavePrimaria() == ramoAtividade.getChavePrimaria()) {
				iterator.remove();
			}
		}
		if (ramoAtividade != null) {
			ramoAtividade.setRamoAtividadeSubstituicaoTributaria(listaSubstituicaoTributarias);
		}
		listaRamoAtividade.add(ramoAtividade);

		request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividade);

		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		model.addAttribute(SEGMENTO_FORM, segmento);
		model.addAttribute(VO_AUXILIAR_RAMO_ATIVIDADE, ramoAtividadeAuxVO);

		if (ramoAtividadeAuxVO != null) {

			if (ramoAtividadeAuxVO.getOperacao() != null && INCLUIR.equalsIgnoreCase(ramoAtividadeAuxVO.getOperacao())) {
				limparSessionSubsTritutaria(request);
				retorno = exibirInsercaoSegmento(segmento, resultList, request, ramoAtividadeAuxVO, resultList, model);
			} else if (ramoAtividadeAuxVO.getOperacao() != null && ALTERAR.equalsIgnoreCase(ramoAtividadeAuxVO.getOperacao())) {
				limparSessionSubsTritutaria(request);
				retorno = exibirAtualizacaoSegmento(segmento, resultList, request, ramoAtividadeAuxVO, resultList, model);
			}
		}

		return retorno;
	}

	/**
	 * Gets the lista ramo atividade.
	 * 
	 * @param request
	 * @return listaRamoAtividades
	 */
	@SuppressWarnings("unchecked")
	private Collection<RamoAtividade> getListaRamoAtividade(HttpServletRequest request) {

		Collection<RamoAtividade> listaRamoAtividades =
				(Collection<RamoAtividade>) request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE);
		if (listaRamoAtividades == null) {
			listaRamoAtividades = new HashSet<RamoAtividade>();
			request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);
		}

		return listaRamoAtividades;

	}

	/**
	 * Alterar ramo atividade inserir
	 * 
	 * @param segmento
	 * @param result
	 * @param request
	 * @param ramoAtividade
	 * @param results
	 * @param ramoAtividadeAuxVO
	 * @param resultRa
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("alterarRamoAtividadeInserir")
	public String alterarRamoAtividadeInserir(SegmentoImpl segmento, BindingResult result, HttpServletRequest request,
			RamoAtividadeImpl ramoAtividade, BindingResult results, RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRa,
			Model model) throws GGASException {
		String retorno = null;

		Collection<RamoAtividade> listaRamoAtividades = getListaRamoAtividade(request);
		Iterator<RamoAtividade> iterator = listaRamoAtividades.iterator();

		while (iterator.hasNext()) {
			RamoAtividade ramoAtiv = iterator.next();
			if (ramoAtiv.getChavePrimaria() == ramoAtividadeAuxVO.getIdRamoAtividade()) {
				this.popularRamoAtividade(ramoAtiv, ramoAtividade, request, ramoAtividadeAuxVO, model);
			}
		}

		request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);

		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		model.addAttribute(SEGMENTO_FORM, segmento);
		model.addAttribute(VO_AUXILIAR_RAMO_ATIVIDADE, ramoAtividadeAuxVO);

		if (ramoAtividadeAuxVO.getOperacao() != null && INCLUIR.equalsIgnoreCase(ramoAtividadeAuxVO.getOperacao())) {
			retorno = exibirInsercaoSegmento(segmento, resultRa, request, ramoAtividadeAuxVO, resultRa, model);
		} else if (ramoAtividadeAuxVO.getOperacao() != null && ALTERAR.equalsIgnoreCase(ramoAtividadeAuxVO.getOperacao())) {
			retorno = exibirAtualizacaoSegmento(segmento, resultRa, request, ramoAtividadeAuxVO, resultRa, model);
		}

		return retorno;
	}

	/**
	 * Limpar lista ramo atividade.
	 * 
	 * @param request
	 * @throws GGASException
	 */
	private void limparListaRamoAtividade(HttpServletRequest request) throws GGASException {

		HttpSession session = request.getSession(false);
		session.removeAttribute(REQUEST_LISTA_RAMO_ATIVIDADE);
	}

	/**
	 * Exibir dados ramo atividade.
	 * 
	 * @param operacao
	 * @param request
	 * @param segmento
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void exibirDadosRamoAtividade(String operacao, HttpServletRequest request, Segmento segmento) throws GGASException {

		Collection<RamoAtividade> listaRamoAtividades = new HashSet<RamoAtividade>();

		if (request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE) != null) {
			listaRamoAtividades = (Collection<RamoAtividade>) request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE);
			request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);
		}

		if (segmento.getChavePrimaria() > 0 && !segmento.getRamosAtividades().isEmpty()) {
			Collection<RamoAtividade> lista = controladorRamoAtividade.listarRamoAtividadePorSegmento(segmento.getChavePrimaria());
			if (request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE) != null) {

				listaRamoAtividades = (Collection<RamoAtividade>) request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE);
			}
			listaRamoAtividades.addAll(lista);
		}

		if (DETALHAR_SEGMENTO.equals(operacao)) {
			listaRamoAtividades = segmento.getRamosAtividades();
		}

		request.setAttribute("listaRamoAtividades", listaRamoAtividades);
		request.setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);

		if (DETALHAR_SEGMENTO.equals(operacao)) {
			preencherPropriedadesPendentes(listaRamoAtividades);
		}

		request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);
	}

	/**
	 * Preenche os ramos de atividade com os dados de substituição tributária e amostragem/intervalo PCS para que
	 * SegmentoAction.popularSegmento() execute corretamente.
	 * 
	 * @param listaRamoAtividades
	 * @throws GGASException
	 */
	private void preencherPropriedadesPendentes(Collection<RamoAtividade> listaRamoAtividades) throws GGASException {
		// FIXME o ideal é usar o carregamento de propriedades do hibernate
		for (RamoAtividade raat : listaRamoAtividades) {
			Collection<RamoAtividadeSubstituicaoTributaria> subsTribPorRamoAtividade =
					controladorRamoAtividadeSubstituicaoTributaria.listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(raat);
			raat.setRamoAtividadeSubstituicaoTributaria(subsTribPorRamoAtividade);

			Collection<RamoAtividadeAmostragemPCS> amostragensPCSPorRamoAtividade =
					controladorRamoAtividade.listarRamoAtividadeAmostragemPCSPorRamoAtividade(raat);
			raat.setRamoAtividadeAmostragemPCS(amostragensPCSPorRamoAtividade);

			Collection<RamoAtividadeIntervaloPCS> intervalosPCSPorRamoAtividade =
					controladorRamoAtividade.listarRamoAtividadeIntervaloPCSPorRamoAtividade(raat);
			raat.setRamoAtividadeIntervaloPCS(intervalosPCSPorRamoAtividade);
		}
	}

	/**
	 * Popular ramo atividade.
	 * 
	 * @param ramoAtividadeAtual
	 * @param ramoAtividadeTmp
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param model
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void popularRamoAtividade(RamoAtividade ramoAtividadeAtual, RamoAtividade ramoAtividadeTmp, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, Model model) throws GGASException {

		try {

			String descRamoAtividade = ramoAtividadeTmp.getDescricao();
			Periodicidade periodicidadeRamoAtividade = ramoAtividadeTmp.getPeriodicidade();

			if (!StringUtils.isEmpty(descRamoAtividade)) {
				ramoAtividadeAtual.setDescricao(descRamoAtividade);
			} else {
				ramoAtividadeAtual.setDescricao(null);
			}
			if (periodicidadeRamoAtividade != null) {
				ramoAtividadeAtual.setPeriodicidade(periodicidadeRamoAtividade);
			} else {
				ramoAtividadeAtual.setPeriodicidade(null);
			}
			ramoAtividadeAtual.setTipoConsumoFaturamento(ramoAtividadeTmp.getTipoConsumoFaturamento());
			ramoAtividadeAtual.setUltimaAlteracao(Calendar.getInstance().getTime());
			ramoAtividadeAtual.setDadosAuditoria(getDadosAuditoria(request));

			controladorRamoAtividade.validarDadosRamoAtividade(ramoAtividadeTmp);

			popularRamoAtividadeAmostragemPCS(ramoAtividadeAtual, ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), request);
			popularRamoAtividadeIntervaloPCS(ramoAtividadeAtual, ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), request);

			Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias =
					(Collection<RamoAtividadeSubstituicaoTributaria>) request.getSession().getAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);

			ramoAtividadeAtual.setRamoAtividadeSubstituicaoTributaria(listaSubstituicaoTributarias);
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}
	}

	/**
	 * Remover ramo atividade inserir.
	 * 
	 * @param ramoAtividadeAuxVO
	 * @param resultRamo
	 * @param request
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerRamoAtividade")
	public String removerRamoAtividade(RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRamo, HttpServletRequest request)
			throws GGASException {

		Collection<RamoAtividade> listaRamoAtividades =
				(Collection<RamoAtividade>) request.getSession().getAttribute(REQUEST_LISTA_RAMO_ATIVIDADE);

		Iterator<RamoAtividade> iterator = listaRamoAtividades.iterator();

		while (iterator.hasNext()) {
			if (ramoAtividadeAuxVO.getIdRamoAtividade().equals(iterator.next().getChavePrimaria())) {
				iterator.remove();
			}
		}

		request.getSession().setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);
		request.setAttribute(REQUEST_LISTA_RAMO_ATIVIDADE, listaRamoAtividades);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificaOperacao(ramoAtividadeAuxVO.getOperacao());
	}

	/**
	 * Método criado para verificação de fluxo entre telas
	 * 
	 * @param operacao
	 * @return String
	 */
	private String verificaOperacao(String operacao) {
		String retorno = null;

		if (INCLUIR.equalsIgnoreCase(operacao)) {
			retorno = "forward:exibirInsercaoSegmento";
		} else if (ALTERAR.equalsIgnoreCase(operacao)) {
			retorno = "forward:exibirAtualizacaoSegmento";
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar segmento.
	 * 
	 * @param segmentoTmp
	 * @param result
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param resultRamo
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirAtualizacaoSegmento")
	public String exibirAtualizacaoSegmento(SegmentoImpl segmentoTmp, BindingResult result, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRamo, Model model) throws GGASException {

		SegmentoImpl segmento = segmentoTmp;
		if (ramoAtividadeAuxVO.isExecutado()) {
			segmento = (SegmentoImpl) controladorSegmento.obter(ramoAtividadeAuxVO.getChavePrimaria(), "segmentoAmostragemPCSs",
					"segmentoIntervaloPCSs", "ramosAtividades");
		}

		this.carregarCamposSegmento(ramoAtividadeAuxVO.getListaSegmentoAmostragemPCSSelecionados(),
				ramoAtividadeAuxVO.getListaSegmentoIntervaloPCSSelecionados(), ramoAtividadeAuxVO.getOperacao(), request, segmento, model);

		if (segmento.getContaContabil() == null) {
			model.addAttribute(CONTA_CONTABIL, "");
		} else {
			model.addAttribute(CONTA_CONTABIL,
					Util.formatarMascara((String) controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2),
							segmento.getContaContabil().getNumeroConta()));
		}
		model.addAttribute(SEGMENTO_FORM, segmento);
		model.addAttribute(VO_AUXILIAR_RAMO_ATIVIDADE, ramoAtividadeAuxVO);
		request.setAttribute(MASCARA_NUMERO_CONTA, controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2));

		return "exibirAtualizacaoSegmento";
	}

	/**
	 * Método responsável por atualizar o segmento.
	 * 
	 * @param segmento
	 * @param results
	 * @param request
	 * @param ramoAtividadeAuxVO
	 * @param resultRa
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("atualizarSegmento")
	public String atualizarSegmento(SegmentoImpl segmento, BindingResult results, HttpServletRequest request,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultRa, Model model) throws GGASException {
		String retorno = null;

		Map<String, Object> erros = segmento.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			retorno = exibirAtualizacaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
		} else {
			try {
				popularSegmento(ramoAtividadeAuxVO.getListaSegmentoAmostragemPCSSelecionados(),
						ramoAtividadeAuxVO.getListaSegmentoIntervaloPCSSelecionados(), segmento, request, ramoAtividadeAuxVO.getOperacao());

				segmento.setDadosAuditoria(getDadosAuditoria(request));

				if (segmento.getContaContabil() != null && !"".equals(segmento.getContaContabil().getNumeroConta())
						&& segmento.getContaContabil().getNumeroConta().length() > 0
						&& !"_._._._.__.___".equals(segmento.getContaContabil().getNumeroConta())) {
					Map<String, Object> filtro2 = new HashMap<String, Object>();
					filtro2.put(NUMERO_CONTA, segmento.getContaContabil().getNumeroConta().replaceAll(DE_0_A_9, ""));
					ContaContabil contaCont = controladorContaContabil.obterContaContabil(filtro2);
					if (contaCont == null) {
						throw new NegocioException(Constantes.ERRO_SEGMENTO_CONTA_CONTABIL_INFORMADA, true);
					} else {
						segmento.setContaContabil(contaCont);
					}
				}

				controladorSegmento.atualizarSegmento(segmento, getDadosAuditoria(request));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Constantes.SEGMENTO_ROTULO);
				retorno = pesquisarSegmentos(segmento, results, segmento.isHabilitado(), request, model);
			} catch (GGASException e) {
				mensagemErro(model, request, new NegocioException(e));
				retorno = exibirAtualizacaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
			} catch (ConstraintViolationException | DataIntegrityViolationException e) {
				retorno = exibirAtualizacaoSegmento(segmento, results, request, ramoAtividadeAuxVO, resultRa, model);
				mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por remover segmento.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerSegmento")
	public String removerSegmento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			controladorSegmento.removerSegmentos(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Constantes.SEGMENTO_ROTULO);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (ConstraintViolationException | DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarSegmentos";
	}

	/**
	 * Carregar campos segmento.
	 * 
	 * @param listaAmostragem
	 * @param listaIntervalo
	 * @param operacao
	 * @param request
	 * @param segmento
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCamposSegmento(Long[] listaAmostragem, Long[] listaIntervalo, String operacao, HttpServletRequest request,
			Segmento segmento, Model model) throws GGASException {

		Collection<TipoSegmento> listaTipoSegmento = controladorSegmento.listarTodosTipoSegmento();
		Collection<Periodicidade> listaPeriodicidades = controladorRota.listarPeriodicidades();
		Collection<EntidadeConteudo> listaTipoConsumoFaturamento = controladorEntidadeConteudo.obterListaTipoConsumoFaturamento();

		request.setAttribute(LISTA_TIPO_SEGMENTO, listaTipoSegmento);
		request.setAttribute("listaPeriodicidades", listaPeriodicidades);
		request.setAttribute(LISTA_TIPO_CONSUMO_FATURAMENTO, listaTipoConsumoFaturamento);

		if (segmento == null || segmento.getChavePrimaria() <= 0) {
			popularSegmentoAmostragemPCS(listaAmostragem, null, request, operacao);
			popularSegmentoIntervaloPCS(listaIntervalo, null, request, operacao);
		} else {
			popularSegmentoAmostragemPCS(listaAmostragem, segmento, request, operacao);
			popularSegmentoIntervaloPCS(listaIntervalo, segmento, request, operacao);
		}

		if (segmento != null && segmento.getVersao() >= 0) {
			model.addAttribute(HABILITADO, String.valueOf(segmento.isHabilitado()));
		}

		if (segmento != null && operacao != null) {
			this.exibirDadosRamoAtividade(operacao, request, segmento);
		}
	}

	/**
	 * Exibir detalhamento segmento.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhamentoSegmento")
	public String exibirDetalhamentoSegmento(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		Segmento segmento =
				(Segmento) controladorSegmento.obter(chavePrimaria, "segmentoAmostragemPCSs", "segmentoIntervaloPCSs", "ramosAtividades");

		Collection<SegmentoAmostragemPCS> listaSegmentoAmostragemPCSSelecionados =
				controladorSegmento.listarSegmentoAmostragemPCS(chavePrimaria);
		Collection<SegmentoIntervaloPCS> listaSegmentoIntervaloPCSSelecionados =
				controladorSegmento.listarSegmentoIntervaloPCS(chavePrimaria);

		if (listaSegmentoIntervaloPCSSelecionados != null && !listaSegmentoIntervaloPCSSelecionados.isEmpty()) {
			long tamRed = 0;
			for (SegmentoIntervaloPCS segmentoIntervaloPCS : segmento.getSegmentoIntervaloPCSs()) {
				if (segmentoIntervaloPCS.getTamanho() != null) {
					tamRed += segmentoIntervaloPCS.getTamanho();
				}
			}
			if (tamRed > 0) {
				request.setAttribute(TAMANHO_REDUCAO, tamRed);
			}
		}

		if (segmento.getContaContabil() != null && segmento.getContaContabil().getNumeroConta() != null) {
			segmento.getContaContabil().setNumeroConta(
					Util.formatarMascara((String) controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2),
							segmento.getContaContabil().getNumeroConta()));
		}

		request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoAmostragemPCSSelecionados);
		request.setAttribute(LISTA_SEGMENTO_INTERVALO_PCS_SELECIONADOS, listaSegmentoIntervaloPCSSelecionados);
		model.addAttribute(SEGMENTO_FORM, segmento);

		return "exibirDetalhamentoSegmento";
	}

	/**
	 * Popular segmento.
	 * 
	 * @param listaIdAmostragemPCSSelecionados
	 * @param listaIdIntervaloPCSSelecionados
	 * @param segmento
	 * @param request
	 * @param operacao
	 * @throws GGASException
	 */
	private void popularSegmento(Long[] listaIdAmostragemPCSSelecionados, Long[] listaIdIntervaloPCSSelecionados, Segmento segmento,
			HttpServletRequest request, String operacao) throws GGASException {

		if (listaIdAmostragemPCSSelecionados == null || listaIdAmostragemPCSSelecionados.length == 0) {
			Collection<SegmentoAmostragemPCS> listaSegmentoAmostragemPCSSelecionados = new HashSet<SegmentoAmostragemPCS>();
			segmento.setSegmentoAmostragemPCSs(listaSegmentoAmostragemPCSSelecionados);
		} else {
			popularSegmentoAmostragemPCS(listaIdAmostragemPCSSelecionados, segmento, request, operacao);
		}

		if (listaIdIntervaloPCSSelecionados == null || listaIdIntervaloPCSSelecionados.length == 0) {
			Collection<SegmentoIntervaloPCS> listaSegmentoIntervaloPCSSelecionados = new HashSet<SegmentoIntervaloPCS>();
			segmento.setSegmentoIntervaloPCSs(listaSegmentoIntervaloPCSSelecionados);
		} else {
			popularSegmentoIntervaloPCS(listaIdIntervaloPCSSelecionados, segmento, request, operacao);
		}

		Collection<RamoAtividade> listaRamoAtividade = new HashSet<RamoAtividade>();

		Collection<RamoAtividade> listaRamoAtividadeAux = getListaRamoAtividade(request);
		Iterator<RamoAtividade> iterator = listaRamoAtividadeAux.iterator();

		while (iterator.hasNext()) {

			RamoAtividade ra = (RamoAtividade) controladorRamoAtividade.criar();

			RamoAtividade raAux = iterator.next();

			Collection<RamoAtividadeAmostragemPCS> listaRamoAtividadeAmostragemPCSSelecionados = new HashSet<RamoAtividadeAmostragemPCS>();
			Iterator<RamoAtividadeAmostragemPCS> itRAA = raAux.getRamoAtividadeAmostragemPCS().iterator();

			while (itRAA.hasNext()) {
				RamoAtividadeAmostragemPCS raaAux = itRAA.next();
				RamoAtividadeAmostragemPCS ramoAtividadeAmostragemPCS = raaAux;
				ramoAtividadeAmostragemPCS.setLocalAmostragemPCS(raaAux.getLocalAmostragemPCS());
				ramoAtividadeAmostragemPCS.setRamoAtividade(ra);
				ramoAtividadeAmostragemPCS.setPrioridade(raaAux.getPrioridade());
				ramoAtividadeAmostragemPCS.setHabilitado(Boolean.TRUE);
				ramoAtividadeAmostragemPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaRamoAtividadeAmostragemPCSSelecionados.add(ramoAtividadeAmostragemPCS);
			}

			ra.setRamoAtividadeAmostragemPCS(listaRamoAtividadeAmostragemPCSSelecionados);

			Collection<RamoAtividadeIntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionados = new HashSet<RamoAtividadeIntervaloPCS>();
			Iterator<RamoAtividadeIntervaloPCS> itRAI = raAux.getRamoAtividadeIntervaloPCS().iterator();

			while (itRAI.hasNext()) {
				RamoAtividadeIntervaloPCS raiAux = itRAI.next();
				RamoAtividadeIntervaloPCS ramoAtividadeIntervaloPCS = raiAux;
				ramoAtividadeIntervaloPCS.setIntervaloPCS(raiAux.getIntervaloPCS());
				ramoAtividadeIntervaloPCS.setRamoAtividade(ra);
				ramoAtividadeIntervaloPCS.setPrioridade(raiAux.getPrioridade());
				ramoAtividadeIntervaloPCS.setHabilitado(Boolean.TRUE);
				ramoAtividadeIntervaloPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaRamoAtividadeIntervaloPCSSelecionados.add(ramoAtividadeIntervaloPCS);
			}

			ra.setRamoAtividadeIntervaloPCS(listaRamoAtividadeIntervaloPCSSelecionados);

			Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
					new HashSet<RamoAtividadeSubstituicaoTributaria>();
			Iterator<RamoAtividadeSubstituicaoTributaria> itRAST = raAux.getRamoAtividadeSubstituicaoTributaria().iterator();

			while (itRAST.hasNext()) {
				RamoAtividadeSubstituicaoTributaria rastAux = itRAST.next();
				RamoAtividadeSubstituicaoTributaria substTributaria =
						(RamoAtividadeSubstituicaoTributaria) controladorRamoAtividadeSubstituicaoTributaria.criar();
				substTributaria.setTipoSubstituicao(rastAux.getTipoSubstituicao());
				substTributaria.setDataInicioVigencia(rastAux.getDataInicioVigencia());
				substTributaria.setDataFimVigencia(rastAux.getDataFimVigencia());
				substTributaria.setValorSubstituto(rastAux.getValorSubstituto());
				substTributaria.setPercentualSubstituto(rastAux.getPercentualSubstituto());
				substTributaria.setRamoAtividade(ra);
				substTributaria.setHabilitado(Boolean.TRUE);
				substTributaria.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaRamoAtividadeSubstituicaoTributaria.add(substTributaria);
			}

			ra.setRamoAtividadeSubstituicaoTributaria(listaRamoAtividadeSubstituicaoTributaria);

			if (raAux.getVersao() >= 0) {
				ra.setChavePrimaria(raAux.getChavePrimaria());
			}
			ra.setSegmento(segmento);
			ra.setDescricao(raAux.getDescricao());
			ra.setPeriodicidade(raAux.getPeriodicidade());
			ra.setTipoConsumoFaturamento(raAux.getTipoConsumoFaturamento());
			ra.setUltimaAlteracao(Calendar.getInstance().getTime());
			ra.setHabilitado(Boolean.TRUE);

			listaRamoAtividade.add(ra);
		}

		segmento.setRamosAtividades(listaRamoAtividade);
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param segmento
	 * @param habilitado
	 * @return Map<String, Object>
	 */
	private Map<String, Object> obterFiltroForm(SegmentoImpl segmento, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (segmento.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, segmento.getChavePrimaria());
		}

		if (!StringUtils.isEmpty(segmento.getDescricao())) {
			filtro.put(DESCRICAO, segmento.getDescricao());
		}

		if (segmento.getTipoSegmento() != null) {
			filtro.put(ID_TIPO_SEGMENTO, segmento.getTipoSegmento().getChavePrimaria());
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		if (segmento.getContaContabil() != null) {
			filtro.put(NOME_CONTA, segmento.getContaContabil().getNomeConta());
		}

		if (segmento.getContaContabil() != null) {
			filtro.put(NUMERO_CONTA, segmento.getContaContabil().getNumeroConta());
		}

		return filtro;
	}

	/**
	 * Exibir popup pesquisa conta contabil segmento.
	 * 
	 * @param contaCont
	 * @param result
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirPopupPesquisaContaContabilSegmento")
	public String exibirPopupPesquisaContaContabilSegmento(ContaContabilImpl contaCont, BindingResult result, Model model)
			throws GGASException {

		model.addAttribute(MASCARA_NUMERO_CONTA, controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2));
		if (contaCont.getNomeConta() != null) {
			model.addAttribute(NOME_CONTA, contaCont.getNomeConta());
		}

		if (contaCont.getNumeroConta() != null) {
			model.addAttribute(NUMERO_CONTA, Util.removerCaracteresEspeciais(contaCont.getNumeroConta()));
		}

		return "popupPesquisaContaContabilSegmento";
	}

	/**
	 * Pesquisa conta contabil segmento.
	 * 
	 * @param contaCont
	 * @param result
	 * @param model
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("pesquisaContaContabilSegmento")
	public String pesquisaContaContabilSegmento(ContaContabilImpl contaCont, BindingResult result, Model model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(NUMERO_CONTA, Util.removerCaracteresEspeciais(contaCont.getNumeroConta()));
		filtro.put(NOME_CONTA, contaCont.getNomeConta());
		String mascara = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(MASCARA_NUMERO_CONTA2);

		Collection<ContaContabil> listaContaContabil = controladorContaContabil.listarContaContabil(filtro);
		for (ContaContabil contaContabil : listaContaContabil) {
			contaContabil.setNumeroConta(Util.formatarMascaraTruncada(mascara, contaContabil.getNumeroConta()));

		}
		model.addAttribute("contaContabil", contaCont);
		model.addAttribute("listaContaContabil", listaContaContabil);
		return "popupPesquisaContaContabilSegmento";
	}

	/**
	 * Popup detalhar ramo atividade.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("popupDetalharRamoAtividade")
	public String popupDetalharRamoAtividade(@RequestParam("idRamoAtividade") Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		RamoAtividade ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(chavePrimaria);

		Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
				controladorRamoAtividadeSubstituicaoTributaria.listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(ramoAtividade);
		Collection<RamoAtividadeAmostragemPCS> listaRamoAtividadeAmostragemPCS =
				controladorRamoAtividade.listarRamoAtividadeAmostragemPCSPorRamoAtividade(ramoAtividade);
		Collection<RamoAtividadeIntervaloPCS> listaRamoAtividadeIntervaloPCS =
				controladorRamoAtividade.listarRamoAtividadeIntervaloPCSPorRamoAtividade(ramoAtividade);

		if (listaRamoAtividadeIntervaloPCS != null && !listaRamoAtividadeIntervaloPCS.isEmpty()) {
			long tamRed = 0;
			for (RamoAtividadeIntervaloPCS ramoAtividadeIntervaloPCS : listaRamoAtividadeIntervaloPCS) {
				if (ramoAtividadeIntervaloPCS.getTamanho() != null) {
					tamRed += ramoAtividadeIntervaloPCS.getTamanho();
				}
			}
			if (tamRed > 0) {
				request.setAttribute(TAMANHO_REDUCAO, tamRed);
			}
		}

		request.setAttribute(LISTA_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
		request.setAttribute(LISTA_RAMO_ATIVIDADE_AMOSTRAGEM_PCS, listaRamoAtividadeAmostragemPCS);
		request.setAttribute(LISTA_RAMO_ATIVIDADE_INTERVALO_PCS, listaRamoAtividadeIntervaloPCS);
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		return "popupDetalharRamoAtividade";

	}

	/**
	 * Popular segmento amostragem pcs.
	 * 
	 * @param listaIdAmostragemPCSSelecionados
	 * @param segmento
	 * @param request
	 * @param operacao
	 * @throws GGASException
	 */
	private void popularSegmentoAmostragemPCS(Long[] listaIdAmostragemPCSSelecionados, Segmento segmento, HttpServletRequest request,
			String operacao) throws GGASException {

		Collection<EntidadeConteudo> listaSegmentoAmostragemPCSDisponiveis =
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(LOCAL_AMOSTRAGEM_PCS);

		if (segmento != null && listaIdAmostragemPCSSelecionados != null && listaIdAmostragemPCSSelecionados.length > 0
				&& !DETALHAR_SEGMENTO.equals(operacao)) {

			Collection<SegmentoAmostragemPCS> listaSegmentoAmostragemPCSSelecionados = new HashSet<SegmentoAmostragemPCS>();

			int prioridade = 1;

			for (Long idAmostragemPCS : listaIdAmostragemPCSSelecionados) {
				SegmentoAmostragemPCS segmentoAmostragemPCS = controladorSegmento.criarSegmentoAmostragemPCS();
				EntidadeConteudo amostragemPCS = controladorEntidadeConteudo.obterEntidadeConteudo(idAmostragemPCS);
				segmentoAmostragemPCS.setLocalAmostragemPCS(amostragemPCS);
				segmentoAmostragemPCS.setSegmento(segmento);
				segmentoAmostragemPCS.setPrioridade(prioridade);
				++prioridade;
				segmentoAmostragemPCS.setHabilitado(Boolean.TRUE);
				segmentoAmostragemPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaSegmentoAmostragemPCSSelecionados.add(segmentoAmostragemPCS);
				listaSegmentoAmostragemPCSDisponiveis.remove(amostragemPCS);
			}

			segmento.setSegmentoAmostragemPCSs(listaSegmentoAmostragemPCSSelecionados);
			request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoAmostragemPCSSelecionados);

		}

		if (segmento != null) {

			Collection<EntidadeConteudo> listaSegmentoAmostragemPCSSelecionados = new HashSet<EntidadeConteudo>();

			if (listaIdAmostragemPCSSelecionados != null) {

				for (Long idAmostragemPCS : listaIdAmostragemPCSSelecionados) {
					EntidadeConteudo amostragemPCS = controladorEntidadeConteudo.obter(idAmostragemPCS);
					listaSegmentoAmostragemPCSSelecionados.add(amostragemPCS);
					listaSegmentoAmostragemPCSDisponiveis.remove(amostragemPCS);
				}
			}

			request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoAmostragemPCSSelecionados);

		}

		if (segmento != null && (listaIdAmostragemPCSSelecionados == null || listaIdAmostragemPCSSelecionados.length == 0)) {

			Collection<SegmentoAmostragemPCS> listaSegmentoIntervaloPCSSelecionadosSegmento = segmento.getSegmentoAmostragemPCSs();
			Collection<EntidadeConteudo> listaSegmentoIntervaloPCSSelecionados = new HashSet<EntidadeConteudo>();

			if (listaSegmentoIntervaloPCSSelecionadosSegmento != null && !listaSegmentoIntervaloPCSSelecionadosSegmento.isEmpty()) {
				for (SegmentoAmostragemPCS segAmostragemPCS : listaSegmentoIntervaloPCSSelecionadosSegmento) {
					EntidadeConteudo amostragemPCS =
							controladorEntidadeConteudo.obterEntidadeConteudo(segAmostragemPCS.getLocalAmostragemPCS().getChavePrimaria());
					segAmostragemPCS.setLocalAmostragemPCS(amostragemPCS);
					listaSegmentoIntervaloPCSSelecionados.add(amostragemPCS);
					listaSegmentoAmostragemPCSDisponiveis.remove(amostragemPCS);
				}
			}

			request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoIntervaloPCSSelecionados);

		}

		if (segmento == null) {

			Collection<EntidadeConteudo> listaSegmentoAmostragemPCSSelecionados = new ArrayList<EntidadeConteudo>();
			if (listaIdAmostragemPCSSelecionados != null) {
				for (Long idAmostragemPCS : listaIdAmostragemPCSSelecionados) {
					EntidadeConteudo amostragemPCS = controladorEntidadeConteudo.obterEntidadeConteudo(idAmostragemPCS);
					listaSegmentoAmostragemPCSSelecionados.add(amostragemPCS);
					listaSegmentoAmostragemPCSDisponiveis.remove(amostragemPCS);
				}
			}

			request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoAmostragemPCSSelecionados);

		}

		if (segmento != null && DETALHAR_SEGMENTO.equals(operacao)) {
			Collection<SegmentoAmostragemPCS> listaSegmentoAmostragemPCSSelecionados = segmento.getSegmentoAmostragemPCSs();
			request.setAttribute(LISTA_SEGMENTO_AMOSTRAGEM_PCS_SELECIONADOS, listaSegmentoAmostragemPCSSelecionados);
		}

		request.setAttribute("listaSegmentoAmostragemPCSDisponiveis", listaSegmentoAmostragemPCSDisponiveis);
	}

	/**
	 * Popular segmento intervalo pcs.
	 * 
	 * @param listaIdIntervaloPCSSelecionados
	 * @param segmento
	 * @param request
	 * @param operacao
	 * @throws GGASException
	 */
	private void popularSegmentoIntervaloPCS(Long[] listaIdIntervaloPCSSelecionados, Segmento segmento, HttpServletRequest request,
			String operacao) throws GGASException {

		Collection<IntervaloPCS> listaSegmentoIntervaloPCSDisponiveis = controladorHistoricoConsumo.listarIntervaloPCS();

		if (segmento != null && segmento.getSegmentoIntervaloPCSs() != null && !segmento.getSegmentoIntervaloPCSs().isEmpty()) {
			long tamRed = 0;
			for (SegmentoIntervaloPCS segmentoIntervaloPCS : segmento.getSegmentoIntervaloPCSs()) {
				if (segmentoIntervaloPCS.getTamanho() != null) {
					tamRed += segmentoIntervaloPCS.getTamanho();
				}
			}
			if (tamRed > 0) {
				request.setAttribute(TAMANHO_REDUCAO, tamRed);
			}
		}

		if (segmento != null && listaIdIntervaloPCSSelecionados != null && listaIdIntervaloPCSSelecionados.length > 0
				&& !DETALHAR_SEGMENTO.equals(operacao)) {

			Collection<SegmentoIntervaloPCS> listaSegmentoIntervaloPCSSelecionados = new HashSet<SegmentoIntervaloPCS>();
			Collection<IntervaloPCS> listaIntervaloPCSSelecionados = new HashSet<IntervaloPCS>();

			int prioridade = 1;

			for (Long idIntervaloPCS : listaIdIntervaloPCSSelecionados) {
				SegmentoIntervaloPCS segmentoIntervaloPCS = controladorSegmento.criarSegmentoIntervaloPCS();
				IntervaloPCS intervaloPCS = controladorHistoricoConsumo.obterIntervaloPCS(idIntervaloPCS);
				listaIntervaloPCSSelecionados.add(intervaloPCS);
				segmentoIntervaloPCS.setIntervaloPCS(intervaloPCS);
				segmentoIntervaloPCS.setSegmento(segmento);
				segmentoIntervaloPCS.setPrioridade(prioridade);
				prioridade = prioridade + 1;
				segmentoIntervaloPCS.setHabilitado(Boolean.TRUE);
				segmentoIntervaloPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaSegmentoIntervaloPCSSelecionados.add(segmentoIntervaloPCS);
				listaSegmentoIntervaloPCSDisponiveis.remove(intervaloPCS);
			}

			segmento.setSegmentoIntervaloPCSs(listaSegmentoIntervaloPCSSelecionados);
			request.setAttribute(LISTA_SEGMENTO_INTERVALO_PCS_SELECIONADOS, listaIntervaloPCSSelecionados);
		}

		if (segmento != null && (listaIdIntervaloPCSSelecionados == null || listaIdIntervaloPCSSelecionados.length == 0)) {

			Collection<SegmentoIntervaloPCS> listaSegmentoIntervaloPCSSelecionadosSegmento = segmento.getSegmentoIntervaloPCSs();
			Collection<IntervaloPCS> listaSegmentoIntervaloPCSSelecionados = new HashSet<IntervaloPCS>();

			if (listaSegmentoIntervaloPCSSelecionadosSegmento != null && !listaSegmentoIntervaloPCSSelecionadosSegmento.isEmpty()) {
				for (SegmentoIntervaloPCS segIntervaloPCS : listaSegmentoIntervaloPCSSelecionadosSegmento) {
					IntervaloPCS intervaloPCS =
							controladorHistoricoConsumo.obterIntervaloPCS(segIntervaloPCS.getIntervaloPCS().getChavePrimaria());
					segIntervaloPCS.setIntervaloPCS(intervaloPCS);
					listaSegmentoIntervaloPCSSelecionados.add(intervaloPCS);
					listaSegmentoIntervaloPCSDisponiveis.remove(intervaloPCS);
				}
			}

			request.setAttribute(LISTA_SEGMENTO_INTERVALO_PCS_SELECIONADOS, listaSegmentoIntervaloPCSSelecionados);

		}

		if (segmento == null) {

			Collection<IntervaloPCS> listaSegmentoIntervaloPCSSelecionados = new HashSet<IntervaloPCS>();

			if (listaIdIntervaloPCSSelecionados != null) {
				for (Long idAmostragemPCS : listaIdIntervaloPCSSelecionados) {
					IntervaloPCS intervaloPCS = controladorHistoricoConsumo.obterIntervaloPCS(idAmostragemPCS);
					listaSegmentoIntervaloPCSSelecionados.add(intervaloPCS);
					listaSegmentoIntervaloPCSDisponiveis.remove(intervaloPCS);
				}
			}

			request.setAttribute(LISTA_SEGMENTO_INTERVALO_PCS_SELECIONADOS, listaSegmentoIntervaloPCSSelecionados);
		}

		request.setAttribute("listaSegmentoIntervaloPCSDisponiveis", listaSegmentoIntervaloPCSDisponiveis);
	}

	/**
	 * Popup adicionar ramo atividade.
	 * 
	 * @param segmento
	 * @param resultSeg
	 * @param ramoAtividade
	 * @param result
	 * @param substTributaria
	 * @param results
	 * @param ramoAtividadeAuxVO
	 * @param resultList
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("popupAdicionarRamoAtividade")
	public String popupAdicionarRamoAtividade(SegmentoImpl segmento, BindingResult resultSeg, RamoAtividadeImpl ramoAtividade,
			BindingResult result, RamoAtividadeSubstituicaoTributariaImpl substTributaria, BindingResult results,
			RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultList, HttpServletRequest request, Model model) throws GGASException {

		Collection<RamoAtividade> listaRamoAtividade = getListaRamoAtividade(request);
		String operacaoRamoAtividade = (String) request.getSession().getAttribute(OPERACAO_RAMO_ATIVIDADE);

		model.addAttribute(SEGMENTO_FORM, segmento);
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		model.addAttribute(OPERACAO, ramoAtividadeAuxVO.getOperacao());

		if (ramoAtividade != null && ramoAtividade.getChavePrimaria() > 0) {

			Iterator<RamoAtividade> iterator = listaRamoAtividade.iterator();

			while (iterator.hasNext()) {
				RamoAtividade ramoAtiviTmp = iterator.next();
				if (ramoAtiviTmp.getChavePrimaria() == ramoAtividade.getChavePrimaria()) {
					break;
				}
			}

		}

		if (operacaoRamoAtividade == null || operacaoRamoAtividade.isEmpty()) {
			// se a tela for iniciada/atualizada sem nenhuma operação a lista de
			// substituição tributaria é zerada
			Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
					new HashSet<RamoAtividadeSubstituicaoTributaria>();
			request.getSession().setAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
		}

		if (ramoAtividade != null && (operacaoRamoAtividade == null || operacaoRamoAtividade.isEmpty())) {
			// carrega os dados do ramo atividade
			carregarPopupRamoAtividade(request, ramoAtividade, false, model);
			popularRamoAtividadeAmostragemPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), request);
			popularRamoAtividadeIntervaloPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), request);
		} else {
			carregarPopupRamoAtividade(request, ramoAtividade, true, model);
			carregarSubsTributaria(request, ramoAtividade, ramoAtividadeAuxVO.getOperacaoSubstituicao(), substTributaria,
					ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), model);
		}

		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		request.getSession().setAttribute(OPERACAO_RAMO_ATIVIDADE, "adicionarRamoAtividade");

		return "popupAdicionarRamoAtividade";
	}

	/**
	 * Carregar popup ramo atividade.
	 * 
	 * @param request
	 * @param ramoAtividade
	 * @param reloadRastList
	 * @param model
	 * @throws GGASException
	 */
	private void carregarPopupRamoAtividade(HttpServletRequest request, RamoAtividade ramoAtividade, boolean reloadRastList, Model model)
			throws GGASException {

		model.addAttribute("listaPeriodicidades", controladorRota.listarPeriodicidades());
		model.addAttribute("listaTipoSubsTributaria",
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(C_TIPO_SUBS_TRIBUTARIA));

		Long constanteMVA = obterValorConstante(Constantes.C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO);
		request.getSession().setAttribute(SUBS_TRIBUT_MVA, constanteMVA);
		Long constantePMPF = obterValorConstante(Constantes.C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL);
		request.getSession().setAttribute(SUBS_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL, constantePMPF);

		if (ramoAtividade != null && ramoAtividade.getChavePrimaria() > 0 && (ramoAtividade.getDescricao() != null)) {

			model.addAttribute("ramoAtividadeForm", ramoAtividade);

			if (reloadRastList) {
				if (ramoAtividade.getRamoAtividadeSubstituicaoTributaria() != null
						&& !ramoAtividade.getRamoAtividadeSubstituicaoTributaria().isEmpty()) {
					Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
							ramoAtividade.getRamoAtividadeSubstituicaoTributaria();
					adicionarRASTSessao(request.getSession(), listaRamoAtividadeSubstituicaoTributaria);
					request.setAttribute(LISTA_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
					request.getSession().setAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);

				} else if (ramoAtividade.getVersao() >= 0) {
					Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
							controladorRamoAtividadeSubstituicaoTributaria
									.listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(ramoAtividade);
					adicionarRASTSessao(request.getSession(), listaRamoAtividadeSubstituicaoTributaria);
					request.setAttribute(LISTA_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
					request.getSession().setAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
				}
			}

		}

	}

	/**
	 * Método responsável por obter valor de constante
	 * 
	 * @param codigoConstante
	 * @return Long
	 * @throws GGASException
	 */
	private Long obterValorConstante(String codigoConstante) throws GGASException {
		String valorConstante = this.controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(codigoConstante);
		if (valorConstante == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA, codigoConstante);
		} else {
			return Long.valueOf(valorConstante);
		}
	}

	/**
	 * Método responsável por adicionar um RamoAtividadeSubstituicaoTributaria a sessão
	 * 
	 * @param httpSession
	 * @param listaRamoAtividadeSubstituicaoTributaria
	 */
	@SuppressWarnings("unchecked")
	private void adicionarRASTSessao(HttpSession httpSession,
			Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria) {

		Collection<RamoAtividadeSubstituicaoTributaria> rastSessao =
				(Collection<RamoAtividadeSubstituicaoTributaria>) httpSession.getAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);
		if (rastSessao != null) {
			listaRamoAtividadeSubstituicaoTributaria.addAll(CollectionUtils.subtract(rastSessao, listaRamoAtividadeSubstituicaoTributaria));
		}
	}

	/**
	 * Intervalo anos data.
	 * 
	 * @return String
	 * @throws GGASException
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String intervaloAnosData = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		intervaloAnosData = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return intervaloAnosData;
	}

	/**
	 * Carregar subs tributaria.
	 * 
	 * @param request
	 * @param ramoAtividade
	 * @param operacao
	 * @param substTributaria
	 * @param listaAmostragem
	 * @param listaIntervalo
	 * @param model
	 * @throws GGASException
	 */
	// Seta a session pra grid da aba Faturamento do popup Ramo Ativdade
	private void carregarSubsTributaria(HttpServletRequest request, RamoAtividade ramoAtividade, String operacaoSubstituicao,
			RamoAtividadeSubstituicaoTributaria substTributaria, Long[] listaAmostragem, Long[] listaIntervalo, Model model)
			throws GGASException {

		Long idSubstituicaoTributaria = substTributaria.getChavePrimaria();
		model.addAttribute("substTributaria", substTributaria);
		@SuppressWarnings("unchecked")
		Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias =
				(Collection<RamoAtividadeSubstituicaoTributaria>) request.getSession().getAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);

		if (listaSubstituicaoTributarias == null) {
			listaSubstituicaoTributarias = new HashSet<RamoAtividadeSubstituicaoTributaria>();
		}

		try {
			if ("removerSubsTributaria".equals(operacaoSubstituicao)) {

				removerSubstituicaoTributaria(model, idSubstituicaoTributaria, listaSubstituicaoTributarias);

			} else if ("alterarSubsTributaria".equals(operacaoSubstituicao)) {

				alterarSubstituicaoTributaria(request, substTributaria, model, idSubstituicaoTributaria, listaSubstituicaoTributarias);

			} else if ("adicionarSubsTributaria".equals(operacaoSubstituicao)) {

				adicionarSubstituicaoTributaria(request, substTributaria, model, listaSubstituicaoTributarias);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		popularRamoAtividadeAmostragemPCS(ramoAtividade, listaAmostragem, request);
		popularRamoAtividadeIntervaloPCS(ramoAtividade, listaIntervalo, request);

		request.getSession().setAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA, listaSubstituicaoTributarias);

	}

	/**
	 * Método responsável por remover uma substituição tributária
	 * 
	 * @param model
	 * @param idSubstituicaoTributaria
	 * @param listaSubstituicaoTributarias
	 */
	private void removerSubstituicaoTributaria(Model model, Long idSubstituicaoTributaria,
			Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias) {
		Iterator<RamoAtividadeSubstituicaoTributaria> iterator = listaSubstituicaoTributarias.iterator();

		while (iterator.hasNext()) {
			RamoAtividadeSubstituicaoTributaria ramoAtividadeSubsTribu = iterator.next();
			if (idSubstituicaoTributaria.equals(ramoAtividadeSubsTribu.getChavePrimaria())) {
				iterator.remove();
			}
		}
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		model.addAttribute(OPERACAO_EXECUTADA, true);
	}

	/**
	 * Método responsável por alterar uma substiruição tributária
	 * 
	 * @param request
	 * @param substTributaria
	 * @param model
	 * @param idSubstituicaoTributaria
	 * @param listaSubstituicaoTributarias
	 * @throws GGASException
	 */
	private void alterarSubstituicaoTributaria(HttpServletRequest request, RamoAtividadeSubstituicaoTributaria substTributaria, Model model,
			Long idSubstituicaoTributaria, Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias)
			throws GGASException {

		validarDadosRamoAtividadeSubstituicaoTributaria(substTributaria, request, model);

		Iterator<RamoAtividadeSubstituicaoTributaria> iterator = listaSubstituicaoTributarias.iterator();

		while (iterator.hasNext()) {
			if (idSubstituicaoTributaria.equals(iterator.next().getChavePrimaria())) {
				iterator.remove();
			}
		}

		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		substTributaria.setChavePrimaria(idSubstituicaoTributaria);
		substTributaria.setVersao(-1);
		listaSubstituicaoTributarias.add(substTributaria);
		model.addAttribute(OPERACAO_EXECUTADA, true);
	}

	/**
	 * Método responsável por adicionar uma substituição tributária
	 * 
	 * @param request
	 * @param substTributaria
	 * @param model
	 * @param listaSubstituicaoTributarias
	 * @throws GGASException
	 */
	private void adicionarSubstituicaoTributaria(HttpServletRequest request, RamoAtividadeSubstituicaoTributaria substTributaria,
			Model model, Collection<RamoAtividadeSubstituicaoTributaria> listaSubstituicaoTributarias) throws GGASException {

		validarDadosRamoAtividadeSubstituicaoTributaria(substTributaria, request, model);

		int chaveTemp = listaSubstituicaoTributarias.size() + 1;
		if (!listaSubstituicaoTributarias.isEmpty()) {
			boolean achou = true;
			while (achou) {
				for (RamoAtividadeSubstituicaoTributaria rast : listaSubstituicaoTributarias) {
					if (rast.getChavePrimaria() == chaveTemp) {
						chaveTemp++;
						achou = true;
						break;
					} else {
						achou = false;
					}
				}
			}
		}

		substTributaria.setChavePrimaria(chaveTemp);
		substTributaria.setVersao(-1);
		listaSubstituicaoTributarias.add(substTributaria);
		model.addAttribute(OPERACAO_EXECUTADA, true);
	}

	/**
	 * Limpar session subs tritutaria.
	 * 
	 * @param request
	 */
	// Limpa a lista(session) de Substituuição Tributária do popup de Ramo
	// Atividade.
	@RequestMapping("limparSessionSubsTritutaria")
	public void limparSessionSubsTritutaria(HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		session.removeAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);
	}

	/**
	 * Popular ramo atividade amostragem pcs.
	 * 
	 * @param ramoAtividade
	 * @param listaIdAmostragemPCSSelecionados
	 * @param request
	 * @throws GGASException
	 */
	private void popularRamoAtividadeAmostragemPCS(RamoAtividade ramoAtividade, Long[] listaIdAmostragemPCSSelecionados,
			HttpServletRequest request) throws GGASException {

		Collection<EntidadeConteudo> listaRamoAtividadeAmostragemPCSDisponiveis =
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(LOCAL_AMOSTRAGEM_PCS);

		if (listaIdAmostragemPCSSelecionados != null && listaIdAmostragemPCSSelecionados.length > 0) {

			Collection<RamoAtividadeAmostragemPCS> listaRamoAtividadeAmostragemPCSSelecionados = new HashSet<RamoAtividadeAmostragemPCS>();
			Collection<EntidadeConteudo> listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo = new HashSet<EntidadeConteudo>();

			int prioridade = 1;

			Map<Long, RamoAtividadeAmostragemPCS> ramoAtividadeAmostragemPCSBanco =
					mapearObjetoPCSporIdENCO(ramoAtividade.getRamoAtividadeAmostragemPCS());

			for (Long idAmostragemPCS : listaIdAmostragemPCSSelecionados) {
				RamoAtividadeAmostragemPCS ramoAtividadeAmostragemPCS =
						buscaObjetoPCS(ramoAtividadeAmostragemPCSBanco, idAmostragemPCS, RamoAtividadeAmostragemPCS.class);
				EntidadeConteudo amostragemPCS = controladorEntidadeConteudo.obterEntidadeConteudo(idAmostragemPCS);
				ramoAtividadeAmostragemPCS.setLocalAmostragemPCS(amostragemPCS);
				ramoAtividadeAmostragemPCS.setRamoAtividade(ramoAtividade);
				ramoAtividadeAmostragemPCS.setPrioridade(prioridade);
				++prioridade;
				ramoAtividadeAmostragemPCS.setHabilitado(Boolean.TRUE);
				ramoAtividadeAmostragemPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaRamoAtividadeAmostragemPCSSelecionados.add(ramoAtividadeAmostragemPCS);

				listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo.add(amostragemPCS);
				listaRamoAtividadeAmostragemPCSDisponiveis.remove(amostragemPCS);
			}

			ramoAtividade.setRamoAtividadeAmostragemPCS(listaRamoAtividadeAmostragemPCSSelecionados);
			request.setAttribute(LISTA_RAMO_ATIVIDADE_AMOSTRAGEM_PCS, listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo);

		} else if (ramoAtividade != null && (listaIdAmostragemPCSSelecionados == null || listaIdAmostragemPCSSelecionados.length == 0)) {

			Collection<RamoAtividadeAmostragemPCS> listaRamoAtividadeAmostragemPCS = new HashSet<RamoAtividadeAmostragemPCS>();

			if (ramoAtividade.getVersao() >= 0) {
				listaRamoAtividadeAmostragemPCS = controladorRamoAtividade.listarRamoAtividadeAmostragemPCSPorRamoAtividade(ramoAtividade);
			} else {
				listaRamoAtividadeAmostragemPCS = ramoAtividade.getRamoAtividadeAmostragemPCS();
			}

			Collection<RamoAtividadeAmostragemPCS> listaRamoAtividadeAmostragemPCSSelecionados = new HashSet<RamoAtividadeAmostragemPCS>();
			Collection<EntidadeConteudo> listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo = new HashSet<EntidadeConteudo>();

			for (RamoAtividadeAmostragemPCS ramoAtividadeAmostragemPCS : listaRamoAtividadeAmostragemPCS) {

				EntidadeConteudo amostragemPCS = controladorEntidadeConteudo
						.obterEntidadeConteudo(ramoAtividadeAmostragemPCS.getLocalAmostragemPCS().getChavePrimaria());
				ramoAtividadeAmostragemPCS.setLocalAmostragemPCS(amostragemPCS);
				listaRamoAtividadeAmostragemPCSSelecionados.add(ramoAtividadeAmostragemPCS);

				listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo.add(amostragemPCS);
				listaRamoAtividadeAmostragemPCSDisponiveis.remove(amostragemPCS);

			}

			ramoAtividade.setRamoAtividadeAmostragemPCS(listaRamoAtividadeAmostragemPCSSelecionados);
			request.setAttribute(LISTA_RAMO_ATIVIDADE_AMOSTRAGEM_PCS, listaRamoAtividadeAmostragemPCSSelecionadosEntidadeConteudo);

		}

		if (ramoAtividade == null) {

			Collection<EntidadeConteudo> listaRamoAtividadeAmostragemPCSSelecionados = new HashSet<EntidadeConteudo>();

			for (Long idAmostragemPCS : listaIdAmostragemPCSSelecionados) {

				EntidadeConteudo amostragemPCS = controladorEntidadeConteudo.obterEntidadeConteudo(idAmostragemPCS);
				listaRamoAtividadeAmostragemPCSSelecionados.add(amostragemPCS);
				listaRamoAtividadeAmostragemPCSDisponiveis.remove(amostragemPCS);

			}

			request.setAttribute(LISTA_RAMO_ATIVIDADE_AMOSTRAGEM_PCS, listaRamoAtividadeAmostragemPCSSelecionados);

		}

		request.setAttribute("listaRamoAtividadeAmostragemPCSDisponiveis", listaRamoAtividadeAmostragemPCSDisponiveis);

	}

	/**
	 * Busca na map um RamoAtividadeAmostragemPCS/RamoAtividadeIntervaloPCS que já existe no banco usando a chave passada como argumento. Se
	 * não existir na map, retorna um objeto que pode ser preenchido.
	 *
	 * @param objetosPcsBanco
	 * @param idENCOPCS
	 * @param tipoResultado
	 * @return objetoPCS
	 * @throws NegocioException
	 */
	private <ObjetoPCS> ObjetoPCS buscaObjetoPCS(Map<Long, ObjetoPCS> objetosPcsBanco, Long idENCOPCS, Class<ObjetoPCS> tipoResultado)
			throws NegocioException {
		ObjetoPCS objetoPCS;
		if (objetosPcsBanco.containsKey(idENCOPCS)) {
			objetoPCS = objetosPcsBanco.get(idENCOPCS);
		} else {
			objetoPCS = criarObjetoPCS(tipoResultado);
		}
		return objetoPCS;
	}

	/**
	 * Cria um RamoAtividadeAmostragemPCS/RamoAtividadeIntervaloPCS pela fachada baseado no tipo passado como argumento.
	 *
	 * @param tipoResultado
	 * @return objetoPCS
	 * @throws NegocioException
	 */
	@SuppressWarnings("unchecked")
	private <ObjetoPCS> ObjetoPCS criarObjetoPCS(Class<ObjetoPCS> tipoResultado) throws NegocioException {

		Object objetoPCS;
		if (tipoResultado.equals(RamoAtividadeAmostragemPCS.class)) {
			objetoPCS = controladorRamoAtividade.criarRamoAtividadeAmostragemPCS();
		} else if (tipoResultado.equals(RamoAtividadeIntervaloPCS.class)) {
			objetoPCS = controladorRamoAtividade.criarRamoAtividadeIntervaloPCS();
		} else {
			throw new IllegalArgumentException();
		}
		return (ObjetoPCS) objetoPCS;
	}

	/**
	 * Cria uma map de RamoAtividadeAmostragemPCS/RamoAtividadeIntervaloPCS indexada pela chave primária da EntidadeNegocio correspondente.
	 *
	 * @param objetosPCS
	 * @return objetoPCSBanco
	 */
	private <ObjetoPCS> Map<Long, ObjetoPCS> mapearObjetoPCSporIdENCO(Collection<ObjetoPCS> objetosPCS) {

		Iterator<ObjetoPCS> iterator = objetosPCS.iterator();
		Map<Long, ObjetoPCS> objetosPCSBanco = new HashMap<Long, ObjetoPCS>();
		while (iterator.hasNext()) {
			inserirObjetoPCS(objetosPCSBanco, iterator.next());
		}
		return objetosPCSBanco;
	}

	/**
	 * Insere um RamoAtividadeAmostragemPCS/RamoAtividadeIntervaloPCS na map indexado pela chave primária da EntidadeNegocio correspondente.
	 *
	 * @param objetosPCSBanco
	 * @param objetoPCS
	 */
	private <ObjetoPCS> void inserirObjetoPCS(Map<Long, ObjetoPCS> objetosPCSBanco, ObjetoPCS objetoPCS) {

		EntidadeNegocio entidadeNegocio;
		if (objetoPCS instanceof RamoAtividadeAmostragemPCS) {
			RamoAtividadeAmostragemPCS ramoAtividadeAmostragemPCS = (RamoAtividadeAmostragemPCS) objetoPCS;
			entidadeNegocio = ramoAtividadeAmostragemPCS.getLocalAmostragemPCS();
		} else if (objetoPCS instanceof RamoAtividadeIntervaloPCS) {
			RamoAtividadeIntervaloPCS ramoAtividadeIntervaloPCS = (RamoAtividadeIntervaloPCS) objetoPCS;
			entidadeNegocio = ramoAtividadeIntervaloPCS.getIntervaloPCS();
		} else {
			throw new IllegalArgumentException();
		}
		objetosPCSBanco.put(entidadeNegocio.getChavePrimaria(), objetoPCS);
	}

	/**
	 * Popular ramo atividade intervalo pcs.
	 * 
	 * @param ramoAtividade
	 * @param listaIdIntervaloPCSSelecionados
	 * @param request
	 * @throws GGASException
	 */
	private void popularRamoAtividadeIntervaloPCS(RamoAtividade ramoAtividade, Long[] listaIdIntervaloPCSSelecionados,
			HttpServletRequest request) throws GGASException {

		Collection<IntervaloPCS> listaRamoAtividadeIntervaloPCSDisponiveis = controladorHistoricoConsumo.listarIntervaloPCS();

		if (listaIdIntervaloPCSSelecionados != null && listaIdIntervaloPCSSelecionados.length > 0) {

			Collection<RamoAtividadeIntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionados = new HashSet<RamoAtividadeIntervaloPCS>();
			Collection<IntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionadosIntevalos = new HashSet<IntervaloPCS>();

			int prioridade = 1;

			Map<Long, RamoAtividadeIntervaloPCS> ramoAtividadeIntervaloPCSBanco =
					mapearObjetoPCSporIdENCO(ramoAtividade.getRamoAtividadeIntervaloPCS());
			for (Long idIntervaloPCS : listaIdIntervaloPCSSelecionados) {
				RamoAtividadeIntervaloPCS ramoAtividadeIntervaloPCS =
						buscaObjetoPCS(ramoAtividadeIntervaloPCSBanco, idIntervaloPCS, RamoAtividadeIntervaloPCS.class);
				IntervaloPCS intervaloPCS = controladorRamoAtividade.obterIntervaloPCSParaRamoAtividade(idIntervaloPCS);
				ramoAtividadeIntervaloPCS.setIntervaloPCS(intervaloPCS);
				ramoAtividadeIntervaloPCS.setRamoAtividade(ramoAtividade);
				ramoAtividadeIntervaloPCS.setPrioridade(prioridade);
				++prioridade;
				ramoAtividadeIntervaloPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaRamoAtividadeIntervaloPCSSelecionados.add(ramoAtividadeIntervaloPCS);

				listaRamoAtividadeIntervaloPCSSelecionadosIntevalos.add(intervaloPCS);
				listaRamoAtividadeIntervaloPCSDisponiveis.remove(intervaloPCS);

			}

			ramoAtividade.setRamoAtividadeIntervaloPCS(listaRamoAtividadeIntervaloPCSSelecionados);
			request.setAttribute(LISTA_RAMO_ATIVIDADE_INTERVALO_PCS, listaRamoAtividadeIntervaloPCSSelecionadosIntevalos);

		} else if (ramoAtividade != null && (listaIdIntervaloPCSSelecionados == null || listaIdIntervaloPCSSelecionados.length == 0)) {

			Collection<RamoAtividadeIntervaloPCS> listaRamoAtividadeIntervaloPCS = new HashSet<RamoAtividadeIntervaloPCS>();

			if (ramoAtividade.getVersao() >= 0) {
				listaRamoAtividadeIntervaloPCS = controladorRamoAtividade.listarRamoAtividadeIntervaloPCSPorRamoAtividade(ramoAtividade);
			} else {
				listaRamoAtividadeIntervaloPCS = ramoAtividade.getRamoAtividadeIntervaloPCS();
			}

			Collection<RamoAtividadeIntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionados = new HashSet<RamoAtividadeIntervaloPCS>();
			Collection<IntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionadosIntevalos = new HashSet<IntervaloPCS>();

			for (RamoAtividadeIntervaloPCS ramoAtividadeIntervaloPCS : listaRamoAtividadeIntervaloPCS) {

				IntervaloPCS intervaloPCS = controladorRamoAtividade
						.obterIntervaloPCSParaRamoAtividade(ramoAtividadeIntervaloPCS.getIntervaloPCS().getChavePrimaria());
				ramoAtividadeIntervaloPCS.setIntervaloPCS(intervaloPCS);
				listaRamoAtividadeIntervaloPCSSelecionados.add(ramoAtividadeIntervaloPCS);

				listaRamoAtividadeIntervaloPCSSelecionadosIntevalos.add(intervaloPCS);
				listaRamoAtividadeIntervaloPCSDisponiveis.remove(intervaloPCS);

			}

			ramoAtividade.setRamoAtividadeIntervaloPCS(listaRamoAtividadeIntervaloPCSSelecionados);
			request.setAttribute(LISTA_RAMO_ATIVIDADE_INTERVALO_PCS, listaRamoAtividadeIntervaloPCSSelecionadosIntevalos);

		}

		if (ramoAtividade == null) {

			Collection<IntervaloPCS> listaRamoAtividadeIntervaloPCSSelecionados = new HashSet<IntervaloPCS>();

			for (Long idIntervaloPCS : listaIdIntervaloPCSSelecionados) {

				IntervaloPCS intervaloPCS = controladorRamoAtividade.obterIntervaloPCSParaRamoAtividade(idIntervaloPCS);
				listaRamoAtividadeIntervaloPCSSelecionados.add(intervaloPCS);
				listaRamoAtividadeIntervaloPCSDisponiveis.remove(intervaloPCS);
			}

			request.setAttribute(LISTA_RAMO_ATIVIDADE_INTERVALO_PCS, listaRamoAtividadeIntervaloPCSSelecionados);

		}

		request.setAttribute("listaRamoAtividadeIntervaloPCSDisponiveis", listaRamoAtividadeIntervaloPCSDisponiveis);

		if (ramoAtividade != null && ramoAtividade.getRamoAtividadeIntervaloPCS() != null) {
			long tamRed = 0;
			for (RamoAtividadeIntervaloPCS rammoAtividadeIntervaloPCS : ramoAtividade.getRamoAtividadeIntervaloPCS()) {
				if (rammoAtividadeIntervaloPCS.getTamanho() != null) {
					tamRed += rammoAtividadeIntervaloPCS.getTamanho();
				}
			}
			if (tamRed > 0) {
				request.setAttribute(TAMANHO_REDUCAO, tamRed);
			}
		}

	}

	/**
	 * Popup alterar ramo atividade.
	 * 
	 * @param segmento
	 * @param resultSeg
	 * @param ramoAtividadeTmp
	 * @param result
	 * @param ramoAtividadeAuxVO
	 * @param resultList
	 * @param substTributaria
	 * @param results
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("popupAlterarRamoAtividade")
	public String popupAlterarRamoAtividade(SegmentoImpl segmento, BindingResult resultSeg, RamoAtividadeImpl ramoAtividadeTmp,
			BindingResult result, RamoAtividadeAuxVO ramoAtividadeAuxVO, BindingResult resultList,
			RamoAtividadeSubstituicaoTributariaImpl substTributaria, BindingResult results, HttpServletRequest request, Model model)
			throws GGASException {

		Collection<EntidadeConteudo> listaTipoConsumoFaturamento = controladorEntidadeConteudo.obterListaTipoConsumoFaturamento();
		request.setAttribute(LISTA_TIPO_CONSUMO_FATURAMENTO, listaTipoConsumoFaturamento);
		Collection<RamoAtividade> listaRamoAtividade = getListaRamoAtividade(request);
		RamoAtividadeImpl ramoAtividade = ramoAtividadeTmp;
		model.addAttribute(SEGMENTO_FORM, segmento);
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		model.addAttribute(OPERACAO, ramoAtividadeAuxVO.getOperacao());

		if (ramoAtividadeAuxVO.getIdSubsTributaria() != null) {
			substTributaria.setChavePrimaria(ramoAtividadeAuxVO.getIdSubsTributaria());
		}

		String operacaoRamoAtividade = (String) request.getSession().getAttribute(OPERACAO_RAMO_ATIVIDADE);

		if (ramoAtividadeAuxVO.getIdRamoAtividade() != null && ramoAtividadeAuxVO.getIdRamoAtividade() > 0) {
			ramoAtividade = (RamoAtividadeImpl) controladorRamoAtividade.obter(ramoAtividadeAuxVO.getIdRamoAtividade());

			Iterator<RamoAtividade> iterator = listaRamoAtividade.iterator();

			while (iterator.hasNext()) {
				ramoAtividade = (RamoAtividadeImpl) iterator.next();
				if (ramoAtividade.getChavePrimaria() == ramoAtividadeAuxVO.getIdRamoAtividade()) {
					break;
				}
			}

		}

		if (operacaoRamoAtividade == null || operacaoRamoAtividade.isEmpty()) {
			// se a tela for iniciada/atualizada sem nenhuma operação a lista de
			// substituição tributaria é zerada
			Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
					new HashSet<RamoAtividadeSubstituicaoTributaria>();
			request.getSession().setAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA, listaRamoAtividadeSubstituicaoTributaria);
		}

		if (ramoAtividade != null && (operacaoRamoAtividade == null || operacaoRamoAtividade.isEmpty())) {
			// carrega os dados do ramo atividade
			carregarPopupRamoAtividade(request, ramoAtividade, false, model);
			popularRamoAtividadeAmostragemPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), request);
			popularRamoAtividadeIntervaloPCS(ramoAtividade, ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), request);
		} else {
			carregarPopupRamoAtividade(request, ramoAtividade, true, model);
			carregarSubsTributaria(request, ramoAtividade, ramoAtividadeAuxVO.getOperacaoSubstituicao(), substTributaria,
					ramoAtividadeAuxVO.getRamoAtividadeAmostragemPCS(), ramoAtividadeAuxVO.getRamoAtividadeIntervaloPCS(), model);
		}

		if (ramoAtividade != null && ramoAtividade.getRamoAtividadeSubstituicaoTributaria() != null
				&& !ramoAtividade.getRamoAtividadeSubstituicaoTributaria().isEmpty()) {
			request.setAttribute(LISTA_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA, ramoAtividade.getRamoAtividadeSubstituicaoTributaria());
		}
		model.addAttribute(RAMO_ATIVIDADE, ramoAtividade);
		request.getSession().setAttribute(OPERACAO_RAMO_ATIVIDADE, "alterarRamoAtividade");
		request.setAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());

		return "popupAlterarRamoAtividade";
	}

	/**
	 * Limpar session ramo atividade.
	 * 
	 * @param request
	 */
	@RequestMapping("limparSessionRamoAtividade")
	public void limparSessionRamoAtividade(HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		session.removeAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);
	}

	/**
	 * Validar dados ramo atividade substituicao tributaria.
	 * 
	 * @param rast
	 * @param request
	 * @param model
	 * @return boolean
	 * @throws GGASException
	 */
	public void validarDadosRamoAtividadeSubstituicaoTributaria(RamoAtividadeSubstituicaoTributaria rast, HttpServletRequest request,
			Model model) throws GGASException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (rast.getDataInicioVigencia() == null || rast.getDataFimVigencia() == null) {
			stringBuilder.append("Data de vigência");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (rast.getTipoSubstituicao() == null) {
			stringBuilder.append("Tipo de Substituição Tributária");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			validaTipoSubstituicaoTributaria(rast, stringBuilder);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			String[] msg = { camposObrigatorios.substring(0, stringBuilder.toString().length() - 2) };
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, msg);
		}

		if (rast.getDataInicioVigencia() != null && rast.getDataFimVigencia() != null
				&& rast.getDataInicioVigencia().compareTo(rast.getDataFimVigencia()) > 0) {
			throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL);
		}

		verificaRASTExistente(rast, request);

	}

	/**
	 * Método que verifica a existência de um RamoAtividadeSubstituicaoTributaria na árvore ordenada e lança exceção caso ocorra.
	 * 
	 * @param rast
	 * @param request
	 * @throws NegocioException
	 */
	@SuppressWarnings("unchecked")
	private void verificaRASTExistente(RamoAtividadeSubstituicaoTributaria rast, HttpServletRequest request) throws NegocioException {
		Collection<RamoAtividadeSubstituicaoTributaria> rastSessao =
				(Collection<RamoAtividadeSubstituicaoTributaria>) request.getSession().getAttribute(REQUEST_LISTA_SUBS_TRIBUTARIA);
		SortedSet<RamoAtividadeSubstituicaoTributaria> treeSet = criarArvoreOrdenadaRast();
		treeSet.add(rast);
		for (RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria : rastSessao) {
			boolean existente = !treeSet.add(ramoAtividadeSubstituicaoTributaria);
			if (existente && rast.getChavePrimaria() != ramoAtividadeSubstituicaoTributaria.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_SOBREPOSICAO_VIGENCIA_SUBS_TRIBUT_RAMO_ATIVIDADE);
			}
		}

	}

	/**
	 * Método que valida o tipo de substituição tributária
	 * 
	 * @param rast
	 * @param stringBuilder
	 */
	private void validaTipoSubstituicaoTributaria(RamoAtividadeSubstituicaoTributaria rast, StringBuilder stringBuilder) {
		Long valorConstanteSubsTributMargemValorAgregado = Long.valueOf(
				this.controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO));
		Long valorConstanteSubsTributPrecoMedioPondAConsFinal = Long.valueOf(this.controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL));

		if (rast.getTipoSubstituicao().getChavePrimaria() == valorConstanteSubsTributMargemValorAgregado) {
			if (rast.getPercentualSubstituto() == null || rast.getPercentualSubstituto().intValue() == 0) {
				stringBuilder.append("Percentual");
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (rast.getTipoSubstituicao().getChavePrimaria() == valorConstanteSubsTributPrecoMedioPondAConsFinal
				&& rast.getValorSubstituto() == null || rast.getValorSubstituto().intValue() == 0) {
			stringBuilder.append(VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
	}

	/**
	 * Criar arvore ordenada rast. A TreeSet possui uma inserção de complexidade O(log(n)), isso torna a validação de intervalos de vigência
	 * de substituição tributária mais eficiente.
	 * 
	 * @return SortedSet
	 */
	private SortedSet<RamoAtividadeSubstituicaoTributaria> criarArvoreOrdenadaRast() {
		return new TreeSet<RamoAtividadeSubstituicaoTributaria>(new Comparator<RamoAtividadeSubstituicaoTributaria>() {
			@Override
			public int compare(RamoAtividadeSubstituicaoTributaria o1, RamoAtividadeSubstituicaoTributaria o2) {
				int result;
				Date dataInicioVigencia = o1.getDataInicioVigencia();
				Date dataFimVigencia = o1.getDataFimVigencia();
				Date dataInicioVigencia2 = o2.getDataInicioVigencia();
				Date dataFimVigencia2 = o2.getDataFimVigencia();
				if (dataFimVigencia.before(dataInicioVigencia2)) {
					result = -1;
				} else if (dataInicioVigencia.after(dataFimVigencia2)) {
					result = 1;
				} else {
					result = 0;
				}
				return result;
			}
		});
	}

}
