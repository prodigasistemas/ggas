/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.cadastro.cliente;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a Tipo de Endereço.
 */
@Controller
public class TipoEnderecoAction extends GenericAction {
	
	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String EXIBIR_PESQUISA_TIPO_ENDERECO = "exibirPesquisaTipoEndereco";

	private static final String TIPO_ENDERECO = "tipoEndereco";

	private static final String EXIBIR_ATUALIZAR_TIPO_ENDERECO = "exibirAtualizarTipoEndereco";

	private static final String EXIBIR_INSERIR_TIPO_ENDERECO = "exibirInserirTipoEndereco";

	private static final Class<TipoEnderecoImpl> CLASSE = TipoEnderecoImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_TIPO_ENDERECO = "listaTipoEndereco";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa tipo de endereço.
	 * 
	 * @param model
	 * @param session
	 * @return String
	 */
	@RequestMapping(EXIBIR_PESQUISA_TIPO_ENDERECO)
	public String exibirPesquisaTipoEndereco(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_TIPO_ENDERECO;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de tipo de endereço.
	 * 
	 * @param tipoEndereco
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarTipoEndereco")
	public String pesquisarTipoEndereco(TipoEnderecoImpl tipoEndereco, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(tipoEndereco, habilitado);

		Collection<TabelaAuxiliar> listaTipoEndereco = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_TIPO_ENDERECO, listaTipoEndereco);
		model.addAttribute(TIPO_ENDERECO, tipoEndereco);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_TIPO_ENDERECO;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento tipo de endereço.
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirDetalhamentoTipoEndereco")
	public String exibirDetalhamentoTipoEndereco(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaTipoEndereco";
		
		try {
			TipoEnderecoImpl tipoEndereco = (TipoEnderecoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(TIPO_ENDERECO, tipoEndereco);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoTipoEndereco";
		} catch (NegocioException e) {
			mensagemErro(model, e);
			
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir tipo de endereço.
	 * 
	 * @return String
	 */
	@RequestMapping(EXIBIR_INSERIR_TIPO_ENDERECO)
	public String exibirInserirEquipamento() {

		return EXIBIR_INSERIR_TIPO_ENDERECO;
	}

	/**
	 * Método responsável por inserir tipo de endereço.
	 * 
	 * @param tipoEndereco
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirTipoEndereco")
	private String inserirTipoEndereco(TipoEnderecoImpl tipoEndereco, BindingResult result, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = EXIBIR_INSERIR_TIPO_ENDERECO;

		try {

			if (tipoEndereco.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoEndereco);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoEndereco, "Tipo de Endereço");
			controladorTabelaAuxiliar.inserir(tipoEndereco);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TipoEndereco.TIPO_ENDERECO);
			retorno = pesquisarTipoEndereco(tipoEndereco, result, tipoEndereco.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute("tipoEndereco", tipoEndereco);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar tipo de endereço.
	 * 
	 * @param tipoEnderecoImpl
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_TIPO_ENDERECO)
	public String exibirAtualizarTipoEndereco(TipoEnderecoImpl tipoEnderecoImpl, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		String retorno = "forward:exibirPesquisaTipoEndereco";
		
		TipoEnderecoImpl tipoEndereco = tipoEnderecoImpl;

		try {
			tipoEndereco = (TipoEnderecoImpl) controladorTabelaAuxiliar.obter(tipoEndereco.getChavePrimaria(), CLASSE);
			model.addAttribute(TIPO_ENDERECO, tipoEndereco);
			model.addAttribute(HABILITADO, habilitado);
			retorno = EXIBIR_ATUALIZAR_TIPO_ENDERECO;
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover tipo de endereço.
	 * 
	 * @param tipoEndereco
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * @throws ConcorrenciaException
	 */
	@RequestMapping("atualizarTipoEndereco")
	private String atualizarTipoEndereco(TipoEnderecoImpl tipoEndereco, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = EXIBIR_ATUALIZAR_TIPO_ENDERECO;

		try {

			if (tipoEndereco.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoEndereco);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoEndereco, "Tipo de Endereço");
			controladorTabelaAuxiliar.atualizar(tipoEndereco, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TipoEndereco.TIPO_ENDERECO);
			retorno = pesquisarTipoEndereco(tipoEndereco, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(TIPO_ENDERECO, tipoEndereco);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o tipo de endereço.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerTipoEndereco")
	public String removerTipoEndereco(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = "forward:pesquisarTipoEndereco";
		
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, TipoEndereco.TIPO_ENDERECO);
			retorno = pesquisarTipoEndereco(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param tipoEndereco
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(TipoEnderecoImpl tipoEndereco, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (tipoEndereco != null) {

			if (tipoEndereco.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, tipoEndereco.getChavePrimaria());
			}

			if (tipoEndereco.getDescricao() != null && !tipoEndereco.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, tipoEndereco.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}

