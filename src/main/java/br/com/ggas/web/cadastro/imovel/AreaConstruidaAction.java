package br.com.ggas.web.cadastro.imovel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorAreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.impl.AreaConstruidaFaixaImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelas operações realizadas
 * em tela referentes a Área Construída.
 *
 * @author esantana
 *
 */
@Controller
public class AreaConstruidaAction extends GenericAction {

	private static final String VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA = "exibirInclusaoAreaConstruida";

	private static final String PAGINA_HELP = "paginaHelp";

	private static final String FAIXA = "faixa";

	private static final String MAIOR_FAIXA = "maiorFaixa";

	private static final String LISTA_TABELA_AUX = "listaTabelaAux";

	private static final Integer TAMANHO_ARRAY_IDS_TABELA_AUXILIAR = 10;

	private static final Integer QTD_LIMITE_FAIXAS = 2;

	private static final Integer TAMANHO_MAXIMO_FAIXA = 5;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	private ControladorAreaConstruidaFaixa controladorAreaConstruidaFaixa;

	/**
	 * Método responsável por exibir a tela de pesquisa de área construída.
	 *
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("exibirPesquisaAreaConstruida")
	public String exibirPesquisaAreaConstruida(HttpServletRequest request, Model model) throws GGASException {

		HttpSession sessao = request.getSession(false);

		sessao.setAttribute(PAGINA_HELP, "/consultadasfaixasdereaconstruda.htm");
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);

		limparFormulario(model);

		return "exibirPesquisaAreaConstruida";
	}

	/**
	 * Método responsável por pesquisar uma lista de faixas de área construída.
	 *
	 * @param model - {@link Model}
	 * @param habilitado = {@link String}
	 * @param faixa - {@link String}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarAreaConstruida")
	public String pesquisarAreaConstruida(Model model, @RequestParam(value = "habilitado", required = false) String habilitado,
			@RequestParam(value = "faixa", required = false) String faixa) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (habilitado != null && !StringUtils.isEmpty(habilitado)) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado));
		}

		if (faixa != null && !StringUtils.isEmpty(faixa)) {
			filtro.put(FAIXA, Integer.parseInt(faixa));
		}

		Collection<TabelaAuxiliar> listaAreaConstruida =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, AreaConstruidaFaixaImpl.class.getName());

		model.addAttribute(FAIXA, faixa);
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		model.addAttribute("listaAreaConstruida", listaAreaConstruida);

		return "exibirPesquisaAreaConstruida";
	}

	/**
	 * Método responsável por exibir a tela de inclusão de área construída.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping(VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA)
	public String exibirInclusaoAreaConstruida(Model model, HttpServletRequest request) throws GGASException {

		HttpSession sessao = request.getSession(false);

		sessao.removeAttribute("listaImovel");

		if (!isPostBack(request)) {
			limparFormulario(model);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();

		Collection<TabelaAuxiliar> listaAreaConstruida =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, AreaConstruidaFaixaImpl.class.getName());

		if (listaAreaConstruida != null) {

			String[] arrayfaixaFinal = new String[listaAreaConstruida.size()];
			Long[] chavesPrimarias = new Long[listaAreaConstruida.size()];
			int count = 0;
			for (TabelaAuxiliar tabelaAuxiliar : listaAreaConstruida) {
				chavesPrimarias[count] = tabelaAuxiliar.getChavePrimaria();
				count++;
			}

			count = 0;
			filtro.put("idAreaConstruidaFaixa", chavesPrimarias);

			for (TabelaAuxiliar tabelaAuxiliar : listaAreaConstruida) {
				arrayfaixaFinal[count] = tabelaAuxiliar.getMaiorFaixa().toString();
				count++;
			}

			model.addAttribute(MAIOR_FAIXA, arrayfaixaFinal);

			if (listaAreaConstruida.isEmpty()) {
				TabelaAuxiliar tabela = controladorTabelaAuxiliar.criar();
				tabela.setMenorFaixa(0);
				listaAreaConstruida.add(tabela);
			}

			sessao.setAttribute(LISTA_TABELA_AUX, listaAreaConstruida);
		}

		sessao.setAttribute(PAGINA_HELP, "/cadastrodareaconstrudainclusoalteraoremoo.htm");

		saveToken(request);

		return VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA;
	}

	/**
	 * Método responsável por realizar a inclusão de uma área construída.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param mdFimParam
	 * @param faixaInicialExclusao - {@link String}
	 * @param faixasHabilitadasParam
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirAreaConstruida")
	public String incluirAreaConstruida(Model model, HttpServletRequest request,
			@RequestParam(value = "maiorFaixa", required = false) String[] mdFimParam,
			@RequestParam(value = "faixaInicialExclusao", required = false) String faixaInicialExclusao,
			@RequestParam(value = "faixasHabilitadas", required = false) String[] faixasHabilitadasParam) {

		String view = null;

		String[] mdFim = this.inicializarParametroArray(mdFimParam);
		String[] faixasHabilitadas = this.inicializarParametroArray(faixasHabilitadasParam);

		try {
			getDadosAuditoria(request);

			Map<String, Object> filtro = new HashMap<String, Object>();

			String mensagemErro = verificarFaixaFim(mdFim);

			if (mensagemErro != null) {
				throw new NegocioException(mensagemErro, Boolean.TRUE);
			}

			List<TabelaAuxiliar> listaTabelaAuxiliarOriginal = (List<TabelaAuxiliar>) controladorTabelaAuxiliar
					.pesquisarTabelaAuxiliar(filtro, AreaConstruidaFaixaImpl.class.getName());

			this.incluirFaixaAreaConstruida(model, request, mdFim, faixasHabilitadas);

			List<TabelaAuxiliar> listaDeAtualizacao = new ArrayList<TabelaAuxiliar>();
			listaDeAtualizacao.addAll((List<TabelaAuxiliar>) request.getSession().getAttribute(LISTA_TABELA_AUX));

			List<TabelaAuxiliar> lista = new ArrayList<TabelaAuxiliar>();
			lista.addAll((List<TabelaAuxiliar>) request.getSession().getAttribute(LISTA_TABELA_AUX));

			lista.removeAll(listaTabelaAuxiliarOriginal);

			removerItemListaTabelaAuxiliar(listaTabelaAuxiliarOriginal, lista);

			controladorTabelaAuxiliar.verificarAreaContruida(faixasHabilitadas, lista);
			request.getSession().getAttribute(LISTA_TABELA_AUX);

			List<TabelaAuxiliar> listaTabelaAuxiliarNovosDados =
					(List<TabelaAuxiliar>) request.getSession().getAttribute("listaTabelaAuxiliarNovosDados");

			removeTabelaAuxiliar(request, listaTabelaAuxiliarOriginal, listaTabelaAuxiliarNovosDados);

			incluiAlteraTabelaAuxiliar(listaTabelaAuxiliarOriginal, listaTabelaAuxiliarNovosDados);

			for (TabelaAuxiliar tabelaAux : lista) {
				if (tabelaAux.getMaiorFaixa() != null) {
					controladorTabelaAuxiliar.inserir(tabelaAux);
				}
			}

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, "Área Construída");
			view = this.pesquisarAreaConstruida(model, "true", "");
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA;
		} catch(DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErro(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
			view = "forward:/exibirInclusaoAreaConstruida";
		}

		return view;
	}

	private void removerItemListaTabelaAuxiliar(List<TabelaAuxiliar> listaTabelaAuxiliarOriginal, List<TabelaAuxiliar> lista) {
		for (int i = 0; i < lista.size(); i++) {
			for (int j = 0; j < listaTabelaAuxiliarOriginal.size(); j++) {
				if (lista.get(i).getMaiorFaixa() != null
						&& lista.get(0).getMaiorFaixa().equals(listaTabelaAuxiliarOriginal.get(j).getMaiorFaixa())) {
					lista.remove(0);
					break;
				}
			}
		}
	}

	private void removeTabelaAuxiliar(HttpServletRequest request, List<TabelaAuxiliar> listaTabelaAuxiliarOriginal,
			List<TabelaAuxiliar> listaTabelaAuxiliarNovosDados) throws GGASException {
		for (TabelaAuxiliar tabOriginal : listaTabelaAuxiliarOriginal) {
			boolean excluir = true;
			Long[] ids = new Long[TAMANHO_ARRAY_IDS_TABELA_AUXILIAR];
			for (TabelaAuxiliar tabNovas : listaTabelaAuxiliarNovosDados) {
				if (tabOriginal.getChavePrimaria() == tabNovas.getChavePrimaria()) {
					excluir = false;
					break;
				}
			}

			if (excluir) {
				ids[0] = tabOriginal.getChavePrimaria();
				controladorAreaConstruidaFaixa.remover(ids, getDadosAuditoria(request));
			}
		}
	}

	private void incluiAlteraTabelaAuxiliar(List<TabelaAuxiliar> listaTabelaAuxiliarOriginal,
			List<TabelaAuxiliar> listaTabelaAuxiliarNovosDados) throws GGASException {
		for (TabelaAuxiliar tabelaAux : listaTabelaAuxiliarNovosDados) {
			if (tabelaAux.getMenorFaixa() != null && tabelaAux.getMaiorFaixa() != null) {
				atualizarItemListaTabelaAuxiliar(listaTabelaAuxiliarOriginal, tabelaAux);
			}
		}
	}

	private void atualizarItemListaTabelaAuxiliar(List<TabelaAuxiliar> listaTabelaAuxiliarOriginal, TabelaAuxiliar tabelaAux)
			throws GGASException {
		for (TabelaAuxiliar tabOriginal : listaTabelaAuxiliarOriginal) {
			if (tabOriginal.getChavePrimaria() == tabelaAux.getChavePrimaria()) {
				controladorTabelaAuxiliar.atualizar(tabelaAux, tabelaAux.getClass());
				break;
			}
		}
	}

	/**
	 * Método responsável por remover uma faixa de área construída.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param mdFimParam
	 * @param faixaInicialExclusao - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("removerFaixaAreaConstruida")
	@SuppressWarnings("unchecked")
	public String removerFaixaAreaConstruida(Model model, HttpServletRequest request,
			@RequestParam(value = "maiorFaixa", required = false) String[] mdFimParam,
			@RequestParam(value = "faixaInicialExclusao", required = false) String faixaInicialExclusao) {

		String[] mdFim = this.inicializarParametroArray(mdFimParam);

		try {
			getDadosAuditoria(request);

			List<TabelaAuxiliar> listaDadosFaixa = (List<TabelaAuxiliar>) request.getSession().getAttribute(LISTA_TABELA_AUX);

			this.montarListaDadosFaixaTarifaOriginal(mdFim, listaDadosFaixa);
			List<TabelaAuxiliar> listaNova = new ArrayList<TabelaAuxiliar>();
			this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

			if (!StringUtils.isEmpty(faixaInicialExclusao)) {
				if (!listaNova.isEmpty()) {
					Iterator<TabelaAuxiliar> itFaixasAreaConstruida = listaNova.iterator();
					removerAreaDeFaixaInicialExclusao(faixaInicialExclusao, itFaixasAreaConstruida);
				}
				controladorTabelaAuxiliar.validarFaixasZeradasTarifa(listaNova);

				listaDadosFaixa.clear();
				listaDadosFaixa.addAll(listaNova);

				if (listaDadosFaixa.get(listaDadosFaixa.size() - 1).getMaiorFaixa() == null) {
					listaDadosFaixa.remove(listaDadosFaixa.size() - 1);
				}

				atualizarValoresRemoverFaixaTarifa(listaDadosFaixa);
				request.getSession().setAttribute(LISTA_TABELA_AUX, listaDadosFaixa);

				Long[] arrayChavePrimaria = new Long[listaDadosFaixa.size()];
				int cont = 0;
				for (TabelaAuxiliar tabelaAuxiliar : listaDadosFaixa) {
					arrayChavePrimaria[cont] = tabelaAuxiliar.getChavePrimaria();
					cont++;
				}
				this.carregarFaixasSemImoveis(arrayChavePrimaria, model, request);
			}

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA;
	}

	private void removerAreaDeFaixaInicialExclusao(String faixaInicialExclusao, Iterator<TabelaAuxiliar> itFaixasAreaConstruida) {
		while (itFaixasAreaConstruida.hasNext()) {
			AreaConstruidaFaixa dadosFaixa = (AreaConstruidaFaixa) itFaixasAreaConstruida.next();
			if (dadosFaixa.getMenorFaixa().equals(Integer.valueOf(faixaInicialExclusao))) {
				itFaixasAreaConstruida.remove();
			}
		}
	}

	private void atualizarValoresRemoverFaixaTarifa(List<TabelaAuxiliar> listaDadosFaixa) {

		String zero = "0";
		String faixaNovaInicio = zero;

		this.obterListaDadosFaixaOrdenada(listaDadosFaixa);

		for (TabelaAuxiliar dados : listaDadosFaixa) {
			dados.setMenorFaixa(Integer.valueOf(faixaNovaInicio));
			if (dados.getMenorFaixa() != null) {
				faixaNovaInicio = dados.getMaiorFaixa() + 1 + "";

				if (Integer.parseInt(faixaNovaInicio) > Constantes.AREA_CONSTRUIDA_FAIXA_FINAL.intValue()) {
					faixaNovaInicio = dados.getMaiorFaixa() + "";
				}
			}
		}
	}

	/**
	 * Método responsável por realizar a inclusão de uma faixa de área construída.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param mdFim
	 * @param faixasHabilitadas
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirFaixaAreaConstruida")
	public String incluirFaixaAreaConstruida(Model model, HttpServletRequest request,
			@RequestParam(value = "maiorFaixa", required = false) String[] mdFim,
			@RequestParam(value = "faixasHabilitadas", required = false) String[] faixasHabilitadas) {

		try {
			this.adicionarFaixaAreaConstruida(model, request, mdFim, faixasHabilitadas);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return VIEW_EXIBIR_INCLUSAO_AREA_CONSTRUIDA;
	}

	private void adicionarFaixaAreaConstruida(Model model, HttpServletRequest request,
			@RequestParam(value = "maiorFaixa", required = false) String[] mdFimParam,
			@RequestParam(value = "faixasHabilitadas", required = false) String[] faixasHabilitadasParam) throws GGASException {

		String[] mdFim = this.inicializarParametroArray(mdFimParam);
		String[] faixasHabilitadas = this.inicializarParametroArray(faixasHabilitadasParam);

		getDadosAuditoria(request);

		String mensagemErro = verificarFaixaFim(mdFim);

		if (mensagemErro == null) {

			List<TabelaAuxiliar> listaDadosFaixa = (List<TabelaAuxiliar>) request.getSession().getAttribute(LISTA_TABELA_AUX);
			if (listaDadosFaixa != null && !listaDadosFaixa.isEmpty()) {
				this.montarListaDadosFaixaTarifaOriginal(mdFim, listaDadosFaixa);
				List<TabelaAuxiliar> listaNova = new ArrayList<TabelaAuxiliar>();
				this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

				this.montarListaDadosFaixaTarifa(mdFim, listaNova);
				String ultimaFaixa = atualizarValoresFaixaTarifa(listaNova);

				controladorTabelaAuxiliar.validarFaixasZeradasTarifa(listaNova);
				listaDadosFaixa.clear();
				listaDadosFaixa.addAll(listaNova);

				Long[] arrayChavePrimaria = habilitarFaixasDeAreaConstruida(faixasHabilitadas, listaDadosFaixa);

				AreaConstruidaFaixa areaConstruidaFaixa = (AreaConstruidaFaixa) controladorAreaConstruidaFaixa.criar();
				areaConstruidaFaixa.setHabilitado(Boolean.TRUE);
				areaConstruidaFaixa.setMenorFaixa(Integer.valueOf(ultimaFaixa));
				Long maiorChave = compararMaiorChavePrimaria(arrayChavePrimaria);
				areaConstruidaFaixa.setChavePrimaria(maiorChave + 1);
				listaDadosFaixa.add(areaConstruidaFaixa);

				this.carregarFaixasSemImoveis(arrayChavePrimaria, model, request);

				request.getSession().setAttribute(LISTA_TABELA_AUX, listaDadosFaixa);
				request.getSession().setAttribute("listaTabelaAuxiliarNovosDados", listaNova);
			}

		} else {
			throw new NegocioException(mensagemErro, Boolean.TRUE);

		}
	}

	private Long compararMaiorChavePrimaria(Long[] arrayChavePrimaria) {
		Long maiorChave = 0L;
		for (Long chave : arrayChavePrimaria) {
			if (chave > maiorChave) {
				maiorChave = chave;
			}
		}
		return maiorChave;
	}

	private Long[] habilitarFaixasDeAreaConstruida(String[] faixasHabilitadas, List<TabelaAuxiliar> listaDadosFaixa) {
		Long[] arrayChavePrimaria = new Long[listaDadosFaixa.size()];
		int aux = 0;
		for (TabelaAuxiliar tabelaAuxiliar : listaDadosFaixa) {
			boolean habilitado = false;
			arrayChavePrimaria[aux] = tabelaAuxiliar.getChavePrimaria();
			for (int i = 0; i < faixasHabilitadas.length; i++) {
				if (faixasHabilitadas[i].equals(String.valueOf(tabelaAuxiliar.getChavePrimaria()))) {
					habilitado = true;
					tabelaAuxiliar.setHabilitado(habilitado);
				}
			}

			if (!habilitado) {
				tabelaAuxiliar.setHabilitado(habilitado);
			}

			aux++;
		}
		return arrayChavePrimaria;
	}

	private String[] inicializarParametroArray(String[] parametro) {
		String[] retorno = new String[] {};

		if (parametro != null) {
			retorno = parametro;
		}

		return retorno;
	}

	@SuppressWarnings("unchecked")
	private void carregarFaixasSemImoveis(Long[] chavesPrimarias, Model model, HttpServletRequest request) throws GGASException {

		Collection<Imovel> listaImovel = (Collection<Imovel>) request.getSession().getAttribute("listaImovel");
		if (listaImovel == null) {
			listaImovel = new ArrayList<Imovel>();
		}

		List<Long> faixasSemImoveis = new ArrayList<Long>();
		for (Long valor : chavesPrimarias) {
			faixasSemImoveis.add(valor);
		}

		for (Imovel imovel : listaImovel) {
			for (Long chave : faixasSemImoveis) {
				if (imovel.getAreaConstruidaFaixa().getChavePrimaria() == chave) {
					int index = faixasSemImoveis.indexOf(chave);
					faixasSemImoveis.set(index, 0L);
					break;
				}
			}
		}

		model.addAttribute("faixasSemImoveis", faixasSemImoveis);
		request.getSession().setAttribute("faixasSemImoveis", faixasSemImoveis);

	}

	private void montarListaDadosFaixaTarifa(String[] mdFim, List<TabelaAuxiliar> listaNova) {

		int count = 0;

		for (TabelaAuxiliar tabelaAuxiliar : listaNova) {

			String faixaFinal;

			if ("".equals(mdFim[count])) {
				faixaFinal = "0";
			} else {
				faixaFinal = mdFim[count].replace(".", "");
			}

			tabelaAuxiliar.setMaiorFaixa(Integer.valueOf(faixaFinal));

			count++;
		}
	}

	private String atualizarValoresFaixaTarifa(List<TabelaAuxiliar> listaNova) {

		final String zero = "0";

		String faixaNovaInicio = zero;
		Boolean achouFaixa = false;

		for (TabelaAuxiliar dados : listaNova) {
			if (dados.getMaiorFaixa().equals(Integer.valueOf(Constantes.AREA_CONSTRUIDA_FAIXA_INICIO))) {
				achouFaixa = true;
			}
		}

		if (achouFaixa) {
			listaNova.remove(listaNova.size() - 1);
		}

		this.obterListaDadosFaixaOrdenada(listaNova);

		for (TabelaAuxiliar dados : listaNova) {
			dados.setMenorFaixa(Integer.valueOf(faixaNovaInicio));
			faixaNovaInicio = dados.getMaiorFaixa() + 1 + "";

			if (Integer.parseInt(faixaNovaInicio) > Constantes.AREA_CONSTRUIDA_FAIXA_FINAL.intValue()) {
				faixaNovaInicio = dados.getMaiorFaixa() + "";
			}
		}
		return faixaNovaInicio;
	}

	private void obterListaDadosFaixaOrdenada(List<TabelaAuxiliar> listaNova) {

		if (listaNova != null && !listaNova.isEmpty()) {

			Collections.sort(listaNova, new Comparator<TabelaAuxiliar>() {

				@Override
				public int compare(TabelaAuxiliar dados1, TabelaAuxiliar dados2) {

					if ((dados1.getMaiorFaixa() != null) && (dados2.getMaiorFaixa() != null)) {
						return Long.valueOf(dados1.getMaiorFaixa()).compareTo(Long.valueOf(dados2.getMaiorFaixa()));
					} else if (dados1.getMaiorFaixa() != null) {
						return 1;
					} else {
						return -1;
					}
				}
			});

		}
	}

	private String verificarFaixaFim(String[] mdFim) {

		String mdFimAux = null;
		int count = 0;
		int encontrouFaixa = 0;
		String excecao = null;

		while (count < mdFim.length) {
			mdFimAux = mdFim[count];

			if (mdFimAux.equals(Constantes.AREA_CONSTRUIDA_FAIXA_INICIO)) {

				excecao = ControladorTabelaAuxiliar.ERRO_NEGOCIO_FAIXA_FIM_ZERO;
				break;

			}

			for (int i = 0; i < mdFim.length; i++) {
				if (mdFimAux.equals(mdFim[i])) {
					encontrouFaixa++;
					if (encontrouFaixa == QTD_LIMITE_FAIXAS) {

						excecao = ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_FAIXA_FIM;
						break;
					}
				}

			}
			encontrouFaixa = 0;
			count++;
		}

		return excecao;
	}

	private void montarListaNovaDadosFaixa(List<TabelaAuxiliar> listaDadosFaixa, List<TabelaAuxiliar> listaNova) {

		for (TabelaAuxiliar tabelaAuxiliar : listaDadosFaixa) {

			AreaConstruidaFaixa areaConstruidaFaixa = (AreaConstruidaFaixa) controladorAreaConstruidaFaixa.criar();
			areaConstruidaFaixa.setChavePrimaria(tabelaAuxiliar.getChavePrimaria());

			areaConstruidaFaixa.setMenorFaixa(tabelaAuxiliar.getMenorFaixa());
			areaConstruidaFaixa.setMaiorFaixa(tabelaAuxiliar.getMaiorFaixa());

			areaConstruidaFaixa.setHabilitado(tabelaAuxiliar.isHabilitado());
			areaConstruidaFaixa.setUltimaAlteracao(tabelaAuxiliar.getUltimaAlteracao());
			areaConstruidaFaixa.setVersao(tabelaAuxiliar.getVersao());

			listaNova.add(areaConstruidaFaixa);
		}
	}

	private void montarListaDadosFaixaTarifaOriginal(String[] mdFim, List<TabelaAuxiliar> listaNova) throws GGASException {

		int count = 0;

		for (TabelaAuxiliar tabelaAuxiliar : listaNova) {

			String faixaFinal;

			if ("".equals(mdFim[count])) {
				faixaFinal = "";
			} else {
				faixaFinal = mdFim[count].replace(".", "");
			}

			if (!"".equals(faixaFinal)) {
				if (faixaFinal.length() > TAMANHO_MAXIMO_FAIXA) {
					throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO, true);
				}
				tabelaAuxiliar.setMaiorFaixa(Integer.valueOf(faixaFinal));
				count++;
			}

		}
	}

	private void limparFormulario(Model model) {
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addAttribute(FAIXA, "");
	}

}
