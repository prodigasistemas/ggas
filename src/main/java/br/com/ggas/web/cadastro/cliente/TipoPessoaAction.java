package br.com.ggas.web.cadastro.cliente;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas ações referentes ao manter Tipo de Pessoa.
 * 
 * @author esantana
 *
 */
@Controller
public class TipoPessoaAction extends GenericAction {

	private static final String TITULO_TABELA_AUXILIAR = "Tipo de Pessoa";

	private static final String FORWARD_PESQUISAR_TIPO_PESSOA = "forward:/pesquisarTipoPessoa";

	private static final String FORWARD_EXIBIR_PESQUISA_TIPO_PESSOA = "forward:/exibirPesquisaTipoPessoa";

	private static final String TIPO_CLIENTE = "tipoCliente";
	
	private static final Integer PESSOA_FISICA = 1;
	
	private static final Integer PESSOA_JURIDICA = 2;

	private static final String ID_ESFERA_PODER = "idEsferaPoder";

	private static final String ID_TIPO_PESSOA = "idTipoPessoa";
	
	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaTipoPessoa")
	public String exibirPesquisaTipoPessoa(Model model) throws GGASException {

		this.prepararCampos(model);

		return "exibirPesquisaTipoPessoa";
	}

	private void prepararCampos(Model model) throws GGASException {
		Collection<EsferaPoder> listaEsferaPoder = controladorCliente.listarEsferaPoder();

		model.addAttribute("listaEsferaPoder", listaEsferaPoder);

		final Map<Integer, String> tipoPessoa = new HashMap<Integer, String>();

		tipoPessoa.put(PESSOA_FISICA, TipoPessoa.DESCRICAO_PESSOA_FISICA);
		tipoPessoa.put(PESSOA_JURIDICA, TipoPessoa.DESCRICAO_PESSOA_JURIDICA);

		model.addAttribute("listaTipoPessoa", tipoPessoa);

		if (!model.containsAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO)) {
			model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		}
	}

	/**
	 * Método responsável por exibir realizar a pesquisa de Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param tipoCliente - {@link TipoClienteImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarTipoPessoa")
	public String pesquisarTipoPessoa(Model model, TipoClienteImpl tipoCliente, BindingResult bindingResult,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(tipoCliente, habilitado);

		Collection<TabelaAuxiliar> listaTabelaAuxiliar =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, TipoClienteImpl.class.getName());

		model.addAttribute("listaTipoCliente", listaTabelaAuxiliar);
		model.addAttribute(TIPO_CLIENTE, tipoCliente);
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);

		return this.exibirPesquisaTipoPessoa(model);
	}

	/**
	 * Método responsável por exibir a tela de inserir Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param tipoCliente - {@link TipoClienteImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("exibirInclusaoTipoPessoa")
	public String exibirInclusaoTipoPessoa(Model model, HttpServletRequest request, TipoClienteImpl tipoCliente,
			BindingResult bindingResult) throws GGASException {

		this.prepararCampos(model);

		if (isPostBack(request)) {
			model.addAttribute(TIPO_CLIENTE, tipoCliente);
		}

		saveToken(request);

		return "exibirInclusaoTipoPessoa";
	}

	/**
	 * Método responsável por inserir um Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param tipoCliente - {@link TipoClienteImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirTipoPessoa")
	public String incluirTipoPessoa(Model model, HttpServletRequest request, TipoClienteImpl tipoCliente, BindingResult bindingResult) {

		String view = "forward:/exibirInclusaoTipoPessoa";
		try {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoCliente.getDescricao(), TipoClienteImpl.class.getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoCliente);
			controladorTabelaAuxiliar.pesquisarTipoClienteTabelaAuxiliar(tipoCliente, TITULO_TABELA_AUXILIAR);
			controladorTabelaAuxiliar.inserir(tipoCliente);
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, TITULO_TABELA_AUXILIAR);
			view = this.pesquisarTipoPessoa(model, tipoCliente, bindingResult, Boolean.TRUE);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param chavePrimaria - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoTipoPessoa")
	public String exibirDetalhamentoTipoPessoa(Model model, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		String view = "exibirDetalhamentoTipoPessoa";

		try {
			TipoCliente tipoCliente = (TipoCliente) controladorTabelaAuxiliar.obter(chavePrimaria, TipoClienteImpl.class);
			model.addAttribute(TIPO_CLIENTE, tipoCliente);
		} catch (GGASException e) {
			this.mensagemErroParametrizado(model, e);
			view = FORWARD_EXIBIR_PESQUISA_TIPO_PESSOA;
		}

		return view;
	}

	/**
	 * Método responsável por remover um Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param chavesPrimarias
	 * @return String {@link String}
	 */
	@RequestMapping("removerTipoPessoa")
	public String removerTipoPessoa(Model model, HttpServletRequest request,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) {

		String view = FORWARD_PESQUISAR_TIPO_PESSOA;

		try {
			getDadosAuditoria(request);

			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TipoClienteImpl.class, getDadosAuditoria(request));

			super.mensagemSucesso(model, Util.getRemoveMsgTabelaAuxiliar(TipoClienteImpl.class), TITULO_TABELA_AUXILIAR);
			view = this.pesquisarTipoPessoa(model, null, null, Boolean.TRUE);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param tipoClienteParam - {@link TipoClienteImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAlteracaoTipoPessoa")
	public String exibirAlteracaoTipoPessoa(Model model, HttpServletRequest request, TipoClienteImpl tipoClienteParam,
			BindingResult bindingResult) {

		String view = "exibirAlteracaoTipoPessoa";

		try {
			TipoCliente tipoCliente = tipoClienteParam;
			
			this.prepararCampos(model);
			
			if (!super.isPostBack(request)) {

				Long chavePrimaria = tipoCliente.getChavePrimaria();

				tipoCliente = (TipoClienteImpl) controladorTabelaAuxiliar.obter(chavePrimaria, TipoClienteImpl.class);
			}

			model.addAttribute(TIPO_CLIENTE, tipoCliente);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_PESQUISA_TIPO_PESSOA;
		}

		return view;
	}

	/**
	 * Método responsável por atualizar um Tipo de Pessoa.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param tipoCliente - {@link TipoClienteImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarTipoPessoa")
	public String alterarTipoPessoa(Model model, HttpServletRequest request, TipoClienteImpl tipoCliente, BindingResult bindingResult) {

		String view = "forward:/exibirAlteracaoTipoPessoa";

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoCliente.getDescricao(), TipoClienteImpl.class.getName());
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoCliente);
			controladorTabelaAuxiliar.pesquisarTipoClienteTabelaAuxiliar(tipoCliente, TITULO_TABELA_AUXILIAR);
			controladorTabelaAuxiliar.atualizar(tipoCliente, tipoCliente.getClass());
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, TITULO_TABELA_AUXILIAR);
			view = this.pesquisarTipoPessoa(model, tipoCliente, bindingResult, tipoCliente.isHabilitado());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private Map<String, Object> prepararFiltro(TipoClienteImpl tipoCliente, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (tipoCliente != null) {
			
			if (tipoCliente.getDescricao() != null && !StringUtils.isEmpty(tipoCliente.getDescricao())) {
				filtro.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, tipoCliente.getDescricao());
			}

			if (tipoCliente.getChavePrimaria() > 0) {
				filtro.put(TabelaAuxiliar.ATRIBUTO_CHAVE_PRIMARIA, tipoCliente.getChavePrimaria());
			}

			EsferaPoder esferaPoder = tipoCliente.getEsferaPoder();
			if (esferaPoder != null && esferaPoder.getChavePrimaria() != -1) {
				filtro.put(ID_ESFERA_PODER, esferaPoder.getChavePrimaria());
			}

			TipoPessoa tipoPessoa = tipoCliente.getTipoPessoa();
			if (tipoPessoa != null && tipoPessoa.getCodigo() != -1) {
				filtro.put(ID_TIPO_PESSOA, tipoPessoa.getCodigo());
			}
		}

		if (habilitado != null) {
			filtro.put(TabelaAuxiliar.ATRIBUTO_HABILITADO, habilitado);
		}

		return filtro;
	}
}
