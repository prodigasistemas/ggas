/*
 * 
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo
 * sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA;
 * sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa;
 * se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 * 
 */

package br.com.ggas.web.cadastro.localidade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.impl.RedeImpl;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * 
 * @author pedro 
 * 
 * 
 * Classe responsável pelas telas relacionadas a Rede
 * 
 * 
 * 
 */
@Controller
public class RedeAction extends GenericAction {

	private static final String LIMPA_TRONCO = "limpaTronco";

	private static final String PRESSAO = "pressao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_UNIDADE_MEDICAO_PRESSAO = "listaUnidadeMedicaoPressao";

	private static final String LISTA_UNIDADE_MEDICAO_EXTENSAO = "listaUnidadeMedicaoExtensao";

	private static final String DESCRICAO = "descricao";

	private static final String INDEX_LISTA = "indexLista";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String REQUEST_LISTA_REDE_TRONCO = "listaRedeTronco";

	private static final String ID_TRONCO = "idTronco";

	private static final String ID_CITY_GATE = "idCityGate";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String REDE = "rede";

	@Autowired
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	private ControladorTronco controladorTronco;

	@Autowired
	private ControladorRede controladorRede;

	@Autowired
	private ControladorUnidade controladorUnidade;

	/**
	 * 
	 * Método responsável pela exibição da tela de pesquisa de rede.
	 * 
	 * @param model
	 * @return
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("exibirPesquisaRede")
	public String exibirPesquisaRede(Model model) throws GGASException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		carregarCamposFiltro(model);

		return "exibirPesquisaRede";
	}

	/**
	 * 
	 * Método responsável pela pesquisa de rede.
	 * 
	 * @param rede
	 * @param result
	 * @param model
	 * @param habilitado
	 * @param idTronco
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("pesquisarRede")
	public String pesquisarRede(RedeImpl rede, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = ID_TRONCO, required = false) Long idTronco) throws GGASException {

		Map<String, Object> filtro = obterFiltroForm(rede, habilitado, idTronco);
		popularModelPesquisarRede(model, filtro, habilitado, idTronco, rede);

		return exibirPesquisaRede(model);
	}

	/**
	 * 
	 * Método responsável por popular um model
	 * 
	 * @param model
	 * @param filtro
	 * @param habilitado
	 * @param idTronco
	 * @param rede
	 * @throws NegocioException
	 * 
	 */
	private void popularModelPesquisarRede(Model model, Map<String, Object> filtro, Boolean habilitado, Long idTronco,
			RedeImpl rede) throws NegocioException {
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(ID_TRONCO, idTronco);
		model.addAttribute(REDE, rede);
		model.addAttribute("listaRede", controladorRede.consultarRedes(filtro));
	}

	/**
	 * 
	 * Método responsável por preparar um filtro.
	 * 
	 * @param rede
	 * @param habilitado
	 * @param idTronco
	 * @return
	 * @throws GGASException
	 * 
	 */
	private Map<String, Object> obterFiltroForm(RedeImpl rede, Boolean habilitado, Long idTronco) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (rede != null && !StringUtils.isEmpty(rede.getDescricao())) {
			filtro.put(DESCRICAO, rede.getDescricao());
		}

		if (rede != null && rede.getLocalidade() != null && rede.getLocalidade().getChavePrimaria() > 0) {
			filtro.put(ID_LOCALIDADE, rede.getLocalidade().getChavePrimaria());
		}

		if (idTronco != null && idTronco > 0) {
			filtro.put(ID_TRONCO, idTronco);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * 
	 * Método responsável por carregar campos.
	 * 
	 * @param model
	 * @throws GGASException
	 * 
	 */
	private void carregarCampos(Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(HABILITADO, true);

		model.addAttribute("listaLocalidade", controladorLocalidade.consultarLocalidades(filtro));
		model.addAttribute("listaRedeDiametro", controladorRede.listarRedeDiametro());
		model.addAttribute("listaRedeMaterial", controladorRede.listarRedeMaterial());
		model.addAttribute("listaCityGate", controladorTronco.listarCityGateExistente());
		model.addAttribute(LISTA_UNIDADE_MEDICAO_PRESSAO, controladorUnidade.listarUnidadesPressao());
		model.addAttribute(LISTA_UNIDADE_MEDICAO_EXTENSAO, controladorUnidade.listarUnidadesExtensao());
	}

	/**
	 * 
	 * @param model
	 * @throws GGASException
	 * 
	 */
	private void carregarCamposFiltro(Model model) throws GGASException {

		Map<String, Object> filtroLocalidade = new HashMap<String, Object>();
		filtroLocalidade.put(HABILITADO, Boolean.TRUE);
		model.addAttribute("listaLocalidade", controladorLocalidade.consultarLocalidades(filtroLocalidade));

	}

	/**
	 * 
	 * Método responsável pela exibição da tela de detalhamento de rede
	 * 
	 * @param chavePrimaria
	 * @param model
	 * @param request
	 * @param redeForm
	 * @param result
	 * @param habilitado
	 * @return String
	 * @throws GGASException
	 * @throws Exception
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirDetalhamentoRede")
	public String exibirDetalhamentoRede(RedeImpl redeForm, BindingResult result,
			@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam(HABILITADO) Boolean habilitado,
			Model model, HttpServletRequest request) throws GGASException {
		
		String view = "exibirDetalhamentoRede";
		model.addAttribute("redeForm", redeForm);
		model.addAttribute(HABILITADO, habilitado);
		Collection<RedeDistribuicaoTronco> listaRedeTronco = new ArrayList<>();
		Rede rede;
		carregarCampos(model);
		try {
			rede = (Rede) controladorRede.obter(chavePrimaria, Rede.PROPRIEDADES_LAZY);
			model.addAttribute(REDE, rede);
			if (!super.isPostBack(request)) {
				if (rede != null) {
					listaRedeTronco.addAll(controladorRede.listarRedeTroncoPorRede(rede));
				}
				request.getSession().setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
			} else {
				listaRedeTronco = (Collection<RedeDistribuicaoTronco>) request.getSession()
						.getAttribute(REQUEST_LISTA_REDE_TRONCO);
			}
		} catch (GGASException e) {
			view = "forward:pesquisarRede";
			mensagemErroParametrizado(model, e);
		}

		request.setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);

		saveToken(request);

		return view;
	}

	/**
	 * 
	 * Método responsável pela exibição da tela de inclusão de rede.
	 * 
	 * @param model
	 * @param request
	 * @param idCityGate
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirInsercaoRede")
	public String exibirInsercaoRede(Model model, HttpServletRequest request,
			@RequestParam(value =ID_CITY_GATE, required = false) Long idCityGate) throws GGASException {

		Collection<RedeDistribuicaoTronco> listaRedeTronco = new ArrayList<RedeDistribuicaoTronco>();
		model.addAttribute(ID_CITY_GATE, idCityGate);
		carregarCampos(model);
		if (!super.isPostBack(request)) {
			request.getSession().setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);

		} else {
			listaRedeTronco = (Collection<RedeDistribuicaoTronco>) request.getSession()
					.getAttribute(REQUEST_LISTA_REDE_TRONCO);
		}

		if (idCityGate != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_CITY_GATE, idCityGate);
			filtro.put(HABILITADO, Boolean.TRUE);
			Collection<Tronco> listaTronco = controladorTronco.consultarTronco(filtro);
			model.addAttribute("listaTronco", listaTronco);
		}

		model.addAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		saveToken(request);

		return "exibirInsercaoRede";
	}

	/**
	 * 
	 * Método responsável pela adição de um tronco a rede.
	 * 
	 * @param rede
	 * @param result
	 * @param idTronco
	 * @param pressao
	 * @param extensao
	 * @param telaAlteracao
	 * @param indexLista
	 * @param request
	 * @param model
	 * @param idCityGate
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarTroncoNaRede")
	public String adicionarTroncoNaRede(RedeImpl rede, BindingResult result,
			@RequestParam(value = ID_TRONCO, required = false) Long idTronco,
			@RequestParam(value =ID_CITY_GATE, required = false) Long idCityGate,
			@RequestParam(value = INDEX_LISTA, required = false) Integer indexLista, HttpServletRequest request,
			Model model) throws GGASException {

		String view;
		String pressao = null;
		String extensao = null;
		Boolean telaAlteracao = null;
		model.addAttribute(ID_CITY_GATE, idCityGate);
		model.addAttribute(ID_TRONCO, idTronco);
		if (request.getParameter(PRESSAO) != null) {
			pressao = request.getParameter(PRESSAO);
		}
		if (request.getParameter("extensao") != null) {
			extensao = request.getParameter("extensao");
		}
		if (request.getParameter("telaAlteracao") != null) {
			telaAlteracao = Boolean.valueOf(request.getParameter("telaAlteracao"));
		}

		List<RedeDistribuicaoTronco> listaRedeTronco = (List<RedeDistribuicaoTronco>) request.getSession()
				.getAttribute(REQUEST_LISTA_REDE_TRONCO);
		model.addAttribute(PRESSAO, pressao);
		popularRede(rede, extensao, pressao, request);

		try {
			validarToken(request);

			if (indexLista == null || indexLista < 0) {
				RedeDistribuicaoTronco redeTronco = (RedeDistribuicaoTronco) controladorRede
						.criarRedeDistribuicaoTronco();
				redeTronco.setDadosAuditoria(getDadosAuditoria(request));
				this.popularRedeDistribuicaoTronco(redeTronco, idTronco);
				if (listaRedeTronco == null) {
					listaRedeTronco = new ArrayList<RedeDistribuicaoTronco>();
				} else {
					controladorRede.validarTroncoPorCityGate(redeTronco, listaRedeTronco);
				}
				listaRedeTronco.add(redeTronco);
				model.addAttribute(LIMPA_TRONCO, true);
			} else {
				this.alterarTroncoNaRede(indexLista, idTronco, listaRedeTronco);
				model.addAttribute(LIMPA_TRONCO, true);
			}

		} catch (GGASException e) {
			model.addAttribute(LIMPA_TRONCO, false);
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute(REDE, rede);
		request.getSession().setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
		model.addAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		if (telaAlteracao != null && telaAlteracao) {
			view = exibirAtualizacaoRede(rede, result, model, rede.getChavePrimaria(), idCityGate, rede.isHabilitado(), request);
		} else {
			view = exibirInsercaoRede(model, request, idCityGate);
		}

		return view;
	}

	/**
	 * 
	 * Método responsável por alterar um Tronco na Rede.
	 * 
	 * @param indexLista
	 * @param idTronco
	 * @param listaRedeTronco
	 * @throws NegocioException
	 * 
	 */
	private void alterarTroncoNaRede(Integer indexLista, Long idTronco, List<RedeDistribuicaoTronco> listaRedeTronco)
			throws NegocioException {

		if (listaRedeTronco != null && indexLista != null) {
			RedeDistribuicaoTronco redeTroncoAlterado = (RedeDistribuicaoTronco) controladorRede
					.criarRedeDistribuicaoTronco();
			this.popularRedeDistribuicaoTronco(redeTroncoAlterado, idTronco);
			RedeDistribuicaoTronco redeTroncoExistente = null;
			for (int i = 0; i < listaRedeTronco.size(); i++) {
				redeTroncoExistente = listaRedeTronco.get(i);
				if (i == indexLista && redeTroncoExistente != null) {
					this.popularRedeDistribuicaoTronco(redeTroncoExistente, idTronco);
				}
			}
		}
	}

	/**
	 * 
	 * Método responsável pela remoção de um tronco da uma rede.
	 * 
	 * @param rede
	 * @param result
	 * @param telaAlteracao
	 * @param indexLista
	 * @param model
	 * @param request
	 * @param response
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerTroncoNaRede")
	public String removerTroncoNaRede(RedeImpl rede, BindingResult result,
			@RequestParam(value = "alteracao", required = false) Boolean telaAlteracao,
			@RequestParam(INDEX_LISTA) Integer indexLista, Model model, HttpServletRequest request,
			HttpServletResponse response) throws GGASException {
		String view;
		Collection<RedeDistribuicaoTronco> listaRedeTronco = (Collection<RedeDistribuicaoTronco>) request.getSession()
				.getAttribute(REQUEST_LISTA_REDE_TRONCO);
		if (request.getParameter(PRESSAO) != null) {
			model.addAttribute(PRESSAO, request.getParameter(PRESSAO));
		}
		try {
			validarToken(request);
			if (listaRedeTronco != null && indexLista != null) {
				((List) listaRedeTronco).remove(indexLista.intValue());
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
		}

		request.getSession().setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
		model.addAttribute(REDE, rede);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		if (telaAlteracao != null && telaAlteracao) {
			view = exibirAtualizacaoRede(rede, result, model, rede.getChavePrimaria(), null, rede.isHabilitado(), request);
		} else {
			view = exibirInsercaoRede(model, request, null);
		}

		return view;
	}

	/**
	 * 
	 * @param redeTronco
	 * @param idTronco
	 * @throws NegocioException
	 * 
	 */
	private void popularRedeDistribuicaoTronco(RedeDistribuicaoTronco redeTronco, Long idTronco)
			throws NegocioException {

		if (idTronco != null && idTronco.intValue() > 0) {
			Tronco tronco = (Tronco) controladorTronco.obter(idTronco);
			redeTronco.setTronco(tronco);
		}

		redeTronco.setHabilitado(Boolean.TRUE);
		redeTronco.setUltimaAlteracao(Calendar.getInstance().getTime());
		controladorRede.validarDadosRedeDistribuicaoTronco(redeTronco);
	}

	/**
	 * 
	 * Método responsável pela inclusão de uma rede
	 * 
	 * @param model
	 * @param rede
	 * @param result
	 * @param pressao
	 * @param extensao
	 * @param request
	 * @param idTronco
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("inserirRede")
	public String inserirRede(Model model, RedeImpl rede, BindingResult result,
			@RequestParam(value = PRESSAO, required = false) String pressao,
			@RequestParam(value = "extensao", required = false) String extensao, 
			@RequestParam(value = ID_TRONCO, required = false) Long idTronco, HttpServletRequest request)
			throws GGASException {

		if (request.getParameter(ID_CITY_GATE) != null) {
			model.addAttribute(ID_CITY_GATE, Long.parseLong(request.getParameter(ID_CITY_GATE)));
		}
		model.addAttribute(ID_TRONCO, idTronco);
		String view = "forward:exibirInsercaoRede";
		model.addAttribute(PRESSAO, pressao);
		popularRede(rede, extensao, pressao, request);
		model.addAttribute(REDE, rede);
		carregarCampos(model);
		try {
			validarToken(request);
			Long idRede = controladorRede.inserir(rede);
			model.addAttribute(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idRede);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Rede.REDE_CAMPO_STRING);
			view = pesquisarRede(rede, null, model, rede.isHabilitado(), null);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
		}

		model.addAttribute(REDE, rede);

		return view;
	}

	/**
	 * 
	 * Método responsável pela remoção de uma rede
	 * 
	 * @param model
	 * @param chavesPrimarias
	 * @param habilitado
	 * @param request
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("removerRede")
	public String removerRede(Model model, @RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam(HABILITADO) Boolean habilitado, HttpServletRequest request) throws GGASException {

		try {
			controladorRede.validarRemoverRede(chavesPrimarias);
			controladorRede.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Rede.REDE_CAMPO_STRING);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarRede(null, null, model, habilitado, null);
	}

	/**
	 * 
	 * @param rede
	 * @param extensao
	 * @param pressao
	 * @param request
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void popularRede(RedeImpl rede, String extensao, String pressao, HttpServletRequest request) {

		try {

			rede.setDadosAuditoria(getDadosAuditoria(request));

			Collection<RedeDistribuicaoTronco> listaRedeTronco = (Collection<RedeDistribuicaoTronco>) request
					.getSession().getAttribute(REQUEST_LISTA_REDE_TRONCO);

			if (!StringUtils.isEmpty(extensao)) {
				rede.setExtensao(Util.converterCampoStringParaValorBigDecimal(Rede.EXTENSAO, extensao,
						Constantes.FORMATO_VALOR_FATOR_K, Constantes.LOCALE_PADRAO));
			}

			if (!StringUtils.isEmpty(pressao)) {
				rede.setPressao(Util.converterCampoStringParaValorBigDecimal(Rede.PRESSAO, pressao,
						Constantes.FORMATO_VALOR_FATOR_K, Constantes.LOCALE_PADRAO));
			}

			if (listaRedeTronco != null && !listaRedeTronco.isEmpty()) {
				rede.getListaRedeTronco().clear();
				RedeDistribuicaoTronco redeTronco = null;
				for (Iterator<RedeDistribuicaoTronco> iterator = listaRedeTronco.iterator(); iterator.hasNext();) {
					redeTronco = iterator.next();
					redeTronco.setChavePrimaria(0);
					redeTronco.setRede(rede);
					redeTronco.setDadosAuditoria(super.getDadosAuditoria(request));
					rede.getListaRedeTronco().add(redeTronco);
				}
			} else {
				rede.getListaRedeTronco().clear();
			}
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	/**
	 * 
	 * Método responsável pela exibição da tela de atualização de rede.
	 * 
	 * @param model
	 * @param chavePrimaria
	 * @param idCityGate
	 * @param request
	 * @param redeForm
	 * @param habilitado
	 * @param result
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	@RequestMapping("exibirAtualizacaoRede")
	public String exibirAtualizacaoRede(RedeImpl redeForm, BindingResult result, Model model,
			@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = ID_CITY_GATE, required = false) Long idCityGate,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request) throws GGASException {
		
		String view = "exibirAtualizacaoRede";
		Collection<RedeDistribuicaoTronco> listaRedeTronco = new ArrayList<RedeDistribuicaoTronco>();
		model.addAttribute("redeForm", redeForm);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(ID_CITY_GATE, idCityGate);
		if (request.getParameter(ID_TRONCO) != null) {
			model.addAttribute(ID_TRONCO, Long.parseLong(request.getParameter(ID_TRONCO)));
		}
		if (!super.isPostBack(request)) {

			try {
				Rede rede = (Rede) controladorRede.obter(chavePrimaria, Rede.PROPRIEDADES_LAZY);
				model.addAttribute(REDE, rede);

				if (rede.getPressao() != null) {
					model.addAttribute(PRESSAO, rede.getPressao() + ",00");
				}

				listaRedeTronco.addAll(controladorRede.listarRedeTroncoPorRede(rede));
				request.getSession().setAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);
			} catch (GGASException e) {
				view = "forward:pesquisarRede";
				mensagemErroParametrizado(model, e);
			}

		} else {
			listaRedeTronco = (Collection<RedeDistribuicaoTronco>) request.getSession()
					.getAttribute(REQUEST_LISTA_REDE_TRONCO);
		}

		carregarCampos(model);
		model.addAttribute(REQUEST_LISTA_REDE_TRONCO, listaRedeTronco);

		if (idCityGate != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_CITY_GATE, idCityGate);
			filtro.put(HABILITADO, Boolean.TRUE);
			Collection<Tronco> listaTronco = controladorTronco.consultarTronco(filtro);
			model.addAttribute("listaTronco", listaTronco);
		}
		saveToken(request);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return view;
	}

	/**
	 * 
	 * Método responsável pela atualização de uma rede
	 * 
	 * @param model
	 * @param rede
	 * @param result
	 * @param pressao
	 * @param extensao
	 * @param request
	 * @param idTronco
	 * @return String
	 * @throws GGASException
	 * @throws Exception
	 * 
	 */
	@RequestMapping("atualizarRede")
	public String atualizarRede(RedeImpl rede, BindingResult result,
			@RequestParam(value = PRESSAO, required = false) String pressao,
			@RequestParam(value = "extensao", required = false) String extensao,
			@RequestParam(value = ID_TRONCO, required = false) Long idTronco, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "forward:exibirAtualizacaoRede";
		if (request.getParameter(ID_CITY_GATE) != null) {
			model.addAttribute(ID_CITY_GATE, Long.parseLong(request.getParameter(ID_CITY_GATE)));
		}
		model.addAttribute(PRESSAO, pressao);
		model.addAttribute(ID_TRONCO, idTronco);
		carregarCampos(model);

		try {
			validarToken(request);
			popularRede(rede, extensao, pressao, request);
			controladorRede.atualizar(rede);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Rede.REDE_CAMPO_STRING);
			view = pesquisarRede(rede, result, model, rede.isHabilitado(), null);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
		}

		model.addAttribute(REQUEST_LISTA_REDE_TRONCO, rede.getListaRedeTronco());
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		model.addAttribute(REDE, rede);

		return view;
	}

}
