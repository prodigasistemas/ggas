/*
 * 
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 * 
 */
package br.com.ggas.web.cadastro.operacional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.impl.EnderecoImpl;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author pedro
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das views referentes a City Gate.
 */
@Controller
public class CityGateAction extends GenericAction {

	private static final String CITY_GATE = "cityGate";

	private static final String ENDERECO = "endereco";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final Class<CityGateImpl> CLASSE = CityGateImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.operacional.impl.CityGateImpl";

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	/**
	 * Método responsável pela exibição da tela de pesquisa relacionada ao City Gate
	 * 
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirPesquisaCityGate")
	public String exibirPesquisaCityGate(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		carregarLocalidade(model);
		return "exibirPesquisaCityGate";
	}

	/**
	 * Método responsável pela pesquisa dos City Gates registrados
	 * 
	 * @param cityGate
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarCityGates")
	public String pesquisarCityGate(CityGateImpl cityGate, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = montarFiltro(cityGate, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(CITY_GATE, cityGate);
		model.addAttribute("listaCityGate", controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING));
		carregarLocalidade(model);

		return "exibirPesquisaCityGate";
	}

	/**
	 * Método responsável pela exibição da tela relacionada ao cadastro do City Gate
	 * 
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirInclusaoCityGate")
	public String exibirInclusaoCityGate(Model model) throws NegocioException {

		carregarLocalidade(model);

		return "exibirInclusaoCityGate";
	}

	/**
	 * Método responsável pela inclusão de uma entidade do tipo City Gate
	 * 
	 * @param cityGate
	 * @param result
	 * @param endereco
	 * @param results
	 * @param cepNumero
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("incluirCityGate")
	public String incluirCityGate(CityGateImpl cityGate, BindingResult result, EnderecoImpl endereco, BindingResult results,
					@RequestParam("cep") String cepNumero, HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		model.addAttribute(CITY_GATE, cityGate);
		model.addAttribute(ENDERECO, endereco);
		model.addAttribute("cep", cepNumero);

		Map<String, Object> erros = cityGate.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			view = exibirInclusaoCityGate(model);
		} else {
			try {
				getDadosAuditoria(request);
				if (endereco != null) {
					verificarCep(endereco, cepNumero);
					cityGate.setEndereco(endereco);
				}
				controladorTabelaAuxiliar.inserir(cityGate);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, CityGate.CITY_GATE);
				view = pesquisarCityGate(cityGate, results, cityGate.isHabilitado(), model);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
				view = exibirInclusaoCityGate(model);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela relacionada a alteração de dados do City Gate
	 * 
	 * @param cityGateTmp
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirAlteracaoCityGate")
	public String exibirAlteracaoCityGate(CityGateImpl cityGateTmp, BindingResult result, HttpServletRequest request, Model model)
					throws NegocioException {

		CityGateImpl cityGate = cityGateTmp;
		carregarLocalidade(model);
		if (!isPostBack(request)) {
			try {
				cityGate = (CityGateImpl) controladorTabelaAuxiliar.obter(cityGate.getChavePrimaria(), CLASSE);
				model.addAttribute(CITY_GATE, cityGate);
				model.addAttribute(ENDERECO, cityGate.getEndereco());
				if (cityGate.getEndereco() != null && cityGate.getEndereco().getCep() != null) {
					model.addAttribute("cep", cityGate.getEndereco().getCep().getCep());
				}
				model.addAttribute("paginaHelp", "/consultadoscitygates.htm");
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		return "exibirAlteracaoCityGate";
	}

	/**
	 * Método relacionado a alteração de dados do City Gate
	 * 
	 * @param cityGate
	 * @param result
	 * @param endereco
	 * @param results
	 * @param cepNumero
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("alterarCityGate")
	public String alterarCityGate(CityGateImpl cityGate, BindingResult result, EnderecoImpl endereco, BindingResult results,
					@RequestParam("cep") String cepNumero, HttpServletRequest request, Model model) throws GGASException {

		String view = null;

		model.addAttribute(CITY_GATE, cityGate);
		model.addAttribute(ENDERECO, endereco);
		model.addAttribute("cep", cepNumero);

		Map<String, Object> erros = cityGate.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			view = exibirAlteracaoCityGate(cityGate, results, request, model);
		} else {
			try {
				getDadosAuditoria(request);
				if (endereco != null) {
					verificarCep(endereco, cepNumero);
					cityGate.setEndereco(endereco);
				}
				controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(cityGate);
				controladorTabelaAuxiliar.atualizar(cityGate, CLASSE);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, CityGate.CITY_GATE);
				view = pesquisarCityGate(cityGate, results, cityGate.isHabilitado(), model);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
				view = exibirAlteracaoCityGate(cityGate, results, request, model);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela remoção do registro de um City Gate
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerCityGate")
	public String removerCityGate(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, CityGate.CITY_GATE);
			return pesquisarCityGate(null, null, Boolean.TRUE, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarCityGates";
	}

	/**
	 * Método responsável pela exibição da tela relacionada ao detalhamento de um City Gate
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("exibirDetalharCityGate")
	public String exibirDetalharCityGate(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
					throws NegocioException {

		try {
			CityGateImpl cityGate = (CityGateImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(CITY_GATE, cityGate);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirDetalharCityGate";
	}

	/**
	 * Método responsável por carregar a lista de localidades
	 * 
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarLocalidade(Model model) throws NegocioException {

		Collection<Localidade> listaLocalidades = controladorLocalidade.listarLocalidades();
		model.addAttribute("listaLocalidades", listaLocalidades);
	}

	/**
	 * Método responsável por verificar o cep
	 * 
	 * @param endereco
	 * @param cepNumero
	 * @throws NegocioException
	 */
	private void verificarCep(EnderecoImpl endereco, String cepNumero) throws NegocioException {

		if (!StringUtils.isEmpty(cepNumero)) {
			Cep cep = controladorEndereco.obterCep(cepNumero);
			if (cep != null) {
				endereco.setCep(cep);
			}
		} else {
			endereco.setCep(null);
		}
	}

	/**
	 * Método responsável por montar filtro utilizado em pesquisa.
	 * 
	 * @param cityGate
	 * @param habilitado
	 * @return Map<String, Object>
	 */
	private Map<String, Object> montarFiltro(CityGateImpl cityGate, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (cityGate != null) {

			if (!StringUtils.isEmpty(cityGate.getDescricao())) {
				filtro.put(DESCRICAO, cityGate.getDescricao());
			}

			if (!StringUtils.isEmpty(cityGate.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", cityGate.getDescricaoAbreviada());
			}

			if (cityGate.getLocalidade() != null) {
				filtro.put("idLocalidade", cityGate.getLocalidade().getChavePrimaria());
			}

		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
