package br.com.ggas.web.cadastro.cliente;

/**
 * 
 * @author pedro
 *
 * Classe responsável pela representação da entidade ClienteFone 
 */
public class ClienteFoneVO {

	private Integer codigoDDD;

	private Integer numero;

	private Integer ramal;

	private Boolean indicadorPrincipal;
	
	private Long tipoFone;

	private Integer clienteResponsavel;

	public Integer getCodigoDDD() {
		return codigoDDD;
	}

	public void setCodigoDDD(Integer codigoDDD) {
		this.codigoDDD = codigoDDD;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getRamal() {
		return ramal;
	}

	public void setRamal(Integer ramal) {
		this.ramal = ramal;
	}

	public Boolean getIndicadorPrincipal() {
		return indicadorPrincipal;
	}

	public void setIndicadorPrincipal(Boolean indicadorPrincipal) {
		this.indicadorPrincipal = indicadorPrincipal;
	}

	public Long getTipoFone() {
		return tipoFone;
	}

	public void setTipoFone(Long tipoFone) {
		this.tipoFone = tipoFone;
	}

	public Integer getClienteResponsavel() {
		return clienteResponsavel;
	}

	public void setClienteResponsavel(Integer clienteResponsavel) {
		this.clienteResponsavel = clienteResponsavel;
	}
}
