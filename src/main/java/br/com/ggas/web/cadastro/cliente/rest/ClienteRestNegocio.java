/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 */

package br.com.ggas.web.cadastro.cliente.rest;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.geral.exception.NegocioException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Armazena a lógica de negócio (camada entre o controller rest e os métodos de acesso a banco de dados)
 * da entidade de cliente
 *
 * @author jose.victor@logiquesistemas.com.br
 */
public class ClienteRestNegocio {

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controlador;

	/**
	 * Consulta uma lista de clientes no banco a partir de uma pesquisa
	 *
	 * @param pesquisa dto com as informações de pesquisa
	 * @return retorna a lista de clientes que casam com o resultado
	 * @throws NegocioException exceção lançada caso haja alguma erro durante o processamento
	 */
	public Collection<Cliente> buscarClientes(PesquisaClienteDTO pesquisa) throws NegocioException {
		Map<String, Object> filtro = this.construirFiltro(pesquisa);
		return controlador.consultarClientes(filtro);
	}

	/**
	 * Busca clientes de forma paginada
	 * @param pesquisa pesquisa dos clientes
	 * @param quantidadeMax quantidade máxima de clientes por página
	 * @param pagina pagina a ser buscada
	 * @return retorna a lista de clientes
	 */
	public Collection<Cliente> buscarClientesPaginados(PesquisaClienteDTO pesquisa, int quantidadeMax, int pagina) {
		Map<String, Object> filtro = this.construirFiltro(pesquisa);
		return controlador.consultarClientesPaginados(filtro, quantidadeMax, pagina);

	}

	/**
	 * Responsável por construir os filtros de busca de um cliente a partir de uma pesquisa
	 *
	 * @param pesquisa pesquisa a ser identificada
	 * @return retorna os filtros de pesquisa construídos
	 */
	private Map<String, Object> construirFiltro(PesquisaClienteDTO pesquisa) {
		Map<String, Object> filtro = new HashMap<>();

		incluirSePreenchido(filtro, pesquisa.getNome(), FiltroCliente.NOME);
		incluirSePreenchido(filtro, pesquisa.getNomeFantasia(), FiltroCliente.NOME_FANTASIA);
		incluirSePreenchido(filtro, pesquisa.getCnpj(), FiltroCliente.CNPJ);
		incluirSePreenchido(filtro, pesquisa.getCpf(), FiltroCliente.CPF);
		incluirSePreenchido(filtro, pesquisa.getNumeroPassaporte(), FiltroCliente.PASSAPORTE);
		incluirSePreenchido(filtro, pesquisa.getInscricaoEstadual(), FiltroCliente.INSCRICAO_ESTADUAL);
		incluirSePreenchido(filtro, pesquisa.getInscricaoMunicipal(), FiltroCliente.INSCRICAO_MUNICIPAL);
		incluirSePreenchido(filtro, pesquisa.getInscricaoRural(), FiltroCliente.INSCRICAO_RURAL);
		incluirSePreenchido(filtro, pesquisa.getCep(), FiltroCliente.CEP);
		incluirSePreenchido(filtro, pesquisa.getNomeGenerico(), FiltroCliente.NOME_GENERICO);

		filtro.put(FiltroCliente.HABILITADO.getChave(), true);

		return filtro;
	}

	/**
	 * Inclui filtro de cliente se o valor informado estiver preenchido
	 *
	 * @param filtro mapa de filtros
	 * @param valor valor a ser adicionado
	 * @param tipoFiltro tipo do filtro
	 */
	private void incluirSePreenchido(Map<String, Object> filtro, String valor, FiltroCliente tipoFiltro) {
		if (!StringUtils.isEmpty(valor)) {
			filtro.put(tipoFiltro.getChave(), valor.trim());
		}
	}

}
