/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.operacional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.endereco.impl.EnderecoImpl;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.operacional.ControladorErp;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.operacional.impl.ErpImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Controller responsável por exibir as telas da entidade Erp.
 * 
 */
@Controller
public class ErpAction extends GenericAction {

	private static final String FORWARD_PESQUISAR_ERP = "forward:pesquisarErp";

	private static final String ERP = "erp";

	private static final String EXIBIR_ATUALIZACAO_ERP = "exibirAtualizacaoErp";

	private static final String EXIBIR_INSERIR_ERP = "exibirInserirErp";

	private static final String HABILITADO = "habilitado";

	private static final String ID_GERENCIA_REGIONAL = "idGerenciaRegional";

	private static final String INDICADOR_MEDICAO = "indicadorMedicao";

	private static final String INDICADOR_PRESSAO = "indicadorPressao";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String CHAVE_CEP_ZERO = "0";

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorErp")
	private ControladorErp controladorErp;

	@Autowired
	@Qualifier("controladorGerenciaRegional")
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	@Qualifier("controladorTronco")
	private ControladorTronco controladorTronco;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	/**
	 * Método responsável por exibir a tela de pesquisa de ERP.
	 * 
	 * @param model
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("exibirPesquisaErp")
	public String exibirPesquisaErp(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, null);
		}

		if (!model.containsAttribute(INDICADOR_MEDICAO)) {
			model.addAttribute(INDICADOR_MEDICAO, null);
		}

		if (!model.containsAttribute(INDICADOR_PRESSAO)) {
			model.addAttribute(INDICADOR_PRESSAO, null);
		}

		model.addAttribute("listaLocalidade", controladorLocalidade.listarLocalidades());

		return "exibirPesquisaErp";
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa da quadra.
	 * 
	 * @param erp
	 * @param result
	 * @param indicadorMedicao
	 * @param indicadorPressao
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("pesquisarErp")
	public String pesquisarErp(ErpImpl erp, BindingResult result,
			@RequestParam(value = INDICADOR_MEDICAO, required = false) Boolean indicadorMedicao,
			@RequestParam(value = INDICADOR_PRESSAO, required = false) Boolean indicadorPressao,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);

		Map<String, Object> filtro = prepararFiltro(erp, indicadorMedicao, indicadorPressao, habilitado);

		Collection<Erp> listaErp = controladorErp.consultarERP(filtro);

		carregarCampos(null, model);

		model.addAttribute("listaErp", listaErp);
		model.addAttribute(ERP, erp);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(INDICADOR_MEDICAO, indicadorMedicao);
		model.addAttribute(INDICADOR_PRESSAO, indicadorPressao);

		return exibirPesquisaErp(model);
	}

	/**
	 * Método responsável por exibir a tela de inserir de ERP.
	 * 
	 * @param erp
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping(EXIBIR_INSERIR_ERP)
	public String exibirInserirErp(ErpImpl erp, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		saveToken(request);
		carregarCampos(null, model);
		model.addAttribute(ERP, erp);
		return EXIBIR_INSERIR_ERP;
	}

	/**
	 * Método responsável por inserir um Erp.
	 * 
	 * @param erp
	 * @param result
	 * @param endereco
	 * @param numeroCep
	 * @param idGerenciaRegional
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("inserirErp")
	public String inserirErp(ErpImpl erp, BindingResult result, EnderecoImpl endereco,
			@RequestParam(value = "cep", required = false) String numeroCep,
			@RequestParam(value = "gerenciaRegional", required = false) Long idGerenciaRegional,
			HttpServletRequest request, Model model) throws GGASException {

		String retorno = EXIBIR_INSERIR_ERP;
		carregarCampos(idGerenciaRegional, model);

		erp.setEndereco(endereco);

		model.addAttribute(ID_GERENCIA_REGIONAL, idGerenciaRegional);
		model.addAttribute("numeroCep", numeroCep);
		model.addAttribute("endereco", endereco);
		model.addAttribute(ERP, erp);

		Map<String, Object> erros = erp.validarDados();

		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
		} else {

			try {
				validarToken(request);
				verificarMedicaoPressao(erp);
				validarEndereco(erp, endereco, numeroCep);

				erp.setHabilitado(true);
				erp.setDadosAuditoria(getDadosAuditoria(request));

				controladorErp.inserir(erp);

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Erp.ESTACAO);
				retorno = pesquisarErp(erp, result, erp.isMedicao(), erp.isPressao(), erp.isHabilitado(), request,
						model);
			} catch (GGASException e) {
				saveToken(request);
				mensagemErroParametrizado(model, request, e);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de ERP.
	 * 
	 * @param erpTmp
	 * @param result
	 * @param habilitado
	 * @param indicadorMedicao
	 * @param indicadorPressao
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("exibirDetalhamentoErp")
	public String exibirDetalhamentoErp(ErpImpl erpTmp, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = INDICADOR_MEDICAO, required = false) Boolean indicadorMedicao,
			@RequestParam(value = INDICADOR_PRESSAO, required = false) Boolean indicadorPressao,
			HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);

		model.addAttribute("erpForm", erpTmp);

		if (!validarEntidadeInexistente(erpTmp.getChavePrimaria(), habilitado, indicadorMedicao, indicadorPressao,
				request, model)) {
			return FORWARD_PESQUISAR_ERP;
		}

		return "exibirDetalhamentoErp";
	}

	/**
	 * Método responsável por exibir a tela de Atualização de ERP.
	 * 
	 * @param erpTmp
	 * @param result
	 * @param habilitado
	 * @param indicadorMedicao
	 * @param indicadorPressao
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping(EXIBIR_ATUALIZACAO_ERP)
	public String exibirAtualizacaoErp(ErpImpl erpTmp, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = INDICADOR_MEDICAO, required = false) Boolean indicadorMedicao,
			@RequestParam(value = INDICADOR_PRESSAO, required = false) Boolean indicadorPressao,
			HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);

		model.addAttribute("erpForm", erpTmp);

		if (!validarEntidadeInexistente(erpTmp.getChavePrimaria(), habilitado, indicadorMedicao, indicadorPressao,
				request, model)) {
			return FORWARD_PESQUISAR_ERP;
		}

		return EXIBIR_ATUALIZACAO_ERP;
	}

	/**
	 * Método responsável por pesquisar ERP.
	 * 
	 * @param erp
	 * @param result
	 * @param endereco
	 * @param numeroCep
	 * @param idGerenciaRegional
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("atualizarErp")
	public String atualizarErp(ErpImpl erp, BindingResult result, EnderecoImpl endereco,
			@RequestParam(value = "cep", required = false) String numeroCep,
			@RequestParam(value = "gerenciaRegional", required = false) Long idGerenciaRegional,
			HttpServletRequest request, Model model) throws GGASException {

		String retorno = EXIBIR_ATUALIZACAO_ERP;
		carregarCampos(idGerenciaRegional, model);

		erp.setEndereco(endereco);

		model.addAttribute(ID_GERENCIA_REGIONAL, idGerenciaRegional);
		model.addAttribute("numeroCep", numeroCep);
		model.addAttribute(ERP, erp);

		Map<String, Object> erros = erp.validarDados();

		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
		} else {

			try {
				validarToken(request);
				verificarMedicaoPressao(erp);
				validarEndereco(erp, endereco, numeroCep);

				erp.setDadosAuditoria(getDadosAuditoria(request));

				controladorErp.atualizar(erp);

				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Erp.ESTACAO);
				retorno = pesquisarErp(erp, result, erp.isMedicao(), erp.isPressao(), erp.isHabilitado(), request,
						model);
			} catch (GGASException e) {
				saveToken(request);
				mensagemErroParametrizado(model, request, e);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por excluir a(s) ERP(s) do sistema.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 * 
	 */
	@RequestMapping("removerErp")
	public String removerERP(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws NegocioException {

		try {
			controladorErp.validarERPsSelecionadas(chavesPrimarias);
			controladorErp.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Erp.ERP_CAMPO_STRING);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_PESQUISAR_ERP;
	}

	/**
	 * Método responsável por validar um Endereco
	 * 
	 * @param erp
	 * @param endereco
	 * @param numeroCep
	 * @throws GGASException
	 * 
	 */
	@SuppressWarnings("deprecation")
	private void validarEndereco(ErpImpl erp, EnderecoImpl endereco, String numeroCep) throws GGASException {

		Cep cep = null;

		if ((CHAVE_CEP_ZERO.equals(String.valueOf(endereco.getCep().getChavePrimaria()))
				&& (StringUtils.isEmpty(numeroCep)))) {
			endereco.setCep(cep);
		} else if ((endereco.getCep().getCep() == null)
				&& (CHAVE_CEP_ZERO.equals((endereco.getCep().getChavePrimaria() + "")))) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CEP_INEXISTENTE);
		} else if ((endereco.getCep().getCep() != null)
				&& (!CHAVE_CEP_ZERO.equals(String.valueOf(endereco.getCep().getChavePrimaria())))) {
			cep = controladorEndereco.obterCepPorChave(endereco.getCep().getChavePrimaria());
			endereco.setCep(cep);
		}

		if (!Constantes.SEM_NUMERO.equals(endereco.getNumero())) {
			Util.validarDominioCaracteres(Endereco.ENDERECO_NUMERO, endereco.getNumero(),
					Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
		}
	}

	/**
	 * Método responsável por verificar se uma entidade é inexistente
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param indicadorMedicao
	 * @param indicadorPressao
	 * @param request
	 * @param model
	 * @return Boolean
	 * @throws GGASException
	 * 
	 */
	public Boolean validarEntidadeInexistente(Long chavePrimaria, Boolean habilitado, Boolean indicadorMedicao,
			Boolean indicadorPressao, HttpServletRequest request, Model model) throws GGASException {

		ErpImpl erp;

		try {
			erp = (ErpImpl) controladorErp.obter(chavePrimaria);
			carregarCampos(erp.getLocalidade().getGerenciaRegional().getChavePrimaria(), model);
			model.addAttribute(ERP, erp);
			model.addAttribute(HABILITADO, habilitado);
			model.addAttribute(INDICADOR_MEDICAO, indicadorMedicao);
			model.addAttribute(INDICADOR_PRESSAO, indicadorPressao);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			return false;
		}

		return true;
	}

	/**
	 * Método responsável por verificar Medicao e Pressao
	 * 
	 * @param erp
	 * @throws NegocioException
	 * 
	 */
	private void verificarMedicaoPressao(ErpImpl erp) throws NegocioException {

		if (!erp.isMedicao() && !erp.isPressao()) {
			throw new NegocioException(ControladorErp.ERRO_NEGOCIO_SELECIONAR_CAMPO_MEDICAO_REGULAGEM_DE_PRESSAO, true);
		}
	}

	/**
	 * Método responsável por carregar campos.
	 * 
	 * @param idGerenciaRegional
	 * @param model
	 * @throws GGASException
	 * 
	 */
	private void carregarCampos(Long idGerenciaRegional, Model model) throws GGASException {

		Collection<Localidade> listaLocalidade = new ArrayList<Localidade>();

		Map<String, Object> filtroGerenciaRegional = new HashMap<String, Object>();
		filtroGerenciaRegional.put(HABILITADO, true);

		Collection<GerenciaRegional> listaGerenciaRegional = controladorGerenciaRegional
				.consultarGerenciaRegional(filtroGerenciaRegional);

		if ((idGerenciaRegional != null) && (idGerenciaRegional > 0)) {
			Map<String, Object> filtroLocalidade = new HashMap<String, Object>();
			filtroLocalidade.put(ID_GERENCIA_REGIONAL, idGerenciaRegional);
			listaLocalidade = controladorLocalidade.consultarLocalidades(filtroLocalidade);
		}

		Map<String, Object> fitroTronco = new HashMap<String, Object>();
		fitroTronco.put(HABILITADO, true);
		Collection<Tronco> listaTronco = controladorTronco.consultarTronco(fitroTronco);

		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);
		model.addAttribute("listaLocalidade", listaLocalidade);
		model.addAttribute("listaTronco", listaTronco);
	}

	/**
	 * Método responsável por preparar o filtro.
	 * 
	 * @param erp
	 * @param indicadorMedicao
	 * @param indicadorPressao
	 * @param habilitado
	 * @return filtro
	 * 
	 */
	private Map<String, Object> prepararFiltro(ErpImpl erp, Boolean indicadorMedicao, Boolean indicadorPressao,
			Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (!StringUtils.isEmpty(erp.getNome())) {
			filtro.put("nome", erp.getNome());
		}

		if (erp.getLocalidade() != null && erp.getLocalidade().getChavePrimaria() > 0) {
			filtro.put(ID_LOCALIDADE, erp.getLocalidade().getChavePrimaria());
		}

		if (indicadorMedicao != null) {
			filtro.put("medicao", indicadorMedicao);
		}

		if (indicadorPressao != null) {
			filtro.put("pressao", indicadorPressao);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
