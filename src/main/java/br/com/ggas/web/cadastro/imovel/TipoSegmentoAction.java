package br.com.ggas.web.cadastro.imovel;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.cadastro.imovel.impl.TipoSegmentoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe responsável pelas telas relacionadas aos tipos de relacionamento.
 */
@Controller
public class TipoSegmentoAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISA_TIPO_SEGMENTO = "exibirPesquisaTipoSegmento";
	private static final String TIPO_SEGMENTO = "tipoSegmento";
	private static final String DESCRICAO = "descricao";
	private static final String HABILITADO = "habilitado";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela tela de pesquisa do tipo de segmento
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_TIPO_SEGMENTO)
	public String exibirPesquisaTipoSegmento(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return TELA_EXIBIR_PESQUISA_TIPO_SEGMENTO;
	}

	/**
	 * Método responsável pela pesquisa dos tipos de segmento
	 * 
	 * @param tipoSegmento
	 *            - {@link TipoSegmentoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarTipoSegmento")
	public String pesquisarTipoSegmento(TipoSegmentoImpl tipoSegmento, BindingResult result,
			@RequestParam("habilitado") Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(tipoSegmento, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(TIPO_SEGMENTO, tipoSegmento);
		model.addAttribute("listaTipoSegmento",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, TipoSegmentoImpl.class.getName()));

		return TELA_EXIBIR_PESQUISA_TIPO_SEGMENTO;
	}

	/**
	 * Método responsável pela exibição da tela de inclusão dos tipos de segmento
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirInclusaoTipoSegmento")
	public String exibirInclusaoTipoSegmento(Model model) {

		return "exibirInclusaoTipoSegmento";
	}

	/**
	 * Método responsável pela inserção dos tipos de segmento
	 * 
	 * @param tipoSegmento
	 *            - {@link TipoSegmentoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("inserirTipoSegmento")
	public String inserirTipoSegmento(TipoSegmentoImpl tipoSegmento, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		String view = "exibirInclusaoTipoSegmento";

		model.addAttribute(TIPO_SEGMENTO, tipoSegmento);
		Map<String, Object> erros = tipoSegmento.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			view = exibirInclusaoTipoSegmento(model);
		} else {
			try {
				controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoSegmento.getDescricao(),
						TipoSegmentoImpl.class.getName());
				controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(tipoSegmento.getDescricaoAbreviada(),
						TipoSegmentoImpl.class.getName());
				controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoSegmento, TipoSegmento.TIPO_SEGMENTO);
				controladorTabelaAuxiliar.inserir(tipoSegmento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TipoSegmento.TIPO_SEGMENTO);
				view = pesquisarTipoSegmento(tipoSegmento, result, tipoSegmento.isHabilitado(), model);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração dos tipos de segmento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoTipoSegmento")
	public String exibirAlteracaoTipoSegmento(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "exibirAlteracaoTipoSegmento";
		TipoSegmentoImpl tipoSegmento;
		try {
			tipoSegmento = (TipoSegmentoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, TipoSegmentoImpl.class);
			model.addAttribute(TIPO_SEGMENTO, tipoSegmento);
		} catch (GGASException e) {
			model.addAttribute(HABILITADO, true);
			mensagemErroParametrizado(model, request, e);
			view = "exibirPesquisaTipoSegmento";
		}

		return view;
	}

	/**
	 * Método responsável pela alteração dos tipos de segmento
	 * 
	 * @param tipoSegmento
	 *            - {@link TipoSegmentoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("alterarTipoSegmento")
	public String alterarTipoSegmento(TipoSegmentoImpl tipoSegmento, BindingResult result, HttpServletRequest request,
			Model model) {
		String view = "exibirAlteracaoTipoSegmento";

		model.addAttribute(TIPO_SEGMENTO, tipoSegmento);
		Map<String, Object> erros = tipoSegmento.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
		} else {
			try {
				controladorTabelaAuxiliar.validarDadosEntidade(tipoSegmento);
				controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoSegmento.getDescricao(),
						TipoSegmentoImpl.class.getName());
				controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(tipoSegmento.getDescricaoAbreviada(),
						TipoSegmentoImpl.class.getName());
				controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoSegmento, TipoSegmentoImpl.TIPO_SEGMENTO);
				controladorTabelaAuxiliar.atualizar(tipoSegmento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TipoSegmentoImpl.TIPO_SEGMENTO);
				view = pesquisarTipoSegmento(tipoSegmento, result, tipoSegmento.isHabilitado(), model);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
			}
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de detalhamento dos tipos de
	 * segmento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalharTipoSegmento")
	public String exibirDetalharTipoSegmento(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) {
		String view = "exibirDetalharTipoSegmento";

		try {
			model.addAttribute(TIPO_SEGMENTO, controladorTabelaAuxiliar.obter(chavePrimaria, TipoSegmentoImpl.class));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:exibirPesquisaTipoSegmento";
		}

		return view;
	}

	/**
	 * Método responsável pela remoção dos tipos de segmento
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param tipoSegmento
	 *            - {@link TipoSegmentoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerTipoSegmento")
	public String removerTipoSegmento(TipoSegmentoImpl tipoSegmento, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(TIPO_SEGMENTO, tipoSegmento);
		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TipoSegmentoImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, TipoSegmentoImpl.TIPO_SEGMENTO);
			return pesquisarTipoSegmento(null, null, true, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarTipoSegmento(tipoSegmento, result, true, model);
	}

	/**
	 * Método responsável por popular o filtro pra pesquisa dos tipos de segmento
	 * 
	 * @param tipoSegmento
	 *            - {@link TipoSegmentoImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> popularFiltro(TipoSegmentoImpl tipoSegmento, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (tipoSegmento != null) {

			if (tipoSegmento.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", tipoSegmento.getChavePrimaria());
			}

			if (!StringUtils.isEmpty(tipoSegmento.getDescricao())) {
				filtro.put(DESCRICAO, tipoSegmento.getDescricao());
			}

			if (!StringUtils.isEmpty(tipoSegmento.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", tipoSegmento.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
