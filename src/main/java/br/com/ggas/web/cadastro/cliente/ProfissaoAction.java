/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.cadastro.cliente;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a Profissão.
 */
@Controller
public class ProfissaoAction extends GenericAction {
	
	private static final int NUMERO_MAXIMO_DESCRICAO = 50;

	private static final String EXIBIR_PESQUISA_PROFISSAO = "exibirPesquisaProfissao";

	private static final String PROFISSAO = "profissao";

	private static final String EXIBIR_ATUALIZAR_PROFISSAO = "exibirAtualizarProfissao";

	private static final String EXIBIR_INSERIR_PROFISSAO = "exibirInserirProfissao";

	private static final Class<ProfissaoImpl> CLASSE = ProfissaoImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.cliente.impl.ProfissaoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_PROFISSAO = "listaProfissao";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa profissão.
	 * 
	 * @param model
	 * @param session
	 * @return String
	 */
	@RequestMapping(EXIBIR_PESQUISA_PROFISSAO)
	public String exibirPesquisaProfissao(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_PROFISSAO;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de profissão.
	 * 
	 * @param profissao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarProfissao")
	public String pesquisarProfissao(ProfissaoImpl profissao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(profissao, habilitado);

		Collection<TabelaAuxiliar> listaProfissao = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_PROFISSAO, listaProfissao);
		model.addAttribute(PROFISSAO, profissao);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_PROFISSAO;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento profissão.
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirDetalhamentoProfissao")
	public String exibirDetalhamentoProfissao(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaProfissao";
		
		try {
			ProfissaoImpl profissao = (ProfissaoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(PROFISSAO, profissao);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoProfissao";
		} catch (NegocioException e) {
			mensagemErro(model, e);
			
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir profissão.
	 * 
	 * @return String
	 */
	@RequestMapping(EXIBIR_INSERIR_PROFISSAO)
	public String exibirInserirEquipamento() {

		return EXIBIR_INSERIR_PROFISSAO;
	}

	/**
	 * Método responsável por inserir profissão.
	 * 
	 * @param profissao
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirProfissao")
	private String inserirProfissao(ProfissaoImpl profissao, BindingResult result, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = EXIBIR_INSERIR_PROFISSAO;

		try {

			if (profissao.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(profissao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(profissao, "Profissão");
			controladorTabelaAuxiliar.inserir(profissao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Profissao.PROFISSAO_ROTULO);
			retorno = pesquisarProfissao(profissao, result, profissao.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute("profissao", profissao);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar profissão.
	 * 
	 * @param profissaoImpl
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_PROFISSAO)
	public String exibirAtualizarProfissao(ProfissaoImpl profissaoImpl, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		String retorno = "forward:exibirPesquisaProfissao";
		
		ProfissaoImpl profissao = profissaoImpl;

		try {
			profissao = (ProfissaoImpl) controladorTabelaAuxiliar.obter(profissao.getChavePrimaria(), CLASSE);
			model.addAttribute(PROFISSAO, profissao);
			model.addAttribute(HABILITADO, habilitado);
			retorno = EXIBIR_ATUALIZAR_PROFISSAO;
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover profissão.
	 * 
	 * @param profissao
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * @throws ConcorrenciaException
	 */
	@RequestMapping("atualizarProfissao")
	private String atualizarProfissao(ProfissaoImpl profissao, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = EXIBIR_ATUALIZAR_PROFISSAO;

		try {

			if (profissao.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(profissao);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(profissao, "Profissão");
			controladorTabelaAuxiliar.atualizar(profissao, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Profissao.PROFISSAO_ROTULO);
			retorno = pesquisarProfissao(profissao, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(PROFISSAO, profissao);
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o profissão.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerProfissao")
	public String removerProfissao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = "forward:pesquisarProfissao";
		
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Profissao.PROFISSAO_ROTULO);
			retorno = pesquisarProfissao(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param profissao
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(ProfissaoImpl profissao, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (profissao != null) {

			if (profissao.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, profissao.getChavePrimaria());
			}

			if (profissao.getDescricao() != null && !profissao.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, profissao.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
