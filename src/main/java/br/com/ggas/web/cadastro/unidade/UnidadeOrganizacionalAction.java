/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.cadastro.unidade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cadastro.unidade.impl.UnidadeOrganizacionalImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas a Unidade Organizacional.
 *
 */

@Controller
public class UnidadeOrganizacionalAction extends GenericAction {

	private static final String LISTA_UNIDADE_TIPO = "listaUnidadeTipo";

	private static final String LISTA_CANAL_ATENDIMENTO = "listaCanalAtendimento";

	private static final String EMPRESAS = "empresas";

	private static final String LISTA_LOCALIDADE = "listaLocalidade";

	private static final String LISTA_GERENCIA_REGIONAL = "listaGerenciaRegional";

	private static final Logger LOG = Logger.getLogger(UnidadeOrganizacionalAction.class);

	private static final String HABILITADO = "habilitado";

	private static final String UNIDADE_ORGANIZACIONAL = "unidadeOrganizacional";

	private static final String EMAIL_CONTATO = "emailContato";

	private static final String INDICADOR_ABRE_CHAMADO = "indicadorAbreChamado";

	private static final String INDICADOR_TRAMITE = "indicadorTramite";

	private static final String DESCRICAO = "descricaoUnidade";

	private static final String ID_EMPRESA = "idEmpresa";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String ID_CANAL_ATENDIMENTO = "idCanalAtendimento";

	private static final String ID_UNIDADE_TIPO = "idUnidadeTipo";

	private static final String ID_UNIDADE_SUPERIOR = "idUnidadeSuperior";

	private static final String ID_UNIDADE_CENTRALIZADORA = "idUnidadeCentralizadora";

	private static final String SIGLA = "sigla";

	private static final String INICIO_EXPEDIENTE = "inicioExpediente";

	private static final String FIM_EXPEDIENTE = "fimExpediente";

	private static final String GERENCIA_REGIONAL = "idGerenciaRegional";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final Integer VALOR_INICIAL_PARA_TRATAR_DATAS = 11;

	private static final Integer VALOR_FINAL_PARA_TRATAR_DATAS = 16;

	private String retorno = "";

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	@Qualifier("controladorGerenciaRegional")
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	@Qualifier("controladorQuadra")
	private ControladorQuadra controladorQuadra;

	/**
	 * Método responsável por exibir a tela de pesquisa de unidade organizacional.
	 *
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @return tela pesquisa Unidade organizacional - {@link - String}
	 * @throws GGASException - {@link - GGASException} Caso ocorra algum erro
	 */

	@RequestMapping("exibirPesquisaUnidadeOrganizacional")
	public String exibirPesquisarUnidadeOrganizacional(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional) throws GGASException {

		carregarCamposFiltro(model, unidadeOrganizacional);

		return "exibirPesquisaUnidadeOrganizacional";
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de unidade organizacional.
	 *
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param result - {@link - BindinResult}
	 * @param habilitado - {@link - Boolean}
	 * @param niveisUnidadeTipo - {@link - String}
	 * @param indicadorAbreChamado - {@link - Boolean}
	 * @param indicadorTramite - {@link - Boolean}
	 * @return as pesquisas das unidades organizacionais - {@link - String}
	 * @throws GGASException Caso ocorra algum erro
	 */

	@RequestMapping("pesquisarUnidadeOrganizacional")
	public String pesquisarUnidadeOrganizacional(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = "niveisUnidadeTipo", required = false) String niveisUnidadeTipo,
			@RequestParam(value = INDICADOR_ABRE_CHAMADO, required = false) Boolean indicadorAbreChamado,
			@RequestParam(value = INDICADOR_TRAMITE, required = false) Boolean indicadorTramite) throws GGASException {

		model.addAttribute(CHAVES_PRIMARIAS, null);

		Map<String, Object> filtro = obterFiltroForm(unidadeOrganizacional, niveisUnidadeTipo, habilitado, indicadorAbreChamado,
				indicadorTramite);

		model.addAttribute("listaUnidadeOrganizacional", controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro));
		montarObjetoRetornoPesquisa(model, unidadeOrganizacional, niveisUnidadeTipo, indicadorAbreChamado, indicadorTramite, habilitado);

		return exibirPesquisarUnidadeOrganizacional(model, unidadeOrganizacional);
	}

	/**
	 * Método responsável por capturar os atributos e setar no model, para ser retornado no JSP.
	 * 
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param niveisUnidadeTipo - {@link - String}
	 * @param indicadorAbreChamado - {@link - Boolean}
	 * @param indicadorTramite - {@link - Boolean}
	 * @param habilitado - {@link - Habilitado}
	 */
	public void montarObjetoRetornoPesquisa(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional, String niveisUnidadeTipo,
			Boolean indicadorAbreChamado, Boolean indicadorTramite, Boolean habilitado) {

		model.addAttribute(UNIDADE_ORGANIZACIONAL, unidadeOrganizacional);
		model.addAttribute("niveisUnidadeTipo", niveisUnidadeTipo);
		model.addAttribute(INDICADOR_TRAMITE, indicadorTramite);
		model.addAttribute(INDICADOR_ABRE_CHAMADO, indicadorAbreChamado);
		model.addAttribute(HABILITADO, habilitado);

	}

	/**
	 * Método responsável por exibir a tela de inserir unidade organizacional.
	 *
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @return tela de inserir unidade organizacional - {@link - String}
	 * @throws GGASException - {@link - GGASException} Caso ocorra algum erro
	 */

	@RequestMapping("exibirInserirUnidadeOrganizacional")
	public String exibirInserirUnidadeOrganizacional(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional) throws GGASException {

		carregarCampos(model, unidadeOrganizacional);

		return "exibirInserirUnidadeOrganizacional";
	}

	/**
	 * Carregar campos para ser exibidos nos formulários.
	 *
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @throws GGASException caso ocorra algum erro
	 */
	private void carregarCampos(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional) throws GGASException {

		Collection<Localidade> listaLocalidade = new ArrayList<Localidade>();
		Collection<UnidadeOrganizacional> listaUnidadeSuperior = new ArrayList<UnidadeOrganizacional>();
		Collection<UnidadeOrganizacional> listaUnidadeRepavimentadora = new ArrayList<UnidadeOrganizacional>();
		Collection<Empresa> listaEmpresas = new ArrayList<Empresa>();

		Map<String, Object> filtroLocalidade = new HashMap<String, Object>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);

		if (unidadeOrganizacional != null && unidadeOrganizacional.getChavePrimaria() > 0
				&& unidadeOrganizacional.getGerenciaRegional() != null) {
			filtroLocalidade.put(GERENCIA_REGIONAL, unidadeOrganizacional.getGerenciaRegional().getChavePrimaria());
			listaLocalidade = controladorLocalidade.consultarLocalidades(filtroLocalidade);
		}

		if (unidadeOrganizacional != null) {
			model.addAttribute(DESCRICAO, unidadeOrganizacional.getDescricao());
			model.addAttribute(SIGLA, unidadeOrganizacional.getSigla());
		}

		model.addAttribute(LISTA_GERENCIA_REGIONAL, controladorGerenciaRegional.consultarGerenciaRegional(filtro));
		model.addAttribute(LISTA_LOCALIDADE, listaLocalidade);
		model.addAttribute(EMPRESAS, listaEmpresas);
		model.addAttribute(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addAttribute(LISTA_UNIDADE_TIPO, controladorUnidadeOrganizacional.listarUnidadeTipo());
		model.addAttribute("listaMunicipio", controladorMunicipio.consultarMunicipios(filtro));
		model.addAttribute("listaUnidadeSuperior", listaUnidadeSuperior);
		filtro.put("tipoUnidadeTipo", UnidadeTipo.CENTRALIZADORA);
		model.addAttribute("listaUnidadeCentralizadora", controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro));
		model.addAttribute("listaUnidadeRepavimentadora", listaUnidadeRepavimentadora);
	}

	/**
	 * Carregar campos filtro.
	 * 
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @throws GGASException caso ocorra algum erro
	 */
	private void carregarCamposFiltro(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional) throws GGASException {

		Map<String, Object> filtroLocalidade = new HashMap<String, Object>();

		if (unidadeOrganizacional.getGerenciaRegional() != null) {
			filtroLocalidade.put(GERENCIA_REGIONAL, unidadeOrganizacional.getGerenciaRegional().getChavePrimaria());
		} else {
			filtroLocalidade.put(GERENCIA_REGIONAL, null);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(HABILITADO, Boolean.TRUE);

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		model.addAttribute(LISTA_GERENCIA_REGIONAL, controladorGerenciaRegional.consultarGerenciaRegional(filtro));
		model.addAttribute(LISTA_LOCALIDADE, controladorLocalidade.consultarLocalidades(filtroLocalidade));
		model.addAttribute(LISTA_UNIDADE_TIPO, controladorUnidadeOrganizacional.listarUnidadeTipo());
		model.addAttribute(EMPRESAS, controladorEmpresa.consultarEmpresas(null));
		model.addAttribute(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addAttribute("listaCentralizadoras", controladorUnidadeOrganizacional.listarUnidadesCentralizadoras());
		model.addAttribute("listaSuperiores", controladorUnidadeOrganizacional.listarUnidadesSuperiores());
	}

	/**
	 * Método responsável por inserir uma unidade organizacional.
	 * 
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param result - {@link - BindingResult}
	 * @param inicioExpediente - {@link - String}
	 * @param fimExpediente - {@link - String}
	 * @param request - {@link - HttpServletRequest}
	 * @param model - {@link - Model}
	 * @return a tela de pesquisar com a unidade inserida. - {@link - String}
	 * @throws GGASException caso ocorra algum erro
	 */
	@RequestMapping("inserirUnidadeOrganizacional")
	public String inserirUnidadeOrganizacional(UnidadeOrganizacionalImpl unidadeOrganizacional, BindingResult result,
			@RequestParam(INICIO_EXPEDIENTE) String inicioExpediente, @RequestParam(FIM_EXPEDIENTE) String fimExpediente,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			model.addAttribute(UNIDADE_ORGANIZACIONAL, unidadeOrganizacional);
			popularUnidadeOrganizacional(unidadeOrganizacional, inicioExpediente, fimExpediente, request, model);
			controladorUnidadeOrganizacional.inserir(unidadeOrganizacional);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, UnidadeOrganizacional.UNIDADE_ORGANIZACIONAL);
			retorno = pesquisarUnidadeOrganizacional(model, unidadeOrganizacional, result, Boolean.TRUE,
					String.valueOf(unidadeOrganizacional.getUnidadeTipo().getNivel()), unidadeOrganizacional.isIndicadorAbreChamado(),
					unidadeOrganizacional.isIndicadorTramite());
		} catch (FormatoInvalidoException | NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, request, e);
			retorno = exibirInserirUnidadeOrganizacional(model, unidadeOrganizacional);
		}
		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar funcionário.
	 *
	 * @param model - {@link - Model}
	 * @param chavePrimaria - {@link - Long}
	 * @return tela de atualizar unidade organizacional - {@link - String}
	 * @throws GGASException Caso ocorra algum erro
	 */

	@RequestMapping("exibirAtualizarUnidadeOrganizacional")
	public String exibirAtualizarUnidadeOrganizacional(Model model, @RequestParam(value = CHAVE_PRIMARIA) Long chavePrimaria)
			throws GGASException {

		UnidadeOrganizacionalImpl unidadeOrganizacional = (UnidadeOrganizacionalImpl) controladorUnidadeOrganizacional.obter(chavePrimaria);

		montarObjetosHorariosUnidadeOrganizacional(unidadeOrganizacional, model);

		model.addAttribute(UNIDADE_ORGANIZACIONAL, unidadeOrganizacional);
		carregarCampos(model, unidadeOrganizacional);

		return "exibirAtualizarUnidadeOrganizacional";
	}

	/**
	 * Prepara os dados de data para serem exibidos na tela de alterar
	 * 
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param model - {@link - Model}
	 */
	private void montarObjetosHorariosUnidadeOrganizacional(UnidadeOrganizacionalImpl unidadeOrganizacional, Model model) {

		String valorInicioExpediente = String.valueOf(unidadeOrganizacional.getInicioExpediente());
		String valorFimExpediente = String.valueOf(unidadeOrganizacional.getFimExpediente());

		String inicioExpediente =
				valorInicioExpediente.substring(VALOR_INICIAL_PARA_TRATAR_DATAS, VALOR_FINAL_PARA_TRATAR_DATAS).replace(":", "");
		String fimExpediente =
				valorFimExpediente.substring(VALOR_INICIAL_PARA_TRATAR_DATAS, VALOR_FINAL_PARA_TRATAR_DATAS).replace(":", "");

		model.addAttribute(INICIO_EXPEDIENTE, inicioExpediente);
		model.addAttribute(FIM_EXPEDIENTE, fimExpediente);

	}

	/**
	 * Método responsável por atualizar a unidade organizacional.
	 * 
	 * @param model - {@link - Model}
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param result - {@link - BindingResult}
	 * @param request - {@link - HttpServletRequest}
	 * @param inicioExpediente - {@link - String}
	 * @param fimExpediente - {@link - String}
	 * @return a tela de pesquisar com a unidade atualizada - {@link - String}
	 * @throws GGASException - {@link - GGASException} caso ocorra algum erro
	 */
	@RequestMapping("atualizarUnidadeOrganizacional")
	public String atualizarUnidadeOrganizacional(Model model, UnidadeOrganizacionalImpl unidadeOrganizacional, BindingResult result,
			HttpServletRequest request, @RequestParam(INICIO_EXPEDIENTE) String inicioExpediente,
			@RequestParam(FIM_EXPEDIENTE) String fimExpediente) throws GGASException {

		try {
			popularUnidadeOrganizacional(unidadeOrganizacional, inicioExpediente, fimExpediente, request, model);
			controladorUnidadeOrganizacional.atualizar(unidadeOrganizacional);
			retorno = pesquisarUnidadeOrganizacional(model, unidadeOrganizacional, result, unidadeOrganizacional.isHabilitado(),
					String.valueOf(unidadeOrganizacional.getUnidadeTipo().getNivel()), unidadeOrganizacional.isIndicadorAbreChamado(),
					unidadeOrganizacional.isIndicadorTramite());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, UnidadeOrganizacional.UNIDADE_ORGANIZACIONAL);
		} catch (FormatoInvalidoException | NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, request, e);
			retorno = exibirAtualizarUnidadeOrganizacional(model, unidadeOrganizacional.getChavePrimaria());
		}

		return retorno;
	}

	/**
	 * Método responsável por tratas dados para inseriruma unidade organizacional.
	 * 
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param inicioExpediente - {@link - String}
	 * @param fimExpediente - {@link - String}
	 * @param request - {@link - HttpServletRequest}
	 * @param model - {@link - Model}
	 * @throws GGASException - {@link - GGASException} caso ocorra algum erro
	 */
	private void popularUnidadeOrganizacional(UnidadeOrganizacionalImpl unidadeOrganizacional, String inicioExpediente,
			String fimExpediente, HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(INICIO_EXPEDIENTE, inicioExpediente);
		model.addAttribute(FIM_EXPEDIENTE, fimExpediente);

		unidadeOrganizacional.setHabilitado(true);
		unidadeOrganizacional.setDadosAuditoria(getDadosAuditoria(request));

		if (!StringUtils.isEmpty(inicioExpediente)) {
			Util.validarHora(inicioExpediente, UnidadeOrganizacional.HORA_INICIO_EXPEDIENTE);
			Date dataInicio = Util.converterCampoStringParaData(UnidadeOrganizacional.HORA_INICIO_EXPEDIENTE,
					gerarDataHora(inicioExpediente), Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
			unidadeOrganizacional.setInicioExpediente(dataInicio);
		} else {
			unidadeOrganizacional.setInicioExpediente(null);
		}

		if (!StringUtils.isEmpty(fimExpediente)) {
			Util.validarHora(fimExpediente, UnidadeOrganizacional.HORA_FIM_EXPEDIENTE);
			Date dataFim = Util.converterCampoStringParaData(UnidadeOrganizacional.HORA_FIM_EXPEDIENTE, gerarDataHora(fimExpediente),
					Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
			unidadeOrganizacional.setFimExpediente(dataFim);
		} else {
			unidadeOrganizacional.setFimExpediente(null);
		}

		if (!unidadeOrganizacional.getEmailContato().isEmpty()) {
			Util.validarEmail(unidadeOrganizacional.getEmailContato());
		}

	}

	/**
	 * Gerar data hora.
	 *
	 * @param inicioExpediente the inicio expediente
	 * @return the string
	 */
	private String gerarDataHora(String inicioExpediente) {

		Date data = Calendar.getInstance().getTime();
		String dataAtual = Util.converterDataParaStringSemHora(data, Constantes.FORMATO_DATA_BR);
		return dataAtual + " " + inicioExpediente;
	}

	/**
	 * Método responsável por remover a unidade organizacional.
	 * 
	 * @param chavesPrimarias - {@link - chavesPrimarias}
	 * @param request - {@link - HttpServletRequest}
	 * @param model - {@link - Model}
	 * @return A tela de pesquisar com a unidade removida {@link - String}
	 * @throws NegocioException - {@link - NegocioException} caso ocorra algum erro.
	 */
	@RequestMapping("removerUnidadeOrganizacional")
	public String removerUnidadeOrganizacional(@RequestParam(value = CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws NegocioException {

		try {
			controladorQuadra.validarQuadrasSelecionadas(chavesPrimarias);
			controladorUnidadeOrganizacional.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, UnidadeOrganizacional.UNIDADE_ORGANIZACIONAL);
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_NEGOCIO_REMOVER_UNIDADE_ORGANIZACIONAL));
		}

		return "forward:pesquisarUnidadeOrganizacional";
	}

	/**
	 * Obtem filtro form
	 * @param unidadeOrganizacional - {@link - UnidadeOrganizacionalImpl}
	 * @param niveisUnidadeTipo - {@link - String}
	 * @param habilitado - {@link - Boolean}
	 * @param indicadorAbreChamado - indicador de abertura de chamado
	 * @param indicadorTramite - indicador de abertura de tramite
	 * @return  retorna os filtros
	 * @throws FormatoInvalidoException caso ocorra alguma falha na montagem do filtro
	 */
	private Map<String, Object> obterFiltroForm(UnidadeOrganizacionalImpl unidadeOrganizacional, String niveisUnidadeTipo,
			Boolean habilitado, Boolean indicadorAbreChamado, Boolean indicadorTramite ) throws FormatoInvalidoException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, unidadeOrganizacional.getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && (unidadeOrganizacional.getUnidadeTipo() != null)
				&& unidadeOrganizacional.getUnidadeTipo().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_TIPO, unidadeOrganizacional.getUnidadeTipo().getChavePrimaria());
		}

		if (niveisUnidadeTipo != null && !niveisUnidadeTipo.isEmpty()) {
			filtro.put("nivelTipoUnidade", Util.converterCampoStringParaValorInteger("Nivel", niveisUnidadeTipo));
		}

		if ((unidadeOrganizacional != null) && (unidadeOrganizacional.getDescricao() != null)
				&& (unidadeOrganizacional.getDescricao().trim().length() > 0)) {
			filtro.put(DESCRICAO, unidadeOrganizacional.getDescricao());
		}

		if ((unidadeOrganizacional != null) && (unidadeOrganizacional.getEmailContato() != null)
				&& (unidadeOrganizacional.getEmailContato().trim().length() > 0)) {
			filtro.put(EMAIL_CONTATO, unidadeOrganizacional.getEmailContato());
		}
		if ((unidadeOrganizacional != null) && (unidadeOrganizacional.getSigla() != null)
				&& (unidadeOrganizacional.getSigla().trim().length() > 0)) {
			filtro.put(SIGLA, unidadeOrganizacional.getSigla());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getEmpresa() != null
				&& unidadeOrganizacional.getEmpresa().getChavePrimaria() > 0) {
			filtro.put(ID_EMPRESA, unidadeOrganizacional.getEmpresa().getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getGerenciaRegional() != null
				&& unidadeOrganizacional.getGerenciaRegional().getChavePrimaria() > 0) {
			filtro.put(GERENCIA_REGIONAL, unidadeOrganizacional.getGerenciaRegional().getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getLocalidade() != null
				&& unidadeOrganizacional.getLocalidade().getChavePrimaria() > 0) {
			filtro.put(ID_LOCALIDADE, unidadeOrganizacional.getLocalidade().getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getCanalAtendimento() != null
				&& unidadeOrganizacional.getCanalAtendimento().getChavePrimaria() > 0) {
			filtro.put(ID_CANAL_ATENDIMENTO, unidadeOrganizacional.getCanalAtendimento().getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getUnidadeSuperior() != null
				&& unidadeOrganizacional.getUnidadeSuperior().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_SUPERIOR, unidadeOrganizacional.getUnidadeSuperior().getChavePrimaria());
		}

		if ((unidadeOrganizacional != null) && unidadeOrganizacional.getUnidadeCentralizadora() != null
				&& unidadeOrganizacional.getUnidadeCentralizadora().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_CENTRALIZADORA, unidadeOrganizacional.getUnidadeCentralizadora().getChavePrimaria());
		}

		if (indicadorTramite != null) {
			filtro.put(INDICADOR_TRAMITE, indicadorTramite);
		}

		if (indicadorAbreChamado != null) {
			filtro.put(INDICADOR_ABRE_CHAMADO, 	indicadorAbreChamado);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * 
	 * Método responsável por exibir a tela de detalhamento de unidade organizacional.
	 * 
	 * @param model - {@link - Model}
	 * @param chavePrimaria - {@link - Long}
	 * @return A tela de detalhamento com a unidade sendo exibida - {@link - String}
	 * @throws NegocioException - {@link - NegocioException} caso ocorra algum erro.
	 */
	@RequestMapping("exibirDetalhamentoUnidadeOrganizacional")
	public String exibirDetalhamentoUnidadeOrganizacional(Model model, @RequestParam(value = CHAVE_PRIMARIA) Long chavePrimaria)
			throws NegocioException {

		montarUnidadeOrganizacionalComHorarios(model, chavePrimaria);

		return "exibirDetalhamentoUnidadeOrganizacional";
	}

	/**
	 * @param model - {@link - Model}
	 * @param chavePrimaria - {@link - Long}
	 * @throws NegocioException - {@link - NegocioException} caso ocorra algum erro.
	 */
	public void montarUnidadeOrganizacionalComHorarios(Model model, Long chavePrimaria) throws NegocioException {

		UnidadeOrganizacionalImpl unidadeOrganizacional = (UnidadeOrganizacionalImpl) controladorUnidadeOrganizacional.obter(chavePrimaria);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(unidadeOrganizacional.getInicioExpediente());
		String inicioExpediente = Util.adicionarZeros(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))) + ":"
				+ Util.adicionarZeros(String.valueOf(calendar.get(Calendar.MINUTE)));

		calendar.setTime(unidadeOrganizacional.getFimExpediente());
		String fimExpediente = Util.adicionarZeros(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))) + ":"
				+ Util.adicionarZeros(String.valueOf(calendar.get(Calendar.MINUTE)));

		model.addAttribute(FIM_EXPEDIENTE, fimExpediente);
		model.addAttribute(INICIO_EXPEDIENTE, inicioExpediente);

		model.addAttribute(UNIDADE_ORGANIZACIONAL, unidadeOrganizacional);

	}
}
