/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.cadastro.localidade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.impl.EnderecoImpl;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.impl.GerenciaRegionalImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Classer responsável pelas telas relacionadas a Gerência Regional
 */
@Controller
public class GerenciaRegionalAction extends GenericAction {

	private static final String GERENCIA_REGIONAL = "gerenciaRegional";

	private static final String GERENCIA_REGIONAL_FORM = "gerenciaRegionalForm";

	private static final String CODIGO = "codigo";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME_ABREVIADO = "nomeAbreviado";

	private static final String HABILITADO = "habilitado";

	private static final String NOME = "nome";

	@Autowired
	@Qualifier("controladorGerenciaRegional")
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	/**
	 * Método responsável por exibir a tela de pesquisa gerencia regional.
	 * 
	 * @param gerenciaRegional
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirPesquisaGerenciaRegional")
	public String exibirPesquisaGerenciaRegional(GerenciaRegionalImpl gerenciaRegional, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) {
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		return "exibirPesquisaGerenciaRegional";
	}

	/**
	 * Método responsável por pesquisar gerencia regional.
	 * 
	 * @param gerenciaRegional
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarGerenciaRegional")
	public String pesquisarGerenciaRegional(GerenciaRegionalImpl gerenciaRegional, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);
		Map<String, Object> filtro = obterFiltroForm(gerenciaRegional, habilitado);
		Collection<GerenciaRegional> listaGerenciaRegional = controladorGerenciaRegional
				.consultarGerenciaRegional(filtro);

		if (gerenciaRegional.getChavePrimaria() > 0) {
			model.addAttribute(CODIGO, gerenciaRegional.getChavePrimaria());
		}
		model.addAttribute(GERENCIA_REGIONAL_FORM, gerenciaRegional);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);

		return exibirPesquisaGerenciaRegional(gerenciaRegional, result, habilitado, request, model);
	}

	/**
	 * Metodo responsavel por exibir a tela de detalhamento de uma gerencia
	 * regional.
	 * 
	 * @param gerenciaRegionalTmp
	 * @param result
	 * @param codigo
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhamentoGerenciaRegional")
	public String exibirDetalhamentoGerenciaRegional(GerenciaRegionalImpl gerenciaRegionalTmp, BindingResult result,
			@RequestParam("codigoForm") Long codigo, @RequestParam(HABILITADO) Boolean habilitado,
			HttpServletRequest request, Model model) throws GGASException {

		if (!super.isPostBack(request)) {
			model.addAttribute(CODIGO, codigo);
			model.addAttribute(GERENCIA_REGIONAL, gerenciaRegionalTmp);
			try {
				GerenciaRegional gerenciaRegional = (GerenciaRegional) controladorGerenciaRegional
						.obter(gerenciaRegionalTmp.getChavePrimaria());
				model.addAttribute(GERENCIA_REGIONAL_FORM, gerenciaRegional);
				model.addAttribute(HABILITADO, habilitado);
			} catch (NegocioException e) {
				mensagemErro(model, e);
				return "forward:exibirPesquisaGerenciaRegional";
			}
		}

		saveToken(request);

		return "exibirDetalhamentoGerenciaRegional";
	}

	/**
	 * Método responsável por exibir a tela de inserir gerencia regional.
	 * 
	 * @param gerenciaRegional
	 * @param result
	 * @param endereco
	 * @param results
	 * @param request
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirInserirGerenciaRegional")
	public String exibirInserirGerenciaRegional(GerenciaRegionalImpl gerenciaRegional, BindingResult result,
			EnderecoImpl endereco, BindingResult results, HttpServletRequest request, Model model) {
		saveToken(request);

		return "exibirInserirGerenciaRegional";
	}

	/**
	 * Método responsável por inserir uma gerencia regional.
	 * 
	 * @param gerenciaRegional
	 * @param result
	 * @param endereco
	 * @param results
	 * @param cep
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("inserirGerenciaRegional")
	public String inserirGerenciaRegional(GerenciaRegionalImpl gerenciaRegional, BindingResult result,
			EnderecoImpl endereco, BindingResult results, @RequestParam("cep") String cep, HttpServletRequest request,
			Model model) throws GGASException {

		String retorno;
		model.addAttribute("cep", cep);
		model.addAttribute("enderecoForm", endereco);
		model.addAttribute(GERENCIA_REGIONAL_FORM, gerenciaRegional);
		Map<String, Object> erros = gerenciaRegional.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			retorno = "forward:exibirInserirGerenciaRegional";
		} else {
			try {
				validarToken(request);
				gerenciaRegional.setDadosAuditoria(getDadosAuditoria(request));
				gerenciaRegional.setHabilitado(true);
				if (endereco != null) {
					verificarCep(endereco);
					gerenciaRegional.setEndereco(endereco);
				}
				controladorGerenciaRegional.inserir(gerenciaRegional);
				model.addAttribute(HABILITADO, gerenciaRegional.isHabilitado());
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						GerenciaRegional.GERENCIA_REGIONAL);
				retorno = pesquisarGerenciaRegional(gerenciaRegional, result, gerenciaRegional.isHabilitado(), request,
						model);
			} catch (GGASException e) {
				retorno = "forward:exibirInserirGerenciaRegional";
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * Metodo responsavel por exibir a tela de atualizacao de uma gerencia gerional.
	 * 
	 * @param gerenciaRegionalTmp
	 * @param result
	 * @param endereco
	 * @param request
	 * @param codigo
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirAtualizacaoGerenciaRegional")
	public String exibirAtualizacaoGerenciaRegional(GerenciaRegionalImpl gerenciaRegionalTmp, BindingResult result,
			EnderecoImpl endereco, HttpServletRequest request, @RequestParam("codigoForm") Long codigo,
			@RequestParam(HABILITADO) Boolean habilitado, Model model)
			throws GGASException {

		model.addAttribute("nome", gerenciaRegionalTmp.getNome());
		model.addAttribute("nomeAbreviado", gerenciaRegionalTmp.getNomeAbreviado());
		
		model.addAttribute(CODIGO, codigo);
		model.addAttribute(GERENCIA_REGIONAL, gerenciaRegionalTmp);
		if (!super.isPostBack(request)) {
			try {
				GerenciaRegional gerenciaRegional = (GerenciaRegional) controladorGerenciaRegional
						.obter(gerenciaRegionalTmp.getChavePrimaria());
				model.addAttribute(GERENCIA_REGIONAL, gerenciaRegional);
				model.addAttribute(HABILITADO, habilitado);
			} catch (NegocioException e) {
				mensagemErro(model, e);
				return "forward:exibirPesquisaGerenciaRegional";
			}

		} else {
			model.addAttribute(GERENCIA_REGIONAL, gerenciaRegionalTmp);
		}

		saveToken(request);

		return "exibirAtualizacaoGerenciaRegional";
	}

	/**
	 * Método responsável por atualizar uma Gerencia Regional.
	 * 
	 * @param gerenciaRegional
	 * @param result
	 * @param habilitado
	 * @param endereco
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("atualizarGerenciaRegional")
	public String atualizarGerenciaRegional(GerenciaRegionalImpl gerenciaRegional, BindingResult result,
			@RequestParam(HABILITADO) Boolean habilitado, EnderecoImpl endereco, HttpServletRequest request,
			Model model) throws GGASException {

		String retorno;

		if (endereco != null) {
			model.addAttribute("cep", endereco.getCep());
			gerenciaRegional.setEndereco(endereco);
		}

		model.addAttribute(GERENCIA_REGIONAL, gerenciaRegional);

		Map<String, Object> erros = gerenciaRegional.validarDados();

		if (!erros.isEmpty()) {
			mensagemErro(model, request, new NegocioException(erros));
			retorno = exibirAtualizacaoGerenciaRegional(gerenciaRegional, result, endereco, request,
					gerenciaRegional.getChavePrimaria(), habilitado, model);
		} else {
			try {
				validarToken(request);
				gerenciaRegional.setDadosAuditoria(getDadosAuditoria(request));

				if (endereco != null) {
					verificarCep(endereco);
					gerenciaRegional.setEndereco(endereco);
				}

				controladorGerenciaRegional.atualizar(gerenciaRegional);
				model.addAttribute(HABILITADO, gerenciaRegional.isHabilitado());
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						GerenciaRegional.GERENCIA_REGIONAL);
				retorno = pesquisarGerenciaRegional(gerenciaRegional, result, gerenciaRegional.isHabilitado(), request,
						model);
			} catch (GGASException e) {
				retorno = exibirAtualizacaoGerenciaRegional(gerenciaRegional, result, endereco, request,
						gerenciaRegional.getChavePrimaria(), habilitado, model);
				mensagemErroParametrizado(model, e);
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por remover um ou mais Gerencia Regional.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerGerenciaRegional")
	public String removerGerenciaRegional(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			validarToken(request);
			controladorGerenciaRegional.validarRemoverGerenciaRegional(chavesPrimarias);
			controladorGerenciaRegional.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, GerenciaRegional.GERENCIA_REGIONAL);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} 

		return "forward:pesquisarGerenciaRegional";
	}

	/**
	 * @param endereco
	 */
	private void verificarCep(EnderecoImpl endereco) {
		if (endereco.getCep().getChavePrimaria() <= 0) {
			endereco.setCep(null);
		}
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param gerenciaRegional
	 * @param habilitado
	 * @return Map<String, Object>
	 * @throws GGASException
	 */
	private Map<String, Object> obterFiltroForm(GerenciaRegionalImpl gerenciaRegional, Boolean habilitado)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (gerenciaRegional.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, gerenciaRegional.getChavePrimaria());
		}

		if (!StringUtils.isEmpty(gerenciaRegional.getNome())) {
			filtro.put(NOME, gerenciaRegional.getNome());
		}

		if (!StringUtils.isEmpty(gerenciaRegional.getNomeAbreviado())) {
			filtro.put(NOME_ABREVIADO, gerenciaRegional.getNomeAbreviado());
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
