package br.com.ggas.web.cadastro.operacional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.operacional.ZonaBloqueio;
import br.com.ggas.cadastro.operacional.impl.ZonaBloqueioImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 * alterações nas informações das views referentes a Zona de Bloqueio.
 */
@Controller
public class ZonaBloqueioAction extends GenericAction {

	private static final String ZONA_BLOQUEIO = "zonaBloqueio";

	private static final String EXIBIR_ATUALIZAR_ZONA_BLOQUEIO = "exibirAtualizarZonaBloqueio";

	private static final String EXIBIR_INSERIR_ZONA_BLOQUEIO = "exibirInserirZonaBloqueio";

	private static final Class<ZonaBloqueioImpl> CLASSE = ZonaBloqueioImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.operacional.impl.ZonaBloqueioImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String LISTA_ZONA_BLOQUEIO = "listaZonaBloqueio";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir
	 * a tela de pesquisa de Zona de Bloqueio.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaZonaBloqueio")
	public String exibirPesquisaZonaBloqueio(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaZonaBloqueio";
	}

	/**
	 * Método responsável por pesquisar uma Zona de Bloqueio.
	 * 
	 * @param zonaBloqueio - {@link ZonaBloqueioImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarZonaBloqueio")
	public String pesquisarZonaBloqueio(ZonaBloqueioImpl zonaBloqueio, BindingResult result, Model model,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(zonaBloqueio, habilitado);

		Collection<TabelaAuxiliar> listaZonaBloqueio = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_ZONA_BLOQUEIO, listaZonaBloqueio);
		model.addAttribute(ZONA_BLOQUEIO, zonaBloqueio);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaZonaBloqueio(model);
	}

	/**
	 * Método responsável por exibir o detalhamento
	 * de uma Zona de Bloqueio.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoZonaBloqueio")
	public String exibirDetalhamentoZonaBloqueio(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			ZonaBloqueio zonaBloqueio = (ZonaBloqueio) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(ZONA_BLOQUEIO, zonaBloqueio);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaZonaBloqueio";
		}

		return "exibirDetalhamentoZonaBloqueio";
	}

	/**
	 * Método responsável por exibir a tela de inserir
	 * Zona de Bloqueio.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_ZONA_BLOQUEIO)
	public String exibirInserirZonaBloqueio() {

		return EXIBIR_INSERIR_ZONA_BLOQUEIO;
	}

	/**
	 * Método responsável por inserir uma Zona de Bloqueio.
	 * 
	 * @param zonaBloqueio - {@link ZonaBloqueioImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirZonaBloqueio")
	private String inserirZonaBloqueio(ZonaBloqueioImpl zonaBloqueio, BindingResult result, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(zonaBloqueio);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(zonaBloqueio, "Zona de Bloqueio");
			controladorTabelaAuxiliar.inserir(zonaBloqueio);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, ZonaBloqueio.ZONA_BLOQUEIO);
			retorno = pesquisarZonaBloqueio(zonaBloqueio, result, model, zonaBloqueio.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(ZONA_BLOQUEIO, zonaBloqueio);
			retorno = EXIBIR_INSERIR_ZONA_BLOQUEIO;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Zona Bloqueio.
	 * 
	 * @param zonaBloqueioParam - {@link ZonaBloqueioImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_ZONA_BLOQUEIO)
	public String exibirAtualizarZonaBloqueio(ZonaBloqueioImpl zonaBloqueioParam, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		ZonaBloqueio zonaBloqueio = zonaBloqueioParam;

		try {
			zonaBloqueio = (ZonaBloqueio) controladorTabelaAuxiliar.obter(zonaBloqueio.getChavePrimaria(), CLASSE);
			model.addAttribute(ZONA_BLOQUEIO, zonaBloqueio);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaZonaBloqueio";
		}

		return EXIBIR_ATUALIZAR_ZONA_BLOQUEIO;
	}

	/**
	 * Método responsável por atualizar um Zona de Bloqueio.
	 * 
	 * @param zonaBloqueio - {@link ZonaBloqueioImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarZonaBloqueio")
	private String atualizarZonaBloqueio(ZonaBloqueioImpl zonaBloqueio, BindingResult result, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(zonaBloqueio);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(zonaBloqueio, "Zona de Bloqueio");
			controladorTabelaAuxiliar.atualizar(zonaBloqueio, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, ZonaBloqueio.ZONA_BLOQUEIO);
			retorno = pesquisarZonaBloqueio(zonaBloqueio, result, model, zonaBloqueio.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(ZONA_BLOQUEIO, zonaBloqueio);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_ZONA_BLOQUEIO;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover uma Zona de Bloqueio.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerZonaBloqueio")
	public String removerZonaBloqueio(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, ZonaBloqueio.ZONA_BLOQUEIO);
			return pesquisarZonaBloqueio(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarZonaBloqueio";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Zona de Bloqueio.
	 * 
	 * @param zonaBloqueio - {@link ZonaBloqueio}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(ZonaBloqueio zonaBloqueio, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (zonaBloqueio != null) {

			if (zonaBloqueio.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, zonaBloqueio.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(zonaBloqueio.getDescricao())) {
				filtro.put(DESCRICAO, zonaBloqueio.getDescricao());
			}

		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
