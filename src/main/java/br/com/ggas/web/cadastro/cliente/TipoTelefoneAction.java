package br.com.ggas.web.cadastro.cliente;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas ao Tipo de Telefone
 *
 */
@Controller
public class TipoTelefoneAction extends GenericAction {

	private static final String HABILITADO = "habilitado";
	private static final String DESCRICAO = "descricao";
	private static final String TIPO_FONE = "tipoTelefone";

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela exibição da tela de pesquisa de tipos de telefone.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaTipoTelefone")
	public String exibirPesquisaTipoTelefone(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaTipoTelefone";
	}

	/**
	 * Método responsável pela pesquisa do tipo de telefone.
	 * 
	 * @param tipofone
	 *            - {@link TipoFoneImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarTipoTelefone")
	public String pesquisarTipoTelefone(TipoFoneImpl tipofone, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(tipofone, habilitado);
		model.addAttribute(TIPO_FONE, tipofone);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("listaTipoTelefone",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, TipoFoneImpl.class.getName()));

		return "exibirPesquisaTipoTelefone";
	}

	/**
	 * Método responsável pela exibição da tela para inclusão do tipo de telefone.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirInclusaoTipoTelefone")
	public String exibirInclusaoTipoTelefone(Model model) throws NegocioException {

		return "exibirInclusaoTipoTelefone";
	}

	/**
	 * 	Método responsável pela inclusão de um tipo de telefone
	 * 
	 * @param tipofone
	 *            - {@link TipoFoneImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirTipoTelefone")
	public String incluirTipoTelefone(TipoFoneImpl tipofone, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "forward:exibirInclusaoTipoTelefone";

		model.addAttribute(TIPO_FONE, tipofone);
		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipofone);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipofone.getDescricao(), TipoFoneImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipofone, TipoFoneImpl.TIPO_FONE);
			controladorTabelaAuxiliar.inserir(tipofone);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, TipoFoneImpl.TIPO_FONE);
			view = pesquisarTipoTelefone(tipofone, result, tipofone.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de detalhamento do tipo de telefone.
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirDetalharTipoTelefone")
	public String exibirDetalharTipoTelefone(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {
		String view = "exibirDetalharTipoTelefone";

		try {
			model.addAttribute(TIPO_FONE, controladorTabelaAuxiliar.obter(chavePrimaria, TipoFoneImpl.class));
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
			view = exibirPesquisaTipoTelefone(model);
		}
		return view;
	}

	/**
	 * Método responsável por exibir a tela de alteração do tipo de telefone.
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirAlteracaoTipoTelefone")
	public String exibirAlteracaoTipoTelefone(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {
		String view = "exibirAlteracaoTipoTelefone";

		try {
			model.addAttribute(TIPO_FONE, controladorTabelaAuxiliar.obter(chavePrimaria, TipoFoneImpl.class));
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
			view = exibirPesquisaTipoTelefone(model);
		}
		return view;
	}

	/**
	 * Método responsável por alterar um tipo de telefone do sistema.
	 * 
	 * @param tipofone
	 *            - {@link TipoFoneImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarTipoTelefone")
	public String alterarTipoTelefone(TipoFoneImpl tipofone, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String view = "exibirAlteracaoTipoTelefone";

		model.addAttribute(TIPO_FONE, tipofone);
		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipofone);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipofone.getDescricao(), TipoFoneImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipofone, TipoFoneImpl.TIPO_FONE);
			controladorTabelaAuxiliar.atualizar(tipofone);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, TipoFoneImpl.TIPO_FONE);
			view = pesquisarTipoTelefone(tipofone, result, tipofone.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por remover um tipo de telefone do sistema.
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerTipoTelefone")
	public String removerTipoTelefone(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TipoFoneImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, TipoFoneImpl.TIPO_FONE);
			return pesquisarTipoTelefone(null, null, true, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarTipoTelefone";
	}

	/**
	 * 
	 * @param tipoFone
	 *            - {@link TipoFoneImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> popularFiltro(TipoFoneImpl tipoFone, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (tipoFone != null) {

			if (tipoFone.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", tipoFone.getChavePrimaria());
			}
			if (!StringUtils.isEmpty(tipoFone.getDescricao())) {
				filtro.put(DESCRICAO, tipoFone.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
