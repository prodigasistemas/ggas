package br.com.ggas.web.cadastro.localidade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.localidade.AreaTipo;
import br.com.ggas.cadastro.localidade.impl.AreaTipoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelos metodos de ações 
 * referentes ao Tipo de Área.
 */
@Controller
public class AreaTipoAction extends GenericAction{
	
	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String TIPO_AREA = "tipoArea";

	private static final String EXIBIR_ALTERAR_TIPO_AREA= "exibirAlterarTipoArea";

	private static final String EXIBIR_INSERIR_TIPO_AREA = "exibirInserirTipoArea";

	private static final Class<AreaTipoImpl> CLASSE = AreaTipoImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.cadastro.localidade.impl.AreaTipoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_TIPO_AREA = "listaTipoArea";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa Tipo de área.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaTipoArea")
	public String exibirPesquisaTipoArea(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaTipoArea";
	}

	/**
	 * Método responsável por efetuar 
	 * a pesquisa dos Tipo de área.
	 * 
	 * @param tipoArea - {@link AreaTipoImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarTipoArea")
	public String pesquisarTipoArea(AreaTipoImpl tipoArea, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(tipoArea, habilitado);

		Collection<TabelaAuxiliar> listaTipoArea= controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_TIPO_AREA, listaTipoArea);
		model.addAttribute(TIPO_AREA, tipoArea);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaTipoArea(model);
	}

	/**
	 * Método responsável por exibir 
	 * o detalhamento de um Tipo de área.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoTipoArea")
	public String exibirDetalhamentoTipoArea(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model, HttpServletRequest request) {

		try {
			AreaTipo tipoArea = (AreaTipo) controladorTabelaAuxiliar.obter(chavePrimaria, AreaTipoImpl.class);
			model.addAttribute(TIPO_AREA, tipoArea);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			return "forward:exibirPesquisaTipoArea";
		}

		return "exibirDetalhamentoTipoArea";	
	}

	/**
	 * Método responsável por exibir 
	 * a tela de inserir Tipo de área.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_TIPO_AREA)
	public String exibirInserirTipoArea() {

		return EXIBIR_INSERIR_TIPO_AREA;
	}

	/**
	 * Método responsável por executar 
	 * a inserção de um Tipo de área.
	 * 
	 * @param tipoArea - {@link AreaTipoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirTipoArea")
	private String inserirTipoArea(AreaTipoImpl tipoArea, BindingResult result, Model model, HttpServletRequest request)
			throws GGASException {

		String retorno = null;

		try {
			if (tipoArea.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (tipoArea.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoArea);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoArea, "Tipo de Área");
			controladorTabelaAuxiliar.inserir(tipoArea);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(AreaTipo.TIPO_AREA));
			retorno = pesquisarTipoArea(tipoArea, result, model, tipoArea.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(TIPO_AREA, tipoArea);
			retorno = EXIBIR_INSERIR_TIPO_AREA;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir 
	 * a tela de alterar o Tipo de área.
	 * 
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param tipoAreaParam - {@link AreaTipoImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ALTERAR_TIPO_AREA)
	public String exibirAlterarTipoArea(AreaTipoImpl tipoAreaParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		AreaTipo tipoArea = tipoAreaParam;

		try {
			tipoArea = (AreaTipo) controladorTabelaAuxiliar.obter(tipoArea.getChavePrimaria(), CLASSE);
			model.addAttribute(TIPO_AREA, tipoArea);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaTipoArea";
		}

		return EXIBIR_ALTERAR_TIPO_AREA;
	}

	/**
	 * Método responsável por efetuar a 
	 * alteração de um Tipo de área.
	 * 
	 * @param tipoArea - {@link AreaTipoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarTipoArea")
	private String alterarTipoArea(AreaTipoImpl tipoArea, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String retorno = null;

		try {

			if (tipoArea.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (tipoArea.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoArea);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoArea, "Tipo de Área");
			controladorTabelaAuxiliar.atualizar(tipoArea, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(AreaTipo.TIPO_AREA));
			retorno = pesquisarTipoArea(tipoArea, result, model, tipoArea.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(TIPO_AREA, tipoArea);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ALTERAR_TIPO_AREA;
		}

		return retorno;
	}

	/**
	 * Método responsável por executar 
	 * a remoção um Tipo de área.
	 * 
	 * @param tipoArea {@link AreaTipoImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerTipoArea")
	public String removerTipoArea(AreaTipoImpl tipoArea, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, super.obterMensagem(AreaTipo.TIPO_AREA));

			return pesquisarTipoArea(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) { 
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarTipoArea";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa do Tipo de área.
	 * 
	 * @param tipoArea - {@link AreaTipo}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(AreaTipo tipoArea, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (tipoArea != null) {
			if (tipoArea.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, tipoArea.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(tipoArea.getDescricao())) {
				filtro.put(DESCRICAO, tipoArea.getDescricao());
			}

			if (StringUtils.isNotEmpty(tipoArea.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, tipoArea.getDescricaoAbreviada());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
