/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.cadastro.empresa;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.localidade.impl.UnidadeNegocioImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a funcionalidade Unidade de Negócio.
 */
@Controller
public class UnidadeNegocioAction extends GenericAction {
	
	private static final int NUMERO_MAXIMO_DESCRICAO = 50;
	
	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final String EXIBIR_PESQUISA_UNIDADE_NEGOCIO = "exibirPesquisaUnidadeNegocio";

	private static final String UNIDADE_NEGOCIO = "unidadeNegocio";

	private static final String EXIBIR_ATUALIZAR_UNIDADE_NEGOCIO = "exibirAtualizarUnidadeNegocio";

	private static final String EXIBIR_INSERIR_UNIDADE_NEGOCIO = "exibirInserirUnidadeNegocio";

	private static final Class<UnidadeNegocioImpl> CLASSE = UnidadeNegocioImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.localidade.impl.UnidadeNegocioImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";
	
	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_UNIDADE_NEGOCIO = "listaUnidadeNegocio";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;
	
	@Autowired
	private ControladorGerenciaRegional controladorGerenciaRegional;

	/**
	 * Método responsável por exibir a tela de pesquisa unidade de negócio.
	 * 
	 * @param  Model model -{@link Model}}
	 * @return String
	 */
	@RequestMapping(EXIBIR_PESQUISA_UNIDADE_NEGOCIO)
	public String exibirPesquisaUnidadeNegocio(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_UNIDADE_NEGOCIO;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de unidade de negócio.
	 * 
	 * @param unidadeNegocio
	 * @param result
	 * @param habilitado
	 * @param  Model model -{@link Model}}
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarUnidadeNegocio")
	public String pesquisarUnidadeNegocio(UnidadeNegocioImpl unidadeNegocio, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		if(result != null) {
			result.getObjectName();
		}
		
		Map<String, Object> filtro = prepararFiltro(unidadeNegocio, habilitado);

		Collection<TabelaAuxiliar> listaUnidadeNegocio = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro,
				CLASSE_STRING);

		model.addAttribute(LISTA_UNIDADE_NEGOCIO, listaUnidadeNegocio);
		model.addAttribute(UNIDADE_NEGOCIO, unidadeNegocio);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_UNIDADE_NEGOCIO;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento unidade de negócio.
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return String
	 */
	@RequestMapping("exibirDetalhamentoUnidadeNegocio")
	public String exibirDetalhamentoUnidadeNegocio(
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaUnidadeNegocio";

		try {
			UnidadeNegocioImpl unidadeNegocio = (UnidadeNegocioImpl) controladorTabelaAuxiliar.obter(chavePrimaria,
					CLASSE);
			model.addAttribute(UNIDADE_NEGOCIO, unidadeNegocio);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoUnidadeNegocio";
		} catch (NegocioException e) {
			mensagemErro(model, e);

		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de inserir unidade de negócio.
	 * 
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_INSERIR_UNIDADE_NEGOCIO)
	public String exibirInserirUnidadeNegocio(Model model) throws GGASException {

		carregarCampos(model);

		return EXIBIR_INSERIR_UNIDADE_NEGOCIO;
	}

	/**
	 * Método responsável por inserir unidade de negócio.
	 * 
	 * @param unidadeNegocio
	 * @param result
	 * @param model
	 * @param request
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("inserirUnidadeNegocio")
	private String inserirUnidadeNegocio(UnidadeNegocioImpl unidadeNegocio, BindingResult result, Model model,
			HttpServletRequest request) throws GGASException {

		result.getObjectName();

		String tela = "forward:exibirInserirUnidadeNegocio";

		try {

			if (unidadeNegocio.getDescricao() != null
					&& unidadeNegocio.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO);
			}

			if (unidadeNegocio.getDescricaoAbreviada() != null
					&& unidadeNegocio.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(
						ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			if (StringUtils.isEmpty(unidadeNegocio.getDescricaoAbreviada())) {
				unidadeNegocio.setDescricaoAbreviada(null);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeNegocio);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidadeNegocio, "Unidade de Negócio");
			controladorTabelaAuxiliar.inserir(unidadeNegocio);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, UnidadeNegocio.UNIDADE_NEGOCIO);
			tela = pesquisarUnidadeNegocio(unidadeNegocio, result, unidadeNegocio.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute("unidadeNegocio", unidadeNegocio);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de atualizar unidade de negócio.
	 * 
	 * @param unidadeNegocioImpl
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_UNIDADE_NEGOCIO)
	public String exibirAtualizarUnidadeNegocio(UnidadeNegocioImpl unidadeNegocioImpl, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		result.getErrorCount();

		String retorno = "forward:exibirPesquisaUnidadeNegocio";

		carregarCampos(model);

		UnidadeNegocioImpl unidadeNegocio = unidadeNegocioImpl;

		try {
			unidadeNegocio = (UnidadeNegocioImpl) controladorTabelaAuxiliar.obter(unidadeNegocio.getChavePrimaria(),
					CLASSE);
			model.addAttribute(UNIDADE_NEGOCIO, unidadeNegocio);
			model.addAttribute(HABILITADO, habilitado);
			retorno = EXIBIR_ATUALIZAR_UNIDADE_NEGOCIO;
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por atualizar unidade de negócio.
	 * 
	 * @param unidadeNegocio
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("atualizarUnidadeNegocio")
	private String atualizarUnidadeNegocio(UnidadeNegocioImpl unidadeNegocio, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		result.getErrorCount();

		String tela = exibirAtualizarUnidadeNegocio(unidadeNegocio, result, habilitado, model);

		try {

			if (unidadeNegocio.getDescricao() != null
					&& unidadeNegocio.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO);
			}

			if (unidadeNegocio.getDescricaoAbreviada() != null
					&& unidadeNegocio.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(
						ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			if (StringUtils.isEmpty(unidadeNegocio.getDescricaoAbreviada())) {
				unidadeNegocio.setDescricaoAbreviada(null);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeNegocio);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidadeNegocio, "Unidade de Negócio");
			controladorTabelaAuxiliar.atualizar(unidadeNegocio, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, UnidadeNegocio.UNIDADE_NEGOCIO);
			tela = pesquisarUnidadeNegocio(unidadeNegocio, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(UNIDADE_NEGOCIO, unidadeNegocio);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por remover o unidade de negócio.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("removerUnidadeNegocio")
	public String removerUnidadeNegocio(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		String retorno = "forward:pesquisarUnidadeNegocio";

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, UnidadeNegocio.UNIDADE_NEGOCIO);
			retorno = pesquisarUnidadeNegocio(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}
	
	/**
	 * Método responsável por carregar campos.
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void carregarCampos(Model model) throws GGASException {

		Map<String, Object> filtroGerenciaRegional = new HashMap<String, Object>();

		filtroGerenciaRegional.put(HABILITADO, true);

		Collection<GerenciaRegional> listaGerenciaRegional = controladorGerenciaRegional
				.consultarGerenciaRegional(filtroGerenciaRegional);

		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);

	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param unidadeNegocio
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(UnidadeNegocioImpl unidadeNegocio, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (unidadeNegocio != null) {

			if (unidadeNegocio.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, unidadeNegocio.getChavePrimaria());
			}

			if (unidadeNegocio.getDescricao() != null && !unidadeNegocio.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, unidadeNegocio.getDescricao());
			}

			if (StringUtils.isNotEmpty(unidadeNegocio.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, unidadeNegocio.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
