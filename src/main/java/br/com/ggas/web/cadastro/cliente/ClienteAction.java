/*
 * ] Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.cliente;

import static br.com.ggas.util.Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_CLIENTE;
import static br.com.ggas.util.Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.atendimento.chamado.dominio.PesquisarClientePopupVO;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.cliente.impl.ClienteAnexoEnderecoVO;
import br.com.ggas.cadastro.cliente.impl.ClienteAnexoIdentificacaoVO;
import br.com.ggas.cadastro.cliente.impl.ClienteAnexoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteFoneImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ContatoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ContatoClienteVO;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ContatoClienteComparator;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.cadastro.cliente.decorator.ClienteResultadoPesquisaDecorator;

/**
 *
 * @author ary.consenso
 * 
 * Classe responsável pelos métodos que gerenciam as telas relacionadas ao Cliente
 * 
 */
@Controller
public class ClienteAction extends GenericAction {

	private static final String FONE2_CONTATO = "fone2Contato";

	private static final String FONE_CONTATO = "foneContato";

	private static final String CLIENTE_FORM = "clienteForm";

	private static final String ALTERACAO_LISTA = "alteracaoLista";

	private static final String LISTA_CONTATO_MODIFICADA = "listaContatoModificada";

	private static final String COM_GRUPO_ECONOMICO = "comGrupoEconomico";

	private static final String FUNCIONALIDADE = "funcionalidade";

	private static final String PESSOA_JURIDICA = "pessoaJuridica";

	private static final String PESSOA_FISICA = "pessoaFisica";

	private static final String LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO = "listaClienteAnexoAbaIdentificacao";

	private static final String LISTA_CLIENTE_ANEXO_ABA_ENDERECO = "listaClienteAnexoAbaEndereco";

	private static final String CODIGO_TIPO_PESSOA = "codigoTipoPessoa";

	private static final String DENEGADO = "denegado";

	private static final String DESCRICAO_ANEXO_ABA_IDENTIFICACAO = "descricaoAnexoAbaIdentificacao";

	private static final String ID_TIPO_ANEXO_ABA_IDENTIFICACAO = "idTipoAnexoAbaIdentificacao";

	private static final Logger LOG = Logger.getLogger(ClienteAction.class);

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_ID_CLIENTE = "listaIdCliente";

	private static final String SEXOS = "sexos";

	private static final String CEP = "cep";

	private static final String TIPO_PESSOA_FISICA = "tipoPessoaFisica";

	private static final String SITUACOES_CLIENTES = "situacoesClientes";

	private static final String LISTA_CLIENTE_EXCLUIDO = "listaClienteExcluido";

	private static final String LISTA_CLIENTE = "listaCliente";

	private static final String LISTA_GRUPO_ECONOMICO = "listaGrupoEconomico";

	private static final String TIPOS_CLIENTES = "tiposClientes";

	private static final String ID_TIPO_CONTATO = "idTipoContato";

	private static final String LISTA_CONTATO = "listaContato";

	private static final String REQUEST_LISTA_CLIENTE_ENDERECO = "listaClienteEndereco";

	private static final String REQUEST_LISTA_CLIENTE_FONE = "listaClienteFone";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String INSCRICAO_RURAL = "inscricaoRural";

	private static final String INSCRICAO_MUNICIPAL = "inscricaoMunicipal";

	private static final String INSCRICAO_ESTADUAL = "inscricaoEstadual";

	private static final String ID_ATIVIDADE_ECONOMICA = "idAtividadeEconomica";

	private static final String CNPJ = "cnpj";

	private static final String NOME_FANTASIA = "nomeFantasia";

	private static final String PASSAPORTE = "passaporte";

	private static final String MASCARA_INSCRICAO_ESTADUAL = "mascaraInscricaoEstadual";

	private static final String ATIVIDADES_ECONOMICAS = "atividadesEconomicas";

	private static final String NACIONALIDADES = "nacionalidades";

	private static final String FAIXAS_RENDA = "faixasRenda";

	private static final String PROFISSOES = "profissoes";

	private static final String UNIDADES_FEDERACAO = "unidadesFederacao";

	private static final String ORGAOS_EMISSORES = "orgaosEmissores";

	private static final String CPF = "cpf";

	private static final String NOME = "nome";

	private static final String VERSAO = "versao";

	private static final String ABA_ENDERECO = "1";

	private static final String ABA_TELEFONE = "2";

	private static final String ABA_CONTATO = "3";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_FUNCIONARIO_FISCAL = "listaFuncionarioFiscal";

	private static final String LISTA_FUNCIONARIO_VENDEDOR = "listaFuncionarioVendedor";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	protected static final String ATRIBUTO_ABA_ID = "abaId";

	private static final String ERRO_CEP_NAO_CADASTRADO = "ERRO_CEP_NAO_CADASTRADO";

	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorIntegracao")
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	@Qualifier("controladorGrupoEconomico")
	private ControladorGrupoEconomico controladorGrupoEconomico;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	/**
	 * Método responsável pela exibição da tela de pesquisa de cliente.
	 * 
	 * @param model {@link Model}
	 * @return exibirPesquisaCliente {@link String}
	 * @throws GGASException @{@link GGASException}
	 */
	@RequestMapping("exibirPesquisaCliente")
	public String exibirPesquisaCliente(Model model) throws GGASException {

		model.addAttribute(ATIVIDADES_ECONOMICAS, controladorCliente.listarAtividadeEconomica());

		model.addAttribute(MASCARA_INSCRICAO_ESTADUAL,
				controladorParametroSistema.obterValorDoParametroPorCodigo("MASCARA_INSCRICAO_ESTADUAL"));
		
		return "exibirPesquisaCliente";
	}

	/**
	 * Método responsável por realizar a pesquisa de cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param result {@link BindingResult}
	 * @param habilitado {@link String}
	 * @param numeroCep {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaCliente {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarCliente")
	public String pesquisarCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente, BindingResult result,
			@RequestParam(value = "habilitado", required = false) String habilitado,
			@RequestParam(value = "cep", required = false) String numeroCep,
			@RequestParam(value = "tipoCliente", required = false) String tipoCliente, HttpServletRequest request, Model model)
			throws GGASException {
		Map<String, Object> filtro = new HashMap<>();
		prepararFiltro(filtro, cliente, habilitado, numeroCep, tipoCliente);
		adicionarFiltroPaginacao(request, filtro);
		
		int tamanhoPaginacao = Constantes.PAGINACAO_PADRAO;
		
		Collection<Cliente> listaCliente = controladorCliente.consultarClientes(filtro);
		
		model.addAttribute("tamanhoPagina", tamanhoPaginacao);
		for (Cliente clienteObj : listaCliente) {
			removerLock(request, ClienteResultadoPesquisaDecorator.LOCK_CLIENTE, clienteObj.getChavePrimaria());
			if (controladorIntegracao.integracaoPrecedenteContratoAtiva() &&
					!controladorIntegracao.validarIntegracaoContratoCliente(clienteObj.getChavePrimaria())) {
				registrarLock(request, ClienteResultadoPesquisaDecorator.LOCK_CLIENTE,
						MensagemUtil.obterMensagem(ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_CLIENTE),
						clienteObj.getChavePrimaria());
			}
		}

		model.addAttribute("listaCliente", listaCliente);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(CLIENTE_FORM, cliente);
		model.addAttribute("cep", numeroCep);
		request.getSession(false);
		return "exibirPesquisaCliente";
	}

	/**
	 * Método Responsável pela exibição da tela de detalhamento de cliente
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @return exibirDetalhamentoCliente {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoCliente")
	public String exibirDetalhamentoCliente(@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam(value="isTelaChamado", required = false) Boolean isTelaChamado,
			@RequestParam(value="idPontoConsumo", required = false) Long idPontoConsumo, Model model,
			HttpServletRequest request) throws GGASException {

		Cliente cliente;
		cliente = controladorCliente.obterCliente(chavePrimaria, Cliente.PROPRIEDADES_LAZY);
		model.addAttribute(VERSAO, cliente.getVersao());
		model.addAttribute(HABILITADO, cliente.isHabilitado());
		model.addAttribute("endereco", cliente.getEnderecoPrincipal());
		model.addAttribute("listaClienteImovel",
				controladorImovel.listarClienteImovelPorChaveCliente(cliente.getChavePrimaria(), false));
		model.addAttribute(CLIENTE_FORM, cliente);
		model.addAttribute("locked", true);
		model.addAttribute("isTelaChamado", isTelaChamado);
		model.addAttribute("idPontoConsumo", idPontoConsumo);

		carregarCampos(request, model, cliente);

		return "exibirDetalhamentoCliente";
	}

	/**
	 * Método responsável por carregar os campos que serão exibidos na tela de
	 * cliente.
	 * 
	 * @param request 
	 * @param model
	 * @param cliente
	 * @throws GGASException
	 */
	private void carregarCampos(HttpServletRequest request, Model model, Cliente cliente) throws GGASException {

		ConstanteSistema constanteCadastroCliente = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CADASTRO_CLIENTES);

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("idIntegracaoFuncao", Long.parseLong(constanteCadastroCliente.getValor()));
		filtro.put(HABILITADO, Boolean.TRUE);

		Collection<IntegracaoSistemaFuncao> consultaIntegracaoSistemaFuncao = controladorIntegracao
				.consultarIntegracaoSistemaFuncao(filtro);

		if (!consultaIntegracaoSistemaFuncao.isEmpty()) {
			model.addAttribute("indicadorIntegracaoCadastroCliente", Boolean.TRUE);
		}

		model.addAttribute(TIPOS_CLIENTES, controladorCliente.listarTipoCliente());
		model.addAttribute(SITUACOES_CLIENTES, controladorCliente.listarClienteSituacao());
		model.addAttribute(LISTA_GRUPO_ECONOMICO, controladorGrupoEconomico.listarGrupoEconomico());
		model.addAttribute(TIPO_PESSOA_FISICA, TipoPessoa.PESSOA_FISICA);
		model.addAttribute("intervaloAnosData", intervaloAnosData());
		model.addAttribute(LISTA_FUNCIONARIO_FISCAL,
				controladorFuncionario.listarFuncionarioFiscalVendedor(true, false));
		model.addAttribute(LISTA_FUNCIONARIO_VENDEDOR,
				controladorFuncionario.listarFuncionarioFiscalVendedor(false, true));
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		String exigeEmailPrincialObrigatorio = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA);
		model.addAttribute("exigeEmailPrincialObrigatorio", exigeEmailPrincialObrigatorio);

		this.exibirDadosAbaDocumentacao(model);
		this.exibirDadosAbaEndereco(request, model, cliente);
		this.exibirDadosAbaTelefone(model, request, cliente);
		this.exibirDadosAbaContato(request, cliente);
		this.exibirDadosAnexo(request, cliente);

	}

	/**
	 * Método responsável pela exibição da tela de inclusão de cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirInclusaoCliente {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoCliente")
	public String exibirInclusaoCliente(ClienteImpl cliente, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		this.carregarDadosInclusaoCliente(model, request, cliente);
		model.addAttribute(CLIENTE_FORM, cliente);
		return "exibirInclusaoCliente";
	}

	/**
	 * Método responsável pelo carregamento de dados para a tela de inclusão de
	 * cliente.
	 * 
	 * @param model
	 * @param request
	 * @param cliente
	 * @throws GGASException
	 */
	private void carregarDadosInclusaoCliente(Model model, HttpServletRequest request, Cliente cliente)
			throws GGASException {

		this.carregarCampos(request, model, cliente);

		model.addAttribute(ID_TIPO_ANEXO_ABA_IDENTIFICACAO, -1L);
		model.addAttribute(DESCRICAO_ANEXO_ABA_IDENTIFICACAO, "");
	}

	/**
	 * Método responsável pela inclusão de cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return retorno {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirCliente")
	public String incluirCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		
		String retorno = null;

		try {
			popularCliente(cliente, request);
			
			cliente.setChavePrimaria(controladorCliente.inserirCliente(cliente));
			model.addAttribute(CLIENTE_FORM, cliente);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Cliente.PESSOA);
			retorno = pesquisarCliente(cliente, result, String.valueOf(cliente.isHabilitado()),
					cliente.getEnderecoPrincipal().getCep().getCep(), null, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = "forward:exibirInclusaoCliente";
		}

		return retorno;
	}

	/**
	 * Método responsável pela exibição da tela de alteração de cliente.
	 * 
	 * @param chavePrimaria {@link ClienteImpl}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @param cliente {@link ClienteImpl}
	 * @param status {@link Boolean}
	 * @return exibirAlteracaoCliente {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoCliente")
	public String exibirAlteracaoCliente(ClienteImpl cliente, BindingResult result, HttpServletRequest request,
			@RequestParam("status") boolean status, Model model) throws GGASException {

		try {
			ClienteImpl clienteForm = null;

			if (!status) {
				Long chavePrimaria = null;
				if(cliente.getCliente() != null && cliente.getCliente().getChavePrimaria() > 0) {
					chavePrimaria = cliente.getCliente().getChavePrimaria();
				} else {
					chavePrimaria = cliente.getChavePrimaria();
				}
				
				clienteForm = (ClienteImpl) controladorCliente.obterCliente(chavePrimaria,
						Cliente.PROPRIEDADES_LAZY);
				model.addAttribute("status", true);
			} else {
				clienteForm = cliente;
			}

			if (controladorIntegracao.integracaoPrecedenteContratoAtiva()) {
				if (controladorIntegracao.validarIntegracaoContratoCliente(clienteForm.getChavePrimaria())) {
					removerLock(request, ClienteResultadoPesquisaDecorator.LOCK_CLIENTE, clienteForm.getChavePrimaria());
				} else {
					throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
				}
			} else {
				removerLock(request, ClienteResultadoPesquisaDecorator.LOCK_CLIENTE, clienteForm.getChavePrimaria());
			}

			carregarCampos(request, model, clienteForm);
			model.addAttribute(CLIENTE_FORM, clienteForm);

		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			String cep = "";
			if (cliente.getEnderecoPrincipal() != null) {
				cep = cliente.getEnderecoPrincipal().getCep().getCep();
			}
			return pesquisarCliente(cliente, result, String.valueOf(cliente.isHabilitado()),cep,
					null, request, model);
		}

		return "exibirAlteracaoCliente";
	}

	/**
	 * Método responsável pela alteração de um cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return retorno {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarCliente")
	public String alterarCliente(ClienteImpl cliente, BindingResult resultClient, HttpServletRequest request,
			Model model) throws GGASException {
		String retorno = null;
		String cep = null;
		boolean atualizarContrato = Boolean.parseBoolean(request.getParameter("atualizarContrato"));

		try {
			popularCliente(cliente, request);
			model.addAttribute(CLIENTE_FORM, cliente);
			cliente.setDadosAuditoria(getDadosAuditoria(request));
			controladorCliente.atualizarCliente(cliente, atualizarContrato);
			if (cliente.getEnderecoPrincipal() != null) {
				cep = cliente.getEnderecoPrincipal().getCep().getCep();
			}
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Cliente.PESSOA);
			retorno = pesquisarCliente(cliente, resultClient, String.valueOf(cliente.isHabilitado()),cep, null, request,
					model);
			
			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession()
					.getAttribute("indicadorAtualizacaoCadastral");
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession()
						.getAttribute("urlRetornoAtualizacaoCastral");
				retorno = "redirect:" + urlRetornoAtualizacaoCastral;
			}
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = exibirAlteracaoCliente(cliente, resultClient, request, true, model);
		}

		return retorno;
	}

	/**
	 * Método responsável pela remoção de um cliente
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return forward:exibirPesquisaCliente {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerCliente")
	public String removerCliente(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		// rotina criada para dar uma mensagem melhor quando tentar excluir um cliente
		// que possua pelo menos um imovel
		// ao ives de dar a msg padrao do sistema
		Map<String, Object> filtro = new HashMap<String, Object>();
		for (Long l : chavesPrimarias) {
			filtro.put("idCliente", l);
			if (!controladorImovel.consultarImoveis(filtro).isEmpty()) {
				mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL));
			}
		}
		try {
			for (Long chave : chavesPrimarias) {
				boolean integracaoAtiva = controladorIntegracao.integracaoPrecedenteContratoAtiva();
				boolean contratoValido = controladorIntegracao.validarIntegracaoContratoCliente(chave);
				if (!integracaoAtiva || (integracaoAtiva && contratoValido)) {
					removerLock(request, ClienteResultadoPesquisaDecorator.LOCK_CLIENTE, chave);
				} else if (integracaoAtiva && !contratoValido) {
					throw new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
				}
			}
			validarSelecaoUmOuMais(chavesPrimarias);
			controladorCliente.removerClientes(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Cliente.CLIENTES);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, new GGASException(e));
		} catch (DataIntegrityViolationException | ConstraintViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:exibirPesquisaCliente";
	}

	/**
	 * Método responsável por montar um filtro com os dados de cliente para realizar
	 * uma consulta.
	 * 
	 * @param filtro
	 * @param cliente
	 * @param habilitado
	 * @param numeroCep
	 * @param tipoCliente 
	 * @throws GGASException
	 */
	private void prepararFiltro(Map<String, Object> filtro, Cliente cliente, String habilitado, String numeroCep, String tipoCliente)
			throws GGASException {

		if (cliente.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, cliente.getChavePrimaria());
		}

		if (!StringUtils.isEmpty(cliente.getNome())) {
			filtro.put(NOME, cliente.getNome());
		}

		if (!StringUtils.isEmpty(cliente.getNomeFantasia())) {
			filtro.put(NOME_FANTASIA, cliente.getNomeFantasia());
		}

		if (!StringUtils.isEmpty(cliente.getCnpj())) {
			filtro.put(CNPJ, cliente.getCnpj());
		}

		if (!StringUtils.isEmpty(cliente.getCpf())) {
			filtro.put(CPF, cliente.getCpf());
		}

		if (!StringUtils.isEmpty(cliente.getNumeroPassaporte())) {
			filtro.put(PASSAPORTE, cliente.getNumeroPassaporte().trim());
		}

		if (!StringUtils.isEmpty(cliente.getInscricaoEstadual())) {
			filtro.put(INSCRICAO_ESTADUAL, cliente.getInscricaoEstadual().trim());
		}

		if (!StringUtils.isEmpty(cliente.getInscricaoMunicipal())) {
			filtro.put(INSCRICAO_MUNICIPAL, cliente.getInscricaoMunicipal().trim());
		}

		if (!StringUtils.isEmpty(cliente.getInscricaoRural())) {
			filtro.put(INSCRICAO_RURAL, cliente.getInscricaoRural().trim());
		}

		if ((cliente.getAtividadeEconomica() != null) && cliente.getAtividadeEconomica().getChavePrimaria() > 0) {
			filtro.put(ID_ATIVIDADE_ECONOMICA, cliente.getAtividadeEconomica().getChavePrimaria());
		}

		if (!StringUtils.isEmpty(numeroCep)) {
			filtro.put(CEP, numeroCep.trim());
		}

		if (!StringUtils.isEmpty(tipoCliente) && Integer.parseInt(tipoCliente) > 0) {
			filtro.put(CODIGO_TIPO_PESSOA, Integer.parseInt(tipoCliente));
		}

		if (!StringUtils.isEmpty(habilitado) && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}

	}

	/**
	 * Método responsável por carregar a lista de telefones relacionados ao cliente.
	 * 
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @param cliente {@link Cliente}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void exibirDadosAbaTelefone(Model model, HttpServletRequest request, Cliente cliente) throws GGASException {

		Collection<ClienteFone> listaClienteFone = new ArrayList<ClienteFone>();

		if (!isPostBack(request)) {
			if (cliente.getFones() != null) {
				listaClienteFone.addAll(cliente.getFones());
			}
			request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_FONE, listaClienteFone);
		} else {
			listaClienteFone = (Collection<ClienteFone>) request.getSession().getAttribute(REQUEST_LISTA_CLIENTE_FONE);
		}

		if (cliente.getTipoCliente() != null) {
			carregarListaTipoContato(request, cliente.getTipoCliente().getChavePrimaria());
		}

		Collections.sort((List<ClienteFone>) listaClienteFone, new Comparator<ClienteFone>() {

			@Override
			public int compare(ClienteFone o1, ClienteFone o2) {

				return o2.getIndicadorPrincipal().compareTo(o1.getIndicadorPrincipal());
			}
		});

		request.setAttribute(REQUEST_LISTA_CLIENTE_FONE, listaClienteFone);
	}

	/**
	 * Método responsável por carregar a lista dos tipos de contatos do cliente.
	 * 
	 * @param request
	 * @param idTipoCliente
	 * @throws GGASException
	 */
	private void carregarListaTipoContato(HttpServletRequest request, Long idTipoCliente) throws GGASException {
		Collection<TipoContato> listaTiposContato = null;
		if (idTipoCliente != null && !idTipoCliente.equals(-1L)) {
			listaTiposContato = controladorCliente.listarTipoContadoPorTipoPessoaDoCliente(idTipoCliente);
		} else {
			listaTiposContato = controladorCliente.listarTipoContato();
		}
		request.setAttribute("listaTiposContato", listaTiposContato);
	}

	/**
	 * Método responsável por exibir dados referentes aos contatos do cliente.
	 * 
	 * @param request
	 *            the request
	 * @param cliente
	 *            the cliente
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private void exibirDadosAbaContato(HttpServletRequest request, Cliente cliente) throws GGASException {

		Collection<TipoFone> listaTiposTelefone = controladorCliente.listarTipoFone();
		request.setAttribute("listaTiposTelefone", listaTiposTelefone);
		request.setAttribute(PROFISSOES, controladorCliente.listarProfissao());

		Collection<ContatoCliente> listaContato = new ArrayList<ContatoCliente>();

		if (!isPostBack(request)) {
			if (cliente != null) {
				listaContato.addAll(cliente.getContatos());
			}
			Collections.sort((List<ContatoCliente>) listaContato, new ContatoClienteComparator());
			request.getSession().setAttribute(LISTA_CONTATO, listaContato);
		} else {
			listaContato = (Collection<ContatoCliente>) request.getSession().getAttribute(LISTA_CONTATO);
		}

		Collections.sort((List<ContatoCliente>) listaContato, new Comparator<ContatoCliente>() {

			@Override
			public int compare(ContatoCliente o1, ContatoCliente o2) {

				Boolean o1Principal = o1.isPrincipal();
				Boolean o2Principal = o2.isPrincipal();
				return o2Principal.compareTo(o1Principal);
			}
		});
		Collections.sort((List<ContatoCliente>) listaContato, new ContatoClienteComparator());
		request.setAttribute(LISTA_CONTATO, listaContato);

	}

	/**
	 * Método responsável pela exibição dos dados relacionados aos endereços do
	 * cliente.
	 * 
	 * @param request
	 *            the request
	 * @param cliente
	 *            the cliente
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void exibirDadosAbaEndereco(HttpServletRequest request, Model model, Cliente cliente) throws GGASException {

		Collection<TipoEndereco> listaTiposEndereco = controladorCliente.listarTipoEndereco();

		Collection<ClienteEndereco> listaClienteEndereco = new ArrayList<ClienteEndereco>();

		if (!isPostBack(request)) {
			if (cliente != null) {
				listaClienteEndereco.addAll(cliente.getEnderecos());
			}
			request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_ENDERECO, listaClienteEndereco);
		} else {
			listaClienteEndereco = (Collection<ClienteEndereco>) request.getSession()
					.getAttribute(REQUEST_LISTA_CLIENTE_ENDERECO);
		}

		Collections.sort((List<ClienteEndereco>) listaClienteEndereco, new Comparator<ClienteEndereco>() {

			@Override
			public int compare(ClienteEndereco o1, ClienteEndereco o2) {

				return o2.getCorrespondencia().compareTo(o1.getCorrespondencia());
			}
		});

		model.addAttribute("listaTiposEndereco", listaTiposEndereco);

		request.setAttribute(REQUEST_LISTA_CLIENTE_ENDERECO, listaClienteEndereco);
	}

	/**
	 * Método responsável por exibir dados do anexo
	 * 
	 * @param request
	 * @param cliente
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void exibirDadosAnexo(HttpServletRequest request, Cliente cliente) throws GGASException {

		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TIPO_ANEXO);
		Collection<EntidadeConteudo> listaTipoAnexo = controladorEntidadeConteudo
				.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		request.setAttribute("listaTipoAnexo", listaTipoAnexo);

		String codigoAbaAnexoIdentificacao = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ABA_ANEXO_IDENTIFICACAO);

		Collection<ClienteAnexo> listaClienteAnexoAbaEndereco = new ArrayList<ClienteAnexo>();
		Collection<ClienteAnexo> listaClienteAnexoAbaIdentificacao = new ArrayList<ClienteAnexo>();

		if (!isPostBack(request)) {
			if (cliente != null) {

				Collection<ClienteAnexo> listaClienteAnexo = cliente.getAnexos();

				if (listaClienteAnexo != null && !listaClienteAnexo.isEmpty()) {
					for (ClienteAnexo clienteAnexo : listaClienteAnexo) {
						if (clienteAnexo.getAbaAnexo().getChavePrimaria() == Long
								.parseLong(codigoAbaAnexoIdentificacao)) {

							listaClienteAnexoAbaIdentificacao.add(clienteAnexo);
						} else {

							listaClienteAnexoAbaEndereco.add(clienteAnexo);
						}
					}
				}

			}

			request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO, listaClienteAnexoAbaEndereco);
			request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO, listaClienteAnexoAbaIdentificacao);

		} else {
			listaClienteAnexoAbaEndereco = (Collection<ClienteAnexo>) request.getSession()
					.getAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO);
			listaClienteAnexoAbaIdentificacao = (Collection<ClienteAnexo>) request.getSession()
					.getAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO);
		}

		Collections.sort((List<ClienteAnexo>) listaClienteAnexoAbaEndereco, new Comparator<ClienteAnexo>() {

			@Override
			public int compare(ClienteAnexo o1, ClienteAnexo o2) {

				return o2.getDescricaoAnexo().compareTo(o1.getDescricaoAnexo());
			}
		});

		Collections.sort((List<ClienteAnexo>) listaClienteAnexoAbaIdentificacao, new Comparator<ClienteAnexo>() {

			@Override
			public int compare(ClienteAnexo o1, ClienteAnexo o2) {

				return o2.getDescricaoAnexo().compareTo(o1.getDescricaoAnexo());
			}
		});

		request.setAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO, listaClienteAnexoAbaEndereco);
		request.setAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO, listaClienteAnexoAbaIdentificacao);
	}

	/**
	 * Método responsável por adicionar um contato a lista de contatos do cliente
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param contatoClienteVO {@link ContatoClienteVO}
	 * @param resultContato {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param model {@link Model}
	 * @param status {@link Boolean}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarContatoDoCliente")
	public String adicionarContatoDoCliente(ClienteImpl cliente, BindingResult resultClient,
			@ModelAttribute("ContatoClienteVO") ContatoClienteVO contatoClienteVO, BindingResult resultContato,
			HttpServletRequest request, @RequestParam("indexLista") Integer indexLista,
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model)
			throws GGASException {

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);

		model.addAttribute(CLIENTE_FORM, cliente);
		List<ContatoCliente> listaContato = (List<ContatoCliente>) request.getSession().getAttribute(LISTA_CONTATO);

		try {

			if (request.getParameter(FONE_CONTATO) != null && !"".equals(request.getParameter(FONE_CONTATO))) {
				contatoClienteVO
						.setFoneContato(Util.converterCampoStringParaValorInteger(request.getParameter(FONE_CONTATO)));
			}

			if (request.getParameter(FONE2_CONTATO) != null && !"".equals(request.getParameter(FONE2_CONTATO))) {
				contatoClienteVO.setFone2Contato(
						Util.converterCampoStringParaValorInteger(request.getParameter(FONE2_CONTATO)));
			}

			if (indexLista == null || indexLista < 0) {
				ContatoClienteImpl contatoCliente = new ContatoClienteImpl();
				this.popularContato(contatoCliente, contatoClienteVO);

				if ((listaContato == null) || (listaContato.isEmpty())) {
					listaContato = new ArrayList<ContatoCliente>();
					contatoCliente.setPrincipal(Boolean.TRUE);
				} else {
					contatoCliente.setPrincipal(Boolean.FALSE);
				}
				listaContato.add(contatoCliente);
			} else {
				this.alterarContatoCliente(indexLista, contatoClienteVO, listaContato, request);
			}

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
		} catch (FormatoInvalidoException e) {
			LOG.info(e);
			mensagemErroParametrizado(model, request,
					new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, ClienteFone.NUMERO));
		}

		Collections.sort((List<ContatoCliente>) listaContato, new ContatoClienteComparator());
		request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, true);
		request.getSession().setAttribute(LISTA_CONTATO, listaContato);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por verificar o fluxo se é de inclusão ou alteracao.
	 * 
	 * @param fluxo {@link String}
	 * @param cliente {@link ClienteImpl}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return String
	 * @throws GGASException {@link GGASException}
	 */
	private String verificacaoFluxo(String fluxo, ClienteImpl cliente, BindingResult result, HttpServletRequest request,
			boolean status, Model model) throws GGASException {

		if ("inclusao".equals(fluxo)) {
			return exibirInclusaoCliente(cliente, result, request, model);
		} else {
			return exibirAlteracaoCliente(cliente, result, request, status, model);
		}
	}

	/**
	 * Método responsável por remover um contato da lista de contatos do cliente
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param model {@link Model}
	 * @param status {@link Boolean}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "removerContatoDoCliente", method = RequestMethod.POST)
	public String removerContatoDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, HttpServletRequest request, @RequestParam("indexLista") Integer indexLista,
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model)
			throws GGASException {

		Collection<ContatoCliente> listaContato = (Collection<ContatoCliente>) request.getSession()
				.getAttribute(LISTA_CONTATO);
		model.addAttribute(CLIENTE_FORM, cliente);
		if (listaContato != null && indexLista != null) {

			ContatoCliente contatoCliente = ((List<ContatoCliente>) listaContato).get(indexLista.intValue());
			if (contatoCliente.isPrincipal()) {
				((List) listaContato).remove(indexLista.intValue());
				if (!listaContato.isEmpty()) {
					((List<ContatoCliente>) listaContato).get(0).setPrincipal(Boolean.TRUE);
				}
			} else {
				((List) listaContato).remove(indexLista.intValue());
			}
			Collections.sort((List<ContatoCliente>) listaContato, new ContatoClienteComparator());
		}
		request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, false);
		request.getSession().setAttribute(LISTA_CONTATO, listaContato);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método reponsável por atualizar o contato principal do cliente
	 * 
	 * @param cliente
	 * @param resultClient
	 * @param clienteContatoVO
	 * @param result
	 * @param request
	 * @param indexLista
	 * @param fluxo
	 * @param model
	 * @param status
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "atualizarContatoPrincipalDoCliente", method = RequestMethod.POST)
	public String atualizarContatoPrincipalDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, HttpServletRequest request, @RequestParam("indexLista") Integer indexLista,
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model)
			throws GGASException {

		Collection<ContatoCliente> listaContato = (Collection<ContatoCliente>) request.getSession()
				.getAttribute(LISTA_CONTATO);
		if (listaContato != null && indexLista != null) {
			ContatoCliente contatoCliente = null;
			for (Integer i = 0; i < listaContato.size(); i++) {
				contatoCliente = ((ArrayList<ContatoCliente>) listaContato).get(i);
				if (i.equals(indexLista)) {
					contatoCliente.setPrincipal(Boolean.TRUE);
				} else {
					contatoCliente.setPrincipal(Boolean.FALSE);
				}
			}

		}
		request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, true);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Popular contato, preenche a entidade contato, com os dados obtidos de um VO
	 * 
	 * @param contatoCliente
	 * @param contatoClienteVO
	 * @throws GGASException
	 */
	private void popularContato(ContatoClienteImpl contatoCliente, ContatoClienteVO contatoClienteVO)
			throws GGASException {

		if (contatoClienteVO.getTipoContato() != null && contatoClienteVO.getTipoContato() > 0) {
			TipoContatoImpl tipoContato = (TipoContatoImpl) controladorCliente
					.buscarTipoContato(contatoClienteVO.getTipoContato());
			contatoCliente.setTipoContato(tipoContato);
		}
		if (!StringUtils.isEmpty(contatoClienteVO.getNomeContato())) {
			contatoCliente.setNome(contatoClienteVO.getNomeContato());
		}
		if (contatoClienteVO.getCodigoDDDContato() != null && contatoClienteVO.getCodigoDDDContato() > 0) {
			contatoCliente.setCodigoDDD(contatoClienteVO.getCodigoDDDContato());
		}
		if (contatoClienteVO.getFoneContato() != null && contatoClienteVO.getFoneContato() > 0) {
			contatoCliente.setFone(contatoClienteVO.getFoneContato());
		}
		if (contatoClienteVO.getRamalContato() != null && contatoClienteVO.getRamalContato() > 0) {
			contatoCliente.setRamal(contatoClienteVO.getRamalContato());
		}
		if (contatoClienteVO.getCodigoDDD2Contato() != null && contatoClienteVO.getCodigoDDD2Contato() > 0) {
			contatoCliente.setCodigoDDD2(contatoClienteVO.getCodigoDDD2Contato());
		}
		if (contatoClienteVO.getFone2Contato() != null && contatoClienteVO.getFone2Contato() > 0) {
			contatoCliente.setFone2(contatoClienteVO.getFone2Contato());
		}
		if (contatoClienteVO.getRamal2Contato() != null && contatoClienteVO.getRamal2Contato() > 0) {
			contatoCliente.setRamal2(contatoClienteVO.getRamal2Contato());
		}
		if (contatoClienteVO.getProfissaoContato() != null && contatoClienteVO.getProfissaoContato() > 0) {
			contatoCliente.setProfissao(controladorCliente.obterProfissao(contatoClienteVO.getProfissaoContato()));
		} else {
			contatoCliente.setProfissao(null);
		}
		if (contatoClienteVO.getDescricaoAreaContato() != null) {
			contatoCliente.setDescricaoArea(contatoClienteVO.getDescricaoAreaContato());
		}
		if (contatoClienteVO.getEmailContato() != null) {
			contatoCliente.setEmail(contatoClienteVO.getEmailContato());
		}
		if (contatoClienteVO.getCpfContato() != null) {
			contatoCliente.setCpf(Util.removerCaracteresEspeciais(contatoClienteVO.getCpfContato()));
		}

		if (contatoClienteVO.getRgContato() != null) {
			contatoCliente.setRg(contatoClienteVO.getRgContato());
		}

		contatoCliente.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorCliente.validarDadosContatoCliente(contatoCliente);
	}

	/**
	 * Método responsável por adicionar o endereço do cliente
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param clienteEndereco {@link ClienteEnderecoImpl}
	 * @param resultEndereco {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarEnderecoDoCliente")
	public String adicionarEnderecoDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @ModelAttribute("ClienteEnderecoImpl") ClienteEnderecoImpl clienteEndereco,
			BindingResult resultEndereco, HttpServletRequest request,
			@RequestParam(value = "indexLista", required = false) Integer indexLista,
			@RequestParam(value = "fluxo", required = false) String fluxo, 
			@RequestParam(value = "chaveCep", required = false) String chaveCep, 
			@RequestParam("status") boolean status,
			Model model) throws GGASException {

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_ENDERECO);
		List<ClienteEndereco> listaClienteEndereco = (List<ClienteEndereco>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_ENDERECO);
		
		
		if(chaveCep != null && !chaveCep.isEmpty()) {
			Cep cep = controladorEndereco.obterCepPorChave(Long.valueOf(chaveCep));
			clienteEndereco.setCep(cep);
		}

		model.addAttribute("indexLista", indexLista);
		model.addAttribute(CLIENTE_FORM, cliente);
		model.addAttribute("clienteEnderecoEnd", clienteEndereco);

		try {
			if (indexLista == null || indexLista < 0) {
				clienteEndereco.setDadosAuditoria(getDadosAuditoria(request));
				clienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());
				controladorCliente.validarDadosClienteEndereco(clienteEndereco);

				if (!(clienteEndereco.getCep().getChavePrimaria() > 0)) {
					throw new NegocioException(ERRO_CEP_NAO_CADASTRADO);
				}

				if (listaClienteEndereco == null || listaClienteEndereco.isEmpty()) {
					listaClienteEndereco = new ArrayList<ClienteEndereco>();
					clienteEndereco.setCorrespondencia(Boolean.TRUE);
				} else {
					clienteEndereco.setCorrespondencia(Boolean.FALSE);
				}
				
				if (clienteEndereco.getCep().getMunicipio() != null && 
						clienteEndereco.getCep().getMunicipio().getChavePrimaria() > 0) {
					clienteEndereco.setMunicipio(clienteEndereco.getCep().getMunicipio());
				}
				
				controladorCliente.verificarNumeroClienteEnderecoExistente(indexLista, listaClienteEndereco,
						clienteEndereco);

				listaClienteEndereco.add(clienteEndereco);
			} else {
				if (!(clienteEndereco.getCep().getChavePrimaria() > 0)) {
					throw new NegocioException(ERRO_CEP_NAO_CADASTRADO);
				}
				this.alterarClienteEndereco(indexLista, clienteEndereco, listaClienteEndereco, request);
			}

		} catch (NegocioException e) {
			model.addAttribute("abaId", 0);
			mensagemErroParametrizado(model, e);
		}

		request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_ENDERECO, listaClienteEndereco);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por adicionar um anexo relacionado a identificação
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param arquivoAnexo {@link MultipartFile}
	 * @param clienteAnexoVO {@link ClienteAnexoIdentificacaoVO}
	 * @param resultAnexo {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarAnexoAbaIdentificacao")
	public String adicionarAnexoAbaIdentificacao(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @RequestParam("anexoAbaIdentificacao") MultipartFile arquivoAnexo,
			@ModelAttribute("ClienteAnexoIdentificacaoVO") ClienteAnexoIdentificacaoVO clienteAnexoVO,
			BindingResult resultAnexo, HttpServletRequest request, @RequestParam("fluxo") String fluxo,
			@RequestParam("status") boolean status, Model model) throws GGASException {

		Integer indexListaAnexoAbaIdentificacao = clienteAnexoVO.getIndexListaAnexoAbaIdentificacao();
		ClienteAnexoImpl clienteAnexo = new ClienteAnexoImpl();
		Boolean isAbaIdentificacao = Boolean.TRUE;

		model.addAttribute(CLIENTE_FORM, cliente);
		clienteAnexo.setDescricaoAnexo(clienteAnexoVO.getDescricaoAnexoIdentificacao());
		clienteAnexo.setTipoAnexo(controladorEntidadeConteudo.obter(clienteAnexoVO.getTipoAnexo()));
		model.addAttribute("clienteAnexoID", clienteAnexo);

		try {

			List<ClienteAnexo> listaClienteAnexoAbaIdentificacao = (List<ClienteAnexo>) request.getSession()
					.getAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO);

			String codigoAbaAnexoIdentificacao = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ABA_ANEXO_IDENTIFICACAO);
			EntidadeConteudo abaAnexoIdentificacao = controladorEntidadeConteudo
					.obterEntidadeConteudo(Long.valueOf(codigoAbaAnexoIdentificacao));

			if (indexListaAnexoAbaIdentificacao == null || indexListaAnexoAbaIdentificacao < 0) {
				clienteAnexo.setDadosAuditoria(getDadosAuditoria(request));

				this.popularClienteAnexopopularClienteAnexoAbaIdentificacao(clienteAnexo, arquivoAnexo,
						abaAnexoIdentificacao);
				if (listaClienteAnexoAbaIdentificacao == null || listaClienteAnexoAbaIdentificacao.isEmpty()) {
					listaClienteAnexoAbaIdentificacao = new ArrayList<ClienteAnexo>();
				}

				controladorCliente.verificarDescricaoAnexo(indexListaAnexoAbaIdentificacao,
						listaClienteAnexoAbaIdentificacao, clienteAnexo);
				controladorCliente.verificarTipoAnexoAnexo(indexListaAnexoAbaIdentificacao,
						listaClienteAnexoAbaIdentificacao, clienteAnexo);
				listaClienteAnexoAbaIdentificacao.add(clienteAnexo);
			} else {
				alterarClienteAnexo(clienteAnexoVO.getIndexListaAnexoAbaIdentificacao(), arquivoAnexo,
						listaClienteAnexoAbaIdentificacao, request, abaAnexoIdentificacao, isAbaIdentificacao,
						clienteAnexo);
			}

			request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO, listaClienteAnexoAbaIdentificacao);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(e));
		}

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por adicionar um anexo relacionado ao endereço
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param clienteEndereco {@link ClienteEnderecoImpl}
	 * @param resultEndereco {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param clienteAnexoVO {@link ClienteAnexoEnderecoVO}
	 * @param arquivoAnexo {@link MultipartFile}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarAnexoAbaEndereco")
	public String adicionarAnexoAbaEndereco(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @ModelAttribute("ClienteEnderecoImpl") ClienteEnderecoImpl clienteEndereco,
			BindingResult resultEndereco, HttpServletRequest request,
			@ModelAttribute("ClienteAnexoEnderecoVO") ClienteAnexoEnderecoVO clienteAnexoVO,
			@RequestParam("anexo") MultipartFile arquivoAnexo, @RequestParam("fluxo") String fluxo,
			@RequestParam("status") boolean status, Model model) throws GGASException {

		ClienteAnexoImpl clienteAnexo = new ClienteAnexoImpl();
		Boolean isAbaIdentificacao = Boolean.FALSE;
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_ENDERECO);
		List<ClienteAnexo> listaClienteAnexoAbaEndereco = (List<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO);

		String codigoAbaAnexoEndereco = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ABA_ANEXO_ENDERECO);
		EntidadeConteudo abaAnexoEndereco = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(codigoAbaAnexoEndereco));

		clienteAnexo.setDescricaoAnexo(clienteAnexoVO.getDescricaoAnexoEndVO());
		clienteAnexo.setTipoAnexo(controladorEntidadeConteudo.obter(clienteAnexoVO.getTipoAnexoDeEndereco()));
		model.addAttribute(CLIENTE_FORM, cliente);
		model.addAttribute("ClienteAnexoEnderecoVO", clienteAnexo);
		model.addAttribute("clienteEndereco", clienteEndereco);
		try {
			if (clienteAnexoVO.getIndexListaAnexoAbaEndereco() < 0) {

				clienteAnexo.setDadosAuditoria(getDadosAuditoria(request));

				this.popularClienteAnexopopularClienteAnexoAbaEndereco(clienteAnexo, arquivoAnexo, abaAnexoEndereco);
				if (listaClienteAnexoAbaEndereco == null || listaClienteAnexoAbaEndereco.isEmpty()) {
					listaClienteAnexoAbaEndereco = new ArrayList<ClienteAnexo>();
				}

				controladorCliente.verificarDescricaoAnexo(clienteAnexoVO.getIndexListaAnexoAbaEndereco(),
						listaClienteAnexoAbaEndereco, clienteAnexo);
				listaClienteAnexoAbaEndereco.add(clienteAnexo);
			} else {
				this.popularClienteAnexopopularClienteAnexoAbaEndereco(clienteAnexo, arquivoAnexo, abaAnexoEndereco);
				alterarClienteAnexo(clienteAnexoVO.getIndexListaAnexoAbaEndereco(), arquivoAnexo,
						listaClienteAnexoAbaEndereco, request, abaAnexoEndereco, isAbaIdentificacao, clienteAnexo);
			}
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(e));
		}

		request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO, listaClienteAnexoAbaEndereco);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por remover endereço do cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerEnderecoDoCliente")
	public String removerEnderecoDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @RequestParam("indexLista") Integer indexLista,
			@RequestParam("fluxo") String fluxo, HttpServletRequest request, @RequestParam("status") boolean status,
			Model model) throws GGASException {

		Collection<ClienteEndereco> listaClienteEndereco = (Collection<ClienteEndereco>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_ENDERECO);

		model.addAttribute(CLIENTE_FORM, cliente);
		if (listaClienteEndereco != null && indexLista != null) {
			ClienteEndereco clienteEndereco = ((List<ClienteEndereco>) listaClienteEndereco).get(indexLista.intValue());
			if (clienteEndereco.getCorrespondencia()) {
				((List) listaClienteEndereco).remove(indexLista.intValue());
				if (!listaClienteEndereco.isEmpty()) {
					((List<ClienteEndereco>) listaClienteEndereco).get(0).setCorrespondencia(Boolean.TRUE);
				}
			} else {
				((List) listaClienteEndereco).remove(indexLista.intValue());
			}
		}
		request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_ENDERECO, listaClienteEndereco);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_ENDERECO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por remover um anexo relacionado ao endereço do cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param indexListaAnexoAbaEndereco {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked" })
	@RequestMapping("removerAnexoAbaEndereco")
	public String removerAnexoAbaEndereco(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @RequestParam("indexListaAnexoAbaEndereco") Integer indexListaAnexoAbaEndereco,
			HttpServletRequest request, @RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status,
			Model model) throws GGASException {

		Collection<ClienteAnexo> listaClienteAnexoAbaEndereco = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO);
		model.addAttribute(CLIENTE_FORM, cliente);
		if (listaClienteAnexoAbaEndereco != null && indexListaAnexoAbaEndereco != null) {
			ClienteAnexo clienteAnexo = ((List<ClienteAnexo>) listaClienteAnexoAbaEndereco)
					.get(indexListaAnexoAbaEndereco.intValue());
			listaClienteAnexoAbaEndereco.remove(clienteAnexo);
		}
		request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO, listaClienteAnexoAbaEndereco);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_ENDERECO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por remover um anexo da listagem na aba de identificação
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultCliente {@link BindingResult}
	 * @param indexListaAnexoAbaIdentificacao {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked" })
	@RequestMapping("removerAnexoAbaIdentificacao")
	public String removerAnexoAbaIdentificacao(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultCliente,
			@RequestParam("indexListaAnexoAbaIdentificacao") Integer indexListaAnexoAbaIdentificacao,
			HttpServletRequest request, @RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status,
			Model model) throws GGASException {

		Collection<ClienteAnexo> listaClienteAnexoAbaIdentificacao = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO);
		model.addAttribute(CLIENTE_FORM, cliente);
		if (listaClienteAnexoAbaIdentificacao != null && indexListaAnexoAbaIdentificacao != null) {
			ClienteAnexo clienteAnexo = ((List<ClienteAnexo>) listaClienteAnexoAbaIdentificacao)
					.get(indexListaAnexoAbaIdentificacao.intValue());
			listaClienteAnexoAbaIdentificacao.remove(clienteAnexo);
		}

		request.getSession().setAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO, listaClienteAnexoAbaIdentificacao);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultCliente, request, status, model);
	}

	/**
	 * Método responsável por atualizar a correspondência do cliente
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param indexLista {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("atualizarCorrespondenciaDoCliente")
	public String atualizarCorrespondenciaDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @RequestParam("indexLista") Integer indexLista, HttpServletRequest request,
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model)
			throws GGASException {

		Collection<ClienteEndereco> listaClienteEndereco = (Collection<ClienteEndereco>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_ENDERECO);
		if (listaClienteEndereco != null && indexLista != null) {
			ClienteEndereco clienteEndereco = null;
			for (int i = 0; i < listaClienteEndereco.size(); i++) {
				clienteEndereco = ((ArrayList<ClienteEndereco>) listaClienteEndereco).get(i);
				if (i == indexLista) {
					clienteEndereco.setCorrespondencia(Boolean.TRUE);
				} else {
					clienteEndereco.setCorrespondencia(Boolean.FALSE);
				}
			}

		}
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_ENDERECO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * 
	 * @param clienteAnexo
	 * @param anexo
	 * @param abaAnexo
	 * @throws GGASException
	 */
	private void popularClienteAnexopopularClienteAnexoAbaEndereco(ClienteAnexoImpl clienteAnexo, MultipartFile anexo,
			EntidadeConteudo abaAnexo) throws GGASException {

		ParametroSistema codigoTamanhoMaximoAnexo = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_TAMANHO_MAXIMO_ANEXO);

		clienteAnexo.setAbaAnexo(abaAnexo);

		if (anexo == null) {
			clienteAnexo.setDocumentoAnexo(null);
		} else {

			String[] extensao = { ".PDF" };

			if (controladorEmpresa.validarImagemDoArquivo(anexo, extensao,
					Integer.parseInt(codigoTamanhoMaximoAnexo.getValor()), ClienteAnexo.ERRO_NEGOCIO_TIPO_ARQUIVO_ANEXO,
					ClienteAnexo.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO, Boolean.TRUE)) {
				try {
					clienteAnexo.setDocumentoAnexo(anexo.getBytes());
				} catch (IOException e) {
					throw new GGASException(e);
				}
			}
		}

		clienteAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorCliente.validarDadosClienteAnexo(clienteAnexo);
	}

	/**
	 * 
	 * @param clienteAnexo
	 * @param arquivoAnexo
	 * @param abaAnexo
	 * @throws GGASException
	 */
	private void popularClienteAnexopopularClienteAnexoAbaIdentificacao(ClienteAnexo clienteAnexo,
			MultipartFile arquivoAnexo, EntidadeConteudo abaAnexo) throws GGASException {

		ParametroSistema codigoTamanhoMaximoAnexo = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_TAMANHO_MAXIMO_ANEXO);

		clienteAnexo.setAbaAnexo(abaAnexo);

		if (arquivoAnexo == null) {
			clienteAnexo.setDocumentoAnexo(null);
		} else {

			String[] extensao = { ".PDF" };

			if (controladorEmpresa.validarImagemDoArquivo(arquivoAnexo, extensao,
					Integer.parseInt(codigoTamanhoMaximoAnexo.getValor()), ClienteAnexo.ERRO_NEGOCIO_TIPO_ARQUIVO_ANEXO,
					ClienteAnexo.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO, Boolean.TRUE)) {
				try {
					clienteAnexo.setDocumentoAnexo(arquivoAnexo.getBytes());
				} catch (IOException e) {
					throw new GGASException(e);
				}
			}
		}

		clienteAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorCliente.validarDadosClienteAnexo(clienteAnexo);
	}

	/**
	 * 
	 * @param clienteEndereco
	 * @param clienteEnderecoAlterado
	 * @throws GGASException
	 */
	private void popularClienteEndereco(ClienteEndereco clienteEndereco, ClienteEnderecoImpl clienteEnderecoAlterado)
			throws GGASException {

		if ((clienteEnderecoAlterado.getTipoEndereco() != null)
				&& (clienteEnderecoAlterado.getTipoEndereco().getChavePrimaria() > 0)) {
			clienteEndereco.setTipoEndereco(controladorCliente
					.buscarTipoEndereco(clienteEnderecoAlterado.getTipoEndereco().getChavePrimaria()));
		}

		if ((clienteEnderecoAlterado.getCep() != null)
				&& (!(clienteEnderecoAlterado.getCep().getChavePrimaria() == 0))) {
			clienteEndereco
					.setCep(controladorEndereco.obterCepPorChave(clienteEnderecoAlterado.getCep().getChavePrimaria()));
		}

		if (clienteEnderecoAlterado.getCep() == null) {
			throw new NegocioException("O(s) campo(s) CEP é(são) de preenchimento obrigatório.");
		} else if (clienteEnderecoAlterado.getCep().getChavePrimaria() == 0) {
			throw new NegocioException("CEP não cadastrado no sistema.");
		}

		if (clienteEnderecoAlterado.getNumero() != null) {
			clienteEndereco.setNumero(clienteEnderecoAlterado.getNumero());
		}

		if (clienteEnderecoAlterado.getComplemento() != null) {
			clienteEndereco.setComplemento(clienteEnderecoAlterado.getComplemento());
		}

		if (clienteEnderecoAlterado.getEnderecoReferencia() != null) {
			clienteEndereco.setEnderecoReferencia(clienteEnderecoAlterado.getEnderecoReferencia());
		}

		if (clienteEnderecoAlterado.getCaixaPostal() != null) {
			clienteEndereco.setCaixaPostal(clienteEnderecoAlterado.getCaixaPostal());
		}

		if (clienteEnderecoAlterado.getCep() != null) {
			clienteEndereco.setMunicipio(controladorMunicipio.obterMunicipioPorNomeUF(
					clienteEnderecoAlterado.getCep().getNomeMunicipio(), clienteEndereco.getCep().getUf()));
		}

		if (clienteEndereco.getCep() != null && clienteEndereco.getCep().getMunicipio() != null) {
			clienteEndereco.setMunicipio(
					(Municipio) controladorMunicipio.obter(clienteEndereco.getCep().getMunicipio().getChavePrimaria()));
		}

		if (clienteEnderecoAlterado.getTipoEndereco() != null
				&& clienteEnderecoAlterado.getTipoEndereco().getChavePrimaria() > 0) {
			clienteEndereco.setChavePrimaria(clienteEnderecoAlterado.getTipoEndereco().getChavePrimaria());
		}

		if (clienteEnderecoAlterado.getIndicadorPrincipal() != null) {
			clienteEndereco.setIndicadorPrincipal(clienteEnderecoAlterado.getIndicadorPrincipal());
		} else {
			clienteEndereco.setIndicadorPrincipal(1);
		}

		clienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorCliente.validarDadosClienteEndereco(clienteEndereco);
	}

	/**
	 * Método responsável por adicionar um telefone do cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param clienteFone {@link ClienteFoneImpl}
	 * @param resultFone {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param numero {@link String}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Moedel}
	 * @param codigoDDD {@link String}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarTelefoneDoCliente")
	public String adicionarTelefoneDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @ModelAttribute("ClienteFoneImpl") ClienteFoneImpl clienteFone,
			BindingResult resultFone, HttpServletRequest request, @RequestParam("numeroFone") String numero, 
			@RequestParam("codigoDDD") String codigoDDD, @RequestParam("indexLista") Integer indexLista, 
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model) throws GGASException {

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_TELEFONE);
		List<ClienteFone> listaClienteFone = (List<ClienteFone>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_FONE);
		try {
			if (numero != null && !"".equals(numero) && numero.length() >= 8 && codigoDDD.length() > 1) {
				clienteFone.setNumero(Util.converterCampoStringParaValorInteger(numero));
			}
			if (indexLista == null || indexLista < 0) {

				clienteFone.setChavePrimaria(0);
				clienteFone.setUltimaAlteracao(Calendar.getInstance().getTime());

				controladorCliente.validarDadosClienteFone(clienteFone);

				if (listaClienteFone == null || listaClienteFone.isEmpty()) {
					listaClienteFone = new ArrayList<ClienteFone>();
					clienteFone.setIndicadorPrincipal(Boolean.TRUE);
				} else {
					clienteFone.setIndicadorPrincipal(Boolean.FALSE);
				}
				model.addAttribute(CLIENTE_FORM, cliente);
				controladorCliente.verificarNumeroTelefoneExistente(indexLista, listaClienteFone, clienteFone);
				model.addAttribute("indexLista", indexLista);
				listaClienteFone.add(clienteFone);
			} else {
				this.alterarClienteFone(indexLista, clienteFone, listaClienteFone);
			}

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, e);
		} catch (FormatoInvalidoException e) {
			LOG.info(e);
			mensagemErroParametrizado(model, request,
					new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, ClienteFone.NUMERO));
		}

		request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_FONE, listaClienteFone);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por remover um telefone do cliente.
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param indexLista {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerTelefoneDoCliente")
	public String removerTelefoneDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, @RequestParam("indexLista") Integer indexLista, HttpServletRequest request,
			@RequestParam("fluxo") String fluxo, @RequestParam("status") boolean status, Model model)
			throws GGASException {

		Collection<ClienteFone> listaClienteFone = (Collection<ClienteFone>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_FONE);

		if (listaClienteFone != null && indexLista != null) {

			ClienteFone clienteFone = ((List<ClienteFone>) listaClienteFone).get(indexLista.intValue());
			if (clienteFone.getIndicadorPrincipal()) {
				((List) listaClienteFone).remove(indexLista.intValue());
				if (!listaClienteFone.isEmpty()) {
					((List<ClienteFone>) listaClienteFone).get(0).setIndicadorPrincipal(Boolean.TRUE);
				}
			} else {
				((List) listaClienteFone).remove(indexLista.intValue());
			}
		}
		request.getSession().setAttribute(REQUEST_LISTA_CLIENTE_FONE, listaClienteFone);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_TELEFONE);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		model.addAttribute(CLIENTE_FORM, cliente);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * Método responsável por atualizar o telefone principal do cliente
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param resultClient {@link BindingResult}
	 * @param resultFone {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param indexLista {@link Integer}
	 * @param fluxo {@link String}
	 * @param status {@link Boolean}
	 * @param model {@link Model}
	 * @return verificacaoFluxo {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("atualizarTelefonePrincipalDoCliente")
	public String atualizarTelefonePrincipalDoCliente(@ModelAttribute("ClienteImpl") ClienteImpl cliente,
			BindingResult resultClient, BindingResult resultFone, HttpServletRequest request,
			@RequestParam("indexLista") Integer indexLista, @RequestParam("fluxo") String fluxo,
			@RequestParam("status") boolean status, Model model) throws GGASException {

		ArrayList<ClienteFone> listaClienteFone = (ArrayList<ClienteFone>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_FONE);

		if (listaClienteFone != null && indexLista != null) {
			ClienteFoneImpl clienteFone = null;
			for (int i = 0; i < listaClienteFone.size(); i++) {
				clienteFone = (ClienteFoneImpl) listaClienteFone.get(i);
				if (i == indexLista) {
					clienteFone.setIndicadorPrincipal(Boolean.TRUE);
				} else {
					clienteFone.setIndicadorPrincipal(Boolean.FALSE);
				}
			}

		}

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_TELEFONE);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return verificacaoFluxo(fluxo, cliente, resultClient, request, status, model);
	}

	/**
	 * 
	 * 
	 * @return String
	 * @throws GGASException {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		return String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();
	}

	/**
	 * 
	 * @param clienteFoneExistente
	 * @param clienteFoneAlterado
	 * @throws GGASException
	 */
	private void popularClienteFone(ClienteFone clienteFoneExistente, ClienteFoneImpl clienteFoneAlterado)
			throws GGASException {

		if ((clienteFoneAlterado.getTipoFone() != null) && clienteFoneAlterado.getTipoFone().getChavePrimaria() > 0) {
			clienteFoneExistente.setTipoFone(
					controladorCliente.buscarTipoFone(clienteFoneAlterado.getTipoFone().getChavePrimaria()));
		}

		if (clienteFoneAlterado.getCodigoDDD() != null) {
			clienteFoneExistente.setCodigoDDD(clienteFoneAlterado.getCodigoDDD());
		}

		if (clienteFoneAlterado.getNumero() != null) {
			clienteFoneExistente.setNumero(clienteFoneAlterado.getNumero());
		}

		if (clienteFoneAlterado.getRamal() != null) {
			clienteFoneExistente.setRamal(clienteFoneAlterado.getRamal());
		} else {
			clienteFoneExistente.setRamal(null);
		}
		if (clienteFoneAlterado.getChavePrimaria() > 0) {
			clienteFoneExistente.setChavePrimaria(clienteFoneAlterado.getChavePrimaria());
		}

		clienteFoneExistente.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorCliente.validarDadosClienteFone(clienteFoneExistente);

	}

	/**
	 * Método responsável por exibir dados relacionados a documentação do cliente.
	 * 
	 * @param model
	 * @throws GGASException
	 */
	private void exibirDadosAbaDocumentacao(Model model) throws GGASException {

		model.addAttribute(ORGAOS_EMISSORES, controladorCliente.listarOrgaoExpedidor());
		model.addAttribute(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		model.addAttribute(FAIXAS_RENDA, controladorCliente.listarFaixaRendaFamiliar());
		model.addAttribute(NACIONALIDADES, controladorCliente.listarNacionalidade());
		model.addAttribute(ATIVIDADES_ECONOMICAS, controladorCliente.listarAtividadeEconomica());
		model.addAttribute(SEXOS, controladorCliente.listarPessoaSexo());

		model.addAttribute(MASCARA_INSCRICAO_ESTADUAL,
				controladorParametroSistema.obterValorDoParametroPorCodigo("MASCARA_INSCRICAO_ESTADUAL"));

	}

	/**
	 * Método responsável por popular as listas do cliente
	 * 
	 * @param cliente
	 * @param model
	 * @param request
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void popularCliente(Cliente cliente, HttpServletRequest request) throws GGASException {

		Collection<ContatoCliente> listaContato = (Collection<ContatoCliente>) request.getSession()
				.getAttribute(LISTA_CONTATO);
		Collection<ClienteEndereco> listaClienteEndereco = (Collection<ClienteEndereco>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_ENDERECO);
		Collection<ClienteFone> listaClienteFone = (Collection<ClienteFone>) request.getSession()
				.getAttribute(REQUEST_LISTA_CLIENTE_FONE);
		Collection<ClienteAnexo> listaClienteAnexoAbaEndereco = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO);
		Collection<ClienteAnexo> listaClienteAnexoAbaIdentificacao = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO);

		if (listaContato != null && !listaContato.isEmpty()) {
			cliente.getContatos().clear();
			for (Iterator<ContatoCliente> iterator = listaContato.iterator(); iterator.hasNext();) {
				cliente.getContatos().add(iterator.next());
			}
		} else {
			cliente.getContatos().clear();
		}

		if (listaClienteEndereco != null && !listaClienteEndereco.isEmpty()) {
			cliente.getEnderecos().clear();
			ClienteEndereco clienteEndereco = null;
			for (Iterator<ClienteEndereco> iterator = listaClienteEndereco.iterator(); iterator.hasNext();) {
				clienteEndereco = iterator.next();
				clienteEndereco.setDadosAuditoria(super.getDadosAuditoria(request));
				cliente.getEnderecos().add(clienteEndereco);
			}
		} else {
			cliente.getEnderecos().clear();
		}

		if (listaClienteFone != null && !listaClienteFone.isEmpty()) {
			cliente.getFones().clear();
			for (ClienteFone fone : listaClienteFone) {
				cliente.getFones().add(fone);
			}

		} else {
			cliente.getFones().clear();
		}

		if (listaClienteAnexoAbaEndereco != null && !listaClienteAnexoAbaEndereco.isEmpty()
				&& listaClienteAnexoAbaIdentificacao != null && !listaClienteAnexoAbaIdentificacao.isEmpty()) {

			cliente.getAnexos().clear();
			ClienteAnexo anexo = null;
			for (Iterator<ClienteAnexo> iterator = listaClienteAnexoAbaEndereco.iterator(); iterator.hasNext();) {
				anexo = iterator.next();
				cliente.getAnexos().add(anexo);
			}

			for (Iterator<ClienteAnexo> iterator = listaClienteAnexoAbaIdentificacao.iterator(); iterator.hasNext();) {
				anexo = iterator.next();
				cliente.getAnexos().add(anexo);
			}

		} else if (listaClienteAnexoAbaEndereco != null && !listaClienteAnexoAbaEndereco.isEmpty()) {

			cliente.getAnexos().clear();
			ClienteAnexo anexo = null;
			for (Iterator<ClienteAnexo> iterator = listaClienteAnexoAbaEndereco.iterator(); iterator.hasNext();) {
				anexo = iterator.next();
				cliente.getAnexos().add(anexo);
			}

		} else if (listaClienteAnexoAbaIdentificacao != null && !listaClienteAnexoAbaIdentificacao.isEmpty()) {

			cliente.getAnexos().clear();
			ClienteAnexo anexo = null;
			for (Iterator<ClienteAnexo> iterator = listaClienteAnexoAbaIdentificacao.iterator(); iterator.hasNext();) {
				anexo = iterator.next();
				cliente.getAnexos().add(anexo);
			}

		} else {

			cliente.getAnexos().clear();
		}

		cliente.setDadosAuditoria(getDadosAuditoria(request));
		if (cliente.getCpf() != null && !"".equals(cliente.getCpf())) {
			cliente.setCpf(Util.removerCaracteresEspeciais(cliente.getCpf()));
			Util.validarCPF(cliente.getCpf());
		} else {
			if (cliente.getCnpj() != null && !"".equals(cliente.getCnpj())) {
				cliente.setCnpj(Util.removerCaracteresEspeciais(cliente.getCnpj()));
				Util.validarCNPJ(cliente.getCnpj());
			}
		}

		controladorCliente.verificarEmailPrinciapObrigatorio(cliente.getEmailPrincipal());
		controladorCliente.verificarClienteExistente(cliente);
	}

	/**
	 * Método responsável pela alteração do telefone do cliente.
	 * 
	 * @param indexLista
	 * @param clienteFone
	 * @param listaClienteFone
	 * @throws GGASException
	 */
	private void alterarClienteFone(Integer indexLista, ClienteFoneImpl clienteFone, List<ClienteFone> listaClienteFone)
			throws GGASException {

		if (listaClienteFone != null && indexLista != null) {
			clienteFone.setUltimaAlteracao(Calendar.getInstance().getTime());
			controladorCliente.validarDadosClienteFone(clienteFone);
			controladorCliente.verificarNumeroTelefoneExistente(indexLista, listaClienteFone, clienteFone);
			ClienteFone clienteFoneExistente = null;
			for (int i = 0; i < listaClienteFone.size(); i++) {
				clienteFoneExistente = listaClienteFone.get(i);
				if (i == indexLista && clienteFoneExistente != null) {
					this.popularClienteFone(clienteFoneExistente, clienteFone);
				}
			}
		}
	}

	/**
	 * 
	 * @param indexLista
	 * @param contatoClienteVO
	 * @param listaContato
	 * @param request
	 * @throws GGASException
	 */
	private void alterarContatoCliente(Integer indexLista, ContatoClienteVO contatoClienteVO,
			List<ContatoCliente> listaContato, HttpServletRequest request) throws GGASException {

		if (listaContato != null && indexLista != null) {
			ContatoClienteImpl contatoClienteAlterado = (ContatoClienteImpl) controladorCliente.criarContatoCliente();
			this.popularContato(contatoClienteAlterado, contatoClienteVO);

			ContatoClienteImpl contatoClienteExistente = null;
			for (int i = 0; i < listaContato.size(); i++) {
				contatoClienteExistente = (ContatoClienteImpl) listaContato.get(i);
				if (i == indexLista && contatoClienteExistente != null) {
					this.popularContato(contatoClienteExistente, contatoClienteVO);
					contatoClienteExistente.setDadosAuditoria(getDadosAuditoria(request));
				}
			}
		}
	}

	/**
	 * 
	 * @param indexLista
	 * @param clienteEnderecoAlterado
	 * @param listaClienteEndereco
	 * @param request
	 * @throws GGASException
	 */
	private void alterarClienteEndereco(Integer indexLista, ClienteEnderecoImpl clienteEnderecoAlterado,
			List<ClienteEndereco> listaClienteEndereco, HttpServletRequest request) throws GGASException {

		if (listaClienteEndereco != null && indexLista != null) {
			ClienteEndereco clienteEnderecoMontado = (ClienteEndereco) controladorCliente.criarClienteEndereco();
			this.popularClienteEndereco(clienteEnderecoMontado, clienteEnderecoAlterado);
			controladorCliente.verificarNumeroClienteEnderecoExistente(indexLista, listaClienteEndereco,
					clienteEnderecoMontado);
			ClienteEndereco clienteEnderecoExistente = null;
			for (int i = 0; i < listaClienteEndereco.size(); i++) {
				clienteEnderecoExistente = listaClienteEndereco.get(i);
				if (i == indexLista && clienteEnderecoExistente != null) {
					this.popularClienteEndereco(clienteEnderecoExistente, clienteEnderecoAlterado);
					clienteEnderecoExistente.setDadosAuditoria(getDadosAuditoria(request));
				}
			}
		}
	}

	/**
	 * 
	 * @param indexListaAnexoAbaEndereco {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @return
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("visualizarAnexoAbaEnderecoDoCliente")
	public String visualizarAnexoAbaEndereco(
			@RequestParam("indexListaAnexoAbaEndereco") Integer indexListaAnexoAbaEndereco, HttpServletRequest request,
			HttpServletResponse response) throws GGASException {

		Collection<ClienteAnexo> listaClienteAnexoAbaEndereco = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_ENDERECO);

		if (listaClienteAnexoAbaEndereco != null && indexListaAnexoAbaEndereco != null) {

			for (int i = 0; i < listaClienteAnexoAbaEndereco.size(); i++) {

				ClienteAnexo clienteAnexo = ((ArrayList<ClienteAnexo>) listaClienteAnexoAbaEndereco).get(i);
				if (i == indexListaAnexoAbaEndereco && clienteAnexo != null) {

					byte[] anexo = clienteAnexo.getDocumentoAnexo();

					FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
					Date dataAtual = Calendar.getInstance().getTime();

					ServletOutputStream servletOutputStream;
					try {
						servletOutputStream = response.getOutputStream();
						response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
						response.setContentLength(anexo.length);

						String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + "_";
						response.addHeader("Content-Disposition", "attachment; filename=anexo" + "_" + data + ".pdf");
						servletOutputStream.write(anexo, 0, anexo.length);
						servletOutputStream.flush();
						servletOutputStream.close();
					} catch (IOException e) {
						throw new GGASException(e);
					}
				}
			}
		}

		return null;
	}

	/**
	 * 
	 * @param indexListaAnexoAbaEndereco {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @return
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("visualizarAnexoAbaIdentificacao")
	public String visualizarAnexoAbaIdentificacao(
			@RequestParam("indexListaAnexoAbaIdentificacao") Integer indexListaAnexoAbaEndereco,
			HttpServletRequest request, HttpServletResponse response) throws GGASException {

		Collection<ClienteAnexo> listaClienteAnexoAbaIdentificacao = (Collection<ClienteAnexo>) request.getSession()
				.getAttribute(LISTA_CLIENTE_ANEXO_ABA_IDENTIFICACAO);

		if (listaClienteAnexoAbaIdentificacao != null && indexListaAnexoAbaEndereco != null) {

			for (int i = 0; i < listaClienteAnexoAbaIdentificacao.size(); i++) {

				ClienteAnexo clienteAnexo = ((ArrayList<ClienteAnexo>) listaClienteAnexoAbaIdentificacao).get(i);
				if (i == indexListaAnexoAbaEndereco && clienteAnexo != null) {

					byte[] anexo = clienteAnexo.getDocumentoAnexo();

					FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
					Date dataAtual = Calendar.getInstance().getTime();

					ServletOutputStream servletOutputStream;

					try {
						servletOutputStream = response.getOutputStream();
						response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
						response.setContentLength(anexo.length);

						String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + ".";
						response.addHeader("Content-Disposition", "attachment; filename=anexo" + "_" + data +"pdf");
						servletOutputStream.write(anexo, 0, anexo.length);
						servletOutputStream.flush();
						servletOutputStream.close();
					} catch (IOException e) {
						throw new GGASException(e);
					}
				}
			}
		}

		return null;
	}

	/**
	 * 
	 * @param indexListaAnexo
	 * @param arquivoAnexo
	 * @param listaClienteAnexo
	 * @param request
	 * @param abaAnexo
	 * @param isAbaIdentificacao
	 * @param clienteAnexo
	 * @throws GGASException
	 */
	private void alterarClienteAnexo(Integer indexListaAnexo, MultipartFile arquivoAnexo,
			List<ClienteAnexo> listaClienteAnexo, HttpServletRequest request, EntidadeConteudo abaAnexo,
			Boolean isAbaIdentificacao, ClienteAnexoImpl clienteAnexo) throws GGASException {

		if (listaClienteAnexo != null && indexListaAnexo != null) {
			ClienteAnexoImpl clienteAnexoExistente = null;
			controladorCliente.verificarTipoAnexoAnexo(indexListaAnexo, listaClienteAnexo, clienteAnexo);
			controladorCliente.verificarDescricaoAnexo(indexListaAnexo, listaClienteAnexo, clienteAnexo);

			for (int i = 0; i < listaClienteAnexo.size(); i++) {
				clienteAnexoExistente = (ClienteAnexoImpl) listaClienteAnexo.get(i);
				if (i == indexListaAnexo && clienteAnexoExistente != null) {

					clienteAnexoExistente.setDescricaoAnexo(clienteAnexo.getDescricaoAnexo());
					clienteAnexoExistente.setTipoAnexo(clienteAnexo.getTipoAnexo());
					if (isAbaIdentificacao) {

						this.popularClienteAnexopopularClienteAnexoAbaIdentificacao(clienteAnexoExistente, arquivoAnexo,
								abaAnexo);
					} else {
						this.popularClienteAnexopopularClienteAnexoAbaEndereco(clienteAnexoExistente, arquivoAnexo,
								abaAnexo);
					}
					clienteAnexoExistente.setDadosAuditoria(getDadosAuditoria(request));
				}
			}
		}
	}

	/**
	 * Popup exibir inclusão cliente
	 * 
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @param cliente {@link ClienteImpl}
	 * @return popupExibirInclusaoCliente {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("popupExibirInclusaoCliente")
	public String popupExibirInclusaoCliente(Model model, HttpServletRequest request, ClienteImpl cliente)
			throws GGASException {

		this.carregarDadosInclusaoCliente(model, request, cliente);

		return "popupExibirInclusaoCliente";

	}

	/**
	 * Método responsável por exibir a tela pesquisa de Cliente
	 * 
	 * @param model {@link Model}
	 * @param popupVO {@link PesquisarClientePopupVO}
	 * @return exibirPesquisaClientePopup {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaClientePopup")
	public String exibirPesquisaClientePopup(Model model, PesquisarClientePopupVO popupVO) throws GGASException {

		validarPessoaFisicaEJuridica(model, popupVO);

		model.addAttribute(ATIVIDADES_ECONOMICAS, controladorCliente.listarAtividadeEconomica());
		model.addAttribute(FUNCIONALIDADE, popupVO.getFuncionalidade());

		return "exibirPesquisaClientePopup";
	}

	/**
	 * Pesquisar cliente popup
	 * 
	 * @param cliente {@link ClienteImpl}
	 * @param pcPopupVo {@link PesquisarClientePopupVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaClientePopup {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarClientePopup")
	public String pesquisarClientePopup(ClienteImpl cliente, PesquisarClientePopupVO pcPopupVo, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (pcPopupVo.isMultSelect()) {
			if ((pcPopupVo.verificaChaves() && pcPopupVo.getChavesPrimarias().length > 0)) {
				filtro.put(CHAVES_PRIMARIAS, pcPopupVo.getChavesPrimarias());
				if ((pcPopupVo.getListaIdCliente() != null) && pcPopupVo.getListaIdCliente().length > 0
						&& (pcPopupVo.getListaIdCliente().length == 1 && pcPopupVo.getListaIdCliente()[0] != 0L)) {
					int i = 0;
					for (Long ids : pcPopupVo.getChavesPrimarias()) {
						if (ids != 0) {
							i++;
						}
					}
					for (Long ids : pcPopupVo.getListaIdCliente()) {
						if (ids != 0) {
							i++;
						}
					}

					Long[] listasIds = new Long[i];
					for (Long ids : pcPopupVo.getChavesPrimarias()) {
						if (ids != 0) {
							listasIds[i] = ids;
							i = i - 1;
						}
					}
					for (Long ids : pcPopupVo.getListaIdCliente()) {
						if (ids != 0) {
							listasIds[i] = ids;
							i = i - 1;
						}
					}
					filtro.put(LISTA_ID_CLIENTE, listasIds);
					filtro.put(CHAVES_PRIMARIAS, listasIds);
				}

				if (pcPopupVo.isComGrupoEconomico()) {
					filtro.put(COM_GRUPO_ECONOMICO, true);
				}
			} else {
				if (pcPopupVo.getListaIdCliente() != null && pcPopupVo.getListaIdCliente().length > 0) {
					filtro.put(LISTA_ID_CLIENTE, pcPopupVo.getListaIdCliente());
				}
			}
		}

		model.addAttribute(CHAVES_PRIMARIAS, null);

		this.prepararFiltro(filtro, cliente, pcPopupVo.getHabilitado(), pcPopupVo.getNumeroCep(), null);

		if (pcPopupVo.isPessoaFisica() != (pcPopupVo.isPessoaJuridica())) {
			if (pcPopupVo.isPessoaFisica()) {
				filtro.put(CODIGO_TIPO_PESSOA, TipoPessoa.PESSOA_FISICA);
			} else {
				filtro.put(CODIGO_TIPO_PESSOA, TipoPessoa.PESSOA_JURIDICA);
			}
		}

		model.addAttribute(CHAVES_PRIMARIAS, null);

		if (!StringUtils.isEmpty(pcPopupVo.getFuncionalidade())) {
			filtro.put(FUNCIONALIDADE, pcPopupVo.getFuncionalidade());
		}

		Collection<Cliente> listaCliente = new ArrayList<Cliente>();

		if (pcPopupVo.isComGrupoEconomico()) {
			filtro.put(COM_GRUPO_ECONOMICO, pcPopupVo.isComGrupoEconomico());
			listaCliente = controladorCliente.consultarClientes(filtro);
			Collection<Cliente> listaClienteExcluido = (Collection<Cliente>) request.getSession()
					.getAttribute(LISTA_CLIENTE_EXCLUIDO);
			if (listaClienteExcluido != null && !listaCliente.isEmpty()) {
				listaCliente.removeAll(listaClienteExcluido);
				listaCliente.addAll(listaClienteExcluido);
			}
			Collection<Cliente> listaClienteInserido = (Collection<Cliente>) request.getSession()
					.getAttribute(LISTA_CLIENTE);
			if (listaClienteInserido != null && !listaClienteInserido.isEmpty()) {
				listaCliente.removeAll(listaClienteInserido);
			}
			filtrarListaClientePopupMultSelect(listaCliente, filtro);
		} else if (StringUtils.isEmpty(pcPopupVo.getFuncionalidade())) {
			listaCliente = controladorCliente.consultarClientes(filtro);
		} else {
			listaCliente = controladorCliente.consultarClientesContrato(filtro);
		}

		model.addAttribute("clientesEncontrados", listaCliente);
		model.addAttribute("cliente", cliente);
		model.addAttribute("numeroCep", pcPopupVo.getNumeroCep());
		model.addAttribute(FUNCIONALIDADE, pcPopupVo.getFuncionalidade());

		if (pcPopupVo.isMultSelect()) {
			return exibirPesquisaClientePopupMultSelect(model, pcPopupVo);
		}
		return exibirPesquisaClientePopup(model, pcPopupVo);
	}

	/**
	 * Exibir pesquisa cliente popup mult select.
	 * 
	 * @param model {@link Model}
	 * @param pcPopupVo {@link PesquisarClientePopupVO}
	 * @return exibirPesquisaClientePopupMultSelect {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaClientePopupMultSelect")
	public String exibirPesquisaClientePopupMultSelect(Model model, PesquisarClientePopupVO pcPopupVo)
			throws GGASException {

		if (pcPopupVo.verificaChaves()) {
			model.addAttribute(CHAVES_PRIMARIAS, pcPopupVo.getChavesPrimarias());
		}

		validarPessoaFisicaEJuridica(model, pcPopupVo);

		model.addAttribute(ATIVIDADES_ECONOMICAS, controladorCliente.listarAtividadeEconomica());
		model.addAttribute(FUNCIONALIDADE, pcPopupVo.getFuncionalidade());

		return "exibirPesquisaClientePopupMultSelect";

	}

	/**
	 * Método responsável por validar os atributos de pessoaFisica e pessoaJuridica
	 * e dependendo da situação exibir os dados na tela dos popUps.
	 * 
	 * @param model
	 * @param pcPopupVo
	 */
	public void validarPessoaFisicaEJuridica(Model model, PesquisarClientePopupVO pcPopupVo) {

		if (pcPopupVo.isPessoaFisica() && !pcPopupVo.isPessoaJuridica()) {
			pcPopupVo.setPessoaFisica(true);
		} else if (pcPopupVo.isPessoaJuridica() && !pcPopupVo.isPessoaFisica()) {
			pcPopupVo.setPessoaJuridica(true);
		} else {
			pcPopupVo.setPessoaFisica(true);
			pcPopupVo.setPessoaJuridica(true);
		}

		model.addAttribute(PESSOA_FISICA, pcPopupVo.isPessoaFisica());
		model.addAttribute(PESSOA_JURIDICA, pcPopupVo.isPessoaJuridica());

	}

	/**
	 * Método responsável por exibir uma tela de inclusão de cliente.
	 * 
	 * @param model {@link Model}
	 * @param contatoCliente {@link ContatoClienteImpl}
	 * @param idTipoContato {@link Long}
	 * @param chavePrimaria {@link Long}
	 * @param alteracaoLista {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @return popupContatosPessoa {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("popupContatosPessoa")
	public String exibirPopupContatosPessoa(Model model, ContatoClienteImpl contatoCliente,
			@RequestParam(value = ID_TIPO_CONTATO, required = false) Long idTipoContato,
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = ALTERACAO_LISTA, required = false) Boolean alteracaoLista, HttpServletRequest request)
			throws GGASException {

		if (alteracaoLista != null && !alteracaoLista) {
			model.addAttribute(LISTA_CONTATO_MODIFICADA, Boolean.FALSE);
		}

		Cliente cliente = controladorCliente.obterCliente(chavePrimaria, Cliente.PROPRIEDADES_LAZY);
		this.exibirDadosAbaContato(request, cliente);
		carregarListaTipoContato(request, cliente.getTipoCliente().getChavePrimaria());

		if (idTipoContato != null && idTipoContato > -1 && !"".equals(contatoCliente.getNome())) {
			if (!"".equals(contatoCliente.getEmail())
					&& !Util.validarDominio(contatoCliente.getEmail(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
				model.addAttribute("contatoCliente", contatoCliente);
			}
		} else {
			model.addAttribute("contatoCliente", contatoCliente);
		}

		model.addAttribute(DENEGADO, String.valueOf(cliente.isIndicadorDenegado()));

		return "popupContatosPessoa";
	}

	/**
	 * Filtrar lista cliente popup mult select.
	 * 
	 * @param listaCliente
	 *            the lista cliente
	 * 
	 * @param filtro
	 *            the filtro
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void filtrarListaClientePopupMultSelect(Collection<Cliente> listaCliente, Map<String, Object> filtro)
			throws GGASException {

		if (filtro != null && listaCliente != null && !listaCliente.isEmpty()) {

			Iterator<Cliente> iterator = listaCliente.iterator();
			while (iterator.hasNext()) {
				Cliente cliente = iterator.next();

				String nome = (String) filtro.get("nome");
				if (nome != null && !StringUtils.isEmpty(nome)
						&& !cliente.getNome().toUpperCase().contains(nome.toUpperCase())) {
					iterator.remove();

				}

				String nomeFantasia = (String) filtro.get("nomeFantasia");
				if (nomeFantasia != null && !StringUtils.isEmpty(nomeFantasia) && cliente.getNomeFantasia() != null
						&& !cliente.getNomeFantasia().contains(nomeFantasia)) {

					iterator.remove();
				}

				String qualquerNome = (String) filtro.get("qualquerNome");
				if (qualquerNome != null && !StringUtils.isEmpty(qualquerNome) && cliente.getNome() != null
						&& !cliente.getNome().toUpperCase().contains(qualquerNome.toUpperCase())) {

					iterator.remove();

				}

				String cnpj = (String) filtro.get(CNPJ);
				if (cnpj != null && !StringUtils.isEmpty(cnpj)) {
					String cnpjNumerico = Util.removerCaracteresEspeciais(cnpj);
					if (cliente.getCnpj() != null && !cliente.getCnpj().equals(cnpjNumerico)) {
						iterator.remove();
					}
				}

				String cpf = (String) filtro.get(CPF);
				if (cpf != null && !StringUtils.isEmpty(cpf)) {
					String cpfNumerico = Util.removerCaracteresEspeciais(cpf);
					if (cliente.getCpf() != null && !cliente.getCpf().equals(cpfNumerico)) {
						iterator.remove();
					}
				}

				String passaporte = (String) filtro.get("passaporte");
				if (passaporte != null && !StringUtils.isEmpty(passaporte) && (cliente.getNumeroPassaporte() != null
						&& !cliente.getNumeroPassaporte().equals(Util.removerCaracteresEspeciais(passaporte)))) {

					iterator.remove();
				}

				String email = (String) filtro.get("email");
				if (email != null && !StringUtils.isEmpty(email)) {
					if (cliente.getEmailPrincipal() != null && !cliente.getEmailPrincipal().equalsIgnoreCase(email)) {
						iterator.remove();
					} else if (cliente.getEmailSecundario() != null && !cliente.getEmailSecundario().isEmpty()
							&& !cliente.getEmailSecundario().equalsIgnoreCase(email)) {
						iterator.remove();
					}
				}

				String inscricaoEstadual = (String) filtro.get(INSCRICAO_ESTADUAL);
				if (inscricaoEstadual != null && !StringUtils.isEmpty(inscricaoEstadual)
						&& (cliente.getInscricaoEstadual() != null && !cliente.getInscricaoEstadual()
								.equalsIgnoreCase(Util.removerCaracteresEspeciais(inscricaoEstadual)))) {

					iterator.remove();
				}

				String inscricaoMunicipal = (String) filtro.get(INSCRICAO_MUNICIPAL);
				if (inscricaoMunicipal != null && !StringUtils.isEmpty(inscricaoMunicipal)
						&& (cliente.getInscricaoMunicipal() != null && !cliente.getInscricaoMunicipal()
								.equalsIgnoreCase(Util.removerCaracteresEspeciais(inscricaoMunicipal)))) {

					iterator.remove();
				}

				String inscricaoRural = (String) filtro.get(INSCRICAO_RURAL);
				if (inscricaoRural != null && !StringUtils.isEmpty(inscricaoRural)
						&& (cliente.getInscricaoRural() != null && !cliente.getInscricaoRural()
								.equalsIgnoreCase(Util.removerCaracteresEspeciais(inscricaoRural)))) {

					iterator.remove();
				}

				String cep = (String) filtro.get(CEP);
				if (cep != null && !StringUtils.isEmpty(cep) && (cliente.getEnderecoPrincipal() != null
						&& !cliente.getEnderecoPrincipal().getCep().getCep().equalsIgnoreCase(cep))) {

					iterator.remove();
				}
			}
		}
	}

}
