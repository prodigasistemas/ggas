package br.com.ggas.web.cadastro.cliente;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.impl.TipoRelacionamentoClienteImovelImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas telas relacionadas ao Tipos de Relacionamentos de
 * Clientes
 *
 */
@Controller
public class TipoRelacionamentoAction extends GenericAction {

	private static final String TIPO_RELACIONAMENTO = "tipoRelacionamento";
	private static final String TELA_EXIBIR_PESQUISA_TIPO_RELACIONAMENTO = "exibirPesquisaTipoRelacionamento";
	private static final String HABILITADO = "habilitado";
	private static final String INDICADOR_PRINCIPAL = "indicadorPrincipal";
	private static final String DESCRICAO = "descricao";

	@Autowired
	@Qualifier("controladorTabelaAuxiliar")
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela tela de exibição de pesquisa de tipo de
	 * relacionamento.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_TIPO_RELACIONAMENTO)
	public String exibirPesquisaTipoRelacionamento(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		if (!model.containsAttribute(INDICADOR_PRINCIPAL)) {
			model.addAttribute(INDICADOR_PRINCIPAL, null);
		}

		return TELA_EXIBIR_PESQUISA_TIPO_RELACIONAMENTO;
	}

	/**
	 * Método responsável pela pesquisa de um tipo de relacionamento
	 * 
	 * @param descricao
	 *            - {@link String}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param indicadorPrincipal
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarTipoRelacionamento")
	public String pesquisarTipoRelacionamento(@RequestParam("descricao") String descricao,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado,
			@RequestParam(value = "indicadorPrincipal", required = false) Boolean indicadorPrincipal, Model model)
			throws GGASException {

		// mosta o filtro para fazer a pesquisa
		Map<String, Object> filtro = montarFiltro(descricao, indicadorPrincipal, habilitado);
		model.addAttribute(DESCRICAO, descricao);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(INDICADOR_PRINCIPAL, indicadorPrincipal);
		model.addAttribute("listaRelacionamentos", controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro,
				TipoRelacionamentoClienteImovelImpl.class.getName()));

		return TELA_EXIBIR_PESQUISA_TIPO_RELACIONAMENTO;
	}

	/**
	 * Método responsável por exibir a tela de inclusão de um tipo de relacionamento
	 * 
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirIncluirTipoRelacionamento")
	public String exibirIncluirTipoRelacionamento() {

		return "exibirIncluirTipoRelacionamento";
	}

	/**
	 * Método responsável pela inclusão de um tipo de relacionamento
	 * 
	 * @param tipoRelacionamento
	 *            - {@link TipoRelacionamentoClienteImovelImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param principal
	 *            - {@link Boolean}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirTipoRelacionamento")
	public String incluirTipoRelacionamento(TipoRelacionamentoClienteImovelImpl tipoRelacionamento,
			BindingResult result, @RequestParam(value = "principal", required = false) Boolean principal,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "exibirIncluirTipoRelacionamento";

		if (principal == null) {
			tipoRelacionamento.setPrincipal(true);
		}
		model.addAttribute(TIPO_RELACIONAMENTO, tipoRelacionamento);
		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoRelacionamento);
			pesquisarTipoRelacionamentoAnterior(tipoRelacionamento);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoRelacionamento.getDescricao(),
					TipoRelacionamentoClienteImovelImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(tipoRelacionamento.getDescricao(),
					TipoRelacionamentoClienteImovelImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoRelacionamento,
					TipoRelacionamentoClienteImovelImpl.TIPO_RELACIONAMENTO);
			controladorTabelaAuxiliar.inserir(tipoRelacionamento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					TipoRelacionamentoClienteImovelImpl.TIPO_RELACIONAMENTO);
			view = pesquisarTipoRelacionamento(tipoRelacionamento.getDescricao(), tipoRelacionamento.isHabilitado(),
					tipoRelacionamento.isPrincipal(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método que verifica se existe uma mesma entidade com mesma descrição e se já
	 * existe uma entidade principal.
	 * 
	 * @param tipoRelacionamento
	 *            - {@link TipoRelacionamentoClienteImovelImpl}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void pesquisarTipoRelacionamentoAnterior(TipoRelacionamentoClienteImovelImpl tipoRelacionamento)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<TabelaAuxiliar> listaTabelaAuxiliar = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro,
				Constantes.TIPO_RELACIONAMETO_CLIENTE_IMOVEL);
		controladorTabelaAuxiliar.validarExistePrincipal(listaTabelaAuxiliar, tipoRelacionamento);
	}

	/**
	 * Método responsável pela tela de alteração de um tipo de relacionamento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirAlterarTipoRelacionamento")
	public String exibirAlterarTipoRelacionamento(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) {
		String view = "exibirAlterarTipoRelacionamento";

		try {
			TipoRelacionamentoClienteImovelImpl tipoRelacionamento = (TipoRelacionamentoClienteImovelImpl) controladorTabelaAuxiliar
					.obter(chavePrimaria, TipoRelacionamentoClienteImovelImpl.class);
			model.addAttribute(TIPO_RELACIONAMENTO, tipoRelacionamento);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:exibirPesquisaTipoRelacionamento";
		}

		return view;
	}

	/**
	 * Método responsável por alterar um tipo de relacionamento
	 * 
	 * @param tipoRelacionamento
	 *            - {@link TipoRelacionamentoClienteImovelImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarTipoRelacionamento")
	public String alterarTipoRelacionamento(TipoRelacionamentoClienteImovelImpl tipoRelacionamento,
			BindingResult result, HttpServletRequest request, Model model) throws GGASException {
		String view = "exibirAlterarTipoRelacionamento";

		model.addAttribute(TIPO_RELACIONAMENTO, tipoRelacionamento);
		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tipoRelacionamento);
			pesquisarTipoRelacionamentoAnterior(tipoRelacionamento);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(tipoRelacionamento.getDescricao(),
					TipoRelacionamentoClienteImovelImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(tipoRelacionamento.getDescricao(),
					TipoRelacionamentoClienteImovelImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(tipoRelacionamento,
					TipoRelacionamentoClienteImovelImpl.TIPO_RELACIONAMENTO);
			controladorTabelaAuxiliar.atualizar(tipoRelacionamento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					TipoRelacionamentoClienteImovelImpl.TIPO_RELACIONAMENTO);
			view = pesquisarTipoRelacionamento(tipoRelacionamento.getDescricao(), tipoRelacionamento.isHabilitado(),
					tipoRelacionamento.isPrincipal(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por detalhar um tipo de relacionamento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalharTipoRelacionamento")
	public String exibirDetalharTipoRelacionamento(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) {
		String view = "exibirDetalharTipoRelacionamento";

		try {
			TipoRelacionamentoClienteImovelImpl tipoRelacionamento = (TipoRelacionamentoClienteImovelImpl) controladorTabelaAuxiliar
					.obter(chavePrimaria, TipoRelacionamentoClienteImovelImpl.class);
			model.addAttribute(TIPO_RELACIONAMENTO, tipoRelacionamento);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:exibirPesquisaTipoRelacionamento";
		}

		return view;
	}

	/**
	 * Método responsável por remover um tipo de relacionamento
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("removerTipoRelacionamento")
	public String removerTipoRelacionamento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) {

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TipoRelacionamentoClienteImovelImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request,
					TipoRelacionamentoClienteImovelImpl.TIPO_RELACIONAMENTO);
			return pesquisarTipoRelacionamento(null, true, null, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return TELA_EXIBIR_PESQUISA_TIPO_RELACIONAMENTO;
	}

	/**
	 * Método responsável por montar o filtro de pesquisa para o tipo de
	 * relacionamento
	 * 
	 * @param descricao
	 *            - {@link String}
	 * @param indicadorPrincipal
	 *            - {@link Boolean}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> montarFiltro(String descricao, Boolean indicadorPrincipal, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(descricao)) {
			filtro.put(DESCRICAO, descricao);
		}

		if (indicadorPrincipal != null) {
			filtro.put(INDICADOR_PRINCIPAL, indicadorPrincipal);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
