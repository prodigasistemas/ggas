package br.com.ggas.web.cadastro.imovel;

import static br.com.ggas.util.Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_IMOVEL;
import static br.com.ggas.util.Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.BindingResultUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.equipamento.ControladorEquipamento;
import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorAreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelRamoAtividade;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.MotivoFimRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.PavimentoCalcada;
import br.com.ggas.cadastro.imovel.PavimentoRua;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoEquipamento;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.impl.ImovelContatoVO;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelLoteVO;
import br.com.ggas.cadastro.imovel.impl.ImovelPesquisaVO;
import br.com.ggas.cadastro.imovel.impl.ImovelPontoConsumoVO;
import br.com.ggas.cadastro.imovel.impl.ImovelRelacionamentoVO;
import br.com.ggas.cadastro.imovel.impl.ImovelServicoTipoRestricaoImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelUnidadeConsumidoraVO;
import br.com.ggas.cadastro.imovel.impl.ImovelVO;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ContatoImovelComparator;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.Util;
import br.com.ggas.web.cadastro.imovel.decorator.ImovelResultadoPesquisaDecorator;

/**
 * Classe responsável pelas operações referentes a entidade Imóvel.
 *
 *
 */
@Controller
public class ImovelAction extends GenericAction {

	private static final String VIEW_EXIBIR_PESQUISA_IMOVEL = "exibirPesquisaImovel";
	private static final String IMOVEL_PESQUISA_VO = "imovelPesquisaVO";
	private static final String FORWARD_EXIBIR_INCLUSAO_EM_LOTE = "forward:/exibirInclusaoEmLote";
	private static final String VIEW_EXIBIR_INCLUSAO_EM_LOTE = "exibirInclusaoEmLote";
	private static final String IMOVEL_LOTE_VO = "imovelLoteVO";
	private static final String QUADRA = "quadra";
	private static final String VIEW_EXIBIR_ALTERACAO_IMOVEL = "exibirAlteracaoImovel";
	private static final String FORWARD_EXIBIR_POPUP_CONTATOS_IMOVEL = "forward:/exibirPopupContatosImovel?alteracaoLista=true";
	private static final String FORWARD_EXIBIR_ALTERACAO_IMOVEL = "forward:/exibirAlteracaoImovel";
	private static final String FORWARD_EXIBIR_INCLUSAO_IMOVEL = "forward:/exibirInclusaoImovel";
	private static final String FORWARD_PESQUISAR_IMOVEL = "forward:/pesquisarImovel";
	private static final String VIEW_EXIBIR_INCLUSAO_IMOVEL = "exibirInclusaoImovel";
	private static final String IMOVEL_PONTO_CONSUMO_VO = "imovelPontoConsumoVO";
	private static final String IMOVEL_UNIDADE_CONSUMIDORA_VO = "imovelUnidadeConsumidoraVO";
	private static final String IMOVEL_RELACIONAMENTO_VO = "imovelRelacionamentoVO";
	private static final String IMOVEL_CONTATO_VO = "imovelContatoVO";
	private static final String IMOVEL_VO = "imovelVO";
	private static final String DATA_MAXIMA = "dataMaxima";
	private static final String DATA_MINIMA = "dataMinima";
	private static final String LISTA_AGENTE = "listaAgentes";
	private static final String SITUACOES_IMOVEL = "listaSituacoesImovel";
	private static final String ESTADO_ALTERACAO_PONTO_CONSUMO = "estadoAlteracaoPontoConsumo";
	private static final String ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO = "estadoAlteracaoPontoConsumoTributo";
	private static final String SIZE_LISTA_CITY_GATE = "sizeListaCityGate";
	private static final String LISTA_CITY_GATE = "listaCityGate";
	private static final String LISTA_PAVIMENTO_CALCADA = "listaPavimentoCalcada";
	private static final String LISTA_PAVIMENTO_RUA = "listaPavimentoRua";
	private static final String LISTA_PADRAO_CONSTRUCAO = "listaPadraoConstrucao";
	private static final String LISTA_AREA_CONSTRUIDA_FAIXA = "listaAreaConstruidaFaixa";
	private static final String LISTA_PERFIL_IMOVEL = "listaPerfilImovel";
	private static final String LISTA_TIPO_BOTIJAO = "listaTipoBotijao";
	private static final String LISTA_TIPO_COMBUSTIVEL = "listaTipoCombustivel";
	private static final String LISTA_EMPRESA = "listaEmpresa";
	private static final String LISTA_MODALIDADE_MEDICAO_IMOVEL = "listaModalidadeMedicaoImovel";
	private static final String LISTA_REDE_DIAMETRO = "listaRedeDiametro";
	private static final String LISTA_REDE_MATERIAL = "listaRedeMaterial";
	private static final String LISTA_SERVICO_TIPO = "listaServicoTipo";
	private static final String QUADRA_FACE_ENDERECO_CEP = "quadraFace.endereco.cep";
	private static final String ID_IMOVEL_CONDOMINIO = "idImovelCondominio";
	private static final String REDE_INTERNA_IMOVEL = "redeInternaImovel";
	private static final String QUADRA_FACE_ENDERECO = "quadraFace.endereco";
	private static final String QUADRA_FACE = "quadraFace";
	private static final String LISTA_MOTIVO_FIM_RELACIONAMENTO = "listaMotivoFimRelacionamentoClienteImovel";
	private static final String LISTA_TIPOS_RELACIONAMENTO = "listaTiposRelacionamento";
	private static final String LISTA_CLIENTE_IMOVEL = "listaClienteImovel";
	private static final String DOCUMENTO_FORMATADO = "documentoFormatado";
	protected static final String ATRIBUTO_ABA_ID = "abaId";
	private static final String ABA_RELACIONAMENTO = "2";
	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";
	private static final String PARAM_ALTERACAO = "paramAlteracao";
	private static final String LISTA_CONTATO = "listaContato";
	private static final String ABA_CONTATO = "3";
	private static final String LISTA_CONTATO_MODIFICADA = "listaContatoModificada";
	private static final String ABA_UNIDADES_CONSUMIDORAS = "4";
	private static final String LISTA_UNIDADE_CONSUMIDORA = "listaUnidadeConsumidora";
	private static final String LISTA_SEGMENTO = "listaSegmento";
	private static final String LISTA_RAMO_ATIVIDADE = "listaRamoAtividade";
	private static final String ID_SEGMENTO = "idSegmento";
	private static final String ID_RAMO_ATIVIDADE = "idRamoAtividade";
	private static final String QUANTIDADE_ECONOMIA = "quantidadeEconomia";
	private static final String ABA_PONTO_CONSUMO = "5";
	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";
	private static final String LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA = "listaPontoConsumoTributoAliquota";
	private static final String LISTA_PONTO_CONSUMO_EQUIPAMENTO = "listaPontoConsumoEquipamento";
	private static final String PONTOS_CONSUMO = "pontosConsumo";
	private static final int LIMITE_CAMPO_DESCRICAO = 100;
	private static final String LISTA_RAMO_ATIVIDADE_PONTO_CONSUMO = "listaRamoAtividadePontoConsumo";
	private static final String SUCESSO_MANUTENCAO_LISTA_TRIBUTO = "sucessoManutencaoListaTributo";
	private static final String ESTADO_ALTERACAO_PONTO_CONSUMO_EQUIPAMENTO = "estadoAlteracaoPontoConsumoEquipamento";
	private static final String SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO = "sucessoManutencaoListaEquipamento";
	private static final String LISTA_MODALIDADE_USO = "listaModalidadeUso";
	private static final String LISTA_POTENCIAS_FIXAS = "listaPotenciasFixas";
	private static final String POTENCIA_ALTA = "POTENCIA_ALTA";
	private static final String POTENCIA_MEDIA = "POTENCIA_MEDIA";
	private static final String POTENCIA_BAIXA = "POTENCIA_BAIXA";
	private static final String LISTA_TRIBUTO = "listaTributo";
	private static final String ATRIBUTO_REQUEST_IMOVEL = "imovel";
	private static final String LISTA_IMOVEL_FILHO_POTENCIAL = "listaImovelFilhoPotencial";
	private static final String LISTA_IMOVEL_FILHO_FACTIVEL = "listaImovelFilhoFactivel";
	private static final Logger LOG = Logger.getLogger(ImovelAction.class);
	private static final String ATRIBUTO_REQUEST_POSSUI_REDE_INTERNA = "possuiRedeInterna";
	private static final String LISTA_IMOVEIS = "listaImoveis";
	private static final String LISTA_IMOVEIS_FILHOS_ALTERADOS = "listaImoveisFilhosAlterados";
	private static final String HABILITADO = "habilitado";
	private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";
	private static final String TIPO_MEDICAO_AMBOS = "tipoMedicaoAmbos";
	private static final String LISTA_IMOVEL = "listaImovel";
	private static final String ID_CLIENTE = "idCliente";
	private static final String MATRICULA_CONDOMINIO = "matriculaCondominio";
	private static final String NOME_MEDICAO = "nomeMedicao";
	private static final String NOME = "nome";
	private static final String NOME_IMOVEL_CONDOMINIO = "nomeImovelCondominio";
	private static final String NUMERO_IMOVEL = "numeroImovel";
	private static final String CEP_IMOVEL = "cepImovel";
	private static final String ID_CEP_IMOVEL = "idCepImovel";
	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";
	private static final String ID_SEGMENTO_PONTO_CONSUMO = "idSegmentoPontoConsumo";
	private static final String ID_RAMO_ATIVIDADE_PONTO_CONSUMO = "idRamoAtividadePontoConsumo";
	private static final String DESCRICAO_PONTO_CONSUMO = "descricaoPontoConsumo";
	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";
	private static final String TIPO_MEDICAO = "tipoMedicao";
	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";
	private static final String ID_ROTA = "idRota";
	private static final String PONTO_CONSUMO_LEGADO = "pontoConsumoLegado";
	private static final String DATA_PREVISAO_ENCERRAMENTO_OBRA = "dataPrevisaoEncerramentoObra";
	private static final String ATRIBUTO_REQUEST_LISTA_BLOCOS = "listaBlocos";
	private static final String IDS_SERVICO_TIPO_RESTRICAO_SALVOS = "idsServicoTipoRestricaoSalvos";
	private static final String LISTA_IMOVEL_SERVICO_TIPO_RESTRICAO_SERVICO_TIPO = "listaImovelServicoTipoRestricao.servicoTipo";
	private static final String MATRICULA = "matricula";
	public static final String DATA_INICIO_ASSINATURA_CONTRATO = "dataInicioAssinaturaContrato";
	public static final String DATA_FIM_ASSINATURA_CONTRATO = "dataFimAssinaturaContrato";
	public static final String DATA_INICIO_ATIVACAO_MEDIDOR = "dataInicioAtivacaoMedidor";
	public static final String DATA_FIM_ATIVACAO_MEDIDOR = "dataFimAtivacaoMedidor";
	public static final String SITUACAO_CONTRATO = "situacaoContrato";
	public static final String SITUACAO_MEDIDOR = "situacaoMedidor";
	private static final String QTD_IMOVEIS = "qtdImoveis";
	public static final String BLOCO = "bloco";

	@Autowired
	private ControladorTributo controladorTributo;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorQuadra controladorQuadra;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorAgente controladorAgente;

	@Autowired
	private ControladorEndereco controladorEndereco;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorAreaConstruidaFaixa controladorAreaConstruidaFaixa;

	@Autowired
	private ControladorRede controladorRede;

	@Autowired
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorEquipamento controladorEquipamento;
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	/**
	 * Método responsável por exibir a tela de incluir imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping(value = VIEW_EXIBIR_INCLUSAO_IMOVEL)
	public String exibirInclusaoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) {

		try {
			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_CONTATO_VO, imovelContatoVO);
			model.addAttribute(IMOVEL_RELACIONAMENTO_VO, imovelRelacionamentoVO);
			model.addAttribute(IMOVEL_UNIDADE_CONSUMIDORA_VO, imovelUnidadeConsumidoraVO);
			model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);
			this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

			super.saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return VIEW_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Método responsável por incluir um imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirImovel")
	public String incluirImovel(Model model, HttpServletRequest request, ImovelVO imovelVO, BindingResult bindingResult) {

		String view = null;

		try {
			validarToken(request);

			Imovel imovel = (Imovel) controladorImovel.criar();

			popularImovel(request, imovel, imovelVO);
			imovel.setDadosAuditoria(getDadosAuditoria(request));

			Long idImovel = controladorImovel.inserirImovel(imovel, imovelVO.getVeioDoChamado());
			ImovelPesquisaVO imovelPesquisa = new ImovelPesquisaVO();
			popularPesquisaVO(imovel, imovelPesquisa, model);

			imovelVO.setChavePrimaria(idImovel);
			imovelVO.setDescricaoPontoConsumo("");
			model.addAttribute(IMOVEL_VO, imovelVO);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(Imovel.IMOVEL_ROTULO));
			view = this.pesquisarImovel(imovelPesquisa, bindingResult, null, request, model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}
		
		return view;
	}

	/**
	 * Método responsável por preencher um VO com os atributos relacionados a entidade Imóvel
	 *
	 * @param imovel - {@link Imovel}
	 * @param model - {@link Model}
	 */
	private void popularPesquisaVO(Imovel imovel, ImovelPesquisaVO imovelPesquisa, Model model) {

		if (!StringUtils.isEmpty(imovel.getCep())) {
			imovelPesquisa.setCepImovel(imovel.getCep());
		}

		if (!StringUtils.isEmpty(imovel.getNumeroImovel())) {
			imovelPesquisa.setNumeroImovel(imovel.getNumeroImovel());
		}

		if (!StringUtils.isEmpty(imovel.getDescricaoComplemento())) {
			imovelPesquisa.setComplementoImovel(imovel.getDescricaoComplemento());
		}

		imovelPesquisa.setNome(imovel.getNome());
		imovelPesquisa.setMatriculaImovel(String.valueOf(imovel.getChavePrimaria()));
		imovelPesquisa.setIndicadorCondominioAmbos(String.valueOf(imovel.getCondominio()));

		if (imovel.getModalidadeMedicaoImovel() != null) {
			imovelPesquisa.setTipoMedicaoAmbos(String.valueOf(imovel.getModalidadeMedicaoImovel().getCodigo()));
		}

		imovelPesquisa.setHabilitado(imovel.isHabilitado());
		model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, imovelPesquisa);
	}

	/**
	 * Método responsável por remover um cliente do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("removerClienteImovelFluxoAlteracao")
	public String removerClienteImovelFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) {

		this.removerClienteImovel(model, request, imovelVO, imovelRelacionamentoVO, bindingResult);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por remover um cliente do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerClienteImovelFluxoInclusao")
	public String removerClienteImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) {

		try {

			Integer indexLista = imovelVO.getIndexLista();

			validarToken(request);

			Collection<ClienteImovel> listaClienteImovel =
					(Collection<ClienteImovel>) request.getSession().getAttribute(LISTA_CLIENTE_IMOVEL);

			if (listaClienteImovel != null && indexLista != null) {

				((List) listaClienteImovel).remove(indexLista.intValue());
			}

			request.getSession().setAttribute(LISTA_CLIENTE_IMOVEL, listaClienteImovel);
			request.setAttribute(ATRIBUTO_ABA_ID, ABA_RELACIONAMENTO);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;

	}

	/**
	 * Método responsável por adicionar o(s) contato(s) do imóvel no PopUp.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarContatoDoImovelFluxoPopup")
	public String adicionarContatoDoImovelFluxoPopup(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelContatoVO imovelContatoVO, BindingResult bindingResult) {

		this.adicionarContatoDoImovel(model, request, imovelVO, imovelContatoVO, bindingResult);

		return FORWARD_EXIBIR_POPUP_CONTATOS_IMOVEL;
	}

	/**
	 * Método responsável por adicionar o(s) contato(s) do imóvel no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarContatoDoImovelFluxoAlteracao")
	public String adicionarContatoDoImovelFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelContatoVO imovelContatoVO, BindingResult bindingResult) {

		this.adicionarContatoDoImovel(model, request, imovelVO, imovelContatoVO, bindingResult);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por adicionar o(s) contato(s) do imóvel no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("adicionarContatoDoImovelFluxoInclusao")
	@SuppressWarnings({ "unchecked" })
	public String adicionarContatoDoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelContatoVO imovelContatoVO,
			BindingResult bindingResult) {

		try {
			request.setAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);

			validarToken(request);

			Integer indexLista = imovelVO.getIndexLista();

			List<ContatoImovel> listaContato = (List<ContatoImovel>) request.getSession().getAttribute(LISTA_CONTATO);

			if (indexLista == null || indexLista < 0) {
				ContatoImovel contato = (ContatoImovel) controladorImovel.criarContatoImovel();
				this.popularContato(contato, imovelContatoVO);
				if ((listaContato == null) || (listaContato.isEmpty())) {
					listaContato = new ArrayList<ContatoImovel>();
					contato.setPrincipal(Boolean.TRUE);
				} else {
					contato.setPrincipal(Boolean.FALSE);
				}
				controladorImovel.verificarEmailContatoExistente(indexLista, listaContato, contato);
				listaContato.add(contato);
			} else {
				this.alterarContatoDoImovel(indexLista, imovelContatoVO, listaContato);
				request.setAttribute(PARAM_ALTERACAO, Boolean.TRUE);
			}
			Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());
			request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, Boolean.TRUE);
			request.getSession().setAttribute(LISTA_CONTATO, listaContato);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Método responsável por remover um contato do imóvel no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerContatoDoImovelFluxoAlteracao")
	public String removerContatoDoImovelFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		this.removerContatoDoImovel(model, request, imovelVO);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por remover um contato do imóvel no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerContatoDoImovelFluxoInclusao")
	@SuppressWarnings({ "unchecked" })
	public String removerContatoDoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		try {
			Integer indexContato = imovelVO.getIndexLista();

			validarToken(request);

			Collection<ContatoImovel> listaContato = (Collection<ContatoImovel>) request.getSession().getAttribute(LISTA_CONTATO);

			this.removerContatoImovelDeListaDeContatos(indexContato, listaContato);

			request.getSession().setAttribute(LISTA_CONTATO, listaContato);
			request.setAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;
	}

	@SuppressWarnings({ "rawtypes" })
	private void removerContatoImovelDeListaDeContatos(Integer indexContato, Collection<ContatoImovel> listaContato) {

		if (listaContato != null && indexContato != null) {

			ContatoImovel contatoImovel = ((List<ContatoImovel>) listaContato).get(indexContato.intValue());
			if (contatoImovel.isPrincipal()) {
				((List) listaContato).remove(indexContato.intValue());
				if (!listaContato.isEmpty()) {
					((List<ContatoImovel>) listaContato).get(0).setPrincipal(Boolean.TRUE);
				}
			} else {
				((List) listaContato).remove(indexContato.intValue());
			}
			Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());
		}
	}

	/**
	 * Método responsável por atualizar o contato principal do imóvel no PopUp.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("atualizarContatoDoImovelFluxoPopup")
	public String atualizarContatoDoImovelFluxoPopup(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		this.atualizarContatoPrincipalDoImovel(model, request, imovelVO);

		return FORWARD_EXIBIR_POPUP_CONTATOS_IMOVEL;
	}

	/**
	 * Método responsável por atualizar o contato principal do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("atualizarContatoPrincipalDoImovelFluxoAlteracao")
	public String atualizarContatoPrincipalDoImovelFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		this.atualizarContatoPrincipalDoImovel(model, request, imovelVO);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por atualizar o contato principal do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("atualizarContatoPrincipalDoImovelFluxoInclusao")
	@SuppressWarnings("unchecked")
	public String atualizarContatoPrincipalDoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		Integer indexLista = imovelVO.getIndexLista();

		Collection<ContatoImovel> listaContato = (Collection<ContatoImovel>) request.getSession().getAttribute(LISTA_CONTATO);

		if (listaContato != null && indexLista != null) {
			ContatoImovel contatoImovel = null;
			for (int i = 0; i < listaContato.size(); i++) {
				contatoImovel = ((ArrayList<ContatoImovel>) listaContato).get(i);
				if (i == indexLista) {
					contatoImovel.setPrincipal(Boolean.TRUE);
				} else {
					contatoImovel.setPrincipal(Boolean.FALSE);
				}
			}
			Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());
		}

		request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, Boolean.TRUE);
		model.addAttribute(ATRIBUTO_ABA_ID, ABA_CONTATO);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Método responsável por adicionar a(s) unidade consumidora(s) do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarUnidadeConsumidoraAlteracao")
	public String adicionarUnidadeConsumidoraAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelContatoVO imovelContatoVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult) {

		this.adicionarUnidadeConsumidora(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelContatoVO, imovelRelacionamentoVO,
				imovelPontoConsumoVO, bindingResult);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por adicionar a(s) unidade consumidora(s) do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarUnidadeConsumidora")
	@SuppressWarnings("unchecked")
	public String adicionarUnidadeConsumidora(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelContatoVO imovelContatoVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult) {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			model.addAttribute(ATRIBUTO_ABA_ID, ABA_UNIDADES_CONSUMIDORAS);

			validarToken(request);

			Integer indexLista = imovelVO.getIndexLista();

			List<ImovelRamoAtividade> listaUnidadesConsumidoras =
					(List<ImovelRamoAtividade>) request.getSession().getAttribute(LISTA_UNIDADE_CONSUMIDORA);

			if (indexLista == null || indexLista < 0) {
				ImovelRamoAtividade imovelRamoAtividade = (ImovelRamoAtividade) controladorImovel.criarImovelRamoAtividade();
				this.popularImovelRamoAtividade(imovelRamoAtividade, imovelUnidadeConsumidoraVO);
				validarAdicionarUnidadeConsumidora(imovelRamoAtividade, listaUnidadesConsumidoras, null);
				listaUnidadesConsumidoras.add(imovelRamoAtividade);
			} else {
				this.alterarImovelRamoAtividade(indexLista, imovelUnidadeConsumidoraVO, listaUnidadesConsumidoras);
			}

			request.getSession().setAttribute(LISTA_UNIDADE_CONSUMIDORA, listaUnidadesConsumidoras);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
			request.setAttribute(PARAM_ALTERACAO, Boolean.TRUE);

			this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

			this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO,
					imovelPontoConsumoVO);

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Método responsável por remover uma unidade consumidora do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerUnidadeConsumidoraFluxoAlteracao")
	public String removerUnidadeConsumidoraFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		this.removerUnidadeConsumidora(model, request, imovelVO);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por remover uma unidade consumidora do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerUnidadeConsumidoraFluxoInclusao")
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String removerUnidadeConsumidora(Model model, HttpServletRequest request, ImovelVO imovelVO) {

		try {
			Integer indexLista = imovelVO.getIndexLista();

			validarToken(request);

			Collection<ImovelRamoAtividade> listaUnidadesConsumidoras =
					(Collection<ImovelRamoAtividade>) request.getSession().getAttribute(LISTA_UNIDADE_CONSUMIDORA);

			if (listaUnidadesConsumidoras != null && indexLista != null) {
				((List) listaUnidadesConsumidoras).remove(indexLista.intValue());
			}

			request.getSession().setAttribute(LISTA_UNIDADE_CONSUMIDORA, listaUnidadesConsumidoras);
			request.setAttribute(ATRIBUTO_ABA_ID, ABA_UNIDADES_CONSUMIDORAS);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e); 
		}

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;
	}

	private void validarAdicionarUnidadeConsumidora(ImovelRamoAtividade imovelRamoAtividade,
			List<ImovelRamoAtividade> listaImovelRamoAtividade, Integer indexList) throws NegocioException {

		if (listaImovelRamoAtividade != null && !listaImovelRamoAtividade.isEmpty()) {
			for (int index = 0; index < listaImovelRamoAtividade.size(); index++) {
				ImovelRamoAtividade imovelRamoAtividadeCompare = listaImovelRamoAtividade.get(index);
				if (imovelRamoAtividade.getRamoAtividade().getChavePrimaria() == imovelRamoAtividadeCompare.getRamoAtividade()
						.getChavePrimaria() && (indexList == null || index != indexList)) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_UNIDADE_CONSUMIDORA_EXISTENTE, true);
				}
			}
		}
	}

	private void popularContato(ContatoImovel contato, ImovelContatoVO imovelContatoVO) throws GGASException {

		Long idTipoContato = imovelContatoVO.getIdTipoContato();
		Long idProfissao = imovelContatoVO.getIdProfissao();
		String nomeContato = imovelContatoVO.getNomeContato();
		String dddContato = imovelContatoVO.getDddContato();
		String telefoneContato = imovelContatoVO.getTelefoneContato();
		String ramalContato = imovelContatoVO.getRamalContato();
		String areaContato = imovelContatoVO.getAreaContato();
		String emailContato = imovelContatoVO.getEmailContato();
		String dddContato2 = imovelContatoVO.getDddContato2();
		String telefoneContato2 = imovelContatoVO.getTelefoneContato2();
		String ramalContato2 = imovelContatoVO.getRamalContato2();

		if (idTipoContato != null && idTipoContato > 0) {
			TipoContato tipoContato = controladorCliente.buscarTipoContato(idTipoContato);
			contato.setTipoContato(tipoContato);
		}
		if (idProfissao != null && idProfissao > 0) {
			Profissao profissao = controladorCliente.obterProfissao(idProfissao);
			contato.setProfissao(profissao);
		}
		if (!StringUtils.isEmpty(nomeContato)) {
			contato.setNome(nomeContato);
		}
		if (!StringUtils.isEmpty(dddContato)) {
			contato.setCodigoDDD(Util.converterCampoStringParaValorInteger(ContatoImovel.DDD, dddContato));
		}
		if (!StringUtils.isEmpty(telefoneContato)) {
			contato.setFone(telefoneContato);
		}
		if (!StringUtils.isEmpty(ramalContato)) {
			contato.setRamal(ramalContato);
		}
		if (!StringUtils.isEmpty(areaContato)) {
			contato.setDescricaoArea(areaContato);
		}
		if (!StringUtils.isEmpty(emailContato)) {
			contato.setEmail(emailContato);
		}
		if (!StringUtils.isBlank(ramalContato2)) {
			contato.setRamal2(ramalContato2);
		}
		if (!StringUtils.isBlank(dddContato2)) {
			contato.setCodigoDDD2(Util.converterCampoStringParaValorInteger(ContatoImovel.DDD2, dddContato2));
		}
		if (!StringUtils.isBlank(telefoneContato2)) {
			contato.setFone2(telefoneContato2);
		}

		if (contato.getChavePrimaria() <= 0) {
			contato.setChavePrimaria(0);
		}
		contato.setHabilitado(true);
		contato.setUltimaAlteracao(Calendar.getInstance().getTime());

		controladorImovel.validarDadosContatoImovel(contato);

	}

	private void alterarContatoDoImovel(Integer indexLista, ImovelContatoVO imovelContatoVO, List<ContatoImovel> listaContato)
			throws GGASException {

		if (listaContato != null && indexLista != null) {
			ContatoImovel contatoImovelAlterado = (ContatoImovel) controladorImovel.criarContatoImovel();
			this.popularContato(contatoImovelAlterado, imovelContatoVO);

			ContatoImovel contatoClienteExistente = null;
			for (int i = 0; i < listaContato.size(); i++) {
				contatoClienteExistente = listaContato.get(i);
				if (i == indexLista && contatoClienteExistente != null) {
					this.popularContato(contatoClienteExistente, imovelContatoVO);
				}
			}
		}
	}

	/**
	 * Método responsável por adicionar o(s) cliente(s) do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 *
	 */
	@RequestMapping("adicionarClienteImovelAlteracao")
	public String adicionarClienteImovelAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) throws GGASException {

		this.adicionarClienteImovel(model, request, imovelVO, imovelRelacionamentoVO, bindingResult);

		return FORWARD_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por adicionar o(s) cliente(s) do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 *
	 */
	@RequestMapping("adicionarClienteImovel")
	@SuppressWarnings("unchecked")
	public String adicionarClienteImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) throws GGASException {

		try {
			model.addAttribute(ATRIBUTO_ABA_ID, ABA_RELACIONAMENTO);

			validarToken(request);

			Integer indexLista = imovelVO.getIndexLista();

			List<ClienteImovel> listaClienteImovel = (List<ClienteImovel>) request.getSession().getAttribute(LISTA_CLIENTE_IMOVEL);
			validarDatasClienteImovel(listaClienteImovel, imovelRelacionamentoVO);

			if (indexLista == null || indexLista < 0) {
				ClienteImovel clienteImovel = (ClienteImovel) controladorImovel.criarClienteImovel();
				this.popularClienteImovel(clienteImovel, imovelRelacionamentoVO);

				controladorImovel.validarDadosClienteImovel(clienteImovel);
				controladorImovel.validarClientesRelacionados(clienteImovel, listaClienteImovel, false);

				listaClienteImovel.add(clienteImovel);
			} else {
				this.alterarClienteImovel(indexLista, imovelRelacionamentoVO, listaClienteImovel);
				model.addAttribute(PARAM_ALTERACAO, Boolean.TRUE);
			}
			
			
			//Faz a ordenacao em ordem decrescente da lista pela RelacaoInicio
			Collections.sort(listaClienteImovel, new Comparator<ClienteImovel>() {
				@Override
				public int compare(ClienteImovel clienteImovel1, ClienteImovel clienteImovel2) {
					return clienteImovel2.getRelacaoInicio().compareTo(clienteImovel1.getRelacaoInicio());
				}
			});
			request.getSession().setAttribute(LISTA_CLIENTE_IMOVEL, listaClienteImovel);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_EXIBIR_INCLUSAO_IMOVEL;
	}

	private void validarDatasClienteImovel(List<ClienteImovel> lista, ImovelRelacionamentoVO imovelVO) throws GGASException {
		for (ClienteImovel clienteImovel : lista) {
			if(clienteImovel.getRelacaoFim() != null && clienteImovel.getRelacaoInicio() != null && 
					Util.converterCampoStringParaData("", imovelVO.getRelacaoInicio(), "dd/MM/yyyy").after(clienteImovel.getRelacaoInicio()) &&
					Util.converterCampoStringParaData("", imovelVO.getRelacaoInicio(), "dd/MM/yyyy").before(clienteImovel.getRelacaoFim())){
				throw new GGASException("A data inicial do relacionamento deve estar fora do intervalo de datas de outro relacionamento!");
			}
			if(clienteImovel.getRelacaoFim() != null && imovelVO.getRelacaoFim() != null && 
					Util.converterCampoStringParaData("", imovelVO.getRelacaoFim(), "dd/MM/yyyy").after(clienteImovel.getRelacaoInicio()) &&
					Util.converterCampoStringParaData("", imovelVO.getRelacaoFim(), "dd/MM/yyyy").before(clienteImovel.getRelacaoFim())){
				throw new GGASException("A data final do relacionamento deve estar fora do intervalo de datas de outro relacionamento!");
			}
		}
		
	}

	private void alterarClienteImovel(Integer indexLista, ImovelRelacionamentoVO imovelRelacionamentoVO,
			List<ClienteImovel> listaClienteImovel) throws GGASException {

		if (listaClienteImovel != null && indexLista != null) {
			ClienteImovel clienteImovel = (ClienteImovel) controladorImovel.criarClienteImovel();

			this.popularClienteImovel(clienteImovel, imovelRelacionamentoVO);
			controladorImovel.validarDadosClienteImovel(clienteImovel);

			controladorImovel.validarClientesRelacionados(clienteImovel, listaClienteImovel, true);

			this.popularClienteImovel(listaClienteImovel.get(indexLista), imovelRelacionamentoVO);
		}
	}

	private void popularClienteImovel(ClienteImovel clienteImovel, ImovelRelacionamentoVO imovelRelacionamentoVO) throws GGASException {

		Long idCliente = imovelRelacionamentoVO.getIdCliente();
		Long idTipoRelacionamento = imovelRelacionamentoVO.getIdTipoRelacionamento();
		String relacaoInicio = imovelRelacionamentoVO.getRelacaoInicio();
		Long idMotivoFimRelacionamentoClienteImovel = imovelRelacionamentoVO.getIdMotivoFimRelacionamentoClienteImovel();
		String relacaoFim = imovelRelacionamentoVO.getRelacaoFim();

		Date dataRelacaoInicio = null;
		if (!StringUtils.isEmpty(relacaoInicio)) {
			dataRelacaoInicio = Util.converterCampoStringParaData(ClienteImovel.RELACAO_INICIO, relacaoInicio, Constantes.FORMATO_DATA_BR);
		}

		Date dataRelacaoFim = null;
		if (!StringUtils.isEmpty(relacaoFim)) {
			dataRelacaoFim = Util.converterCampoStringParaData(ClienteImovel.RELACAO_FIM, relacaoFim, Constantes.FORMATO_DATA_BR);
		}

		controladorImovel.validarDatasAbaRelacionamento(dataRelacaoInicio, dataRelacaoFim);

		clienteImovel.setRelacaoInicio(dataRelacaoInicio);
		clienteImovel.setRelacaoFim(dataRelacaoFim);

		if (idCliente != null && idCliente > 0) {
			Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
			clienteImovel.setCliente(cliente);
		}
		if (idTipoRelacionamento != null && idTipoRelacionamento > 0) {
			TipoRelacionamentoClienteImovel tipoRelacionamento =
					controladorImovel.buscarTipoRelacionamentoClienteImovel(idTipoRelacionamento);
			clienteImovel.setTipoRelacionamentoClienteImovel(tipoRelacionamento);
		} else {
			clienteImovel.setTipoRelacionamentoClienteImovel(null);
		}

		if (idMotivoFimRelacionamentoClienteImovel != null && idMotivoFimRelacionamentoClienteImovel > 0) {
			MotivoFimRelacionamentoClienteImovel motivoFimRelacionamentoClienteImovel =
					controladorImovel.obterMotivoFimRelacionamentoClienteImovel(idMotivoFimRelacionamentoClienteImovel);
			clienteImovel.setMotivoFimRelacionamentoClienteImovel(motivoFimRelacionamentoClienteImovel);
		} else {
			clienteImovel.setMotivoFimRelacionamentoClienteImovel(null);
		}

		if (clienteImovel.getChavePrimaria() <= 0) {
			clienteImovel.setChavePrimaria(0);
		}

		if (clienteImovel.getRelacaoFim() == null && clienteImovel.getMotivoFimRelacionamentoClienteImovel() == null) {
			clienteImovel.setHabilitado(true);
		} else {
			clienteImovel.setHabilitado(false);
		}

		clienteImovel.setUltimaAlteracao(Calendar.getInstance().getTime());

	}

	/**
	 * Método responsável por remover um ponto de consumo de um imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumoFluxoAlteracao")
	public String removerPontoConsumoFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelContatoVO imovelContatoVO, ImovelRelacionamentoVO imovelRelacionamentoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult) {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.removerPontoConsumo(model, request, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO,
					imovelPontoConsumoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Método responsável por remover um ponto de consumo de um imóvel no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumo")
	public String removerPontoConsumoFluxoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelContatoVO imovelContatoVO, ImovelRelacionamentoVO imovelRelacionamentoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {

			this.removerPontoConsumo(model, request, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO,
					imovelPontoConsumoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void removerPontoConsumo(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelContatoVO imovelContatoVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		Integer indexLista = imovelVO.getIndexLista();

		validarToken(request);

		Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		if (listaPontoConsumo != null && indexLista != null) {
			PontoConsumo pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());
			controladorImovel.validarRemocaoPontoConsumoImovel(pontoConsumo.getChavePrimaria());
			((List) listaPontoConsumo).remove(indexLista.intValue());
			listaPontoConsumoTributoAliquota.clear();
			listaPontoConsumoEquipamento.clear();
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		carregarCityGatePorQuadraFace(imovelVO, model, request);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Exibir alteração ponto consumo no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirAlteracaoPontoConsumoAlteracao")
	public String exibirAlteracaoPontoConsumoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;
		try {
			this.exibirAlteracaoPontoConsumo(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Exibir alteração ponto consumo no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirAlteracaoPontoConsumo")
	public String exibirAlteracaoPontoConsumoFluxoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			this.exibirAlteracaoPontoConsumo(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void exibirAlteracaoPontoConsumo(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		validarToken(request);

		Integer indexLista = imovelVO.getIndexLista();

		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		Integer indexListaPontoConsumoTributo = imovelPontoConsumoVO.getIndexListaPontoConsumoTributo();
		if (indexListaPontoConsumoTributo != null && indexListaPontoConsumoTributo > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, true);
		}

		Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota = new ArrayList<PontoConsumoTributoAliquota>();
		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento = new ArrayList<PontoConsumoEquipamento>();

		if (listaPontoConsumo != null && indexLista != null) {
			PontoConsumo pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());

			popularImovelPontoConsumoVO(model, request, imovelVO, imovelPontoConsumoVO, listaPontoConsumoTributoAliquota,
					listaPontoConsumoEquipamento, pontoConsumo);
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void popularImovelPontoConsumoVO(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota,
			Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento, PontoConsumo pontoConsumo)
			throws GGASException {

		if (pontoConsumo.getDescricao() != null && !pontoConsumo.getDescricao().isEmpty()) {
			imovelPontoConsumoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		} else {
			imovelPontoConsumoVO.setDescricaoPontoConsumo(null);
		}
		imovelPontoConsumoVO.setObservacaoPontoConsumo(pontoConsumo.getObservacao());

		if (pontoConsumo.getLatitudeGrau() != null) {
			imovelPontoConsumoVO.setLatitudeGrau(NumeroUtil.converterCampoValorDecimalParaString(pontoConsumo.getLatitudeGrau(), 12));
		} else {
			imovelPontoConsumoVO.setLatitudeGrau(null);
		}

		if (pontoConsumo.getLongitudeGrau() != null) {
			imovelPontoConsumoVO.setLongitudeGrau(NumeroUtil.converterCampoValorDecimalParaString(pontoConsumo.getLongitudeGrau(), 12));
		} else {
			imovelPontoConsumoVO.setLongitudeGrau(null);
		}

		if (pontoConsumo.getDescricaoComplemento() != null && !pontoConsumo.getDescricaoComplemento().isEmpty()) {
			imovelPontoConsumoVO.setDescricaoComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
		} else {
			imovelPontoConsumoVO.setDescricaoComplementoPontoConsumo(null);
		}

		if (pontoConsumo.getNumeroSequenciaLeitura() != null && pontoConsumo.getNumeroSequenciaLeitura() != 0) {
			imovelPontoConsumoVO.setNumeroSequenciaLeitura(pontoConsumo.getNumeroSequenciaLeitura().toString());
		} else {
			imovelPontoConsumoVO.setNumeroSequenciaLeitura(null);
		}

		if (pontoConsumo.getSegmento() != null && pontoConsumo.getSegmento().getChavePrimaria() != 0) {
			imovelPontoConsumoVO.setIdSegmentoPontoConsumo(pontoConsumo.getSegmento().getChavePrimaria());
		} else {
			imovelPontoConsumoVO.setIdSegmentoPontoConsumo(null);
		}

		if (pontoConsumo.getRamoAtividade() != null && pontoConsumo.getRamoAtividade().getChavePrimaria() != 0) {
			imovelPontoConsumoVO.setIdRamoAtividadePontoConsumo(pontoConsumo.getRamoAtividade().getChavePrimaria());
		} else {
			imovelPontoConsumoVO.setIdRamoAtividadePontoConsumo(null);
		}

		if (pontoConsumo.getModalidadeUso() != null && pontoConsumo.getModalidadeUso().getChavePrimaria() != 0) {
			imovelPontoConsumoVO.setIdModalidadeUso(pontoConsumo.getModalidadeUso().getChavePrimaria());
		} else {
			imovelPontoConsumoVO.setIdModalidadeUso(null);
		}

		if (pontoConsumo.getCep() != null && !pontoConsumo.getCep().getCep().isEmpty()) {
			imovelPontoConsumoVO.setCepPontosConsumo(pontoConsumo.getCep().getCep());
		} else {
			imovelPontoConsumoVO.setCepPontosConsumo(null);
		}

		if (pontoConsumo.getEnderecoReferencia() != null && !pontoConsumo.getEnderecoReferencia().isEmpty()) {
			imovelPontoConsumoVO.setEnderecoReferenciaPontoConsumo(pontoConsumo.getEnderecoReferencia());
		} else {
			imovelPontoConsumoVO.setEnderecoReferenciaPontoConsumo(null);
		}

		if (pontoConsumo.getCodigoPontoConsumoSupervisorio() != null && !pontoConsumo.getCodigoPontoConsumoSupervisorio().isEmpty()) {
			imovelPontoConsumoVO.setEnderecoRemotoPontoConsumo(pontoConsumo.getCodigoPontoConsumoSupervisorio());
		} else {
			imovelPontoConsumoVO.setEnderecoRemotoPontoConsumo(null);
		}

		if (pontoConsumo.getNumeroImovel() != null && !pontoConsumo.getNumeroImovel().isEmpty()) {
			imovelPontoConsumoVO.setNumeroImovelPontoConsumo(pontoConsumo.getNumeroImovel().toString());
		} else {
			imovelPontoConsumoVO.setNumeroImovelPontoConsumo(null);
		}

		if (pontoConsumo.getDescricaoComplemento() != null && !pontoConsumo.getDescricaoComplemento().isEmpty()) {
			imovelPontoConsumoVO.setDescricaoComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
		} else {
			imovelPontoConsumoVO.setDescricaoComplementoPontoConsumo(null);
		}

		imovelPontoConsumoVO.setIdPontoConsumo(pontoConsumo.getChavePrimaria());

		if (pontoConsumo.getCodigoLegado() != null) {
			imovelPontoConsumoVO.setPontoConsumoLegado(pontoConsumo.getCodigoLegado());
		} else {
			imovelPontoConsumoVO.setPontoConsumoLegado(null);
		}

		imovelPontoConsumoVO.setIndicadorClassificacaoFiscal(String.valueOf(pontoConsumo.getIndicadorClassificacaoFiscal()));
		imovelPontoConsumoVO.setHabilitadoPontoConsumo(String.valueOf(pontoConsumo.isHabilitado()));
		imovelPontoConsumoVO.setIdQuadraFacePontoConsumo(null);
		imovelPontoConsumoVO.setIdQuadraPontoConsumo(null);

		model.addAttribute(SIZE_LISTA_CITY_GATE, 0);
		imovelVO.setPercentualCityGate(null);
		model.addAttribute(LISTA_CITY_GATE, null);
		request.getSession().setAttribute(LISTA_CITY_GATE, null);

		if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getChavePrimaria() != 0) {
			imovelPontoConsumoVO.setIdRota(pontoConsumo.getRota().getChavePrimaria());
		}

		if (pontoConsumo.getQuadraFace() != null && pontoConsumo.getQuadraFace().getChavePrimaria() != 0) {
			imovelPontoConsumoVO.setIdQuadraFacePontoConsumo(pontoConsumo.getQuadraFace().getChavePrimaria());

			Quadra quadra = controladorImovel.obterQuadraPorFaceQuadraPontoConsumo(pontoConsumo.getQuadraFace().getChavePrimaria());
			if (quadra != null && quadra.getChavePrimaria() != 0) {
				imovelPontoConsumoVO.setIdQuadraPontoConsumo(quadra.getChavePrimaria());
			}

			List<PontoConsumoCityGate> listaPontoConsumoCityGate = new ArrayList<PontoConsumoCityGate>();
			Collection<CityGate> listaCityGate = null;
			listaCityGate = controladorQuadra.listarCityGatePorQuadraFace(pontoConsumo.getQuadraFace().getChavePrimaria());

			List<PontoConsumoCityGate> listaPontoConsumoCityGateAuxiliar = new ArrayList<PontoConsumoCityGate>();
			listaPontoConsumoCityGateAuxiliar.addAll(pontoConsumo.getListaPontoConsumoCityGate());

			ComparatorChain comparador = new ComparatorChain();
			comparador.addComparator(new BeanComparator("dataVigencia"), true);

			Collections.sort(listaPontoConsumoCityGateAuxiliar, comparador);

			verificaListaPontoConsumoCityGateAuxiliar(listaPontoConsumoCityGateAuxiliar);

			listaPontoConsumoCityGate.addAll(listaPontoConsumoCityGateAuxiliar);
			Collections.sort(listaPontoConsumoCityGate, new Comparator<PontoConsumoCityGate>() {

				@Override
				public int compare(PontoConsumoCityGate o1, PontoConsumoCityGate o2) {

					return o1.getCityGate().getDescricao().compareToIgnoreCase(o2.getCityGate().getDescricao());
				}
			});

			this.carregarParametroPercentualCityGate(listaCityGate, imovelVO, listaPontoConsumoCityGate, model, request);
		}

		if (pontoConsumo.getListaPontoConsumoTributoAliquota() != null
				&& !pontoConsumo.getListaPontoConsumoTributoAliquota().isEmpty()) {
			for (PontoConsumoTributoAliquota pontoConsumoTributoAliquota : pontoConsumo.getListaPontoConsumoTributoAliquota()) {
				listaPontoConsumoTributoAliquota.add(pontoConsumoTributoAliquota);
			}
		}

		if (pontoConsumo.getListaPontoConsumoEquipamento() != null && !pontoConsumo.getListaPontoConsumoEquipamento().isEmpty()) {
			for (PontoConsumoEquipamento pontoConsumoEquipamento : pontoConsumo.getListaPontoConsumoEquipamento()) {
				listaPontoConsumoEquipamento.add(pontoConsumoEquipamento);
			}
		}

		if (pontoConsumo.getSituacaoConsumo() != null) {
			imovelPontoConsumoVO.setIdSituacaoPontoConsumo(pontoConsumo.getSituacaoConsumo().getChavePrimaria());
		} else {
			imovelPontoConsumoVO.setIdSituacaoPontoConsumo(null);
		}
		
		imovelPontoConsumoVO.setDescricaoMensagemAdicional(pontoConsumo.getDescricaoMensagemAdicional());
		imovelPontoConsumoVO.setIndicadorMensagemAdicional(pontoConsumo.isIndicadorMensagemAdicional());
	}

	/**
	 * Limpar ponto consumo tributo aliquota no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("limparPontoConsumoTributoEquipamentoAlteracao")
	public String limparPontoConsumoTributoEquipamentoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.limparPontoConsumoTributoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}
		return view;
	}

	/**
	 * Limpar ponto consumo tributo aliquota no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("limparPontoConsumoTributoEquipamento")
	public String limparPontoConsumoTributoEquipamentoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;
		try {
			this.limparPontoConsumoTributoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	private void limparPontoConsumoTributoEquipamento(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		limparPontoConsumoTributoAliquota(imovelVO, model, request);

		limparPontoConsumoEquipamento(imovelVO, model, request);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Adicionar ponto consumo tributo aliquota no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarPontoConsumoTributoAliquotaAlteracao")
	public String adicionarPontoConsumoTributoAliquotaAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult) {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.adicionarPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}
		return view;
	}

	/**
	 * Adicionar ponto consumo tributo aliquota no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarPontoConsumoTributoAliquota")
	public String adicionarPontoConsumoTributoAliquotaInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			this.adicionarPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void adicionarPontoConsumoTributoAliquota(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, null);

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		Integer indexListaPontoConsumoTributo = imovelPontoConsumoVO.getIndexListaPontoConsumoTributo();
		if (indexListaPontoConsumoTributo != null && indexListaPontoConsumoTributo > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, true);
		}

		PontoConsumo pontoConsumo = null;
		if (indexLista != null && indexLista > -1) {
			Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
			if (listaPontoConsumo != null) {
				pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());
			}
		} else {
			pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
			this.popularImovelPontoConsumo(pontoConsumo, imovelVO, imovelPontoConsumoVO, request);
		}

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
		PontoConsumoTributoAliquota pontoConsumoTributoAliquota =
				(PontoConsumoTributoAliquota) controladorImovel.criarPontoConsumoTributoAliquota();
		this.popularImovelPontoConsumoTributo(pontoConsumo, pontoConsumoTributoAliquota, imovelPontoConsumoVO);

		controladorImovel.validarPontoConsumoTributoAliquota(listaPontoConsumoTributoAliquota, pontoConsumoTributoAliquota,
				Integer.valueOf(-1));
		listaPontoConsumoTributoAliquota.add(pontoConsumoTributoAliquota);

		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA_TRIBUTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Alterar ponto consumo tributo aliquota no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("alterarPontoConsumoTributoAliquotaAlteracao")
	public String alterarPontoConsumoTributoAliquotaAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.alterarPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Alterar ponto consumo tributo aliquota no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("alterarPontoConsumoTributoAliquota")
	public String alterarPontoConsumoTributoAliquotaInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			this.alterarPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void alterarPontoConsumoTributoAliquota(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		Integer indexListaPontoConsumoTributo = imovelPontoConsumoVO.getIndexListaPontoConsumoTributo();
		if (indexListaPontoConsumoTributo != null && indexListaPontoConsumoTributo > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, true);
		}

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		PontoConsumo pontoConsumo = null;
		if (indexLista != null && indexLista > -1) {
			Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
			if (listaPontoConsumo != null) {
				pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());
			}
		}

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
		if ((listaPontoConsumoTributoAliquota != null) && (indexListaPontoConsumoTributo != null)) {
			PontoConsumoTributoAliquota pontoConsumoTributoAliquota =
					(PontoConsumoTributoAliquota) controladorImovel.criarPontoConsumoTributoAliquota();

			pontoConsumoTributoAliquota.setChavePrimaria(
					((PontoConsumoTributoAliquota) ((List) listaPontoConsumoTributoAliquota).get(indexListaPontoConsumoTributo.intValue()))
							.getChavePrimaria());
			this.popularImovelPontoConsumoTributo(pontoConsumo, pontoConsumoTributoAliquota, imovelPontoConsumoVO);

			controladorImovel.validarPontoConsumoTributoAliquota(listaPontoConsumoTributoAliquota, pontoConsumoTributoAliquota,
					indexListaPontoConsumoTributo);
			if (pontoConsumo == null) {
				pontoConsumoTributoAliquota.setPontoConsumo(((PontoConsumoTributoAliquota) ((List) listaPontoConsumoTributoAliquota)
						.get(indexListaPontoConsumoTributo.intValue())).getPontoConsumo());

			}

			((List) listaPontoConsumoTributoAliquota).remove(indexListaPontoConsumoTributo.intValue());

			listaPontoConsumoTributoAliquota.add(pontoConsumoTributoAliquota);

			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);

		}

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA_TRIBUTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Remover ponto consumo tributo aliquota no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumoTributoAliquotaAlteracao")
	public String removerPontoConsumoTributoAliquotaAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.removerPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Remover ponto consumo tributo aliquota no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumoTributoAliquota")
	public String removerPontoConsumoTributoAliquotaInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			this.removerPontoConsumoTributoAliquota(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void removerPontoConsumoTributoAliquota(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		Integer indexListaPontoConsumoTributo = imovelPontoConsumoVO.getIndexListaPontoConsumoTributo();

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);

		if (listaPontoConsumoTributoAliquota != null && indexListaPontoConsumoTributo != null) {
			((List) listaPontoConsumoTributoAliquota).remove(indexListaPontoConsumoTributo.intValue());
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA_TRIBUTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		imovelPontoConsumoVO.setIndexListaPontoConsumoTributo(-1);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Alterar o equipamento do ponto de consumo no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarPontoConsumoEquipamentoAlteracao")
	public String alterarPontoConsumoEquipamentoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.alterarPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Alterar o equipamento do ponto de consumo no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarPontoConsumoEquipamento")
	public String alterarPontoConsumoEquipamentoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {
			this.alterarPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void alterarPontoConsumoEquipamento(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		Integer indexListaPontoConsumoEquipamento = imovelPontoConsumoVO.getIndexListaPontoConsumoEquipamento();
		if (indexListaPontoConsumoEquipamento != null && indexListaPontoConsumoEquipamento > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_EQUIPAMENTO, true);
		}

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		PontoConsumo pontoConsumo = null;
		if (indexLista != null && indexLista > -1) {
			Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
			if (listaPontoConsumo != null) {
				pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());
			}
		}

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		if ((listaPontoConsumoEquipamento != null) && (indexListaPontoConsumoEquipamento != null)) {
			PontoConsumoEquipamento pontoConsumoEquipamento = (PontoConsumoEquipamento) controladorImovel.criarPontoConsumoEquipamento();

			pontoConsumoEquipamento.setChavePrimaria(
					((PontoConsumoEquipamento) ((List) listaPontoConsumoEquipamento).get(indexListaPontoConsumoEquipamento.intValue()))
							.getChavePrimaria());
			this.popularImovelPontoConsumoEquipamento(pontoConsumo, pontoConsumoEquipamento, imovelPontoConsumoVO);

			controladorImovel.validarPontoConsumoEquipamento(listaPontoConsumoEquipamento, pontoConsumoEquipamento,
					indexListaPontoConsumoEquipamento);

			if (pontoConsumo == null) {
				pontoConsumoEquipamento.setPontoConsumo(
						((PontoConsumoEquipamento) ((List) listaPontoConsumoEquipamento).get(indexListaPontoConsumoEquipamento.intValue()))
								.getPontoConsumo());

			}

			((List) listaPontoConsumoEquipamento).remove(indexListaPontoConsumoEquipamento.intValue());

			listaPontoConsumoEquipamento.add(pontoConsumoEquipamento);

			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		}

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	/**
	 * Remover o equipamento de ponto de consumo no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumoEquipamentoAlteracao")
	public String removerPontoConsumoEquipamentoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;
		try {

			this.removerPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Remover o equipamento de ponto de consumo no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerPontoConsumoEquipamento")
	public String removerPontoConsumoEquipamentoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;
		try {

			this.removerPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void removerPontoConsumoEquipamento(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		Integer indexListaPontoConsumoEquipamento = imovelPontoConsumoVO.getIndexListaPontoConsumoEquipamento();

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		if (listaPontoConsumoEquipamento != null && indexListaPontoConsumoEquipamento != null) {
			((List) listaPontoConsumoEquipamento).remove(indexListaPontoConsumoEquipamento.intValue());
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);

		imovelPontoConsumoVO.setIndexListaPontoConsumoEquipamento(-1);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);
	}

	/**
	 * Adicionar equipamento do ponto de consumo no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("adicionarPontoConsumoEquipamentoAlteracao")
	public String adicionarPontoConsumoEquipamentoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {

			this.adicionarPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Adicionar equipamento do ponto de consumo no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("adicionarPontoConsumoEquipamento")
	public String adicionarPontoConsumoEquipamentoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO, BindingResult bindingResult)
			throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {

			this.adicionarPontoConsumoEquipamento(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO,
					imovelRelacionamentoVO, imovelContatoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	private void adicionarPontoConsumoEquipamento(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO) throws GGASException {

		validarToken(request);

		model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_EQUIPAMENTO, null);

		Integer indexLista = imovelVO.getIndexLista();
		if (indexLista != null && indexLista > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
		}

		Integer indexListaPontoConsumoEquipamento = imovelPontoConsumoVO.getIndexListaPontoConsumoEquipamento();
		if (indexListaPontoConsumoEquipamento != null && indexListaPontoConsumoEquipamento > -1) {
			request.setAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_EQUIPAMENTO, true);
		}

		PontoConsumo pontoConsumo = null;
		if (indexLista != null && indexLista > -1) {
			Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
			if (listaPontoConsumo != null) {
				pontoConsumo = (PontoConsumo) ((List) listaPontoConsumo).get(indexLista.intValue());
			}
		} else {
			pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
			this.popularImovelPontoConsumo(pontoConsumo, imovelVO, imovelPontoConsumoVO, request);
		}

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		String qtdeEquipamentos = imovelPontoConsumoVO.getQtdeEquipamentos();

		if (!StringUtils.isEmpty(qtdeEquipamentos)) {
			int qtdeEquipamentosInt = Util.converterCampoStringParaValorInteger(qtdeEquipamentos);
			for (int i = 0; i < qtdeEquipamentosInt; i++) {
				PontoConsumoEquipamento pontoConsumoEquipamento =
						(PontoConsumoEquipamento) controladorImovel.criarPontoConsumoEquipamento();
				this.popularImovelPontoConsumoEquipamento(pontoConsumo, pontoConsumoEquipamento, imovelPontoConsumoVO);

				controladorImovel.validarPontoConsumoEquipamento(listaPontoConsumoEquipamento, pontoConsumoEquipamento,
						Integer.valueOf(-1));
				listaPontoConsumoEquipamento.add(pontoConsumoEquipamento);
			}
		} else {
			PontoConsumoEquipamento pontoConsumoEquipamento = (PontoConsumoEquipamento) controladorImovel.criarPontoConsumoEquipamento();
			this.popularImovelPontoConsumoEquipamento(pontoConsumo, pontoConsumoEquipamento, imovelPontoConsumoVO);

			controladorImovel.validarPontoConsumoEquipamento(listaPontoConsumoEquipamento, pontoConsumoEquipamento, Integer.valueOf(-1));
			listaPontoConsumoEquipamento.add(pontoConsumoEquipamento);
		}

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO,
				new ArrayList<PontoConsumoEquipamento>(listaPontoConsumoEquipamento));
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	private void popularImovelPontoConsumoEquipamento(PontoConsumo pontoConsumo, PontoConsumoEquipamento pontoConsumoEquipamento,
			ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		Long idEquipamento = imovelPontoConsumoVO.getIdEquipamento();
		String potencia = imovelPontoConsumoVO.getPotencia();
		String horasPorDia = imovelPontoConsumoVO.getHorasPorDia();
		String diasPorSemana = imovelPontoConsumoVO.getDiasPorSemana();
		Long idPotenciaFixa = imovelPontoConsumoVO.getIdPotenciaFixa();

		pontoConsumoEquipamento.setPontoConsumo(pontoConsumo);

		if (idEquipamento != null && idEquipamento > 0) {
			Equipamento equipamento = controladorEquipamento.obterEquipamento(idEquipamento);
			pontoConsumoEquipamento.setEquipamento(equipamento);
		} else {
			pontoConsumoEquipamento.setEquipamento(null);
		}

		if (!StringUtils.isEmpty(potencia)) {
			pontoConsumoEquipamento
					.setPotencia(Util.converterCampoStringParaValorBigDecimal(PontoConsumoEquipamento.PONTO_CONSUMO_EQUIPAMENTO_POTENCIA,
							potencia, Constantes.FORMATO_VALOR_FATOR_K, Constantes.LOCALE_PADRAO));
		} else {
			pontoConsumoEquipamento.setPotencia(null);
		}

		if (!StringUtils.isEmpty(horasPorDia)) {
			pontoConsumoEquipamento.setHorasPorDia(Util.converterCampoStringParaValorInteger(horasPorDia));
		} else {
			pontoConsumoEquipamento.setPotencia(null);
		}

		if (!StringUtils.isEmpty(diasPorSemana)) {
			pontoConsumoEquipamento.setDiasPorSemana(Util.converterCampoStringParaValorInteger(diasPorSemana));
		} else {
			pontoConsumoEquipamento.setPotencia(null);
		}

		if (idPotenciaFixa != null && idPotenciaFixa >= 0) {
			pontoConsumoEquipamento.setPotenciaFixa(idPotenciaFixa);
		} else {
			pontoConsumoEquipamento.setPotenciaFixa(null);
		}
	}

	private void verificaListaPontoConsumoCityGateAuxiliar(List<PontoConsumoCityGate> listaPontoConsumoCityGateAuxiliar) {
		if (!listaPontoConsumoCityGateAuxiliar.isEmpty()) {
			List<PontoConsumoCityGate> listaPontoConsumoCityGateRemocao = new ArrayList<PontoConsumoCityGate>();
			PontoConsumoCityGate pontoConsumoCityGateReferencia = Util.primeiroElemento(listaPontoConsumoCityGateAuxiliar);
			for (PontoConsumoCityGate pontoConsumoCityGate : listaPontoConsumoCityGateAuxiliar) {
				if (Util.compararDatas(pontoConsumoCityGateReferencia.getDataVigencia(), pontoConsumoCityGate.getDataVigencia()) > 0) {
					listaPontoConsumoCityGateRemocao.add(pontoConsumoCityGate);
				}
			}
			listaPontoConsumoCityGateAuxiliar.removeAll(listaPontoConsumoCityGateRemocao);
		}
	}

	/**
	 * Método responsável por adicionar o ponto de consumo do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarPontoConsumoAlteracao")
	public String adicionarPontoConsumoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelContatoVO imovelContatoVO, ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;

		try {
			this.adicionarPontoConsumo(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO, imovelContatoVO,
					imovelRelacionamentoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	/**
	 * Método responsável por adicionar o ponto de consumo do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("adicionarPontoConsumo")
	public String adicionarPontoConsumoFluxoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelContatoVO imovelContatoVO, ImovelRelacionamentoVO imovelRelacionamentoVO, BindingResult bindingResult) {

		String view = VIEW_EXIBIR_INCLUSAO_IMOVEL;

		try {

			this.adicionarPontoConsumo(model, request, imovelVO, imovelPontoConsumoVO, imovelUnidadeConsumidoraVO, imovelContatoVO,
					imovelRelacionamentoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_IMOVEL;
		}

		return view;
	}

	@SuppressWarnings("unchecked")
	private void adicionarPontoConsumo(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelContatoVO imovelContatoVO, ImovelRelacionamentoVO imovelRelacionamentoVO) throws GGASException {

		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		validarToken(request);

		Integer indexLista = imovelVO.getIndexLista();

		if (indexLista != null && indexLista == -1) {
			imovelPontoConsumoVO.setIdPontoConsumo(0L);
		}
		List<PontoConsumo> listaPontoConsumo = (List<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);

		if (indexLista == null || indexLista < 0) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
			this.popularImovelPontoConsumo(pontoConsumo, imovelVO, imovelPontoConsumoVO, request);
			controladorImovel.verificarDuplicidadeEnderecoRemoto(indexLista, pontoConsumo.getCodigoPontoConsumoSupervisorio(),
					listaPontoConsumo);
			controladorImovel.validarDadosPontoConsumo(pontoConsumo);
			listaPontoConsumo.add(pontoConsumo);
			listaPontoConsumoTributoAliquota.clear();
			listaPontoConsumoEquipamento.clear();
		} else {
			this.alterarPontoConsumo(indexLista, imovelVO, imovelPontoConsumoVO, listaPontoConsumo, model, request);
			model.addAttribute(PARAM_ALTERACAO, Boolean.TRUE);
		}

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		limparPontoConsumoTributoAliquota(imovelVO, model, request);
		limparPontoConsumoEquipamento(imovelVO, model, request);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		saveToken(request);
	}

	@SuppressWarnings("unchecked")
	private void limparPontoConsumoEquipamento(ImovelVO imovelVO, Model model, HttpServletRequest request) throws GGASException {

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);
		listaPontoConsumoEquipamento.clear();
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		carregarCityGatePorQuadraFace(imovelVO, model, request);
	}

	@SuppressWarnings("unchecked")
	private void limparPontoConsumoTributoAliquota(ImovelVO imovelVO, Model model, HttpServletRequest request) throws GGASException {

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
		listaPontoConsumoTributoAliquota.clear();
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		carregarCityGatePorQuadraFace(imovelVO, model, request);
	}

	private void carregarCityGatePorQuadraFace(ImovelVO imovelVO, Model model, HttpServletRequest request) throws GGASException {

		Long idQuadraFace = imovelVO.getIdQuadraFace();
		Collection<PontoConsumoCityGate> listaPontoConsumoCityGate = new ArrayList<PontoConsumoCityGate>();
		if (idQuadraFace == null || idQuadraFace == -1) {
			listaPontoConsumoCityGate.clear();
			model.addAttribute(SIZE_LISTA_CITY_GATE, 0);
			model.addAttribute(LISTA_CITY_GATE, listaPontoConsumoCityGate);
			request.getSession().setAttribute(LISTA_CITY_GATE, listaPontoConsumoCityGate);
		} else {
			Collection<CityGate> listaCityGate = null;
			listaCityGate = controladorQuadra.listarCityGatePorQuadraFace(idQuadraFace);
			this.carregarParametroPercentualCityGate(listaCityGate, imovelVO, listaPontoConsumoCityGate, model, request);
		}
	}

	private void alterarPontoConsumo(Integer indexLista, ImovelVO imovelVO, ImovelPontoConsumoVO imovelPontoConsumoVO,
			List<PontoConsumo> listaPontoConsumo, Model model, HttpServletRequest request) throws GGASException {

		if ((listaPontoConsumo != null) && (indexLista != null)) {
			model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);

			Integer indexListaPontoConsumoTributo = imovelPontoConsumoVO.getIndexListaPontoConsumoTributo();
			if (indexListaPontoConsumoTributo != null && indexListaPontoConsumoTributo > -1) {
				model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, true);
			}

			String idPontoConsumoLegado = imovelPontoConsumoVO.getPontoConsumoLegado();
			Long idPontoConsumo = imovelPontoConsumoVO.getIdPontoConsumo();

			if (!idPontoConsumoLegado.isEmpty()) {
				controladorPontoConsumo.validarPontoConsumoECodigoLegado(idPontoConsumoLegado, idPontoConsumo);
			}

			PontoConsumo pontoConsumoAlterado = (PontoConsumo) controladorImovel.criarPontoConsumo();
			this.popularImovelPontoConsumo(pontoConsumoAlterado, imovelVO, imovelPontoConsumoVO, request);
			controladorImovel.verificarDuplicidadeEnderecoRemoto(indexLista, pontoConsumoAlterado.getCodigoPontoConsumoSupervisorio(),
					listaPontoConsumo);
			controladorImovel.validarDadosPontoConsumo(pontoConsumoAlterado);
			PontoConsumo pontoConsumoExistente = null;
			for (int i = 0; i < listaPontoConsumo.size(); i++) {
				pontoConsumoExistente = listaPontoConsumo.get(i);
				if ((i == indexLista) && (pontoConsumoExistente != null)) {
					this.popularImovelPontoConsumo(pontoConsumoExistente, imovelVO, imovelPontoConsumoVO, request);
					model.addAttribute(LISTA_RAMO_ATIVIDADE_PONTO_CONSUMO, pontoConsumoExistente.getListaPontoConsumoTributoAliquota());
				}
			}
		}
	}

	/**
	 * Método responsável por carregar o city gate do ponto de consumo do imóvel no fluxo de alteração.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("carregarCityGateFluxoAlteracao")
	public String carregarCityGateFluxoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult) {

		this.carregarCityGate(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_RELACIONAMENTO_VO, imovelRelacionamentoVO);
		model.addAttribute(IMOVEL_CONTATO_VO, imovelContatoVO);
		model.addAttribute(IMOVEL_UNIDADE_CONSUMIDORA_VO, imovelUnidadeConsumidoraVO);
		model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);

		saveToken(request);

		return VIEW_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Método responsável por carregar o city gate do ponto de consumo do imóvel no fluxo de inclusão.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("carregarCityGateFluxoInclusao")
	public String carregarCityGateFluxoInclusao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult) {

		this.carregarCityGate(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_RELACIONAMENTO_VO, imovelRelacionamentoVO);
		model.addAttribute(IMOVEL_CONTATO_VO, imovelContatoVO);
		model.addAttribute(IMOVEL_UNIDADE_CONSUMIDORA_VO, imovelUnidadeConsumidoraVO);
		model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);

		saveToken(request);

		return VIEW_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Exibir outra página da tabela de equipamento no fluxo de alteração do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPaginaTabelaEquipamentoAlteracao")
	public String exibirPaginaTabelaEquipamentoAlteracao(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult)
			throws GGASException {

		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		model.addAttribute(SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO, true);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		return VIEW_EXIBIR_ALTERACAO_IMOVEL;
	}

	/**
	 * Exibir outra página da tabela de equipamento no fluxo de inclusão do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPaginaTabelaEquipamento")
	public String exibirPaginaTabelaEquipamento(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult)
			throws GGASException {

		model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		model.addAttribute(SUCESSO_MANUTENCAO_LISTA_EQUIPAMENTO, true);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		return VIEW_EXIBIR_INCLUSAO_IMOVEL;
	}

	private void carregarCityGate(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) {

		try {
			Long idQuadraFacePontoConsumo = imovelVO.getIdQuadraFacePontoConsumo();
			Collection<PontoConsumoCityGate> listaPontoConsumoCityGate = new ArrayList<PontoConsumoCityGate>();

			Integer indexLista = imovelVO.getIndexLista();
			if (indexLista != null && indexLista > -1) {
				model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO, true);
			}

			Integer indexListaPontoConsumoTributo = imovelVO.getIndexListaPontoConsumoTributo();
			if (indexListaPontoConsumoTributo != null && indexListaPontoConsumoTributo > -1) {
				model.addAttribute(ESTADO_ALTERACAO_PONTO_CONSUMO_TRIBUTO, true);
			}

			if (idQuadraFacePontoConsumo != null && idQuadraFacePontoConsumo > 0) {
				Collection<CityGate> listaCityGate = null;
				listaCityGate = controladorQuadra.listarCityGatePorQuadraFace(idQuadraFacePontoConsumo);
				this.carregarParametroPercentualCityGate(listaCityGate, imovelVO, listaPontoConsumoCityGate, model, request);
			} else {
				listaPontoConsumoCityGate.clear();
				model.addAttribute(SIZE_LISTA_CITY_GATE, 0);
				imovelVO.setPercentualCityGate(null);
				model.addAttribute(LISTA_CITY_GATE, listaPontoConsumoCityGate);
				request.getSession().setAttribute(LISTA_CITY_GATE, listaPontoConsumoCityGate);
			}

			this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}
	}

	private void carregarParametroPercentualCityGate(Collection<CityGate> listaCityGate, ImovelVO imovelVO,
			Collection<PontoConsumoCityGate> listaPontoConsumoCityGate, Model model, HttpServletRequest request) throws GGASException {

		String percentualParticipacao;
		String[] arrayPercentualCityGate;
		int i = 0;
		if (!listaPontoConsumoCityGate.isEmpty()) {

			if (listaPontoConsumoCityGate.size() != listaCityGate.size()) {

				arrayPercentualCityGate = new String[listaCityGate.size()];
				arrayPercentualCityGate = realizarCalculoPercentualCityGate(listaCityGate, arrayPercentualCityGate);

				model.addAttribute(SIZE_LISTA_CITY_GATE, listaCityGate.size());

			} else {

				arrayPercentualCityGate = new String[listaPontoConsumoCityGate.size()];
				for (PontoConsumoCityGate pontoConsumoCityGate : listaPontoConsumoCityGate) {
					percentualParticipacao = pontoConsumoCityGate.getPercentualParticipacao().toString();
					arrayPercentualCityGate[i] = percentualParticipacao.replace(".", ",");
					i++;
				}
				model.addAttribute(SIZE_LISTA_CITY_GATE, listaPontoConsumoCityGate.size());
			}

			imovelVO.setPercentualCityGate(arrayPercentualCityGate);
			imovelVO.setDataInicioVigenciaCityGate(DataUtil.converterDataParaString(
					listaPontoConsumoCityGate.iterator().next().getDataVigencia()));
		} else if (listaCityGate != null && !listaCityGate.isEmpty()) {

			arrayPercentualCityGate = new String[listaCityGate.size()];
			model.addAttribute(SIZE_LISTA_CITY_GATE, listaCityGate.size());
			arrayPercentualCityGate = realizarCalculoPercentualCityGate(listaCityGate, arrayPercentualCityGate);
			imovelVO.setPercentualCityGate(arrayPercentualCityGate);
			imovelVO.setDataInicioVigenciaCityGate(DataUtil.converterDataParaString(Calendar.getInstance().getTime()));
		}
		model.addAttribute(LISTA_CITY_GATE, listaCityGate);
		request.getSession().setAttribute(LISTA_CITY_GATE, listaCityGate);
	}

	private String[] realizarCalculoPercentualCityGate(Collection<CityGate> listaCityGate, String[] arrayPercentualCityGate) {

		String parametroPercentualParticipacaoCityGate =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL_PARTICIPACAO_CITY_GATE);
		double percentualCityGate = Integer.parseInt(parametroPercentualParticipacaoCityGate) / (double) listaCityGate.size();
		DecimalFormat quatroCasasFormat = new DecimalFormat("##.####");

		int i = 0;
		int testador;
		if (listaCityGate.size() < 3) {
			testador = arrayPercentualCityGate.length;
		} else {
			testador = (arrayPercentualCityGate.length - 1);
		}
		double somadorParcial = 0;

		for (i = 0; i < testador; i++) {
			String temporario = quatroCasasFormat.format(percentualCityGate);
			double parcial = Double.parseDouble(temporario.replace(",", "."));
			somadorParcial = somadorParcial + parcial;
			arrayPercentualCityGate[i] = temporario.replace(".", ",");
		}
		if (listaCityGate.size() >= 3) {
			DecimalFormat somadorFormat = new DecimalFormat("###.####");
			String somadorTotal = "100.0000";
			double somadorConvertido = Double.parseDouble(somadorTotal);
			String somadorParcialString = quatroCasasFormat.format(somadorParcial);
			somadorParcial = Double.parseDouble(somadorParcialString.replace(",", "."));
			somadorConvertido = somadorConvertido - somadorParcial;
			String somadConvert = somadorFormat.format(somadorConvertido);
			arrayPercentualCityGate[i] = somadConvert.replace(".", ",");
		}

		return arrayPercentualCityGate;
	}

	private void carregarCamposVeioDochamado(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		this.carregarCampos(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);
	}

	private void carregarCampos(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		String quantidadeAnosCalendario =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		model.addAttribute("intervaloAnosAteDataAtual", intervaloAnosAteDataAtual(quantidadeAnosCalendario));
		model.addAttribute("intervaloAnosData", Util.intervaloAnosData(quantidadeAnosCalendario));

		Map<String, String> datas;
		try {
			datas = Util.dataMinimaMaxima();
		} catch (Exception e) {
			throw new GGASException(e);
		}

		model.addAttribute(DATA_MINIMA, datas.get(DATA_MINIMA));
		model.addAttribute(DATA_MAXIMA, datas.get(DATA_MAXIMA));

		this.exibirDadosAbaIdentificacaoLocalizacao(model, imovelVO);
		this.exibirDadosAbaCaracteristicas(model, imovelVO);
		this.exibirDadosAbaRelacionamento(model, imovelVO, request);
		this.exibirDadosAbaContato(imovelVO, request);
		this.exibirDadosAbaUnidadesConsumidoras(imovelVO, imovelUnidadeConsumidoraVO, request);
		this.exibirDadosAbaPontoConsumo(imovelVO, imovelPontoConsumoVO, model, request);
	}

	@SuppressWarnings("unchecked")
	private void exibirDadosAbaUnidadesConsumidoras(ImovelVO imovelVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			HttpServletRequest request) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();
		Long idSegmento = imovelUnidadeConsumidoraVO.getIdSegmento();

		Collection<ImovelRamoAtividade> listaImovelRamoAtividade = new ArrayList<ImovelRamoAtividade>();

		if (!isPostBack(request)) {
			if (chavePrimaria != null && chavePrimaria != 0) {
				listaImovelRamoAtividade.addAll(controladorImovel.listarUnidadeConsumidoraPorChaveImovel(chavePrimaria));
			}
			request.getSession().setAttribute(LISTA_UNIDADE_CONSUMIDORA, listaImovelRamoAtividade);
		} else {
			listaImovelRamoAtividade = (Collection<ImovelRamoAtividade>) request.getSession().getAttribute(LISTA_UNIDADE_CONSUMIDORA);
		}

		request.setAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		if (idSegmento != null && idSegmento > 0) {
			request.setAttribute(LISTA_RAMO_ATIVIDADE, controladorRamoAtividade.listarRamoAtividadePorSegmento(idSegmento));
		}

		request.setAttribute(LISTA_UNIDADE_CONSUMIDORA, exibirListaUnidadeConsumidora(listaImovelRamoAtividade));
	}

	private List<Map<String, Object>> exibirListaUnidadeConsumidora(Collection<ImovelRamoAtividade> listaImovelRamoAtividade) {

		List<Map<String, Object>> unidadesConsumidoras = new ArrayList<Map<String, Object>>();

		if (listaImovelRamoAtividade != null && !listaImovelRamoAtividade.isEmpty()) {
			for (ImovelRamoAtividade imovelRamoAtividade : listaImovelRamoAtividade) {
				Map<String, Object> dados = new HashMap<String, Object>();
				dados.put(ID_SEGMENTO, imovelRamoAtividade.getRamoAtividade().getSegmento().getChavePrimaria());
				dados.put("descricaoSegmento", imovelRamoAtividade.getRamoAtividade().getSegmento().getDescricao());
				dados.put(ID_RAMO_ATIVIDADE, imovelRamoAtividade.getRamoAtividade().getChavePrimaria());
				dados.put("descricaoRamoAtividade", imovelRamoAtividade.getRamoAtividade().getDescricao());
				dados.put(QUANTIDADE_ECONOMIA, imovelRamoAtividade.getQuantidadeEconomia());

				unidadesConsumidoras.add(dados);
			}
		}

		return unidadesConsumidoras;
	}

	private void exibirDadosAbaIdentificacaoLocalizacao(Model model, ImovelVO imovelVO) throws GGASException {

		Long idQuadraFace = imovelVO.getIdQuadraFace();
		Long chavePrimaria = imovelVO.getChavePrimaria();

		if (StringUtils.isNotBlank(imovelVO.getCepImovel())) {
			model.addAttribute("listaQuadra", controladorQuadra.listarQuadraPorCep(imovelVO.getCepImovel()));
		}

		if ((imovelVO.getIdQuadraImovel() != null) && (imovelVO.getIdQuadraImovel() >= 0)) {
			List<QuadraFace> faces = controladorQuadra.obterQuadraFacePorQuadra(imovelVO.getIdQuadraImovel(), imovelVO.getCepImovel());
			model.addAttribute("listaFacesQuadra", faces);
		}

		String[] codigoSitucaoImovel = { controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO) };

		Imovel imovel = null;

		if (chavePrimaria != null && chavePrimaria > 0) {
			imovel = (Imovel) controladorImovel.obter(chavePrimaria, QUADRA_FACE, QUADRA_FACE_ENDERECO, QUADRA_FACE_ENDERECO_CEP, QUADRA);
		}

		if (imovel != null && imovel.getSituacaoImovel().getChavePrimaria() == Integer.parseInt(codigoSitucaoImovel[0])) {
			imovelVO.setIdSituacao(Long.parseLong(codigoSitucaoImovel[0]));
			imovelVO.setCodigoSitucaoImovel(codigoSitucaoImovel[0]);
		}

		model.addAttribute(SITUACOES_IMOVEL,
				controladorImovel.verificarSitucaoImovelPorRedeIndicador(
						idQuadraFace == null || idQuadraFace == -1 ? null : String.valueOf(idQuadraFace),
						chavePrimaria == null || imovel.getChavePrimaria() <= 0 ? null : String.valueOf(imovel.getChavePrimaria())));

		model.addAttribute(LISTA_AGENTE, controladorAgente.obterAgentePorChaveFuncionario(null, "true"));
	}

	private void exibirDadosAbaCaracteristicas(Model model, ImovelVO imovelVO) throws GGASException {

		model.addAttribute(LISTA_PAVIMENTO_CALCADA, controladorImovel.listarPavimentoCalcada());
		model.addAttribute(LISTA_PAVIMENTO_RUA, controladorImovel.listarPavimentoRua());
		model.addAttribute(LISTA_PADRAO_CONSTRUCAO, controladorImovel.listarPadraoConstrucao());
		model.addAttribute(LISTA_AREA_CONSTRUIDA_FAIXA, controladorAreaConstruidaFaixa.listarAreaConstruidaFaixa());
		model.addAttribute(LISTA_PERFIL_IMOVEL, controladorImovel.listarPerfilImovel());
		model.addAttribute(LISTA_TIPO_BOTIJAO, controladorImovel.listarTipoBotijao());
		model.addAttribute(LISTA_TIPO_COMBUSTIVEL, controladorImovel.listarTipoCombustivel());

		model.addAttribute(LISTA_MODALIDADE_MEDICAO_IMOVEL, controladorImovel.listarModalidadeMedicaoImovel());
		model.addAttribute(LISTA_EMPRESA, controladorEmpresa.listarEmpresasConstrutoras());

		model.addAttribute(LISTA_REDE_MATERIAL, controladorRede.listarRedeMaterial());
		model.addAttribute(LISTA_REDE_DIAMETRO, controladorRede.listarRedeDiametro());

		model.addAttribute(LISTA_SERVICO_TIPO, controladorImovel.consultarServicoTipoOrdenados());

		Long chavePrimaria = imovelVO.getChavePrimaria();
		if (chavePrimaria != null && chavePrimaria.intValue() > 0) {
			Imovel imovel = (Imovel) controladorImovel.obter(chavePrimaria, REDE_INTERNA_IMOVEL, QUADRA_FACE, QUADRA_FACE_ENDERECO,
					QUADRA_FACE_ENDERECO_CEP);
			int qtdimovelAssociado = controladorImovel.quantidadeImoveisFilhos(imovel.getChavePrimaria());
			boolean naoPermiteAlteracao = Boolean.FALSE;
			if ((imovel.getCondominio()) && (qtdimovelAssociado > 0)) {
				naoPermiteAlteracao = Boolean.TRUE;
			}
			model.addAttribute("imovelAssociadoVerifado", naoPermiteAlteracao);
		}

	}

	@SuppressWarnings("unchecked")
	private void exibirDadosAbaRelacionamento(Model model, ImovelVO imovelVO, HttpServletRequest request) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();

		Collection<ClienteImovel> listaClienteImovel = new ArrayList<ClienteImovel>();

		if (!isPostBack(request)) {
			if (chavePrimaria != null && chavePrimaria != 0) {
				listaClienteImovel.addAll(controladorImovel.listarClienteImovelPorChaveImovel(chavePrimaria));
			}
			request.getSession().setAttribute(LISTA_CLIENTE_IMOVEL, listaClienteImovel);
		} else {
			listaClienteImovel = (Collection<ClienteImovel>) request.getSession().getAttribute(LISTA_CLIENTE_IMOVEL);
		}

		model.addAttribute(LISTA_MOTIVO_FIM_RELACIONAMENTO, controladorImovel.listarMotivoFimRelacionamentoClienteImovel());
		model.addAttribute(LISTA_TIPOS_RELACIONAMENTO, controladorImovel.listarTipoRelacionamentoClienteImovel());
		model.addAttribute(LISTA_CLIENTE_IMOVEL, exibirListaRelacionamento(listaClienteImovel));
	}

	@SuppressWarnings("unchecked")
	private void exibirDadosAbaContato(ImovelVO imovelVO, HttpServletRequest request) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();

		Collection<TipoContato> listaTiposContato = controladorCliente.listarTipoContato();
		Collection<Profissao> listaProfissao = controladorCliente.listarProfissao();

		request.setAttribute("listaTiposContato", listaTiposContato);
		request.setAttribute("listaProfissao", listaProfissao);

		Collection<ContatoImovel> listaContato = new ArrayList<ContatoImovel>();

		if (!isPostBack(request)) {
			if (chavePrimaria != null && chavePrimaria != 0) {
				listaContato.addAll(controladorImovel.listarContatoImovelPorChaveImovel(chavePrimaria));
			}
			Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());
			request.getSession().setAttribute(LISTA_CONTATO, listaContato);
		} else {
			listaContato = (Collection<ContatoImovel>) request.getSession().getAttribute(LISTA_CONTATO);
		}

		Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());
		request.setAttribute(LISTA_CONTATO, listaContato);
	}

	private List<Map<String, Object>> exibirListaRelacionamento(Collection<ClienteImovel> listaClienteImovel) throws GGASException {

		List<Map<String, Object>> relacionamentos = new ArrayList<Map<String, Object>>();

		if (listaClienteImovel != null && !listaClienteImovel.isEmpty()) {
			for (ClienteImovel clienteImovel : listaClienteImovel) {
				Map<String, Object> dados = new HashMap<String, Object>();

				Cliente cliente = (Cliente) controladorCliente.obter(clienteImovel.getCliente().getChavePrimaria(), "enderecos");

				dados.put("idCliente", cliente.getChavePrimaria());
				dados.put("nome", cliente.getNome());
				if (!StringUtils.isEmpty(cliente.getCpf())) {
					dados.put(DOCUMENTO_FORMATADO, cliente.getCpfFormatado());
				} else {
					if (!StringUtils.isEmpty(cliente.getCnpj())) {
						dados.put(DOCUMENTO_FORMATADO, cliente.getCnpjFormatado());
					} else {
						dados.put(DOCUMENTO_FORMATADO, "");
					}
				}
				if (cliente.getEnderecoPrincipal() != null) {
					dados.put("enderecoFormatado", cliente.getEnderecoPrincipal().getEnderecoFormatado());
				}
				dados.put("email", cliente.getEmailPrincipal());

				if (clienteImovel.getTipoRelacionamentoClienteImovel() != null) {
					dados.put("idTipoRelacionamento", clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria());
					dados.put("descricaoTipoRelacionamento", clienteImovel.getTipoRelacionamentoClienteImovel().getDescricao());
				}
				
				if (clienteImovel.getRelacaoInicio() != null) {
					dados.put("inicioRelacionamento",
							Util.converterDataParaStringSemHora(clienteImovel.getRelacaoInicio(), Constantes.FORMATO_DATA_BR));
				}

				if (clienteImovel.getMotivoFimRelacionamentoClienteImovel() != null) {
					dados.put("idMotivoRelacionamento", clienteImovel.getMotivoFimRelacionamentoClienteImovel().getChavePrimaria());
					dados.put("descricaoMotivoRelacionamento", clienteImovel.getTipoRelacionamentoClienteImovel().getDescricao());
				}
				if (clienteImovel.getRelacaoFim() != null) {
					dados.put("fimRelacionamento",
							Util.converterDataParaStringSemHora(clienteImovel.getRelacaoFim(), Constantes.FORMATO_DATA_BR));
				}
				if(clienteImovel.getMotivoFimRelacionamentoClienteImovel() != null){
					dados.put("descricaoMotivoFimRelacionamento", clienteImovel.getMotivoFimRelacionamentoClienteImovel().getDescricao());
				}

				relacionamentos.add(dados);
			}
		}

		return relacionamentos;
	}

	private String intervaloAnosAteDataAtual(String quantidadeAnosCalendario) throws GGASException {

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(quantidadeAnosCalendario));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	private void popularImovel(HttpServletRequest request, Imovel imovel, ImovelVO imovelVO) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();
		Integer versao = imovelVO.getVersao();

		if (chavePrimaria != null && chavePrimaria.longValue() > 0) {
			imovel.setChavePrimaria(chavePrimaria);
		}
		if (versao != null && versao.intValue() > 0) {
			imovel.setVersao(versao);
		}
		if (imovelVO.getImovelPai() != null && "true".equals(imovelVO.getImovelPai())
				&& (imovelVO.getMatriculaImovelPai() == null || "".equals(imovelVO.getMatriculaImovelPai()))) {
			final StringBuilder camposObrigatorios = new StringBuilder();
			camposObrigatorios.append("Imovel Condominio");
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
			camposObrigatorios.insert(0, "da aba Características: ");
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, camposObrigatorios.length()));

		}

		if (imovelVO.getMatriculaImovelPai() != null && !"".equals(imovelVO.getMatriculaImovelPai())) {
			ImovelImpl imovelPai = new ImovelImpl();
			imovelPai.setChavePrimaria(Long.parseLong(imovelVO.getMatriculaImovelPai()));
			imovel.setImovelCondominio(imovelPai);
		}

		if ("false".equals(imovelVO.getImovelPai()) || "".equals(imovelVO.getImovelPai())) {
			imovel.setImovelCondominio(null);
		}


		this.popularIdentificacaoLocalizacaoImovel(imovel, imovelVO);

		this.popularCaracteristicasImovel(request, imovel, imovelVO);

		this.popularRelacionamentos(imovel, request);

		this.popularContatos(imovel, request);

		this.popularUnidadesConsumidoras(imovel, request);

		this.popularPontosConsumo(imovel, request);
	}

	private void popularPontosConsumo(Imovel imovel, HttpServletRequest request) throws GGASException {

		Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			imovel.getListaPontoConsumo().clear();

			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				controladorSegmento.obter(pontoConsumo.getSegmento().getChavePrimaria(), "segmentoAmostragemPCSs", "segmentoIntervaloPCSs",
						"ramosAtividades");
				controladorRamoAtividade.obter(pontoConsumo.getRamoAtividade().getChavePrimaria());
				if (pontoConsumo.getModalidadeUso() != null) {
					controladorEntidadeConteudo.obter(pontoConsumo.getModalidadeUso().getChavePrimaria());
				}
				if (pontoConsumo.getCep() != null) {
					controladorEndereco.obterCepPorChave(pontoConsumo.getCep().getChavePrimaria());
				}
				controladorImovel.obterSituacaoConsumo(pontoConsumo.getSituacaoConsumo().getChavePrimaria());

				pontoConsumo.setImovel(imovel);

				imovel.getListaPontoConsumo().add(pontoConsumo);
			}
		} else {
			imovel.getListaPontoConsumo().clear();
		}
	}

	private void popularIdentificacaoLocalizacaoImovel(Imovel imovel, ImovelVO imovelVO) throws GGASException {

		if ( imovelVO.getIndicadorAlterarImovelCondominioFilho() != null) {
			imovel.setIndicadorAlterarImovelCondominioFilho( imovelVO.getIndicadorAlterarImovelCondominioFilho());
		}

		if (imovelVO.getIdAgente() != null && imovelVO.getIdAgente() > 0) {
			imovel.setAgente(controladorAgente.obterAgente(imovelVO.getIdAgente()));
		} else {
			imovel.setAgente(null);
		}

		if (StringUtils.isEmpty(imovelVO.getNome())) {
			imovel.setNome(null);
		} else {
			imovel.setNome(imovelVO.getNome());
		}
		if (!StringUtils.isEmpty(imovelVO.getCepImovel())) {
			imovel.setCep(imovelVO.getCepImovel());
			controladorEndereco.obterCeps(imovelVO.getCepImovel(), true);
			if ((imovelVO.getIdQuadraImovel() == null) || (imovelVO.getIdQuadraImovel() <= 0)) {
				imovel.setQuadra(null);
			} else {
				preencheQuadraImovel(imovel, imovelVO);
			}
		} else {
			imovel.setQuadraFace(null);
		}

		if (StringUtils.isEmpty(imovelVO.getNumeroImovel())) {
			imovel.setNumeroImovel(null);
		} else {
			if (!Constantes.SEM_NUMERO.equals(imovelVO.getNumeroImovel())) {
				Util.validarDominioCaracteres(Imovel.NUMERO_IMOVEL, imovelVO.getNumeroImovel(), Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_CARACTERES_ALFA_SEM_ACENTO);
			}
			imovel.setNumeroImovel(imovelVO.getNumeroImovel());
		}
		if (StringUtils.isEmpty(imovelVO.getComplementoImovel())) {
			imovel.setDescricaoComplemento(null);
		} else {
			imovel.setDescricaoComplemento(imovelVO.getComplementoImovel());
		}
		if (StringUtils.isEmpty(imovelVO.getEnderecoReferencia())) {
			imovel.setEnderecoReferencia(null);
		} else {
			imovel.setEnderecoReferencia(imovelVO.getEnderecoReferencia());
		}
		if (StringUtils.isEmpty(imovelVO.getNumeroLote())) {
			imovel.setNumeroLote(null);
		} else {
			imovel.setNumeroLote(Util.converterCampoStringParaValorInteger(Imovel.NUMERO_LOTE, imovelVO.getNumeroLote()));
		}
		if (StringUtils.isEmpty(imovelVO.getNumeroSublote())) {
			imovel.setNumeroSublote(null);
		} else {
			imovel.setNumeroSublote(Util.converterCampoStringParaValorInteger(Imovel.NUMERO_SUBLOTE, imovelVO.getNumeroSublote()));
		}
		if (StringUtils.isEmpty(imovelVO.getTestadaLote())) {
			imovel.setNumeroTestada(null);
		} else {
			imovel.setNumeroTestada(Util.converterCampoStringParaValorInteger(Imovel.TESTADA_LOTE, imovelVO.getTestadaLote()));
		}
		if (imovelVO.getIdSituacao() == null || imovelVO.getIdSituacao() <= 0) {
			imovel.setSituacaoImovel(null);
		} else {
			SituacaoImovel situacaoImovel = controladorImovel.obterSituacaoImovel(imovelVO.getIdSituacao());
			imovel.setSituacaoImovel(situacaoImovel);
		}
		if (!StringUtils.isEmpty(imovelVO.getHabilitado())) {
			imovel.setHabilitado(Boolean.valueOf(imovelVO.getHabilitado()));
		}
		if (imovelVO.getDataPrevisaoEncerramentoObra() != null && !imovelVO.getDataPrevisaoEncerramentoObra().isEmpty()) {
			Date dataEncerramento = Util.converterCampoStringParaData(Imovel.IMOVEL_DATA_PREVISAO_ENCERRAMENTO_OBRA,
					imovelVO.getDataPrevisaoEncerramentoObra(), Constantes.FORMATO_DATA_BR);
			imovel.setDataPrevisaoEncerramentoObra(dataEncerramento);
		} else {
			imovel.setDataPrevisaoEncerramentoObra(null);
		}
		if (imovelVO.getIdRotaPrevista() != null && imovelVO.getIdRotaPrevista() > 0) {
			Rota rota = (Rota) controladorRota.obter(imovelVO.getIdRotaPrevista());
			imovel.setRota(rota);
		} else {
			imovel.setRota(null);
		}
	}

	/**
	 * Metodo para preencher os dados da quadra do imovel
	 * @param imovel Imovel modelo
	 * @param imovelVO imovel representado no form
	 * @throws NegocioException Excecao de erro de negocio
	 */
	private void preencheQuadraImovel(Imovel imovel, ImovelVO imovelVO) throws NegocioException {
		final Quadra quadra = (Quadra) controladorQuadra.obter(imovelVO.getIdQuadraImovel(), "quadrasFace");
		imovel.setQuadra(quadra);
		if (imovelVO.getIdQuadraFace() == null || imovelVO.getIdQuadraFace() <= 0) {
			imovel.setQuadraFace(null);
		} else {
			if (imovelVO.getIdCepImovel() != null && !"".equals(imovelVO.getIdCepImovel())
					&& imovelVO.getIdQuadraImovel() != null) {
				final List<QuadraFace> faces = controladorQuadra
						.obterQuadraFacePorQuadraIdCep(imovelVO.getIdQuadraImovel(), imovelVO.getIdCepImovel());
				imovel.setQuadraFace(faces.get(0));
			} else {
				final QuadraFace quadraFace = controladorQuadra.obterQuadraFace(imovelVO.getIdQuadraFace());
				imovel.setQuadraFace(quadraFace);
			}
		}
	}

	private void popularCaracteristicasImovel(HttpServletRequest request, Imovel imovel, ImovelVO imovelVO) throws GGASException {

		if (StringUtils.isEmpty(imovelVO.getQuantidadeBanheiro())) {
			imovel.setQuantidadeBanheiro(null);
		} else {
			imovel.setQuantidadeBanheiro(
					Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_BANHEIRO, imovelVO.getQuantidadeBanheiro()));
		}

		if (imovelVO.getIdPavimentoCalcada() == null || imovelVO.getIdPavimentoCalcada() <= 0) {
			imovel.setPavimentoCalcada(null);
		} else {
			PavimentoCalcada pavimentoCalcada = controladorImovel.obterPavimentoCalcada(imovelVO.getIdPavimentoCalcada());
			imovel.setPavimentoCalcada(pavimentoCalcada);
		}
		if (imovelVO.getIdPavimentoRua() == null || imovelVO.getIdPavimentoRua() <= 0) {
			imovel.setPavimentoRua(null);
		} else {
			PavimentoRua pavimentoRua = controladorImovel.obterPavimentoRua(imovelVO.getIdPavimentoRua());
			imovel.setPavimentoRua(pavimentoRua);
		}
		if (imovelVO.getIdPadraoConstrucao() == null || imovelVO.getIdPadraoConstrucao() <= 0) {
			imovel.setPadraoConstrucao(null);
		} else {
			PadraoConstrucao padraoConstrucao = controladorImovel.obterPadraoConstrucao(imovelVO.getIdPadraoConstrucao());
			imovel.setPadraoConstrucao(padraoConstrucao);
		}
		if (imovelVO.getIdAreaConstruidaFaixa() == null || imovelVO.getIdAreaConstruidaFaixa() <= 0) {
			imovel.setAreaConstruidaFaixa(null);
		} else {
			AreaConstruidaFaixa areaConstruidaFaixa =
					(AreaConstruidaFaixa) controladorAreaConstruidaFaixa.obter(imovelVO.getIdAreaConstruidaFaixa());
			imovel.setAreaConstruidaFaixa(areaConstruidaFaixa);
		}
		if (imovelVO.getIdPerfilImovel() == null || imovelVO.getIdPerfilImovel() <= 0) {
			imovel.setPerfilImovel(null);
		} else {
			PerfilImovel perfilImovel = controladorImovel.obterPerfilImovel(imovelVO.getIdPerfilImovel());
			imovel.setPerfilImovel(perfilImovel);
		}
		if (imovelVO.getCondominio() == null || !Boolean.valueOf(imovelVO.getCondominio())) {
			if (imovelVO.getCondominio() == null) {
				imovel.setCondominio(null);
			} else {
				imovel.setCondominio(false);
			}
		} else {
			imovel.setCondominio(true);
			if (imovelVO.getCodigoModalidadeMedicao() == null || imovelVO.getCodigoModalidadeMedicao() <= 0) {
				imovel.setModalidadeMedicaoImovel(null);
			} else {
				ModalidadeMedicaoImovel modalidadeMedicaoImovel =
						controladorImovel.obterModalidadeMedicaoImovel(imovelVO.getCodigoModalidadeMedicao());
				imovel.setModalidadeMedicaoImovel(modalidadeMedicaoImovel);
			}
			if (imovelVO.getIdTipoBotijao() == null || imovelVO.getIdTipoBotijao() <= 0) {
				imovel.setTipoBotijao(null);
			} else {
				TipoBotijao tipoBotijao = controladorImovel.obterTipoBotijao(imovelVO.getIdTipoBotijao());
				imovel.setTipoBotijao(tipoBotijao);
			}
			if (StringUtils.isEmpty(imovelVO.getQuantidadeBloco())) {
				imovel.setQuantidadeBloco(null);
			} else {
				imovel.setQuantidadeBloco(
						Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_BLOCO, imovelVO.getQuantidadeBloco()));
			}
			if (StringUtils.isEmpty(imovelVO.getQuantidadeAndar())) {
				imovel.setQuantidadeAndar(null);
			} else {
				imovel.setQuantidadeAndar(
						Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_ANDAR, imovelVO.getQuantidadeAndar()));
			}
			if (StringUtils.isEmpty(imovelVO.getQuantidadeApartamentoAndar())) {
				imovel.setQuantidadeApartamentoAndar(null);
			} else {
				imovel.setQuantidadeApartamentoAndar(Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_APARTAMENTO_ANDAR,
						imovelVO.getQuantidadeApartamentoAndar()));
			}
			if (imovelVO.getValvulaBloqueio() == null || !Boolean.valueOf(imovelVO.getValvulaBloqueio())) {
				imovel.setValvulaBloqueio(false);
			} else {
				imovel.setValvulaBloqueio(true);
			}
			if (StringUtils.isEmpty(imovelVO.getDataEntrega())) {
				imovel.setDataEntrega(null);
			} else {
				imovel.setDataEntrega(
						Util.converterCampoStringParaData(Imovel.DATA_ENTREGA, imovelVO.getDataEntrega(), Constantes.FORMATO_DATA_BR));
			}
			if (imovelVO.getIdEmpresa() == null || imovelVO.getIdEmpresa() <= 0) {
				imovel.setEmpresa(null);
			} else {
				Empresa empresa = (Empresa) controladorEmpresa.obter(imovelVO.getIdEmpresa());
				imovel.setEmpresa(empresa);
			}
			if (imovelVO.getPortaria() == null || Boolean.valueOf(imovelVO.getPortaria())) {
				imovel.setPortaria(true);
			} else {
				imovel.setPortaria(false);
			}
		}
		if (imovelVO.getRedePreexistente() == null || !Boolean.valueOf(imovelVO.getRedePreexistente())) {
			imovel.setRedePreexistente(false);
			imovel.setRedeInternaImovel(null);
		} else {
			imovel.setRedePreexistente(true);
			popularRedeInternaImovel(request, imovel, imovelVO);
		}

		if (imovelVO.getIndicadorObraTubulacao() != null && !imovelVO.getIndicadorObraTubulacao().isEmpty()) {
			Boolean indicadorObra = Boolean.valueOf(imovelVO.getIndicadorObraTubulacao());
			imovel.setIndicadorObraTubulacao(indicadorObra);
		}

		if (imovelVO.getIdTipoCombustivel() != null && imovelVO.getIdTipoCombustivel() > 0) {
			EntidadeConteudo tipoCombustivel = controladorEntidadeConteudo.obter(imovelVO.getIdTipoCombustivel());
			imovel.setTipoCombustivel(tipoCombustivel);
		}

		popularImovelServicoTipoRestricao(imovel, imovelVO);
	}

	private void popularRedeInternaImovel(HttpServletRequest request, Imovel imovel, ImovelVO imovelVO) throws GGASException {

		Long idRedeMaterial = imovelVO.getIdRedeMaterial();
		String quantidadePrumada = imovelVO.getQuantidadePrumada();
		Long idRedeDiametroCentral = imovelVO.getIdRedeDiametroCentral();
		Long idRedeDiametroPrumada = imovelVO.getIdRedeDiametroPrumada();
		Long idRedeDiametroPrumadaApartamento = imovelVO.getIdRedeDiametroPrumadaApartamento();
		String quantidadeReguladorHall = imovelVO.getQuantidadeReguladorHall();
		String ventilacaoHall = imovelVO.getVentilacaoHall();
		String ventilacaoApartamento = imovelVO.getVentilacaoApartamento();

		RedeInternaImovel redeInternaImovel = null;
		if (imovel.getRedeInternaImovel() == null) {
			redeInternaImovel = (RedeInternaImovel) controladorImovel.criarRedeInternaImovel();
			redeInternaImovel.setChavePrimaria(0);
		} else {
			redeInternaImovel = imovel.getRedeInternaImovel();
		}
		if (idRedeMaterial == null || idRedeMaterial <= 0) {
			redeInternaImovel.setRedeMaterial(null);
		} else {
			RedeMaterial redeMaterial = controladorRede.obterRedeMaterial(idRedeMaterial);
			redeInternaImovel.setRedeMaterial(redeMaterial);
		}
		if (StringUtils.isEmpty(quantidadePrumada)) {
			redeInternaImovel.setQuantidadePrumada(null);
		} else {
			redeInternaImovel.setQuantidadePrumada(Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_PRUMADA, quantidadePrumada));
		}
		if (idRedeDiametroCentral == null || idRedeDiametroCentral <= 0) {
			redeInternaImovel.setRedeDiametroCentral(null);
		} else {
			RedeDiametro redeDiametro = controladorRede.obterRedeDiametro(idRedeDiametroCentral);
			redeInternaImovel.setRedeDiametroCentral(redeDiametro);
		}
		if (idRedeDiametroPrumada == null || idRedeDiametroPrumada <= 0) {
			redeInternaImovel.setRedeDiametroPrumada(null);
		} else {
			RedeDiametro redeDiametro = controladorRede.obterRedeDiametro(idRedeDiametroPrumada);
			redeInternaImovel.setRedeDiametroPrumada(redeDiametro);
		}
		if (idRedeDiametroPrumadaApartamento == null || idRedeDiametroPrumadaApartamento <= 0) {
			redeInternaImovel.setRedeDiametroPrumadaApartamento(null);
		} else {
			RedeDiametro redeDiametro = controladorRede.obterRedeDiametro(idRedeDiametroPrumadaApartamento);
			redeInternaImovel.setRedeDiametroPrumadaApartamento(redeDiametro);
		}
		if (StringUtils.isEmpty(quantidadeReguladorHall)) {
			redeInternaImovel.setQuantidadeReguladorHall(null);
		} else {
			redeInternaImovel.setQuantidadeReguladorHall(
					Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_REGULADOR_HALL, quantidadeReguladorHall));
		}
		if (ventilacaoHall == null || !Boolean.valueOf(ventilacaoHall)) {
			redeInternaImovel.setVentilacaoHall(false);
		} else {
			redeInternaImovel.setVentilacaoHall(true);
		}
		if (ventilacaoApartamento == null || !Boolean.valueOf(ventilacaoApartamento)) {
			redeInternaImovel.setVentilacaoApartamento(false);
		} else {
			redeInternaImovel.setVentilacaoApartamento(true);
		}
		redeInternaImovel.setImovel(imovel);
		redeInternaImovel.setUltimaAlteracao(Calendar.getInstance().getTime());
		redeInternaImovel.setDadosAuditoria(this.getDadosAuditoria(request));
		imovel.setRedeInternaImovel(redeInternaImovel);
	}

	private void popularImovelServicoTipoRestricao(Imovel imovel, ImovelVO imovelVO) {

		Long[] idsServicoTipoRestricao = imovelVO.getIdsServicoTipoRestricao();
		String indicadorRestricaoServico = imovelVO.getIndicadorRestricaoServico();

		List<ImovelServicoTipoRestricao> listaImovelServicoTipo = new ArrayList<ImovelServicoTipoRestricao>();

		if (indicadorRestricaoServico != null && !indicadorRestricaoServico.isEmpty()) {
			Boolean indicadorRestricaoServ = Boolean.valueOf(indicadorRestricaoServico);
			imovel.setIndicadorRestricaoServico(indicadorRestricaoServ);

			if (indicadorRestricaoServ) {
				for (Long idServicoTipo : idsServicoTipoRestricao) {
					ServicoTipo servicoTipo = controladorImovel.consultarServicoTipo(idServicoTipo);
					if (servicoTipo != null) {
						ImovelServicoTipoRestricao imovelServicoTipoRestricao =
								(ImovelServicoTipoRestricao) controladorImovel.criarImovelServicoTipoRestricao();
						imovelServicoTipoRestricao.setImovel(imovel);
						imovelServicoTipoRestricao.setServicoTipo(servicoTipo);

						listaImovelServicoTipo.add(imovelServicoTipoRestricao);
					}
				}

			}
		}

		imovel.setListaImovelServicoTipoRestricao(new HashSet<ImovelServicoTipoRestricao>(listaImovelServicoTipo));
	}

	@SuppressWarnings("unchecked")
	private void popularRelacionamentos(Imovel imovel, HttpServletRequest request) throws GGASException {

		Collection<ClienteImovel> listaClienteImovel = (Collection<ClienteImovel>) request.getSession().getAttribute(LISTA_CLIENTE_IMOVEL);

		if (listaClienteImovel != null && !listaClienteImovel.isEmpty()) {
			imovel.getListaClienteImovel().clear();

			for (ClienteImovel clienteImovel : listaClienteImovel) {
				controladorCliente.obter(clienteImovel.getCliente().getChavePrimaria());
				controladorImovel
						.buscarTipoRelacionamentoClienteImovel(clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria());
				if (clienteImovel.getMotivoFimRelacionamentoClienteImovel() != null) {
					controladorImovel.obterMotivoFimRelacionamentoClienteImovel(
							clienteImovel.getMotivoFimRelacionamentoClienteImovel().getChavePrimaria());
				}
				clienteImovel.setImovel(imovel);
				imovel.getListaClienteImovel().add(clienteImovel);
			}
		} else {
			imovel.getListaClienteImovel().clear();
		}
	}

	@SuppressWarnings("unchecked")
	private void popularContatos(Imovel imovel, HttpServletRequest request) throws GGASException {

		Collection<ContatoImovel> listaContatoImovel = (Collection<ContatoImovel>) request.getSession().getAttribute(LISTA_CONTATO);

		if (listaContatoImovel != null && !listaContatoImovel.isEmpty()) {
			imovel.getContatos().clear();

			for (ContatoImovel contatoImovel : listaContatoImovel) {
				controladorCliente.buscarTipoContato(contatoImovel.getTipoContato().getChavePrimaria());

				contatoImovel.setImovel(imovel);
				imovel.getContatos().add(contatoImovel);
			}
		} else {
			imovel.getContatos().clear();
		}
	}

	private void popularImovelRamoAtividade(ImovelRamoAtividade imovelRamoAtividade, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO)
			throws GGASException {

		Long idSegmento = imovelUnidadeConsumidoraVO.getIdSegmento();
		Long idRamoAtividade = imovelUnidadeConsumidoraVO.getIdRamoAtividade();

		Integer quantidadeEconomia = null;

		if (StringUtils.isEmpty((imovelUnidadeConsumidoraVO.getQuantidadeEconomia()))) {
			imovelRamoAtividade.setQuantidadeEconomia(null);

		} else {
			imovelRamoAtividade.setQuantidadeEconomia(Util.converterCampoStringParaValorInteger(ImovelRamoAtividade.QUANTIDADE_ECONOMIA,
					imovelUnidadeConsumidoraVO.getQuantidadeEconomia()));
			quantidadeEconomia = imovelRamoAtividade.getQuantidadeEconomia();
		}

		controladorImovel.validarDadosImovelRamoAtividade(idSegmento, idRamoAtividade, quantidadeEconomia);

		if (idRamoAtividade == null || idRamoAtividade <= 0) {
			imovelRamoAtividade.setRamoAtividade(null);
		} else {
			RamoAtividade ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(idRamoAtividade);
			imovelRamoAtividade.setRamoAtividade(ramoAtividade);
		}

		if (imovelRamoAtividade.getChavePrimaria() <= 0) {
			imovelRamoAtividade.setChavePrimaria(0);
		}
		imovelRamoAtividade.setHabilitado(true);
		imovelRamoAtividade.setUltimaAlteracao(Calendar.getInstance().getTime());
	}

	private void alterarImovelRamoAtividade(Integer indexLista, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			List<ImovelRamoAtividade> listaUnidadesConsumidoras) throws GGASException {

		if (listaUnidadesConsumidoras != null && indexLista != null) {
			ImovelRamoAtividade imovelRamoAtividadeAlterado = (ImovelRamoAtividade) controladorImovel.criarImovelRamoAtividade();
			this.popularImovelRamoAtividade(imovelRamoAtividadeAlterado, imovelUnidadeConsumidoraVO);
			ImovelRamoAtividade imovelRamoAtividadeExistente = null;
			for (int i = 0; i < listaUnidadesConsumidoras.size(); i++) {
				imovelRamoAtividadeExistente = listaUnidadesConsumidoras.get(i);
				if (i == indexLista && imovelRamoAtividadeExistente != null) {
					validarAdicionarUnidadeConsumidora(imovelRamoAtividadeAlterado, listaUnidadesConsumidoras, indexLista);
					this.popularImovelRamoAtividade(imovelRamoAtividadeExistente, imovelUnidadeConsumidoraVO);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void popularUnidadesConsumidoras(Imovel imovel, HttpServletRequest request) throws GGASException {

		Collection<ImovelRamoAtividade> listaImovelRamoAtividade =
				(Collection<ImovelRamoAtividade>) request.getSession().getAttribute(LISTA_UNIDADE_CONSUMIDORA);

		if (listaImovelRamoAtividade != null && !listaImovelRamoAtividade.isEmpty()) {
			imovel.getUnidadesConsumidoras().clear();

			for (ImovelRamoAtividade imovelRamoAtividade : listaImovelRamoAtividade) {
				controladorRamoAtividade.obter(imovelRamoAtividade.getRamoAtividade().getChavePrimaria());
				imovelRamoAtividade.setImovel(imovel);
				imovel.getUnidadesConsumidoras().add(imovelRamoAtividade);
			}
		} else {
			imovel.getUnidadesConsumidoras().clear();
		}
	}

	@SuppressWarnings("unchecked")
	private void popularImovelPontoConsumo(PontoConsumo pontoConsumo, ImovelVO imovelVO, ImovelPontoConsumoVO imovelPontoConsumoVO,
			HttpServletRequest request) throws GGASException {

		Long idSegmento = imovelPontoConsumoVO.getIdSegmentoPontoConsumo();
		Long idRamoAtividade = imovelPontoConsumoVO.getIdRamoAtividadePontoConsumo();
		Long idModalidadeUso = imovelPontoConsumoVO.getIdModalidadeUso();
		String enderecoReferenciaPC = imovelPontoConsumoVO.getEnderecoReferenciaPontoConsumo();
		String enderecoRemotoPC = imovelPontoConsumoVO.getEnderecoRemotoPontoConsumo();
		String descricaoPontoConsumo = imovelPontoConsumoVO.getDescricaoPontoConsumo();
		String descricaoComplemento = imovelPontoConsumoVO.getDescricaoComplementoPontoConsumo();
		String cep = imovelPontoConsumoVO.getCepPontosConsumo();
		String numeroImovel = imovelPontoConsumoVO.getNumeroImovelPontoConsumo();
		String numeroSequenciaLeitura = imovelPontoConsumoVO.getNumeroSequenciaLeitura();
		Long idQuadraFace = imovelPontoConsumoVO.getIdQuadraFacePontoConsumo();
		Long idSituacaoPontoconsumo = imovelPontoConsumoVO.getIdSituacaoPontoConsumo();
		String latitudeGrau = imovelPontoConsumoVO.getLatitudeGrau();
		String longitudeGrau = imovelPontoConsumoVO.getLongitudeGrau();
		Long idRota = imovelPontoConsumoVO.getIdRota();
		String idPontoConsumoLegado = imovelPontoConsumoVO.getPontoConsumoLegado();
		Long idPontoconsumo = imovelPontoConsumoVO.getIdPontoConsumo();
		String indicadorClassificacaoFiscal = imovelPontoConsumoVO.getIndicadorClassificacaoFiscal();
		String habilitadoPontoConsumo = imovelPontoConsumoVO.getHabilitadoPontoConsumo();
		String observacaoPontoConsumo = imovelPontoConsumoVO.getObservacaoPontoConsumo();
		boolean indicadorMensagemAdicional = imovelPontoConsumoVO.isIndicadorMensagemAdicional();
		String descricaoMensagemAdicional = imovelPontoConsumoVO.getDescricaoMensagemAdicional();

		controladorPontoConsumo.validarPontoConsumoECodigoLegado(idPontoConsumoLegado, idPontoconsumo);

		if (indicadorClassificacaoFiscal != null) {
			pontoConsumo.setIndicadorClassificacaoFiscal(Boolean.valueOf(indicadorClassificacaoFiscal));
		} else {
			pontoConsumo.setIndicadorClassificacaoFiscal(Boolean.FALSE);
		}

		pontoConsumo.setObservacao(observacaoPontoConsumo);

		if (habilitadoPontoConsumo != null) {
			pontoConsumo.setHabilitado(Boolean.valueOf(habilitadoPontoConsumo));
		} else {
			pontoConsumo.setHabilitado(Boolean.TRUE);
		}

		SituacaoConsumo situacaoConsumo = null;
		if (idSituacaoPontoconsumo != null && idSituacaoPontoconsumo > 0L) {
			situacaoConsumo = controladorImovel.obterSituacaoConsumo(idSituacaoPontoconsumo);
		} else {
			situacaoConsumo = controladorImovel.obterSituacaoConsumo(Long.parseLong(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO)));
		}
		pontoConsumo.setSituacaoConsumo(situacaoConsumo);

		if ((idSegmento != null) && (idSegmento > 0)) {
			Segmento segmento =
					(Segmento) controladorSegmento.obter(idSegmento, "segmentoAmostragemPCSs", "segmentoIntervaloPCSs", "ramosAtividades");
			pontoConsumo.setSegmento(segmento);
		}

		if ((idRamoAtividade != null) && (idRamoAtividade > 0)) {
			RamoAtividade ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(idRamoAtividade);
			pontoConsumo.setRamoAtividade(ramoAtividade);
		}

		if ((idModalidadeUso != null) && (idModalidadeUso > 0)) {
			EntidadeConteudo modalidadeUso = controladorEntidadeConteudo.obter(idModalidadeUso);
			pontoConsumo.setModalidadeUso(modalidadeUso);
		} else {
			pontoConsumo.setModalidadeUso(null);
		}

		if ((idRota != null) && (idRota > 0)) {
			Rota rota = (Rota) controladorRota.obter(idRota, PONTOS_CONSUMO);
			int quantidadePontosConsumoRota = 0;
			if (rota != null) {
				pontoConsumo.setRota(rota);
				if (rota.getPontosConsumo() != null || !rota.getPontosConsumo().isEmpty()) {
					quantidadePontosConsumoRota = rota.getPontosConsumo().size();
				}
			}

			int numeroSequencia = 1;
			Collection<PontoConsumo> listaPontoConsumo = null;
			if (request.getSession().getAttribute(LISTA_PONTO_CONSUMO) != null) {
				listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
				numeroSequencia = numeroSequencia + listaPontoConsumo.size() + quantidadePontosConsumoRota;
			}

			if (!StringUtils.isEmpty(numeroSequenciaLeitura)) {
				pontoConsumo
						.setNumeroSequenciaLeitura(Util.converterCampoStringParaValorInteger(PontoConsumo.NUMERO, numeroSequenciaLeitura));
			} else {
				pontoConsumo.setNumeroSequenciaLeitura(numeroSequencia);
			}

		} else {
			pontoConsumo.setRota(null);
		}

		if (!StringUtils.isEmpty(enderecoReferenciaPC)) {
			pontoConsumo.setEnderecoReferencia(enderecoReferenciaPC);
		} else {
			pontoConsumo.setEnderecoReferencia("");
		}

		if (!StringUtils.isEmpty(enderecoRemotoPC)) {
			controladorImovel.validarEnderecoRemoto(enderecoRemotoPC);
			pontoConsumo.setCodigoPontoConsumoSupervisorio(enderecoRemotoPC);
		} else {
			pontoConsumo.setCodigoPontoConsumoSupervisorio("");
		}

		if (!StringUtils.isEmpty(latitudeGrau)) {
			pontoConsumo.setLatitudeGrau(NumeroUtil.converterCampoStringParaValorBigDecimal(Constantes.LAT_GRAU, latitudeGrau,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			pontoConsumo.setLatitudeGrau(null);
		}

		if (!StringUtils.isEmpty(longitudeGrau)) {
			pontoConsumo.setLongitudeGrau(NumeroUtil.converterCampoStringParaValorBigDecimal(Constantes.LONG_GRAU, longitudeGrau,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			pontoConsumo.setLongitudeGrau(null);
		}

		if (!StringUtils.isEmpty(cep)) {
			Cep cepPontoConsumo = controladorEndereco.obterCep(cep);
			pontoConsumo.setCep(cepPontoConsumo);
		}

		if (!StringUtils.isEmpty(descricaoPontoConsumo)) {
			if (descricaoPontoConsumo.length() > LIMITE_CAMPO_DESCRICAO) {
				pontoConsumo.setDescricao(descricaoPontoConsumo.substring(0, LIMITE_CAMPO_DESCRICAO));
			} else {
				pontoConsumo.setDescricao(descricaoPontoConsumo);
			}
		}

		if (!StringUtils.isEmpty(descricaoComplemento)) {
			pontoConsumo.setDescricaoComplemento(descricaoComplemento);
		} else {
			pontoConsumo.setDescricaoComplemento("");
		}

		if (!StringUtils.isEmpty(numeroImovel)) {
			if (!Constantes.SEM_NUMERO.equals(numeroImovel)) {
				Util.validarDominioCaracteres(PontoConsumo.NUMERO, numeroImovel, Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			}
			pontoConsumo.setNumeroImovel(numeroImovel);

		} else {
			pontoConsumo.setNumeroImovel(numeroImovel);
		}

		if (pontoConsumo.getChavePrimaria() <= 0) {
			pontoConsumo.setChavePrimaria(0);
		}

		pontoConsumo.setCodigoLegado(idPontoConsumoLegado);

		if ((idQuadraFace != null) && (idQuadraFace > 0)) {
			QuadraFace quadraFace = controladorQuadra.obterQuadraFace(idQuadraFace);
			pontoConsumo.setQuadraFace(quadraFace);
		} else {
			request.getSession().removeAttribute(LISTA_CITY_GATE);
		}

		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
		if (listaPontoConsumoTributoAliquota != null) {
			popularImovelPontoConsumoTributo(pontoConsumo, listaPontoConsumoTributoAliquota);
		} else {
			pontoConsumo.getListaPontoConsumoTributoAliquota().clear();
		}
		request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);
		if (listaPontoConsumoEquipamento != null) {
			popularImovelPontoEquipamento(pontoConsumo, listaPontoConsumoEquipamento);
		} else {
			pontoConsumo.getListaPontoConsumoEquipamento().clear();
		}
		request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		Collection<CityGate> listaCityGate = (Collection<CityGate>) request.getSession().getAttribute(LISTA_CITY_GATE);
		if ((listaCityGate != null) && !(listaCityGate.isEmpty())) {
			Collection<PontoConsumoCityGate> listaPtoConsumoCityGate = new ArrayList<PontoConsumoCityGate>();
			listaPtoConsumoCityGate.addAll(pontoConsumo.getListaPontoConsumoCityGate());
			pontoConsumo.getListaPontoConsumoCityGate().clear();
			request.setAttribute(SIZE_LISTA_CITY_GATE, listaCityGate.size());
			String[] percentualCityGate = imovelVO.getPercentualCityGate();
			Date dataInicioVigenciaCityGate =
					Util.converterCampoStringParaData("Data", imovelVO.getDataInicioVigenciaCityGate(), Constantes.FORMATO_DATA_BR);
			controladorPontoConsumo.validarPercentualParticipacao(percentualCityGate);
			int cont = 0;
			for (CityGate cityGate : listaCityGate) {
				PontoConsumoCityGate pontoConsumoCityGate = (PontoConsumoCityGate) controladorPontoConsumo.criarPontoConsumoCityGate();
				pontoConsumoCityGate.setPontoConsumo(pontoConsumo);
				pontoConsumoCityGate.setCityGate(cityGate);
				if (percentualCityGate.length > 0) {
					pontoConsumoCityGate.setPercentualParticipacao(Util.converterCampoStringParaValorBigDecimal("Percentual Participação",
							percentualCityGate[cont], Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
				}
				pontoConsumoCityGate.setDataVigencia(dataInicioVigenciaCityGate);
				pontoConsumoCityGate.setUltimaAlteracao(Calendar.getInstance().getTime());
				pontoConsumo.getListaPontoConsumoCityGate().add(pontoConsumoCityGate);
				for (PontoConsumoCityGate ptoConsumoCityGate : listaPtoConsumoCityGate) {
					if (Util.compararDatas(ptoConsumoCityGate.getDataVigencia(), pontoConsumoCityGate.getDataVigencia()) != 0) {
						pontoConsumo.getListaPontoConsumoCityGate().add(ptoConsumoCityGate);
					}
				}
				cont++;
			}
			request.getSession().setAttribute(LISTA_CITY_GATE, listaCityGate);
		} else {
			pontoConsumo.getListaPontoConsumoCityGate().clear();
		}

		pontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
		pontoConsumo.setDescricaoMensagemAdicional(descricaoMensagemAdicional);
		pontoConsumo.setIndicadorMensagemAdicional(indicadorMensagemAdicional);
	}

	private void popularImovelPontoEquipamento(PontoConsumo pontoConsumo, Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento)
			throws GGASException {
		pontoConsumo.getListaPontoConsumoEquipamento().clear();
		for (PontoConsumoEquipamento pcEquipamento : listaPontoConsumoEquipamento) {
			PontoConsumoEquipamento pontoConsumoEquipamento = (PontoConsumoEquipamento) controladorImovel.criarPontoConsumoEquipamento();

			if (pontoConsumo.getSegmento() != null) {
				controladorImovel.validarPontoConsumoEEquipamento(pontoConsumo, pcEquipamento);
			}

			pontoConsumoEquipamento.setChavePrimaria(pcEquipamento.getChavePrimaria());
			pontoConsumoEquipamento.setPontoConsumo(pontoConsumo);
			pontoConsumoEquipamento.setEquipamento(pcEquipamento.getEquipamento());
			pontoConsumoEquipamento.setPotencia(pcEquipamento.getPotencia());
			pontoConsumoEquipamento.setHorasPorDia(pcEquipamento.getHorasPorDia());
			pontoConsumoEquipamento.setDiasPorSemana(pcEquipamento.getDiasPorSemana());
			pontoConsumoEquipamento.setPotenciaFixa(pcEquipamento.getPotenciaFixa());

			pontoConsumoEquipamento.setHabilitado(Boolean.TRUE);
			pontoConsumoEquipamento.setUltimaAlteracao(Calendar.getInstance().getTime());
			pontoConsumo.getListaPontoConsumoEquipamento().add(pontoConsumoEquipamento);
		}
	}

	private void popularImovelPontoConsumoTributo(PontoConsumo pontoConsumo,
			Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota) {
		pontoConsumo.getListaPontoConsumoTributoAliquota().clear();
		for (PontoConsumoTributoAliquota tributoAliquota : listaPontoConsumoTributoAliquota) {
			PontoConsumoTributoAliquota pontoConsumoTributoAliquota =
					(PontoConsumoTributoAliquota) controladorImovel.criarPontoConsumoTributoAliquota();

			pontoConsumoTributoAliquota.setChavePrimaria(tributoAliquota.getChavePrimaria());
			pontoConsumoTributoAliquota.setPontoConsumo(pontoConsumo);
			pontoConsumoTributoAliquota.setTributo(tributoAliquota.getTributo());
			pontoConsumoTributoAliquota.setIndicadorIsencao(tributoAliquota.getIndicadorIsencao());
			pontoConsumoTributoAliquota.setDestaqueIndicadorNota(tributoAliquota.getDestaqueIndicadorNota());
			pontoConsumoTributoAliquota.setIndicadorCreditoIcms(tributoAliquota.getIndicadorCreditoIcms());

			if (tributoAliquota.getPorcentagemAliquota() != null) {
				pontoConsumoTributoAliquota.setPorcentagemAliquota(tributoAliquota.getPorcentagemAliquota());
			} else {
				pontoConsumoTributoAliquota.setPorcentagemAliquota(null);
			}

			if (tributoAliquota.getDataInicioVigencia() != null) {
				pontoConsumoTributoAliquota.setDataInicioVigencia(tributoAliquota.getDataInicioVigencia());
			} else {
				pontoConsumoTributoAliquota.setDataInicioVigencia(null);
			}

			if (tributoAliquota.getDataFimVigencia() != null) {
				pontoConsumoTributoAliquota.setDataFimVigencia(tributoAliquota.getDataFimVigencia());
			} else {
				pontoConsumoTributoAliquota.setDataFimVigencia(null);
			}

			if (tributoAliquota.getIndicadorIcmsSubstitutoTarifa() != null) {
				pontoConsumoTributoAliquota.setIndicadorIcmsSubstitutoTarifa(tributoAliquota.getIndicadorIcmsSubstitutoTarifa());
			} else {
				pontoConsumoTributoAliquota.setIndicadorIcmsSubstitutoTarifa(null);
			}

			if (tributoAliquota.getIndicadorIcmsSubstitutoTarifa() != null) {
				pontoConsumoTributoAliquota.setIndicadorDrawback(tributoAliquota.getIndicadorDrawback());
			} else {
				pontoConsumoTributoAliquota.setIndicadorDrawback(null);
			}

			if (tributoAliquota.getIndicadorDrawback() != null) {
				pontoConsumoTributoAliquota.setIndicadorDrawback(tributoAliquota.getIndicadorDrawback());
			} else {
				pontoConsumoTributoAliquota.setIndicadorDrawback(null);
			}

			if (tributoAliquota.getValorDrawback() != null) {
				pontoConsumoTributoAliquota.setValorDrawback(tributoAliquota.getValorDrawback());
			} else {
				pontoConsumoTributoAliquota.setValorDrawback(null);
			}

			pontoConsumoTributoAliquota.setHabilitado(Boolean.TRUE);
			pontoConsumoTributoAliquota.setUltimaAlteracao(Calendar.getInstance().getTime());
			pontoConsumo.getListaPontoConsumoTributoAliquota().add(pontoConsumoTributoAliquota);
		}
	}

	private void popularImovelPontoConsumoTributo(PontoConsumo pontoConsumo, PontoConsumoTributoAliquota pontoConsumoTributoAliquota,
			ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		Long idPontoConsumoTributo = imovelPontoConsumoVO.getIdPontoConsumoTributo();
		String indicadorIsento = imovelPontoConsumoVO.getIndicadorIsento();
		String indicadorDestaqueNota = imovelPontoConsumoVO.getIndicadorDestaqueNota();

		String percentualAliquota = imovelPontoConsumoVO.getPercentualAliquota();
		String dataInicioVigencia = imovelPontoConsumoVO.getDataInicioVigencia();
		String dataFimVigencia = imovelPontoConsumoVO.getDataFimVigencia();
		String creditoIcms = imovelPontoConsumoVO.getCreditoIcms();
		String indicadorIcmsSubstitutoTarifa = imovelPontoConsumoVO.getIndicadorIcmsSubstitutoTarifa();

		String indicadorDrawback = imovelPontoConsumoVO.getIndicadorDrawback();
		String valorDrawback = imovelPontoConsumoVO.getValorDrawback();

		pontoConsumoTributoAliquota.setPontoConsumo(pontoConsumo);

		if (idPontoConsumoTributo != null && idPontoConsumoTributo > 0) {
			Tributo tributo = controladorTributo.obterTributo(idPontoConsumoTributo);
			pontoConsumoTributoAliquota.setTributo(tributo);
		} else {
			pontoConsumoTributoAliquota.setTributo(null);
		}

		if (indicadorIsento != null) {
			pontoConsumoTributoAliquota.setIndicadorIsencao(Boolean.valueOf(indicadorIsento));
		}

		if (indicadorDestaqueNota != null) {
			pontoConsumoTributoAliquota.setDestaqueIndicadorNota(Boolean.valueOf(indicadorDestaqueNota));
			if (!StringUtils.isEmpty(creditoIcms)) {
				pontoConsumoTributoAliquota.setIndicadorCreditoIcms(Boolean.valueOf(creditoIcms));
			}
		} else {
			pontoConsumoTributoAliquota.setDestaqueIndicadorNota(Boolean.FALSE);
		}

		if (!StringUtils.isEmpty(percentualAliquota)) {
			BigDecimal percentualAliquotaAux = Util.converterCampoStringParaValorBigDecimal("Percentual Alíquota", percentualAliquota,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			pontoConsumoTributoAliquota.setPorcentagemAliquota(Util.converterBigDecimalParaPercentual(percentualAliquotaAux));
		} else {
			pontoConsumoTributoAliquota.setPorcentagemAliquota(null);
		}

		if (dataInicioVigencia != null && !dataInicioVigencia.isEmpty()) {
			pontoConsumoTributoAliquota.setDataInicioVigencia(
					Util.converterCampoStringParaData("Data de Ínicio da Vigência", dataInicioVigencia, Constantes.FORMATO_DATA_BR));
		} else {
			pontoConsumoTributoAliquota.setDataInicioVigencia(null);
		}

		if (dataFimVigencia != null && !dataFimVigencia.isEmpty()) {
			pontoConsumoTributoAliquota.setDataFimVigencia(
					Util.converterCampoStringParaData("Data de Fim da Vigência", dataFimVigencia, Constantes.FORMATO_DATA_BR));
		} else {
			pontoConsumoTributoAliquota.setDataFimVigencia(null);
		}

		if (indicadorIcmsSubstitutoTarifa != null && !StringUtils.isEmpty(indicadorIcmsSubstitutoTarifa)) {
			pontoConsumoTributoAliquota.setIndicadorIcmsSubstitutoTarifa(Boolean.valueOf(indicadorIcmsSubstitutoTarifa));
		}

		if (indicadorDrawback != null && !StringUtils.isEmpty(indicadorDrawback)) {
			pontoConsumoTributoAliquota.setIndicadorDrawback(Boolean.valueOf(indicadorDrawback));
		}

		if (!StringUtils.isEmpty(valorDrawback)) {
			pontoConsumoTributoAliquota.setValorDrawback(Util.converterCampoStringParaValorBigDecimal("Valor Drawback", valorDrawback,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		} else {
			pontoConsumoTributoAliquota.setValorDrawback(null);
		}
	}

	private void atualizarModelo(Model model, ImovelVO imovelVO, ImovelContatoVO imovelContatoVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO,
			ImovelPontoConsumoVO imovelPontoConsumoVO) {

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_CONTATO_VO, imovelContatoVO);
		model.addAttribute(IMOVEL_RELACIONAMENTO_VO, imovelRelacionamentoVO);
		model.addAttribute(IMOVEL_UNIDADE_CONSUMIDORA_VO, imovelUnidadeConsumidoraVO);
		model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);
	}

	private void exibirDadosAbaPontoConsumo(ImovelVO imovelVO, ImovelPontoConsumoVO imovelPontoConsumoVO, Model model,
			HttpServletRequest request) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();
		Long idQuadraPontoConsumo = imovelPontoConsumoVO.getIdQuadraPontoConsumo();
		Long idQuadraImovel = imovelVO.getIdQuadraImovel();

		String cep = imovelPontoConsumoVO.getCepPontosConsumo();
		if (cep != null && !cep.isEmpty()) {
			request.setAttribute("listaQuadraPontoConsumo", controladorQuadra.listarQuadraPorCep(cep));
		}

		if ((idQuadraPontoConsumo != null) && (idQuadraPontoConsumo >= 0)) {
			Collection<Rota> rotas = controladorRota.listarRotasPorSetorComercialDaQuadra(idQuadraPontoConsumo);
			request.setAttribute("listaRotaQuadraPontoConsumo", rotas);
		}

		if (idQuadraImovel != null && idQuadraImovel >= 0) {
			Collection<Rota> rotasPrevistasImovel = controladorRota.listarRotasPorSetorComercialDaQuadra(idQuadraImovel);
			request.setAttribute("listaRotaPrevista", rotasPrevistasImovel);
		}

		if ((idQuadraPontoConsumo != null) && (idQuadraPontoConsumo >= 0)) {
			Quadra quadra = (Quadra) controladorQuadra.obter(idQuadraPontoConsumo, "quadrasFace");
			request.setAttribute("listaFacesQuadraPontoConsumo", quadra.getQuadrasFace());
		}

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota = new ArrayList<PontoConsumoTributoAliquota>();
		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento = new ArrayList<PontoConsumoEquipamento>();
		Collection<CityGate> listaCityGate = null;

		if (!isPostBack(request)) {
			if (chavePrimaria != null && chavePrimaria != 0) {
				listaPontoConsumo.addAll(controladorImovel.listarPontoConsumoPorChaveImovelAtivosInativos(chavePrimaria));
			}
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		} else {
			listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
			listaPontoConsumoTributoAliquota =
					(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
			listaPontoConsumoEquipamento =
					(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		}
		listaCityGate = (Collection<CityGate>) request.getSession().getAttribute(LISTA_CITY_GATE);
		Long idSegmento = imovelPontoConsumoVO.getIdSegmentoPontoConsumo();
		if (idSegmento != null && idSegmento > 0) {
			model.addAttribute(LISTA_RAMO_ATIVIDADE_PONTO_CONSUMO, controladorRamoAtividade.listarRamoAtividadePorSegmento(idSegmento));
		}

		request.setAttribute(LISTA_CITY_GATE, listaCityGate);
		request.setAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		request.setAttribute(LISTA_MODALIDADE_USO, controladorImovel.listarModalidadeUso());
		request.setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);

		request.setAttribute(LISTA_POTENCIAS_FIXAS, controladorEquipamento.listarPotenciasFixas());
		request.setAttribute(LISTA_SERVICO_TIPO, controladorImovel.consultarServicoTipoOrdenados());

		request.setAttribute(POTENCIA_ALTA,
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_ALTA).getValorLong());

		request.setAttribute(POTENCIA_MEDIA,
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_MEDIA).getValorLong());

		request.setAttribute(POTENCIA_BAIXA,
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_BAIXA).getValorLong());

		if (chavePrimaria != null && chavePrimaria != 0) {
			request.setAttribute("exigeSequenciaLeitura", controladorImovel.exigeSequenciaLeituraAlteracaoPontoConsumo(chavePrimaria));
		}

		this.carregarParametroSistemaRotaObrigatoria(model);

		request.setAttribute(LISTA_TRIBUTO, controladorTributo.consultarTributos(null));
	}

	private String carregarParametroSistemaRotaObrigatoria(Model model) throws GGASException {

		String indicadorRotaObrigatoria = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA);
		model.addAttribute("indicadorRotaObrigatoria", indicadorRotaObrigatoria);

		return indicadorRotaObrigatoria;
	}

	/**
	 * Método responsável por exibir a tela de pesquisar imóvel.
	 *
	 * @param imovel - {@link ImovelPesquisaVO}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param idGrupoFaturamento - {@link Long}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping(VIEW_EXIBIR_PESQUISA_IMOVEL)
	public String exibirPesquisaImovel(ImovelPesquisaVO imovel, BindingResult result, HttpServletRequest request,
			@RequestParam(value = ID_GRUPO_FATURAMENTO, required = false) Long idGrupoFaturamento, Model model) throws GGASException {

		// As informações de grupo e rota serão
		// usadas quando a tela de pesquisa tiver
		// filtro por esses campos.
		request.setAttribute("listaGrupoFaturamento", controladorRota.listarGruposFaturamentoRotas());
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			model.addAttribute("listaRota", controladorRota.listarRotasPorGrupoFaturamento(idGrupoFaturamento));
		}

		if (!model.containsAttribute(ATRIBUTO_REQUEST_IMOVEL)) {
			imovel.setHabilitado(true);
			model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, imovel);
		}

		request.getSession().removeAttribute(CHAVES_PRIMARIAS);
		request.getSession().removeAttribute(LISTA_IMOVEIS_FILHOS_ALTERADOS);
		request.getSession().removeAttribute(LISTA_IMOVEL_FILHO_FACTIVEL);
		request.getSession().removeAttribute(LISTA_IMOVEL_FILHO_POTENCIAL);
		request.getSession().removeAttribute(LISTA_IMOVEL);

		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute("listaSituacaoPontoConsumo", Fachada.getInstancia().listarSituacoesPontoConsumo());

		return VIEW_EXIBIR_PESQUISA_IMOVEL;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Imóvel.
	 *
	 * @param imovel - {@link ImovelPesquisaVO}
	 * @param result - {@link BindingResult}
	 * @param idGrupoFaturamento - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("pesquisarImovel")
	public String pesquisarImovel(ImovelPesquisaVO imovel, BindingResult result,
			@RequestParam(value = ID_GRUPO_FATURAMENTO, required = false) Long idGrupoFaturamento, HttpServletRequest request, Model model)
			throws GGASException {
		try {

			Map<String, Object> filtro = new HashMap<String, Object>();
			model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, imovel);

			verificaDadosCliente(imovel, model);
			this.prepararFiltro(filtro, imovel);
			super.adicionarFiltroPaginacao(request, filtro);

			Collection<Imovel> list = controladorImovel.consultarImoveis(filtro);

			for (Imovel imovelObj : list) {
				registrarLock(request, ImovelResultadoPesquisaDecorator.LOCK_IMOVEL,
						MensagemUtil.obterMensagem(ERRO_INTEGRACAO_NAO_PROCESSADO_CONTRATO_IMOVEL),
						imovelObj.getChavePrimaria());

				boolean integracaoPrecedenteAtiva = controladorIntegracao.integracaoPrecedenteContratoAtiva();
				boolean integracaoContratoImovelValido =
						controladorIntegracao.validarIntegracaoContratoImovel(imovelObj.getChavePrimaria());
				if (!integracaoPrecedenteAtiva || (integracaoPrecedenteAtiva && integracaoContratoImovelValido)){
					removerLock(request, ImovelResultadoPesquisaDecorator.LOCK_IMOVEL, imovelObj.getChavePrimaria());
				}
				
				montarInformacoesPontoConsumo(imovelObj);
			}

			model.addAttribute(LISTA_IMOVEIS, super.criarColecaoPaginada(request, filtro, list));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaImovel(imovel, result, request, idGrupoFaturamento, model);
	}

	private void montarInformacoesPontoConsumo(Imovel imovelObj) {

		if (imovelObj.getListaPontoConsumo() != null && !imovelObj.getListaPontoConsumo().isEmpty()
				&& !imovelObj.getCondominio()) {
			PontoConsumo pontoConsumo = imovelObj.getListaPontoConsumo().iterator().next();

			imovelObj.setSituacaoPontoConsumo(pontoConsumo.getSituacaoConsumo().getDescricao());

		}

	}

	/**
	 * Método responsável por excluir os imóveis selecionados.
	 *
	 * @param chavesPrimarias
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerImovel")
	public String removerImovel(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			for (Long chave : chavesPrimarias) {
				if (controladorIntegracao.integracaoPrecedenteContratoAtiva() && !controladorIntegracao.validarIntegracaoContratoImovel(chave)) {
					throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
				}
			}
			controladorImovel.validarRemoverImoveis(chavesPrimarias);
			controladorImovel.removerImovel(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Imovel.IMOVEIS);
		} catch (NegocioException e) {
			//LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, e));
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarImovel";
	}

	/**
	 * Exibir detalhamento imovel.
	 *
	 * @param imovelTmp - {@link ImovelPesquisaVO}
	 * @param result - {@link BindingResult}
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoImovel")
	public String exibirDetalhamentoImovel(ImovelPesquisaVO imovelTmp, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoImovel";
		String latitude = "";
		String longitude = "";
		request.setAttribute(ATRIBUTO_REQUEST_IMOVEL, imovelTmp);
		model.addAttribute("imovelPesquisa", imovelTmp);
		try {
			ImovelImpl imovel = (ImovelImpl) controladorImovel.obter(chavePrimaria, QUADRA_FACE, QUADRA_FACE_ENDERECO,
					QUADRA_FACE_ENDERECO_CEP, QUADRA);

			this.popularForm(model, imovel);

			model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, imovel);

			model.addAttribute("enderecoFormatado", imovel.getEnderecoFormatado());
			model.addAttribute("descricaoSetor", controladorImovel.obterQuadraPorFaceQuadra(imovel.getQuadraFace().getChavePrimaria())
					.getSetorComercial().getDescricao());
			model.addAttribute("numeroQuadra", String
					.valueOf(controladorImovel.obterQuadraPorFaceQuadra(imovel.getQuadraFace().getChavePrimaria()).getNumeroQuadra()));
			if (imovel.getCondominio()) {
				Collection<Imovel> obterImoveisFilhosDoCondominio = controladorImovel.obterImoveisFilhosDoCondominio(imovel);
				request.setAttribute("imoveisAssociados", obterImoveisFilhosDoCondominio);
				model.addAttribute(QTD_IMOVEIS, obterImoveisFilhosDoCondominio.size());
			}
			
			request.setAttribute(ATRIBUTO_REQUEST_POSSUI_REDE_INTERNA, imovel.getRedePreexistente());

			Collection<ClienteImovel> listarClienteImovelPorChaveImovelAux = new ArrayList<ClienteImovel>();
			boolean aux = false;
			for (ClienteImovel clienteImovel : controladorImovel.listarCliImovelPorChaveImovel(imovel.getChavePrimaria())) {
				if (clienteImovel.getCliente().getEnderecoPrincipal() != null
						&& clienteImovel.getCliente().getEnderecoPrincipal().getCorrespondencia()) {
					listarClienteImovelPorChaveImovelAux.add(clienteImovel);
					aux = true;
				}
				if (!aux) {
					listarClienteImovelPorChaveImovelAux.add(clienteImovel);
				}
				aux = false;
			}

			Collection<ContatoImovel> listaContato = controladorImovel.listarContatoImovelPorChaveImovel(imovel.getChavePrimaria());
			Collections.sort((List<ContatoImovel>) listaContato, new ContatoImovelComparator());

			model.addAttribute("relacionamentosCliente", listarClienteImovelPorChaveImovelAux);
			model.addAttribute("contatos", listaContato);
			model.addAttribute("unidadesConsumidoras", controladorImovel.listarUnidadeConsumidoraPorChaveImovel(imovel.getChavePrimaria()));
			Collection<PontoConsumo> listaPontoConsumo =
					controladorImovel.listarPontoConsumoPorChaveImovelAtivosInativos(imovel.getChavePrimaria());
			
			model.addAttribute("pontosDeConsumo", listaPontoConsumo);
			
			
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				PontoConsumo ponto = listaPontoConsumo.iterator().next();

				if (ponto.getLatitudeGrau() != null && ponto.getLongitudeGrau() != null) {
					latitude = ponto.getLatitudeGrau().toString();
					longitude = ponto.getLongitudeGrau().toString();
				}
			}
			
			model.addAttribute("latitude", latitude);
			model.addAttribute("longitude", longitude);
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_PESQUISAR_IMOVEL;
		}

		return view;
	}

	/**
	 * Método responsável por manter os dados do filtro de pesquisa de imóvel.
	 *
	 * @param imovelTmp - {@link ImovelPesquisaVO}
	 * @param result - {@link BindingResult}
	 * @param idGrupoFaturamento - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("voltarImovel")
	public String voltar(ImovelPesquisaVO imovelTmp, BindingResult result,
			@RequestParam(value = ID_GRUPO_FATURAMENTO, required = false) Long idGrupoFaturamento, HttpServletRequest request, Model model)
			throws GGASException {
		ImovelPesquisaVO imovel = imovelTmp;

		if (request.getSession().getAttribute("imovelPesquisaSessao") != null) {
			imovel = (ImovelPesquisaVO) request.getSession().getAttribute("imovelPesquisaSessao");
			request.getSession().removeAttribute("imovelPesquisaSessao");
		}

		return pesquisarImovel(imovel, result, idGrupoFaturamento, request, model);
	}

	private void popularForm(Model model, Imovel imovel) {

		if (Boolean.TRUE.equals(imovel.getValvulaBloqueio())) {
			model.addAttribute("existeValvulaBloqueio", "Sim");
		} else {
			model.addAttribute("existeValvulaBloqueio", "Não");
		}

		if (Boolean.TRUE.equals(imovel.getPortaria())) {
			model.addAttribute("possuiPortaria", "Sim");
		} else {
			model.addAttribute("possuiPortaria", "Não");
		}

		if (imovel.getRedeInternaImovel() != null && Boolean.TRUE.equals(imovel.getRedeInternaImovel().getVentilacaoHall())) {
			model.addAttribute("possuiVentilacaoHall", "Sim");
		} else {
			model.addAttribute("possuiVentilacaoHall", "Não");
		}

		if (imovel.getRedeInternaImovel() != null && Boolean.TRUE.equals(imovel.getRedeInternaImovel().getVentilacaoApartamento())) {
			model.addAttribute("possuiVentilacaoApartamento", "Sim");
		} else {
			model.addAttribute("possuiVentilacaoApartamento", "Não");
		}

		if (imovel.getDataEntrega() != null) {
			model.addAttribute("dataEntrega",
					Util.converterDataParaStringSemHora(imovel.getDataEntrega(), Constantes.FORMATO_DATA_HORA_BR));
		}

		if (imovel.getDataPrevisaoEncerramentoObra() != null) {
			String dataPrevisaoEncerramento =
					Util.converterDataParaStringSemHora(imovel.getDataPrevisaoEncerramentoObra(), Constantes.FORMATO_DATA_HORA_BR);
			model.addAttribute(DATA_PREVISAO_ENCERRAMENTO_OBRA, dataPrevisaoEncerramento);
		}
	}

	/**
	 * @param imovel
	 * @param model
	 */
	private void verificaDadosCliente(ImovelPesquisaVO imovel, Model model) {
		if (imovel.getCliente() != null) {
			if (imovel.getCliente().getCnpj() != null) {
				model.addAttribute(DOCUMENTO_FORMATADO, imovel.getCliente().getCnpjFormatado());
			} else {
				model.addAttribute(DOCUMENTO_FORMATADO, imovel.getCliente().getCpfFormatado());
			}
			if (imovel.getCliente().getEnderecoPrincipal() != null) {
				model.addAttribute("enderecoFormatadoCliente", imovel.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
			}
		}
	}

	/**
	 * Método responsável por preparar o filtro relacionado a pesquisa
	 *
	 * @param filtro -{@link Map}
	 * @param imovel - {@link ImovelPesquisaVO}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, ImovelPesquisaVO imovel) throws FormatoInvalidoException {

		if (imovel.getCliente() != null && imovel.getCliente().getChavePrimaria() > 0) {
			filtro.put(ID_CLIENTE, imovel.getCliente().getChavePrimaria());
		}

		if (!StringUtils.isEmpty(imovel.getMatriculaImovel())) {
			filtro.put(CHAVE_PRIMARIA, Util.converterCampoStringParaValorLong(Imovel.MATRICULA, imovel.getMatriculaImovel()));
		}

		if (!StringUtils.isEmpty(imovel.getMatriculaCondominio())) {
			filtro.put(MATRICULA_CONDOMINIO,
					Util.converterCampoStringParaValorLong(Imovel.MATRICULA_CONDOMINIO, imovel.getMatriculaCondominio()));
		}

		if (!StringUtils.isEmpty(imovel.getNomeMedicao())) {
			filtro.put(NOME_MEDICAO, Util.converterCampoStringParaValorLong(Imovel.NOME_MEDICAO, imovel.getNomeMedicao()));
		}

		if (!StringUtils.isEmpty(imovel.getCepImovel())) {
			filtro.put(CEP_IMOVEL, imovel.getCepImovel());
		}
		if (!StringUtils.isEmpty(imovel.getIdCepImovel())) {
			filtro.put(ID_CEP_IMOVEL, imovel.getIdCepImovel());
		}

		filtro.put(INDICADOR_CONDOMINIO, imovel.getIndicadorCondominio());

		filtro.put(TIPO_MEDICAO, imovel.getTipoMedicao());

		if (!StringUtils.isEmpty(imovel.getIndicadorCondominioAmbos())) {
			filtro.put(INDICADOR_CONDOMINIO_AMBOS, imovel.getIndicadorCondominioAmbos());
		}

		if (!StringUtils.isEmpty(imovel.getTipoMedicaoAmbos())) {
			filtro.put(TIPO_MEDICAO_AMBOS, imovel.getTipoMedicaoAmbos());
		}

		if (!StringUtils.isEmpty(imovel.getNome())) {
			filtro.put(NOME, imovel.getNome());
		}

		if (!StringUtils.isEmpty(imovel.getDescricaoPontoConsumo())) {
			filtro.put(DESCRICAO_PONTO_CONSUMO, imovel.getDescricaoPontoConsumo());
		}

		if (!StringUtils.isEmpty(imovel.getComplementoImovel())) {
			filtro.put(COMPLEMENTO_IMOVEL, imovel.getComplementoImovel());
		}

		if (!StringUtils.isEmpty(imovel.getNumeroImovel())) {
			filtro.put(NUMERO_IMOVEL, imovel.getNumeroImovel());
		}

		if (imovel.getHabilitado() != null) {
			filtro.put(HABILITADO, imovel.getHabilitado());
		}

		if (imovel.getIdSegmento() != null && imovel.getIdSegmento() > 0) {
			filtro.put(ID_SEGMENTO, imovel.getIdSegmento());
		}

		if (imovel.getIdGrupoFaturamento() != null && imovel.getIdGrupoFaturamento() > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, imovel.getIdGrupoFaturamento());
		}

		if (imovel.getRota() != null && imovel.getRota() > 0) {
			filtro.put(ID_ROTA, imovel.getRota());
		}

		if (!StringUtils.isEmpty(imovel.getPontoConsumoLegado()) && imovel.getPontoConsumoLegado() != null) {
			filtro.put(PONTO_CONSUMO_LEGADO, String.valueOf(imovel.getPontoConsumoLegado()));
		}

		if (imovel.getIdSegmentoPontoConsumo() != null && imovel.getIdSegmentoPontoConsumo() > 0) {
			filtro.put(ID_SEGMENTO_PONTO_CONSUMO, imovel.getIdSegmentoPontoConsumo());
		}

		if (imovel.getIdRamoAtividadePontoConsumo() != null && imovel.getIdRamoAtividadePontoConsumo() > 0) {
			filtro.put(ID_RAMO_ATIVIDADE_PONTO_CONSUMO, imovel.getIdRamoAtividadePontoConsumo());
		}
		
		if (imovel.getIdSituacaoPontoConsumo() != null && imovel.getIdSituacaoPontoConsumo() > 0) {
			filtro.put("idSituacaoPontoConsumo", imovel.getIdSituacaoPontoConsumo());
		}
	}

	/**
	 * Ordenar ponto consumo.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("ordenarPontoConsumo")
	@SuppressWarnings({ "unchecked" })
	public String ordenarPontoConsumo(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult)
			throws GGASException {

		request.setAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);

		Collection<PontoConsumo> listaPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO);
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota =
				(Collection<PontoConsumoTributoAliquota>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);

		Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento =
				(Collection<PontoConsumoEquipamento>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO);

		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_TRIBUTO_ALIQUOTA, listaPontoConsumoTributoAliquota);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_EQUIPAMENTO, listaPontoConsumoEquipamento);
		request.setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		return VIEW_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Método responsável por exibir a tela de inclusão em lote de imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping(VIEW_EXIBIR_INCLUSAO_EM_LOTE)
	public String exibirInclusaoEmLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();

		Imovel imovel =
				(Imovel) controladorImovel.obter(chavePrimaria, QUADRA_FACE, QUADRA_FACE_ENDERECO, QUADRA_FACE_ENDERECO_CEP, QUADRA);

		model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, imovel);

		this.popularForm(imovelVO, imovel, model);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);

		if (imovel.getQuantidadeBloco() != null) {
			String quantidadeBloco = String.valueOf(controladorImovel.obterQtdBlocosRestantesImoveisEmLote(imovel));
			imovelVO.setQuantidadeBloco(quantidadeBloco);
			Integer blocosRestantes = Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_BLOCO, quantidadeBloco);
			if (blocosRestantes > 0) {
				model.addAttribute(ATRIBUTO_REQUEST_LISTA_BLOCOS, new String[blocosRestantes]);
			}

			if (imovel.getQuantidadeApartamentoAndar() != null && imovel.getQuantidadeAndar() != null) {
				model.addAttribute(QTD_IMOVEIS, imovel.getQuantidadeApartamentoAndar() * imovel.getQuantidadeAndar() * blocosRestantes);
			}
		}

		saveToken(request);

		ConstanteSistema constanteSistema = Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_TIPO_MODALIDADE_USO);

		Collection<EntidadeConteudo> modalidadeUsoList = Fachada.getInstancia().listarEntidadeConteudo(constanteSistema.getClasse());

		request.setAttribute(LISTA_MODALIDADE_USO, modalidadeUsoList);
		request.setAttribute(LISTA_AREA_CONSTRUIDA_FAIXA, controladorAreaConstruidaFaixa.listarAreaConstruidaFaixa());

		return VIEW_EXIBIR_INCLUSAO_EM_LOTE;
	}

	/**
	 * Método responsável por carregar os imóveis que serão preenchidos na tela de inclusão em lote de imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("carregarImoveisEmLote")
	public String carregarImoveisEmLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_EM_LOTE;

		try {
			carregarCamposImoveisLote(request, imovelVO, imovelLoteVO, true);

			ConstanteSistema constanteSistema = Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_TIPO_MODALIDADE_USO);

			Collection<EntidadeConteudo> modalidadeUsoList = Fachada.getInstancia().listarEntidadeConteudo(constanteSistema.getClasse());

			model.addAttribute(LISTA_MODALIDADE_USO, modalidadeUsoList);
			model.addAttribute(LISTA_AREA_CONSTRUIDA_FAIXA, controladorAreaConstruidaFaixa.listarAreaConstruidaFaixa());

			this.carregarParametroSistemaRotaObrigatoria(model);

			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_EM_LOTE;
		}

		return view;
	}

	private void carregarCamposImoveisLote(HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO, boolean gerarEmlote)
			throws GGASException {

		Long idModalidadeUso = imovelVO.getIdModalidadeUso();
		Long chavePrimaria = imovelVO.getChavePrimaria();
		Long idFaixaAreaConsumo = imovelVO.getIdAreaConstruidaFaixa();
		String numeroBlocos = imovelVO.getQuantidadeBloco();

		Long quantidadeBanheiro = null;
		if (!"".equals(imovelVO.getQuantidadeBanheiro())) {
			quantidadeBanheiro = Long.parseLong(imovelVO.getQuantidadeBanheiro());
		}

		String complemento = null;
		if (!"".equals(imovelVO.getComplementoImovel())) {
			complemento = imovelVO.getComplementoImovel();
		}

		String[] nomeBlocos = imovelLoteVO.getNomeBlocos();
		String numeroAndares = imovelVO.getQuantidadeAndar();
		String numeroAptAndar = imovelVO.getQuantidadeApartamentoAndar();
		String inicioApartamento = imovelLoteVO.getInicioApartamento();
		String incrementoApartamento = imovelLoteVO.getIncrementoApartamento();
		String tipoUnidade = imovelLoteVO.getTipoUnidade();
		
		int totalImoveis = 0;

		Imovel imovel = (Imovel) controladorImovel.obter(chavePrimaria, LISTA_PONTO_CONSUMO, QUADRA_FACE, QUADRA_FACE_ENDERECO,
				QUADRA_FACE_ENDERECO_CEP);

		if (imovel.getQuantidadeBloco() != null) {
			String quantidadeBloco = String.valueOf(controladorImovel.obterQtdBlocosRestantesImoveisEmLote(imovel));
			imovelVO.setQuantidadeBloco(quantidadeBloco);
			Integer blocosRestantes = Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_BLOCO, quantidadeBloco);
			if (blocosRestantes > 0) {
				request.setAttribute(ATRIBUTO_REQUEST_LISTA_BLOCOS, new String[blocosRestantes]);
			}

		}

		controladorImovel.validarCarregarImoveisEmLote(imovel, numeroBlocos, numeroAndares, numeroAptAndar, inicioApartamento,
				incrementoApartamento, tipoUnidade);

		if (imovel.getAreaConstruidaFaixa() != null) {
			imovelVO.setIdAreaConstruidaFaixa(imovel.getAreaConstruidaFaixa().getChavePrimaria());
			imovelVO.setFaixaAreaConstruidaImovel(
					imovel.getAreaConstruidaFaixa().getMenorFaixa() + " - " + imovel.getAreaConstruidaFaixa().getMaiorFaixa());
		}

		request.setAttribute(LISTA_AREA_CONSTRUIDA_FAIXA, controladorAreaConstruidaFaixa.listarAreaConstruidaFaixa());
		request.setAttribute(ATRIBUTO_REQUEST_IMOVEL, imovel);

		List<Imovel> listaImovel = new ArrayList<Imovel>();
		if (gerarEmlote) {
			listaImovel = controladorImovel.gerarImoveisEmLote(imovel, numeroBlocos, numeroAndares, numeroAptAndar);
			List<String> listaApartamentos = controladorImovel.gerarApartamentosImoveisEmLote(numeroBlocos, numeroAndares, numeroAptAndar,
					inicioApartamento, incrementoApartamento);
			String[] apartamentos = new String[listaApartamentos.size()];
			listaApartamentos.toArray(apartamentos);

			imovelLoteVO.setApartamentoImovelLote(apartamentos);

			List<String> listaNumeroLeitura = controladorImovel.gerarNumeroLeituraImoveisEmLote(listaApartamentos.size());
			String[] numeroLeituras = new String[listaNumeroLeitura.size()];
			listaNumeroLeitura.toArray(numeroLeituras);
			imovelLoteVO.setNumeroSequenciaLeituraImovelLote(numeroLeituras);

			if (numeroBlocos != null) {
				Integer numBLoco = Integer.parseInt(numeroBlocos);
				if (nomeBlocos != null && nomeBlocos.length < 1 && numBLoco > 0) {
					nomeBlocos = new String[numBLoco];
				}
			}
			List<String> listaBlocos = controladorImovel.gerarBlocosImoveisEmLote(numeroBlocos, numeroAndares, numeroAptAndar, nomeBlocos);
			String[] blocos = new String[listaBlocos.size()];
			listaBlocos.toArray(blocos);
			imovelLoteVO.setBlocoImovelLote(blocos);

			// quantidade de banheiro
			defineComplementoLote(imovelLoteVO, complemento, listaBlocos);

			// quantidade de banheiro
			defineQuantidadeBanheiro(imovelLoteVO, quantidadeBanheiro, listaBlocos);

			// modalidade de uso
			Long[] modalidadeUsoLote = new Long[listaBlocos.size()];
			for (int x = 0; x < listaBlocos.size(); x++) {
				modalidadeUsoLote[x] = idModalidadeUso;
			}
			imovelLoteVO.setModalidadeUsoLote(modalidadeUsoLote);

			// faixa area construida
			Long[] faixaAreaConstruida = new Long[listaBlocos.size()];
			for (int x = 0; x < listaBlocos.size(); x++) {
				faixaAreaConstruida[x] = idFaixaAreaConsumo;
			}
			imovelLoteVO.setFaixaAreaConstruidaImovelLote(faixaAreaConstruida);

		} else {
			listaImovel = (ArrayList<Imovel>) request.getSession().getAttribute(LISTA_IMOVEL);
		}

		ConstanteSistema constanteSistema = Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_TIPO_MODALIDADE_USO);
		Collection<EntidadeConteudo> modalidadeUsoList = Fachada.getInstancia().listarEntidadeConteudo(constanteSistema.getClasse());

		request.setAttribute(LISTA_MODALIDADE_USO, modalidadeUsoList);
		request.setAttribute(LISTA_IMOVEL, listaImovel);
		if (listaImovel != null) {
			if(imovel.getNumeroSublote() != null){ 
				String[] numeroSubLoteImovelLote = new String[listaImovel.size()];
				for(int i = 0; i < listaImovel.size(); i++) {
					numeroSubLoteImovelLote[i] = imovel.getNumeroSublote().toString();
				}
				imovelLoteVO.setNumeroSubLoteImovelLote(numeroSubLoteImovelLote);
			}
			totalImoveis = listaImovel.size();
		} else {
			totalImoveis = 1;
		}
		request.setAttribute(QTD_IMOVEIS, totalImoveis);
		if (numeroBlocos != null) {
			request.setAttribute(ATRIBUTO_REQUEST_LISTA_BLOCOS,
					new String[Util.converterCampoStringParaValorInteger(Imovel.QUANTIDADE_BLOCO, numeroBlocos)]);
		}

		request.getSession().setAttribute(LISTA_MODALIDADE_USO, modalidadeUsoList);
		request.getSession().setAttribute(LISTA_IMOVEL, listaImovel);
		request.getSession().setAttribute(LISTA_PONTO_CONSUMO, imovel.getListaPontoConsumo());
		request.getSession().setAttribute("listaSegmento", controladorSegmento.listarSegmento());
	}

	/**
	 *
	 * @param imovelLoteVO {@link ImovelLoteVO}
	 * @param quantidadeBanheiro {@link Long}
	 * @param listaBlocos {@link List}
	 */
	private void defineQuantidadeBanheiro(ImovelLoteVO imovelLoteVO, Long quantidadeBanheiro,
			List<String> listaBlocos) {
		String[] qtdBanheiroLote = new String[listaBlocos.size()];
		for (int x = 0; x < listaBlocos.size(); x++) {
			if (quantidadeBanheiro != null) {
				qtdBanheiroLote[x] = quantidadeBanheiro.toString();
			} else {
				qtdBanheiroLote[x] = "";
			}
		}
		imovelLoteVO.setQuantidadeBanheiroLote(qtdBanheiroLote);
	}

	/**
	 *
	 * @param imovelLoteVO {@link ImovelLoteVO}
	 * @param complemento {@link String}
	 * @param listaBlocos {@link List}
	 */
	private void defineComplementoLote(ImovelLoteVO imovelLoteVO, String complemento, List<String> listaBlocos) {
		String[] complementoLote = new String[listaBlocos.size()];
		for (int x = 0; x < listaBlocos.size(); x++) {
			if (complemento != null) {
				complementoLote[x] = complemento.toString();
			} else {
				complementoLote[x] = "";
			}
		}
		imovelLoteVO.setComplementoImovelLote(complementoLote);
	}

	/**
	 * Método responsável por incluir os imóveis em lote.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("incluirImoveisEmLote")
	public String incluirImoveisEmLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		String view = "forward:/exibirPesquisaImovel";

		try {
			this.incluirImoveisEmLote(model, request, imovelVO, imovelLoteVO);
			model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);
			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(ATRIBUTO_REQUEST_IMOVEL, new ImovelPesquisaVO());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_EM_LOTE;
		}

		return view;
	}

	private void incluirImoveisEmLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO)
			throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();
		Imovel imovelCondominio = (Imovel) controladorImovel.obter(chavePrimaria, LISTA_PONTO_CONSUMO, QUADRA_FACE, QUADRA_FACE_ENDERECO,
				QUADRA_FACE_ENDERECO_CEP, LISTA_IMOVEL_SERVICO_TIPO_RESTRICAO_SERVICO_TIPO);

		String[] bloco = imovelLoteVO.getBlocoImovelLote();
		this.carregarCamposImoveisLote(request, imovelVO, imovelLoteVO, false);

		String tipoUnidade = imovelLoteVO.getTipoUnidade();
		String[] numeroSubLote = imovelLoteVO.getNumeroSubLoteImovelLote();
		String[] numeroSequenciaLeitura = imovelLoteVO.getNumeroSequenciaLeituraImovelLote();
		String[] apartamento = imovelLoteVO.getApartamentoImovelLote();
		String[] complemento = imovelLoteVO.getComplementoImovelLote();

		String[] quantidadeBanheiro = imovelLoteVO.getQuantidadeBanheiroLote();
		Long[] modalidadeUso = imovelLoteVO.getModalidadeUsoLote();

		Long[] faixaAreaConstruida = imovelLoteVO.getFaixaAreaConstruidaImovelLote();
		List<Imovel> listaImovel = (ArrayList<Imovel>) request.getSession().getAttribute(LISTA_IMOVEL);

		String numeroBlocos = imovelLoteVO.getNumeroBlocos();
		imovelVO.setQuantidadeBloco(numeroBlocos);

		Long idSegmento = imovelLoteVO.getIdSegmentoPC();
		Long idRamoAtividade = imovelLoteVO.getIdRamoAtividadePC();
		Long idRota = imovelLoteVO.getIdRotaPC();

		Map<String, Object> filtroRota = new HashMap<String, Object>();
		filtroRota.put(CHAVE_PRIMARIA, idRota);

		Rota rota = null;
		if (idRota != null && !idRota.equals(-1L)) {
			rota = obterRota(idRota, imovelCondominio);
		}

		try {
			controladorImovel.validarCamaposImovelEmLote(apartamento, bloco, quantidadeBanheiro);
			controladorImovel.validarInclusaoImoveisEmLote(imovelCondominio, apartamento, bloco, numeroBlocos, tipoUnidade,
					numeroSequenciaLeitura);
		} catch (Exception e) {
			request.getSession().setAttribute(LISTA_IMOVEL, null);
			throw e;
		}
		imovelCondominio.getQuadraFace().getEndereco().getCep();

		if (apartamento.length != complemento.length) {
			complemento = new String[apartamento.length];
			for (int i = 0; i < complemento.length; i++) {
				complemento[i] = "";
			}
		}
		if (apartamento.length != bloco.length) {
			bloco = new String[apartamento.length];
			for (int i = 0; i < bloco.length; i++) {
				bloco[i] = "";
			}
		}
		if (apartamento.length != numeroSubLote.length) {
			numeroSubLote = new String[apartamento.length];
			for (int i = 0; i < numeroSubLote.length; i++) {
				numeroSubLote[i] = "";
			}
		}

		for (int i = 0; i < apartamento.length; i++) {
			if (StringUtils.isNotEmpty(apartamento[i])) {

				Imovel imovel = listaImovel.get(i);
				String descricaoComplemento = "";

				if (bloco[i] == null || "".equals(bloco[i])) {
					descricaoComplemento = tipoUnidade + " " + apartamento[i] + " " + complemento[i];
				} else {
					descricaoComplemento = bloco[i] + " " + tipoUnidade + " " + apartamento[i] + " " + complemento[i];
				}
				imovel.setDescricaoComplemento(descricaoComplemento);
				if ((numeroSubLote[i] != null) && (!("").equals(numeroSubLote[i]))) {
					imovel.setNumeroSublote(Integer.valueOf(numeroSubLote[i]));
				} else {
					imovel.setNumeroSublote(null);
				}
				if (!"".equals(numeroSequenciaLeitura[i])) {
					imovel.setNumeroSequenciaLeitura(Integer.valueOf(numeroSequenciaLeitura[i]));
				}
				if ((faixaAreaConstruida[i] != null) && (faixaAreaConstruida[i] > 0)) {
					AreaConstruidaFaixa areaConstruidaFaixa =
							(AreaConstruidaFaixa) controladorAreaConstruidaFaixa.obter(faixaAreaConstruida[i]);
					imovel.setAreaConstruidaFaixa(areaConstruidaFaixa);
				}
				if ((quantidadeBanheiro.length > 0) && (quantidadeBanheiro[i] != null) && (!("").equals(quantidadeBanheiro[i]))) {
					imovel.setQuantidadeBanheiro(Integer.parseInt(quantidadeBanheiro[i]));
				}

				imovel.setIndicadorRestricaoServico(imovelCondominio.getIndicadorRestricaoServico());
				if (imovelCondominio.getIndicadorRestricaoServico()) {
					imovel.setListaImovelServicoTipoRestricao(
							criarListaServicoTipoRestricao(imovel, imovelCondominio.getListaImovelServicoTipoRestricao()));
				}

				imovel.setCep(imovelCondominio.getQuadraFace().getEndereco().getCep().getCep());
				imovel.setDadosAuditoria(getDadosAuditoria(request));
				imovel.setCondominio(Boolean.FALSE);
				imovel.setVersao(0);
				if (bloco[i] == null || "".equals(bloco[i])) {
					imovel.setNome(imovel.getNome() + ", " + tipoUnidade + " " + apartamento[i] + " " + complemento[i]);
				} else {
					imovel.setNome(imovel.getNome() + ", " + bloco[i] + ", " + tipoUnidade + " " + apartamento[i] + " " + complemento[i]);
				}

				// Inserir ponto de consumo no
				// imóvel
				PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();

				if (modalidadeUso[i] > 0) {
					EntidadeConteudo modalidadeUsoPontoConsumo = Fachada.getInstancia().obterEntidadeConteudo(modalidadeUso[i]);

					pontoConsumo.setModalidadeUso(modalidadeUsoPontoConsumo);
				} else {
					pontoConsumo.setModalidadeUso(null);
				}

				pontoConsumo.setCep(imovelCondominio.getQuadraFace().getEndereco().getCep());
				pontoConsumo.setDescricao(imovel.getNome().replace(", ", " - "));
				pontoConsumo.setDescricaoComplemento(imovel.getDescricaoComplemento());
				pontoConsumo.setNumeroImovel(imovel.getNumeroImovel());
				SituacaoConsumo situacaoConsumo = controladorImovel.obterSituacaoConsumo(Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO)));
				pontoConsumo.setSituacaoConsumo(situacaoConsumo);
				pontoConsumo.setQuadraFace(imovelCondominio.getQuadraFace());
				pontoConsumo.setHabilitado(Boolean.TRUE);
				pontoConsumo.setImovel(imovel);
				pontoConsumo.setDadosAuditoria(getDadosAuditoria(request));
				pontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
				pontoConsumo.setVersao(0);

				if (rota != null) {
					pontoConsumo.setNumeroSequenciaLeitura(Integer.valueOf(numeroSequenciaLeitura[i]));
				}

				if (imovelCondominio.getListaPontoConsumo() != null && !imovelCondominio.getListaPontoConsumo().isEmpty()) {

					pontoConsumo.setLatitudeGrau(imovelCondominio.getListaPontoConsumo().iterator().next().getLatitudeGrau());
					pontoConsumo.setLongitudeGrau(imovelCondominio.getListaPontoConsumo().iterator().next().getLongitudeGrau());
					pontoConsumo.setRota(rota);
					pontoConsumo.setSegmento(imovelCondominio.getListaPontoConsumo().iterator().next().getSegmento());
					pontoConsumo.setRamoAtividade(imovelCondominio.getListaPontoConsumo().iterator().next().getRamoAtividade());
				} else {

					controladorSegmento.validarSegmento(idSegmento);
					controladorRamoAtividade.validarRamoAtividade(idRamoAtividade);

					String indicadorRotaObrigatoria = this.carregarParametroSistemaRotaObrigatoria(model);
					if ("1".equals(indicadorRotaObrigatoria)) {
						controladorRota.validarRota(idRota);
					}

					Segmento segmento = controladorSegmento.obterSegmento(idSegmento);
					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(CHAVE_PRIMARIA, idRamoAtividade);
					Collection<RamoAtividade> collRamoAtividade = controladorRamoAtividade.consultarRamoAtividade(filtro);

					pontoConsumo.setRota(rota);
					pontoConsumo.setSegmento(segmento);
					pontoConsumo.setRamoAtividade(collRamoAtividade.iterator().next());

				}
				Collection<CityGate> listaCityGate =
						controladorQuadra.listarCityGatePorQuadraFace(imovelCondominio.getQuadraFace().getChavePrimaria());

				for (CityGate cityGate : listaCityGate) {
					int tam = listaCityGate.size();
					int count = 100;

					PontoConsumoCityGate pontoConsumoCityGate = (PontoConsumoCityGate) controladorPontoConsumo.criarPontoConsumoCityGate();

					pontoConsumoCityGate.setPontoConsumo(pontoConsumo);
					pontoConsumoCityGate.setCityGate(cityGate);
					pontoConsumoCityGate.setPercentualParticipacao(Util.converterCampoStringParaValorBigDecimal("Percentual Participação",
							String.valueOf(count / tam), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));

					pontoConsumoCityGate.setDataVigencia(Calendar.getInstance().getTime());
					pontoConsumoCityGate.setUltimaAlteracao(Calendar.getInstance().getTime());
					pontoConsumo.getListaPontoConsumoCityGate().add(pontoConsumoCityGate);
				}

				Collection<PontoConsumo> listaPontoConsumo = new HashSet<PontoConsumo>();
				listaPontoConsumo.add(pontoConsumo);

				imovel.setListaPontoConsumo(listaPontoConsumo);
			}

		}
		insereNumeroSequencialLeituraPorRota(listaImovel, rota);

		for (Imovel imovel : listaImovel) {
			controladorImovel.inserirImovel(imovel);
		}

		Map<String, Object> filtroImovelCondominio = new HashMap<String, Object>();
		filtroImovelCondominio.put("matriculaCondominio", imovelCondominio.getChavePrimaria());

		Collection<Imovel> listaImoveisAdicionados = controladorImovel.consultarImoveis(filtroImovelCondominio);
		Collection<Imovel> listaImoveisAdicionadosAux = new ArrayList<Imovel>();

		listaImoveisAdicionadosAux.add(imovelCondominio);
		listaImoveisAdicionadosAux.addAll(listaImoveisAdicionados);

		listaImoveisAdicionados.clear();
		listaImoveisAdicionados.addAll(listaImoveisAdicionadosAux);

		request.getSession().removeAttribute(LISTA_IMOVEL);
		super.mensagemSucesso(model, Constantes.SUCESSO_IMOVEIS_EM_LOTE);

		super.adicionarFiltroPaginacao(request, filtroImovelCondominio);

		model.addAttribute(LISTA_IMOVEIS, super.criarColecaoPaginada(request, filtroImovelCondominio, listaImoveisAdicionados));
	}

	/**
	 *
	 * @param listaImovel
	 * @param rota
	 */
	private void insereNumeroSequencialLeituraPorRota(List<Imovel> listaImovel, Rota rota) {
		List<PontoConsumo> listaPontoConsumo = new LinkedList<>();
		for (Imovel imovel : listaImovel) {
			listaPontoConsumo.addAll(imovel.getListaPontoConsumo());
		}
		if (rota != null && rota.getPontosConsumo() != null && !rota.getPontosConsumo().isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				pontoConsumo.setNumeroSequenciaLeitura(rota.getPontosConsumo().size() + pontoConsumo.getNumeroSequenciaLeitura());
			}
		}
	}

	private Rota obterRota(Long idRota, Imovel imovelCondominio) throws NegocioException {

		Rota rota = null;
		if (idRota != null && idRota > 0) {
			rota = (Rota) controladorRota.obter(idRota, PONTOS_CONSUMO);
		} else {
			if (imovelCondominio.getListaPontoConsumo() != null && !imovelCondominio.getListaPontoConsumo().isEmpty()
					&& imovelCondominio.getListaPontoConsumo().iterator().next().getRota() != null) {
				Long chaveRota = imovelCondominio.getListaPontoConsumo().iterator().next().getRota().getChavePrimaria();
				rota = (Rota) controladorRota.obter(chaveRota, PONTOS_CONSUMO);
			}
		}
		return rota;
	}

	private void popularForm(ImovelVO imovelVO, Imovel imovel, Model model) {

		imovelVO.setChavePrimaria(imovel.getChavePrimaria());
		imovelVO.setVersao(imovel.getVersao());
		imovelVO.setNome(imovel.getNome());

		if ((imovel.getQuadraFace() != null) && (imovel.getQuadraFace().getEndereco() != null)) {
			imovelVO.setCepImovel(imovel.getQuadraFace().getEndereco().getCep().getCep());
			imovelVO.setChaveCep(String.valueOf(imovel.getQuadraFace().getEndereco().getCep().getChavePrimaria()));
		}

		imovelVO.setNumeroImovel(imovel.getNumeroImovel());
		imovelVO.setComplementoImovel(imovel.getDescricaoComplemento());

		if (!StringUtils.isEmpty(imovel.getEnderecoReferencia())) {
			imovelVO.setEnderecoReferencia(imovel.getEnderecoReferencia());
		}

		if (imovel.getQuantidadeBanheiro() != null) {
			imovelVO.setQuantidadeBanheiro(imovel.getQuantidadeBanheiro().toString());
		} else {
			imovelVO.setQuantidadeBanheiro("");
		}

		imovelVO.setIdQuadraImovel(imovel.getQuadraFace().getQuadra().getChavePrimaria());
		imovelVO.setIdQuadraFace(imovel.getQuadraFace().getChavePrimaria());
		imovelVO.setQuadraFaceImovel(imovel.getQuadraFace().getQuadraFaceFormatada());

		if (imovel.getNumeroLote() != null) {
			imovelVO.setNumeroLote(String.valueOf(imovel.getNumeroLote()));
		}

		if (imovel.getNumeroSublote() != null) {
			imovelVO.setNumeroSublote(imovel.getNumeroSublote().toString());
		} else {
			imovelVO.setNumeroSublote("");
		}
		if (imovel.getNumeroTestada() != null) {
			imovelVO.setTestadaLote(imovel.getNumeroTestada().toString());
		} else {
			imovelVO.setTestadaLote("");
		}
		if (imovel.getSituacaoImovel() != null) {
			imovelVO.setIdSituacao(imovel.getSituacaoImovel().getChavePrimaria());
			imovelVO.setDescricaoSituacaoImovel(imovel.getSituacaoImovel().getDescricao());

		}
		if (imovel.getPavimentoCalcada() != null) {
			imovelVO.setIdPavimentoCalcada(imovel.getPavimentoCalcada().getChavePrimaria());
			imovelVO.setDescricaoPavimentoCalcadaImovel(imovel.getPavimentoCalcada().getDescricao());
		}
		if (imovel.getPavimentoRua() != null) {
			imovelVO.setIdPavimentoRua(imovel.getPavimentoRua().getChavePrimaria());
			imovelVO.setDescricaoPavimentoRuaImovel(imovel.getPavimentoRua().getDescricao());
		}
		if (imovel.getPadraoConstrucao() != null) {
			imovelVO.setIdPadraoConstrucao(imovel.getPadraoConstrucao().getChavePrimaria());
			imovelVO.setDescricaoPadraoConstrucaoImovel(imovel.getPadraoConstrucao().getDescricao());
		}
		if (imovel.getAreaConstruidaFaixa() != null) {
			imovelVO.setIdAreaConstruidaFaixa(imovel.getAreaConstruidaFaixa().getChavePrimaria());
			imovelVO.setFaixaAreaConstruidaImovel(
					imovel.getAreaConstruidaFaixa().getMenorFaixa() + " - " + imovel.getAreaConstruidaFaixa().getMaiorFaixa());
		}
		if (imovel.getPerfilImovel() != null) {
			imovelVO.setIdPerfilImovel(imovel.getPerfilImovel().getChavePrimaria());
			imovelVO.setDescricaoPerfilImovel(imovel.getPerfilImovel().getDescricao());
		}
		if (imovel.getCondominio() != null) {
			imovelVO.setCondominio(imovel.getCondominio().toString());
		}
		if (imovel.getModalidadeMedicaoImovel() != null) {
			imovelVO.setCodigoModalidadeMedicao(imovel.getModalidadeMedicaoImovel().getCodigo());
			imovelVO.setDescricaoModalidadeMedicaoImovel(imovel.getModalidadeMedicaoImovel().getDescricao());
		}
		if (imovel.getTipoBotijao() != null) {
			imovelVO.setIdTipoBotijao(imovel.getTipoBotijao().getChavePrimaria());
			imovelVO.setDescricaoTipoBotijaoImovel(imovel.getTipoBotijao().getDescricao());
		}
		if (imovel.getTipoCombustivel() != null) {
			imovelVO.setIdTipoCombustivel(imovel.getTipoCombustivel().getChavePrimaria());
			imovelVO.setDescricaoTipoCombustivelImovel(imovel.getTipoCombustivel().getDescricao());
		}
		if (imovel.getQuantidadeBloco() != null) {
			imovelVO.setQuantidadeBloco(String.valueOf(imovel.getQuantidadeBloco()));
		}
		if (imovel.getQuantidadeAndar() != null) {
			imovelVO.setQuantidadeAndar(String.valueOf(imovel.getQuantidadeAndar()));
		}
		if (imovel.getQuantidadeBanheiro() != null) {
			imovelVO.setQuantidadeBanheiro(String.valueOf(imovel.getQuantidadeBanheiro()));
		}
		if (imovel.getQuantidadeApartamentoAndar() != null) {
			imovelVO.setQuantidadeApartamentoAndar(String.valueOf(imovel.getQuantidadeApartamentoAndar()));
		}
		if (imovel.getValvulaBloqueio() != null) {
			imovelVO.setValvulaBloqueio(imovel.getValvulaBloqueio().toString());
		}
		if (imovel.getDataEntrega() != null) {
			imovelVO.setDataEntrega(Util.converterDataParaStringSemHora(imovel.getDataEntrega(), Constantes.FORMATO_DATA_HORA_BR));
		}
		if (imovel.getEmpresa() != null) {
			imovelVO.setIdEmpresa(imovel.getEmpresa().getChavePrimaria());
			imovelVO.setNomeConstrutora(imovel.getEmpresa().getCliente().getNome());
		}
		if (imovel.getPortaria() != null) {
			imovelVO.setPortaria(imovel.getPortaria().toString());
		}

		if (imovel.getRedePreexistente() != null) {
			imovelVO.setRedePreexistente(imovel.getRedePreexistente().toString());
		}
		if (imovel.getRedeInternaImovel() != null) {
			if (imovel.getRedeInternaImovel().getRedeMaterial() != null) {
				imovelVO.setIdRedeMaterial(imovel.getRedeInternaImovel().getRedeMaterial().getChavePrimaria());
				imovelVO.setDescricaoRedeMaterial(imovel.getRedeInternaImovel().getRedeMaterial().getDescricao());
			}
			if (imovel.getRedeInternaImovel().getQuantidadePrumada() != null) {
				imovelVO.setQuantidadePrumada(imovel.getRedeInternaImovel().getQuantidadePrumada().toString());
			} else {
				imovelVO.setQuantidadePrumada("");
			}
			if (imovel.getRedeInternaImovel().getRedeDiametroCentral() != null) {
				imovelVO.setIdRedeDiametroCentral(imovel.getRedeInternaImovel().getRedeDiametroCentral().getChavePrimaria());
				imovelVO.setDiametroRedeTrechoPrumada(imovel.getRedeInternaImovel().getRedeDiametroCentral().getDescricao());
			}
			if (imovel.getRedeInternaImovel().getRedeDiametroPrumada() != null) {
				imovelVO.setIdRedeDiametroPrumada(imovel.getRedeInternaImovel().getRedeDiametroPrumada().getChavePrimaria());
				imovelVO.setDiametroRedeNasPrumada(imovel.getRedeInternaImovel().getRedeDiametroPrumada().getDescricao());
			}
			if (imovel.getRedeInternaImovel().getRedeDiametroPrumadaApartamento() != null) {
				imovelVO.setIdRedeDiametroPrumadaApartamento(
						imovel.getRedeInternaImovel().getRedeDiametroPrumadaApartamento().getChavePrimaria());
				imovelVO.setDiametroRedeSecundaria(imovel.getRedeInternaImovel().getRedeDiametroPrumadaApartamento().getDescricao());
			}
			if (imovel.getRedeInternaImovel().getQuantidadeReguladorHall() != null) {
				imovelVO.setQuantidadeReguladorHall(imovel.getRedeInternaImovel().getQuantidadeReguladorHall().toString());
			} else {
				imovelVO.setQuantidadeReguladorHall("");
			}
			if (imovel.getRedeInternaImovel().getVentilacaoHall()) {
				imovelVO.setVentilacaoHall(imovel.getRedeInternaImovel().getVentilacaoHall().toString());
			}
			if (imovel.getRedeInternaImovel().getVentilacaoApartamento()) {
				imovelVO.setVentilacaoApartamento(imovel.getRedeInternaImovel().getVentilacaoApartamento().toString());
			}

		}
		if (imovel.getRota() != null) {
			String descricaoRota = imovel.getRota().getNumeroRota();
			Long idRotaPrevista = imovel.getRota().getChavePrimaria();
			imovelVO.setDescricaoRotaPrevista(descricaoRota);
			imovelVO.setIdRotaPrevista(idRotaPrevista);
		}
		if (imovel.getDataPrevisaoEncerramentoObra() != null) {
			String dataPrevisaoEncerramento =
					Util.converterDataParaStringSemHora(imovel.getDataPrevisaoEncerramentoObra(), Constantes.FORMATO_DATA_HORA_BR);
			imovelVO.setDataPrevisaoEncerramentoObra(dataPrevisaoEncerramento);
		}
		if (imovel.getIndicadorObraTubulacao() != null) {
			imovelVO.setIndicadorObraTubulacao(String.valueOf(imovel.getIndicadorObraTubulacao()));
		}
		imovelVO.setHabilitado(String.valueOf(imovel.isHabilitado()));

		if (imovel.getAgente() != null) {
			imovelVO.setIdAgente(imovel.getAgente().getChavePrimaria());
			imovelVO.setNomeAgente(imovel.getAgente().getFuncionario().getNome());
		}

		if (imovel.getListaImovelServicoTipoRestricao() != null) {

			Collection<ImovelServicoTipoRestricao> listaImovelServicoTipo = imovel.getListaImovelServicoTipoRestricao();

			Long[] idsServicoTipoRestricao = new Long[listaImovelServicoTipo.size()];

			Iterator<ImovelServicoTipoRestricao> it = listaImovelServicoTipo.iterator();
			for (int i = 0; i < listaImovelServicoTipo.size(); i++) {
				idsServicoTipoRestricao[i] = it.next().getServicoTipo().getChavePrimaria();
			}
			model.addAttribute(IDS_SERVICO_TIPO_RESTRICAO_SALVOS, idsServicoTipoRestricao);
			imovelVO.setIdsServicoTipoRestricao(idsServicoTipoRestricao);
		}

		if (imovel.getIndicadorRestricaoServico() != null) {
			imovelVO.setIndicadorRestricaoServico(String.valueOf(imovel.getIndicadorRestricaoServico()));
		}

		imovelVO.setIndicadorAlterarImovelCondominioFilho(false);
		if (imovel.getIdImovelCondominio() != null) {
			try {
				final Imovel imovelCondominio = (Imovel) controladorImovel.obter(
						imovel.getIdImovelCondominio().longValue(), REDE_INTERNA_IMOVEL, QUADRA_FACE,
						QUADRA_FACE_ENDERECO, QUADRA_FACE_ENDERECO_CEP, ID_IMOVEL_CONDOMINIO);
				imovelVO.setCidadeImovelPai(imovelCondominio.getQuadraFace().getEndereco().getCep().getNomeMunicipio());
				imovelVO.setNumeroImovelPai(imovelCondominio.getQuadraFace().getEndereco().getNumero());
				imovelVO.setImovelPai("true");
				imovelVO.setNomeFantasiaImovelPai(imovelCondominio.getNome());
				imovelVO.setMatriculaImovelPai(String.valueOf(imovelCondominio.getChavePrimaria()));
				imovelVO.setIndicadorCondominioImovelPai(imovelCondominio.getCondominio().toString());
			} catch ( Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Cria lista de servico tipo restrição
	 *
	 * @param imovel
	 * @param listaImovelServicoTipoRestricao
	 * @return listaImovelServicoTipoRestricao
	 */
	private Collection<ImovelServicoTipoRestricao> criarListaServicoTipoRestricao(Imovel imovel,
			Collection<ImovelServicoTipoRestricao> listaImovelServicoTipoRestricao) {

		HashSet<ImovelServicoTipoRestricao> lista = new HashSet<ImovelServicoTipoRestricao>();

		for (ImovelServicoTipoRestricao imovelServicoTipoRestricao : listaImovelServicoTipoRestricao) {
			ImovelServicoTipoRestricao restricao = new ImovelServicoTipoRestricaoImpl();
			restricao.setImovel(imovel);
			restricao.setServicoTipo(imovelServicoTipoRestricao.getServicoTipo());
			restricao.setUltimaAlteracao(new Date());
			restricao.setHabilitado(true);
			restricao.setVersao(imovelServicoTipoRestricao.getVersao());
			lista.add(restricao);
		}

		return lista;
	}

	/**
	 * Método responsável por remover um apartamento da lista de apartamentos do imóvel condomínio.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("removerApartamentoImoveisEmLote")
	@SuppressWarnings("unchecked")
	public String removerApartamentoImoveisEmLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_EM_LOTE;
		try {
			Integer indexListaLote = imovelLoteVO.getIndexListaLote();

			String[] apartamentos = imovelLoteVO.getApartamentoImovelLote();
			String[] blocos = imovelLoteVO.getBlocoImovelLote();
			String[] numerosSublote = imovelLoteVO.getNumeroSubLoteImovelLote();
			String[] numerosSequencia = imovelLoteVO.getNumeroSequenciaLeituraImovelLote();
			String[] complementos = imovelLoteVO.getComplementoImovelLote();
			Long[] faixasAreaConstruida = imovelLoteVO.getFaixaAreaConstruidaImovelLote();

			List<Imovel> listaImoveis = (ArrayList<Imovel>) request.getSession().getAttribute(LISTA_IMOVEL);
			List<String> listaApartamento = new ArrayList<String>();
			List<String> listaBloco = new ArrayList<String>();
			List<String> listaNumeroSubLote = new ArrayList<String>();
			List<String> listaNumeroSequencia = new ArrayList<String>();
			List<String> listaComplemento = new ArrayList<String>();
			List<Long> listaFaixaAreaConstruida = new ArrayList<Long>();

			if (listaImoveis != null) {
				for (int i = 0; i < listaImoveis.size(); i++) {
					if(apartamentos.length > 0){
						listaApartamento.add(apartamentos[i]);
					}
					if(blocos.length > 0){
						listaBloco.add(blocos[i]);
					}
					if(numerosSublote.length > 0){
						listaNumeroSubLote.add(numerosSublote[i]);
					}
					if(numerosSequencia.length > 0){
						listaNumeroSequencia.add(numerosSequencia[i]);
					}
					if(complementos.length > 0){
						listaComplemento.add(complementos[i]);
					}
					if(faixasAreaConstruida.length > 0){
						listaFaixaAreaConstruida.add(faixasAreaConstruida[i]);
					}
				}

				if ((!listaImoveis.isEmpty()) && (indexListaLote != null)) {
					listaImoveis.remove(indexListaLote.intValue());
					listaApartamento.remove(indexListaLote.intValue());
					listaBloco.remove(indexListaLote.intValue());
					if(!listaNumeroSubLote.isEmpty()){
						listaNumeroSubLote.remove(indexListaLote.intValue());
					}
					listaNumeroSequencia.remove(indexListaLote.intValue());
					if(!listaComplemento.isEmpty()){
						listaComplemento.remove(indexListaLote.intValue());
					}
					listaFaixaAreaConstruida.remove(indexListaLote.intValue());
				}
			}

			request.getSession().setAttribute(LISTA_IMOVEL, listaImoveis);

			String[] arrayApartamentos = new String[listaApartamento.size()];
			String[] arrayBlocos = new String[listaBloco.size()];
			String[] arrayNumeroSubLote = new String[listaNumeroSubLote.size()];
			String[] arrayNumeroSequencia = new String[listaNumeroSequencia.size()];
			String[] arrayComplemento = new String[listaComplemento.size()];
			Long[] arrayFaixaAreaConstruida = new Long[listaFaixaAreaConstruida.size()];

			listaApartamento.toArray(arrayApartamentos);
			listaBloco.toArray(arrayBlocos);
			listaNumeroSubLote.toArray(arrayNumeroSubLote);
			listaNumeroSequencia.toArray(arrayNumeroSequencia);
			listaComplemento.toArray(arrayComplemento);
			listaFaixaAreaConstruida.toArray(arrayFaixaAreaConstruida);

			List<String> listaNumeroLeitura = controladorImovel.gerarNumeroLeituraImoveisEmLote(listaApartamento.size());
			String[] numeroLeituras = new String[listaNumeroLeitura.size()];
			listaNumeroLeitura.toArray(numeroLeituras);

			imovelLoteVO.setApartamentoImovelLote(arrayApartamentos);
			imovelLoteVO.setBlocoImovelLote(arrayBlocos);
			imovelLoteVO.setNumeroSubLoteImovelLote(arrayNumeroSubLote);
			imovelLoteVO.setNumeroSequenciaLeituraImovelLote(numeroLeituras);
			imovelLoteVO.setComplementoImovelLote(arrayComplemento);
			imovelLoteVO.setFaixaAreaConstruidaImovelLote(arrayFaixaAreaConstruida);

			carregarCamposImoveisLote(request, imovelVO, imovelLoteVO, false);

			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_EM_LOTE;
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de alterar logradouro em lote.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAlteracaoLogradouroLote")
	public String exibirAlteracaoLogradouroLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		Long[] chavesPrimarias = imovelVO.getChavesPrimarias();

		Long[] chavesPrimariasSessao = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);
		if (chavesPrimariasSessao != null) {
			chavesPrimarias = chavesPrimariasSessao;
		}

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);

		Collection<Imovel> listaImovel = controladorImovel.consultarImoveis(filtro);
		Collection<Imovel> listaImovelFilho = new ArrayList<Imovel>();
		Collection<Imovel> hashSet = new HashSet<Imovel>();

		for (Imovel imovel : listaImovel) {
			if (imovel.getCondominio()) {
				Collection<Imovel> imoveisFilhos = controladorImovel.obterImoveisFilhosDoCondominio(imovel);
				listaImovelFilho.addAll(imoveisFilhos);
			}
		}

		hashSet.addAll(listaImovel);
		hashSet.addAll(listaImovelFilho);
		listaImovel.clear();
		listaImovel.addAll(hashSet);

		chavesPrimarias = new Long[listaImovel.size()];
		int cont = 0;
		for (Imovel imovel : listaImovel) {
			chavesPrimarias[cont] = imovel.getChavePrimaria();
			cont = cont + 1;
		}
		request.getSession().setAttribute(CHAVES_PRIMARIAS, chavesPrimarias);

		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		listaImovel = controladorImovel.consultarImoveis(filtro);
		model.addAttribute(LISTA_IMOVEL, listaImovel);
		request.getSession().setAttribute(LISTA_IMOVEL, listaImovel);

		this.exibirDadosAbaIdentificacaoLocalizacao(model, imovelVO);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);
		model.addAttribute("listaNomeImoveis", controladorImovel.listarNomeImoveis(chavesPrimarias));

		return "exibirAlteracaoLogradouroLote";
	}

	/**
	 * Carregar imoveis filhos por situacao imovel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("carregarImoveisFilhosPorSituacaoImovel")
	public String carregarImoveisFilhosPorSituacaoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelLoteVO imovelLoteVO, BindingResult bindingResult) throws GGASException {

		Long idQuadraFace = imovelVO.getIdQuadraFace();
		QuadraFace novaQuadraFace = controladorQuadra.obterQuadraFace(idQuadraFace);
		int novoIndicadorRede = novaQuadraFace.getRedeIndicador().getCodigo();

		Long[] idImoveis = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVES_PRIMARIAS, idImoveis);
		Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);

		List<Imovel> listaImovelCondominioFactivel = new ArrayList<Imovel>();
		List<Imovel> listaImovelCondominioPotencial = new ArrayList<Imovel>();
		for (Imovel imovel : listaImoveis) {

			Imovel imovelPai = imovel.getImovelCondominio();
			int imovelIndicadorRede = imovel.getQuadraFace().getRedeIndicador().getCodigo();

			if (imovelIndicadorRede == RedeIndicador.NAO_TEM && novoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE) {
				if (imovelPai != null && !listaImovelCondominioFactivel.contains(imovelPai)) {
					listaImovelCondominioFactivel.add(imovelPai);
				}

			} else if (imovelIndicadorRede == RedeIndicador.TEM && novoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE && imovelPai != null
					&& !listaImovelCondominioPotencial.contains(imovelPai)) {
				listaImovelCondominioPotencial.add(imovelPai);
			}
		}

		Collection<Imovel> listaImovelFilhoFactivel = new ArrayList<Imovel>();
		Collection<Imovel> listaImovelFilhoPotencial = new ArrayList<Imovel>();

		for (Imovel imovel : listaImovelCondominioFactivel) {
			Collection<Imovel> imoveisFilhos = controladorImovel.obterImoveisFilhosDoCondominio(imovel);
			listaImovelFilhoFactivel.addAll(imoveisFilhos);
		}

		for (Imovel imovel : listaImovelCondominioPotencial) {
			Collection<Imovel> imoveisFilhos = controladorImovel.obterImoveisFilhosDoCondominio(imovel);

			ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);
			Long situacaoImovel = imovel.getSituacaoImovel().getChavePrimaria();
			Long situacaoImovelFactivel = Long.parseLong(constanteSituacaoImovel.getValor());

			if (situacaoImovelFactivel.equals(situacaoImovel)) {
				listaImovelFilhoPotencial.addAll(imoveisFilhos);
			}
		}

		if (listaImovelFilhoFactivel.isEmpty() && listaImovelFilhoPotencial.isEmpty()) {
			imovelLoteVO.setExisteImoveisFilhos(Boolean.FALSE);
		} else {
			imovelLoteVO.setExisteImoveisFilhos(null);
		}

		request.setAttribute(LISTA_IMOVEL_FILHO_FACTIVEL, listaImovelFilhoFactivel);
		request.getSession().setAttribute(LISTA_IMOVEL_FILHO_FACTIVEL, listaImovelFilhoFactivel);

		request.setAttribute(LISTA_IMOVEL_FILHO_POTENCIAL, listaImovelFilhoPotencial);
		request.getSession().setAttribute(LISTA_IMOVEL_FILHO_POTENCIAL, listaImovelFilhoPotencial);

		this.exibirDadosAbaIdentificacaoLocalizacao(model, imovelVO);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);

		return "exibirAlteracaoLogradouroLote";
	}

	/**
	 * Exibir popup alteração de logradouro.
	 *
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPopupAlteracaoLogradouro")
	public String exibirPopupAlteracaoLogradouro(Model model) {

		return "exibirPopupAlteracaoLogradouro";
	}

	/**
	 * Alterar logradouro lote.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarLogradouroLote")
	public String alterarLogradouroLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) {

		String view = "forward:/exibirAlteracaoLogradouroLote";

		try {
			view = this.alterarLogradouroLote(model, request, imovelVO, imovelLoteVO);
			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private String alterarLogradouroLote(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO)
			throws GGASException {

		Long[] chavesPrimarias = imovelVO.getChavesPrimarias();
		Long chavePrimaria = imovelVO.getChavePrimaria();
		String nome = imovelVO.getNome();
		String numeroImovel = imovelVO.getNumeroImovel();
		String nomeAntigoBloco = imovelLoteVO.getNomeAntigoBloco();
		String nomeNovoBloco = imovelLoteVO.getNomeNovoBloco();
		chavesPrimarias = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		filtro.put("bloco", nomeAntigoBloco);

		Collection<Imovel> listaImovel = controladorImovel.consultarImoveis(filtro);
		request.getSession().setAttribute(LISTA_IMOVEL, listaImovel);

		filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		Collection<Imovel> listaImovelPai = controladorImovel.consultarImoveis(filtro);

		for (Imovel imovelPai : listaImovelPai) {
			imovelPai.getNome();
		}
		QuadraFace quadraFace = null;
		Quadra quadra = null;
		Long idQuadraFace = imovelVO.getIdQuadraFace();
		if (idQuadraFace != null && idQuadraFace > 0) {
			quadraFace = controladorQuadra.obterQuadraFace(idQuadraFace);
			controladorImovel.alterarSituacaoImovelPorIndicadorRede(listaImovel, quadraFace);
		}

		Long idQuadra = imovelVO.getIdQuadraImovel();
		if (idQuadra != null && idQuadra > 0) {
			quadra = (Quadra) controladorQuadra.obter(idQuadra);
		}

		String cepImovel = imovelVO.getCepImovel();

		for (Imovel imovel : listaImovel) {
			if (quadra != null) {
				imovel.setQuadra(quadra);
			}
			if (quadraFace != null) {
				imovel.setQuadraFace(quadraFace);
			}
			if (cepImovel != null && !cepImovel.isEmpty()) {
				imovel.setCep(cepImovel);
			}
			if (nome != null && !nome.isEmpty()) {
				if (imovel.getCondominio() != null && imovel.getCondominio()) {
					imovel.setNome(nome);
				} else {
					String antigoNomeImovel = imovel.getNome();
					String[] arrayAntigoNomeImovel = antigoNomeImovel.split(",");
					if (arrayAntigoNomeImovel.length == 3) {
						imovel.setNome(nome + ", " + arrayAntigoNomeImovel[1] + ", " + arrayAntigoNomeImovel[2]);
					} else {
						imovel.setNome(nome + ", " + imovel.getDescricaoComplemento());
					}
				}
			}

			if (imovel.getCondominio() != null && imovel.getCondominio()) {
				if (nome != null && !nome.isEmpty()) {
					request.setAttribute("nome", nome);
				} else {
					request.setAttribute("nome", imovel.getNome());
				}
			}

			if (numeroImovel != null && !numeroImovel.isEmpty()) {
				imovel.setNumeroImovel(numeroImovel);
			}

			defineDescricaoComplementoNome(nomeNovoBloco, imovel);

			QuadraFace qf = imovel.getQuadraFace();
			Endereco endereco = qf.getEndereco();
			Cep cep = endereco.getCep();
			for (PontoConsumo pontoConsumo : imovel.getListaPontoConsumo()) {
				if (quadraFace != null) {
					pontoConsumo.setCep(cep);
					pontoConsumo.setQuadraFace(qf);
				}
				if (nome != null && !nome.isEmpty() && imovel.getDescricaoComplemento() == null) {
					pontoConsumo.setDescricao(nome);
				}
				if (nomeNovoBloco != null && !nomeNovoBloco.isEmpty()) {
					pontoConsumo.setDescricao(imovel.getDescricaoComplemento());
				}
				if (numeroImovel != null && !numeroImovel.isEmpty()) {
					pontoConsumo.setNumeroImovel(numeroImovel);
				}
			}

		}

		atualizarListaImoveisFilhosSituacaoAlterada(request, listaImovel);

		controladorImovel.atualizarLogradouroImovel(listaImovel, super.getDadosAuditoria(request));

		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Imovel.IMOVEL_ROTULO));

		return FORWARD_PESQUISAR_IMOVEL;
	}

	private void defineDescricaoComplementoNome(String nomeNovoBloco, Imovel imovel) {
		String nomeFormatado = null;
		if (nomeNovoBloco != null && !nomeNovoBloco.isEmpty()) {
			String nomeImovel = imovel.getNome();
			String descricaoComplemento = null;
			String[] arrayNome = nomeImovel.split(",");
			nomeFormatado = arrayNome[0] + ", " + nomeNovoBloco;

			if (arrayNome.length == 2) {
				nomeFormatado = nomeFormatado + "," + arrayNome[1];
				descricaoComplemento = arrayNome[1];
			} else if (arrayNome.length >= 3) {
				nomeFormatado = nomeFormatado + "," + arrayNome[2];
				descricaoComplemento = nomeNovoBloco + arrayNome[2];
			}
			imovel.setNome(nomeFormatado);
			imovel.setDescricaoComplemento(descricaoComplemento);

		}
	}

	private void atualizarListaImoveisFilhosSituacaoAlterada(HttpServletRequest request, Collection<Imovel> listaImovel)
			throws GGASException {

		Long[] idsImoveis = (Long[]) request.getSession().getAttribute(LISTA_IMOVEIS_FILHOS_ALTERADOS);

		if (idsImoveis != null) {
			Collection<Imovel> listaImoveisFilhosAlterados = controladorQuadra.alterarSituacaoImoveis(idsImoveis);

			for (Imovel imovel : listaImovel) {
				for (Imovel imovelFilho : listaImoveisFilhosAlterados) {
					if (imovelFilho.getChavePrimaria() == imovel.getChavePrimaria()) {
						imovel.setSituacaoImovel(imovelFilho.getSituacaoImovel());
						break;
					}
				}
			}
			listaImovel.removeAll(listaImoveisFilhosAlterados);
			controladorImovel.atualizarColecao(listaImoveisFilhosAlterados, Imovel.class);
		}
	}

	/**
	 * Método responsável por exibir a tela de alterar imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelRelacionamentoVO - {@link ImovelRelacionamentoVO}
	 * @param imovelContatoVO - {@link ImovelContatoVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @param bindingResult - {@link BindingResultUtils}
	 * @param aba - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping(VIEW_EXIBIR_ALTERACAO_IMOVEL)
	public String exibirAlteracaoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelRelacionamentoVO imovelRelacionamentoVO, ImovelContatoVO imovelContatoVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO, BindingResult bindingResult,
			@RequestParam(value = "aba", required = false) String aba) throws GGASException {

		String view = VIEW_EXIBIR_ALTERACAO_IMOVEL;
		
		if (request.getSession().getAttribute("imovelPesquisaSessao") != null) {
			request.getSession().removeAttribute("imovelPesquisaSessao");
		}

		try {
			if (aba != null && ("5").equals(aba)) {
				model.addAttribute(ATRIBUTO_ABA_ID, ABA_PONTO_CONSUMO);
			}

			Imovel imovel = null;
			if (!isPostBack(request)) {
				Long chavePrimaria = null;
				
				if(imovelVO.getPontoConsumo() != null && imovelVO.getPontoConsumo().getImovel() != null && imovelVO.getPontoConsumo().getImovel().getChavePrimaria() > 0) {
					chavePrimaria = imovelVO.getPontoConsumo().getImovel().getChavePrimaria();
				} else {
					chavePrimaria = imovelVO.getChavePrimaria();
				}
				
				imovel = (Imovel) controladorImovel.obter(chavePrimaria,REDE_INTERNA_IMOVEL, QUADRA_FACE, QUADRA_FACE_ENDERECO,
						QUADRA_FACE_ENDERECO_CEP, ID_IMOVEL_CONDOMINIO);

				if (controladorIntegracao.integracaoPrecedenteContratoAtiva() &&
						!controladorIntegracao.validarIntegracaoContratoImovel(imovel.getChavePrimaria())) {
					throw new NegocioException(ERRO_INTEGRACAO_NAO_PROCESSADO_GENERICO, true);
				}

				this.popularForm(imovelVO, imovel, model);
			} else {
				this.popularSituacaoImovel(imovelVO);

			}

			this.carregarCampos2(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

			if (!isPostBack(request) && imovel != null && imovel.getQuadraFace() != null) {
				Collection<CityGate> listaCityGate = null;
				Collection<PontoConsumoCityGate> listaPontoConsumoCityGate = new ArrayList<PontoConsumoCityGate>();

				if (imovel.getNumeroImovel() != null) {
					imovelPontoConsumoVO.setNumeroImovelPontoConsumo(imovel.getNumeroImovel().toString());
				}

				if (imovel.getDescricaoComplemento() != null && imovel.getDescricaoComplemento().length() > LIMITE_CAMPO_DESCRICAO) {
					imovelPontoConsumoVO
							.setDescricaoPontoConsumo(imovel.getDescricaoComplemento().toString().substring(0, LIMITE_CAMPO_DESCRICAO));
				} else if(imovel.getDescricaoComplemento() != null){
					imovelPontoConsumoVO.setDescricaoPontoConsumo(imovel.getDescricaoComplemento().toString());
				}

				if (imovel.getEnderecoReferencia() != null) {
					imovelPontoConsumoVO.setEnderecoReferenciaPontoConsumo(imovel.getEnderecoReferencia().toString());
				}

				imovelPontoConsumoVO.setIdQuadraFacePontoConsumo(imovel.getQuadraFace().getChavePrimaria());
				imovelPontoConsumoVO.setIdQuadraPontoConsumo(imovel.getQuadraFace().getQuadra().getChavePrimaria());

				if (imovel.getTipoCombustivel() != null) {
					imovelVO.setIdTipoCombustivel(imovel.getTipoCombustivel().getChavePrimaria());
				}

				listaCityGate = controladorQuadra.listarCityGatePorQuadraFace(imovel.getQuadraFace().getChavePrimaria());
				this.carregarParametroPercentualCityGate(listaCityGate, imovelVO, listaPontoConsumoCityGate, model, request);
				request.setAttribute(LISTA_CITY_GATE, listaCityGate);
			}

			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);

			saveToken(request);

			this.atualizarModelo(model, imovelVO, imovelContatoVO, imovelRelacionamentoVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		} catch(NegocioException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_IMOVEL;
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_IMOVEL;
		}

		return view;
	}

	private void popularSituacaoImovel(ImovelVO imovelVO) throws GGASException {

		Long idSituacaoImovel = imovelVO.getIdSituacao();
		if (idSituacaoImovel == null || idSituacaoImovel < 0) {
			Long chavePrimaria = imovelVO.getChavePrimaria();
			Imovel imovel = (Imovel) controladorImovel.obter(chavePrimaria, REDE_INTERNA_IMOVEL, QUADRA_FACE, QUADRA_FACE_ENDERECO,
					QUADRA_FACE_ENDERECO_CEP);
			if (imovel.getSituacaoImovel() != null) {
				imovelVO.setIdSituacao(imovel.getSituacaoImovel().getChavePrimaria());
			}
		}
	}

	private void carregarCampos2(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		request.setAttribute("intervaloAnosAteDataAtual", intervaloAnosAteDataAtual());
		request.setAttribute("intervaloAnosData", intervaloAnosData());

		Map<String, String> datas;
		try {
			datas = Util.dataMinimaMaxima();
		} catch (Exception e) {
			throw new GGASException(e);
		}

		request.setAttribute(DATA_MINIMA, datas.get(DATA_MINIMA));
		request.setAttribute(DATA_MAXIMA, datas.get(DATA_MAXIMA));

		this.exibirDadosAbaIdentificacaoLocalizacao(model, imovelVO);
		this.exibirDadosAbaCaracteristicas(model, imovelVO);
		this.exibirDadosAbaRelacionamento2(imovelVO, model, request);
		this.exibirDadosAbaContato(imovelVO, request);
		this.exibirDadosAbaUnidadesConsumidoras(imovelVO, imovelUnidadeConsumidoraVO, request);
		this.exibirDadosAbaPontoConsumo(imovelVO, imovelPontoConsumoVO, model, request);
	}

	private String intervaloAnosAteDataAtual() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	private void exibirDadosAbaRelacionamento2(ImovelVO imovelVO, Model model, HttpServletRequest request) throws GGASException {

		Long chavePrimaria = imovelVO.getChavePrimaria();

		Collection<ClienteImovel> listaClienteImovel = new ArrayList<ClienteImovel>();

		if (!isPostBack(request)) {
			if (chavePrimaria != null && chavePrimaria != 0) {
				Collection<ClienteImovel> listarClienteImovelPorChaveImovelAux = new ArrayList<ClienteImovel>();
				boolean aux = false;
				for (ClienteImovel clienteImovel : controladorImovel.listarCliImovelPorChaveImovel(chavePrimaria)) {
					if (clienteImovel.getCliente().getEnderecoPrincipal() != null
							&& clienteImovel.getCliente().getEnderecoPrincipal().getCorrespondencia()) {
						listarClienteImovelPorChaveImovelAux.add(clienteImovel);
						aux = true;
					}
					if (!aux) {
						listarClienteImovelPorChaveImovelAux.add(clienteImovel);
					}
					aux = false;
				}

				listaClienteImovel.addAll(listarClienteImovelPorChaveImovelAux);
			}
			request.getSession().setAttribute(LISTA_CLIENTE_IMOVEL, listaClienteImovel);
		} else {
			listaClienteImovel = (Collection<ClienteImovel>) request.getSession().getAttribute(LISTA_CLIENTE_IMOVEL);
		}

		model.addAttribute(LISTA_MOTIVO_FIM_RELACIONAMENTO, controladorImovel.listarMotivoFimRelacionamentoClienteImovel());
		model.addAttribute(LISTA_TIPOS_RELACIONAMENTO, controladorImovel.listarTipoRelacionamentoClienteImovel());
		model.addAttribute(LISTA_CLIENTE_IMOVEL, exibirListaRelacionamento(listaClienteImovel));
	}

	/**
	 * Método responsável por alterar um imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @param imovelVO - {@link ImovelVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("alterarImovel")
	public String alterarImovel(Model model, HttpServletRequest request, HttpServletResponse response, ImovelVO imovelVO,
			BindingResult bindingResult) throws GGASException {

		String view = null;

		try {
			Long chavePrimaria = imovelVO.getChavePrimaria();
			Imovel imovel = (Imovel) controladorImovel.obter(chavePrimaria, REDE_INTERNA_IMOVEL, "listaClienteImovel",
					"unidadesConsumidoras", "contatos", LISTA_PONTO_CONSUMO, QUADRA_FACE, QUADRA_FACE_ENDERECO, QUADRA_FACE_ENDERECO_CEP);

			// FIXME: Parâmetro substituido por constante
			String codigoSitucaoImovel = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO);

			if (imovel.getSituacaoImovel().getChavePrimaria() == Long.parseLong(codigoSitucaoImovel)) {
				imovelVO.setIdSituacao(Long.parseLong(codigoSitucaoImovel));
			}

			popularImovel(request, imovel, imovelVO);
			if (imovel.getImovelCondominio()!=null) {
				imovelVO.setIdImovelCondominio(imovel.getImovelCondominio().getChavePrimaria());
			}

			imovel.setDadosAuditoria(getDadosAuditoria(request));

			controladorImovel.atualizarImovel(imovel);
			imovelVO.setChavePrimaria(chavePrimaria);

			ImovelPesquisaVO imovelPesquisa = new ImovelPesquisaVO();
			popularPesquisaVO(imovel, imovelPesquisa, model);

			Boolean indicadorAtualizacaoCadastralImediata = (Boolean) request.getSession().getAttribute("indicadorAtualizacaoCadastral");
			if (indicadorAtualizacaoCadastralImediata != null) {
				String urlRetornoAtualizacaoCastral = (String) request.getSession().getAttribute("urlRetornoAtualizacaoCastral");
				this.redirecionarParaAtualizacaoCadastral(response, urlRetornoAtualizacaoCastral);
			}

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Imovel.IMOVEL_ROTULO));
			imovelVO.setDescricaoPontoConsumo("");
			view = this.pesquisarImovel(imovelPesquisa, bindingResult, null, request, model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		} catch(DataIntegrityViolationException | ConstraintViolationException e) {
			super.mensagemErroParametrizado(model, request,
					new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, Boolean.TRUE));
			view = FORWARD_EXIBIR_ALTERACAO_IMOVEL;
		}

		return view;
	}

	private void redirecionarParaAtualizacaoCadastral(HttpServletResponse response, String urlRetornoAtualizacaoCastral)
			throws GGASException {
		try {
			response.sendRedirect(urlRetornoAtualizacaoCastral);
		} catch (IOException e) {
			LOG.error(e);
			throw new GGASException(e);
		}
	}

	/**
	 * Método responsável por exibir a tela do popup de pesquisa imóvel.
	 *
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaImovelPopup")
	public String exibirPesquisaImovelPopup() {

		return "exibirPesquisaImovelPopup";
	}

	/**
	 * Método responsável por realizar a consulta do popup de pesquisa de imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelPesquisaVO - {@link ImovelPesquisaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarImovelPopup")
	public String pesquisarImovelPopup(Model model, HttpServletRequest request, ImovelPesquisaVO imovelPesquisaVO,
			BindingResult bindingResult) {

		model.addAttribute(IMOVEL_PESQUISA_VO, imovelPesquisaVO);

		try {
			Map<String, Object> filtro = new HashMap<String, Object>();

			prepararFiltroPopup(filtro, imovelPesquisaVO);

			super.adicionarFiltroPaginacao(request, filtro);

			Collection<?> listaImoveis = controladorImovel.consultarImoveis(filtro);

			model.addAttribute(LISTA_IMOVEIS, super.criarColecaoPaginada(request, filtro, listaImoveis));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaImovelPopup";
	}

	/**
	 * Preparar filtro popup.
	 *
	 * @param filtro - {@link Map}
	 * @param imovelPesquisaVO - {@link ImovelPesquisaVO}
	 */
	private void prepararFiltroPopup(Map<String, Object> filtro, ImovelPesquisaVO imovelPesquisaVO) throws FormatoInvalidoException {

		String cep = imovelPesquisaVO.getCepImovel();
		if (!StringUtils.isEmpty(cep)) {
			filtro.put(CEP_IMOVEL, cep);
		}

		String nomeCliente = imovelPesquisaVO.getNomeCliente();
		if (!StringUtils.isEmpty(nomeCliente)) {
			filtro.put("nomeCliente", nomeCliente);
		}

		String nomeFantasia = imovelPesquisaVO.getNomeFantasia();
		if (!StringUtils.isEmpty(nomeFantasia)) {
			filtro.put("nome", nomeFantasia);
		}

		Boolean habilitadoImovel = imovelPesquisaVO.getHabilitado();
		if (habilitadoImovel != null) {
			filtro.put("habilitado", habilitadoImovel);
		}

		String chavePrimaria = imovelPesquisaVO.getMatriculaImovel();
		if (!StringUtils.isEmpty(chavePrimaria)) {
			filtro.put(CHAVE_PRIMARIA, Long.valueOf(chavePrimaria));
		}

		String complementoImovel = imovelPesquisaVO.getComplementoImovel();
		if (!StringUtils.isEmpty(complementoImovel)) {
			filtro.put(COMPLEMENTO_IMOVEL, complementoImovel);
		}

		String numeroImovel = imovelPesquisaVO.getNumeroImovel();
		if (!StringUtils.isEmpty(numeroImovel)) {
			filtro.put(NUMERO_IMOVEL, numeroImovel);
		}

		String indicadorCondominioAmbos = imovelPesquisaVO.getIndicadorCondominioAmbos();
		if (!StringUtils.isEmpty(indicadorCondominioAmbos)) {
			filtro.put("indicadorCondominio", Boolean.valueOf(indicadorCondominioAmbos));
		}

		String idPontoConsumoLegado = imovelPesquisaVO.getPontoConsumoLegado();
		if (idPontoConsumoLegado != null && !StringUtils.isEmpty(idPontoConsumoLegado)) {
			filtro.put(PONTO_CONSUMO_LEGADO, idPontoConsumoLegado);
		}

	}

	/**
	 * Método responsável por exibir a tela do popup de pesquisa(completa) imóvel.
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirPesquisaImovelCompletoPopup")
	public String exibirPesquisaImovelCompletoPopup(Model model) {

		ImovelPesquisaVO imovelPesquisaVO = new ImovelPesquisaVO();
		imovelPesquisaVO.setHabilitado(Boolean.TRUE);

		model.addAttribute(IMOVEL_PESQUISA_VO, imovelPesquisaVO);

		return "exibirPesquisaImovelCompletoPopup";
	}

	/**
	 * Método responsável por pesquisar um imóvel atraves do popup de "Pesquisa Imovel".
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelPesquisaVO - {@link ImovelPesquisaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException The GGASException
	 *
	 */
	@RequestMapping("pesquisarImovelCompletoPopup")
	public String pesquisarImovelCompletoPopup(Model model, HttpServletRequest request, ImovelPesquisaVO imovelPesquisaVO,
			BindingResult bindingResult) throws GGASException {
		try {

			imovelPesquisaVO.setChavesPrimarias(null);
			model.addAttribute(IMOVEL_PESQUISA_VO, imovelPesquisaVO);

			Map<String, Object> filtro = new HashMap<String, Object>();
			this.prepararFiltroPopup(filtro, imovelPesquisaVO);
			super.adicionarFiltroPaginacao(request, filtro);

			Collection<?> listaImoveis = controladorImovel.consultarImoveis(filtro);

			model.addAttribute(LISTA_IMOVEIS, super.criarColecaoPaginada(request, filtro, listaImoveis));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaImovelCompletoPopup";
	}

	/**
	 * Método responsável por exibir a tela do popup de pesquisa pontos de consumo não associados a uma rota e pontos de consumo não
	 * pertencentes a um condomínio com medição coletiva.
	 *
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPopupPesquisaPontoConsumoRota")
	public String exibirPopupPesquisaPontoConsumoRota(Model model) throws GGASException {

		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		return "exibirPesquisaPontoConsumoRotaPopup";
	}

	@RequestMapping("pesquisarPontoConsumoRota")
	public String pesquisarPontoConsumoRota(Model model, HttpServletRequest request, ImovelPesquisaVO imovelPesquisaVO,
			BindingResult bindingResult) throws GGASException {
		try {

			model.addAttribute(IMOVEL_PESQUISA_VO, imovelPesquisaVO);
			Map<String, Object> filtro = new HashMap<String, Object>();


			String descricao = imovelPesquisaVO.getDescricaoPontoConsumo();
			if (!StringUtils.isEmpty(descricao)) {

				filtro.put(DESCRICAO_PONTO_CONSUMO, descricao);
			}

			Long idSegmento = imovelPesquisaVO.getIdSegmentoPontoConsumo();
			if (idSegmento != null && idSegmento != -1) {
				filtro.put(ID_SEGMENTO_PONTO_CONSUMO, idSegmento);
				model.addAttribute(LISTA_RAMO_ATIVIDADE_PONTO_CONSUMO, controladorRamoAtividade.listarRamoAtividadePorSegmento(idSegmento));
			}

			Long idRamoAtividade = imovelPesquisaVO.getIdRamoAtividadePontoConsumo();
			if (idRamoAtividade != null && idRamoAtividade != -1) {
				filtro.put(ID_RAMO_ATIVIDADE_PONTO_CONSUMO, idRamoAtividade);
			}


			String nomeImovel = imovelPesquisaVO.getNome();
			if (!StringUtils.isEmpty(nomeImovel)) {

				filtro.put(NOME, nomeImovel);
			}

			String matricula = imovelPesquisaVO.getMatriculaImovel();
			if (!StringUtils.isEmpty(matricula)) {
				filtro.put(MATRICULA, Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matricula));
			}

			String nomeImovelCondominio = imovelPesquisaVO.getNomeImovelCondominio();
			if (!StringUtils.isEmpty(nomeImovelCondominio)) {
				filtro.put(NOME_IMOVEL_CONDOMINIO, nomeImovelCondominio);
			}

			Integer situacaoContrato = imovelPesquisaVO.getSituacaoContrato();
			if (situacaoContrato != null) {
				if (situacaoContrato == 1) {
					long situacaoAtivo =
							Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
					filtro.put(SITUACAO_CONTRATO, situacaoAtivo);
				}
				if (situacaoContrato == 0) {
					long situacaoEncerrado = Long.parseLong(
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ENCERRADO));
					filtro.put(SITUACAO_CONTRATO, situacaoEncerrado);
				}
			}
			Integer situacaoMedidor = imovelPesquisaVO.getSituacaoMedidor();
			if (situacaoMedidor != null) {
				if (situacaoMedidor == 1) {
					long situacaoConsumoAtivo = Long.parseLong(
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO));
					filtro.put(SITUACAO_MEDIDOR, situacaoConsumoAtivo);
				}
				if (situacaoMedidor == 0) {
					long situacaoConsumoAguardandoAtivacao = Long.parseLong(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO));
					filtro.put(SITUACAO_MEDIDOR, situacaoConsumoAguardandoAtivacao);
				}
			}
			Date dataInicioAssinatura = null;
			if (!StringUtils.isEmpty(imovelPesquisaVO.getDataInicioAssinaturaContrato())) {
				dataInicioAssinatura = Util.converterCampoStringParaData("Data Inicio Assinatura",
						imovelPesquisaVO.getDataInicioAssinaturaContrato(), Constantes.FORMATO_DATA_BR);
				filtro.put(DATA_INICIO_ASSINATURA_CONTRATO, dataInicioAssinatura);
			}

			if (!StringUtils.isEmpty(imovelPesquisaVO.getDataFimAssinaturaContrato())) {
				Date dataFimAssinatura = Util.converterCampoStringParaData("Data Fim Assinatura",
						imovelPesquisaVO.getDataFimAssinaturaContrato(), Constantes.FORMATO_DATA_BR);
				filtro.put(DATA_FIM_ASSINATURA_CONTRATO, dataFimAssinatura);
				if(dataInicioAssinatura != null && dataInicioAssinatura.after(dataFimAssinatura)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
			}
			Date dataInicioAtivacao = null;
			if (!StringUtils.isEmpty(imovelPesquisaVO.getDataInicioAtivacaoMedidor())) {
				dataInicioAtivacao = Util.converterCampoStringParaData("Data Inicio Ativação",
						imovelPesquisaVO.getDataInicioAtivacaoMedidor(), Constantes.FORMATO_DATA_BR);
				filtro.put(DATA_INICIO_ATIVACAO_MEDIDOR, dataInicioAtivacao);
			}

			if (!StringUtils.isEmpty(imovelPesquisaVO.getDataFimAtivacaoMedidor())) {
				Date dataFimAtivacao = Util.converterCampoStringParaData("Data Fim Ativação",
						imovelPesquisaVO.getDataFimAtivacaoMedidor(), Constantes.FORMATO_DATA_BR);
				filtro.put(DATA_FIM_ATIVACAO_MEDIDOR, dataFimAtivacao);

				if(dataInicioAtivacao != null && dataInicioAtivacao.after(dataFimAtivacao)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
			}

			List<Long> chavesPontoConsumo = (List<Long>) request.getSession().getAttribute("listaChavesPontoConsumo");

			Collection<PontoConsumo> listaPontoConsumoRota = controladorImovel.listarPontoConsumoSemRota(filtro, chavesPontoConsumo);
			controladorPontoConsumo.removerImovelFilhoComPaiMedicaoColetiva((List<PontoConsumo>) listaPontoConsumoRota);
			request.setAttribute(LISTA_PONTO_CONSUMO, prepararListaParaExibicaoImovel(listaPontoConsumoRota, false));

		} catch (GGASException e) {

			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPopupPesquisaPontoConsumoRota(model);
	}

	/**
	 * Método responsável por exibir uma tela de inclusão de contato.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param chavePrimaria - {@link String}
	 * @param paramAlteracaoLista - {@link String}
	 * @return view - {@link String}
	 * @throws GGASException O GGASException
	 */
	@RequestMapping("exibirPopupContatosImovel")
	public String exibirPopupContatosImovel(Model model, HttpServletRequest request, ImovelVO imovelVO, BindingResult bindingResult,
			@RequestParam(value = "chavePrimaria", required = false) String chavePrimaria,
			@RequestParam(value = "alteracaoLista", required = false) String paramAlteracaoLista) throws GGASException {

		Long idImovel = null;
		if (StringUtils.isNotEmpty(chavePrimaria)) {
			idImovel = Long.parseLong(chavePrimaria);
		} else {
			idImovel = imovelVO.getChavePrimaria();
		}

		Boolean alteracaoLista = Boolean.parseBoolean(paramAlteracaoLista);
		if (alteracaoLista != null && !alteracaoLista) {
			request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, Boolean.FALSE);
		}

		saveToken(request);
		Imovel imovel = (Imovel) controladorImovel.obter(idImovel);

		imovelVO.setNome(imovel.getNome());
		imovelVO.setChavePrimaria(idImovel);

		this.exibirDadosAbaContato(imovelVO, request);

		model.addAttribute(IMOVEL_VO, imovelVO);

		return "popupContatosImovel";
	}

	/**
	 * Carregar imoveis em lote erro.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelLoteVO - {@link ImovelLoteVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException o GGASExcption
	 *
	 */
	@RequestMapping("carregarImoveisEmLoteErro")
	public String carregarImoveisEmLoteErro(Model model, HttpServletRequest request, ImovelVO imovelVO, ImovelLoteVO imovelLoteVO,
			BindingResult bindingResult) throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_EM_LOTE;

		try {
			carregarCamposImoveisLote(request, imovelVO, imovelLoteVO, false);
			model.addAttribute(IMOVEL_VO, imovelVO);
			model.addAttribute(IMOVEL_LOTE_VO, imovelLoteVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_EM_LOTE;
		}

		return view;
	}

	/**
	 * Popup exibir inclusao imovel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param imovelUnidadeConsumidoraVO - {@link ImovelUnidadeConsumidoraVO}
	 * @param imovelPontoConsumoVO - {@link ImovelPontoConsumoVO}
	 * @return view - {@link String}
	 * @throws o GGASException
	 *
	 */
	@RequestMapping("popupExibirInclusaoImovel")
	public String popupExibirInclusaoImovel(Model model, HttpServletRequest request, ImovelVO imovelVO,
			ImovelUnidadeConsumidoraVO imovelUnidadeConsumidoraVO, ImovelPontoConsumoVO imovelPontoConsumoVO) throws GGASException {

		this.carregarCamposVeioDochamado(model, request, imovelVO, imovelUnidadeConsumidoraVO, imovelPontoConsumoVO);

		imovelVO.setVeioDoChamado(Boolean.TRUE);

		model.addAttribute(IMOVEL_VO, imovelVO);
		model.addAttribute(IMOVEL_UNIDADE_CONSUMIDORA_VO, imovelUnidadeConsumidoraVO);
		model.addAttribute(IMOVEL_PONTO_CONSUMO_VO, imovelPontoConsumoVO);

		saveToken(request);

		return VIEW_EXIBIR_INCLUSAO_IMOVEL;
	}

	/**
	 * Método responsável por salvar lista de contato do imóvel.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param imovelVO - {@link ImovelVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param chavePrimaria - {@link String}
	 * @return view - {@link String}
	 * @throws GGASException o GGASException
	 *
	 */
	@RequestMapping("salvarContatosImovel")
	public String salvarContatosImovel(Model model, HttpServletRequest request, ImovelVO imovelVO, BindingResult bindingResult,
			@RequestParam(value = "chavePrimaria", required = false) String chavePrimaria) throws GGASException {

		try {
			validarToken(request);

			Long idImovel = null;
			if (StringUtils.isNotEmpty(chavePrimaria)) {
				idImovel = Long.parseLong(chavePrimaria);
			} else {
				idImovel = imovelVO.getChavePrimaria();
			}
			Imovel imovel = (Imovel) controladorImovel.obter(idImovel);
			QuadraFace quadraFace = imovel.getQuadraFace();

			imovel.setQuadra(quadraFace.getQuadra());
			imovel.setCep(quadraFace.getEndereco().getCep().getCep());

			popularContatos(imovel, request);

			controladorImovel.atualizarImovel(imovel);
			request.getSession().setAttribute(LISTA_CONTATO_MODIFICADA, Boolean.FALSE);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Imovel.IMOVEL_ROTULO));

			imovelVO.setNome(imovel.getNome());
			imovelVO.setChavePrimaria(idImovel);

			this.exibirDadosAbaContato(imovelVO, request);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_EXIBIR_POPUP_CONTATOS_IMOVEL;
	}

	/**
	 * Alterar logradouro imoveis filhos.
	 *
	 * @param session - {@link HttpSession}
	 * @param imovelVO - {@link ImovelVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException o GGASException
	 *
	 */
	@RequestMapping("alterarLogradouroImoveisFilhos")
	public String alterarLogradouroImoveisFilhos(Model model, HttpSession session, ImovelVO imovelVO, BindingResult bindingResult)
			throws GGASException {

		Long[] chavesPrimarias = imovelVO.getChavesPrimarias();

		session.setAttribute(LISTA_IMOVEIS_FILHOS_ALTERADOS, chavesPrimarias);

		model.addAttribute(IMOVEL_VO, imovelVO);

		return this.exibirPopupAlteracaoLogradouro(model);
	}

	/**
	 * Metodo para Agrupar os pontos de consumo pelo imovel pai
	 * @param listaPontoConsumoRota Lista com os pontos para exibicao
	 * @param exibeDescricao Se exibe ou nao a descricao ou nao do ponto de consumo
	 * @return Lista grupada
	 */
	public Collection<PontoConsumo> prepararListaParaExibicaoImovel(Collection<PontoConsumo> listaPontoConsumoRota, boolean exibeDescricao) {
		Collection<PontoConsumo> listaRetorno = new ArrayList<PontoConsumo>();
		Collection<PontoConsumo> listaPais = new ArrayList<PontoConsumo>();
		Collection<PontoConsumo> listaFilhos = new ArrayList<PontoConsumo>();
		if (listaPontoConsumoRota!=null && !listaPontoConsumoRota.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
				if (pontoConsumo.getImovel().getCondominio()) {
					listaPais.add(pontoConsumo);
				}else {
					listaFilhos.add(pontoConsumo);
				}
			}
			alimentaListaPaiImovel(listaPais, listaFilhos, listaRetorno, exibeDescricao);
			alimentaListaFilhosImovel(listaFilhos, listaRetorno, exibeDescricao);
		}
		return listaRetorno;
	}

	/**
	 * Metodo criado para alimentar a lista de imoveis filhos
	 * @param listaPais Lista com os imoveis pai
	 * @param listaFilhos Lsita com os imoveis filhos
	 * @param listaRetorno Lista retorno com os pais e filhos rganizados
	 * @param exibeDescricao Se vai exibir a descricao do imovel ou nao.
	 */
	private void alimentaListaFilhosImovel(Collection<PontoConsumo> listaFilhos,
			Collection<PontoConsumo> listaRetorno, boolean exibeDescricao) {
		if (!listaFilhos.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaFilhos) {
				if (!verificaPontoConsumoNaListaImovel(pontoConsumo, listaRetorno)) {
					organizaPontoConsumoImovel(pontoConsumo, listaRetorno, exibeDescricao, listaFilhos);
				}
			}
		}
	}

	/**
	 * Metodo criado para organizao o ponto de consumo entre pai e filho
	 * @param pontoConsumo Ponto de consumo a ser organizado
	 * @param listaRetorno Lista de retorno final com os pontos de consumo
	 * @param exibeDescricao Se exibe a descricao ou nao
	 * @param listaFilhos Lista com os imoveis filhos.
	 */
	private void organizaPontoConsumoImovel(PontoConsumo pontoConsumo, Collection<PontoConsumo> listaRetorno, boolean exibeDescricao,
			Collection<PontoConsumo> listaFilhos) {
		if (pontoConsumo.getImovel().getIdImovelCondominio()!= null) {
			PontoConsumo pontoConsumoImovelPai = recuperaAgrupamentoPaiPontoConsumoImovel(
					pontoConsumo.getImovel().getIdImovelCondominio(), listaRetorno);
			if (pontoConsumoImovelPai == null) {
				pontoConsumoImovelPai = new PontoConsumoImpl();
				pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel().getImovelCondominio());
				pontoConsumoImovelPai.setListaPontosConsumoImoveisFilho(new ArrayList<>());
				if (exibeDescricao) {
					pontoConsumoImovelPai.setDescricao(pontoConsumo.getImovel().getNome());
				}
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().addAll(preencheImoveisFilhosImovel(
					pontoConsumo.getImovel().getIdImovelCondominio(), listaFilhos));
				listaRetorno.add(pontoConsumoImovelPai);
			}else {
				pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
			}
		}else {
			listaRetorno.add(pontoConsumo);
		}

	}

	/**
	 * Metodo criado para alimentar a lista de imoveis pai
	 * @param listaPais Lista com os imoveis pai
	 * @param listaFilhos Lsita com os imoveis filhos
	 * @param listaRetorno Lista retorno com os pais e filhos rganizados
	 * @param exibeDescricao Se vai exibir a descricao do imovel ou nao.
	 */
	private void alimentaListaPaiImovel(Collection<PontoConsumo> listaPais, Collection<PontoConsumo> listaFilhos,
			Collection<PontoConsumo> listaRetorno, boolean exibeDescricao) {
		if (!listaPais.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPais) {
				PontoConsumo pontoConsumoImovelPai = recuperaAgrupamentoPaiPontoConsumoImovel(pontoConsumo.getImovel().getChavePrimaria(),
						listaRetorno);
				if (pontoConsumoImovelPai == null) {
					pontoConsumoImovelPai = criaImovelPaiImovel(pontoConsumo, exibeDescricao, listaFilhos);
					listaRetorno.add(pontoConsumoImovelPai);
				}else {
					pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
				}

			}
		}

	}

	/**
	 * Metodo criado para adicionar os dados no imovel pai
	 * @param pontoConsumo Ponto de consumo corrente
	 * @param exibeDescricao Se no campo descricao, vamos motrar a descricao do imovel
	 * @param listaFilhos lista com os imoveis de condominio definidos ate o momnoto
	 * @return Ponto de consumo pronto para a lista
	 */
	private PontoConsumo criaImovelPaiImovel(PontoConsumo pontoConsumo, boolean exibeDescricao, Collection<PontoConsumo> listaFilhos) {
		PontoConsumo pontoConsumoImovelPai = new PontoConsumoImpl();
		pontoConsumoImovelPai.setImovel(pontoConsumo.getImovel());
		pontoConsumoImovelPai.setListaPontosConsumoImoveisFilho(new ArrayList<>());
		pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().add(pontoConsumo);
		if (exibeDescricao) {
			pontoConsumoImovelPai.setDescricao(pontoConsumo.getImovel().getNome());
		}
		pontoConsumoImovelPai.getListaPontosConsumoImoveisFilho().addAll(preencheImoveisFilhosImovel(
			pontoConsumo.getImovel().getChavePrimaria(), listaFilhos));
		return pontoConsumoImovelPai;
	}

	/**
	 * Verifica se o Imovel Pai ja esta na lista de retorno para nao duplicar
	 * @param pontoConsumo Ponto de consumo pai
	 * @param listaRetorno Lista Final
	 * @return Objeto existente
	 */
	private PontoConsumo recuperaAgrupamentoPaiPontoConsumoImovel(long chavePrimariaImovel,
			Collection<PontoConsumo> listaRetorno) {
		for (PontoConsumo pontoConsumoExistente : listaRetorno) {
			if (pontoConsumoExistente.getImovel().getChavePrimaria() == chavePrimariaImovel) {
				return pontoConsumoExistente;
			}
		}
		return null;
	}


	/**
	 * Verifica se o item esta na lista de retorno
	 * @param pontoConsumo Ponto de Consumo de referencia para ser verificado
	 * @param listaRetorno Lista de retorno para a tela
	 * @return Se o ponto ja esta na lista de retono ou nao
	 */
	private boolean verificaPontoConsumoNaListaImovel(PontoConsumo pontoConsumo, Collection<PontoConsumo> listaRetorno) {
		for (PontoConsumo pontoConsumoExistente : listaRetorno) {
			if (pontoConsumoExistente.getChavePrimaria() != 0) {
				if (pontoConsumo.getChavePrimaria() == pontoConsumoExistente.getChavePrimaria()) {
					return true;
				}
			}else {
				if (verificaPontoConsumoListaImovelPai(pontoConsumoExistente.getListaPontosConsumoImoveisFilho(), pontoConsumo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo para verificar se o ponto de consumo ja existe na lista como filho de alguem
	 * @param list pontoConsumoExistente.getListaPpntoConsumoImoveisFilho
	 * @param pontoConsumo Ponto de consumo a ser verificado
	 * @return Se o ponto ja existe na lista ou nao
	 */
	private boolean verificaPontoConsumoListaImovelPai(List<PontoConsumo> list, PontoConsumo pontoConsumo) {
		for (PontoConsumo pontoConsumoFilho : list) {
			if (pontoConsumoFilho.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para percorrer a lista e procurar um imovel filho para agrupar
	 * @param chavePrimaria Identificaxdor do imovel pai
	 * @param listaPontoConsumoRota Lista completa com os resultados
	 * @return Lista agrupada com os imoveis filhos
	 */
	private List<PontoConsumo> preencheImoveisFilhosImovel(long chavePrimaria,
			Collection<PontoConsumo> listaPontoConsumoRota) {
		List<PontoConsumo> retorno = new ArrayList<PontoConsumo>();
		for (PontoConsumo pontoConsumo : listaPontoConsumoRota) {
			if (pontoConsumo.getImovel().getIdImovelCondominio()!=null
					&& pontoConsumo.getImovel().getIdImovelCondominio() == chavePrimaria) {
				retorno.add(pontoConsumo);
			}
		}
		return retorno;
	}

}
