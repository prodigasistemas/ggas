/*
 * 
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 * 
 */
package br.com.ggas.web.cadastro.operacional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.operacional.ControladorRamal;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.Ramal;
import br.com.ggas.cadastro.operacional.impl.RamalImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas ações referentes ao manter Ramal.
 * 
 * @author esantana
 *
 */
@Controller
public class RamalAction extends GenericAction {

	private static final String FORWARD_PESQUISAR_RAMAL = "forward:/pesquisarRamal";
	private static final String ATRIBUTO_FILTRO_HABILITADO = "filtroHabilitado";
	private static final String ATRIBUTO_FILTRO_PESQUISA = "filtroPesquisa";
	private static final String RAMAL = "ramal";
	private static final String LISTA_REDE = "listaRede";
	private static final String NOME = "nome";
	private static final String ID_REDE = "idRede";

	@Autowired
	private ControladorRede controladorRede;

	@Autowired
	private ControladorRamal controladorRamal;

	/**
	 * Método responsável por exibir a tela de pesquisa de Ramal.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param habilitado -{@link Boolean}
	 * @return view - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaRamal")
	public String exibirPesquisaRamal(Model model,
			@RequestParam(value = "habilitado", required = false, defaultValue = "true") Boolean habilitado)
			throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_REDE, controladorRede.consultarRedes(filtro));
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		return "exibirPesquisaRamal";
	}

	/**
	 * Método responsável por realizar a pesquisa por Ramal.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param ramal
	 *            - {@link RamalImpl}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return view - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarRamal")
	public String pesquisarRamal(HttpServletRequest request, Model model, RamalImpl ramal, BindingResult bindingResult,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		super.saveToken(request);

		model.addAttribute(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, null);

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, ramal, habilitado);

		Collection<Ramal> listaRamal = controladorRamal.consultarRamal(filtro);

		model.addAttribute("listaRamal", listaRamal);
		model.addAttribute(RAMAL, ramal);
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);

		return this.exibirPesquisaRamal(model, habilitado);
	}

	/**
	 * Método responsável por detalhar um ramal.
	 * 
	 * @param ramal
	 *            - {@link RamalImpl}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoRamal")
	public String exibirDetalhamentoRamal(RamalImpl ramal, BindingResult bindingResult, HttpServletRequest request,
			Model model, @RequestParam(value = "habilitado", required = false) Boolean habilitado)
			throws GGASException {

		model.addAttribute(ATRIBUTO_FILTRO_PESQUISA, ramal);
		model.addAttribute(ATRIBUTO_FILTRO_HABILITADO, habilitado);

		if (!super.isPostBack(request)) {
			model.addAttribute(RAMAL, controladorRamal.obter(ramal.getChavePrimaria()));
		}

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		request.setAttribute(LISTA_REDE, controladorRede.consultarRedes(filtro));

		saveToken(request);

		return "exibirDetalhamentoRamal";
	}

	/**
	 * Método responsável por exibir a tela para alteração de um Ramal.
	 * 
	 * @param ramal
	 *            - {@link RamalImpl}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirAtualizacaoRamal")
	public String exibirAtualizacaoRamal(RamalImpl ramal, BindingResult bindingResult, HttpServletRequest request,
			Model model, @RequestParam(value = "habilitado", required = false) Boolean habilitado)
			throws GGASException {

		if (!super.isPostBack(request)) {
			model.addAttribute(ATRIBUTO_FILTRO_PESQUISA, ramal);
			model.addAttribute(ATRIBUTO_FILTRO_HABILITADO, habilitado);
			model.addAttribute(RAMAL, controladorRamal.obter(ramal.getChavePrimaria()));
		} else {
			this.manterFiltroPesquisa(request, model);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		request.setAttribute(LISTA_REDE, controladorRede.consultarRedes(filtro));

		saveToken(request);

		return "exibirAtualizacaoRamal";
	}

	/**
	 * Método responsável por atualizar um Ramal.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param ramal
	 *            - {@link RamalImpl}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("atualizarRamal")
	public String atualizarRamal(HttpServletRequest request, RamalImpl ramal, BindingResult bindingResult,
			Model model) {
		String view = FORWARD_PESQUISAR_RAMAL;

		model.addAttribute(RAMAL, ramal);

		try {
			validarToken(request);

			this.popularRamal(ramal, request);

			controladorRamal.atualizar(ramal);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA,
					super.obterMensagem(Ramal.RAMAL_CAMPO_STRING));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirAtualizacaoRamal";
		}

		return view;
	}

	/**
	 * Método responsável por excluir ramais.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param chavesPrimarias
	 * @return view - {@link String}
	 */
	@RequestMapping("excluirRamais")
	public String excluirRamais(HttpServletRequest request, Model model,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias) {

		try {
			validarToken(request);

			controladorRamal.remover(chavesPrimarias, getDadosAuditoria(request));
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA,
					super.obterMensagem(Ramal.RAMAL_CAMPO_STRING));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new GGASException(ControladorRamal.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return FORWARD_PESQUISAR_RAMAL;
	}

	/**
	 * Método responsável por exibir a tela de inserção de ramais.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirInserirRamal")
	public String exibirInserirRamal(HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);

		if (!isPostBack(request)) {
			Ramal ramal = (Ramal) controladorRamal.criar();
			ramal.setRede(null);
			model.addAttribute(RAMAL, ramal);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addAttribute(LISTA_REDE, controladorRede.consultarRedes(filtro));

		return "exibirInserirRamal";
	}

	/**
	 * Método responsável por inserir um Ramal.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param ramal
	 *            - {@link RamalImpl}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("inserirRamal")
	public String inserirRamal(HttpServletRequest request, RamalImpl ramal, BindingResult bindingResult, Model model) {

		String view = FORWARD_PESQUISAR_RAMAL;

		model.addAttribute(RAMAL, ramal);

		try {
			validarToken(request);

			popularRamal(ramal, request);

			ramal.setHabilitado(Boolean.TRUE);
			controladorRamal.inserir(ramal);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA,
					super.obterMensagem(Ramal.RAMAL_CAMPO_STRING));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirInserirRamal";
		}

		return view;
	}

	/**
	 * 
	 * @param ramal
	 * @param request
	 * @throws GGASException
	 */
	private void popularRamal(Ramal ramal, HttpServletRequest request) throws GGASException {

		if (!StringUtils.isEmpty(ramal.getNome())) {
			Util.validarDominioCaracteres(Ramal.NOME_RAMAL, ramal.getNome(),
					Constantes.EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME);
		}

		ramal.setDadosAuditoria(getDadosAuditoria(request));
	}

	/**
	 * Método responsável por preparar filtro para pesquisa
	 * 
	 * @param filtro
	 * @param ramal
	 * @param habilitado
	 * @throws FormatoInvalidoException
	 */
	private void prepararFiltro(Map<String, Object> filtro, Ramal ramal, Boolean habilitado)
			throws FormatoInvalidoException {

		if (ramal != null) {
			if (ramal.getChavePrimaria() > 0) {
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, ramal.getChavePrimaria());
			}

			if (!StringUtils.isEmpty(ramal.getNome())) {
				Util.validarDominioCaracteres(Ramal.NOME_RAMAL, ramal.getNome(),
						Constantes.EXPRESSAO_REGULAR_DOMINIO_CARACTERES_VALIDOS_NOME);
				filtro.put(NOME, ramal.getNome());
			}

			Rede rede = ramal.getRede();
			if ((rede != null) && (rede.getChavePrimaria() > 0)) {
				filtro.put(ID_REDE, rede.getChavePrimaria());
			}

			if (habilitado != null) {
				filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
			}
		}
	}

	/**
	 * Método responsável por manter filtro de pesquisa
	 * 
	 * @param request
	 * @param model
	 * @throws GGASException
	 * 
	 */
	private void manterFiltroPesquisa(HttpServletRequest request, Model model) throws GGASException {

		Ramal filtroPesquisa = (Ramal) controladorRamal.criar();

		filtroPesquisa.setNome(String.valueOf(request.getParameter("filtroNome")));

		String idRede = request.getParameter("filtroRede");
		if (StringUtils.isNotBlank(idRede)) {
			filtroPesquisa.setRede((Rede) controladorRede.obter(Long.parseLong(idRede)));
		}

		if (request.getParameter(ATRIBUTO_FILTRO_HABILITADO) != null) {
			model.addAttribute(ATRIBUTO_FILTRO_HABILITADO, request.getParameter(ATRIBUTO_FILTRO_HABILITADO));
		}

		model.addAttribute(ATRIBUTO_FILTRO_PESQUISA, filtroPesquisa);
	}
}
