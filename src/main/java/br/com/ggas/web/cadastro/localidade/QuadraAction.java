/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.localidade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.dadocensitario.ControladorSetorCensitario;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.localidade.ControladorAreaTipo;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorPerfilQuadra;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.ControladorZeis;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.impl.LocalidadeImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraImpl;
import br.com.ggas.cadastro.localidade.impl.SetorComercialImpl;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.ControladorZonaBloqueio;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelos métodos relacionados as telas de Quadra
 */
@Controller
public class QuadraAction extends GenericAction {

	private static final String ID_REDE = "idRede";

	private static final int TEM_PARCIALMENTE = 3;

	private static final int TEM = 1;

	private static final int NAO_TEM = 2;

	private static final String TELA_EXIBIR_INSERCAO_QUADRA = "exibirInsercaoQuadra";

	private static final String REDE_INDICADOR = "redeIndicador";

	private static final String CEP = "cep";

	private static final String NUMERO_FACE = "numeroFace";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String HABILITADO = "habilitado";

	private static final String ID_ZEIS = "idZeis";

	private static final String ID_PERFIL_QUADRA = "idPerfilQuadra";

	private static final String NUMERO_QUADRA = "numeroQuadra";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_QUADRAS = "listaQuadras";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_GERENCIA_REGIONAL = "idGerenciaRegional";

	private static final String REQUEST_LISTA_GERENCIA_REGIONAL = "listaGerenciaRegional";

	private static final String REQUEST_LISTA_ZEIS = "listaZeis";

	private static final String REQUEST_LISTA_SETOR_COMERCIAL = "listaSetorComercial";

	private static final String REQUEST_LISTA_PERFIL_QUADRA = "listaPerfilQuadra";

	private static final String REQUEST_LISTA_LOCALIDADE = "listaLocalidade";

	private static final String REQUEST_LISTA_QUADRA_FACE = "listaQuadraFace";

	private static final String REQUEST_LISTA_SETOR_CENSITARIO = "listaSetorCensitario";

	private static final String REQUEST_LISTA_ZONA_BLOQUEIO = "listaZonaBloqueio";

	private static final String REQUEST_LISTA_AREA_TIPO = "listaAreaTipo";

	private static final String REQUEST_REDE_INDICADOR_TEM_PARCIALMENTE = "redeIndicadorTemParcialmente";

	private static final String REQUEST_REDE_INDICADOR_NAO_TEM = "redeIndicadorNaoTem";

	private static final String REQUEST_REDE_INDICADOR_TEM = "redeIndicadorTem";

	private static final String REQUEST_REDES = "redes";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String LISTA_IMOVEIS = "listaImoveis";

	private static final String QUADRA = "quadra";

	private static final String FORWARD_PESQUISAR_QUADRA = "forward:pesquisarQuadra";

	private static final String ERRO_ALTERACAO = "erroAlterar";

	@Autowired
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	private ControladorPerfilQuadra controladorPerfilQuadra;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorRede controladorRede;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorSetorComercial controladorSetorComercial;

	@Autowired
	private ControladorQuadra controladorQuadra;

	@Autowired
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	private ControladorSetorCensitario controladorSetorCensitario;

	@Autowired
	private ControladorAreaTipo controladorAreaTipo;

	@Autowired
	private ControladorZonaBloqueio controladorZonaBloqueio;

	@Autowired
	private ControladorZeis controladorZeis;

	@Autowired
	private ControladorEndereco controladorEndereco;

	private List<QuadraFace> listaQuadraFace;

	private Integer indexListas;

	/**
	 * Método resposável exibir a tela de pesquisar quadra.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @return Retorna a página de exibirPesquisarQuadra
	 * @throws GGASException caso ocorra algum erro. {@link - GGASException}
	 */
	@RequestMapping("exibirPesquisaQuadra")
	public String exibirPesquisaQuadra(HttpServletRequest request, Model model) throws GGASException {

		carregarCamposParaPesquisa(model);
		request.getSession().removeAttribute(QUADRA);
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		request.getSession().setAttribute(LISTA_IMOVEIS, null);
		request.getSession().setAttribute(NUMERO_FACE, null);

		return "exibirPesquisaQuadra";
	}

	/**
	 * Método responsável por exibir a tela de inserir quadra.
	 * 
	 * @param localidade {@link - Long}
	 * @param gerenciaRegional {@link - Long}
	 * @param quadra {@link - Quadra}
	 * @param bindingResult {@link - BindingResult}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(TELA_EXIBIR_INSERCAO_QUADRA)
	public String exibirInsercaoQuadra(@RequestParam(required = false, value = "localidade") Long localidade,
			@RequestParam(required = false, value = "gerenciaRegional") Long gerenciaRegional, QuadraImpl quadra,
			BindingResult bindingResult, Model model, HttpServletRequest request) throws GGASException {

		carregarCampos(request, model, gerenciaRegional, localidade, null);
		saveToken(request);

		model.addAttribute("localidade", localidade);
		model.addAttribute("gerenciaRegional", gerenciaRegional);
		Quadra q = (QuadraImpl) request.getSession().getAttribute(QUADRA);
		if (q != null) {
			model.addAttribute(QUADRA, q);
			request.getSession().removeAttribute(QUADRA);
		}
		model.addAttribute(QUADRA, quadra);

		request.getSession().setAttribute(LISTA_IMOVEIS, null);

		return TELA_EXIBIR_INSERCAO_QUADRA;
	}

	/**
	 * Método responsável por inserir um quadra no sistema.
	 * 
	 * @param quadraTmp - {@link QuadraImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param localidade - {@link Localidade}
	 * @param gerenciaRegional - {@link GerenciaRegional}
	 * @param setorComercial - {@link SetorComercial}
	 * @param request - {@link HttpServletRequest}
	 * @return String - - {@link String}
	 * @throws GGASException caso ocorra algum erro.
	 */
	@RequestMapping("inserirQuadra")
	public String inserirQuadra(QuadraImpl quadraTmp, BindingResult result, Model model,
			@RequestParam(value = "localidade", required = false) Localidade localidade,
			@RequestParam(value = "gerenciaRegional", required = false) GerenciaRegional gerenciaRegional,
			@RequestParam(value = "setorComercial", required = false) SetorComercial setorComercial, HttpServletRequest request)
			throws GGASException {

		QuadraImpl quadra = quadraTmp;
		manterFaces(model, request);

		model.addAttribute(REDE_INDICADOR, Long.parseLong(request.getParameter(REDE_INDICADOR)));
		model.addAttribute(ID_REDE, Long.parseLong(request.getParameter(ID_REDE)));

		String view = TELA_EXIBIR_INSERCAO_QUADRA;
		popularListaQuadraFace(request.getSession(), quadra);

		if (!quadra.validarDados().isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(quadra.validarDados()));
			saveToken(request);

			quadra = montarQuadra(quadra, localidade, gerenciaRegional);
			carregarCampos(request, model, quadra);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			return view;
		}
		carregarCampos(request, model, quadra);

		try {
			validarToken(request);
			quadra.setDadosAuditoria(getDadosAuditoria(request));
			Long idQuadra = controladorQuadra.inserir(quadra);

			model.addAttribute(CHAVE_PRIMARIA, idQuadra);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, obterMensagem(Quadra.QUADRA_ROTULO));
			view = pesquisarQuadras(quadra, result, null, quadra.isHabilitado(), model, request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			view = TELA_EXIBIR_INSERCAO_QUADRA;
		}

		request.getSession().removeAttribute(QUADRA);

		return view;
	}

	private QuadraImpl montarQuadra(QuadraImpl quadra, Localidade localidade, GerenciaRegional gerenciaRegional) {
		if (quadra.getSetorComercial() == null) {
			SetorComercial setorComercial = new SetorComercialImpl();
			setorComercial.setLocalidade(localidade);
			if (localidade == null) {
				setorComercial.setLocalidade(new LocalidadeImpl());
			}
			setorComercial.getLocalidade().setGerenciaRegional(gerenciaRegional);
			quadra.setSetorComercial(setorComercial);
		}
		return quadra;
	}

	/**
	 * Método responsável por remover uma Quadra.
	 * 
	 * @param chavesPrimarias {@link - Long}
	 * @param model {@link - Model}
	 * @param habilitado {@link - Boolean}
	 * @param request {@link - HttpServletRequest}
	 * @return a própria página com a quadra removida.
	 * @throws Exception caso ocorra algum erro.
	 */
	@RequestMapping("removerQuadra")
	public String removerQuadra(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, Model model,
			@RequestParam(HABILITADO) Boolean habilitado, HttpServletRequest request) {

		model.addAttribute(HABILITADO, habilitado);

		try {
			controladorQuadra.validarQuadrasSelecionadas(chavesPrimarias);
			controladorQuadra.removerQuadras(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, obterMensagem(Quadra.QUADRA_ROTULO));
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
			saveToken(request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_PESQUISAR_QUADRA;

	}

	/**
	 * Método responsável por adicionar faces na quadra.
	 * 
	 * @param quadraFaceVO {@link - QuadraFaceVO}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @param alteracaoTela {@link Boolean}
	 * @param quadraTmp {@link QuadraImpl}
	 * @param result {@link BindingResult}
	 * @param localidade {@link Localidade}
	 * @param gerenciaRegional {@link GerenciaRegional}
	 * @param setorComercial {@link SetorComercial}
	 * @return A própria tela com uma quadra face adicionada.
	 * @throws GGASException caso ocorra algum erro.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarFaceNaQuadra")
	public String adicionarFaceNaQuadra(QuadraFaceVO quadraFaceVO,
			@RequestParam(value = "alteracaoTela", required = false) Boolean alteracaoTela, QuadraImpl quadraTmp, BindingResult result,
			Model model, @RequestParam(value = "localidade", required = false) Localidade localidade,
			@RequestParam(value = "gerenciaRegional", required = false) GerenciaRegional gerenciaRegional,
			@RequestParam(value = "setorComercial", required = false) SetorComercial setorComercial, HttpServletRequest request) throws GGASException {

		QuadraImpl quadra = quadraTmp;
		String view;
		Long idLocalidade = 0L;
		Long idGerenecia = 0L;
		quadra.setSetorComercial(setorComercial);
		quadra = montarQuadra(quadra, localidade, gerenciaRegional);
		request.getSession().setAttribute(QUADRA, quadra);
		boolean isAlterarFaceQuadra = false;

		if (localidade != null) {
			idLocalidade = localidade.getChavePrimaria();
		}
		if (gerenciaRegional != null) {
			idGerenecia = gerenciaRegional.getChavePrimaria();
		}

		this.indexListas = quadraFaceVO.getIndexListas();

		listaQuadraFace = (List<QuadraFace>) request.getSession().getAttribute(REQUEST_LISTA_QUADRA_FACE);

		try {
			validarToken(request);
			if (indexListas == null || indexListas < 0) {
				QuadraFace quadraFace = (QuadraFace) controladorQuadra.criarQuadraFace();
				quadraFace.setDadosAuditoria(getDadosAuditoria(request));
				this.popularQuadraFace(quadraFace, quadraFaceVO, request, isAlterarFaceQuadra);
				if (listaQuadraFace == null) {
					listaQuadraFace = new ArrayList<QuadraFace>();
				}

				controladorQuadra.validarNumeroQuadraFace(indexListas, listaQuadraFace, quadraFace);
				listaQuadraFace.add(quadraFace);
				request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
			} else {
				model.addAttribute("indexLista", indexListas);
				this.alterarFaceNaQuadra(indexListas, quadraFaceVO, model, listaQuadraFace, request);
			}
		} catch (NegocioException e) {
			saveToken(request);
			mensagemErroParametrizado(model, request, e);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
		} catch (FormatoInvalidoException el) {
			saveToken(request);
			mensagemErroParametrizado(model, request, el);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
		} catch (GGASException ex) {
			mensagemErroParametrizado(model, ex);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
		}

		request.getSession().setAttribute(REQUEST_LISTA_QUADRA_FACE, listaQuadraFace);
		request.getSession().setAttribute(NUMERO_FACE, quadraFaceVO.getNumeroFace());

		model.addAttribute(REQUEST_LISTA_QUADRA_FACE, listaQuadraFace);
		model.addAttribute(REDE_INDICADOR, quadraFaceVO.getRedeIndicador());
		model.addAttribute(NUMERO_FACE, quadraFaceVO.getNumeroFace());
		model.addAttribute(CEP, quadraFaceVO.getCep());
		model.addAttribute(ID_REDE, quadraFaceVO.getIdRede());

		if (alteracaoTela != null && alteracaoTela) {
			view = exibirAtualizacaoQuadra(quadra, result, quadra.isHabilitado(), request, model);
		} else {
			view = exibirInsercaoQuadra(idLocalidade, idGerenecia, quadra, result, model, request);
		}

		return view;
	}

	/**
	 * Método responsável por remover uma quadra face.
	 * 
	 * @param quadra
	 * @param result
	 * @param localidade
	 * @param gerenciaRegional
	 * @param setorComercial
	 * @param request
	 * @param model
	 * @return String - a própria página com uma quadra face removida
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerFaceNaQuadra")
	public String removerFaceNaQuadra(QuadraImpl quadra, BindingResult result,
			@RequestParam(value = "localidade", required = false) Localidade localidade,
			@RequestParam(value = "gerenciaRegional", required = false) GerenciaRegional gerenciaRegional,
			@RequestParam(value = "setorComercial", required = false) SetorComercial setorComercial, HttpServletRequest request,
			Model model) throws GGASException {

		String view;
		Long idLocalidade = 0L;
		Long idGerenecia = 0L;
		Boolean alteracaoTela;
		Long indexLista;

		alteracaoTela = Boolean.valueOf(request.getParameter("alteracaoTela"));
		indexLista = Long.parseLong(request.getParameter("indexListas"));

		if (localidade != null) {
			idLocalidade = localidade.getChavePrimaria();
		}
		if (gerenciaRegional != null) {
			idGerenecia = gerenciaRegional.getChavePrimaria();
		}

		listaQuadraFace = (List<QuadraFace>) request.getSession().getAttribute(REQUEST_LISTA_QUADRA_FACE);

		try {
			validarToken(request);
			if (listaQuadraFace != null && indexLista != null) {
				(listaQuadraFace).remove(indexLista.intValue());
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
		}

		request.getSession().setAttribute(REQUEST_LISTA_QUADRA_FACE, request.getSession().getAttribute(REQUEST_LISTA_QUADRA_FACE));

		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		if (alteracaoTela != null && alteracaoTela) {
			view = "forward:exibirAtualizacaoQuadra";
		} else {
			view = exibirInsercaoQuadra(idLocalidade, idGerenecia, quadra, result, model, request);
		}

		return view;

	}

	/**
	 * Método responsável por receber uma quadraFace e popular os seus dados.
	 * 
	 * @param quadraFace {@link - QuadraFace}
	 * @param quadraFaceVO {@link - QuadraFaceVO}
	 * @param request {@link - HttpServletRequest}
	 * @param isAlterarFaceQuadra {@link - Boolean}
	 * @throws Exception caso ocorra algum erro.
	 */
	private void popularQuadraFace(QuadraFace quadraFace, QuadraFaceVO quadraFaceVO, HttpServletRequest request,
			boolean isAlterarFaceQuadra) throws GGASException {

		/*
		 * Comparar os tipos de rede indicador
		 */

		Long idRede = null;

		if (quadraFaceVO.getRedeIndicador() != null && quadraFaceVO.getRedeIndicador() != NAO_TEM) {
			idRede = quadraFaceVO.getIdRede();
		}

		Endereco endereco = null;
		Cep cep = null;
		RedeIndicador redeIndicador = null;

		endereco = controladorEndereco.criarEndereco();

		if (!StringUtils.isEmpty(quadraFaceVO.getNumeroFace())) {
			Util.validarDominioCaracteres(QuadraFace.NUMERO, quadraFaceVO.getNumeroFace(), Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			endereco.setNumero(quadraFaceVO.getNumeroFace());
		}

		if ((!StringUtils.isEmpty(quadraFaceVO.getCep())) && (StringUtils.isEmpty(quadraFaceVO.getChaveCep()))) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CEP_INEXISTENTE);
		}

		if ((!StringUtils.isEmpty(quadraFaceVO.getCep())) && (!StringUtils.isEmpty(quadraFaceVO.getChaveCep()))) {
			cep = controladorEndereco.obterCepPorChave(Util.converterCampoStringParaValorLong("CEP", quadraFaceVO.getChaveCep()));
			endereco.setCep(cep);
		}

		quadraFace.setEndereco(endereco);

		quadraFace.setHabilitado(Boolean.TRUE);
		quadraFace.setUltimaAlteracao(Calendar.getInstance().getTime());
		if (isAlterarFaceQuadra) {
			if (quadraFaceVO.getRedeIndicador() == null) {
				quadraFaceVO.setRedeIndicador(TEM_PARCIALMENTE);
			}
			if (quadraFaceVO.getRedeIndicador() == TEM || quadraFaceVO.getRedeIndicador() == NAO_TEM) {
				this.filtrarTipoIndicadorRede(quadraFace, quadraFaceVO.getRedeIndicador(), redeIndicador, idRede, request);
			} else {
				redeIndicador = controladorRede.obterRedeIndicador(TEM_PARCIALMENTE);
				quadraFace.setRedeIndicador(redeIndicador);
				obterRede(quadraFace, idRede);

			}
		} else {
			redeIndicador = controladorRede.obterRedeIndicador(quadraFaceVO.getRedeIndicador());
			quadraFace.setRedeIndicador(redeIndicador);
			obterRede(quadraFace, idRede);
		}

		controladorQuadra.validarDadosQuadraFace(quadraFace);

	}

	/**
	 * Método responsável popular as novad quadras faces adicionadas a uma quadra.
	 * 
	 * @param session {@link - HttpSession}
	 * @param quadra {@link - Quadra}
	 */
	private void popularListaQuadraFace(HttpSession session, Quadra quadra) {

		Collection<QuadraFace> listaQuadraFace = (Collection<QuadraFace>) session.getAttribute(REQUEST_LISTA_QUADRA_FACE);

		if (listaQuadraFace != null && !listaQuadraFace.isEmpty()) {
			if (quadra.getQuadrasFace() != null && !quadra.getQuadrasFace().isEmpty()) {
				quadra.getQuadrasFace().clear();
			}
			quadra.getQuadrasFace().addAll(listaQuadraFace);

			for (QuadraFace quadraFace : quadra.getQuadrasFace()) {
				quadraFace.setQuadra(quadra);
			}
		} else {
			quadra.getQuadrasFace().clear();
		}
	}

	/**
	 * Método responsável por filtros para cada tipo de indicador de rede.
	 * 
	 * @param face {@link - QuadraFace}
	 * @param novoIndicador {@link - Integer}
	 * @param indicadorRede {@link - RedeIndicador}
	 * @param idRede {@link - Long}
	 * @param request {@link - HttpServletRequest}
	 * @throws GGASException caso ocorra algum erro {@link - GGASException}
	 */
	public void filtrarTipoIndicadorRede(QuadraFace face, Integer novoIndicador, RedeIndicador indicadorRede, Long idRede,
			HttpServletRequest request) throws GGASException {

		RedeIndicador redeIndicador = indicadorRede;
		redeIndicador = controladorRede.obterRedeIndicador(novoIndicador);
		face.setRedeIndicador(redeIndicador);
		obterRede(face, idRede);
		Collection<Imovel> listaImoveis;
		if (!listaQuadraFace.isEmpty()) {
			QuadraFace quadraFace = listaQuadraFace.get(indexListas);

			Integer antigoIndicador = quadraFace.getRedeIndicador().getCodigo();

			// ROTINA CRIADA PARA GUARDAR VARIOS IMOVEIS
			Collection<Imovel> listaComImoveis = new ArrayList<Imovel>();
			if (request.getSession().getAttribute(LISTA_IMOVEIS) != null
					&& !((Collection<Imovel>) request.getSession().getAttribute(LISTA_IMOVEIS)).isEmpty()) {
				listaComImoveis = (Collection<Imovel>) request.getSession().getAttribute(LISTA_IMOVEIS);
			}
			if (antigoIndicador == RedeIndicador.NAO_TEM && novoIndicador == RedeIndicador.TEM) {
				listaImoveis = controladorQuadra.alterarSituacaoImoveisFactivel(quadraFace.getChavePrimaria());
				if (!listaComImoveis.isEmpty()) {
					listaImoveis.addAll(listaComImoveis);
				}
				request.getSession().setAttribute(LISTA_IMOVEIS, listaImoveis);
				request.setAttribute(LISTA_IMOVEIS, listaImoveis);
			} else if (antigoIndicador == RedeIndicador.TEM_PARCIALMENTE && novoIndicador == RedeIndicador.NAO_TEM) {

				if (temInterligado(quadraFace)) {
					request.getSession().setAttribute(LISTA_IMOVEIS, new ArrayList<Imovel>());
					request.setAttribute(LISTA_IMOVEIS, new ArrayList<Imovel>());

					throw new NegocioException(
							"Quadra não pode ser alterada pois possui pelo menos um Imóvel com a situação de Interligado");
				} else {
					listaImoveis = controladorQuadra.alterarSituacaoImoveisPontencial(quadraFace.getChavePrimaria());
					if (!listaComImoveis.isEmpty()) {
						listaImoveis.addAll(listaComImoveis);
					}
					request.getSession().setAttribute(LISTA_IMOVEIS, listaImoveis);
					request.setAttribute(LISTA_IMOVEIS, listaImoveis);
				}
			} else if (antigoIndicador == TEM_PARCIALMENTE && novoIndicador == TEM) {
				listaImoveis = controladorQuadra.alterarSituacaoImoveisFactivel(quadraFace.getChavePrimaria());
				if (!listaComImoveis.isEmpty()) {
					listaImoveis.addAll(listaComImoveis);
				}
				request.getSession().setAttribute(LISTA_IMOVEIS, listaImoveis);
				request.setAttribute(LISTA_IMOVEIS, listaImoveis);
			} else if (antigoIndicador == TEM && novoIndicador == NAO_TEM) {

				if (temInterligado(quadraFace)) {
					request.getSession().setAttribute(LISTA_IMOVEIS, new ArrayList<Imovel>());
					request.setAttribute(LISTA_IMOVEIS, new ArrayList<Imovel>());

					throw new NegocioException(
							"Quadra não pode ser alterada pois possui pelo menos um Imóvel com a situação de Interligado");
				} else {
					listaImoveis = controladorQuadra.alterarSituacaoImoveisPontencial(quadraFace.getChavePrimaria());
					if (!listaComImoveis.isEmpty()) {
						listaImoveis.addAll(listaComImoveis);
					}
					request.getSession().setAttribute(LISTA_IMOVEIS, listaImoveis);
					request.setAttribute(LISTA_IMOVEIS, listaImoveis);

				}
			}
		}
	}

	/**
	 * Método responsável por popular uma rede na quadraFace
	 * 
	 * @param quadraFace {@link - QuadraFace}
	 * @param idRede {@link - Long}
	 * @throws GGASException caso ocorra algum erro.
	 */
	private void obterRede(QuadraFace quadraFace, Long idRede) throws GGASException {

		Rede rede = null;
		if (idRede != null && idRede > 0) {
			rede = (Rede) controladorRede.obter(idRede);
		}

		quadraFace.setRede(rede);
	}

	/**
	 * Carregar campos para pesquisa.
	 * 
	 * @param model {@link - Model}
	 * @throws GGASException { @link - GGASException }
	 */
	private void carregarCamposParaPesquisa(Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);

		model.addAttribute(REQUEST_LISTA_PERFIL_QUADRA, controladorPerfilQuadra.consultarPerfilQuadra(filtro));
		model.addAttribute(REQUEST_LISTA_SETOR_COMERCIAL, controladorSetorComercial.consultarSetorComercial(filtro));
		model.addAttribute(REQUEST_LISTA_ZEIS, controladorZeis.consultarZeis(filtro));
	}

	/**
	 * Método responsável por pesquisar as quadras e exibir na tela de pesquisa.
	 * 
	 * @param quadra {@link - Quadra}
	 * @param result {@link - BindingResult}
	 * @param cep {@link - String}
	 * @param habilitado {@link - Boolean}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @return retorna as quadras encontradas de acordo com a pesquisa
	 * @throws GGASException caso ocorra algum erro.
	 */
	@RequestMapping("pesquisarQuadra")
	public String pesquisarQuadras(QuadraImpl quadra, BindingResult result, @RequestParam(value = CEP, required = false) String cep,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model, HttpServletRequest request)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, quadra, cep, habilitado);
		saveToken(request);

		model.addAttribute(LISTA_QUADRAS, controladorQuadra.consultarQuadras(filtro));
		model.addAttribute(QUADRA, quadra);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(CEP, cep);

		request.getSession().setAttribute(LISTA_IMOVEIS, null);

		return exibirPesquisaQuadra(request, model);
	}

	/**
	 * Método responsável por popular um mapa para pesquisa
	 * 
	 * @param filtro {@link - Map}
	 * @param quadra {@link - Quadra}
	 * @param cep {@link - String}
	 * @param habilitado {@link - Boolean}
	 */
	private void prepararFiltro(Map<String, Object> filtro, QuadraImpl quadra, String cep, Boolean habilitado) {

		if (quadra != null && quadra.getSetorComercial() != null && quadra.getSetorComercial().getChavePrimaria() > 0) {
			filtro.put(ID_SETOR_COMERCIAL, quadra.getSetorComercial().getChavePrimaria());
		}

		if (quadra != null && quadra.getNumeroQuadra() != null && !StringUtils.isEmpty(quadra.getNumeroQuadra())) {
			filtro.put(NUMERO_QUADRA, quadra.getNumeroQuadra());
		}

		if (quadra != null && quadra.getPerfilQuadra() != null && quadra.getPerfilQuadra().getChavePrimaria() > 0) {
			filtro.put(ID_PERFIL_QUADRA, quadra.getPerfilQuadra().getChavePrimaria());
		}

		if (quadra != null && quadra.getZeis() != null && quadra.getZeis().getChavePrimaria() > 0) {
			filtro.put(ID_ZEIS, quadra.getZeis().getChavePrimaria());
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		if (StringUtils.isNotBlank(cep)) {
			filtro.put(CEP, cep);
		}
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de uma Quadra.
	 * 
	 * @param chavePrimaria {@link - Long}
	 * @param habilitado {@link - Boolean}
	 * @param cep {@link - String}
	 * @param request {@link - HttpServletRequest}
	 * @param response {@link - HttpServletResponse}
	 * @param model {@link - Model}
	 * @param quadraTmp {@link - QuadraImpl}
	 * @param result {@link - BindingResult}
	 * @return retorna a tela de detalhes com os campos preenchidos.
	 * @throws GGASException caso ocorra algum erro. {@link - GGASException}
	 */
	@RequestMapping("exibirDetalhamentoQuadra")
	public String exibirDetalhamentoQuadra(QuadraImpl quadraTmp, BindingResult result, @RequestParam(HABILITADO) Boolean habilitado,
			@RequestParam(CEP) String cep, HttpServletRequest request, HttpServletResponse response, Model model) throws GGASException {

		saveToken(request);

		String view = "exibirDetalhamentoQuadra";
		model.addAttribute("quadraForm", quadraTmp);
		model.addAttribute("cepTela", cep);
		model.addAttribute(HABILITADO, habilitado);
		QuadraImpl quadra = quadraTmp;
		try {
			quadra = (QuadraImpl) controladorQuadra.obter(quadraTmp.getChavePrimaria(), "quadrasFace");
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_PESQUISAR_QUADRA;
		}
		carregarCampos(request, model, quadra);

		return view;
	}

	/**
	 * Método responsável por exibir a tela de atualizar quadra, com os seus respectivos campos carregados.
	 * 
	 * @param chavePrimaria {@link - Long}
	 * @param habilitado {@link - Boolean}
	 * @param cep {@link - String}
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @param quadraTmp {@link - QuadraImpl}
	 * @param result {@link - BindingResult}
	 * @return A tela de atualização de quadra.
	 * @throws GGASException caso ocorra algum erro.
	 */
	@RequestMapping("exibirAtualizacaoQuadra")
	public String exibirAtualizacaoQuadra(QuadraImpl quadraTmp, BindingResult result, @RequestParam(HABILITADO) Boolean habilitado,
			HttpServletRequest request, Model model) throws GGASException {

		saveToken(request);
		String view = "exibirAtualizarQuadra";
		model.addAttribute("quadraForm", quadraTmp);
		QuadraImpl quadra = quadraTmp;
		try {
			quadra = (QuadraImpl) controladorQuadra.obter(quadraTmp.getChavePrimaria(), "quadrasFace");
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = FORWARD_PESQUISAR_QUADRA;
		}
		model.addAttribute(HABILITADO, habilitado);

		QuadraImpl q = (QuadraImpl) request.getSession().getAttribute(QUADRA);
		if (q != null) {
			q.setQuadrasFace(quadra.getQuadrasFace());
			quadra = q;
		}

		carregarCampos(request, model, quadra);
		return view;
	}

	/**
	 * Método responsável por altera quadraface de uma quadra.
	 * 
	 * @param indexLista {@link - Integer}
	 * @param quadraFaceVO {@link - QuadraFaceVO}
	 * @param model {@link - Model}
	 * @param listaQuadraFace {@link - List}
	 * @param request {@link - HttpServletRequest}
	 * @throws GGASException caso ocorra algum erro.
	 */
	private void alterarFaceNaQuadra(Integer indexLista, QuadraFaceVO quadraFaceVO, Model model, List<QuadraFace> listaQuadraFace,
			HttpServletRequest request) throws GGASException {

		boolean isAlterarFace = true;
		try {
			if (listaQuadraFace != null && indexLista != null) {
				QuadraFace quadraFaceAlterado = (QuadraFace) controladorQuadra.criarQuadraFace();
				this.popularQuadraFace(quadraFaceAlterado, quadraFaceVO, request, isAlterarFace);
				controladorQuadra.validarNumeroQuadraFace(indexLista, listaQuadraFace, quadraFaceAlterado);
				QuadraFace quadraFaceExistente = null;
				for (int i = 0; i < listaQuadraFace.size(); i++) {
					quadraFaceExistente = listaQuadraFace.get(i);
					if (i == indexLista && quadraFaceExistente != null) {
						this.popularQuadraFace(quadraFaceExistente, quadraFaceVO, request, isAlterarFace);
						request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
					}
				}
			}
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			request.setAttribute(ERRO_ALTERACAO, true);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			request.setAttribute(ERRO_ALTERACAO, true);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
		}
	}

	/**
	 * Método responsável por atualizar uma quadra
	 * 
	 * @param quadraTmp {@link - Quadra}
	 * @param result {@link - BindingResult}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @param localidade {@link - Localidade}
	 * @param gerenciaRegional {@link - GerenciaRegional}
	 * @param setorComercial {@link - SetorComercial}
	 * @return a tela de pesquisar com a quadra atualizada.
	 * @throws GGASException caso ocorra algum erro {@link - GGASException}
	 */
	@RequestMapping("atualizarQuadra")
	public String atualizarQuadra(QuadraImpl quadraTmp, BindingResult result, Model model,
			@RequestParam(value = "localidade", required = false) Localidade localidade,
			@RequestParam(value = "gerenciaRegional", required = false) GerenciaRegional gerenciaRegional,
			@RequestParam(value = "setorComercial", required = false) SetorComercial setorComercial, HttpServletRequest request)
			throws GGASException {

		QuadraImpl quadra = quadraTmp;
		String view = "exibirAtualizarQuadra";
		manterFaces(model, request);

		model.addAttribute(REDE_INDICADOR, Long.parseLong(request.getParameter(REDE_INDICADOR)));
		model.addAttribute(ID_REDE, Long.parseLong(request.getParameter(ID_REDE)));

		popularListaQuadraFace(request.getSession(), quadra);
		if (!quadra.validarDados().isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(quadra.validarDados()));
			saveToken(request);
			quadra.setSetorComercial(setorComercial);
			quadra = montarQuadra(quadra, localidade, gerenciaRegional);
			carregarCampos(request, model, quadra);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			return view;
		}

		quadra.setDadosAuditoria(getDadosAuditoria(request));
		Collection<Imovel> listaImoveis = (Collection<Imovel>) request.getSession().getAttribute(LISTA_IMOVEIS);
		request.getSession().setAttribute(LISTA_IMOVEIS, null);
		request.getSession().setAttribute(NUMERO_FACE, null);

		try {
			validarToken(request);
			controladorQuadra.atualizarQuadra(quadra);

			if (listaImoveis != null && !listaImoveis.isEmpty()) {
				controladorImovel.atualizarColecao(listaImoveis, Imovel.class);
			}
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, obterMensagem(Quadra.QUADRA_ROTULO));
			view = pesquisarQuadras(quadra, result, null, quadra.isHabilitado(), model, request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			saveToken(request);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			carregarCampos(request, model, quadra);
		}
		request.getSession().removeAttribute(QUADRA);
		return view;
	}

	/**
	 * @param model
	 * @param request
	 */
	private void manterFaces(Model model, HttpServletRequest request) {
		if (request.getParameter(CEP) != null) {
			model.addAttribute(CEP, request.getParameter(CEP));
		}
		if (request.getParameter(NUMERO_FACE) != null) {
			model.addAttribute(NUMERO_FACE, request.getParameter(NUMERO_FACE));
		}
	}

	/**
	 * Método responsável por carregar campos para ser exibido na tela.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @param quadra {@link - Quadra}
	 * @throws GGASException caso ocorra algum erro {@link - GGASException}
	 */
	private void carregarCampos(HttpServletRequest request, Model model, Quadra quadra) throws GGASException {

		GerenciaRegional gerenciaRegional = null;
		Localidade localidade = null;

		if (quadra != null && quadra.getSetorComercial() != null) {

			SetorComercial setorComercial = quadra.getSetorComercial();

			localidade = setorComercial.getLocalidade();

			if (localidade != null) {
				gerenciaRegional = localidade.getGerenciaRegional();
			}
		}

		this.carregarCampos(request, model, gerenciaRegional, localidade, quadra);
	}

	/**
	 * Método responsável por carregar campos para ser exibido na tela.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @param gerenciaRegional {@link - GerenciaRegional}
	 * @param localidade {@link - Localidade}
	 * @param quadra {@link - Quadra}
	 * @throws GGASException caso ocorra algum erro {@link - GGASException}
	 */
	private void carregarCampos(HttpServletRequest request, Model model, GerenciaRegional gerenciaRegional, Localidade localidade,
			Quadra quadra) throws GGASException {

		Long idGerenciaRegional = null;
		Long idLocalidade = null;

		if (gerenciaRegional != null) {
			idGerenciaRegional = gerenciaRegional.getChavePrimaria();
		}

		if (localidade != null) {
			idLocalidade = localidade.getChavePrimaria();
		}

		this.carregarCampos(request, model, idGerenciaRegional, idLocalidade, quadra);
	}

	/**
	 * Método responsável por carregar campos para ser exibido na tela.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @param idGerenciaRegional {@link - Long}
	 * @param idLocalidade {@link - Long}
	 * @param quadra {@link - Quadra}
	 * @throws GGASException caso ocorra algum erro.
	 */
	private void carregarCampos(HttpServletRequest request, Model model, Long idGerenciaRegional, Long idLocalidade, Quadra quadra)
			throws GGASException {

		Collection<SetorComercial> listaSetorComercial = new ArrayList<SetorComercial>();
		Collection<Localidade> listaLocalidade = new ArrayList<Localidade>();
		Collection<QuadraFace> listaQuadraFace = new ArrayList<QuadraFace>();

		Map<String, Object> filtroLocalidade = new HashMap<String, Object>();
		filtroLocalidade.put(HABILITADO, Boolean.TRUE);

		Map<String, Object> filtroSetorComercial = new HashMap<String, Object>();
		filtroSetorComercial.put(HABILITADO, Boolean.TRUE);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);

		if ((idGerenciaRegional != null) && (idGerenciaRegional > 0)) {
			filtroLocalidade.put(ID_GERENCIA_REGIONAL, idGerenciaRegional);
			listaLocalidade = controladorLocalidade.consultarLocalidades(filtroLocalidade);
		}

		if ((idLocalidade != null) && (idLocalidade > 0)) {
			filtroSetorComercial.put(ID_LOCALIDADE, idLocalidade);
			listaSetorComercial = controladorSetorComercial.consultarSetorComercial(filtroSetorComercial);

		}

		model.addAttribute(REQUEST_LISTA_LOCALIDADE, listaLocalidade);
		model.addAttribute(REQUEST_LISTA_SETOR_COMERCIAL, listaSetorComercial);
		model.addAttribute(REQUEST_LISTA_GERENCIA_REGIONAL, controladorGerenciaRegional.consultarGerenciaRegional(filtro));
		model.addAttribute(REQUEST_LISTA_PERFIL_QUADRA, controladorPerfilQuadra.consultarPerfilQuadra(filtro));
		model.addAttribute(REQUEST_LISTA_ZEIS, controladorZeis.consultarZeis(filtro));

		model.addAttribute(REQUEST_LISTA_SETOR_CENSITARIO, controladorSetorCensitario.consultarSetorCensitario(filtro));
		model.addAttribute(REQUEST_LISTA_ZONA_BLOQUEIO, controladorZonaBloqueio.consultarZonaBloqueio(filtro));
		model.addAttribute(REQUEST_LISTA_AREA_TIPO, controladorAreaTipo.consultarAreaTipo(filtro));

		if (!isPostBack(request)) {
			if (quadra != null) {
				listaQuadraFace.addAll(quadra.getQuadrasFace());
			}
			request.getSession().setAttribute(REQUEST_LISTA_QUADRA_FACE, listaQuadraFace);
		} else {
			listaQuadraFace = (Collection<QuadraFace>) request.getSession().getAttribute(REQUEST_LISTA_QUADRA_FACE);
		}

		model.addAttribute(REQUEST_LISTA_QUADRA_FACE, listaQuadraFace);
		model.addAttribute(REQUEST_REDES, controladorRede.obterTodas());
		model.addAttribute(REQUEST_REDE_INDICADOR_TEM, TEM);
		model.addAttribute(REQUEST_REDE_INDICADOR_NAO_TEM, NAO_TEM);
		if (!model.containsAttribute(REDE_INDICADOR)) {
			model.addAttribute(REDE_INDICADOR, NAO_TEM);
		}
		model.addAttribute(REQUEST_REDE_INDICADOR_TEM_PARCIALMENTE, TEM_PARCIALMENTE);
		model.addAttribute(QUADRA, quadra);

	}

	/**
	 * Verifica se tem imovel interligado com quadraFace.
	 * 
	 * @param quadraFace {@link - QuadraFace}
	 * @return retorna um booleano indicando o resultado.
	 * @throws GGASException caso ocorra algum erro.
	 */
	private boolean temInterligado(QuadraFace quadraFace) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("quadraFaceChavePrimaria", quadraFace.getChavePrimaria());
		Collection<Imovel> listaImovelAux = controladorImovel.consultarImoveis(filtro);

		for (Imovel imo : listaImovelAux) {
			if (imo.getSituacaoImovel().getChavePrimaria() == Long
					.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO))) {
				return true;
			}
		}
		return false;
	}

}
