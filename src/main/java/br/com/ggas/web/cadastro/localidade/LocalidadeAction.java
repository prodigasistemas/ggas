/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.cadastro.localidade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.localidade.ControladorGerenciaRegional;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorUnidadeNegocio;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.localidade.impl.LocalidadeImpl;
import br.com.ggas.cadastro.localidade.impl.LocalidadeVO;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 *
 * Classe responsável pelas telas relacionadas à localidade.
 * 
 */
@Controller
public class LocalidadeAction extends GenericAction {

	private static final String LOCALIDADE_FORM = "localidadeForm";

	private static final String TELA_EXIBIR_PESQUISA_LOCALIDADE = "exibirPesquisaLocalidade";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ID_GERENCIA_REGIONAL = "idGerenciaRegional";

	private static final String HABILITADO = "habilitado";

	private static final String ID_UNIDADE_NEGOCIO = "idUnidadeNegocio";

	private static final String DESCRICAO = "descricao";

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorGerenciaRegional")
	private ControladorGerenciaRegional controladorGerenciaRegional;

	@Autowired
	@Qualifier("controladorUnidadeNegocio")
	private ControladorUnidadeNegocio controladorUnidadeNegocio;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	/**
	 * Metodo responsavel por exibir a tela de detalhamento de uma localidade.
	 * 
	 * @param localidadeVO
	 * @param result
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalhamentoLocalidade")
	public String exibirDetalhamentoLocalidade(LocalidadeVO localidadeVO, BindingResult result,
			@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {
		String retorno = null;

		try {
			LocalidadeImpl localidade = (LocalidadeImpl) controladorLocalidade.obter(chavePrimaria);
			model.addAttribute(LOCALIDADE_FORM, localidade);
			model.addAttribute("indicadorDeUSo", localidadeVO.getHabilitado());
			retorno = "exibirDetalhamentoLocalidade";
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = TELA_EXIBIR_PESQUISA_LOCALIDADE;
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de localidade.
	 * 
	 * @param localidade
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_LOCALIDADE)
	public String exibirPesquisaLocalidade(LocalidadeImpl localidade, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, Model model)
			throws NegocioException {

		carregarCamposFiltro(localidade, model);
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return TELA_EXIBIR_PESQUISA_LOCALIDADE;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de localidade no
	 * sistema.
	 * 
	 * @param localidade
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping("pesquisarLocalidade")
	public String pesquisarLocalidade(LocalidadeImpl localidade, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, Model model)
			throws NegocioException {

		model.addAttribute(CHAVES_PRIMARIAS, null);

		Map<String, Object> filtro = obterFiltroForm(localidade, habilitado);
		Collection<Localidade> listaLocalidade = controladorLocalidade.consultarLocalidades(filtro);
		model.addAttribute("listaLocalidade", listaLocalidade);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(LOCALIDADE_FORM, localidade);

		return exibirPesquisaLocalidade(localidade, result, habilitado, model);
	}

	/**
	 * Método responsável por exibir a tela de inserir localidade.
	 * 
	 * @param localidadeVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirInsercaoLocalidade")
	public String exibirInsercaoLocalidade(LocalidadeVO localidadeVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		carregarCampos(model, localidadeVO);
		saveToken(request);
		return "exibirInsercaoLocalidade";
	}

	/**
	 * Método responsável por inserir uma localidade.
	 * 
	 * @param localidadeVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(value = "inserirLocalidade", method = RequestMethod.POST)
	public String inserirLocalidade(LocalidadeVO localidadeVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		
		String retorno = null;
		model.addAttribute(LOCALIDADE_FORM, localidadeVO);
		try {
			validarToken(request);
			LocalidadeImpl localidade = (LocalidadeImpl) controladorLocalidade.criar();
			if (localidadeVO.getCep() != null) {
				model.addAttribute("cep", localidadeVO.getCep());
			}
			popularLocalidade(localidadeVO, localidade);
			Map<String, Object> erros = localidade.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
				retorno = exibirInsercaoLocalidade(localidadeVO, result, request, model);
			} else {
				localidade.setDadosAuditoria(getDadosAuditoria(request));
				localidade.setHabilitado(true);
				controladorLocalidade.inserirLocalidade(localidade);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
						Localidade.LOCALIDADE_CAMPO_STRING);

				retorno = pesquisarLocalidade(localidade, result, localidade.isHabilitado(), model);
			}
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = exibirInsercaoLocalidade(localidadeVO, result, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			retorno = exibirInsercaoLocalidade(localidadeVO, result, request, model);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar localidade.
	 * 
	 * @param localidadeVO
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("exibirAtualizacaoLocalidade")
	public String exibirAtualizacaoLocalidade(LocalidadeVO localidadeVO, HttpServletRequest request, Model model)
			throws GGASException {
		String retorno = "exibirAtualizacaoLocalidade";

		if (!super.isPostBack(request)) {
			LocalidadeImpl localidade = null;
			try {
				localidade = (LocalidadeImpl) controladorLocalidade.obter(localidadeVO.getChavePrimaria());
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, request, e);
				retorno = TELA_EXIBIR_PESQUISA_LOCALIDADE;
			}
			if (localidade != null) {
				popularLocalidadeForm(localidadeVO, localidade);
			}
			model.addAttribute(LOCALIDADE_FORM, localidadeVO);
			if (localidade != null && (localidade.getEndereco() != null && localidade.getEndereco().getCep() != null)) {
				model.addAttribute("cep", localidade.getEndereco().getCep().getCep());
			}
		}
		carregarCampos(model, localidadeVO);
		saveToken(request);
		return retorno;
	}

	/**
	 * Método responsável por atualizar a localidade.
	 * 
	 * @param localidadeVO
	 * @param result
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping(value = "atualizarLocalidade", method = RequestMethod.POST)
	public String atualizarLocalidade(LocalidadeVO localidadeVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String retorno = null;
		model.addAttribute(LOCALIDADE_FORM, localidadeVO);
		if (localidadeVO.getCep() != null) {
			model.addAttribute("cep", localidadeVO.getCep());
		}
		try {
			validarToken(request);
			LocalidadeImpl localidade = (LocalidadeImpl) controladorLocalidade.obter(localidadeVO.getChavePrimaria());
			popularLocalidade(localidadeVO, localidade);

			Map<String, Object> erros = localidade.validarDados();
			if (!erros.isEmpty()) {
				mensagemErro(model, request, new NegocioException(erros));
				retorno = exibirAtualizacaoLocalidade(localidadeVO, request, model);
			} else {
				localidade.setDadosAuditoria(getDadosAuditoria(request));
				controladorLocalidade.atualizar(localidade);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						Localidade.LOCALIDADE_CAMPO_STRING);
				retorno = pesquisarLocalidade(localidade, result, localidade.isHabilitado(), model);
			}
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			retorno = exibirAtualizacaoLocalidade(localidadeVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			retorno = exibirAtualizacaoLocalidade(localidadeVO, request, model);
		}

		return retorno;
	}

	/**
	 * Método responsável por remover localidade.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return String
	 * @throws NegocioException
	 */
	@RequestMapping(value = "removerLocalidade", method = RequestMethod.POST)
	public String removerLocalidade(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws NegocioException {

		try {
			controladorLocalidade.validarRemoverLocalidades(chavesPrimarias);
			controladorLocalidade.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Localidade.LOCALIDADES);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "forward:pesquisarLocalidade";
	}

	/**
	 * Popular localidade.
	 * 
	 * @param localidadeVO
	 * @param localidade
	 * @throws GGASException
	 */
	private void popularLocalidade(LocalidadeVO localidadeVO, LocalidadeImpl localidade) throws GGASException  {

		if (localidadeVO.getDddTelefone() != null) {
			localidade.setCodigoFoneDDD(localidadeVO.getDddTelefone());
		} else {
			localidade.setCodigoFoneDDD(null);
		}

		if (localidadeVO.getDddFax() != null) {
			localidade.setCodigoFaxDDD(localidadeVO.getDddFax());
		} else {
			localidade.setCodigoFaxDDD(null);
		}

		if (localidadeVO.getCodigo() != null && localidadeVO.getCodigo() > 0) {
			localidade.setChavePrimaria(localidadeVO.getCodigo());
		}
		if (localidadeVO.getVersao() != null && localidadeVO.getVersao() > 0) {
			localidade.setVersao(localidadeVO.getVersao());
		}
		if (StringUtils.isEmpty(localidadeVO.getDescricao())) {
			localidade.setDescricao(null);
		} else {
			localidade.setDescricao(localidadeVO.getDescricao());
		}
		if (localidadeVO.getGerenciaRegional() != null) {
			localidade.setGerenciaRegional(localidadeVO.getGerenciaRegional());
		} else {
			localidade.setGerenciaRegional(null);
		}
		if (StringUtils.isEmpty(localidadeVO.getFone())) {
			localidade.setFone(null);
		} else {
			Util.converterCampoStringParaValorInteger(GerenciaRegional.TELEFONE, localidadeVO.getFone());
			localidade.setFone(localidadeVO.getFone());
		}
		if (StringUtils.isEmpty(localidadeVO.getRamalFone())) {
			localidade.setRamalFone(null);
		} else {
			Util.converterCampoStringParaValorInteger(GerenciaRegional.RAMAL, localidadeVO.getRamalFone());
			localidade.setRamalFone(localidadeVO.getRamalFone());
		}
		if (StringUtils.isEmpty(localidadeVO.getFax())) {
			localidade.setFax(null);
		} else {
			Util.converterCampoStringParaValorInteger(GerenciaRegional.FAX, localidadeVO.getFax());
			localidade.setFax(localidadeVO.getFax());
		}
		if (StringUtils.isEmpty(localidadeVO.getEmail())) {
			localidade.setEmail(null);
		} else {
			localidade.setEmail(localidadeVO.getEmail());
		}

		localidade.setInformatizada(localidadeVO.getInformatizada());

		if (StringUtils.isEmpty(localidadeVO.getCodigoCentroCusto())) {
			localidade.setCodigoCentroCusto(null);
		} else {
			localidade.setCodigoCentroCusto(localidadeVO.getCodigoCentroCusto());
		}
		
		preenchendoLocalidadeEntidades(localidadeVO, localidade);

		preencherCamposEndereco(localidadeVO, localidade);
	}

	/**
	 * @param localidadeVO
	 * @param localidade
	 */
	private void preenchendoLocalidadeEntidades(LocalidadeVO localidadeVO, LocalidadeImpl localidade) {
		if (localidadeVO.getUnidadeNegocio() != null) {
			localidade.setUnidadeNegocio(localidadeVO.getUnidadeNegocio());
		} else {
			localidade.setUnidadeNegocio(null);
		}
		if (localidadeVO.getLocalidadeClasse() != null) {
			localidade.setClasse(localidadeVO.getLocalidadeClasse());
		} else {
			localidade.setClasse(null);
		}
		if (localidadeVO.getLocalidadePorte() != null) {
			localidade.setPorte(localidadeVO.getLocalidadePorte());
		} else {
			localidade.setPorte(null);
		}
		if (localidadeVO.getMedidorLocalArmazenagem() != null) {
			localidade.setMedidorLocalArmazenagem(localidadeVO.getMedidorLocalArmazenagem());
		} else {
			localidade.setMedidorLocalArmazenagem(null);
		}
		if (localidadeVO.getCliente() != null) {
			localidade.setCliente(localidadeVO.getCliente());
		} else {
			localidade.setCliente(null);
		}
	}

	/**
	 * @param localidadeVO
	 * @param localidade
	 * @throws NegocioException
	 * @throws FormatoInvalidoException
	 */
	private void preencherCamposEndereco(LocalidadeVO localidadeVO, LocalidadeImpl localidade)
			throws NegocioException, FormatoInvalidoException {
		Endereco endereco = controladorEndereco.criarEndereco();

		if (!StringUtils.isEmpty(localidadeVO.getCep())) {
			Cep cep =null;
			if(localidadeVO.getChaveCep() == null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CEP_INEXISTENTE, true);
			}else {
				cep = controladorEndereco.obterCepPorChave(localidadeVO.getChaveCep());
			}
			endereco.setCep(cep);
		} else {
			endereco.setCep(null);
		}
		
		if (StringUtils.isEmpty(localidadeVO.getNumeroEndereco())) {
			if (!"S/N".equals(localidadeVO.getNumeroEndereco())) {
				Util.validarDominioCaracteres(Endereco.ENDERECO_NUMERO, localidadeVO.getNumeroEndereco(),
						Constantes.EXPRESSAO_REGULAR_DOMINIO_DE_NUMEROS);
			}
			endereco.setNumero(null);
		} else {
			endereco.setNumero(localidadeVO.getNumeroEndereco());
		}
		if (StringUtils.isEmpty(localidadeVO.getComplementoEndereco())) {
			endereco.setComplemento(null);
		} else {
			endereco.setComplemento(localidadeVO.getComplementoEndereco());
		}
		localidade.setEndereco(endereco);

		if (localidadeVO.getHabilitado() != null) {
			localidade.setHabilitado(localidadeVO.getHabilitado());
		}
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 * @param localidadeVO
	 * @throws GGASException
	 */
	private void carregarCampos(Model model, LocalidadeVO localidadeVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, true);

		Collection<GerenciaRegional> listaGerenciaRegional = controladorGerenciaRegional
				.consultarGerenciaRegional(filtro);
		Collection<LocalidadeClasse> listaLocalidadeClasse = controladorLocalidade.consultarLocalidadeClasses(filtro);
		Collection<LocalidadePorte> listaLocalidadePorte = controladorLocalidade.consultarLocalidadePortes(filtro);

		Collection<MedidorLocalArmazenagem> medidorLocalArmazenagem = controladorLocalidade
				.consultarMedidorLocalArmazenagem(filtro);

		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);
		model.addAttribute("listaLocalidadeClasse", listaLocalidadeClasse);
		model.addAttribute("listaLocalidadePorte", listaLocalidadePorte);
		model.addAttribute("listaMedidorLocalArmazenagem", medidorLocalArmazenagem);

		if (localidadeVO.getGerenciaRegional() != null && localidadeVO.getGerenciaRegional().getChavePrimaria() > 0) {
			filtro.put(ID_GERENCIA_REGIONAL, localidadeVO.getGerenciaRegional().getChavePrimaria());
			Collection<UnidadeNegocio> listaUnidadeNegocio = controladorUnidadeNegocio.consultarUnidadesNegocio(filtro);
			model.addAttribute("listaUnidadeNegocio", listaUnidadeNegocio);
		}

	}

	/**
	 * Carregar campos filtro.
	 * 
	 * @param localidade
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarCamposFiltro(LocalidadeImpl localidade, Model model) throws NegocioException {

		Map<String, Object> filtroGerenciaRegional = new HashMap<String, Object>();
		Map<String, Object> filtroUnidadeNegocio = new HashMap<String, Object>();

		filtroGerenciaRegional.put(HABILITADO, Boolean.TRUE);
		Collection<GerenciaRegional> listaGerenciaRegional = controladorGerenciaRegional
				.consultarGerenciaRegional(filtroGerenciaRegional);

		Collection<UnidadeNegocio> listaUnidadeNegocio = new ArrayList<UnidadeNegocio>();
		if (localidade.getGerenciaRegional() != null && localidade.getGerenciaRegional().getChavePrimaria() > 0) {
			filtroUnidadeNegocio.put(HABILITADO, Boolean.TRUE);
			filtroUnidadeNegocio.put(ID_GERENCIA_REGIONAL, localidade.getGerenciaRegional().getChavePrimaria());
			listaUnidadeNegocio = controladorUnidadeNegocio.consultarUnidadesNegocio(filtroUnidadeNegocio);
		}

		model.addAttribute("listaGerenciaRegional", listaGerenciaRegional);
		model.addAttribute("listaUnidadeNegocio", listaUnidadeNegocio);

	}

	/**
	 * Obter filtro form.
	 * 
	 * @param localidade
	 * @param habilitado
	 * @return Map<String, Object>
	 */
	private Map<String, Object> obterFiltroForm(LocalidadeImpl localidade, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(localidade.getCodigoCentroCusto())) {
			filtro.put(CHAVE_PRIMARIA, Long.valueOf(localidade.getChavePrimaria()));
		}
		if (!StringUtils.isEmpty(localidade.getDescricao())) {
			filtro.put(DESCRICAO, localidade.getDescricao());
		}
		if (localidade.getGerenciaRegional() != null && localidade.getGerenciaRegional().getChavePrimaria() > 0) {
			filtro.put(ID_GERENCIA_REGIONAL, localidade.getGerenciaRegional().getChavePrimaria());
		}
		if (localidade.getUnidadeNegocio() != null && localidade.getUnidadeNegocio().getChavePrimaria() > 0) {
			filtro.put(ID_UNIDADE_NEGOCIO, localidade.getUnidadeNegocio().getChavePrimaria());
		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * Popular form.
	 * 
	 * @param localidadeVO
	 * @param localidade
	 */
	private void popularLocalidadeForm(LocalidadeVO localidadeVO, LocalidadeImpl localidade) {

		if (localidade.getDescricao() != null) {
			localidadeVO.setDescricao(localidade.getDescricao());
		}
		if (localidade.getCodigoFoneDDD() != null) {
			localidadeVO.setDddTelefone(localidade.getCodigoFoneDDD());
		}
		if (localidade.getCodigoFaxDDD() != null) {
			localidadeVO.setDddFax(localidade.getCodigoFaxDDD());
		}
		if (localidade.getUnidadeNegocio() != null) {
			localidadeVO.setUnidadeNegocio(localidade.getUnidadeNegocio());
		}
		if (localidade.getGerenciaRegional() != null) {
			localidadeVO.setGerenciaRegional(localidade.getGerenciaRegional());
		}
		if (localidade.getClasse() != null) {
			localidadeVO.setLocalidadeClasse(localidade.getClasse());
		}
		if (localidade.getPorte() != null) {
			localidadeVO.setLocalidadePorte(localidade.getPorte());
		}
		if (localidade.getMedidorLocalArmazenagem() != null) {
			localidadeVO.setMedidorLocalArmazenagem(localidade.getMedidorLocalArmazenagem());
		}
		if (localidade.getCliente() != null) {
			localidadeVO.setCliente(localidade.getCliente());
		}
		if (localidade.getEndereco() != null) {
			if (localidade.getEndereco() != null && localidade.getEndereco().getCep() != null) {
				localidadeVO.setCep(localidade.getEndereco().getCep().getCep());
			}
			localidadeVO.setNumeroEndereco(localidade.getEndereco().getNumero());
			localidadeVO.setComplementoEndereco(localidade.getEndereco().getComplemento());
		}

		localidadeVO.setHabilitado(localidade.isHabilitado());

		if (localidade.getEmail() != null) {
			localidadeVO.setEmail(localidade.getEmail());
		}

		if (localidade.getCodigoCentroCusto() != null) {
			localidadeVO.setCodigoCentroCusto(localidade.getCodigoCentroCusto());
		}

		if (localidade.getFone() != null) {
			localidadeVO.setFone(localidade.getFone());
		}

		if (localidade.getRamalFone() != null) {
			localidadeVO.setRamalFone(localidade.getRamalFone());
		}

		if (localidade.getFax() != null) {
			localidadeVO.setFax(localidade.getFax());
		}

		if (localidade.getChavePrimaria() > 0) {
			localidadeVO.setCodigo(localidade.getChavePrimaria());
		}

		localidadeVO.setInformatizada(localidade.isInformatizada());
	}

}
