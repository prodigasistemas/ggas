/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.contabil;

import java.io.Serializable;

/**
 * @author bruno silva
 * @see ConsultaLancamentoContabilAction
 * @see ConfigurarEventoComercialAction
 * 
 * Classe responsável pela representação de valores referentes as
 * funcionalidades Consultar Lançamentos Contábeis e Configurar Evento Comercial.
 */
public class ContabilVO implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 338643981051491558L;
	
	private String dataIntervaloContabilInicial;
	private String dataIntervaloContabilFinal;
	private String cnpj;
	private String numeroNotaFiscalInicial;
	private String numeroNotaFiscalFinal;
	private String dataContabil;
	private String eventoComercialDescricao;
	private String segmentoDescricao;
	private String itemContabilDescricao;
	private String contaDebitoDetalhamento;
	private String contaCreditoDetalhamento;
	private String idContaAuxiliarDebito;
	private String idContaAuxiliarCredito;
	private String idContaAuxiliar;
	private String idHistorico;
	private String descricao;
	private String tabela;
	private String coluna;
	private String textoHistorico;
	private String textoContaAuxiliar;
	private String descricaoHistorico;
	private String descricaoContaAuxiliar;
	private String descricaoContaAuxiliarDebito;
	private String descricaoContaAuxiliarCredito;

	private Integer indexLista = -1;

	private Long chavePrimaria;
	private Long idCredito;
	private Long idDebito;
	private Long idTributo;
	private Long idItemContabil;
	private Long checkEventoComercial;
	private Long idTabela;
	private Long idColuna;
	private Long idTextoHistorico;
	private Long idColunaHistorico;
	private Long idColunaContaAuxiliar;
	private Long idModulo;
	private Long idEventoComercial;
	private Long idLancamentoItemContabil;
	private Long idSegmento;
	private Long idContaDebito;
	private Long idContaCredito;

	private Long[] chavesPrimarias;

	private Boolean habilitado = Boolean.TRUE;
	private Boolean indicadorRegimeContabilCompetencia = Boolean.TRUE;

	/**
	 * @return dataIntervaloContabilInicial {@link String}
	 */
	public String getDataIntervaloContabilInicial() {
		return dataIntervaloContabilInicial;
	}

	/**
	 * @param dataIntervaloContabilInicial
	 *            {@link String}
	 */
	public void setDataIntervaloContabilInicial(String dataIntervaloContabilInicial) {
		this.dataIntervaloContabilInicial = dataIntervaloContabilInicial;
	}

	/**
	 * @return dataIntervaloContabilFinal {@link String}
	 */
	public String getDataIntervaloContabilFinal() {
		return dataIntervaloContabilFinal;
	}

	/**
	 * @param dataIntervaloContabilFinal
	 *            {@link String}
	 */
	public void setDataIntervaloContabilFinal(String dataIntervaloContabilFinal) {
		this.dataIntervaloContabilFinal = dataIntervaloContabilFinal;
	}

	/**
	 * @return cnpj {@link String}
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj
	 *            {@link String}
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * @return numeroNotaFiscalInicial {@link String}
	 */
	public String getNumeroNotaFiscalInicial() {
		return numeroNotaFiscalInicial;
	}

	/**
	 * @param numeroNotaFiscalInicial
	 *            {@link String}
	 */
	public void setNumeroNotaFiscalInicial(String numeroNotaFiscalInicial) {
		this.numeroNotaFiscalInicial = numeroNotaFiscalInicial;
	}

	/**
	 * @return numeroNotaFiscalFinal {@link String}
	 */
	public String getNumeroNotaFiscalFinal() {
		return numeroNotaFiscalFinal;
	}

	/**
	 * @param numeroNotaFiscalFinal
	 *            {@link String}
	 */
	public void setNumeroNotaFiscalFinal(String numeroNotaFiscalFinal) {
		this.numeroNotaFiscalFinal = numeroNotaFiscalFinal;
	}

	/**
	 * @return dataContabil {@link String}
	 */
	public String getDataContabil() {
		return dataContabil;
	}

	/**
	 * @param dataContabil
	 *            {@link String}
	 */
	public void setDataContabil(String dataContabil) {
		this.dataContabil = dataContabil;
	}

	/**
	 * @return eventoComercialDescricao {@link String}
	 */
	public String getEventoComercialDescricao() {
		return eventoComercialDescricao;
	}

	/**
	 * @param eventoComercialDescricao
	 *            {@link String}
	 */
	public void setEventoComercialDescricao(String eventoComercialDescricao) {
		this.eventoComercialDescricao = eventoComercialDescricao;
	}

	/**
	 * @return segmentoDescricao {@link String}
	 */
	public String getSegmentoDescricao() {
		return segmentoDescricao;
	}

	/**
	 * @param segmentoDescricao
	 *            {@link String}
	 */
	public void setSegmentoDescricao(String segmentoDescricao) {
		this.segmentoDescricao = segmentoDescricao;
	}

	/**
	 * @param itemContabilDescricao
	 *            {@link String}
	 */
	public String getItemContabilDescricao() {
		return itemContabilDescricao;
	}

	/**
	 * @param itemContabilDescricao
	 *            {@link String}
	 */
	public void setItemContabilDescricao(String itemContabilDescricao) {
		this.itemContabilDescricao = itemContabilDescricao;
	}

	/**
	 * @param contaDebitoDetalhamento
	 *            {@link String}
	 */
	public String getContaDebitoDetalhamento() {
		return contaDebitoDetalhamento;
	}

	/**
	 * @param contaDebitoDetalhamento
	 *            {@link String}
	 */
	public void setContaDebitoDetalhamento(String contaDebitoDetalhamento) {
		this.contaDebitoDetalhamento = contaDebitoDetalhamento;
	}

	/**
	 * @param contaCreditoDetalhamento
	 *            {@link String}
	 */
	public String getContaCreditoDetalhamento() {
		return contaCreditoDetalhamento;
	}

	/**
	 * @param contaCreditoDetalhamento
	 *            {@link String}
	 */
	public void setContaCreditoDetalhamento(String contaCreditoDetalhamento) {
		this.contaCreditoDetalhamento = contaCreditoDetalhamento;
	}

	/**
	 * @param idContaAuxiliarDebito
	 *            {@link String}
	 */
	public String getIdContaAuxiliarDebito() {
		return idContaAuxiliarDebito;
	}

	/**
	 * @param idContaAuxiliarDebito
	 *            {@link String}
	 */
	public void setIdContaAuxiliarDebito(String idContaAuxiliarDebito) {
		this.idContaAuxiliarDebito = idContaAuxiliarDebito;
	}

	/**
	 * @param idContaAuxiliarCredito
	 *            {@link String}
	 */
	public String getIdContaAuxiliarCredito() {
		return idContaAuxiliarCredito;
	}

	/**
	 * @param idContaAuxiliarCredito
	 *            {@link String}
	 */
	public void setIdContaAuxiliarCredito(String idContaAuxiliarCredito) {
		this.idContaAuxiliarCredito = idContaAuxiliarCredito;
	}

	/**
	 * @param idContaAuxiliar
	 *            {@link String}
	 */
	public String getIdContaAuxiliar() {
		return idContaAuxiliar;
	}

	/**
	 * @param idContaAuxiliar
	 *            {@link String}
	 */
	public void setIdContaAuxiliar(String idContaAuxiliar) {
		this.idContaAuxiliar = idContaAuxiliar;
	}

	/**
	 * @param idHistorico
	 *            {@link String}
	 */
	public String getIdHistorico() {
		return idHistorico;
	}

	/**
	 * @param idHistorico
	 *            {@link String}
	 */
	public void setIdHistorico(String idHistorico) {
		this.idHistorico = idHistorico;
	}

	/**
	 * @param descricao
	 *            {@link String}
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            {@link String}
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @param tabela
	 *            {@link String}
	 */
	public String getTabela() {
		return tabela;
	}

	/**
	 * @param tabela
	 *            {@link String}
	 */
	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	/**
	 * @param coluna
	 *            {@link String}
	 */
	public String getColuna() {
		return coluna;
	}

	/**
	 * @param coluna
	 *            {@link String}
	 */
	public void setColuna(String coluna) {
		this.coluna = coluna;
	}

	/**
	 * @param textoHistorico
	 *            {@link String}
	 */
	public String getTextoHistorico() {
		return textoHistorico;
	}

	/**
	 * @param textoHistorico
	 *            {@link String}
	 */
	public void setTextoHistorico(String textoHistorico) {
		this.textoHistorico = textoHistorico;
	}

	/**
	 * @param textoContaAuxiliar
	 *            {@link String}
	 */
	public String getTextoContaAuxiliar() {
		return textoContaAuxiliar;
	}

	/**
	 * @param textoContaAuxiliar
	 *            {@link String}
	 */
	public void setTextoContaAuxiliar(String textoContaAuxiliar) {
		this.textoContaAuxiliar = textoContaAuxiliar;
	}

	/**
	 * @param descricaoHistorico
	 *            {@link String}
	 */
	public String getDescricaoHistorico() {
		return descricaoHistorico;
	}

	/**
	 * @param descricaoHistorico
	 *            {@link String}
	 */
	public void setDescricaoHistorico(String descricaoHistorico) {
		this.descricaoHistorico = descricaoHistorico;
	}

	/**
	 * @param descricaoContaAuxiliar
	 *            {@link String}
	 */
	public String getDescricaoContaAuxiliar() {
		return descricaoContaAuxiliar;
	}

	/**
	 * @param descricaoContaAuxiliar
	 *            {@link String}
	 */
	public void setDescricaoContaAuxiliar(String descricaoContaAuxiliar) {
		this.descricaoContaAuxiliar = descricaoContaAuxiliar;
	}

	/**
	 * @param descricaoContaAuxiliarDebito
	 *            {@link String}
	 */
	public String getDescricaoContaAuxiliarDebito() {
		return descricaoContaAuxiliarDebito;
	}

	/**
	 * @param descricaoContaAuxiliarDebito
	 *            {@link String}
	 */
	public void setDescricaoContaAuxiliarDebito(String descricaoContaAuxiliarDebito) {
		this.descricaoContaAuxiliarDebito = descricaoContaAuxiliarDebito;
	}

	/**
	 * @param descricaoContaAuxiliarCredito
	 *            {@link String}
	 */
	public String getDescricaoContaAuxiliarCredito() {
		return descricaoContaAuxiliarCredito;
	}

	/**
	 * @param descricaoContaAuxiliarCredito
	 *            {@link String}
	 */
	public void setDescricaoContaAuxiliarCredito(String descricaoContaAuxiliarCredito) {
		this.descricaoContaAuxiliarCredito = descricaoContaAuxiliarCredito;
	}

	/**
	 * @param indexLista
	 *            {@link Integer}
	 */
	public Integer getIndexLista() {
		return indexLista;
	}

	/**
	 * @param indexLista
	 *            {@link Integer}
	 */
	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}

	/**
	 * @param chavePrimaria
	 *            {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @param idCredito
	 *            {@link Long}
	 */
	public Long getIdCredito() {
		return idCredito;
	}

	/**
	 * @param idCredito
	 *            {@link Long}
	 */
	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	/**
	 * @param idDebito
	 *            {@link Long}
	 */
	public Long getIdDebito() {
		return idDebito;
	}

	/**
	 * @param idDebito
	 *            {@link Long}
	 */
	public void setIdDebito(Long idDebito) {
		this.idDebito = idDebito;
	}

	/**
	 * @param idTributo
	 *            {@link Long}
	 */
	public Long getIdTributo() {
		return idTributo;
	}

	/**
	 * @param idTributo
	 *            {@link Long}
	 */
	public void setIdTributo(Long idTributo) {
		this.idTributo = idTributo;
	}

	/**
	 * @param idItemContabil
	 *            {@link Long}
	 */
	public Long getIdItemContabil() {
		return idItemContabil;
	}

	/**
	 * @param idItemContabil
	 *            {@link Long}
	 */
	public void setIdItemContabil(Long idItemContabil) {
		this.idItemContabil = idItemContabil;
	}

	/**
	 * @param checkEventoComercial
	 *            {@link Long}
	 */
	public Long getCheckEventoComercial() {
		return checkEventoComercial;
	}

	/**
	 * @param checkEventoComercial
	 *            {@link Long}
	 */
	public void setCheckEventoComercial(Long checkEventoComercial) {
		this.checkEventoComercial = checkEventoComercial;
	}

	/**
	 * @param idTabela
	 *            {@link Long}
	 */
	public Long getIdTabela() {
		return idTabela;
	}

	/**
	 * @param idTabela
	 *            {@link Long}
	 */
	public void setIdTabela(Long idTabela) {
		this.idTabela = idTabela;
	}

	/**
	 * @param idColuna
	 *            {@link Long}
	 */
	public Long getIdColuna() {
		return idColuna;
	}

	/**
	 * @param idColuna
	 *            {@link Long}
	 */
	public void setIdColuna(Long idColuna) {
		this.idColuna = idColuna;
	}

	/**
	 * @param idTextoHistorico
	 *            {@link Long}
	 */
	public Long getIdTextoHistorico() {
		return idTextoHistorico;
	}

	/**
	 * @param idTextoHistorico
	 *            {@link Long}
	 */
	public void setIdTextoHistorico(Long idTextoHistorico) {
		this.idTextoHistorico = idTextoHistorico;
	}

	/**
	 * @param idColunaHistorico
	 *            {@link Long}
	 */
	public Long getIdColunaHistorico() {
		return idColunaHistorico;
	}

	/**
	 * @param idColunaHistorico
	 *            {@link Long}
	 */
	public void setIdColunaHistorico(Long idColunaHistorico) {
		this.idColunaHistorico = idColunaHistorico;
	}

	/**
	 * @param idColunaContaAuxiliar
	 *            {@link Long}
	 */
	public Long getIdColunaContaAuxiliar() {
		return idColunaContaAuxiliar;
	}

	/**
	 * @param idColunaContaAuxiliar
	 *            {@link Long}
	 */
	public void setIdColunaContaAuxiliar(Long idColunaContaAuxiliar) {
		this.idColunaContaAuxiliar = idColunaContaAuxiliar;
	}

	/**
	 * @param idModulo
	 *            {@link Long}
	 */
	public Long getIdModulo() {
		return idModulo;
	}

	/**
	 * @param idModulo
	 *            {@link Long}
	 */
	public void setIdModulo(Long idModulo) {
		this.idModulo = idModulo;
	}

	/**
	 * @param idEventoComercial
	 *            {@link Long}
	 */
	public Long getIdEventoComercial() {
		return idEventoComercial;
	}

	/**
	 * @param idEventoComercial
	 *            {@link Long}
	 */
	public void setIdEventoComercial(Long idEventoComercial) {
		this.idEventoComercial = idEventoComercial;
	}

	/**
	 * @param idLancamentoItemContabil
	 *            {@link Long}
	 */
	public Long getIdLancamentoItemContabil() {
		return idLancamentoItemContabil;
	}

	/**
	 * @param idLancamentoItemContabil
	 *            {@link Long}
	 */
	public void setIdLancamentoItemContabil(Long idLancamentoItemContabil) {
		this.idLancamentoItemContabil = idLancamentoItemContabil;
	}

	/**
	 * @param idSegmento
	 *            {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}

	/**
	 * @param idSegmento
	 *            {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	/**
	 * @param idContaDebito
	 *            {@link Long}
	 */
	public Long getIdContaDebito() {
		return idContaDebito;
	}

	/**
	 * @param idContaDebito
	 *            {@link Long}
	 */
	public void setIdContaDebito(Long idContaDebito) {
		this.idContaDebito = idContaDebito;
	}

	/**
	 * @param idContaCredito
	 *            {@link Long}
	 */
	public Long getIdContaCredito() {
		return idContaCredito;
	}

	/**
	 * @param idContaCredito
	 *            {@link Long}
	 */
	public void setIdContaCredito(Long idContaCredito) {
		this.idContaCredito = idContaCredito;
	}

	/**
	 * @param chavesPrimarias
	 *            {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if (this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}

	/**
	 * @param chavesPrimarias
	 *            {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}
	}

	/**
	 * @param habilitado
	 *            {@link Boolean}
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}

	/**
	 * @param habilitado
	 *            {@link Boolean}
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * @param indicadorRegimeContabilCompetencia
	 *            {@link Boolean}
	 */
	public Boolean getIndicadorRegimeContabilCompetencia() {
		return indicadorRegimeContabilCompetencia;
	}

	/**
	 * @param indicadorRegimeContabilCompetencia
	 *            {@link Boolean}
	 */
	public void setIndicadorRegimeContabilCompetencia(Boolean indicadorRegimeContabilCompetencia) {
		this.indicadorRegimeContabilCompetencia = indicadorRegimeContabilCompetencia;
	}

}
