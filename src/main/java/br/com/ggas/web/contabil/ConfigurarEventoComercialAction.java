/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.contabil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.contabil.ControladorContaContabil;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Configurar Evento Comercial.
 */
@Controller
public class ConfigurarEventoComercialAction extends GenericAction {

	private static final String DADOS_PESQUISA = "dadosPesquisa";

	private static final String LISTA_COMPLETA = "listaCompleta";

	private static final String HABILITADO = "habilitado";

	private static final String ATRIBUTO_FORM_ID_MODULO = "idModulo";

	private static final String LISTA_EVENTO_COMERCIAL = "listaEventoComercial";

	private static final String LISTA_LANCAMENTO_EVENTO_COMERCIAL = "listaLancamentoEventoComercial";

	private static final String DESCRICAO_EVENTO_COMERCIAL = "eventoComercialDescricao";

	private static final String ATRIBUTO_FORM_HISTORICO = "descricaoHistorico";

	private static final String ATRIBUTO_FORM_CONTA_AUXILIAR_DEBITO = "descricaoContaAuxiliarDebito";

	private static final String ATRIBUTO_FORM_CONTA_AUXILIAR_CREDITO = "descricaoContaAuxiliarCredito";

	@Autowired
	private ControladorModulo controladorModulo;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorContabil controladorContabil;
	
	@Autowired
	private ControladorContaContabil controladorContaContabil;
	
	@Autowired
	private ControladorTributo controladorTributo;
	
	@Autowired
	private ControladorRubrica controladorRubrica;
	
	@Autowired
	private ControladorAuditoria controladorAuditoria;
	
	/**
	 * Exibir pesquisar evento comercial.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaEventoComercial")
	public String exibirPesquisaEventoComercial(HttpServletRequest request, Model model) throws GGASException {

		carregarCamposEventoComercial(model);

		request.getSession().removeAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		request.getSession().removeAttribute(LISTA_COMPLETA);

		request.getSession().removeAttribute(DADOS_PESQUISA);

		return "exibirPesquisaEventoComercial";
	}

	/**
	 * Pesquisar evento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarEventoComercial")
	public String pesquisarEventoComercial(ContabilVO contabilVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Boolean acao = Boolean.valueOf(request.getParameter("acao"));

		if (acao && request.getSession().getAttribute(DADOS_PESQUISA) != null) {
			contabilVO = (ContabilVO) request.getSession().getAttribute(DADOS_PESQUISA);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();

		prepararFiltro(filtro, contabilVO);

		Collection<EventoComercial> listaEvento = controladorContabil.consultarTodosEventosComerciaisPorFiltro(filtro);

		model.addAttribute(LISTA_EVENTO_COMERCIAL, listaEvento);

		model.addAttribute("contabilVO", contabilVO);

		return exibirPesquisaEventoComercial(request, model);
	}

	/**
	 * Configurar evento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return configurarEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("configurarEventoComercial")
	public String configurarEventoComercial(ContabilVO contabilVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Boolean acao = Boolean.valueOf(request.getParameter("acao"));

		try {
			exibirDadosLancamento(contabilVO, request, model);
			carregarCamposEventoComercial(model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("contabilVO", contabilVO);

		if (acao) {
			manterDadosPesquisa(contabilVO, request);
			limparFormularioEventoComercial(contabilVO);
		}

		return "configurarEventoComercial";
	}

	/**
	 * Remover evento comercial lancamento.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return configurarEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerEventoComercialLancamento")
	public String removerEventoComercialLancamento(ContabilVO contabilVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Collection<EventoComercialLancamento> listaEvento = (Collection<EventoComercialLancamento>) request.getSession()
				.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		if (listaEvento != null && contabilVO.getIndexLista() != null) {

			((List<EventoComercialLancamento>) listaEvento).get(contabilVO.getIndexLista().intValue());
			((List<EventoComercialLancamento>) listaEvento).remove(contabilVO.getIndexLista().intValue());
		}

		request.getSession().setAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL, listaEvento);

		return configurarEventoComercial(contabilVO, result, request, model);
	}

	/**
	 * Adicionar lancamento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return configurarEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarLancamentoComercial")
	public String adicionarLancamentoComercial(ContabilVO contabilVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Integer indexLista = contabilVO.getIndexLista();

		try {

			exibirDadosLancamento(contabilVO, request, model);

			carregarCamposEventoComercial(model);

			List<EventoComercialLancamento> lista = (List<EventoComercialLancamento>) request.getSession()
					.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

			if (indexLista == null || indexLista < 0) {

				EventoComercialLancamento evento = controladorContabil.criarEventoComercialLancamento();

				popularLancamento(evento, contabilVO);

				if ((lista == null) || (lista.isEmpty())) {
					lista = new ArrayList<EventoComercialLancamento>();
				}

				controladorContabil.validarNovoEventoComercialLancamento(evento, lista);

				lista.add(evento);

			} else {
				alterarLancamentoComercial(contabilVO, lista, request);
			}

			model.addAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL, lista);

			request.getSession().setAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL, lista);

			limparFormularioEventoComercial(contabilVO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return configurarEventoComercial(contabilVO, result, request, model);
	}

	/**
	 * Exibir lancamento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return configurarEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirLancamentoComercial")
	public String exibirLancamentoComercial(ContabilVO contabilVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Integer indexLista = contabilVO.getIndexLista();

		exibirDadosLancamento(contabilVO, request, model);

		carregarCamposEventoComercial(model);

		List<EventoComercialLancamento> lista = (List<EventoComercialLancamento>) request.getSession()
				.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		if (indexLista != null && lista != null) {

			EventoComercialLancamento eventoSelecionado = lista.get(indexLista);

			if (eventoSelecionado != null) {
				popularFormularioLancamento(eventoSelecionado, contabilVO);
			}

		}

		return configurarEventoComercial(contabilVO, result, request, model);
	}

	/**
	 * Alterar lancamento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param listaEvento {@link List}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void alterarLancamentoComercial(ContabilVO contabilVO, List<EventoComercialLancamento> listaEvento,
			HttpServletRequest request) throws GGASException {

		if (listaEvento != null && contabilVO.getIndexLista() != null) {

			EventoComercialLancamento eventoAlterado = controladorContabil.criarEventoComercialLancamento();
			popularLancamento(eventoAlterado, contabilVO);
			EventoComercialLancamento eventoExistente = null;

			controladorContabil.validarNovoEventoComercialLancamento(eventoAlterado,
					(Collection<EventoComercialLancamento>) request.getSession()
							.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL));

			eventoExistente = listaEvento.get(contabilVO.getIndexLista());

			if (eventoExistente != null) {

				popularLancamento(eventoExistente, contabilVO);

			}
		}
	}

	/**
	 * Popular formulario lancamento.
	 * 
	 * @param evento {@link EventoComercialLancamento}
	 * @param contabilVO {@link ContabilVO}
	 */
	private void popularFormularioLancamento(EventoComercialLancamento evento, ContabilVO contabilVO){

		if (evento.getContaContabilDebito() != null) {
			contabilVO.setIdDebito(evento.getContaContabilDebito().getChavePrimaria());
		}

		if (evento.getContaContabilCredito() != null) {
			contabilVO.setIdCredito(evento.getContaContabilCredito().getChavePrimaria());
		}

		if (evento.getSegmento() != null) {
			contabilVO.setIdSegmento(evento.getSegmento().getChavePrimaria());
		}

		if (evento.getTributo() != null) {
			contabilVO.setIdTributo(evento.getTributo().getChavePrimaria());
		}

		if (evento.getLancamentoItemContabil() != null) {
			contabilVO.setIdItemContabil(evento.getLancamentoItemContabil().getChavePrimaria());
		}

		popularFormularioLancamentoDois(evento, contabilVO);

	}

	/**
	 * Popular formulario lancamento.
	 * 
	 * @param evento {@link EventoComercialLancamento}
	 * @param contabilVO {@link ContabilVO}
	 */
	public void popularFormularioLancamentoDois(EventoComercialLancamento evento, ContabilVO contabilVO) {
		if (evento.getEventoComercial() != null) {
			contabilVO.setIdEventoComercial(evento.getEventoComercial().getChavePrimaria());
		}

		if (evento.getDescricaoHistorico() != null) {
			contabilVO.setIdHistorico(evento.getDescricaoHistorico());
		}

		if (evento.getDescricaoContaAuxiliarDebito() != null) {
			contabilVO.setIdContaAuxiliarDebito(evento.getDescricaoContaAuxiliarDebito());
		}
		
		if (evento.getDescricaoContaAuxiliarCredito() != null) {
			contabilVO.setIdContaAuxiliarCredito(evento.getDescricaoContaAuxiliarCredito());
		}

		if (evento.getIndicadorRegimeContabilCompetencia() != null) {
			contabilVO.setIndicadorRegimeContabilCompetencia(evento.getIndicadorRegimeContabilCompetencia());
		}
	}

	/**
	 * Popular lancamento.
	 * 
	 * @param evento {@link EventoComercialLancamento}
	 * @param contabilVO {@link ContabilVO}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularLancamento(EventoComercialLancamento evento, ContabilVO contabilVO) throws GGASException {

		Long idDebito = contabilVO.getIdDebito();
		Long idCredito = contabilVO.getIdCredito();
		Long idEventoComercial = contabilVO.getIdEventoComercial();

		evento.setDescricaoHistorico(contabilVO.getDescricaoHistorico());
		evento.setDescricaoContaAuxiliarDebito(contabilVO.getDescricaoContaAuxiliarDebito());
		evento.setDescricaoContaAuxiliarCredito(contabilVO.getDescricaoContaAuxiliarCredito());

		if (idEventoComercial != null && idEventoComercial > 0) {
			evento.setEventoComercial(controladorContabil.obterEventoComercial(idEventoComercial));
		}

		if (idCredito != null && idCredito > 0) {
			evento.setContaContabilCredito(controladorContaContabil.obterContaContabil(idCredito));
		} else {
			evento.setContaContabilCredito(null);
		}

		if (idDebito != null && idDebito > 0) {
			evento.setContaContabilDebito(controladorContaContabil.obterContaContabil(idDebito));
		} else {
			evento.setContaContabilDebito(null);
		}

		popularLacamentoSegmentoTributoItemContabilIndicador(evento, contabilVO);

		evento.setHabilitado(true);
		evento.setChavePrimaria(0);
		evento.setUltimaAlteracao(Calendar.getInstance().getTime());
	}

	/**
	 * Popular lancamento.
	 * 
	 * @param evento {@link EventoComercialLancamento}
	 * @param contabilVO {@link ContabilVO}
	 * @throws GGASException {@link GGASException}
	 */
	public void popularLacamentoSegmentoTributoItemContabilIndicador(EventoComercialLancamento evento,
			ContabilVO contabilVO) throws GGASException {

		if (contabilVO.getIdTributo() != null && contabilVO.getIdTributo() > 0) {
			evento.setTributo(controladorTributo.obterTributo(contabilVO.getIdTributo()));
		} else {
			evento.setTributo(null);
		}

		if (contabilVO.getIdSegmento() != null && contabilVO.getIdSegmento() > 0) {
			evento.setSegmento(controladorSegmento.obterSegmento(contabilVO.getIdSegmento()));
		} else {
			evento.setSegmento(null);
		}

		if (contabilVO.getIdItemContabil() != null && contabilVO.getIdItemContabil() > 0) {
			evento.setLancamentoItemContabil(
					controladorRubrica.obterLancamentoItemContabil(contabilVO.getIdItemContabil()));
		} else {
			evento.setLancamentoItemContabil(null);
		}

		if (contabilVO.getIndicadorRegimeContabilCompetencia() != null) {
			evento.setIndicadorRegimeContabilCompetencia(contabilVO.getIndicadorRegimeContabilCompetencia());
		}
	}

	/**
	 * Exibir dados lancamento.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void exibirDadosLancamento(ContabilVO contabilVO, HttpServletRequest request, Model model)
			throws GGASException {

		Long chavePrimaria = contabilVO.getIdEventoComercial();

		Collection<EventoComercialLancamento> lista = null;

		Collection<EventoComercialLancamento> listaSessao = (Collection<EventoComercialLancamento>) request.getSession()
				.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		if (listaSessao == null) {

			lista = controladorContabil.consultarEventoComercialLancamentoPorEvento(chavePrimaria);
			Collection<EventoComercialLancamento> listaCompleta = controladorContabil
					.consultarEventoComercialLancamentoPorEvento(chavePrimaria);
			request.getSession().setAttribute(LISTA_COMPLETA, listaCompleta);

		} else {

			lista = (Collection<EventoComercialLancamento>) request.getSession()
					.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		}

		lista = aplicarMascaraContaContabilEmEventoComercialLancamento(lista);

		request.getSession().setAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL, lista);

		model.addAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL, lista);
	}

	/**
	 * Salvar alteracoes configuracao.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return configurarEventoComercial {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("salvarAlteracoesConfiguracao")
	public String salvarAlteracoesConfiguracao(ContabilVO contabilVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		Collection<EventoComercialLancamento> l1 = (Collection<EventoComercialLancamento>) request.getSession()
				.getAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

		Collection<EventoComercialLancamento> l2 = (Collection<EventoComercialLancamento>) request.getSession()
				.getAttribute(LISTA_COMPLETA);

		try {

			controladorContabil.removerEventosComerciaisLancamento(l1, l2);
			request.getSession().removeAttribute(LISTA_COMPLETA);
			request.getSession().removeAttribute(LISTA_LANCAMENTO_EVENTO_COMERCIAL);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_CONFIGURADA, "Evento Comercial");

			limparFormularioEventoComercial(contabilVO);

			return pesquisarEventoComercial(contabilVO, result, request, model);

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return configurarEventoComercial(contabilVO, result, request, model);

	}

	/**
	 * Carregar campos evento comercial.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCamposEventoComercial(Model model) throws GGASException {

		Map<String, String> filtroModulo = new HashMap<String, String>();

		filtroModulo.put("indicadorContabil", "true");

		model.addAttribute("listaModulo", controladorModulo.consultarModulos(filtroModulo));
		model.addAttribute("listaContabil", controladorContabil.listarContaContabil());
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaTributo", controladorTributo.listarTributos());
		model.addAttribute("listaItemContabil", controladorRubrica.listarLancamentoItemContabil());

		Collection<Coluna> lista = controladorAuditoria.listarTabelaVariavel();

		model.addAttribute("listaTabela", lista);

	}

	/**
	 * Aplicar mascara conta contabil em evento comercial lancamento.
	 * 
	 * @param lista {@link Collection}
	 * @return eventoComercialLancamentoList {@link List}
	 * @throws NegocioException {@link NegocioException}
	 */
	public List<EventoComercialLancamento> aplicarMascaraContaContabilEmEventoComercialLancamento(
			Collection<EventoComercialLancamento> lista) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String mascara = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_CONTABILIDADE_MASCARA_NUMERO_CONTA);

		List<EventoComercialLancamento> eventoComercialLancamentoList = new ArrayList<EventoComercialLancamento>();

		for (EventoComercialLancamento eventoComercialLancamento : lista) {
			if (eventoComercialLancamento.getContaContabilCredito() != null) {
				eventoComercialLancamento.getContaContabilCredito().setNumeroConta(Util.formatarMascaraTruncada(mascara,
						eventoComercialLancamento.getContaContabilCredito().getNumeroConta()));
			}
			if (eventoComercialLancamento.getContaContabilDebito() != null) {
				eventoComercialLancamento.getContaContabilDebito().setNumeroConta(Util.formatarMascaraTruncada(mascara,
						eventoComercialLancamento.getContaContabilDebito().getNumeroConta()));
			}
			eventoComercialLancamentoList.add(eventoComercialLancamento);
		}

		return eventoComercialLancamentoList;
	}
	
	/**
	 * Limpar formulario evento comercial.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 */
	private void limparFormularioEventoComercial(ContabilVO contabilVO) {

		contabilVO.setIdModulo(-1L);
		contabilVO.setEventoComercialDescricao("");
		contabilVO.setIdDebito(-1L);
		contabilVO.setIdCredito(-1L);
		contabilVO.setIdSegmento(-1L);
		contabilVO.setIdTributo(-1L);
		contabilVO.setIdItemContabil(-1L);
		contabilVO.setIdHistorico("");
		contabilVO.setIdContaAuxiliarCredito("");
		contabilVO.setIdContaAuxiliarDebito("");
		contabilVO.setIndicadorRegimeContabilCompetencia(true);
		contabilVO.setDescricaoHistorico("");
		contabilVO.setDescricaoContaAuxiliarCredito("");
		contabilVO.setDescricaoContaAuxiliarDebito("");

	}
	
	/**
	 * Método responsável por preservar os dados de uma pesquisa
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param request {@link HttpServletRequest}
	 */
	private void manterDadosPesquisa(ContabilVO contabilVO, HttpServletRequest request) {

		ContabilVO contabilVODadosPesquisa = new ContabilVO();

		contabilVODadosPesquisa.setIdModulo(contabilVO.getIdModulo());
		contabilVODadosPesquisa.setEventoComercialDescricao(contabilVO.getEventoComercialDescricao());
		contabilVODadosPesquisa.setIdSegmento(contabilVO.getIdSegmento());
		contabilVODadosPesquisa.setIdDebito(contabilVO.getIdDebito());
		contabilVODadosPesquisa.setIdCredito(contabilVO.getIdCredito());
		contabilVODadosPesquisa.setIdTributo(contabilVO.getIdTributo());
		contabilVODadosPesquisa.setIdItemContabil(contabilVO.getIdItemContabil());
		contabilVODadosPesquisa.setDescricaoHistorico(contabilVO.getDescricaoHistorico());
		contabilVODadosPesquisa.setDescricaoContaAuxiliarCredito(contabilVO.getDescricaoContaAuxiliarCredito());
		contabilVODadosPesquisa.setDescricaoContaAuxiliarDebito(contabilVO.getDescricaoContaAuxiliarDebito());

		request.getSession().setAttribute(DADOS_PESQUISA, contabilVODadosPesquisa);

	}
	
	/**
	 * Método responsável por preparar o filtro
	 * para pesquisa.
	 * 
	 * @param filtro {@link Map}
	 * @param contabilVO {@link ContabilVO}
	 */
	private void prepararFiltro(Map<String, Object> filtro, ContabilVO contabilVO){
		
		filtro.put(HABILITADO, contabilVO.getHabilitado());

		if ((contabilVO.getIdModulo() != null) && (contabilVO.getIdModulo() > 0)) {
			filtro.put(ATRIBUTO_FORM_ID_MODULO, contabilVO.getIdModulo());
		}

		if ((contabilVO.getIdDebito() != null) && (contabilVO.getIdDebito() > 0)) {
			filtro.put("idDebito1", contabilVO.getIdDebito());
		}

		if ((contabilVO.getIdCredito() != null) && (contabilVO.getIdCredito() > 0)) {
			filtro.put("idCredito1", contabilVO.getIdCredito());
		}

		if ((contabilVO.getIdItemContabil() != null) && (contabilVO.getIdItemContabil() > 0)) {
			filtro.put("idItemContabil1", contabilVO.getIdItemContabil());
		}

		prepararFiltroDois(filtro, contabilVO);

	}

	/**
	 * Método responsável por preparar o filtro
	 * para pesquisa.
	 * 
	 * @param filtro {@link Map}
	 * @param contabilVO {@link ContabilVO}
	 */
	public void prepararFiltroDois(Map<String, Object> filtro, ContabilVO contabilVO) {

		if ((contabilVO.getIdTributo() != null) && (contabilVO.getIdTributo() > 0)) {
			filtro.put("idTributo1", contabilVO.getIdTributo());
		}
		
		if ((contabilVO.getIdSegmento() != null) && (contabilVO.getIdSegmento() > 0)) {
			filtro.put("idSegmento1", contabilVO.getIdSegmento());
		}

		if (StringUtils.isNotEmpty(contabilVO.getEventoComercialDescricao())) {
			filtro.put(DESCRICAO_EVENTO_COMERCIAL, contabilVO.getEventoComercialDescricao());
		}

		if (StringUtils.isNotEmpty(contabilVO.getDescricaoHistorico())) {
			filtro.put(ATRIBUTO_FORM_HISTORICO, contabilVO.getDescricaoHistorico());
		}

		if (StringUtils.isNotEmpty(contabilVO.getDescricaoContaAuxiliarDebito())) {
			filtro.put(ATRIBUTO_FORM_CONTA_AUXILIAR_DEBITO, contabilVO.getDescricaoContaAuxiliarDebito());
		}

		if (StringUtils.isNotEmpty(contabilVO.getDescricaoContaAuxiliarCredito())) {
			filtro.put(ATRIBUTO_FORM_CONTA_AUXILIAR_CREDITO, contabilVO.getDescricaoContaAuxiliarCredito());
		}
	}
	
}
