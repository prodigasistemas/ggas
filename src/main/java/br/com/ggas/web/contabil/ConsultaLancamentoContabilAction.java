/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.contabil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercial;
import br.com.ggas.contabil.LancamentoContabilAnalitico;
import br.com.ggas.contabil.LancamentoContabilDetalhamentoVO;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.contabil.LancamentoContabilSinteticoVO;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a funcionalidade
 * Consultar Lançamentos Contábeis.
 */
@Controller
public class ConsultaLancamentoContabilAction extends GenericAction {

	private static final String NUMERO_NOTA_FISCAL_FINAL = "numeroNotaFiscalFinal";

	private static final String NUMERO_NOTA_FISCAL_INICIAL = "numeroNotaFiscalInicial";

	private static final String CLIENTE = "cliente";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String LISTA_LANCAMENTO_ITEM_CONTABIL = "listaLancamentoItemContabil";

	private static final String HABILITADO = "habilitado";

	private static final String ATRIBUTO_FORM_DATA_INTERVALO_CONTABIL_INICIAL = "dataIntervaloContabilInicial";

	private static final String ATRIBUTO_FORM_DATA_INTERVALO_CONTABIL_FINAL = "dataIntervaloContabilFinal";

	private static final String ATRIBUTO_FORM_ID_MODULO = "idModulo";

	private static final String ATRIBUTO_FORM_ID_LANCAMENTO_CONTABIL_ITEM = "idItemContabil";

	private static final String ATRIBUTO_FORM_ID_SEGMENTO = "idSegmento";

	private static final String LISTA_EVENTO_COMERCIAL = "listaEventoComercial";
	
	private static final String ATRIBUTO_FORM_ID_EVENTO_COMERCIAL = "idEventoComercial";

	@Autowired
	private ControladorModulo controladorModulo;
	
	@Autowired
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	private ControladorDevolucao controladorDevolucao;
	
	@Autowired
	private ControladorContabil controladorContabil;
	
	@Autowired
	private ControladorTributo controladorTributo;
	
	@Autowired
	private ControladorRubrica controladorRubrica;
	
	@Autowired
	private ControladorCobranca controladorCobranca;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorFatura controladorFatura;
	
	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;
	
	@Autowired
	private ControladorParcelamento controladorParcelamento;
	
	@Autowired
	private ControladorRecebimento controladorRecebimento;
	
	@Autowired
	private ControladorArrecadacao controladorArrecadacao;
	
	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de Lancamentos Contábeis.
	 * 
	 * @param model {@link Model}
	 * @return exibirPesquisaContabil {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaLancamentoContabil")
	public String exibirPesquisaLancamentoContabil(Model model) throws GGASException {

		carregarCampos(model);

		return "exibirPesquisaLancamentoContabil";
	}
	
	/**
	 * Método responsável por pesquisar os
	 * Lançamentos Contábeis no sistema.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaLancamentoContabil {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarLancamentoContabil")
	public String pesquisarLancamentoContabil(ContabilVO contabilVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();

		Collection<LancamentoContabilSinteticoVO> listaLancamentoContabilSinteticoVO = null;

		try {

			prepararFiltro(contabilVO, filtro);

			if (validarNotaFiscalInicialFinal(contabilVO)) {
				listaLancamentoContabilSinteticoVO = controladorContabil
						.consultarLancamentoContabilSinteticoPorFaturamento(filtro);
			}
			
			Collection<LancamentoContabilSintetico> listaLancamentoContabilSintetico = controladorContabil
					.consultarLancamentoContabilSintetico(filtro);

			controladorContabil.popularContaCreditoDebitoLancamentoSintetico(listaLancamentoContabilSintetico);

			model.addAttribute("listaLancamentoContabilSintetico", listaLancamentoContabilSintetico);
			model.addAttribute("listaLancamentoContabilSinteticoVO", listaLancamentoContabilSinteticoVO);

			carregarCampos(model);

			filtro.put(ATRIBUTO_FORM_ID_MODULO, contabilVO.getIdModulo());
			model.addAttribute(LISTA_EVENTO_COMERCIAL, controladorContabil.consultarTodosEventosComerciais(filtro));

			if (contabilVO.getIdEventoComercial() != null && contabilVO.getIdEventoComercial() > 0) {

				EventoComercial eventoComercial = controladorContabil
						.obterEventoComercial(contabilVO.getIdEventoComercial());

				if (eventoComercial.getIndicadorComplemento() != null) {
					
					verificarIndicadorComplemento(eventoComercial, request, model);
				
				}
			}
			
		} catch (GGASException e) {
			
			mensagemErroParametrizado(model, request, e);
			
			String mensagem = (String) model.asMap().get("msg");
			
			if(("!").equals(mensagem.substring(0,1))) {
				model.asMap().put("msg", mensagem.substring(1, mensagem.length()-1));
			}
			
			carregarComplemento(contabilVO, bindingResult, request, model);
		}
		
		saveToken(request);

		return exibirPesquisaLancamentoContabil(model);
	}

	/**
	 * Método responsável por verificar o indicador de complemento
	 * 
	 * @param eventoComercial {@link EventoComercial}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	public void verificarIndicadorComplemento(EventoComercial eventoComercial, HttpServletRequest request, Model model)
			throws GGASException {
		
		if ("I".equals(eventoComercial.getIndicadorComplemento())) {
			model.addAttribute(LISTA_LANCAMENTO_ITEM_CONTABIL,
					controladorRubrica.listarLancamentoItemContabil());
		} else if ("T".equals(eventoComercial.getIndicadorComplemento())) {
			model.addAttribute(LISTA_LANCAMENTO_ITEM_CONTABIL,
					controladorTributo.consultarTributos(null));
		}
	}

	/**
	 * Método responsável por exibir a tela de
	 * detalhamento dos Lançamentos Contabeis.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return tela  {@link String}
	 * @throws GGASException  {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoLancamentoContabil")
	public String exibirDetalhamentoLancamentoContabil(ContabilVO contabilVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String tela = "exibirDetalhamentoLancamentoContabil";

		if (!super.isPostBack(request)) {

			Collection<LancamentoContabilDetalhamentoVO> listaLancamentoContabilDetalhamentoVO = null;

			Long chavePrimaria = contabilVO.getChavePrimaria();

			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("chavePrimaria", chavePrimaria);

			adicionarFiltroPaginacao(request, filtro);

			Collection<LancamentoContabilSintetico> listaLancamentoContabilSintetico = controladorContabil
					.consultarLancamentoContabilSintetico(filtro);

			controladorContabil.popularContaCreditoDebitoLancamentoSintetico(listaLancamentoContabilSintetico);

			LancamentoContabilSintetico lancamentoContabilSintetico = listaLancamentoContabilSintetico.iterator()
					.next();

			if (lancamentoContabilSintetico.getDataContabil() != null) {
				contabilVO.setDataContabil(Util.converterDataParaString(lancamentoContabilSintetico.getDataContabil()));
			}
			if (lancamentoContabilSintetico.getEventoComercial() != null) {
				contabilVO.setEventoComercialDescricao(lancamentoContabilSintetico.getEventoComercial().getDescricao());
			}
			if (lancamentoContabilSintetico.getSegmento() != null) {
				contabilVO.setSegmentoDescricao(lancamentoContabilSintetico.getSegmento().getDescricao());
			}
			if (lancamentoContabilSintetico.getLancamentoItemContabil() != null) {
				contabilVO.setItemContabilDescricao(
						lancamentoContabilSintetico.getLancamentoItemContabil().getDescricao());
			}
			if (lancamentoContabilSintetico.getTributo() != null) {
				contabilVO.setItemContabilDescricao(lancamentoContabilSintetico.getTributo().getDescricao());
			}
			if (lancamentoContabilSintetico.getContaContabilCredito() != null) {
				contabilVO.setContaCreditoDetalhamento(
						lancamentoContabilSintetico.getContaContabilCredito().getNomeConta());
			}
			if (lancamentoContabilSintetico.getContaContabilDebito() != null) {
				contabilVO.setContaDebitoDetalhamento(
						lancamentoContabilSintetico.getContaContabilDebito().getNomeConta());
			}

			try {

				listaLancamentoContabilDetalhamentoVO = montarListaLancamentoContabilDetalhamentoVO(
						lancamentoContabilSintetico);

				model.addAttribute("listaLancamentoContabilDetalhamentoVO", listaLancamentoContabilDetalhamentoVO);

			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
				tela = pesquisarLancamentoContabil(contabilVO, bindingResult, request, model);
			}

		}

		return tela;
	}

	/**
	 * Método responsável por montar a lista de
	 * LancamentoContabilDetalhamentoVO.
	 * 
	 * @param lancamentoContabilSintetico {@link LancamentoContabilSintetico}
	 * @return listaLancamentoContabilDetalhamentoVO {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unused")
	private Collection<LancamentoContabilDetalhamentoVO> montarListaLancamentoContabilDetalhamentoVO(
			LancamentoContabilSintetico lancamentoContabilSintetico) throws GGASException {

		Collection<LancamentoContabilDetalhamentoVO> listaLancamentoContabilDetalhamentoVO = new ArrayList<LancamentoContabilDetalhamentoVO>();

		Collection<LancamentoContabilAnalitico> listaLancamentoContabilAnalitico = controladorContabil
				.consultarLancamentoContabilAnaliticoPorLancamentoContabilSintetico(lancamentoContabilSintetico);

		EventoComercial eventoComercial = controladorContabil.obterEventoComercial(
				lancamentoContabilSintetico.getEventoComercial().getChavePrimaria(), "tabelaReferencia");

		for (LancamentoContabilAnalitico lancamentoContabilAnalitico : listaLancamentoContabilAnalitico) {
			LancamentoContabilDetalhamentoVO lcdVO = new LancamentoContabilDetalhamentoVO();

			if ("FATURA".equals(eventoComercial.getTabelaReferencia().getNome())) {

				Fatura fatura = (Fatura) controladorCobranca.obterFatura(lancamentoContabilAnalitico.getCodigoObjeto(),
						"tipoDocumento", PONTO_CONSUMO, CLIENTE);

				if (fatura != null) {
					if (fatura.getCliente() != null) {
						lcdVO.setCliente(fatura.getCliente().getNome());
					}
					if (fatura.getPontoConsumo() != null) {
						lcdVO.setPontoConsumo(fatura.getPontoConsumo().getDescricao());
					}

					EventoComercial volumeNaoFaturado = controladorContabil.obterEventoComercial(controladorContabil
							.obterChaveEventoComercial("EVENTO_COMERCIAL_INCLUIR_VOLUME_NAO_FATURADO"));

					EventoComercial rendaNaoFaturada = controladorContabil.obterEventoComercial(controladorContabil
							.obterChaveEventoComercial("EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA"));

					EventoComercial rendaNaoFaturadaTributo = controladorContabil
							.obterEventoComercial(controladorContabil
									.obterChaveEventoComercial("EVENTO_COMERCIAL_INCLUIR_RENDA_NAO_FATURADA_TRIBUTO"));

					EventoComercial pddSemestral = controladorContabil.obterEventoComercial(
							controladorContabil.obterChaveEventoComercial("EVENTO_COMERCIAL_PERDA_CREDITO_SEMESTRAL"));

					EventoComercial pddAnual = controladorContabil.obterEventoComercial(
							controladorContabil.obterChaveEventoComercial("EVENTO_COMERCIAL_PERDA_CREDITO_ANUAL"));

					if (lancamentoContabilSintetico.getEventoComercial().getChavePrimaria() == volumeNaoFaturado
							.getChavePrimaria()
							|| lancamentoContabilSintetico.getEventoComercial().getChavePrimaria() == rendaNaoFaturada
									.getChavePrimaria()
							|| lancamentoContabilSintetico.getEventoComercial()
									.getChavePrimaria() == rendaNaoFaturadaTributo.getChavePrimaria()
							|| lancamentoContabilSintetico.getEventoComercial().getChavePrimaria() == pddSemestral
									.getChavePrimaria()
							|| lancamentoContabilSintetico.getEventoComercial().getChavePrimaria() == pddAnual
									.getChavePrimaria()) {

						if (lancamentoContabilSintetico.getSegmento() != null) {
							lcdVO.setObjeto(lancamentoContabilSintetico.getSegmento().getDescricao());
						}
						DateTime dateTime = new DateTime(lancamentoContabilSintetico.getDataContabil());
						lcdVO.setComplemento(dateTime.getMonthOfYear() + "/" + dateTime.getYear());

					} else {

						if (fatura.getTipoDocumento() != null) {

							String parametroSistema = controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);

							if (fatura.getTipoDocumento().getChavePrimaria() == Long.parseLong(parametroSistema)) {
								lcdVO.setObjeto("Nota Fiscal");
								DocumentoFiscal documentoFiscal = controladorFatura
										.obterDocumentoFiscalPorFaturaMaisRecente(fatura.getChavePrimaria());
								if (documentoFiscal != null) {
									lcdVO.setComplemento(String.valueOf(documentoFiscal.getNumero()));
								}

							} else {

								lcdVO.setObjeto(fatura.getTipoDocumento().getDescricao());
								lcdVO.setComplemento(String.valueOf(fatura.getChavePrimaria()));
							}

						} else {
							lcdVO.setComplemento(String.valueOf(fatura.getChavePrimaria()));
						}
					}
				}

			} else if ("CREDITO_DEBITO_NEGOCIADO".equals(eventoComercial.getTabelaReferencia().getNome())) {

				CreditoDebitoNegociado cdn = controladorCreditoDebito.obterCreditoDebitoNegociado(
						lancamentoContabilAnalitico.getCodigoObjeto(), CLIENTE, PONTO_CONSUMO, "rubrica");
				if (cdn != null) {
					if (cdn.getCliente() != null) {
						lcdVO.setCliente(cdn.getCliente().getNome());
					}
					if (cdn.getPontoConsumo() != null) {
						lcdVO.setPontoConsumo(cdn.getPontoConsumo().getDescricao());
					}
					if (cdn.getRubrica() != null) {
						lcdVO.setObjeto(cdn.getRubrica().getDescricao());
					}
				}

			} else if ("PARCELAMENTO".equals(eventoComercial.getTabelaReferencia().getNome())) {

				Parcelamento parcelamento = (Parcelamento) controladorParcelamento
						.obter(lancamentoContabilAnalitico.getCodigoObjeto(), CLIENTE, PONTO_CONSUMO);

				verificaParcelamento(lcdVO, parcelamento);

			} else if ("RECEBIMENTO".equals(eventoComercial.getTabelaReferencia().getNome())) {
				Recebimento recebimento = (Recebimento) controladorRecebimento
						.obter(lancamentoContabilAnalitico.getCodigoObjeto(), CLIENTE, PONTO_CONSUMO, "faturaGeral");

				if (recebimento != null && recebimento.getFaturaGeral() != null
						&& recebimento.getFaturaGeral().getFaturaAtual() != null) {
					
					Fatura faturaAtual = null;
					
					faturaAtual = (Fatura) controladorCobranca.obterFatura(
							recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria(), "tipoDocumento");
				}

				verificaRecebimento(lancamentoContabilSintetico, lcdVO, recebimento);
			}
			lcdVO.setValor(Util.converterCampoCurrencyParaString(lancamentoContabilAnalitico.getValor(),
					Constantes.LOCALE_PADRAO));

			lcdVO.setDescricaoValor(eventoComercial.getDescricaoValor());

			listaLancamentoContabilDetalhamentoVO.add(lcdVO);
		}
		return listaLancamentoContabilDetalhamentoVO;
	}

	/**
	 * Método responsável por verificar Parcelamento
	 * 
	 * @param lancamentoContabilDetalhamentoVO {@link LancamentoContabilDetalhamentoVO}
	 * @param parcelamento {@link parcelamento}
	 */
	private void verificaParcelamento(LancamentoContabilDetalhamentoVO lancamentoContabilDetalhamentoVO,
			Parcelamento parcelamento) {

		if (parcelamento != null) {

			if (parcelamento.getCliente() != null) {
				lancamentoContabilDetalhamentoVO.setCliente(parcelamento.getCliente().getNome());
			}

			if (parcelamento.getPontoConsumo() != null) {
				lancamentoContabilDetalhamentoVO.setPontoConsumo(parcelamento.getPontoConsumo().getDescricao());
			}

			lancamentoContabilDetalhamentoVO.setObjeto("PARCELAMENTO");

			if (parcelamento.getNumeroPrestacoes() != null) {
				lancamentoContabilDetalhamentoVO.setComplemento(parcelamento.getNumeroPrestacoes().toString());
			}
		}
	}

	/**
	 * Método responsável por verificar Recebimento
	 * 	
	 * @param lancamentoContabilSintetico {@link LancamentoContabilSintetico}
	 * @param lcdVO {@link LancamentoContabilDetalhamentoVO}
	 * @param recebimento {@link Recebimento}
	 * @throws GGASException {@link GGASException}
	 */
	private void verificaRecebimento(LancamentoContabilSintetico lancamentoContabilSintetico,
			LancamentoContabilDetalhamentoVO lcdVO, Recebimento recebimento) throws GGASException {
		if (recebimento != null) {
			if (recebimento.getCliente() != null) {
				lcdVO.setCliente(recebimento.getCliente().getNome());
			}
			if (recebimento.getPontoConsumo() != null) {
				lcdVO.setPontoConsumo(recebimento.getPontoConsumo().getDescricao());
			}

			lcdVO.setObjeto("Recebimento");
			if (lancamentoContabilSintetico.getContaBancaria() != null) {
				ContaBancaria contaBancaria = controladorArrecadacao
						.obterContaBancaria(lancamentoContabilSintetico.getContaBancaria().getChavePrimaria());
				Agencia agencia = controladorArrecadacao
						.obterAgenciaPorChave(contaBancaria.getAgencia().getChavePrimaria());
				lcdVO.setComplemento(agencia.getBanco().getNome() + "/" + agencia.getCodigo());
			}

		}
	}

	/**
	 * Método responsável por carregar os campos
	 * na tela de pesquisa lancamento contabil.
	 * 
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCampos(Model model) throws GGASException {

		Map<String, String> filtroModulo = new HashMap<String, String>();
		filtroModulo.put("indicadorContabil", "true");

		model.addAttribute("listaModulo", controladorModulo.consultarModulos(filtroModulo));
		model.addAttribute("listaSegmento", controladorSegmento.consultarSegmento(null));

	}

	/**
	 * Método responsável por carregar eventos
	 * comerciais.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaLancamentoContabil {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("carregarEventoComercial")
	public String carregarEventoComercial(ContabilVO contabilVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		carregarCampos(model);

		filtro.put(ATRIBUTO_FORM_ID_MODULO, contabilVO.getIdModulo());

		model.addAttribute(LISTA_EVENTO_COMERCIAL, controladorContabil.consultarTodosEventosComerciais(filtro));

		saveToken(request);

		return exibirPesquisaLancamentoContabil(model);
	}

	/**
	 * Método responsável por carregar os
	 * Complementos.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaLancamentoContabil {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("carregarComplemento")
	public String carregarComplemento(ContabilVO contabilVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		try {

			carregarCampos(model);

			if (contabilVO.getIdEventoComercial() != null && contabilVO.getIdEventoComercial() > 0) {

				EventoComercial eventoComercial = controladorContabil
						.obterEventoComercial(contabilVO.getIdEventoComercial());

				if (eventoComercial.getIndicadorComplemento() != null) {

					verificarIndicadorComplemento(eventoComercial, request, model);

				}
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		carregarEventoComercial(contabilVO, bindingResult, request, model);

		saveToken(request);

		return exibirPesquisaLancamentoContabil(model);
	}

	/**
	 * Validar nota fiscal inicial final.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @return validacaoNotaFiscal {@link Boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	private Boolean validarNotaFiscalInicialFinal(ContabilVO contabilVO) throws NegocioException {

		boolean validacaoNotaFiscal = false;

		String numeroNotaFiscalInicial = contabilVO.getNumeroNotaFiscalInicial();
		String numeroNotaFiscalFinal = contabilVO.getNumeroNotaFiscalFinal();

		if (StringUtils.isEmpty(numeroNotaFiscalInicial) && StringUtils.isNotEmpty(numeroNotaFiscalFinal)) {
			throw new NegocioException(Util.getMensagens().getString("ERRO_NUMERO_NOTA_FISCAL_INICIAL_OBRIGATORIA"));
		}

		if (StringUtils.isNotEmpty(numeroNotaFiscalInicial) && StringUtils.isNotEmpty(numeroNotaFiscalFinal)) {

			validacaoNotaFiscal = true;

			if (Long.parseLong(numeroNotaFiscalInicial) > Long.parseLong(numeroNotaFiscalFinal)) {
				throw new NegocioException(Util.getMensagens().getString("ERRO_NOTA_INICIAL_MAIOR_NOTA_FINAL"));
			}
		}

		if (StringUtils.isNotEmpty(numeroNotaFiscalInicial) && StringUtils.isEmpty(numeroNotaFiscalFinal)) {
			validacaoNotaFiscal = true;
		}

		return validacaoNotaFiscal;

	}
	
	/**
	 * Método responsável por verificar Evento Comercial.
	 * 
	 * @param filtro {@link Map}
	 * @param contabilVO {@link ContabilVO}
	 */
	private void verificaEventoComercial(Map<String, Object> filtro, ContabilVO contabilVO) throws NegocioException {

		if ((contabilVO.getIdEventoComercial() != null) && (contabilVO.getIdEventoComercial() > 0)) {

			filtro.put(ATRIBUTO_FORM_ID_EVENTO_COMERCIAL, contabilVO.getIdEventoComercial());

			EventoComercial eventoComercial = controladorContabil
					.obterEventoComercial(contabilVO.getIdEventoComercial());

			verificarIndicadorComplementoPrepararFiltro(filtro, contabilVO, eventoComercial);

		}
	}

	/**
	 * Método responsável por verificar o indicador de complemento e adicionar ao
	 * filtro para ser usado na pesquisa
	 * 
	 * @param filtro {@link Map}
	 * @param contabilVO {@link ContabilVO}
	 * @param eventoComercial {@link EventoComercial}
	 */
	public void verificarIndicadorComplementoPrepararFiltro(Map<String, Object> filtro, ContabilVO contabilVO,
			EventoComercial eventoComercial) {

		if (eventoComercial.getIndicadorComplemento() != null) {

			if ("I".equals(eventoComercial.getIndicadorComplemento())) {

				Long idContabilItem = contabilVO.getIdLancamentoItemContabil();

				if ((idContabilItem != null) && (idContabilItem > 0)) {
					filtro.put(ATRIBUTO_FORM_ID_LANCAMENTO_CONTABIL_ITEM, idContabilItem);
				}

			} else if ("T".equals(eventoComercial.getIndicadorComplemento())) {

				Long idTributo = contabilVO.getIdLancamentoItemContabil();

				if ((idTributo != null) && (idTributo > 0)) {
					filtro.put("idTributo", idTributo);
				}
			}

		}
	}
	
	/**
	 * Método responsável por preparar o filtro
	 * para pesquisa.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltro(ContabilVO contabilVO, Map<String, Object> filtro) throws GGASException {

		if (contabilVO.getHabilitado() != null) {
			filtro.put(HABILITADO, contabilVO.getHabilitado());
		}
		
		String dataIntervaloGeracaoInicial = contabilVO.getDataIntervaloContabilInicial();
		String dataIntervaloGeracaoFinal = contabilVO.getDataIntervaloContabilFinal();

		controladorDevolucao.validarDataDevolucaoInicialFinal(dataIntervaloGeracaoInicial, dataIntervaloGeracaoFinal);
		
		verificaEventoComercial(filtro, contabilVO);
		
		prepararFiltroCamposSelects(contabilVO, filtro);
		

		if (StringUtils.isNotEmpty(dataIntervaloGeracaoInicial)) {
			filtro.put(ATRIBUTO_FORM_DATA_INTERVALO_CONTABIL_INICIAL, Util.converterCampoStringParaData("Data Inicio",
					dataIntervaloGeracaoInicial, Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(dataIntervaloGeracaoFinal)) {
			filtro.put(ATRIBUTO_FORM_DATA_INTERVALO_CONTABIL_FINAL, Util.converterCampoStringParaData("Data Fim",
					dataIntervaloGeracaoFinal, Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(contabilVO.getNumeroNotaFiscalInicial())) {
			filtro.put(NUMERO_NOTA_FISCAL_INICIAL, contabilVO.getNumeroNotaFiscalInicial());
		}

		if (StringUtils.isNotEmpty(contabilVO.getNumeroNotaFiscalFinal())) {
			filtro.put(NUMERO_NOTA_FISCAL_FINAL, contabilVO.getNumeroNotaFiscalFinal());
		}
		

	}
	
	/**
	 * Método responsável por preparar o filtro
	 * para pesquisa.
	 * 
	 * @param contabilVO {@link ContabilVO}
	 * @param filtro {@link Map}
	 */
	private void prepararFiltroCamposSelects(ContabilVO contabilVO, Map<String, Object> filtro) {

		if (contabilVO.getIdSegmento() != null && (contabilVO.getIdSegmento() > 0)) {
			filtro.put(ATRIBUTO_FORM_ID_SEGMENTO, contabilVO.getIdSegmento());
		}
		
		if (contabilVO.getIdModulo() != null && contabilVO.getIdModulo() > 0) {
			filtro.put(ATRIBUTO_FORM_ID_MODULO, contabilVO.getIdModulo());
		}

		if ((contabilVO.getIdLancamentoItemContabil() != null) && (contabilVO.getIdLancamentoItemContabil() > 0)) {
			filtro.put(ATRIBUTO_FORM_ID_LANCAMENTO_CONTABIL_ITEM, contabilVO.getIdLancamentoItemContabil());
		}

	}

}
