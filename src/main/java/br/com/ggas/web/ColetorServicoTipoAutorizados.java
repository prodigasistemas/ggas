package br.com.ggas.web;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

import com.google.common.base.Function;

/**
 * Classe reponsável por extrair o {@link ServicoTipo} do 
 * {@link ServicoAutorizacao}. A operação desta classe
 * está sujeita a {@link NullPointerException} caso um valor nulo
 * seja passado como argumento.
 */
public class ColetorServicoTipoAutorizados implements Function<ServicoAutorizacao, ServicoTipo> {

	@Override
	public ServicoTipo apply(ServicoAutorizacao autorizacao) {
		return autorizacao.getServicoTipo();
	}

}
