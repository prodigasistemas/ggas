package br.com.ggas.web.configuracao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelos metodos de ações referentes a Unidade.
 */
@Controller
public class UnidadeAction extends GenericAction {

	private static final String LISTA_UNIDADE_CLASSE = "listaUnidadeClasse";

	private static final String LISTA_UNIDADE = "listaUnidade";

	private static final String DESCRICAO = "descricao";

	private static final String UNIDADE = "unidade";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String HABILITADO = "habilitado";

	@Autowired
	private ControladorUnidade controladorUnidade;
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de Unidade.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaUnidade")
	public String exibirPesquisaUnidade(Model model) throws NegocioException {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		Collection<ClasseUnidade> lista = controladorUnidade.listarClasseUnidade();
		model.addAttribute(LISTA_UNIDADE_CLASSE, lista);

		return "exibirPesquisaUnidade";

	}

	/**
	 * Método responsável por efetuar a pesquisa de Unidade.
	 * 
	 * @param unidade - {@link UnidadeImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarUnidade")
	public String pesquisarUnidade(Model model, UnidadeImpl unidade, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(unidade, habilitado);

		Collection<TabelaAuxiliar> listaUnidade = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, UnidadeImpl.class.getName());

		model.addAttribute(LISTA_UNIDADE, listaUnidade);
		model.addAttribute(UNIDADE, unidade);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaUnidade(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de uma Unidade.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoUnidade")
	public String exibirDetalhamentoUnidade(Model model, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) {

		try {
			UnidadeImpl unidade = (UnidadeImpl) controladorTabelaAuxiliar.obter(chavePrimaria, UnidadeImpl.class, "classeUnidade");
			String unidadeClasse = unidade.getClasseUnidade().getDescricao();
			popularIndicadorPadrao(model, unidade);

			model.addAttribute(UNIDADE, unidade);
			model.addAttribute("descricaoUnidadeClasse", unidadeClasse);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaUnidade";
		}

		return "exibirDetalhamentoUnidade";
	}

	/**
	 * Método responsável por exibir a tela de inserir de Unidade.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirInserirUnidade")
	public String exibirInserirUnidade(Model model) throws NegocioException {

		Collection<ClasseUnidade> listaClasseUnidade = controladorEntidadeConteudo.listarClasseUnidade();
		model.addAttribute(LISTA_UNIDADE_CLASSE, listaClasseUnidade);

		return "exibirInserirUnidade";
	}

	/**
	 * Método responsável por executar a inserção de uma Unidade.
	 * 
	 * @param unidade - {@link UnidadeImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirUnidade")
	public String inserirUnidade(Model model, UnidadeImpl unidade, BindingResult result, HttpServletRequest request) throws GGASException {

		String retorno = null;

		try {

			popularCampos(unidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidade, "Unidade");
			controladorTabelaAuxiliar.inserir(unidade);
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(Unidade.UNIDADE_MENSAGEM));
			retorno = pesquisarUnidade(model, unidade, result, unidade.isHabilitado());
		} catch (GGASException e) {
			Collection<ClasseUnidade> listaClasseUnidade = controladorEntidadeConteudo.listarClasseUnidade();
			model.addAttribute(LISTA_UNIDADE_CLASSE, listaClasseUnidade);
			model.addAttribute(UNIDADE, unidade);
			super.mensagemErroParametrizado(model, request, e);
			retorno = "exibirInserirUnidade";
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de alterar a Unidade.
	 * 
	 * @param model - {@link Model}
	 * @param chavePrimaria - {@link Long}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */

	@RequestMapping("exibirAlterarUnidade")
	public String exibirAlterarUnidade(Model model, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		try {
			Collection<ClasseUnidade> listaClasseUnidade = controladorEntidadeConteudo.listarClasseUnidade();
			model.addAttribute(LISTA_UNIDADE_CLASSE, listaClasseUnidade);

			UnidadeImpl unidade = (UnidadeImpl) controladorTabelaAuxiliar.obter(chavePrimaria, UnidadeImpl.class, "classeUnidade");
			model.addAttribute(UNIDADE, unidade);

		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaUnidade";
		}

		return "exibirAlterarUnidade";

	}

	/**
	 * Método responsável por efetuar a alteração de uma Unidade.
	 * 
	 * @param unidade - {@link UnidadeImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarUnidade")
	private String alterarUnidade(Model model, UnidadeImpl unidade, BindingResult result, HttpServletRequest request) throws GGASException {

		String retorno = null;

		try {

			popularCampos(unidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidade, "Unidade");
			controladorTabelaAuxiliar.atualizar(unidade, UnidadeImpl.class);
			Collection<ClasseUnidade> listaClasseUnidade = controladorEntidadeConteudo.listarClasseUnidade();
			model.addAttribute(LISTA_UNIDADE_CLASSE, listaClasseUnidade);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Unidade.UNIDADE_MENSAGEM));
			retorno = pesquisarUnidade(model, unidade, result, unidade.isHabilitado());
		} catch (NegocioException e) {

			Collection<ClasseUnidade> listaClasseUnidade = controladorEntidadeConteudo.listarClasseUnidade();
			model.addAttribute(LISTA_UNIDADE_CLASSE, listaClasseUnidade);
			model.addAttribute(UNIDADE, unidade);
			super.mensagemErroParametrizado(model, request, e);
			retorno = "exibirAlterarUnidade";
		}

		return retorno;
	}

	/**
	 * Método responsável por executar a remoção um Tipo de Unidade.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("removerUnidade")
	public String removerUnidade(Model model, HttpServletRequest request,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) {

		try {

			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, UnidadeImpl.class, dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, super.obterMensagem(Unidade.UNIDADE_MENSAGEM));

			return pesquisarUnidade(model, null, null, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarUnidade";
	}

	private Map<String, Object> prepararFiltro(UnidadeImpl unidade, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (unidade != null) {

			if (unidade.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", unidade.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(unidade.getDescricao())) {
				filtro.put(DESCRICAO, unidade.getDescricao());
			}

			if (StringUtils.isNotEmpty(unidade.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, unidade.getDescricaoAbreviada());
			}

			if (unidade.getClasseUnidade() != null) {
				filtro.put("idUnidadeClasse", unidade.getClasseUnidade().getChavePrimaria());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * Popular indicador padrao.
	 * 
	 * @param model
	 * @param unidade
	 */
	private void popularIndicadorPadrao(Model model, UnidadeImpl unidade) {

		String indicadorPadrao = null;

		if (unidade.isIndicadorPadrao()) {
			indicadorPadrao = Constantes.SIM;
		} else {
			indicadorPadrao = Constantes.NAO;
		}
		model.addAttribute("descricaoIndicadorPadrao", indicadorPadrao);
	}

	private void popularCampos(UnidadeImpl unidade) throws GGASException {

		String descricao = unidade.getDescricao();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(descricao, UnidadeImpl.class.getName());
			unidade.setDescricao(descricao);
		}

		// Seta a descrição abreviada
		// informada
		String descricaoAbreviada = unidade.getDescricaoAbreviada();
		if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(descricaoAbreviada.trim(), UnidadeImpl.class.getName());
			unidade.setDescricaoAbreviada(descricaoAbreviada.trim());
		}

	}

}
