/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.configuracao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Controlador responsável pelas telas relacionadas ao Entidade Conteudo
 * 
 * @author pedro
 *
 */
@Controller
public class EntidadeConteudoAction extends GenericAction {

	private static final String DESCRICAO = "descricao";

	private static final String ENTIDADE_CONTEUDO = "entidadeConteudo";

	private static final String HABILITADO = "habilitado";

	private static final String TITULO = "Entidade Conteúdo";
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela exibição da tela para pesquisa de entidade conteudo.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaEntidadeConteudo")
	public String exibirPesquisaEntidadeConteudo(Model model) throws NegocioException {
		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}
		carregarEntidadeClasse(model);
		return "exibirPesquisaEntidadeConteudo";
	}

	/**
	 * Método responsável pela pesquisa dos Entidade Conteudo registrados
	 * 
	 * @param entidadeConteudo
	 *            - {@link EntidadeConteudoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarEntidadeConteudo")
	public String pesquisarEntidadeConteudo(EntidadeConteudoImpl entidadeConteudo, BindingResult result,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado, HttpServletRequest request,
			Model model) throws GGASException {

		Map<String, Object> filtro = montarFiltro(entidadeConteudo, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(ENTIDADE_CONTEUDO, entidadeConteudo);
		model.addAttribute("listaEntidadeConteudo",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, EntidadeConteudoImpl.class.getName()));
		carregarEntidadeClasse(model);

		return "exibirPesquisaEntidadeConteudo";
	}

	/**
	 * Método responsável pela exibição da tela relacionada ao cadastro do Entidade
	 * Conteudo
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirInclusaoEntidadeConteudo")
	public String exibirInclusaoEntidadeConteudo(Model model) throws NegocioException {

		carregarEntidadeClasse(model);

		return "exibirInclusaoEntidadeConteudo";
	}

	/**
	 * Método responsável pela inclusão de uma entidade do tipo Entidade Conteudo
	 * 
	 * @param entidadeConteudo
	 *            - {@link EntidadeConteudoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("incluirEntidadeConteudo")
	public String incluirEntidadeConteudo(EntidadeConteudoImpl entidadeConteudo, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "forward:exibirInclusaoEntidadeConteudo";

		model.addAttribute(ENTIDADE_CONTEUDO, entidadeConteudo);

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(entidadeConteudo, TITULO);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(entidadeConteudo);
			controladorTabelaAuxiliar.inserir(entidadeConteudo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					EntidadeConteudoImpl.ENTIDADE_CONTEUDO_ROTULO);
			view = pesquisarEntidadeConteudo(entidadeConteudo, result, entidadeConteudo.isHabilitado(), request, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela relacionada a alteração de dados do
	 * Entidade Conteúdo
	 * 
	 * @param entidadeConteudoTmp
	 *            - {@link EntidadeConteudoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirAlteracaoEntidadeConteudo")
	public String exibirAlteracaoEntidadeConteudo(EntidadeConteudoImpl entidadeConteudoTmp, BindingResult result,
			HttpServletRequest request, Model model) throws NegocioException {

		EntidadeConteudoImpl entidadeConteudo = entidadeConteudoTmp;
		carregarEntidadeClasse(model);
		if (!isPostBack(request)) {
			try {
				entidadeConteudo = (EntidadeConteudoImpl) controladorTabelaAuxiliar
						.obter(entidadeConteudo.getChavePrimaria(), EntidadeConteudoImpl.class);
				model.addAttribute(ENTIDADE_CONTEUDO, entidadeConteudo);
				saveToken(request);
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		return "exibirAlteracaoEntidadeConteudo";
	}

	/**
	 * Método relacionado a alteração de dados do Entidade Conteúdo
	 * 
	 * @param entidadeConteudo
	 *            - {@link EntidadeConteudoImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarEntidadeConteudo")
	public String alterarEntidadeConteudo(EntidadeConteudoImpl entidadeConteudo, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "forward:exibirAlteracaoEntidadeConteudo";

		model.addAttribute(ENTIDADE_CONTEUDO, entidadeConteudo);

		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(entidadeConteudo);
			controladorTabelaAuxiliar.atualizar(entidadeConteudo, EntidadeConteudoImpl.class);
			controladorEntidadeConteudo.updateCache(entidadeConteudo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
					EntidadeConteudoImpl.ENTIDADE_CONTEUDO_ROTULO);
			view = pesquisarEntidadeConteudo(entidadeConteudo, result, entidadeConteudo.isHabilitado(), request, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela relacionada ao detalhamento de um
	 * Entidade Conteúdo
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirDetalharEntidadeConteudo")
	public String exibirDetalharEntidadeConteudo(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws NegocioException {

		try {
			EntidadeConteudoImpl entidadeConteudo = (EntidadeConteudoImpl) controladorTabelaAuxiliar
					.obter(chavePrimaria, EntidadeConteudoImpl.class, "entidadeClasse");
			model.addAttribute(ENTIDADE_CONTEUDO, entidadeConteudo);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirDetalharEntidadeConteudo";
	}

	/**
	 * Método responsável pela remoção do registro de uma Entidade Conteúdo
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerEntidadeConteudo")
	public String removerEntidadeConteudo(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "forward:exibirPesquisaEntidadeConteudo";
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, EntidadeConteudoImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
					EntidadeConteudoImpl.ENTIDADE_CONTEUDO_ROTULO);
			view = pesquisarEntidadeConteudo(null, null, true, request, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return view;
	}

	/**
	 * 
	 * @param entidadeConteudo
	 * @param habilitado
	 * @return
	 */
	private Map<String, Object> montarFiltro(EntidadeConteudoImpl entidadeConteudo, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (entidadeConteudo != null) {
			if (!StringUtils.isEmpty(entidadeConteudo.getDescricao())) {
				filtro.put(DESCRICAO, entidadeConteudo.getDescricao());
			}

			if (!StringUtils.isEmpty(entidadeConteudo.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", entidadeConteudo.getDescricaoAbreviada());
			}

			if (entidadeConteudo.getEntidadeClasse() != null) {
				filtro.put("idEntidadeClasse", entidadeConteudo.getEntidadeClasse().getChavePrimaria());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

	/**
	 * Método responsável por carregar uma lista de Entidade de classe
	 * 
	 * @param model
	 *            - {@link Model}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	private void carregarEntidadeClasse(Model model) throws NegocioException {
		Collection<EntidadeClasse> listaEntidadeClasse = controladorEntidadeConteudo.listarEntidadeClasse();
		model.addAttribute("listaEntidadeClasse", listaEntidadeClasse);
	}
}
