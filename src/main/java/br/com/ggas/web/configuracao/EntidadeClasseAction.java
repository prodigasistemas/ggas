/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.configuracao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeClasseImpl;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a Entidade Classe.
 */
@Controller
public class EntidadeClasseAction extends GenericAction {

	private static final String EXIBIR_PESQUISA_ENTIDADE_CLASSE = "exibirPesquisaEntidadeClasse";

	private static final String ENTIDADE_CLASSE = "entidadeClasse";

	private static final String EXIBIR_ATUALIZAR_ENTIDADE_CLASSE = "exibirAtualizarEntidadeClasse";

	private static final String EXIBIR_INSERIR_ENTIDADE_CLASSE = "exibirInserirEntidadeClasse";

	private static final Class<EntidadeClasseImpl> CLASSE = EntidadeClasseImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.geral.impl.EntidadeClasseImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";
	
	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_ENTIDADE_CLASSE = "listaEntidadeClasse";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de Entidade Classe
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISA_ENTIDADE_CLASSE)
	public String exibirPesquisaEntidadeClasse(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_ENTIDADE_CLASSE;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de Entidade Classe.
	 * 
	 * @param entidadeClasse - {@link EntidadeClasseImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarEntidadeClasse")
	public String pesquisarEntidadeClasse(EntidadeClasseImpl entidadeClasse, BindingResult bindingResult,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(entidadeClasse, habilitado);

		Collection<TabelaAuxiliar> listaEntidadeClasse = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_ENTIDADE_CLASSE, listaEntidadeClasse);
		model.addAttribute(ENTIDADE_CLASSE, entidadeClasse);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_ENTIDADE_CLASSE;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Entidade Classe.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoEntidadeClasse")
	public String exibirDetalhamentoEntidadeClasse(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			EntidadeClasse entidadeClasse = (EntidadeClasse) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(ENTIDADE_CLASSE, entidadeClasse);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaEntidadeClasse";
		}

		return "exibirDetalhamentoEntidadeClasse";
	}

	/**
	 * Método responsável por exibir a tela de inserir Entidade Classe.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_ENTIDADE_CLASSE)
	public String exibirInserirEntidadeClasse() {

		return EXIBIR_INSERIR_ENTIDADE_CLASSE;
	}

	/**
	 * Método responsável por inserir uma Entidade Classe.
	 * 
	 * @param entidadeClasse - {@link EntidadeClasseImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirEntidadeClasse")
	private String inserirEntidadeClasse(EntidadeClasseImpl entidadeClasse, BindingResult bindingResult, Model model,
					HttpServletRequest request) throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(entidadeClasse);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(entidadeClasse, "Entidade Classe");
			controladorTabelaAuxiliar.inserir(entidadeClasse);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, EntidadeClasse.ENTIDADE_CLASSE);
			retorno = pesquisarEntidadeClasse(entidadeClasse, bindingResult, entidadeClasse.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(ENTIDADE_CLASSE, entidadeClasse);
			retorno = EXIBIR_INSERIR_ENTIDADE_CLASSE;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar Entidade Classe.
	 * 
	 * @param entidadeClasse - {@link EntidadeClasseImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param entidadeClasseParam - {@link EntidadeClasseImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_ENTIDADE_CLASSE)
	public String exibirAtualizarEntidadeClasse(EntidadeClasseImpl entidadeClasseParam, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		EntidadeClasse entidadeClasse = entidadeClasseParam;

		try {
			entidadeClasse = (EntidadeClasse) controladorTabelaAuxiliar.obter(entidadeClasse.getChavePrimaria(), CLASSE);
			model.addAttribute(ENTIDADE_CLASSE, entidadeClasse);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaEntidadeClasse";
		}

		return EXIBIR_ATUALIZAR_ENTIDADE_CLASSE;
	}

	/**
	 * Método responsável por atualizar uma Entidade Classe.
	 * 
	 * @param entidadeClasse - {@link EntidadeClasseImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarEntidadeClasse")
	private String atualizarEntidadeClasse(EntidadeClasseImpl entidadeClasse, BindingResult result, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(entidadeClasse);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(entidadeClasse, "Entidade Classe");
			controladorTabelaAuxiliar.atualizar(entidadeClasse, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, EntidadeClasse.ENTIDADE_CLASSE);
			retorno = pesquisarEntidadeClasse(entidadeClasse, result, entidadeClasse.isHabilitado(), model);
		} catch (NegocioException e) {
			model.addAttribute(ENTIDADE_CLASSE, entidadeClasse);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_ENTIDADE_CLASSE;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover uma Entidade Classe.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerEntidadeClasse")
	public String removerEntidadeClasse(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, EntidadeClasse.ENTIDADE_CLASSE);
			return pesquisarEntidadeClasse(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarEntidadeClasse";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Entidade Classe.
	 * 
	 * @param entidadeClasse - {@link EntidadeClasseImpl}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(EntidadeClasseImpl entidadeClasse, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (entidadeClasse != null) {

			if (entidadeClasse.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, entidadeClasse.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(entidadeClasse.getDescricao())) {
				filtro.put(DESCRICAO, entidadeClasse.getDescricao());
			}

			if (StringUtils.isNotEmpty(entidadeClasse.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, entidadeClasse.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
