package br.com.ggas.web.configuracao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelas telas relacionadas a entidade Unidade Classe
 *
 */
@Controller
public class UnidadeClasseAction extends GenericAction {

	private static final String UNIDADE_CLASSE = "unidadeClasse";
	private static final String TELA_EXIBIR_PESQUISA_UNIDADE_CLASSE = "exibirPesquisaUnidadeClasse";
	private static final String DESCRICAO = "descricao";
	private static final String HABILITADO = "habilitado";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável pela tela de pesquisa do tipo de segmento
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_UNIDADE_CLASSE)
	public String exibirPesquisaUnidadeClasse(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return TELA_EXIBIR_PESQUISA_UNIDADE_CLASSE;
	}

	/**
	 * Método responsável pela pesquisa dos tipos de segmento
	 * 
	 * @param unidadeClasse
	 *            - {@link ClasseUnidadeImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarUnidadeClasse")
	public String pesquisarUnidadeClasse(ClasseUnidadeImpl unidadeClasse, BindingResult result,
			@RequestParam("habilitado") Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = popularFiltro(unidadeClasse, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(UNIDADE_CLASSE, unidadeClasse);
		model.addAttribute("listaUnidadeClasse",
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, ClasseUnidadeImpl.class.getName()));

		return TELA_EXIBIR_PESQUISA_UNIDADE_CLASSE;
	}

	/**
	 * Método responsável pela exibição da tela de inclusão dos tipos de segmento
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirInclusaoUnidadeClasse")
	public String exibirInclusaoUnidadeClasse(Model model) {

		return "exibirInclusaoUnidadeClasse";
	}

	/**
	 * Método responsável pela inserção dos tipos de segmento
	 * 
	 * @param unidadeClasse
	 *            - {@link ClasseUnidadeImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("inserirUnidadeClasse")
	public String inserirUnidadeClasse(ClasseUnidadeImpl unidadeClasse, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "exibirInclusaoUnidadeClasse";

		model.addAttribute(UNIDADE_CLASSE, unidadeClasse);

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeClasse);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(unidadeClasse.getDescricao(),
					ClasseUnidadeImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(unidadeClasse.getDescricaoAbreviada(),
					ClasseUnidadeImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidadeClasse, ClasseUnidadeImpl.CLASSE_UNIDADE);
			controladorTabelaAuxiliar.inserir(unidadeClasse);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, ClasseUnidadeImpl.CLASSE_UNIDADE);
			view = pesquisarUnidadeClasse(unidadeClasse, result, unidadeClasse.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração dos tipos de segmento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoUnidadeClasse")
	public String exibirAlteracaoUnidadeClasse(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {
		String view = "exibirAlteracaoUnidadeClasse";
		ClasseUnidadeImpl unidadeClasse;
		try {
			unidadeClasse = (ClasseUnidadeImpl) controladorTabelaAuxiliar.obter(chavePrimaria, ClasseUnidadeImpl.class);
			model.addAttribute(UNIDADE_CLASSE, unidadeClasse);
		} catch (GGASException e) {
			model.addAttribute(HABILITADO, true);
			mensagemErroParametrizado(model, request, e);
			view = "exibirPesquisaUnidadeClasse";
		}

		return view;
	}

	/**
	 * Método responsável pela alteração dos tipos de segmento
	 * 
	 * @param unidadeClasse
	 *            - {@link ClasseUnidadeImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("alterarUnidadeClasse")
	public String alterarUnidadeClasse(ClasseUnidadeImpl unidadeClasse, BindingResult result,
			HttpServletRequest request, Model model) {
		String view = "exibirAlteracaoUnidadeClasse";

		model.addAttribute(UNIDADE_CLASSE, unidadeClasse);

		try {
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(unidadeClasse);
			controladorTabelaAuxiliar.verificarTamanhoDescricao(unidadeClasse.getDescricao(),
					ClasseUnidadeImpl.class.getName());
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(unidadeClasse.getDescricaoAbreviada(),
					ClasseUnidadeImpl.class.getName());
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(unidadeClasse, ClasseUnidadeImpl.CLASSE_UNIDADE);
			controladorTabelaAuxiliar.atualizar(unidadeClasse);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, ClasseUnidadeImpl.CLASSE_UNIDADE);
			view = pesquisarUnidadeClasse(unidadeClasse, result, unidadeClasse.isHabilitado(), model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de detalhamento dos tipos de
	 * segmento
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirDetalharUnidadeClasse")
	public String exibirDetalharUnidadeClasse(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) {
		String view = "exibirDetalharUnidadeClasse";

		try {
			model.addAttribute(UNIDADE_CLASSE, controladorTabelaAuxiliar.obter(chavePrimaria, ClasseUnidadeImpl.class));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "forward:exibirPesquisaUnidadeClasse";
		}

		return view;
	}

	/**
	 * Método responsável pela remoção dos tipos de segmento
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param unidadeClasse
	 *            - {@link ClasseUnidadeImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerUnidadeClasse")
	public String removerUnidadeClasse(ClasseUnidadeImpl unidadeClasse, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(UNIDADE_CLASSE, unidadeClasse);
		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, ClasseUnidadeImpl.class,
					getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, ClasseUnidadeImpl.CLASSE_UNIDADE);
			return pesquisarUnidadeClasse(null, null, true, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return pesquisarUnidadeClasse(unidadeClasse, result, true, model);
	}

	/**
	 * Método responsável por popular o filtro pra pesquisa dos tipos de segmento
	 * 
	 * @param unidadeClasse
	 *            - {@link ClasseUnidadeImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @return Map - {@link Map}
	 */
	private Map<String, Object> popularFiltro(ClasseUnidadeImpl unidadeClasse, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (unidadeClasse != null) {

			if (unidadeClasse.getChavePrimaria() > 0) {
				filtro.put("chavePrimaria", unidadeClasse.getChavePrimaria());
			}

			if (!StringUtils.isEmpty(unidadeClasse.getDescricao())) {
				filtro.put(DESCRICAO, unidadeClasse.getDescricao());
			}

			if (!StringUtils.isEmpty(unidadeClasse.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", unidadeClasse.getDescricaoAbreviada());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
