package br.com.ggas.web.configuracao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.feriado.Feriado;
import br.com.ggas.geral.feriado.impl.FeriadoVO;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelo cadastro e consulta de feriados.
 * 
 * @author orube
 *
 */
@Controller
public class FeriadoAction extends GenericAction {
	private static final String LABEL_FERIADO = "Feriado";
	private static final String ROTA_INCLUIR_FERIADO = "incluirFeriado";
	private static final String LISTA_TABELA_AUX = "listaTabelaAux";
	private static final String CLASS_FERIADO = "FeriadoImpl";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	private ControladorFeriado controladorFeriado;

	/**
	 * Método responsável pela pesquisa de feriados.
	 * 
	 * @param request   - {@link HttpServletRequest}
	 * @param model     - {@link Model}
	 * @param feriadoVO - {@link FeriadoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarFeriado")
	public String pesquisarFeriado(HttpServletRequest request, Model model, FeriadoVO feriadoVO) {
		try {
			HttpSession sessao = request.getSession(false);

			this.carregarDados(model, feriadoVO);

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = controladorTabelaAuxiliar
					.pesquisarTabelaAuxiliar(this.prepararFiltro(request, feriadoVO), CLASS_FERIADO);

			popularListaDataFeriado(sessao, listaTabelaAuxiliar);

			request.setAttribute("listaTabelaAuxiliar", listaTabelaAuxiliar);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("feriadoVO", feriadoVO);

		return "exibirPesquisaFeriado";
	}

	/**
	 * Método responsável pela replicação de feriados móveis.
	 * 
	 * @param model     - {@link Model}
	 * @param request   - {@link HttpServletRequest}
	 * @param feriadoVO - {@link FeriadoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("inserirFeriadoReplicado")
	public String inserirFeriadoReplicado(Model model, HttpServletRequest request, FeriadoVO feriadoVO) {

		try {
			HttpSession sessao = request.getSession(Boolean.FALSE);

			@SuppressWarnings("unchecked")
			Collection<TabelaAuxiliar> listaTabelaAuxiliar = (Collection<TabelaAuxiliar>) sessao
					.getAttribute(LISTA_TABELA_AUX);

			controladorTabelaAuxiliar.inserirFeriadoReplicado(listaTabelaAuxiliar);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, LABEL_FERIADO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.pesquisarFeriado(request, model, feriadoVO);

	}

	/**
	 * Método responsável pela inclusão de um feriado.
	 * 
	 * @param model     - {@link Model}
	 * @param request   - {@link HttpServletRequest}
	 * @param feriadoVO - {@link FeriadoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping(ROTA_INCLUIR_FERIADO)
	public String incluirFeriado(Model model, HttpServletRequest request, FeriadoVO feriadoVO) {

		try {
			Feriado feriado = popularFeriado(feriadoVO);

			String datasFeriadoSelecionado = feriadoVO.getDatasFeriado();

			String[] listaDatasFeriadoSelecionados = null;
			String mensagemErro = null;

			if (!StringUtils.isEmpty(datasFeriadoSelecionado)) {
				listaDatasFeriadoSelecionados = datasFeriadoSelecionado.split(";");
				mensagemErro = controladorTabelaAuxiliar.inserirFeriado(listaDatasFeriadoSelecionados, feriado);
			} else {
				mensagemErro = ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_FERIADO_DATA;
			}

			if (mensagemErro != null) {
				super.mensagemErro(model, new NegocioException(mensagemErro, Boolean.TRUE));
			} else {
				super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, LABEL_FERIADO);
			}
			
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.pesquisarFeriado(request, model, feriadoVO);

	}

	private void popularListaDataFeriado(HttpSession sessao, Collection<TabelaAuxiliar> listaTabelaAuxiliar) {
		String datasFeriado = "";
		String dia = "";
		String mes = "";
		String dataCompleta = "";

		Collection<TabelaAuxiliar> listaTabelaAux = new ArrayList<TabelaAuxiliar>();
		StringBuilder dataFeriadoAux = new StringBuilder();
		int count = 0;

		for (TabelaAuxiliar tabelaAux : listaTabelaAuxiliar) {

			datasFeriado = DataUtil.converterDataParaString(tabelaAux.getData());

			if (count == listaTabelaAuxiliar.size() - 1) {

				dataFeriadoAux.append(datasFeriado);
			} else {

				dataFeriadoAux.append(datasFeriado);
				dataFeriadoAux.append(";");
			}

			dia = datasFeriado.substring(0, 2);
			mes = datasFeriado.substring(3, 5);

			dataCompleta = mes + "/" + dia + datasFeriado.substring(5);

			tabelaAux.setDataFormatada(dataCompleta);
			listaTabelaAux.add(tabelaAux);
			count++;

		}

		sessao.setAttribute(LISTA_TABELA_AUX, listaTabelaAux);
		sessao.setAttribute("dataFeriado", dataFeriadoAux);
	}

	private Map<String, Object> prepararFiltro(HttpServletRequest request, FeriadoVO feriadoVO) {
		Map<String, Object> filtro = new HashMap<>();

		String descricao = feriadoVO.getDescricao();
		if (request.getRequestURI().endsWith(ROTA_INCLUIR_FERIADO)) {
			filtro.put(EntidadeConteudo.ATRIBUTO_DESCRICAO, null);
		} else if (descricao != null && !StringUtils.isEmpty(descricao)) {
			filtro.put(EntidadeConteudo.ATRIBUTO_DESCRICAO, descricao);
		}

		String habilitado = feriadoVO.getHabilitado();
		if (habilitado != null && !StringUtils.isEmpty(habilitado)) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado));
		}
		return filtro;
	}

	private void carregarDados(Model model, FeriadoVO feriadoVO) throws GGASException {
		limparFormulario(feriadoVO);

		Collection<Municipio> lista = controladorMunicipio.listarMunicipios();
		model.addAttribute("listaMunicipios", lista);

		ParametroSistema parametroQuantidadeAnosVigenciaFeriado = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO);

		String intervaloAnosData = Util.intervaloAnosData(parametroQuantidadeAnosVigenciaFeriado.getValor());

		model.addAttribute("intervaloAnosData", intervaloAnosData);
		Calendar calendar = Calendar.getInstance();

		model.addAttribute("anoAtual", calendar.get(Calendar.YEAR));
	}

	private void limparFormulario(FeriadoVO feriadoVO) {
		feriadoVO.setIdMunicipio(-1L);
		feriadoVO.setDescricao("");
		feriadoVO.setHabilitado("true");
	}

	private Feriado popularFeriado(FeriadoVO feriadoVO) throws GGASException {
		Feriado feriado = (Feriado) controladorFeriado.criar();
		String descricao = feriadoVO.getDescricao();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(descricao, CLASS_FERIADO);
			feriado.setDescricao(descricao);

		}

		String habilitado = feriadoVO.getHabilitado();
		if (habilitado != null) {
			feriado.setHabilitado(Boolean.valueOf(habilitado));
		}

		Long idMunicipio = feriadoVO.getIdMunicipio();
		if (idMunicipio != null && idMunicipio != -1) {

			Municipio municipio = (Municipio) controladorMunicipio.criar();
			municipio.setChavePrimaria(idMunicipio);

			feriado.setMunicipio(municipio);
		}

		String indicadorMunicipal = feriadoVO.getIndicadorMunicipal();
		if (indicadorMunicipal != null) {
			feriado.setIndicadorMunicipal(Boolean.valueOf(indicadorMunicipal));
		}

		String indicadorTipo = feriadoVO.getIndicadorTipo();
		if (indicadorTipo != null) {
			feriado.setIndicadorTipo(Boolean.valueOf(indicadorTipo));
		}

		return feriado;
	}
}
