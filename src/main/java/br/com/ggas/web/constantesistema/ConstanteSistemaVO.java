/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/09/2013 15:51:17
 @author wcosta
 */

package br.com.ggas.web.constantesistema;

import br.com.ggas.constantesistema.ConstanteSistema;

import java.io.Serializable;
import java.util.Collection;

/**
 * Classe utilizada para transferência de dados relacionados a constante
 *
 */
public class ConstanteSistemaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -404986443056081500L;

	private ConstanteSistema constanteSistema;



	private Collection<? extends Serializable> listaPreValores;
	private Collection<? extends Serializable> listaValores;


	private String valor;
	private String valorPre;

	public ConstanteSistema getConstanteSistema() {

		return constanteSistema;
	}

	public void setConstanteSistema(ConstanteSistema constanteSistema) {

		this.constanteSistema = constanteSistema;
	}


	public void setListaPreValores(Collection<? extends Serializable> listaValores) {

		this.listaPreValores = listaValores;
	}

	public Collection<? extends Serializable> getListaPreValores() {

		return listaPreValores;
	}

	public Collection<? extends Serializable> getListaValores() {

		return listaValores;
	}

	public void setListaValores(Collection<? extends Serializable> listaValores) {

		this.listaValores = listaValores;
	}

	public String getValor() {

		return valor;
	}

	public void setValor(String valor) {

		this.valor = valor;
	}

	public String getValorPre() {

		return valorPre;
	}

	public void setValorPre(String valorPre) {

		this.valorPre = valorPre;
	}

}
