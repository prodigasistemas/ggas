package br.com.ggas.web.constantesistema;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * classe responsável por ações relacionadas as Constantes do sistema
 *
 */
@Controller
public class ConstanteSistemaAction extends GenericAction {

	private static final String ID_TABELA = "tabela";
	private static final String CHAVE_CONSTANTE = "chaveConstante";
	private static final String VALOR_CONSTANTE = "valorConstante";

	@Autowired
	private ControladorAuditoria controladorAuditoria;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	/**
	 * Metodo responsável por exibir as constantes do sistema.
	 * @param request  - {@link HttpServletRequest}
	 * @param model - model do spring
	 * @param idTabela id da tabela
	 * @return retorna a informação da constante do sistema
	 * @throws NegocioException exceção caso ocorra erro ao carregar dados
	 */
	@RequestMapping("exibirConstanteSistema")
	public String exibirConstanteSistema(HttpServletRequest request, Model model,
			@RequestParam(value = ID_TABELA, required = false) Long idTabela) throws NegocioException {

		this.carregarDados(model);

		return "exibirConstanteSistema";

	}

	/**
	 * Carregar dados.
	 *
	 * @param model
	 * @throws NegocioException the negocio exception
	 */
	public void carregarDados(Model model) throws NegocioException {

		List<Tabela> listaOrdenada = Util.ordenarColecaoPorAtributo(controladorAuditoria.listarTabelasConstantes(), "descricao", true);
		model.addAttribute("listaTabelas", listaOrdenada);
	}

	/**
	 * Método responsável por carregar as constantes do sistema que serão exibidas.
	 *
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param idTabela - {@link Long}
	 * @return view {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("carregarConstantes")
	public String carregarConstantes(Model model, HttpServletRequest request,
			@RequestParam(value = ID_TABELA, required = false) Long idTabela) throws GGASException {

		String view = "forward:/exibirConstanteSistema";
		try {
			view = this.buscarConstantes(model, request, idTabela);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;

	}

	private String buscarConstantes(Model model, HttpServletRequest request, Long idTabela) throws GGASException {

		List<ConstanteSistema> listaConstantes;
		Collection<ConstanteSistemaVO> listaConstantesVO = new ArrayList<ConstanteSistemaVO>();

		if (idTabela != null && idTabela > 0) {

			listaConstantes = controladorConstanteSistema.listarConstantesSistemaPorTabela(idTabela);

			if (listaConstantes == null || listaConstantes.isEmpty()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_CADASTRADA, true);
			}

			Tabela tabela = controladorAuditoria.obterTabela(idTabela);
			model.addAttribute("tabelaSelecionada", tabela.getDescricao());

			List<Object> listaConteudoTabelaColuna = controladorIntegracao.listarConteudoTabelaColuna(tabela, null);

			if (!listaConteudoTabelaColuna.isEmpty()) {

				@SuppressWarnings("rawtypes")
				Class classe = Util.primeiroElemento(listaConteudoTabelaColuna).getClass();
				@SuppressWarnings("rawtypes")
				Class superClasse = classe.getSuperclass();

				boolean achou = false;
				if ("TabelaAuxiliarImpl".equals(superClasse.getSimpleName())) {
					achou = true;
				} else {
					try {
						for (java.lang.reflect.Method m : Class.forName(
								Util.primeiroElemento(listaConteudoTabelaColuna).getClass().getName().toString()).getDeclaredMethods()) {
							if (m.toString().indexOf("getDescricao") > 0) {
								achou = true;
								break;
							}
						}
					} catch (ClassNotFoundException e) {
						throw new NegocioException(e);
					}
				}
				if (!achou) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_TABELA_CONSTANTE_NAO_EXISTE, true);
				}
			}

			model.addAttribute("listaValoresConstanteSistema", listaConteudoTabelaColuna);

			if (!listaConstantes.isEmpty()) {

				Collection<? extends Serializable> tiposChamados = Collections.emptyList();
				if (tabela.getNome().equals(Constantes.CHAMADO_ASSUNTO)) {
					tiposChamados = controladorChamadoTipo.listarTodosHabilitado();
				}
				for (ConstanteSistema constanteSistema : listaConstantes) {
					ConstanteSistemaVO constanteSistemaVO = criarConstanteSistemaVO(tabela.getNome().equals(Constantes.CHAMADO_ASSUNTO),
							constanteSistema, listaConteudoTabelaColuna, tiposChamados);
					listaConstantesVO.add(constanteSistemaVO);
				}

			}

			model.addAttribute("listaValoresConstanteSistemaVO", listaConstantesVO);

		} else {

			listaConstantes = controladorConstanteSistema.listarConstantesSistemaSemTabela();
			model.addAttribute("tabelaSelecionada", "Constantes do Sistema");
			model.addAttribute("listaValoresConstanteSistema", null);

		}

		if (!listaConstantesVO.isEmpty()) {
			model.addAttribute("listaConstanteSistema", null);
		} else {
			List<ConstanteSistema> listaOrdenada = Util.ordenarColecaoPorAtributo(listaConstantes, "descricao", true);
			model.addAttribute("listaConstanteSistema", listaOrdenada);
		}

		this.carregarDados(model);

		return exibirConstanteSistema(request, model, idTabela);
	}

	/**
	 * Cria um objeto ConstanteSistemaVO. Se a constante possuir uma EntidadeClasse atrelada, então seus valores possíveis serão as
	 * EntidadeConteudo pertencentes a classe. Senão, os valores possíveis de atribuição serão definidos por listaConteudoTabelaColuna.
	 *
	 * @param constanteSistema
	 * @return ConstanteSistemaVO
	 * @throws GGASException the GGAS exception
	 */
	private ConstanteSistemaVO criarConstanteSistemaVO(boolean isChamadoAssunto, ConstanteSistema constanteSistema,
			List<Object> listaConteudoTabelaColuna, Collection<? extends Serializable> tiposChamados)
			throws GGASException {
		ConstanteSistemaVO constanteSistemaVO = new ConstanteSistemaVO();
		constanteSistemaVO.setConstanteSistema(constanteSistema);

		Collection<? extends Object> valoresPrePossiveisConstante = Collections.emptyList();
		Collection<? extends Object> valoresPossiveisConstante;
		Long classeConstanteSistema = constanteSistema.getClasse();
		if (classeConstanteSistema == null) {

			if (isChamadoAssunto) {
				try {
					ChamadoAssunto chamadoAssunto =
							(ChamadoAssunto) controladorChamadoAssunto.obter(Long.valueOf(constanteSistema.getValor()));
					final ChamadoTipo chamadoTipo = chamadoAssunto.getChamadoTipo();

					constanteSistemaVO.setValorPre(String.valueOf(chamadoTipo.getChavePrimaria()));
					valoresPossiveisConstante = controladorChamadoAssunto.consultarChamado(chamadoTipo.getChavePrimaria());
					valoresPrePossiveisConstante = tiposChamados;

				} catch (Exception e) {
					valoresPossiveisConstante = listaConteudoTabelaColuna;
				}
			} else {
				valoresPossiveisConstante = listaConteudoTabelaColuna;
			}
		} else {
			valoresPossiveisConstante = controladorEntidadeConteudo.listarEntidadeConteudo(classeConstanteSistema);
		}

		constanteSistemaVO.setListaValores((Collection<? extends Serializable>) valoresPossiveisConstante);
		constanteSistemaVO.setListaPreValores((Collection<? extends Serializable>) valoresPrePossiveisConstante);
		return constanteSistemaVO;
	}

	/**
	 * Meétodo responsável por salvar as Constantes do Sistema.
	 *
	 * @param model - {@link Model}
	 * @param valorConstante - {@link String[]}
	 * @param chaveConstante - {@link Long[]}
	 * @param request - {@link HttpServletRequest}
	 * @param idTabela - {@link Long}
	 * @return view {@link String}
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("salvarConstanteSistema")
	public String salvarConstanteSistema(Model model, @RequestParam(value = VALOR_CONSTANTE, required = false) String[] valorConstante,
			@RequestParam(value = CHAVE_CONSTANTE, required = false) Long[] chaveConstante, HttpServletRequest request,
			@RequestParam(value = ID_TABELA, required = false) Long idTabela) throws GGASException {

		for (int i = 0; i < chaveConstante.length; i++) {

			if (chaveConstante[i] > 0) {

				ConstanteSistema constanteSistema = (ConstanteSistema) controladorConstanteSistema.obter(chaveConstante[i]);

				if (constanteSistema.getValor() == null) {
					constanteSistema.setValor(valorConstante[i]);
					controladorConstanteSistema.atualizar(constanteSistema);
				} else if (!constanteSistema.getValor().equals(valorConstante[i])) {
					constanteSistema.setValor(valorConstante[i]);
					controladorConstanteSistema.atualizar(constanteSistema);
				}

			}

		}

		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADES_ALTERADAS, super.obterMensagem(ConstanteSistema.CONSTANTES));

		this.carregarDados(model);

		return exibirConstanteSistema(request, model, idTabela);

	}

}
