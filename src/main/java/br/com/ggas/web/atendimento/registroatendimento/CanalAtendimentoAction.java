/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */


package br.com.ggas.web.atendimento.registroatendimento;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações referentes ao Canal de Atendimento.
 */
@Controller
public class CanalAtendimentoAction extends GenericAction {

	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final int NUMERO_MAXIMO_DESCRICAO = 20;

	private static final String CANAL_ATENDIMENTO = "canalAtendimento";

	private static final String EXIBIR_ATUALIZAR_CANAL_ATENDIMENTO = "exibirAtualizarCanalAtendimento";

	private static final String EXIBIR_INSERIR_CANAL_ATENDIMENTO = "exibirInserirCanalAtendimento";

	private static final Class<CanalAtendimentoImpl> CLASSE = CanalAtendimentoImpl.class;

	private static final String CLASSE_STRING = "br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String LISTA_CANAL_ATENDIMENTO = "listaCanalAtendimento";

	private static final String HABILITADO = "habilitado";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa em Canal de Atendimento .
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaCanalAtendimento")
	public String exibirPesquisaCanalAtendimento(Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return "exibirPesquisaCanalAtendimento";
	}

	/**
	 * Método responsável por pesquisar um Canal de Atendimento.
	 * 
	 * @param canalAtendimento - {@link CanalAtendimentoImpl}
	 * @param result - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("pesquisarCanalAtendimento")
	public String pesquisarCanalAtendimento(CanalAtendimentoImpl canalAtendimento, BindingResult result, Model model,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = this.prepararFiltro(canalAtendimento, habilitado);

		Collection<TabelaAuxiliar> listaCanalAtendimento = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_CANAL_ATENDIMENTO, listaCanalAtendimento);
		model.addAttribute(CANAL_ATENDIMENTO, canalAtendimento);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaCanalAtendimento(model);
	}

	/**
	 * Método responsável por exibir o detalhamento de um Canal de atendimento.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 */

	@RequestMapping("exibirDetalhamentoCanalAtendimento")
	public String exibirDetalhamentoCanalAtendimento(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			CanalAtendimento canalAtendimento =
					(CanalAtendimento) controladorTabelaAuxiliar.obter(chavePrimaria, CanalAtendimentoImpl.class);
			model.addAttribute(CANAL_ATENDIMENTO, canalAtendimento);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCanalAtendimento";
		}

		return "exibirDetalhamentoCanalAtendimento";
	}

	/**
	 * Método responsável por exibir a tela de inserir Canal de Atendimento.
	 * 
	 * @return view - {@link String}
	 */
	@RequestMapping(EXIBIR_INSERIR_CANAL_ATENDIMENTO)
	public String exibirInserirCanalAtendimento() {

		return EXIBIR_INSERIR_CANAL_ATENDIMENTO;
	}

	/**
	 * Método responsável por inserir um Canal de Atendimento.
	 * 
	 * @param canalAtendimento - {@link CanalAtendimentoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("inserirCanalAtendimento")
	private String inserirCanalAtendimento(CanalAtendimentoImpl canalAtendimento, BindingResult result, Model model,
			HttpServletRequest request) throws GGASException {

		String retorno = null;

		try {
			if (canalAtendimento.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (canalAtendimento.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(canalAtendimento);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(canalAtendimento, "Canal Atendimento");
			controladorTabelaAuxiliar.inserir(canalAtendimento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(CanalAtendimento.CANAL_ATENDIMENTO));
			retorno = pesquisarCanalAtendimento(canalAtendimento, result, model, canalAtendimento.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CANAL_ATENDIMENTO, canalAtendimento);
			retorno = EXIBIR_INSERIR_CANAL_ATENDIMENTO;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar o Canal de Atendimento.
	 * 
	 * @param canalAtendimento - {@link CanalAtendimentoImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @param canalAtendimentoParam	 - {@link canalAtendimentoImpl}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_CANAL_ATENDIMENTO)
	public String exibirAtualizarCanalAtendimento(CanalAtendimentoImpl canalAtendimentoParam, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		CanalAtendimento canalAtendimento = canalAtendimentoParam;

		try {
			canalAtendimento = (CanalAtendimento) controladorTabelaAuxiliar.obter(canalAtendimento.getChavePrimaria(), CLASSE);
			model.addAttribute(CANAL_ATENDIMENTO, canalAtendimento);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaCanalAtendimento";
		}

		return EXIBIR_ATUALIZAR_CANAL_ATENDIMENTO;
	}

	/**
	 * Método responsável por atualizar um Canal de Atendimento.
	 * 
	 * @param canalAtendimento - {@link CanalAtendimentoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarCanalAtendimento")
	private String atualizarCanalAtendimento(CanalAtendimentoImpl canalAtendimento, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String retorno = null;

		try {

			if (canalAtendimento.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, NUMERO_MAXIMO_DESCRICAO);
			}
			if (canalAtendimento.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(canalAtendimento);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(canalAtendimento, "Canal de Atendimento");
			controladorTabelaAuxiliar.atualizar(canalAtendimento, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(CanalAtendimento.CANAL_ATENDIMENTO));
			retorno = pesquisarCanalAtendimento(canalAtendimento, result, model, canalAtendimento.isHabilitado());
		} catch (NegocioException e) {
			model.addAttribute(CANAL_ATENDIMENTO, canalAtendimento);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_CANAL_ATENDIMENTO;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover um Canal de Atendimento.
	 * 
	 * @param canalAtendimento {@link CanalAtendimentoImpl}
	 * @param result {@link BingingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerCanalAtendimento")
	public String removerCanalAtendimento(CanalAtendimentoImpl canalAtendimento, BindingResult result,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(CanalAtendimento.CANAL_ATENDIMENTO));

			return pesquisarCanalAtendimento(null, null, model, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarCanalAtendimento";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa do Canal de Atendimento.
	 * 
	 * @param canalAtendimento - {@link CanalAtendimendo}
	 * @param habilitado - {@link Boolean}
	 */
	private Map<String, Object> prepararFiltro(CanalAtendimento canalAtendimento, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (canalAtendimento != null) {
			if (canalAtendimento.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, canalAtendimento.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(canalAtendimento.getDescricao())) {
				filtro.put(DESCRICAO, canalAtendimento.getDescricao());
			}

			if (StringUtils.isNotEmpty(canalAtendimento.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, canalAtendimento.getDescricaoAbreviada());
			}

		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}
}
