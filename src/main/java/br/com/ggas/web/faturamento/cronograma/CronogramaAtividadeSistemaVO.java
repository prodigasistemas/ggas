/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
public class CronogramaAtividadeSistemaVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2143163486827928916L;

	private Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO;

	private CronogramaFaturamento cronogramaFaturamento;

	private Integer anoMes;

	private Integer ciclo;

	/**
	 * @return the listaAtividadeSistemaVO
	 */
	public Collection<CronogramaAtividadeFaturamentoVO> getListaCronogramaAtividadeFaturamentoVO() {

		return listaCronogramaAtividadeFaturamentoVO;
	}

	/**
	 * @param listaAtividadeSistemaVO
	 *            the listaAtividadeSistemaVO to
	 *            set
	 */
	public void setListaCronogramaAtividadeFaturamentoVO(Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO) {

		this.listaCronogramaAtividadeFaturamentoVO = listaCronogramaAtividadeFaturamentoVO;
	}

	/**
	 * @return the anoMes
	 */
	public Integer getAnoMes() {

		return anoMes;
	}

	/**
	 * @param anoMes
	 *            the anoMes to set
	 */
	public void setAnoMes(Integer anoMes) {

		this.anoMes = anoMes;
	}

	/**
	 * @return the ciclo
	 */
	public Integer getCiclo() {

		return ciclo;
	}

	/**
	 * @param ciclo
	 *            the ciclo to set
	 */
	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}

	/**
	 * @return the cronogramaFaturamento
	 */
	public CronogramaFaturamento getCronogramaFaturamento() {

		return cronogramaFaturamento;
	}

	/**
	 * @param cronogramaFaturamento
	 *            the cronogramaFaturamento to set
	 */
	public void setCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento) {

		this.cronogramaFaturamento = cronogramaFaturamento;
	}

	public String getMesAnoCicloFormatado() {

		String mesAnoCiclo = "";

		if(anoMes != 0 && String.valueOf(anoMes).length() == 6 && ciclo > 0) {
			mesAnoCiclo = String.valueOf(Util.converterAnoMesEmMesAno(anoMes.toString()));
			mesAnoCiclo = mesAnoCiclo.substring(0, 2) + "/" + mesAnoCiclo.substring(2, 6);
			mesAnoCiclo = mesAnoCiclo + " - " + ciclo;
		}

		return mesAnoCiclo;
	}

	public String getMesAnoFormatado() {

		String mesAno = "";

		if(anoMes != 0 && String.valueOf(anoMes).length() == 6 && ciclo > 0) {
			mesAno = String.valueOf(Util.converterAnoMesEmMesAno(anoMes.toString()));
			mesAno = mesAno.substring(0, 2) + "/" + mesAno.substring(2, 6);
		}

		return mesAno;
	}
}
