package br.com.ggas.web.faturamento.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.impl.IndiceFinanceiroFormVO;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelo manter Índice Financeiro.
 * @author esantana
 *
 */
@Controller
public class IndiceFinanceiroAction extends GenericAction {

	private static final int QTD_MESES_POR_ANO = 12;

	private static final int SEGUNDOS_DE_DATA_FIM_INDICE = 59;

	private static final int MINUTOS_DE_DATA_FIM_INDICE = 23;

	private static final String LISTA_ANOS = "listaAnos";

	private static final String LISTA_MESES = "listaMeses";

	private static final String DATA_FIM_INDICE = "dataFimIndice";

	private static final String DATA_INICIO_INDICE = "dataInicioIndice";

	private static final String ID_INDICE_FINANCEIRO = "idIndiceFinanceiro";

	private static final String LISTA_INDICE_FINANCEIRO = "listaIndiceFinanceiro";

	private static final String INICIO = "Início";

	@Autowired
	@Qualifier("controladorTarifa")
	private ControladorTarifa controladorTarifa;

	/**
	 * Método responsável por exibir a tela de manter índice financeiro.
	 * 
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param idIndiceFinanceiro - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirManutencaoIndiceFinanceiro")
	public String exibirManutencaoIndiceFinanceiro(IndiceFinanceiroFormVO indiceFinanceiroFormVO, BindingResult bindingResult,
			@RequestParam(value = ID_INDICE_FINANCEIRO, required = false) Long idIndiceFinanceiro, HttpServletRequest request,
			Model model) {

		try {
			this.carregarCamposIndiceFinanceiro(model, idIndiceFinanceiro);

			popularDadosIndice(model, idIndiceFinanceiro);

			if (!super.isPostBack(request)) {
				indiceFinanceiroFormVO.setAnoInicio(null);
				indiceFinanceiroFormVO.setMesInicio(null);
				indiceFinanceiroFormVO.setAnoFim(null);
				indiceFinanceiroFormVO.setMesFim(null);
			}

			model.addAttribute("indiceFinanceiroFormVO", indiceFinanceiroFormVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "exibirManutencaoIndiceFinanceiro";
	}

	/**
	 * Método responsável carregar e exibir a lista de índices financeiros.
	 * 
	 * @param idIndiceFinanceiro - {@link Long}
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirIndiceFinanceiro")
	public String exibirIndiceFinanceiro(@RequestParam(value = ID_INDICE_FINANCEIRO, required = false) Long idIndiceFinanceiro,
			IndiceFinanceiroFormVO indiceFinanceiroFormVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute("indiceFinanceiroFormVO", indiceFinanceiroFormVO);

		try {

			this.carregarCamposIndiceFinanceiro(model, idIndiceFinanceiro);
			this.popularDadosIndice(model, idIndiceFinanceiro);

			Map<String, Date> filtro = this.carregarFiltroDataIndiceFinanceiro(request, indiceFinanceiroFormVO);

			Collection<IndiceFinanceiroValorNominal> listaIndiceFinanceiro = controladorTarifa
					.gerarIndicesFinanceiros(filtro.get(DATA_INICIO_INDICE), filtro.get(DATA_FIM_INDICE), idIndiceFinanceiro);

			this.manterDadosAlteradosDeListaIndiceFinanceiro(request, indiceFinanceiroFormVO, listaIndiceFinanceiro);

			String[] meses = gerarListaMesAno(indiceFinanceiroFormVO);

			model.addAttribute("meses", meses);
			model.addAttribute("listaIndiceFinanceiroCarregado", listaIndiceFinanceiro);
		} catch (GGASException e) {
			this.mensagemErroParametrizado(model,request, e);
		}
		
		saveToken(request);
		
		return "exibirManutencaoIndiceFinanceiro";
	}
	
	/**
	 * Método responsável por atualizar os índices financeiros.
	 * 
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param idIndiceFinanceiro - {@link Long}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("atualizarIndiceFinanceiro")
	public String atualizarIndiceFinanceiro(IndiceFinanceiroFormVO indiceFinanceiroFormVO, BindingResult bindingResult,
			HttpServletRequest request, Model model, @RequestParam(ID_INDICE_FINANCEIRO) Long idIndiceFinanceiro) throws GGASException {

		try {
			validarToken(request);
			
			DadosAuditoria dadosAuditoria = super.getDadosAuditoria(request);

			this.atualizarIndiceFinanceiro(indiceFinanceiroFormVO, idIndiceFinanceiro, dadosAuditoria);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ATUALIZADA, super.obterMensagem(IndiceFinanceiro.INDICE_FINANCEIRO));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return this.exibirIndiceFinanceiro(idIndiceFinanceiro, indiceFinanceiroFormVO, bindingResult, request, model);
	}

	/**
	 * Método responsável por atualizar os índices financeiros.
	 * 
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @param idIndiceFinanceiro - {@link Long}
	 * @param dadosAuditoria - {@link DadosAuditoria}
	 * @throws GGASException - {@link GGASException}
	 */
	private void atualizarIndiceFinanceiro(IndiceFinanceiroFormVO indiceFinanceiroFormVO, Long idIndiceFinanceiro,
			DadosAuditoria dadosAuditoria) throws GGASException {

		String[] datasIndiceFinanceiro = indiceFinanceiroFormVO.getDataIndiceFinanceiro();
		String[] mesIndicesFinanceiro = indiceFinanceiroFormVO.getMesIndiceFinanceiro();
		String[] valoresNominais = indiceFinanceiroFormVO.getValorIndice();
		String[] indicadorUtilizado = indiceFinanceiroFormVO.getIndicadorUtilizado();

		IndiceFinanceiroValorNominal indiceFinanceiroValorNominal =
				this.criarIndiceFinanceiroValorNominal(idIndiceFinanceiro, dadosAuditoria);

		IndiceFinanceiro indiceFinanceiro = indiceFinanceiroValorNominal.getIndiceFinanceiro();

		if ((datasIndiceFinanceiro != null) && (valoresNominais != null)) {

			controladorTarifa.atualizarIndicesFinanceiros(datasIndiceFinanceiro, mesIndicesFinanceiro, valoresNominais,
					indiceFinanceiroValorNominal, indicadorUtilizado);

			if ((indiceFinanceiro != null) && (indiceFinanceiro.isIndicadorMensal())) {

				Date mesInicio = Util.converterCampoStringParaData(INICIO, mesIndicesFinanceiro[0], Constantes.FORMATO_DATA_BR);
				Date mesFim = Util.converterCampoStringParaData("Fim", mesIndicesFinanceiro[mesIndicesFinanceiro.length - 1],
						Constantes.FORMATO_DATA_BR);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(mesInicio);
				indiceFinanceiroFormVO.setMesInicio(calendar.get(Calendar.MONTH) + 1);
				indiceFinanceiroFormVO.setAnoInicio(calendar.get(Calendar.YEAR));
				calendar.setTime(mesFim);
				indiceFinanceiroFormVO.setMesFim(calendar.get(Calendar.MONTH) + 1);
				indiceFinanceiroFormVO.setAnoFim(calendar.get(Calendar.YEAR));

			} else if ((indiceFinanceiro != null) && (!indiceFinanceiro.isIndicadorMensal())) {
				indiceFinanceiroFormVO.setDataInicio(datasIndiceFinanceiro[0]);
				indiceFinanceiroFormVO.setDataFim(datasIndiceFinanceiro[datasIndiceFinanceiro.length - 1]);
			}
		}
	}

	/**
	 * Método responsável por criar e popular a entidade {@link IndiceFinanceiroValorNominal}.
	 * 
	 * @param idIndiceFinanceiro - {@link Long}
	 * @param dadosAuditoria - {@link DadosAuditoria}
	 * @return {@link IndiceFinanceiroValorNominal}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private IndiceFinanceiroValorNominal criarIndiceFinanceiroValorNominal(Long idIndiceFinanceiro, DadosAuditoria dadosAuditoria)
			throws NegocioException {

		IndiceFinanceiroValorNominal indiceFinanceiroValorNominal =
				(IndiceFinanceiroValorNominal) controladorTarifa.criarIndiceFinanceiroValorNominal();

		IndiceFinanceiro indice = null;
		if ((idIndiceFinanceiro != null) && (idIndiceFinanceiro > 0)) {
			indice = controladorTarifa.obterIndiceFinanceiro(idIndiceFinanceiro);
			indiceFinanceiroValorNominal.setIndiceFinanceiro(indice);
		}

		indiceFinanceiroValorNominal.setValorNominal(BigDecimal.ZERO);
		indiceFinanceiroValorNominal.setDataReferencia(Calendar.getInstance().getTime());
		indiceFinanceiroValorNominal.setDadosAuditoria(dadosAuditoria);

		return indiceFinanceiroValorNominal;
	}

	/**
	 * Método responsável por criar o filtro utilizando datas de início e fim para a consulta de Índice Financeiro.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @return filtro - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	private Map<String, Date> carregarFiltroDataIndiceFinanceiro(HttpServletRequest request, IndiceFinanceiroFormVO indiceFinanceiroFormVO)
			throws GGASException {

		Map<String, Date> filtro = new HashMap<>();

		Date dataInicioIndice = null;
		Date dataFimIndice = null;
		if (isPostBack(request)) {
			String[] dataIndiceFinanceiro = indiceFinanceiroFormVO.getDataIndiceFinanceiro();
			if ((dataIndiceFinanceiro != null) && (dataIndiceFinanceiro.length > 0)) {
				dataInicioIndice = Util.converterCampoStringParaData(INICIO, dataIndiceFinanceiro[0], Constantes.FORMATO_DATA_BR);
				dataFimIndice = Util.converterCampoStringParaData("Fim", dataIndiceFinanceiro[dataIndiceFinanceiro.length - 1],
						Constantes.FORMATO_DATA_BR);
			}
		} else {
			if (!StringUtils.isEmpty(indiceFinanceiroFormVO.getDataInicio())) {
				dataInicioIndice =
						Util.converterCampoStringParaData(INICIO, indiceFinanceiroFormVO.getDataInicio(), Constantes.FORMATO_DATA_BR);
			}
			if (!StringUtils.isEmpty(indiceFinanceiroFormVO.getDataFim())) {
				dataFimIndice = Util.converterCampoStringParaData("Fim", indiceFinanceiroFormVO.getDataFim(), Constantes.FORMATO_DATA_BR);
			}
		}

		Calendar calendar = Calendar.getInstance();
		if ((indiceFinanceiroFormVO.getMesInicio() != null && indiceFinanceiroFormVO.getMesInicio() > 0)
				&& (indiceFinanceiroFormVO.getAnoInicio() != null && indiceFinanceiroFormVO.getAnoInicio() > 0)) {
			calendar.set(indiceFinanceiroFormVO.getAnoInicio(), indiceFinanceiroFormVO.getMesInicio() - 1, 1, 0, 0, 0);
			dataInicioIndice = calendar.getTime();
		}

		if ((indiceFinanceiroFormVO.getMesFim() != null && indiceFinanceiroFormVO.getMesFim() > 0)
				&& (indiceFinanceiroFormVO.getAnoFim() != null && indiceFinanceiroFormVO.getAnoFim() > 0)) {
			calendar.set(indiceFinanceiroFormVO.getAnoFim(), indiceFinanceiroFormVO.getMesFim() - 1, 1, 
					MINUTOS_DE_DATA_FIM_INDICE, SEGUNDOS_DE_DATA_FIM_INDICE, SEGUNDOS_DE_DATA_FIM_INDICE);
			dataFimIndice = calendar.getTime();
		}

		filtro.put(DATA_INICIO_INDICE, dataInicioIndice);
		filtro.put(DATA_FIM_INDICE, dataFimIndice);

		return filtro;
	}

	/**
	 * Método responsável por manter os dados alterados em tela referentes a lista de índice financeiro.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @param listaIndiceFinanceiro - {@link Collection}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	private void manterDadosAlteradosDeListaIndiceFinanceiro(HttpServletRequest request, IndiceFinanceiroFormVO indiceFinanceiroFormVO,
			Collection<IndiceFinanceiroValorNominal> colecaoIndiceFinanceiro) throws FormatoInvalidoException {

		List<IndiceFinanceiroValorNominal> listaIndiceFinanceiro = new ArrayList<>(colecaoIndiceFinanceiro);

		String[] valores = indiceFinanceiroFormVO.getValorNominal();
		String[] indicadorUtilizado = indiceFinanceiroFormVO.getIndicadorUtilizado();
		if (isPostBack(request) && valores != null) {
			for (int i = 0; i < listaIndiceFinanceiro.size(); i++) {
				IndiceFinanceiroValorNominal indiceFinanceiroValor = listaIndiceFinanceiro.get(i);

				if (StringUtils.isEmpty(valores[i])) {
					indiceFinanceiroValor.setValorNominal(null);
				} else {
					BigDecimal valorNominal = Util.converterCampoStringParaValorBigDecimal("Valor Nominal", valores[i],
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
					indiceFinanceiroValor.setValorNominal(valorNominal);
				}
				if (StringUtils.isEmpty(indicadorUtilizado[i])) {
					indiceFinanceiroValor.setIndicadorUtilizado(Boolean.FALSE);
				} else {
					indiceFinanceiroValor.setIndicadorUtilizado(Boolean.valueOf(indicadorUtilizado[i]));
				}
			}
		}
	}

	/**
	 * Gerar lista mes ano.
	 * 
	 * @param indiceFinanceiroFormVO - {@link IndiceFinanceiroFormVO}
	 * @throws GGASException - {@link GGASException}
	 */
	private String[] gerarListaMesAno(IndiceFinanceiroFormVO indiceFinanceiroFormVO)  {

		Integer mesInicio = indiceFinanceiroFormVO.getMesInicio();
		Integer mesFim = indiceFinanceiroFormVO.getMesFim();
		Integer anoInicio = indiceFinanceiroFormVO.getAnoInicio();
		Integer anoFim = indiceFinanceiroFormVO.getAnoFim();
		String[] meses = null;

		if ((mesInicio != null) && (mesFim != null) && (anoInicio != null) && (anoFim != null)) {

			Calendar calendar = Calendar.getInstance();
			calendar.set(anoInicio, mesInicio - 1, 1, 0, 0, 0);
			Date dataInicio = calendar.getTime();

			calendar.set(anoFim, mesFim - 1, 1, MINUTOS_DE_DATA_FIM_INDICE, 
					SEGUNDOS_DE_DATA_FIM_INDICE, SEGUNDOS_DE_DATA_FIM_INDICE);
			Date dataFim = calendar.getTime();
			int qtdMeses = Util.intervaloMeses(dataInicio, dataFim) + 1;

			meses = this.popularListaMesesIndiceFinanceiro(qtdMeses, mesInicio);
		}

		return meses;
	}
	
	/**
	 * Método responsável por popular um array contendo a descrição dos meses do ano.
	 *  
	 * @param qtdMeses - {@link Integer}
	 * @param mesInicio - {@link Integer}
	 * @return array de meses do ano
	 */
	private String[] popularListaMesesIndiceFinanceiro(Integer qtdMeses, Integer mesInicial) {
		Integer mesInicio = mesInicial;
		Map<Integer, String> listaMeses = Mes.MESES_ANO;
		String[] meses = new String[qtdMeses];

		for (int i = 0; i < meses.length; i++) {
			if (mesInicio > QTD_MESES_POR_ANO) {
				int resto = mesInicio % QTD_MESES_POR_ANO;
				if (resto == 0) {
					meses[i] = listaMeses.get(QTD_MESES_POR_ANO);
				} else {
					meses[i] = listaMeses.get(resto);
				}
			} else {
				meses[i] = listaMeses.get(mesInicio);
			}
			mesInicio++;
		}

		return meses;
	}
	
	

	/**
	 * Carregar campos indice financeiro.
	 * 
	 * @param model - {@link Model}
	 * @param idIndiceFinanceiro - {@link Long}
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarCamposIndiceFinanceiro(Model model, Long idIndiceFinanceiro) throws GGASException {

		Collection<IndiceFinanceiro> listaIndiceFinanceiro = controladorTarifa.listaIndiceFinanceiro();

		if (idIndiceFinanceiro != null) {
			for (IndiceFinanceiro indice : listaIndiceFinanceiro) {
				if (indice != null && idIndiceFinanceiro == indice.getChavePrimaria()) {
					this.adicionarValorMinimoIndiceFinanceiro(model, indice);
					break;
				}
			}
		}

		model.addAttribute(LISTA_INDICE_FINANCEIRO, controladorTarifa.listaIndiceFinanceiro());
		model.addAttribute(LISTA_MESES, Mes.MESES_ANO);
		model.addAttribute(LISTA_ANOS, Util.listarAnos("10"));
	}
	
	/**
	 * Método responsável por adicionar o valor mínimo no modelo.
	 * 
	 * @param model - {@link Model}
	 * @param indice - {@link IndiceFinanceiro}
	 */
	private void adicionarValorMinimoIndiceFinanceiro(Model model, IndiceFinanceiro indice) {
		if (indice.isIndicadorPermiteNegativo()) {
			model.addAttribute("valorMinimo", "-999999999.9999");
		} else {
			model.addAttribute("valorMinimo", "0");
		}
	}

	/**
	 * Popular dados indice.
	 * 
	 * @param model - {@link Model}
	 * @param idIndiceFinanceiro - {@link Long}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularDadosIndice(Model model, Long idIndiceFinanceiro) throws GGASException {

		if ((idIndiceFinanceiro != null) && (idIndiceFinanceiro > 0)) {
			IndiceFinanceiro indiceFinanceiro = controladorTarifa.obterIndiceFinanceiro(idIndiceFinanceiro);

			if (indiceFinanceiro != null) {
				model.addAttribute("indicadorMensal", String.valueOf(indiceFinanceiro.isIndicadorMensal()));
				model.addAttribute("indicadorPercentual", String.valueOf(indiceFinanceiro.isIndicadorValorPercentual()));
				model.addAttribute("indicadorPermiteNegativo", String.valueOf(indiceFinanceiro.isIndicadorPermiteNegativo()));
				model.addAttribute("idIndiceFinanceiro", idIndiceFinanceiro);
			}
		}
	}
}
