/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

package br.com.ggas.web.faturamento.tributo;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.faturamento.tributo.impl.TributoAliquotaImpl;
import br.com.ggas.faturamento.tributo.impl.TributoImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bruno silva
 *         Classe controladora responsável por gerenciar os eventos e acionar as classes
 *         e seus respectivos metodos relacionados as regras de negócio e de modelo para realizar
 *         alterações nas informações das telas referentes a funcionalidade Tributo.
 */
@Controller
public class TributoAction extends GenericAction {
	
	private static final String VALOR_ALIQUOTA = "valorAliquota";

	private static final String VALOR_REDUCAO_BASE_CALCULO = "valorReducaoBaseCalculo";

	private static final String LISTA_ESFERA_PODER = "listaEsferaPoder";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String TRIBUTO_ALIQUOTA = "tributoAliquota";

	private static final String LISTA_TRIBUTO_ALIQUOTA = "listaTributoAliquota";

	private static final String INDICADOR_SERVICO = "indicadorServico";

	private static final String DATA_VIGENCIA = "Data Vigência";
	
	private static final String TIPO_APLICACAO_REDUCAO_BASE_CALCULO = "Tipo de Aplicação da Redução da Base de Cálculo";
	
	private static final String VALOR_REDUCAO_BASE_DE_CALCULO = "Valor Redução Base de Cálculo";

	private static final int NUMERO_MAXIMO_DESCRICAO = 50;
	
	private static final int NUMERO_MAXIMO_DESCRICAO_ABREVIADA = 5;

	private static final String EXIBIR_PESQUISA_TRIBUTO = "exibirPesquisaTributo";

	private static final String TRIBUTO = "tributo";

	private static final String EXIBIR_ATUALIZAR_TRIBUTO = "exibirAtualizarTributo";

	private static final String EXIBIR_INSERIR_TRIBUTO = "exibirInserirTributo";

	private static final Class<TributoImpl> CLASSE = TributoImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.faturamento.tributo.impl.TributoImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";
	
	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_TRIBUTO = "listaTributo";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired
	private ControladorTributo controladorTributo;
	
	@Autowired
	private ControladorMunicipio controladorMunicipio;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Método responsável por exibir a tela de pesquisa tributo.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return exibirPesquisaTributo {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_PESQUISA_TRIBUTO)
	public String exibirPesquisaTributo(Model model, HttpServletRequest request) throws GGASException {

		request.getSession().removeAttribute(LISTA_TRIBUTO_ALIQUOTA);

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		model.addAttribute(LISTA_ESFERA_PODER, controladorCliente.listarEsferaPoder());

		return EXIBIR_PESQUISA_TRIBUTO;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de tributo.
	 * 
	 * @param tributo - {@link TributoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param habilitado - {@link Boolean}
	 * @param indicadorServico - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarTributo")
	public String pesquisarTributo(TributoImpl tributo, BindingResult result, HttpServletRequest request,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado,
			@RequestParam(value = INDICADOR_SERVICO, required = false) Boolean indicadorServico, Model model)
			throws GGASException {

		Map<String, Object> filtro = prepararFiltro(tributo, indicadorServico, habilitado);

		Collection<TabelaAuxiliar> listaTributo = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro,
				CLASSE_STRING);

		model.addAttribute(LISTA_TRIBUTO, listaTributo);
		model.addAttribute(TRIBUTO, tributo);
		model.addAttribute(INDICADOR_SERVICO, indicadorServico);
		model.addAttribute(HABILITADO, habilitado);

		return exibirPesquisaTributo(model, request);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento tributo.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoTributo")
	public String exibirDetalhamentoTributo(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		String retorno = "forward:exibirPesquisaTributo";

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {
			TributoImpl tributo = (TributoImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE, "esferaPoder");

			filtro.put(TRIBUTO, tributo);

			Collection<TributoAliquota> listaTributoAliquota = controladorTributo.consultarTributosAliquota(filtro);

			for (TributoAliquota tributoAliquota : listaTributoAliquota) {
				ajustarValorAliquotaParaExibicao(tributoAliquota);
			}

			model.addAttribute(LISTA_TRIBUTO_ALIQUOTA, listaTributoAliquota);
			model.addAttribute(TRIBUTO, tributo);
			model.addAttribute(HABILITADO, habilitado);
			retorno = "exibirDetalhamentoTributo";
		} catch (NegocioException e) {
			mensagemErro(model, e);

		}

		return retorno;
	}
	
	/**
	 * Método responsável por exibir a tela de inserir tributo.
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException} 
	 */
	@RequestMapping(EXIBIR_INSERIR_TRIBUTO)
	public String exibirInserirTributo(Model model, HttpSession session) throws GGASException {

		model.addAttribute(LISTA_ESFERA_PODER, controladorCliente.listarEsferaPoder());
		model.addAttribute("listaMunicipios", controladorMunicipio.listarMunicipios());
		definirArrayValoresAliquota(session);

		return EXIBIR_INSERIR_TRIBUTO;
	}
	

	/**
	 * Método responsável por inserir tributo.
	 * 
	 * @param tributoParamTela
	 * @param result
	 * @param tributoAliquota
	 * @param results
	 * @param model
	 * @param request
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("inserirTributo")
	private String inserirTributo(TributoImpl tributoParamTela, BindingResult result,
			TributoAliquotaImpl tributoAliquota, BindingResult results, Model model, HttpServletRequest request)
			throws GGASException {

		getDadosAuditoria(request);

		String tela = "forward:exibirInserirTributo";
		Boolean indicadorTipo = Boolean.TRUE;

		String dataVigencia = request.getParameter(DATA_VIGENCIA);
		String aliquotaStr = request.getParameter(VALOR_ALIQUOTA);
		String valorBaseCalculoStr = request.getParameter(VALOR_REDUCAO_BASE_CALCULO);

		model.addAttribute(VALOR_ALIQUOTA, aliquotaStr);
		model.addAttribute(VALOR_REDUCAO_BASE_CALCULO, valorBaseCalculoStr);
		model.addAttribute(DATA_VIGENCIA, dataVigencia);

		try {

			if (tributoParamTela.getDescricao() != null
					&& tributoParamTela.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO);
			}

			if (tributoParamTela.getDescricaoAbreviada() != null
					&& tributoParamTela.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(
						ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			if (StringUtils.isEmpty(tributoParamTela.getDescricaoAbreviada())) {
				tributoParamTela.setDescricaoAbreviada(null);
			}

			if (tributoParamTela.isIndicadorProduto() && tributoParamTela.isIndicadorServico()) {
				indicadorTipo = null;
			} else if (tributoParamTela.isIndicadorProduto()) {
				indicadorTipo = false;
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tributoParamTela);
			controladorTabelaAuxiliar.pesquisarTributoTabelaAuxiliar(tributoParamTela);

			Collection<TabelaAuxiliar> listaTributoAliquota = (Collection<TabelaAuxiliar>) request.getSession()
					.getAttribute(LISTA_TRIBUTO_ALIQUOTA);

			if ((request.getSession().getAttribute(LISTA_TRIBUTO_ALIQUOTA) == null)
					|| (listaTributoAliquota.isEmpty())) {

				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_TRIBUTO, true);
			}

			Long idTributo = controladorTabelaAuxiliar.inserir(tributoParamTela);

			Tributo tributo = new TributoImpl();
			tributo.setChavePrimaria(idTributo);

			for (TabelaAuxiliar tributoAliquotaAux : listaTributoAliquota) {
				tributoAliquotaAux.setTributo(tributo);
				ajustarValorAliquotaParaPersistencia(tributoAliquotaAux);
				controladorTabelaAuxiliar.inserir(tributoAliquotaAux);
			}

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Tributo.TRIBUTO_CAMPO_STRING);
			tela = pesquisarTributo(tributoParamTela, results, request, tributoParamTela.isHabilitado(), indicadorTipo,
					model);
		} catch (NegocioException e) {
			model.addAttribute(TRIBUTO, tributoParamTela);
			model.addAttribute(TRIBUTO_ALIQUOTA, tributoAliquota);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}
	
	/**
	 * Método responsável por exibir a tela de atualizar tributo.
	 * 
	 * @param tributoImpl - {@link TributoImpl}
	 * @param result - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(EXIBIR_ATUALIZAR_TRIBUTO)
	public String exibirAtualizarTributo(TributoImpl tributoImpl, BindingResult result, HttpServletRequest request,
			@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		String tela = "forward:exibirPesquisaTributo";

		model.addAttribute(LISTA_ESFERA_PODER, controladorCliente.listarEsferaPoder());
		model.addAttribute("listaMunicipios", controladorMunicipio.listarMunicipios());
		model.addAttribute(HABILITADO, habilitado);
		definirArrayValoresAliquota(request.getSession());

		Collection<TributoAliquota> listaTributoAliquota = (Collection<TributoAliquota>) request.getSession()
				.getAttribute(LISTA_TRIBUTO_ALIQUOTA);

		if (!isPostBack(request)) {

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(TRIBUTO, tributoImpl);
			listaTributoAliquota = controladorTributo.consultarTributosAliquota(filtro);

			if (listaTributoAliquota != null) {
				for (TributoAliquota tributoAliquota : listaTributoAliquota) {
					ajustarValorAliquotaParaExibicao(tributoAliquota);
				}
			}

			TributoImpl tributo = tributoImpl;

			try {
				tributo = (TributoImpl) controladorTabelaAuxiliar.obter(tributo.getChavePrimaria(), CLASSE);
				model.addAttribute(TRIBUTO, tributo);
				tela = EXIBIR_ATUALIZAR_TRIBUTO;
			} catch (NegocioException e) {
				mensagemErro(model, e);
			}

		} else {
			tela = EXIBIR_ATUALIZAR_TRIBUTO;
		}

		request.getSession().setAttribute(LISTA_TRIBUTO_ALIQUOTA, listaTributoAliquota);

		return tela;
	}

	/**
	 * Método responsável por atualizar tributo.
	 * 
	 * @param tributo
	 * @param result
	 * @param tributoAliquota
	 * @param results
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("atualizarTributo")
	private String atualizarTributo(TributoImpl tributo, BindingResult result, TributoAliquotaImpl tributoAliquota,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		String tela = "forward:exibirAtualizarTributo";

		String dataVigencia = request.getParameter(DATA_VIGENCIA);
		String aliquotaStr = request.getParameter(VALOR_ALIQUOTA);
		String valorBaseCalculoStr = request.getParameter(VALOR_REDUCAO_BASE_CALCULO);

		model.addAttribute(VALOR_ALIQUOTA, aliquotaStr);
		model.addAttribute(VALOR_REDUCAO_BASE_CALCULO, valorBaseCalculoStr);
		model.addAttribute(DATA_VIGENCIA, dataVigencia);

		// Boolean habilitado = Boolean.valueOf(request.getParameter(HABILITADO));
		model.addAttribute(HABILITADO, Boolean.valueOf(request.getParameter(HABILITADO)));
		Boolean indicadorTipo = Boolean.TRUE;

		try {

			if (tributo.getDescricao() != null && tributo.getDescricao().length() > NUMERO_MAXIMO_DESCRICAO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO);
			}

			if (tributo.getDescricaoAbreviada() != null
					&& tributo.getDescricaoAbreviada().length() > NUMERO_MAXIMO_DESCRICAO_ABREVIADA) {
				throw new NegocioException(
						ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO,
						NUMERO_MAXIMO_DESCRICAO_ABREVIADA);
			}

			if (StringUtils.isEmpty(tributo.getDescricaoAbreviada())) {
				tributo.setDescricaoAbreviada(null);
			}

			if (tributo.isIndicadorProduto() && tributo.isIndicadorServico()) {
				indicadorTipo = null;
			} else if (tributo.isIndicadorProduto()) {
				indicadorTipo = false;
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tributo);
			controladorTabelaAuxiliar.pesquisarTributoTabelaAuxiliar(tributo);

			Collection<TabelaAuxiliar> listaTributoAliquota = (Collection<TabelaAuxiliar>) request.getSession()
					.getAttribute(LISTA_TRIBUTO_ALIQUOTA);

			if ((listaTributoAliquota == null) || (listaTributoAliquota.isEmpty())) {

				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_TRIBUTO, true);
			}

			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put(TRIBUTO, tributo);

			Collection<TributoAliquota> listaTributoAliquotaAux = controladorTributo.consultarTributosAliquota(filtro);

			Long[] chavesPrimarias = new Long[listaTributoAliquotaAux.size()];

			int count = 0;

			for (TributoAliquota tributoAliquotaAux : listaTributoAliquotaAux) {
				chavesPrimarias[count] = tributoAliquotaAux.getChavePrimaria();
				count++;
			}

			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, TributoAliquotaImpl.class,
					getDadosAuditoria(request));

			controladorTabelaAuxiliar.atualizar(tributo, tributo.getClass());

			for (TabelaAuxiliar tributoAliquotaAux : listaTributoAliquota) {
				tributoAliquotaAux.setTributo(tributo);
				ajustarValorAliquotaParaPersistencia(tributoAliquotaAux);
				controladorTabelaAuxiliar.inserir(tributoAliquotaAux);
			}

			controladorTabelaAuxiliar.atualizar(tributo);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Tributo.TRIBUTO_CAMPO_STRING);
			tela = pesquisarTributo(tributo, result, request, tributo.isHabilitado(), indicadorTipo, model);
		} catch (NegocioException e) {
			model.addAttribute(TRIBUTO, tributo);
			model.addAttribute(TRIBUTO_ALIQUOTA, tributoAliquota);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por remover o tributo.
	 * 
	 * @param chavesPrimarias - Array do tipo {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("removerTributo")
	public String removerTributo(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		String retorno = "forward:pesquisarTributo";

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, Tributo.TRIBUTO_CAMPO_STRING);
			retorno = pesquisarTributo(null, null, request, Boolean.TRUE, null, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return retorno;
	}
	
	/**
	 * Incluir tributo aliquota.
	 * 
	 * @param tributoAliquota - {@link TributoAliquotaImpl}
	 * @param result - {@link BindingResult}
	 * @param tributo - {@link TributoImpl}
	 * @param results - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param indexLista - {@link Integer}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirTributoAliquota")
	public String incluirTributoAliquota(TributoAliquotaImpl tributoAliquota, BindingResult result, TributoImpl tributo,
			BindingResult results, HttpServletRequest request,
			@RequestParam(value = "indexLista", required = false, defaultValue = "-1") Integer indexLista, Model model)
			throws GGASException {

		getDadosAuditoria(request);

		String tela = null;

		String dataVigencia = request.getParameter(DATA_VIGENCIA);
		String aliquotaStr = request.getParameter(VALOR_ALIQUOTA);
		String valorBaseCalculoStr = request.getParameter(VALOR_REDUCAO_BASE_CALCULO);

		model.addAttribute(VALOR_ALIQUOTA, aliquotaStr);
		model.addAttribute(VALOR_REDUCAO_BASE_CALCULO, valorBaseCalculoStr);
		model.addAttribute(DATA_VIGENCIA, dataVigencia);
		model.addAttribute(TRIBUTO, tributo);
		model.addAttribute(TRIBUTO_ALIQUOTA, tributoAliquota);

		Collection<TabelaAuxiliar> listaTributoAliquota = (Collection<TabelaAuxiliar>) request.getSession()
				.getAttribute(LISTA_TRIBUTO_ALIQUOTA);

		try {

			if (dataVigencia != null && !dataVigencia.isEmpty()) {
				tributoAliquota.setDataVigencia(
						Util.converterCampoStringParaData(DATA_VIGENCIA, dataVigencia, Constantes.FORMATO_DATA_BR));
			}

			if (!aliquotaStr.isEmpty()) {
				BigDecimal valorAliquota = NumeroUtil.converterCampoStringParaValorBigDecimal("Valor Alíquota",
						aliquotaStr);
				if (NumeroUtil.maiorQueZero(valorAliquota)) {
					tributoAliquota.setValorAliquota(valorAliquota);
				}
			}

			if (tributoAliquota.isIndicadorReducaoBaseCalculo()) {

				if (("-1").equals(request.getParameter("tipoAplicacaoReducaoBaseCalculo"))) {
					throw new FormatoInvalidoException(Constantes.ERRO_DADOS_INVALIDOS,
							TIPO_APLICACAO_REDUCAO_BASE_CALCULO);
				}

				BigDecimal valorReducaoBaseCalculo = NumeroUtil
						.converterCampoStringParaValorBigDecimal(VALOR_REDUCAO_BASE_DE_CALCULO, valorBaseCalculoStr);

				tributoAliquota.setValorReducaoBaseCalculo(valorReducaoBaseCalculo);
			}

			if (indexLista == null || indexLista < 0) {

				controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tributoAliquota);

				if ((listaTributoAliquota == null) || (listaTributoAliquota.isEmpty())) {
					listaTributoAliquota = new ArrayList<TabelaAuxiliar>();
					tributoAliquota.setHabilitado(true);
				} else {
					tributoAliquota.setHabilitado(false);
				}

				verificaDataVigenciaValida(listaTributoAliquota, tributoAliquota);

				listaTributoAliquota.add(tributoAliquota);

				model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
				model.addAttribute(TRIBUTO_ALIQUOTA, null);

			} else {
				alterarTributoAliquota(indexLista, tributoAliquota, listaTributoAliquota, model);
			}

		} catch (GGASException e) {

			if (indexLista > -1) {
				model.addAttribute("habilatarBotaoAlterar", true);
			}

			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			mensagemErroParametrizado(model, request, e);
		}

		request.getSession().setAttribute(LISTA_TRIBUTO_ALIQUOTA, listaTributoAliquota);

		if ("incluir".equals(request.getParameter("acao"))) {
			tela = exibirInserirTributo(model, request.getSession());
		} else {
			tela = exibirAtualizarTributo(tributo, results, request, tributo.isHabilitado(), model);
		}

		return tela;

	}
	
	/**
	 * Alterar tributo aliquota.
	 * 
	 * @param indexLista
	 * @param tributoAliquota
	 * @param listaTributoAliquota
	 * @param model
	 * @throws GGASException
	 */
	@SuppressWarnings("rawtypes")
	private void alterarTributoAliquota(Integer indexLista, TributoAliquota tributoAliquota,
			Collection<TabelaAuxiliar> listaTributoAliquota, Model model) throws GGASException {

		try {

			if (listaTributoAliquota != null && indexLista != null) {

				boolean alterado = false;

				alterado = alterarTributoAliquotaSelecionado(indexLista, tributoAliquota, listaTributoAliquota,
						alterado);

				if (alterado) {
					((List) listaTributoAliquota).remove(indexLista.intValue());
					listaTributoAliquota.add(tributoAliquota);
				}

				model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

			}

			model.addAttribute(TRIBUTO_ALIQUOTA, null);

		} catch (GGASException e) {
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, false);
			model.addAttribute("habilatarBotaoAlterar", true);
			mensagemErroParametrizado(model, e);
		}

	}

	/**
	 * Método responsável por percorrer lista e validar os dados preenchidos de um Tributo Aliquota de
	 * acordo com o id selecionado na tela
	 * 
	 * @param indexLista - {@link Integer}
	 * @param tributoAliquota - {@link TributoAliquota}
	 * @param listaTributoAliquota - {@link Collection}
	 * @param alterado - {@link Boolean}
	 * @return isAlterado - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public boolean alterarTributoAliquotaSelecionado(Integer indexLista, TributoAliquota tributoAliquota,
			Collection<TabelaAuxiliar> listaTributoAliquota, boolean alterado) throws NegocioException {
		for (int i = 0; i < listaTributoAliquota.size(); i++) {
			if (i == indexLista && tributoAliquota != null) {
				controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(tributoAliquota);
				alterado = true;
				break;
			}
		}
		return alterado;
	}
	
	/**
	 * Remover tributo aliquota.
	 * 
	 * @param tributo - {@link TributoImpl}
	 * @param result - {@link BindingResult}
	 * @param indexLista - {@link Integer}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("removerTributoAliquota")
	public String removerTributoAliquota(TributoImpl tributo, BindingResult result,
			@RequestParam(value = "indexLista", required = false) Integer indexLista, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute(TRIBUTO, tributo);

		String tela = null;

		Collection<TabelaAuxiliar> listaTributoAliquota = (Collection<TabelaAuxiliar>) request.getSession()
				.getAttribute(LISTA_TRIBUTO_ALIQUOTA);

		if (listaTributoAliquota != null && indexLista != null) {
			((List) listaTributoAliquota).remove(indexLista.intValue());
		}

		request.getSession().setAttribute(LISTA_TRIBUTO_ALIQUOTA, listaTributoAliquota);

		model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		if ("incluir".equals(request.getParameter("acao"))) {
			tela = exibirInserirTributo(model, request.getSession());
		} else {
			tela = exibirAtualizarTributo(tributo, result, request, tributo.isHabilitado(), model);
		}

		return tela;

	}
	
	/**
	 * Método responsável por ajustar valores aliquota para exibicao
	 * 
	 * @param tributoAliquota
	 * @throws NegocioException
	 */
	private void ajustarValorAliquotaParaExibicao(TabelaAuxiliar tributoAliquota) throws NegocioException {
		if (controladorConstanteSistema
				.isTipoAplicacaoTributoAliquotaPercentual(tributoAliquota.getTipoAplicacaoTributoAliquota())) {
			tributoAliquota.setValorAliquota(tributoAliquota.getValorPercentualAliquota());
		}
	}
	
	/**
	 * Método responsável por ajustar o valor do atributo aliquota antes de persistir
	 * 
	 * @param tributoAliquota
	 * @throws NegocioException
	 */
	private void ajustarValorAliquotaParaPersistencia(TabelaAuxiliar tributoAliquota) throws NegocioException {
		if (controladorConstanteSistema
				.isTipoAplicacaoTributoAliquotaPercentual(tributoAliquota.getTipoAplicacaoTributoAliquota())) {
			tributoAliquota.setValorAliquota(
					NumeroUtil.converterPercentualParaValorAbsoluto(tributoAliquota.getValorAliquota()));
		}
	}
	
	/**
	 * Método responsável por definir um array de valores aliquota
	 * 
	 * @param sessao
	 * @throws NegocioException
	 */
	private void definirArrayValoresAliquota(HttpSession sessao) throws NegocioException {

		Collection<EntidadeConteudo> listaTipoTributoAliquota = controladorEntidadeConteudo
				.obterListaTipoAplicacaoTributoAliquota();
		sessao.setAttribute("listaTipoTributoAliquota", listaTipoTributoAliquota);

		Collection<EntidadeConteudo> listaTipoReducaoBaseCalculo = controladorEntidadeConteudo
				.obterListaTipoAplicacaoReducaoBaseCalculo();
		sessao.setAttribute("listaTipoReducaoBaseCalculo", listaTipoReducaoBaseCalculo);
	}

	/**
	 * Método responsável por verificar se Data Vigência é válida
	 * 
	 * @param listaTributoAliquota
	 * @param tributoAliquota
	 * @throws NegocioException
	 */
	private void verificaDataVigenciaValida(Collection<TabelaAuxiliar> listaTributoAliquota,
			TributoAliquota tributoAliquota) throws NegocioException {

		for (TabelaAuxiliar tabelaAuxiliar : listaTributoAliquota) {
			if (tabelaAuxiliar.getDataVigencia().equals(tributoAliquota.getDataVigencia())) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_TRIBUTO_ALIQUOTA, true);
			}
		}

	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param tributo
	 * @param indicadorServico
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(Tributo tributo, Boolean indicadorServico, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (tributo != null) {

			if (tributo.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, tributo.getChavePrimaria());
			}

			if (tributo.getDescricao() != null && !tributo.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, tributo.getDescricao());
			}

			if (StringUtils.isNotEmpty(tributo.getDescricaoAbreviada())) {
				filtro.put(DESCRICAO_ABREVIADA, tributo.getDescricaoAbreviada());
			}

			if (tributo.getEsferaPoder() != null && tributo.getEsferaPoder().getChavePrimaria() > 0) {
				filtro.put("idEsferaPoder", tributo.getEsferaPoder().getChavePrimaria());
			}
		}

		if (indicadorServico != null) {
			filtro.put(INDICADOR_SERVICO, indicadorServico);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
