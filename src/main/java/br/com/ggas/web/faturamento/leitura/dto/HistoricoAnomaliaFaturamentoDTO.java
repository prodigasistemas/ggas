/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO de histórico anomalia faturamento
 * @author gilberto.silva@logiquesistemas.com.br
 */
public class HistoricoAnomaliaFaturamentoDTO {
	private Long historicoId;
	private Long imovelId;
	private Long anormalidadeId;
	private Long faturaId;
	private String historicoDescricao;
	private String imovel;
	private String pontoConsumo;
	private String segmento;
	private String anormalidade;
	private String anoMes;
	private Integer ciclo;
	private String dataEmissao;
	private String dataResolucao;
	private String analisada;
	private String usuario;
	private List<String> tarifas;

	/**
	 * Cria um objeto HistoricoAnomaliaFaturamentoDTO a partir de um HistoricoAnomaliaFaturamento
	 * @param historico o HistoricoAnomaliaFaturamento
	 * @return Um objeto HistoricoAnomaliaFaturamentoDTO referente ao HistoricoAnomaliaFaturamento
	 */
	public static HistoricoAnomaliaFaturamentoDTO build(HistoricoAnomaliaFaturamento historico) {
		HistoricoAnomaliaFaturamentoDTO historicoDTO = new HistoricoAnomaliaFaturamentoDTO();
		historicoDTO.setHistoricoId(historico.getChavePrimaria());
		historicoDTO.setHistoricoDescricao(historico.getDescricaoComplementar());
		historicoDTO.setImovelId(historico.getPontoConsumo().getImovel().getChavePrimaria());
		historicoDTO.setImovel(historico.getPontoConsumo().getImovel().getNome());
		historicoDTO.setPontoConsumo(historico.getPontoConsumo().getDescricao());
		historicoDTO.setSegmento(historico.getPontoConsumo().getSegmento().getDescricaoAbreviada());
		historicoDTO.setAnormalidadeId(historico.getFaturamentoAnormalidade().getChavePrimaria());
		historicoDTO.setAnormalidade(historico.getFaturamentoAnormalidade().getDescricao());
		historicoDTO.setAnoMes(historico.getAnoMesFaturamentoFormatado());
		historicoDTO.setCiclo(historico.getNumeroCiclo());
		if (historico.getFatura() != null) {
			historicoDTO.setFaturaId(historico.getFatura().getChavePrimaria());
			List<String> tarifas = new ArrayList<>();
			for (FaturaItem faturaItem : historico.getFatura().getListaFaturaItem()) {
				if (faturaItem.getTarifa() != null) {
					tarifas.add(faturaItem.getTarifa().getDescricaoAbreviada());
				}
			}
			historicoDTO.setTarifas(tarifas);
		}
		if (historico.getDataGeracao() != null) {
			String dataFormatada = Util.converterDataParaStringSemHora(historico.getDataGeracao(), Constantes.FORMATO_DATA_BR);
			historicoDTO.setDataEmissao(dataFormatada);
		}
		if (historico.getDataAnalise() != null) {
			String dataFormatada = Util.converterDataParaStringSemHora(historico.getDataAnalise(), Constantes.FORMATO_DATA_BR);
			historicoDTO.setDataResolucao(dataFormatada);
		}
		if (historico.isAnalisada()) {
			historicoDTO.setAnalisada("Sim");
		} else {
			historicoDTO.setAnalisada("Não");
		}
		if (historico.getAnalisador() != null && historico.getAnalisador().getFuncionario() != null) {
			historicoDTO.setUsuario(historico.getAnalisador().getFuncionario().getNome());
		}

		return historicoDTO;
	}

	public Long getHistoricoId() {
		return historicoId;
	}

	public void setHistoricoId(Long historicoId) {
		this.historicoId = historicoId;
	}

	public Long getImovelId() {
		return imovelId;
	}

	public void setImovelId(Long imovelId) {
		this.imovelId = imovelId;
	}

	public Long getAnormalidadeId() {
		return anormalidadeId;
	}

	public void setAnormalidadeId(Long anormalidadeId) {
		this.anormalidadeId = anormalidadeId;
	}

	public Long getFaturaId() {
		return faturaId;
	}

	public void setFaturaId(Long faturaId) {
		this.faturaId = faturaId;
	}

	public String getHistoricoDescricao() {
		return historicoDescricao;
	}

	public void setHistoricoDescricao(String historicoDescricao) {
		this.historicoDescricao = historicoDescricao;
	}

	public String getImovel() {
		return imovel;
	}

	public void setImovel(String imovel) {
		this.imovel = imovel;
	}

	public String getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getAnormalidade() {
		return anormalidade;
	}

	public void setAnormalidade(String anormalidade) {
		this.anormalidade = anormalidade;
	}

	public String getAnoMes() {
		return anoMes;
	}

	public void setAnoMes(String anoMes) {
		this.anoMes = anoMes;
	}

	public Integer getCiclo() {
		return ciclo;
	}

	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getDataResolucao() {
		return dataResolucao;
	}

	public void setDataResolucao(String dataResolucao) {
		this.dataResolucao = dataResolucao;
	}

	public String getAnalisada() {
		return analisada;
	}

	public void setAnalisada(String analisada) {
		this.analisada = analisada;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<String> getTarifas() {
		return tarifas;
	}

	public void setTarifas(List<String> tarifas) {
		this.tarifas = tarifas;
	}
}
