/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.fatura;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.ecarta.negocio.ControladorLoteECarta;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.DadosGeraisItensFatura;
import br.com.ggas.faturamento.fatura.DadosItemFaturamento;
import br.com.ggas.faturamento.fatura.DadosItensFatura;
import br.com.ggas.faturamento.fatura.DadosLeituraConsumoFatura;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.faturamento.fatura.impl.OrdenacaoDataEmissaoFatura;
import br.com.ggas.faturamento.fatura.vo.CreditoDebitoRefaturarVO;
import br.com.ggas.faturamento.impl.AnoMesReferenciaVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.contrato.ContratoAction;
import br.com.ggas.web.contrato.contrato.ContratoVO;

/**
 * Classe responsável por gerenciar as telas do gerenciamento de faturas.
 *
 */
@Controller
public class FaturaAction extends GenericAction {

	private static final int CENTENA = 100;

	private static final int INDICE_ANO_FIM = 7;

	private static final int INDICE_ANO_INICIO = 3;

	private static final int INDICE_MES_FIM = 2;

	private static final String ERROR = "ERROR";

	private static final String DESCRICAO_MOTIVO_INCLUSAO = "descricaoMotivoInclusao";

	private static final String DATA_EMISSAO = "dataEmissao";

	private static final String LISTA_DADOS_GERAIS = "listaDadosGerais";

	private static final String LISTA_DADOS_LEITURA_CONSUMO_FATURA = "listaDadosLeituraConsumoFatura";

	private static final String LISTA_FATURAS_PENDENTES = "listaFaturasPendentes";

	private static final String LISTA_MOTIVO_INCLUSAO = "listaMotivoInclusao";

	private static final String LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS = "listaIdsPontoConsumoRemovidosAgrupados";

	private static final String ENDERECOS = "enderecos";

	private static final String GERAR_RELATORIO_EMISSAO_FATURA = "gerarRelatorioEmissaoFatura";

	private static final String RELATORIO_EMISSAO_FATURA = "relatorioEmissaoFatura";

	private static final String ID_CONTRATO_ENCERRAR_RESCINDIR = "idContratoEncerrarRescindir";

	private static final String VOLUME_APURADO = "volumeApurado";

	private static final String IMOVEL = "imovel";

	private static final String COLECAO_ITENS_FATURAMENTO = "colecaoItensFaturamento";

	private static final String DADOS_RESUMO_FATURA = "dadosResumoFatura";

	private static final String DADOS_RESUMO_FATURA_AUXILIAR = "dadosResumoFaturaAux";

	private static final String DADOS_INDICADOR_RESUMO = "dadosIndicadorResumo";

	private static final String ANO_MES_CICLO_LISTA = "anoMesCicloLista";

	private static final String LISTA_HISTORICO_CONSUMO = "listaHistoricoConsumo";

	private static final String LISTA_MOTIVOS_REVISAO = "listaMotivosRevisao";

	private static final String ID_SITUACAO = "idSituacao";
	
	private static final String ID_SITUACAO_PAGAMENTO = "idSituacaoPagamento";

	private static final String LISTA_LEITURISTA = "listaLeiturista";

	private static final String LISTA_LEITURA_CONSUMO_FATURA = "listaLeituraConsumoFatura";

	private static final String ITENS_FATURA = "itensFatura";

	private static final String CHAVES_CREDITO_DEBITO = "chavesCreditoDebito";

	private static final String LISTA_CREDITO_DEBITO = "listaCreditoDebito";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String REFERENCIA = "referencia";

	private static final String CICLO = "ciclo";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String INDICADOR_REVISAO = "indicadorRevisao";

	private static final String INDICADOR_COMPLEMENTAR = "indicadorComplementar";

	private static final String ID_ROTA = "idRota";

	private static final String ID_GRUPO = "idGrupo";

	private static final String NUMERO_DOCUMENTO_FINAL = "numeroDocumentoFinal";

	private static final String NUMERO_DOCUMENTO_INICIAL = "numeroDocumentoInicial";

	private static final String NUMERO_NOTA_FISCAL_INICIAL = "numeroNotaFiscalInicial";

	private static final String NUMERO_NOTA_FISCAL_FINAL = "numeroNotaFiscalFinal";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String LISTA_SITUACAO = "listaSituacao";

	private static final String LISTA_GRUPOS_FATURAMENTO = "listaGruposFaturamento";

	private static final String INCIDEM_JUROS_MULTA = "incidemJurosMulta";

	private static final String DATA_VENCIMENTO = "dataVencimento";

	private static final String MOTIVO_ALTERACAO = "motivoAlteracao";

	private static final String LISTA_MOTIVO = "listaMotivo";

	private static final String LISTA_FATURA = "listaFatura";

	private static final String LISTA_FATURA_VO = "listaFaturaVO";

	private static final String ID_CLIENTE = "idCliente";

	public static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String LISTA_MOTIVO_CANCELAMENTO = "listaMotivoCancelamento";

	private static final String LISTA_MOTIVO_COMPLEMENTO = "listaMotivoComplemento";

	private static final String LISTA_FATURA_CANCELAR = "listaFaturaCancelar";

	private static final String LISTA_FATURA_SESSAO = "listaFaturaSessao";

	// -- valores da tela de revisão
	private static final String TELA_REVISAO_TITULO = "tituloTela";

	protected static final String FORWARD_SUCESSO_CONTRATO = "sucesso_contrato";

	private static final String RELATORIO_NOTA_DEBITO_CREDITO = "relatorioNotaDebitoCredito";

	private static final String GERAR_RELATORIO_NOTA_DEBITO_CREDITO = "gerarRelatorioNotaDebitoCredito";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String DATA_DEVOLUCAO = "dataDevolucao";

	private static final String LISTA_FUNCIONARIO = "listaFuncionario";

	private static final String INSTALACAO_MEDIDOR = "instalacaoMedidor";

	public static final String NOME_ARQUIVO_DANFE_IMPRESSAO = "relatorioDanfeCopergas";

	public static final String ASSUNTO_EMAIL_FATURA = "Fatura Online";

	public static final String NOME_ANEXO_EMAIL_FATURA = "Fatura Nº ";

	public static final String NOME_ANEXO_EMAIL_DANFE = "Danfe referente à fatura Nº ";

	public static final String ASSUNTO_EMAIL_DANFE = "Danfe Online";

	public static final String CORPO_MSG_EMAIL_DANFE = "Em anexo o DANFE referente à fatura Nº %d.";

	public static final String PARTICIPA_ECARTAS = "participaECartas";
	public static final String VLR_UNITARIO_ITEM = "vlrUnitarioItem";
	public static final String CONSUMO_ITEM = "consumoItem";
	public static final String EMISSAO_CLIENTE = "emissaoCliente";
	public static final String TIPO_FATURA = "tipoFatura";
	public static final String INDICADOR_PRODUCAO = "indicadorProducao";
	
	private boolean isConsolidado = false;

	private Long idHistoricoMedicao;

	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String EXIBIR_TARIFA = "exibirTarifa";

	private static final String SCALA_CONSUMO_APURADO = "escalaConsumoApurado";


	@Autowired
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorTransportadora controladorTransportadora;

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorLoteECarta controladorLoteECarta;

	/**
	 * Método responsável por exibir a tela pesquisa de Fatura.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaFatura")
	public String exibirPesquisaFatura(HttpServletRequest request, Model model) throws NegocioException {

		ParametroSistema participaECartas =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_PARTICIPA_CONVENIO_ECARTAS);

		ParametroSistema indicadorProducao = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.INDICADOR_AMBIENTE_PRODUCAO);
		
		model.addAttribute(PARTICIPA_ECARTAS, participaECartas.getValor());
		model.addAttribute(LISTA_GRUPOS_FATURAMENTO, controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute(LISTA_SITUACAO, controladorCreditoDebito.listarCreditoDebitoSituacao());
		model.addAttribute(INDICADOR_PRODUCAO, indicadorProducao.getValor());
		
		request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
		request.getSession().removeAttribute(COLECAO_ITENS_FATURAMENTO);
		request.getSession().removeAttribute(VOLUME_APURADO);
		model.addAttribute("listaCiclo", controladorCronogramaFaturamento.listarCiclosCronograma());
		model.addAttribute("listaSituacaoPagamento", controladorEntidadeConteudo.listarSituacaoPagamento());

		return "exibirPesquisaFatura";
	}


	/**
	 * Método responsável por pesquisar uma Fatura.
	 * 
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("pesquisarFatura")
	public String pesquisarFatura(FaturaPesquisaVO faturaPesquisaVO, HttpServletRequest request, Model model) throws NegocioException {


		try {
			faturaPesquisaVO.setChavesPrimarias(null);
			Map<String, Object> filtro = new HashMap<String, Object>();

			salvarPesquisaFiltro(faturaPesquisaVO, model);

			String codigoTipoDocumentoFatura =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
			faturaPesquisaVO.setTipoDocumento(codigoTipoDocumentoFatura);

			if (faturaPesquisaVO.getIsValidarFiltroPesquisaFatura() == null
					|| (faturaPesquisaVO.getIsValidarFiltroPesquisaFatura() != null
							&& faturaPesquisaVO.getIsValidarFiltroPesquisaFatura())) {
				this.prepararFiltro(filtro, faturaPesquisaVO, request);
				controladorFatura.validarFiltroPesquisaFatura(filtro);
			}

			if (verificaCampo(faturaPesquisaVO.getIdPontoConsumo())) {
				pesquisarPontosConsumo(faturaPesquisaVO, model);
			}
			super.adicionarFiltroPaginacao(request, filtro);

			// Não pesquisar as faturas caso o usuário
			// tenha pesquisado os pontos de consumo.
			if (faturaPesquisaVO.getIndicadorPesquisaPontos() == null || !faturaPesquisaVO.getIndicadorPesquisaPontos()) {

				filtro.put(ControladorFatura.ORDENACAO_NUMERO_FATURA_DESC, Boolean.TRUE);
				OrdenacaoDataEmissaoFatura.setOrdenacao(filtro, OrdenacaoDataEmissaoFatura.DESCENDENTE);
				filtro.remove("colecaoPaginada");

				Collection<Fatura> listaFatura = controladorFatura.consultarFaturaPesquisa(filtro);
				Collection<FaturaVO> listaFaturaVO = new ArrayList<FaturaVO>();

				for (Fatura fatura : listaFatura) {

					fatura.setMotivoRevisao(((Fatura) controladorCobranca.obterFatura(fatura.getChavePrimaria(), "motivoRevisao",
							"listaFaturaItem", "faturaGeral", "cliente", "creditoDebitoSituacao", "pontoConsumo")).getMotivoRevisao());

					if (fatura.getDataRevisao() != null) {
						DateTime dataRevisaoCalculada = new DateTime(fatura.getDataRevisao());
						dataRevisaoCalculada = dataRevisaoCalculada.plusDays(fatura.getMotivoRevisao().getPrazoMaximoDias());

						fatura.setDataRevisao(dataRevisaoCalculada.toDate());
					}

					Collection<DocumentoFiscal> documentosFiscais =
							controladorFatura.obterDocumentosFiscaisPorIdFatura(fatura.getChavePrimaria());
					FaturaVO faturaVO = null;
					Date dataInicioMedicao = null;
					Date dataFinalMedicao = null;

					if (fatura.getHistoricoMedicao() != null
							&& fatura.getHistoricoMedicao().getDataLeituraInformada() != null
							&& fatura.getHistoricoConsumo() != null
							&& fatura.getHistoricoConsumo().getDiasConsumo() != null) {
						dataFinalMedicao = fatura.getHistoricoMedicao().getDataLeituraInformada();
						Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
								.consultarHistoricoConsumoPorHistoricoConsumoSintetizador(fatura.getHistoricoConsumo());
						if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
							dataInicioMedicao = listaHistoricoConsumo.iterator().next().getHistoricoAtual()
									.getDataLeituraInformada();
						} else {
							dataInicioMedicao = DataUtil.decrementarDia(dataFinalMedicao,
									fatura.getHistoricoConsumo().getDiasConsumo() - 1);
						}

					} else {
						Collection<Fatura> colecaoFaturasFilhas = controladorFatura
								.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());

						if (!colecaoFaturasFilhas.isEmpty()) {
							Fatura faturaFilha = colecaoFaturasFilhas.iterator().next();
							dataFinalMedicao = faturaFilha.getHistoricoMedicao().getDataLeituraInformada();

							Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
									.consultarHistoricoConsumoPorHistoricoConsumoSintetizador(
											faturaFilha.getHistoricoConsumo());
							if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
								dataInicioMedicao = listaHistoricoConsumo.iterator().next().getHistoricoAtual()
										.getDataLeituraInformada();
							} else {
								dataInicioMedicao = DataUtil.decrementarDia(dataFinalMedicao,
										fatura.getHistoricoConsumo().getDiasConsumo() - 1);
							}
						}
					}

					if (documentosFiscais != null && !documentosFiscais.isEmpty()) {
						for (DocumentoFiscal documentoFiscal : documentosFiscais) {

							faturaVO = new FaturaVO();
							faturaVO.setFatura(fatura);
							faturaVO.setDocumentoFiscal(documentoFiscal);
							faturaVO.setDataInicioMedicao(dataInicioMedicao);
							faturaVO.setDataFinalMedicao(dataFinalMedicao);

							Long quantidadeFaturaRevisao =
									controladorFatura.obterQuantidadeFaturaHistoricoRevisaoPorFatura(fatura.getChavePrimaria());
							faturaVO.setQuantidadeRevisao(quantidadeFaturaRevisao);
							listaFaturaVO.add(faturaVO);

						}
					
					} else {

						faturaVO = new FaturaVO();
						faturaVO.setFatura(fatura);
						faturaVO.setDataInicioMedicao(dataInicioMedicao);
						faturaVO.setDataFinalMedicao(dataFinalMedicao);

						Long quantidadeFaturaRevisao =
								controladorFatura.obterQuantidadeFaturaHistoricoRevisaoPorFatura(fatura.getChavePrimaria());
						faturaVO.setQuantidadeRevisao(quantidadeFaturaRevisao);

						listaFaturaVO.add(faturaVO);

					}

				}

				model.addAttribute(LISTA_FATURA_VO, listaFaturaVO);
				request.getSession().setAttribute(LISTA_FATURA_SESSAO, listaFatura);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaFatura(request, model);
	}

	private void salvarPesquisaFiltro(FaturaPesquisaVO faturaPesquisaVO, Model model) throws NegocioException {
		model.addAttribute("faturaForm", faturaPesquisaVO);
		if (verificaCampo(faturaPesquisaVO.getIdCliente())) {
			Cliente cliente = controladorCliente.obterCliente(faturaPesquisaVO.getIdCliente(), ENDERECOS);
			model.addAttribute("cliente", cliente);
		} else {
			if (verificaCampo(faturaPesquisaVO.getIdImovel())) {
				ImovelImpl imovel = (ImovelImpl) controladorImovel.obter(faturaPesquisaVO.getIdImovel());
				model.addAttribute("imovel", imovel);
			}
		}
	}

	/**
	 * Exibir pesquisa.
	 * 
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisa")
	public String exibirPesquisa(FaturaPesquisaVO faturaPesquisaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String retorno = "exibirPontoConsumoContrato";
		salvarPesquisaFiltro(faturaPesquisaVO, model);
		if (faturaPesquisaVO.getIdContratoEncerrarRescindir() == null) {
			retorno = "forward:exibirPesquisaFatura";
		}

		return retorno;
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo de um cliente ou um imóvel na tela de pesquisa de fatura.
	 *
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarPontoConsumoFatura")
	public String pesquisarPontoConsumoFatura(FaturaPesquisaVO faturaPesquisaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			salvarPesquisaFiltro(faturaPesquisaVO, model);
			request.getSession().removeAttribute(DADOS_INDICADOR_RESUMO);
			pesquisarPontosConsumo(faturaPesquisaVO, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaFatura(request, model);
	}

	/**
	 * Pesquisar pontos consumo.
	 *
	 */
	private void pesquisarPontosConsumo(FaturaPesquisaVO faturaPesquisaVO, Model model) throws GGASException {

		// FIXME: Tratar preenchimento dos dados do cliente e imóvel, pois da forma que
		// está gera inconsistência com a pesquisa informada
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		if (verificaCampo(faturaPesquisaVO.getIdImovel())) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(faturaPesquisaVO.getIdImovel(), Boolean.TRUE);
		} else if (verificaCampo(faturaPesquisaVO.getIdCliente())) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorChaveCliente(faturaPesquisaVO.getIdCliente());
		}
		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}

	/**
	 * Método responsável por exibir a tela de detalhar Fatura.
	 *
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult }
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirDetalhamentoFatura")
	public String exibirDetalhamentoFatura(FaturaPesquisaVO faturaPesquisaVO, BindingResult result, HttpServletRequest request,
			Model model) {

		try {
			this.salvarPesquisaFiltro(faturaPesquisaVO, model);
			String scala = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO).getValor();

			DadosResumoFatura dadosResumoFatura;
			dadosResumoFatura = controladorFatura.obterDadosDetalhamentoFatura(faturaPesquisaVO.getChavePrimaria(),
					faturaPesquisaVO.getIdDocumentoFiscal());

			model.addAttribute(EXIBIR_TARIFA, true);
			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

			model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
			model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());
			model.addAttribute(LISTA_DADOS_GERAIS, dadosResumoFatura.getListaDadosGeraisItens());
			model.addAttribute(REFERENCIA, Util.formatarAnoMes(dadosResumoFatura.getReferencia()));
			model.addAttribute(CICLO, dadosResumoFatura.getCiclo());
			model.addAttribute(DATA_EMISSAO,
					Util.converterDataParaStringSemHora(dadosResumoFatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			model.addAttribute(DESCRICAO_MOTIVO_INCLUSAO, dadosResumoFatura.getMotivo());
			model.addAttribute(NUMERO_CONTRATO, dadosResumoFatura.getNumeroContrato());
			model.addAttribute(SCALA_CONSUMO_APURADO, scala);

			if (dadosResumoFatura.getStatusNfe() != null) {
				model.addAttribute("statusNfe", dadosResumoFatura.getStatusNfe().getDescricao());
			} else {
				model.addAttribute("statusNfe", null);
			}

			if (dadosResumoFatura.getNumeroSerieDocFiscal() != null) {
				model.addAttribute("numeroSerieDocFiscal", dadosResumoFatura.getNumeroSerieDocFiscal());
			} else {
				model.addAttribute("numeroSerieDocFiscal", null);
			}

		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return "exibirDetalhamentoFatura";
	}

	/**
	 * Método responsável por exibir uma tela de inclusão de Fatura.
	 * 
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoFatura")
	public String exibirInclusaoFatura(FaturaPesquisaVO faturaPesquisaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "exibirInclusaoFatura";
		try {
			salvarPesquisaFiltro(faturaPesquisaVO, model);
			carregarDados(faturaPesquisaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarFatura(faturaPesquisaVO, request, model);
		}
		return tela;
	}

	/**
	 * Método responsável por exibir uma tela de inclusão de Fatura em avulso.
	 * 
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoFaturaAvulso")
	public String exibirInclusaoFaturaAvulso(FaturaPesquisaVO faturaPesquisaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String tela = "exibirInclusaoFaturaAvulso";

		try {
			salvarPesquisaFiltro(faturaPesquisaVO, model);
			
			if(faturaPesquisaVO.getIdPontoConsumo() == null) {
				throw new NegocioException(Util.getMensagens().getString("ERRO_PONTO_CONSUMO_FATURA"), true);
			}
			
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(
					faturaPesquisaVO.getIdPontoConsumo(), INSTALACAO_MEDIDOR, IMOVEL, "imovel.quadraFace",
					"imovel.quadraFace.endereco", "imovel.quadraFace.endereco.cep");

			model.addAttribute("isAvulso", Boolean.TRUE);
			model.addAttribute("idImovel", faturaPesquisaVO.getIdImovel());
			model.addAttribute("idCliente", faturaPesquisaVO.getIdCliente());
			model.addAttribute("listaTransportadora", controladorTransportadora.listarTransportadoras());

			controladorPontoConsumo.validarFaturaAvulso(pontoConsumo);
			ContratoPontoConsumo contratoPontoConsumo =
					controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

			if (contratoPontoConsumo == null) {
				throw new NegocioException(Constantes.ERRO_PONTO_CONSUMO_NAO_POSSUI_CONTRATO, true);
			}

			if (contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento() == null
					|| contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().isEmpty()) {
				throw new NegocioException(Constantes.ERRO_CONTRATO_NAO_POSSUI_ITEM_FATURAMENTO, true);
			}

			ContratoPontoConsumoItemFaturamento contrato =
					contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().iterator().next();
			Tarifa tarifa = contrato.getTarifa();
			model.addAttribute("tarifaAplicada", tarifa.getDescricao());
			carregarDados(faturaPesquisaVO, request, model);

			String[] volumes = (String[]) request.getSession().getAttribute(VOLUME_APURADO);

			model.addAttribute(VOLUME_APURADO, volumes);

			request.getSession().setAttribute(VOLUME_APURADO, volumes);

			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

			for (DadosLeituraConsumoFatura dados : dadosResumoFatura.getListaDadosLeituraConsumoFatura()) {
				dados.setConsumoApurado(null);
				dados.setConsumoFaturado(null);
				dados.setDataLeituraAnterior(null);
				dados.setDataLeituraAtual(null);
				dados.setIdHistoricoMedicao(0);
				dados.setLeituraAnterior(null);
				dados.setLeituraAtual(null);
				dados.setVolumeAnteriorApurado(null);
			}
			model.addAttribute(ANO_MES_REFERENCIA, faturaPesquisaVO.getAnoMesReferencia());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarPontoConsumoFatura(faturaPesquisaVO, result, request, model);
		}

		return tela;
	}

	/**
	 * Incluir fatura avulso.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirFaturaAvulso")
	public String incluirFaturaAvulso(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {
		String tela = "exibirResumoInclusaoFaturaAvulso";
		try {
			popularIncluirFatura(faturaVO, request, model);

			isConsolidado = Boolean.FALSE;
			tela = pesquisarFatura(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por incluir uma Fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirFatura")
	public String incluirFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			popularIncluirFatura(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			return 	pesquisarPontoConsumoFatura(faturaVO, result, request, model);

		}

		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Popular incluir fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return FaturaPesquisaVO {@link FaturaPesquisaVO}
	 * @throws GGASException {@link GGASException}
	 */
	private FaturaPesquisaVO popularIncluirFatura(FaturaPesquisaVO faturaVO, HttpServletRequest request, Model model) throws GGASException {

		try {
			validarToken(request);

			Fatura fatura = (Fatura) controladorFatura.criar();
			fatura.setDadosAuditoria(getDadosAuditoria(request));
			fatura.setHabilitado(true);

			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			if (dadosResumoFatura != null && faturaVO != null) {

				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));

				if (faturaVO.getAnoMesFormatado() != null && !"".equals(faturaVO.getAnoMesFormatado())) {
					String mes = faturaVO.getAnoMesFormatado().substring(0, INDICE_MES_FIM);
					String ano = faturaVO.getAnoMesFormatado().substring(INDICE_ANO_INICIO, INDICE_ANO_FIM);
					String anoMes = ano + mes;
					dadosResumoFatura.setReferencia(Integer.valueOf(anoMes));
				}

				if (faturaVO.getNumeroCiclo() != null && !"".equals(faturaVO.getNumeroCiclo())) {
					dadosResumoFatura.setCiclo(Integer.parseInt(faturaVO.getNumeroCiclo()));
				}

				if (faturaVO.getIndicadorFaturasPendentes() != null && faturaVO.getIndicadorFaturasPendentes()
						&& faturaVO.getChavesFaturasPendentes().length > 0) {
					dadosResumoFatura.setChavesFaturasPendentes(faturaVO.getChavesFaturasPendentes());
				}

				dadosResumoFatura.setChavesFaturasPendentes(null);

			}

			controladorFatura.processarInclusaoFatura(dadosResumoFatura, Boolean.TRUE, Boolean.FALSE, this.getDadosAuditoria(request));

			if (faturaVO != null) {
				faturaVO.setIsValidarFiltroPesquisaFatura(Boolean.TRUE);
				model.addAttribute("fatura", faturaVO);
				salvarPesquisaFiltro(faturaVO, model);
			}
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Fatura.FATURA_ROTULO);
		} catch (NegocioException ex) {
			LOG.error("Erro ao incluir fatura", ex);
			mensagemErro(model, request, ex);
		}

		return faturaVO;
	}

	/**
	 * Gerar relatorio emissao fatura.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("gerarRelatorioEmissaoFatura")
	public String gerarRelatorioEmissaoFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		salvarPesquisaFiltro(faturaVO, model);
		byte[] retornoRelatorio = (byte[]) request.getSession().getAttribute(RELATORIO_EMISSAO_FATURA);

		if (retornoRelatorio != null) {
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(retornoRelatorio.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorio.pdf");
			servletOutputStream.write(retornoRelatorio, 0, retornoRelatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		request.removeAttribute(GERAR_RELATORIO_EMISSAO_FATURA);
		request.getSession().removeAttribute(RELATORIO_EMISSAO_FATURA);
		return null;
	}

	/**
	 * Classe privada reponsável por criar uma ordenação especial por ciclo e referência.
	 *
	 * @author capgemini
	 */
	private class OrdenacaoEspecialCicloReferencia implements OrdenacaoEspecial {

		private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

		private static final String NUMERO_CICLO = "numeroCiclo";

		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			criteria.addOrder(Order.asc(NUMERO_CICLO));
			criteria.addOrder(Order.asc(ANO_MES_REFERENCIA));

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {
			return null;
		}
	}

	/**
	 * Preparar filtro.
	 *
	 */
	private void prepararFiltro(Map<String, Object> filtro, FaturaPesquisaVO faturaPesquisaVO, HttpServletRequest request)
			throws GGASException {


		if (verificaCampo(faturaPesquisaVO.getTipoFatura())) {
			filtro.put(TIPO_FATURA, faturaPesquisaVO.getTipoFatura());
		}

		if (verificaCampo(faturaPesquisaVO.getIdCliente())) {
			filtro.put(ID_CLIENTE, faturaPesquisaVO.getIdCliente());
		}

		if (verificaCampo(faturaPesquisaVO.getIdPontoConsumo())) {
			Collection<PontoConsumo> listaChavesPontoConsumo = new ArrayList<PontoConsumo>();
			listaChavesPontoConsumo.add((PontoConsumo) controladorPontoConsumo.obter(faturaPesquisaVO.getIdPontoConsumo()));
			filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaChavesPontoConsumo));
		} else if (verificaCampo(faturaPesquisaVO.getIdImovel())) {
			Collection<PontoConsumo> listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(faturaPesquisaVO.getIdImovel());
			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
				filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
			}
		}
		if (verificaCampo(faturaPesquisaVO.getIdSituacao())) {
			filtro.put(ID_SITUACAO, faturaPesquisaVO.getIdSituacao());
		}
		
		if (verificaCampo(faturaPesquisaVO.getIdSituacaoPagamento())) {
			filtro.put(ID_SITUACAO_PAGAMENTO, faturaPesquisaVO.getIdSituacaoPagamento());
		}
		
		Date fimEmissao = null;
		if (!StringUtils.isEmpty(faturaPesquisaVO.getDataEmissaoInicial())) {
			Date inicioEmissao = Util.converterCampoStringParaData("Data Emissão Inicial", faturaPesquisaVO.getDataEmissaoInicial(),
					Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_EMISSAO_INICIAL, inicioEmissao);

			Date dataAtual = Calendar.getInstance().getTime();
			if (DataUtil.gerarDataHmsZerados(inicioEmissao).compareTo(DataUtil.gerarDataHmsZerados(dataAtual)) <= 0
					&& faturaPesquisaVO.getDataEmissaoFinal() == null || StringUtils.isEmpty(faturaPesquisaVO.getDataEmissaoFinal())) {

				fimEmissao = dataAtual;
				filtro.put(DATA_EMISSAO_FINAL, fimEmissao);
				request.setAttribute("dtFinal", Util.converterDataParaString(dataAtual));
			}
		}
		if ((fimEmissao == null) && (!StringUtils.isEmpty(faturaPesquisaVO.getDataEmissaoFinal()))) {
			filtro.put(DATA_EMISSAO_FINAL, Util.converterCampoStringParaData("Data Emissão Final", faturaPesquisaVO.getDataEmissaoFinal(),
					Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(faturaPesquisaVO.getNumeroDocumentoInicial())) {
			filtro.put(NUMERO_DOCUMENTO_INICIAL, Long.valueOf(faturaPesquisaVO.getNumeroDocumentoInicial()));
		}
		if (!StringUtils.isEmpty(faturaPesquisaVO.getNumeroDocumentoFinal())) {
			filtro.put(NUMERO_DOCUMENTO_FINAL, Long.valueOf(faturaPesquisaVO.getNumeroDocumentoFinal()));
		}

		if (!StringUtils.isEmpty(faturaPesquisaVO.getNumeroNotaFiscalInicial())) {
			filtro.put(NUMERO_NOTA_FISCAL_INICIAL, Long.valueOf(faturaPesquisaVO.getNumeroNotaFiscalInicial()));
		}
		if (!StringUtils.isEmpty(faturaPesquisaVO.getNumeroNotaFiscalFinal())) {
			filtro.put(NUMERO_NOTA_FISCAL_FINAL, Long.valueOf(faturaPesquisaVO.getNumeroNotaFiscalFinal()));
		}

		if (!StringUtils.isEmpty(faturaPesquisaVO.getCodigoCliente())) {
			filtro.put(ID_CLIENTE, Long.valueOf(faturaPesquisaVO.getCodigoCliente()));
		}
		if (verificaCampo(faturaPesquisaVO.getIdGrupo())) {
			filtro.put(ID_GRUPO, faturaPesquisaVO.getIdGrupo());
		}
		if (verificaCampo(faturaPesquisaVO.getIdRota())) {
			filtro.put(ID_ROTA, faturaPesquisaVO.getIdRota());
		}
		if (!StringUtils.isEmpty(faturaPesquisaVO.getTipoDocumento())) {
			filtro.put("idTipoDocumento", Long.valueOf(faturaPesquisaVO.getTipoDocumento()));
		}
		if (!StringUtils.isEmpty(faturaPesquisaVO.getIndicadorComplementar())) {
			filtro.put(INDICADOR_COMPLEMENTAR, Boolean.parseBoolean(faturaPesquisaVO.getIndicadorComplementar()));
		}
		if (verificaCampo(faturaPesquisaVO.getAnoMesReferencia()) && !"".equals(faturaPesquisaVO.getAnoMesReferencia())) {
			filtro.put(ANO_MES_REFERENCIA, Integer.parseInt(faturaPesquisaVO.getAnoMesReferencia().replace("/", "").replaceAll("_", "")));
		}
		if (faturaPesquisaVO.getNumeroCiclo() != null && !"-1".equals(faturaPesquisaVO.getNumeroCiclo().toString())
				&& !"".equals(faturaPesquisaVO.getNumeroCiclo())) {
			filtro.put(NUMERO_CICLO, Integer.parseInt(faturaPesquisaVO.getNumeroCiclo()));
		}
		if (faturaPesquisaVO.getIndicadorRevisao() != null) {
			filtro.put(INDICADOR_REVISAO, faturaPesquisaVO.getIndicadorRevisao());
		}
	}

	/**
	 * Carregar dados.
	 *
	 */
	@SuppressWarnings("unchecked")
	private void carregarDados(FaturaPesquisaVO faturaPesquisaVO, HttpServletRequest request, Model model) throws GGASException {

		Collection<Fatura> faturasPendentes = null;
		DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

		// Fluxo normal de inclusão de fatura
		// on-line.
		if (faturaPesquisaVO.getIdContratoEncerrarRescindir() == null) {
			dadosResumoFatura = inclusaoFluxoNormal(faturaPesquisaVO);
		} else {
			// Fluxo alternativo para inclusão de
			// fatura de encerramento de contrato.
			dadosResumoFatura = incluirEncerramentoContrato(faturaPesquisaVO, request, model);
		}

		if (faturaPesquisaVO.getIdMotivoInclusao() != null) {
			dadosResumoFatura.setIdMotivo(faturaPesquisaVO.getIdMotivoInclusao());
		}

		request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);

		// Caso a fatura for avulsa... deve retornar todos os créditos e débitos
		atualizarListaCreditoDebitoFaturaAvulsa(request, faturaPesquisaVO.getIdCliente(), faturaPesquisaVO.getIdPontoConsumo(),
				dadosResumoFatura);

		if (!(dadosResumoFatura == null)) {
			removerCreditoDebitoFaturas(dadosResumoFatura);
		}

		model.addAttribute(LISTA_LEITURISTA, controladorRota.listarLeituristas());

		if (dadosResumoFatura != null && dadosResumoFatura.getCiclo() != null) {
			faturaPesquisaVO.setNumeroCiclo(String.valueOf(dadosResumoFatura.getCiclo()));
		}

		// carrega o(s) Mês(ses)/Ano-Ciclo(s) que faltam ser faturados
		boolean isIncluirCiclo = false;

		if (dadosResumoFatura != null && dadosResumoFatura.getListaPontoConsumo() != null
				&& !dadosResumoFatura.getListaPontoConsumo().isEmpty()) {
			isIncluirCiclo = true;
		}
		List<AnoMesReferenciaVO> historicoMesAnoCiclo = obterHistoricoConsumoTodos(faturaPesquisaVO.getIdPontoConsumo(), isIncluirCiclo);

		// incluindo ano mês referência atual no histórico de ano mes ciclo
		// pois a lista está trazendo apenas os antigos é preciso adicionar o atual
		AnoMesReferenciaVO anoMesReferencia = new AnoMesReferenciaVO();

		incluirAnoMesReferenciaAtual(faturaPesquisaVO, dadosResumoFatura, isIncluirCiclo, anoMesReferencia);

		// se a lista não estiver com o ano mes atual inserir
		boolean anoMesAtualExisteEmHistorico = false;
		for (AnoMesReferenciaVO anoMesReferenciaCurrent : historicoMesAnoCiclo) {
			if (anoMesReferenciaCurrent.getAnoMesFormatado().equals(anoMesReferencia.getAnoMesFormatado())) {
				anoMesAtualExisteEmHistorico = true;
				break;
			}
		}

		if (!anoMesAtualExisteEmHistorico) {
			historicoMesAnoCiclo.add(anoMesReferencia);
		}

		model.addAttribute(ANO_MES_CICLO_LISTA, historicoMesAnoCiclo);

		if (dadosResumoFatura != null && dadosResumoFatura.getIdMotivo() > 0) {
			faturaPesquisaVO.setIdMotivoInclusao(dadosResumoFatura.getIdMotivo());
		}

		if (dadosResumoFatura != null) {
			faturaPesquisaVO.setMedicaoDiaria(dadosResumoFatura.isMedicaoDiaria());
		}

		Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRetorno = new ArrayList<CreditoDebitoRefaturarVO>();

		Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<CreditoDebitoARealizar>();

		if (dadosResumoFatura != null) {
			listaCreditoDebito = dadosResumoFatura.getListaCreditoDebitoARealizarTotal();
		}
		if (isPostBack(request)) {
			Long[] chavesCreditoDebito = faturaPesquisaVO.getChavesCreditoDebito();
			Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar =
					controladorCreditoDebito.consultarCreditosDebitosPorChaves(chavesCreditoDebito);
			if (listaCreditoDebitoARealizar != null) {
				dadosResumoFatura.setListaCreditoDebitoARealizar(listaCreditoDebitoARealizar);
			}
		} else {
			if (faturaPesquisaVO.getIdContratoEncerrarRescindir() == null) {
				this.marcarCreditosDebitosAtuais(faturaPesquisaVO, dadosResumoFatura, dadosResumoFatura.getReferencia());
			}
		}

		BigDecimal valorTotalCreditoDebito = BigDecimal.ZERO;

		iteraListaCreditoDebito(dadosResumoFatura, listaCreditoDebitoRetorno, listaCreditoDebito, valorTotalCreditoDebito);

		model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebitoRetorno);
		model.addAttribute("isRefaturar", Boolean.FALSE);

		if (dadosResumoFatura != null) {
			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());
		}

		Collection<DadosItemFaturamento> colecaoItensFaturamento =
				(Collection<DadosItemFaturamento>) request.getSession().getAttribute(COLECAO_ITENS_FATURAMENTO);
		if ((colecaoItensFaturamento == null) || (colecaoItensFaturamento.isEmpty())) {
			colecaoItensFaturamento = new ArrayList<DadosItemFaturamento>();
			DadosItemFaturamento itemFaturamento = null;

			if (dadosResumoFatura != null) {
				for (DadosGeraisItensFatura dadosGerais : dadosResumoFatura.getListaDadosGeraisItens()) {
					itemFaturamento = controladorFatura.criarDadosItemFaturamento();
					itemFaturamento.setDescricao(dadosGerais.getDescricao());
					itemFaturamento.setDataVencimento(dadosGerais.getDataVencimento());
					colecaoItensFaturamento.add(itemFaturamento);
				}
			}
			request.getSession().setAttribute(COLECAO_ITENS_FATURAMENTO, colecaoItensFaturamento);
		}

		model.addAttribute(ITENS_FATURA, colecaoItensFaturamento);
		if (dadosResumoFatura != null) {
			model.addAttribute(LISTA_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());
		}
		model.addAttribute(LISTA_MOTIVO_INCLUSAO, controladorEntidadeConteudo.obterListaMotivoInclusao());

		if (dadosResumoFatura != null && dadosResumoFatura.getCliente() != null) {
			model.addAttribute("cliente", controladorCliente.obterCliente(dadosResumoFatura.getCliente().getChavePrimaria(), ENDERECOS));
			faturaPesquisaVO.setIdCliente(dadosResumoFatura.getCliente().getChavePrimaria());
		}

		if (dadosResumoFatura != null && dadosResumoFatura.getListaPontoConsumo() != null
				&& !dadosResumoFatura.getListaPontoConsumo().isEmpty() && dadosResumoFatura.getListaPontoConsumo().size() == 1) {
			model.addAttribute("pontoConsumo", Util.primeiroElemento(dadosResumoFatura.getListaPontoConsumo()));
		}

		faturasPendentes = null;
		if (dadosResumoFatura != null && dadosResumoFatura.getListaFaturasPendentes() != null) {
			faturasPendentes = dadosResumoFatura.getListaFaturasPendentes();
		}

		model.addAttribute(LISTA_FATURAS_PENDENTES, faturasPendentes);
	}

	private void incluirAnoMesReferenciaAtual(FaturaPesquisaVO faturaPesquisaVO, DadosResumoFatura dadosResumoFatura,
			boolean isIncluirCiclo, AnoMesReferenciaVO anoMesReferencia) {
		if (dadosResumoFatura != null) {
			anoMesReferencia.setAnoMes(dadosResumoFatura.getReferencia());
			anoMesReferencia.setCiclo(dadosResumoFatura.getCiclo());
			if (dadosResumoFatura.getCiclo() != null) {
				faturaPesquisaVO.setIdAnoMesReferencia(dadosResumoFatura.getReferencia() + dadosResumoFatura.getCiclo());
				faturaPesquisaVO.setAnoMesReferencia(String.valueOf(dadosResumoFatura.getReferencia()));
			} else {
				faturaPesquisaVO.setIdAnoMesReferencia(dadosResumoFatura.getReferencia());
			}
		}

		if (dadosResumoFatura != null && !isIncluirCiclo) {
			anoMesReferencia.setAnoMesFormatado(Util.formatarAnoMes(dadosResumoFatura.getReferencia()));
		} else if (dadosResumoFatura != null) {
			anoMesReferencia.setAnoMesFormatado(
					Util.formatarAnoMes(dadosResumoFatura.getReferencia()) + "-" + dadosResumoFatura.getCiclo().toString());
		}
	}

	private DadosResumoFatura incluirEncerramentoContrato(FaturaPesquisaVO faturaPesquisaVO, HttpServletRequest request, Model model)
			throws GGASException {
		DadosResumoFatura dadosResumoFatura;
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

		String dataRecisao = request.getParameter("periodoRecisaoCampo");
		if (dataRecisao != null && !"".equals(dataRecisao)) {
			request.getSession().setAttribute("periodoRecisaoCampo", dataRecisao);
		}

		String listaIdsAgrupados = request.getParameter("paramListaIdsPontoConsumoRemovidosAgrupados");
		model.addAttribute(LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS, listaIdsAgrupados);
		
		PontoConsumo pontoConsumo = null;
		if (faturaPesquisaVO.getIdPontoConsumo() != null) {
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(faturaPesquisaVO.getIdPontoConsumo(),
					"quadraFace", IMOVEL, "rota", "ramoAtividade", INSTALACAO_MEDIDOR);
		} 
		
		listaPontoConsumo.add(pontoConsumo);

		preencherListaPontoConsumoByIds(listaPontoConsumo, listaIdsAgrupados);

		dadosResumoFatura = controladorFatura.obterDadosInclusaoFatura(
				controladorCliente.obterClientePorPontoConsumo(faturaPesquisaVO.getIdPontoConsumo()).getChavePrimaria(), listaPontoConsumo,
				null, null, true, null);
		return dadosResumoFatura;
	}

	private DadosResumoFatura inclusaoFluxoNormal(FaturaPesquisaVO faturaPesquisaVO) throws GGASException {
		DadosResumoFatura dadosResumoFatura;
		PontoConsumo pontoConsumo;
		if (faturaPesquisaVO.getIdPontoConsumo() != null) {
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(faturaPesquisaVO.getIdPontoConsumo(),
					"quadraFace", IMOVEL, "rota", "ramoAtividade", INSTALACAO_MEDIDOR);
		} else {
			throw new NegocioException(Util.getMensagens().getString("ERRO_PONTO_CONSUMO_FATURA"), true);
		}

		dadosResumoFatura = controladorFatura.obterDadosInclusaoFatura(faturaPesquisaVO.getIdCliente(), pontoConsumo, null);
		return dadosResumoFatura;
	}

	private void iteraListaCreditoDebito(DadosResumoFatura dadosResumoFatura,
			Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRetorno, Collection<CreditoDebitoARealizar> listaCreditoDebito,
			BigDecimal valorTotalCreditoDebitoTmp) throws GGASException {
		BigDecimal valorTotalCreditoDebito = valorTotalCreditoDebitoTmp;

		if (listaCreditoDebito != null && !listaCreditoDebito.isEmpty()) {
			for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {
				valorTotalCreditoDebito = valorTotalCreditoDebito.add(creditoDebitoARealizar.getValor());

				CreditoDebitoRefaturarVO creditoDebitoVO = new CreditoDebitoRefaturarVO();
				creditoDebitoVO.setCreditoDebitoARealizar(creditoDebitoARealizar);
				if ((dadosResumoFatura.getListaCreditoDebitoARealizar() != null)
						&& (dadosResumoFatura.getListaCreditoDebitoARealizar().contains(creditoDebitoARealizar))) {

					creditoDebitoVO.setIncluidoNaFatura(true);
				} else {
					creditoDebitoVO.setIncluidoNaFatura(false);
				}

				BigDecimal valorCreditoDebito = creditoDebitoARealizar.getValor();
				if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
					valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
				}
				creditoDebitoVO.setSaldoCredito(valorCreditoDebito);

				listaCreditoDebitoRetorno.add(creditoDebitoVO);
			}
		}
	}

	/**
	 * Preencher lista ponto consumo by ids.
	 *
	 */
	private void preencherListaPontoConsumoByIds(Collection<PontoConsumo> listaPontoConsumo, String listaIdsAgrupados)
			throws GGASException {

		Long[] idsPontoConsumo;

		if (listaIdsAgrupados != null && !"".equals(listaIdsAgrupados)) {
			idsPontoConsumo = Util.arrayStringParaArrayLong(listaIdsAgrupados.split(","));

			if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
				for (int i = 0; i < idsPontoConsumo.length; i++) {
					PontoConsumo pontoConsumo =
							(PontoConsumo) controladorPontoConsumo.obter(idsPontoConsumo[i], "quadraFace", IMOVEL, "rota", "ramoAtividade");
					listaPontoConsumo.add(pontoConsumo);
				}
			}
		}

	}

	/**
	 * Atualizar lista credito debito fatura avulsa.
	 *
	 */
	private void atualizarListaCreditoDebitoFaturaAvulsa(HttpServletRequest request, Long idCliente, Long idPontoConsumo,
			DadosResumoFatura dadosResumoFatura) throws GGASException {

		Boolean isAvulso = (Boolean) request.getAttribute("isAvulso");

		if (isAvulso != null && isAvulso) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			// pegar todos os debitos e creditos e setar na lista de dadosResumoFatura
			Long[] idsPontoConsumo = new Long[] { idPontoConsumo };
			filtro.put(CHAVES_PONTO_CONSUMO, idsPontoConsumo);
			filtro.put(ID_CLIENTE, idCliente);
			controladorFatura.popularFiltroCreditoDebitos(filtro);
			Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);
			dadosResumoFatura.getListaCreditoDebitoARealizarTotal().clear();
			dadosResumoFatura.getListaCreditoDebitoARealizarTotal().addAll(listaCreditoDebito);
		}
	}

	/**
	 * Remover credito debito faturas.
	 *
	 */
	private void removerCreditoDebitoFaturas(DadosResumoFatura dadosResumoFatura) {

		Collection<DadosGeraisItensFatura> listaDadosGerais = dadosResumoFatura.getListaDadosGeraisItens();
		for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGerais) {
			Iterator<DadosItensFatura> iterator = dadosGeraisItensFatura.getListaDadosItensFatura().iterator();

			while (iterator.hasNext()) {
				DadosItensFatura dadosItensFatura = iterator.next();
				if (dadosItensFatura.getCreditoDebitoARealizar() != null) {
					iterator.remove();
				}
			}
		}
	}

	// se existir mais de um historicoConsumo para um mesmo mês-ano-ciclo manter
	// apenas o mais atual na lista
	/**
	 * Remover historico fatura duplicados.
	 *
	 */
	// porque os outros foram excluidos pelo sistema e apenas o mais atual é
	// utilizado
	private List<HistoricoConsumo> removerHistoricoFaturaDuplicados(List<HistoricoConsumo> listaHistoricoConsumo) {

		Map<Integer, HistoricoConsumo> mapHistoricoConsumo = new HashMap<Integer, HistoricoConsumo>();

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			Integer anoMesFaturamentoCicloChaveMap =
					Integer.parseInt(historicoConsumo.getAnoMesFaturamento().toString() + historicoConsumo.getNumeroCiclo().toString());

			if (!mapHistoricoConsumo.containsKey(anoMesFaturamentoCicloChaveMap)) {
				mapHistoricoConsumo.put(anoMesFaturamentoCicloChaveMap, historicoConsumo);
			} else {
				if (historicoConsumo.getUltimaAlteracao().getTime() > mapHistoricoConsumo.get(anoMesFaturamentoCicloChaveMap)
						.getUltimaAlteracao().getTime()) {
					mapHistoricoConsumo.put(anoMesFaturamentoCicloChaveMap, historicoConsumo);
				}
			}
		}

		return new ArrayList<HistoricoConsumo>(mapHistoricoConsumo.values());
	}

	/**
	 * Obter historico consumo todos.
	 * 
	 * @param idPontoConsumo {@link Long}
	 * @param isExibirCiclo {@link boolean}
	 * @return List {@link List}
	 * @throws NegocioException {@link NegocioException}
	 */
	public List<AnoMesReferenciaVO> obterHistoricoConsumoTodos(Long idPontoConsumo, boolean isExibirCiclo) throws NegocioException {

		List<HistoricoConsumo> historicoConsumo = null;

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			historicoConsumo = controladorHistoricoConsumo.obterHistoricoConsumoTodos(idPontoConsumo, false, true, false);
		} else {
			historicoConsumo = controladorHistoricoConsumo.obterHistoricoConsumoTodos(0L, false, true, false);
		}

		historicoConsumo = removerHistoricoFaturaDuplicados(historicoConsumo);
		List<AnoMesReferenciaVO> anoMesReferenciaLista = new ArrayList<AnoMesReferenciaVO>();

		for (HistoricoConsumo historicoConsumoItem : historicoConsumo) {
			AnoMesReferenciaVO anoMesReferenciaVO = new AnoMesReferenciaVO();

			if (!isExibirCiclo) {
				anoMesReferenciaVO.setAnoMes(historicoConsumoItem.getAnoMesFaturamento());
				anoMesReferenciaVO.setAnoMesFormatado(historicoConsumoItem.getAnoMesFaturamentoFormatado());
				anoMesReferenciaVO.setCiclo(historicoConsumoItem.getNumeroCiclo());
			} else {
				anoMesReferenciaVO.setAnoMes(historicoConsumoItem.getAnoMesFaturamento());
				anoMesReferenciaVO.setAnoMesFormatado(
						historicoConsumoItem.getAnoMesFaturamentoFormatado() + "-" + historicoConsumoItem.getNumeroCiclo());
				anoMesReferenciaVO.setCiclo(historicoConsumoItem.getNumeroCiclo());
			}
			anoMesReferenciaLista.add(anoMesReferenciaVO);
		}

		return anoMesReferenciaLista;
	}

	/**
	 * Marcar creditos debitos atuais.
	 *
	 */
	private void marcarCreditosDebitosAtuais(FaturaPesquisaVO faturaPesquisaVO, DadosResumoFatura dadosResumoFatura, Integer referencia) {

		dadosResumoFatura.getListaCreditoDebitoARealizar().clear();
		for (CreditoDebitoARealizar creditoDebitoARealizar : dadosResumoFatura.getListaCreditoDebitoARealizarTotal()) {
			if (creditoDebitoARealizar.getAnoMesFaturamento().compareTo(referencia) == 0) {
				dadosResumoFatura.getListaCreditoDebitoARealizar().add(creditoDebitoARealizar);
			}
		}

		if ((dadosResumoFatura.getListaCreditoDebitoARealizar() != null)
				&& (!dadosResumoFatura.getListaCreditoDebitoARealizar().isEmpty())) {
			faturaPesquisaVO
					.setChavesCreditoDebito(Util.collectionParaArrayChavesPrimarias(dadosResumoFatura.getListaCreditoDebitoARealizar()));
		}
	}

	/**
	 * Método responsável por exibir a tela de alteração de vencimento.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirAlteracaoVencimentoFatura")
	public String exibirAlteracaoVencimentoFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "exibirAlteracaoVencimentoFatura";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			controladorFatura.validarSituacaoAlteracaoVencimento(faturaVO.getChavesPrimarias());
			controladorFatura.verificarFaturasAlteracaoVencimento(faturaVO.getChavesPrimarias());
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, faturaVO.getChavesPrimarias());
			model.addAttribute(LISTA_FATURA, controladorFatura.consultarFatura(filtro));
			model.addAttribute(LISTA_MOTIVO, null);
			saveToken(request);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarFatura(faturaVO, request, model);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de alteração de vencimento.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("alterarVencimentoFatura")
	public String alterarVencimentoFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "forward:exibirAlteracaoVencimentoFatura";
		salvarPesquisaFiltro(faturaVO, model);
		try {
			validarToken(request);

			Map<String, Object> parametros = new HashMap<String, Object>();
			if (faturaVO.getChavesPrimarias() != null && faturaVO.getChavesPrimarias().length > 0) {
				parametros.put(CHAVES_PRIMARIAS, faturaVO.getChavesPrimarias());
			}
			if (!StringUtils.isEmpty(faturaVO.getMotivoAlteracao())) {
				parametros.put(MOTIVO_ALTERACAO, faturaVO.getMotivoAlteracao());
			}
			if (!StringUtils.isEmpty(faturaVO.getDataVencimento())) {
				parametros.put(DATA_VENCIMENTO, Util.converterCampoStringParaData(Fatura.DATA_VENCIMENTO, faturaVO.getDataVencimento(),
						Constantes.FORMATO_DATA_BR));
			}
			if (faturaVO.getIncidemJurosMulta() != null) {
				parametros.put(INCIDEM_JUROS_MULTA, faturaVO.getIncidemJurosMulta());
			}

			byte[] retornoRelatorio = controladorFatura.atualizarVencimentoFatura(parametros, getDadosAuditoria(request));

			if (retornoRelatorio != null) {
				model.addAttribute(GERAR_RELATORIO_EMISSAO_FATURA, true);
				request.getSession().setAttribute(RELATORIO_EMISSAO_FATURA, retornoRelatorio);
			}

			mensagemSucesso(model, Fatura.SUCESSO_ALTERACAO_VENCIMENTO_FATURA);
			tela = pesquisarFatura(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de revisar Fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirRevisaoFatura")
	public String exibirRevisaoFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws NegocioException {

		String tela = "exibirRevisaoFatura";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			boolean existeMotivoRevisao = controladorFatura.validarRevisaoFatura(faturaVO.getChavesPrimarias());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, faturaVO.getChavesPrimarias());
			Collection<Fatura> listaFatura = controladorFatura.consultarFatura(filtro);

			if (existeMotivoRevisao) {
				model.addAttribute(TELA_REVISAO_TITULO, "Retirar Fatura da Revisão");

				for (Fatura fatura : listaFatura) {
					DateTime dataRevisaoCalculada = new DateTime(fatura.getDataRevisao());
					dataRevisaoCalculada = dataRevisaoCalculada.plusDays(fatura.getMotivoRevisao().getPrazoMaximoDias());

					fatura.setDataRevisao(dataRevisaoCalculada.toDate());
				}

			} else {
				model.addAttribute(TELA_REVISAO_TITULO, "Colocar Fatura em Revisão");
				Collection<FaturaMotivoRevisao> listaFaturaMotivoRevisao = controladorFatura.listarMotivoRevisaoFatura();
				if (listaFaturaMotivoRevisao != null) {
					model.addAttribute(LISTA_MOTIVOS_REVISAO, listaFaturaMotivoRevisao);
				}
			}
			model.addAttribute(LISTA_FATURA, listaFatura);
			faturaVO.setIndicadorRevisao(existeMotivoRevisao);

			Map<String, Object> filtroFuncionario = new HashMap<String, Object>();
			filtroFuncionario.put("habilitado", true);
			Collection<Funcionario> listaFuncionario = controladorFuncionario.consultarFuncionarios(filtroFuncionario);
			model.addAttribute(LISTA_FUNCIONARIO, listaFuncionario);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarFatura(faturaVO, request, model);
		}

		return tela;
	}

	/**
	 * Revisar fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 */
	@RequestMapping("revisarFatura")
	public String revisarFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException, ConcorrenciaException {

		try {
			controladorFatura.revisarFatura(faturaVO.getChavesPrimarias(), faturaVO.getIdMotivoRevisao(), faturaVO.getIndicadorRevisao(),
					getDadosAuditoria(request), faturaVO.getIdFuncionario());

			if (faturaVO.getIndicadorRevisao()) {
				mensagemSucesso(model, Fatura.SUCESSO_RETIRA_REVISAO_FATURA);
			} else {
				mensagemSucesso(model, Fatura.SUCESSO_COLOCA_REVISAO_FATURA);
			}
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Método responsável por exibir a tela de cancelar Fatura.
	 *
	 * @param faturaVo {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirCancelarFatura")
	public String exibirCancelarFatura(FaturaPesquisaVO faturaVo, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "exibirCancelarFatura";
		try {
			// Limpando da sessão a lista de faturas
			// para cancelar
			request.removeAttribute(LISTA_FATURA_CANCELAR);
			request.removeAttribute(DADOS_RESUMO_FATURA);
			request.getSession().setAttribute("faturaVO", faturaVo);
			salvarPesquisaFiltro(faturaVo, model);

			Collection<Fatura> listaFaturas = (Collection<Fatura>) request.getSession().getAttribute(LISTA_FATURA_SESSAO);

			Collection<Fatura> listaFaturaCancelar = controladorFatura.listaFaturaCancelar(listaFaturas, faturaVo.getChavesPrimarias());

			model.addAttribute(LISTA_FATURA_CANCELAR, listaFaturaCancelar);
			model.addAttribute(LISTA_MOTIVO_CANCELAMENTO, controladorEntidadeConteudo.obterListaMotivoCancelamento());

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			return pesquisarFatura(faturaVo, request, model);
		}

		return tela;
	}

	/**
	 * Método responsável por cancelar uma fatura.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 */
	@RequestMapping("cancelarFatura")
	public String cancelarFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException, ConcorrenciaException {

		String tela = "exibirPesquisaFatura";
		try {
			controladorFatura.validarMotivoCancelamento(faturaVO.getIdMotivoCancelamento());

			// TODO testar. alterado após integração
			// com Nf-e.
			// fachada.cancelarFaturas(chavesPrimarias,
			// idMotivoCancelamento,
			// getDadosAuditoria(request), false)
			controladorFatura.solicitarCancelamentoFaturas(faturaVO.getChavesPrimarias(), faturaVO.getIdMotivoCancelamento(),
					getDadosAuditoria(request), false);

			mensagemSucesso(model, Fatura.SUCESSO_CANCELAMENTO_FATURA);
			
			faturaVO = (FaturaPesquisaVO) request.getSession().getAttribute("faturaVO");
			salvarPesquisaFiltro(faturaVO, model);
			request.getSession().removeAttribute("faturaVO");
			
			return pesquisarFatura(faturaVO, request, model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
			tela = exibirCancelarFatura(faturaVO, result, request, model);
		}

		return tela;
	}

	/**
	 * Excluir fatura.
	 * 
	 * @param faturaVo {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("excluirFatura")
	public String excluirFatura(FaturaPesquisaVO faturaVo, BindingResult result, HttpServletRequest request, Model model)
			throws ConcorrenciaException, NegocioException {

		try {
			salvarPesquisaFiltro(faturaVo, model);
			controladorFatura.removerFaturas(faturaVo.getChavesPrimarias(), getDadosAuditoria(request));
			mensagemSucesso(model, Fatura.SUCESSO_EXCLUSAO_FATURA);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return pesquisarFatura(faturaVo, request, model);
	}

	/**
	 * Metódo responsável por enviar fatura por email
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("enviarEmailFatura")
	public String enviarEmailFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			enviarFatura(faturaVO.getChavePrimaria(), faturaVO.getIdDocumentoFiscal(), request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Método para iniciar processo de envio de fatura em lote
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("enviarEmailFaturaEmLote")
	public String enviarEmailFaturaEmLote(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			for (int i = 0; i < faturaVO.getChavesPrimarias().length; i++) {
				this.enviarFatura(faturaVO.getChavesPrimarias()[i], null, request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Método privado de envio das faturas
	 * 
	 * @throws GGASException
	 */
	private void enviarFatura(Long idFatura, Long chavePrimariaDocFiscal, HttpServletRequest request, Model model) throws GGASException {

		if ((idFatura != null) && (idFatura > 0)) {
			Fatura fatura = (Fatura) controladorCobranca.obterFatura(idFatura, "listaFaturaItem", "faturaGeral");
			fatura.setDadosAuditoria(getDadosAuditoria(request));

			DocumentoFiscal documentoFiscal = null;

			if (chavePrimariaDocFiscal != null && chavePrimariaDocFiscal > 0) {
				documentoFiscal = controladorFatura.obterDocumentoFiscalPorChave(chavePrimariaDocFiscal);
			} else {
				documentoFiscal = controladorFatura.obterDocumentoFiscalporNumeroFatura(idFatura);
			}

			byte[] faturaRelatorio = controladorFatura.gerarImpressaoFaturaSegundaVia(fatura, documentoFiscal);

			enviarFaturaPorEmail(faturaRelatorio, idFatura, model);

			mensagemSucesso(model, Fatura.SUCESSO_ENVIO_FATURA);
		}

	}

	/**
	 * Método responsável por imprimir a segunda via da fatura / nota fiscal.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirSegundaViaFaturaTelaFatura")
	public String imprimirSegundaVia(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {
		salvarPesquisaFiltro(faturaVO, model);
		try {
			if ((faturaVO.getChavePrimaria() != null) && (faturaVO.getChavePrimaria() > 0)) {
				Fatura fatura = (Fatura) controladorFatura.obter(faturaVO.getChavePrimaria(), "listaFaturaItem", "faturaGeral");
				fatura.setDadosAuditoria(getDadosAuditoria(request));

				DocumentoFiscal documentoFiscal = null;

				if (faturaVO.getIdDocumentoFiscal() != null && faturaVO.getIdDocumentoFiscal() > 0) {
					documentoFiscal = controladorFatura.obterDocumentoFiscalPorChave(faturaVO.getIdDocumentoFiscal());
				}

				imprimirPDFRelatorio("relatorioFaturaNotaFiscal", controladorFatura.gerarImpressaoFaturaSegundaVia(fatura, documentoFiscal),
						response);

			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaFatura(request, model);
	}

	/**
	 * Método responsável por imprimir o DANFE referente à fatura escolhida
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirDanfe")
	public String imprimirDanfe(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			Model model) throws GGASException {

		salvarPesquisaFiltro(faturaVO, model);
		try {
			if (faturaVO.getChavePrimaria() != null && faturaVO.getChavePrimaria() > 0
					&& faturaVO.getIdDocumentoFiscal() != null && faturaVO.getIdDocumentoFiscal() > 0) {
				DocumentoFiscal documentoFiscal = controladorFatura
						.obterDocumentoFiscalPorChave(faturaVO.getIdDocumentoFiscal());
				
				CreditoDebitoSituacao creditoSituacao = null;
				
				if (documentoFiscal.getFatura() != null
						&& documentoFiscal.getFatura().getCreditoDebitoSituacao() != null) {
					creditoSituacao = documentoFiscal.getFatura().getCreditoDebitoSituacao();
				}
				
				if (creditoSituacao != null) {
					if (creditoSituacao.getChavePrimaria() == 5l || creditoSituacao.getChavePrimaria() == 6l
							|| creditoSituacao.getChavePrimaria() == 9l) {
						throw new NegocioException("A NFe da fatura n " + documentoFiscal.getFatura().getChavePrimaria()
								+ " está pendente de validação na SEFAZ. Apenas NFe validadas com sucesso na SEFAZ permitem a impressão do PDF do DANFE",
								true);
					}
				}
				
				imprimirPDFRelatorio(NOME_ARQUIVO_DANFE_IMPRESSAO,
						controladorFatura.gerarImpressaoDanfe(documentoFiscal), response);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Método para iniciar processo de envio de danfe
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param request {@link BindingResult}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("enviarEmailDanfe")
	public String enviarEmailDanfe(FaturaPesquisaVO faturaVO, HttpServletRequest request, Model model) throws GGASException {

		salvarPesquisaFiltro(faturaVO, model);
		try {
			if (faturaVO.getChavePrimaria() != null && faturaVO.getChavePrimaria() > 0
					&& faturaVO.getIdDocumentoFiscal() != null && faturaVO.getIdDocumentoFiscal() > 0) {
				DocumentoFiscal documentoFiscal = controladorFatura
						.obterDocumentoFiscalPorChave(faturaVO.getIdDocumentoFiscal());
				
				CreditoDebitoSituacao creditoSituacao = null;
				
				if (documentoFiscal.getFatura() != null
						&& documentoFiscal.getFatura().getCreditoDebitoSituacao() != null) {
					creditoSituacao = documentoFiscal.getFatura().getCreditoDebitoSituacao();
				}
				
				if (creditoSituacao != null) {
					if (creditoSituacao.getChavePrimaria() == 5l || creditoSituacao.getChavePrimaria() == 6l
							|| creditoSituacao.getChavePrimaria() == 9l) {
						throw new NegocioException("A NFe da fatura n " + documentoFiscal.getFatura().getChavePrimaria()
								+ " está pendente de validação na SEFAZ. Apenas NFe validadas com sucesso na SEFAZ permitem a impressão do PDF do DANFE",
								true);
					}
				}
				
				enviarDanfePorEmail(controladorFatura.gerarImpressaoDanfe(documentoFiscal), faturaVO.getChavePrimaria(),
						model);
				mensagemSucesso(model, Fatura.SUCESSO_ENVIO_FATURA);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Gera um zip e-Cartas à partir de uma fatura
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("gerarECartas")
	public String gerarECartas(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			Model model) throws NegocioException {

		if ((faturaVO.getIdFatura() != null) && (faturaVO.getIdFatura() > 0)) {
			try {
				salvarPesquisaFiltro(faturaVO, model);
				List<FaturaImpressao> faturas = new ArrayList<>();
				FaturaImpressao faturaImpressao = controladorLoteECarta.obterFaturaImpressaoPorFatura(faturaVO.getIdFatura());
				if (faturaImpressao != null) {
					Contrato contrato =
							controladorContrato.consultarContratoPontoConsumoPorPontoConsumoAtivoOuMaisRecente(faturaImpressao.getFatura());
					if (contrato.getIndicadorParticipanteECartas() == null || contrato.getIndicadorParticipanteECartas()) {
						throw new NegocioException(Constantes.ERRO_CONTRATO_NAO_PARTICIPA_ECARTAS, true);
					}
					faturas.add(faturaImpressao);
				}
				gerarECartas(response, controladorLoteECarta, faturas);
			} catch (NegocioException e) {
				mensagemErro(model, request, e.getChaveErro());
				LOG.info(e.getMessage());
			} catch (Exception e) {
				mensagemErro(model, request, Constantes.ERRO_INESPERADO);
				LOG.info(e.getMessage());
			}
		}
		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Gera um zip e-Cartas à partir de uma lista de faturas
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("gerarECartasEmLote")
	public String gerarECartasEmLote(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {

		try {
			salvarPesquisaFiltro(faturaVO, model);
			List<FaturaImpressao> faturas = new ArrayList<>();

			for (int i = 0; i < faturaVO.getChavesPrimarias().length; i++) {
				FaturaImpressao faturaImpressao = controladorLoteECarta.obterFaturaImpressaoPorFatura(faturaVO.getChavesPrimarias()[i]);
				faturas.add(faturaImpressao);
			}
			gerarECartas(response, controladorLoteECarta, faturas);
		} catch (NegocioException e) {
			mensagemErro(model, request, e.getChaveErro());
			LOG.info(e.getMessage());
		} catch (Exception e) {
			mensagemErro(model, request, Constantes.ERRO_INESPERADO);
			LOG.info(e.getMessage());
		}
		return pesquisarFatura(faturaVO, request, model);
	}

	/**
	 * Método responsável por fazer a chamada do arquivo e-Carta e baixá-lo caso exista
	 * 
	 */
	private void gerarECartas(HttpServletResponse response, ControladorLoteECarta controladorLoteECarta, List<FaturaImpressao> faturas)
			throws IOException, GGASException, ParserConfigurationException {
		File arquivo = controladorLoteECarta.gerarECartas(faturas);
		if (arquivo.exists()) {
			baixarZip(arquivo.getName(), Util.converterFileParaByte(arquivo), response);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_NAO_ENCONTRADO_EM_DIRETORIO, true);
		}
	}

	/**
	 * Método para envio de um arquivo por e-mail
	 * 
	 */
	private void enviarArquivoPorEmail(byte[] arquivo, Long idFatura, String corpoMsg, String assunto, String nomeAnexo, Model model)
			throws NegocioException {

		Cliente cliente = controladorCliente.obterClientePorFatura(idFatura);
		String email;
		if (cliente.getEmailPrincipal() != null && !cliente.getEmailPrincipal().isEmpty()) {
			email = cliente.getEmailPrincipal();
		} else {
			email = cliente.getEmailSecundario();
		}
		if (email == null) {
			throw new NegocioException(Constantes.CLIENTE_ERRO_EMAIL, true);
		}
		ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(arquivo, "application/pdf");
		try {
			String valorConstanteSistemaEmailRemetentePadrao =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
			getJavaMailUtil().enviar(valorConstanteSistemaEmailRemetentePadrao, email, assunto, corpoMsg, nomeAnexo, byteArrayDataSource);
		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENVIO_ARQUIVO_EMAIL, true));
		}
	}

	/**
	 * Método para envio do DANFE
	 * 
	 * @param danfe {@link byte}
	 * @param idFatura {@link Long}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void enviarDanfePorEmail(byte[] danfe, Long idFatura, Model model) throws NegocioException {

		String assunto = ASSUNTO_EMAIL_DANFE;
		String attachmentFilename = NOME_ANEXO_EMAIL_DANFE + idFatura;
		String conteudoEmail = String.format(CORPO_MSG_EMAIL_DANFE, idFatura);
		enviarArquivoPorEmail(danfe, idFatura, conteudoEmail, assunto, attachmentFilename, model);
	}

	/**
	 * Método para envio do relatório
	 * 
	 * @param faturaRelatorio {@link byte}
	 * @param idFatura {@link Long}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void enviarFaturaPorEmail(byte[] faturaRelatorio, Long idFatura, Model model) throws NegocioException {
		String assunto = ASSUNTO_EMAIL_FATURA;
		String attachmentFilename = NOME_ANEXO_EMAIL_FATURA + idFatura;
		String conteudoEmail = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEXTO_EMAIL_FATURA);
		enviarArquivoPorEmail(faturaRelatorio, idFatura, conteudoEmail, assunto, attachmentFilename, model);
	}

	private JavaMailUtil getJavaMailUtil() {

		return (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
	}

	/**
	 * Imprimir gerar fatura pendente.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 *
	 */
	@RequestMapping("imprimirGerarFaturaPendenteFatura")
	public String imprimirGerarFaturaPendente(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws GGASException {

		try {
			String dataVencimento;

			Fatura fatura = (Fatura) controladorCobranca.obterFatura(faturaVO.getChavePrimaria());
			if (StringUtils.isEmpty(faturaVO.getNovaDataVencimento())) {
				dataVencimento = fatura.getDataVencimentoFormatada();
			} else {
				dataVencimento = faturaVO.getNovaDataVencimento();
			}

			byte[] arquivo = controladorFatura.gerarBoletoBancarioPadrao(fatura, Boolean.TRUE, Constantes.C_TIPO_DOCUMENTO_FATURA,
					dataVencimento, true);

			imprimirPDFRelatorio("relatorioFaturaNotaFiscal", arquivo, response);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaFatura(request, model);
	}

	/**
	 * Exibir resumo inclusao fatura avulso.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirResumoInclusaoFaturaAvulso")
	public String exibirResumoInclusaoFaturaAvulso(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		// chama o método consistir... criando uma variavel boolean... se for avulso
		// preencher os dados da medição padrão.
		// não é levado cronograma nem nada do tipo
		String tela = "exibirResumoInclusaoFaturaAvulso";
		try {
			Boolean isFaturaAvulso = Boolean.TRUE;
			controladorFatura.validarFaturaAvulsaTransportadora(faturaVO.getIdTransportadora(), faturaVO.getIdVeiculo(),
					faturaVO.getIdMotorista());
			salvarPesquisaFiltro(faturaVO, model);
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			request.getSession().setAttribute(DADOS_INDICADOR_RESUMO, Boolean.TRUE);
			request.getSession().setAttribute(DADOS_RESUMO_FATURA_AUXILIAR, dadosResumoFatura);

			if (dadosResumoFatura != null) {

				List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();

				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				this.popularDadosLeituraConsumoFaturaAvulso(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao,
						dadosResumoFatura, request, model);
				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);

				for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
					controladorHistoricoMedicao.validarDataAtualInformada(historicoMedicao);
				}
				if (isConsolidado) {
					controladorHistoricoMedicao.inativarHistoricoMedicao(idHistoricoMedicao);
					isConsolidado = false;
				}

				if (!isConsolidado) {
					controladorFatura.consistirLeituraFaturamento(listaHistoricoMedicao, isFaturaAvulso);
					HistoricoMedicao historicoMedicao = listaHistoricoMedicao.get(0);
					idHistoricoMedicao = historicoMedicao.getChavePrimaria();
					isConsolidado = Boolean.TRUE;
				}

				request.getSession().setAttribute(DADOS_INDICADOR_RESUMO, Boolean.TRUE);

				if (dadosResumoFatura.getCliente() != null) {
					Cliente cliente = controladorCliente.obterCliente(dadosResumoFatura.getCliente().getChavePrimaria(), ENDERECOS);
					model.addAttribute("cliente", cliente);
					faturaVO.setIdCliente(cliente.getChavePrimaria());
				}

				if ((dadosResumoFatura.getListaPontoConsumo() != null) && (!dadosResumoFatura.getListaPontoConsumo().isEmpty())
						&& (dadosResumoFatura.getListaPontoConsumo().size() == 1)) {
					PontoConsumo pontoConsumo = Util.primeiroElemento(dadosResumoFatura.getListaPontoConsumo());
					model.addAttribute("pontoConsumo", pontoConsumo);
					faturaVO.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
				}

				List<Fatura> faturasPendentes = new ArrayList<Fatura>();
				if ((dadosResumoFatura.getListaFaturasPendentes() != null) && (!dadosResumoFatura.getListaFaturasPendentes().isEmpty())
						&& (faturaVO.getChavesFaturasPendentes() != null) && (faturaVO.getChavesFaturasPendentes().length > 0)) {

					// Adicionando as faturas
					// pendentes selecionadas.
					for (Fatura faturaPendente : dadosResumoFatura.getListaFaturasPendentes()) {
						if (ArrayUtils.contains(faturaVO.getChavesFaturasPendentes(), faturaPendente.getChavePrimaria())) {
							faturasPendentes.add(faturaPendente);
						}
					}
				}

				model.addAttribute(LISTA_FATURAS_PENDENTES, faturasPendentes);
				model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

				int quantidadeCiclo = -1;
				if (dadosResumoFatura.getQuantidadeCiclo() != null) {
					quantidadeCiclo = dadosResumoFatura.getQuantidadeCiclo();
				}
				if (!isConsolidado) {
					controladorFatura.validarAvancarResumoInclusaoFatura(listaHistoricoMedicao);
				}
				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));

				Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
				if ((faturaVO.getChavesCreditoDebito() != null) && (faturaVO.getChavesCreditoDebito().length > 0)) {

					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(CHAVES_CREDITO_DEBITO, faturaVO.getChavesCreditoDebito());

					if (dadosResumoFatura.getCliente() != null) {
						filtro.put(ID_CLIENTE, dadosResumoFatura.getCliente().getChavePrimaria());
					}
					if (faturaVO.getIdContratoEncerrarRescindir() == null) {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);
					} else {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitosParaFaturaEncerramento(filtro);
					}
				}

				Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRetorno = new ArrayList<CreditoDebitoRefaturarVO>();

				if (listaCreditoDebito != null && !listaCreditoDebito.isEmpty()) {
					for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {

						CreditoDebitoRefaturarVO creditoDebitoVO = new CreditoDebitoRefaturarVO();
						creditoDebitoVO.setCreditoDebitoARealizar(creditoDebitoARealizar);
						if ((dadosResumoFatura.getListaCreditoDebitoARealizar() != null)
								&& (dadosResumoFatura.getListaCreditoDebitoARealizar().contains(creditoDebitoARealizar))) {

							creditoDebitoVO.setIncluidoNaFatura(true);
						} else {
							creditoDebitoVO.setIncluidoNaFatura(false);
						}

						BigDecimal valorCreditoDebito = creditoDebitoARealizar.getValor();
						if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
						}
						creditoDebitoVO.setSaldoCredito(valorCreditoDebito);

						listaCreditoDebitoRetorno.add(creditoDebitoVO);
					}
				}

				model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebitoRetorno);
				// TODO adicionar motivo padrao

				if (faturaVO.getIdMotivoInclusao() != null) {
					dadosResumoFatura.setIdMotivo(faturaVO.getIdMotivoInclusao());
				}

				dadosResumoFatura = controladorFatura.obterDadosResumoFatura(dadosResumoFatura, listaCreditoDebito, Boolean.TRUE,
						ControladorApuracaoPenalidade.DISTRIBUICAO_FATURAMENTO, Boolean.FALSE, null, null);

				if (faturaVO.getIdTransportadora() != null) {
					dadosResumoFatura.setIdTransportadora(faturaVO.getIdTransportadora());
					if (faturaVO.getIdVeiculo() != null) {
						dadosResumoFatura.setIdVeiculo(faturaVO.getIdVeiculo());
					}
					if (faturaVO.getIdMotorista() != null) {
						dadosResumoFatura.setIdMotorista(faturaVO.getIdMotorista());
					}
				}

				if (quantidadeCiclo != -1) {
					dadosResumoFatura.setQuantidadeCiclo(quantidadeCiclo);
				}

				model.addAttribute(DATA_EMISSAO,
						Util.converterDataParaStringSemHora(dadosResumoFatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
				model.addAttribute(DESCRICAO_MOTIVO_INCLUSAO, dadosResumoFatura.getMotivo());
				model.addAttribute(NUMERO_CONTRATO, dadosResumoFatura.getNumeroContrato());

				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);

				model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
				Collection<DadosGeraisItensFatura> listaDadosGeraisItens = dadosResumoFatura.getListaDadosGeraisItens();

				Collection<DadosItensFatura> listaDadosItensFatura =
						listaDadosGeraisItens.iterator().next().getListaDadosItensFaturaExibicao();

				Collections.sort((List<DadosItensFatura>) listaDadosItensFatura, new Comparator<DadosItensFatura>() {

					@Override
					public int compare(DadosItensFatura item1, DadosItensFatura item2) {

						int sequenciaData;
						if (item1.getCreditoDebitoARealizar() != null && item2.getCreditoDebitoARealizar() != null) {
							Date dataItem1 = item1.getCreditoDebitoARealizar().getUltimaAlteracao();
							Date dataItem2 = item2.getCreditoDebitoARealizar().getUltimaAlteracao();

							if (dataItem1.before(dataItem2)) {
								sequenciaData = -1;
							} else {
								if (dataItem1.after(dataItem2)) {
									sequenciaData = +1;
								} else {
									sequenciaData = 0;
								}
							}
						} else {
							sequenciaData = 0;
						}
						return sequenciaData;
					}
				});

				BigDecimal percentualParametro = new BigDecimal(CENTENA);
				percentualParametro = obterPercentualParametro(percentualParametro);
				BigDecimal valorCreditoDebitoTotal = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaSemDebito = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaComDesconto = BigDecimal.ZERO;
				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFaturaExibicao()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() == null) {
							valorTotalFaturaSemDebito = valorTotalFaturaSemDebito.add(dadosItensFatura.getValorTotal());
						}
					}
				}
				valorTotalFaturaComDesconto = valorTotalFaturaSemDebito.multiply(percentualParametro.divide(new BigDecimal(CENTENA)));

				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFaturaExibicao()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							BigDecimal valorCreditoDebito =
									controladorCreditoDebito.obterSaldoCreditoDebito(dadosItensFatura.getCreditoDebitoARealizar());
							if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) == 0
									&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) == 0)) {
								valorCreditoDebitoTotal = valorCreditoDebitoTotal.add(valorCreditoDebito);
								if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) < 0
										&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) < 0)) {
									dadosItensFatura.setValorTotalDisponivel(
											valorTotalFaturaComDesconto.subtract(valorCreditoDebitoTotal.subtract(valorCreditoDebito)));
									valorCreditoDebitoTotal = valorTotalFaturaComDesconto;
								}
							} else {
								dadosItensFatura.setValorTotalDisponivel(BigDecimal.ZERO);
							}
						}

						if (dadosItensFatura.getValorTotalDisponivel() == null) {
							dadosItensFatura.setValorTotalDisponivel(dadosItensFatura.getValorTotal());
						}
					}
					dadosGeraisItensFatura
							.setValorTotalFatura(dadosGeraisItensFatura.getValorTotalDebitos().subtract(valorCreditoDebitoTotal));
				}

				model.addAttribute(LISTA_DADOS_GERAIS, listaDadosGeraisItens);
				model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());

				if ((dadosResumoFatura.getExibirAlerta() != null) && (dadosResumoFatura.getExibirAlerta())) {
					super.mensagemAlerta(model, ControladorFatura.ERRO_NEGOCIO_CREDITO_REMANESCENTE);
				}

			}
			model.addAttribute(LISTA_MOTIVO_INCLUSAO, controladorEntidadeConteudo.obterListaMotivoInclusao());
			saveToken(request);
		} catch (GGASException e) {
			if (e.getMessage() != null && "O Consumo não foi informado ou é inválido.".equals(e.getMessage())) {
				e = new NegocioException("ERRO_NEGOCIO_CONSUMO_NAO_INFORMADO");
			}
			mensagemErroParametrizado(model, request, e);
			tela = exibirInclusaoFaturaAvulso(faturaVO, result, request, model);
		}
		return tela;
	}

	private BigDecimal obterPercentualParametro(BigDecimal percentualParametro) throws NegocioException {
		try {
			percentualParametro = Util.converterCampoStringParaValorBigDecimal(
					Constantes.PARAMETRO_MODULO_FATURAMENTO_PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO,
					(String) controladorParametroSistema.obterValorDoParametroPorCodigo(
							Constantes.PARAMETRO_MODULO_FATURAMENTO_PERCENTUAL_VALOR_FATURA_LIMITE_DESCONTO),
					Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
		} catch (FormatoInvalidoException e) {
			Logger.getLogger(ERROR).debug(e.getStackTrace(), e);
		}
		return percentualParametro;
	}

	/**
	 * Método responsável por exibir a tela de revisar Fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("exibirResumoInclusaoFatura")
	public String exibirResumoInclusaoFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws Exception {

		String tela = "exibirResumoInclusaoFatura";
		try {
			Boolean indicadorResumo = (Boolean) request.getSession().getAttribute(DADOS_INDICADOR_RESUMO);
			DadosResumoFatura dadosResumoFatura = null;
			salvarPesquisaFiltro(faturaVO, model);
			if (indicadorResumo != null && indicadorResumo) {
				dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA_AUXILIAR);
			} else {
				dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
				request.getSession().setAttribute(DADOS_RESUMO_FATURA_AUXILIAR, dadosResumoFatura);
				request.getSession().setAttribute(DADOS_INDICADOR_RESUMO, Boolean.TRUE);
			}

			if (dadosResumoFatura != null) {
				if (faturaVO.getIdMotivoInclusao() != null) {
					dadosResumoFatura.setIdMotivo(faturaVO.getIdMotivoInclusao());
				}

				if (dadosResumoFatura.getCliente() != null) {
					model.addAttribute("cliente", controladorCliente.obter(dadosResumoFatura.getCliente().getChavePrimaria(), ENDERECOS));
				}

				for (HistoricoConsumo historicoConsumo : dadosResumoFatura.getListaHistoricoConsumo()) {
					if (historicoConsumo.isIndicadorConsumoCiclo()) {
						controladorHistoricoConsumo.validarPeriodicidadeHistoricoConsumo(historicoConsumo);
					}
				}

				List<Fatura> faturasPendentes = new ArrayList<Fatura>();
				if ((dadosResumoFatura.getListaFaturasPendentes() != null) && (!dadosResumoFatura.getListaFaturasPendentes().isEmpty())
						&& (faturaVO.getChavesFaturasPendentes() != null) && (faturaVO.getChavesFaturasPendentes().length > 0)) {

					// Adicionando as faturas
					// pendentes selecionadas.
					for (Fatura faturaPendente : dadosResumoFatura.getListaFaturasPendentes()) {
						if (ArrayUtils.contains(faturaVO.getChavesFaturasPendentes(), faturaPendente.getChavePrimaria())) {
							faturasPendentes.add(faturaPendente);
						}
					}
				}

				model.addAttribute(LISTA_FATURAS_PENDENTES, faturasPendentes);
				model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

				int quantidadeCiclo = -1;
				if (dadosResumoFatura.getQuantidadeCiclo() != null) {
					quantidadeCiclo = dadosResumoFatura.getQuantidadeCiclo();
				}

				List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();

				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				this.popularDadosLeituraConsumoFatura(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao, dadosResumoFatura,
						request);

				controladorFatura.validarAvancarResumoInclusaoFatura(listaHistoricoMedicao);
				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));

				Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
				if ((faturaVO.getChavesCreditoDebito() != null) && (faturaVO.getChavesCreditoDebito().length > 0)) {

					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(CHAVES_CREDITO_DEBITO, faturaVO.getChavesCreditoDebito());

					if (dadosResumoFatura.getCliente() != null) {
						filtro.put(ID_CLIENTE, dadosResumoFatura.getCliente().getChavePrimaria());
					}
					if (faturaVO.getIdContratoEncerrarRescindir() == null || faturaVO.getIdContratoEncerrarRescindir() > 0) {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);
					} else {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitosParaFaturaEncerramento(filtro);
					}
				}

				Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRetorno = new ArrayList<CreditoDebitoRefaturarVO>();

				if (listaCreditoDebito != null && !listaCreditoDebito.isEmpty()) {
					for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {

						CreditoDebitoRefaturarVO creditoDebitoVO = new CreditoDebitoRefaturarVO();
						creditoDebitoVO.setCreditoDebitoARealizar(creditoDebitoARealizar);
						creditoDebitoVO.setIncluidoNaFatura(((dadosResumoFatura.getListaCreditoDebitoARealizar() != null)
								&& (dadosResumoFatura.getListaCreditoDebitoARealizar().contains(creditoDebitoARealizar))));

						BigDecimal valorCreditoDebito = creditoDebitoARealizar.getValor();
						if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
						}
						creditoDebitoVO.setSaldoCredito(valorCreditoDebito);

						listaCreditoDebitoRetorno.add(creditoDebitoVO);
					}
				}

				model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebitoRetorno);

				dadosResumoFatura = controladorFatura.obterDadosResumoFatura(dadosResumoFatura, listaCreditoDebito, Boolean.TRUE,
						ControladorApuracaoPenalidade.DISTRIBUICAO_FATURAMENTO, Boolean.FALSE, null, null);

				if (quantidadeCiclo != -1) {
					dadosResumoFatura.setQuantidadeCiclo(quantidadeCiclo);
				}

				model.addAttribute(DATA_EMISSAO,
						Util.converterDataParaStringSemHora(dadosResumoFatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
				model.addAttribute(DESCRICAO_MOTIVO_INCLUSAO, dadosResumoFatura.getMotivo());
				model.addAttribute(NUMERO_CONTRATO, dadosResumoFatura.getNumeroContrato());

				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);

				model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
				Collection<DadosGeraisItensFatura> listaDadosGeraisItens = dadosResumoFatura.getListaDadosGeraisItens();

				Collection<DadosItensFatura> listaDadosItensFatura =
						listaDadosGeraisItens.iterator().next().getListaDadosItensFaturaExibicao();

				Collections.sort((List<DadosItensFatura>) listaDadosItensFatura, new Comparator<DadosItensFatura>() {

					@Override
					public int compare(DadosItensFatura item1, DadosItensFatura item2) {

						int sequenciaDadas;
						if (item1.getCreditoDebitoARealizar() != null && item2.getCreditoDebitoARealizar() != null) {
							Date dataItem1 = item1.getCreditoDebitoARealizar().getUltimaAlteracao();
							Date dataItem2 = item2.getCreditoDebitoARealizar().getUltimaAlteracao();
							if (dataItem1.before(dataItem2)) {
								sequenciaDadas = -1;
							} else {
								if (dataItem1.after(dataItem2)) {
									sequenciaDadas = +1;
								} else {
									sequenciaDadas = 0;
								}
							}
						} else {
							sequenciaDadas = 0;
						}
						return sequenciaDadas;
					}
				});

				BigDecimal percentualParametro = new BigDecimal(CENTENA);
				percentualParametro = obterPercentualParametro(percentualParametro);

				BigDecimal valorCreditoDebitoTotal = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaSemDebito = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaComDesconto = BigDecimal.ZERO;
				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFaturaExibicao()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() == null && dadosItensFatura.getValorTotal() != null) {
							valorTotalFaturaSemDebito = valorTotalFaturaSemDebito.add(dadosItensFatura.getValorTotal());
						}
					}
				}
				valorTotalFaturaComDesconto = valorTotalFaturaSemDebito.multiply(percentualParametro.divide(new BigDecimal(CENTENA)));

				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					List<DadosItensFatura> listaItens = new ArrayList<>();
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFatura()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							BigDecimal valorCreditoDebito =
									controladorCreditoDebito.obterSaldoCreditoDebito(dadosItensFatura.getCreditoDebitoARealizar());
							if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) == 0
									&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) == 0)) {
								valorCreditoDebitoTotal = valorCreditoDebitoTotal.add(valorCreditoDebito);
								if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) < 0
										&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) < 0)) {
									dadosItensFatura.setValorTotalDisponivel(
											valorTotalFaturaComDesconto.subtract(valorCreditoDebitoTotal.subtract(valorCreditoDebito)));
									valorCreditoDebitoTotal = valorTotalFaturaComDesconto;
								}
							} else {
								dadosItensFatura.setValorTotalDisponivel(BigDecimal.ZERO);
							}
						}

						if (dadosItensFatura.getValorTotalDisponivel() == null) {
							dadosItensFatura.setValorTotalDisponivel(dadosItensFatura.getValorTotal());
						}
					}
					if (!listaItens.isEmpty()) {
						dadosGeraisItensFatura.setListaDadosItensFatura(listaItens);
						dadosGeraisItensFatura.setListaDadosItensFaturaExibicao(listaItens);
					}
					dadosGeraisItensFatura
							.setValorTotalFatura(dadosGeraisItensFatura.getValorTotalDebitos().subtract(valorCreditoDebitoTotal));
				}

				for (DadosGeraisItensFatura itens : listaDadosGeraisItens) {
					for (DadosTributoVO tributoVO : itens.getListaDadosTributoVO()) {
						tributoVO.setBaseCalculo(itens.getValorTotalFatura());
					}
				}

				model.addAttribute(LISTA_DADOS_GERAIS, listaDadosGeraisItens);
				model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());

				if ((dadosResumoFatura.getExibirAlerta() != null) && (dadosResumoFatura.getExibirAlerta())) {
					mensagemAlerta(model, ControladorFatura.ERRO_NEGOCIO_CREDITO_REMANESCENTE);
				}
			}

			model.addAttribute(LISTA_MOTIVO_INCLUSAO, controladorEntidadeConteudo.obterListaMotivoInclusao());
			saveToken(request);
		} catch (GGASException e) {
			if (e.getMessage() != null && "Existe(m) fatura(s) em aberto para o ponto de consumo.".equals(e.getMessage())) {
				e = new NegocioException("ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO");
			}
			mensagemErroParametrizado(model, request, e);
			tela = exibirInclusaoFatura(faturaVO, result, request, model);
		}

		return tela;
	}

	/**
	 * Exibir resumo fatura avulso.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirResumoFaturaAvulso")
	public String exibirResumoFaturaAvulso(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "exibirResumoFaturaAvulso";
		try {
			Boolean indicadorResumo = (Boolean) request.getSession().getAttribute(DADOS_INDICADOR_RESUMO);

			DadosResumoFatura dadosResumoFatura = null;

			if (indicadorResumo != null && indicadorResumo) {
				dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA_AUXILIAR);
			} else {
				dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
				request.getSession().setAttribute(DADOS_RESUMO_FATURA_AUXILIAR, dadosResumoFatura);
				request.getSession().setAttribute(DADOS_INDICADOR_RESUMO, Boolean.TRUE);
			}

			if (dadosResumoFatura != null) {
				if (faturaVO.getIdMotivoInclusao() != null) {
					dadosResumoFatura.setIdMotivo(faturaVO.getIdMotivoInclusao());
				}

				if (dadosResumoFatura.getCliente() != null) {
					model.addAttribute("cliente", controladorCliente.obter(dadosResumoFatura.getCliente().getChavePrimaria()));
				}

				for (HistoricoConsumo historicoConsumo : dadosResumoFatura.getListaHistoricoConsumo()) {
					if (historicoConsumo.isIndicadorConsumoCiclo()) {
						controladorHistoricoConsumo.validarPeriodicidadeHistoricoConsumo(historicoConsumo);
					}
				}

				List<Fatura> faturasPendentes = new ArrayList<Fatura>();
				if ((dadosResumoFatura.getListaFaturasPendentes() != null) && (!dadosResumoFatura.getListaFaturasPendentes().isEmpty())
						&& (faturaVO.getChavesFaturasPendentes() != null) && (faturaVO.getChavesFaturasPendentes().length > 0)) {

					// Adicionando as faturas
					// pendentes selecionadas.
					for (Fatura faturaPendente : dadosResumoFatura.getListaFaturasPendentes()) {
						if (ArrayUtils.contains(faturaVO.getChavesFaturasPendentes(), faturaPendente.getChavePrimaria())) {
							faturasPendentes.add(faturaPendente);
						}
					}
				}

				model.addAttribute(LISTA_FATURAS_PENDENTES, faturasPendentes);
				model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

				int quantidadeCiclo = -1;
				if (dadosResumoFatura.getQuantidadeCiclo() != null) {
					quantidadeCiclo = dadosResumoFatura.getQuantidadeCiclo();
				}

				List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();

				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				this.popularDadosLeituraConsumoFaturaAvulso(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao,
						dadosResumoFatura, request, model);

				controladorFatura.validarAvancarResumoInclusaoFatura(listaHistoricoMedicao);
				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));

				Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
				if ((faturaVO.getChavesCreditoDebito() != null) && (faturaVO.getChavesCreditoDebito().length > 0)) {

					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(CHAVES_CREDITO_DEBITO, faturaVO.getChavesCreditoDebito());

					if (dadosResumoFatura.getCliente() != null) {
						filtro.put(ID_CLIENTE, dadosResumoFatura.getCliente().getChavePrimaria());
					}
					if (faturaVO.getIdContratoEncerrarRescindir() == null) {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);
					} else {
						listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitosParaFaturaEncerramento(filtro);
					}
				}

				Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRetorno = new ArrayList<CreditoDebitoRefaturarVO>();

				if (listaCreditoDebito != null && !listaCreditoDebito.isEmpty()) {
					for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {

						CreditoDebitoRefaturarVO creditoDebitoVO = new CreditoDebitoRefaturarVO();
						creditoDebitoVO.setCreditoDebitoARealizar(creditoDebitoARealizar);
						creditoDebitoVO.setIncluidoNaFatura(((dadosResumoFatura.getListaCreditoDebitoARealizar() != null)
								&& (dadosResumoFatura.getListaCreditoDebitoARealizar().contains(creditoDebitoARealizar))));

						BigDecimal valorCreditoDebito = creditoDebitoARealizar.getValor();
						if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
						}
						creditoDebitoVO.setSaldoCredito(valorCreditoDebito);

						listaCreditoDebitoRetorno.add(creditoDebitoVO);
					}
				}

				model.addAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebitoRetorno);

				dadosResumoFatura = controladorFatura.obterDadosResumoFatura(dadosResumoFatura, listaCreditoDebito, Boolean.TRUE,
						ControladorApuracaoPenalidade.DISTRIBUICAO_FATURAMENTO, Boolean.FALSE, null, null);

				if (quantidadeCiclo != -1) {
					dadosResumoFatura.setQuantidadeCiclo(quantidadeCiclo);
				}

				model.addAttribute(DATA_EMISSAO,
						Util.converterDataParaStringSemHora(dadosResumoFatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
				model.addAttribute(DESCRICAO_MOTIVO_INCLUSAO, dadosResumoFatura.getMotivo());
				model.addAttribute(NUMERO_CONTRATO, dadosResumoFatura.getNumeroContrato());

				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);

				model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
				Collection<DadosGeraisItensFatura> listaDadosGeraisItens = dadosResumoFatura.getListaDadosGeraisItens();

				Collection<DadosItensFatura> listaDadosItensFatura =
						listaDadosGeraisItens.iterator().next().getListaDadosItensFaturaExibicao();

				Collections.sort((List<DadosItensFatura>) listaDadosItensFatura, new Comparator<DadosItensFatura>() {

					@Override
					public int compare(DadosItensFatura item1, DadosItensFatura item2) {

						int sequenciaDatas;
						if (item1.getCreditoDebitoARealizar() != null && item2.getCreditoDebitoARealizar() != null) {
							Date dataItem1 = item1.getCreditoDebitoARealizar().getUltimaAlteracao();
							Date dataItem2 = item2.getCreditoDebitoARealizar().getUltimaAlteracao();
							if (dataItem1.before(dataItem2)) {
								sequenciaDatas = -1;
							} else {
								if (dataItem1.after(dataItem2)) {
									sequenciaDatas = +1;
								} else {
									sequenciaDatas = 0;
								}
							}
						} else {
							sequenciaDatas = 0;
						}
						return sequenciaDatas;
					}
				});

				BigDecimal percentualParametro = new BigDecimal(CENTENA);
				percentualParametro = obterPercentualParametro(percentualParametro);
				BigDecimal valorCreditoDebitoTotal = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaSemDebito = BigDecimal.ZERO;
				BigDecimal valorTotalFaturaComDesconto = BigDecimal.ZERO;
				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFaturaExibicao()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() == null) {
							valorTotalFaturaSemDebito = valorTotalFaturaSemDebito.add(dadosItensFatura.getValorTotal());
						}
					}
				}
				valorTotalFaturaComDesconto = valorTotalFaturaSemDebito.multiply(percentualParametro.divide(new BigDecimal(CENTENA)));

				for (DadosGeraisItensFatura dadosGeraisItensFatura : listaDadosGeraisItens) {
					for (DadosItensFatura dadosItensFatura : dadosGeraisItensFatura.getListaDadosItensFaturaExibicao()) {
						if (dadosItensFatura.getCreditoDebitoARealizar() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
								&& dadosItensFatura.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							BigDecimal valorCreditoDebito =
									controladorCreditoDebito.obterSaldoCreditoDebito(dadosItensFatura.getCreditoDebitoARealizar());
							if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) == 0
									&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) == 0)) {
								valorCreditoDebitoTotal = valorCreditoDebitoTotal.add(valorCreditoDebito);
								if (!(valorCreditoDebito.compareTo(valorTotalFaturaComDesconto) < 0
										&& valorCreditoDebitoTotal.compareTo(valorTotalFaturaComDesconto) < 0)) {
									dadosItensFatura.setValorTotalDisponivel(
											valorTotalFaturaComDesconto.subtract(valorCreditoDebitoTotal.subtract(valorCreditoDebito)));
									valorCreditoDebitoTotal = valorTotalFaturaComDesconto;
								}
							} else {
								dadosItensFatura.setValorTotalDisponivel(BigDecimal.ZERO);
							}
						}

						if (dadosItensFatura.getValorTotalDisponivel() == null) {
							dadosItensFatura.setValorTotalDisponivel(dadosItensFatura.getValorTotal());
						}
					}
					dadosGeraisItensFatura
							.setValorTotalFatura(dadosGeraisItensFatura.getValorTotalDebitos().subtract(valorCreditoDebitoTotal));
				}
				for (DadosGeraisItensFatura itens : listaDadosGeraisItens) {
					for (DadosTributoVO tributoVO : itens.getListaDadosTributoVO()) {
						tributoVO.setBaseCalculo(itens.getValorTotalFatura());
					}
				}
				model.addAttribute(LISTA_DADOS_GERAIS, listaDadosGeraisItens);
				model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());

				if ((dadosResumoFatura.getExibirAlerta() != null) && (dadosResumoFatura.getExibirAlerta())) {
					mensagemAlerta(model, ControladorFatura.ERRO_NEGOCIO_CREDITO_REMANESCENTE, null);
				}
			}

			model.addAttribute(LISTA_MOTIVO_INCLUSAO, controladorEntidadeConteudo.obterListaMotivoInclusao());
			saveToken(request);
			salvarPesquisaFiltro(faturaVO, model);
		} catch (GGASException e) {
			if (e.getMessage() != null && "Existe(m) fatura(s) em aberto para o ponto de consumo.".equals(e.getMessage())) {
				e = new NegocioException("ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO");
			}
			tela = "exibirRefaturar";
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Exibir resumo refaturar fatura avulso.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 *
	 */
	@RequestMapping("exibirResumoRefaturarFaturaAvulso")
	public String exibirResumoRefaturarFaturaAvulso(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		String tela = "exibirResumoRefaturarFaturaAvulso";
		try {
			Boolean isFaturaAvulso = Boolean.TRUE;
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();
			if (dadosResumoFatura != null) {
				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				this.popularDadosLeituraConsumoFaturaAvulso(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao,
						dadosResumoFatura, request, null);
				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);
			}
			controladorFatura.consistirLeituraFaturamento(listaHistoricoMedicao, isFaturaAvulso);
			popularResumoRefaturar(faturaVO, request, model);
			tela = exibirPesquisaFatura(request, model);
		} catch (GGASException e) {
			if (e.getMessage() != null && "Existe(m) fatura(s) em aberto para o ponto de consumo.".equals(e.getMessage())) {
				e = new NegocioException("ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO");
			}
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de resumo do refaurar Fatura.
	 *
	 * @author gsantos
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws Exception {@link Exception}
	 */
	@RequestMapping("exibirResumoRefaturarFatura")
	public String exibirResumoRefaturarFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws Exception {

		String tela = "exibirResumoRefaturarFatura";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			popularResumoRefaturar(faturaVO, request, model);
		} catch (GGASException e) {
			if (e.getMessage() != null && "Existe(m) fatura(s) em aberto para o ponto de consumo.".equals(e.getMessage())) {
				e = new NegocioException("ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO");
			}
			tela = exibirRefaturar(faturaVO, result, request, model);
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Popular resumo refaturar.
	 *
	 */
	private void popularResumoRefaturar(FaturaPesquisaVO faturaVO, HttpServletRequest request, Model model) throws GGASException {

		DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

		if (dadosResumoFatura != null) {

			if ((faturaVO.getIdMotivoRefaturamento() != null) && (faturaVO.getIdMotivoRefaturamento() > 0)) {
				dadosResumoFatura.setIdMotivo(faturaVO.getIdMotivoRefaturamento());
			}

			if (dadosResumoFatura.getCliente() != null) {
				model.addAttribute("cliente",
						controladorCliente.obterCliente(dadosResumoFatura.getCliente().getChavePrimaria(), ENDERECOS));
			}

			// TODO ver depois onde esta validação
			// vai ficar.
			for (HistoricoConsumo historicoConsumo : dadosResumoFatura.getListaHistoricoConsumo()) {
				if (historicoConsumo.isIndicadorConsumoCiclo()) {
					controladorHistoricoConsumo.validarPeriodicidadeHistoricoConsumo(historicoConsumo);
				}
			}

			List<Fatura> faturasPendentes = new ArrayList<Fatura>();
			if ((dadosResumoFatura.getListaFaturasPendentes() != null) && (!dadosResumoFatura.getListaFaturasPendentes().isEmpty())
					&& (faturaVO.getChavesFaturasPendentes() != null) && (faturaVO.getChavesFaturasPendentes().length > 0)) {

				// Adicionando as faturas
				// pendentes selecionadas.
				for (Fatura faturaPendente : dadosResumoFatura.getListaFaturasPendentes()) {
					if (ArrayUtils.contains(faturaVO.getChavesFaturasPendentes(), faturaPendente.getChavePrimaria())) {
						faturasPendentes.add(faturaPendente);
					}
				}
			}
			model.addAttribute(LISTA_FATURAS_PENDENTES, faturasPendentes);

			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

			if (faturaVO.getChavesCreditoDebito() != null && faturaVO.getChavesCreditoDebito().length > 0) {
				Collection<CreditoDebitoARealizar> listaCreditoDebito =
						controladorCreditoDebito.consultarCreditosDebitosPorChaves(faturaVO.getChavesCreditoDebito());
				dadosResumoFatura.setListaCreditoDebitoARealizar(listaCreditoDebito);
			}

			dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));

			Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
			if ((faturaVO.getChavesCreditoDebito() != null) && (faturaVO.getChavesCreditoDebito().length > 0)) {

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(CHAVES_CREDITO_DEBITO, faturaVO.getChavesCreditoDebito());

				if (dadosResumoFatura.getCliente() != null) {
					filtro.put(ID_CLIENTE, dadosResumoFatura.getCliente().getChavePrimaria());
				}
				listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);
			}

			dadosResumoFatura = controladorFatura.obterDadosResumoFatura(dadosResumoFatura, listaCreditoDebito, Boolean.FALSE,
					ControladorApuracaoPenalidade.DISTRIBUICAO_FATURAMENTO, Boolean.FALSE, null, null);

			model.addAttribute(DATA_EMISSAO,
					Util.converterDataParaStringSemHora(dadosResumoFatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			model.addAttribute(DESCRICAO_MOTIVO_INCLUSAO, dadosResumoFatura.getMotivo());
			model.addAttribute(NUMERO_CONTRATO, dadosResumoFatura.getNumeroContrato());
			faturaVO.setNumeroCiclo(String.valueOf(dadosResumoFatura.getCiclo()));

			request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);
			model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
			// TODO ver se precisa os dois
			// atributos com a mesma lista.
			model.addAttribute(LISTA_DADOS_GERAIS, dadosResumoFatura.getListaDadosGeraisItens());
			model.addAttribute(ITENS_FATURA, dadosResumoFatura.getListaDadosGeraisItens());
			model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());
		}

		saveToken(request);
	}

	/**
	 * Método responsável por consistir a leitura dos pontos de consumo.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("consistirLeituraFaturamento")
	public String consistirLeituraFaturamento(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {
		String tela = request.getParameter("tela");
		try {
			salvarPesquisaFiltro(faturaVO, model);
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();
			if (dadosResumoFatura != null) {
				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				// se não for avulso... segue o fluxo senao preenche os dados da leitura
				popularDadosLeituraConsumoFatura(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao, dadosResumoFatura,
						request);

				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);
			}

			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				controladorHistoricoMedicao.validarDataAtualInformada(historicoMedicao);
			}

			controladorFatura.consistirLeituraFaturamento(listaHistoricoMedicao, false);

			request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			if ("exibirInclusaoFatura".equals(tela)) {
				tela = exibirInclusaoFatura(faturaVO, result, request, model);
			} else {
				tela = exibirInclusaoFaturaAvulso(faturaVO, result, request, model);
			}
		}

		return "forward:" + tela;
	}

	/**
	 * Método responsável por consistir a leitura dos pontos de consumo no fluxo de refaturar.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("consistirLeituraRefaturar")
	public String consistirLeituraRefaturar(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = request.getParameter("tela");
		try {
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			List<HistoricoMedicao> listaHistoricoMedicao = new ArrayList<HistoricoMedicao>();

			if (dadosResumoFatura != null) {
				Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura =
						dadosResumoFatura.getListaDadosLeituraConsumoFatura();
				this.popularDadosLeituraConsumoFatura(faturaVO, listaDadosLeituraConsumoFatura, listaHistoricoMedicao, dadosResumoFatura,
						request);
				request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumoFatura);
			}

			controladorFatura.consistirLeituraFaturamento(listaHistoricoMedicao, false);

			request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			if ("exibirRefaturar".equals(tela)) {
				tela = exibirRefaturar(faturaVO, result, request, model);
			} else {
				tela = exibirRefaturarAvulso(faturaVO, result, request, model);
			}
		}
		return "forward:" + tela;
	}

	/**
	 * Popular dados leitura consumo fatura avulso.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param listaDadosLeituraConsumoFatura {@link Collection}
	 * @param listaHistoricoMedicao {@link List}
	 * @param dadosResumoFatura {@link DadosResumoFatura}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularDadosLeituraConsumoFaturaAvulso(FaturaPesquisaVO faturaVO,
			Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura, List<HistoricoMedicao> listaHistoricoMedicao,
			DadosResumoFatura dadosResumoFatura, HttpServletRequest request, Model model) throws GGASException {

		try {
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

			String[] leituraAtu = new String[1];
			leituraAtu[0] = "";

			if ((listaDadosLeituraConsumoFatura != null) && (!listaDadosLeituraConsumoFatura.isEmpty())) {

				Date data = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

				String dataLeituraAtual = formatter.format(data);
				if (!StringUtils.isEmpty(dataLeituraAtual)) {
					Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_INFORMADA, dataLeituraAtual,
							Constantes.FORMATO_DATA_BR);
				}

				String[] dataLeituraArray = new String[1];
				dataLeituraArray[0] = dataLeituraAtual;
				faturaVO.setDataLeituraAtual(dataLeituraArray);
				Boolean[] indicadorLeituraCliente = new Boolean[1];
				indicadorLeituraCliente[0] = true;
				faturaVO.setIndicadorLeituraCliente(indicadorLeituraCliente);

				request.getSession().setAttribute(VOLUME_APURADO, faturaVO.getVolumeApurado());

				String[] volumeApuradoAnterior = new String[1];
				volumeApuradoAnterior[0] = "";
				faturaVO.setVolumeApurado(volumeApuradoAnterior);

				Integer anoMesReferencia = null;
				String ano = dataLeituraAtual.toString().substring(6, 10);
				String mes = dataLeituraAtual.toString().substring(3, 5);

				Integer ciclo = Integer.valueOf(1);

				anoMesReferencia = Integer.valueOf(ano + mes);

				faturaVO.setAnoMesFormatado(mes + "/" + ano + "-" + ciclo);
				faturaVO.setAnoMesReferencia(String.valueOf(anoMesReferencia));
				faturaVO.setNumeroCiclo(String.valueOf(ciclo));
				dadosResumoFatura.setCiclo(ciclo);
				dadosResumoFatura.setReferencia(anoMesReferencia);
				dadosResumoFatura.setFaturaAvulso(Boolean.TRUE);

				DadosLeituraConsumoFatura dadosLeituraConsumoFatura =
						((List<DadosLeituraConsumoFatura>) listaDadosLeituraConsumoFatura).get(0);

				dadosLeituraConsumoFatura.setIdHistoricoMedicao(0);

				popularDadosLeituraGeral(listaDadosLeituraConsumoFatura, listaHistoricoMedicao, dadosResumoFatura, dadosAuditoria,
						faturaVO);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
	}

	/**
	 * Popular dados leitura consumo fatura.
	 *
	 */
	private void popularDadosLeituraConsumoFatura(FaturaPesquisaVO faturaVO,
			Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura, List<HistoricoMedicao> listaHistoricoMedicao,
			DadosResumoFatura dadosResumoFatura, HttpServletRequest request) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		if ((listaDadosLeituraConsumoFatura != null) && (!listaDadosLeituraConsumoFatura.isEmpty())) {

			// 15/05/2014
			Integer anoMesReferencia = null;
			if (faturaVO.getIdAnoMesReferencia() != null && faturaVO.getIdAnoMesReferencia() > 0) {
				anoMesReferencia = faturaVO.getIdAnoMesReferencia();
				faturaVO.setAnoMesReferencia(String.valueOf(faturaVO.getIdAnoMesReferencia()));
			}

			if (anoMesReferencia != null) {
				String ano = anoMesReferencia.toString().substring(0, 4);
				String mes = anoMesReferencia.toString().substring(4);

				faturaVO.setAnoMesFormatado(mes + "/" + ano + "-" + faturaVO.getNumeroCiclo());
			} else {
				faturaVO.setAnoMesReferencia(dadosResumoFatura.getReferencia().toString());
				faturaVO.setNumeroCiclo(dadosResumoFatura.getCiclo().toString());
			}

			popularDadosLeituraGeral(listaDadosLeituraConsumoFatura, listaHistoricoMedicao, dadosResumoFatura, dadosAuditoria, faturaVO);
		}
	}

	/**
	 * Popular dados leitura geral.
	 *
	 */
	private void popularDadosLeituraGeral(Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura,
			List<HistoricoMedicao> listaHistoricoMedicao, DadosResumoFatura dadosResumoFatura, DadosAuditoria dadosAuditoria,
			FaturaPesquisaVO faturaVO) throws GGASException {

		Long idSituacaoRealizada =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_LEITURA_REALIZADA));
		SituacaoLeitura situacaoLeitura = controladorLeituraMovimento.obterSituacaoLeitura(idSituacaoRealizada);

		HistoricoMedicao historicoMedicao = null;
		for (int i = 0; i < listaDadosLeituraConsumoFatura.size(); i++) {

			DadosLeituraConsumoFatura dadosLeituraConsumoFatura = ((List<DadosLeituraConsumoFatura>) listaDadosLeituraConsumoFatura).get(i);

			Long idHistoricoMedicaoLocal = dadosLeituraConsumoFatura.getIdHistoricoMedicao();
			if (idHistoricoMedicaoLocal > 0) {
				historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao.obter(idHistoricoMedicaoLocal);
			} else {
				historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao.criar();
			}

			if (faturaVO.getNumeroCiclo() != null && !"".equals(faturaVO.getNumeroCiclo())) {
				historicoMedicao.setNumeroCiclo(Integer.parseInt(faturaVO.getNumeroCiclo()));
			} else {
				historicoMedicao.setNumeroCiclo(null);
			}
			if (faturaVO.getAnoMesReferencia() != null && !"".equals(faturaVO.getAnoMesReferencia())) {
				historicoMedicao.setAnoMesLeitura(Integer.valueOf(faturaVO.getAnoMesReferencia()));
			} else {
				historicoMedicao.setAnoMesLeitura(null);
			}
			historicoMedicao.setSequenciaLeitura(1);
			historicoMedicao.setDataProcessamento(Calendar.getInstance().getTime());
			historicoMedicao.setUltimaAlteracao(Calendar.getInstance().getTime());
			historicoMedicao.setHabilitado(Boolean.TRUE);
			historicoMedicao.setSituacaoLeitura(situacaoLeitura);

			Long idPontoConsumo = dadosLeituraConsumoFatura.getIdPontoConsumo();
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, INSTALACAO_MEDIDOR, IMOVEL,
					"imovel.quadraFace", "imovel.quadraFace.endereco", "imovel.quadraFace.endereco.cep");
			if (historicoMedicao.getPontoConsumo() == null) {
				historicoMedicao.setPontoConsumo(pontoConsumo);
			}

			if (pontoConsumo.getInstalacaoMedidor() != null && historicoMedicao.getHistoricoInstalacaoMedidor() == null) {
				historicoMedicao.setHistoricoInstalacaoMedidor(pontoConsumo.getInstalacaoMedidor());
			}

			boolean isMedicaoDiaria = dadosResumoFatura.isMedicaoDiaria();
			if (!isMedicaoDiaria) {
				// Obter a maior leitura do
				// ciclo anterior.
				HistoricoMedicao historicoMedicaoCicloAnterior =
						controladorHistoricoMedicao.obterUltimoHistoricoMedicao(idPontoConsumo, null, null, Boolean.TRUE);

				if (historicoMedicaoCicloAnterior != null) {
					historicoMedicao.setAnormalidadeLeituraAnterior(historicoMedicaoCicloAnterior.getAnormalidadeLeituraInformada());
					historicoMedicao.setNumeroLeituraAnterior(historicoMedicaoCicloAnterior.getNumeroLeituraInformada());
					historicoMedicao.setDataLeituraAnterior(historicoMedicaoCicloAnterior.getDataLeituraInformada());
					historicoMedicao.setCreditoSaldo(historicoMedicaoCicloAnterior.getCreditoVolume());

					BigDecimal saldoCredito = BigDecimal.ZERO;

					if (historicoMedicaoCicloAnterior.getCreditoVolume() != null) {
						saldoCredito = historicoMedicaoCicloAnterior.getCreditoVolume();

						HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
								.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicaoCicloAnterior.getChavePrimaria());
						if (historicoConsumo != null && historicoConsumo.getConsumoCredito() != null) {
							saldoCredito = saldoCredito.subtract(historicoConsumo.getConsumoCredito());
						}
					}

					historicoMedicao.setCreditoSaldo(saldoCredito);
				} else if (pontoConsumo.getInstalacaoMedidor() != null) {
					Date dataLeitura =
							controladorHistoricoMedicao.setarDataLeituraAnterior(pontoConsumo.getInstalacaoMedidor(), null, null);
					BigDecimal leitura = controladorHistoricoMedicao.setarLeituraAnterior(pontoConsumo.getInstalacaoMedidor(), null, null);

					historicoMedicao.setNumeroLeituraAnterior(leitura);
					historicoMedicao.setDataLeituraAnterior(dataLeitura);
				}
			}

			if (faturaVO.getLeituraAtual() != null && faturaVO.getLeituraAtual().length > 0) {

				String numeroLeituraInformada = String.valueOf(faturaVO.getLeituraAtual()[i]);
				BigDecimal numeroLeituraAtual = null;
				if (!StringUtils.isEmpty(numeroLeituraInformada)) {
					numeroLeituraAtual = new BigDecimal(numeroLeituraInformada);
							
							/*Util.converterCampoStringParaValorBigDecimal(
							HistoricoMedicao.NUMERO_LEITURA_INFORMADA, numeroLeituraInformada,
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);*/
				}
				dadosLeituraConsumoFatura.setLeituraAtual(numeroLeituraAtual);
				historicoMedicao.setNumeroLeituraInformada(numeroLeituraAtual);
			}

			if (faturaVO.getDataLeituraAtual() != null && faturaVO.getDataLeituraAtual().length > 0) {
				String dataLeituraAtual = faturaVO.getDataLeituraAtual()[i];
				Date dataAtualLeitura = null;
				if (!StringUtils.isEmpty(dataLeituraAtual)) {
					dataAtualLeitura = Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_INFORMADA, dataLeituraAtual,
							Constantes.FORMATO_DATA_BR);
				}
				dadosLeituraConsumoFatura.setDataLeituraAtual(dataAtualLeitura);
				historicoMedicao.setDataLeituraInformada(dataAtualLeitura);
			}

			if ((faturaVO.getIndicadorLeituraCliente() != null && faturaVO.getIndicadorLeituraCliente().length > 0)
					&& (faturaVO.getIndicadorLeituraCliente()[i] != null)) {
				dadosLeituraConsumoFatura.setInformadoCliente(faturaVO.getIndicadorLeituraCliente()[i]);
				historicoMedicao.setIndicadorInformadoCliente(faturaVO.getIndicadorLeituraCliente()[i]);

				if (faturaVO.getIndicadorLeituraCliente()[i] != null && !faturaVO.getIndicadorLeituraCliente()[i]) {
					Long chaveLeiturista = (faturaVO.getIdLeiturista().length > 0) ? faturaVO.getIdLeiturista()[i] : 0L;
					Leiturista leiturista = null;

					if ((chaveLeiturista != null) && (chaveLeiturista > 0)) {
						leiturista = controladorRota.obterLeiturista(chaveLeiturista);
					}
					dadosLeituraConsumoFatura.setLeiturista(leiturista);
					historicoMedicao.setLeiturista(leiturista);
				}
			}
			if (faturaVO.getConsumoApurado() != null && faturaVO.getConsumoApurado().length > 0) {
				String consumo = faturaVO.getConsumoApurado()[i];
				String consumoAnterior = "";
				if (faturaVO.getConsumoApuradoAnterior() != null && faturaVO.getConsumoApuradoAnterior().length > 0) {
					consumoAnterior = faturaVO.getConsumoApuradoAnterior()[i];
				}

				BigDecimal valorConsumoApurado = null;
				if (!StringUtils.isEmpty(consumo)) {
					valorConsumoApurado = Util.converterCampoStringParaValorBigDecimal(HistoricoMedicao.CONSUMO_INFORMADO, consumo,
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
				}

				BigDecimal valorConsumoApuradoAnterior = null;
				if (!StringUtils.isEmpty(consumoAnterior)) {
					valorConsumoApuradoAnterior = Util.converterCampoStringParaValorBigDecimal(HistoricoMedicao.CONSUMO_INFORMADO,
							consumoAnterior, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
				}

				dadosLeituraConsumoFatura.setConsumoApurado(valorConsumoApurado);

				if (valorConsumoApurado != null) {
					if (valorConsumoApuradoAnterior != null && valorConsumoApuradoAnterior.compareTo(valorConsumoApurado) != 0) {
						historicoMedicao.setConsumoInformado(valorConsumoApurado);
					} else {
						historicoMedicao.setConsumoInformado(null);
					}
				} else {
					historicoMedicao.setConsumoInformado(null);
				}
			}

			historicoMedicao.setDadosAuditoria(dadosAuditoria);
			listaHistoricoMedicao.add(historicoMedicao);
		}
	}

	/**
	 * Método responsável por exibir a lista de histórico de consumo de um ponto de consumo.
	 * 
	 * @param faturaVo {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("carregarHistoricoConsumoFatura")
	public String carregarHistoricoConsumoFatura(FaturaPesquisaVO faturaVo, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		if ((faturaVo.getIdPontoConsumo() != null) && (faturaVo.getIdPontoConsumo() > 0)
				&& (!StringUtils.isEmpty(faturaVo.getAnoMesReferencia())) && (!StringUtils.isEmpty(faturaVo.getNumeroCiclo()))) {

			Integer numeroCiclo = null;
			if (!StringUtils.isEmpty(faturaVo.getNumeroCiclo())) {
				numeroCiclo = Util.converterCampoStringParaValorInteger(HistoricoMedicao.NUMERO_CICLO, faturaVo.getNumeroCiclo());
			}
			Integer anoMesReferencia = null;
			if (!StringUtils.isEmpty(faturaVo.getAnoMesReferencia())) {
				String anoMes = faturaVo.getAnoMesReferencia().substring(0, INDICE_MES_FIM);
				anoMes = faturaVo.getAnoMesReferencia().substring(3) + anoMes;
				anoMesReferencia = Util.converterCampoStringParaValorInteger(HistoricoMedicao.ANO_MES_LEITURA, anoMes);
			}

			if ((numeroCiclo != null) && (anoMesReferencia != null)) {
				DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

				if (dadosResumoFatura != null) {
					Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumo(
							faturaVo.getIdPontoConsumo(), anoMesReferencia, numeroCiclo, !dadosResumoFatura.isMedicaoDiaria(), null, null);
					model.addAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);
				}
			}
		}

		return exibirInclusaoFatura(faturaVo, result, request, model);
	}

	/**
	 * Exibir refaturar avulso.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirRefaturarAvulso")
	public String exibirRefaturarAvulso(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "exibirRefaturarAvulso";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			montarObjetoRefaturar(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = exibirPesquisa(faturaVO, result, request, model);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de refaturar.
	 *
	 * @author gsantos
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirRefaturar")
	public String exibirRefaturar(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "exibirRefaturar";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			montarObjetoRefaturar(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = exibirPesquisa(faturaVO, result, request, model);
		}

		return tela;
	}

	/**
	 * Montar objeto refaturar.
	 *
	 */
	private void montarObjetoRefaturar(FaturaPesquisaVO faturaVO, HttpServletRequest request, Model model) throws GGASException {

		DadosResumoFatura dadosResumo = null;
		Long idFatura = null;
		if (faturaVO.getIdCliente() != null) {
			faturaVO.setCodigoCliente(String.valueOf(faturaVO.getIdCliente()));
		}
		if (faturaVO.getChavesPrimarias() == null || faturaVO.getChavesPrimarias().length <= 0) {
			dadosResumo = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			if (dadosResumo != null) {
				faturaVO.setChavesPrimarias(new Long[] { dadosResumo.getIdFatura() });
				idFatura = dadosResumo.getIdFatura();
			}
		} else {
			controladorFatura.validarFaturasParaRefaturar(faturaVO.getChavesPrimarias());

			if (faturaVO.getChavesPrimarias().length == 1) {
				dadosResumo = controladorFatura.obterDadosRefaturarFatura(faturaVO.getChavesPrimarias()[0]);
				idFatura = faturaVO.getChavesPrimarias()[0];
			} else {
				// TODO - Em caso de refaturamento
				// em lote.
			}
		}

		if (dadosResumo != null) {
			faturaVO.setAnoMesFormatado(Util.formatarAnoMes(dadosResumo.getReferencia()));
			faturaVO.setNumeroCiclo(String.valueOf(dadosResumo.getCiclo()));
		}

		if ((dadosResumo != null) && (dadosResumo.getListaDadosGeraisItens() == null || dadosResumo.getListaDadosGeraisItens().isEmpty())) {
			dadosResumo.setListaDadosLeituraConsumoFatura(new ArrayList<DadosLeituraConsumoFatura>());
			dadosResumo.setListaHistoricoConsumo(new ArrayList<HistoricoConsumo>());
		}

		if (dadosResumo != null) {
			request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumo);
			model.addAttribute(ITENS_FATURA, dadosResumo.getListaDadosGeraisItens());
			model.addAttribute(LISTA_LEITURA_CONSUMO_FATURA, dadosResumo.getListaDadosLeituraConsumoFatura());
		}
		model.addAttribute("listaMotivoRefaturar", controladorEntidadeConteudo.obterListaMotivoRefaturar());

		if (dadosResumo != null && dadosResumo.getListaPontoConsumo() != null && !dadosResumo.getListaPontoConsumo().isEmpty()) {
			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumo.getListaPontoConsumo());
		}

		if (dadosResumo != null && dadosResumo.getCliente() != null && dadosResumo.getCliente().getChavePrimaria() > 0) {
			model.addAttribute("cliente", controladorCliente.obterCliente(dadosResumo.getCliente().getChavePrimaria(), ENDERECOS));
		}

		// Se não for faturamento agrupado só haverá um ponto de consumo, o que será
		// mostra acima das abas.
		boolean indicadorFaturamentoAgrupado = false;
		if (dadosResumo != null) {
			indicadorFaturamentoAgrupado = dadosResumo.isFaturamentoAgrupado();
		}
		if ((dadosResumo != null && dadosResumo.getListaPontoConsumo() != null) && (!dadosResumo.getListaPontoConsumo().isEmpty())) {
			//faturaVO.setIdCliente(null);

			if (!indicadorFaturamentoAgrupado) {
				Collection<Leiturista> listaLeiturista = controladorRota.listarLeituristas();
				model.addAttribute(LISTA_LEITURISTA, listaLeiturista);
			}
		}

		if ((dadosResumo != null && dadosResumo.getListaPontoConsumo() != null) && (!dadosResumo.getListaPontoConsumo().isEmpty())
				&& (dadosResumo.getListaPontoConsumo().size() == 1)) {
			PontoConsumo pontoConsumo = dadosResumo.getListaPontoConsumo().get(0);
			model.addAttribute("pontoConsumo", pontoConsumo);
		}

		if (dadosResumo != null && dadosResumo.getCiclo() != null) {
			faturaVO.setNumeroCiclo(String.valueOf(dadosResumo.getCiclo()));
		}

		if (dadosResumo != null && dadosResumo.getIdMotivo() > 0 && faturaVO.getIdMotivoRefaturamento() == null) {
			faturaVO.setIdMotivoRefaturamento(dadosResumo.getIdMotivo());
		}

		Collection<CreditoDebitoARealizar> listaCreditoDebitoFatura = null;
		Collection<CreditoDebitoARealizar> listaCreditoDebitoTotal = null;

		// Montar objeto contendo informações dos créditos e débitos e um boleano
		// informando se faz parte da fatura ou não.
		if (dadosResumo != null) {
			listaCreditoDebitoFatura = dadosResumo.getListaCreditoDebitoARealizar();
			listaCreditoDebitoTotal = dadosResumo.getListaCreditoDebitoARealizarTotal();
			faturaVO.setChavesCreditoDebito(Util.collectionParaArrayChavesPrimarias(listaCreditoDebitoFatura));
		}

		Collection<CreditoDebitoRefaturarVO> listaCreditoDebitoRefaturarVO = new ArrayList<CreditoDebitoRefaturarVO>();

		// Verifica o tipo de financiamento da fatura
		String paramFinancTipoProduto = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PRODUTO);
		FinanciamentoTipo tipoFinancFatura = controladorRubrica.obterTipoFinanciamento(Long.parseLong(paramFinancTipoProduto));
		if (dadosResumo != null) {
			for (CreditoDebitoARealizar creditoDebito : dadosResumo.getListaCreditoDebitoARealizar()) {
				if (creditoDebito.getCreditoDebitoNegociado() != null
						&& creditoDebito.getCreditoDebitoNegociado().getFinanciamentoTipo() != null) {
					tipoFinancFatura = creditoDebito.getCreditoDebitoNegociado().getFinanciamentoTipo();
				}
			}
		}

		if (listaCreditoDebitoTotal != null) {
			for (CreditoDebitoARealizar creditoDebito : listaCreditoDebitoTotal) {

				if (creditoDebito.getCreditoDebitoNegociado() != null
						&& creditoDebito.getCreditoDebitoNegociado().getFinanciamentoTipo() != null
						&& creditoDebito.getCreditoDebitoNegociado().getFinanciamentoTipo().getChavePrimaria() == tipoFinancFatura
								.getChavePrimaria()) {
					CreditoDebitoRefaturarVO creditoDebitoRefaturarVO = new CreditoDebitoRefaturarVO();
					creditoDebitoRefaturarVO.setCreditoDebitoARealizar(creditoDebito);

					BigDecimal valorCreditoDebito = creditoDebito.getValor();
					BigDecimal valorLancado = BigDecimal.ZERO;

					if (listaCreditoDebitoFatura.contains(creditoDebito)) {

						if (creditoDebito.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							valorLancado = controladorCreditoDebito.obterValorCobradoCredito(creditoDebito.getChavePrimaria(), idFatura);
							valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebito);
							valorCreditoDebito = valorCreditoDebito.add(valorLancado);
						}
						creditoDebitoRefaturarVO.setIncluidoNaFatura(Boolean.TRUE);
					} else {
						if (creditoDebito.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
							valorCreditoDebito = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebito);
						}
						creditoDebitoRefaturarVO.setIncluidoNaFatura(Boolean.FALSE);
					}

					creditoDebitoRefaturarVO.setSaldoCredito(valorCreditoDebito);
					creditoDebitoRefaturarVO.setValorLancado(valorLancado);
					listaCreditoDebitoRefaturarVO.add(creditoDebitoRefaturarVO);
				}
			}
		}

		model.addAttribute("listaCreditoDebito", listaCreditoDebitoRefaturarVO);
		model.addAttribute("isRefaturar", Boolean.TRUE);

		request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumo);
	}

	/**
	 * Refaturar fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("refaturarFatura")
	public String refaturarFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			salvarPesquisaFiltro(faturaVO, model);
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

			if (dadosResumoFatura != null) {
				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));
			}

			// Método que irá cancelar a fatura e
			// criar uma nova.
			Collection<Fatura> listaFaturaRefaturada =
					controladorFatura.refaturarFatura(dadosResumoFatura, this.getDadosAuditoria(request));

			List<byte[]> retornoRelatorioAgrupado = new ArrayList<byte[]>();

			Boolean ultimaFatura = Boolean.TRUE;
			BigDecimal acumuladoFaturas = BigDecimal.ZERO;

			Iterator<Fatura> iterator = listaFaturaRefaturada.iterator();
			while (iterator.hasNext()) {
				Fatura faturaRefaturada = iterator.next();

				// emissão agrupada
				if (faturaRefaturada.getContratoAtual().getAgrupamentoConta().equals(Boolean.TRUE)) {
					// caso não seja uma fatura filha.
					if (faturaRefaturada.getFaturaAgrupada() == null) {
						if (iterator.hasNext()) {
							ultimaFatura = Boolean.FALSE;
						} else {
							ultimaFatura = Boolean.TRUE;
						}

						retornoRelatorioAgrupado.add(controladorFatura.gerarEmissaoFatura(faturaRefaturada, null, null, null,
								getDadosAuditoria(request), ultimaFatura, acumuladoFaturas, null, ultimaFatura));
						acumuladoFaturas = acumuladoFaturas.add(faturaRefaturada.getValorTotal());
					}
					// emissão normal
				} else {
					// caso não seja uma fatura filha.
					if (faturaRefaturada.getFaturaAgrupada() == null) {
						retornoRelatorioAgrupado.add(controladorFatura.gerarEmissaoFatura(faturaRefaturada, null, null, null,
								getDadosAuditoria(request), Boolean.TRUE, acumuladoFaturas, null, ultimaFatura));
					}
				}
			}

			if (!retornoRelatorioAgrupado.isEmpty()) {
				byte[] retornoRelatorio = RelatorioUtil.unificarRelatoriosPdf(retornoRelatorioAgrupado);

				if (retornoRelatorio != null) {
					model.addAttribute(GERAR_RELATORIO_EMISSAO_FATURA, true);
					request.getSession().setAttribute(RELATORIO_EMISSAO_FATURA, retornoRelatorio);
				}
			}

			request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
			request.getSession().removeAttribute(COLECAO_ITENS_FATURAMENTO);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Fatura.FATURA_ROTULO);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaFatura";
	}

	/**
	 * Gerar relatorio nota debito credito.
	 *
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws IOException {@link IOException}
	 */
	@RequestMapping("gerarRelatorioNotaDebitoCreditoFatura")
	public String gerarRelatorioNotaDebitoCredito(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, Model model) throws IOException {

		byte[] relatorioNotaDebitoCredito = (byte[]) request.getSession().getAttribute(RELATORIO_NOTA_DEBITO_CREDITO);
		request.removeAttribute(GERAR_RELATORIO_NOTA_DEBITO_CREDITO);
		request.getSession().removeAttribute(RELATORIO_NOTA_DEBITO_CREDITO);

		if (relatorioNotaDebitoCredito != null) {
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(relatorioNotaDebitoCredito.length);
			response.addHeader("Content-Disposition", "attachment; filename=relatorioNotaDebitoCredito.pdf");
			servletOutputStream.write(relatorioNotaDebitoCredito, 0, relatorioNotaDebitoCredito.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}

		return null;
	}

	// TODO: Incluir fatura de encerramento.

	/**
	 * Método responsável por exibir a tela de incluir fatura de encerramento.
	 *
	 * @param mapping the mapping
	 * @param form O formulário
	 * @param request O objeto request
	 * @param response O objeto response
	 * @return ActionForward O retorno da ação
	 * @throws Exception Caso ocorra algum erro public ActionForward exibirIncluirFaturaEncerramentoContrato (ActionMapping mapping,
	 *             ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception { DynaActionForm dynaForm =
	 *             (DynaActionForm) form; Long chavePrimaria = (Long) dynaForm.get("chavePrimaria"); String dataVencimento = (String)
	 *             dynaForm.get("dataVencimento"); DadosAuditoria dadosAuditoria = this .getDadosAuditoria(request); Contrato selecionado =
	 *             fachada. obterContratoListasCarregadas (chavePrimaria); selecionado. setClienteAssinatura (fachada.obterCliente
	 *             (selecionado .getClienteAssinatura ().getChavePrimaria(), "enderecoPrincipal")); selecionado.setDadosAuditoria(
	 *             dadosAuditoria); Contrato novo = fachada.popularAditarContrato (selecionado, dadosAuditoria); // Contrato novo =
	 *             (Contrato)ServiceLocator .getInstancia ().getBeanPorID(Contrato .BEAN_ID_CONTRATO); // ConvertUtils.register(new
	 *             BigDecimalConverter(null), java.math.BigDecimal.class); // Util.copyProperties(novo, selecionado); //
	 *             novo.setChavePrimaria(0); // novo.setDadosAuditoria( dadosAuditoria); // novo. setListaContratoPontoConsumo (null); //
	 *             novo.setListaContratoQDC(null); // novo.setListaContratoCliente( null); // Atualizando data de recisão do contrato String
	 *             periodoRecisao = request.getParameter( "periodoRecisaoCampo"); if(periodoRecisao != null && !periodoRecisao.equals("")){
	 *             novo.setDataRecisao(Util. converterCampoStringParaData ("", periodoRecisao, FORMATO_DATA_BR)); }else{
	 *             novo.setDataRecisao(Util. zerarHorario(new DateTime()).toDate()); } // Atualizando situação do contrato SituacaoContrato
	 *             encerrado = fachada.obterSituacaoContrato( SituacaoContrato.ENCERRADO); novo.setSituacao(encerrado); String
	 *             valorIndenizacao = request.getParameter( "valorIndenizacao"); if(valorIndenizacao != null &&
	 *             !valorIndenizacao.equals("")){ // Cirando rubrica de débito no valor da multa recisória do contrato (caso haja) e
	 *             cancelando o // débito anterior de multa recisória para o cliente desse contrato (caso haja). Fatura notaDebitoCredito =
	 *             fachada.criarFatura(); fachada.validarDataVencimento( Util .converterCampoStringParaData ("", dataVencimento,
	 *             FORMATO_DATA_BR)); this.popularNotaDebitoCredito( notaDebitoCredito ,selecionado,new BigDecimal(valorIndenizacao),
	 *             dataVencimento); Long[] chavesPrimarias = (Long[]) dynaForm.get(CHAVES_PRIMARIAS); Object[] retornoInsercaoNota = fachada
	 *             .inserirNotaDebitoCredito (notaDebitoCredito, chavesPrimarias); Long chavePrimaria1 = (Long) retornoInsercaoNota[1];
	 *             dynaForm.set(CHAVE_PRIMARIA, chavePrimaria1); byte[] relatorioNotaDebitoCredito = (byte[]) retornoInsercaoNota[0]; if
	 *             (relatorioNotaDebitoCredito != null) { request.getSession(). setAttribute (RELATORIO_NOTA_DEBITO_CREDITO,
	 *             relatorioNotaDebitoCredito); request.setAttribute( GERAR_RELATORIO_NOTA_DEBITO_CREDITO , "true"); //
	 *             gerarRelatorioNotaDebitoCredito (mapping, form, request, response); } }else{ // Cancelando o débito anterior de multa
	 *             recisória para o cliente desse contrato (caso haja). Long idRubricaMultaRecisoria = Long.valueOf((String) fachada.
	 *             obterValorDoParametroPorCodigo( Constantes. PARAMETRO_CODIGO_RUBRICA_MULTA_RECISORIA )); Rubrica rubrica =
	 *             fachada.obterRubrica (idRubricaMultaRecisoria); fachada. cancelarCreditoDebitoAnteriorPorClienteRubrica (selecionado.
	 *             getClienteAssinatura(), rubrica, dadosAuditoria, false); } if(!novo.getDataRecisao(). equals
	 *             (selecionado.getDataRecisao()) || !novo.getSituacao().equals( selecionado.getSituacao())){ if (selecionado.
	 *             getChavePrimariaPai() == null ) { novo.setChavePrimariaPai( selecionado .getChavePrimaria()); }else{
	 *             novo.setChavePrimariaPai( selecionado .getChavePrimariaPai()); } fachada.alterarContrato(novo, selecionado); }
	 *             Collection<ContratoPontoConsumo > pontosContrato = selecionado. getListaContratoPontoConsumo(); Collection<PontoConsumo>
	 *             pontos = new ArrayList<PontoConsumo>(); for (ContratoPontoConsumo contratoPontoConsumo: pontosContrato) {
	 *             pontos.add(fachada. buscarPontoConsumoPorChave (contratoPontoConsumo .getPontoConsumo ().getChavePrimaria(), "imovel"));
	 *             } this.popularFormContrato( dynaForm, selecionado); this.popularFormCliente( dynaForm, selecionado.
	 *             getClienteAssinatura()); request.setAttribute( ContratoAction .SITUACAO_CONTRATO, selecionado.getSituacao());
	 *             request.setAttribute( ContratoAction .LISTA_PONTO_CONSUMO, pontos); super.exibirMensagem(request, Constantes
	 *             .SUCESSO_ENTIDADE_INCLUIDA, Fatura.NOTA_DEBITO_CREDITO); //return new NotaDebitoCreditoAction
	 *             ().pesquisarNotaDebitoCredito (mapping, dynaForm, request, response); return mapping.findForward( Constantes
	 *             .RETORNO_PESQUISAR_CONTRATO); }
	 */

	/**
	 * Método responsável por incluir a fatura de encerramento.
	 *
	 */
	/*
	 * public ActionForward incluirFaturaEncerramentoContrato(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	 * HttpServletResponse response) throws Exception {
	 * 
	 * return mapping.findForward(FORWARD_SUCESSO_CONTRATO); }
	 */

	/**
	 * Método responsável por incluir uma Fatura de Encerramento.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	@RequestMapping("incluirFaturaEncerramento")
	public String incluirFaturaEncerramento(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException, IllegalAccessException, InvocationTargetException {

		String tela = request.getParameter("tela");
		try {
			validarToken(request);
			Fatura fatura = (Fatura) controladorFatura.criar();
			fatura.setDadosAuditoria(getDadosAuditoria(request));
			fatura.setHabilitado(true);
			fatura.setEncerramento(true);

			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			if (dadosResumoFatura != null) {
				dadosResumoFatura.setDadosAuditoria(getDadosAuditoria(request));
			}

			if (dadosResumoFatura != null && (faturaVO.getIndicadorFaturasPendentes() != null && faturaVO.getIndicadorFaturasPendentes())
					&& (faturaVO.getChavesFaturasPendentes() != null) && (faturaVO.getChavesFaturasPendentes().length > 0)) {
				dadosResumoFatura.setChavesFaturasPendentes(faturaVO.getChavesFaturasPendentes());
			} else if (dadosResumoFatura != null) {
				dadosResumoFatura.setChavesFaturasPendentes(null);
			}

			Collection<Fatura> listaFatura = controladorFatura.processarInclusaoFatura(dadosResumoFatura, Boolean.TRUE, Boolean.FALSE,
					this.getDadosAuditoria(request));

			byte[] retornoRelatorio = null;
			retornoRelatorio = controladorFatura.processarDocumentoRetornoFatura(getDadosAuditoria(request), listaFatura);

			if (retornoRelatorio != null) {
				model.addAttribute(GERAR_RELATORIO_EMISSAO_FATURA, true);
				request.getSession().setAttribute(RELATORIO_EMISSAO_FATURA, retornoRelatorio);
			}

			request.getSession().removeAttribute(DADOS_RESUMO_FATURA);
			request.getSession().removeAttribute(COLECAO_ITENS_FATURAMENTO);
			request.getSession().removeAttribute(DADOS_RESUMO_FATURA_AUXILIAR);
			request.getSession().removeAttribute(DADOS_INDICADOR_RESUMO);

			faturaVO.setIdPontoConsumo(null);

			request.getSession().setAttribute(ID_CONTRATO_ENCERRAR_RESCINDIR, faturaVO.getIdContratoEncerrarRescindir());
			request.getSession().setAttribute(LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS, LISTA_IDS_PONTO_CONSUMO_REMOVIDOS_AGRUPADOS);
			
			
			ContratoAction contrato = new ContratoAction();
			ContratoVO contratoVO = new ContratoVO();
			
			contratoVO.setChavePrimaria(dadosResumoFatura.getContrato().getChavePrimaria());
			contratoVO.setChamadoTela(Boolean.FALSE);
			
			tela = contrato.exibirPontoConsumoContrato(contratoVO, result, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de incluir NFD.
	 *
	 * @author wcosta
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirIncluirNFDFatura")
	public String exibirIncluirNFDFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "exibirIncluirNFDFatura";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			DadosResumoFatura dadosResumo = null;
			DocumentoFiscal documentoFiscal = null;
			Fatura fatura = null;

			if (faturaVO.getChavesPrimarias() == null || faturaVO.getChavesPrimarias().length <= 0) {
				dadosResumo = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
				if (dadosResumo != null) {
					faturaVO.setIdFatura(dadosResumo.getIdFatura());
				}
			} else {
				if (faturaVO.getChavesPrimarias().length == 1) {
					dadosResumo = controladorFatura.obterDadosIncluirNFDFatura(faturaVO.getChavesPrimarias()[0]);
					fatura = (Fatura) controladorCobranca.obterFatura(faturaVO.getChavesPrimarias()[0], "contratoAtual", "cliente",
							"creditoDebitoSituacao");

					if (fatura.getCreditoDebitoSituacao() == null) {
						documentoFiscal =
								controladorFatura.obterDocumentoFiscalPorCodigoFatura(faturaVO.getChavesPrimarias()[0], Boolean.FALSE);
					} else {
						documentoFiscal = controladorFatura.obterDocumentoFiscalPorCodigoFatura(faturaVO.getChavesPrimarias()[0],
								fatura.getCreditoDebitoSituacao().isValido());
					}
					faturaVO.setIdDocumentoFiscal(documentoFiscal.getChavePrimaria());
					documentoFiscal = controladorFatura.obterDocumentoFiscalPorChave(documentoFiscal.getChavePrimaria());
				}
			}

			model.addAttribute(LISTA_MOTIVO_CANCELAMENTO, controladorEntidadeConteudo.obterListaMotivoCancelamento());

			DadosResumoFatura dadosResumoFatura =
					controladorFatura.obterDadosDetalhamentoFatura(faturaVO.getChavesPrimarias()[0], faturaVO.getIdDocumentoFiscal());

			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

			model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
			model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());
			model.addAttribute(LISTA_DADOS_GERAIS, dadosResumoFatura.getListaDadosGeraisItens());

			if (fatura != null && fatura.getContratoAtual() != null) {
				faturaVO.setNumeroContrato(fatura.getContratoAtual().getNumeroCompletoContrato());
			}

			if (fatura != null) {
				faturaVO.setIdCliente(fatura.getCliente().getChavePrimaria());
				model.addAttribute("cliete", controladorCliente.obter(fatura.getCliente().getChavePrimaria()));
			}

			if (documentoFiscal != null) {
				faturaVO.setTipoNota(documentoFiscal.getTipoFaturamento().getDescricao());
			}

			faturaVO.setIsValidarFiltroPesquisaFatura(Boolean.FALSE);
			if (fatura != null) {
				faturaVO.setIdFatura(fatura.getChavePrimaria());
			}
			if (faturaVO.getIndicadorTipoNotaNormalEletronica() == null) {
				faturaVO.setIndicadorTipoNotaNormalEletronica(0);
			}

			request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumo);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			tela = pesquisarFatura(faturaVO, request, model);
		}

		return tela;

	}

	/**
	 * Incluir nfd fatura.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirNFDFatura")
	public String incluirNFDFatura(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "forward:/exibirIncluirNFDFatura";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
			DocumentoFiscal documentoFiscal =
					(DocumentoFiscal) ServiceLocator.getInstancia().getBeanPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);

			faturaVO.setIsValidarFiltroPesquisaFatura(Boolean.FALSE);

			if (faturaVO.getNumeroNotaDevolucao() != null && faturaVO.getNumeroNotaDevolucao() > 0) {
				documentoFiscal.setNumero(faturaVO.getNumeroNotaDevolucao());
			}
			documentoFiscal.setChaveAcesso(faturaVO.getChaveAcesso());
			documentoFiscal.setDescricaoSerie(faturaVO.getNumeroSerie());
			if (faturaVO.getDataDevolucao() != null && !"".equals(faturaVO.getDataDevolucao().trim())) {
				documentoFiscal.setDataEmissao(
						Util.converterCampoStringParaData(DATA_DEVOLUCAO, faturaVO.getDataDevolucao(), Constantes.FORMATO_DATA_BR));
			}
			documentoFiscal.setNumeroProtocolo(faturaVO.getNumeroProtocoloDevolucao());
			documentoFiscal.setMensagem(faturaVO.getObservacoesDevolucao());
			documentoFiscal.setIndicadorTipoNota(faturaVO.getIndicadorTipoNotaNormalEletronica());
			documentoFiscal.setEmissaoCliente(faturaVO.getEmissaoCliente());

			controladorFatura.inserirNotaFiscalDevolucao(documentoFiscal, dadosResumoFatura, faturaVO.getIdFatura(),
					faturaVO.getIdMotivoCancelamento(), faturaVO.getConsumoItem(), faturaVO.getVlrUnitarioItem(), dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Fatura.FATURA_NOTA_DEVOLUCAO);
			tela = pesquisarFatura(faturaVO, request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	/**
	 * Método responsável por exibir a tela de incluir fatura complementar.
	 *
	 * @author wcosta
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirIncluirFaturaComplementar")
	public String exibirIncluirFaturaComplementar(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws NegocioException {

		String tela = "exibirIncluirFaturaComplementar";
		DadosResumoFatura dadosResumo = null;
		Long idFatura = null;
		Long idDocumentoFiscal = null;
		DocumentoFiscal documentoFiscal = null;
		Fatura fatura = null;
		try {
			salvarPesquisaFiltro(faturaVO, model);
			if (faturaVO.getChavesPrimarias() == null || faturaVO.getChavesPrimarias().length <= 0) {
				dadosResumo = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);
				if (dadosResumo != null) {
					idFatura = dadosResumo.getIdFatura();
				}
			} else {
				if (faturaVO.getChavesPrimarias().length == 1) {
					idFatura = faturaVO.getChavesPrimarias()[0];
					faturaVO.setIdFatura(idFatura);
					dadosResumo = controladorFatura.obterDadosIncluirNFDFatura(faturaVO.getChavesPrimarias()[0]);
					fatura = (Fatura) controladorCobranca.obterFatura(idFatura, "contratoAtual", "cliente", "creditoDebitoSituacao");
					documentoFiscal = consultaDocumentoFiscal(idFatura, fatura);
					idDocumentoFiscal = documentoFiscal.getChavePrimaria();
					documentoFiscal = controladorFatura.obterDocumentoFiscalPorChave(idDocumentoFiscal);
				}
			}

			model.addAttribute(LISTA_MOTIVO_COMPLEMENTO, controladorEntidadeConteudo.obterListaMotivoCcomplementoFatura());

			if (idFatura == null) {
				throw new NegocioException("ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES");
			}
			DadosResumoFatura dadosResumoFatura = controladorFatura.obterDadosDetalhamentoFatura(idFatura, idDocumentoFiscal);

			model.addAttribute(LISTA_PONTO_CONSUMO, dadosResumoFatura.getListaPontoConsumo());

			model.addAttribute(LISTA_HISTORICO_CONSUMO, dadosResumoFatura.getListaHistoricoConsumo());
			model.addAttribute(LISTA_DADOS_LEITURA_CONSUMO_FATURA, dadosResumoFatura.getListaDadosLeituraConsumoFatura());
			model.addAttribute(LISTA_DADOS_GERAIS, dadosResumoFatura.getListaDadosGeraisItens());

			if (fatura != null && fatura.getContratoAtual() != null) {
				faturaVO.setNumeroContrato(fatura.getContratoAtual().getNumeroCompletoContrato());
			}

			if (documentoFiscal != null) {
				faturaVO.setChaveAcesso(documentoFiscal.getChaveAcesso());
				faturaVO.setTipoNota(documentoFiscal.getTipoFaturamento().getDescricao());
				faturaVO.setNumeroSerie(documentoFiscal.getDescricaoSerie());
			}

			faturaVO.setIsValidarFiltroPesquisaFatura(Boolean.FALSE);

			if (faturaVO.getIndicadorTipoNotaNormalEletronica() == null) {
				faturaVO.setIndicadorTipoNotaNormalEletronica(0);
			}

			request.getSession().setAttribute(DADOS_RESUMO_FATURA, dadosResumo);

		} catch (GGASException e) {
			tela = pesquisarFatura(faturaVO, request, model);
			mensagemErroParametrizado(model, request, e);
		}

		salvarPesquisaFiltro(faturaVO, model);
		return tela;
	}

	private DocumentoFiscal consultaDocumentoFiscal(Long idFatura, Fatura fatura) throws NegocioException {
		DocumentoFiscal documentoFiscal;
		if (fatura.getCreditoDebitoSituacao() == null) {
			documentoFiscal = controladorFatura.obterDocumentoFiscalPorCodigoFatura(idFatura, Boolean.FALSE);
		} else {
			documentoFiscal = controladorFatura.obterDocumentoFiscalPorCodigoFatura(idFatura, fatura.getCreditoDebitoSituacao().isValido());
		}
		return documentoFiscal;
	}

	/**
	 * Incluir fatura complementar.
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 *
	 */
	@RequestMapping("incluirFaturaComplementar")
	public String incluirFaturaComplementar(FaturaPesquisaVO faturaVO, BindingResult result, HttpServletRequest request, Model model)
			throws GGASException {

		String tela = "forward:exibirIncluirFaturaComplementar";
		try {
			salvarPesquisaFiltro(faturaVO, model);
			validacoesFaturaComplementar(faturaVO.getIdMotivoComplemento(), faturaVO.getIdFatura());

			DadosResumoFatura dadosResumoFatura = (DadosResumoFatura) request.getSession().getAttribute(DADOS_RESUMO_FATURA);

			PontoConsumo pc = dadosResumoFatura.getListaPontoConsumo().get(0);
			ContratoPontoConsumo cpc = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pc.getChavePrimaria());

			HistoricoMedicao historicoMedicao =
					controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pc.getChavePrimaria(), null, null, Boolean.TRUE);

			controladorFatura.incluirFaturaComplementar(faturaVO.getIdMotivoComplemento(), faturaVO.getIdFatura(),
					faturaVO.getVolumeApurado(), faturaVO.getValor(), faturaVO.getObservacoesComplemento(), dadosResumoFatura, pc, cpc,
					historicoMedicao, getDadosAuditoria(request));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Fatura.FATURA_ROTULO);
			tela = exibirPesquisaFatura(request, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return tela;
	}

	private void validacoesFaturaComplementar(Long idMotivoComplemento, Long idFatura) throws GGASException {

		if (idMotivoComplemento == null || idMotivoComplemento == -1 || idMotivoComplemento == 0) {
			throw new NegocioException(Constantes.ERRO_FATURA_COMPLEMENTAR_MOTIVO_NAO_INFORMADO, true);
		}

		Long tributoIcms = Long.valueOf(controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ICMS).getValor());

		Long motivoComplemetoIcmsNormal =
				Long.valueOf(controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_MOTIVO_COMPLEMENTO_ICMS).getValor());

		Long motivoComplementoIcmsST =
				Long.valueOf(controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_MOTIVO_COMPLEMENTO_ICMS_ST).getValor());

		if (idMotivoComplemento.equals(motivoComplemetoIcmsNormal) || idMotivoComplemento.equals(motivoComplementoIcmsST)) {
			Collection<FaturaItem> itens = controladorFatura.listarFaturaItemPorChaveFatura(idFatura);
			FaturaItem item = null;
			if (!CollectionUtils.isEmpty(itens)) {
				item = itens.iterator().next();
			}

			if (item != null) {
				Tarifa tarifa = item.getTarifa();
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("idTarifa", tarifa.getChavePrimaria());
				Collection<TarifaVigencia> tarifasVigencias = controladorTarifa.consultarTarifasVigencia(filtro);

				if (!CollectionUtils.isEmpty(tarifasVigencias)) {
					TarifaVigencia tarifaVigente = tarifasVigencias.iterator().next();
					if (tarifaVigente != null) {
						if (idMotivoComplemento.equals(motivoComplementoIcmsST)) {
							if (tarifaVigente.getBaseCalculoIcmsSubstituido() == null
									|| tarifaVigente.getBaseCalculoIcmsSubstituido().compareTo(BigDecimal.ZERO) <= 0) {
								throw new NegocioException(Constantes.ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_ST, true);
							}
						} else if (idMotivoComplemento.equals(motivoComplemetoIcmsNormal)) {
							filtro.remove("idTarifa");
							filtro.put("idTarifaVigencia", tarifaVigente.getChavePrimaria());
							Collection<TarifaVigenciaTributo> colecaoTarifaVigenciaTributo =
									controladorTarifa.consultarTarifasTributo(filtro);
							if (!CollectionUtils.isEmpty(colecaoTarifaVigenciaTributo)) {
								Boolean possuiIcms = false;
								for (TarifaVigenciaTributo tvt : colecaoTarifaVigenciaTributo) {
									if (tvt.getTributo().getChavePrimaria() == tributoIcms) {
										possuiIcms = true;
										break;
									}
								}
								if (!possuiIcms) {
									throw new NegocioException(Constantes.ERRO_FATURA_COMPLEMENTAR_TRIBUTO_ICMS_NAO_VINCULADO_A_TARIFA,
											true);
								}
							}
						}
					}
				}
			}
		}
	}

	private boolean verificaCampo(Object campo) {
		boolean campoValido = false;

		if (campo != null) {
			if (campo instanceof String) {
				campoValido = true;
			} else if (campo instanceof Integer) {
				campoValido = (Integer) campo > 0;
			} else {
				campoValido = (Long) campo > 0;
			}
		}

		return campoValido;
	}
}