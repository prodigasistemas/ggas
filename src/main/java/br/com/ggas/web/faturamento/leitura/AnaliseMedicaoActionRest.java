/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.TipoAtividadeSistema;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Pair;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.leitura.dto.AnaliseLeituraDTO;
import br.com.ggas.web.faturamento.leitura.dto.DadosMedicaoDTO;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoSerializador;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaLeituraMovimento;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * Action responsável por gerenciar o fluxo de listagem de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
@Controller
public class AnaliseMedicaoActionRest extends GenericActionRest {

	private static final Logger LOG = Logger.getLogger(AnaliseMedicaoActionRest.class);

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private LeituraMovimentoNegocio negocio;
	
	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;
	
	@Autowired
	private ControladorAnormalidade controladorAnormalidade;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Exibe a tela de aprovação de leitura
	 * @param rotas rota da listagem
	 * @param grupoFaturamento grupo de faturamento
	 * @param idCronograma id do cronograma
	 * @return retorna o {@link ModelAndView} referente a página de retorno
	 * @throws GGASException lançada caso ocorra falha na busca da rota ou grupo de faturamento informados
	 */
	@RequestMapping("analiseMedicao")
	public ModelAndView exibirListagem(@RequestParam(required = false) List<Long> rotas, @RequestParam(required = false) Long grupoFaturamento,
									   @RequestParam(required = false) Long idCronograma) throws GGASException {
		Fachada fachada = Fachada.getInstancia();
		Integer ciclo = null;
		AtividadeSistema atividadeRegistrarLeitura = null;

		if (idCronograma != null) {
			ciclo = fachada.obterCronogramaFaturamento(idCronograma).getNumeroCiclo();
			atividadeRegistrarLeitura = fachada.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(idCronograma).stream()
					.map(CronogramaAtividadeFaturamento::getAtividadeSistema)
					.filter(c -> Long.valueOf(TipoAtividadeSistema.REGISTRAR_LEITURA.getNumeroSequencia())
							.equals(c.getChavePrimaria()))
					.findFirst().orElse(null);
		}

		final Collection<GrupoFaturamento> grupos = controladorRota.listarGruposFaturamentoRotas();
		final Collection<AnormalidadeLeitura> anormalidades = fachada.listarAnormalidadesLeitura();
		final Collection<AnormalidadeConsumo> anormalidadesConsumo = fachada.listarAnormalidadesConsumo();
		final Collection<Segmento> segmentos = controladorSegmento.listarSegmento();
		return new ModelAndView("analiseMedicao")
				.addObject("idCronograma", idCronograma)
				.addObject("anormalidades", anormalidades)
				.addObject("anormalidadeConsumo", anormalidadesConsumo)
				.addObject("ciclo", ciclo)
				.addObject("rotas", rotas)
				.addObject("grupoFaturamento", grupoFaturamento)
				.addObject("segmentos", segmentos)
				.addObject("grupos", grupos)
				.addObject("atividadeRegistrarLeitura", atividadeRegistrarLeitura);
	}

	/**
	 * Obtém o período de data a partir do qual deverá ser realizada a busca de leituras movimentos na tela de listagem
	 * @param rota chave da rota
	 * @param grupoFaturamento chave do grupo faturamento
	 * @return retorna o range de data em string no formato esperado pelo front end: 'inicio - fim'
	 */
	@RequestMapping("analiseMedicao/obterPeriodoBusca")
	public ResponseEntity<String> obterPeriodoBusca(@RequestParam Long rota, @RequestParam Long grupoFaturamento) {
		final String range = montarRange(rota, grupoFaturamento);
		return new ResponseEntity<>(range, HttpStatus.OK);
	}

	/**
	 * Busca as leituras de movimentos de forma paginada
	 * @param pesquisa pesquisa de leitura movimento
	 * @return retorna a lista de leituras movimentos
	 * @throws GGASException exceção lançada caso haja alguma falha na consulta
	 */
	@RequestMapping(value = "analiseMedicao/buscarLeituras", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarLeituras(@RequestBody PesquisaLeituraMovimento pesquisa) throws GGASException {

		final DatatablesServerSideDTO<LeituraMovimentoDTO> datatable = negocio.buscarLeituras(pesquisa);
		return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
	}

	/**
	 * Altera os dados de medição de leituras movimento
	 * @param dadosMedicao lista de informações de dados de medições das leituras que devem ser atualizadas
	 * @param grupoFaturamento Chave primaria do grupo de faturamento
	 * @return retorna o status de sucesso
	 */
	@RequestMapping(value = "analiseMedicao/alterarDadosMedicao/{grupoFaturamento}", produces = "application/json; charset=utf-8",
			consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> alterarDadosMedicao(@RequestBody List<DadosMedicaoDTO> dadosMedicao,
													  @PathVariable Long grupoFaturamento) {

		ResponseEntity<String> entity;
		try {
			negocio.atualizarDadosMedicao(grupoFaturamento, dadosMedicao);
			entity = new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Falha ao alterar valor de leitura", e);
			entity = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return entity;
	}

	/**
	 * Altera o status das leituras para que seja possível a releitura
	 * @param leituras as leituras para alteração de status
	 * @return o status de sucesso ou falha
	 */
	@RequestMapping(value = "analiseMedicao/solicitarReleitura", produces = "application/json; charset=utf-8",
			consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> solicitarReleitura(@RequestBody List<Long> leituras) {

		ResponseEntity<String> entity;
		try {
			negocio.solicitarReleitura(leituras);
			entity = new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Falha ao alterar valor de leitura", e);
			entity = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return ImmutableMap.<Class, JsonSerializer>builder().put(LeituraMovimentoDTO.class, new LeituraMovimentoSerializador()).build();
	}

	/**
	 * Monta o período de busca da leitura de movimento.
	 * Consiste na primeira data que possui leitura movimento para período especificado + 1 mês
	 * @param rota chave da rota
	 * @param grupoFaturamento chave do grupo
	 * @return retorna o range no formato 'inicio - fim'
	 */
	private String montarRange(Long rota, Long grupoFaturamento) {
		Pair<LocalDateTime, LocalDateTime> range = new Pair<>(LocalDateTime.now().minusDays(1L), LocalDateTime.now());

		if (rota != null && grupoFaturamento != null) {
			range = this.negocio.buscarRangeLeitura(rota, grupoFaturamento);
		}

		return MessageFormat.format("{0} - {1}",
				DataUtil.converterDataParaString(range.getFirst()),
				DataUtil.converterDataParaString(range.getSecond()));
	}
	
	@RequestMapping(value = "analiseMedicao/consistirLeitura", produces = "application/json; charset=utf-8", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> consistirLeitura(@RequestBody AnaliseLeituraDTO analiseLeitura, HttpServletRequest request) {

		ResponseEntity<String> entity;
		try {

			HistoricoMedicao historicoMedicao = null;
			if (analiseLeitura.getChaveHistoricoMedicao() != null && analiseLeitura.getChaveHistoricoMedicao() > 0) {
				historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao
						.obter(analiseLeitura.getChaveHistoricoMedicao());

				popularHistoricoMedicao(historicoMedicao, analiseLeitura, request);
				
			}
			
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
					.obter(analiseLeitura.getChavePontoConsumo(), "imovel", "instalacaoMedidor");
	
			controladorHistoricoMedicao.atualizarDadosFaturamentoLeitura(pontoConsumo, historicoMedicao,
					null, Boolean.valueOf(analiseLeitura.getIsConsistir()));

			entity = new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Falha ao alterar valor de leitura", e);
			entity = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return entity;
	}
	
	
	private void popularHistoricoMedicao(HistoricoMedicao historicoMedicao, AnaliseLeituraDTO analiseLeituraDTO, HttpServletRequest request) throws GGASException {

		if (analiseLeituraDTO.getLeituraAtual() == null
				|| StringUtils.isEmpty(analiseLeituraDTO.getLeituraAtual())) {
			historicoMedicao.setNumeroLeituraInformada(null);
		} else {
			historicoMedicao.setNumeroLeituraInformada(Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.NUMERO_LEITURA_INFORMADA, analiseLeituraDTO.getLeituraAtual(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		
		if (analiseLeituraDTO.getDataLeitura() == null
				|| StringUtils.isEmpty(analiseLeituraDTO.getDataLeitura())) {
			historicoMedicao.setDataLeituraInformada(null);
		} else {

			historicoMedicao
					.setDataLeituraInformada(Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_INFORMADA,
							analiseLeituraDTO.getDataLeitura(), Constantes.FORMATO_DATA_BR));
		}
		
		if (analiseLeituraDTO.getChaveAnormalidadeLeitura() == null
				|| analiseLeituraDTO.getChaveAnormalidadeLeitura() <= 0) {
			historicoMedicao.setAnormalidadeLeituraInformada(null);
			historicoMedicao.setAnormalidadeLeituraFaturada(null);
		} else {
			AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade
					.obterAnormalidadeLeitura(analiseLeituraDTO.getChaveAnormalidadeLeitura());
			historicoMedicao.setAnormalidadeLeituraInformada(anormalidadeLeitura);
		}

		if (analiseLeituraDTO.getLeituraAnterior() == null
				|| StringUtils.isEmpty(analiseLeituraDTO.getLeituraAnterior())) {
			historicoMedicao.setNumeroLeituraAnterior(null);
		} else {
			historicoMedicao.setNumeroLeituraAnterior(Util.converterCampoStringParaValorBigDecimal(
					HistoricoMedicao.NUMERO_LEITURA_ANTERIOR, analiseLeituraDTO.getLeituraAnterior(),
					Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO));
		}
		if (analiseLeituraDTO.getDataLeituraAnterior() == null
				|| StringUtils.isEmpty(analiseLeituraDTO.getDataLeituraAnterior())) {
			historicoMedicao.setDataLeituraAnterior(null);
		} else {
			historicoMedicao
					.setDataLeituraAnterior(Util.converterCampoStringParaData(HistoricoMedicao.DATA_LEITURA_ANTERIOR,
							analiseLeituraDTO.getDataLeituraAnterior(), Constantes.FORMATO_DATA_BR));
		}
		
		historicoMedicao.setLeiturista(Fachada.getInstancia().obterLeiturista(1l));

		historicoMedicao.setConsumoCorretor(historicoMedicao.getNumeroLeituraInformada().subtract(historicoMedicao.getNumeroLeituraAnterior()));	
		
		historicoMedicao.setDadosAuditoria(Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
				(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr()));
	}
	
	
	@RequestMapping(value = "analiseMedicao/buscarHistoricoLeitura", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarHistoricoLeitura(@RequestBody Map<String, Object> requestData) throws GGASException {

		Long chavePontoConsumo = Long.valueOf(requestData.get("chavePontoConsumo").toString());
		final DatatablesServerSideDTO<LeituraMovimentoDTO> datatable = negocio.buscarHistoricoLeituras(chavePontoConsumo);
		return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
	}

}
