/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.web.faturamento.simulacaocalculo.decorator;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;

/**
 * 
 *
 */
public class PontoConsumoSimularCalculoDecorator extends TableDecorator {

	private static final String SEPARADOR_CITYGATE = ", ";
	private String contextPath;

	/**
	 * @return the contextPath
	 */
	public String getContextPath() {

		return contextPath;
	}

	/**
	 * @param contextPath
	 *            the contextPath to set
	 */
	public void setContextPath(String contextPath) {

		this.contextPath = contextPath;
	}

	/*
	 * (non-Javadoc)
	 * @see org.displaytag.decorator.Decorator#init(javax.servlet.jsp.PageContext, java.lang.Object, org.displaytag.model.TableModel)
	 */
	@Override
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {

		super.init(pageContext, decorated, tableModel);
		this.contextPath = ((HttpServletRequest) pageContext.getRequest()).getContextPath();
	}

	/*
	 * (non-Javadoc)
	 * @see org.displaytag.decorator.TableDecorator#addRowClass()
	 */
	@Override
	public String addRowClass() {

		String rowClass = super.addRowClass();
		String idPontoConsumoSelecionado = (String) getPageContext().getRequest().getParameter("chavePrimaria");
		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();
		if(!StringUtils.isEmpty(idPontoConsumoSelecionado)
						&& idPontoConsumoSelecionado.equals(String.valueOf(pontoConsumo.getChavePrimaria()))) {
			rowClass = "clickedRow";
		}
		
		if (rowClass != null) {
			return rowClass;
		} else {
			
			rowClass = "";
			return rowClass;
		}
	}
	
	public String getCityGates() {
		PontoConsumo pontoConsumo = (PontoConsumo) getCurrentRowObject();
		Iterator<RedeDistribuicaoTronco> listaRedeTronco = pontoConsumo.getQuadraFace().getRede().getListaRedeTronco().iterator();
		StringBuilder cityGates = new StringBuilder();
		
		while(listaRedeTronco.hasNext()) {
			cityGates.append(listaRedeTronco.next().getTronco().getCityGate().getDescricao());
			if(listaRedeTronco.hasNext()) {
				cityGates.append(SEPARADOR_CITYGATE);
			}
		}
		return cityGates.toString();
	}

}
