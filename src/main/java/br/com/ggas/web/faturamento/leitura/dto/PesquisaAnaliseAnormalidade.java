package br.com.ggas.web.faturamento.leitura.dto;

/**
 * Classe representando um conjunto de critérios de busca para anormalidades
 */
public class PesquisaAnaliseAnormalidade {
	private int qtdRegistros;
	private int offset;

	private String dataGeracaoInicial;
	private String dataGeracaoFinal;
	private String dataMedicaoInicial;
	private String dataMedicaoFinal;

	private String percentualVariacaoValorFatura;
	private String referencia;
	private String ciclo;

	private Long idPontoConsumo;
	private String cepImovel;
	private String numeroImovel;
	private String complementoImovel;
	private String nomeImovel;
	private String matriculaImovel;
	private Boolean indicadorCondominio;

	private String cpfCliente;
	private String rgCliente;
	private String nomeCliente;
	private String numeroPassaporte;
	private String cnpjCliente;
	private String nomeFantasiaCliente;

	private Long chavePrimaria;
	private Long idLocalidade;
	private Long idSetorComercial;
	private Long idGrupoFaturamento;
	private Long idSegmento;
	private Long idSituacaoPontoConsumo;
	private Long idTipoConsumo;
	private Long idAnormalidadeFaturamento;
	
	private Boolean analisada = false;
	private Boolean indicadorImpedeFaturamento;

	
	private Long[] chavesPrimarias;
	private Long[] idsRota;

	public int getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(int qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getDataGeracaoInicial() {
		return dataGeracaoInicial;
	}

	public void setDataGeracaoInicial(String dataGeracaoInicial) {
		this.dataGeracaoInicial = dataGeracaoInicial;
	}

	public String getDataGeracaoFinal() {
		return dataGeracaoFinal;
	}

	public void setDataGeracaoFinal(String dataGeracaoFinal) {
		this.dataGeracaoFinal = dataGeracaoFinal;
	}

	public String getDataMedicaoInicial() {
		return dataMedicaoInicial;
	}

	public void setDataMedicaoInicial(String dataMedicaoInicial) {
		this.dataMedicaoInicial = dataMedicaoInicial;
	}

	public String getDataMedicaoFinal() {
		return dataMedicaoFinal;
	}

	public void setDataMedicaoFinal(String dataMedicaoFinal) {
		this.dataMedicaoFinal = dataMedicaoFinal;
	}

	/**
	 * @return percentualVariacaoValorFatura {@link String}
	 */
	public String getPercentualVariacaoValorFatura() {
		return percentualVariacaoValorFatura;
	}
	/**
	 * @param percentualVariacaoValorFatura {@link String}
	 */
	public void setPercentualVariacaoValorFatura(String percentualVariacaoValorFatura) {
		this.percentualVariacaoValorFatura = percentualVariacaoValorFatura;
	}
	/**
	 * @return referencia {@link String}
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia {@link String}
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return ciclo {@link String}
	 */
	public String getCiclo() {
		return ciclo;
	}
	/**
	 * @param ciclo {@link String}
	 */
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	/**
	 * @return cepImovel {@link String}
	 */
	public String getCepImovel() {
		return cepImovel;
	}
	/**
	 * @param cepImovel {@link String}
	 */
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	/**
	 * @return matriculaImovel {@link String}
	 */
	public String getMatriculaImovel() {
		return matriculaImovel;
	}
	/**
	 * @param matriculaImovel {@link String}
	 */
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}
	/**
	 * @return nomeImovel {@link String}
	 */
	public String getNomeImovel() {
		return nomeImovel;
	}
	/**
	 * @param nomeImovel {@link String}
	 */
	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}
	/**
	 * @return complementoImovel {@link String}
	 */
	public String getComplementoImovel() {
		return complementoImovel;
	}
	/**
	 * @param complementoImovel {@link String}
	 */
	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}
	/**
	 * @return numeroImovel {@link String}
	 */
	public String getNumeroImovel() {
		return numeroImovel;
	}
	/**
	 * @param numeroImovel {@link String}
	 */
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	/**
	 * @return analisada {@link Boolean}
	 */
	public Boolean getAnalisada() {
		return analisada;
	}
	/**
	 * @param analisada {@link Boolean}
	 */
	public void setAnalisada(Boolean analisada) {
		this.analisada = analisada;
	}
	/**
	 * @return indicadorImpedeFaturamento {@link Boolean}
	 */
	public Boolean getIndicadorImpedeFaturamento() {
		return indicadorImpedeFaturamento;
	}
	/**
	 * @param indicadorImpedeFaturamento {@link Boolean}
	 */
	public void setIndicadorImpedeFaturamento(Boolean indicadorImpedeFaturamento) {
		this.indicadorImpedeFaturamento = indicadorImpedeFaturamento;
	}
	/**
	 * @return indicadorCondominio {@link Boolean}
	 */
	public Boolean getIndicadorCondominio() {
		return indicadorCondominio;
	}
	/**
	 * @param indicadorCondominio {@link Boolean}
	 */
	public void setIndicadorCondominio(Boolean indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}
	
	/**
	 * @return chavePrimaria {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return idLocalidade {@link Long}
	 */
	public Long getIdLocalidade() {
		return idLocalidade;
	}
	/**
	 * @param idLocalidade {@link Long}
	 */
	public void setIdLocalidade(Long idLocalidade) {
		this.idLocalidade = idLocalidade;
	}
	/**
	 * @return idLocalidade {@link Long}
	 */
	public Long getIdSetorComercial() {
		return idSetorComercial;
	}
	/**
	 * @param idSetorComercial {@link Long}
	 */
	public void setIdSetorComercial(Long idSetorComercial) {
		this.idSetorComercial = idSetorComercial;
	}
	/**
	 * @return idGrupoFaturamento {@link Long}
	 */
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	/**
	 * @param idGrupoFaturamento {@link Long}
	 */
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	/**
	 * @return idSegmento {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}
	/**
	 * @param idSegmento {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}
	/**
	 * @return idSituacaoPontoConsumo {@link Long}
	 */
	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}
	/**
	 * @param idSituacaoPontoConsumo {@link Long}
	 */
	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}
	/**
	 * @return idTipoConsumo {@link Long}
	 */
	public Long getIdTipoConsumo() {
		return idTipoConsumo;
	}
	/**
	 * @param idTipoConsumo {@link Long}
	 */
	public void setIdTipoConsumo(Long idTipoConsumo) {
		this.idTipoConsumo = idTipoConsumo;
	}
	
	/**
	 * @return retorno {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if(this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}
	/**
	 * @param chavesPrimarias {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if(chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}
	}
	/**
	 * @return retorno {@link Long}
	 */
	public Long[] getIdsRota() {
		Long[] retorno = null;
		if(this.idsRota != null) {
			retorno = this.idsRota.clone();
		}
		return retorno;
	}
	/**
	 * @param idsRota {@link Long}
	 */
	public void setIdsRota(Long[] idsRota) {
		if(idsRota != null) {
			this.idsRota = idsRota.clone();
		}
	}

	/**
	 * Obtém id da anormalidade de faturamento
	 * @return chave primaria da anormalidade
	 */
	public Long getIdAnormalidadeFaturamento() {
		return idAnormalidadeFaturamento;
	}

	/**
	 * Configura anormalidade de faturamento
	 * @param idAnormalidadeFaturamento identificador da anormalidade
	 */
	public void setIdAnormalidadeFaturamento(Long idAnormalidadeFaturamento) {
		this.idAnormalidadeFaturamento = idAnormalidadeFaturamento;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getRgCliente() {
		return rgCliente;
	}

	public void setRgCliente(String rgCliente) {
		this.rgCliente = rgCliente;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getNumeroPassaporte() {
		return numeroPassaporte;
	}

	public void setNumeroPassaporte(String numeroPassaporte) {
		this.numeroPassaporte = numeroPassaporte;
	}

	public String getCnpjCliente() {
		return cnpjCliente;
	}

	public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	public String getNomeFantasiaCliente() {
		return nomeFantasiaCliente;
	}

	public void setNomeFantasiaCliente(String nomeFantasiaCliente) {
		this.nomeFantasiaCliente = nomeFantasiaCliente;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
}
