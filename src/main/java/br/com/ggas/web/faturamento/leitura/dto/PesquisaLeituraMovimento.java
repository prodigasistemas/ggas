/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.geral.apresentacao.deserializadorjson.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDateTime;
import java.util.List;

/**
 * DTO de pesquisa de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientBranchCoverage", "common-java:InsufficientLineCoverage",
		"common-java:InsufficientCommentDensity"})
public class PesquisaLeituraMovimento {

	private int qtdRegistros;
	private int offset;

	private Integer ciclo;
	private String segmento;
	private List<Long> rotas;
	private Long grupoFaturamento;
	private List<PontoConsumoDTO> pontosConsumo;
	private List<Long> situacaoLeituras;
	private String referencia;
	private Boolean possuiAnormalidadeLeitura;
	private Boolean possuiAnormalidadeConsumo;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataInicial;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataFinal;

	private List<Long> chavesAnormalidades;

	private List<Long> chavesAnormalidadesConsumo;

	private Boolean anormalidadeConsumoBloqueiaFaturamento;

	private Boolean anormalidadeLeituraBloqueiaFaturamento;

	public List<PontoConsumoDTO> getPontosConsumo() {
		return pontosConsumo;
	}

	public void setPontosConsumo(List<PontoConsumoDTO> pontoConsumo) {
		this.pontosConsumo = pontoConsumo;
	}

	public LocalDateTime getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(LocalDateTime dataInicial) {
		this.dataInicial = dataInicial;
	}

	public LocalDateTime getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDateTime dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<Long> getChavesAnormalidades() {
		return chavesAnormalidades;
	}

	public void setChavesAnormalidades(List<Long> chavesAnormalidades) {
		this.chavesAnormalidades = chavesAnormalidades;
	}

	public List<Long> getChavesAnormalidadesConsumo() {
		return chavesAnormalidadesConsumo;
	}

	public void setChavesAnormalidadesConsumo(List<Long> chavesAnormalidadesConsumo) {
		this.chavesAnormalidadesConsumo = chavesAnormalidadesConsumo;
	}

	public List<Long> getRotas() {
		return rotas;
	}

	public void setRotas(List<Long> rotas) {
		this.rotas = rotas;
	}

	public Long getGrupoFaturamento() {
		return grupoFaturamento;
	}

	public void setGrupoFaturamento(Long grupoFaturamento) {
		this.grupoFaturamento = grupoFaturamento;
	}

	public int getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(int qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<Long> getSituacaoLeituras() {
		return situacaoLeituras;
	}

	public void setSituacaoLeituras(List<Long> situacaoLeituras) {
		this.situacaoLeituras = situacaoLeituras;
	}

	public Integer getCiclo() {
		return ciclo;
	}

	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public Boolean getAnormalidadeConsumoBloqueiaFaturamento() {
		return anormalidadeConsumoBloqueiaFaturamento;
	}

	public void setAnormalidadeConsumoBloqueiaFaturamento(Boolean anormalidadeConsumoBloqueiaFaturamento) {
		this.anormalidadeConsumoBloqueiaFaturamento = anormalidadeConsumoBloqueiaFaturamento;
	}

	public Boolean getAnormalidadeLeituraBloqueiaFaturamento() {
		return anormalidadeLeituraBloqueiaFaturamento;
	}

	public void setAnormalidadeLeituraBloqueiaFaturamento(Boolean anormalidadeLeituraBloqueiaFaturamento) {
		this.anormalidadeLeituraBloqueiaFaturamento = anormalidadeLeituraBloqueiaFaturamento;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Boolean getPossuiAnormalidadeLeitura() {
		return possuiAnormalidadeLeitura;
	}

	public void setPossuiAnormalidadeLeitura(Boolean possuiAnormalidadeLeitura) {
		this.possuiAnormalidadeLeitura = possuiAnormalidadeLeitura;
	}

	public Boolean getPossuiAnormalidadeConsumo() {
		return possuiAnormalidadeConsumo;
	}

	public void setPossuiAnormalidadeConsumo(Boolean possuiAnormalidadeConsumo) {
		this.possuiAnormalidadeConsumo = possuiAnormalidadeConsumo;
	}
}
