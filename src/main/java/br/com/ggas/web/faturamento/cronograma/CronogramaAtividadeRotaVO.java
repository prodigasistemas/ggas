/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.medicao.rota.Rota;

/**
 * 
 * 
 */
public class CronogramaAtividadeRotaVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 7082315988723909922L;

	private Rota rota;

	private String dataPrevista;

	private String dataCiclo;

	private Integer diasLeitura;

	private Integer quantidadeLeitura;

	private String nomeleiturista;

	private boolean emAlerta = false;

	private Collection<Rota> rotasEmAlerta;

	private CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento;

	private String dataInicio;

	private String dataRealizada;

	/**
	 * @return the dataPrevista
	 */
	public String getDataPrevista() {

		return dataPrevista;
	}

	/**
	 * @param dataPrevista
	 *            the dataPrevista to set
	 */
	public void setDataPrevista(String dataPrevista) {

		this.dataPrevista = dataPrevista;
	}

	/**
	 * @return the cronogramaAtividadeFaturamento
	 */
	public CronogramaAtividadeFaturamento getCronogramaAtividadeFaturamento() {

		return cronogramaAtividadeFaturamento;
	}

	/**
	 * @param cronogramaAtividadeFaturamento
	 *            the
	 *            cronogramaAtividadeFaturamento
	 *            to set
	 */
	public void setCronogramaAtividadeFaturamento(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) {

		this.cronogramaAtividadeFaturamento = cronogramaAtividadeFaturamento;
	}

	/**
	 * @return the quantidadeLeitura
	 */
	public Integer getQuantidadeLeitura() {

		return quantidadeLeitura;
	}

	/**
	 * @param quantidadeLeitura
	 *            the quantidadeLeitura to set
	 */
	public void setQuantidadeLeitura(Integer quantidadeLeitura) {

		this.quantidadeLeitura = quantidadeLeitura;
	}

	/**
	 * @return the dataCiclo
	 */
	public String getDataCiclo() {

		return dataCiclo;
	}

	/**
	 * @param dataCiclo
	 *            the dataCiclo to set
	 */
	public void setDataCiclo(String dataCiclo) {

		this.dataCiclo = dataCiclo;
	}

	/**
	 * @return the rota
	 */
	public Rota getRota() {

		return rota;
	}

	/**
	 * @param rota
	 *            the rota to set
	 */
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	/**
	 * @return the diasLeitura
	 */
	public Integer getDiasLeitura() {

		return diasLeitura;
	}

	/**
	 * @param diasLeitura
	 *            the diasLeitura to set
	 */
	public void setDiasLeitura(Integer diasLeitura) {

		this.diasLeitura = diasLeitura;
	}

	/**
	 * @return the nomeleiturista
	 */
	public String getNomeleiturista() {

		return nomeleiturista;
	}

	/**
	 * @param nomeleiturista
	 *            the nomeleiturista to set
	 */
	public void setNomeleiturista(String nomeleiturista) {

		this.nomeleiturista = nomeleiturista;
	}

	/**
	 * @return the emAlerta
	 */
	public boolean isEmAlerta() {

		return emAlerta;
	}

	/**
	 * @param emAlerta
	 *            the emAlerta to set
	 */
	public void setEmAlerta(boolean emAlerta) {

		this.emAlerta = emAlerta;
	}

	public String getEmAlertaFormatado() {

		String emAlertaFormatado = "Não";
		if(isEmAlerta()) {
			emAlertaFormatado = "Sim";
		}
		return emAlertaFormatado;
	}

	/**
	 * @return the rotasEmAlerta
	 */
	public Collection<Rota> getRotasEmAlerta() {

		return rotasEmAlerta;
	}

	/**
	 * @param rotasEmAlerta
	 *            the rotasEmAlerta to set
	 */
	public void setRotasEmAlerta(Collection<Rota> rotasEmAlerta) {

		this.rotasEmAlerta = rotasEmAlerta;
	}

	public String getRotasEmAlertaFormatado() {

		StringBuilder sb = new StringBuilder("Rota(s):[");
		if(this.rotasEmAlerta != null) {
			for (Rota rotaAlerta : this.rotasEmAlerta) {
				sb.append("(").append(rotaAlerta.getNumeroRota()).append(")");
			}
		}
		sb.append("]");
		return sb.toString();

	}

	public String getDataInicio() {

		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {

		this.dataInicio = dataInicio;
	}

	public String getDataRealizada() {

		return dataRealizada;
	}

	public void setDataRealizada(String dataRealizada) {

		this.dataRealizada = dataRealizada;
	}

}
