package br.com.ggas.web.faturamento.mensagemfaturamento;
/**
 * Classe utilizada para transferência de dados entre as telas de mensagem de faturamento.
 *
 */
public class MensagemFaturamentoVO {
	
	private Integer versao;
	private Long idTipoMensagem;
	private Long idSegmento;
	private String dataInicioVigencia;
	private String dataFinalVigencia;
	private Long idImovel;
	private String mensagem;
	private Boolean habilitado;
	
	private Long[] segmentosSelecionados;
	private Long[] ramoAtividadeSelecionados;
	private Long[] formaCobrancaSelecionados;
	private Integer[] dataVencimentoSelecionados;
	
	public Integer getVersao() {
		return versao;
	}
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	public Long getIdTipoMensagem() {
		return idTipoMensagem;
	}
	public void setIdTipoMensagem(Long idTipoMensagem) {
		this.idTipoMensagem = idTipoMensagem;
	}
	
	public Long getIdSegmento() {
		return idSegmento;
	}
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}
	
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	public String getDataFinalVigencia() {
		return dataFinalVigencia;
	}
	public void setDataFinalVigencia(String dataFinalVigencia) {
		this.dataFinalVigencia = dataFinalVigencia;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public Long[] getSegmentosSelecionados() {
	
		Long[] segmentos = null;
		if (this.segmentosSelecionados != null) {
			segmentos = this.segmentosSelecionados.clone();
		}
		return segmentos;
		
	}
	public void setSegmentosSelecionados(Long[] segmentosSelecionados) {
		
		if (segmentosSelecionados != null) {
			this.segmentosSelecionados = segmentosSelecionados.clone();
		}
	}
	public Long[] getRamoAtividadeSelecionados() {
		
		Long[] ramoAtividade = null;
		if (this.ramoAtividadeSelecionados != null) {
			ramoAtividade = this.ramoAtividadeSelecionados.clone();
		}
		return ramoAtividade;
	}
	public void setRamoAtividadeSelecionados(Long[] ramoAtividadeSelecionados) {
		
		if (ramoAtividadeSelecionados != null) {
			this.ramoAtividadeSelecionados = ramoAtividadeSelecionados.clone();
		}
	}
	public Long[] getFormaCobrancaSelecionados() {
		
		Long[] formaCobranca = null;
		if (this.formaCobrancaSelecionados != null) {
			formaCobranca = this.formaCobrancaSelecionados.clone();
		}
		return formaCobranca;
	}
	public void setFormaCobrancaSelecionados(Long[] formaCobrancaSelecionados) {
		
		if (formaCobrancaSelecionados != null) {
			this.formaCobrancaSelecionados = formaCobrancaSelecionados.clone();
		} else {
			this.formaCobrancaSelecionados = null;
		}
	}
	public Integer[] getDataVencimentoSelecionados() {
		
		Integer[] dataVencimento = null;
		if (this.dataVencimentoSelecionados != null) {
			dataVencimento = this.dataVencimentoSelecionados.clone();
		}
		return dataVencimento;
	}
	public void setDataVencimentoSelecionados(Integer[] dataVencimentoSelecionados) {
		
		if (dataVencimentoSelecionados != null) {
			this.dataVencimentoSelecionados = dataVencimentoSelecionados.clone();
		} else {
			this.dataVencimentoSelecionados = null;
		}
	}

}
