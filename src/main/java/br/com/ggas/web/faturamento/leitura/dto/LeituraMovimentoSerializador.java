/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.geral.apresentacao.serializadorjson.AbstractGGASSerializador;
import br.com.ggas.geral.apresentacao.serializadorjson.GenericSafeJsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Serializador da leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientCommentDensity"})
public class LeituraMovimentoSerializador extends AbstractGGASSerializador<LeituraMovimentoDTO> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void serializar(LeituraMovimentoDTO valor, GenericSafeJsonGenerator gen, SerializerProvider provider)
			throws IOException {
		gen.writeStartObject();

		gen.writeNumberField("chaveLeituraMovimento", valor.getChaveLeituraMovimento());
		gen.writeStringField("pontoConsumo", valor.getPontoConsumo());
		gen.writeStringField("mesAnoCiclo", valor.getMesAnoCiclo());
		gen.writeStringField("observacaoPontoConsumo", valor.getObservacaoPontoConsumo());
		gen.writeStringField("dataLeitura", valor.getDataLeitura());
		gen.writeNumberField("leituraAnterior", valor.getLeituraAnterior());
		gen.writeNumberField("leituraAtual", valor.getLeituraAtual());
		gen.writeNumberField("volume", valor.getVolume());
		gen.writeNumberField("pressao", valor.getPressao());
		gen.writeNumberField("temperatura", valor.getTemperatura());
		gen.writeNumberField("volumePtz", valor.getVolumePtz());
		gen.writeNumberField("fcPtz", valor.getFcPtz());
		gen.writeStringField("observacaoLeitura", valor.getObservacaoLeitura());
		gen.writeStringField("tipoConsumo", valor.getTipoConsumo());
		gen.writeStringField("origem", valor.getOrigem());
		gen.writeNumberField("consumoApurado", valor.getConsumoApurado());
		gen.writeObjectFieldNullSafe("anormalidadeConsumo", valor.getAnormalidadeConsumo());
		gen.writeObjectFieldNullSafe("anormalidadeLeitura", valor.getAnormalidadeLeitura());
		gen.writeObjectFieldNullSafe("corrigePT", valor.getCorrigePT());
		if (valor.getSituacaoLeitura() != null) {
			gen.writeObjectFieldStart("situacaoLeitura");
			gen.writeNumberField("chavePrimaria", valor.getSituacaoLeitura().getChavePrimaria());
			gen.writeStringField("descricao", valor.getSituacaoLeitura().getDescricao());
			gen.writeEndObject();
		}
		
		gen.writeStringField("situacaoPontoConsumo", valor.getSituacaoPontoConsumo());
		gen.writeNumberField("chaveRota", valor.getChaveRota());
		gen.writeStringField("fotoColetor", valor.getFotoColetor());
		gen.writeStringField("dataLeituraAnterior", valor.getDataLeituraAnterior());
		gen.writeNumberField("chaveHistoricoMedicao", valor.getChaveHistoricoMedicao());
		gen.writeNumberField("chavePontoConsumo", valor.getChavePontoConsumo());
		
		gen.writeNumberField("consumo", valor.getConsumo());
		gen.writeNumberField("fcPcs", valor.getFcPcs());
		gen.writeNumberField("quantidadeDias", valor.getQuantidadeDias());
		
		gen.writeEndObject();

	}

}
