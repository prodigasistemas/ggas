/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.web.faturamento.tarifa;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.DadosFaixasTarifa;
import br.com.ggas.faturamento.tarifa.DadosTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaPesquisar;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tarifa.impl.FiltroTarifaVigenciaDescontos;
import br.com.ggas.faturamento.tarifa.impl.TarifaFormVO;
import br.com.ggas.faturamento.tarifa.impl.TarifaImpl;
import br.com.ggas.faturamento.tarifa.impl.TarifaVigenciaDescontoImpl;
import br.com.ggas.faturamento.tarifa.impl.TarifaVigenciaImpl;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

/**
 * Classe responsÃ¡vel pelas operaÃ§Ãµes sobre a entidade Tarifa.
 * 
 * @author esantana
 *
 */
@Controller
public class TarifaAction extends GenericAction {

	private static final String EXIBIR_INSERCAO_TARIFA = "exibirInsercaoTarifa";
	private static final String FORWARD_EXIBIR_TARIFA_APOS_INSERIR_FAIXA = "forward:exibirTarifaAposInserirFaixa";
	private static final String FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA = "forward:exibirTarifaAposAlterarFaixa";
	private static final String ICMS_SUBSTITUIDO = "ICMS SubstituÃ­do";
	private static final String BASE_CALCULO_ICMS_SUBSTITUÍDO  = "Base Calculo ICMS Substituído";
	private static final String TARIFA_FORM_VO = "tarifaFormVO";
	private static final String FORWARD_PESQUISAR_TARIFA = "forward:pesquisarTarifa";
	private static final String TARIFA_VIGENCIA = "tarifaVigencia";
	private static final String TARIFA_VIGENCIA_DESCONTO = "tarifaVigenciaDesconto";
	private static final String POSSUI_ALCADA_AUTORIZACAO = "possuiAlcadaAutorizacao";
	public static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String INTERVALO_ANOS_DATA = "intervaloAnosData";
	private static final String LISTA_SEGMENTO = "listaSegmento";
	private static final String LISTA_TIPO_CONTRATO = "listaTipoContrato";
	private static final String LISTA_ITEM_FATURA = "listaItemFatura";
	private static final String STATUS_AUTORIZADO = "statusAutorizado";
	private static final String STATUS_NAO_AUTORIZADO = "statusNaoAutorizado";
	private static final String STATUS_PENDENTE = "statusPendente";
	private static final String LISTA_TARIFA = "listaTarifa";
	private static final String TARIFA = "tarifa";
	private static final String DESCRICAO_TARIFA = "descricaoTarifa";
	private static final String DESCRICAO_ABREVIADA_TARIFA = "descricaoAbreviadaTarifa";
	private static final String DATA_VIGENCIA = "dataVigencia";
	private static final String ID_SEGMENTO = "idSegmento";
	private static final String ID_RAMO_ATIVIDADE = "idRamoAtividade";
	private static final String ID_ITEM_FATURA = "idItemFatura";
	private static final String TIPO_CONTRATO = "tipoContrato";
	private static final String SEGMENTO = "segmento";
	private static final String RAMO_ATIVIDADE = "ramoAtividade";
	private static final String STATUS = "status";
	private static final String LISTA_TARIFAS_VIGENCIA = "listaTarifasVigencia";
	private static final String ID_TARIFA = "idTarifa";
	private static final String LISTA_TARIFAS_VIGENCIA_DESCONTO = "listaTarifasVigenciaDesconto";
	private static final String TARIFA_VIGENCIA_FAIXAS = "tarifaVigenciaFaixas";
	private static final String LISTA_TARIFAS_FAIXA_DESCONTO = "listaTarifasFaixaDesconto";
	private static final String LISTA_RAMO_ATIVIDADE = "listaRamoAtividade";
	private static final String LISTA_TIPO_CALCULO = "listaTipoCalculo";
	private static final String LISTA_BASE_CALCULO = "listaBaseCalculo";
	private static final String LISTA_UNIDADE_MONETARIA = "listaUnidadeMonetaria";
	private static final String LISTA_CLONE_FAIXA = "listaCloneFaixa";
	private static final String LISTA_TRIBUTOS = "listaTributos";
	private static final String LISTA_TRIBUTOS_ASSOCIADOS = "listaTributosAssociados";
	private static final String LISTA_DESCONTOS_FAIXA = "listaDescontosFaixa";
	private static final String LISTA_DADOS_FAIXA = "listaDadosFaixa";
	private static final String LISTA_DESCONTOS_CADASTRADOS = "listaDescontosCadastrados";
	private static final String LISTA_VIGENCIAS_TARIFA = "listaVigenciasTarifa";
	private static final String LISTA_TIPO_VALOR = "listaTipoValor";
	private static final String IS_TARIFA_UTILIZADA = "isTarifaUtilizada";
	private static final String VALOR_MAXIMO_FAIXA_FINAL = "99999999999999999";
	private static final String DESCRICAO_VIGENCIA_TIPO_CALCULO = "descricaoVigenciaTipoCalculo";
	private static final String DESCRICAO_VIGENCIA_UNIDADE_MONETARIA = "descricaoVigenciaUnidadeMonetaria";
	private static final String OBSERVACAO_VIGENCIA_DESCONTO = "descricaoVigenciaDesconto";
	private static final String OBSERVACAO_VIGENCIA = "observacaoVigencia";
	private static final String DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO = "descricaoVigenciaVolumeBaseCalculo";
	private static final String BASE_CALCULO_ICMS_SUBSTITUIDO = "baseCalculoIcms";
	private static final String ICMS_SUBSTITUIDO_METRO_CUBICO = "icmsSubstituido";
	private static final String MENSAGEM_ICMS_SUBSTITUIDO = "mensagemIcmsSubstituto";
	private static final String DATA_ATUAL = "dataAtual";
	public static final String ID_TIPO_DESCONTO = "idTipoDesconto";
	private static final String TRIBUTOS_ASSOCIADOS = "tributosAssociados";

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorAlcada controladorAlcada;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;

	@Autowired
	private ControladorTributo controladorTributo;

	@Autowired
	private ControladorUsuario controladorUsuario;

	/**
	 * Método responsável pela exibição da tela de inserção de tarifas.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param idTributosAssociados
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping(EXIBIR_INSERCAO_TARIFA)
	public String exibirInsercaoTarifa(TarifaImpl tarifa, BindingResult bindingResult,
			@RequestParam(value = "tributosAssociados", required = false) Long[] idTributosAssociados, 
			Model model, HttpServletRequest request) {

		try {
			request.getSession().removeAttribute(LISTA_DADOS_FAIXA);
			request.getSession().removeAttribute(LISTA_DESCONTOS_FAIXA);
			this.carregarCamposParaManterTarifa(tarifa, model, idTributosAssociados);
			this.carregarListasIniciaisFaixasDescontos(request.getSession());
			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return EXIBIR_INSERCAO_TARIFA;
	}
	
	/**
	 * Método responsável por carregar os campos para atualização e inserção de tarifas.
	 * 
	 * @param tarifa - {@link Tarifa}
	 * @param model - {@link Model}
	 * @param idTributosAssociados
	 * @throws GGASException - {@link GGASException}
	 */
	private void carregarCamposParaManterTarifa(Tarifa tarifa, Model model, Long[] idTributosAssociados) throws GGASException {
		
		this.carregarCampos(model, tarifa);
		this.tratarCamposSelect(idTributosAssociados, model);
		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
	}

	/**
	 * Método responsável por inserir uma tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("inserirTarifa")
	public String inserirTarifa(TarifaImpl tarifa, TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaVigenciaImpl tarifaVigencia,
			TarifaFormVO tarifaFormVO, BindingResult bindingResult, HttpServletRequest request, Model model)
			throws ReflectiveOperationException {

		String view = FORWARD_PESQUISAR_TARIFA;

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		try {
			this.carregarCamposParaManterTarifa(tarifa, model, tarifaFormVO.getTributosAssociados());
			
			validarToken(request);
			
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

			this.adicionarTributosEmTarifaVigencia(tarifaFormVO.getTributosAssociados(), tarifaVigencia, dadosAuditoria);

			tarifa.setDadosAuditoria(dadosAuditoria);

			tarifaVigenciaDesconto.setHabilitado(Boolean.TRUE);
			tarifaVigenciaDesconto.setUltimaAlteracao(Calendar.getInstance().getTime());
			tarifaVigenciaDesconto.setTarifa(tarifa);
			tarifaVigenciaDesconto.setDadosAuditoria(dadosAuditoria);

			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);

			this.popularTarifaVigenciaFaixa(listaDadosFaixa, tarifaVigencia, tarifaFormVO, dadosAuditoria, Boolean.FALSE);
			controladorTarifa.validarAdicaoFaixaDesconto(listaDadosFaixa, Boolean.TRUE);

			this.popularTarifaVigencia(model, tarifaVigencia, tarifaFormVO);
			tarifaVigencia.setTarifa(tarifa);
			tarifaVigencia.setDadosAuditoria(dadosAuditoria);

			tarifaVigenciaDesconto.setTarifaVigencia(tarifaVigencia);

			this.popularFaixaDesconto(listaDadosFaixa, tarifaFormVO);

			List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
			this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

			controladorTarifa.inserirTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaNova);
			listaDadosFaixa.clear();
			listaDadosFaixa.addAll(listaNova);
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixa);

			model.addAttribute(DESCRICAO_TARIFA, tarifa.getDescricao());
			model.addAttribute(DESCRICAO_ABREVIADA_TARIFA, tarifa.getDescricaoAbreviada());
			model.addAttribute(TARIFA, tarifa);

			request.getSession().removeAttribute(LISTA_DADOS_FAIXA);
			request.getSession().removeAttribute(LISTA_DESCONTOS_FAIXA);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(Tarifa.TARIFA_CAMPO_STRING));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			saveToken(request);
			view = EXIBIR_INSERCAO_TARIFA;
		}

		return view;
	}

	/**
	 * Método responsável por realizar as alterações na tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigenciaParam - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDescontoParam - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("alterarTarifa")
	public String alterarTarifa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigenciaParam,
			TarifaVigenciaDescontoImpl tarifaVigenciaDescontoParam, TarifaFormVO tarifaFormVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws ReflectiveOperationException {

		String view = FORWARD_PESQUISAR_TARIFA;

		try {
			
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

			Long chavePrimariaVigencia = tarifaFormVO.getChavePrimariaVigencia();
			Long chavePrimariaVigenciaDesconto = tarifaFormVO.getChavePrimariaVigenciaDesconto();
			
			tarifa.setDadosAuditoria(dadosAuditoria);
			tarifa.setHabilitado(Boolean.TRUE);
			
			boolean isNovaVigencia = false;
			if ((chavePrimariaVigencia == null) || (chavePrimariaVigencia.longValue() == 0L)) {
				isNovaVigencia = true;
			}

			TarifaVigencia tarifaVigencia =
					this.popularTarifaVigencia(tarifaVigenciaParam, isNovaVigencia, dadosAuditoria, chavePrimariaVigencia, tarifaFormVO);

			boolean isNovoDesconto = false;
			if ((chavePrimariaVigenciaDesconto == null) || (chavePrimariaVigenciaDesconto.longValue() == 0L)) {
				isNovoDesconto = true;
			}

			TarifaVigenciaDesconto tarifaVigenciaDesconto =
					this.popularTarifaVigenciaDesconto(chavePrimariaVigenciaDesconto, isNovoDesconto, tarifaVigenciaDescontoParam);
			
			tarifaVigenciaDesconto.setDadosAuditoria(dadosAuditoria);
			tarifaVigenciaDesconto.setTarifa(tarifa);

			model.addAttribute(TARIFA, tarifa);
			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
			model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

			validarToken(request);
			
			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);
			this.popularTarifaVigenciaFaixa(listaDadosFaixa, tarifaVigencia, tarifaFormVO, dadosAuditoria, Boolean.TRUE);

			tarifaVigencia.setTarifa(tarifa);
			tarifaVigencia.setDadosAuditoria(dadosAuditoria);
			
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixa);
			this.popularFaixaDesconto(listaDadosFaixa, tarifaFormVO);

			if (!isNovaVigencia) {
				controladorTarifa.validarNovaVigenciaDesconto(tarifaVigenciaDesconto, chavePrimariaVigenciaDesconto);
			}

			controladorTarifa.atualizarTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa, isNovaVigencia);

			request.getSession().removeAttribute(LISTA_DADOS_FAIXA);
			request.getSession().removeAttribute(LISTA_DESCONTOS_FAIXA);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(Tarifa.TARIFA_CAMPO_STRING));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:exibirAlteracaoTarifa";
		}

		return view;
	}

	private TarifaVigencia popularTarifaVigencia(TarifaVigencia tarifaVigenciaAtualizada, Boolean isNovaVigencia,
			DadosAuditoria dadosAuditoria, Long chavePrimariaVigencia, TarifaFormVO faixasTarifaVO) throws GGASException {

		TarifaVigencia tarifaVigencia = tarifaVigenciaAtualizada;

		if (!isNovaVigencia) {

			tarifaVigencia = controladorTarifa.obterTarifaVigencia(chavePrimariaVigencia, TARIFA_VIGENCIA_FAIXAS);

			this.popularTarifaVigencia(tarifaVigencia, tarifaVigenciaAtualizada);
		}

		if (!StringUtils.isEmpty(faixasTarifaVO.getVigenciaBaseCalculoIcms())) {
			tarifaVigencia.setBaseCalculoIcmsSubstituido(Util.converterCampoStringParaValorBigDecimal(BASE_CALCULO_ICMS_SUBSTITUÍDO ,
					faixasTarifaVO.getVigenciaBaseCalculoIcms(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(faixasTarifaVO.getVigenciaIcmsSubstituidoMetroCubico())) {
			tarifaVigencia.setIcmsSubstituidoMetroCubico(Util.converterCampoStringParaValorBigDecimal(ICMS_SUBSTITUIDO,
					faixasTarifaVO.getVigenciaIcmsSubstituidoMetroCubico(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}
		
		if (!StringUtils.isEmpty(faixasTarifaVO.getVigenciaIcmsSubstituidoMetroCubico())) {
			tarifaVigencia.setMensagemIcmsSubstituto(tarifaVigenciaAtualizada.getMensagemIcmsSubstituto());
		} else if(isNovaVigencia) {
			tarifaVigencia.setMensagemIcmsSubstituto(null);
		}

		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(statusPendente));

		this.adicionarTributosEmTarifaVigencia(faixasTarifaVO.getTributosAssociados(), tarifaVigencia, dadosAuditoria);

		tarifaVigencia.setHabilitado(true);
		tarifaVigencia.setUltimaAlteracao(Calendar.getInstance().getTime());
		tarifaVigencia.setStatus(status);

		return tarifaVigencia;
	}

	private void popularTarifaVigencia(TarifaVigencia tarifaVigencia, TarifaVigencia tarifaVigenciaAtualizada) {

		if (tarifaVigenciaAtualizada.getInformarMsgIcmsSubstituido() != null) {
			tarifaVigencia.setInformarMsgIcmsSubstituido(tarifaVigenciaAtualizada.getInformarMsgIcmsSubstituido());
		}

		if (tarifaVigenciaAtualizada.getDataVigencia() != null) {
			tarifaVigencia.setDataVigencia(tarifaVigenciaAtualizada.getDataVigencia());
		} else {
			tarifaVigencia.setDataVigencia(null);
		}

		if (tarifaVigenciaAtualizada.getUnidadeMonetaria() != null) {
			tarifaVigencia.setUnidadeMonetaria(tarifaVigenciaAtualizada.getUnidadeMonetaria());
		} else {
			tarifaVigencia.setUnidadeMonetaria(null);
		}

		if (tarifaVigenciaAtualizada.getTipoCalculo() != null) {
			tarifaVigencia.setTipoCalculo(tarifaVigenciaAtualizada.getTipoCalculo());
		} else {
			tarifaVigencia.setTipoCalculo(null);
		}

		if (tarifaVigenciaAtualizada.getBaseApuracao() != null) {
			tarifaVigencia.setBaseApuracao(tarifaVigenciaAtualizada.getBaseApuracao());
		} else {
			tarifaVigencia.setBaseApuracao(null);
		}
		
		if (!StringUtils.isEmpty(tarifaVigenciaAtualizada.getComentario())) {
			tarifaVigencia.setComentario(tarifaVigenciaAtualizada.getComentario());
		} else {
			tarifaVigencia.setComentario(null);
		}
	}

	private TarifaVigenciaDesconto popularTarifaVigenciaDesconto(Long chavePrimariaVigenciaDesconto, Boolean isNovoDesconto,
			TarifaVigenciaDesconto tarifaVigenciaDescontoAtualizada) throws NegocioException {

		TarifaVigenciaDesconto tarifaVigenciaDesconto = tarifaVigenciaDescontoAtualizada;

		if (!isNovoDesconto) {
			tarifaVigenciaDesconto = controladorTarifa.obterTarifaVigenciaDesconto(chavePrimariaVigenciaDesconto);

			if (tarifaVigenciaDescontoAtualizada.getInicioVigencia() != null) {
				tarifaVigenciaDesconto.setInicioVigencia(tarifaVigenciaDescontoAtualizada.getInicioVigencia());
			} else {
				tarifaVigenciaDesconto.setInicioVigencia(null);
			}

			if (tarifaVigenciaDescontoAtualizada.getFimVigencia() != null) {
				tarifaVigenciaDesconto.setFimVigencia(tarifaVigenciaDescontoAtualizada.getFimVigencia());
			} else {
				tarifaVigenciaDesconto.setFimVigencia(null);
			}

			tarifaVigenciaDesconto.setImpressaoDesconto(tarifaVigenciaDescontoAtualizada.getImpressaoDesconto());

			tarifaVigenciaDesconto.setImpressaoDescricao(tarifaVigenciaDescontoAtualizada.getImpressaoDescricao());

			if (!StringUtils.isEmpty(tarifaVigenciaDescontoAtualizada.getDescricaoDescontoVigencia())) {
				tarifaVigenciaDesconto.setDescricaoDescontoVigencia(tarifaVigenciaDescontoAtualizada.getDescricaoDescontoVigencia());
			} else {
				tarifaVigenciaDesconto.setDescricaoDescontoVigencia(null);
			}
		}

		tarifaVigenciaDesconto.setHabilitado(Boolean.TRUE);
		tarifaVigenciaDesconto.setUltimaAlteracao(Calendar.getInstance().getTime());

		return tarifaVigenciaDesconto;
	}

	/**
	 * Método responsável por exibir a tela de alteração de tarifas.
	 * 
	 * @param tarifaParam - {@link TarifaImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param tributosAssociados
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoTarifa")
	public String exibirAlteracaoTarifa(TarifaImpl tarifaParam,
			BindingResult bindingResult,
			@RequestParam(value = "tributosAssociados", required = false) Long[] tributosAssociados, Model model,
			HttpServletRequest request) throws GGASException {

		saveToken(request);
		
		Tarifa tarifa = tarifaParam;
		Long[] idTributosAssociados = tributosAssociados;

		DadosTarifa dadosTarifa = controladorTarifa.montarDadosTarifa(tarifa.getChavePrimaria());
		HttpSession session = request.getSession();

		if (!super.isPostBack(request)) {

			TarifaFormVO tarifaFormVO = new TarifaFormVO();

			session.removeAttribute(LISTA_DADOS_FAIXA);
			session.removeAttribute(LISTA_DESCONTOS_FAIXA);

			session.setAttribute(LISTA_DADOS_FAIXA, dadosTarifa.getFaixasTarifa());
			session.setAttribute(LISTA_DESCONTOS_FAIXA, dadosTarifa.getFaixasDesconto());

			tarifa = dadosTarifa.getTarifa();

			model.addAttribute(TARIFA, tarifa);
			model.addAttribute(TARIFA_VIGENCIA, dadosTarifa.getTarifaVigencia());

			tarifaFormVO.setIdVigenciaTarifa(dadosTarifa.getTarifaVigencia().getChavePrimaria());
			tarifaFormVO.setChavePrimariaVigencia(dadosTarifa.getTarifaVigencia().getChavePrimaria());

			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, dadosTarifa.getTarifaVigenciaDesconto());

			if (dadosTarifa.getTarifaVigenciaDesconto() != null) {
				tarifaFormVO.setIdDescontoCadastrado(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());
				tarifaFormVO.setChavePrimariaVigenciaDesconto(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());
			}

			this.popularCamposVigenciaICMS(dadosTarifa.getTarifaVigencia(), tarifaFormVO);
			this.setarTipoDescontoForm(tarifaFormVO, dadosTarifa.getFaixasDesconto());

			idTributosAssociados = this.getIdTributosAssociadosTarifaVigencia(dadosTarifa.getTarifaVigencia());

			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);
		}

		model.addAttribute(LISTA_VIGENCIAS_TARIFA, controladorTarifa.consultarVigenciasTarifa(tarifa.getChavePrimaria()));
		model.addAttribute(LISTA_DESCONTOS_CADASTRADOS,
				controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(dadosTarifa.getTarifaVigencia().getChavePrimaria()));
		model.addAttribute(LISTA_TIPO_VALOR, controladorEntidadeConteudo.obterListaTipoValorTarifa());

		this.carregarCampos(model, tarifa);
		this.tratarCamposSelect(idTributosAssociados, model);

		Boolean isTarifaUtilizada = controladorTarifa.verificaSeTarifaEstaSendoUtilizada(tarifa.getChavePrimaria());
		model.addAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
		session.setAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());

		setVigenciaNova(model, session, false);

		model.addAttribute(DATA_ATUAL, DataUtil.converterDataParaString(Calendar.getInstance().getTime(), Boolean.FALSE));

		return "exibirAlteracaoTarifa";
	}

	private void setarTipoDescontoForm(TarifaFormVO faixasTarifaVO, List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa) {

		Long idTipoDesconto = null;
		if (listaDadosFaixasDescontoTarifa != null && !listaDadosFaixasDescontoTarifa.isEmpty()) {
			for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixasDescontoTarifa) {
				if (dadosFaixasTarifa.getTipoDesconto() != null) {
					idTipoDesconto = dadosFaixasTarifa.getTipoDesconto();
					break;
				}
			}
		}
		faixasTarifaVO.setIdTipoDesconto(idTipoDesconto);
	}

	private void popularCamposVigenciaICMS(TarifaVigencia tarifaVigencia, TarifaFormVO faixasTarifaVO) {

		if (tarifaVigencia.getInformarMsgIcmsSubstituido() != null && tarifaVigencia.getInformarMsgIcmsSubstituido().equals(Boolean.TRUE)) {
			if (tarifaVigencia.getBaseCalculoIcmsSubstituido() != null) {
				faixasTarifaVO.setVigenciaBaseCalculoIcms(Util.converterCampoValorDecimalParaString(BASE_CALCULO_ICMS_SUBSTITUÍDO ,
						tarifaVigencia.getBaseCalculoIcmsSubstituido(), Constantes.LOCALE_PADRAO));
			}
			if (tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
				faixasTarifaVO.setVigenciaIcmsSubstituidoMetroCubico(Util.converterCampoValorDecimalParaString("ICMS SubstituÃ­do ",
						tarifaVigencia.getIcmsSubstituidoMetroCubico(), Constantes.LOCALE_PADRAO));
			}
		}
	}

	private void setVigenciaNova(Model model, HttpSession session, boolean isVigenciaNova) {

		model.addAttribute("isVigenciaNova", isVigenciaNova);
		session.setAttribute("isVigenciaNova", isVigenciaNova);
	}

	private Long[] getIdTributosAssociadosTarifaVigencia(TarifaVigencia tarifaVigencia) {

		Collection<Tributo> listaTributo = new ArrayList<Tributo>();
		Long[] idTributosAssociados = Util.collectionParaArrayChavesPrimarias(listaTributo);
		if (tarifaVigencia.getTributos() != null && !tarifaVigencia.getTributos().isEmpty()) {
			for (TarifaVigenciaTributo tarifaTributo : tarifaVigencia.getTributos()) {
				listaTributo.add(tarifaTributo.getTributo());
			}
			idTributosAssociados = Util.collectionParaArrayChavesPrimarias(listaTributo);
		}

		return idTributosAssociados;
	}

	private void popularCamposSelectNoForm(Model model, TarifaVigencia tarifaVigencia) {

		Collection<Tributo> listaTributo = new ArrayList<Tributo>();
		model.addAttribute(TRIBUTOS_ASSOCIADOS, Util.collectionParaArrayChavesPrimarias(listaTributo));
		if (tarifaVigencia.getTributos() != null && !tarifaVigencia.getTributos().isEmpty()) {
			for (TarifaVigenciaTributo tarifaTributo : tarifaVigencia.getTributos()) {
				listaTributo.add(tarifaTributo.getTributo());
			}
			model.addAttribute(TRIBUTOS_ASSOCIADOS, Util.collectionParaArrayChavesPrimarias(listaTributo));
		}
	}

	private void popularTarifaVigencia(Model model, TarifaVigencia tarifaVigencia, TarifaFormVO tarifaFormVO)
			throws FormatoInvalidoException {

		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(statusPendente));

		tarifaVigencia.setStatus(status);

		if (!StringUtils.isEmpty(tarifaFormVO.getVigenciaBaseCalculoIcms())) {
			tarifaVigencia.setBaseCalculoIcmsSubstituido(Util.converterCampoStringParaValorBigDecimal(BASE_CALCULO_ICMS_SUBSTITUÍDO ,
					tarifaFormVO.getVigenciaBaseCalculoIcms(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(tarifaFormVO.getVigenciaIcmsSubstituidoMetroCubico())) {
			tarifaVigencia.setIcmsSubstituidoMetroCubico(Util.converterCampoStringParaValorBigDecimal(ICMS_SUBSTITUIDO,
					tarifaFormVO.getVigenciaIcmsSubstituidoMetroCubico(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isEmpty(tarifaFormVO.getVigenciaIcmsSubstituidoMetroCubico())) {
			tarifaVigencia.setMensagemIcmsSubstituto(null);
		}

		this.popularCamposSelectNoForm(model, tarifaVigencia);

		tarifaVigencia.setHabilitado(true);
		tarifaVigencia.setUltimaAlteracao(Calendar.getInstance().getTime());
	}

	/**
	 * Popular faixa desconto.
	 * 
	 * @param listaDadosFaixa - {@link List}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 */
	private void popularFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixa, TarifaFormVO tarifaFormVO) {

		if (listaDadosFaixa != null && !listaDadosFaixa.isEmpty()) {

			String[] valoresFixos = tarifaFormVO.getDescontoFixoSemImposto();
			String[] valoresVariaveis = tarifaFormVO.getDescontoVariavelSemImposto();
			Long idTipoDesconto = tarifaFormVO.getIdTipoDesconto();

			if (!listaDadosFaixa.isEmpty()) {

				int count = 0;
				for (DadosFaixasTarifa dadosFaixaTarifa : listaDadosFaixa) {

					String valorFixo;
					String valorVariavel;

					valorFixo = Util.getValorArray(valoresFixos, count);
					valorVariavel = Util.getValorArray(valoresVariaveis, count);

					dadosFaixaTarifa.setDescontoFixoSemImposto(valorFixo);
					dadosFaixaTarifa.setDescontoVariavelSemImposto(valorVariavel);
					dadosFaixaTarifa.setTipoDesconto(idTipoDesconto);

					count++;
				}
			}
		}
	}

	/**
	 * Popula uma faixa de uma vigência de tarifa.
	 * 
	 * @param listaDadosFaixa - {@link List}
	 * @param tarifaVigencia - {@link TarifaVigencia}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param dadosAuditoria - {@link DadosAuditoria}
	 * @param isAlteracao - {@link Boolean}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularTarifaVigenciaFaixa(List<DadosFaixasTarifa> listaDadosFaixa, TarifaVigencia tarifaVigencia,
			TarifaFormVO faixasTarifaVO, DadosAuditoria dadosAuditoria, Boolean isAlteracao)
			throws GGASException {

		this.popularLinhaNaoAdicionadaVigenciaFaixa(listaDadosFaixa, faixasTarifaVO);
		if (isAlteracao) {
			this.popularLinhasAdicionadasVigenciaFaixaAlteracao(listaDadosFaixa, tarifaVigencia, dadosAuditoria);
		} else {
			this.popularLinhasAdicionadasVigenciaFaixa(listaDadosFaixa, tarifaVigencia, dadosAuditoria);
		}

	}

	/**
	 * Popular linhas adicionadas vigencia faixa alteracao.
	 * 
	 * @param listaDadosFaixa - {@link List}
	 * @param dadosAuditoria - {@link DadosAuditoria}
	 * @throws GGASException - {@link GGASException}
	 */
	private void popularLinhasAdicionadasVigenciaFaixaAlteracao(List<DadosFaixasTarifa> listaDadosFaixa, TarifaVigencia tarifaVigencia,
			DadosAuditoria dadosAuditoria) throws GGASException {

		List<TarifaVigenciaFaixa> listaVigenciaFaixaBD = new ArrayList<TarifaVigenciaFaixa>(tarifaVigencia.getTarifaVigenciaFaixas());

		Collection<TarifaVigenciaFaixa> lstTarifaVigenciaFaixas = new HashSet<TarifaVigenciaFaixa>();

		int qtdeListaFaixaBD = listaVigenciaFaixaBD.size();
		int qtdeListaDadosFaixa = listaDadosFaixa.size();

		int count = 0;
		for (DadosFaixasTarifa dadosFaixa : listaDadosFaixa) {
			TarifaVigenciaFaixa tvf = null;
			int countAux = count + 1;
			if ((qtdeListaFaixaBD < qtdeListaDadosFaixa) && (countAux > qtdeListaFaixaBD)) {
				tvf = (TarifaVigenciaFaixa) controladorTarifa.criarTarifaVigenciaFaixa();
			} else {
				tvf = listaVigenciaFaixaBD.get(count);
			}

			if (!StringUtils.isEmpty(dadosFaixa.getFaixaInicial())) {
				tvf.setMedidaInicio(Util.converterCampoStringParaValorBigDecimal("Faixa Inicial", dadosFaixa.getFaixaInicial(),
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tvf.setMedidaInicio(null);
			}
			if (!StringUtils.isEmpty(dadosFaixa.getFaixaFinal())) {
				tvf.setMedidaFim(Util.converterCampoStringParaValorBigDecimal("Faixa Final", dadosFaixa.getFaixaFinal(),
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tvf.setMedidaFim(null);
			}
			if (!StringUtils.isEmpty(dadosFaixa.getValorFixoSemImposto())) {
				tvf.setValorFixo(Util.converterCampoStringParaValorBigDecimal("Valor Fixo Sem Imposto", dadosFaixa.getValorFixoSemImposto(),
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tvf.setValorFixo(null);
			}
			tvf.setValorCompra(definirValor("Valor de Compra", dadosFaixa.getValorCompra()));
			tvf.setMargemValorAgregado(definirValor("Margem de Valor Agregado", dadosFaixa.getMargemValorAgregado()));
			if (!StringUtils.isEmpty(dadosFaixa.getValorVariavelSemImposto())) {
				tvf.setValorVariavel(Util.converterCampoStringParaValorBigDecimal("Valor VariÃ¡vel Sem Imposto",
						dadosFaixa.getValorVariavelSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tvf.setValorVariavel(null);
			}
			tvf.setHabilitado(true);
			tvf.setUltimaAlteracao(Calendar.getInstance().getTime());
			tvf.setDadosAuditoria(dadosAuditoria);

			lstTarifaVigenciaFaixas.add(tvf);
			count++;
		}

		tarifaVigencia.getTarifaVigenciaFaixas().clear();
		tarifaVigencia.getTarifaVigenciaFaixas().addAll(lstTarifaVigenciaFaixas);
	}

	private void popularLinhaNaoAdicionadaVigenciaFaixa(List<DadosFaixasTarifa> listaDadosFaixa, TarifaFormVO faixasTarifaVO)
			throws GGASException {

		final String valorMaxFaixaFinal = VALOR_MAXIMO_FAIXA_FINAL;

		this.montarListaDadosFaixaTarifaOriginal(faixasTarifaVO, listaDadosFaixa);
		List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
		this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

		this.montarListaDadosFaixaTarifa(faixasTarifaVO, listaNova);
		controladorTarifa.validarFaixasZeradasTarifa(listaNova);
		for (DadosFaixasTarifa dadosFaixasTarifa : listaNova) {
			if ("0".equals(dadosFaixasTarifa.getFaixaFinal())) {
				dadosFaixasTarifa.setFaixaFinal(valorMaxFaixaFinal);
			}
		}
		this.obterListaDadosFaixaOrdenada(listaNova);
		listaNova.get(listaNova.size() - 1).setFaixaFinal(VALOR_MAXIMO_FAIXA_FINAL);

		controladorTarifa.validarAdicaoFaixaDesconto(listaNova, Boolean.TRUE);
		controladorTarifa.validarFaixasTarifa(listaNova, Boolean.TRUE);

		listaDadosFaixa.clear();
		listaDadosFaixa.addAll(listaNova);
	}

	private void popularLinhasAdicionadasVigenciaFaixa(List<DadosFaixasTarifa> listaDadosFaixa, TarifaVigencia tarifaVigencia,
			DadosAuditoria dadosAuditoria) throws GGASException {

		Collection<TarifaVigenciaFaixa> lstTarifaVigenciaFaixas = new HashSet<TarifaVigenciaFaixa>();
		for (DadosFaixasTarifa dadosFaixa : listaDadosFaixa) {

			TarifaVigenciaFaixa tarifaVigenciaFaixa = (TarifaVigenciaFaixa) controladorTarifa.criarTarifaVigenciaFaixa();
			if (!StringUtils.isEmpty(dadosFaixa.getFaixaInicial())) {
				tarifaVigenciaFaixa.setMedidaInicio(Util.converterCampoStringParaValorBigDecimal("Faixa Inicial",
						dadosFaixa.getFaixaInicial(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tarifaVigenciaFaixa.setMedidaInicio(null);
			}
			if (!StringUtils.isEmpty(dadosFaixa.getFaixaFinal())) {
				tarifaVigenciaFaixa.setMedidaFim(Util.converterCampoStringParaValorBigDecimal("Faixa Final", dadosFaixa.getFaixaFinal(),
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tarifaVigenciaFaixa.setMedidaFim(null);
			}
			if (!StringUtils.isEmpty(dadosFaixa.getValorFixoSemImposto())) {
				tarifaVigenciaFaixa.setValorFixo(Util.converterCampoStringParaValorBigDecimal("Valor Fixo Sem Imposto",
						dadosFaixa.getValorFixoSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tarifaVigenciaFaixa.setValorFixo(null);
			}
			tarifaVigenciaFaixa.setValorCompra(definirValor("Valor de Compra", dadosFaixa.getValorCompra()));
			tarifaVigenciaFaixa.setMargemValorAgregado(definirValor("Margem de Valor Agregado", dadosFaixa.getMargemValorAgregado()));
			if (!StringUtils.isEmpty(dadosFaixa.getValorVariavelSemImposto())) {
				tarifaVigenciaFaixa.setValorVariavel(Util.converterCampoStringParaValorBigDecimal("Valor VariÃ¡vel Sem Imposto",
						dadosFaixa.getValorVariavelSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
			} else {
				tarifaVigenciaFaixa.setValorVariavel(null);
			}
			tarifaVigenciaFaixa.setHabilitado(true);
			tarifaVigenciaFaixa.setUltimaAlteracao(Calendar.getInstance().getTime());
			tarifaVigenciaFaixa.setDadosAuditoria(dadosAuditoria);

			lstTarifaVigenciaFaixas.add(tarifaVigenciaFaixa);
		}

		tarifaVigencia.getTarifaVigenciaFaixas().clear();
		tarifaVigencia.getTarifaVigenciaFaixas().addAll(lstTarifaVigenciaFaixas);
	}

	private BigDecimal definirValor(String rotulo, String valor) throws FormatoInvalidoException {
		BigDecimal valorNumerico = null;
		if (!StringUtils.isEmpty(valor)) {
			valorNumerico =
					Util.converterCampoStringParaValorBigDecimal(rotulo, valor, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
		}
		return valorNumerico;
	}

	private void adicionarTributosEmTarifaVigencia(Long[] tributosAssociados, TarifaVigencia tarifaVigencia, DadosAuditoria dadosAuditoria)
			throws GGASException {

		Collection<TarifaVigenciaTributo> lstTarifaTributos = new ArrayList<TarifaVigenciaTributo>();

		if (tributosAssociados != null && tributosAssociados.length > 0) {
			for (Long idTributo : tributosAssociados) {
				TarifaVigenciaTributo tarifaTributo = (TarifaVigenciaTributo) controladorTarifa.criarTarifaTributo();

				Tributo tributo = controladorTributo.obterTributo(idTributo);
				tarifaTributo.setTributo(tributo);
				tarifaTributo.setHabilitado(Boolean.TRUE);
				tarifaTributo.setUltimaAlteracao(Calendar.getInstance().getTime());
				tarifaTributo.setDataVigenciaInicial(tarifaVigencia.getDataVigencia());

				tarifaTributo.setDadosAuditoria(dadosAuditoria);

				lstTarifaTributos.add(tarifaTributo);
			}
			tarifaVigencia.getTributos().clear();
			tarifaVigencia.getTributos().addAll(lstTarifaTributos);
		} else {
			tarifaVigencia.getTributos().clear();
		}
	}

	/**
	 * Método responsável por adicionar uma faixa na alteração de Tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("adicionarFaixaTarifaAlteracao")
	public String adicionarFaixaTarifaAlteracao(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, Model model, HttpSession session)
			throws ReflectiveOperationException {

		this.adicionarFaixaTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, tarifaFormVO, model, session);

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por adicionar uma faixa na inserção da Tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("adicionarFaixaTarifa")
	public String adicionarFaixaTarifaInsercao(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, BindingResult bindingResult, TarifaFormVO tarifaFormVO, Model model,
			HttpSession session) throws ReflectiveOperationException {

		this.adicionarFaixaTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, tarifaFormVO, model, session);

		return FORWARD_EXIBIR_TARIFA_APOS_INSERIR_FAIXA;
	}

	private void adicionarFaixaTarifa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, Model model, HttpSession session)
			throws ReflectiveOperationException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		try {
			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) session.getAttribute(LISTA_DADOS_FAIXA);
			if (listaDadosFaixa != null && !listaDadosFaixa.isEmpty()) {
				this.montarListaDadosFaixaTarifaOriginal(tarifaFormVO, listaDadosFaixa);
				List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
				this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);
				this.montarListaDadosFaixaTarifa(tarifaFormVO, listaNova);

				String ultimaFaixa = atualizarValoresFaixaTarifa(listaNova);

				controladorTarifa.validarFaixasZeradasTarifa(listaNova);
				controladorTarifa.validarAdicaoFaixaDesconto(listaNova, false);
				controladorTarifa.validarFaixasTarifa(listaNova, false);

				listaDadosFaixa.clear();
				listaDadosFaixa.addAll(listaNova);

				DadosFaixasTarifa novaFaixa = controladorTarifa.criarDadosFaixasTarifa();
				novaFaixa.setFaixaInicial(ultimaFaixa);

				if (tarifaFormVO.getUltimaFaixa() == null || tarifaFormVO.getUltimaFaixa() == 0) {
					listaDadosFaixa = controladorTarifa.adicionarFaixaDesconto(novaFaixa, listaDadosFaixa);
					Collections.sort(listaDadosFaixa, new Comparator<DadosFaixasTarifa>() {

						@Override
						public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

							return Long.valueOf(o1.getFaixaInicial()).compareTo(Long.valueOf(o2.getFaixaInicial()));
						}
					});
				} else {
					listaDadosFaixa = controladorTarifa.adicionarFaixaFinalDesconto(listaDadosFaixa, tarifaFormVO.getUltimaFaixa());
				}

				session.setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixa);

			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}
	}

	/**
	 * Método responsável por remover uma faixa de tarifa durante a alteração da tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param session - {@link HttpSession}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("removerFaixaTarifaAlteracao")
	public String removerFaixaTarifaAlteracao(TarifaImpl tarifa, TarifaFormVO tarifaFormVO, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, BindingResult bindingResult, HttpSession session, Model model)
			throws ReflectiveOperationException {

		this.removerFaixaTarifa(tarifa, tarifaFormVO, tarifaVigencia, tarifaVigenciaDesconto, session, model);

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por remover uma faixa de tarifa durante a inserção da tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param session - {@link HttpSession}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("removerFaixaTarifa")
	public String removerFaixaTarifaInsercao(TarifaImpl tarifa, TarifaFormVO tarifaFormVO, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, BindingResult bindingResult, HttpSession session, Model model)
			throws ReflectiveOperationException {

		this.removerFaixaTarifa(tarifa, tarifaFormVO, tarifaVigencia, tarifaVigenciaDesconto, session, model);

		return FORWARD_EXIBIR_TARIFA_APOS_INSERIR_FAIXA;
	}

	private void removerFaixaTarifa(TarifaImpl tarifa, TarifaFormVO tarifaFormVO, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, HttpSession session, Model model) throws ReflectiveOperationException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		try {
			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) session.getAttribute(LISTA_DADOS_FAIXA);

			this.montarListaDadosFaixaTarifaOriginal(tarifaFormVO, listaDadosFaixa);
			List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
			this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

			if (!StringUtils.isEmpty(tarifaFormVO.getFaixaInicialExclusao())) {
				this.removerItemDeListaFaixasDeTarifa(listaNova, tarifaFormVO.getFaixaInicialExclusao());
				this.montarListaDadosFaixaTarifa(listaNova);
				controladorTarifa.validarFaixasZeradasTarifa(listaNova);
				controladorTarifa.validarAdicaoFaixaDesconto(listaNova, Boolean.TRUE);
				controladorTarifa.validarFaixasTarifa(listaNova, Boolean.TRUE);
				listaDadosFaixa.clear();
				listaDadosFaixa.addAll(listaNova);
				atualizarValoresFaixaTarifa(listaDadosFaixa);
				session.setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixa);
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}
	}

	private void removerItemDeListaFaixasDeTarifa(List<DadosFaixasTarifa> listaDadosFaixaTarifa, String faixaInicialExclusao) {

		if (listaDadosFaixaTarifa != null && !listaDadosFaixaTarifa.isEmpty()) {
			Iterator<DadosFaixasTarifa> itFaixasTarifa = listaDadosFaixaTarifa.iterator();
			while (itFaixasTarifa.hasNext()) {
				DadosFaixasTarifa dadosFaixa = itFaixasTarifa.next();
				if (dadosFaixa.getFaixaInicial().equals(faixaInicialExclusao)) {
					itFaixasTarifa.remove();
				}
			}
		}
	}

	/**
	 * Método responsável por excluir um vigência de uma tarifa cadastrada.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("excluirVigenciaCadastrada")
	public String excluirVigenciaCadastrada(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);

		try {

			controladorTarifa.removerVigenciaCadastrada(tarifaFormVO.getIdVigenciaTarifa(), getDadosAuditoria(request));

			DadosTarifa dadosTarifa = controladorTarifa.montarDadosTarifa(tarifa.getChavePrimaria());

			tarifaFormVO.setChavePrimariaVigencia(dadosTarifa.getTarifaVigencia().getChavePrimaria());

			this.popularCamposVigenciaICMS(dadosTarifa.getTarifaVigencia(), tarifaFormVO);
			this.popularCamposSelectNoForm(model, dadosTarifa.getTarifaVigencia());

			if (dadosTarifa.getTarifaVigenciaDesconto() != null) {
				tarifaFormVO.setIdDescontoCadastrado(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());
				tarifaFormVO.setChavePrimariaVigenciaDesconto(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());
			}

			this.setarTipoDescontoForm(tarifaFormVO, dadosTarifa.getFaixasTarifa());

			model.addAttribute(TARIFA, dadosTarifa.getTarifa());
			model.addAttribute(TARIFA_VIGENCIA, dadosTarifa.getTarifaVigencia());
			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, dadosTarifa.getTarifaVigenciaDesconto());
			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

			request.getSession().setAttribute(LISTA_DADOS_FAIXA, dadosTarifa.getFaixasTarifa());
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, dadosTarifa.getFaixasDesconto());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por excluir um desconto cadastrado.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDescontoParam - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("excluirDescontoCadastrado")
	public String excluirDescontoCadastrado(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDescontoParam, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) throws ReflectiveOperationException {

		try {
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto = tarifaVigenciaDescontoParam;

			model.addAttribute(TARIFA, tarifa);
			model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

			final String valorMaxFaixaFinal = VALOR_MAXIMO_FAIXA_FINAL;

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_TARIFA, tarifa.getChavePrimaria());
			List<DadosFaixasTarifa> listaDadosFaixasTarifa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);

			this.montarListaDadosFaixaTarifaOriginal(tarifaFormVO, listaDadosFaixasTarifa);
			List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
			this.montarListaNovaDadosFaixa(listaDadosFaixasTarifa, listaNova);

			this.montarListaDadosFaixaTarifa(tarifaFormVO, listaNova);
			controladorTarifa.validarFaixasZeradasTarifa(listaNova);
			for (DadosFaixasTarifa dadosFaixasTarifa : listaNova) {
				if ("0".equals(dadosFaixasTarifa.getFaixaFinal())) {
					dadosFaixasTarifa.setFaixaFinal(valorMaxFaixaFinal);
				}
			}

			this.atualizarValoresFaixaTarifa(listaNova);
			listaNova.get(listaNova.size() - 1).setFaixaFinal(VALOR_MAXIMO_FAIXA_FINAL);

			controladorTarifa.validarAdicaoFaixaDesconto(listaNova, Boolean.TRUE);
			controladorTarifa.validarFaixasTarifa(listaNova, Boolean.TRUE);

			listaDadosFaixasTarifa.clear();
			listaDadosFaixasTarifa.addAll(listaNova);

			controladorTarifa.removerDescontoCadastrado(tarifaFormVO.getIdDescontoCadastrado(), getDadosAuditoria(request));

			for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixasTarifa) {
				dadosFaixasTarifa.setDescontoFixoSemImposto("");
				dadosFaixasTarifa.setDescontoVariavelSemImposto("");
			}

			Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = controladorTarifa.consultarTarifasVigenciaDesconto(filtro);
			tarifaVigenciaDesconto = (TarifaVigenciaDescontoImpl) this.carregarTarifaVigenciaDesconto(listaTarifaVigenciaDesconto);
			List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa =
					controladorTarifa.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tarifaVigenciaDesconto);
			this.setarTipoDescontoForm(tarifaFormVO, listaDadosFaixasDescontoTarifa);
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixasDescontoTarifa);

			this.limparTarifaVigenciaDesconto(model, tarifaFormVO);

			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(TarifaVigenciaDesconto.TARIFA_DESCONTO));

			this.aplicarFaixasTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, tarifaFormVO, request.getSession(), model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por aplicar as faixas de tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param impressaoDesconto - {@link Boolean}
	 * @param acaoAlterarTarifa - {@link Boolean}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("aplicarFaixasTarifa")
	public String aplicarFaixasTarifa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia, TarifaFormVO tarifaFormVO,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, BindingResult bindingResult,
			@RequestParam(value = "impressaoDesconto", required = false, defaultValue = "true") Boolean impressaoDesconto,
			@RequestParam("acaoAlterarTarifa") Boolean acaoAlterarTarifa, HttpServletRequest request, Model model)
			throws ReflectiveOperationException {

		String view = "";

		tarifaVigenciaDesconto.setImpressaoDesconto(impressaoDesconto);

		this.aplicarFaixasTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, tarifaFormVO, request.getSession(), model);

		if (acaoAlterarTarifa != null && acaoAlterarTarifa) {
			view = this.exibirTarifaAposAlterarFaixa(tarifa, tarifaFormVO, bindingResult, model, request);
		} else {
			view = this.exibirTarifaAposInserirFaixa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, tarifaFormVO, bindingResult, model,
					request);
		}

		return view;
	}

	private void aplicarFaixasTarifa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, HttpSession session, Model model)
			throws ReflectiveOperationException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);

		try {
			if (tarifa.getChavePrimaria() > 0) {
				Boolean isTarifaUtilizada = controladorTarifa.verificaSeTarifaEstaSendoUtilizada(tarifa.getChavePrimaria());
				model.addAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
				session.setAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
			}

			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) session.getAttribute(LISTA_DADOS_FAIXA);

			final String valorMaxFaixaFinal = VALOR_MAXIMO_FAIXA_FINAL;

			this.montarListaDadosFaixaTarifaOriginal(tarifaFormVO, listaDadosFaixa);
			List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
			this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

			this.montarListaDadosFaixaTarifa(tarifaFormVO, listaNova);
			controladorTarifa.validarFaixasZeradasTarifa(listaNova);
			for (DadosFaixasTarifa dadosFaixasTarifa : listaNova) {
				if ("0".equals(dadosFaixasTarifa.getFaixaFinal())) {
					dadosFaixasTarifa.setFaixaFinal(valorMaxFaixaFinal);
				}
			}
			this.atualizarValoresFaixaTarifa(listaNova);
			listaNova.get(listaNova.size() - 1).setFaixaFinal(VALOR_MAXIMO_FAIXA_FINAL);

			controladorTarifa.validarAdicaoFaixaDesconto(listaNova, Boolean.TRUE);
			controladorTarifa.validarFaixasTarifa(listaNova, Boolean.TRUE);

			listaDadosFaixa.clear();
			listaDadosFaixa.addAll(listaNova);

			session.setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixa);
			session.setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixa);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}
	}

	/**
	 * Método responsável por carregar uma vigência de tarifa cadastrada.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("carregarVigenciaTarifaCadastrada")
	public String carregarVigenciaTarifaCadastrada(TarifaImpl tarifa, TarifaVigenciaDescontoImpl tarifaVigenciaDesconto,
			TarifaFormVO tarifaFormVO, BindingResult bindingResult, @RequestParam("chavePrimaria") Long chavePrimaria, Model model,
			HttpServletRequest request) throws GGASException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		
		HttpSession session = request.getSession();
		
		if (tarifaFormVO.getIdVigenciaTarifa() != null && tarifaFormVO.getIdVigenciaTarifa() > 0) {
			TarifaVigencia tarifaVigencia =
					controladorTarifa.obterTarifaVigencia(tarifaFormVO.getIdVigenciaTarifa(), TARIFA, TARIFA_VIGENCIA_FAIXAS);

			this.popularCamposSelectNoForm(model, tarifaVigencia);
			Long[] idTributosAssociados = this.getIdTributosAssociadosTarifaVigencia(tarifaVigencia);

			this.popularCamposVigenciaICMS(tarifaVigencia, tarifaFormVO);
			tarifaFormVO.setTributosAssociados(idTributosAssociados);
			tarifaFormVO.setChavePrimariaVigencia(tarifaVigencia.getChavePrimaria());

			List<DadosFaixasTarifa> listaDadosFaixasTarifa = controladorTarifa.popularDadosTarifaVigenciaFaixa(tarifaVigencia);
			request.getSession().setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixasTarifa);

			setVigenciaNova(model, session, false);

			model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);

			DadosTarifa dadosTarifa =
					controladorTarifa.carregarDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tarifaVigencia.getChavePrimaria());
			if (dadosTarifa.getTarifaVigenciaDesconto() != null) {
				model.addAttribute(TARIFA_VIGENCIA_DESCONTO, dadosTarifa.getTarifaVigenciaDesconto());

				tarifaFormVO.setIdDescontoCadastrado(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());
				tarifaFormVO.setChavePrimariaVigenciaDesconto(dadosTarifa.getTarifaVigenciaDesconto().getChavePrimaria());

				this.setarTipoDescontoForm(tarifaFormVO, dadosTarifa.getFaixasDesconto());
				session.setAttribute(LISTA_DESCONTOS_FAIXA, dadosTarifa.getFaixasDesconto());
			} else {
				this.limparTarifaVigenciaDesconto(model, tarifaFormVO);
				session.removeAttribute(LISTA_DESCONTOS_FAIXA);
			}
		} else {
			session.removeAttribute(LISTA_DADOS_FAIXA);
			session.removeAttribute(LISTA_DESCONTOS_FAIXA);
			this.limparTarifaVigencia(model, tarifaFormVO);
			this.carregarListasIniciaisFaixasDescontos(session);
			setVigenciaNova(model, session, true);
		}

		if (tarifaFormVO.getIdVigenciaTarifa() != null) {
			model.addAttribute(LISTA_DESCONTOS_CADASTRADOS,
					controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(tarifaFormVO.getIdVigenciaTarifa()));
		}

		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);
		model.addAttribute(LISTA_VIGENCIAS_TARIFA, controladorTarifa.consultarVigenciasTarifa(chavePrimaria));

		return this.exibirTarifaAposAlterarFaixa(tarifa, tarifaFormVO, bindingResult, model, request);
	}

	/**
	 * Método responsável por carregar um desconto aplicado a uma tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("carregarDescontoCadastrado")
	public String carregarDescontoCadastrado(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia, TarifaFormVO tarifaFormVO,
			BindingResult bindingResult, @RequestParam("chavePrimaria") Long chavePrimaria, Model model, HttpServletRequest request)
			throws GGASException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		DadosTarifa dadosTarifa = controladorTarifa.montarDadosTarifa(chavePrimaria);
		List<DadosFaixasTarifa> listaDadosFaixasTarifa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", tarifaFormVO.getIdDescontoCadastrado());
		Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = controladorTarifa.consultarTarifasVigenciaDesconto(filtro);

		TarifaVigenciaDesconto tarifaVigenciaDesconto = this.carregarTarifaVigenciaDesconto(listaTarifaVigenciaDesconto);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		tarifaFormVO.setChavePrimariaVigenciaDesconto(tarifaVigenciaDesconto.getChavePrimaria());

		if (tarifaFormVO.getIdDescontoCadastrado() != null && tarifaFormVO.getIdDescontoCadastrado() > 0) {

			List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa =
					controladorTarifa.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tarifaVigenciaDesconto);

			this.setarTipoDescontoForm(tarifaFormVO, listaDadosFaixasDescontoTarifa);
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixasDescontoTarifa);
			model.addAttribute(LISTA_DESCONTOS_CADASTRADOS,
					controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(tarifaFormVO.getIdVigenciaTarifa()));
		} else {
			this.limparTarifaVigenciaDesconto(model, tarifaFormVO);
			List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa =
					controladorTarifa.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa);
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixasDescontoTarifa);
			model.addAttribute(LISTA_DESCONTOS_CADASTRADOS,
					controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(tarifaFormVO.getIdVigenciaTarifa()));
		}

		model.addAttribute(LISTA_VIGENCIAS_TARIFA, controladorTarifa.consultarVigenciasTarifa(chavePrimaria));
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		Boolean isTarifaUtilizada = controladorTarifa.verificaSeTarifaEstaSendoUtilizada(chavePrimaria);
		model.addAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
		request.getSession().setAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);
		model.addAttribute(DATA_VIGENCIA,
				Util.converterDataParaStringSemHora(dadosTarifa.getTarifaVigencia().getDataVigencia(), Constantes.FORMATO_DATA_BR));

		return this.exibirTarifaAposAlterarFaixa(tarifa, tarifaFormVO, bindingResult, model, request);
	}

	private TarifaVigenciaDesconto carregarTarifaVigenciaDesconto(Collection<TarifaVigenciaDesconto> tarifasVigenciasDesconto) {

		TarifaVigenciaDesconto tarifaVigenciaDesconto = null;
		if (tarifasVigenciasDesconto != null && !tarifasVigenciasDesconto.isEmpty()) {

			List<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = new ArrayList<TarifaVigenciaDesconto>(tarifasVigenciasDesconto);
			tarifaVigenciaDesconto = listaTarifaVigenciaDesconto.get(0);
		}
		return tarifaVigenciaDesconto;
	}

	private void limparTarifaVigencia(Model model, TarifaFormVO faixasTarifaVO) {

		TarifaVigencia tarifaVigencia = new TarifaVigenciaImpl();

		tarifaVigencia.setChavePrimaria(0L);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);

		faixasTarifaVO.setIdVigenciaTarifa(0L);
		faixasTarifaVO.setChavePrimariaVigencia(0L);

		faixasTarifaVO.setVigenciaBaseCalculoIcms("");
		faixasTarifaVO.setVigenciaIcmsSubstituidoMetroCubico("");
		faixasTarifaVO.setTributosAssociados(null);
	}

	private void limparTarifaVigenciaDesconto(Model model, TarifaFormVO tarifaFormVO) {

		TarifaVigenciaDescontoImpl tarifaVigenciaDesconto = new TarifaVigenciaDescontoImpl();

		tarifaVigenciaDesconto.setImpressaoDesconto(Boolean.TRUE);
		tarifaVigenciaDesconto.setImpressaoDescricao(Boolean.FALSE);

		tarifaFormVO.setIdTipoDesconto(0L);
		tarifaFormVO.setChavePrimariaVigenciaDesconto(0L);

		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
	}

	/**
	 * Método responsável por exibir a tarifa após inserir uma faixa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirTarifaAposInserirFaixa")
	public String exibirTarifaAposInserirFaixa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		this.modificarDadosFaixaTarifa(tarifa, tarifaFormVO.getIdVigenciaTarifa(), tarifaFormVO.getTributosAssociados(), model, request);

		return EXIBIR_INSERCAO_TARIFA;
	}

	/**
	 * Método responsável por exibir a tarifa após realizar a alteração da faixa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirTarifaAposAlterarFaixa")
	public String exibirTarifaAposAlterarFaixa(TarifaImpl tarifa, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) {

		this.modificarDadosFaixaTarifa(tarifa, tarifaFormVO.getIdVigenciaTarifa(), tarifaFormVO.getTributosAssociados(), model, request);

		return "exibirAlteracaoTarifa";
	}

	private void modificarDadosFaixaTarifa(TarifaImpl tarifa, Long idVigenciaTarifa, Long[] idTributosAssociados, Model model,
			HttpServletRequest request) {

		try {
			saveToken(request);
			
			this.carregarCampos(model, tarifa);
			this.tratarCamposSelect(idTributosAssociados, model);

			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);
			if (listaDadosFaixa == null || listaDadosFaixa.isEmpty()) {
				List<DadosFaixasTarifa> listaPrimeiraFaixa = new ArrayList<DadosFaixasTarifa>();
				DadosFaixasTarifa dadosFaixa = controladorTarifa.criarDadosFaixasTarifa();
				dadosFaixa.setFaixaInicial("0");
				listaPrimeiraFaixa.add(dadosFaixa);
				request.getSession().setAttribute(LISTA_DADOS_FAIXA, listaPrimeiraFaixa);
			}

			model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
			model.addAttribute(LISTA_TIPO_VALOR, controladorEntidadeConteudo.obterListaTipoValorTarifa());
			this.carregarListasVigencias(model, tarifa.getChavePrimaria(), idVigenciaTarifa);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}
	}

	/**
	 * Método responsável por reajustar as faixas de uma tarifa.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("reajustarFaixaTarifa")
	public String reajustarFaixaTarifa(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpSession session) throws GGASException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		Integer qtdCasasDecimais = null;
		if (tarifa.getQuantidadeCasaDecimal() != null) {
			qtdCasasDecimais = tarifa.getQuantidadeCasaDecimal().intValue();
		} else {
			qtdCasasDecimais = Integer.valueOf(2);
		}

		controladorTarifa.validarCamposReajuste(tarifaFormVO.getValorReajuste(), tarifaFormVO.getIdTipoReajuste());
		Collection<DadosFaixasTarifa> listaDadosFaixa = (Collection<DadosFaixasTarifa>) session.getAttribute(LISTA_DADOS_FAIXA);
		Collection<DadosFaixasTarifa> listaDescontosFaixa = (Collection<DadosFaixasTarifa>) session.getAttribute(LISTA_DESCONTOS_FAIXA);

		String[] vlFixo = tarifaFormVO.getValorFixoSemImposto();
		String[] vlCompra = tarifaFormVO.getValorCompraEscondido();
		String[] vlMargemValorAgregado = tarifaFormVO.getMargemValorAgregadoEscondido();
		String[] vlVariavel = tarifaFormVO.getValorVariavelSemImpostoEscondido();

		for (int i = 0; i < vlFixo.length; i++) {
			DadosFaixasTarifa dadosFaixasTarifa = (DadosFaixasTarifa) listaDadosFaixa.toArray()[i];

			dadosFaixasTarifa.setValorFixoSemImposto(vlFixo[i]);
			dadosFaixasTarifa.setValorCompra(vlCompra[i]);
			dadosFaixasTarifa.setMargemValorAgregado(vlMargemValorAgregado[i]);
			dadosFaixasTarifa.setValorVariavelSemImposto(vlVariavel[i]);

			if (CollectionUtils.isNotEmpty(listaDescontosFaixa)) {
				DadosFaixasTarifa dadosDescontosTarifa = (DadosFaixasTarifa) listaDescontosFaixa.toArray()[i];

				dadosDescontosTarifa.setValorFixoSemImposto(vlFixo[i]);
				dadosDescontosTarifa.setValorCompra(vlCompra[i]);
				dadosDescontosTarifa.setMargemValorAgregado(vlMargemValorAgregado[i]);
				dadosDescontosTarifa.setValorVariavelSemImposto(vlVariavel[i]);
			}
		}

		controladorTarifa.reajustarFaixaTarifa(tarifaFormVO.getValorReajuste(), tarifaFormVO.getIdTipoReajuste(), listaDadosFaixa, null,
				qtdCasasDecimais);
		controladorTarifa.reajustarFaixaTarifa(tarifaFormVO.getValorReajuste(), tarifaFormVO.getIdTipoReajuste(), listaDescontosFaixa, null,
				qtdCasasDecimais);

		session.setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixa);
		session.setAttribute(LISTA_DESCONTOS_FAIXA, listaDescontosFaixa);

		Boolean isTarifaUtilizada = (Boolean) session.getAttribute(IS_TARIFA_UTILIZADA);
		model.addAttribute(IS_TARIFA_UTILIZADA, isTarifaUtilizada);

		model.addAttribute(DATA_ATUAL, Util.converterDataParaString(Calendar.getInstance().getTime()));

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por clonar um vigência de uma tarifa na tela de alteração de tarifas.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaVigencia - {@link TarifaVigenciaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("clonarFaixaTarifaAlteracao")
	public String clonarFaixaTarifaAlteracao(TarifaImpl tarifa, TarifaVigenciaImpl tarifaVigencia, TarifaFormVO tarifaFormVO,
			BindingResult bindingResult, Model model, HttpServletRequest request) throws GGASException {

		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		this.clonarFaixaTarifa(tarifa, tarifaFormVO, model, request);

		return FORWARD_EXIBIR_TARIFA_APOS_ALTERAR_FAIXA;
	}

	/**
	 * Método responsável por clonar uma vigência de uma tarifa na tela de inserção de tarifas.
	 * 
	 * @param tarifa - {@link TarifaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("clonarFaixaTarifa")
	public String clonarFaixaTarifaInsercao(TarifaImpl tarifa, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) throws GGASException {

		this.clonarFaixaTarifa(tarifa, tarifaFormVO, model, request);

		return FORWARD_EXIBIR_TARIFA_APOS_INSERIR_FAIXA;
	}

	/**
	 * Método responsável cadastrar descontos de uma tarifa.
	 * 
	 * @param tarifaParam - {@link TarifaImpl}
	 * @param tarifaVigenciaParam - {@link TarifaVigenciaImpl}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDescontoImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 */
	@RequestMapping("cadastrarDescontos")
	public String cadastrarDescontos(TarifaImpl tarifaParam, TarifaVigenciaImpl tarifaVigenciaParam,
			TarifaVigenciaDescontoImpl tarifaVigenciaDesconto, TarifaFormVO tarifaFormVO, BindingResult bindingResult, Model model,
			HttpServletRequest request) throws ReflectiveOperationException {

		TarifaImpl tarifa = tarifaParam;
		TarifaVigencia tarifaVigencia = tarifaVigenciaParam;

		try {
			model.addAttribute(TARIFA, tarifa);
			model.addAttribute(TARIFA_VIGENCIA, tarifaVigencia);
			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

			tarifa = (TarifaImpl) controladorTarifa.obter(tarifa.getChavePrimaria(), SEGMENTO, "itemFatura", RAMO_ATIVIDADE, TIPO_CONTRATO);
			tarifaVigencia = controladorTarifa.obterTarifaVigencia(tarifaFormVO.getIdVigenciaTarifa(), TARIFA_VIGENCIA_FAIXAS);

			tarifaVigenciaDesconto.setTarifa(tarifa);
			tarifaVigenciaDesconto.setTarifaVigencia(tarifaVigencia);
			tarifaVigenciaDesconto.setDadosAuditoria(getDadosAuditoria(request));

			List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DESCONTOS_FAIXA);

			Collections.sort(listaDadosFaixa, new Comparator<DadosFaixasTarifa>() {

				@Override
				public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

					return Long.valueOf(o1.getFaixaInicial()).compareTo(Long.valueOf(o2.getFaixaInicial()));
				}
			});

			this.popularFaixaDesconto(listaDadosFaixa, tarifaFormVO);

			List<DadosFaixasTarifa> listaNova = new ArrayList<DadosFaixasTarifa>();
			this.montarListaNovaDadosFaixa(listaDadosFaixa, listaNova);

			controladorTarifa.cadastrarDescontos(tarifaVigencia, tarifaVigenciaDesconto, listaNova);

			listaDadosFaixa.clear();
			listaDadosFaixa.addAll(listaNova);

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_TARIFA, tarifa.getChavePrimaria());

			Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = controladorTarifa.consultarTarifasVigenciaDesconto(filtro);
			TarifaVigenciaDesconto tvd = this.carregarTarifaVigenciaDesconto(listaTarifaVigenciaDesconto);

			if (tvd != null) {
				tarifaFormVO.setIdDescontoCadastrado(tvd.getChavePrimaria());
				tarifaFormVO.setChavePrimariaVigenciaDesconto(tvd.getChavePrimaria());
			}

			List<DadosFaixasTarifa> listaDadosFaixasTarifa = (List<DadosFaixasTarifa>) request.getSession().getAttribute(LISTA_DADOS_FAIXA);
			List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa =
					controladorTarifa.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tvd);
			this.setarTipoDescontoForm(tarifaFormVO, listaDadosFaixasDescontoTarifa);
			this.carregarListasVigencias(model, tarifa.getChavePrimaria(), tarifaFormVO.getIdVigenciaTarifa());

			model.addAttribute(TARIFA_VIGENCIA_DESCONTO, tarifaVigenciaDesconto);
			model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);
			request.getSession().setAttribute(LISTA_DESCONTOS_FAIXA, listaDadosFaixasDescontoTarifa);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, super.obterMensagem(TarifaVigenciaDesconto.TARIFA_DESCONTO));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirTarifaAposAlterarFaixa(tarifa, tarifaFormVO, bindingResult, model, request);
	}

	private void clonarFaixaTarifa(TarifaImpl tarifa, TarifaFormVO faixasTarifaVO, Model model, HttpServletRequest request)
			throws GGASException {

		try {

			validarToken(request);

			controladorTarifa.validarSelecaoVigencia(faixasTarifaVO.getIdVigenciasTarifa());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chaveTarifaVigencia", faixasTarifaVO.getIdVigenciasTarifa());
			filtro.put("ordenadoPor", "medidaInicio");

			Collection<TarifaVigenciaFaixa> tarifaVigenciaFaixa = controladorTarifa.consultarTarifasVigenciaFaixa(filtro);

			Integer quantidadeCasasDecimais = null;
			if (tarifa.getQuantidadeCasaDecimal() != null) {
				quantidadeCasasDecimais = tarifa.getQuantidadeCasaDecimal().intValue();
			}

			carregarGridFaixaVigencia(tarifaVigenciaFaixa, request.getSession(), quantidadeCasasDecimais);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}
	}

	/**
	 * Carregar grid faixa vigencia.
	 * 
	 * @param listaVigenciaFaixa - {@link Collection}
	 * @param session - {@link HttpSession}
	 * @param quantidadeCasasDecimais - {@link Integer}
	 * @throws GGASException - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void carregarGridFaixaVigencia(Collection<TarifaVigenciaFaixa> listaVigenciaFaixa, HttpSession session,
			Integer quantidadeCasasDecimais) throws GGASException {

		List<DadosFaixasTarifa> listaDadosFaixa = (List<DadosFaixasTarifa>) session.getAttribute(LISTA_DADOS_FAIXA);

		int qtdCasasDecimais = 2;
		if (quantidadeCasasDecimais != null) {
			qtdCasasDecimais = quantidadeCasasDecimais;
		}

		if (!listaVigenciaFaixa.isEmpty()) {

			listaDadosFaixa.clear();
			Integer contador = 0;
			for (TarifaVigenciaFaixa tarifaVigenciaFaixa : listaVigenciaFaixa) {
				DadosFaixasTarifa dadosFaixasTarifa = controladorTarifa.criarDadosFaixasTarifa();

				dadosFaixasTarifa.setFaixaInicial(tarifaVigenciaFaixa.getMedidaInicio().toString());
				dadosFaixasTarifa.setFaixaFinal(tarifaVigenciaFaixa.getMedidaFim().toString());
				dadosFaixasTarifa.setValorFixoSemImposto(Util.converterCampoValorDecimalParaString("Valor Fixo",
						tarifaVigenciaFaixa.getValorFixo().setScale(qtdCasasDecimais, RoundingMode.DOWN), Constantes.LOCALE_PADRAO,
						qtdCasasDecimais));
				dadosFaixasTarifa.setValorCompra(tarifaVigenciaFaixa.getValorCompraFormatado(qtdCasasDecimais));
				dadosFaixasTarifa.setMargemValorAgregado(tarifaVigenciaFaixa.getMargemValorAgregadoFormatado(qtdCasasDecimais));
				dadosFaixasTarifa.setValorVariavelSemImposto(Util.converterCampoValorDecimalParaString("Valor VariÃ¡vel",
						tarifaVigenciaFaixa.getValorVariavel().setScale(qtdCasasDecimais, RoundingMode.DOWN), Constantes.LOCALE_PADRAO,
						qtdCasasDecimais));
				dadosFaixasTarifa.setNumeroColunaFinal(contador);
				listaDadosFaixa.add(dadosFaixasTarifa);
				contador++;
			}

			List<DadosFaixasTarifa> listaDadosFaixaCopia = new ArrayList<DadosFaixasTarifa>(listaDadosFaixa);

			listaDadosFaixa = controladorTarifa.ordenarFaixasPorVO(listaDadosFaixaCopia, listaDadosFaixa);

			session.setAttribute(LISTA_DADOS_FAIXA, listaDadosFaixa);

		}
	}

	private String atualizarValoresFaixaTarifa(List<DadosFaixasTarifa> listaNova) {

		final String zero = "0";

		String faixaNovaInicio = zero;
		this.obterListaDadosFaixaOrdenada(listaNova);

		for (DadosFaixasTarifa dados : listaNova) {
			dados.setFaixaInicial(faixaNovaInicio);
			if (dados.getFaixaFinal() != null) {
				faixaNovaInicio = dados.getFaixaFinal().replace(".", "");
				faixaNovaInicio = new BigInteger(faixaNovaInicio).add(BigInteger.ONE).toString();
			}
		}
		return faixaNovaInicio;
	}

	private void obterListaDadosFaixaOrdenada(List<DadosFaixasTarifa> listaNova) {

		if (listaNova != null && !listaNova.isEmpty()) {

			Collections.sort(listaNova, new Comparator<DadosFaixasTarifa>() {

				@Override
				public int compare(DadosFaixasTarifa dados1, DadosFaixasTarifa dados2) {

					if ((dados1.getFaixaFinal() != null) && (dados2.getFaixaFinal() != null)) {
						return Long.valueOf(Util.removerVirgulasPontos(dados1.getFaixaFinal()))
								.compareTo(Long.valueOf(Util.removerVirgulasPontos(dados2.getFaixaFinal())));
					} else if (dados1.getFaixaFinal() != null) {
						return 1;
					} else {
						return -1;
					}
				}
			});

		}
	}

	private void carregarListasVigencias(Model model, Long chavePrimaria, Long idVigenciaTarifa) throws GGASException {

		if (idVigenciaTarifa != null && idVigenciaTarifa > 0) {
			model.addAttribute(LISTA_DESCONTOS_CADASTRADOS,
					controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(idVigenciaTarifa));
		}

		if (chavePrimaria != null && chavePrimaria > 0) {
			model.addAttribute(LISTA_VIGENCIAS_TARIFA, controladorTarifa.consultarVigenciasTarifa(chavePrimaria));
		}
	}

	private void montarListaDadosFaixaTarifaOriginal(TarifaFormVO faixasTarifaVO, List<DadosFaixasTarifa> listaNova) {

		int i = 0;

		for (DadosFaixasTarifa dados : listaNova) {

			String faixaFinal;

			if ("".equals(faixasTarifaVO.getFaixaFinal()[i])) {
				faixaFinal = "";
			} else {
				faixaFinal = faixasTarifaVO.getFaixaFinal()[i].replace(".", "");
			}

			dados.setFaixaFinal(faixaFinal);
			dados.setValorFixoSemImposto(faixasTarifaVO.getValorFixoSemImposto()[i]);
			dados.setValorCompra(faixasTarifaVO.getValorCompraEscondido()[i]);
			dados.setMargemValorAgregado(faixasTarifaVO.getMargemValorAgregadoEscondido()[i]);
			dados.setValorVariavelSemImposto(faixasTarifaVO.getValorVariavelSemImpostoEscondido()[i]);
			i++;
		}
	}

	private void montarListaNovaDadosFaixa(List<DadosFaixasTarifa> listaDadosFaixa, List<DadosFaixasTarifa> listaNova)
			throws GGASException {

		for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
			DadosFaixasTarifa dadosFaixa = controladorTarifa.criarDadosFaixasTarifa();
			Util.copyProperties(dadosFaixa, dadosFaixasTarifa);

			listaNova.add(dadosFaixa);
		}
	}

	private void montarListaDadosFaixaTarifa(List<DadosFaixasTarifa> listaNova) {

		for (DadosFaixasTarifa dados : listaNova) {

			if (dados.getFaixaFinal() == null || "".equals(dados.getFaixaFinal())) {
				dados.setFaixaFinal("0");
			}
			if (dados.getValorFixoSemImposto() == null || "".equals(dados.getValorFixoSemImposto())) {
				dados.setValorFixoSemImposto("0");
			}
			if (dados.getValorVariavelSemImposto() == null || "".equals(dados.getValorVariavelSemImposto())) {
				dados.setValorVariavelSemImposto("0");
			}
		}
	}

	private void montarListaDadosFaixaTarifa(TarifaFormVO faixasTarifaVO, List<DadosFaixasTarifa> listaNova) {

		int i = 0;

		for (DadosFaixasTarifa dados : listaNova) {

			String faixaFinal;
			if ("".equals(faixasTarifaVO.getFaixaFinal()[i])) {
				faixaFinal = "0";
			} else {
				faixaFinal = faixasTarifaVO.getFaixaFinal()[i].replace(".", "");
			}

			String valorFixoSemImposto = faixasTarifaVO.getValorFixoSemImposto()[i];
			String valorVariavelSemImposto = faixasTarifaVO.getValorVariavelSemImpostoEscondido()[i];

			if ("".equals(valorFixoSemImposto.trim())) {
				valorFixoSemImposto = "0,00";
			}

			if ("".equals(valorVariavelSemImposto.trim())) {
				valorVariavelSemImposto = "0,00";
			}

			dados.setFaixaFinal(faixaFinal);

			if ("".equals(valorFixoSemImposto.trim())) {
				valorFixoSemImposto = "0,00";
			}

			if ("".equals(valorVariavelSemImposto.trim())) {
				valorVariavelSemImposto = "0,00";
			}

			dados.setValorFixoSemImposto(valorFixoSemImposto);
			dados.setValorVariavelSemImposto(valorVariavelSemImposto);
			i++;
		}
	}

	private void carregarCampos(Model model, Tarifa tarifa) throws GGASException {
		Long idSegmento = null;

		Segmento segmento = tarifa.getSegmento();

		if (segmento != null) {
			idSegmento = segmento.getChavePrimaria();
		}

		this.carregarCampos(model, tarifa.getChavePrimaria(), idSegmento);
	}

	private void carregarCampos(Model model, Long chavePrimariaTarifa, Long idSegmento) throws GGASException {

		if (chavePrimariaTarifa > 0 && idSegmento == null) {
			DadosTarifa dadosTarifa = controladorTarifa.montarDadosTarifa(chavePrimariaTarifa);
			if (dadosTarifa != null && dadosTarifa.getTarifa() != null && dadosTarifa.getTarifa().getSegmento() != null) {
				model.addAttribute(ID_SEGMENTO, dadosTarifa.getTarifa().getSegmento().getChavePrimaria());
			}
		}

		model.addAttribute(LISTA_TARIFA, controladorTarifa.listarTarifas());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());

		if (idSegmento != null) {
			model.addAttribute(LISTA_RAMO_ATIVIDADE, controladorRamoAtividade.listarRamoAtividadePorSegmento(idSegmento));
		}

		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFatura());
		model.addAttribute(LISTA_TIPO_CALCULO, controladorEntidadeConteudo.obterListaTipoCalculo());
		model.addAttribute(LISTA_BASE_CALCULO, controladorEntidadeConteudo.obterListaBaseApuracao());
		model.addAttribute(LISTA_UNIDADE_MONETARIA, controladorEntidadeConteudo.obterListaUnidadeMonetaria());
		model.addAttribute(LISTA_TIPO_VALOR, controladorEntidadeConteudo.obterListaTipoValorTarifa());
		
		Map<String, Object> filtro = new HashMap<String, Object>();

		model.addAttribute(LISTA_CLONE_FAIXA, controladorTarifa.consultarTarifas(filtro));
		filtro.put("indicadorProduto", Boolean.TRUE);
		model.addAttribute(LISTA_TRIBUTOS, controladorTributo.consultarTributos(filtro));
		String idTipoPercentual = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL);
		model.addAttribute("idTipoPercentual", idTipoPercentual);
		model.addAttribute("listaTipoContrato", controladorEntidadeConteudo.obterListaTipoModelo());
	}

	private void tratarCamposSelect(Long[] idTributosAssociados, Model model) throws GGASException {

		if (idTributosAssociados != null && idTributosAssociados.length > 0) {
			Collection<Tributo> listaTributosAssociados = new ArrayList<Tributo>();
			for (Long chaveSegmento : idTributosAssociados) {
				Tributo tributo = controladorTributo.obterTributo(chaveSegmento);
				listaTributosAssociados.add(tributo);
			}
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("indicadorProduto", Boolean.TRUE);
			Collection<Tributo> listaTributoDisp = controladorTributo.consultarTributos(filtro);
			listaTributoDisp.removeAll(listaTributosAssociados);
			model.addAttribute(LISTA_TRIBUTOS, listaTributoDisp);
			model.addAttribute(LISTA_TRIBUTOS_ASSOCIADOS, listaTributosAssociados);
		}
	}

	private void carregarListasIniciaisFaixasDescontos(HttpSession session) throws GGASException {

		List<DadosFaixasTarifa> listaPrimeiraFaixa = new ArrayList<DadosFaixasTarifa>();
		DadosFaixasTarifa dadosFaixa = controladorTarifa.criarDadosFaixasTarifa();
		dadosFaixa.setFaixaInicial("0");
		dadosFaixa.setNumeroColunaFinal(0);
		listaPrimeiraFaixa.add(dadosFaixa);
		session.setAttribute(LISTA_DADOS_FAIXA, listaPrimeiraFaixa);
	}

	/**
	 * Método responsável por exibir a tela de pesquisa de tarifas.
	 * 
	 * @param model - {@link Model}
	 * @param session - {@link HttpSession}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaTarifa")
	public String exibirPesquisaTarifa(Model model, HttpSession session,
			@RequestParam(value = "habilitado", required = false, defaultValue = "true") Boolean habilitado) throws GGASException {

		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
		model.addAttribute(LISTA_SEGMENTO, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_TIPO_CONTRATO, controladorEntidadeConteudo.obterListaTipoModelo());

		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFatura());

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
		model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));
		model.addAttribute(STATUS_PENDENTE, Long.valueOf(statusPendente));
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);

		Usuario usuarioLogado = (Usuario) session.getAttribute(ATRIBUTO_USUARIO_LOGADO);
		Collection<Tarifa> tarifasPendentes = controladorAlcada.obterTarifasPendentesPorUsuario(usuarioLogado);

		if (!tarifasPendentes.isEmpty()) {
			model.addAttribute(POSSUI_ALCADA_AUTORIZACAO, true);
		}

		return "exibirPesquisaTarifa";
	}

	/**
	 * Método responsável por realizar a pesquisa de tarifas.
	 * 
	 * @param model - {@link Model}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param tarifa - {@link TarifaImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarTarifa")
	public String pesquisarTarifa(TarifaFormVO tarifaFormVO, TarifaImpl tarifa, 
			BindingResult bindingResult, Model model, HttpServletRequest request) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();

		if (tarifaFormVO.getIsAutorizacao() != null && tarifaFormVO.getIsAutorizacao()) {

			if (tarifaFormVO.getHabilitado() != null) {
				filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, tarifaFormVO.getHabilitado());
			}

			if (StringUtils.isNotEmpty(tarifaFormVO.getChavesPrimarias())) {
				String chavesPrimarias = tarifaFormVO.getChavesPrimarias();
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, Util.arrayStringParaArrayLong(chavesPrimarias.split(",")));
			}
		} else {
			prepararFiltro(tarifa, tarifaFormVO.getDataVigencia(), tarifaFormVO.getHabilitado(), tarifaFormVO.getStatus(), filtro);
		}

		Collection<TarifaVigenciaPesquisar> listaTarifa = controladorTarifa.obterTarifaVigenciaPesquisar(filtro);

		model.addAttribute(LISTA_TARIFA, listaTarifa);
		model.addAttribute(TARIFA, tarifa);
		model.addAttribute(Tarifa.ATRIBUTO_STATUS, tarifaFormVO.getStatus());
		model.addAttribute(DATA_VIGENCIA, tarifaFormVO.getDataVigencia());

		return this.exibirPesquisaTarifa(model, request.getSession(), tarifaFormVO.getHabilitado());
	}

	private void prepararFiltro(Tarifa tarifa, String dataVigencia, Boolean habilitado, Long status, Map<String, Object> filtro)
			throws GGASException {

		if (!StringUtils.isEmpty(tarifa.getDescricao())) {
			filtro.put(DESCRICAO_TARIFA, tarifa.getDescricao());
		}

		if (!StringUtils.isEmpty(tarifa.getDescricaoAbreviada())) {
			filtro.put(DESCRICAO_ABREVIADA_TARIFA, tarifa.getDescricaoAbreviada());
		}

		if (!StringUtils.isEmpty(dataVigencia)) {
			filtro.put(DATA_VIGENCIA, Util.converterCampoStringParaData("Data vigÃªncia", dataVigencia, Constantes.FORMATO_DATA_BR));
		}

		Segmento segmento = tarifa.getSegmento();
		if (segmento != null && segmento.getChavePrimaria() > 0) {
			filtro.put(ID_SEGMENTO, segmento.getChavePrimaria());
		}

		RamoAtividade ramoAtividade = tarifa.getRamoAtividade();
		if (ramoAtividade != null && ramoAtividade.getChavePrimaria() > 0) {
			filtro.put(ID_RAMO_ATIVIDADE, ramoAtividade.getChavePrimaria());
		}

		EntidadeConteudo itemFatura = tarifa.getItemFatura();
		if (itemFatura != null && itemFatura.getChavePrimaria() > 0) {
			filtro.put(ID_ITEM_FATURA, itemFatura.getChavePrimaria());
		}

		EntidadeConteudo tipoContrato = tarifa.getTipoContrato();
		if (tipoContrato != null && tipoContrato.getChavePrimaria() > 0) {
			filtro.put(TIPO_CONTRATO, tipoContrato.getChavePrimaria());
		}

		if (habilitado != null) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		}

		if (status != null && status > 0) {
			filtro.put(STATUS, status);
		}
	}

	/**
	 * Método responsável por excluir uma tarifa.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param chavesPrimarias
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("excluirTarifa")
	public String excluirTarifa(HttpServletRequest request, @RequestParam("chavesPrimarias") Long[] chavesPrimarias, Model model)
			throws GGASException {

		try {
			DadosAuditoria auditoria = getDadosAuditoria(request);

			controladorTarifa.removerTarifa(chavesPrimarias, auditoria);
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, super.obterMensagem(Tarifa.TARIFA_CAMPO_STRING));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new GGASException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}
		
		return FORWARD_PESQUISAR_TARIFA;
	}

	/**
	 * Método responsável por exportar o relatório de uma tarifa.
	 * 
	 * @param chavePrimariaTarifa - {@link Long}
	 * @param tipoExportacao - {@link String}
	 * @param chavePrimariaVigencia - {@link Long}
	 * @param chavePrimariaVigenciaDesconto - {@link Long}
	 * @param model - {@link Model}
	 * @return bytes do relatÃ³rio - {@link ResponseEntity}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exportarRelatorioTarifa")
	public ResponseEntity<byte[]> exportarRelatorioTarifa(@RequestParam("chavePrimaria") Long chavePrimariaTarifa,
			@RequestParam("formatoImpressao") String tipoExportacao, @RequestParam("chavePrimariaVigencia") Long chavePrimariaVigencia,
			@RequestParam("chavePrimariaVigenciaDesconto") Long chavePrimariaVigenciaDesconto, Model model) throws GGASException {

		DadosTarifa dadosTarifa = controladorTarifa.montarDadosTarifa(chavePrimariaTarifa);

		Long idTipoDesconto = getIdTipoDesconto(dadosTarifa.getFaixasDesconto());

		String descricaoTipoDesconto = getDescricaoTipoDesconto(idTipoDesconto);

		FiltroTarifaVigenciaDescontos filtro = FiltroTarifaVigenciaDescontos.montarFiltroTarifaVigenciaDescontos(chavePrimariaTarifa,
				chavePrimariaVigencia, chavePrimariaVigenciaDesconto, descricaoTipoDesconto, tipoExportacao);

		byte[] relatorioTarifasCadastradas = controladorTarifa.exportarRelatorioTarifa(filtro);

		MediaType contentType = MediaType.parseMediaType(super.obterContentTypeRelatorio(filtro.getFormatoImpressao()));
		String filename = String.format("relatorioTarifa%s", this.obterFormatoRelatorio(filtro.getFormatoImpressao()));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(contentType);
		headers.setContentDispositionFormData("attachment", filename);

		return new ResponseEntity<byte[]>(relatorioTarifasCadastradas, headers, HttpStatus.OK);
	}

	/**
	 * Método responsável por autorizar a vigência de uma tarifa.
	 * 
	 * @param chavePrimariaVigenciaParam - {@link Long}
	 * @param autorizado - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 */
	@RequestMapping("autorizarTarifaVigencia")
	public String autorizarTarifaVigencia(@RequestParam(value = "chavePrimariaVigencia") Long chavePrimariaVigenciaParam,
			@RequestParam("statusAutorizacaoVigencia") Long autorizado, HttpServletRequest request, Model model)
			throws ConcorrenciaException {

		Long chavePrimariaVigencia = 0L;
		if (chavePrimariaVigenciaParam != null) {
			chavePrimariaVigencia = chavePrimariaVigenciaParam;
		}

		try {
			if (autorizado != null) {

				EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(autorizado);

				if (status != null) {

					TarifaVigencia tarifaVigencia = controladorTarifa.obterTarifaVigencia(chavePrimariaVigencia, "tipoCalculo",
							"unidadeMonetaria", "baseApuracao", "status");

					if (tarifaVigencia != null) {
						tarifaVigencia.setStatus(status);

						DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

						tarifaVigencia.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());
						tarifaVigencia.setDadosAuditoria(dadosAuditoria);

						controladorTarifa.atualizar(tarifaVigencia, TarifaVigenciaImpl.class);
					}
				}

				super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, super.obterMensagem(TarifaVigencia.TARIFA_VIGENCIA));
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return FORWARD_PESQUISAR_TARIFA;
	}

	/**
	 * Método responsável por exibir o detalhamento de uma tarifa.
	 * 
	 * @param filtroPesquisaTarifa - {@link TarifaImpl}
	 * @param tarifaFormVO - {@link TarifaFormVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param idTarifaVigencia - {@link Long}
	 * @param session - {@link HttpSession}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoTarifa")
	public String exibirDetalhamentoTarifa(TarifaFormVO tarifaFormVO, TarifaImpl filtroPesquisaTarifa, BindingResult bindingResult,
			Model model, @RequestParam(value = "tarifasVigencia", required = false) Long idTarifaVigencia, HttpSession session)
			throws GGASException {

		model.addAttribute("filtroPesquisaTarifa", filtroPesquisaTarifa);
		model.addAttribute(TARIFA_FORM_VO, tarifaFormVO);

		Tarifa tarifa = (Tarifa) controladorTarifa.obter(filtroPesquisaTarifa.getChavePrimaria(), SEGMENTO, RAMO_ATIVIDADE, TIPO_CONTRATO);

		Usuario usuarioLogado = (Usuario) session.getAttribute(ATRIBUTO_USUARIO_LOGADO);

		model.addAttribute("idUsuario", usuarioLogado.getChavePrimaria());
		model.addAttribute(TARIFA, tarifa);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_TARIFA, tarifa.getChavePrimaria());
		filtro.put("habilitado", Boolean.TRUE);

		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFatura());
		model.addAttribute(LISTA_TARIFAS_VIGENCIA, controladorTarifa.consultarTarifasVigencia(filtro));
		model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());

		if (idTarifaVigencia != null) {
			model.addAttribute(LISTA_TARIFAS_VIGENCIA_DESCONTO,
					controladorTarifa.consultarTarifasVigenciaDescontoPorTarifaVigencia(idTarifaVigencia));
		}

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);

		model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
		model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));

		return "exibirDetalhamentoTarifa";
	}

	/**
	 * Método responsável por exibir o detalhamento dos descontos de uma tarifa.
	 * 
	 * @param idTarifaVigencia - {@link Long}
	 * @param idTarifaVigenciaDesconto - {@link Long}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoTarifaFaixaDesconto")
	public String exibirDetalhamentoTarifaFaixaDesconto(@RequestParam(value = "tarifasVigencia", required = false) Long idTarifaVigencia,
			@RequestParam(value = "tarifasVigenciaDesconto", required = false) Long idTarifaVigenciaDesconto, Model model)
			throws GGASException {

		try {
			controladorTarifa.validarVigenciaEDescontoParaExibicaoFaixas(idTarifaVigencia);

			TarifaVigencia tarifaVigencia = controladorTarifa.obterTarifaVigencia(idTarifaVigencia, TARIFA_VIGENCIA_FAIXAS);

			TarifaVigenciaDesconto tarifaVigenciaDesconto = null;
			if (idTarifaVigenciaDesconto != null && idTarifaVigenciaDesconto > 0) {
				tarifaVigenciaDesconto = controladorTarifa.obterTarifaVigenciaDesconto(idTarifaVigenciaDesconto);
			}

			model.addAttribute("tarifasVigencia", idTarifaVigencia);
			model.addAttribute("tarifasVigenciaDesconto", idTarifaVigenciaDesconto);

			model.addAttribute(INTERVALO_ANOS_DATA, intervaloAnosData());
			model.addAttribute(LISTA_TARIFAS_FAIXA_DESCONTO,
					controladorTarifa.montarListaFaixasTarifaDesconto(tarifaVigencia, tarifaVigenciaDesconto));

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return "forward:exibirDetalhamentoTarifa";
	}

	/**
	 * Obter tarifa vigencia.
	 *
	 * @param chavePrimaria - {@link Long}
	 * @return dados da vigência de uma tarifa - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("obterTarifaVigencia")
	@ResponseBody
	public Map<String, Object> obterTarifaVigencia(@RequestParam("chavePrimaria") Long chavePrimaria) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();

		if (chavePrimaria != null && chavePrimaria > 0) {

			TarifaVigencia tarifaVigencia = controladorTarifa.obterTarifaVigencia(chavePrimaria, "tipoCalculo", "unidadeMonetaria",
					"baseApuracao", STATUS, "tributos");
			if (tarifaVigencia != null) {

				if (tarifaVigencia.getTipoCalculo().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_TIPO_CALCULO, tarifaVigencia.getTipoCalculo().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_TIPO_CALCULO, "");
				}

				if (tarifaVigencia.getUnidadeMonetaria().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_UNIDADE_MONETARIA, tarifaVigencia.getUnidadeMonetaria().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_UNIDADE_MONETARIA, "");
				}

				if (tarifaVigencia.getComentario() != null) {
					dados.put(OBSERVACAO_VIGENCIA, tarifaVigencia.getComentario());
				} else {
					dados.put(OBSERVACAO_VIGENCIA, "");
				}

				if (tarifaVigencia.getBaseApuracao().getDescricao() != null) {
					dados.put(DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO, tarifaVigencia.getBaseApuracao().getDescricao());
				} else {
					dados.put(DESCRICAO_VIGENCIA_VOLUME_BASE_CALCULO, "");
				}

				if (tarifaVigencia.getInformarMsgIcmsSubstituido() != null) {
					if (tarifaVigencia.getInformarMsgIcmsSubstituido().equals(Boolean.TRUE)) {
						dados.put(EntidadeConteudo.ATRIBUTO_HABILITADO, "Sim");
					} else {
						dados.put(EntidadeConteudo.ATRIBUTO_HABILITADO, "NÃ£o");
					}
					if (tarifaVigencia.getBaseCalculoIcmsSubstituido() != null) {
						dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, tarifaVigencia.getBaseCalculoIcmsSubstituido());
					} else {
						dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, "");
					}
					if (tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
						dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, tarifaVigencia.getIcmsSubstituidoMetroCubico());
					} else {
						dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, "");
					}
					if (tarifaVigencia.getMensagemIcmsSubstituto() != null) {
						dados.put(MENSAGEM_ICMS_SUBSTITUIDO, tarifaVigencia.getMensagemIcmsSubstituto());
					} else {
						dados.put(MENSAGEM_ICMS_SUBSTITUIDO, "");
					}
				} else {
					dados.put(EntidadeConteudo.ATRIBUTO_HABILITADO, "NÃ£o");
					dados.put(BASE_CALCULO_ICMS_SUBSTITUIDO, "");
					dados.put(ICMS_SUBSTITUIDO_METRO_CUBICO, "");
					dados.put(MENSAGEM_ICMS_SUBSTITUIDO, "");
				}

				String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
				if (tarifaVigencia.getStatus() != null && paramStatusPendente != null) {
					dados.put(STATUS, tarifaVigencia.getStatus().getDescricao());
					if (tarifaVigencia.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
						dados.put(STATUS_PENDENTE, Boolean.TRUE);
					}
				}
				StringBuilder tributos = new StringBuilder();

				for (TarifaVigenciaTributo tarifaTributo : tarifaVigencia.getTributos()) {
					tributos.append(" ").append(tarifaTributo.getTributo().getDescricao());
				}

				dados.put("tributos", tributos.toString());
			}
		}
		return dados;
	}

	/**
	 * Obter um desconto de uma vigência de tarifa por identificador da vigência.
	 *
	 * @param chavePrimariaTarifaVigencia - {@link Long}
	 * @return dados dos descontos de uma vigÃªncia de tarifa - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("obterTarifaVigenciaDescontoPorTarifaVigencia")
	@ResponseBody
	public Map<String, String> obterTarifaVigenciaDescontoPorTarifaVigencia(
			@RequestParam("chavePrimariaTarifaVigencia") Long chavePrimariaTarifaVigencia) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<String, String>();

		if (chavePrimariaTarifaVigencia != null && chavePrimariaTarifaVigencia > 0) {

			Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto =
					controladorTarifa.obterTarifaVigenciaDescontoPorTarifaVigencia(chavePrimariaTarifaVigencia);
			if (listaTarifaVigenciaDesconto != null && !listaTarifaVigenciaDesconto.isEmpty()) {

				for (TarifaVigenciaDesconto tarifaVigenciaDesconto : listaTarifaVigenciaDesconto) {
					dados.put(String.valueOf(tarifaVigenciaDesconto.getChavePrimaria()),
							Util.formatarDataTarifa(tarifaVigenciaDesconto.getInicioVigencia(), tarifaVigenciaDesconto.getFimVigencia()));
				}
			}
		}
		return dados;
	}

	/**
	 * Obter um desconto de uma vigência de tarifa.
	 *
	 * @param chavePrimaria - {@link Long}
	 * @return retorna uma {@link TarifaVigenciaDesconto} - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("obterTarifaVigenciaDesconto")
	@ResponseBody
	public Map<String, String> obterTarifaVigenciaDesconto(@RequestParam("chavePrimaria") Long chavePrimaria) throws GGASException {

		Map<String, String> dados = new HashMap<String, String>();

		if (chavePrimaria != null && chavePrimaria > 0) {
			TarifaVigenciaDesconto tarifaVigenciaDesconto = controladorTarifa.obterTarifaVigenciaDesconto(chavePrimaria);

			if (tarifaVigenciaDesconto.getDescricaoDescontoVigencia() != null) {
				dados.put(OBSERVACAO_VIGENCIA_DESCONTO, tarifaVigenciaDesconto.getDescricaoDescontoVigencia());
			} else {
				dados.put(OBSERVACAO_VIGENCIA_DESCONTO, "");
			}
		}

		return dados;

	}

	/**
	 * Verifica se o usuário possui autorização para acessar a vigência de tarifa..
	 *
	 * @param idUsuario - {@link Long}
	 * @param idTarifaVigencia - {@link Long}
	 * @return verificaÃ§Ã£o de autorizaÃ§Ã£o do usuÃ¡rio sobre uma vigÃªncia de tarifa - {@link Map}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("possuiAlcadaAutorizacaoTarifaVigencia")
	@ResponseBody
	public Map<String, Boolean> possuiAlcadaAutorizacaoTarifaVigencia(@RequestParam("idUsuario") Long idUsuario,
			@RequestParam("idTarifaVigencia") Long idTarifaVigencia) throws GGASException {

		Map<String, Boolean> dados = new LinkedHashMap<String, Boolean>();

		if (idUsuario != null && idTarifaVigencia != null) {
			dados.put("isUsuarioAutorizado", controladorAlcada.possuiAlcadaAutorizacaoTarifaVigencia(
					controladorUsuario.obterUsuario(idUsuario), controladorTarifa.obterTarifaVigencia(idTarifaVigencia)));
		}

		return dados;
	}

	private String getDescricaoTipoDesconto(Long idTipoDesconto) throws NegocioException {
		String descricaoTipoDesconto = "";

		Collection<EntidadeConteudo> listaTipoDesconto = controladorEntidadeConteudo.obterListaTipoValorTarifa();
		for (EntidadeConteudo entidadeConteudo : listaTipoDesconto) {
			if (idTipoDesconto != null && entidadeConteudo.getChavePrimaria() == idTipoDesconto) {
				descricaoTipoDesconto = entidadeConteudo.getDescricao();
			}
		}

		return descricaoTipoDesconto;
	}

	private Long getIdTipoDesconto(List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa) {

		Long idTipoDesconto = null;

		if (listaDadosFaixasDescontoTarifa != null && !listaDadosFaixasDescontoTarifa.isEmpty()) {
			for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixasDescontoTarifa) {
				if (dadosFaixasTarifa.getTipoDesconto() != null) {
					idTipoDesconto = dadosFaixasTarifa.getTipoDesconto();
					break;
				}
			}
		}

		return idTipoDesconto;
	}

	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));
		DateTime dataFinal = dataAtual.plusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataFinal.getYear();

		return retorno;
	}

	/**
	 * Responsável por redefinir o tratamento de {@link String} contendo decimais separados por vírgula.
	 * 
	 * @param binder - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}

}
