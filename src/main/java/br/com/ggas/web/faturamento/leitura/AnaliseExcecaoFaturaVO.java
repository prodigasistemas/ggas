/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

/**
 * @author bruno silva
 * 
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Análise de Anormalidade de Faturamento.
 */
public class AnaliseExcecaoFaturaVO {
	
	private String dataGeracaoInicial;
	private String dataGeracaoFinal;
	private String dataMedicaoInicial;
	private String dataMedicaoFinal;
	private String pontoSemConsumo;
	private String consumoLido;
	private String percentualVariacaoConsumo;
	private String percentualVariacaoValorFatura;
	private String referencia;
	private String ciclo;
	private String pontoConsumoLegado;
	private String cepImovel;
	private String matriculaImovel;
	private String nomeImovel;
	private String complementoImovel;
	private String numeroImovel;
	private String comentarioAlteracaoLeitura;

	private Long chavePrimaria;
	private Long idHistoricoMedicao;
	private Long idLocalidade;
	private Long idSetorComercial;
	private Long idGrupoFaturamento;
	private Long idSegmento;
	private Long idSituacaoPontoConsumo;
	private Long idTipoConsumo;
	private Long idCliente;
	private Long idAnormalidadeFaturamento;
	
	private Boolean analisada = false;
	private Boolean indicadorImpedeFaturamento;
	private Boolean indicadorCondominio;
	
	private Long[] chavesPrimarias;
	private Long[] idsRota;
	private Long[] idsAnormalidadeFatura;
	
	/**
	 * @return dataGeracaoInicial {@link String}
	 */
	public String getDataGeracaoInicial() {
		return dataGeracaoInicial;
	}
	/**
	 * @param dataGeracaoInicial {@link String}
	 */
	public void setDataGeracaoInicial(String dataGeracaoInicial) {
		this.dataGeracaoInicial = dataGeracaoInicial;
	}
	/**
	 * @return dataGeracaoFinal {@link String}
	 */
	public String getDataGeracaoFinal() {
		return dataGeracaoFinal;
	}
	/**
	 * @param dataGeracaoFinal {@link String}
	 */
	public void setDataGeracaoFinal(String dataGeracaoFinal) {
		this.dataGeracaoFinal = dataGeracaoFinal;
	}
	/**
	 * @return dataMedicaoInicial {@link String}
	 */
	public String getDataMedicaoInicial() {
		return dataMedicaoInicial;
	}
	/**
	 * @param dataMedicaoInicial {@link String}
	 */
	public void setDataMedicaoInicial(String dataMedicaoInicial) {
		this.dataMedicaoInicial = dataMedicaoInicial;
	}
	/**
	 * @return dataMedicaoFinal {@link String}
	 */
	public String getDataMedicaoFinal() {
		return dataMedicaoFinal;
	}
	/**
	 * @param dataMedicaoFinal {@link String}
	 */
	public void setDataMedicaoFinal(String dataMedicaoFinal) {
		this.dataMedicaoFinal = dataMedicaoFinal;
	}
	/**
	 * @return pontoSemConsumo {@link String}
	 */
	public String getPontoSemConsumo() {
		return pontoSemConsumo;
	}
	/**
	 * @param pontoSemConsumo {@link String}
	 */
	public void setPontoSemConsumo(String pontoSemConsumo) {
		this.pontoSemConsumo = pontoSemConsumo;
	}
	/**
	 * @return consumoLido {@link String}
	 */
	public String getConsumoLido() {
		return consumoLido;
	}
	/**
	 * @param consumoLido {@link String}
	 */
	public void setConsumoLido(String consumoLido) {
		this.consumoLido = consumoLido;
	}
	/**
	 * @return percentualVariacaoConsumo {@link String}
	 */
	public String getPercentualVariacaoConsumo() {
		return percentualVariacaoConsumo;
	}
	/**
	 * @param percentualVariacaoConsumo {@link String}
	 */
	public void setPercentualVariacaoConsumo(String percentualVariacaoConsumo) {
		this.percentualVariacaoConsumo = percentualVariacaoConsumo;
	}
	/**
	 * @return percentualVariacaoValorFatura {@link String}
	 */
	public String getPercentualVariacaoValorFatura() {
		return percentualVariacaoValorFatura;
	}
	/**
	 * @param percentualVariacaoValorFatura {@link String}
	 */
	public void setPercentualVariacaoValorFatura(String percentualVariacaoValorFatura) {
		this.percentualVariacaoValorFatura = percentualVariacaoValorFatura;
	}
	/**
	 * @return referencia {@link String}
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia {@link String}
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return ciclo {@link String}
	 */
	public String getCiclo() {
		return ciclo;
	}
	/**
	 * @param ciclo {@link String}
	 */
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	/**
	 * @return pontoConsumoLegado {@link String}
	 */
	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}
	/**
	 * @param pontoConsumoLegado {@link String}
	 */
	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}
	/**
	 * @return cepImovel {@link String}
	 */
	public String getCepImovel() {
		return cepImovel;
	}
	/**
	 * @param cepImovel {@link String}
	 */
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}
	/**
	 * @return matriculaImovel {@link String}
	 */
	public String getMatriculaImovel() {
		return matriculaImovel;
	}
	/**
	 * @param matriculaImovel {@link String}
	 */
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}
	/**
	 * @return nomeImovel {@link String}
	 */
	public String getNomeImovel() {
		return nomeImovel;
	}
	/**
	 * @param nomeImovel {@link String}
	 */
	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}
	/**
	 * @return complementoImovel {@link String}
	 */
	public String getComplementoImovel() {
		return complementoImovel;
	}
	/**
	 * @param complementoImovel {@link String}
	 */
	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}
	/**
	 * @return numeroImovel {@link String}
	 */
	public String getNumeroImovel() {
		return numeroImovel;
	}
	/**
	 * @param numeroImovel {@link String}
	 */
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}
	/**
	 * @return comentarioAlteracaoLeitura {@link String}
	 */
	public String getComentarioAlteracaoLeitura() {
		return comentarioAlteracaoLeitura;
	}
	/**
	 * @param comentarioAlteracaoLeitura {@link String}
	 */
	public void setComentarioAlteracaoLeitura(String comentarioAlteracaoLeitura) {
		this.comentarioAlteracaoLeitura = comentarioAlteracaoLeitura;
	}
	
	/**
	 * @return analisada {@link Boolean}
	 */
	public Boolean getAnalisada() {
		return analisada;
	}
	/**
	 * @param analisada {@link Boolean}
	 */
	public void setAnalisada(Boolean analisada) {
		this.analisada = analisada;
	}
	/**
	 * @return indicadorImpedeFaturamento {@link Boolean}
	 */
	public Boolean getIndicadorImpedeFaturamento() {
		return indicadorImpedeFaturamento;
	}
	/**
	 * @param indicadorImpedeFaturamento {@link Boolean}
	 */
	public void setIndicadorImpedeFaturamento(Boolean indicadorImpedeFaturamento) {
		this.indicadorImpedeFaturamento = indicadorImpedeFaturamento;
	}
	/**
	 * @return indicadorCondominio {@link Boolean}
	 */
	public Boolean getIndicadorCondominio() {
		return indicadorCondominio;
	}
	/**
	 * @param indicadorCondominio {@link Boolean}
	 */
	public void setIndicadorCondominio(Boolean indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}
	
	/**
	 * @return chavePrimaria {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	/**
	 * @return idHistoricoMedicao {@link Long}
	 */
	public Long getIdHistoricoMedicao() {
		return idHistoricoMedicao;
	}
	/**
	 * @param idHistoricoMedicao {@link Long}
	 */
	public void setIdHistoricoMedicao(Long idHistoricoMedicao) {
		this.idHistoricoMedicao = idHistoricoMedicao;
	}
	/**
	 * @return idLocalidade {@link Long}
	 */
	public Long getIdLocalidade() {
		return idLocalidade;
	}
	/**
	 * @param idLocalidade {@link Long}
	 */
	public void setIdLocalidade(Long idLocalidade) {
		this.idLocalidade = idLocalidade;
	}
	/**
	 * @return idLocalidade {@link Long}
	 */
	public Long getIdSetorComercial() {
		return idSetorComercial;
	}
	/**
	 * @param idSetorComercial {@link Long}
	 */
	public void setIdSetorComercial(Long idSetorComercial) {
		this.idSetorComercial = idSetorComercial;
	}
	/**
	 * @return idGrupoFaturamento {@link Long}
	 */
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	/**
	 * @param idGrupoFaturamento {@link Long}
	 */
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	/**
	 * @return idSegmento {@link Long}
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}
	/**
	 * @param idSegmento {@link Long}
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}
	/**
	 * @return idSituacaoPontoConsumo {@link Long}
	 */
	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}
	/**
	 * @param idSituacaoPontoConsumo {@link Long}
	 */
	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}
	/**
	 * @return idTipoConsumo {@link Long}
	 */
	public Long getIdTipoConsumo() {
		return idTipoConsumo;
	}
	/**
	 * @param idTipoConsumo {@link Long}
	 */
	public void setIdTipoConsumo(Long idTipoConsumo) {
		this.idTipoConsumo = idTipoConsumo;
	}
	/**
	 * @return idCliente {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return retorno {@link Long}
	 */
	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if(this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}
	/**
	 * @param chavesPrimarias {@link Long}
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if(chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}
	}
	/**
	 * @return retorno {@link Long}
	 */
	public Long[] getIdsRota() {
		Long[] retorno = null;
		if(this.idsRota != null) {
			retorno = this.idsRota.clone();
		}
		return retorno;
	}
	/**
	 * @param idsRota {@link Long}
	 */
	public void setIdsRota(Long[] idsRota) {
		if(idsRota != null) {
			this.idsRota = idsRota.clone();
		}
	}
	/**
	 * @return retorno {@link Long}
	 */
	public Long[] getIdsAnormalidadeFatura() {
		Long[] retorno = null;
		if(this.idsAnormalidadeFatura != null) {
			retorno = this.idsAnormalidadeFatura.clone();
		}
		return retorno;
	}
	/**
	 * @param idsAnormalidadeFatura {@link Long}
	 */
	public void setIdsAnormalidadeFatura(Long[] idsAnormalidadeFatura) {
		if(idsAnormalidadeFatura != null) {
			this.idsAnormalidadeFatura = idsAnormalidadeFatura.clone();
		}
	}

	/**
	 * Obtém id da anormalidade de faturamento
	 * @return chave primaria da anormalidade
	 */
	public Long getIdAnormalidadeFaturamento() {
		return idAnormalidadeFaturamento;
	}

	/**
	 * Configura anormalidade de faturamento
	 * @param idAnormalidadeFaturamento identificador da anormalidade
	 */
	public void setIdAnormalidadeFaturamento(Long idAnormalidadeFaturamento) {
		this.idAnormalidadeFaturamento = idAnormalidadeFaturamento;
	}
}
