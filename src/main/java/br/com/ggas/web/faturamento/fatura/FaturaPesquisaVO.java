package br.com.ggas.web.faturamento.fatura;

import java.io.Serializable;

/**
 * 
 * Classe VO responsável pela transferencia de dados entre as telas de Fatura.
 * 
 * @author pedro
 *
 */
public class FaturaPesquisaVO implements Serializable {

	private static final long serialVersionUID = 1242102897525908369L;
	private Long idRota;
	private Long idGrupo;
	private Long idFatura;
	private Long idImovel;
	private Long idCliente;
	private Long idSituacao;
	private Long chavePrimaria;
	private Long idMotivoInclusao;
	private Long idDocumentoFiscal;
	private Long idMotivoRefaturamento;
	private Long idMotivoCancelamento;
	private Long chavePrimariaDocFiscal;
	private Long idContratoEncerrarRescindir;
	private Long idMotivoRevisao;
	private Long idFuncionario;
	private Long idMotivoComplemento;
	private Long idPontoConsumo;
	private Long idTransportadora;
	private Long idVeiculo;
	private Long idMotorista;
	private Long numeroNotaDevolucao;
	private Integer tipoFatura;
	private Integer emissaoCliente;
	private Integer idAnoMesReferencia;
	private Integer indicadorTipoNotaNormalEletronica;
	private Float[] consumoItem;
	private Float[] vlrUnitarioItem;
	private boolean medicaoDiaria;
	private Boolean existeMotivoRevisao;
	private Boolean incidemJurosMulta;
	private Boolean indicadorPesquisaPontos;
	private Boolean indicadorFaturasPendentes;
	private Boolean isValidarFiltroPesquisaFatura;
	private Boolean indicadorRevisao;
	private String observacoesDevolucao;
	private String dataDevolucao;
	private String numeroProtocoloDevolucao;
	private String numeroContrato;
	private String numeroSerie;
	private String tipoNota;
	private String chaveAcesso;
	private String numeroCiclo;
	private String dataEmissaoInicial;
	private String dataEmissaoFinal;
	private String numeroDocumentoInicial;
	private String numeroDocumentoFinal;
	private String numeroNotaFiscalInicial;
	private String numeroNotaFiscalFinal;
	private String anoMesFormatado;
	private String codigoCliente;
	private String anoMesReferencia;
	private String tipoDocumento;
	private String indicadorComplementar;
	private String motivoAlteracao;
	private String dataVencimento;
	private String valor;
	private String observacoesComplemento;
	private String novaDataVencimento;
	private Long idSituacaoPagamento;
	private Long[] idLeiturista;
	private Long[] chavesCreditoDebito;
	private Long[] chavesPrimarias;
	private Long[] chavesPontoConsumo;
	private Long[] chavesFaturasPendentes;
	private Double[] leituraAtual;
	private String[] consumoApurado;
	private String[] dataLeituraAtual;
	private String[] consumoApuradoAnterior;
	private String[] volumeApurado;
	private Boolean[] indicadorLeituraCliente;
	public String descricaoMotivoInclusao;

	
	/**
	 * @return situacaoPagamento {@link String}
	 */
	public Long getIdSituacaoPagamento() {
		return idSituacaoPagamento;
	}
	/**
	 * @param situacaoPagamento {@link Long}
	 */
	public void setIdSituacaoPagamento(Long idSituacaoPagamento) {
		this.idSituacaoPagamento = idSituacaoPagamento;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public Long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public Long getIdFatura() {
		return idFatura;
	}

	public void setIdFatura(Long idFatura) {
		this.idFatura = idFatura;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Long idSituacao) {
		this.idSituacao = idSituacao;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Long getIdMotivoInclusao() {
		return idMotivoInclusao;
	}

	public void setIdMotivoInclusao(Long idMotivoInclusao) {
		this.idMotivoInclusao = idMotivoInclusao;
	}

	public Long getIdDocumentoFiscal() {
		return idDocumentoFiscal;
	}

	public void setIdDocumentoFiscal(Long idDocumentoFiscal) {
		this.idDocumentoFiscal = idDocumentoFiscal;
	}

	public Long getIdMotivoRefaturamento() {
		return idMotivoRefaturamento;
	}

	public void setIdMotivoRefaturamento(Long idMotivoRefaturamento) {
		this.idMotivoRefaturamento = idMotivoRefaturamento;
	}

	public Long getIdMotivoCancelamento() {
		return idMotivoCancelamento;
	}

	public void setIdMotivoCancelamento(Long idMotivoCancelamento) {
		this.idMotivoCancelamento = idMotivoCancelamento;
	}

	public Long getChavePrimariaDocFiscal() {
		return chavePrimariaDocFiscal;
	}

	public void setChavePrimariaDocFiscal(Long chavePrimariaDocFiscal) {
		this.chavePrimariaDocFiscal = chavePrimariaDocFiscal;
	}

	public Long getIdContratoEncerrarRescindir() {
		return idContratoEncerrarRescindir;
	}

	public void setIdContratoEncerrarRescindir(Long idContratoEncerrarRescindir) {
		this.idContratoEncerrarRescindir = idContratoEncerrarRescindir;
	}

	public Long getIdMotivoRevisao() {
		return idMotivoRevisao;
	}

	public void setIdMotivoRevisao(Long idMotivoRevisao) {
		this.idMotivoRevisao = idMotivoRevisao;
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Long getIdMotivoComplemento() {
		return idMotivoComplemento;
	}

	public void setIdMotivoComplemento(Long idMotivoComplemento) {
		this.idMotivoComplemento = idMotivoComplemento;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public Long getIdTransportadora() {
		return idTransportadora;
	}

	public void setIdTransportadora(Long idTransportadora) {
		this.idTransportadora = idTransportadora;
	}

	public Long getIdVeiculo() {
		return idVeiculo;
	}

	public void setIdVeiculo(Long idVeiculo) {
		this.idVeiculo = idVeiculo;
	}

	public Long getIdMotorista() {
		return idMotorista;
	}

	public void setIdMotorista(Long idMotorista) {
		this.idMotorista = idMotorista;
	}

	public Long getNumeroNotaDevolucao() {
		return numeroNotaDevolucao;
	}

	public void setNumeroNotaDevolucao(Long numeroNotaDevolucao) {
		this.numeroNotaDevolucao = numeroNotaDevolucao;
	}

	public Integer getTipoFatura() {
		return tipoFatura;
	}

	public void setTipoFatura(Integer tipoFatura) {
		this.tipoFatura = tipoFatura;
	}

	public Integer getEmissaoCliente() {
		return emissaoCliente;
	}

	public void setEmissaoCliente(Integer emissaoCliente) {
		this.emissaoCliente = emissaoCliente;
	}

	public Integer getIdAnoMesReferencia() {
		return idAnoMesReferencia;
	}

	public void setIdAnoMesReferencia(Integer idAnoMesReferencia) {
		this.idAnoMesReferencia = idAnoMesReferencia;
	}

	public Integer getIndicadorTipoNotaNormalEletronica() {
		return indicadorTipoNotaNormalEletronica;
	}

	public void setIndicadorTipoNotaNormalEletronica(Integer indicadorTipoNotaNormalEletronica) {
		this.indicadorTipoNotaNormalEletronica = indicadorTipoNotaNormalEletronica;
	}

	public Float[] getConsumoItem() {
		Float[] consumoItemTmp = null;
		if (this.consumoItem != null) {
			consumoItemTmp = consumoItem.clone();
		}
		return consumoItemTmp;
	}

	public void setConsumoItem(Float[] consumoItem) {
		if (consumoItem != null) {
			this.consumoItem = consumoItem.clone();
		} else {
			this.consumoItem = null;
		}
	}

	public Float[] getVlrUnitarioItem() {
		Float[] vlrUnitarioItemTmp = null;
		if (this.vlrUnitarioItem != null) {
			vlrUnitarioItemTmp = this.vlrUnitarioItem.clone();
		}
		return vlrUnitarioItemTmp;
	}

	public void setVlrUnitarioItem(Float[] vlrUnitarioItem) {
		this.vlrUnitarioItem = vlrUnitarioItem.clone();
	}

	public boolean isMedicaoDiaria() {
		return medicaoDiaria;
	}

	public void setMedicaoDiaria(boolean medicaoDiaria) {
		this.medicaoDiaria = medicaoDiaria;
	}

	public Boolean getExisteMotivoRevisao() {
		return existeMotivoRevisao;
	}

	public void setExisteMotivoRevisao(Boolean existeMotivoRevisao) {
		this.existeMotivoRevisao = existeMotivoRevisao;
	}

	public Boolean getIncidemJurosMulta() {
		return incidemJurosMulta;
	}

	public void setIncidemJurosMulta(Boolean incidemJurosMulta) {
		this.incidemJurosMulta = incidemJurosMulta;
	}

	public Boolean getIndicadorPesquisaPontos() {
		return indicadorPesquisaPontos;
	}

	public void setIndicadorPesquisaPontos(Boolean indicadorPesquisaPontos) {
		this.indicadorPesquisaPontos = indicadorPesquisaPontos;
	}

	public Boolean getIndicadorFaturasPendentes() {
		return indicadorFaturasPendentes;
	}

	public void setIndicadorFaturasPendentes(Boolean indicadorFaturasPendentes) {
		this.indicadorFaturasPendentes = indicadorFaturasPendentes;
	}

	public Boolean getIsValidarFiltroPesquisaFatura() {
		return isValidarFiltroPesquisaFatura;
	}

	public void setIsValidarFiltroPesquisaFatura(Boolean isValidarFiltroPesquisaFatura) {
		this.isValidarFiltroPesquisaFatura = isValidarFiltroPesquisaFatura;
	}

	public Boolean getIndicadorRevisao() {
		return indicadorRevisao;
	}

	public void setIndicadorRevisao(Boolean indicadorRevisao) {
		this.indicadorRevisao = indicadorRevisao;
	}

	public String getObservacoesDevolucao() {
		return observacoesDevolucao;
	}

	public void setObservacoesDevolucao(String observacoesDevolucao) {
		this.observacoesDevolucao = observacoesDevolucao;
	}

	public String getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public String getNumeroProtocoloDevolucao() {
		return numeroProtocoloDevolucao;
	}

	public void setNumeroProtocoloDevolucao(String numeroProtocoloDevolucao) {
		this.numeroProtocoloDevolucao = numeroProtocoloDevolucao;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(String tipoNota) {
		this.tipoNota = tipoNota;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public String getNumeroCiclo() {
		return numeroCiclo;
	}

	public void setNumeroCiclo(String numeroCiclo) {
		this.numeroCiclo = numeroCiclo;
	}

	public String getDataEmissaoInicial() {
		return dataEmissaoInicial;
	}

	public void setDataEmissaoInicial(String dataEmissaoInicial) {
		this.dataEmissaoInicial = dataEmissaoInicial;
	}

	public String getDataEmissaoFinal() {
		return dataEmissaoFinal;
	}

	public void setDataEmissaoFinal(String dataEmissaoFinal) {
		this.dataEmissaoFinal = dataEmissaoFinal;
	}

	public String getNumeroDocumentoInicial() {
		return numeroDocumentoInicial;
	}

	public void setNumeroDocumentoInicial(String numeroDocumentoInicial) {
		this.numeroDocumentoInicial = numeroDocumentoInicial;
	}

	public String getNumeroDocumentoFinal() {
		return numeroDocumentoFinal;
	}

	public void setNumeroDocumentoFinal(String numeroDocumentoFinal) {
		this.numeroDocumentoFinal = numeroDocumentoFinal;
	}

	public String getNumeroNotaFiscalInicial() {
		return numeroNotaFiscalInicial;
	}

	public void setNumeroNotaFiscalInicial(String numeroNotaFiscalInicial) {
		this.numeroNotaFiscalInicial = numeroNotaFiscalInicial;
	}

	public String getNumeroNotaFiscalFinal() {
		return numeroNotaFiscalFinal;
	}

	public void setNumeroNotaFiscalFinal(String numeroNotaFiscalFinal) {
		this.numeroNotaFiscalFinal = numeroNotaFiscalFinal;
	}

	public String getAnoMesFormatado() {
		return anoMesFormatado;
	}

	public void setAnoMesFormatado(String anoMesFormatado) {
		this.anoMesFormatado = anoMesFormatado;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getAnoMesReferencia() {
		return anoMesReferencia;
	}

	public void setAnoMesReferencia(String anoMesReferencia) {
		this.anoMesReferencia = anoMesReferencia;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getIndicadorComplementar() {
		return indicadorComplementar;
	}

	public void setIndicadorComplementar(String indicadorComplementar) {
		this.indicadorComplementar = indicadorComplementar;
	}

	public String getMotivoAlteracao() {
		return motivoAlteracao;
	}

	public void setMotivoAlteracao(String motivoAlteracao) {
		this.motivoAlteracao = motivoAlteracao;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getObservacoesComplemento() {
		return observacoesComplemento;
	}

	public void setObservacoesComplemento(String observacoesComplemento) {
		this.observacoesComplemento = observacoesComplemento;
	}

	public String getNovaDataVencimento() {
		return novaDataVencimento;
	}

	public void setNovaDataVencimento(String novaDataVencimento) {
		this.novaDataVencimento = novaDataVencimento;
	}

	public Long[] getIdLeiturista() {
		Long[] idLeituristaTmp = null;
		if (this.idLeiturista != null) {
			idLeituristaTmp = this.idLeiturista.clone();
		}
		return idLeituristaTmp;
	}

	public void setIdLeiturista(Long[] idLeituristaTmp) {
		if (idLeituristaTmp != null) {
			this.idLeiturista = idLeituristaTmp.clone();
		} else {
			this.idLeiturista = null;
		}
	}

	public Long[] getChavesCreditoDebito() {
		Long[] chavesCreditoDebitoTmp = null;
		if (this.chavesCreditoDebito != null) {
			chavesCreditoDebitoTmp = this.chavesCreditoDebito.clone();
		}
		return chavesCreditoDebitoTmp;
	}

	public void setChavesCreditoDebito(Long[] chavesCreditoDebitoTmp) {
		if (chavesCreditoDebitoTmp != null) {
			this.chavesCreditoDebito = chavesCreditoDebitoTmp.clone();
		} else {
			this.chavesCreditoDebito = null;
		}
	}

	public Long[] getChavesPrimarias() {
		Long[] chavesPrimariasTmp = null;
		if (this.chavesPrimarias != null) {
			chavesPrimariasTmp = this.chavesPrimarias.clone();
		}
		return chavesPrimariasTmp;
	}

	public void setChavesPrimarias(Long[] chavesPrimariasTmp) {
		if (chavesPrimariasTmp != null) {
			this.chavesPrimarias = chavesPrimariasTmp.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

	public Long[] getChavesPontoConsumo() {
		Long[] chavesPontoConsumoTmp = null;
		if (this.chavesPontoConsumo != null) {
			chavesPontoConsumoTmp = this.chavesPontoConsumo.clone();
		}
		return chavesPontoConsumoTmp;
	}

	public void setChavesPontoConsumo(Long[] chavesPonto) {
		if (chavesPonto != null) {
			this.chavesPontoConsumo = chavesPonto.clone();
		} else {
			this.chavesPontoConsumo = null;
		}
	}

	public Long[] getChavesFaturasPendentes() {
		Long[] chavesFaturasPendentesTmp = null;
		if (this.chavesFaturasPendentes != null) {
			chavesFaturasPendentesTmp = this.chavesFaturasPendentes.clone();
		}
		return chavesFaturasPendentesTmp;
	}

	public void setChavesFaturasPendentes(Long[] chavesFaturas) {
		if (chavesFaturas != null) {
			this.chavesFaturasPendentes = chavesFaturas.clone();
		} else {
			this.chavesFaturasPendentes = null;
		}
	}

	public Double[] getLeituraAtual() {
		Double[] leituraAtualTmp = null;
		if (this.leituraAtual != null) {
			leituraAtualTmp = this.leituraAtual.clone();
		}
		return leituraAtualTmp;
	}

	public void setLeituraAtual(Double[] leituraAtualTmp) {
		if (leituraAtualTmp != null) {
			this.leituraAtual = leituraAtualTmp.clone();
		} else {
			this.consumoApurado = null;
		}
	}

	public String[] getConsumoApurado() {
		String[] consumoApuradoTmp = null;
		if (this.consumoApurado != null) {
			consumoApuradoTmp = this.consumoApurado.clone();
		}
		return consumoApuradoTmp;
	}

	public void setConsumoApurado(String[] consumo) {
		if (consumo != null) {
			this.consumoApurado = consumo.clone();
		} else {
			this.consumoApurado = null;
		}
	}

	public String[] getDataLeituraAtual() {
		String[] dataLeituraAtualTmp = null;
		if (this.dataLeituraAtual != null) {
			dataLeituraAtualTmp = this.dataLeituraAtual.clone();
		}
		return dataLeituraAtualTmp;
	}

	public void setDataLeituraAtual(String[] dataLeitura) {
		if (dataLeitura != null) {
			this.dataLeituraAtual = dataLeitura.clone();
		} else {
			this.dataLeituraAtual = null;
		}
	}

	public String[] getConsumoApuradoAnterior() {
		String[] consumoApuradoAnteriorTmp = null;
		if (this.consumoApuradoAnterior != null) {
			consumoApuradoAnteriorTmp = this.consumoApuradoAnterior.clone();
		}
		return consumoApuradoAnteriorTmp;
	}

	public void setConsumoApuradoAnterior(String[] consumoApurado) {
		if (consumoApurado != null) {
			this.consumoApuradoAnterior = consumoApurado.clone();
		} else {
			this.consumoApuradoAnterior = null;
		}
	}

	public String[] getVolumeApurado() {
		String[] volumeApuradoTmp = null;
		if (this.volumeApurado != null) {
			volumeApuradoTmp = this.volumeApurado.clone();
		}
		return volumeApuradoTmp;
	}

	public void setVolumeApurado(String[] volume) {
		if (volume != null) {
			this.volumeApurado = volume.clone();
		} else {
			this.volumeApurado = null;
		}
	}

	public Boolean[] getIndicadorLeituraCliente() {
		Boolean[] indicadorLeitura = null;
		if (this.indicadorLeituraCliente != null) {
			indicadorLeitura = this.indicadorLeituraCliente.clone();
		}
		return indicadorLeitura;
	}

	public void setIndicadorLeituraCliente(Boolean[] indicadorLeitura) {
		if (indicadorLeitura != null) {
			this.indicadorLeituraCliente = indicadorLeitura.clone();
		} else {
			this.indicadorLeituraCliente = null;
		}
	}
	
	public String getDescricaoMotivoInclusao() {
		return this.descricaoMotivoInclusao;
	}
	
	public void setDescricaoMotivoInclusao(String descricaoMotivoInclusao) {
		this.descricaoMotivoInclusao = descricaoMotivoInclusao;
	}
}
