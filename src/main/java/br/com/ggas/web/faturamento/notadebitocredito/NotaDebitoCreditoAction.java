package br.com.ggas.web.faturamento.notadebitocredito;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.impl.HistoricoNotaDebitoCredito;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaConciliacao;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.*;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Pair;
import br.com.ggas.util.Util;
import br.com.ggas.web.cobranca.parcelamento.ParcelaVO;
import br.com.ggas.web.faturamento.creditodebito.CreditoDebitoVO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Classe responsável pelas operações referentes a Nota de Crédito / Débito.
 * 
 * @author Paulo Burlamaqui
 */
@Controller
public class NotaDebitoCreditoAction extends GenericAction {

	private static final String ATRIBUTO_QUADRA_FACE = "quadraFace";

	private static final String FORWARD_PESQUISAR_NOTA_DEBITO_CREDITO = "forward:/pesquisarNotaDebitoCredito";

	private static final String CREDITO_DEBITO_VO = "creditoDebitoVO";

	private static final String LISTA_NOTA_CREDITO = "listaNotaCredito";

	private static final String VALOR_TOTAL_CAMPO = "Valor Total";

	private static final String INDICADOR_IMOVEL_SEM_PONTO_CONSUMO = "indicadorImovelSemPontoConsumo";

	private static final String LISTA_SITUACAO_NOTA = "listaSituacaoNota";

	private static final String SITUACAO_PAGAMENTO = "situacaoPagamento";

	private static final String GERAR_RELATORIO_NOTA_DEBITO_CREDITO = "gerarRelatorioNotaDebitoCredito";

	private static final String RELATORIO_NOTA_DEBITO_CREDITO = "relatorioNotaDebitoCredito";

	private static final String PESQUISA = "pesquisa";

	private static final String LISTA_NOTAS_CREDITO = "listaNotasCredito";

	private static final String LISTA_NOTAS_DEBITO = "listaNotasDebito";

	private static final String ID_TIPO_DOCUMENTO = "idTipoDocumento";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String LISTA_NOTA_DEBITO_CREDITO = "listaNotaDebitoCredito";

	private static final String LISTA_MOTIVO_CANCELAMENTO = "listaMotivoCancelamento";

	private static final String LISTA_NOTA_DEBITO = "listaNotaDebito";

	private static final String LISTA_PARCELAMENTOS = "listaParcelamentos";

	private static final String LISTA_DETALHAMENTO_PARCELAMENTO = "listaDetalhamentoParcelamento";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String MANTER_NOTA_DEBITO = "manterNotaDebito";

	private static final String ID_CLIENTE = "idCliente";

	private static final String CREDITO = "credito";

	private static final String CREDITO_PENALIDADE = "creditoPenalidade";

	private static final String DEBITO = "debito";

	private static final String INDICADOR_CREDITO_DEBITO = "indicadorCreditoDebito";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String PONTO_CONSUMO_LEGADO = "pontoConsumoLegado";

	private static final String LISTA_APURACAO_PENALIDADES = "listaApuracaoPenalidade";

	private static final String LISTA_APURACAO_PENALIDADE_PERIODICIDADE = "listaApuracaoPenalidadePeriodicidade";

	private static final String CHAVES_TIPO_DOCUMENTO = "chavesTipoDocumento";

	private static final String FILTRO_NOTA_CREDITO_DEBITO_VO = "FILTRO_CREDITO_DEBITO_VO";

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorParcelamento controladorParcelamento;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorIntegracao controladorIntegracao;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	/**
	 * Método responsável por exibir a tela de pesquisa de nota de débito e crédito.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirPesquisaNotaDebitoCredito")
	public String exibirPesquisaNotaDebitoCredito(HttpServletRequest request, Model model) {

		try {
			request.getSession().removeAttribute(LISTA_NOTA_CREDITO);
			request.getSession().removeAttribute(LISTA_NOTA_DEBITO);
			request.getSession().removeAttribute(LISTA_APURACAO_PENALIDADES);
			request.getSession().removeAttribute(LISTA_APURACAO_PENALIDADE_PERIODICIDADE);

			Long idSituacaoPendente = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
			model.addAttribute("idSituacaoPendente", idSituacaoPendente);

			Long idSituacaoParcial = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));
			model.addAttribute("idSituacaoParcial", idSituacaoParcial);

			Long idNotaDebito = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
			model.addAttribute("idNotaDebito", idNotaDebito);

			model.addAttribute(LISTA_SITUACAO_NOTA, controladorCreditoDebito.listarCreditoDebitoSituacao());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaNotaDebitoCredito";
	}

	/**
	 * Método responsável por pesquisar notas de débito e crédito cadastradas no
	 * sistema.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 *
	 */
	@RequestMapping("pesquisarNotaDebitoCredito")
	public String pesquisarNotaDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		try {
			Map<String, Object> filtro = new HashMap<String, Object>();

			creditoDebitoVO.setChavesPrimarias(null);

			prepararFiltro(filtro, creditoDebitoVO);

			Long idNotaDebito = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
			Long idNotaDebitoPenalidade = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));

			Long idNotaCredito = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));

			Long idNotaCreditoPenalidade = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE));

			String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
			if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
				if ("debito".equals(indicadorCreditoDebito)) {
					filtro.put(CHAVES_TIPO_DOCUMENTO, new Long[] { idNotaDebito, idNotaDebitoPenalidade });
				} else if ("credito".equals(indicadorCreditoDebito)) {
					filtro.put(CHAVES_TIPO_DOCUMENTO, new Long[] { idNotaCredito, idNotaCreditoPenalidade });
				}
			} else {
				filtro.put(CHAVES_TIPO_DOCUMENTO,
						new Long[] { idNotaDebito, idNotaCredito, idNotaDebitoPenalidade, idNotaCreditoPenalidade });
			}

			Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();

			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				pesquisarPontosConsumo(creditoDebitoVO, request);
			}

			super.adicionarFiltroPaginacao(request, filtro);
			controladorFatura.validarFiltroPesquisaNotaDebito(filtro);

			Collection<Fatura> listaNotaDebitoCredito = controladorFatura.consultarFatura(filtro);

			Collection<NotaDebitoCreditoVO> listaNotaDebitoCreditoVO = this
					.popularNotaDebitoCreditoVO(listaNotaDebitoCredito);

			model.addAttribute(LISTA_NOTA_DEBITO_CREDITO,
					super.criarColecaoPaginada(request, filtro, listaNotaDebitoCreditoVO));
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaNotaDebitoCredito(request, model);
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo de um imóvel na tela de
	 * pesquisa de nota de débito e crédito.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 */
	@RequestMapping("pesquisarPontoConsumoNotaDebitoCredito")
	public String pesquisarPontoConsumoNotaDebitoCredito(HttpServletRequest request, Model model,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		try {
			pesquisarPontosConsumo(request, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPesquisaNotaDebitoCredito(request, model);
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de nota de débito e
	 * crédito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVOParam
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoNotaDebitoCredito")
	public String exibirDetalhamentoNotaDebitoCredito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVOParam, BindingResult bindingResult) {

		String view = "exibirDetalhamentoNotaDebitoCredito";

		CreditoDebitoVO creditoDebitoVO = creditoDebitoVOParam;

		request.getSession().setAttribute(FILTRO_NOTA_CREDITO_DEBITO_VO, creditoDebitoVO);
		try {
			Fatura notaDebitoCredito = (Fatura) controladorCobranca.obterFatura(creditoDebitoVO.getChavePrimaria(),
					"listaFaturaItem", "pontoConsumo", "cliente", "tipoDocumento", "motivoCancelamento",
					"creditoDebitoSituacao");
			creditoDebitoVO = new CreditoDebitoVO();
			popularForm(creditoDebitoVO, request, notaDebitoCredito);
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_NOTA_DEBITO_CREDITO;
		}

		return view;
	}

	/**
	 * Método utilizado para recuperar o filtro de pesquisa utilizado para realizar
	 * o detalhamento.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("voltarParaPesquisarNotaCreditoDebito")
	public String voltarParaPesquisarCreditoDebito(Model model, HttpServletRequest request) throws GGASException {

		CreditoDebitoVO creditoDebitoVO = (CreditoDebitoVO) request.getSession()
				.getAttribute(FILTRO_NOTA_CREDITO_DEBITO_VO);

		request.getSession().removeAttribute(FILTRO_NOTA_CREDITO_DEBITO_VO);

		return this.pesquisarNotaDebitoCredito(model, request, creditoDebitoVO, null);
	}

	/**
	 * Método responsável por exibir a tela de inclusão de nota de débito e crédito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirInclusaoNotaDebitoCredito")
	public String exibirInclusaoNotaDebitoCredito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		String view = "exibirInclusaoNotaDebitoCredito";

		try {
			carregarCampos(model, request, creditoDebitoVO);
			saveToken(request);

			ConstanteSistema constanteTitulo = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idIntegracaoFuncao", Long.valueOf(constanteTitulo.getValor()));
			Collection<IntegracaoSistemaFuncao> listaIntegracaoFuncao = controladorIntegracao
					.consultarIntegracaoSistemaFuncao(filtro);

			if (listaIntegracaoFuncao.iterator().hasNext()) {
				if (listaIntegracaoFuncao.iterator().next().isHabilitado()) {
					request.getSession().setAttribute("integracaoBaixa", true);
				} else {
					request.getSession().setAttribute("integracaoBaixa", false);
				}
			}

			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		} catch (GGASException e) {
			view = "forward:/exibirPesquisaNotaDebitoCredito";
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por inserir a nota de débito e crédito no sistema.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 */
	@Transactional
	@RequestMapping("incluirNotaDebitoCredito")
	public String incluirNotaDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		String view = "forward:/exibirInclusaoNotaDebitoCredito";
		try {
			this.salvarNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
			view = this.pesquisarNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por salvar e validar a nota de débito e crédito no
	 * sistema.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public void salvarNotaDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) throws GGASException {
		validarToken(request);

		Fatura notaDebitoCredito = (Fatura) controladorFatura.criar();
		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			controladorFatura.validarPontoConsumoSelecionado(idPontoConsumo);
		}
		this.popularNotaDebitoCredito(creditoDebitoVO, notaDebitoCredito, request);
		controladorFatura.validarDadosNotaDebito(notaDebitoCredito);
		Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
		Pair<byte[], Fatura> retornoInsercaoNota = controladorFatura.inserirNotaDebitoCredito(notaDebitoCredito,
				chavesPrimarias, null);
		Long chavePrimaria = (Long) retornoInsercaoNota.getSecond().getChavePrimaria();
		creditoDebitoVO.setChavePrimaria(chavePrimaria);

		// Ajustar as apurações
		List<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoPenalidadePeriodicidade = 
				(List<ApuracaoQuantidadePenalidadePeriodicidade>) request.getSession().getAttribute(LISTA_APURACAO_PENALIDADE_PERIODICIDADE);

		if (listaApuracaoPenalidadePeriodicidade != null) {
			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : listaApuracaoPenalidadePeriodicidade) {
				controladorApuracaoPenalidade
						.atualizarApuracaoQuantidadePenalidadePeriodicidade(apuracaoQuantidadePenalidadePeriodicidade);
			}
		}

		byte[] relatorioNotaDebitoCredito = retornoInsercaoNota.getFirst();
		if (relatorioNotaDebitoCredito != null) {
			request.getSession().setAttribute(RELATORIO_NOTA_DEBITO_CREDITO, relatorioNotaDebitoCredito);
			request.setAttribute(GERAR_RELATORIO_NOTA_DEBITO_CREDITO, "true");
		}
		// Setando ponto de consumo null para não
		// pesquisar os pontos de consumo para a
		// tela de pesquisa.
		creditoDebitoVO.setIdPontoConsumo(null);

		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA,
				GenericAction.obterMensagem(Fatura.NOTA_DEBITO_CREDITO));

		request.getSession().removeAttribute(LISTA_NOTA_CREDITO);
		request.getSession().removeAttribute(LISTA_NOTA_DEBITO);
	}

	/**
	 * Método responsável por exibir a tela de conciliar de nota de débito e
	 * crédito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirConciliarNotaDebitoCredito")
	public String exibirConciliarNotaDebitoCredito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		String view = "exibirConciliarNotaDebitoCredito";
		try {
			this.exibirConciliarNotaDebitoCredito(model, request, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_NOTA_DEBITO_CREDITO;
		}

		return view;
	}

	private void exibirConciliarNotaDebitoCredito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO) throws GGASException {
		Long idImovel = creditoDebitoVO.getIdImovel();
		Long[] chavesPontoConsumo = null;
		Long idCliente = creditoDebitoVO.getIdCliente();
		Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();

		controladorFatura.validarSelecaoNotas(chavesPrimarias, idCliente, idImovel);

		if ((idCliente != null) && (idCliente > 0)) {
			Cliente cliente = (Cliente) controladorCliente.obter(idCliente, "enderecos");
			this.popularFormCliente(creditoDebitoVO, cliente);
		}

		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, "imovel",
					ATRIBUTO_QUADRA_FACE);
			this.popularFormPontoConsumo(creditoDebitoVO, pontoConsumo);
		} else {
			if ((idImovel != null) && (idImovel > 0)) {
				Collection<PontoConsumo> listaPontosConsumo = controladorImovel
						.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
				if (listaPontosConsumo != null && !listaPontosConsumo.isEmpty()) {
					chavesPontoConsumo = new Long[listaPontosConsumo.size()];

					int count = 0;
					for (PontoConsumo pontoConsumo : listaPontosConsumo) {
						chavesPontoConsumo[count] = pontoConsumo.getChavePrimaria();
						count++;
					}

				}
			}
		}

		Collection<Fatura> listaNotasDebito = null;
		Collection<Fatura> listaNotasCredito = null;
		Collection<Fatura> listaNotasCreditoPenalidade = null;
		Collection<Fatura> listaFaturas = null;

		Long idNotaDebito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		Long idNotaCredito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));
		Long idNotaCreditoPenalidade = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE));
		Long idTipoDocumentoFatura = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));

		Long idSituacaoPendente = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		Long idSituacaoParcial = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ControladorFatura.ORDENACAO_NUMERO_FATURA_DESC, Boolean.TRUE);
		filtro.put(SITUACAO_PAGAMENTO, new Long[] { idSituacaoPendente, idSituacaoParcial });

		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			filtro.put(ID_PONTO_CONSUMO, idPontoConsumo);
		}
		if (chavesPontoConsumo != null) {
			filtro.put(CHAVES_PONTO_CONSUMO, chavesPontoConsumo);
		}

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);

			filtro.put(ID_TIPO_DOCUMENTO, idNotaDebito);
			listaNotasDebito = controladorFatura.consultarFatura(filtro);

			filtro.remove(ID_TIPO_DOCUMENTO);

			filtro.put(ID_TIPO_DOCUMENTO, idNotaCredito);
			listaNotasCredito = controladorFatura.consultarFatura(filtro);

			filtro.put(ID_TIPO_DOCUMENTO, idNotaCreditoPenalidade);
			listaNotasCreditoPenalidade = controladorFatura.consultarFatura(filtro);

		}

		filtro.remove(CHAVES_PRIMARIAS);

		if (listaNotasDebito == null || listaNotasDebito.isEmpty()) {

			filtro.remove(ID_TIPO_DOCUMENTO);
			filtro.put(ID_TIPO_DOCUMENTO, idNotaDebito);

			if ((idCliente == null) || (idCliente == 0)) {
				for (Fatura notaCredito : listaNotasCredito) {
					filtro.put(ID_CLIENTE, notaCredito.getCliente().getChavePrimaria());
				}
			}

			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				for (Fatura notaCredito : listaNotasCredito) {
					filtro.put(ID_PONTO_CONSUMO, notaCredito.getPontoConsumo().getChavePrimaria());
				}
			}

			listaNotasDebito = controladorFatura.consultarFatura(filtro);

			filtro.remove(ID_TIPO_DOCUMENTO);

			filtro.put(ID_TIPO_DOCUMENTO, idTipoDocumentoFatura);
			filtro.remove(CHAVES_PRIMARIAS);
			listaFaturas = controladorFatura.consultarFatura(filtro);

			if (listaNotasDebito != null && listaFaturas != null) {

				listaNotasDebito.addAll(listaFaturas);

			} else if (listaNotasDebito == null && listaFaturas != null) {

				listaNotasDebito = listaFaturas;

			}

		}

		if (listaNotasCredito == null || listaNotasCredito.isEmpty()) {

			filtro.remove(ID_TIPO_DOCUMENTO);
			filtro.put(ID_TIPO_DOCUMENTO, idNotaCredito);

			if ((idCliente == null) || (idCliente == 0)) {
				for (Fatura notaCredito : listaNotasCredito) {
					filtro.put(ID_CLIENTE, notaCredito.getCliente().getChavePrimaria());
				}
			}

			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				for (Fatura notaCredito : listaNotasCredito) {
					filtro.put(ID_PONTO_CONSUMO, notaCredito.getPontoConsumo().getChavePrimaria());
				}
			}

			listaNotasCredito = controladorFatura.consultarFatura(filtro);

			filtro.remove(ID_TIPO_DOCUMENTO);
			filtro.put(ID_TIPO_DOCUMENTO, idNotaCreditoPenalidade);

			listaNotasCreditoPenalidade = controladorFatura.consultarFatura(filtro);

			if (listaNotasCreditoPenalidade != null && !listaNotasCreditoPenalidade.isEmpty()) {
				if (listaNotasCredito != null && !listaNotasCredito.isEmpty()) {
					listaNotasCredito.addAll(listaNotasCreditoPenalidade);
				} else {
					listaNotasCredito = listaNotasCreditoPenalidade;
				}

			}

		}

		carregarSaldoNotas(listaNotasDebito, listaNotasCredito);

		if (CollectionUtils.isEmpty(listaNotasDebito) || CollectionUtils.isEmpty(listaNotasCredito)) {
			throw new NegocioException(Constantes.NOTA_DEBITO_CREDITO_NAO_CONCILIADA, Boolean.TRUE);
		}

		model.addAttribute(LISTA_NOTAS_DEBITO, listaNotasDebito);
		model.addAttribute(LISTA_NOTAS_CREDITO, listaNotasCredito);
		model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);

		saveToken(request);
	}

	/**
	 * Método responsável por conciliar a nota de débito e crédito no sistema.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("conciliarNotaDebitoCredito")
	public String conciliarNotaDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		String view = FORWARD_PESQUISAR_NOTA_DEBITO_CREDITO;
		try {
			Long[] chavesPrimariasDebitos = creditoDebitoVO.getChavesPrimariasDebitos();
			Long[] chavesPrimariasCreditos = creditoDebitoVO.getChavesPrimariasCreditos();

			validarToken(request);

			controladorFatura.validarNotasSelecionadas(chavesPrimariasDebitos, chavesPrimariasCreditos);

			if ((chavesPrimariasDebitos != null) && (chavesPrimariasDebitos.length > 0)
					&& (chavesPrimariasCreditos != null) && (chavesPrimariasCreditos.length > 0)) {
				controladorFatura.conciliarNotasDebitoCredito(chavesPrimariasDebitos, chavesPrimariasCreditos,
						getDadosAuditoria(request));
			}

			super.mensagemSucesso(model, Constantes.SUCESSO_NOTAS_CONCILIADA);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirConciliarNotaDebitoCredito";
		}

		return view;
	}

	/**
	 * Método responsável exibir a lista de notas de débito ou notas de crédito de
	 * um ponto de consumo.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping("exibirNotasDebitoCredito")
	public String exibirNotasDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		try {
			Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();
			Long idCliente = creditoDebitoVO.getIdCliente();
			String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();

			controladorFatura.validarFiltroExibicaoNotasDebitoCredito(idPontoConsumo, indicadorCreditoDebito,
					idCliente);

			Collection<Fatura> listaNotaDebitoCredito;
			if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
					filtro.put(CHAVES_PONTO_CONSUMO, new Long[] { idPontoConsumo });
				}
				if (idCliente != null) {
					filtro.put(ID_CLIENTE, idCliente);
				}
				filtro.put(MANTER_NOTA_DEBITO, Boolean.TRUE);
				filtro.put(PESQUISA, Boolean.FALSE);

				// FIXME: Substituir por constante

				Long idSituacaoPendente = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
				Long idSituacaoParcial = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));
				filtro.put(SITUACAO_PAGAMENTO, new Long[] { idSituacaoPendente, idSituacaoParcial });
				filtro.put(ControladorFatura.ORDENACAO_NUMERO_FATURA_DESC, Boolean.TRUE);

				// FIXME: Substituir por constante
				Long idNotaDebito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
				Long idNotaCredito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));
				// FIXME: Substituir por constante
				Long idFatura = Long.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_DOCUMENTO_FATURA));

				if (DEBITO.equals(indicadorCreditoDebito)) {
					request.getSession().removeAttribute(LISTA_NOTA_DEBITO);
					filtro.put("situacaoValida", Boolean.TRUE);
					filtro.put(CHAVES_TIPO_DOCUMENTO, new Long[] { idNotaCredito });
					listaNotaDebitoCredito = controladorFatura.consultarFatura(filtro);
					carregarSaldoNotas(null, listaNotaDebitoCredito);
					request.setAttribute(LISTA_NOTA_CREDITO, listaNotaDebitoCredito);
					request.getSession().setAttribute(LISTA_NOTA_CREDITO, listaNotaDebitoCredito);
				} else if (CREDITO.equals(indicadorCreditoDebito)
						|| CREDITO_PENALIDADE.equals(indicadorCreditoDebito)) {
					request.getSession().removeAttribute(LISTA_NOTA_CREDITO);
					filtro.put("situacaoValida", Boolean.TRUE);
					filtro.put(CHAVES_TIPO_DOCUMENTO, new Long[] { idNotaDebito, idFatura });
					listaNotaDebitoCredito = controladorFatura.consultarFatura(filtro);
					carregarSaldoNotas(listaNotaDebitoCredito, null);
					request.setAttribute(LISTA_NOTA_DEBITO, listaNotaDebitoCredito);
					request.getSession().setAttribute(LISTA_NOTA_DEBITO, listaNotaDebitoCredito);
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirInclusaoNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
	}

	/**
	 * Método responsável imprimir na tela o relatório da nota inserida, caso seja
	 * nota de débito.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @return relatório - {@link ResponseEntity}
	 * 
	 */
	@RequestMapping("gerarRelatorioNotaDebitoCredito")
	public ResponseEntity<byte[]> gerarRelatorioNotaDebitoCredito(HttpServletRequest request) throws Exception {

		byte[] relatorioNotaDebitoCredito = (byte[]) request.getSession().getAttribute(RELATORIO_NOTA_DEBITO_CREDITO);
		request.removeAttribute(GERAR_RELATORIO_NOTA_DEBITO_CREDITO);
		request.getSession().removeAttribute(RELATORIO_NOTA_DEBITO_CREDITO);

		ResponseEntity<byte[]> responseEntity = null;

		if (relatorioNotaDebitoCredito != null) {

			MediaType contentType = MediaType.parseMediaType(Constantes.MEDIA_TYPE);
			String filename = "relatorioNotaDebitoCredito.pdf";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(contentType);
			headers.setContentDispositionFormData(Constantes.HEADER_PARAM, filename);

			responseEntity = new ResponseEntity<>(relatorioNotaDebitoCredito, headers, HttpStatus.OK);
		}

		return responseEntity;
	}

	/**
	 * Método responsável por imprimir boleto de fatura pendente.
	 * 
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param response
	 *            - {@link HttpServletResponse}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("imprimirGerarFaturaPendente")
	public String imprimirGerarFaturaPendente(Model model, HttpServletRequest request, HttpServletResponse response,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		try {
			Fatura fatura = (Fatura) controladorCobranca.obterFatura(creditoDebitoVO.getChavePrimaria());

			byte[] arquivo = controladorFatura.gerarBoletoBancarioPadrao(fatura, Boolean.TRUE,
					Constantes.C_TIPO_DOCUMENTO_FATURA, creditoDebitoVO.getNovaDataVencimento(), true);

			imprimirPDF("boleto_nota_debito", arquivo, response);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.pesquisarNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
	}

	/**
	 * Imprimir pdf.
	 * 
	 */
	private void imprimirPDF(String nomeArquivo, byte[] relatorio, HttpServletResponse response) {

		if (relatorio != null) {
			try {
				response.setContentType("application/pdf");
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition", "attachment; filename=" + nomeArquivo + ".pdf");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e);
				throw new InfraestruturaException(e);
			}
		}
	}

	/**
	 * Método responsável por exibir a tela de cancelamento de nota de débito e
	 * crédito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirCancelamentoNotaDebitoCredito")
	public String exibirCancelamentoNotaDebitoCredito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) throws GGASException {

		String view = "exibirCancelamentoNotaDebitoCredito";

		request.getSession().setAttribute(FILTRO_NOTA_CREDITO_DEBITO_VO, creditoDebitoVO);

		try {
			Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
			controladorFatura.validarSituacaoNotasCancelamento(chavesPrimarias);

			Map<String, Object> filtro = new HashMap<String, Object>();
			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
			}

			Collection<Fatura> listaNotas = controladorFatura.consultarFatura(filtro);
			model.addAttribute(LISTA_NOTA_DEBITO_CREDITO, listaNotas);
			model.addAttribute(LISTA_MOTIVO_CANCELAMENTO, controladorEntidadeConteudo.obterListaMotivoCancelamento());
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_NOTA_DEBITO_CREDITO;
		}

		return view;
	}

	/**
	 * Método responsável por cancelar as notas de débito e crédito selecionadas.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param chavesNotasSelecionadas
	 * @param idMotivoCancelamento
	 *            - {@link Long}
	 * @param descricaoCancelamento
	 *            - {@link String}
	 * @return view - {@link String}
	 */
	@RequestMapping("cancelarNotaDebitoCredito")
	public String cancelarNotaDebitoCredito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		String view = "forward:/exibirCancelamentoNotaDebitoCredito";

		try {
			validarToken(request);

			controladorFatura.cancelarNotaDebitoCredito(creditoDebitoVO.getChavesNotasSelecionadas(),
					creditoDebitoVO.getIdMotivoCancelamento(), creditoDebitoVO.getDescricaoCancelamento(),
					getDadosAuditoria(request));

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_CANCELADA,
					GenericAction.obterMensagem(Fatura.NOTA_DEBITO_CREDITO));
			view = this.exibirPesquisaNotaDebitoCredito(request, model);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por imprimir a segunda via da nota de crédito / débito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param response
	 *            - {@link HttpServletResponse}
	 * @param idNotaDebitoCredito
	 *            - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("imprimirSegundaVia")
	public String imprimirSegundaVia(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "chavePrimaria", required = false) Long idNotaDebitoCredito) throws Exception {

		try {
			if ((idNotaDebitoCredito != null) && (idNotaDebitoCredito > 0)) {

				Fatura notaDebitoCredito = (Fatura) controladorCobranca.obterFatura(idNotaDebitoCredito,
						"listaFaturaItem", "faturaGeral");
				notaDebitoCredito.setDadosAuditoria(getDadosAuditoria(request));

				byte[] relatorioNotaDebitoCredito = controladorFatura.gerarRelatorioNotaDebitoCredito(notaDebitoCredito,
						Boolean.TRUE);

				if (relatorioNotaDebitoCredito != null) {
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType("application/pdf");
					response.setContentLength(relatorioNotaDebitoCredito.length);
					response.addHeader("Content-Disposition", "attachment; filename=relatorioNotaDebitoCredito.pdf");
					servletOutputStream.write(relatorioNotaDebitoCredito, 0, relatorioNotaDebitoCredito.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaNotaDebitoCredito(request, model);
	}

	/**
	 * Exibir penalidades ponto consumo.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - String
	 */
	@RequestMapping("exibirPenalidadesPontoConsumo")
	public String exibirPenalidadesPontoConsumo(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {
		try {
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaFinal = this
					.exibirPenalidadesPontoConsumo(creditoDebitoVO);

			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
			request.getSession().setAttribute(LISTA_APURACAO_PENALIDADES, listaFinal);
			model.addAttribute(LISTA_APURACAO_PENALIDADES, listaFinal);
			request.getSession().removeAttribute(LISTA_APURACAO_PENALIDADE_PERIODICIDADE);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirInclusaoNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
	}

	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> exibirPenalidadesPontoConsumo(
			CreditoDebitoVO creditoDebitoVO) throws GGASException {
		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();

		Map<String, Object> params = new HashMap<String, Object>();
		Collection<ApuracaoQuantidadePenalidadePeriodicidade> lista = null;
		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaFinal = new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();

		if (idPontoConsumo != null) {
			params.put(ID_PONTO_CONSUMO, idPontoConsumo);
			params.put("possuiQPNR", Boolean.TRUE);
			lista = controladorApuracaoPenalidade.listarApuracaoQuantidadePenalidadePeriodicidades(params);
			if (CollectionUtils.isEmpty(lista)) {
				throw new NegocioException(Constantes.ERRO_NOTA_DEBITO_CREDITO_NAO_EXISTE_PENALIDADE_APURADA, true);
			}
		}

		Contrato contrato = null;
		PontoConsumo pontoConsumo = null;

		BigDecimal saldoQPNR = BigDecimal.ZERO;
		BigDecimal saldoRecuperado = BigDecimal.ZERO;
		Integer anoMesCorrente = Util.obterAnoMesCorrente();
		if (lista != null) {
			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : lista) {

				if (apuracaoQuantidadePenalidadePeriodicidade.getListaRecuperacao() != null) {
					for (Recuperacao recuperacao : apuracaoQuantidadePenalidadePeriodicidade.getListaRecuperacao()) {
						Integer anoMesRecuperacao = Util.obterAnoMes(recuperacao.getDataRecuperacao());
						if (anoMesCorrente.equals(anoMesRecuperacao)) {
							saldoRecuperado = saldoRecuperado.add(recuperacao.getMedicaoQR());
						}
					}
				}

				pontoConsumo = apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo();
				if (apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual() != null) {
					contrato = apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual();
				}

				BigDecimal recuperadoAQPP = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
				saldoQPNR = saldoQPNR.add(recuperadoAQPP);

			}
		}

		saldoQPNR = saldoQPNR.add(saldoRecuperado);
		if (contrato != null) {
			contrato = (Contrato) controladorContrato.obter(contrato.getChavePrimaria());
		}
		HistoricoConsumo consumo = controladorHistoricoConsumo.obterUltimoHistoricoConsumoFaturado(idPontoConsumo);

		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades = new ArrayList<ContratoPontoConsumoModalidade>();

		contratoPontoConsumoModalidades
				.addAll(controladorContrato.consultarContratoPontoConsumoModalidades(contrato, pontoConsumo));
		Boolean existeConfiguracaoModalidade = Boolean.FALSE;

		BigDecimal pctMaximo = BigDecimal.ZERO;
		BigDecimal pctMinimo = BigDecimal.ZERO;
		Integer anosValidade = null;

		Boolean habilitaSaldo = Boolean.TRUE;

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumoModalidades) {

			if (contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR() != null) {

				existeConfiguracaoModalidade = Boolean.TRUE;

				if (contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR() != null
						&& contratoPontoConsumoModalidade.getPercentualMinimoQDCContratoQPNR() != null) {
					pctMinimo = contratoPontoConsumoModalidade.getPercentualMinimoQDCContratoQPNR();
					if (contrato.getDataVencimentoObrigacoes() != null
							&& Util.getDataCorrente(false).compareTo(contrato.getDataVencimentoObrigacoes()) > 0
							&& contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR() != null) {
						pctMaximo = contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR();
					} else {
						pctMaximo = contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR();
					}
				}

				Date dataCorrente = Util.getDataCorrente(false);
				if (contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR() != null
						&& contratoPontoConsumoModalidade.getDataFimRetiradaQPNR() != null) {
					if (contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0
							&& dataCorrente.compareTo(contratoPontoConsumoModalidade.getDataFimRetiradaQPNR()) >= 0) {
						habilitaSaldo = Boolean.FALSE;
					}
				} else if (contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR() != null) {
					if (contratoPontoConsumoModalidade.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0) {
						habilitaSaldo = Boolean.FALSE;
					}
				} else if (contratoPontoConsumoModalidade.getDataFimRetiradaQPNR() != null
						&& dataCorrente.compareTo(contratoPontoConsumoModalidade.getDataFimRetiradaQPNR()) >= 0) {
					habilitaSaldo = Boolean.FALSE;
				}

				anosValidade = contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR();

			}

		}

		if (!existeConfiguracaoModalidade && contrato.getAnosValidadeRetiradaQPNR() != null) {

			existeConfiguracaoModalidade = Boolean.TRUE;

			if (contrato.getPercentualQDCContratoQPNR() != null
					&& contrato.getPercentualMinimoQDCContratoQPNR() != null) {
				pctMinimo = contrato.getPercentualMinimoQDCContratoQPNR();
				if (contrato.getDataVencimentoObrigacoes() != null
						&& Util.getDataCorrente(false).compareTo(contrato.getDataVencimentoObrigacoes()) > 0
						&& contrato.getPercentualQDCFimContratoQPNR() != null) {
					pctMaximo = contrato.getPercentualQDCFimContratoQPNR();
				} else {
					pctMaximo = contrato.getPercentualQDCContratoQPNR();
				}
			}

			Date dataCorrente = Util.getDataCorrente(false);
			if (contrato.getDataInicioRetiradaQPNR() != null && contrato.getDataFimRetiradaQPNR() != null) {
				if (contrato.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0
						&& dataCorrente.compareTo(contrato.getDataFimRetiradaQPNR()) >= 0) {
					habilitaSaldo = Boolean.FALSE;
				}
			} else if (contrato.getDataInicioRetiradaQPNR() != null) {
				if (contrato.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0) {
					habilitaSaldo = Boolean.FALSE;
				}
			} else if (contrato.getDataFimRetiradaQPNR() != null
					&& dataCorrente.compareTo(contrato.getDataFimRetiradaQPNR()) >= 0) {
				habilitaSaldo = Boolean.FALSE;
			}

			anosValidade = contrato.getAnosValidadeRetiradaQPNR();
		}

		if (habilitaSaldo) {

			String anoMesApuracaoAux = "01/" + Util.formatarAnoMes(consumo.getAnoMesFaturamento());
			Date dataApuracao = Util.converterCampoStringParaData("Data Apuracação", anoMesApuracaoAux,
					Constantes.FORMATO_DATA_BR);

			Date dataFimApuracao = Util.adicionarDiasData(dataApuracao, consumo.getDiasConsumo() - 1);
			BigDecimal totalQDC = controladorContrato.obterSomaQdcContratoPorPeriodo(contrato.getChavePrimaria(),
					dataApuracao, dataFimApuracao);
			BigDecimal qDCMedia = totalQDC.divide(new BigDecimal(consumo.getDiasConsumo()), BigDecimal.ROUND_UP);

			DateTime dataAux = new DateTime(dataApuracao);
			dataAux = dataAux.minusYears(anosValidade);
			Date dataApuracaoAux = dataAux.toDate();

			if (dataApuracao.compareTo(dataApuracaoAux) >= 0) {

				BigDecimal recuperadoAQPP = saldoQPNR;

				BigDecimal limiteInferior = BigDecimal.ZERO;
				BigDecimal limiteSuperior = BigDecimal.ZERO;
				BigDecimal faixaRecuperacao = BigDecimal.ZERO;
				BigDecimal mediaConsumo = BigDecimal.ZERO;
				BigDecimal volMaximo = BigDecimal.ZERO;
				BigDecimal volApurado = BigDecimal.ZERO;
				BigDecimal refencia = BigDecimal.ZERO;

				Integer qtdDias = consumo.getDiasConsumo();
				limiteInferior = qDCMedia.multiply(pctMinimo);
				limiteSuperior = qDCMedia.multiply(pctMaximo);
				faixaRecuperacao = limiteSuperior.subtract(limiteInferior);
				volMaximo = faixaRecuperacao.multiply(new BigDecimal(qtdDias));
				mediaConsumo = consumo.getConsumoApurado().divide(new BigDecimal(qtdDias), BigDecimal.ROUND_UP);
				volApurado = mediaConsumo.subtract(limiteInferior).multiply(new BigDecimal(qtdDias));

				if (volMaximo.compareTo(volApurado) < 0) {
					refencia = volMaximo;
				} else {
					refencia = volApurado;
				}

				if (recuperadoAQPP.compareTo(refencia) > 0) {
					recuperadoAQPP = refencia;
				}

				if (recuperadoAQPP.compareTo(BigDecimal.ZERO) > 0) {
					saldoQPNR = saldoQPNR.add(recuperadoAQPP);
				} else {
					recuperadoAQPP = BigDecimal.ZERO;
				}
				recuperadoAQPP = recuperadoAQPP.subtract(saldoRecuperado);

				if (recuperadoAQPP.compareTo(BigDecimal.ZERO) > 0) {
					saldoQPNR = recuperadoAQPP;
				} else {
					saldoQPNR = BigDecimal.ZERO;
				}

			}

		}
		if (lista != null) {
			listaFinal.addAll(lista);
		}

		creditoDebitoVO.setSaldoQPNR(saldoQPNR.toString());

		return listaFinal;
	}

	/**
	 * Calcular valor volume penalidade.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("calcularValorVolumePenalidade")
	public String calcularValorVolumePenalidade(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		try {
			Long[] idsApuracaoPenalidade = creditoDebitoVO.getIdsApuracaoPenalidade();
			Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();
			String[] valorQPNR = creditoDebitoVO.getValorQPNR();
			String[] volumeRecuperacao = creditoDebitoVO.getVolumeRecuperacao();
			Long idRubrica = creditoDebitoVO.getIdRubrica();
			String saldoQPNR = creditoDebitoVO.getSaldoQPNR();
			Rubrica rubrica = null;
			if (idRubrica != null && idRubrica > 0) {
				rubrica = (Rubrica) controladorRubrica.obter(idRubrica);
			}

			List<ApuracaoQuantidadePenalidadePeriodicidade> lista = controladorApuracaoPenalidade
					.listarApuracaoQuantidadePenalidadePeriodicidadePorChaves(idsApuracaoPenalidade);

			BigDecimal valor = controladorApuracaoPenalidade.validarRecuperacaoPenalidade(valorQPNR, volumeRecuperacao,
					lista, idPontoConsumo, rubrica);

			String quantidade = calcularVolumeRecuperacao(volumeRecuperacao, saldoQPNR);

			creditoDebitoVO.setQuantidade(quantidade);
			creditoDebitoVO.setValor(
					Util.converterCampoValorDecimalParaString(VALOR_TOTAL_CAMPO, valor, Constantes.LOCALE_PADRAO));
			creditoDebitoVO.setValorTotal(
					Util.converterCampoValorDecimalParaString(VALOR_TOTAL_CAMPO, valor, Constantes.LOCALE_PADRAO));

			request.setAttribute(LISTA_APURACAO_PENALIDADE_PERIODICIDADE, lista);
			request.getSession().setAttribute(LISTA_APURACAO_PENALIDADE_PERIODICIDADE, lista);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirInclusaoNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);

	}

	/**
	 * Exibir prorrogacao nota credito debito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirProrrogacaoNotaCreditoDebito")
	public String exibirProrrogacaoNotaCreditoDebito(Model model, HttpServletRequest request,
			CreditoDebitoVO creditoDebitoVO, BindingResult bindingResult) {

		String view = "exibirProrrogacaoNotaDebitoCredito";

		try {
			Map<String, Object> filtro = new HashMap<String, Object>();

			Collection<NotaDebitoCreditoVO> listaNotaDebitVos = null;

			Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
			Boolean telaHistorico = creditoDebitoVO.getTelaHistorico();

			if (telaHistorico != null && telaHistorico) {
				chavesPrimarias = (Long[]) request.getSession()
						.getAttribute(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			}

			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
				Collection<Fatura> faturas = controladorFatura.consultarFatura(filtro);
				if (faturas != null) {
					controladorFatura.validarSituacaoPagamentoFatura((ArrayList<Fatura>) faturas);
					listaNotaDebitVos = this.popularNotaDebitoCreditoVO(faturas);
				}
			}

			model.addAttribute("listaNotaDebitVos", listaNotaDebitVos);

			if (telaHistorico != null && !telaHistorico) {
				request.getSession().setAttribute(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			}

			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirPesquisaNotaDebitoCredito";
		}

		return view;
	}

	/**
	 * Prorrogar nota credito debito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param creditoDebitoVO
	 *            - {@link CreditoDebitoVO}
	 * @param bindingResult
	 *            - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("prorrogarNotaCreditoDebito")
	public String prorrogarNotaCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) throws Exception {
		
		String view = "forward:/exibirProrrogacaoNotaCreditoDebito";

		try {
			Map<String, Object> filtro = new HashMap<String, Object>();

			Long[] chavesPrimarias = (Long[]) request.getSession().getAttribute(CHAVES_PRIMARIAS);

			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);

			List<Fatura> faturas = (ArrayList<Fatura>) controladorFatura.consultarFatura(filtro);

			List<HistoricoNotaDebitoCredito> historicos = popularHistoticoNotaDebitoCredito(faturas, creditoDebitoVO,
					request);

			this.inserirHistoricosNotasDEbitoCredito(historicos);

			this.atualizarFaturas(faturas);
			
			controladorFatura.atualizarVencimentoNotaDebito(faturas);

			request.getSession().setAttribute("chaveNotaSelecionada", null);
			
//			for (Fatura fatura : faturas) {
//				DocumentoCobranca documentoCobranca = controladorDocumentoCobranca.obterUltimoDocumentoCobranca(fatura.getChavePrimaria());
//
//				if (documentoCobranca != null) {
//					documentoCobranca.setDataVencimento(fatura.getDataVencimento());
//					documentoCobranca.setHabilitado(Boolean.TRUE);
//					documentoCobranca.setUltimaAlteracao(Calendar.getInstance().getTime());
//
//					controladorArrecadacao.inserirCobrancaBancariaMovimentoPorOcorrencia(fatura, null,
//							Constantes.C_OCORRENCIA_ENVIO_ALTERACAO_VENCIMENTO_TITULO_SANTANDER, documentoCobranca);
//					
//					controladorDocumentoCobranca.atualizar(documentoCobranca);
//				}
//			}
			
			this.atualizarIntegracaoFaruras(faturas);
			
			super.mensagemSucesso(model, Constantes.NOTA_DEBITO_CREDITO_PRORROGADA_COM_SUCESSO);
			view = this.pesquisarNotaDebitoCredito(model, request, creditoDebitoVO, bindingResult);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Exibir historico nota debito credito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirHistoricoNotaDebitoCredito")
	public String exibirHistoricoNotaDebitoCredito(Model model,
			@RequestParam(value = "chaveNotaSelecionada", required = false) Long chavePrimaria) throws Exception {

		Collection<HistoricoNotaDebitoCredito> historico = controladorFatura
				.getListaHistoricoNotaDebitoCredito(chavePrimaria);

		if (historico != null && !historico.isEmpty()) {
			model.addAttribute("listaHistorico", historico);
		}

		return "exibirHistoricoNotaDebitoCredito";
	}

	private List<HistoricoNotaDebitoCredito> popularHistoticoNotaDebitoCredito(Collection<Fatura> faturas,
			CreditoDebitoVO creditoDebitoVO, HttpServletRequest request) throws GGASException {

		List<HistoricoNotaDebitoCredito> historicos = new ArrayList<HistoricoNotaDebitoCredito>();
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
		Usuario usuario = dadosAuditoria.getUsuario();

		String observacoes = creditoDebitoVO.getObservacoesNota();
		String dataProrrogacao = creditoDebitoVO.getDataProrrogacao();

		for (Fatura fatura : faturas) {
			HistoricoNotaDebitoCredito historicoNotaDebitoCredito = new HistoricoNotaDebitoCredito();
			historicoNotaDebitoCredito.setDataAnterior(fatura.getDataVencimento());
			historicoNotaDebitoCredito.setFatura(fatura);
			historicoNotaDebitoCredito.setUsuario(usuario);
			historicoNotaDebitoCredito.setObservacoes(observacoes);
			if (dataProrrogacao != null) {
				Date novaData = Util.converterCampoStringParaData("Data Prorrogação", dataProrrogacao,
						Constantes.FORMATO_DATA_BR);
				if (Util.compararDatas(novaData, fatura.getDataVencimento()) <= 0) {
					throw new NegocioException(Constantes.ERRO_DATA_PRORROGACAO_NOTA_DEBITO_INVALIDA, true);
				}
				fatura.setDataVencimento(Util.converterCampoStringParaData("Data Prorrogação", dataProrrogacao,
						Constantes.FORMATO_DATA_BR));
				historicoNotaDebitoCredito.setDataProrrogacao(fatura.getDataVencimento());
			}
			historicos.add(historicoNotaDebitoCredito);
		}

		return historicos;
	}

	private void atualizarIntegracaoFaruras(List<Fatura> faturas) throws NegocioException, ConcorrenciaException {

		Long idNotaDebito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		Long idNotaDebitoPenalidade = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));
		Long idNotaCredito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));
		Long idNotaCreditoPenalidade = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE));

		for (Fatura fatura : faturas) {

			if (fatura.getTipoDocumento().getChavePrimaria() == idNotaCredito
					|| fatura.getTipoDocumento().getChavePrimaria() == idNotaCreditoPenalidade) {
				controladorIntegracao.atualizarIntegracaoFatura(fatura, false);
			}
			if (fatura.getTipoDocumento().getChavePrimaria() == idNotaDebito
					|| fatura.getTipoDocumento().getChavePrimaria() == idNotaDebitoPenalidade) {
				controladorIntegracao.atualizarIntegracaoFatura(fatura, true);
			}
		}
	}

	private void inserirHistoricosNotasDEbitoCredito(List<HistoricoNotaDebitoCredito> historicos)
			throws NegocioException {

		for (HistoricoNotaDebitoCredito historico : historicos) {
			controladorFatura.inserirHistoricoNotaDebitoCredito(historico);
		}
	}

	private void atualizarFaturas(List<Fatura> faturas) throws GGASException {

		for (Fatura fatura : faturas) {
			controladorFatura.atualizar(fatura);

		}
	}

	private String calcularVolumeRecuperacao(String[] volumeRecuperacao, String saldoQPNR)
			throws FormatoInvalidoException, NegocioException {

		BigDecimal saldoMaximoPermitido = BigDecimal.ZERO;
		BigDecimal valorTotal = BigDecimal.ZERO;
		BigDecimal volumeRecuperacaoValor = BigDecimal.ZERO;
		for (int i = 0; i < volumeRecuperacao.length; i++) {
			String volumeRecuperacaoString = volumeRecuperacao[i];
			volumeRecuperacaoValor = Util.converterCampoStringParaValorBigDecimal("Volume Recuperação",
					volumeRecuperacaoString, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			valorTotal = valorTotal.add(volumeRecuperacaoValor);

		}
		saldoMaximoPermitido = Util.converterCampoStringParaValorBigDecimal("Saldo QPNR", saldoQPNR.replace(".", ","),
				Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
		if (valorTotal.compareTo(saldoMaximoPermitido) > 0) {
			throw new NegocioException(Constantes.ERRO_VOLUMERECUPERAVEL_MAIOR_QPNR, true);
		}

		return valorTotal.toString();
	}

	private void carregarSaldoNotas(Collection<Fatura> listaNotasDebito, Collection<Fatura> listaNotasCredito)
			throws GGASException {

		if (listaNotasDebito != null) {
			for (Fatura notaDebito : listaNotasDebito) {
				BigDecimal saldo = notaDebito.getValorTotal().subtract(notaDebito.getValorConciliado());
				BigDecimal valorRecebimento = controladorCobranca
						.obterValorRecebimentoPelaFatura(notaDebito.getChavePrimaria());
				if (valorRecebimento != null) {
					saldo = saldo.subtract(valorRecebimento);
				}
				notaDebito.setValorConciliado(saldo);
			}
		}

		if (listaNotasCredito != null) {
			for (Fatura notaCredito : listaNotasCredito) {
				notaCredito.setValorConciliado(notaCredito.getValorTotal().subtract(notaCredito.getValorConciliado()));
			}
		}
	}

	private void popularNotaDebitoCredito(CreditoDebitoVO creditoDebitoVO, Fatura notaDebitoCredito,
			HttpServletRequest request) throws GGASException {

		FaturaItem faturaItem = (FaturaItem) controladorFatura.criarFaturaItem();
		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();

		Long idCliente = creditoDebitoVO.getIdCliente();
		if ((idCliente != null) && (idCliente > 0)) {
			Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
			notaDebitoCredito.setCliente(cliente);
		}

		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(idPontoConsumo);
			notaDebitoCredito.setPontoConsumo(pontoConsumo);

			if (pontoConsumo.getSegmento() != null) {
				notaDebitoCredito.setSegmento(pontoConsumo.getSegmento());
			}

			consultaCliente(notaDebitoCredito, idPontoConsumo, idCliente);

			consultaContrato(notaDebitoCredito, pontoConsumo);
		}

		notaDebitoCredito.setValorConciliado(BigDecimal.ZERO);

		String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			Long idTipoDocumento = null;
			if (DEBITO.equals(indicadorCreditoDebito)) {
				idTipoDocumento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
				String dataVencimento = creditoDebitoVO.getDataVencimento();
				if (!StringUtils.isEmpty(dataVencimento)) {
					Date vencimento = Util.converterCampoStringParaData(Fatura.DATA_VENCIMENTO, dataVencimento,
							Constantes.FORMATO_DATA_BR);
					notaDebitoCredito.setDataVencimento(vencimento);
					// TODO ver
					// faturaItem.setDataVencimento(vencimento)
				}
			} else if (CREDITO.equals(indicadorCreditoDebito)) {
				idTipoDocumento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));
				notaDebitoCredito.setDataVencimento(Calendar.getInstance().getTime());
				// TODO ver.
				// faturaItem.setDataVencimento(Calendar.getInstance().getTime())
			} else if (CREDITO_PENALIDADE.equals(indicadorCreditoDebito)) {
				idTipoDocumento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO_PENALIDADE));
				notaDebitoCredito.setDataVencimento(Calendar.getInstance().getTime());
			}
			TipoDocumento tipoDocumento = controladorArrecadacao.obterTipoDocumento(idTipoDocumento);
			notaDebitoCredito.setTipoDocumento(tipoDocumento);
		}

		String observacaoNota = creditoDebitoVO.getObservacaoNota();
		if (!StringUtils.isEmpty(observacaoNota)) {
			notaDebitoCredito.setObservacaoNota(observacaoNota);
		}

		faturaItem.setFatura(notaDebitoCredito);

		Long idRubrica = creditoDebitoVO.getIdRubrica();
		if ((idRubrica != null) && (idRubrica > 0)) {
			Rubrica rubrica = (Rubrica) controladorRubrica.obter(idRubrica);
			faturaItem.setRubrica(rubrica);
		}

		String qtd = creditoDebitoVO.getQuantidade();
		BigDecimal quantidade = null;
		if (!StringUtils.isEmpty(qtd)) {
			quantidade = Util.converterCampoStringParaValorBigDecimal("Quantidade", qtd, Constantes.FORMATO_VALOR_BR,
					Constantes.LOCALE_PADRAO);
			faturaItem.setQuantidade(quantidade);
		}

		String valorTotal = creditoDebitoVO.getValor();
		if (!StringUtils.isEmpty(valorTotal)) {
			BigDecimal valor = Util.converterCampoStringParaValorBigDecimal(VALOR_TOTAL_CAMPO, valorTotal,
					Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			faturaItem.setValorUnitario(valor);
			if (quantidade != null) {
				valor = valor.multiply(quantidade);
				faturaItem.setValorTotal(valor);
				notaDebitoCredito.setValorTotal(valor);
			}
		}

		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setDadosAuditoria(getDadosAuditoria(request));

		Long idSituacaoNormal = Long.valueOf((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL));
		CreditoDebitoSituacao creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(idSituacaoNormal);
		notaDebitoCredito.setCreditoDebitoSituacao(creditoDebitoSituacao);

		Long idSituacaoPendente = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(idSituacaoPendente);
		notaDebitoCredito.setSituacaoPagamento(situacaoPagamento);
		notaDebitoCredito.setCobrada(Boolean.TRUE);

		faturaItem.setNumeroSequencial(1);

		notaDebitoCredito.getListaFaturaItem().add(faturaItem);
		notaDebitoCredito.setDataEmissao(Util.getDataCorrente(false));
		notaDebitoCredito.setDadosAuditoria(getDadosAuditoria(request));
		notaDebitoCredito.setProvisaoDevedoresDuvidosos(Boolean.FALSE);
	}

	private void consultaCliente(Fatura notaDebitoCredito, Long idPontoConsumo, Long idCliente) throws GGASException {
		if ((idCliente == null) || (idCliente <= 0)) {
			Cliente cliente = controladorCliente.obterClientePorPontoConsumoDeContratoAtivo(idPontoConsumo);
			notaDebitoCredito.setCliente(cliente);
		}
	}

	private void consultaContrato(Fatura notaDebitoCredito, PontoConsumo pontoConsumo) throws GGASException {
		Contrato contrato = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo).getContrato();

		// TKT 3332 ok
		if (contrato == null) {
			contrato = controladorContrato
					.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria()).getContrato();
		}

		if (contrato != null) {
			contrato = (Contrato) controladorContrato.obter(contrato.getChavePrimaria());
			notaDebitoCredito.setContratoAtual(contrato);

			if (contrato.getChavePrimariaPai() != null) {
				Contrato contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
				notaDebitoCredito.setContrato(contratoPai);
			} else {
				notaDebitoCredito.setContrato(contrato);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void carregarCampos(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO)
			throws GGASException {

		creditoDebitoVO.setValorTotal(creditoDebitoVO.getValor());
		pesquisarPontosConsumo(creditoDebitoVO, request);

		if (!isPostBack(request)) {
			creditoDebitoVO.setIndicadorCreditoDebito(null);
		} else {
			Collection<Fatura> listaNotaCredito = (Collection<Fatura>) request.getSession()
					.getAttribute(LISTA_NOTA_CREDITO);
			Collection<Fatura> listaNotaDebito = (Collection<Fatura>) request.getSession()
					.getAttribute(LISTA_NOTA_DEBITO);

			if ((listaNotaCredito != null) && (!listaNotaCredito.isEmpty())) {
				model.addAttribute(LISTA_NOTA_CREDITO, listaNotaCredito);
			} else if ((listaNotaDebito != null) && (!listaNotaDebito.isEmpty())) {
				model.addAttribute(LISTA_NOTA_DEBITO, listaNotaDebito);
			}
		}

		Long idImovel = creditoDebitoVO.getIdImovel();

		Imovel imovel = null;

		if ((idImovel != null) && (idImovel > 0)) {
			imovel = (Imovel) controladorImovel.obter(idImovel, ATRIBUTO_QUADRA_FACE, "quadraFace.endereco",
					"quadraFace.endereco.cep", "quadra");
			creditoDebitoVO.setEnderecoImovel(imovel.getEnderecoFormatado());
		}

		String tamanhoDescricao = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TAMANHO_DESCRICAO_COMPLEMENTAR_NOTA);
		model.addAttribute("tamanhoDescricao", tamanhoDescricao);

		Long idNotaDebito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		model.addAttribute("idNotaDebito", idNotaDebito);
	}

	private void popularForm(CreditoDebitoVO creditoDebitoVO, HttpServletRequest request, Fatura notaDebitoCredito)
			throws GGASException {

		if (notaDebitoCredito.getCliente() != null) {
			Cliente cliente = controladorCliente.obterCliente(notaDebitoCredito.getCliente().getChavePrimaria(),
					"enderecos");
			popularFormCliente(creditoDebitoVO, cliente);
		}

		if (notaDebitoCredito.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
					.obter(notaDebitoCredito.getPontoConsumo().getChavePrimaria(), "imovel", ATRIBUTO_QUADRA_FACE);
			popularFormPontoConsumo(creditoDebitoVO, pontoConsumo);
		}
		Long idImovel = creditoDebitoVO.getIdImovel();

		Imovel imovel = null;

		if ((idImovel != null) && (idImovel > 0)) {
			imovel = (Imovel) controladorImovel.obter(idImovel, ATRIBUTO_QUADRA_FACE, "quadraFace.endereco",
					"quadraFace.endereco.cep", "quadra");
			creditoDebitoVO.setEnderecoImovel(imovel.getEnderecoFormatado());
		}

		Collection<FaturaItem> listaFaturaItem = notaDebitoCredito.getListaFaturaItem();
		if ((listaFaturaItem != null) && (!listaFaturaItem.isEmpty())) {
			FaturaItem faturaItem = null;

			// A FaturaItem das notas de débito e de crédito serão sempre a primeira
			// da lista, pois somente a Fatura pode ter mais de um FaturaItem.
			for (FaturaItem faturaItemNota : listaFaturaItem) {
				faturaItem = faturaItemNota;
				break;
			}

			if (faturaItem != null) {
				Rubrica rubrica = controladorRubrica.obterRubricaPorFaturaItem(faturaItem.getChavePrimaria());
				if (rubrica != null) {
					creditoDebitoVO.setIdRubrica(rubrica.getChavePrimaria());
					creditoDebitoVO.setDescricaoRubrica(rubrica.getDescricao());
				}
				creditoDebitoVO.setQuantidade(String.valueOf(faturaItem.getQuantidade()));
				creditoDebitoVO.setValor(
						Util.converterCampoCurrencyParaString(faturaItem.getValorUnitario(), Constantes.LOCALE_PADRAO));
			}
		}

		if (!StringUtils.isEmpty(notaDebitoCredito.getObservacaoNota())) {
			creditoDebitoVO.setObservacaoNota(notaDebitoCredito.getObservacaoNota());
		}

		creditoDebitoVO.setValorTotal(
				Util.converterCampoCurrencyParaString(notaDebitoCredito.getValorTotal(), Constantes.LOCALE_PADRAO));

		BigDecimal saldo = notaDebitoCredito.getValorTotal().subtract(notaDebitoCredito.getValorConciliado());
		BigDecimal valorRecebimento = controladorCobranca
				.obterValorRecebimentoPelaFatura(notaDebitoCredito.getChavePrimaria());
		if (valorRecebimento != null) {
			saldo = saldo.subtract(valorRecebimento);
		}
		creditoDebitoVO.setSaldo(Util.converterCampoCurrencyParaString(saldo, Constantes.LOCALE_PADRAO));

		if (notaDebitoCredito.getDataVencimento() != null) {
			creditoDebitoVO.setDataVencimento(Util.converterDataParaStringSemHora(notaDebitoCredito.getDataVencimento(),
					Constantes.FORMATO_DATA_BR));
		}

		Long idNotaDebito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		Long idNotaCredito = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));

		Collection<FaturaConciliacao> listaFaturaConciliacao = controladorFatura
				.consultarFaturaConciliacao(notaDebitoCredito.getChavePrimaria());

		if (notaDebitoCredito.getTipoDocumento().getChavePrimaria() == idNotaDebito) {
			creditoDebitoVO.setIndicadorCreditoDebito(DEBITO);
			if ((listaFaturaConciliacao != null) && (!listaFaturaConciliacao.isEmpty())) {
				request.setAttribute(LISTA_NOTA_CREDITO, listaFaturaConciliacao);
			}
		} else if (notaDebitoCredito.getTipoDocumento().getChavePrimaria() == idNotaCredito) {
			creditoDebitoVO.setIndicadorCreditoDebito(CREDITO);
			if ((listaFaturaConciliacao != null) && (!listaFaturaConciliacao.isEmpty())) {
				request.setAttribute(LISTA_NOTA_DEBITO, listaFaturaConciliacao);
			}
		}

		if (notaDebitoCredito.getDataEmissao() != null) {
			creditoDebitoVO.setDataEmissao(Util.converterDataParaStringSemHora(notaDebitoCredito.getDataEmissao(),
					Constantes.FORMATO_DATA_BR));
		}

		if (notaDebitoCredito.getMotivoCancelamento() != null) {
			StringBuilder motivo = new StringBuilder();
			motivo.append(notaDebitoCredito.getMotivoCancelamento().getDescricao());
			if (!StringUtils.isEmpty(notaDebitoCredito.getDescricaoCancelamento())) {
				motivo.append(" - ");
				motivo.append(notaDebitoCredito.getDescricaoCancelamento());
			}
			creditoDebitoVO.setDescricaoCancelamento(motivo.toString());
		}
		// Caso seja uma nota de debito parcelada, busca as faturas geradas no
		// parcelamento.

		if ("Parcelada".equals(notaDebitoCredito.getCreditoDebitoSituacao().getDescricao())) {
			Parcelamento parcelamento = controladorParcelamento
					.buscarParcelamentoPorFaturaGeral(notaDebitoCredito.getFaturaGeral().getChavePrimaria());
			// Recupera as Parcelas do Parcelamento
			Collection<ParcelaVO> listaParcelas = controladorParcelamento
					.consultarParcelamentosNotaDebito(parcelamento.getChavePrimaria());
			if (!listaParcelas.isEmpty()) {
				request.setAttribute("mostrarColunaDataVencimento", true);
				request.setAttribute("exibirNumeroDocumento", true);
			} else {
				listaParcelas = controladorParcelamento
						.consultarParcelamentosCreditoARealizar(parcelamento.getChavePrimaria());
				request.setAttribute("mostrarColunaDataVencimento", false);
				request.setAttribute("exibirNumeroDocumento", false);
			}
			request.setAttribute(LISTA_PARCELAMENTOS, listaParcelas);
		}
		// Recupera as informações dos encargos
		request.setAttribute(LISTA_DETALHAMENTO_PARCELAMENTO,
				controladorFatura.carregarCreditoDebitoDetalhamentoPorFatura(notaDebitoCredito.getChavePrimaria()));

		creditoDebitoVO.setSituacao(notaDebitoCredito.getCreditoDebitoSituacao().getDescricao());
	}

	private void popularFormCliente(CreditoDebitoVO creditoDebitoVO, Cliente cliente) {

		creditoDebitoVO.setIdCliente(cliente.getChavePrimaria());
		creditoDebitoVO.setNomeCompletoCliente(cliente.getNome());
		if (cliente.getCpf() != null) {
			creditoDebitoVO.setDocumentoFormatado(cliente.getCpfFormatado());
		}
		if (cliente.getCnpj() != null) {
			creditoDebitoVO.setDocumentoFormatado(cliente.getCnpjFormatado());
		}
		if (cliente.getEnderecoPrincipal() != null) {
			creditoDebitoVO.setEnderecoFormatadoCliente(cliente.getEnderecoPrincipal().getEnderecoFormatado());
		}
		if (cliente.getEmailPrincipal() != null) {
			creditoDebitoVO.setEmailCliente(cliente.getEmailPrincipal());
		}
	}

	private void popularFormPontoConsumo(CreditoDebitoVO creditoDebitoVO, PontoConsumo pontoConsumo) {

		creditoDebitoVO.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
		creditoDebitoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		creditoDebitoVO.setEnderecoPontoConsumo(pontoConsumo.getEnderecoFormatado());
		creditoDebitoVO.setCepPontoConsumo(pontoConsumo.getQuadraFace().getEndereco().getCep().getCep());
		creditoDebitoVO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
		creditoDebitoVO.setPontoConsumoLegado(pontoConsumo.getCodigoLegado());
	}

	private void pesquisarPontosConsumo(HttpServletRequest request, CreditoDebitoVO creditoDebitoVO)
			throws GGASException {

		Long idImovel = creditoDebitoVO.getIdImovel();
		Long idCliente = creditoDebitoVO.getIdCliente();
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}
		request.setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}

	private void prepararFiltro(Map<String, Object> filtro, CreditoDebitoVO creditoDebitoVO) throws GGASException {

		Collection<PontoConsumo> listaPontoConsumo = null;
		boolean indicadorImovelSemPtoConsumo = false;

		Long chavePrimaria = creditoDebitoVO.getChavePrimaria();
		if ((chavePrimaria != null) && (chavePrimaria > 0)) {
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		}

		Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		}

		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();
		Long idImovel = creditoDebitoVO.getIdImovel();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			filtro.put(ID_PONTO_CONSUMO, idPontoConsumo);

		} else if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
			if (listaPontoConsumo != null) {
				filtro.put(CHAVES_PONTO_CONSUMO, Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
			}
			if (listaPontoConsumo == null || listaPontoConsumo.isEmpty()) {
				indicadorImovelSemPtoConsumo = true;
				filtro.put(INDICADOR_IMOVEL_SEM_PONTO_CONSUMO, indicadorImovelSemPtoConsumo);
			}
		}

		Long idCliente = creditoDebitoVO.getIdCliente();
		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			filtro.put(INDICADOR_CREDITO_DEBITO, indicadorCreditoDebito);
		}

		String dataVencimentoInicial = creditoDebitoVO.getDataVencimentoInicial();
		if (!StringUtils.isEmpty(dataVencimentoInicial)) {
			Date dataInicial = Util.converterCampoStringParaData("Intervalo de Vencimento Inicial",
					dataVencimentoInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_VENCIMENTO_INICIAL, dataInicial);
		}

		String dataVencimentoFinal = creditoDebitoVO.getDataVencimentoFinal();
		if (!StringUtils.isEmpty(dataVencimentoFinal)) {
			Date dataFinal = Util.converterCampoStringParaData("Intervalo de Vencimento Final", dataVencimentoFinal,
					Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_VENCIMENTO_FINAL, dataFinal);
		}

		Long idSituacao = creditoDebitoVO.getIdSituacao();
		if ((idSituacao != null) && (idSituacao > 0)) {
			filtro.put("idCreditoDebitoSituacao", idSituacao);
		}

		String numeroDocumento = creditoDebitoVO.getNumeroDocumento();
		if ((numeroDocumento != null) && (!"".equals(numeroDocumento))) {
			filtro.put(CHAVE_PRIMARIA, Long.valueOf(numeroDocumento));
		}

		String idPontoConsumoLegado = creditoDebitoVO.getPontoConsumoLegado();
		if (!StringUtils.isEmpty(idPontoConsumoLegado) && idPontoConsumoLegado != null) {
			filtro.put(PONTO_CONSUMO_LEGADO, idPontoConsumoLegado);
		}

	}

	private Collection<NotaDebitoCreditoVO> popularNotaDebitoCreditoVO(Collection<Fatura> listaFatura)
			throws GGASException {

		List<NotaDebitoCreditoVO> listaFaturaVO = new LinkedList<NotaDebitoCreditoVO>();

		for (Fatura fatura : listaFatura) {

			NotaDebitoCreditoVO notaDebitoCredito = new NotaDebitoCreditoVO();

			notaDebitoCredito.setChavePrimaria(fatura.getChavePrimaria());
			if (fatura.getCicloReferenciaFormatado() != null) {
				notaDebitoCredito.setCicloReferencia(fatura.getCicloReferenciaFormatado());
			}

			BigDecimal valorTotal = fatura.getValorTotal();
			notaDebitoCredito.setValor(Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO));
			notaDebitoCredito.setValorTotal(valorTotal);

			if (fatura.getCreditoDebitoSituacao() != null) {
				notaDebitoCredito.setCreditoDebitoSituacao(fatura.getCreditoDebitoSituacao());
			}

			if (fatura.getDataEmissao() != null) {
				notaDebitoCredito.setDataEmissao(fatura.getDataEmissao());
			}
			if (fatura.getDataVencimento() != null) {
				notaDebitoCredito.setDataVencimento(fatura.getDataVencimento());
			}
			if (fatura.getTipoDocumento() != null) {
				notaDebitoCredito.setTipoDocumento(fatura.getTipoDocumento());
			}
			if (fatura.getSituacaoPagamento() != null) {
				notaDebitoCredito.setSituacaoPagamento(fatura.getSituacaoPagamento());
			}
			if (fatura.getPontoConsumo() != null) {
				notaDebitoCredito.setPontoConsumo(fatura.getPontoConsumo());
			}

			Long[] chavesPontoConsumo = null;
			chavesPontoConsumo = controladorFatura.obterChavesPontosConsumoFatura(fatura.getChavePrimaria());
			if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
				notaDebitoCredito.setIndicadorPontoConsumo(Boolean.TRUE);
			} else {
				notaDebitoCredito.setIndicadorPontoConsumo(Boolean.FALSE);
			}

			if (fatura.getCliente() != null) {
				notaDebitoCredito.setNomeCliente(
						((Cliente) controladorCliente.obter(fatura.getCliente().getChavePrimaria())).getNome());
			}

			notaDebitoCredito.setHabilitado(fatura.isHabilitado());
			notaDebitoCredito.setUltimaAlteracao(fatura.getUltimaAlteracao());

			listaFaturaVO.add(notaDebitoCredito);
		}

		return listaFaturaVO;
	}

	private void pesquisarPontosConsumo(CreditoDebitoVO creditoDebitoVO, HttpServletRequest request)
			throws GGASException {

		Long idImovel = creditoDebitoVO.getIdImovel();
		Long idCliente = creditoDebitoVO.getIdCliente();
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		if ((idImovel != null) && (idImovel > 0)) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(idImovel, Boolean.TRUE);
		} else if ((idCliente != null) && (idCliente > 0)) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}
		request.setAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
	}
}
