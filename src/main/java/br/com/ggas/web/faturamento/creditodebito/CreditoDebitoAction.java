package br.com.ggas.web.faturamento.creditodebito;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ggas.geral.exception.NegocioException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas operações referentes a Crédito / Débito a Realizar.
 * 
 * @author esantana
 *
 */
@Controller
public class CreditoDebitoAction extends GenericAction {

	private static final String FILTRO_CREDITO_DEBITO_VO = "filtroCreditoDebitoVO";

	private static final String FORWARD_PESQUISAR_CREDITO_DEBITO = "forward:/pesquisarCreditoDebito";

	private static final String CREDITO_DEBITO_VO = "creditoDebitoVO";

	private static final String ERRO_NEGOCIO_IMOVEL_SEM_CONTRATO_ATIVO = "ERRO_NEGOCIO_IMOVEL_SEM_CONTRATO_ATIVO";

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorAlcada controladorAlcada;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	private ControladorFatura controladorFatura;

	private static final String STATUS_AUTORIZADO = "statusAutorizado";

	private static final String STATUS_NAO_AUTORIZADO = "statusNaoAutorizado";

	private static final String STATUS_PENDENTE = "statusPendente";

	private static final String ID_CLIENTE = "idCliente";

	private static final String POSSUI_PERMISSAO_AUTORIZAR = "hasPermissaoAutorizar";

	private static final String STATUS = "status";

	private static final String CODIGO_PONTO_CONSUMO = "codigoPontoConsumo";

	private static final String NUMERO_DOCUMENTO = "numeroDocumento";

	private static final String PERIODICIDADE_JUROS = "periodicidadeJuros";

	private static final String TIPO_DOCUMENTO = "tipoDocumento";
	
	private static final String DADOS_GERAIS = "dadosGerais";
	
	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ID_CREDITO_DEBITO_NEGOCIADO = "idCreditoDebitoNegociado";

	private static final String VALOR = "valorHidden";
	
	private static final String VALOR_UNITARIO = "valorUnitarioHidden";

	private static final String TIPO_AMORTIZACAO = "tipoAmortizacao";
	
	private static final String VALOR_ENTRADA = "valorEntrada";

	private static final String PARCELAS = "parcelas";
	
	private static final String TAXA_JUROS = "taxaJuros";

	private static final String ID_PERIODICIDADE_JUROS = "idPeriodicidadeJuros";
	
	private static final String ID_PERIODICIDADE = "idPeriodicidade";
	
	private static final String DATA_INICIO_CREDITO = "dataInicioCredito";

	private static final String DATA_INICIO_COBRANCA = "dataInicioCobranca";

	private static final String ID_CREDITO_ORIGEM = "idCreditoOrigem";

	private static final String ID_RUBRICA = "idRubrica";

	private static final String QUANTIDADE = "quantidade";
	
	private static final String QUANTIDADE_DIAS_COBRANCA = "qtdDiasCobranca";
	
	private static final String ID_INICIO_COBRANCA = "idInicioCobranca";

	private static final String DESCRICAO = "complementoDescricao";

	private static final String MELHOR_DIA_VENCIMENTO = "melhorDiaVencimento";
	
	private static final String RELATORIO_EXIBICAO = "relatorioExibicao";
	
	private static final String HEADER_PARAM = "attachment";

	private static final String MEDIA_TYPE = "application/pdf";


	/**
	 * Método responsável por exibir a tela pesquisa de cliente.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("exibirPesquisaCreditoDebito")
	public String exibirPesquisaCreditoDebito(Model model, HttpServletRequest request) throws GGASException {

		Collection<EntidadeConteudo> listaMotivoCancelamento = controladorEntidadeConteudo.obterListaMotivoCancelamento();
		model.addAttribute("listaMotivoCancelamento", listaMotivoCancelamento);
		
		request.getSession().removeAttribute(FILTRO_CREDITO_DEBITO_VO);
		
		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);
		String statusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
		model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));
		model.addAttribute(STATUS_PENDENTE, Long.valueOf(statusPendente));

		return "exibirPesquisaCreditoDebito";
	}

	/**
	 * Método responsável por exibir uma tela de inclusão de Crédito/Débito a Realizar.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}'
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirInclusaoCreditoDebito")
	public String exibirInclusaoCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) throws GGASException {

		String view = "exibirInclusaoCreditoDebito";
		try {
			carregarCampos(model, creditoDebitoVO);
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
			
			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirPesquisaCreditoDebito";
		}

		return view;
	}

	/**
	 * Método responsável por pesquisar um Credito a Realizar.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param chavesPrimarias - {@link String}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException
	 * 
	 */
	@RequestMapping("pesquisarCreditoDebito")
	public String pesquisarCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			@RequestParam(value = "chavesPrimarias", required = false) String chavesPrimarias, BindingResult bindingResult)
			throws GGASException {

		creditoDebitoVO.setChavesPrimarias(null);

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(creditoDebitoVO.getIsAutorizacao()) && Boolean.valueOf(creditoDebitoVO.getIsAutorizacao())) {

			if (creditoDebitoVO.getStatus() != null && creditoDebitoVO.getStatus() > 0) {
				filtro.put(STATUS, creditoDebitoVO.getStatus());
			}

			if (StringUtils.isNotBlank(chavesPrimarias)) {
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, Util.arrayStringParaArrayLong(chavesPrimarias.split(",")));
			}
		} else {
			this.prepararFiltro(filtro, creditoDebitoVO);
		}

		Collection<Map<String, Object>> listaExibicaoCreditoDebito = controladorCreditoDebito.consultarPesquisaCreditoDebito(filtro);

		model.addAttribute("listaCreditoDebito", listaExibicaoCreditoDebito);
		model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		
		return this.exibirPesquisaCreditoDebito(model, request);
	}

	/**
	 * Método responsável cancelar um lançamento de crédito/débito.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("cancelarLancamentoCreditoDebito")
	public String cancelarLancamentoCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		
		try {
			Long[] chavesPrimarias = creditoDebitoVO.getChavesPrimarias();
			Long idMotivoCancelamento = creditoDebitoVO.getIdMotivoCancelamento();
			controladorCreditoDebito.cancelarLancamentoCreditoDebito(chavesPrimarias, idMotivoCancelamento, getDadosAuditoria(request));

			super.mensagemSucesso(model, Constantes.SUCESSO_CREDITO_DEBITO_CANCELADO, request);
			
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return FORWARD_PESQUISAR_CREDITO_DEBITO;
	}

	/**
	 * Método responsável por exibir a tela de detalhar CreditoDebitoARealizar.
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirDetalhamentoCreditoDebito")
	public String exibirDetalhamentoCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult)  {

		String view = "exibirDetalhamentoCreditoDebito";
		
		request.getSession().setAttribute(FILTRO_CREDITO_DEBITO_VO, creditoDebitoVO);
		
		try {
			Long chavePrimaria = creditoDebitoVO.getChavePrimaria();

			CreditoDebitoNegociado creditoDebitoNegociado = controladorCreditoDebito.obterCreditoDebitoNegociado(chavePrimaria, "rubrica",
					PERIODICIDADE_JUROS, "periodicidadeCobranca", "creditoOrigem", "eventoInicioCobranca", "ultimoUsuarioAlteracao", STATUS);
			creditoDebitoNegociado.setValor(creditoDebitoNegociado.getValor().add(creditoDebitoNegociado.getValorEntrada()));

			Usuario usuarioLogado = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			model.addAttribute(POSSUI_PERMISSAO_AUTORIZAR,
					controladorAlcada.possuiAlcadaAutorizacaoCreditosDebitos(usuarioLogado, creditoDebitoNegociado));

			String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
			String statusNaoAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_NAO_AUTORIZADO);

			model.addAttribute(STATUS_AUTORIZADO, Long.valueOf(statusAutorizado));
			model.addAttribute(STATUS_NAO_AUTORIZADO, Long.valueOf(statusNaoAutorizado));

			creditoDebitoVO = new CreditoDebitoVO();
			this.popularForm(model, creditoDebitoVO, creditoDebitoNegociado);
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
			
		} catch(GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_CREDITO_DEBITO;
		}

		return view;
	}
	
	/**
	 * Método utilizado para recuperar o filtro de pesquisa utilizado para realizar o detalhamento.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("voltarParaPesquisarCreditoDebito")
	public String voltarParaPesquisarCreditoDebito(Model model, HttpServletRequest request) throws GGASException {
		
		CreditoDebitoVO creditoDebitoVO = (CreditoDebitoVO) request.getSession().getAttribute(FILTRO_CREDITO_DEBITO_VO);
		
		request.getSession().removeAttribute(FILTRO_CREDITO_DEBITO_VO);
		
		return this.pesquisarCreditoDebito(model, request, creditoDebitoVO, null, null);
	}
	
	/**
	 * Método responsável por autorizar um crédito/débito.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("autorizarCreditoDebito")
	public String autorizarCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {

		try {
			Long chavePrimaria = creditoDebitoVO.getChavePrimaria();
			Long autorizado = creditoDebitoVO.getStatus();
			if (autorizado != null) {

				String paramStatusAutorizada =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
				EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(autorizado);

				if (status != null) {
					CreditoDebitoNegociado creditoDebitoNegociado = controladorCreditoDebito.obterCreditoDebitoNegociado(chavePrimaria,
							"rubrica", PERIODICIDADE_JUROS, "periodicidadeCobranca", "creditoOrigem", "eventoInicioCobranca", STATUS);

					this.atualizarCreditoDebitoNegociado(status, paramStatusAutorizada, request, creditoDebitoNegociado);
					super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
							super.obterMensagem(CreditoDebitoARealizar.CREDITO_DEBITO_A_REALIZAR));
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirDetalhamentoCreditoDebito(model, request, creditoDebitoVO, bindingResult);
	}
	
	private void atualizarCreditoDebitoNegociado(EntidadeConteudo status, String paramStatusAutorizada,
			HttpServletRequest request, CreditoDebitoNegociado creditoDebitoNegociado) throws GGASException {

		if (creditoDebitoNegociado != null) {
			creditoDebitoNegociado.setStatus(status);
			creditoDebitoNegociado.setUltimoUsuarioAlteracao(getDadosAuditoria(request).getUsuario());
			creditoDebitoNegociado.setDadosAuditoria(getDadosAuditoria(request));

			if (Long.parseLong(paramStatusAutorizada) == status.getChavePrimaria()) {
				Date dataAutorizacao = new Date(System.currentTimeMillis());

				if (creditoDebitoNegociado.getDataInicioCobranca() != null
						&& dataAutorizacao.compareTo(creditoDebitoNegociado.getDataInicioCobranca()) > 0) {
					creditoDebitoNegociado.setDataInicioCobranca(dataAutorizacao);

					Long chavePontoConsumo = null;

					if (creditoDebitoNegociado.getPontoConsumo() != null) {
						chavePontoConsumo = creditoDebitoNegociado.getPontoConsumo().getChavePrimaria();
					}

					controladorCreditoDebito.atualizarDataAnoMesFaturamento(chavePontoConsumo, dataAutorizacao, creditoDebitoNegociado,
							getDadosAuditoria(request));

				}
			}

			controladorCreditoDebito.atualizar(creditoDebitoNegociado, controladorCreditoDebito.getClasseEntidadeCreditoDebitoNegociado());
		}
	}
	
	/**
	 * Método responsável por gerar as parcelas de crédito e débito.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("gerarParcelasCreditoDebito")
	public String gerarParcelasCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult) {
		try {
			Map<String, Object> dados = this.popularMapa(creditoDebitoVO);

			DadosGeraisParcelas dadosGerais = controladorCreditoDebito.gerarParcelasCreditoDebito(dados, false);
			request.getSession().setAttribute(DADOS_GERAIS, dadosGerais);
			request.setAttribute("listaDadosParcelas", dadosGerais.getListaDadosParcelas());
			model.addAttribute(CREDITO_DEBITO_VO, creditoDebitoVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "forward:/exibirInclusaoCreditoDebito";
	}
	
	/**
	 * Método responsável por salvar os Créditos/Débitos a Realizar.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param creditoDebitoVO - {@link CreditoDebitoVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws IOException 
	 * @throws GGASException 
	 */
	@RequestMapping("salvarParcelasCreditoDebito")
	public String salvarParcelasCreditoDebito(Model model, HttpServletRequest request, CreditoDebitoVO creditoDebitoVO,
			BindingResult bindingResult, HttpServletResponse response) throws IOException, GGASException {

		String view = FORWARD_PESQUISAR_CREDITO_DEBITO;

		try {
			validarToken(request);

			DadosGeraisParcelas dadosGerais = (DadosGeraisParcelas) request.getSession().getAttribute(DADOS_GERAIS);
			if (dadosGerais != null) {
				dadosGerais.setDadosAuditoria(getDadosAuditoria(request));
			}

			Map<String, Object> dados = this.popularMapa(creditoDebitoVO);

			byte[] retornoRelatorio = null;
			if (dadosGerais != null) {
				retornoRelatorio = controladorCreditoDebito.salvarCreditoDebitoARealizar(dados, dadosGerais);
			}

			String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
			boolean tipoDebito = false;
			if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
				tipoDebito = Boolean.valueOf(indicadorCreditoDebito);
			}
			String valorEntrada = creditoDebitoVO.getValorEntrada();

			if (retornoRelatorio != null) {
				request.setAttribute("gerarRelatorioExtratoDebito", true);
				request.getSession().setAttribute(RELATORIO_EXIBICAO, retornoRelatorio);
			}


			request.getSession().removeAttribute(DADOS_GERAIS);
			super.mensagemSucesso(model, Constantes.SUCESSO_EFETUAR_PARCELAMENTO, request,
					super.obterMensagem(CreditoDebitoARealizar.CREDITO_DEBITO_A_REALIZAR));

		} catch (GGASException e) {
			view = "forward:/exibirInclusaoCreditoDebito";
			super.mensagemErroParametrizado(model, request, e);
			return view;
		}

		return this.pesquisarCreditoDebito(model, request, creditoDebitoVO, "", bindingResult);
	}
	
	/**
	 * Responsável em gerar o relatório de Extrato
	 * de débito.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @return relatório - {@link ResponseEntity}
	 */
	@RequestMapping("gerarRelatorioExtratoDebito")
	public ResponseEntity<byte[]> gerarRelatorioExtratoDebito(HttpServletRequest request) throws GGASException {

		byte[] relatorioExibicao = (byte[]) request.getSession().getAttribute(RELATORIO_EXIBICAO);

		ResponseEntity<byte[]> responseEntity = null;
		
		if (relatorioExibicao != null) {
			MediaType contentType = MediaType.parseMediaType(MEDIA_TYPE);
			String filename = "relatorio.pdf";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(contentType);
			headers.setContentDispositionFormData(HEADER_PARAM, filename);

			responseEntity = new ResponseEntity<byte[]>(relatorioExibicao, headers, HttpStatus.OK);
		}

		request.removeAttribute("gerarRelatorioConfissaoDividaNotaPromissoria");
		request.getSession().removeAttribute(RELATORIO_EXIBICAO);

		return responseEntity;
	}
	
	private Map<String, Object> popularMapa(CreditoDebitoVO creditoDebitoVO) throws GGASException {

		Map<String, Object> dados = new HashMap<String, Object>();

		Long idCliente = creditoDebitoVO.getIdCliente();
		if ((idCliente != null) && (idCliente > 0)) {
			dados.put(ID_CLIENTE, idCliente);
		}

		Long idPontoConsumo = creditoDebitoVO.getIdPontoConsumo();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			dados.put(ID_PONTO_CONSUMO, idPontoConsumo);
		}

		String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			dados.put(TIPO_DOCUMENTO, indicadorCreditoDebito);
		}

		Long idCreditoDebitoNegociado = creditoDebitoVO.getIdCreditoDebitoNegociado();
		if ((idCreditoDebitoNegociado != null) && (idCreditoDebitoNegociado > 0)) {
			dados.put(ID_CREDITO_DEBITO_NEGOCIADO, idCreditoDebitoNegociado);
		}

		String valor = creditoDebitoVO.getValorHidden();
		if (!StringUtils.isEmpty(valor)) {
			valor = creditoDebitoVO.getValorHidden();
			dados.put(VALOR, Util.converterCampoStringParaValorBigDecimal("Valor", valor, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO));
		}

		String valorUnitario = creditoDebitoVO.getValorUnitarioHidden();
		if (!StringUtils.isEmpty(valorUnitario)) {
			valorUnitario = creditoDebitoVO.getValorUnitarioHidden();
			dados.put(VALOR_UNITARIO, Util.converterCampoStringParaValorBigDecimal("Valor Unitário", valorUnitario,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		Long tipoAmortizacao = creditoDebitoVO.getTipoAmortizacao();
		if ((tipoAmortizacao != null) && (tipoAmortizacao > 0)) {
			dados.put(TIPO_AMORTIZACAO, tipoAmortizacao);
		}

		String valorEntrada = creditoDebitoVO.getValorEntrada();
		if (!StringUtils.isEmpty(valorEntrada)) {
			dados.put(VALOR_ENTRADA, Util.converterCampoStringParaValorBigDecimal("Valor Entrada", valorEntrada,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		String parcelas = creditoDebitoVO.getParcelas();
		if (!StringUtils.isEmpty(parcelas)) {
			dados.put(PARCELAS, Util.converterCampoStringParaValorInteger(CreditoDebitoARealizar.PARCELAS, parcelas));
		}

		String taxaJuros = creditoDebitoVO.getTaxaJuros();
		if (!StringUtils.isEmpty(taxaJuros)) {
			BigDecimal percentualJuros = Util.converterCampoStringParaValorBigDecimal("Taxa Juros", taxaJuros,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			dados.put(TAXA_JUROS, percentualJuros);
		}

		Long idPeriodicidadeJuros = creditoDebitoVO.getIdPeriodicidadeJuros();
		if ((idPeriodicidadeJuros != null) && (idPeriodicidadeJuros > 0)) {
			dados.put(ID_PERIODICIDADE_JUROS, idPeriodicidadeJuros);
		}

		Long idPeriodicidadeCobranca = creditoDebitoVO.getIdPeriodicidade();
		if ((idPeriodicidadeCobranca != null) && (idPeriodicidadeCobranca > 0)) {
			dados.put(ID_PERIODICIDADE, idPeriodicidadeCobranca);
		}

		String dataInicioCredito = creditoDebitoVO.getDataInicioCredito();
		if (!StringUtils.isEmpty(dataInicioCredito)) {
			dados.put(DATA_INICIO_CREDITO,
							Util.converterCampoStringParaData("Iniciar crédito após", dataInicioCredito, Constantes.FORMATO_DATA_BR));
		}

		String dataInicioCobranca = creditoDebitoVO.getDataInicioCobranca();
		if (!StringUtils.isEmpty(dataInicioCobranca)) {
			dados.put(DATA_INICIO_COBRANCA,
							Util.converterCampoStringParaData("Iniciar cobrança após", dataInicioCobranca, Constantes.FORMATO_DATA_BR));
		}

		Long idRubrica = creditoDebitoVO.getIdRubrica();
		if ((idRubrica != null) && (idRubrica > 0)) {
			dados.put(ID_RUBRICA, idRubrica);
		}

		Long idCreditoOrigem = creditoDebitoVO.getIdCreditoOrigem();
		if ((idCreditoOrigem != null) && (idCreditoOrigem > 0)) {
			dados.put(ID_CREDITO_ORIGEM, idCreditoOrigem);
		}

		String quantidade = creditoDebitoVO.getQuantidade();
		if (!StringUtils.isEmpty(quantidade)) {

			dados.put(QUANTIDADE, Util.converterCampoStringParaValorBigDecimal("Quantidade", quantidade, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO));
		}

		String qtdDiasCobranca =  creditoDebitoVO.getQtdDiasCobranca();
		if (!StringUtils.isEmpty(qtdDiasCobranca)) {
			dados.put(QUANTIDADE_DIAS_COBRANCA, qtdDiasCobranca);
		}

		Long idInicioCobranca = creditoDebitoVO.getIdInicioCobranca();
		if (idInicioCobranca != null) {
			dados.put(ID_INICIO_COBRANCA, idInicioCobranca);
		}

		String descricao = creditoDebitoVO.getComplementoDescricao();
		if (!StringUtils.isEmpty(descricao)) {
			dados.put(DESCRICAO, descricao);
		}

		String melhorDiaVencimento = creditoDebitoVO.getMelhorDiaVencimento();
		if (!StringUtils.isEmpty(melhorDiaVencimento) && !"-1".equals(melhorDiaVencimento)) {
			dados.put(MELHOR_DIA_VENCIMENTO, melhorDiaVencimento);
		}

		return dados;
	}


	private void popularForm(Model model, CreditoDebitoVO creditoDebitoVO,
			CreditoDebitoNegociado creditoDebitoNegociado) throws GGASException {

		if (creditoDebitoNegociado != null) {
			creditoDebitoVO.setChavePrimaria(creditoDebitoNegociado.getChavePrimaria());

			verificaCliente(creditoDebitoVO, creditoDebitoNegociado);

			verificaPontoConsumo(creditoDebitoVO, creditoDebitoNegociado);

			if (creditoDebitoNegociado.getRubrica() != null) {
				creditoDebitoVO.setIdRubrica(creditoDebitoNegociado.getRubrica().getChavePrimaria());
				creditoDebitoVO.setDescricaoRubrica(creditoDebitoNegociado.getRubrica().getDescricao());

				if (creditoDebitoNegociado.getRubrica().getAmortizacao() != null) {
					creditoDebitoVO.setTipoAmortizacao(creditoDebitoNegociado.getRubrica().getAmortizacao().getChavePrimaria());
				}

			}

			creditoDebitoVO.setIdCreditoDebitoNegociado(creditoDebitoNegociado.getChavePrimaria());

			if (creditoDebitoNegociado.getCreditoOrigem() != null) {
				creditoDebitoVO.setDescricaoOrigemCredito(creditoDebitoNegociado.getCreditoOrigem().getDescricao());
			} else {
				if (creditoDebitoNegociado.getValorEntrada() != null) {
					creditoDebitoVO.setValorEntrada(creditoDebitoNegociado.getValorEntrada().toString());
				}
				if (creditoDebitoNegociado.getPercentualJuros() != null) {
					creditoDebitoVO.setJuros(creditoDebitoNegociado.getPercentualJuros().toString());
				}
				if (creditoDebitoNegociado.getPeriodicidadeJuros() != null) {
					creditoDebitoVO.setPeriodicidadeJuros(creditoDebitoNegociado.getPeriodicidadeJuros().getDescricao());
				}
			}
		}

		if ((creditoDebitoNegociado != null) && (creditoDebitoNegociado.getChavePrimaria() > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			creditoDebitoVO.setIdCreditoDebitoNegociado(creditoDebitoNegociado.getChavePrimaria());

			Collection<CreditoDebitoARealizar> listaParcelasCreditoDebito =
					controladorCreditoDebito.consultarCreditoDebitoARealizar(filtro);

			for (CreditoDebitoARealizar creditoDebitoARealizar : listaParcelasCreditoDebito) {
				if (creditoDebitoARealizar.getOrigem() != null) {
					FaturaGeral faturaGeral =
							controladorArrecadacao.obterFaturaGeral(creditoDebitoARealizar.getOrigem().getChavePrimaria());
					if (faturaGeral.getFaturaAtual() != null) {
						Long idDocumentoFatura = Long.valueOf(
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
						Long idDocumentoNotaDebito = Long.valueOf(
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

						Fatura fatura =
								(Fatura) controladorCobranca.obterFatura(faturaGeral.getFaturaAtual().getChavePrimaria(), TIPO_DOCUMENTO);

						creditoDebitoVO.setTipoDocumento(fatura.getTipoDocumento().getDescricao());
						if (fatura.getTipoDocumento().getChavePrimaria() == idDocumentoNotaDebito) {
							creditoDebitoVO.setNumeroDocumento(String.valueOf(fatura.getChavePrimaria()));
						} else if (fatura.getTipoDocumento().getChavePrimaria() == idDocumentoFatura) {
							String numeroDocumento = "";
							numeroDocumento = numeroDocumento.concat(String.valueOf(fatura.getChavePrimaria()));

							Collection<DocumentoFiscal> listaDocumentoFiscal =
									controladorFatura.obterDocumentosFiscaisPorFatura(fatura.getChavePrimaria());
							for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {

								creditoDebitoVO.setNumeroNotaFiscal(String.valueOf(documentoFiscal.getNumero()).trim().concat("-")
										.concat(documentoFiscal.getDescricaoSerie().trim()));
								break;
							}

							creditoDebitoVO.setNumeroDocumento(numeroDocumento);
						}

						break;
					}
				}
			}

		}

		if (creditoDebitoNegociado != null) {

			creditoDebitoVO.setValor(Util.converterCampoCurrencyParaString(creditoDebitoNegociado.getValor(), Constantes.LOCALE_PADRAO));
			creditoDebitoVO.setValorEntrada(
					Util.converterCampoCurrencyParaString(creditoDebitoNegociado.getValorEntrada(), Constantes.LOCALE_PADRAO));
			creditoDebitoVO.setParcelas(String.valueOf(creditoDebitoNegociado.getQuantidadeTotalPrestacoes()));
			if (creditoDebitoNegociado.getPeriodicidadeCobranca() != null) {
				creditoDebitoVO.setIdPeriodicidade(creditoDebitoNegociado.getPeriodicidadeCobranca().getChavePrimaria());
				creditoDebitoVO.setDescricaoPeriodicidade(creditoDebitoNegociado.getPeriodicidadeCobranca().getDescricao());
			}
			if (creditoDebitoNegociado.getPercentualJuros() != null) {
				creditoDebitoVO.setTaxaJuros(
						Util.converterCampoCurrencyParaString(creditoDebitoNegociado.getPercentualJuros(), Constantes.LOCALE_PADRAO));
			}
			if (creditoDebitoNegociado.getPeriodicidadeJuros() != null) {
				creditoDebitoVO.setIdPeriodicidadeJuros(creditoDebitoNegociado.getPeriodicidadeJuros().getChavePrimaria());
				creditoDebitoVO.setDescricaoPeriodicidadeJuros(creditoDebitoNegociado.getPeriodicidadeJuros().getDescricao());
			}
			if (creditoDebitoNegociado.getDescricao() != null) {
				creditoDebitoVO.setComplementoDescricao(creditoDebitoNegociado.getDescricao());
			}

			if (creditoDebitoNegociado.getDataInicioCobranca() != null) {
				creditoDebitoVO.setDataInicioCobranca(
						Util.converterDataParaStringSemHora(creditoDebitoNegociado.getDataInicioCobranca(), Constantes.FORMATO_DATA_BR));
			}
		}

		if (creditoDebitoNegociado != null && creditoDebitoNegociado.getCreditoOrigem() != null) {
			creditoDebitoVO.setIndicadorCreditoDebito("credito");
			if (creditoDebitoNegociado.getDataInicioCobranca() != null) {
				creditoDebitoVO.setDataInicioCredito(
						Util.converterDataParaStringSemHora(creditoDebitoNegociado.getDataInicioCobranca(), Constantes.FORMATO_DATA_BR));
			}
		} else {
			creditoDebitoVO.setIndicadorCreditoDebito("debito");
			if (creditoDebitoNegociado != null && creditoDebitoNegociado.getNumeroDiasCarencia() != null) {
				creditoDebitoVO.setQtdDiasCobranca(String.valueOf(creditoDebitoNegociado.getNumeroDiasCarencia()));
				if (creditoDebitoNegociado.getEventoInicioCobranca() != null) {
					creditoDebitoVO.setIdInicioCobranca(creditoDebitoNegociado.getEventoInicioCobranca().getChavePrimaria());
					creditoDebitoVO.setDescricaoEventoInicioCobranca(creditoDebitoNegociado.getEventoInicioCobranca().getDescricao());
				}
			}
			if (creditoDebitoNegociado != null && creditoDebitoNegociado.getDataInicioCobranca() != null) {
				creditoDebitoVO.setDataInicioCobranca(
						Util.converterDataParaStringSemHora(creditoDebitoNegociado.getDataInicioCobranca(), Constantes.FORMATO_DATA_BR));
			}
		}

		Collection<DadosParcelas> listaDadosParcelas = null;

		DadosGeraisParcelas dadosGeraisParcelas =
				controladorCreditoDebito.montarParcelasDetalhamento(creditoDebitoVO.getIdCreditoDebitoNegociado());
		listaDadosParcelas = dadosGeraisParcelas.getListaDadosParcelas();

		if (!StringUtils.isEmpty(dadosGeraisParcelas.getMotivoCancelamento())) {
			model.addAttribute("motivoCancelamento", dadosGeraisParcelas.getMotivoCancelamento());
		}

		if (dadosGeraisParcelas.getQuantidade() != null) {
			model.addAttribute("quantidade", dadosGeraisParcelas.getQuantidade());
			BigDecimal valorUnitarioExibicao = creditoDebitoNegociado.getValor();
			valorUnitarioExibicao = valorUnitarioExibicao.divide(dadosGeraisParcelas.getQuantidade(), 6, BigDecimal.ROUND_HALF_UP);
			model.addAttribute("valorUnitario",
					Util.converterCampoValorDecimalParaString("valorUnitario", valorUnitarioExibicao, Constantes.LOCALE_PADRAO, 2));

		}

		model.addAttribute("listaDadosParcelas", listaDadosParcelas);
	}

	private void verificaCliente(CreditoDebitoVO creditoDebitoVO, CreditoDebitoNegociado creditoDebitoNegociado) throws GGASException {
		if (creditoDebitoNegociado.getCliente() != null) {

			Cliente cliente = (Cliente) controladorCliente.obter(creditoDebitoNegociado.getCliente().getChavePrimaria(), "enderecos");
			creditoDebitoVO.setIdCliente(cliente.getChavePrimaria());
			creditoDebitoVO.setNomeCompletoCliente(cliente.getNome());
			if (cliente.getCpf() != null) {
				creditoDebitoVO.setDocumentoFormatado(cliente.getNumeroDocumentoFormatado());
			}
			if (cliente.getEnderecoPrincipal() != null) {
				creditoDebitoVO.setEnderecoFormatadoCliente(cliente.getEnderecoPrincipal().getEnderecoFormatado());
			}
			if (cliente.getEmailPrincipal() != null) {
				creditoDebitoVO.setEmailCliente(cliente.getEmailPrincipal());
			}
		}
	}

	private void verificaPontoConsumo(CreditoDebitoVO creditoDebitoVO, CreditoDebitoNegociado creditoDebitoNegociado) throws GGASException {
		if (creditoDebitoNegociado.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo
					.obter(creditoDebitoNegociado.getPontoConsumo().getChavePrimaria(), "imovel", "quadraFace");
			creditoDebitoVO.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
			creditoDebitoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			creditoDebitoVO.setEnderecoPontoConsumo(pontoConsumo.getEnderecoFormatado());
			creditoDebitoVO.setCepPontoConsumo(pontoConsumo.getQuadraFace().getEndereco().getCep().getCep());
			creditoDebitoVO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
			creditoDebitoVO.setCodigoPontoConsumo(pontoConsumo.getCodigoPontoConsumo());
		}
	}

	private void prepararFiltro(Map<String, Object> filtro, CreditoDebitoVO creditoDebitoVO) throws GGASException {

		Long idImovel = creditoDebitoVO.getIdImovel();
		if ((idImovel != null) && (idImovel > 0)) {
			Collection<PontoConsumo> listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(idImovel);
			Long[] chavesPontoConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumo);
			filtro.put("chavesPontoConsumo", chavesPontoConsumo);
		}

		Long idCliente = creditoDebitoVO.getIdCliente();
		if ((idCliente != null) && (idCliente > 0)) {
			filtro.put(ID_CLIENTE, idCliente);
		}

		Long status = creditoDebitoVO.getStatus();
		if (status != null && status > 0) {
			filtro.put(STATUS, status);
		}
		String idCodigoPontoConsumo = creditoDebitoVO.getCodigoPontoConsumo();
		if (!StringUtils.isEmpty(idCodigoPontoConsumo) && idCodigoPontoConsumo != null) {
			filtro.put(CODIGO_PONTO_CONSUMO, idCodigoPontoConsumo);
		}

		String numeroDocumento = creditoDebitoVO.getNumeroDocumento();
		if (numeroDocumento != null && !numeroDocumento.isEmpty()) {
			filtro.put(NUMERO_DOCUMENTO, numeroDocumento);
		}
	}

	private void carregarCampos(Model model, CreditoDebitoVO creditoDebitoVO) throws GGASException {

		Long idCliente = creditoDebitoVO.getIdCliente();
		Long idImovel = creditoDebitoVO.getIdImovel();
		Imovel imovel = null;
		Collection<PontoConsumo> colecaoPontoConsumo = null;

		Collection<EntidadeConteudo> colecaoPeriodicidade = new ArrayList<EntidadeConteudo>();

		if (((idImovel != null) && (idImovel > 0)) && ((idCliente == null) || (idCliente == 0))) {
			colecaoPontoConsumo = controladorPontoConsumo.listarPontoConsumoComContratoAtivoPorImovel(idImovel);

			if (colecaoPontoConsumo.size() == 0) {
				throw new NegocioException(ERRO_NEGOCIO_IMOVEL_SEM_CONTRATO_ATIVO, false);
			}

			imovel = (Imovel) controladorImovel.obter(idImovel, "quadraFace", "quadraFace.endereco", "quadraFace.endereco.cep", "quadra");
			creditoDebitoVO.setNomeImovel(imovel.getNome());
			colecaoPeriodicidade = controladorEntidadeConteudo.obterListaPeriodicidadeCobranca();

			QuadraFace quadraFace = imovel.getQuadraFace();
			creditoDebitoVO.setNomeFantasiaImovel(imovel.getNome());
			creditoDebitoVO.setMatriculaImovel(String.valueOf(imovel.getChavePrimaria()));
			creditoDebitoVO.setCidadeImovel(quadraFace.getEndereco().getCep().getNomeMunicipio());
			creditoDebitoVO.setEnderecoImovel(imovel.getEnderecoFormatado());
		} else if ((idCliente != null) && (idCliente > 0)) {
			if (idImovel != null && (idImovel > 0)) {
				colecaoPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
				String periodicidadeMensal = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_MENSAL);
				EntidadeConteudo periodicidade = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(periodicidadeMensal));
				colecaoPeriodicidade.add(periodicidade);
				String periodicidadeFaturamento =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_A_CADA_FATURAMENTO);
				EntidadeConteudo periodicidadeaCadaFaturamento =
						controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(periodicidadeFaturamento));
				colecaoPeriodicidade.add(periodicidadeaCadaFaturamento);

			} else {
				colecaoPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
				String periodicidadeMensal = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_MENSAL);
				EntidadeConteudo periodicidade = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(periodicidadeMensal));
				colecaoPeriodicidade.add(periodicidade);
			}
		}
		model.addAttribute("listaPontoConsumo", colecaoPontoConsumo);

		String indicadorCreditoDebito = creditoDebitoVO.getIndicadorCreditoDebito();
		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			Collection<Rubrica> colecaoRubrica =
					controladorRubrica.processarRubricasPortipoCreditoDebito(Boolean.parseBoolean(indicadorCreditoDebito));
			model.addAttribute("listaRubrica", colecaoRubrica);
		}

		Collection<CreditoOrigem> colecaoCreditoOrigem = controladorCreditoDebito.listarCreditoOrigem();
		model.addAttribute("listaCreditoOrigem", colecaoCreditoOrigem);

		model.addAttribute("listaPeriodicidade", colecaoPeriodicidade);

		Collection<EntidadeConteudo> colecaoDiasAposInicioCobranca = controladorEntidadeConteudo.obterListaDiasAposInicioCobranca();
		model.addAttribute("listaAposInicioCobranca", colecaoDiasAposInicioCobranca);

		Collection<EntidadeConteudo> colecaoPeriodicidadeJuros = controladorEntidadeConteudo.obterListaPeriodicidadeJuros();
		model.addAttribute("listaPeriodicidadeJuros", colecaoPeriodicidadeJuros);

		ParametroSistema parametroTamanhoDescricao =
				controladorParametroSistema.obterParametroPorCodigo(ParametroSistema.TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR);
		model.addAttribute("tamanhoDescricao", parametroTamanhoDescricao.getValor());
	}
	
	
	/**
	 * Método responsável por imprimir a segunda via da nota de crédito / débito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param response
	 *            - {@link HttpServletResponse}
	 * @param idCreditoDebitoNegociado
	 *            - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("imprimirSegundaViaCreditoDebitoRealizar")
	public String imprimirSegundaViaCreditoDebitoRealizar(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "chavePrimaria", required = false) Long idCreditoDebitoNegociado) throws Exception {

		try {
			if ((idCreditoDebitoNegociado != null) && (idCreditoDebitoNegociado > 0)) {

				CreditoDebitoNegociado creditoDebitoNegociado = controladorCreditoDebito.obterCreditoDebitoNegociado(idCreditoDebitoNegociado, "rubrica",
						PERIODICIDADE_JUROS, "periodicidadeCobranca", "creditoOrigem", "eventoInicioCobranca", "ultimoUsuarioAlteracao", STATUS);

				Map<String, Object> filtro = new HashMap<>();
				if ((idCreditoDebitoNegociado != null) && (idCreditoDebitoNegociado > 0)) {
					filtro.put("idCreditoDebitoNegociado", idCreditoDebitoNegociado);
				}

				Collection<CreditoDebitoARealizar> listaCDARealizar = controladorCreditoDebito.consultarCreditoDebitoARealizar(filtro);
				
				byte[] relatorioNotaDebitoCredito = null;
				
				if (listaCDARealizar != null && !listaCDARealizar.isEmpty()) {
					relatorioNotaDebitoCredito = controladorCreditoDebito
							.relatorioCreditoDebitoRealizar(creditoDebitoNegociado, listaCDARealizar);
				}

				if (relatorioNotaDebitoCredito != null) {
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType("application/pdf");
					response.setContentLength(relatorioNotaDebitoCredito.length);
					response.addHeader("Content-Disposition", "attachment; filename=relatorioNotaDebitoCredito.pdf");
					servletOutputStream.write(relatorioNotaDebitoCredito, 0, relatorioNotaDebitoCredito.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaCreditoDebito(model, request);
	}
}
