package br.com.ggas.web.faturamento.simulacaocalculo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.exception.CalculoFornecimentoException;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas operações referentes a simulação do cálculo
 * utilizando dados fictícios.
 * 
 * @author esantana
 *
 */
@Controller
public class SimulacaoDadosFicticiosAction extends GenericAction {

	private static final String CONSUMO_APURADO = "consumoApurado";

	private static final String LISTA_TARIFA_DADOS_FICTICIOS = "listaTarifa";

	private static final String ID_TARIFA_SIMULADA = "idTarifa";

	private static final String DATA_FIM = "dataFim";

	private static final String DATA_INICIO = "dataInicio";

	private static final String ITEM_FATURA = "itemFatura";

	private static final String SCALA_CONSUMO_APURADO = "escalaConsumoApurado";

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas;

	/**
	 * Controlador parametros sistema
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;
	
	/**
	 * Método responsável por exibir a página de Simulação de Cálculo de Consumo com
	 * Dados Fictícios.
	 * 
	 * @param model            - {@link Model}
	 * @param request          - {@link HttpServletRequest}
	 * @param idTarifaSimulada - {@link Long}
	 * @param dataInicio       - {@link String}
	 * @param dataFim          - {@link String}
	 * @param totalConsumido   - {@link String}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("exibirPesquisaSimulacaoCalculoDadosFicticios")
	public String exibirPesquisaSimulacaoCalculoDadosFicticios(Model model, HttpServletRequest request,
			@RequestParam(value = ID_TARIFA_SIMULADA, required = false, defaultValue = "-1") Long idTarifaSimulada,
			@RequestParam(value = DATA_INICIO, required = false) String dataInicio,
			@RequestParam(value = DATA_FIM, required = false) String dataFim,
			@RequestParam(value = CONSUMO_APURADO, required = false) String totalConsumido) {

		try {
			saveToken(request);

			Collection<Tarifa> listaTarifas = controladorTarifa.listarTarifas();

			model.addAttribute(LISTA_TARIFA_DADOS_FICTICIOS, listaTarifas);

			if (!super.isPostBack(request)) {
				request.getSession().removeAttribute("listaFornecimentoGasSimulado");
			}

			model.addAttribute(ID_TARIFA_SIMULADA, idTarifaSimulada);
			model.addAttribute(DATA_INICIO, dataInicio);
			model.addAttribute(DATA_FIM, dataFim);
			model.addAttribute(CONSUMO_APURADO, totalConsumido);
		} catch (NegocioException e) {
			super.mensagemErro(model, e);
		}

		return "exibirPesquisaSimulacaoCalculoDadosFicticios";
	}

	/**
	 * Calcular dados ficticios.
	 * 
	 * @param request          - {@link HttpServletRequest}
	 * @param model            - {@link Model}
	 * @param idTarifaSimulada - {@link Long}
	 * @param dataInicio       - {@link String}
	 * @param dataFim          - {@link String}
	 * @param totalConsumido   - {@link String}
	 * @return view - {@link String}
	 * @throws NegocioException 
	 */
	@RequestMapping("calcularDadosFicticios")
	public String calcularDadosFicticios(HttpServletRequest request, Model model,
			@RequestParam(value = ID_TARIFA_SIMULADA, required = false) Long idTarifaSimulada,
			@RequestParam(value = DATA_INICIO, required = false) String dataInicio,
			@RequestParam(value = DATA_FIM, required = false) String dataFim,
			@RequestParam(value = CONSUMO_APURADO, required = false) String totalConsumido) throws NegocioException {
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		String scala = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO)
				.getValor();
		
		String view = "forward:/exibirPesquisaSimulacaoCalculoDadosFicticios";

		model.addAttribute(ID_TARIFA_SIMULADA, idTarifaSimulada);
		model.addAttribute(DATA_INICIO, dataInicio);
		model.addAttribute(DATA_FIM, dataFim);
		model.addAttribute(CONSUMO_APURADO, totalConsumido);

		try {
			validarToken(request);

			Date dataInicial = null;
			Date dataFinal = null;

			if (!StringUtils.isEmpty(dataInicio)) {
				dataInicial = Util.converterCampoStringParaData("Data de Início do Período", dataInicio,
						Constantes.FORMATO_DATA_BR);
			}
			if (!StringUtils.isEmpty(dataFim)) {
				dataFinal = Util.converterCampoStringParaData("Data Final do Período", dataFim,
						Constantes.FORMATO_DATA_BR);
			}

			controladorCalculoFornecimentoGas.validaDadosCalcularFornecimentoFicticio(idTarifaSimulada, dataInicial,
					dataFinal, totalConsumido);
			Tarifa tarifaSimulada = (Tarifa) controladorTarifa.obter(idTarifaSimulada, ITEM_FATURA);
			BigDecimal qtdCasasDecimaisTarifaSimulada = tarifaSimulada.getQuantidadeCasaDecimal();

			BigDecimal consumo = Util.converterCampoStringParaValorBigDecimal("Total Consumido", totalConsumido,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

			DadosFornecimentoGasVO dadosFornecimentoGasSimulado = controladorCalculoFornecimentoGas
					.calcularValorFornecimentoGas(tarifaSimulada.getItemFatura(), dataInicial, dataFinal, consumo,
							tarifaSimulada, Boolean.TRUE, false, null, Boolean.FALSE);

			Collection<DadosFaixaVO> dadosFaixaSimulado = controladorCalculoFornecimentoGas
					.agruparDadosFaixaParaExibicao(dadosFornecimentoGasSimulado);

			if (dadosFaixaSimulado == null || dadosFaixaSimulado.isEmpty()) {
				dadosFaixaSimulado = controladorCalculoFornecimentoGas
						.agruparDadosVigenciaParaExibicao(dadosFornecimentoGasSimulado);
			}

			controladorCalculoFornecimentoGas.preencherValoresTotalLiquido(dadosFaixaSimulado);

			Collection<DadosTributoVO> listaTributosAliquotaVO = dadosFornecimentoGasSimulado.getColecaoTributos();

			saveToken(request);
			model.addAttribute(LISTA_TARIFA_DADOS_FICTICIOS, controladorTarifa.listarTarifas());

			String mascaraTarifaSimulada = Util
					.montarMascaraPorQtdCasasDecimais(qtdCasasDecimaisTarifaSimulada.intValue());

			request.getSession().setAttribute("listaTributacao", listaTributosAliquotaVO);
			request.getSession().setAttribute("listaFornecimentoGasSimulado", dadosFaixaSimulado);
			request.getSession().setAttribute("maskTarifaSimulada", mascaraTarifaSimulada);
			request.getSession().setAttribute(SCALA_CONSUMO_APURADO, scala);
			view = "exibirPesquisaSimulacaoCalculoDadosFicticios";
		} catch (CalculoFornecimentoException e) {
			super.mensagemErro(model, e.getMessage());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}
		
	 
}
