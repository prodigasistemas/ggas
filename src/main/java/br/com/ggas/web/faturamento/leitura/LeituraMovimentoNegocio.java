/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.RepositorioAnaliseMedicao;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.TipoAtividadeSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.*;
import br.com.ggas.medicao.leitura.impl.SituacaoLeituraMovimentoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.web.faturamento.leitura.dto.DadosMedicaoDTO;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaLeituraMovimento;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Classe de negócio da leitura movimento
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
@Transactional
public class LeituraMovimentoNegocio {

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	private RepositorioAnaliseMedicao repositorioContrato;

	@Autowired
	private CalculadoraVolumeLeitura calculadoraVolume;

	@Autowired
	@Qualifier(ControladorCronogramaAtividadeFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO)
	private ControladorCronogramaAtividadeFaturamento controladorAtvFaturamento;

	@Autowired
	private ControladorRota controladorRota;

	/**
	 * Busca a lista de dtos de Leitura movimento
	 * @param pesquisa parametro de busca apra filtrar a pesquisa de leituras movimento
	 * @return retorna o {@link DatatablesServerSideDTO}
	 */
	public DatatablesServerSideDTO<LeituraMovimentoDTO> buscarLeituras(PesquisaLeituraMovimento pesquisa) {
		final Pair<List<LeituraMovimentoDTO>, Long> dadosPar = controladorLeituraMovimento.criarListaLeituraMovimentoDTO(pesquisa);
		final List<LeituraMovimentoDTO> dados = dadosPar.getFirst();
		for (LeituraMovimentoDTO info : dados) {
			info.setVolume(calculadoraVolume.calcularVolume(info));
			this.popularPtz(info);
		}
		return new DatatablesServerSideDTO<>(pesquisa.getOffset(), dadosPar.getSecond(), dadosPar.getFirst());
	}

	/**
	 * Atualiza as informações de dados de medição da leitura movimento
	 *
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param dadosMedicao dados de medição
	 * @throws GGASException caso ocorra alguma falha na execução
	 */
	public void atualizarDadosMedicao(Long grupoFaturamento, List<DadosMedicaoDTO> dadosMedicao) throws GGASException {
		if (CollectionUtils.isNotEmpty(dadosMedicao)) {

			for (DadosMedicaoDTO dados : dadosMedicao) {
				final LeituraMovimento leitura = (LeituraMovimento) this.controladorLeituraMovimento.obter(dados.getChaveLeitura());
				final BigDecimal valorLeitura = Optional.ofNullable(dados.getNovaLeitura()).map(BigDecimal::valueOf).orElse(null);
				final BigDecimal pressao = Optional.ofNullable(dados.getPressao()).map(BigDecimal::valueOf).orElse(null);
				final BigDecimal temperatura = Optional.ofNullable(dados.getTemperatura()).map(BigDecimal::valueOf).orElse(null);
				final Date dataLeitura = Optional.ofNullable(dados.getDataLeitura()).map(DataUtil::toDate).orElse(null);

				leitura.setValorLeitura(valorLeitura);
				leitura.setCodigoAnormalidadeLeitura(dados.getChaveAnormalidade());
				leitura.setPressaoInformada(pressao);
				leitura.setTemperaturaInformada(temperatura);
				leitura.setDataLeitura(dataLeitura);
				leitura.setObservacaoLeitura(dados.getObservacaoLeitura());
				leitura.setIndicadorConfirmacaoLeitura(true);

				final SituacaoLeituraMovimento situacao = new SituacaoLeituraMovimentoImpl();
				situacao.setChavePrimaria(TipoAtividadeSistema.REGISTRAR_LEITURA.getNumeroSequencia());
				leitura.setSituacaoLeitura(situacao);
				leitura.setOrigem(OrigemLeituraMovimento.ANALISE_MEDICAO.name());

				this.controladorLeituraMovimento.atualizar(leitura);
			}

			final GrupoFaturamento grupo = controladorRota.obterGrupoFaturamento(grupoFaturamento);
			this.controladorAtvFaturamento.atualizarAtividadeFaturamentoSeExistir(grupoFaturamento,
					TipoAtividadeSistema.REALIZAR_LEITURA.getNumeroSequencia(), grupo.getAnoMesReferencia());
		}
	}

	/**
	 * Altera status das leituras informadas para que elas possam ser relidas
	 * @param leituras As chaves das leituras para alteração do status
	 * @throws GGASException GGASException
	 */
	public void solicitarReleitura(List<Long> leituras) throws GGASException {
		if (CollectionUtils.isNotEmpty(leituras)) {
			for (Long chaveLeitura : leituras) {
				final LeituraMovimento leitura = (LeituraMovimento) this.controladorLeituraMovimento.obter(chaveLeitura);
				final SituacaoLeituraMovimento situacao = new SituacaoLeituraMovimentoImpl();
				situacao.setChavePrimaria(SituacaoLeituraMovimento.GERADO);
				leitura.setSituacaoLeitura(situacao);
				this.controladorLeituraMovimento.atualizar(leitura);
			}
		}
	}

	/**
	 * Busca o range de leitura em que possui a primeira data válida, para a rota e o id do grupo de faturamento
	 * @param idRota identificador da rota
	 * @param idGrupoFaturamento identificador do grupo
	 * @return retorna um para de datas, correspondendo inicio e fim respectivamente
	 */
	public Pair<LocalDateTime, LocalDateTime> buscarRangeLeitura(Long idRota, Long idGrupoFaturamento) {
		LocalDateTime dataInicio = YearMonth.from(LocalDateTime.now()).atDay(1).atStartOfDay();

		if (idRota != null && idGrupoFaturamento != null) {
			final LocalDateTime inicioLeituraMovimento = controladorLeituraMovimento.buscarInicioLeituraMovimento(idRota, idGrupoFaturamento);
			if (inicioLeituraMovimento != null) {
				dataInicio = inicioLeituraMovimento;
			}
		}

		LocalDateTime dataFim = dataInicio.plusMonths(1L);

		return new Pair<>(dataInicio, dataFim);
	}

	/**
	 * Popula a informação de ptz (volume e fator de correção) no dto
	 * @param dto dto a ser preenchido a informação
	 */
	private void popularPtz(LeituraMovimentoDTO dto) {
		if (dto.getVolume() != null) {
			final ContratoPontoConsumo contrato = this.repositorioContrato
					.obterContratoParaAnaliseMedicao(dto.getChavePontoConsumo());

			final Double fatorCorrecao = Optional.ofNullable(contrato)
					.map(ContratoPontoConsumo::getNumeroFatorCorrecao)
					.map(BigDecimal::doubleValue).orElse(1.0d);

			final Double volumePtz = dto.getVolume() * fatorCorrecao;

			dto.setFcPtz(fatorCorrecao);
			dto.setVolumePtz(volumePtz);
		}
	}
	
	public DatatablesServerSideDTO<LeituraMovimentoDTO> buscarHistoricoLeituras(Long chavePontoConsumo) {
		final Pair<List<LeituraMovimentoDTO>, Long> dadosPar = controladorLeituraMovimento.criarListaHistoricoLeituraMovimentoDTO(chavePontoConsumo);
		return new DatatablesServerSideDTO<>(1, dadosPar.getSecond(), dadosPar.getFirst());
	}

}
