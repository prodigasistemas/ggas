/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 25/06/2013 16:31:44
 * @author asoares
 */

package br.com.ggas.web.faturamento.serienotafiscal;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.faturamento.fatura.impl.SerieImpl;
import br.com.ggas.faturamento.fatura.impl.SerieVO;
import br.com.ggas.faturamento.serie.ControladorSerie;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas com a Série de Nota Fiscal e seus
 * respectivos métodos.
 *
 */
@Controller
public class ManterSerieAction extends GenericAction {

	private static final String EXIBIR_PESQUISA_SERIE_NOTA_FISCAL = "exibirPesquisaSerieNotaFiscal";

	private static final String SERIE_VO = "serieVO";

	private static final String SERIE_NOTA_FISCAL = "serieNotaFiscal";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String NUMERO = "numero";

	private static final String DESCRICAO = "descricao";

	private static final String SEGMENTO = "segmento";

	private static final String ANO_MES = "anoMes";

	private static final String TIPO_FATURAMENTO = "tipoFaturamento";

	private static final String TIPO_OPERACAO = "operacao";

	private static final String HABILITADO = "habilitado";

	private static final String HABILITADO_SCAN = "indicadorContingenciaScan";

	private static final String HABILITADO_ELETRONICA = "indicadorSerieEletronica";

	private static final String HABILITADO_MENSAL = "indicadorControleMensal";

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorSerie controladorSerie;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Método responsável por exibir tela de pesquisa serie nota fiscal.
	 *
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping(EXIBIR_PESQUISA_SERIE_NOTA_FISCAL)
	public String exibirPesquisaSerieNotaFiscal(Model model) throws GGASException {
		carregarCombos(model);

		return EXIBIR_PESQUISA_SERIE_NOTA_FISCAL;
	}

	/**
	 * Método responsável por pesquisar serie nota fiscal.
	 *
	 * @param serie
	 *            {@link SerieImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param serieVO {@link SerieVO}
	 * @param results {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("pesquisarSerieNotaFiscal")
	public String pesquisarSerieNotaFiscal(SerieImpl serie, BindingResult result, SerieVO serieVO,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		model.addAttribute(SERIE_NOTA_FISCAL, serie);
		model.addAttribute(SERIE_VO, serieVO);
		prepararFiltro(filtro, serie, serieVO, request);
		super.adicionarFiltroPaginacao(request, filtro);
		Collection<Serie> listaSerie = controladorSerie.listarSerie(filtro);

		model.addAttribute("listaSerie", super.criarColecaoPaginada(request, filtro, listaSerie));

		return exibirPesquisaSerieNotaFiscal(model);
	}

	/**
	 * Método resposável por carregar combos para tela.
	 *
	 * @param model
	 *            {@link Model}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	private void carregarCombos(Model model) throws GGASException {

		String cOperacao = obterConstanteSistema("C_TIPO_OPERACAO");
		String cTipoFaturamento = obterConstanteSistema("C_TIPO_FATURAMENTO");

		model.addAttribute("listaOperacao",
				controladorEntidadeConteudo.listarEntidadeConteudo(Long.parseLong(cOperacao)));
		model.addAttribute("listaTipoFaturamento",
				controladorEntidadeConteudo.listarEntidadeConteudo(Long.parseLong(cTipoFaturamento)));
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
	}

	/**
	 * Método responsável por preparar filtro para pesquisa.
	 *
	 * @param filtro
	 *            {@link Map}
	 * @param serie
	 *            {@link SerieImpl}
	 * @param request
	 *            {@link HttpServletRequest}
	 */
	private void prepararFiltro(Map<String, Object> filtro, SerieImpl serie, SerieVO serieVO,
			HttpServletRequest request) {

		if (serie.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, serie.getChavePrimaria());
		}

		if (serie.getNumero() != null) {
			filtro.put(NUMERO, serie.getNumero());
		}

		if (serie.getDescricao() != null) {
			filtro.put(DESCRICAO, serie.getDescricao());
		}

		if (serie.getTipoFaturamento() != null) {
			filtro.put(TIPO_FATURAMENTO, serie.getTipoFaturamento().getChavePrimaria());
		}

		if (serie.getTipoOperacao() != null) {
			filtro.put(TIPO_OPERACAO, serie.getTipoOperacao().getChavePrimaria());
		}

		if (serie.getSegmento() != null) {
			filtro.put(SEGMENTO, serie.getSegmento().getChavePrimaria());
		}

		String anoMes = request.getParameter(ANO_MES);
		if (!StringUtils.isEmpty(anoMes)) {
			filtro.put(ANO_MES, anoMes);
		}

		filtro.put(HABILITADO, serieVO.getHabilitado());

		filtro.put(HABILITADO_SCAN, serieVO.getIndicadorContingenciaScan());

		filtro.put(HABILITADO_ELETRONICA, serieVO.getIndicadorSerieEletronica());

		filtro.put(HABILITADO_MENSAL, serieVO.getIndicadorControleMensal());

	}

	/**
	 * Método responsável por popular serie.
	 *
	 * @param serie
	 *            {@link SerieImpl}
	 * @param model
	 *            {@link Model}
	 * @throws FormatoInvalidoException
	 */
	private void popularSerie(SerieImpl serie, SerieVO serieVO, Model model)
			throws FormatoInvalidoException {

		model.addAttribute(SERIE_VO, serieVO);
		if (serieVO.getIndicadorControleMensal() != null) {
			serie.setIndicadorControleMensal(serieVO.getIndicadorControleMensal());
		}

		if (serieVO.getIndicadorSerieEletronica()) {
			serie.setIndicadorSerieEletronica(serieVO.getIndicadorSerieEletronica());
		}

		if (serieVO.getIndicadorContingenciaScan()) {
			serie.setIndicadorContingenciaScan(serieVO.getIndicadorContingenciaScan());
		}

		if (!StringUtils.isEmpty(serieVO.getAnoMes())) {
			String valorAnoMes = serieVO.getAnoMes().replace("/", "").replace("_", "");
			Integer valorInteiroAnoMes = 0;
			if (valorAnoMes.length() == 6) {
				valorInteiroAnoMes = Integer.valueOf(valorAnoMes.substring(2, 6) + valorAnoMes.substring(0, 2));
			}
			serie.setAnoMes(valorInteiroAnoMes);
			model.addAttribute(ANO_MES, serie.getAnoMesFormatado());
		} else {
			serie.setAnoMes(null);
		}

		if (!serieVO.getSequenciaNumeracao().isEmpty()) {
			serie.setSequenciaNumeracao(
					Util.converterCampoStringParaValorLong("Sequencial Numeração", serieVO.getSequenciaNumeracao()));
		}

		if (serieVO.getHabilitado() != null) {
			serie.setHabilitado(serieVO.getHabilitado());
		}
	}

	/**
	 * Obter constante sistema.
	 *
	 * @param codigo
	 *            {@link String}
	 * @return String {@link String}
	 */
	private String obterConstanteSistema(String codigo) {

		return controladorConstanteSistema.obterConstantePorCodigo(codigo).getValor();
	}

	/**
	 * Método responsável por exibir tela de inclusao serie nota fiscal.
	 *
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoSerieNotaFiscal")
	public String exibirInclusaoSerieNotaFiscal(Model model) throws GGASException {
		carregarCombos(model);

		return "exibirInclusaoSerieNotaFiscal";
	}

	/**
	 * Método responsável por incluir serie nota fiscal.
	 *
	 * @param serie
	 *            {@link SerieImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param serieVO
	 *            {@link SerieVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("incluirSerieNotaFiscal")
	public String incluirSerieNotaFiscal(SerieImpl serie, BindingResult result, SerieVO serieVO, BindingResult results,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirInclusaoSerieNotaFiscal";
		model.addAttribute(SERIE_NOTA_FISCAL, serie);
		model.addAttribute(SERIE_VO, serieVO);
		try {
			popularSerie(serie, serieVO, model);
			serie.setDadosAuditoria(getDadosAuditoria(request));
			serie.setHabilitado(true);
			serieVO.setHabilitado(true);
			controladorSerie.inserir(serie);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Serie.SERIE_CAMPO_STRING);
			view = pesquisarSerieNotaFiscal(serie, result, serieVO, results, request, model);
		} catch (GGASException e) {
			carregarCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir alteracao serie nota fiscal.
	 *
	 *
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoSerieNotaFiscal")
	public String exibirAlteracaoSerieNotaFiscal(@RequestParam("chavePrimaria") Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoSerieNotaFiscal";

		try {
			carregarCombos(model);
			Serie serie = (Serie) controladorSerie.obter(chavePrimaria);
			EntidadeConteudo tipoOperacao = controladorEntidadeConteudo.obterEntidadeConteudo(serie.getTipoOperacao().getChavePrimaria());
			EntidadeConteudo tipoFaturamento =
					controladorEntidadeConteudo.obterEntidadeConteudo(serie.getTipoFaturamento().getChavePrimaria());

			Segmento segmento = null;
			if (serie.getSegmento() != null) {
				segmento = controladorSegmento.obterSegmento(serie.getSegmento().getChavePrimaria());
			} else {
				segmento = null;
			}

            serie.setTipoOperacao(tipoOperacao);
            serie.setTipoFaturamento(tipoFaturamento);
            serie.setSegmento(segmento);

			model.addAttribute(SERIE_NOTA_FISCAL, serie);
			model.addAttribute(CHAVE_PRIMARIA, serie.getChavePrimaria());
			model.addAttribute(ANO_MES, serie.getAnoMesFormatado());
			boolean serieEmUso = controladorSerie.existeSerieEmUso(chavePrimaria);
			model.addAttribute("serieEmUso", serieEmUso);
			saveToken(request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = EXIBIR_PESQUISA_SERIE_NOTA_FISCAL;
		}

		return view;
	}

	/**
	 * Método responsável por alterar serie nota fiscal.
	 *
	 * @param serie
	 *            {@link SerieImpl}
	 * @param result
	 *            {@link BindingResult}
	 * @param serieVO
	 *            {@link SerieVO}
	 * @param results
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("alterarSerieNotaFiscal")
	public String alterarSerieNotaFiscal(SerieImpl serie, BindingResult result, SerieVO serieVO, BindingResult results,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirAlteracaoSerieNotaFiscal";
		model.addAttribute(SERIE_NOTA_FISCAL, serie);
		model.addAttribute(SERIE_VO, serieVO);
		try {
			Serie serieConsultada = (Serie) controladorSerie.obter(serie.getChavePrimaria());
			serie.setTipoFaturamento(controladorEntidadeConteudo.obterEntidadeConteudo(serieConsultada.getTipoFaturamento().getChavePrimaria()));
			serie.setTipoOperacao(controladorEntidadeConteudo.obterEntidadeConteudo(serieConsultada.getTipoOperacao().getChavePrimaria()));
			
			if (serieConsultada.getSegmento() != null) {
				serie.setSegmento(controladorSegmento.obterSegmento(serieConsultada.getSegmento().getChavePrimaria()));
			}

			popularSerie(serie, serieVO, model);
			serie.setDadosAuditoria(getDadosAuditoria(request));
			controladorSerie.atualizar(serie);

			model.addAttribute(SERIE_NOTA_FISCAL, serie);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Serie.SERIE_CAMPO_STRING);
			view = pesquisarSerieNotaFiscal(serie, result, serieVO, results, request, model);
		} catch (GGASException e) {
			carregarCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por remover serie nota fiscal.
	 *
	 * @param chavesPrimarias
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("removerSerieNotaFiscal")
	public String removerSerieNotaFiscal(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias,
			HttpServletRequest request, Model model) {

		try {
			validarSelecaoUmOuMais(chavesPrimarias);
			controladorSerie.verificarSerieEmUso(chavesPrimarias);
			controladorSerie.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, Serie.SERIE_CAMPO_STRING);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return EXIBIR_PESQUISA_SERIE_NOTA_FISCAL;
	}

	/**
	 * Método responsável por exibir tela de detalhamento de serie nota fiscal.
	 *
	 * @param chavePrimaria
	 *            {@link Long}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("exibirDetalheSerieNotaFiscal")
	public String exibirDetalheSerieNotaFiscal(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalheSerieNotaFiscal";
		try {
			SerieImpl serie = (SerieImpl) controladorSerie.obter(chavePrimaria, "tipoOperacao", TIPO_FATURAMENTO,
					SEGMENTO);

			model.addAttribute("serie", serie);
			saveToken(request);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = exibirPesquisaSerieNotaFiscal(model);
		}

		return view;
	}
}
