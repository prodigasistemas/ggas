package br.com.ggas.web.faturamento.simulacaocalculo;
/**
 * @author pedro
 * Classe responsável pela transferência de dados entre os métodos relacionados a simulação de dados reais.
 *
 */
public class SimulacaoDadosReaisVO {

	private Long idHistoricoConsumo;
	private Long idVigenciaTarifa;
	private Long idTarifaSimulada;
	private Long idPontoConsumo;
	private Long chavePrimaria;
	private Long idTarifaAtual;
	private Long idMesAnoCiclo;
	private Long idItemFatura;
	private Long idVigencia;
	private Long idContrato;
	private Long idCliente;
	private Long idImovel;
	private String dataFim;
	private String qtdConsumo;
	private String dataInicio;
	private String indicadorPesquisa;
	
	public Long getIdHistoricoConsumo() {
		return idHistoricoConsumo;
	}
	public void setIdHistoricoConsumo(Long idHistoricoConsumo) {
		this.idHistoricoConsumo = idHistoricoConsumo;
	}
	public Long getIdVigenciaTarifa() {
		return idVigenciaTarifa;
	}
	public void setIdVigenciaTarifa(Long idVigenciaTarifa) {
		this.idVigenciaTarifa = idVigenciaTarifa;
	}
	public Long getIdTarifaSimulada() {
		return idTarifaSimulada;
	}
	public void setIdTarifaSimulada(Long idTarifaSimulada) {
		this.idTarifaSimulada = idTarifaSimulada;
	}
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Long getIdTarifaAtual() {
		return idTarifaAtual;
	}
	public void setIdTarifaAtual(Long idTarifaAtual) {
		this.idTarifaAtual = idTarifaAtual;
	}
	public Long getIdMesAnoCiclo() {
		return idMesAnoCiclo;
	}
	public void setIdMesAnoCiclo(Long idMesAnoCiclo) {
		this.idMesAnoCiclo = idMesAnoCiclo;
	}
	public Long getIdItemFatura() {
		return idItemFatura;
	}
	public void setIdItemFatura(Long idItemFatura) {
		this.idItemFatura = idItemFatura;
	}
	public Long getIdVigencia() {
		return idVigencia;
	}
	public void setIdVigencia(Long idVigencia) {
		this.idVigencia = idVigencia;
	}
	public Long getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getQtdConsumo() {
		return qtdConsumo;
	}
	public void setQtdConsumo(String qtdConsumo) {
		this.qtdConsumo = qtdConsumo;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

}
