/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * aLong with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Optional;

/**
 * DTO de leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientBranchCoverage", "common-java:InsufficientLineCoverage",
		"common-java:InsufficientCommentDensity"})
public class LeituraMovimentoDTO {

	private Long chaveLeituraMovimento;
	private Long chavePontoConsumo;
	private String pontoConsumo;
	private String mesAnoCiclo;
	private String observacaoPontoConsumo;
	private String observacaoLeitura;
	private String dataLeitura;
	private Double leituraAnterior;
	private Double leituraAtual;
	private Double volume;
	private Double pressao;
	private Double temperatura;
	private Double fcPtz;
	private Double volumePtz;
	private AnormalidadeDTO anormalidadeLeitura;
	private SituacaoLeituraMovimento situacaoLeitura;
	private AnormalidadeDTO anormalidadeConsumo;
	private Double consumoApurado;
	private String tipoConsumo;
	private String origem;
	private Integer corrigePT;
	private String fotoColetor;
	private Long chaveRota;
	private String situacaoPontoConsumo;
	private String dataLeituraAnterior;
	private Long chaveHistoricoMedicao;
	
	private Double consumo;
	private Double fcPcs;
	private Integer quantidadeDias;

	public Long getChaveLeituraMovimento() {
		return chaveLeituraMovimento;
	}

	public void setChaveLeituraMovimento(Long chaveLeituraMovimento) {
		this.chaveLeituraMovimento = chaveLeituraMovimento;
	}

	public Long getChavePontoConsumo() {
		return chavePontoConsumo;
	}

	public void setChavePontoConsumo(Long chavePontoConsumo) {
		this.chavePontoConsumo = chavePontoConsumo;
	}

	public String getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	public String getObservacaoPontoConsumo() {
		return observacaoPontoConsumo;
	}

	public void setObservacaoPontoConsumo(String observacaoPontoConsumo) {
		this.observacaoPontoConsumo = observacaoPontoConsumo;
	}

	public String getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}

	public Double getLeituraAnterior() {
		return leituraAnterior;
	}

	public void setLeituraAnterior(Double leituraAnterior) {
		this.leituraAnterior = leituraAnterior;
	}

	public Double getLeituraAtual() {
		return leituraAtual;
	}

	public void setLeituraAtual(Double leituraAtual) {
		this.leituraAtual = leituraAtual;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getPressao() {
		return pressao;
	}

	public void setPressao(Double pressao) {
		this.pressao = pressao;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public AnormalidadeDTO getAnormalidadeLeitura() {
		return anormalidadeLeitura;
	}

	public void setAnormalidadeLeitura(AnormalidadeDTO anormalidadeLeitura) {
		this.anormalidadeLeitura = anormalidadeLeitura;
	}

	public Double getFcPtz() {
		return fcPtz;
	}

	public void setFcPtz(Double fcPtz) {
		this.fcPtz = fcPtz;
	}

	public Double getVolumePtz() {
		return volumePtz;
	}

	public void setVolumePtz(Double volumePtz) {
		this.volumePtz = volumePtz;
	}

	public SituacaoLeituraMovimento getSituacaoLeitura() {
		return situacaoLeitura;
	}

	public void setSituacaoLeitura(SituacaoLeituraMovimento situacaoLeitura) {
		this.situacaoLeitura = situacaoLeitura;
	}

	public String getObservacaoLeitura() {
		return observacaoLeitura;
	}

	public void setObservacaoLeitura(String observacaoLeitura) {
		this.observacaoLeitura = observacaoLeitura;
	}

	public AnormalidadeDTO getAnormalidadeConsumo() {
		return anormalidadeConsumo;
	}

	public void setAnormalidadeConsumo(AnormalidadeDTO anormalidadeConsumo) {
		this.anormalidadeConsumo = anormalidadeConsumo;
	}

	public String getTipoConsumo() {
		return tipoConsumo;
	}

	public void setTipoConsumo(String tipoConsumo) {
		this.tipoConsumo = tipoConsumo;
	}

	public Double getConsumoApurado() {
		return consumoApurado;
	}

	public void setConsumoApurado(Double consumoApurado) {
		this.consumoApurado = consumoApurado;
	}

	public String getMesAnoCiclo() {
		return mesAnoCiclo;
	}

	public void setMesAnoCiclo(String mesAnoCiclo) {
		this.mesAnoCiclo = mesAnoCiclo;
	}

	@Override public boolean equals(Object o) {
		boolean equals;

		if (this == o) {
			equals = true;
		} else if (o == null || getClass() != o.getClass()) {
			equals = false;
		} else {
			LeituraMovimentoDTO that = (LeituraMovimentoDTO) o;

			equals = new EqualsBuilder().append(chaveLeituraMovimento, that.chaveLeituraMovimento)
					.append(pontoConsumo, that.pontoConsumo)
					.append(mesAnoCiclo, that.mesAnoCiclo)
					.append(observacaoPontoConsumo, that.observacaoPontoConsumo)
					.append(observacaoLeitura, that.observacaoLeitura)
					.append(dataLeitura, that.dataLeitura)
					.append(leituraAnterior, that.leituraAnterior).append(leituraAtual, that.leituraAtual)
					.append(volume, that.volume)
					.append(pressao, that.pressao).append(temperatura, that.temperatura).append(fcPtz, that.fcPtz)
					.append(volumePtz, that.volumePtz).append(anormalidadeLeitura, that.anormalidadeLeitura)
					.append(situacaoLeitura, that.situacaoLeitura)
					.append(anormalidadeConsumo, that.anormalidadeConsumo)
					.append(consumoApurado, that.consumoApurado).append(tipoConsumo, that.tipoConsumo).isEquals();
		}

		return equals;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(chaveLeituraMovimento).append(pontoConsumo).append(mesAnoCiclo).append(observacaoPontoConsumo)
				.append(observacaoLeitura).append(dataLeitura).append(leituraAnterior).append(leituraAtual).append(volume)
				.append(pressao).append(temperatura).append(fcPtz).append(volumePtz).append(anormalidadeLeitura).append(situacaoLeitura)
				.append(anormalidadeConsumo).append(consumoApurado).append(tipoConsumo).toHashCode();
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setUseClassName(false);
		style.setUseIdentityHashCode(false);

		return new ToStringBuilder(this, style).appendSuper(super.toString()).append("chaveLeituraMovimento", chaveLeituraMovimento)
				.append("pontoConsumo", pontoConsumo).append("mesAnoCiclo", mesAnoCiclo)
				.append("observacaoPontoConsumo", observacaoPontoConsumo)
				.append("observacaoLeitura", observacaoLeitura).append("dataLeitura", dataLeitura)
				.append("leituraAnterior", leituraAnterior).append("leituraAtual", leituraAtual).append("volume", volume)
				.append("pressao", pressao).append("temperatura", temperatura).append("fcPtz", fcPtz)
				.append("volumePtz", volumePtz)
				.append("anormalidadeLeitura", anormalidadeLeitura)
				.append("situacaoLeitura", Optional.ofNullable(situacaoLeitura).map(SituacaoLeituraMovimento::getDescricao).orElse(null))
				.append("anormalidadeConsumo", anormalidadeConsumo)
				.append("consumoApurado", consumoApurado)
				.append("tipoConsumo", tipoConsumo)
				.append("corrigePT", corrigePT)
				.append("situacaoPontoConsumo", situacaoPontoConsumo)
				.append("chaveRota", chaveRota)
				.append("dataLeituraAnterior", dataLeituraAnterior)
				.append("chaveHistoricoMedicao", chaveHistoricoMedicao)
				.append("consumo", consumo)
				.append("fcPcs", fcPcs)
				.append("quantidadeDias", quantidadeDias)
				.append("fotoColetor", fotoColetor).toString();
		
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public Integer getCorrigePT() {
		return corrigePT;
	}

	public void setCorrigePT(Integer corrigePT) {
		this.corrigePT = corrigePT;
	}

	public String getFotoColetor() {
		return fotoColetor;
	}

	public void setFotoColetor(String fotoColetor) {
		this.fotoColetor = fotoColetor;
	}

	public Long getChaveRota() {
		return chaveRota;
	}

	public void setChaveRota(Long chaveRota) {
		this.chaveRota = chaveRota;
	}

	public String getSituacaoPontoConsumo() {
		return situacaoPontoConsumo;
	}

	public void setSituacaoPontoConsumo(String situacaoPontoConsumo) {
		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}

	public String getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}

	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}

	public Long getChaveHistoricoMedicao() {
		return chaveHistoricoMedicao;
	}

	public void setChaveHistoricoMedicao(Long chaveHistoricoMedicao) {
		this.chaveHistoricoMedicao = chaveHistoricoMedicao;
	}

	public Double getConsumo() {
		return consumo;
	}

	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}

	public Double getFcPcs() {
		return fcPcs;
	}

	public void setFcPcs(Double fcPcs) {
		this.fcPcs = fcPcs;
	}

	public Integer getQuantidadeDias() {
		return quantidadeDias;
	}

	public void setQuantidadeDias(Integer quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
	}
}
