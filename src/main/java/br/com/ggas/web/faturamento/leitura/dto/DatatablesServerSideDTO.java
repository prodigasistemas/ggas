/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * DTO para retornar os dados do datatable server side
 * @param <T> tipo de dados do datatable
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientBranchCoverage", "common-java:InsufficientLineCoverage",
		"common-java:InsufficientCommentDensity"})
public class DatatablesServerSideDTO<T> {

	/**
	 * Índice refere-se a página atual solicitada pelo datatable.
	 * Este índice inicia de 0
	 */
	private int pagina;

	/**
	 * Quantidade total de registros a serem apresentados.
	 * Não se refere ao tamanho da lista de {@link DatatablesServerSideDTO#dados} mas
	 * sim ao total de registros que ainda existem no banco
	 */
	private Long quantidadeTotal;

	/**
	 * Lista de dados filtrados pela consulta
	 */
	private List<T> dados;

	/**
	 * Construtor padrão do DTO de datatable
	 * @param pagina numero da pagina atual
	 * @param quantidadeTotal quantidade total de dados
	 * @param dados lista de dados
	 */
	public DatatablesServerSideDTO(int pagina, Long quantidadeTotal, List<T> dados) {
		this.pagina = pagina;
		this.quantidadeTotal = quantidadeTotal;
		this.dados = dados;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}

	public Long getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Long quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public List<T> getDados() {
		return dados;
	}

	public void setDados(List<T> dados) {
		this.dados = dados;
	}

	@Override
	public boolean equals(Object o) {
		boolean equals;

		if (this == o) {
			equals = true;
		} else if (o == null || getClass() != o.getClass()) {
			equals = false;
		} else {
			DatatablesServerSideDTO that = (DatatablesServerSideDTO) o;

			equals = new EqualsBuilder()
					.append(pagina, that.pagina)
					.append(quantidadeTotal, that.quantidadeTotal)
					.append(dados, that.dados)
					.isEquals();
		}

		return equals;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(pagina)
				.append(quantidadeTotal)
				.append(dados).toHashCode();
	}

	@Override
	public String toString() {
		StandardToStringStyle style = new StandardToStringStyle();
		style.setUseClassName(false);
		style.setUseIdentityHashCode(false);

		return new ToStringBuilder(this, style).appendSuper(super.toString())
				.append("pagina", pagina)
				.append("quantidadeTotal", quantidadeTotal)
				.append("dados", dados).toString();
	}
}
