package br.com.ggas.web.faturamento.cronograma;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Método responsável pela representação de valores de um Cronograma.
 * 
 * @author esantana
 *
 */
public class CronogramaVO {

	private Long chavePrimaria;
	
	private Long idRota;

	private Long idPontoConsumo;
	
	private String codigoRotaDetalhamento;
	
	private Long chaveAtividadeSistema = 0L;
	
	private Long periodicidade;
	
	private String mesAnoFaturamentoInicial;
	
	private String mesAnoFaturamentoFinal;

	private Long grupoFaturamento;
	
	private String habilitado;
	
	private String situacao;
	
	private String chavesPrimariasAtividades;
	
	private String chavesPrimariasMarcadasAtividades;
	
	private String quantidadeCronogramas;
	
	private String mesAnoPartida;
	
	private String cicloPartida;
	
	private String descricaoPeriodicidade;
	
	private String qtdRotas;
	
	private String qtdMedidores;
	
	private Boolean aplicarCronogramaRotasAlteracao;
	
	private String[] listaDatasAtividadesIniciais;
	
	private String[] listaDatasAtividadesFinais;
	
	private Long chaveCronogramaFaturamento;
	
	private String descricaoGrupoFaturamento;
	
	private String anoMesCiclo;
	
	private Long[] chavesPrimariasCronograma;
	
	private Integer[] duracoes;
	
	private String[] datasPrevistas;
	
	private Long[] chavesPrimariasMarcadas;
	
	private String[] datasPrevistasRotas;
	
	private Long[] chavesPrimariasCronogramaRota;
	
	private Integer qtdAtividadesPorCronograma;
	
	private String dataInicialAtividade;
	
	private String dataFinalAtividade;
	
	private String dataInicio;
	
	private String dataFim;
	
	private boolean escalonamentoRealizado;
	
	private Long[] leituristaAtual;
	
	private Long[] leituristaSuplente;
	
	private String descricaoAtividade;
	
	private String[] situacoesPontoConsumo = new String[] {};
	
	private Integer indexLista;
	
	private Integer versao;
	
	private Boolean primeiroCronograma = Boolean.TRUE;
	
	private String qtdeMaximaCronogramasAbertos;
	
	private String descPeriodicidade;
	
	private String cronogramasAbertos;
	
	private String referenciaAtual;
	
	private String mesAnoCiclo;

	private Long ultimaEtapa;
	
	private String dataInicioLeituraSuper;
	
	private String dataFinalLeituraSuper;
	
	
	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public Boolean getPrimeiroCronograma() {
		return primeiroCronograma;
	}

	public void setPrimeiroCronograma(Boolean primeiroCronograma) {
		this.primeiroCronograma = primeiroCronograma;
	}

	public String getQtdeMaximaCronogramasAbertos() {
		return qtdeMaximaCronogramasAbertos;
	}

	public void setQtdeMaximaCronogramasAbertos(String qtdeMaximaCronogramasAbertos) {
		this.qtdeMaximaCronogramasAbertos = qtdeMaximaCronogramasAbertos;
	}

	public String getDescPeriodicidade() {
		return descPeriodicidade;
	}

	public void setDescPeriodicidade(String descPeriodicidade) {
		this.descPeriodicidade = descPeriodicidade;
	}

	public String getCronogramasAbertos() {
		return cronogramasAbertos;
	}

	public void setCronogramasAbertos(String cronogramasAbertos) {
		this.cronogramasAbertos = cronogramasAbertos;
	}

	public Long[] getChavesPrimariasCronograma() {
		Long[] retorno = null;
		if(this.chavesPrimariasCronograma != null) {
			retorno = this.chavesPrimariasCronograma.clone();
		}
		return retorno;
	}

	public void setChavesPrimariasCronograma(Long[] chavesPrimariasCronograma) {
		if(chavesPrimariasCronograma != null) {
			this.chavesPrimariasCronograma = chavesPrimariasCronograma.clone();
		} else {
			this.chavesPrimariasCronograma = null;
		}
		
	}

	public Integer[] getDuracoes() {
		Integer[] retorno = null;
		if(this.duracoes != null) {
			retorno = this.duracoes.clone();
		}
		return retorno;
	}

	public void setDuracoes(Integer[] duracoes) {
		if(duracoes != null) {
			this.duracoes = duracoes.clone();
		} else {
			this.duracoes = null;
		}
		
	}

	public String[] getDatasPrevistas() {
		String[] retorno = null;
		if(this.datasPrevistas != null) {
			retorno = this.datasPrevistas.clone();
		}
		return retorno;
	}

	public void setDatasPrevistas(String[] datasPrevistas) {
		if(datasPrevistas != null) {
			this.datasPrevistas = datasPrevistas.clone();
		} else {
			this.datasPrevistas = null;
		}
		
	}

	public Long[] getChavesPrimariasMarcadas() {
		Long[] retorno = null;
		if(this.chavesPrimariasMarcadas != null) {
			retorno = this.chavesPrimariasMarcadas.clone();
		}
		return retorno;
	}

	public void setChavesPrimariasMarcadas(Long[] chavesPrimariasMarcadas) {
		if(chavesPrimariasMarcadas != null) {
			this.chavesPrimariasMarcadas = chavesPrimariasMarcadas.clone();
		} else {
			this.chavesPrimariasMarcadas = null;
		}
	}

	public String getDescricaoPeriodicidade() {
		return descricaoPeriodicidade;
	}

	public void setDescricaoPeriodicidade(String descricaoPeriodicidade) {
		this.descricaoPeriodicidade = descricaoPeriodicidade;
	}

	public String getQtdRotas() {
		return qtdRotas;
	}

	public void setQtdRotas(String qtdRotas) {
		this.qtdRotas = qtdRotas;
	}

	public String getQtdMedidores() {
		return qtdMedidores;
	}

	public void setQtdMedidores(String qtdMedidores) {
		this.qtdMedidores = qtdMedidores;
	}

	public Boolean getAplicarCronogramaRotasAlteracao() {
		return aplicarCronogramaRotasAlteracao;
	}

	public void setAplicarCronogramaRotasAlteracao(Boolean aplicarCronogramaRotasAlteracao) {
		this.aplicarCronogramaRotasAlteracao = aplicarCronogramaRotasAlteracao;
	}

	public String[] getListaDatasAtividadesIniciais() {
		String[] retorno = null;
		if(this.listaDatasAtividadesIniciais != null) {
			retorno = this.listaDatasAtividadesIniciais.clone();
		}
		return retorno;
	}

	public void setListaDatasAtividadesIniciais(String[] listaDatasAtividadesIniciais) {
		if(listaDatasAtividadesIniciais != null) {
			this.listaDatasAtividadesIniciais = listaDatasAtividadesIniciais.clone();
		} else {
			this.listaDatasAtividadesIniciais = null;
		}
	}

	public String[] getListaDatasAtividadesFinais() {
		String[] retorno = null;
		if(this.listaDatasAtividadesFinais != null) {
			retorno = this.listaDatasAtividadesFinais.clone();
		}
		return retorno;
	}

	public void setListaDatasAtividadesFinais(String[] listaDatasAtividadesFinais) {
		if(listaDatasAtividadesFinais != null) {
			this.listaDatasAtividadesFinais = listaDatasAtividadesFinais.clone();
		} else {
			this.listaDatasAtividadesFinais = null;
		}
		 
	}

	public Long getChaveCronogramaFaturamento() {
		return chaveCronogramaFaturamento;
	}

	public void setChaveCronogramaFaturamento(Long chaveCronogramaFaturamento) {
		this.chaveCronogramaFaturamento = chaveCronogramaFaturamento;
	}

	public String getDescricaoGrupoFaturamento() {
		return descricaoGrupoFaturamento;
	}

	public void setDescricaoGrupoFaturamento(String descricaoGrupoFaturamento) {
		this.descricaoGrupoFaturamento = descricaoGrupoFaturamento;
	}

	public String getAnoMesCiclo() {
		return anoMesCiclo;
	}

	public void setAnoMesCiclo(String anoMesCiclo) {
		this.anoMesCiclo = anoMesCiclo;
	}

	public String getQuantidadeCronogramas() {
		return quantidadeCronogramas;
	}

	public void setQuantidadeCronogramas(String quantidadeCronogramas) {
		this.quantidadeCronogramas = quantidadeCronogramas;
	}

	public String getMesAnoPartida() {
		return mesAnoPartida;
	}

	public void setMesAnoPartida(String mesAnoPartida) {
		this.mesAnoPartida = mesAnoPartida;
	}

	public String getCicloPartida() {
		return cicloPartida;
	}

	public void setCicloPartida(String cicloPartida) {
		this.cicloPartida = cicloPartida;
	}

	public Long getGrupoFaturamento() {
		return grupoFaturamento;
	}

	public void setGrupoFaturamento(Long grupoFaturamento) {
		this.grupoFaturamento = grupoFaturamento;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public String getCodigoRotaDetalhamento() {
		return codigoRotaDetalhamento;
	}

	public void setCodigoRotaDetalhamento(String codigoRotaDetalhamento) {
		this.codigoRotaDetalhamento = codigoRotaDetalhamento;
	}

	public Long getChaveAtividadeSistema() {
		return chaveAtividadeSistema;
	}

	public void setChaveAtividadeSistema(Long chaveAtividadeSistema) {
		this.chaveAtividadeSistema = chaveAtividadeSistema;
	}

	public Long getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(Long periodicidade) {
		this.periodicidade = periodicidade;
	}

	public String getMesAnoFaturamentoInicial() {
		return mesAnoFaturamentoInicial;
	}

	public void setMesAnoFaturamentoInicial(String mesAnoFaturamentoInicial) {
		this.mesAnoFaturamentoInicial = mesAnoFaturamentoInicial;
	}

	public String getMesAnoFaturamentoFinal() {
		return mesAnoFaturamentoFinal;
	}

	public void setMesAnoFaturamentoFinal(String mesAnoFaturamentoFinal) {
		this.mesAnoFaturamentoFinal = mesAnoFaturamentoFinal;
	}

	public String getChavesPrimariasAtividades() {
		return chavesPrimariasAtividades;
	}

	public void setChavesPrimariasAtividades(String chavesPrimariasAtividades) {
		this.chavesPrimariasAtividades = chavesPrimariasAtividades;
	}

	public String getChavesPrimariasMarcadasAtividades() {
		return chavesPrimariasMarcadasAtividades;
	}

	public void setChavesPrimariasMarcadasAtividades(String chavesPrimariasMarcadasAtividades) {
		this.chavesPrimariasMarcadasAtividades = chavesPrimariasMarcadasAtividades;
	}

	public String[] getDatasPrevistasRotas() {
		String[] retorno = null;
		if(this.datasPrevistasRotas != null) {
			retorno = this.datasPrevistasRotas.clone();
		}
		return retorno;
	}

	public void setDatasPrevistasRotas(String[] datasPrevistasRotas) {
		if(datasPrevistasRotas != null) {
			this.datasPrevistasRotas = datasPrevistasRotas.clone();
		} else {
			this.datasPrevistasRotas = null;
		}
	}

	public Long[] getChavesPrimariasCronogramaRota() {
		Long[] retorno = null;
		if(this.chavesPrimariasCronogramaRota != null) {
			retorno = this.chavesPrimariasCronogramaRota.clone();
		}
		return retorno;
	}

	public void setChavesPrimariasCronogramaRota(Long[] chavesPrimariasCronogramaRota) {
		if(chavesPrimariasCronogramaRota != null) {
			this.chavesPrimariasCronogramaRota = chavesPrimariasCronogramaRota.clone();
		} else {
			this.chavesPrimariasCronogramaRota = null;
		}
		
	}

	public Integer getQtdAtividadesPorCronograma() {
		return qtdAtividadesPorCronograma;
	}

	public void setQtdAtividadesPorCronograma(Integer qtdAtividadesPorCronograma) {
		this.qtdAtividadesPorCronograma = qtdAtividadesPorCronograma;
	}

	public String getDataInicialAtividade() {
		return dataInicialAtividade;
	}

	public void setDataInicialAtividade(String dataInicialAtividade) {
		this.dataInicialAtividade = dataInicialAtividade;
	}

	public String getDataFinalAtividade() {
		return dataFinalAtividade;
	}

	public void setDataFinalAtividade(String dataFinalAtividade) {
		this.dataFinalAtividade = dataFinalAtividade;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public boolean isEscalonamentoRealizado() {
		return escalonamentoRealizado;
	}

	public void setEscalonamentoRealizado(boolean escalonamentoRealizado) {
		this.escalonamentoRealizado = escalonamentoRealizado;
	}

	public Long[] getLeituristaAtual() {
		Long[] retorno = null;
		if(this.leituristaAtual != null) {
			retorno = this.leituristaAtual.clone();
		}
		return retorno;
	}

	public void setLeituristaAtual(Long[] leituristaAtual) {
		if(leituristaAtual != null) {
			this.leituristaAtual = leituristaAtual.clone();
		} else {
			this.leituristaAtual = null;
		}
	}

	public Long[] getLeituristaSuplente() {
		Long[] retorno = null;
		if(this.leituristaSuplente != null) {
			retorno = this.leituristaSuplente.clone();
		}
		return retorno;
	}

	public void setLeituristaSuplente(Long[] leituristaSuplente) {
		if(leituristaSuplente != null) {
			this.leituristaSuplente = leituristaSuplente.clone();
		} else {
			this.leituristaSuplente = null;
		}
	}

	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}

	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}

	public String[] getSituacoesPontoConsumo() {
		String[] retorno = null;
		if(this.situacoesPontoConsumo != null) {
			retorno = this.situacoesPontoConsumo.clone();
		}
		return retorno;
	}

	public void setSituacoesPontoConsumo(String[] situacoesPontoConsumo) {
		if(situacoesPontoConsumo != null) {
			this.situacoesPontoConsumo = situacoesPontoConsumo.clone();
		} else {
			this.situacoesPontoConsumo = null;
		}
		
	}

	public Integer getIndexLista() {
		return indexLista;
	}

	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}

	public String getReferenciaAtual() {
		return referenciaAtual;
	}

	public void setReferenciaAtual(String referenciaAtual) {
		this.referenciaAtual = referenciaAtual;
	}

	public String getMesAnoCiclo() {
		return mesAnoCiclo;
	}

	public void setMesAnoCiclo(String mesAnoCiclo) {
		this.mesAnoCiclo = mesAnoCiclo;
	}

	/**
	 * Obtém a chave primaria da ultima etapa executada
	 * @return retorna a chave primaria
	 */
	public Long getUltimaEtapa() {
		return ultimaEtapa;
	}

	/**
	 * Configura a chave primária da ultima etapa executada
	 * @param ultimaEtapa chave primária da última etapa
	 */
	public void setUltimaEtapa(Long ultimaEtapa) {
		this.ultimaEtapa = ultimaEtapa;
	}

	/**
	 * Obtém a data de início da leitura
	 * @return a data início
	 */
	public String getDataInicioLeituraSuper() {
		return dataInicioLeituraSuper;
	}

	/**
	 * Configura a data de início da leitura
	 * @param data em formato Date
	 */
	public void setDataInicioLeituraSuper(Date pDataInicioLeituraSuper) {
		if(pDataInicioLeituraSuper != null) {
			SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
			dataInicioLeituraSuper = data.format(pDataInicioLeituraSuper);
		}else {
			dataInicioLeituraSuper = String.valueOf("");
		}
		
	}

	/**
	 * Obtém a data final da leitura
	 * @return a data final
	 */
	public String getDataFinalLeituraSuper() {
		return dataFinalLeituraSuper;
	}

	/**
	 * Configura a data final da leitura
	 * @param data em formato Date
	 */
	public void setDataFinalLeituraSuper(Date pDataFinalLeituraSuper) {
		if(pDataFinalLeituraSuper != null) {
			SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
			dataFinalLeituraSuper = data.format(pDataFinalLeituraSuper);
		}else {
			dataFinalLeituraSuper = String.valueOf("");
		}
		
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
}
