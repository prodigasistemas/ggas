/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Strings;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoAcompanhamentoCronogramaVO;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.impl.CronogramaRotaVO;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.RotaHistorico;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 * Classe responsável pelas operações referentes ao Cronograma de Faturamento.
 *
 */
@Controller
public class CronogramaAction extends GenericAction {

	private static final String FORWARD_EXIBIR_INCLUSAO_CRONOGRAMA = "forward:/exibirInclusaoCronograma";

	private static final String VIEW_EXIBIR_ESCALONAMENTO_LEITURISTA = "exibirEscalonamentoLeiturista";

	private static final String VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA = "exibirInclusaoResumoCronograma";

	private static final String FORWARD_PESQUISAR_CRONOGRAMA = "forward:/pesquisarCronograma";

	private static final String CRONOGRAMA_VO = "cronogramaVO";

	private static final String PREVISTA = "Prevista";

	private static final String LISTA_LEITURISTAS = "listaLeituristas";

	private static final String PRIMEIRO_ESCALONAMENTO = "primeiroEscalonamento";

	private static final String CHAVE_CRONOGRAMA = "chaveCronograma";

	private static final String TIPO_ATIVIDADE = "tipoAtividade";

	private static final String NAO_ESTA_PRONTO_PARA_PROCESSAR = "NAO_ESTA_PRONTO_PARA_PROCESSAR";

	private static final String PROCESSADO_COM_ANORMALIDADE = "PROCESSADO_COM_ANORMALIDADE";

	private static final String PROCESSADO_COM_ANORMALIDADE_BLOQUEIO = "PROCESSADO_COM_ANORMALIDADE_BLOQUEIO";

	private static final String PRONTO_PROCESSAR = "PRONTO_PROCESSAR";

	private static final String PROCESSADO_SEM_ANORMALIDADE = "PROCESSADO_SEM_ANORMALIDADE";

	private static final String PROCESSADO_ANORMALIDADE_ANALISADA = "PROCESSADO_ANORMALIDADE_ANALISADA";

	private static final String CICLO = "ciclo";

	private static final String LISTA_DATAS_ATIVIDADES_FINAIS = "listaDatasAtividadesFinais";

	private static final String LISTA_DATAS_ATIVIDADES_INICIAIS = "listaDatasAtividadesIniciais";

	private static final String LISTA_LEITURISTA = "listaLeiturista";

	private static final String LISTA_CRONOGRAMA_FATURAMENTO_VO = "listaCronogramaFaturamentoVO";

	private static final String QTD_MEDIDORES = "qtdMedidores";

	private static final String QTD_ROTAS = "qtdRotas";

	private static final String DESCRICAO_GRUPO_FATU = "descricaoGrupoFatu";

	private static final String DESCRICAO_PERIODICIDADE = "descricaoPeriodicidade";

	private static final String CHAVE_ROTA = "chaveRota";

	private static final String ANO_MES_CICLO = "anoMesCiclo";

	private static final String LISTA_ATIVIDADE_FATURAMENTOS = "listaAtividadeFaturamentos";

	private static final String LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO = "listaCronogramaAtividadeSistemaVO";

	private static final String DATAS_PREVISTAS = "datasPrevistas";

	private static final String MES_ANO_PARTIDA = "mesAnoPartida";

	private static final String LISTA_DETALHAMENTO_ROTA = "listaDetalhamentoRota";

	private static final String CICLO_PARTIDA = "cicloPartida";

	private static final String QUANTIDADE_CRONOGRAMAS = "quantidadeCronogramas";

	private static final String CHAVES_PRIMARIAS_MARCADAS = "chavesPrimariasMarcadas";

	private static final String DURACOES = "duracoes";

	private static final String HABILITADO = "habilitado";

	private static final String GRUPO_FATURAMENTO = "grupoFaturamento";

	private static final String CHAVES_PRIMARIAS_CRONOGRAMA = "chavesPrimariasCronograma";

	private static final String PERIODICIDADE = "periodicidade";

	private static final String MES_ANO_FATURAMENTO_INICIAL = "mesAnoFaturamentoInicial";

	private static final String MES_ANO_FATURAMENTO_FINAL = "mesAnoFaturamentoFinal";

	private static final String DATA_INICIO = "dataInicio";

	private static final String CRONOGRAMA_ATIVIDADE_SISTEMA = "cronogramaAtividadeSistema";

	private static final String LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO = "listaCronogramaAtividadeRotaVO";

	private static final String DATAS_PREVISTAS_ROTAS = "datasPrevistasRotas";

	private static final String LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO = "listaCronogramaAtividadeFaturamentoVO";

	private static final String CHAVES_PRIMARIAS_CRONOGRAMA_ROTA = "chavesPrimariasCronogramaRota";

	private static final String CHAVE_CRONOGRAMA_FATURAMENTO = "chaveCronogramaFaturamento";

	private static final String SITUACAO = "situacao";

	private static final Long CHAVE_REALIZAR_LEITURA = 2L;

	// Informando os tipos de situação possíveis para Pontos de consumo
	private static final String[] situacoesPontoConsumo = { PROCESSADO_COM_ANORMALIDADE, PROCESSADO_SEM_ANORMALIDADE,
			PRONTO_PROCESSAR, NAO_ESTA_PRONTO_PARA_PROCESSAR };

	private static final String LISTA_CRONOGRAMA = "listaCronograma";

	private static final String EXIBIR_PESQUISA_CRONOGRAMA = "exibirPesquisaCronograma";

	@Autowired
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorLeiturista controladorLeiturista;

	@Autowired
	private CalculadoraPontoConsumoCronograma calculadoraPontoConsumoCronograma;

	@Autowired
	private ControladorCronogramaAtividadeFaturamento controladorCronogramaAtividadeFaturamento;

	/**
	 * Método responsável por exibir a tela de pesquisa de Cronograma.
	 * 
	 * @param model   - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirPesquisaCronograma")
	public String exibirPesquisaCronograma(Model model, HttpServletRequest request) {

		try {
			request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);
			this.carregarCampos(model);

			if (!model.containsAttribute(CRONOGRAMA_VO)) {
				CronogramaVO cronogramaVO = new CronogramaVO();

				cronogramaVO.setHabilitado(String.valueOf(true));
				model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
			}

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return "exibirPesquisaCronograma";

	}

	/**
	 * Exibir pesquisa acompanhamento cronograma.
	 * 
	 * @param model   - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisaAcompanhamentoCronograma")
	public String exibirPesquisaAcompanhamentoCronograma(Model model, HttpServletRequest request) {

		return this.exibirPesquisaCronograma(model, request);
	}

	/**
	 * Método responsável por excluir uma Rota.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param chavePrimaria - {@link Long}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("removerCronograma")
	public String removerCronograma(Model model, HttpServletRequest request,
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		try {
			if (chavePrimaria != null && chavePrimaria > 0) {
				controladorCronogramaFaturamento.removerCronogramaFaturamento(chavePrimaria,
						getDadosAuditoria(request));
			}

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request,
					CronogramaFaturamento.ROTULO_CRONOGRAMA_FATURAMENTO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return FORWARD_PESQUISAR_CRONOGRAMA;
	}

	/**
	 * Desfazer cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 *
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping("desfazerCronograma")
	public String desfazerCronograma(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, Model model,
			HttpServletRequest request, CronogramaVO cronogramaVO, BindingResult bindingResult) throws GGASException {

		Long chavePrimaria = cronogramaVO.getChavePrimaria();
		String codigoRota = cronogramaVO.getCodigoRotaDetalhamento();
		Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();

		CronogramaFaturamento cronogramaFaturamento = controladorCronogramaFaturamento
				.obterCronogramaFaturamento(chavePrimaria);

		for (Long idRota : chavesPrimarias) {

			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("numeroCiclo", cronogramaFaturamento.getNumeroCiclo());
			parametros.put("anoMesFaturamento", cronogramaFaturamento.getAnoMesFaturamento());
			parametros.put(CHAVE_ROTA, idRota);
			parametros.put(CHAVE_CRONOGRAMA_FATURAMENTO, chavePrimaria);

			if (chaveAtividadeSistema == AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA) {

				ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

				Collection<Fatura> listaFatura = controladorFatura.consultarFaturasPorRota(parametros);

				if (listaFatura.isEmpty()) {
					controladorCronogramaFaturamento.desfazerConsistirLeitura(parametros);
					super.mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
				} else {
					super.mensagemErro(model, Constantes.ERRO_DESFAZER_CONSISTIR_LEITURA, "");
				}
			} else if (chaveAtividadeSistema == AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA) {
				Collection<HistoricoConsumo> listaHistoricoConsumo = controladorCronogramaFaturamento
						.consultarDadosConsistirLeitura(parametros);
				if (listaHistoricoConsumo.isEmpty()) {
					controladorCronogramaFaturamento.desfazerRegistrarLeitura(parametros);
					super.mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);

				} else {
					super.mensagemErro(model, Constantes.ERRO_DESFAZER_REGISTRAR_LEITURA, "");
				}
			} else if (chaveAtividadeSistema == AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA) {
				ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
				Collection<LeituraMovimento> listaLeituraMovimento = controladorLeituraMovimento
						.consultarMovimentosLeituraNaoProcessadosPorRota(parametros);
				if (!listaLeituraMovimento.isEmpty()) {
					controladorCronogramaFaturamento.desfazerGerarDados(parametros);
					super.mensagemSucesso(model, Constantes.MENSAGEM_OPERACAO_REALIZADA_COM_SUCESSO);
				} else {
					super.mensagemErro(model, Constantes.ERRO_DESFAZER_GERAR_DADOS_LEITURA, "");
				}

				ControladorCronogramaAtividadeFaturamento controladorCronograma = (ControladorCronogramaAtividadeFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(
								ControladorCronogramaAtividadeFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
				ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

				Rota rota = controladorRota.obterPorId(idRota);
				controladorCronograma.desfazerAtividadeCronograma(AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA,
						rota);
			}
		}

		super.mensagemSucesso(model, Constantes.SUCESSO_DESFAZER_CRONOGRAMA);

		if (codigoRota != null && !codigoRota.isEmpty()) {
			return exibirDetalhamentoRotaCronogramaAcompanhamento(model, request, cronogramaVO, bindingResult);
		} else {
			return exibirDetalhamentoCronogramaAcompanhamento(model, request, cronogramaVO, bindingResult);
		}

	}

	/**
	 * Pesquisar cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("pesquisarCronograma")
	public String pesquisarCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {

			cronogramaVO.setChavePrimaria(null);

			Map<String, Object> filtro = prepararFiltro(cronogramaVO);

			controladorCronogramaFaturamento.validarFiltroPesquisaCronogramaFaturamento(filtro);

			Collection<CronogramaFaturamento> listaCronogramaFaturamentos = controladorCronogramaFaturamento
					.consultarCronogramaFaturamento(filtro);

			for (CronogramaFaturamento cronogramaFaturamento : listaCronogramaFaturamentos) {
				CronogramaFaturamento cronogramaAnterior = controladorCronogramaFaturamento
						.obterCronogramaFaturamentoAnterior(cronogramaFaturamento);
				String periodoLeitura = "";
				Date dataFim = DataUtil.decrementarDia(cronogramaFaturamento.getRegistrarLeitura().getDataFim(), 1);
				if(cronogramaAnterior != null) {
					periodoLeitura = cronogramaAnterior.getRegistrarLeitura().getdataFimFormatada() + " - " + DataUtil.converterDataParaString(dataFim);
				} else {
					Date dataInicial = DataUtil.gerarDataSemHoraPrimeiroDiaMes(DataUtil.decrementarDia(dataFim, 1));
					periodoLeitura = DataUtil.converterDataParaString(dataInicial) + " - " + DataUtil.converterDataParaString(dataFim);
				}
				
				cronogramaFaturamento.setPeriodoLeitura(periodoLeitura);
			}

			if (!isPostBack(request) && listaCronogramaFaturamentos != null
					&& listaCronogramaFaturamentos.size() == 1) {
				cronogramaVO.setChavePrimaria(listaCronogramaFaturamentos.iterator().next().getChavePrimaria());
			}

			model.addAttribute("listaCronograma", listaCronogramaFaturamentos);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirPesquisaCronograma(model, request);
	}

	/**
	 * Pesquisar acompanhamento cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("pesquisarAcompanhamentoCronograma")
	public String pesquisarAcompanhamentoCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {
			cronogramaVO.setChavePrimaria(null);

			Map<String, Object> filtro = prepararFiltro(cronogramaVO);

			controladorCronogramaFaturamento.validarFiltroPesquisaCronogramaFaturamento(filtro);

			Collection<CronogramaFaturamento> listaCronogramaFaturamentos = controladorCronogramaFaturamento
					.consultarCronogramaFaturamento(filtro);

			if (!isPostBack(request) && listaCronogramaFaturamentos != null
					&& listaCronogramaFaturamentos.size() == 1) {
				cronogramaVO.setChavePrimaria(listaCronogramaFaturamentos.iterator().next().getChavePrimaria());
			}

			request.setAttribute("listaCronograma", listaCronogramaFaturamentos);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaAcompanhamentoCronograma(model, request);
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirInclusaoCronograma")
	public String exibirInclusaoCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = "exibirInclusaoCronograma";
		try {
			request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);
			request.getSession().removeAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);
			request.getSession().removeAttribute(ANO_MES_CICLO);

			Collection<GrupoFaturamento> listaGrupoFaturamentos = controladorRota.listarGruposFaturamentoRotas();
			request.setAttribute("listaGrupoFaturamentos", listaGrupoFaturamentos);

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaSessao = (Collection<CronogramaAtividadeFaturamentoVO>) request
					.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);

			if (listaCronogramaSessao != null) {
				request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO, listaCronogramaSessao);
				if (request.getSession().getAttribute(GRUPO_FATURAMENTO) != null) {
					GrupoFaturamento grupoFaturamento = (GrupoFaturamento) request.getSession()
							.getAttribute(GRUPO_FATURAMENTO);
					cronogramaVO.setGrupoFaturamento(grupoFaturamento.getChavePrimaria());
					cronogramaVO.setQuantidadeCronogramas(
							String.valueOf(Util.coalescenciaNula(request.getParameter(QUANTIDADE_CRONOGRAMAS),
									request.getSession().getAttribute(QUANTIDADE_CRONOGRAMAS))));
					cronogramaVO.setMesAnoPartida(String.valueOf(request.getSession().getAttribute(MES_ANO_PARTIDA)));
					cronogramaVO.setCicloPartida(String.valueOf(request.getSession().getAttribute(CICLO_PARTIDA)));
				}
			} else {

				Collection<AtividadeSistema> listaAtividadeSistemas = controladorCronogramaFaturamento
						.listarAtividadeSistemaPorCronograma();

				Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = this
						.montarListaCronogramaAtividadeFaturamentoVO(listaAtividadeSistemas);

				request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO,
						listaCronogramaAtividadeFaturamentoVO);

				popularChavesPrimariasDeAtividades(cronogramaVO, listaAtividadeSistemas);
			}

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = "forward:/exibirPesquisaCronograma";
		}

		return view;
	}

	private void popularChavesPrimariasDeAtividades(CronogramaVO cronogramaVO,
			Collection<AtividadeSistema> listaAtividadeSistemas) {
		StringBuilder chavesPrimariasAtividades = new StringBuilder();
		StringBuilder chavesPrimariasMarcadasAtividades = new StringBuilder();

		for (AtividadeSistema atividadeSistema : listaAtividadeSistemas) {

			chavesPrimariasAtividades.append(atividadeSistema.getChavePrimaria());
			chavesPrimariasAtividades.append(", ");

			if (atividadeSistema.isObrigatoria()) {
				chavesPrimariasMarcadasAtividades.append(atividadeSistema.getChavePrimaria());
				chavesPrimariasMarcadasAtividades.append(", ");
			} else {
				chavesPrimariasMarcadasAtividades.append(", ");
			}
		}

		cronogramaVO.setChavesPrimariasAtividades(chavesPrimariasAtividades.toString());
		cronogramaVO.setChavesPrimariasMarcadasAtividades(chavesPrimariasMarcadasAtividades.toString());
	}

	/**
	 * Montar lista cronograma atividade faturamento vo.
	 * 
	 * @param listaAtividadeSistemas - {@link Collection}
	 * @return coleção de atividades do sistema
	 */
	private Collection<CronogramaAtividadeFaturamentoVO> montarListaCronogramaAtividadeFaturamentoVO(
			Collection<AtividadeSistema> listaAtividadeSistemas) {

		Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = new ArrayList<CronogramaAtividadeFaturamentoVO>();

		for (AtividadeSistema atividadeSistema : listaAtividadeSistemas) {
			CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO = new CronogramaAtividadeFaturamentoVO();
			cronogramaAtividadeFaturamentoVO.setAtividadeSistema(atividadeSistema);
			listaCronogramaAtividadeFaturamentoVO.add(cronogramaAtividadeFaturamentoVO);
		}

		return listaCronogramaAtividadeFaturamentoVO;
	}

	/**
	 * Método responsável por exibir a tela de alteração de Cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirAlteracaoCronograma")
	public String exibirAlteracaoCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = "exibirAlteracaoCronograma";
		try {
			this.exibirAlteracaoCronograma(model, request, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_CRONOGRAMA;
		}

		return view;
	}

	@SuppressWarnings("unchecked")
	private void exibirAlteracaoCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO)
			throws GGASException {
		Long chavePrimaria = cronogramaVO.getChavePrimaria();
		Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();
		Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO;
		Collection<AtividadeSistema> listaAtividadeSistemas = controladorCronogramaFaturamento
				.listarAtividadeSistemaPorCronograma();
		Long idGrupoFaturamento = cronogramaVO.getGrupoFaturamento();
		CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVOAux = null;

		CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = (CronogramaAtividadeSistemaVO) request.getSession()
				.getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

		controladorCronogramaFaturamento.validarCronogramaRealizado(chavePrimaria);

		Collection<CronogramaAtividadeFaturamento> listaAtividadeFaturamentos = controladorCronogramaFaturamento
				.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(chavePrimaria);
		listaCronogramaAtividadeFaturamentoVO = (Collection<CronogramaAtividadeFaturamentoVO>) request.getSession()
				.getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);

		CronogramaFaturamento cronogramaFaturamento = controladorCronogramaFaturamento
				.obterCronogramaFaturamento(chavePrimaria);

		String mesAno = Util.converterAnoMesEmMesAno(String.valueOf(cronogramaFaturamento.getAnoMesFaturamento()));
		cronogramaVO.setMesAnoPartida(mesAno);
		cronogramaVO.setCicloPartida(String.valueOf(cronogramaFaturamento.getNumeroCiclo()));

		listaAtividadeFaturamentos = controladorCronogramaFaturamento
				.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(chavePrimaria);

		StringBuilder datasIniciais = new StringBuilder();
		StringBuilder datasFinais = new StringBuilder();

		String[] arrayDatasIniciais = new String[1];
		String[] arrayDatasFinais = new String[1];

		Map<Long, Object> mapaCronogramaAtividadeFaturamento = new HashMap<Long, Object>();
		idGrupoFaturamento = popularMapaCronogramaAtividadeFaturamento(idGrupoFaturamento, listaAtividadeFaturamentos,
				mapaCronogramaAtividadeFaturamento);

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
		Periodicidade periodicidade = controladorRota.obterPeriodicidadePorGrupoFaturamento(idGrupoFaturamento);

		cronogramaVO.setDescricaoPeriodicidade(periodicidade.getDescricao());
		cronogramaVO
				.setQtdRotas(String.valueOf(controladorRota.quantidadeRotasPorGrupoFaturamento(idGrupoFaturamento)));
		cronogramaVO.setQtdMedidores(
				String.valueOf(controladorPontoConsumo.quantidadeMedidoresPorGrupoFaturamento(idGrupoFaturamento)));
		Boolean aplicarCronogramaRotasAlteracao = cronogramaVO.getAplicarCronogramaRotasAlteracao();

		Collection<CronogramaAtividadeFaturamento> listaAtividadesCronograma = null;
		listaAtividadesCronograma = controladorCronogramaFaturamento
				.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(chavePrimaria);

		request.setAttribute(LISTA_ATIVIDADE_FATURAMENTOS, listaAtividadesCronograma);

		listaCronogramaAtividadeFaturamentoVO = new ArrayList<CronogramaAtividadeFaturamentoVO>();

		for (AtividadeSistema atividadeSistema : listaAtividadeSistemas) {
			CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO = new CronogramaAtividadeFaturamentoVO();
			cronogramaAtividadeFaturamentoVO.setAtividadeSistema(atividadeSistema);
			if (mapaCronogramaAtividadeFaturamento.containsKey(atividadeSistema.getChavePrimaria())) {

				cronogramaAtividadeFaturamentoVO.setIndicadorMarcado(true);
				CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) mapaCronogramaAtividadeFaturamento
						.get(atividadeSistema.getChavePrimaria());

				cronogramaAtividadeFaturamentoVO.setChavePrimaria(cronogramaAtividadeFaturamento.getChavePrimaria());
				cronogramaAtividadeFaturamentoVO.setVersao(cronogramaAtividadeFaturamento.getVersao());

				cronogramaAtividadeFaturamentoVO.setDataPrevista(cronogramaAtividadeFaturamento.getDataFim());

				DateTime dataPrevistaAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());

				// definicao da data inicial
				DateTime dataInicial = dataPrevistaAux;
				if (periodicidade.getNumeroMinimoDiasCiclo() != null) {

					int minimoDias = periodicidade.getQuantidadeDias() - periodicidade.getNumeroMinimoDiasCiclo();
					dataInicial = dataPrevistaAux.minusDays(minimoDias);

				}
				dataInicial = controladorCronogramaFaturamento.verificarDiaUtil(dataInicial, null);
				cronogramaAtividadeFaturamentoVO.setDataInicialAtividade(
						Util.converterDataParaStringSemHora(dataInicial.toDate(), Constantes.FORMATO_DATA_BR));

				// definicao da data final
				DateTime dataFinal = dataPrevistaAux;
				if (periodicidade.getNumeroMaximoDiasCiclo() != null) {

					int maximoDias = periodicidade.getNumeroMaximoDiasCiclo() - periodicidade.getQuantidadeDias();
					dataFinal = dataPrevistaAux.plusDays(maximoDias);

				}
				dataFinal = controladorCronogramaFaturamento.verificarDiaUtil(dataFinal, null);
				cronogramaAtividadeFaturamentoVO.setDataFinalAtividade(
						Util.converterDataParaStringSemHora(dataFinal.toDate(), Constantes.FORMATO_DATA_BR));

				datasIniciais
						.append(Util.converterDataParaStringSemHora(dataInicial.toDate(), Constantes.FORMATO_DATA_BR))
						.append(",");
				datasFinais.append(Util.converterDataParaStringSemHora(dataFinal.toDate(), Constantes.FORMATO_DATA_BR))
						.append(",");

				cronogramaAtividadeFaturamentoVO
						.setDuracao(Util.intervaloDatas(cronogramaAtividadeFaturamento.getDataInicio(),
								cronogramaAtividadeFaturamento.getDataFim()) + 1);

				if (chaveAtividadeSistema != null && atividadeSistema.getChavePrimaria() == chaveAtividadeSistema) {
					cronogramaAtividadeFaturamentoVOAux = cronogramaAtividadeFaturamentoVO;
				}

			} else {

				List<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamentos = controladorCronogramaFaturamento
						.consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(cronogramaFaturamento);

				CronogramaAtividadeFaturamento cronogramaAtividadeFaturamentoCicloAnterior = listaCronogramaAtividadeFaturamentos
						.get(listaCronogramaAtividadeFaturamentos.size() - 1);

				DateTime dataUltimaAtividadeCicloAnterior = null;

				dataUltimaAtividadeCicloAnterior = new DateTime(
						cronogramaAtividadeFaturamentoCicloAnterior.getDataFim());

				if (cronogramaAtividadeFaturamentoCicloAnterior.getDataRealizacao() != null) {

					dataUltimaAtividadeCicloAnterior = new DateTime(
							cronogramaAtividadeFaturamentoCicloAnterior.getDataRealizacao());
				}

				Map<String, Object> mapaDatas = controladorCronogramaFaturamento.obterDatasSugestaoCronograma(
						dataUltimaAtividadeCicloAnterior, periodicidade, null, null, null, Boolean.TRUE,
						grupoFaturamento.getTipoLeitura(), grupoFaturamento.getNumeroCiclo(), null);

				cronogramaAtividadeFaturamentoVO.setIndicadorMarcado(false);

				cronogramaAtividadeFaturamentoVO.setDataInicialAtividade(Util.converterDataParaStringSemHora(
						(Date) mapaDatas.get("dataInicial"), Constantes.FORMATO_DATA_BR));
				cronogramaAtividadeFaturamentoVO.setDataFinalAtividade(Util
						.converterDataParaStringSemHora((Date) mapaDatas.get("dataFinal"), Constantes.FORMATO_DATA_BR));

				datasIniciais.append(Util.converterDataParaStringSemHora((Date) mapaDatas.get("dataInicial"),
						Constantes.FORMATO_DATA_BR)).append(",");
				datasFinais.append(Util.converterDataParaStringSemHora((Date) mapaDatas.get("dataPrevista"),
						Constantes.FORMATO_DATA_BR)).append(",");
			}

			atualizarListaCronogramaAtividadeRotaVO(request, chavePrimaria, chaveAtividadeSistema,
					cronogramaAtividadeFaturamentoVOAux, aplicarCronogramaRotasAlteracao,
					cronogramaAtividadeFaturamentoVO);

			listaCronogramaAtividadeFaturamentoVO.add(cronogramaAtividadeFaturamentoVO);
		}

		arrayDatasIniciais[0] = datasIniciais.toString();
		arrayDatasFinais[0] = datasFinais.toString();

		cronogramaVO.setListaDatasAtividadesIniciais(arrayDatasIniciais);
		cronogramaVO.setListaDatasAtividadesFinais(arrayDatasFinais);

		request.getSession().setAttribute(LISTA_DATAS_ATIVIDADES_INICIAIS, arrayDatasIniciais);
		request.getSession().setAttribute(LISTA_DATAS_ATIVIDADES_FINAIS, arrayDatasFinais);

		cronogramaVO.setChaveCronogramaFaturamento(cronogramaFaturamento.getChavePrimaria());
		cronogramaVO.setDescricaoGrupoFaturamento(cronogramaFaturamento.getGrupoFaturamento().getDescricao());
		cronogramaVO.setAnoMesCiclo(
				cronogramaFaturamento.getAnoMesFaturamentoMascara() + "-" + cronogramaFaturamento.getNumeroCiclo());
		cronogramaVO.setGrupoFaturamento(cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria());

		if (cronogramaAtividadeSistemaVO == null) {

			cronogramaAtividadeSistemaVO = new CronogramaAtividadeSistemaVO();
		}

		if (aplicarCronogramaRotasAlteracao == null) {

			cronogramaAtividadeSistemaVO
					.setListaCronogramaAtividadeFaturamentoVO(listaCronogramaAtividadeFaturamentoVO);
			request.getSession().setAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA, cronogramaAtividadeSistemaVO);
		}

		request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO,
				listaCronogramaAtividadeFaturamentoVO);

		model.addAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO, listaCronogramaAtividadeFaturamentoVO);

		cronogramaVO.setAplicarCronogramaRotasAlteracao(null);

		model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
	}

	private Long popularMapaCronogramaAtividadeFaturamento(Long idGrupoFaturamento,
			Collection<CronogramaAtividadeFaturamento> listaAtividadeFaturamentos,
			Map<Long, Object> mapaCronogramaAtividadeFaturamento) {
		for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaAtividadeFaturamentos) {

			if (idGrupoFaturamento == null || idGrupoFaturamento == -1) {

				idGrupoFaturamento = cronogramaAtividadeFaturamento.getCronogramaFaturamento().getGrupoFaturamento()
						.getChavePrimaria();
			}
			mapaCronogramaAtividadeFaturamento.put(
					cronogramaAtividadeFaturamento.getAtividadeSistema().getChavePrimaria(),
					cronogramaAtividadeFaturamento);

		}
		return idGrupoFaturamento;
	}

	@SuppressWarnings("unchecked")
	private void atualizarListaCronogramaAtividadeRotaVO(HttpServletRequest request, Long chavePrimaria,
			Long chaveAtividadeSistema, CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVOAux,
			Boolean aplicarCronogramaRotasAlteracao, CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO)
			throws GGASException {
		Collection<CronogramaRota> listaCronogramaRotas;
		if (chaveAtividadeSistema != null) {

			Collection<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = null;

			if (aplicarCronogramaRotasAlteracao == null) {

				listaCronogramaRotas = controladorRota.listarCronogramasRotasPorAtividade(chaveAtividadeSistema,
						chavePrimaria);

				if (cronogramaAtividadeFaturamentoVO.getAtividadeSistema()
						.getChavePrimaria() == chaveAtividadeSistema) {

					cronogramaAtividadeFaturamentoVO.setListaCronogramaRota(listaCronogramaRotas);
				}

				listaCronogramaAtividadeRotaVO = this.montarListaCronogramaRotasVO(listaCronogramaRotas,
						chaveAtividadeSistema, cronogramaAtividadeFaturamentoVOAux);
			} else {

				listaCronogramaAtividadeRotaVO = (Collection<CronogramaAtividadeRotaVO>) request.getSession()
						.getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO);

			}

			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO, listaCronogramaAtividadeRotaVO);
			request.setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO, listaCronogramaAtividadeRotaVO);
		}
	}

	/**
	 * Método responsável por alterar um cronograma de faturamento.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("alterarCronograma")
	public String alterarCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = "forward:/exibirAlteracaoCronograma";

		try {
			Long chavePrimaria = cronogramaVO.getChavePrimaria();

			Long[] chavesPrimarias = cronogramaVO.getChavesPrimariasCronograma();
			Integer[] duracoes = cronogramaVO.getDuracoes();
			String[] datasPrevistas = cronogramaVO.getDatasPrevistas();

			String[] listaDatasAtividadesIniciais = (String[]) request.getSession()
					.getAttribute(LISTA_DATAS_ATIVIDADES_INICIAIS);
			String[] listaDatasAtividadesFinais = (String[]) request.getSession()
					.getAttribute(LISTA_DATAS_ATIVIDADES_FINAIS);

			String[] listaDatasAtividadesIniciaisAux = listaDatasAtividadesIniciais[0].split(",");
			String[] listaDatasAtividadesFinaisAux = listaDatasAtividadesFinais[0].split(",");

			Long[] chavesPrimariasMarcadas = cronogramaVO.getChavesPrimariasMarcadas();
			Long idGrupoFaturamento = cronogramaVO.getGrupoFaturamento();
			String mesAnoPartida = cronogramaVO.getMesAnoPartida();
			String cicloPartida = cronogramaVO.getCicloPartida();

			controladorCronogramaFaturamento.validarListaCamposObrigatoriosGerarCronogramas(chavesPrimarias,
					chavesPrimariasMarcadas, duracoes, datasPrevistas);

			Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = controladorCronogramaFaturamento
					.gerarListaCronogramaAtividade(chavesPrimarias, duracoes, datasPrevistas, chavesPrimariasMarcadas,
							mesAnoPartida, cicloPartida, 1, idGrupoFaturamento, listaDatasAtividadesIniciaisAux,
							listaDatasAtividadesFinaisAux, Boolean.TRUE);

			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVOSessao = (CronogramaAtividadeSistemaVO) request
					.getSession().getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = listaCronogramaAtividadeSistemaVO.iterator()
					.next();

			popularListaCronogramaAtividadeFaturamento(cronogramaAtividadeSistemaVOSessao,
					cronogramaAtividadeSistemaVO);

			CronogramaFaturamento cronogramaFaturamento = controladorCronogramaFaturamento
					.obterCronogramaFaturamento(chavePrimaria);

			cronogramaAtividadeSistemaVO.setCronogramaFaturamento(cronogramaFaturamento);

			controladorCronogramaFaturamento.atualizarCronogramaFaturamento(cronogramaAtividadeSistemaVO,
					getDadosAuditoria(request));

			request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA,
					super.obterMensagem(CronogramaFaturamento.ROTULO_CRONOGRAMA_FATURAMENTO));

			cronogramaVO.setHabilitado(String.valueOf(Boolean.TRUE));

			view = this.pesquisarCronograma(model, request, cronogramaVO, bindingResult);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private void popularListaCronogramaAtividadeFaturamento(
			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVOSessao,
			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO) {
		for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : cronogramaAtividadeSistemaVO
				.getListaCronogramaAtividadeFaturamentoVO()) {
			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVOSessao : cronogramaAtividadeSistemaVOSessao
					.getListaCronogramaAtividadeFaturamentoVO()) {
				if (cronogramaAtividadeFaturamentoVO.getAtividadeSistema()
						.getChavePrimaria() == cronogramaAtividadeFaturamentoVOSessao.getAtividadeSistema()
								.getChavePrimaria()) {

					cronogramaAtividadeFaturamentoVO
							.setChavePrimaria(cronogramaAtividadeFaturamentoVOSessao.getChavePrimaria());
					cronogramaAtividadeFaturamentoVO.setVersao(cronogramaAtividadeFaturamentoVOSessao.getVersao());

					cronogramaAtividadeFaturamentoVO
							.setListaCronogramaRota(cronogramaAtividadeFaturamentoVOSessao.getListaCronogramaRota());
				}
			}
		}
	}

	/**
	 * Aplicar cronograma rotas alteracao.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("aplicarCronogramaRotasAlteracao")
	public String aplicarCronogramaRotasAlteracao(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {
			Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();
			popularMapa(cronogramaVO);
			String[] listaDatasPrevistasRotas = cronogramaVO.getDatasPrevistasRotas();
			Long[] listaChavesPrimariasCronogramaRota = cronogramaVO.getChavesPrimariasCronogramaRota();

			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = (CronogramaAtividadeSistemaVO) request
					.getSession().getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = (Collection<CronogramaAtividadeFaturamentoVO>) request
					.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);

			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
				if (chaveAtividadeSistema != null && cronogramaAtividadeFaturamentoVO.getAtividadeSistema()
						.getChavePrimaria() == chaveAtividadeSistema) {
					this.popularDataPrevistaCronogramaRota(cronogramaAtividadeFaturamentoVO.getListaCronogramaRota(),
							listaDatasPrevistasRotas, listaChavesPrimariasCronogramaRota);

					Collection<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = this
							.montarListaCronogramaRotasVOInsercao(
									cronogramaAtividadeFaturamentoVO.getListaCronogramaRota(), chaveAtividadeSistema,
									cronogramaAtividadeFaturamentoVO, null);
					request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO,
							listaCronogramaAtividadeRotaVO);
					break;
				}
			}
			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO,
					listaCronogramaAtividadeFaturamentoVO);

			cronogramaAtividadeSistemaVO
					.setListaCronogramaAtividadeFaturamentoVO(listaCronogramaAtividadeFaturamentoVO);
			request.getSession().setAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA, cronogramaAtividadeSistemaVO);

			cronogramaVO.setAplicarCronogramaRotasAlteracao(Boolean.TRUE);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.exibirAlteracaoCronograma(model, request, cronogramaVO, bindingResult);
	}

	/**
	 * Método responsável por inserir um cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirCronograma")
	@SuppressWarnings("unchecked")
	public String incluirCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = FORWARD_EXIBIR_INCLUSAO_CRONOGRAMA;
		try {

			Long idGrupoFaturamento = cronogramaVO.getGrupoFaturamento();

			Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = (Collection<CronogramaAtividadeSistemaVO>) request
					.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);

			controladorCronogramaFaturamento.inserirCronogramaFaturamento(idGrupoFaturamento,
					listaCronogramaAtividadeSistemaVO, getDadosAuditoria(request));
			controladorCronogramaFaturamento.verificarCronogramaGrupoFaturamento(idGrupoFaturamento,
					listaCronogramaAtividadeSistemaVO.iterator().next().getAnoMes(),
					listaCronogramaAtividadeSistemaVO.iterator().next().getCiclo());

			cronogramaVO.setGrupoFaturamento(null);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
					CronogramaFaturamento.ROTULO_CRONOGRAMA_FATURAMENTO);

			cronogramaVO.setHabilitado(String.valueOf(Boolean.TRUE));

			view = obterUltimoCronogramaFaturamento(model, idGrupoFaturamento);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;

	}

	private String obterUltimoCronogramaFaturamento(Model model, Long idGrupoFaturamento) throws NegocioException {
		CronogramaFaturamento cronogramaFaturamento = this.controladorCronogramaFaturamento
				.obterUltimoCronogramaFaturamentoPorGrupoFaturamento(idGrupoFaturamento);
		Collection<CronogramaFaturamento> lista = new ArrayList<>();
		lista.add(cronogramaFaturamento);
		model.addAttribute(LISTA_CRONOGRAMA, lista);
		return EXIBIR_PESQUISA_CRONOGRAMA;
	}

	/**
	 * Método responsável por gerar Cronogramas.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("gerarCronograma")
	public String gerarCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) throws GGASException {

		String view = VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA;

		String[] listaDatasAtividadesIniciaisAux = cronogramaVO.getListaDatasAtividadesIniciais()[0].split(",");

		Integer qtdCronogramas = 0;
		Integer[] listaDuracao;

		try {
			request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);
			request.getSession().removeAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);
			request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO);
			request.getSession().removeAttribute(GRUPO_FATURAMENTO);

			if (!cronogramaVO.getQuantidadeCronogramas().isEmpty()) {
				qtdCronogramas = Integer.parseInt(cronogramaVO.getQuantidadeCronogramas());
			}

			GrupoFaturamento grupoFaturamento = controladorRota
					.obterGrupoFaturamento(cronogramaVO.getGrupoFaturamento());
			request.getSession().setAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
			request.getSession().setAttribute(MES_ANO_PARTIDA, cronogramaVO.getMesAnoPartida());

			Map<String, Object> dados = this.popularMapa(cronogramaVO);

			controladorCronogramaFaturamento.validarCamposObrigatoriosGerarCronogramas(dados);
			controladorCronogramaFaturamento.validarListaCamposObrigatoriosGerarCronogramas(
					cronogramaVO.getChavesPrimariasCronograma(), cronogramaVO.getChavesPrimariasMarcadas(),
					cronogramaVO.getDuracoes(), cronogramaVO.getDatasPrevistas());
			controladorCronogramaFaturamento.validarCronogramas(cronogramaVO.getGrupoFaturamento(),
					Integer.parseInt(cronogramaVO.getQuantidadeCronogramas()));
			controladorCronogramaFaturamento.validarAnoMesFaturamento(cronogramaVO.getMesAnoPartida(),
					cronogramaVO.getCicloPartida(), grupoFaturamento);

			listaDuracao = listaDuracaoSemNulos(cronogramaVO.getDuracoes());

			for (int i = 0; i < listaDuracao.length; i++) {

				if (cronogramaVO.getDatasPrevistas()[i] != null && !"".equals(cronogramaVO.getDatasPrevistas()[i])) {
					controladorCronogramaFaturamento.validarDuracaoDias(listaDuracao[i],
							Util.converterCampoStringParaData(DATA_INICIO, listaDatasAtividadesIniciaisAux[i],
									Constantes.FORMATO_DATA_BR),
							Util.converterCampoStringParaData("dataLimite", cronogramaVO.getDatasPrevistas()[i],
									Constantes.FORMATO_DATA_BR));
				}

			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_CRONOGRAMA;
		} finally {
			view = popularListaCronogramaAtividadeSistema(request, cronogramaVO, qtdCronogramas, model, view);
		}

		model.addAttribute(CRONOGRAMA_VO, cronogramaVO);

		return view;

	}

	private Integer[] listaDuracaoSemNulos(Integer[] duracoes) {
		Integer[] novaLista;
		int tamanho = 0;
		int contador = 0;
		for (int i = 0; i < duracoes.length; i++) {
			if (duracoes[i] != null) {
				tamanho++;
			}
		}
		novaLista = new Integer[tamanho];
		for (int i = 0; i < duracoes.length; i++) {
			if (duracoes[i] != null) {
				novaLista[contador] = duracoes[i];
				contador++;
			}
		}
		return novaLista;
	}

	@SuppressWarnings("unchecked")
	private String popularListaCronogramaAtividadeSistema(HttpServletRequest request, CronogramaVO cronogramaVO,
			Integer qtdCronogramas, Model model, String viewParam) {

		String view = viewParam;

		Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = (Collection<CronogramaAtividadeFaturamentoVO>) request
				.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO);

		try {
			String[] listaDatasAtividadesIniciaisAux = cronogramaVO.getListaDatasAtividadesIniciais()[0].split(",");
			String[] listaDatasAtividadesFinaisAux = cronogramaVO.getListaDatasAtividadesFinais()[0].split(",");

			Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = controladorCronogramaFaturamento
					.gerarListaCronogramaAtividade(cronogramaVO.getChavesPrimariasCronograma(),
							cronogramaVO.getDuracoes(), cronogramaVO.getDatasPrevistas(),
							cronogramaVO.getChavesPrimariasMarcadas(), cronogramaVO.getMesAnoPartida(),
							cronogramaVO.getCicloPartida(), qtdCronogramas, cronogramaVO.getGrupoFaturamento(),
							listaDatasAtividadesIniciaisAux, listaDatasAtividadesFinaisAux, Boolean.TRUE);
			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO, listaCronogramaAtividadeSistemaVO);

			cronogramaVO.setQtdAtividadesPorCronograma(cronogramaVO.getChavesPrimariasMarcadas().length);

			preencherPropriedadesPendentes(listaCronogramaAtividadeFaturamentoVO, cronogramaVO.getDuracoes(),
					cronogramaVO.getDatasPrevistas());
			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_FATURAMENTO_VO,
					listaCronogramaAtividadeFaturamentoVO);
			request.getSession().setAttribute(QUANTIDADE_CRONOGRAMAS, cronogramaVO.getQuantidadeCronogramas());
			request.getSession().setAttribute(MES_ANO_PARTIDA, cronogramaVO.getMesAnoPartida());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_EXIBIR_INCLUSAO_CRONOGRAMA;
		}

		return view;
	}

	private void preencherPropriedadesPendentes(Collection<CronogramaAtividadeFaturamentoVO> lista, Integer[] duracoes,
			String[] datasPrevistas) throws GGASException {
		int j = 0;
		int i = 0;
		for (CronogramaAtividadeFaturamentoVO atividade : lista) {
			Integer duracao = 0;
			if (duracoes[i] != null) {
				duracao = duracoes[i];

				atividade.setDuracao(duracao);
				if (!Strings.isNullOrEmpty(datasPrevistas[j])) {
					atividade.setDataPrevista(
							Util.converterCampoStringParaData(PREVISTA, datasPrevistas[j], Constantes.FORMATO_DATA_BR));
				}
				j++;
			}
			i++;
		}
	}

	/**
	 * Obter dados mes ano ciclo.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("obterDadosMesAnoCiclo")
	@SuppressWarnings("unchecked")
	public String obterDadosMesAnoCiclo(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		request.getSession().removeAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);
		request.getSession().removeAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO);

		String anoMesCiclo = null;

		anoMesCiclo = cronogramaVO.getAnoMesCiclo();
		if (anoMesCiclo != null && "".equals(anoMesCiclo) && request.getSession().getAttribute(ANO_MES_CICLO) != null) {
			anoMesCiclo = (String) request.getSession().getAttribute(ANO_MES_CICLO);
		}

		Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = (Collection<CronogramaAtividadeSistemaVO>) request
				.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);

		for (CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO : listaCronogramaAtividadeSistemaVO) {

			String anoMesCicloAux = String.valueOf(cronogramaAtividadeSistemaVO.getAnoMes())
					+ String.valueOf(cronogramaAtividadeSistemaVO.getCiclo());
			if (anoMesCicloAux.equals(anoMesCiclo)) {
				request.getSession().setAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA, cronogramaAtividadeSistemaVO);
				request.getSession().setAttribute(ANO_MES_CICLO, anoMesCiclo);
			}
		}

		request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO, listaCronogramaAtividadeSistemaVO);

		request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO, null);

		return VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA;
	}

	/**
	 * Aplicar atividade sistema.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("aplicarAtividadeSistema")
	@SuppressWarnings("unchecked")
	public String aplicarAtividadeSistema(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {
			Long[] chavesPrimarias = cronogramaVO.getChavesPrimariasCronograma();
			Integer[] duracoes = cronogramaVO.getDuracoes();
			String[] datasPrevistas = cronogramaVO.getDatasPrevistas();
			Long[] chavesPrimariasMarcadas = cronogramaVO.getChavesPrimariasMarcadas();

			Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = (Collection<CronogramaAtividadeSistemaVO>) request
					.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);
			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = (CronogramaAtividadeSistemaVO) request
					.getSession().getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = cronogramaAtividadeSistemaVO
					.getListaCronogramaAtividadeFaturamentoVO();
			Collection<CronogramaAtividadeFaturamentoVO> listaAtividadesPopuladas = new ArrayList<CronogramaAtividadeFaturamentoVO>();

			Map<String, Object> dados = this.popularMapa(cronogramaVO);
			Integer[] listaDuracoesAtividades = (Integer[]) dados.get(DURACOES);
			String[] listaDatasPrevistasAtividades = (String[]) dados.get(DATAS_PREVISTAS);

			controladorCronogramaFaturamento.validarListaCamposObrigatoriosGerarCronogramas(chavesPrimarias,
					chavesPrimariasMarcadas, duracoes, datasPrevistas);

			int i = 0;
			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {

				controladorCronogramaFaturamento.validarDuracaoDias(listaDuracoesAtividades[i],
						Util.converterCampoStringParaData(DATA_INICIO,
								cronogramaAtividadeFaturamentoVO.getDataInicialAtividade(), Constantes.FORMATO_DATA_BR),
						Util.converterCampoStringParaData("dataLimite", listaDatasPrevistasAtividades[i],
								Constantes.FORMATO_DATA_BR));

				i++;
			}

			this.popularlistaCronogramaAtividadeFaturamentoVO(listaCronogramaAtividadeFaturamentoVO,
					listaDuracoesAtividades, listaDatasPrevistasAtividades, chavesPrimariasMarcadas);
			listaAtividadesPopuladas.addAll(listaCronogramaAtividadeFaturamentoVO);

			cronogramaAtividadeSistemaVO.getListaCronogramaAtividadeFaturamentoVO().clear();
			cronogramaAtividadeSistemaVO.getListaCronogramaAtividadeFaturamentoVO().addAll(listaAtividadesPopuladas);

			request.getSession().setAttribute(ANO_MES_CICLO, String.valueOf((cronogramaAtividadeSistemaVO.getAnoMes())
					+ String.valueOf(cronogramaAtividadeSistemaVO.getCiclo())));

			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO, listaCronogramaAtividadeSistemaVO);
			request.getSession().setAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA, cronogramaAtividadeSistemaVO);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return this.obterDadosMesAnoCiclo(model, request, cronogramaVO, bindingResult);
	}

	/**
	 * Popularlista cronograma atividade faturamento vo.
	 * 
	 * @param listaCronogramaAtividadeFaturamentoVO the lista cronograma atividade
	 *                                              faturamento vo
	 * @param listaDuracoesAtividades               the lista duracoes atividades
	 * @param listaDatasPrevistasAtividades         the lista datas previstas
	 *                                              atividades
	 * @param chavesPrimariasMarcadas               the chaves primarias marcadas
	 * @throws GGASException the GGAS exception
	 */
	private void popularlistaCronogramaAtividadeFaturamentoVO(
			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO,
			Integer[] listaDuracoesAtividades, String[] listaDatasPrevistasAtividades, Long[] chavesPrimariasMarcadas)
			throws GGASException {

		int i = 0;
		Collection<CronogramaAtividadeFaturamentoVO> listaAux = new ArrayList<CronogramaAtividadeFaturamentoVO>();
		List<Long> listaChavePrimaria = Arrays.asList(chavesPrimariasMarcadas);

		for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
			if (listaDuracoesAtividades[i] != null) {
				cronogramaAtividadeFaturamentoVO.setDuracao(listaDuracoesAtividades[i]);
			}
			if ((listaDatasPrevistasAtividades[i] != null) && (!"".equals(listaDatasPrevistasAtividades[i]))) {
				cronogramaAtividadeFaturamentoVO.setDataPrevista(Util.converterCampoStringParaData(PREVISTA,
						listaDatasPrevistasAtividades[i], Constantes.FORMATO_DATA_BR));
				DateTime dataInicioAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				dataInicioAux = dataInicioAux.minusDays(cronogramaAtividadeFaturamentoVO.getDuracao());
				cronogramaAtividadeFaturamentoVO.setDataInicio(dataInicioAux.toDate());

				for (CronogramaRota cronogramaRota : cronogramaAtividadeFaturamentoVO.getListaCronogramaRota()) {
					cronogramaRota.setDataPrevista(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				}

			}
			if (listaChavePrimaria
					.contains(cronogramaAtividadeFaturamentoVO.getAtividadeSistema().getChavePrimaria())) {
				listaAux.add(cronogramaAtividadeFaturamentoVO);
			}
			i++;
		}

		listaCronogramaAtividadeFaturamentoVO.clear();
		listaCronogramaAtividadeFaturamentoVO.addAll(listaAux);
	}

	/**
	 * Exibir detalhamento rotas.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoRotas")
	@SuppressWarnings("unchecked")
	public String exibirDetalhamentoRotas(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) throws GGASException {

		Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = (Collection<CronogramaAtividadeSistemaVO>) request
				.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);
		CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = (CronogramaAtividadeSistemaVO) request.getSession()
				.getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

		Integer[] arrayAnoMesCiclo = new Integer[listaCronogramaAtividadeSistemaVO.size()];
		int i = 0;
		for (CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVOAux : listaCronogramaAtividadeSistemaVO) {
			arrayAnoMesCiclo[i] = Integer.parseInt(String.valueOf(cronogramaAtividadeSistemaVOAux.getAnoMes())
					+ String.valueOf(cronogramaAtividadeSistemaVOAux.getCiclo()));
			i++;
		}

		Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();

		Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = cronogramaAtividadeSistemaVO
				.getListaCronogramaAtividadeFaturamentoVO();

		Collection<CronogramaRota> listaCronogramaRota;
		if (chaveAtividadeSistema != null) {
			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
				if (chaveAtividadeSistema.longValue() == cronogramaAtividadeFaturamentoVO.getAtividadeSistema()
						.getChavePrimaria()) {
					listaCronogramaRota = cronogramaAtividadeFaturamentoVO.getListaCronogramaRota();

					Collection<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = this
							.montarListaCronogramaRotasVOInsercao(listaCronogramaRota, chaveAtividadeSistema,
									cronogramaAtividadeFaturamentoVO, arrayAnoMesCiclo);
					request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO,
							listaCronogramaAtividadeRotaVO);

					if (listaCronogramaAtividadeRotaVO != null && !listaCronogramaAtividadeRotaVO.isEmpty()) {

						cronogramaVO.setDataInicialAtividade(
								listaCronogramaAtividadeRotaVO.iterator().next().getDataInicio());
						cronogramaVO.setDataFinalAtividade(
								listaCronogramaAtividadeRotaVO.iterator().next().getDataPrevista());
					}

				}
			}
		}

		request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO, listaCronogramaAtividadeSistemaVO);
		model.addAttribute(CRONOGRAMA_VO, cronogramaVO);

		return VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA;
	}

	/**
	 * Aplicar cronograma rotas.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("aplicarCronogramaRotas")
	@SuppressWarnings("unchecked")
	public String aplicarCronogramaRotas(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {
			Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();

			Map<String, Object> dados = popularMapa(cronogramaVO);
			String[] listaDatasPrevistasRotas = (String[]) dados.get(DATAS_PREVISTAS_ROTAS);
			Long[] listaChavesPrimariasCronogramaRota = (Long[]) dados.get(CHAVES_PRIMARIAS_CRONOGRAMA_ROTA);

			Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = (Collection<CronogramaAtividadeSistemaVO>) request
					.getSession().getAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO);
			CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = (CronogramaAtividadeSistemaVO) request
					.getSession().getAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA);

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = cronogramaAtividadeSistemaVO
					.getListaCronogramaAtividadeFaturamentoVO();

			Collection<CronogramaRota> listaCronogramaRota;
			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
				if (cronogramaAtividadeFaturamentoVO.getAtividadeSistema().getChavePrimaria() == chaveAtividadeSistema
						.longValue()) {
					listaCronogramaRota = cronogramaAtividadeFaturamentoVO.getListaCronogramaRota();

					this.popularDataPrevistaCronogramaRota(listaCronogramaRota, listaDatasPrevistasRotas,
							listaChavesPrimariasCronogramaRota);

					Collection<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = this
							.montarListaCronogramaRotasVOInsercao(listaCronogramaRota, chaveAtividadeSistema,
									cronogramaAtividadeFaturamentoVO, null);
					request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO,
							listaCronogramaAtividadeRotaVO);
				}
			}

			request.getSession().setAttribute(LISTA_CRONOGRAMA_ATIVIDADE_SISTEMA_VO, listaCronogramaAtividadeSistemaVO);
			request.getSession().setAttribute(CRONOGRAMA_ATIVIDADE_SISTEMA, cronogramaAtividadeSistemaVO);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);

		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA;
	}

	/**
	 * Popular data prevista cronograma rota.
	 * 
	 * @param listaCronogramaRota                the lista cronograma rota
	 * @param listaDatasPrevistasRotas           the lista datas previstas rotas
	 * @param listaChavesPrimariasCronogramaRota the lista chaves primarias
	 *                                           cronograma rota
	 * @throws GGASException the GGAS exception
	 */
	private void popularDataPrevistaCronogramaRota(Collection<CronogramaRota> listaCronogramaRota,
			String[] listaDatasPrevistasRotas, Long[] listaChavesPrimariasCronogramaRota) throws GGASException {

		for (CronogramaRota cronogramaRota : listaCronogramaRota) {
			for (int j = 0; j < listaChavesPrimariasCronogramaRota.length; j++) {
				if (cronogramaRota.getRota().getChavePrimaria() == listaChavesPrimariasCronogramaRota[j]) {
					cronogramaRota.setDataPrevista(Util.converterCampoStringParaData(PREVISTA,
							listaDatasPrevistasRotas[j], Constantes.FORMATO_DATA_BR));
					break;
				}
			}
		}
	}

	/**
	 * Popular mapa.
	 */
	private Map<String, Object> popularMapa(CronogramaVO cronogramaVO) {

		Map<String, Object> dados = new HashMap<String, Object>();

		Integer[] duracoes = cronogramaVO.getDuracoes();
		if (duracoes != null) {
			dados.put(DURACOES, duracoes);
		}

		Long grupoFaturamento = cronogramaVO.getGrupoFaturamento();
		if (grupoFaturamento != null && grupoFaturamento > 0) {
			dados.put(GRUPO_FATURAMENTO, grupoFaturamento);
		}

		String mesAnoPartida = cronogramaVO.getMesAnoPartida();
		if (!StringUtils.isEmpty(mesAnoPartida)) {
			dados.put(MES_ANO_PARTIDA, mesAnoPartida);
		}

		String cicloPartida = cronogramaVO.getCicloPartida();
		if (!StringUtils.isEmpty(cicloPartida)) {
			dados.put(CICLO_PARTIDA, cicloPartida);
		}

		String quantidadeCronogramas = cronogramaVO.getQuantidadeCronogramas();
		if (!StringUtils.isEmpty(quantidadeCronogramas)) {
			dados.put(QUANTIDADE_CRONOGRAMAS, quantidadeCronogramas);
		}

		String[] datasPrevistas = cronogramaVO.getDatasPrevistas();
		if (datasPrevistas != null) {
			dados.put(DATAS_PREVISTAS, datasPrevistas);
		}

		String[] datasPrevistasRotas = cronogramaVO.getDatasPrevistasRotas();
		if (datasPrevistasRotas != null) {
			dados.put(DATAS_PREVISTAS_ROTAS, datasPrevistasRotas);
		}

		this.popularMapaComChavesPrimarias(cronogramaVO, dados);

		return dados;
	}

	private void popularMapaComChavesPrimarias(CronogramaVO cronogramaVO, Map<String, Object> dados) {

		Long[] chavesPrimarias = cronogramaVO.getChavesPrimariasCronograma();
		if (chavesPrimarias != null) {
			dados.put(CHAVES_PRIMARIAS_CRONOGRAMA, chavesPrimarias);
		}

		Long[] chavesPrimariasMarcadas = cronogramaVO.getChavesPrimariasMarcadas();
		if (chavesPrimariasMarcadas != null) {
			dados.put(CHAVES_PRIMARIAS_MARCADAS, chavesPrimariasMarcadas);
		}

		Long[] chavesPrimariasCronogramaRota = cronogramaVO.getChavesPrimariasCronogramaRota();
		if (chavesPrimariasCronogramaRota != null) {
			dados.put(CHAVES_PRIMARIAS_CRONOGRAMA_ROTA, chavesPrimariasCronogramaRota);
		}
	}

	/**
	 * Método responsável por exibir a tela de inclusão-rotas de Cronograma.
	 *
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping("exibirInclusaoRotasCronograma")
	public String exibirInclusaoRotasCronograma() {

		return "exibirInclusaoRotasCronograma";
	}

	/**
	 * Método responsável por exibir a tela de inclusão-resumo de Cronograma.
	 *
	 * @return view - {@link String}
	 *
	 */
	@RequestMapping(VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA)
	public String exibirInclusaoResumoCronograma() {

		return VIEW_EXIBIR_INCLUSAO_RESUMO_CRONOGRAMA;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Cronograma.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoCronograma")
	public String exibirDetalhamentoCronograma(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = FORWARD_PESQUISAR_CRONOGRAMA;

		Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();
		Long chavePrimaria = cronogramaVO.getChavePrimaria();

		CronogramaFaturamento cronogramaFaturamento = null;

		try {
			cronogramaFaturamento = controladorCronogramaFaturamento.obterCronogramaFaturamento(chavePrimaria);

			Map<String, String> mapaCamposGrupoFaturamento = controladorCronogramaFaturamento
					.mapearCamposGrupoFaturamento(cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
							cronogramaFaturamento.getChavePrimaria(), null, null);

			if (mapaCamposGrupoFaturamento != null) {
				cronogramaVO.setQtdRotas(mapaCamposGrupoFaturamento.get(QTD_ROTAS));
				cronogramaVO.setQtdMedidores(mapaCamposGrupoFaturamento.get(QTD_MEDIDORES));
			}

			Periodicidade periodicidade = controladorRota.obterPeriodicidade(
					cronogramaFaturamento.getGrupoFaturamento().getPeriodicidade().getChavePrimaria());

			GrupoFaturamento grupoFaturamento = controladorRota
					.obterGrupoFaturamento(cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria());

			cronogramaVO.setSituacoesPontoConsumo(situacoesPontoConsumo);

			Long tipoLeitura = grupoFaturamento.getTipoLeitura().getChavePrimaria();

			// Código para verificar data inicial e final dos cronogramas do
			// supervisório(tipo de leitura = 1)
			if (tipoLeitura == 1) {
				cronogramaVO.setDataInicioLeituraSuper(
						controladorCronogramaFaturamento.obterDataMaximaOuMininaSupervisorioCronograma(
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo(),
								grupoFaturamento.getChavePrimaria(), Boolean.FALSE));

				cronogramaVO.setDataFinalLeituraSuper(
						controladorCronogramaFaturamento.obterDataMaximaOuMininaSupervisorioCronograma(
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo(),
								grupoFaturamento.getChavePrimaria(), Boolean.TRUE));
			}

			cronogramaVO.setAnoMesCiclo(
					cronogramaFaturamento.getAnoMesFaturamentoMascara() + "-" + cronogramaFaturamento.getNumeroCiclo());
			cronogramaVO.setDescricaoGrupoFaturamento(cronogramaFaturamento.getGrupoFaturamento().getDescricao());
			cronogramaVO.setDescricaoPeriodicidade(periodicidade.getDescricao());

			Collection<CronogramaAtividadeFaturamento> listaAtividadesCronograma = controladorCronogramaFaturamento
					.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(chavePrimaria);

			if (listaAtividadesCronograma != null && cronogramaFaturamento.getGrupoFaturamento().getTipoLeitura()
					.getDescricao().equals("ELETROCORRETOR")) {
				request.setAttribute("mapaPontoConsumoCronograma",
						calculadoraPontoConsumoCronograma.gerarQuantidadesPontoConsumoSupervisorio(
								cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo(),
								cronogramaFaturamento.getChavePrimaria(),
								cronogramaFaturamento.getGrupoFaturamento().getTipoLeitura().getChavePrimaria()));

				request.setAttribute("mapaPontoConsumoTotaisCronograma",
						calculadoraPontoConsumoCronograma.gerarQuantidadesPontoConsumoTotaisSupervisorio(
								cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo(),
								cronogramaFaturamento.getGrupoFaturamento().getTipoLeitura().getChavePrimaria()));
			} else {
				request.setAttribute("mapaPontoConsumoCronograma",
						calculadoraPontoConsumoCronograma.gerarQuantidadesPontoConsumo(
								cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo(),
								cronogramaFaturamento.getChavePrimaria()));

				request.setAttribute("mapaPontoConsumoTotaisCronograma",
						calculadoraPontoConsumoCronograma.gerarQuantidadesPontoConsumoTotais(
								cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
								cronogramaFaturamento.getAnoMesFaturamento(), cronogramaFaturamento.getNumeroCiclo()));
			}

			model.addAttribute(LISTA_ATIVIDADE_FATURAMENTOS, listaAtividadesCronograma);

			Collection<CronogramaRota> listaCronogramaRotas = null;

			if (chaveAtividadeSistema != null) {

				popularAtividadeDeCronogramaVO(cronogramaVO, chaveAtividadeSistema, listaAtividadesCronograma);
				listaCronogramaRotas = controladorRota.listarCronogramasRotasPorAtividade(chaveAtividadeSistema,
						chavePrimaria);
				Collection<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = this
						.montarListaCronogramaRotasVO(listaCronogramaRotas, chaveAtividadeSistema, null);

				model.addAttribute(LISTA_CRONOGRAMA_ATIVIDADE_ROTA_VO, listaCronogramaAtividadeRotaVO);
			}
			if (!isPostBack(request)) {
				obterSituacoesPontoConsumo(model);
			} else {
				preencherFiltroPontoConsumo(model, cronogramaVO);
			}

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
			model.addAttribute("tipoLeitura", tipoLeitura);

			inserirInformacoesRealizarLeitura(cronogramaFaturamento, model);

			view = "exibirDetalhamentoCronograma";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private void popularAtividadeDeCronogramaVO(CronogramaVO cronogramaVO, Long chaveAtividadeSistema,
			Collection<CronogramaAtividadeFaturamento> listaAtividadesCronograma) {
		if (listaAtividadesCronograma != null && !listaAtividadesCronograma.isEmpty()) {
			for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaAtividadesCronograma) {
				if (chaveAtividadeSistema == cronogramaAtividadeFaturamento.getAtividadeSistema().getChavePrimaria()) {
					cronogramaVO.setDataInicio(cronogramaAtividadeFaturamento.getdataInicioFormatada());
					cronogramaVO.setDataFim(cronogramaAtividadeFaturamento.getdataFimFormatada());
					cronogramaVO
							.setDescricaoAtividade(cronogramaAtividadeFaturamento.getAtividadeSistema().getDescricao());
				}
			}
		}
	}

	private void obterSituacoesPontoConsumo(Model model) {
		for (SituacaoPontoConsumo situacao : SituacaoPontoConsumo.values()) {
			model.addAttribute(situacao.toString(), Boolean.TRUE);
		}
	}

	private void inserirInformacoesRealizarLeitura(CronogramaFaturamento cronogramaFaturamento, Model model) {

		controladorRota
				.listarCronogramasRotasPorAtividade(CHAVE_REALIZAR_LEITURA, cronogramaFaturamento.getChavePrimaria())
				.stream().findFirst().ifPresent(cronogramaRota -> {
					model.addAttribute("chaveRotaRealizarLeitura", cronogramaRota.getRota().getChavePrimaria());
				});
		model.addAttribute("chaveGrupoFaturamentoRealizarLeitura",
				cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria());

	}

	private Map<String, Object> preencherFiltroPontoConsumo(Model model, CronogramaVO cronogramaVO) {

		for (String situacao : cronogramaVO.getSituacoesPontoConsumo()) {
			model.addAttribute(situacao, Boolean.TRUE);
		}

		return model.asMap();
	}

	/**
	 * Exibir detalhamento cronograma acompanhamento.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoCronogramaAcompanhamento")
	public String exibirDetalhamentoCronogramaAcompanhamento(Model model, HttpServletRequest request,
			CronogramaVO cronogramaVO, BindingResult bindingResult) {
		this.exibirDetalhamentoCronograma(model, request, cronogramaVO, bindingResult);
		String chaveFaturarGrupoStr = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ATIVIDADE_SISTEMA_FATURAR_GRUPO);
		if (chaveFaturarGrupoStr != null) {
			Long chaveFaturarGrupo = Long.valueOf(chaveFaturarGrupoStr);
			CronogramaAtividadeFaturamento atividadeFaturar = controladorCronogramaAtividadeFaturamento
					.obterCronogramaAtividadeFaturamento(cronogramaVO.getChavePrimaria(), chaveFaturarGrupo);
			boolean faturado = false;
			if (atividadeFaturar != null && atividadeFaturar.getDataRealizacao() != null) {
				faturado = true;
			}
			model.addAttribute("faturado", faturado);
		}
		return "exibirDetalhamentoCronogramaAcompanhamento";
	}

	/**
	 * Montar lista cronograma rotas vo.
	 * 
	 * @param listaCronogramaRotas             the lista cronograma rotas
	 * @param idAtividadeSistema               the id atividade sistema
	 * @param cronogramaAtividadeFaturamentoVO the cronograma atividade faturamento
	 *                                         vo
	 * @return the collection
	 * @throws GGASException
	 */
	private Collection<CronogramaAtividadeRotaVO> montarListaCronogramaRotasVO(
			Collection<CronogramaRota> listaCronogramaRotas, Long idAtividadeSistema,
			CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO) throws GGASException {

		List<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = new ArrayList<CronogramaAtividadeRotaVO>();
		CronogramaAtividadeRotaVO cronogramaVO = null;
		Integer quantidadeLeitura = null;

		for (CronogramaRota cronogramaRota : listaCronogramaRotas) {
			cronogramaVO = new CronogramaAtividadeRotaVO();

			if ((idAtividadeSistema != null) && (cronogramaRota.getRota() != null)
					&& (cronogramaRota.getDataPrevista() != null)) {
				Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(
						cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo(),
						cronogramaRota.getRota().getPeriodicidade().getQuantidadeCiclo());

				CronogramaRota cronogramaRotaPesquisado = controladorRota.consultarCronogramaRotaAtividade(
						cronogramaRota.getRota().getChavePrimaria(), idAtividadeSistema, referenciaCicloAnterior);
				if ((cronogramaRotaPesquisado != null) && (cronogramaRotaPesquisado.getDataRealizada() != null)) {
					cronogramaVO.setDataCiclo(cronogramaRotaPesquisado.getDataRealizadaFormatada());
				}
				if ((cronogramaRotaPesquisado != null) && (cronogramaRotaPesquisado.getDataPrevista() != null)) {
					Integer diasLeitura = Util.intervaloDatas(cronogramaRotaPesquisado.getDataPrevista(),
							cronogramaRota.getDataPrevista());
					cronogramaVO.setDiasLeitura(diasLeitura);
				}
			}

			cronogramaVO.setDataRealizada(cronogramaRota.getDataRealizadaFormatada());
			cronogramaVO.setRota(cronogramaRota.getRota());
			cronogramaVO.setDataPrevista(cronogramaRota.getDataPrevistaFormatada());
			if (cronogramaRota.getRota().getLeiturista() != null) {
				cronogramaVO.setNomeleiturista(cronogramaRota.getRota().getLeiturista().getFuncionario().getNome());
			}
			quantidadeLeitura = controladorPontoConsumo
					.quantidadeLeituraPorRota(cronogramaRota.getRota().getChavePrimaria());
			cronogramaVO.setQuantidadeLeitura(quantidadeLeitura);

			cronogramaVO.setEmAlerta(controladorRota.validarCronogramaRotaEmAlerta(cronogramaRota,
					cronogramaRota.getRota().getChavePrimaria()));
			cronogramaVO.setRotasEmAlerta(controladorRota.listarRotasCronogramaEmAlerta(cronogramaRota));

			if (cronogramaAtividadeFaturamentoVO != null) {

				DateTime dataPrevistaAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				Integer duracaoAux = cronogramaAtividadeFaturamentoVO.getDuracao() - 1;
				cronogramaVO.setDataInicio(Util.converterDataParaStringSemHora(
						dataPrevistaAux.minusDays(duracaoAux).toDate(), Constantes.FORMATO_DATA_BR));
			}

			listaCronogramaAtividadeRotaVO.add(cronogramaVO);

		}

		return listaCronogramaAtividadeRotaVO;
	}

	/**
	 * Montar lista cronograma rotas vo insercao.
	 * 
	 * @param listaCronogramaRotas             the lista cronograma rotas
	 * @param idAtividadeSistema               the id atividade sistema
	 * @param cronogramaAtividadeFaturamentoVO the cronograma atividade faturamento
	 *                                         vo
	 * @param arrayAnoMesCiclo                 the array ano mes ciclo
	 * @return the collection
	 * @throws GGASException
	 */
	private Collection<CronogramaAtividadeRotaVO> montarListaCronogramaRotasVOInsercao(
			Collection<CronogramaRota> listaCronogramaRotas, Long idAtividadeSistema,
			CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO, Integer[] arrayAnoMesCiclo)
			throws GGASException {

		List<CronogramaAtividadeRotaVO> listaCronogramaAtividadeRotaVO = new ArrayList<CronogramaAtividadeRotaVO>();
		CronogramaAtividadeRotaVO cronogramaVO = null;
		Integer quantidadeLeitura = null;

		for (CronogramaRota cronogramaRota : listaCronogramaRotas) {
			cronogramaVO = new CronogramaAtividadeRotaVO();

			Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(
					cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo(),
					cronogramaRota.getRota().getPeriodicidade().getQuantidadeCiclo());

			Boolean consultarCronogramaRotaAtividade = Boolean.TRUE;

			if ((idAtividadeSistema != null) && (cronogramaRota.getRota() != null)
					&& (cronogramaRota.getDataPrevista() != null)) {
				if (arrayAnoMesCiclo != null && arrayAnoMesCiclo.length > 0) {

					Integer anoMesCiclo = Integer.parseInt(String.valueOf(referenciaCicloAnterior.get("referencia"))
							+ String.valueOf(referenciaCicloAnterior.get(CICLO)));

					for (int i = 0; i < arrayAnoMesCiclo.length; i++) {
						if (arrayAnoMesCiclo[i].equals(anoMesCiclo)) {
							consultarCronogramaRotaAtividade = Boolean.FALSE;
							break;
						}
					}

				}

				if (consultarCronogramaRotaAtividade) {

					CronogramaRota cronogramaRotaPesquisado = controladorRota.consultarCronogramaRotaAtividade(
							cronogramaRota.getRota().getChavePrimaria(), idAtividadeSistema, referenciaCicloAnterior);
					if ((cronogramaRotaPesquisado != null) && (cronogramaRotaPesquisado.getDataRealizada() != null)) {
						cronogramaVO.setDataCiclo(cronogramaRotaPesquisado.getDataRealizadaFormatada());
					}
					if ((cronogramaRotaPesquisado != null) && (cronogramaRotaPesquisado.getDataPrevista() != null)) {
						Integer diasLeitura = Util.intervaloDatas(cronogramaRotaPesquisado.getDataPrevista(),
								cronogramaRota.getDataPrevista());
						cronogramaVO.setDiasLeitura(diasLeitura);
					}
				}
			}
			cronogramaVO.setDataRealizada(cronogramaRota.getDataRealizadaFormatada());
			cronogramaVO.setRota(cronogramaRota.getRota());
			cronogramaVO.setDataPrevista(cronogramaRota.getDataPrevistaFormatada());
			if (cronogramaRota.getRota().getLeiturista() != null) {
				cronogramaVO.setNomeleiturista(cronogramaRota.getRota().getLeiturista().getFuncionario().getNome());
			}
			quantidadeLeitura = controladorPontoConsumo
					.quantidadeLeituraPorRota(cronogramaRota.getRota().getChavePrimaria());
			cronogramaVO.setQuantidadeLeitura(quantidadeLeitura);

			cronogramaVO.setEmAlerta(controladorRota.validarCronogramaRotaEmAlerta(cronogramaRota,
					cronogramaRota.getRota().getChavePrimaria()));
			cronogramaVO.setRotasEmAlerta(controladorRota.listarRotasCronogramaEmAlerta(cronogramaRota));

			DateTime dataPrevistaAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());
			Integer duracaoAux = cronogramaAtividadeFaturamentoVO.getDuracao() - 1;
			cronogramaVO.setDataInicio(Util.converterDataParaStringSemHora(
					dataPrevistaAux.minusDays(duracaoAux).toDate(), Constantes.FORMATO_DATA_BR));

			listaCronogramaAtividadeRotaVO.add(cronogramaVO);

		}

		return listaCronogramaAtividadeRotaVO;
	}

	/**
	 * Método responsável por exibir a tela de Alterar-rotas de Cronograma.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException
	 *
	 */

	private void carregarCampos(Model model) throws GGASException {

		Collection<GrupoFaturamento> listaGrupoFaturamentos = controladorRota.listarGruposFaturamentoRotas();
		Collection<Periodicidade> listaPeriodicidades = controladorRota.listarPeriodicidades();

		final Collection<AtividadeSistema> atividadesSistema = controladorCronogramaFaturamento
				.listarAtividadeSistemaPorCronograma();

		model.addAttribute("atividadesSistema", atividadesSistema);

		model.addAttribute("listaSituacao", controladorCronogramaFaturamento.obterListaSituacao());
		model.addAttribute("listaGrupoFaturamentos", listaGrupoFaturamentos);
		model.addAttribute("listaPeriodicidades", listaPeriodicidades);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param cronogramaVO - {@link CronogramaVO}
	 */
	private Map<String, Object> prepararFiltro(CronogramaVO cronogramaVO) {

		Map<String, Object> filtro = new HashMap<>();

		Long idPeriodicidade = cronogramaVO.getPeriodicidade();
		if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
			filtro.put(PERIODICIDADE, idPeriodicidade);
		}
		String mesAnoFaturamentoInicial = cronogramaVO.getMesAnoFaturamentoInicial();
		if (!StringUtils.isEmpty(mesAnoFaturamentoInicial)) {
			filtro.put(MES_ANO_FATURAMENTO_INICIAL, Util.formatarReferenciaMesAno(mesAnoFaturamentoInicial));
		}

		String mesAnoFaturamentoFinal = cronogramaVO.getMesAnoFaturamentoFinal();
		if (!StringUtils.isEmpty(mesAnoFaturamentoFinal)) {
			filtro.put(MES_ANO_FATURAMENTO_FINAL, Util.formatarReferenciaMesAno(mesAnoFaturamentoFinal));
		}

		Long codigoGrupoFaturamento = cronogramaVO.getGrupoFaturamento();
		if (codigoGrupoFaturamento != null && codigoGrupoFaturamento > 0) {
			filtro.put(GRUPO_FATURAMENTO, codigoGrupoFaturamento);
		}

		String habilitado = cronogramaVO.getHabilitado();
		if (!StringUtils.isEmpty(habilitado)) {
			filtro.put(HABILITADO, Boolean.valueOf(habilitado));
		}

		String situacao = cronogramaVO.getSituacao();
		if (!StringUtils.isEmpty(situacao)) {
			filtro.put(SITUACAO, situacao);
		}

		final Long ultimaEtapa = cronogramaVO.getUltimaEtapa();
		if (ultimaEtapa != null && ultimaEtapa != -1) {
			filtro.put("ultimaEtapa", ultimaEtapa);
		}

		return filtro;
	}

	/**
	 * Exibir detalhamento rota cronograma acompanhamento.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoRotaCronogramaAcompanhamento")
	public String exibirDetalhamentoRotaCronogramaAcompanhamento(Model model, HttpServletRequest request,
			CronogramaVO cronogramaVO, BindingResult bindingResult) {

		try {
			Long chaveCronograma = cronogramaVO.getChavePrimaria();

			Long codigoRota = Long.parseLong(cronogramaVO.getCodigoRotaDetalhamento());
			Long chaveAtividadeSistema = cronogramaVO.getChaveAtividadeSistema();
			String chaveConsistirLeituraStr = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO);
			if (chaveConsistirLeituraStr != null) {
				Long chaveConsistirLeitura = Long.valueOf(chaveConsistirLeituraStr);
				model.addAttribute("chaveConsistirLeitura", chaveConsistirLeitura);
			}
			String chaveFaturarGrupoStr = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ATIVIDADE_SISTEMA_FATURAR_GRUPO);
			if (chaveFaturarGrupoStr != null) {
				Long chaveFaturarGrupo = Long.valueOf(chaveFaturarGrupoStr);
				model.addAttribute("chaveFaturarGrupo", chaveFaturarGrupo);
			}

			Map<String, Object> filtro = new HashMap<>();

			filtro.put("idRota", codigoRota);
			filtro.put("habilitado", Boolean.TRUE);

			Collection<PontoConsumo> pontoConsumoList = controladorPontoConsumo
					.consultarPontosConsumoDetalhamentoCronograma(filtro);

			Collection<PontoConsumoAcompanhamentoCronogramaVO> listaPontoConsumoSituacao = consultarPontoCosumoSituacaoAcompanhamento(
					pontoConsumoList, chaveAtividadeSistema, chaveCronograma);

			model.addAttribute(LISTA_DETALHAMENTO_ROTA, listaPontoConsumoSituacao);
			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return exibirDetalhamentoCronogramaAcompanhamento(model, request, cronogramaVO, bindingResult);
	}

	/**
	 * Checks if is existe ponto consumo.
	 * 
	 * @param pontoConsumoInformado the ponto consumo informado
	 * @param listaPontoConsumo     the lista ponto consumo
	 * @return true, if is existe ponto consumo
	 */
	private boolean isExistePontoConsumo(Long chavePontoConsumoInformado, Collection<Long> listaPontoConsumo) {

		for (Long pontoConsumoChavePrimaria : listaPontoConsumo) {
			if (pontoConsumoChavePrimaria.equals(chavePontoConsumoInformado)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks if is existe ponto consumo.
	 * 
	 * @param pontoConsumoInformado the ponto consumo informado
	 * @param listaPontoConsumo     the lista ponto consumo
	 * @return true, if is existe ponto consumo
	 */
	private boolean isExistePontoConsumo(PontoConsumo pontoConsumoInformado,
			Collection<PontoConsumo> listaPontoConsumo) {

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			if (pontoConsumo.getChavePrimaria() == pontoConsumoInformado.getChavePrimaria()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Consultar ponto cosumo situacao acompanhamento.
	 * 
	 * @param pontoConsumoList      the ponto consumo list
	 * @param chaveAtividadeSistema the chave atividade sistema
	 * @param chaveCronograma       the chave cronograma
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<PontoConsumoAcompanhamentoCronogramaVO> consultarPontoCosumoSituacaoAcompanhamento(
			Collection<PontoConsumo> pontoConsumoList, Long chaveAtividadeSistema, Long chaveCronograma)
			throws GGASException {

		CronogramaFaturamento cronogramaFaturamento = controladorCronogramaFaturamento
				.obterCronogramaFaturamento(chaveCronograma);

		ConstanteSistema constanteSistemaRegistrarLeitura = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_CODIGO_ATIVIDADE_REGISTRAR_LEITURA);
		ConstanteSistema constanteSistemaConsistirLeituraConsumo = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO);
		ConstanteSistema constanteSistemaFaturarGrupo = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ATIVIDADE_SISTEMA_FATURAR_GRUPO);
		ConstanteSistema constanteSistemaEmitirFatura = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ATIVIDADE_SISTEMA_EMITIR_FATURA);
		ConstanteSistema constanteSistemaLeituraMovimentoEmitir = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_LEITURA_MOVIMENTO_EMITIR);

		Long cdRegistrarLeitura = Long.parseLong(constanteSistemaRegistrarLeitura.getValor());
		Long cdConsistirLeituraConsumo = Long.parseLong(constanteSistemaConsistirLeituraConsumo.getValor());
		Long cdFaturarGrupo = Long.parseLong(constanteSistemaFaturarGrupo.getValor());
		Long cdEmitirFatura = Long.parseLong(constanteSistemaEmitirFatura.getValor());
		Long cdLeituraMovimentoEmitir = Long.parseLong(constanteSistemaLeituraMovimentoEmitir.getValor());

		List<PontoConsumoAcompanhamentoCronogramaVO> listaRetorno = new ArrayList<>();

		Map<String, Object> filtro = new HashMap<String, Object>();

		// filtro.put(GRUPO_FATURAMENTO,
		// pontoConsumo.getRota().getGrupoFaturamento().getChavePrimaria());
		filtro.put(GRUPO_FATURAMENTO, cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria());
		filtro.put("anoMes", cronogramaFaturamento.getAnoMesFaturamento());
		filtro.put(CICLO, cronogramaFaturamento.getNumeroCiclo());
		filtro.put("idLeituraMovimentoEmitir", cdLeituraMovimentoEmitir);

		if (cdRegistrarLeitura.compareTo(chaveAtividadeSistema) == 0) {

			listaRetorno.addAll(montarListaSituacaoRegistrarLeitura(pontoConsumoList, filtro));

		} else if (cdConsistirLeituraConsumo.compareTo(chaveAtividadeSistema) == 0) {

			listaRetorno.addAll(montarListaSituacaoConsistirLeitura(pontoConsumoList, filtro));

		} else if (cdFaturarGrupo.compareTo(chaveAtividadeSistema) == 0) {

			listaRetorno.addAll(montarListaSituacaoFaturarGrupo(pontoConsumoList, filtro));

		} else if (cdEmitirFatura.compareTo(chaveAtividadeSistema) == 0) {

			listaRetorno.addAll(montarListaSituacaoEmitirFatura(pontoConsumoList, filtro));

		}

		return listaRetorno;
	}

	/**
	 * Montar lista situacao emitir fatura.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param filtro            the filtro
	 * @return the collection
	 */
	private Collection<PontoConsumoAcompanhamentoCronogramaVO> montarListaSituacaoEmitirFatura(
			Collection<PontoConsumo> listaPontoConsumo, Map<String, Object> filtro) {

		Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade(filtro);
		Collection<Long> consultarPontosConsumoEmitirFaturaProntoProcessar = controladorPontoConsumo
				.consultarPontosConsumoEmitirFaturaProntoProcessar(filtro);
		Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoEmitirFaturaProcessadoAnormalidade(filtro);

		List<PontoConsumoAcompanhamentoCronogramaVO> listaRetorno = new ArrayList<PontoConsumoAcompanhamentoCronogramaVO>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			PontoConsumoAcompanhamentoCronogramaVO pontoConsumoAcompanhamento = new PontoConsumoAcompanhamentoCronogramaVO();

			pontoConsumoAcompanhamento.setCodigoPontoConsumo(String.valueOf(pontoConsumo.getChavePrimaria()));
			pontoConsumoAcompanhamento.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			pontoConsumoAcompanhamento.setDescricaoSegmento(pontoConsumo.getSegmento().getDescricao());

			if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_SEM_ANORMALIDADE);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoEmitirFaturaProntoProcessar)) {
				pontoConsumoAcompanhamento.setSituacao(PRONTO_PROCESSAR);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoEmitirFaturaProcessadoAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_COM_ANORMALIDADE);
			} else {
				pontoConsumoAcompanhamento.setSituacao(NAO_ESTA_PRONTO_PARA_PROCESSAR);
			}

			listaRetorno.add(pontoConsumoAcompanhamento);
		}

		return listaRetorno;
	}

	/**
	 * Montar lista situacao faturar grupo.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param filtro            the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	private Collection<PontoConsumoAcompanhamentoCronogramaVO> montarListaSituacaoFaturarGrupo(
			Collection<PontoConsumo> listaPontoConsumo, Map<String, Object> filtro) throws GGASException {

		Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(filtro);
		Collection<Long> consultarPontosConsumoFaturarGrupoProntoProcessar = controladorPontoConsumo
				.consultarPontosConsumoFaturarGrupoProntoProcessar(filtro);
		Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(filtro);

		List<PontoConsumoAcompanhamentoCronogramaVO> listaRetorno = new ArrayList<PontoConsumoAcompanhamentoCronogramaVO>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			PontoConsumoAcompanhamentoCronogramaVO pontoConsumoAcompanhamento = new PontoConsumoAcompanhamentoCronogramaVO();

			pontoConsumoAcompanhamento.setCodigoPontoConsumo(String.valueOf(pontoConsumo.getChavePrimaria()));
			pontoConsumoAcompanhamento.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			pontoConsumoAcompanhamento.setDescricaoSegmento(pontoConsumo.getSegmento().getDescricao());

			filtro.put(TIPO_ATIVIDADE, Constantes.C_ATIVIDADE_SISTEMA_FATURAR_GRUPO);
			BigDecimal consumo = obterConsumoApurado(pontoConsumo, filtro);
			pontoConsumoAcompanhamento.setConsumo(consumo);

			if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_SEM_ANORMALIDADE);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoFaturarGrupoProntoProcessar)) {
				pontoConsumoAcompanhamento.setSituacao(PRONTO_PROCESSAR);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoFaturarGrupoProcessadoAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_COM_ANORMALIDADE);
			} else {
				pontoConsumoAcompanhamento.setSituacao(NAO_ESTA_PRONTO_PARA_PROCESSAR);
			}

			listaRetorno.add(pontoConsumoAcompanhamento);
		}

		return listaRetorno;
	}

	/**
	 * Montar lista situacao consistir leitura.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param filtro            the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	private Collection<PontoConsumoAcompanhamentoCronogramaVO> montarListaSituacaoConsistirLeitura(
			Collection<PontoConsumo> listaPontoConsumo, Map<String, Object> filtro) throws GGASException {

		Collection<Long> pontosConsumoConsistirLeituraProcessadoSemAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(filtro);
		Collection<Long> consultarPontosConsumoConsistirLeituraProntoProcessar = controladorPontoConsumo
				.consultarPontosConsumoConsistirLeituraProntoProcessar(filtro);

		Collection<Long> consultarPontosConsumoAnormalidadeMasAnalisada = controladorPontoConsumo
				.consultarPontosConsumoConsistirLeituraProcessadoComAnormalidadeMasAnalisada(filtro);
		// Pesquisar somente anormalidades que não bloqueiam faturamento
		filtro.put("bloqueiaFaturamento", Boolean.FALSE);
		Collection<PontoConsumo> consultarPontosConsumoConsistirLeituraProcessadoAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(filtro);

		// Pesquisar somente anormalidades que bloqueiam faturamento
		filtro.put("bloqueiaFaturamento", Boolean.TRUE);
		Collection<PontoConsumo> consultarPontosConsumoConsistirLeituraProcessadoAnormalidadeBloqueio = controladorPontoConsumo
				.consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(filtro);

		List<PontoConsumoAcompanhamentoCronogramaVO> listaRetorno = new ArrayList<PontoConsumoAcompanhamentoCronogramaVO>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			PontoConsumoAcompanhamentoCronogramaVO pontoConsumoAcompanhamento = new PontoConsumoAcompanhamentoCronogramaVO();

			pontoConsumoAcompanhamento.setCodigoPontoConsumo(String.valueOf(pontoConsumo.getChavePrimaria()));
			pontoConsumoAcompanhamento.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			pontoConsumoAcompanhamento.setDescricaoSegmento(pontoConsumo.getSegmento().getDescricao());

			filtro.put(TIPO_ATIVIDADE, Constantes.C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO);
			BigDecimal consumo = obterConsumoApurado(pontoConsumo, filtro);
			pontoConsumoAcompanhamento.setConsumo(consumo);

			if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					pontosConsumoConsistirLeituraProcessadoSemAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_SEM_ANORMALIDADE);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoConsistirLeituraProntoProcessar)) {
				pontoConsumoAcompanhamento.setSituacao(PRONTO_PROCESSAR);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoAnormalidadeMasAnalisada)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_ANORMALIDADE_ANALISADA);
			} else if (isExistePontoConsumo(pontoConsumo,
					consultarPontosConsumoConsistirLeituraProcessadoAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_COM_ANORMALIDADE);
			} else if (isExistePontoConsumo(pontoConsumo,
					consultarPontosConsumoConsistirLeituraProcessadoAnormalidadeBloqueio)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_COM_ANORMALIDADE_BLOQUEIO);
			} else {
				pontoConsumoAcompanhamento.setSituacao(NAO_ESTA_PRONTO_PARA_PROCESSAR);
			}

			listaRetorno.add(pontoConsumoAcompanhamento);
		}

		return listaRetorno;
	}

	/**
	 * Montar lista situacao registrar leitura.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param filtro            the filtro
	 * @return the collection
	 */
	private Collection<PontoConsumoAcompanhamentoCronogramaVO> montarListaSituacaoRegistrarLeitura(
			Collection<PontoConsumo> listaPontoConsumo, Map<String, Object> filtro) {

		Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(filtro);
		Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade = controladorPontoConsumo
				.consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(filtro);
		Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessar = controladorPontoConsumo
				.consultarPontosConsumoRegistrarLeituraProntoProcessar(filtro);
		Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessarColetor = controladorPontoConsumo
				.consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(filtro);

		List<PontoConsumoAcompanhamentoCronogramaVO> listaRetorno = new ArrayList<PontoConsumoAcompanhamentoCronogramaVO>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			PontoConsumoAcompanhamentoCronogramaVO pontoConsumoAcompanhamento = new PontoConsumoAcompanhamentoCronogramaVO();

			pontoConsumoAcompanhamento.setCodigoPontoConsumo(String.valueOf(pontoConsumo.getChavePrimaria()));
			pontoConsumoAcompanhamento.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			pontoConsumoAcompanhamento.setDescricaoSegmento(pontoConsumo.getSegmento().getDescricao());

			if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_SEM_ANORMALIDADE);
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoRegistrarLeituraProntoProcessarColetor)) {
				pontoConsumoAcompanhamento.setSituacao(PRONTO_PROCESSAR);

				if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
						consultarPontosConsumoRegistrarLeituraProntoProcessar)) {
					pontoConsumoAcompanhamento.setSituacao(PRONTO_PROCESSAR);
				}
			} else if (isExistePontoConsumo(pontoConsumo.getChavePrimaria(),
					consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade)) {
				pontoConsumoAcompanhamento.setSituacao(PROCESSADO_COM_ANORMALIDADE);
			} else {
				pontoConsumoAcompanhamento.setSituacao(NAO_ESTA_PRONTO_PARA_PROCESSAR);
			}

			listaRetorno.add(pontoConsumoAcompanhamento);
		}

		return listaRetorno;
	}

	/**
	 * Obter consumo apurado.
	 * 
	 * @param pontoConsumo the ponto consumo
	 * @param filtro       the filtro
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	private BigDecimal obterConsumoApurado(PontoConsumo pontoConsumo, Map<String, Object> filtro) throws GGASException {

		String tipoAtividade = (String) filtro.get(TIPO_ATIVIDADE);
		Collection<PontoConsumo> lista = new ArrayList<PontoConsumo>();
		lista.add(pontoConsumo);
		Integer anoMesFaturamento = (Integer) filtro.get("anoMes");
		Integer numeroCiclo = (Integer) filtro.get(CICLO);
		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();
		BigDecimal consumo = null;

		if (tipoAtividade.equals(Constantes.C_ATIVIDADE_SISTEMA_CONSISTIR_LEITURA_CONSUMO)) {
			consumo = controladorHistoricoConsumo.obterConsumoApuradoPontoConsumoPorReferencia(anoMesFaturamento,
					numeroCiclo, lista);
		} else if (tipoAtividade.equals(Constantes.C_ATIVIDADE_SISTEMA_FATURAR_GRUPO)) {
			Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
					.obterHistoricoConsumoTodos(pontoConsumo.getChavePrimaria(), true, true, true);
			consumo = BigDecimal.ZERO;
			for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
				if (historicoConsumo.getConsumoApurado() != null) {
					consumo = consumo.add(historicoConsumo.getConsumoApurado());
				} else if (historicoConsumo.getConsumoApuradoMedio() != null) {
					consumo = consumo.add(historicoConsumo.getConsumoApuradoMedio());
				}
			}
			if (consumo.compareTo(BigDecimal.ZERO) == 0) {
				consumo = null;
			}
		}

		return consumo;
	}

	/**
	 * Exibir escalonamento leiturista.
	 * 
	 * @param request      - {@link HttpServletRequest}
	 * @param cronogramaVO - {@link CronogramaVO}
	 * @param model        - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping(VIEW_EXIBIR_ESCALONAMENTO_LEITURISTA)
	public String exibirEscalonamentoLeiturista(Model model, HttpServletRequest request, CronogramaVO cronogramaVO) {

		String view = VIEW_EXIBIR_ESCALONAMENTO_LEITURISTA;
		try {
			Long chavePrimaria = cronogramaVO.getChavePrimaria();

			Map<String, Object> filtro = popularFormEscalonamento(request, chavePrimaria);

			List<CronogramaRota> listaCronogramaRota = (ArrayList<CronogramaRota>) controladorCronogramaFaturamento
					.consultarCronogramaFaturamentoEscalonamento(filtro);

			List<CronogramaRotaVO> listaCronogramaFaturamentoVO = obterListaCronogramaFaturamentoVO(
					listaCronogramaRota);

			boolean fluxoInclusao = verificaFluxo(listaCronogramaFaturamentoVO);

			request.getSession().setAttribute("fluxoInclusao", fluxoInclusao);
			request.getSession().setAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO, listaCronogramaFaturamentoVO);
			request.getSession().setAttribute(LISTA_LEITURISTA, controladorRota.listarLeituristas());
			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = FORWARD_PESQUISAR_CRONOGRAMA;
		}

		return view;
	}

	private Map<String, Object> popularFormEscalonamento(HttpServletRequest request, Long chavePrimaria)
			throws GGASException {

		CronogramaFaturamento cronogramaFaturamento = controladorCronogramaFaturamento
				.obterCronogramaFaturamento(chavePrimaria);

		Map<String, String> mapaCamposGrupoFaturamento = controladorCronogramaFaturamento.mapearCamposGrupoFaturamento(
				cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria(),
				cronogramaFaturamento.getChavePrimaria(), null, null);

		if (mapaCamposGrupoFaturamento != null) {
			request.getSession().setAttribute(QTD_ROTAS, mapaCamposGrupoFaturamento.get(QTD_ROTAS));
			request.getSession().setAttribute(QTD_MEDIDORES, mapaCamposGrupoFaturamento.get(QTD_MEDIDORES));

		}

		Periodicidade periodicidade = controladorRota
				.obterPeriodicidade(cronogramaFaturamento.getGrupoFaturamento().getPeriodicidade().getChavePrimaria());

		request.getSession().setAttribute(ANO_MES_CICLO,
				cronogramaFaturamento.getAnoMesFaturamentoMascara() + "-" + cronogramaFaturamento.getNumeroCiclo());
		request.getSession().setAttribute(DESCRICAO_GRUPO_FATU,
				cronogramaFaturamento.getGrupoFaturamento().getDescricao());
		request.getSession().setAttribute(DESCRICAO_PERIODICIDADE, periodicidade.getDescricao());

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(CHAVE_CRONOGRAMA_FATURAMENTO, chavePrimaria);
		return filtro;
	}

	private List<CronogramaRotaVO> obterListaCronogramaFaturamentoVO(Collection<CronogramaRota> listaCronogramaRota)
			throws GGASException {

		List<CronogramaRotaVO> listaCronogramaFaturamentoVO = new ArrayList<CronogramaRotaVO>();

		CronogramaRotaVO cronogramaRotaVO = null;

		for (CronogramaRota cronogramaRota : listaCronogramaRota) {
			cronogramaRotaVO = new CronogramaRotaVO();
			cronogramaRotaVO.setChavePrimaria(cronogramaRota.getChavePrimaria());
			cronogramaRotaVO.setDataPrevista(cronogramaRota.getDataPrevista());
			cronogramaRotaVO.setDataRealizacao(cronogramaRota.getDataRealizada());
			cronogramaRotaVO.setRota(cronogramaRota.getRota());
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_ROTA, cronogramaRota.getRota().getChavePrimaria());
			filtro.put(CHAVE_CRONOGRAMA, cronogramaRota.getChavePrimaria());
			Collection<RotaHistorico> listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro,
					Boolean.FALSE);
			if (CollectionUtils.isEmpty(listaRotaHistorico)) {
				filtro.remove(CHAVE_CRONOGRAMA);
				listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro, Boolean.FALSE);
				if (!CollectionUtils.isEmpty(listaRotaHistorico)) {
					RotaHistorico rotaHistorico = listaRotaHistorico.iterator().next();
					if (rotaHistorico.getLeiturista() != null) {
						cronogramaRotaVO.setLeituristaAnterior(rotaHistorico.getLeiturista());
					}
				}
			} else {
				RotaHistorico rotaHistorico = listaRotaHistorico.iterator().next();
				Date dataCadastro = rotaHistorico.getDataCadastro();
				filtro.put("dataCadastro", dataCadastro);
				filtro.remove(CHAVE_CRONOGRAMA);
				listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro, Boolean.TRUE);
				if (!CollectionUtils.isEmpty(listaRotaHistorico)
						&& listaRotaHistorico.iterator().next().getLeiturista() != null) {
					cronogramaRotaVO.setLeituristaAnterior(listaRotaHistorico.iterator().next().getLeiturista());
				}

				if (rotaHistorico.getLeiturista() != null) {
					cronogramaRotaVO.setLeituristaAtual(rotaHistorico.getLeiturista());
				}

				if (cronogramaRota.getRota().getLeituristaSuplente() != null) {
					cronogramaRotaVO.setLeituristaSuplente(rotaHistorico.getLeituristaSuplente());
				}
			}

			listaCronogramaFaturamentoVO.add(cronogramaRotaVO);
		}
		return listaCronogramaFaturamentoVO;
	}

	/**
	 * Método responsável pode realizar o escalonamento automático dos leituristas.
	 *
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("escalonarLeituristaAutomaticamente")
	public String escalonarLeituristaAutomaticamente(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		try {
			boolean escalonamentoRealizado = cronogramaVO.isEscalonamentoRealizado();

			List<CronogramaRotaVO> listaCronogramaRota = (ArrayList<CronogramaRotaVO>) request.getSession()
					.getAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO);

			request.getSession().setAttribute(LISTA_LEITURISTA, controladorRota.listarLeituristas());

			validarEscalonamento(listaCronogramaRota);
			if (!escalonamentoRealizado) {
				listaCronogramaRota = escalonar(listaCronogramaRota);
			}

			request.getSession().removeAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO);

			request.getSession().setAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO, listaCronogramaRota);

			cronogramaVO.setEscalonamentoRealizado(Boolean.TRUE);

			this.carregarCampos(model);

			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return VIEW_EXIBIR_ESCALONAMENTO_LEITURISTA;
	}

	/**
	 * Escalonar.
	 * 
	 * @param listaCronogramaRota the lista cronograma rota
	 * @return the array list
	 * @throws GGASException the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private List<CronogramaRotaVO> escalonar(List<CronogramaRotaVO> listaCronogramaRota) throws GGASException {

		Map<String, Object> dados = obterSequenciaParaEscalonamento(listaCronogramaRota);
		Boolean primeiroEscalonamento = (Boolean) dados.get(PRIMEIRO_ESCALONAMENTO);
		List<Leiturista> leituristas = (ArrayList<Leiturista>) dados.get(LISTA_LEITURISTAS);
		if (CollectionUtils.isEmpty(listaCronogramaRota)) {
			throw new NegocioException(Constantes.NAO_EXISTEM_LEITURISTA_CADASTRADOS, true);
		}

		realizarEscalonamento(listaCronogramaRota, leituristas, primeiroEscalonamento);

		return listaCronogramaRota;
	}

	/**
	 * Realizar escalonamento.
	 * 
	 * @param listaCronogramaRota the lista cronograma rota
	 * @param listaLeiturista     the lista leiturista
	 */
	private void realizarEscalonamento(List<CronogramaRotaVO> listaCronogramaRota, List<Leiturista> listaLeiturista,
			Boolean primeiroEscalonamento) {

		// Obtendo sequecia leituristas principais para o escalonamento atual
		if (!primeiroEscalonamento) {
			Leiturista leituristasPrincipais = listaLeiturista.get(0);
			listaLeiturista.remove(leituristasPrincipais);
			listaLeiturista.add(leituristasPrincipais);
		}
		// executa escalonamento dos leituristas principais
		int sizeListaLeiturista = listaLeiturista.size();
		int cont = 0;
		for (int i = 0; i < listaCronogramaRota.size(); i++) {
			if (cont == sizeListaLeiturista) {
				cont = 0;
			}

			listaCronogramaRota.get(i).setLeituristaAtual(listaLeiturista.get(cont));
			cont++;
		}

		// obtendo sequencia dos leituristas suplentes.
		Leiturista leituristaSuplente = listaLeiturista.get(0);
		listaLeiturista.remove(leituristaSuplente);
		listaLeiturista.add(leituristaSuplente);
		// realiza escalonamento dos leituristas suplentes.
		cont = 0;
		for (int i = 0; i < listaCronogramaRota.size(); i++) {
			if (cont == sizeListaLeiturista) {
				cont = 0;
			}

			listaCronogramaRota.get(i).setLeituristaSuplente(listaLeiturista.get(cont));
			cont++;
		}
	}

	/**
	 * Obter sequencia para escalonamento.
	 * 
	 * @param listaCronogramaRota the lista cronograma rota
	 * @return the array list
	 * @throws GGASException the GGAS exception
	 */
	private Map<String, Object> obterSequenciaParaEscalonamento(List<CronogramaRotaVO> listaCronogramaRota)
			throws GGASException {

		Map<String, Object> dadosEscalonamento = new HashMap<String, Object>();

		List<Leiturista> leituristaCadastrados = (ArrayList<Leiturista>) controladorRota.listarLeituristas();
		removerLeituristaNovos(leituristaCadastrados, listaCronogramaRota);

		int qtdLeituristas = leituristaCadastrados.size();
		// Obtendo Última Sequecia Escalonamento
		int cont = 1;
		List<Leiturista> leituristaEscalondosAnteriormente = new ArrayList<Leiturista>();
		if (!CollectionUtils.isEmpty(listaCronogramaRota)) {
			for (CronogramaRotaVO cronogramaRotaVO : listaCronogramaRota) {
				if (cronogramaRotaVO.getLeituristaAnterior() != null) {
					leituristaEscalondosAnteriormente.add(cronogramaRotaVO.getLeituristaAnterior());
					if (cont == qtdLeituristas) {
						break;
					}
					cont++;
				}
			}
		}

		if (CollectionUtils.isEmpty(leituristaEscalondosAnteriormente)) {
			dadosEscalonamento.put(PRIMEIRO_ESCALONAMENTO, Boolean.TRUE);
			dadosEscalonamento.put(LISTA_LEITURISTAS,
					(ArrayList<Leiturista>) controladorLeiturista.obterListaLeituristaEmOrdemAlfabetica(null));
			return dadosEscalonamento;
		}

		if (!CollectionUtils.isEmpty(leituristaEscalondosAnteriormente)) {
			List<Leiturista> novosLeiturista = verificaLeituristasNovos(leituristaEscalondosAnteriormente);
			for (Leiturista leiturista : novosLeiturista) {
				leituristaEscalondosAnteriormente.add(leiturista);
			}

			verificarRemocaoLeiturista(leituristaEscalondosAnteriormente);
		}

		dadosEscalonamento.put(LISTA_LEITURISTAS, leituristaEscalondosAnteriormente);
		dadosEscalonamento.put(PRIMEIRO_ESCALONAMENTO, Boolean.FALSE);
		return dadosEscalonamento;
	}

	/**
	 * Salvar escalonamento.
	 * 
	 * @param model         - {@link Model}
	 * @param request       - {@link HttpServletRequest}
	 * @param cronogramaVO  - {@link CronogramaVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("salvarEscalonamento")
	public String salvarEscalonamento(Model model, HttpServletRequest request, CronogramaVO cronogramaVO,
			BindingResult bindingResult) {

		String view = VIEW_EXIBIR_ESCALONAMENTO_LEITURISTA;

		try {
			List<CronogramaRotaVO> listaCronogramaRota = (ArrayList<CronogramaRotaVO>) request.getSession()
					.getAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO);

			boolean fluxoInclusao = (boolean) request.getSession().getAttribute("fluxoInclusao");

			validaPreenchimentoCampos(cronogramaVO);

			if (!fluxoInclusao) {
				this.salvarHistorico(listaCronogramaRota, cronogramaVO);
			} else {
				atualizarHistorico(listaCronogramaRota, cronogramaVO);
			}

			this.atualizarRotas(listaCronogramaRota, cronogramaVO);

			limparSessao(request);

			super.mensagemSucesso(model, Constantes.ESCALONAMENTO_REALIZADO_COM_SUCESSO);

			cronogramaVO.setHabilitado(String.valueOf(true));
			model.addAttribute(CRONOGRAMA_VO, cronogramaVO);

			view = this.exibirPesquisaCronograma(model, request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Salvar historico.
	 * 
	 * @param listaCronogramaRotaVO the lista cronograma rota vo
	 * @param form                  the form
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	private boolean salvarHistorico(List<CronogramaRotaVO> listaCronogramaRotaVO, CronogramaVO cronogramaVO)
			throws GGASException {

		Long[] leituristasAtuais = cronogramaVO.getLeituristaAtual();
		Long[] leituristasSuplentes = cronogramaVO.getLeituristaSuplente();

		Collection<RotaHistorico> listaHistoricoRota = new ArrayList<RotaHistorico>();

		Leiturista leituristaPrincipal = null;
		Leiturista leituristaSuplente = null;
		boolean campoNaoPreenchido = false;
		for (int i = 0; i < listaCronogramaRotaVO.size(); i++) {
			RotaHistorico rotahistorico = new RotaHistorico();
			if (leituristasAtuais[i] > 0) {
				leituristaPrincipal = (Leiturista) controladorLeiturista.obter(leituristasAtuais[i]);
				rotahistorico.setLeiturista(leituristaPrincipal);
			} else {
				campoNaoPreenchido = true;
			}
			if (leituristasSuplentes[i] > 0) {
				leituristaSuplente = (Leiturista) controladorLeiturista.obter(leituristasSuplentes[i]);
				rotahistorico.setLeituristaSuplente(leituristaSuplente);
			}
			rotahistorico.setRota(listaCronogramaRotaVO.get(i).getRota());
			rotahistorico.setCronogramaRota(
					controladorRota.obterCronogramaRota(listaCronogramaRotaVO.get(i).getChavePrimaria()));

			rotahistorico.setDataCadastro(new DateTime().withMillisOfSecond(0).toDate());
			listaHistoricoRota.add(rotahistorico);
		}

		if (campoNaoPreenchido) {
			throw new NegocioException(Constantes.CAMPOS_NAO_PREENCHIDOS, true);
		}

		return controladorRota.salvarRotaHistorico(listaHistoricoRota);
	}

	/**
	 * Atualizar rotas.
	 * 
	 * @param listaCronogramaRotaVO the lista cronograma rota vo
	 * @param form                  the form
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	private boolean atualizarRotas(List<CronogramaRotaVO> listaCronogramaRotaVO, CronogramaVO cronogramaVO)
			throws GGASException {

		Long[] leituristasAtuais = cronogramaVO.getLeituristaAtual();
		Long[] leituristasSuplentes = cronogramaVO.getLeituristaSuplente();

		Rota rota = null;
		for (int i = 0; i < listaCronogramaRotaVO.size(); i++) {
			rota = listaCronogramaRotaVO.get(i).getRota();
			if (leituristasAtuais[i] > 0) {
				rota.setLeiturista((Leiturista) controladorLeiturista.obter(leituristasAtuais[i]));
			}

			if (leituristasSuplentes[i] > 0) {
				rota.setLeituristaSuplente((Leiturista) controladorLeiturista.obter(leituristasSuplentes[i]));
			}
			if (rota != null) {
				controladorRota.atualizar(rota);
			}
		}

		return true;
	}

	/**
	 * Limpar sessao.
	 * 
	 * @param request the request
	 */
	private void limparSessao(HttpServletRequest request) {

		request.getSession().removeAttribute(QTD_ROTAS);
		request.getSession().removeAttribute(QTD_MEDIDORES);
		request.getSession().removeAttribute(ANO_MES_CICLO);
		request.getSession().removeAttribute(DESCRICAO_GRUPO_FATU);
		request.getSession().removeAttribute(DESCRICAO_PERIODICIDADE);
		request.getSession().removeAttribute(LISTA_CRONOGRAMA_FATURAMENTO_VO);
		request.getSession().removeAttribute(LISTA_LEITURISTA);
	}

	/**
	 * Verifica leituristas novos.
	 * 
	 * @param leituristasEscalonadosAnteriormente the leituristas escalonados
	 *                                            anteriormente
	 * @return the array list
	 * @throws GGASException the GGAS exception
	 */
	private List<Leiturista> verificaLeituristasNovos(List<Leiturista> leituristasEscalonadosAnteriormente)
			throws GGASException {

		List<Leiturista> leituristas = (ArrayList<Leiturista>) controladorRota.listarLeituristas();
		List<Leiturista> novosLeituristas = new ArrayList<Leiturista>();

		for (Leiturista leiturista : leituristas) {
			boolean existe = false;
			for (Leiturista leituristaEscalonado : leituristasEscalonadosAnteriormente) {
				if (leiturista.getChavePrimaria() == leituristaEscalonado.getChavePrimaria()) {
					existe = true;
				}
			}

			if (!existe) {
				novosLeituristas.add(leiturista);
			}
		}

		if (!CollectionUtils.isEmpty(novosLeituristas)) {
			List<Long> chavesPrimarias = new ArrayList<Long>();
			for (int i = 0; i < novosLeituristas.size(); i++) {
				chavesPrimarias.add(novosLeituristas.get(i).getChavePrimaria());
			}
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			novosLeituristas = (ArrayList<Leiturista>) controladorLeiturista
					.obterListaLeituristaEmOrdemAlfabetica(filtro);
		}

		return novosLeituristas;

	}

	/**
	 * Verificar remocao leiturista.
	 * 
	 * @param leituristasEscalonadosAnteriormente the leituristas escalonados
	 *                                            anteriormente
	 * @throws GGASException the GGAS exception
	 */
	// verifica se existe algum leiturista que foi inativado ou removido no GGAS
	private void verificarRemocaoLeiturista(List<Leiturista> leituristasEscalonadosAnteriormente) throws GGASException {

		List<Leiturista> leituristas = (ArrayList<Leiturista>) controladorRota.listarLeituristas();

		List<Leiturista> leituristasARemover = new ArrayList<Leiturista>();
		for (Leiturista leituristaEscalonado : leituristasEscalonadosAnteriormente) {
			boolean continuaNaLista = false;
			for (Leiturista leituristaCadastrado : leituristas) {
				if (leituristaEscalonado.getChavePrimaria() == leituristaCadastrado.getChavePrimaria()) {
					continuaNaLista = true;
				}
			}

			if (!continuaNaLista) {
				leituristasARemover.add(leituristaEscalonado);
			}
		}

		if (!CollectionUtils.isEmpty(leituristasARemover)) {
			for (Leiturista leiturista : leituristasARemover) {
				leituristasEscalonadosAnteriormente.remove(leiturista);
			}
		}
	}

	/**
	 * Validar escalonamento.
	 * 
	 * @param listaCronogramaRotaVO the lista cronograma rota vo
	 * @throws GGASException the GGAS exception
	 */
	private void validarEscalonamento(List<CronogramaRotaVO> listaCronogramaRotaVO) throws GGASException {

		boolean cronogramaEscalonado = false;

		for (CronogramaRotaVO cronogramaRotaVO : listaCronogramaRotaVO) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_CRONOGRAMA, cronogramaRotaVO.getChavePrimaria());
			filtro.put(CHAVE_ROTA, cronogramaRotaVO.getRota().getChavePrimaria());
			Collection<RotaHistorico> listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro,
					Boolean.FALSE);
			if (!CollectionUtils.isEmpty(listaRotaHistorico)) {
				cronogramaEscalonado = true;
				break;
			}
		}
		if (cronogramaEscalonado) {
			throw new NegocioException(Constantes.CRONOGRAMA_JA_ESCALONADO, true);
		}
	}

	/**
	 * Atualizar historico.
	 * 
	 * @param listaCronogramaRotaVO the lista cronograma rota vo
	 * @param form                  the form
	 * @throws GGASException the GGAS exception
	 */
	private void atualizarHistorico(List<CronogramaRotaVO> listaCronogramaRotaVO, CronogramaVO cronogramaVO)
			throws GGASException {

		Long[] leituristasAtuais = cronogramaVO.getLeituristaAtual();
		Long[] leituristasSuplentes = cronogramaVO.getLeituristaSuplente();

		boolean campoNaoPreenchido = false;
		for (int i = 0; i < listaCronogramaRotaVO.size(); i++) {
			if (leituristasAtuais[i] > 0) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(CHAVE_CRONOGRAMA, listaCronogramaRotaVO.get(i).getChavePrimaria());
				filtro.put(CHAVE_ROTA, listaCronogramaRotaVO.get(i).getRota().getChavePrimaria());
				Collection<RotaHistorico> listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro,
						Boolean.FALSE);
				if (!CollectionUtils.isEmpty(listaRotaHistorico)) {
					listaRotaHistorico.iterator().next()
							.setLeiturista((Leiturista) controladorLeiturista.obter(leituristasAtuais[i]));
				}

				if (leituristasSuplentes[i] > 0) {
					listaRotaHistorico.iterator().next()
							.setLeituristaSuplente((Leiturista) controladorLeiturista.obter(leituristasSuplentes[i]));
				}
				controladorRota.atualizarRotaHistorico(listaRotaHistorico.iterator().next());
			} else {
				campoNaoPreenchido = true;
			}

		}

		if (campoNaoPreenchido) {
			throw new NegocioException(Constantes.CAMPOS_NAO_PREENCHIDOS, true);
		}
	}

	/**
	 * Verifica fluxo.
	 * 
	 * @param listaCronogramaRotaVO the lista cronograma rota vo
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	// verificação para identificar se o fluxo é de alteração ou inclusão para o
	// histórico
	private boolean verificaFluxo(List<CronogramaRotaVO> listaCronogramaRotaVO) throws GGASException {

		boolean cronogramaEscalonado = false;

		for (CronogramaRotaVO cronogramaRotaVO : listaCronogramaRotaVO) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_CRONOGRAMA, cronogramaRotaVO.getChavePrimaria());
			filtro.put(CHAVE_ROTA, cronogramaRotaVO.getRota().getChavePrimaria());
			Collection<RotaHistorico> listaRotaHistorico = controladorRota.obterHistoricoRotaMaisRecente(filtro,
					Boolean.FALSE);
			if (!CollectionUtils.isEmpty(listaRotaHistorico)) {
				cronogramaEscalonado = true;
				break;
			}
		}
		return cronogramaEscalonado;
	}

	private void validaPreenchimentoCampos(CronogramaVO cronogramaVO) throws GGASException {

		Long[] leituristasAtuais = cronogramaVO.getLeituristaAtual();

		boolean campoNaoPreenchido = false;
		for (int i = 0; i < leituristasAtuais.length; i++) {
			if (leituristasAtuais[i] < 1) {
				campoNaoPreenchido = true;
				break;
			}
		}

		if (campoNaoPreenchido) {
			throw new NegocioException(Constantes.CAMPOS_NAO_PREENCHIDOS, true);
		}
	}

	// Remove o da lista de leituristas cadastrados os leituristas que não
	// participaram do último escalonamento
	private void removerLeituristaNovos(List<Leiturista> leituristasCadastrados,
			List<CronogramaRotaVO> listaCronogramaRota) {

		List<Leiturista> leituristasARemover = new ArrayList<Leiturista>();
		for (Leiturista leituristaCadastrado : leituristasCadastrados) {
			boolean participou = false;
			for (CronogramaRotaVO cronogramaVO : listaCronogramaRota) {
				if (cronogramaVO.getLeituristaAnterior() != null && leituristaCadastrado
						.getChavePrimaria() == cronogramaVO.getLeituristaAnterior().getChavePrimaria()) {
					participou = true;
				}
			}
			if (!participou) {
				leituristasARemover.add(leituristaCadastrado);
			}

		}
		for (Leiturista leituristaARemover : leituristasARemover) {
			leituristasCadastrados.remove(leituristaARemover);
		}
	}

	/**
	 * Responsável por redefinir o tratamento de {@link String} contendo decimais
	 * separados por vírgula.
	 *
	 * @param binder - {@link WebDataBinder}
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String[].class, new StringArrayPropertyEditor(null));
	}
}