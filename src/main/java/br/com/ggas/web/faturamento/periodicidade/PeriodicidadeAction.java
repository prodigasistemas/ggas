package br.com.ggas.web.faturamento.periodicidade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.impl.PeriodicidadeImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas operações referentes às periodicidades.
 * 
 * @author esantana
 *
 */
@Controller
public class PeriodicidadeAction extends GenericAction {

	private static final String LISTA_PERIODICIDADE = "listaPeriodicidade";

	private static final String PERIODICIDADE = "periodicidade";

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisa de periodicidades.
	 * 
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirPesquisarPeriodicidade")
	public String exibirPesquisarPeriodicidade(Model model) {

		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE);

		return "exibirPesquisarPeriodicidade";
	}

	/**
	 * Método responsável por realizar a pesquisa por periodicidades.
	 * 
	 * @param model - {@link Model}
	 * @param periodicidade - {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("pesquisarPeriodicidade")
	public String pesquisarPeriodicidade(Model model, PeriodicidadeImpl periodicidade, BindingResult bindingResult,
			@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(periodicidade, habilitado);

		Collection<TabelaAuxiliar> listaPeriodicidade =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, PeriodicidadeImpl.class.getName());

		model.addAttribute(LISTA_PERIODICIDADE, listaPeriodicidade);
		model.addAttribute(PERIODICIDADE, periodicidade);
		model.addAttribute(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);

		return "exibirPesquisarPeriodicidade";
	}

	/**
	 * Método responsável por exibir a tela de inserção de periodicidades.
	 * 
	 * @param model - {@link Model}
	 * @param periodicidade - {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirInserirPeriodicidade")
	public String exibirInserirPeriodicidade(Model model, PeriodicidadeImpl periodicidade, BindingResult bindingResult) {

		model.addAttribute(PERIODICIDADE, periodicidade);

		return "exibirInserirPeriodicidade";
	}

	/**
	 * Método responsável por realizar a inclusão de uma periodicidade.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param periodicidade- {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("inserirPeriodicidade")
	public String inserirPeriodicidade(Model model, HttpServletRequest request, PeriodicidadeImpl periodicidade,
			BindingResult bindingResult) {

		String view = null;

		try {
			getDadosAuditoria(request);
			this.verificarTamanhoCamposDeDescricao(periodicidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(periodicidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(periodicidade, "Periodicidade");
			controladorTabelaAuxiliar.verificarQuantidadeMinimaMaximaPeriodicidade(periodicidade);
			controladorTabelaAuxiliar.inserir(periodicidade);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Periodicidade.PERIODICIDADE_ROTULO);
			view = this.pesquisarPeriodicidade(model, periodicidade, bindingResult, periodicidade.isHabilitado());
		} catch (GGASException e) {
			view = "forward:/exibirInserirPeriodicidade";
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	private void verificarTamanhoCamposDeDescricao(Periodicidade periodicidade) throws GGASException {

		String descricao = periodicidade.getDescricao();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricao(descricao, PeriodicidadeImpl.class.getName());

		}

		String descricaoAbreviada = periodicidade.getDescricaoAbreviada();
		if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
			controladorTabelaAuxiliar.verificarTamanhoDescricaoAbreviada(descricaoAbreviada, PeriodicidadeImpl.class.getName());
		}
	}

	/**
	 * Método responsável por exibir a tela de atualização de periodicidades.
	 * 
	 * @param model - {@link Model}
	 * @param periodicidadeParam - {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAtualizarPeriodicidade")
	public String exibirAtualizarPeriodicidade(Model model, PeriodicidadeImpl periodicidadeParam, BindingResult bindingResult) {

		Periodicidade periodicidade = periodicidadeParam;

		String view = "exibirAtualizarPeriodicidade";

		try {
			periodicidade = (Periodicidade) controladorTabelaAuxiliar.obter(periodicidade.getChavePrimaria(), PeriodicidadeImpl.class);
			model.addAttribute(PERIODICIDADE, periodicidade);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			view = "forward:/exibirPesquisarPeriodicidade";
		}

		return view;
	}

	/**
	 * Método responsável por realizar a alteração de uma periodicidade.
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @param periodicidade - {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("atualizarPeriodicidade")
	public String atualizarPeriodicidade(Model model, HttpServletRequest request, PeriodicidadeImpl periodicidade,
			BindingResult bindingResult) {

		String view = null;

		model.addAttribute(PERIODICIDADE, periodicidade);

		try {
			this.verificarTamanhoCamposDeDescricao(periodicidade);
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(periodicidade);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(periodicidade, "Periodicidade");
			controladorTabelaAuxiliar.verificarQuantidadeMinimaMaximaPeriodicidade(periodicidade);
			controladorTabelaAuxiliar.atualizar(periodicidade, PeriodicidadeImpl.class);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Periodicidade.PERIODICIDADE_ROTULO);
			view = this.pesquisarPeriodicidade(model, periodicidade, bindingResult, periodicidade.isHabilitado());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "exibirAtualizarPeriodicidade";
		}

		return view;
	}

	/**
	 * Método responsável pela remoção de uma periodicidade.
	 * 
	 * @param periodicidade - {@link PeriodicidadeImpl}
	 * @param bindingResult - {@link BindingResult}
	 * @param chavesPrimarias
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 * @throws GGASException
	 */
	@RequestMapping("removerPeriodicidade")
	public String removerPeriodicidade(PeriodicidadeImpl periodicidade, BindingResult bindingResult,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) {

		String view = "forward:/pesquisarPeriodicidade";

		try {
			DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, PeriodicidadeImpl.class, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Periodicidade.PERIODICIDADE_ROTULO);
			view = this.pesquisarPeriodicidade(model, null, bindingResult, Boolean.TRUE);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por detalhar uma periodicidade.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalhamentoPeriodicidade")
	public String exibirDetalhamentoPeriodicidade(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			Model model) {

		try {
			Periodicidade periodicidade = (Periodicidade) controladorTabelaAuxiliar.obter(chavePrimaria, PeriodicidadeImpl.class);
			model.addAttribute(PERIODICIDADE, periodicidade);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:/exibirPesquisarPeriodicidade";
		}

		return "exibirDetalhamentoPeriodicidade";
	}

	/**
	 * Método responsável por preparar o filtro para pesquisa de Periodicidade.
	 * 
	 * @param periodicidade - {@link Periodicidade}
	 * @param habilitado - {@link Boolean}
	 * @return filtro - {@link Map}
	 */
	private Map<String, Object> prepararFiltro(Periodicidade periodicidade, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (periodicidade != null) {

			if (periodicidade.getChavePrimaria() > 0) {
				filtro.put(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, periodicidade.getChavePrimaria());
			}

			if (StringUtils.isNotEmpty(periodicidade.getDescricao())) {
				filtro.put(EntidadeConteudo.ATRIBUTO_DESCRICAO, periodicidade.getDescricao());
			}

			final String descricaoAbreviada = periodicidade.getDescricaoAbreviada();
			if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
				filtro.put("descricaoAbreviada", descricaoAbreviada);
			}
		}

		if (habilitado != null) {
			filtro.put(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado);
		}

		return filtro;
	}
}
