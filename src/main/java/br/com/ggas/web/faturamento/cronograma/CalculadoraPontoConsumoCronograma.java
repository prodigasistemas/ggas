/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.util.Fachada;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Classe responsável por calcular a quantidade de pontos de consumos existentes por atividade no cronograma informado
 * @author jose.victor@logiquesistemas.com.br
 */
@Component
public class CalculadoraPontoConsumoCronograma {

	private static final Long CHAVE_GERAR_DADOS = 1L;
	private static final Long CHAVE_REALIZAR = 2L;
	private static final Long CHAVE_RETORNAR = 3L;
	private static final Long CHAVE_CONSISTIR = 4L;
	private static final Long CODIGO_ATIVIDADE_FATURAR = 8L;
	private static final Long CODIGO_ATIVIDADE_EMITIR_FATURA = 9L;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;
	@Autowired
	private ControladorSupervisorio controladorSupervisorio;
	@Autowired
	private ControladorFatura controladorFatura;
	@Autowired
	private ControladorCronogramaAtividadeFaturamento controladorCronogramaAtividadeFaturamento;
	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	/**
	 * Gera um mapa com as quantidades de ponto de consumo por atividade sistema para um determinado grupo de
	 * faturamento e rota. Informando quantos pontos de consumo estão em cada atividade sistema. As atividades sistema
	 * retornadas são: Gerar Dados Realizar Leitura, Retornar Leitura e Consistir Leitura
	 * @param grupoFaturamento chave primária do grupo de faturamento
	 * @param anoMesFaturamento chave primária da rota
	 * @param ciclo número do ciclo
	 * @return retorna um mapa de atividade sistema por quantidades de ponto,
	 * @throws GGASException exceção lançada caso ocorra alguma falha ai buscar atividade do sistema
	 * por atividade
	 */
	public Map<AtividadeSistema, Long> gerarQuantidadesPontoConsumo(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo)
			throws GGASException {
		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(CHAVE_GERAR_DADOS);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_REALIZAR);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_RETORNAR);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_CONSISTIR);
		final AtividadeSistema faturarGrupo = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_FATURAR);

		return ImmutableMap.<AtividadeSistema, Long>builder()
				.put(gerarDados, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_GERAR_DADOS,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(realizarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_REALIZAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(retornarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_RETORNAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(consistirLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_CONSISTIR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(faturarGrupo, controladorFatura.consultarQuantidadePontosConsumoFatura(grupoFaturamento, anoMesFaturamento, ciclo))
				.build();
	}

	public Map<AtividadeSistema, Long> gerarQuantidadesPontoConsumo(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo,
			Long cronograma)
			throws GGASException {
		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(CHAVE_GERAR_DADOS);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_REALIZAR);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_RETORNAR);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_CONSISTIR);
		final AtividadeSistema faturarGrupo = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_FATURAR);
		final AtividadeSistema emitirFatura = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_EMITIR_FATURA);

		Long qtdEmitirFatura =controladorFatura.consultarQuantidadePontosConsumoFatura(grupoFaturamento, anoMesFaturamento, ciclo);
		
		Long qtdFaturarGrupo = controladorHistoricoConsumo.consultarQuantidadeQtdPontoConsumosAFaturar(grupoFaturamento,
				anoMesFaturamento, ciclo);
		
		Long qtdAnormalidadeNaoAnalisada = controladorHistoricoConsumo.
				consultarQuantidadeQtdPontoConsumoComanormalidadeBloqueiaFaturamentoNaoAnalisada(grupoFaturamento, anoMesFaturamento, ciclo);
		
		Long qtdConsistirLeitura = controladorLeituraMovimento.consultarQuantidadePontosConsumoConsistirLeitura(anoMesFaturamento, ciclo, grupoFaturamento) + qtdAnormalidadeNaoAnalisada;
		
		Long verificarEmitirFatura = controladorCronogramaAtividadeFaturamento.consultarQuantidadePontosConsumoEmissaoFatura(
				grupoFaturamento, anoMesFaturamento, ciclo, cronograma, CODIGO_ATIVIDADE_EMITIR_FATURA);
		
		if(verificarEmitirFatura > 0){
			qtdEmitirFatura = 0L;
		}

		return ImmutableMap.<AtividadeSistema, Long>builder()
				.put(gerarDados, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_GERAR_DADOS,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(realizarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(CHAVE_REALIZAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(retornarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoRegistrarLeitura(anoMesFaturamento, ciclo, grupoFaturamento))
				.put(consistirLeitura, qtdConsistirLeitura)
				.put(faturarGrupo, qtdFaturarGrupo)
				.put(emitirFatura, qtdEmitirFatura).build();
	}

	public Map<AtividadeSistema, Long> gerarQuantidadesPontoConsumoTotais(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo)
			throws GGASException {
		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(CHAVE_GERAR_DADOS);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_REALIZAR);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_RETORNAR);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_CONSISTIR);
		final AtividadeSistema faturarGrupo = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_FATURAR);
		final AtividadeSistema emitirFatura = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_EMITIR_FATURA);

		return ImmutableMap.<AtividadeSistema, Long>builder()
				.put(gerarDados, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CHAVE_GERAR_DADOS,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(realizarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CHAVE_REALIZAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(retornarLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CHAVE_RETORNAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(consistirLeitura, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CHAVE_CONSISTIR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(faturarGrupo, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CODIGO_ATIVIDADE_FATURAR,
						grupoFaturamento, anoMesFaturamento, ciclo))
				.put(emitirFatura, controladorLeituraMovimento.consultarQuantidadePontosConsumoTotaisNaAtividade(CODIGO_ATIVIDADE_EMITIR_FATURA,
						grupoFaturamento, anoMesFaturamento, ciclo)).build();
	}

	public Map<AtividadeSistema, Long> gerarQuantidadesPontoConsumoSupervisorio(Long grupoFaturamento, Integer anoMesFaturamento,
			Integer ciclo, Long cronograma, Long tipoLeitura) throws GGASException {

		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(CHAVE_GERAR_DADOS);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_REALIZAR);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_RETORNAR);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_CONSISTIR);
		final AtividadeSistema faturarGrupo = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_FATURAR);
		final AtividadeSistema emitirFatura = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_EMITIR_FATURA);

		Long qtdTotalPontoConsumoRota = controladorSupervisorio.consultarQuantidadePontosConsumoTotaisNaAtividade(grupoFaturamento,
				anoMesFaturamento, ciclo, tipoLeitura);

		Long qtdConsistirLeitura =
				qtdTotalPontoConsumoRota - controladorHistoricoConsumo.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(
						CHAVE_CONSISTIR, grupoFaturamento, anoMesFaturamento, ciclo);
		Long qtdFaturarGrupo =
				qtdTotalPontoConsumoRota - controladorHistoricoConsumo.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(
						CODIGO_ATIVIDADE_FATURAR, grupoFaturamento, anoMesFaturamento, ciclo);
		Long qtdEmitirFatura = qtdTotalPontoConsumoRota
				- controladorFatura.consultarQuantidadePontosConsumoFatura(grupoFaturamento, anoMesFaturamento, ciclo);
		
		Long verificarEmitirFatura = controladorCronogramaAtividadeFaturamento.consultarQuantidadePontosConsumoEmissaoFatura(
				grupoFaturamento, anoMesFaturamento, ciclo, cronograma, CODIGO_ATIVIDADE_EMITIR_FATURA);
		if(verificarEmitirFatura > 0){
			qtdEmitirFatura = 0L;
		}
		
		return ImmutableMap.<AtividadeSistema, Long> builder()
				.put(gerarDados, qtdTotalPontoConsumoRota -
						controladorSupervisorio.consultarQuantidadePontosConsumoNaAtividade(grupoFaturamento, anoMesFaturamento, ciclo,
								tipoLeitura))
				.put(realizarLeitura,
						controladorSupervisorio.consultarQuantidadePontosConsumoNaAtividade(grupoFaturamento, anoMesFaturamento, ciclo,
								tipoLeitura))
				.put(retornarLeitura,
						controladorSupervisorio.consultarQuantidadePontosConsumoNaAtividade(grupoFaturamento, anoMesFaturamento, ciclo,
								tipoLeitura))
				.put(consistirLeitura, qtdConsistirLeitura).put(faturarGrupo, qtdFaturarGrupo).put(emitirFatura, qtdEmitirFatura).build();

	}


	public Map<AtividadeSistema, Long> gerarQuantidadesPontoConsumoTotaisSupervisorio(Long grupoFaturamento, Integer anoMesFaturamento,
			Integer ciclo, Long leituraTipo) throws GGASException {

		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(CHAVE_GERAR_DADOS);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_REALIZAR);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_RETORNAR);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(CHAVE_CONSISTIR);
		final AtividadeSistema faturarGrupo = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_FATURAR);
		final AtividadeSistema emitirFatura = Fachada.getInstancia().obterAtividadeSistema(CODIGO_ATIVIDADE_EMITIR_FATURA);

		Long qtdPontoConsumo = controladorSupervisorio.consultarQuantidadePontosConsumoTotaisNaAtividade(grupoFaturamento, anoMesFaturamento,
				ciclo, leituraTipo);

		return ImmutableMap.<AtividadeSistema, Long> builder()
				.put(gerarDados,qtdPontoConsumo)
				.put(realizarLeitura,qtdPontoConsumo)
				.put(retornarLeitura,qtdPontoConsumo)
				.put(consistirLeitura,qtdPontoConsumo)
				.put(faturarGrupo,qtdPontoConsumo)
				.put(emitirFatura, qtdPontoConsumo)
				.build();
	}

}
