package br.com.ggas.web.faturamento.mensagemfaturamento;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.mensagem.ControladorMensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemCobranca;
import br.com.ggas.faturamento.mensagem.MensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemSegmento;
import br.com.ggas.faturamento.mensagem.MensagemVencimento;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas as Mensagens de Faturamento
 *
 */
@Controller
public class MensagemFaturamentoAction extends GenericAction {

	// by
	// hsilva on
	// 19/07/10
	// 11:30

	private static final String DATA_FINAL = "Data Final";

	private static final String TIPO_MENSAGEM = "tipoMensagem";

	private static final String LISTA_TIPO_MENSAGEM = "listaTiposMensagem";

	private static final String LISTA_SEGMENTOS_SELECIONADOS = "segmentosSelecionados";

	private static final String LISTA_SEGMENTOS_DISPONIVEIS = "segmentosDisponiveis";

	private static final String LISTA_RAMO_ATIVIDADE_SELECIONADOS = "ramoAtividadeSelecionados";

	private static final String LISTA_RAMO_ATIVIDADE_DISPONIVEIS = "ramoAtividadeDisponiveis";

	private static final String LISTA_FORMA_COBRANCA_SELECIONADOS = "formaCobrancaSelecionados";

	private static final String LISTA_FORMA_COBRANCA_DISPONIVEIS = "formaCobrancaDisponiveis";

	private static final String LISTA_DATA_VENC_SELECIONADOS = "dataVencimentoSelecionados";

	private static final String LISTA_DATA_VENC_DISPONIVEIS = "dataVencimentoDisponiveis";

	private static final String ID_IMOVEL = "idImovel";

	private static final String NOME_FANTASIA_IMOVEL = "nomeFantasiaImovel";

	private static final String MATRICULA_IMOVEL = "matriculaImovel";

	private static final String ENDERECO_IMOVEL = "enderecoImovel";

	private static final String CONDOMINIO_IMOVEL = "condominio";

	private static final String ID_TIPO_MENSAGEM = "idTipoMensagem";

	private static final String DESC_TIPO_MENSAGEM = "descricaoTipoMensagem";

	private static final String DATA_INICIO = "dataInicioVigencia";

	private static final String DATA_FIM = "dataFinalVigencia";

	private static final String MENSAGEM = "mensagem";

	private static final String HABILITADO = "habilitado";

	private static final String USADA = "usada";

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorMensagemFaturamento controladorMensagemFaturamento;

	@Autowired
	private ControladorRamoAtividade controladorRamoAtividade;

	@Autowired
	private ControladorImovel controladorImovel;

	/**
	 * Método responsável por exibir a tela de pesquisa de Mensagens de Faturamento.
	 * 
	 * @param model
	 *            {@link Model}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("exibirPesquisaMensagemFaturamento")
	public String exibirPesquisaMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO)
			throws GGASException {

		model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_TIPO_MENSAGEM, controladorEntidadeConteudo.listarTipoMensagem());

		mensagemFaturamentoVO.setHabilitado(Boolean.TRUE);
		model.addAttribute("mensagemFaturamentoVO", mensagemFaturamentoVO);

		return "exibirPesquisaMensagemFaturamento";
	}

	/**
	 * Método responsável por pesquisar de Mensagens de Faturamento.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("pesquisarMensagemFaturamento")
	public String pesquisarMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO)
			throws GGASException {

		Long idSegmento = mensagemFaturamentoVO.getIdSegmento();
		Long idTipoMensagem = mensagemFaturamentoVO.getIdTipoMensagem();
		String dataInicio = mensagemFaturamentoVO.getDataInicioVigencia();
		String dataFim = mensagemFaturamentoVO.getDataFinalVigencia();
		Boolean habilitado = mensagemFaturamentoVO.getHabilitado();

		Map<String, Object> filtro = new HashMap<String, Object>();
		if (idSegmento != null && idSegmento > 0) {
			filtro.put("segmento", idSegmento);
		}

		if (idTipoMensagem != null && idTipoMensagem > 0) {
			filtro.put(TIPO_MENSAGEM, idTipoMensagem);
		}

		if (dataInicio != null && !dataInicio.isEmpty() && dataFim != null && !dataFim.isEmpty()) {
			Date dtInicio = Util.converterCampoStringParaData("Data Inicial", dataInicio, Constantes.FORMATO_DATA_BR);
			Date dtFim = Util.converterCampoStringParaData(DATA_FINAL, dataFim, Constantes.FORMATO_DATA_BR);

			Util.validarIntervaloDatas(dtInicio, dtFim);

			filtro.put(DATA_INICIO, dtInicio);
			filtro.put(DATA_FIM, dtFim);
		} else if (dataInicio != null && !dataInicio.isEmpty()) {
			Date dtInicio = Util.converterCampoStringParaData("Data Inicial", dataInicio, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_INICIO, dtInicio);
		} else if (dataFim != null && !dataFim.isEmpty()) {
			Date dtFim = Util.converterCampoStringParaData(DATA_FINAL, dataFim, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_FIM, dtFim);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		Collection<MensagemFaturamento> colecaoMensagensFaturamento = controladorMensagemFaturamento
				.consultarMensagemFaturamento(filtro);

		model.addAttribute("listaMensagensFaturamento", colecaoMensagensFaturamento);
		model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, controladorSegmento.listarSegmento());
		model.addAttribute(LISTA_TIPO_MENSAGEM, controladorEntidadeConteudo.listarTipoMensagem());
		model.addAttribute("mensagemFaturamentoVO", mensagemFaturamentoVO);

		return "exibirPesquisaMensagemFaturamento";
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de Mensagens de
	 * Faturamento.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param chavePrimaria
	 *            {@link Long}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("exibirDetalhamentoMensagemFaturamento")
	public String exibirDetalhamentoMensagemFaturamento(Model model,
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) throws GGASException {

		try {
			MensagemFaturamento mensagemFaturamento = (MensagemFaturamento) controladorMensagemFaturamento
					.obter(chavePrimaria, TIPO_MENSAGEM);

			model.addAttribute(DESC_TIPO_MENSAGEM, mensagemFaturamento.getTipoMensagem().getDescricao());
			model.addAttribute(DATA_INICIO, mensagemFaturamento.getDataInicioVigenciaFormatada());
			model.addAttribute(DATA_FIM, mensagemFaturamento.getDataFimVigenciaFormatada());
			model.addAttribute(MENSAGEM, mensagemFaturamento.getDescricao());

			if (mensagemFaturamento.getImovel() != null) {
				model.addAttribute(NOME_FANTASIA_IMOVEL, mensagemFaturamento.getImovel().getNome());
				model.addAttribute(MATRICULA_IMOVEL, mensagemFaturamento.getImovel().getChavePrimaria());
				model.addAttribute(ENDERECO_IMOVEL, mensagemFaturamento.getImovel().getEnderecoFormatado());
			}

			List<Segmento> listaSegmento = mensagemFaturamento.getListaSegmentosOrderDescricao();
			List<MensagemSegmento> listaRamoAtividade = mensagemFaturamento
					.getListaSegmentosOrderPorRamoAtivdadeDescricao();

			carregarTabelaSegmentoRamoAtividade(listaSegmento, listaRamoAtividade, model);
			model.addAttribute(LISTA_FORMA_COBRANCA_SELECIONADOS, mensagemFaturamento.getListaFormasCobranca());
			model.addAttribute(LISTA_DATA_VENC_SELECIONADOS,
					mensagemFaturamento.getListMensagemVencimentoOrderDiaVencimento());
			model.addAttribute("chavePrimaria", chavePrimaria);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMensagemFaturamento";
		}
		return "exibirDetalhamentoMensagemFaturamento";
	}

	/**
	 * Carregar tabela segmento ramo atividade.
	 * 
	 * @param listaSegmento
	 * @param listaRamoAtividade
	 * @param model
	 * @param request
	 *            the request
	 */
	public void carregarTabelaSegmentoRamoAtividade(List<Segmento> listaSegmento,
			List<MensagemSegmento> listaRamoAtividade, Model model) {

		List<TabelaAuxiliar> listaSegmentoRamoAtividade = new ArrayList<TabelaAuxiliar>();

		for (Segmento segmento : listaSegmento) {
			TabelaAuxiliar tabelaAuxiliar = new TabelaAuxiliarImpl();
			// DESCRIÇÃO DO SEGMENTO
			tabelaAuxiliar.setDescricao(segmento.getDescricao());
			for (MensagemSegmento mensagemSegmento : listaRamoAtividade) {
				if (mensagemSegmento.getSegmento().getDescricao().equals(segmento.getDescricao())) {
					// DESCRIÇÃO DO RAMO DE ATIVIDADE
					tabelaAuxiliar.setDescricaoAbreviada(mensagemSegmento.getRamoAtividade().getDescricao());
				}
			}
			listaSegmentoRamoAtividade.add(tabelaAuxiliar);
		}
		model.addAttribute("listaSegmentoRamoAtividade", listaSegmentoRamoAtividade);
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Mensagens de Faturamento.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("exibirInclusaoMensagemFaturamento")
	public String exibirInclusaoMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO)
			throws GGASException {

		Long[] codigosSegmentosSelecionados = mensagemFaturamentoVO.getSegmentosSelecionados();
		Long[] codigosRamosAtividadeSelecionados = mensagemFaturamentoVO.getRamoAtividadeSelecionados();
		Long[] codigosFormasCobrancaSelecionados = mensagemFaturamentoVO.getFormaCobrancaSelecionados();
		Integer[] codigosVencimentosSelecionados = mensagemFaturamentoVO.getDataVencimentoSelecionados();

		Collection<Long> listaCodigosSelecionados = null;

		/*
		 * Verifica quais os SEGMENTOS que estão já estão selecionados e quais não
		 * estão. Divide em duas coleções (uma de selecionadas e outra de disponíveis) e
		 * disponibiliza no request
		 */
		Collection<Segmento> listaSegmentos = controladorSegmento.listarSegmento();
		Collection<Segmento> listaSegmentosDisponiveis = new ArrayList<Segmento>();
		Collection<Segmento> listaSegmentosSelecionados = new ArrayList<Segmento>();

		if (codigosSegmentosSelecionados != null && codigosSegmentosSelecionados.length > 0) {
			listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosSegmentosSelecionados));
			for (Segmento segmento : listaSegmentos) {
				if (listaCodigosSelecionados.contains(segmento.getChavePrimaria())) {
					listaSegmentosSelecionados.add(segmento);
				} else {
					listaSegmentosDisponiveis.add(segmento);
				}
			}
			model.addAttribute(LISTA_SEGMENTOS_SELECIONADOS, listaSegmentosSelecionados);
			model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, listaSegmentosDisponiveis);

			/*
			 * Verifica qual os RAMOS DE ATIVIDADE devem ser exibidos e quais desses já
			 * estão selecionados. Divide em duas coleções (uma de selecionadas e outra de
			 * disponíveis) e disponibiliza no request
			 */
			Collection<RamoAtividade> listaRamosAtividade = controladorRamoAtividade
					.consultarRamoAtividadePorSegmentos(codigosSegmentosSelecionados);
			Collection<RamoAtividade> listaRamosAtividadeDisponiveis = new ArrayList<RamoAtividade>();
			Collection<RamoAtividade> listaRamosAtividadeSelecionados = new ArrayList<RamoAtividade>();

			if (codigosRamosAtividadeSelecionados != null && codigosRamosAtividadeSelecionados.length > 0) {
				listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosRamosAtividadeSelecionados));
				for (RamoAtividade ramoAtividade : listaRamosAtividade) {
					if (listaCodigosSelecionados.contains(ramoAtividade.getChavePrimaria())) {
						listaRamosAtividadeSelecionados.add(ramoAtividade);
					} else {
						listaRamosAtividadeDisponiveis.add(ramoAtividade);
					}
				}
				model.addAttribute(LISTA_RAMO_ATIVIDADE_SELECIONADOS, listaRamosAtividadeSelecionados);
				model.addAttribute(LISTA_RAMO_ATIVIDADE_DISPONIVEIS, listaRamosAtividadeDisponiveis);
			} else {
				model.addAttribute(LISTA_RAMO_ATIVIDADE_DISPONIVEIS, listaRamosAtividade);
			}

		} else {
			model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, listaSegmentos);
		}

		/*
		 * Verifica quais as FORMAS DE COBRANÇA que estão já estão selecionadas e quais
		 * não estão. Divide em duas coleções (uma de selecionadas e outra de
		 * disponíveis) e disponibiliza no request
		 */
		Collection<EntidadeConteudo> listaFormasCobranca = controladorEntidadeConteudo.listarFormaCobranca();
		Collection<EntidadeConteudo> listaFormasCobrancaDisponiveis = new ArrayList<EntidadeConteudo>();
		Collection<EntidadeConteudo> listaFormasCobrancaSelecionados = new ArrayList<EntidadeConteudo>();

		if (codigosFormasCobrancaSelecionados != null && codigosFormasCobrancaSelecionados.length > 0) {
			listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosFormasCobrancaSelecionados));
			for (EntidadeConteudo formaCobranca : listaFormasCobranca) {
				if (listaCodigosSelecionados.contains(formaCobranca.getChavePrimaria())) {
					listaFormasCobrancaSelecionados.add(formaCobranca);
				} else {
					listaFormasCobrancaDisponiveis.add(formaCobranca);
				}
			}
			model.addAttribute(LISTA_FORMA_COBRANCA_SELECIONADOS, listaFormasCobrancaSelecionados);
			model.addAttribute(LISTA_FORMA_COBRANCA_DISPONIVEIS, listaFormasCobrancaDisponiveis);
		} else {
			model.addAttribute(LISTA_FORMA_COBRANCA_DISPONIVEIS, listaFormasCobranca);
		}

		/*
		 * Verifica quais as DATAS DE VENCIMENTO que estão já estão selecionadas e quais
		 * não estão. Divide em duas coleções (uma de selecionadas e outra de
		 * disponíveis) e disponibiliza no request
		 */
		Collection<Integer> listaVencimentosDisponiveis = new ArrayList<Integer>();
		Collection<Integer> listaVencimentosSelecionados = new ArrayList<Integer>();

		Collection<Integer> listaInteirosSelecionados = new ArrayList<Integer>();
		if (codigosVencimentosSelecionados != null) {
			listaInteirosSelecionados.addAll(Arrays.asList(codigosVencimentosSelecionados));
		}

		for (int i = 1; i <= 31; i++) {
			if (listaInteirosSelecionados.contains(i)) {
				listaVencimentosSelecionados.add(i);
			} else {
				listaVencimentosDisponiveis.add(i);
			}
		}

		model.addAttribute(LISTA_DATA_VENC_SELECIONADOS, listaVencimentosSelecionados);
		model.addAttribute(LISTA_DATA_VENC_DISPONIVEIS, listaVencimentosDisponiveis);

		model.addAttribute(LISTA_TIPO_MENSAGEM, controladorEntidadeConteudo.listarTipoMensagem());

		return "exibirInclusaoMensagemFaturamento";
	}

	/**
	 * Método responsável por incluir uma nova mensagem de fatura.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("inserirMensagemFaturamento")
	public String inserirMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO,
			HttpServletRequest request) throws Exception {

		String dataInicio = mensagemFaturamentoVO.getDataInicioVigencia();
		String dataFim = mensagemFaturamentoVO.getDataFinalVigencia();

		MensagemFaturamento mensagemFaturamento = controladorMensagemFaturamento.criarMensagemFaturamento();

		try {
			popularMensagemFaturamento(model, mensagemFaturamento, mensagemFaturamentoVO, request);
			popularColecoes(model, mensagemFaturamento);

			if (!Util.isDataValida(dataInicio, Constantes.FORMATO_DATA_BR)) {
				String dataInicioVigencia = MensagemUtil.obterMensagem(
						ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, MensagemUtil.LOCALE),
						Constantes.MENSAGEM_FATURAMENTO_INICIO_VIGENCIA);

				model.addAttribute(DATA_INICIO, dataInicio);
				model.addAttribute(DATA_FIM, dataFim);
				throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, dataInicioVigencia);
			}

			if (!Util.isDataValida(dataFim, Constantes.FORMATO_DATA_BR)) {
				String dataFimVigencia = MensagemUtil.obterMensagem(
						ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, MensagemUtil.LOCALE),
						Constantes.MENSAGEM_FATURAMENTO_FIM_VIGENCIA);

				model.addAttribute(DATA_INICIO, dataInicio);
				model.addAttribute(DATA_FIM, dataFim);
				throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, dataFimVigencia);
			}

			controladorMensagemFaturamento.inserirMensagemFaturamento(mensagemFaturamento);

		} catch (FormatoInvalidoException e) {
			popularForm(model, mensagemFaturamento);
			super.mensagemErroParametrizado(model, request, e);
			return "exibirInclusaoMensagemFaturamento";
		} catch (GGASException e) {
			popularForm(model, mensagemFaturamento);
			super.mensagemErroParametrizado(model, request, e);
			return exibirInclusaoMensagemFaturamento(model, mensagemFaturamentoVO);
		}
		mensagemFaturamentoVO.setHabilitado(Boolean.TRUE);
		popularForm(model, mensagemFaturamento);
		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request,
				MensagemFaturamento.MENSAGENS_FATURAMENTO);

		return pesquisarMensagemFaturamento(model, mensagemFaturamentoVO);
	}

	/**
	 * Método responsável por exibir a tela de Alteração de Mensagens de
	 * Faturamento.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @param chavePrimaria
	 *            {@link Long}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */

	@RequestMapping("exibirAlteracaoMensagemFaturamento")
	public String exibirAlteracaoMensagemFaturamento(Model model, HttpServletRequest request,
			MensagemFaturamentoVO mensagemFaturamentoVO,
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) throws GGASException {

		try {
			MensagemFaturamento mensagemFaturamento = (MensagemFaturamento) controladorMensagemFaturamento
					.obter(chavePrimaria, TIPO_MENSAGEM);
			if (isPostBack(request)) {
				popularMensagemFaturamento(model, mensagemFaturamento, mensagemFaturamentoVO, request);
			}
			model.addAttribute("chavePrimaria", chavePrimaria);
			popularForm(model, mensagemFaturamento);
			popularColecoes(model, mensagemFaturamento);
			popularFlagUtilizacao(request, mensagemFaturamento);

		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaMensagemFaturamento";
		}

		return "exibirAlteracaoMensagemFaturamento";
	}

	/**
	 * Método Responsável por Alterar uma Mensagem de Fatura já existente.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param chavePrimaria
	 *            {@link Long}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("alterarMensagemFaturamento")
	public String alterarMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO,
			HttpServletRequest request, @RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria)
			throws GGASException {

		String dataInicio = mensagemFaturamentoVO.getDataInicioVigencia();
		String dataFim = mensagemFaturamentoVO.getDataFinalVigencia();

		MensagemFaturamento mensagemFaturamento = (MensagemFaturamento) controladorMensagemFaturamento
				.obter(chavePrimaria);

		String view = null;

		try {
			popularMensagemFaturamento(model, mensagemFaturamento, mensagemFaturamentoVO, request);

			popularColecoes(model, mensagemFaturamento);

			if (mensagemFaturamentoVO.getHabilitado() == null) {
				mensagemFaturamentoVO.setHabilitado(Boolean.TRUE);
				model.addAttribute("habilitado", mensagemFaturamentoVO.getHabilitado());
			}

			if (!Util.isDataValida(dataInicio, Constantes.FORMATO_DATA_BR)) {
				String dataInicioVigencia = MensagemUtil.obterMensagem(
						ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, MensagemUtil.LOCALE),
						Constantes.MENSAGEM_FATURAMENTO_INICIO_VIGENCIA);
				model.addAttribute(DATA_INICIO, dataInicio);
				model.addAttribute(DATA_FIM, dataFim);
				throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, dataInicioVigencia);
			}

			if (!Util.isDataValida(dataFim, Constantes.FORMATO_DATA_BR)) {
				String dataFimVigencia = MensagemUtil.obterMensagem(
						ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, MensagemUtil.LOCALE),
						Constantes.MENSAGEM_FATURAMENTO_FIM_VIGENCIA);
				model.addAttribute(DATA_INICIO, dataInicio);
				model.addAttribute(DATA_FIM, dataFim);
				throw new FormatoInvalidoException(Constantes.ERRO_FORMATO_INVALIDO_CAMPOS, dataFimVigencia);
			}

			Map<String, Object> erros = mensagemFaturamento.validarDados();

			if (erros.isEmpty()) {
				controladorMensagemFaturamento.alterarMensagemFaturamento(mensagemFaturamento);
				super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request,
						MensagemFaturamento.MENSAGENS_FATURAMENTO);
				view = this.pesquisarMensagemFaturamento(model, mensagemFaturamentoVO);
			} else if (!erros.isEmpty()) {

				String parametrosMensagemErro = MensagemUtil.obterMensagemErro(
						erros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS).toString(), Constantes.LOCALE_PADRAO);

				throw new FormatoInvalidoException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, parametrosMensagemErro);
			}
		} catch (FormatoInvalidoException e) {
			super.mensagemErroParametrizado(model, request, e);
			popularForm(model, mensagemFaturamento);
			view = "exibirAlteracaoMensagemFaturamento";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoMensagemFaturamento(model, request, mensagemFaturamentoVO, chavePrimaria);
		}

		return view;
	}

	/**
	 * Método responsável por remover uma mensagem de fatura.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param chavePrimarias
	 *            {@link Long}
	 * @return view {@link String}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	@RequestMapping("removerMensagemFaturamento")
	public String removerMensagemFaturamento(Model model, MensagemFaturamentoVO mensagemFaturamentoVO,
			HttpServletRequest request,
			@RequestParam(value = "chavesPrimarias", required = false) Long[] chavesPrimarias) throws GGASException {

		controladorMensagemFaturamento.removerMensagemFaturamento(chavesPrimarias, getDadosAuditoria(request));

		super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request,
				MensagemFaturamento.MENSAGENS_FATURAMENTO);

		return pesquisarMensagemFaturamento(model, mensagemFaturamentoVO);

	}

	/**
	 * Como não é possível julgar se uma mensagem de faturamento está sendo
	 * utilizada no GGÁS, foi admitido que: se a data de início da vigência for
	 * anterior a data atual, a mensagem de faturamento pode ter sido usada. Neste
	 * caso, o usuário da aplicação não pode alterar a data de início de vigência.
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param mensagemFaturamento
	 *            {@link MensagemFaturamento}
	 */
	private void popularFlagUtilizacao(HttpServletRequest request, MensagemFaturamento mensagemFaturamento) {
		Date dataAtual = new Date();
		Date dataInicioVigencia = mensagemFaturamento.getDataInicioVigencia();
		int resultadoComparacao = dataAtual.compareTo(dataInicioVigencia);
		boolean mensagemFaturamentoUsada = resultadoComparacao >= 0;
		request.setAttribute(USADA, mensagemFaturamentoUsada);
	}

	/**
	 * Método responsável por popular uma Mensagem de Faturamento de acordo com um
	 * Forumulário Dinâmico.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamento
	 *            {@link MensagemFaturamento}
	 * @param mensagemFaturamentoVO
	 *            {@link MensagemFaturamentoVO}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return MensagemFaturamento populada
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */
	private void popularMensagemFaturamento(Model model, MensagemFaturamento mensagemFaturamento,
			MensagemFaturamentoVO mensagemFaturamentoVO, HttpServletRequest request) throws GGASException {

		mensagemFaturamento.setDadosAuditoria(getDadosAuditoria(request));

		if (mensagemFaturamentoVO.getVersao() != null && mensagemFaturamentoVO.getVersao() > 0) {
			mensagemFaturamento.setVersao(mensagemFaturamentoVO.getVersao());
		}

		if (mensagemFaturamentoVO.getIdTipoMensagem() != null && mensagemFaturamentoVO.getIdTipoMensagem() > 0) {
			EntidadeConteudo tipoMensagem = controladorEntidadeConteudo
					.obter(mensagemFaturamentoVO.getIdTipoMensagem());
			mensagemFaturamento.setTipoMensagem(tipoMensagem);
		} else {
			mensagemFaturamento.setTipoMensagem(null);
		}

		if (!StringUtils.isEmpty(mensagemFaturamentoVO.getDataInicioVigencia())) {
			Date dataInicioVigencia = null;

			if (Util.isDataValida(mensagemFaturamentoVO.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR)) {
				dataInicioVigencia = Util.converterCampoStringParaData("Data de Início",
						mensagemFaturamentoVO.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR);
			}

			mensagemFaturamento.setDataInicioVigencia(dataInicioVigencia);
		} else {
			mensagemFaturamento.setDataInicioVigencia(null);
		}

		if (!StringUtils.isEmpty(mensagemFaturamentoVO.getDataFinalVigencia())) {
			Date dataFimVigencia = null;

			if (Util.isDataValida(mensagemFaturamentoVO.getDataFinalVigencia(), Constantes.FORMATO_DATA_BR)) {
				dataFimVigencia = Util.converterCampoStringParaData(DATA_FINAL,
						mensagemFaturamentoVO.getDataFinalVigencia(), Constantes.FORMATO_DATA_BR);
			}

			mensagemFaturamento.setDataFimVigencia(dataFimVigencia);
		} else {
			mensagemFaturamento.setDataFimVigencia(null);
		}

		if (mensagemFaturamentoVO.getIdImovel() != null && mensagemFaturamentoVO.getIdImovel() > 0) {
			Imovel imovel = (Imovel) controladorImovel.obter(mensagemFaturamentoVO.getIdImovel());
			mensagemFaturamento.setImovel(imovel);
		} else {
			mensagemFaturamento.setImovel(null);
		}

		if (mensagemFaturamentoVO.getHabilitado() != null) {
			mensagemFaturamento.setHabilitado(mensagemFaturamentoVO.getHabilitado());
		}

		if (!StringUtils.isEmpty(mensagemFaturamentoVO.getMensagem())) {
			mensagemFaturamento.setDescricao(mensagemFaturamentoVO.getMensagem().trim());
		} else {
			mensagemFaturamento.setDescricao(null);
		}

		if (mensagemFaturamento.getListaFormasCobranca() != null) {
			mensagemFaturamento.getListaFormasCobranca().clear();
		}
		Set<MensagemCobranca> listaCobrancas = new HashSet<MensagemCobranca>();
		if (mensagemFaturamentoVO.getFormaCobrancaSelecionados() != null
				&& mensagemFaturamentoVO.getFormaCobrancaSelecionados().length > 0) {
			for (int i = 0; i < mensagemFaturamentoVO.getFormaCobrancaSelecionados().length; i++) {
				EntidadeConteudo formaCobranca = controladorEntidadeConteudo
						.obter(mensagemFaturamentoVO.getFormaCobrancaSelecionados()[i]);
				MensagemCobranca mensagemCobranca = controladorMensagemFaturamento.criarMensagemCobranca();
				mensagemCobranca.setFormaCobranca(formaCobranca);
				mensagemCobranca.setMensagemFaturamento(mensagemFaturamento);
				mensagemCobranca.setDadosAuditoria(getDadosAuditoria(request));
				mensagemCobranca.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaCobrancas.add(mensagemCobranca);
			}
			model.addAttribute(LISTA_FORMA_COBRANCA_SELECIONADOS, listaCobrancas);
		}
		if (mensagemFaturamento.getListaFormasCobranca() != null) {
			mensagemFaturamento.getListaFormasCobranca().addAll(listaCobrancas);
		} else {
			mensagemFaturamento.setListaFormasCobranca(listaCobrancas);
		}

		if (mensagemFaturamento.getListaVencimentos() != null) {
			mensagemFaturamento.getListaVencimentos().clear();
		}
		Set<MensagemVencimento> listaVencimentos = new HashSet<MensagemVencimento>();
		if (mensagemFaturamentoVO.getDataVencimentoSelecionados() != null
				&& mensagemFaturamentoVO.getDataVencimentoSelecionados().length > 0) {
			for (int i = 0; i < mensagemFaturamentoVO.getDataVencimentoSelecionados().length; i++) {
				MensagemVencimento mensagemVencimento = controladorMensagemFaturamento.criarMensagemVencimento();
				mensagemVencimento.setDiaVencimento(mensagemFaturamentoVO.getDataVencimentoSelecionados()[i]);
				mensagemVencimento.setMensagemFaturamento(mensagemFaturamento);
				mensagemVencimento.setDadosAuditoria(getDadosAuditoria(request));
				mensagemVencimento.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaVencimentos.add(mensagemVencimento);
			}
			model.addAttribute(LISTA_DATA_VENC_SELECIONADOS, listaVencimentos);
		}
		if (mensagemFaturamento.getListaVencimentos() != null) {
			mensagemFaturamento.getListaVencimentos().addAll(listaVencimentos);
		} else {
			mensagemFaturamento.setListaVencimentos(listaVencimentos);
		}

		if (mensagemFaturamento.getListaSegmentos() != null) {
			mensagemFaturamento.getListaSegmentos().clear();
		}
		Set<MensagemSegmento> listaMensagemSegmentosRamoAtividade = new HashSet<MensagemSegmento>();
		List<RamoAtividade> listaRamosAtividade = new ArrayList<RamoAtividade>();
		Set<Long> listaCodigosSegmentos = new HashSet<Long>();
		iteraRamosAtividade(mensagemFaturamento, request, mensagemFaturamentoVO.getRamoAtividadeSelecionados(),
				listaMensagemSegmentosRamoAtividade, listaRamosAtividade, listaCodigosSegmentos);
		if (mensagemFaturamento.getListaSegmentos() != null) {
			mensagemFaturamento.getListaSegmentos().addAll(listaMensagemSegmentosRamoAtividade);
		} else {
			mensagemFaturamento.setListaSegmentos(listaMensagemSegmentosRamoAtividade);
		}

		Set<MensagemSegmento> listaMensagemSegmentos = new HashSet<MensagemSegmento>();
		if (mensagemFaturamentoVO.getSegmentosSelecionados() != null
				&& mensagemFaturamentoVO.getSegmentosSelecionados().length > 0) {
			for (int i = 0; i < mensagemFaturamentoVO.getSegmentosSelecionados().length; i++) {
				if (!listaCodigosSegmentos.contains(mensagemFaturamentoVO.getSegmentosSelecionados()[i])) {
					Segmento segmento = controladorSegmento
							.obterSegmento(mensagemFaturamentoVO.getSegmentosSelecionados()[i]);
					MensagemSegmento mensagemSegmento = controladorMensagemFaturamento.criarMensagemSegmento();
					mensagemSegmento.setMensagemFaturamento(mensagemFaturamento);
					mensagemSegmento.setSegmento(segmento);
					mensagemSegmento.setRamoAtividade(null);
					mensagemSegmento.setDadosAuditoria(getDadosAuditoria(request));
					mensagemSegmento.setUltimaAlteracao(Calendar.getInstance().getTime());
					listaMensagemSegmentos.add(mensagemSegmento);
				}
			}
			mensagemFaturamento.getListaSegmentos().addAll(listaMensagemSegmentos);
		}
	}

	private void iteraRamosAtividade(MensagemFaturamento mensagemFaturamento, HttpServletRequest request,
			Long[] ramosAtividade, Set<MensagemSegmento> listaMensagemSegmentosRamoAtividade,
			List<RamoAtividade> listaRamosAtividade, Set<Long> listaCodigosSegmentos) throws GGASException {
		if (ramosAtividade != null && ramosAtividade.length > 0) {
			for (int i = 0; i < ramosAtividade.length; i++) {
				RamoAtividade ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(ramosAtividade[i]);
				listaRamosAtividade.add(ramoAtividade);
				MensagemSegmento mensagemSegmento = controladorMensagemFaturamento.criarMensagemSegmento();
				mensagemSegmento.setMensagemFaturamento(mensagemFaturamento);
				mensagemSegmento.setSegmento(ramoAtividade.getSegmento());
				mensagemSegmento.setRamoAtividade(ramoAtividade);
				mensagemSegmento.setDadosAuditoria(getDadosAuditoria(request));
				mensagemSegmento.setUltimaAlteracao(Calendar.getInstance().getTime());
				listaMensagemSegmentosRamoAtividade.add(mensagemSegmento);
				listaCodigosSegmentos.add(ramoAtividade.getSegmento().getChavePrimaria());
			}

		}
	}

	/**
	 * Popular colecoes.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamento
	 *            {@linkMensagemFaturamento}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularColecoes(Model model, MensagemFaturamento mensagemFaturamento) throws GGASException {

		Collection<Long> listaCodigosSelecionados = null;

		/*
		 * Verifica quais os SEGMENTOS que estão já estão selecionados e quais não
		 * estão. Divide em duas coleções (uma de selecionadas e outra de disponíveis) e
		 * disponibiliza no request
		 */
		Collection<Segmento> listaSegmentos = controladorSegmento.listarSegmento();
		Collection<Segmento> listaSegmentosDisponiveis = new ArrayList<Segmento>();
		Collection<Segmento> listaSegmentosSelect = new ArrayList<Segmento>();
		Collection<Segmento> listaSegmentosSelecionados = mensagemFaturamento.getListaSegmentosOrderDescricao();

		Long[] codigosSegmentosSelecionados = Util.collectionParaArrayChavesPrimarias(listaSegmentosSelecionados);
		if (codigosSegmentosSelecionados != null && codigosSegmentosSelecionados.length > 0) {
			listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosSegmentosSelecionados));
			for (Segmento segmento : listaSegmentos) {
				if (listaCodigosSelecionados.contains(segmento.getChavePrimaria())) {
					listaSegmentosSelect.add(segmento);
				} else {
					listaSegmentosDisponiveis.add(segmento);
				}
			}
			model.addAttribute(LISTA_SEGMENTOS_SELECIONADOS, listaSegmentosSelect);
			model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, listaSegmentosDisponiveis);

			/*
			 * Verifica qual os RAMOS DE ATIVIDADE devem ser exibidos e quais desses já
			 * estão selecionados. Divide em duas coleções (uma de selecionadas e outra de
			 * disponíveis) e disponibiliza no request
			 */
			Collection<RamoAtividade> listaRamosAtividade = controladorRamoAtividade
					.consultarRamoAtividadePorSegmentos(codigosSegmentosSelecionados);
			Collection<RamoAtividade> listaRamosAtividadeDisponiveis = new ArrayList<RamoAtividade>();
			Collection<RamoAtividade> listaRamosAtividadeSelect = new ArrayList<RamoAtividade>();
			Collection<RamoAtividade> listaRamosAtividadeSelecionados = mensagemFaturamento
					.getListaRamosAtividadeOrderDescricao();
			Long[] codigosRamosAtividadeSelecionados = Util
					.collectionParaArrayChavesPrimarias(listaRamosAtividadeSelecionados);
			if (codigosRamosAtividadeSelecionados != null && codigosRamosAtividadeSelecionados.length > 0) {
				listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosRamosAtividadeSelecionados));
				for (RamoAtividade ramoAtividade : listaRamosAtividade) {
					String descricaoFormatada = ramoAtividade.getDescricao() + " - Segmento: "
							+ ramoAtividade.getSegmento().getDescricao();
					ramoAtividade.setDescricao(descricaoFormatada);
					if (listaCodigosSelecionados.contains(ramoAtividade.getChavePrimaria())) {
						listaRamosAtividadeSelect.add(ramoAtividade);
					} else {
						listaRamosAtividadeDisponiveis.add(ramoAtividade);
					}
				}
				model.addAttribute(LISTA_RAMO_ATIVIDADE_SELECIONADOS, listaRamosAtividadeSelect);
				model.addAttribute(LISTA_RAMO_ATIVIDADE_DISPONIVEIS, listaRamosAtividadeDisponiveis);

			} else {
				model.addAttribute(LISTA_RAMO_ATIVIDADE_DISPONIVEIS, listaRamosAtividade);
			}

		} else {
			model.addAttribute(LISTA_SEGMENTOS_DISPONIVEIS, listaSegmentos);
		}

		/*
		 * Verifica quais as FORMAS DE COBRANÇA que estão já estão selecionadas e quais
		 * não estão. Divide em duas coleções (uma de selecionadas e outra de
		 * disponíveis) e disponibiliza no request
		 */
		Collection<EntidadeConteudo> listaFormasCobranca = controladorEntidadeConteudo.listarFormaCobranca();
		Collection<EntidadeConteudo> listaFormasCobrancaDisponiveis = new ArrayList<EntidadeConteudo>();
		Collection<EntidadeConteudo> listaFormasCobrancaSelect = new ArrayList<EntidadeConteudo>();
		Collection<EntidadeConteudo> listaFormasCobrancaSelecionados = mensagemFaturamento
				.getListaNaoRepetidaFormasCobranca();
		Long[] codigosFormasCobrancaSelecionados = Util
				.collectionParaArrayChavesPrimarias(listaFormasCobrancaSelecionados);

		if (codigosFormasCobrancaSelecionados != null && codigosFormasCobrancaSelecionados.length > 0) {
			listaCodigosSelecionados = new ArrayList<Long>(Arrays.asList(codigosFormasCobrancaSelecionados));
			for (EntidadeConteudo formaCobranca : listaFormasCobranca) {
				if (listaCodigosSelecionados.contains(formaCobranca.getChavePrimaria())) {
					listaFormasCobrancaSelect.add(formaCobranca);
				} else {
					listaFormasCobrancaDisponiveis.add(formaCobranca);
				}
			}
			model.addAttribute(LISTA_FORMA_COBRANCA_SELECIONADOS, listaFormasCobrancaSelect);
			model.addAttribute(LISTA_FORMA_COBRANCA_DISPONIVEIS, listaFormasCobrancaDisponiveis);
		} else {
			model.addAttribute(LISTA_FORMA_COBRANCA_DISPONIVEIS, listaFormasCobranca);
		}

		/*
		 * Verifica quais as DATAS DE VENCIMENTO que estão já estão selecionadas e quais
		 * não estão. Divide em duas coleções (uma de selecionadas e outra de
		 * disponíveis) e disponibiliza no request
		 */
		List<Integer> listaVencimentosDisponiveis = new ArrayList<Integer>();
		List<Integer> listaVencimentosSelecionados = new ArrayList<Integer>();

		Collection<Integer> listaInteirosSelecionados = new ArrayList<Integer>();
		listaInteirosSelecionados.addAll(mensagemFaturamento.getListaNaoRepetidaDatasVencimento());

		for (int i = 1; i <= 31; i++) {
			if (listaInteirosSelecionados.contains(i)) {
				listaVencimentosSelecionados.add(i);
			} else {
				listaVencimentosDisponiveis.add(i);
			}
		}

		Collections.sort(listaVencimentosSelecionados);
		Collections.sort(listaVencimentosDisponiveis);

		model.addAttribute(LISTA_DATA_VENC_SELECIONADOS, listaVencimentosSelecionados);
		model.addAttribute(LISTA_DATA_VENC_DISPONIVEIS, listaVencimentosDisponiveis);

		model.addAttribute(LISTA_TIPO_MENSAGEM, controladorEntidadeConteudo.listarTipoMensagem());
	}

	/**
	 * Popular form.
	 * 
	 * @param model
	 *            {@link Model}
	 * @param mensagemFaturamento
	 *            {@link MensagemFaturamentos}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularForm(Model model, MensagemFaturamento mensagemFaturamento) throws GGASException {

		if (mensagemFaturamento.getTipoMensagem() != null) {
			model.addAttribute(ID_TIPO_MENSAGEM, mensagemFaturamento.getTipoMensagem().getChavePrimaria());
		}

		if (mensagemFaturamento.getDataInicioVigencia() != null) {
			model.addAttribute(DATA_INICIO, Util.converterDataParaStringSemHora(
					mensagemFaturamento.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR));
		}
		if (mensagemFaturamento.getDataFimVigencia() != null) {
			model.addAttribute(DATA_FIM, Util.converterDataParaStringSemHora(mensagemFaturamento.getDataFimVigencia(),
					Constantes.FORMATO_DATA_BR));
		}
		if (mensagemFaturamento.getDescricao() != null) {
			model.addAttribute(MENSAGEM, mensagemFaturamento.getDescricao());
		}

		if (mensagemFaturamento.getImovel() != null) {
			model.addAttribute(ID_IMOVEL, mensagemFaturamento.getImovel().getChavePrimaria());
			model.addAttribute(NOME_FANTASIA_IMOVEL, mensagemFaturamento.getImovel().getNome());
			model.addAttribute(MATRICULA_IMOVEL, mensagemFaturamento.getImovel().getChavePrimaria());
			model.addAttribute(ENDERECO_IMOVEL, mensagemFaturamento.getImovel().getEnderecoFormatado());
			model.addAttribute(CONDOMINIO_IMOVEL, mensagemFaturamento.getImovel().getCondominio().toString());
			model.addAttribute(HABILITADO, mensagemFaturamento.getImovel().getCondominio().toString());
		}
	}

}
