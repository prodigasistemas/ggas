/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.fatura;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.FaturamentoGrupoRotaImpressao;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.ControladorFaturaControleImpressao;
import br.com.ggas.faturamento.impl.FaturamentoGrupoRotaImpressaoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Controlador responsável pelas telas relacionadas ao Controle de Impressão
 *
 */
@Controller
public class FaturaControleImpressaoAction extends GenericAction {

	public static final String LISTA_ROTA = "listaRota";

	public static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	public static final String GRUPO_FATURAMENTO = "idGrupoFaturamento";

	public static final String ID_ROTA = "idRota";

	public static final String ID_CLIENTE = "idCliente";

	public static final String INTERVALO_DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	public static final String INTERVALO_DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	public static final String INTERVALO_DATA_GERACAO_INICIAL = "dataGeracaoInicial";

	public static final String INTERVALO_DATA_GERACAO_FINAL = "dataGeracaoFinal";

	public static final String INDICADOR_EMISSAO = "indicadorEmissao";

	public static final String LISTA_FATURA_CONTROLE_IMPRESSAO = "listaFaturaControleImpressao";

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorFaturaControleImpressao controladorFaturaControleImpressao;

	@Autowired
	private ControladorProcesso controladorProcesso;

	/**
	 * Exibir pesquisa fatura controle impressao.
	 * 
	 * Método responsável por exibir tela para consulta de faturas para impressão
	 * 
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaFaturaControleImpressao")
	public String exibirPesquisaFaturaControleImpressao(Model model) throws GGASException {

		try {
			Collection<Rota> listaRota = controladorRota.consultarRota(null);
			Collection<GrupoFaturamento> grupoFaturamento = controladorFatura.consultarGrupoFaturamento(null);

			model.addAttribute(LISTA_GRUPO_FATURAMENTO, grupoFaturamento);
			model.addAttribute(LISTA_ROTA, listaRota);
		} catch (NegocioException e) {
			mensagemErro(model, e);
		}

		return "exibirPesquisaFaturaControleImpressao";
	}

	/**
	 * Exibir detalhar fatura controle impressao.
	 * 
	 * Método responsável por detalhar faturas disponíveis para impressão.
	 * 
	 * @param model         {@link Model}
	 * @param chavePrimaria {@link Long}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalharFaturaControleImpressao")
	public String exibirDetalharFaturaControleImpressao(Model model, Long chavePrimaria) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		FaturamentoGrupoRotaImpressao faturaImpressao = (FaturamentoGrupoRotaImpressao) controladorFaturaControleImpressao
				.obter(chavePrimaria, FaturamentoGrupoRotaImpressaoImpl.class, "grupoFaturamento", "rota");

		if (faturaImpressao != null) {
			filtro.put("anoMesReferencia", faturaImpressao.getAnoMesReferencia());
			filtro.put("ciclo", faturaImpressao.getCiclo());
		}

		Collection<FaturamentoGrupoRotaImpressao> faturaImpressaoList = controladorFaturaControleImpressao
				.consultarFaturaControleImpressao(filtro);

		model.addAttribute(LISTA_FATURA_CONTROLE_IMPRESSAO, faturaImpressaoList);

		return "exibirDetalharFaturaControleImpressao";
	}

	/**
	 * Pesquisar controle fatura.
	 * 
	 * Método responsável por pesquisar faturas para impressão a partir de filtro
	 * fornecido em tela.
	 * 
	 * @param faturaImpressaoVO {@link FaturaImpressaoVO}
	 * @param result            {@link BindingResult}
	 * @param request           {@link HttpServletRequest}
	 * @param model             {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarControleFatura")
	public String pesquisarControleFatura(FaturaImpressaoVO faturaImpressaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute("faturaImpressaoVO", faturaImpressaoVO);

		Map<String, Object> filtro = new HashMap<String, Object>();

		try {
			this.popularFiltroPesquisaControleFaturaImpressao(faturaImpressaoVO, filtro);

			Collection<FaturamentoGrupoRotaImpressao> faturaImpressaoList = controladorFaturaControleImpressao
					.consultarFaturaControleImpressao(filtro);

			agruparFaturaImpressao(faturaImpressaoList);

			model.addAttribute(LISTA_FATURA_CONTROLE_IMPRESSAO, faturaImpressaoList);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaFaturaControleImpressao(model);
	}

	/**
	 * Gerar relatorio controle impressao.
	 * 
	 * Método responsável por criar e iniciar um processo batch relacionado a
	 * emissão de faturas
	 * 
	 * @param faturaImpressaoVO {@link FaturaImpressaoVO}
	 * @param result            {@link BindingResult}
	 * @param request           {@link HttpServletRequest}
	 * @param model             {@link Model}
	 * @throws GGASException {@link GGASException}
	 * @return String {@link String}
	 */
	@RequestMapping("gerarRelatorioControleImpressao")
	public String gerarRelatorioControleImpressao(FaturaImpressaoVO faturaImpressaoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		try {
			if (faturaImpressaoVO.getChavesPrimarias() == null) {
				mensagemErro(model,
						new NegocioException(ControladorFaturaControleImpressao.ERRO_SELECIONAR_REGISTRO, true));
			} else if (faturaImpressaoVO.getChavesPrimarias().length > 1) {
				mensagemErro(model, new NegocioException(
						ControladorFaturaControleImpressao.ERRO_SELECIONAR_REGISTRO_APENAS_UM, true));
			} else {

				FaturamentoGrupoRotaImpressao faturamentoGrupoRotaImpressao = (FaturamentoGrupoRotaImpressao) controladorFaturaControleImpressao
						.obter(faturaImpressaoVO.getChavesPrimarias()[0], FaturamentoGrupoRotaImpressaoImpl.class,
								"grupoFaturamento", "rota");

				Long idGrupoFaturamento = faturamentoGrupoRotaImpressao.getGrupoFaturamento().getChavePrimaria();
				Long idRota = faturamentoGrupoRotaImpressao.getRota().getChavePrimaria();

				Operacao operacao = controladorFaturaControleImpressao.obterOperacaoEmitirFatura();

				Processo processo = (Processo) controladorProcesso.criar();
				processo.setAgendado(false);
				processo.setVersao(0);
				processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
				processo.setDataFinalAgendamento(null);
				processo.setHabilitado(true);
				processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
				processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
				processo.setUsuario(super.obterUsuario(request));
				processo.setDiaNaoUtil(true);
				processo.setDadosAuditoria(super.getDadosAuditoria(request));

				processo.setOperacao(operacao);
				processo.setDescricao("Emitir Fatura Controle Impressão Grupo Faturamento: "
						+ faturamentoGrupoRotaImpressao.getGrupoFaturamento().getDescricao() + " Rota: "
						+ faturamentoGrupoRotaImpressao.getRota().getNumeroRota());

				Map<String, String> parametros = new HashMap<String, String>();

				parametros.put(GRUPO_FATURAMENTO, String.valueOf(idGrupoFaturamento));
				parametros.put(ID_ROTA, String.valueOf(idRota));

				processo.setParametros(parametros);

				controladorProcesso.inserir(processo);

				mensagemSucesso(model, ControladorFaturaControleImpressao.MSG_PROCESSO_ADICIONADO_A_FILA);
			}

		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return exibirPesquisaFaturaControleImpressao(model);
	}

	/**
	 * Popular filtro pesquisa controle fatura impressao.
	 * 
	 * Método responsável pelo preenchimento do filtro de pesquisa
	 * 
	 * @param faturaImpressaoVO {@link FaturaImpressaoVO}
	 * @param filtro {@link Map}
	 * 
	 */
	private void popularFiltroPesquisaControleFaturaImpressao(FaturaImpressaoVO faturaImpressaoVO,
			Map<String, Object> filtro) throws GGASException {

		filtro.put(ControladorFaturaControleImpressao.INDICADOR_EMISSAO, faturaImpressaoVO.getIndicadorEmissao());

		verificarCampoDataFiltro(faturaImpressaoVO.getDataEmissaoInicial(), filtro,
				ControladorFaturaControleImpressao.INTERVALO_DATA_EMISSAO_INICIAL, "Data Inicial");

		verificarCampoDataFiltro(faturaImpressaoVO.getDataEmissaoFinal(), filtro,
				ControladorFaturaControleImpressao.INTERVALO_DATA_EMISSAO_FINAL, "Data Final");

		verificarCampoDataFiltro(faturaImpressaoVO.getDataGeracaoInicial(), filtro,
				ControladorFaturaControleImpressao.INTERVALO_DATA_GERACAO_INICIAL, "Data Inicial");

		verificarCampoDataFiltro(faturaImpressaoVO.getDataGeracaoFinal(), filtro,
				ControladorFaturaControleImpressao.INTERVALO_DATA_GERACAO_FINAL, "Data Final");

		if (faturaImpressaoVO.getIdRota() != null && faturaImpressaoVO.getIdRota() > 0) {
			filtro.put(ControladorFaturaControleImpressao.ROTA, faturaImpressaoVO.getIdRota());
		}

		if (faturaImpressaoVO.getIdGrupoFaturamento() != null && faturaImpressaoVO.getIdGrupoFaturamento() > 0) {
			filtro.put(ControladorFaturaControleImpressao.GRUPO_FATURAMENTO, faturaImpressaoVO.getIdGrupoFaturamento());
		}

		if (faturaImpressaoVO.getIdCliente() != null && faturaImpressaoVO.getIdCliente() > 0) {
			filtro.put(ControladorFaturaControleImpressao.CLIENTE, faturaImpressaoVO.getIdCliente());
		}

	}

	/**
	 * Agrupar fatura impressao.
	 * 
	 * @param faturaImpressaoList {@link Collection}
	 */
	private void agruparFaturaImpressao(Collection<FaturamentoGrupoRotaImpressao> faturaImpressaoList) {

		if (faturaImpressaoList != null && !faturaImpressaoList.isEmpty()) {
			Map<Integer, FaturamentoGrupoRotaImpressao> mapa = new HashMap<Integer, FaturamentoGrupoRotaImpressao>();
			for (FaturamentoGrupoRotaImpressao faturaImpressaoGrupoRotaImpressao : faturaImpressaoList) {
				if (!mapa.containsKey(faturaImpressaoGrupoRotaImpressao.getAnoMesReferencia())) {
					mapa.put(faturaImpressaoGrupoRotaImpressao.getAnoMesReferencia(),
							faturaImpressaoGrupoRotaImpressao);
				} else {
					FaturamentoGrupoRotaImpressao faturaImpre = mapa
							.get(faturaImpressaoGrupoRotaImpressao.getAnoMesReferencia());
					if (!faturaImpressaoGrupoRotaImpressao.getCiclo().equals(faturaImpre.getCiclo())) {
						mapa.put(faturaImpressaoGrupoRotaImpressao.getAnoMesReferencia(),
								faturaImpressaoGrupoRotaImpressao);
					} else if (faturaImpressaoGrupoRotaImpressao.getCiclo().equals(faturaImpre.getCiclo())
							&& faturaImpressaoGrupoRotaImpressao.getSeqImpressao() > faturaImpre.getSeqImpressao()) {
						mapa.values().remove(faturaImpre);
						mapa.put(faturaImpressaoGrupoRotaImpressao.getAnoMesReferencia(),
								faturaImpressaoGrupoRotaImpressao);
					}
				}
			}
			faturaImpressaoList.clear();
			faturaImpressaoList.addAll(new ArrayList<FaturamentoGrupoRotaImpressao>(mapa.values()));
		}

	}

	/**
	 * Método responsável por validar campos data.
	 * 
	 * @param campoData {@link String - valor do campo}
	 * @param filtro    {@link Map - o filtro}
	 * @param descricao {@link String - nome do campo}
	 * @param nomeCampo {@link String - rótulo do campo}
	 * @throws GGASException {@link GGASException}
	 */
	private void verificarCampoDataFiltro(String campoData, Map<String, Object> filtro, String descricao,
			String nomeCampo) throws GGASException {
		if (campoData != null && !"".equals(campoData)) {
			filtro.put(descricao, Util.converterCampoStringParaData(nomeCampo, campoData, Constantes.FORMATO_DATA_BR));
		}
	}

}
