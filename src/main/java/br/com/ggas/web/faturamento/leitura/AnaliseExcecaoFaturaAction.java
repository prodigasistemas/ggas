/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.*;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosItemFaturaVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.impl.DadosVigenciaVO;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.*;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
import br.com.ggas.medicao.leitura.impl.SituacaoLeituraMovimentoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.web.faturamento.cronograma.CronogramaVO;
import br.com.ggas.util.*;
import br.com.ggas.web.faturamento.leitura.dto.AnormalidadeDTO;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.HistoricoAnomaliaFaturamentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaAnaliseAnormalidade;
import br.com.ggas.web.medicao.leitura.HistoricoMedicaoAgrupadoVO;
import br.com.ggas.web.medicao.leitura.PontoConsumoVO;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.util.concurrent.Service;

import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Classe controladora responsável por gerenciar os eventos e acionar as classes
 * e seus respectivos metodos relacionados as regras de negócio e de modelo
 * realizando alterações nas informações das telas referentes a Análise de
 * Anormalidade de Faturamento.
 */
@Controller
public class AnaliseExcecaoFaturaAction extends GenericAction {

	public static final String PONTO_CONSUMO = "pontoConsumo";
	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	private static final String EXIBIR_PESQUISA_EXCECAO_LEITURA_FATURAMENTO = "exibirPesquisaExcecaoLeituraFaturamento";

	private static final String INTERVALO_GERACAO = "Intervalo de Geração";

	private static final String DATA_GERACAO_INICIAL = "dataGeracaoInicial";

	private static final String DATA_GERACAO_FINAL = "dataGeracaoFinal";

	private static final String DATA_MEDICAO_INICIAL = "dataMedicaoInicial";

	private static final String DATA_MEDICAO_FINAL = "dataMedicaoFinal";

	private static final String ROTA_LEITURISTA_FUNCIONARIO = "rota.leiturista.funcionario";

	private static final String LAZY_INSTALACAO_MEDIDOR_MEDIDOR = "instalacaoMedidor.medidor";

	private static final String ANALISADA = "analisada";

	private static final String INDICADOR_IMPEDIMENTO_FATURAMENTO = "indicadorImpedeFaturamento";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";

	private static final String ID_TIPO_CONSUMO = "idTipoConsumo";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String IDS_ROTA = "idsRota";

	private static final String NOME_IMOVEL = "nomeImovel";

	private static final String PERCENTUAL_VARIACAO_VALOR_FATURA = "percentualVariacaoValorFatura";

	private static final String MATRICULA_IMOVEL = "matriculaImovel";

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String ID_ANORMALIDADE_FATURAMENTO = "idAnormalidadeFaturamento";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ID_SITUACAO_PONTO_CONSUMO = "idSituacaoPontoConsumo";

	private static final String ID_LOCALIDADE = "idLocalidade";

	private static final String ID_SETOR_COMERCIAL = "idSetorComercial";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String LAZY_ROTA_EMPRESA = "rota.empresa";

	private static final String LAZY_ROTA_GRUPO_FATURAMENTO = "rota.grupoFaturamento";

	private static final String LISTA_PONTOS_CONSUMO = "listaPontosConsumo";

	private static final String REFERENCIA = "referencia";

	private static final String CICLO = "ciclo";

	public static final String CPF_CLIENTE = "cpfCliente";

	public static final String NOME_CLIENTE = "nomeCliente";

	public static final String RG_CLIENTE = "rgCliente";

	public static final String NUMERO_PASSAPORTE = "numeroPassaporte";

	public static final String CNPJ_CLIENTE = "cnpjCliente";

	public static final String NOME_FANTASIA_CLIENTE = "nomeFantasiaCliente";

	public static final String OFFSET = "offset";

	public static final String MAX_RESULTS = "maxResults";

	public static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	public static final String PESQUISA_AUTOMATICA = "pesquisaAutomatica";
	public static final int MULTIPLICADOR_PORCENTAGEM = 100;

	@Autowired
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	private ControladorSetorComercial controladorSetorComercial;

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorFaturamentoAnormalidade controladorFaturamentoAnormalidade;

	@Autowired
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorTributo controladorTributo;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorRubrica controladorRubrica;

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa para análise de exceção
	 * de leitura de consumo.
	 *
	 * @param pesquisaAnaliseAnormalidade {@link AnaliseExcecaoFaturaVO}
	 * @param model {@link Model}
	 * @return exibirPesquisaExcecaoLeituraFaturamento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(EXIBIR_PESQUISA_EXCECAO_LEITURA_FATURAMENTO)
	public String exibirPesquisaExcecaoLeituraFaturamento(PesquisaAnaliseAnormalidade pesquisaAnaliseAnormalidade, Model model)
			throws GGASException {

		model.addAttribute("analiseExcecaoFaturaVO", pesquisaAnaliseAnormalidade);

		carregarCampos(pesquisaAnaliseAnormalidade, model);

		return EXIBIR_PESQUISA_EXCECAO_LEITURA_FATURAMENTO;
	}

	/**
	 * Busca as anormalidades de faturamento de acordo com os filtros informados
	 * @param pesquisa O objeto representando todos os filtros informados no formulário
	 * @return A lista das anormalidades encontradas
	 */
	@RequestMapping(value = "pesquisarExcecaoLeituraFaturaDataTable", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> buscarLeituras(@RequestBody PesquisaAnaliseAnormalidade pesquisa) {
		try {
			Map<String, Object> filtro = prepararFiltro(pesquisa);
			Pair<List<HistoricoAnomaliaFaturamento>, Long> resultados =
					controladorHistoricoAnomaliaFaturamento.consultarHistoricoAnomaliaFaturamentoPaginada(filtro);
			List<HistoricoAnomaliaFaturamentoDTO> listaHistorico =
					resultados.getFirst().stream().map(h -> HistoricoAnomaliaFaturamentoDTO.build(h)).collect(Collectors.toList());
			final DatatablesServerSideDTO<HistoricoAnomaliaFaturamentoDTO> datatable =
					new DatatablesServerSideDTO<>(pesquisa.getOffset(), resultados.getSecond(), listaHistorico);
			return new ResponseEntity<>(serializarJson(datatable), HttpStatus.OK);
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Exibe a tela de detalhamento das anormalidades a partir de uma anormalidade de faturamento
	 * @param historicoId A chave primária do histórico de anormalidade de faturamento
	 * @param model O model
	 * @return A página de detalhamento
	 * @throws GGASException GGASExpetion
	 */
	@RequestMapping(value = "exibirDetalhamentoAnormalidadeFaturamento", method = RequestMethod.GET , params = {"historico"})
	public String exibirDetalhamentoAnormalidadeFaturamento(@RequestParam(value = "historico") long historicoId, Model model)
			throws GGASException {
		HistoricoAnomaliaFaturamento historico =
				(HistoricoAnomaliaFaturamento) controladorHistoricoAnomaliaFaturamento.obter(historicoId, PONTO_CONSUMO, "faturamentoAnormalidade");
		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(historico.getPontoConsumo().getChavePrimaria());
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		HistoricoMedicao historicoMedicao = controladorHistoricoMedicao
				.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(), historico.getAnoMesFaturamento(),
						historico.getNumeroCiclo());

		LeituraMovimento leituraMovimentoPesquisa = new LeituraMovimentoImpl();
		leituraMovimentoPesquisa.setPontoConsumo(pontoConsumo);

		leituraMovimentoPesquisa.setAnoMesFaturamento(historico.getAnoMesFaturamento());
		leituraMovimentoPesquisa.setCiclo(historico.getNumeroCiclo());
		SituacaoLeituraMovimento situacaoLeituraMovimento = new SituacaoLeituraMovimentoImpl();
		situacaoLeituraMovimento.setChavePrimaria(SituacaoLeituraMovimento.PROCESSADO);
		leituraMovimentoPesquisa.setSituacaoLeitura(situacaoLeituraMovimento);
		LeituraMovimento leituraMovimento = controladorLeituraMovimento.consultarPorSituacaoAnoMesCicloPontoConsumo(leituraMovimentoPesquisa);

		List<AnormalidadeDTO> anormalidades = new ArrayList<>();
		montarListaDeAnormalidadeFaturamento(historico, anormalidades);
		montarListaDeAnormalidadeLeitura(leituraMovimento, anormalidades);

		if (historicoMedicao != null) {
			HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
					.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicao.getChavePrimaria());

			montarListaDeAnormalidadeConsumo(anormalidades, historicoConsumo);
			if (historico.getFatura() != null){
				Fachada fachada = Fachada.getInstancia();
				DadosResumoFatura dadosResumoFatura = fachada.obterDadosDetalhamentoFatura(historico.getFatura().getChavePrimaria(), 0L);
				model.addAttribute("listaDadosGerais", dadosResumoFatura.getListaDadosGeraisItens());
			} else {
				model.addAttribute("simulado", true);
				model.addAttribute("historicoConsumo", historicoConsumo);
				if (historicoConsumo != null) {
					gerarSimulacaoDeFatura(model, pontoConsumo, historicoMedicao, historicoConsumo);
				}
			}

			model.addAttribute("historicoConsumo", historicoConsumo);
		}
		String mesAno = "";
		if (historico.getAnoMesFaturamento() != null) {
			mesAno = String.valueOf(historico.getAnoMesFaturamento());
			mesAno = mesAno.substring(mesAno.length() - 2) + "/" + mesAno.substring(0, 4);
		}
		model.addAttribute("mesAno", mesAno);
		model.addAttribute("anormalidades", anormalidades);
		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute("historicoFaturamento", historico);
		if (contratoPontoConsumo != null) {
			model.addAttribute("contrato", contratoPontoConsumo.getContrato().getNumeroFormatado());
		}
		return "exibirDetalhamentoAnormalidadeFaturamento";
	}

	private void montarListaDeAnormalidadeFaturamento(HistoricoAnomaliaFaturamento historico, List<AnormalidadeDTO> anormalidades) {
		FaturamentoAnormalidade faturamentoAnormalidade = historico.getFaturamentoAnormalidade();
		AnormalidadeDTO anormalidadeFaturamento =
				new AnormalidadeDTO(faturamentoAnormalidade.getChavePrimaria(), faturamentoAnormalidade.getDescricao(),
						faturamentoAnormalidade.getIndicadorBloqueiaFaturamento());
		anormalidadeFaturamento.setTipo("Faturamento");
		anormalidadeFaturamento.setDataApontamento(historico.getUltimaAlteracao());
		anormalidades.add(anormalidadeFaturamento);
	}

	private void montarListaDeAnormalidadeLeitura(LeituraMovimento leituraMovimento, List<AnormalidadeDTO> anormalidades)
			throws NegocioException {
		if (leituraMovimento != null && leituraMovimento.getCodigoAnormalidadeLeitura() != null
				&& leituraMovimento.getCodigoAnormalidadeLeitura() != 0) {
			AnormalidadeLeitura leituraAnormalidade =
					controladorAnormalidade.obterAnormalidadeLeitura(leituraMovimento.getCodigoAnormalidadeLeitura());
			AnormalidadeDTO anormalidadeLeitura =
					new AnormalidadeDTO(leituraAnormalidade.getChavePrimaria(), leituraAnormalidade.getDescricao(),
							leituraAnormalidade.getBloquearFaturamento());
			anormalidadeLeitura.setTipo("Leitura");
			anormalidadeLeitura.setDataApontamento(leituraMovimento.getUltimaAlteracao());
			anormalidades.add(anormalidadeLeitura);
		}
	}

	private void montarListaDeAnormalidadeConsumo(List<AnormalidadeDTO> anormalidades, HistoricoConsumo historicoConsumo)
			throws NegocioException {
		if (historicoConsumo != null && historicoConsumo.getAnormalidadeConsumo() != null) {
			AnormalidadeConsumo consumoAnormalidade =
					controladorAnormalidade.obterAnormalidadeConsumo(historicoConsumo.getAnormalidadeConsumo().getChavePrimaria());
			AnormalidadeDTO anormalidadeConsumo =
					new AnormalidadeDTO(consumoAnormalidade.getChavePrimaria(), consumoAnormalidade.getDescricao(),
							consumoAnormalidade.getBloquearFaturamento());
			anormalidadeConsumo.setTipo("Consumo");
			anormalidadeConsumo.setDataApontamento(historicoConsumo.getUltimaAlteracao());
			anormalidades.add(anormalidadeConsumo);
		}
	}

	private void gerarSimulacaoDeFatura(Model model, PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao,
			HistoricoConsumo historicoConsumo) throws GGASException {
		List<DadosItemFaturaVO> itensFatura = new ArrayList<>();
		Long chaveRubrica =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_CONSUMO_GAS));
		Rubrica rubricaConsumoGas = (Rubrica) controladorRubrica.obter(chaveRubrica);
		
		BigDecimal totalFatura = BigDecimal.ZERO;
		
		
		try {
			ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas = ServiceLocator.getInstancia()
					.getControladorCalculoFornecimentoGas();
			BigDecimal consumoAFaturar = controladorFatura.definirVolumeAFaturarSemProgramacao(pontoConsumo,
					historicoConsumo, Boolean.FALSE);

			DadosFornecimentoGasVO dadosFornecimentoGasReal = controladorCalculoFornecimentoGas
					.calcularValorFornecimentoGas(rubricaConsumoGas.getItemFatura(),
							historicoMedicao.getDataLeituraAnterior(), historicoMedicao.getDataLeituraInformada(),
							consumoAFaturar, Arrays.asList(pontoConsumo), Boolean.FALSE, null, false, null,
							Boolean.FALSE, null, null);


			for (DadosVigenciaVO dadosVigencia : dadosFornecimentoGasReal.getDadosVigencia()) {

				Date dataInicio = dadosVigencia.getDataPeriodoInicial().toDate();
				Date dataFim = dadosVigencia.getDataPeriodoFinal().toDate();

				for (DadosFaixaVO dadosFaixa : dadosVigencia.getDadosFaixa()) {
					TarifaVigenciaFaixa tarifaVigenciaFaixa = dadosFaixa.getTarifaVigenciaFaixa();
					String faixas = tarifaVigenciaFaixa.getMedidaInicio() + " - " + tarifaVigenciaFaixa.getMedidaFim();
					String tarifaVigencia = tarifaVigenciaFaixa.getTarifaVigencia().getTarifa().getDescricao() + " - "
							+ tarifaVigenciaFaixa.getTarifaVigencia().getDataVigenciaFormatada();

					BigDecimal valorFaixa = dadosFaixa.getValorFixoComImpostoComDesconto();
					valorFaixa = valorFaixa.add(dadosFaixa.getValorVariavelComImpostoComDesconto());
					
					
					totalFatura = totalFatura.add(valorFaixa);

					BigDecimal valorUnitario = valorFaixa
							.divide(dadosFaixa.getConsumoApurado(), 4, RoundingMode.HALF_EVEN);

					DadosItemFaturaVO itemFaturaVO = new DadosItemFaturaVO();
					itemFaturaVO.setDescricao(rubricaConsumoGas.getDescricao());
					itemFaturaVO.setTipo("Débito");
					itemFaturaVO.setQuantidade(dadosFaixa.getConsumoApurado());
					itemFaturaVO.setDataInicio(dataInicio);
					itemFaturaVO.setDataFim(dataFim);
					itemFaturaVO.setValorUnitario(valorUnitario);
					itemFaturaVO.setValorTotal(valorFaixa);
					itemFaturaVO.setFaixas(faixas);
					itemFaturaVO.setTarifaVigencia(tarifaVigencia);
					itensFatura.add(itemFaturaVO);

				}
			}
		} catch (GGASException e) {
			List<TarifaVigenciaFaixa> faixasTarifarias =
					controladorTarifa.listarTarifaVigenciaFaixaPorPontoConsumo(pontoConsumo.getChavePrimaria());
			if (historicoConsumo.getConsumoApurado() == null) {
				historicoConsumo.setConsumoApurado(BigDecimal.ZERO);
			}

			for (TarifaVigenciaFaixa faixa : faixasTarifarias) {
				BigDecimal consumoFaixa;
				BigDecimal inicioFaixa = faixa.getMedidaInicio();
				BigDecimal finalFaixa = faixa.getMedidaFim();
				String faixas = faixa.getMedidaInicio() + " - " + faixa.getMedidaFim();
				if (!inicioFaixa.equals(BigDecimal.ZERO)) {
					inicioFaixa = inicioFaixa.subtract(BigDecimal.ONE);
				}
				if (historicoConsumo.getConsumoApurado().compareTo(finalFaixa) > 0) {
					consumoFaixa = finalFaixa.subtract(inicioFaixa);
				} else {
					consumoFaixa = historicoConsumo.getConsumoApurado().subtract(inicioFaixa);
				}
				BigDecimal valorTotal = BigDecimal.ZERO;
				BigDecimal valorUnitario = BigDecimal.ZERO;

				if (faixa.getValorFixo() != null && faixa.getValorFixo().compareTo(BigDecimal.ZERO) > 0) {
					valorTotal = faixa.getValorFixo();
					valorUnitario = faixa.getValorFixo().divide(faixa.getMedidaFim());
				}

				if (faixa.getValorVariavel() != null && faixa.getValorVariavel().compareTo(BigDecimal.ZERO) > 0) {
					valorTotal = consumoFaixa.multiply(faixa.getValorVariavel());
					valorUnitario = faixa.getValorVariavel();
				}

				totalFatura = totalFatura.add(valorTotal);

				DadosItemFaturaVO itemFaturaVO = new DadosItemFaturaVO();
				itemFaturaVO.setDescricao(rubricaConsumoGas.getDescricao());
				itemFaturaVO.setTipo("Débito");
				itemFaturaVO.setQuantidade(consumoFaixa);
				itemFaturaVO.setDataInicio(historicoMedicao.getDataLeituraAnterior());
				itemFaturaVO.setDataFim(historicoMedicao.getDataLeituraInformada());
				itemFaturaVO.setValorUnitario(valorUnitario);
				itemFaturaVO.setValorTotal(valorTotal);
				itemFaturaVO.setFaixas(faixas);
				itemFaturaVO.setTarifaVigencia(pontoConsumo.getSegmento().getDescricao());
				itensFatura.add(itemFaturaVO);
				
				if (historicoConsumo.getConsumoApurado().compareTo(finalFaixa) <= 0) {
					break;
				}
			}

		}
		

		Map<String, Object> filtroTributo = new HashMap<>();
		filtroTributo.put("indicadorProduto", true);
		Collection<Tributo> tributos = controladorTributo.consultarTributos(filtroTributo);
		if (!tributos.isEmpty()) {
			Long[] chavesTributos = tributos.stream().map(t -> t.getChavePrimaria()).toArray(Long[]::new);
			Collection<TributoAliquota> aliquotasTributos = controladorTributo.listaTributoAliquotaVigente(chavesTributos);
			List<DadosTributoVO> listaTributoVO = new ArrayList<>();
			for (TributoAliquota tributo : aliquotasTributos) {
				DadosTributoVO tributoVO = new DadosTributoVO();
				tributoVO.setTributoAliquota(tributo);
				tributoVO.setBaseCalculo(totalFatura);
				tributoVO.setValor(totalFatura.multiply(tributo.getValorAliquota()));
				tributoVO.setPercentual(tributo.getValorAliquota().multiply(BigDecimal.valueOf(MULTIPLICADOR_PORCENTAGEM)));
				listaTributoVO.add(tributoVO);
			}
			model.addAttribute("tributos", listaTributoVO);
		}
		model.addAttribute("totalFatura", totalFatura);
		model.addAttribute("itensFatura", itensFatura);
	}

	@RequestMapping("exibirAnaliseExcecaoFatura")
	public String exibirAnaliseExcecaoFatura(Model model, CronogramaVO cronogramaVO) throws GGASException {

		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(cronogramaVO.getIdPontoConsumo());
		Rota rota = pontoConsumo.getRota();
		Long[] rotas = new Long[1];
		rotas[0] = rota.getChavePrimaria();
		GrupoFaturamento grupo = rota.getGrupoFaturamento();

		PesquisaAnaliseAnormalidade pesquisaAnaliseAnormalidade = new PesquisaAnaliseAnormalidade();
		pesquisaAnaliseAnormalidade.setIdPontoConsumo(cronogramaVO.getIdPontoConsumo());
		pesquisaAnaliseAnormalidade.setIdsRota(rotas);
		pesquisaAnaliseAnormalidade.setIdGrupoFaturamento(grupo.getChavePrimaria());

		model.addAttribute(PESQUISA_AUTOMATICA, true);
		return exibirPesquisaExcecaoLeituraFaturamento(pesquisaAnaliseAnormalidade, model);
	}

	/**
	 * Método responsável pela geração do
	 * relatório de análise de consumo.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param model {@link Model}
	 * @return responseEntity {@link ResponseEntity}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioAnaliseFatura")
	public ResponseEntity<byte[]> gerarRelatorioAnaliseFatura(AnaliseExcecaoFaturaVO analiseExcecaoFaturaVO,
			Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");
		filtro.put(FORMATO_IMPRESSAO, formatoImpressao);

		ResponseEntity<byte[]> responseEntity = null;

		if (analiseExcecaoFaturaVO.getChavesPrimarias() != null
				&& analiseExcecaoFaturaVO.getChavesPrimarias().length > 0) {

			byte[] relatorioAnaliseFatura = controladorHistoricoAnomaliaFaturamento
					.gerarRelatorioAnomaliaFaturamento(analiseExcecaoFaturaVO.getChavesPrimarias(), filtro);

			if (relatorioAnaliseFatura != null) {

				FormatoImpressao formatoImpressao2 = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);

				MediaType contentType = MediaType.parseMediaType(this.obterContentTypeRelatorio(formatoImpressao2));

				String filename = String.format("relatorioAnomaliaHistorico%s",
						this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));

				HttpHeaders headers = new HttpHeaders();

				headers.setContentType(contentType);
				headers.setContentDispositionFormData("attachment", filename);

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				responseEntity = new ResponseEntity<byte[]>(relatorioAnaliseFatura, headers, HttpStatus.OK);
			}
		}

		return responseEntity;
	}

	/**
	 * Exibir detalhamento excecao leitura consumo.
	 *
	 * @param pesquisaAnaliseAnormalidade {@link AnaliseExcecaoFaturaVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaExcecaoLeituraFaturamento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoExcecaoLeituraConsumo")
	public String exibirDetalhamentoExcecaoLeituraConsumo(PesquisaAnaliseAnormalidade pesquisaAnaliseAnormalidade,
			HttpServletRequest request, Model model) throws GGASException {

		Long chavePrimariaPontoConsumo = pesquisaAnaliseAnormalidade.getChavePrimaria();

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavePrimariaPontoConsumo,
				"situacaoConsumo", "segmento", "rota", LAZY_ROTA_GRUPO_FATURAMENTO, "imovel", "imovel.listaPontoConsumo",
				"instalacaoMedidor", "imovel.quadraFace.endereco.cep");

		Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
				.consultarHistoricoConsumo(chavePrimariaPontoConsumo, true, null, null);

		Collection<HistoricoMedicao> listaHistoricoMedicao = controladorHistoricoMedicao
				.consultarHistoricoAgrupadoMedicao(chavePrimariaPontoConsumo, null, null);

		// Montar um VO para exibição com
		// indicação se existe comentário para a
		// linha
		Collection<HistoricoMedicaoAgrupadoVO> listaHistoricoMedicaoAgrupadoVO = new ArrayList<HistoricoMedicaoAgrupadoVO>();

		if (listaHistoricoMedicao != null && !listaHistoricoMedicao.isEmpty()) {
			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				boolean existeComentario = controladorHistoricoMedicao.existeMedicaoHistoricoComentario(
						chavePrimariaPontoConsumo, historicoMedicao.getAnoMesLeitura(),
						historicoMedicao.getNumeroCiclo());
				HistoricoMedicaoAgrupadoVO historico = new HistoricoMedicaoAgrupadoVO();
				historico.setHistoricoMedicao(historicoMedicao);
				historico.setExisteComentario(existeComentario);
				listaHistoricoMedicaoAgrupadoVO.add(historico);
			}
		}

		String anoMesReferencia = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);

		model.addAttribute("clientePrincipal",
				controladorCliente.obterClientePrincipal(pontoConsumo.getChavePrimaria()));

		Collection<PontoConsumo> listaPontosConsumo = consultarPontosConsumoImovel(pontoConsumo.getImovel());
		listaPontosConsumo.remove(pontoConsumo);
		Collection<PontoConsumoVO> listaPontosConsumoVO = montarListaPontosConsumoVo(listaPontosConsumo);
		model.addAttribute(LISTA_PONTOS_CONSUMO, listaPontosConsumoVO);

		model.addAttribute(PONTO_CONSUMO, pontoConsumo);
		model.addAttribute("listaHistoricoConsumo", listaHistoricoConsumo);
		model.addAttribute("listaHistoricoMedicao", listaHistoricoMedicao);
		model.addAttribute("listaHistoricoMedicaoVO", listaHistoricoMedicaoAgrupadoVO);
		model.addAttribute("listaAnormalidadeFatura",
				controladorFaturamentoAnormalidade.listaAnormalidadeFaturamento());
		model.addAttribute("listaLeiturista", controladorRota.listarLeituristas());
		model.addAttribute("comentarioObrigatorio", controladorHistoricoMedicao.exigeComentarioAnaliseExcecao());
		model.addAttribute("anoMesReferencia",
				Util.converterCampoStringParaValorInteger("Parâmetro Ano/Mês de Faturamento", anoMesReferencia));
		model.addAttribute("intervaloAnosData", intervaloAnosData());

		String gerarCreditoVolumeAutomatico = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_GERA_CREDITO_VOLUME_AUTOMATICAMENTE);
		model.addAttribute("gerarCreditoVolumeAutomatico", "1".equals(gerarCreditoVolumeAutomatico));

		saveToken(request);

		return exibirPesquisaExcecaoLeituraFaturamento(pesquisaAnaliseAnormalidade, model);
	}

	/**
	 * Montar lista pontos consumo vo.
	 *
	 * @param listaPontosConsumo {@link PontoConsumoVO}
	 * @return listaPontosConsumoVO {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	private Collection<PontoConsumoVO> montarListaPontosConsumoVo(Collection<PontoConsumo> listaPontosConsumo)
			throws GGASException {

		List<PontoConsumoVO> listaPontosConsumoVO = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoVO = null;

		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			pontoVO = new PontoConsumoVO();
			PontoConsumo pontoConsumoCompleto = (PontoConsumo) controladorPontoConsumo.obter(
					pontoConsumo.getChavePrimaria(), LAZY_ROTA_GRUPO_FATURAMENTO, LAZY_ROTA_EMPRESA,
					LAZY_INSTALACAO_MEDIDOR_MEDIDOR, ROTA_LEITURISTA_FUNCIONARIO);
			pontoVO.setPontoConsumo(pontoConsumoCompleto);

			Map<String, Integer> referenciaCiclo = controladorPontoConsumo
					.obterReferenciaCicloAtual(pontoConsumoCompleto);

			if (referenciaCiclo != null) {
				pontoVO.setCiclo(String.valueOf(referenciaCiclo.get(CICLO)));
				pontoVO.setReferencia(Util.formatarAnoMes(referenciaCiclo.get(REFERENCIA)));
			}

			listaPontosConsumoVO.add(pontoVO);

		}

		Collections.sort(listaPontosConsumoVO, new Comparator<PontoConsumoVO>() {

			@Override
			public int compare(PontoConsumoVO o1, PontoConsumoVO o2) {

				int retorno = 0;
				if (o2 == null || o2.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
					retorno = -1;
				} else {
					if (o1 == null || o1.getPontoConsumo().getNumeroSequenciaLeitura() == null) {
						retorno = 1;
					} else {
						retorno = o1.getPontoConsumo().getNumeroSequenciaLeitura()
								.compareTo(o2.getPontoConsumo().getNumeroSequenciaLeitura());
					}
				}
				return retorno;
			}
		});

		return listaPontosConsumoVO;
	}

	/**
	 * Consultar pontos consumo imovel.
	 *
	 * @param imovel {@link Imovel}
	 * @return listaPontosConsumo {@link PontoConsumo}
	 */
	private Collection<PontoConsumo> consultarPontosConsumoImovel(Imovel imovel) {

		Collection<PontoConsumo> listaPontosConsumo = new ArrayList<PontoConsumo>();

		if (imovel != null) {
			Collection<PontoConsumo> pontosDeConsumo = imovel.getListaPontoConsumo();
			if (pontosDeConsumo != null && !pontosDeConsumo.isEmpty()) {
				listaPontosConsumo.addAll(pontosDeConsumo);
			}
		}

		return listaPontosConsumo;
	}

	/**
	 * Intervalo anos data.
	 *
	 * @return intervaloAnosData {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Método responsável por alterar os dados de
	 * faturamento de um histórico de
	 * leitura de um ponto de consumo.
	 *
	 * @param pesquisaAnaliseAnormalidade {@link String}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarExcecaoLeituraFatura {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarDadosAnomaliaFaturamento")
	public String alterarDadosAnomaliaFaturamento(PesquisaAnaliseAnormalidade pesquisaAnaliseAnormalidade,
			HttpServletRequest request, Model model) throws GGASException {

		Long[] chavesPrimarias = pesquisaAnaliseAnormalidade.getChavesPrimarias();
		
		
		HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento = null;
		Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento = new ArrayList<HistoricoAnomaliaFaturamento>();

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {

			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					HistoricoAnomaliaFaturamento historicoAnomalia = controladorHistoricoAnomaliaFaturamento
							.obterHistoricoAnomaliaFaturamento(chavesPrimarias[i]);
					listaHistoricoAnomaliaFaturamento.add(historicoAnomalia);
				}
				if (!listaHistoricoAnomaliaFaturamento.isEmpty()) {
					controladorHistoricoAnomaliaFaturamento
							.validarAlteracaoFaturaHistoricoAnomaliaFaturamento(listaHistoricoAnomaliaFaturamento);
				}

			}

			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					historicoAnomaliaFaturamento = controladorHistoricoAnomaliaFaturamento
							.obterHistoricoAnomaliaFaturamento(chavesPrimarias[i]);
					historicoAnomaliaFaturamento.setAnalisada(true);
					historicoAnomaliaFaturamento.setDataAnalise(Calendar.getInstance().getTime());
					historicoAnomaliaFaturamento.setDadosAuditoria(dadosAuditoria);
					historicoAnomaliaFaturamento.setAnalisador(dadosAuditoria.getUsuario());

					// cancelar fatura anomalia se
					// houver fatura
					controladorFatura.cancelarFaturaAnomalia(historicoAnomaliaFaturamento, dadosAuditoria);

					controladorHistoricoAnomaliaFaturamento.atualizar(historicoAnomaliaFaturamento);
					
					if ("EXISTEM PONTOS DE CONSUMO SEM CONSUMO A SER FATURADO."
							.equals(historicoAnomaliaFaturamento.getFaturamentoAnormalidade().getDescricao())) {
						Collection<HistoricoConsumo> colecaoHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumo(
								historicoAnomaliaFaturamento.getPontoConsumo().getChavePrimaria(),
								historicoAnomaliaFaturamento.getAnoMesFaturamento(),
								historicoAnomaliaFaturamento.getNumeroCiclo(), null, Boolean.FALSE, null);
						
						for(HistoricoConsumo historico : colecaoHistoricoConsumo) {
							HistoricoMedicao historicoMed = historico.getHistoricoAtual();
							historicoMed.setDataLeituraFaturada(historicoMed.getDataLeituraInformada());
							historicoMed.setNumeroLeituraFaturada(historicoMed.getNumeroLeituraInformada());
							
							controladorHistoricoMedicao.atualizar(historicoMed);
							
							historico.setIndicadorFaturamento(Boolean.TRUE);
							controladorHistoricoConsumo.atualizar(historico);
						}
						
					}
				}

			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		model.addAttribute(PESQUISA_AUTOMATICA, true);
		return exibirPesquisaExcecaoLeituraFaturamento(pesquisaAnaliseAnormalidade, model);

	}

	/**
	 * Preparar filtro.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @return filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private Map<String, Object> prepararFiltro(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(MAX_RESULTS, analiseExcecaoFaturaVO.getQtdRegistros());
		filtro.put(OFFSET, analiseExcecaoFaturaVO.getOffset());

		prepararFiltroDatas(analiseExcecaoFaturaVO, filtro);
		prepararFiltroLocalizacao(analiseExcecaoFaturaVO, filtro);
		prepararFiltroConsumo(analiseExcecaoFaturaVO, filtro);
		prepararFiltroIndicadores(analiseExcecaoFaturaVO, filtro);
		prepararFiltroClientePontoConsumo(analiseExcecaoFaturaVO, filtro);
		prepararFiltroImovelPontoConsumo(analiseExcecaoFaturaVO, filtro);

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getPercentualVariacaoValorFatura())) {
			filtro.put(PERCENTUAL_VARIACAO_VALOR_FATURA, Util.converterCampoStringParaValorBigDecimal("Percentual de Variação Valor Fatura",
					analiseExcecaoFaturaVO.getPercentualVariacaoValorFatura(), Constantes.FORMATO_VALOR_FATOR_K, Constantes.LOCALE_PADRAO));
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getReferencia())) {
			filtro.put(REFERENCIA, analiseExcecaoFaturaVO.getReferencia().replaceAll("/", ""));
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getCiclo())
				&& Integer.parseInt(analiseExcecaoFaturaVO.getCiclo()) >= 0) {
			filtro.put(CICLO, analiseExcecaoFaturaVO.getCiclo());
		}

		return filtro;

	}

	/**
	 * Preparar filtro datas.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltroDatas(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro) throws GGASException {

		String ldataGeracaoInicial = analiseExcecaoFaturaVO.getDataGeracaoInicial();
		String ldataGeracaoFinal = analiseExcecaoFaturaVO.getDataGeracaoFinal();
		String ldataMedicaoInicial = analiseExcecaoFaturaVO.getDataMedicaoInicial();
		String ldataMedicaoFinal = analiseExcecaoFaturaVO.getDataMedicaoFinal();

		if (ldataGeracaoInicial != null) {
			Date dataGeracaoInicial = Util.converterCampoStringParaData(INTERVALO_GERACAO, ldataGeracaoInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_GERACAO_INICIAL, dataGeracaoInicial);
		}
		if (ldataGeracaoFinal != null) {
			Date dataGeracaoFinal = Util.converterCampoStringParaData(INTERVALO_GERACAO, ldataGeracaoFinal, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_GERACAO_FINAL, dataGeracaoFinal);
		}
		if (ldataMedicaoInicial != null) {
			Date dataMedicaoInicial = Util.converterCampoStringParaData(INTERVALO_GERACAO, ldataMedicaoInicial, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_MEDICAO_INICIAL, dataMedicaoInicial);
		}
		if (ldataMedicaoFinal != null) {
			Date dataMedicaoFinal = Util.converterCampoStringParaData(INTERVALO_GERACAO, ldataMedicaoFinal, Constantes.FORMATO_DATA_BR);
			filtro.put(DATA_MEDICAO_FINAL, dataMedicaoFinal);
		}
	}

	/**
	 * Preparar filtro localização.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param filtro {@link Map}
	 */
	private void prepararFiltroLocalizacao(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro) {

		if (analiseExcecaoFaturaVO.getIdLocalidade() > 0) {
			filtro.put(ID_LOCALIDADE, analiseExcecaoFaturaVO.getIdLocalidade());
		}

		if (analiseExcecaoFaturaVO.getIdSetorComercial() != null && analiseExcecaoFaturaVO.getIdSetorComercial() > 0) {
			filtro.put(ID_SETOR_COMERCIAL, analiseExcecaoFaturaVO.getIdSetorComercial());
		}

		if (analiseExcecaoFaturaVO.getIdGrupoFaturamento() != null
				&& analiseExcecaoFaturaVO.getIdGrupoFaturamento() > 0) {
			filtro.put(ID_GRUPO_FATURAMENTO, analiseExcecaoFaturaVO.getIdGrupoFaturamento());
		}

		if (analiseExcecaoFaturaVO.getIdSegmento() != null && analiseExcecaoFaturaVO.getIdSegmento() > 0) {
			filtro.put(ID_SEGMENTO, analiseExcecaoFaturaVO.getIdSegmento());
		}

		if (analiseExcecaoFaturaVO.getIdsRota() != null && analiseExcecaoFaturaVO.getIdsRota().length > 0) {
			filtro.put(IDS_ROTA, analiseExcecaoFaturaVO.getIdsRota());
		}

	}

	/**
	 * Preparar filtro consumo.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltroConsumo(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro) {

		if (analiseExcecaoFaturaVO.getIdTipoConsumo() != null && analiseExcecaoFaturaVO.getIdTipoConsumo() > 0) {
			filtro.put(ID_TIPO_CONSUMO, analiseExcecaoFaturaVO.getIdTipoConsumo());
		}

		if (analiseExcecaoFaturaVO.getIdSituacaoPontoConsumo() != null
				&& analiseExcecaoFaturaVO.getIdSituacaoPontoConsumo() > 0) {
			filtro.put(ID_SITUACAO_PONTO_CONSUMO, analiseExcecaoFaturaVO.getIdSituacaoPontoConsumo());
		}

		Long idAnormalidadeFatura = analiseExcecaoFaturaVO.getIdAnormalidadeFaturamento();
		if (idAnormalidadeFatura != null && idAnormalidadeFatura > 0) {
			filtro.put(ID_ANORMALIDADE_FATURAMENTO, idAnormalidadeFatura);
		}

	}

	/**
	 * Preparar filtro indicadores.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param filtro {@link Map}
	 */
	private void prepararFiltroIndicadores(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro) {

		if (analiseExcecaoFaturaVO.getAnalisada() != null) {
			filtro.put(ANALISADA, analiseExcecaoFaturaVO.getAnalisada());
		}

		if (analiseExcecaoFaturaVO.getIndicadorImpedeFaturamento() != null) {
			filtro.put(INDICADOR_IMPEDIMENTO_FATURAMENTO, analiseExcecaoFaturaVO.getIndicadorImpedeFaturamento());
		}

	}

	/**
	 * Preparar filtro imóvel ponto consumo.
	 *
	 * @param analiseExcecaoFaturaVO
	 * @param filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltroImovelPontoConsumo(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro)
			throws GGASException {

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getCepImovel())) {
			filtro.put(CEP_IMOVEL, analiseExcecaoFaturaVO.getCepImovel());
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getMatriculaImovel())) {
			controladorImovel.existeImovelPorMatricula(Util.converterCampoStringParaValorLong(
					"Matrícula do imóvel condomínio", analiseExcecaoFaturaVO.getMatriculaImovel()));
			filtro.put(MATRICULA_IMOVEL, Util.converterCampoStringParaValorLong(Imovel.MATRICULA,
					analiseExcecaoFaturaVO.getMatriculaImovel()));
		}

		Long idPontoConsumo = analiseExcecaoFaturaVO.getIdPontoConsumo();
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			filtro.put(ID_PONTO_CONSUMO, analiseExcecaoFaturaVO.getIdPontoConsumo());
		}

		if (analiseExcecaoFaturaVO.getIndicadorCondominio() != null) {
			filtro.put(INDICADOR_CONDOMINIO, analiseExcecaoFaturaVO.getIndicadorCondominio());
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getNomeImovel())) {
			filtro.put(NOME_IMOVEL, analiseExcecaoFaturaVO.getNomeImovel());
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getComplementoImovel())) {
			filtro.put(COMPLEMENTO_IMOVEL, Util.formatarTextoConsulta(analiseExcecaoFaturaVO.getComplementoImovel()));
		}

		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getNumeroImovel())) {
			filtro.put(NUMERO_IMOVEL, analiseExcecaoFaturaVO.getNumeroImovel());
		}

	}

	/**
	 * Preparar filtro cliente ponto consumo.
	 *
	 * @param analiseExcecaoFaturaVO
	 * @param filtro {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	private void prepararFiltroClientePontoConsumo(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Map<String, Object> filtro) {
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getCpfCliente())) {
			filtro.put(CPF_CLIENTE, analiseExcecaoFaturaVO.getCpfCliente());
		}
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getNomeCliente())) {
			filtro.put(NOME_CLIENTE, analiseExcecaoFaturaVO.getNomeCliente());
		}
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getRgCliente())) {
			filtro.put(RG_CLIENTE, analiseExcecaoFaturaVO.getRgCliente());
		}
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getNumeroPassaporte())) {
			filtro.put(NUMERO_PASSAPORTE, analiseExcecaoFaturaVO.getNumeroPassaporte());
		}
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getCnpjCliente())) {
			filtro.put(CNPJ_CLIENTE, analiseExcecaoFaturaVO.getCnpjCliente());
		}
		if (StringUtils.isNotEmpty(analiseExcecaoFaturaVO.getNomeFantasiaCliente())) {
			filtro.put(NOME_FANTASIA_CLIENTE, analiseExcecaoFaturaVO.getNomeFantasiaCliente());
		}
	}

	/**
	 * Carregar campos.
	 *
	 * @param analiseExcecaoFaturaVO {@link AnaliseExcecaoFaturaVO}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void carregarCampos(PesquisaAnaliseAnormalidade analiseExcecaoFaturaVO, Model model) throws GGASException {

		model.addAttribute("listaLocalidade", controladorLocalidade.listarLocalidades());
		model.addAttribute("listaSetorComercial", controladorSetorComercial.listarSetoresComerciais());
		model.addAttribute("listaGrupoFaturamento", controladorRota.listarGruposFaturamentoRotas());
		model.addAttribute("listaSegmento", controladorSegmento.listarSegmento());
		model.addAttribute("listaAnormalidadeFatura", controladorFaturamentoAnormalidade.listaAnormalidadeFaturamento()
				.stream().sorted(Comparator.comparing(FaturamentoAnormalidade::getDescricaoLower)).collect(Collectors.toList())
		);
		model.addAttribute("listaAnormalidadeConsumo", controladorAnormalidade.listarAnormalidadesConsumo());
		model.addAttribute("listaTiposConsumo", controladorHistoricoConsumo.listarTiposConsumo());
		model.addAttribute("listaSituacaoPontoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());

		model.addAttribute("listaCiclos",
				new ArrayList<Integer>(Arrays.asList(Constantes.Ciclo.UM.getValor(), Constantes.Ciclo.DOIS.getValor(),
						Constantes.Ciclo.TRES.getValor(), Constantes.Ciclo.QUATRO.getValor(),
						Constantes.Ciclo.CINCO.getValor(), Constantes.Ciclo.SEIS.getValor())));

		if (analiseExcecaoFaturaVO.getIdGrupoFaturamento() != null
				&& analiseExcecaoFaturaVO.getIdGrupoFaturamento() > 0) {
			model.addAttribute("listaRota",
					controladorRota.listarRotasPorGrupoFaturamento(analiseExcecaoFaturaVO.getIdGrupoFaturamento()));
		}

	}

	/**
	 * Incluir fatura anomalia.
	 *
	 * @param pesquisaAnaliseAnormalidade {@link AnaliseExcecaoFaturaVO}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarExcecaoLeituraFatura {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirFaturaAnomalia")
	public String incluirFaturaAnomalia(PesquisaAnaliseAnormalidade pesquisaAnaliseAnormalidade, HttpServletRequest request,
			Model model) throws GGASException {

		Long[] chavesPrimarias = pesquisaAnaliseAnormalidade.getChavesPrimarias();

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento = new ArrayList<HistoricoAnomaliaFaturamento>();

		boolean isTodosInseridosSucesso = Boolean.TRUE;

		try {

			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento = controladorHistoricoAnomaliaFaturamento
							.obterHistoricoAnomaliaFaturamento(chavesPrimarias[i]);
					listaHistoricoAnomaliaFaturamento.add(historicoAnomaliaFaturamento);
				}
				if (!listaHistoricoAnomaliaFaturamento.isEmpty()) {
					controladorHistoricoAnomaliaFaturamento
							.validarInclusaoFaturaHistoricoAnomaliaFaturamento(listaHistoricoAnomaliaFaturamento);
					boolean isAtualInseridoSucesso = controladorFatura.processarInclusaoFaturaAnomaliaCorrigida(
							listaHistoricoAnomaliaFaturamento, dadosAuditoria);
					isTodosInseridosSucesso = isAtualInseridoSucesso;
				}

			}

			if (isTodosInseridosSucesso) {
				mensagemSucesso(model, Constantes.SUCESSO_ANOMALIA_TRATADA, request);
			} else {
				mensagemErroParametrizado(model, Constantes.FALHA_ANOMALIA_TRATADA, request);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		model.addAttribute(PESQUISA_AUTOMATICA, true);
		return exibirPesquisaExcecaoLeituraFaturamento(pesquisaAnaliseAnormalidade, model);
	}
}
