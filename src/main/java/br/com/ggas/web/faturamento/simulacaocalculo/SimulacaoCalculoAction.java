/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.simulacaocalculo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.exception.CalculoFornecimentoException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável pela exibição da simulação dos Calculos de Fornecimento de
 * Gás, utilizando dados Reais.
 *
 */
@Controller
public class SimulacaoCalculoAction extends GenericAction {

	private static final String MOSTRAR_VALORES = "mostrarValores";

	private static final String SIMULACAO_DADOS_REAIS_VO = "simulacaoDadosReaisVO";

	private static final String EXIBIR_PONTOS_CONSUMO = "exibirPontosConsumo";

	private static final String LISTA_FORNECIMENTO_GAS = "listaFornecimentoGas";

	private static final String LISTA_SIMULAR_FORNECIMENTO_GAS = "listaSimularFornecimentoGas";

	private static final String ITEM_FATURA = "itemFatura";

	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	private static final String LISTA_TARIFAS_PONTO_CONSUMO = "listaTarifasPontoConsumo";

	private static final String LISTA_HISTORICO_CONSUMO = "listaHistoricoConsumo";

	private static final String LISTA_TARIFA_DADOS_FICTICIOS = "listaTarifa";

	private static final long CHAVE_AGRUPAMENTO_POR_VOLUME = 100;

	@Autowired
	private ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	private ControladorFatura controladorFatura;

	@Autowired
	private ControladorCliente controladorCliente;

	/**
	 * Método responsável por exibir a página de Simulação de Cálculo de Consumo com
	 * Dados Reais.
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirPesquisaSimulacaoCalculoDadosReais")
	public String exibirPesquisaSimulacaoCalculoDadosReais(HttpServletRequest request, Model model) {

		saveToken(request);
		return "exibirPesquisaSimulacaoCalculoDadosReais";
	}

	/**
	 * *********************** TESTE ************************. /** Método
	 * responsável por exibir a página de Exibição de Pontos de Consumo
	 *
	 * @param simulacaoDadosReaisVO
	 *            {@link SimulacaoDadosReaisVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException
	 *             {@link NegocioException}
	 */
	@RequestMapping("exibirPontosConsumoSimulacao")
	public String exibirPontosConsumoSimulacao(SimulacaoDadosReaisVO simulacaoDadosReaisVO, BindingResult result,
			HttpServletRequest request, Model model) throws NegocioException {

		String view = EXIBIR_PONTOS_CONSUMO;
		saveToken(request);
		Collection<PontoConsumo> pontosConsumo = null;
		Imovel imovel = null;
		ClienteImpl cliente = null;
		model.addAttribute(SIMULACAO_DADOS_REAIS_VO, simulacaoDadosReaisVO);

		try {
			if (simulacaoDadosReaisVO.getIdCliente() != null && simulacaoDadosReaisVO.getIdCliente() > 0) {
				cliente = (ClienteImpl) controladorCliente.obterCliente(simulacaoDadosReaisVO.getIdCliente());
				model.addAttribute("cliente", cliente);
				pontosConsumo = controladorPontoConsumo
						.listarPontoConsumoPorClienteSimulacaoCalculo(simulacaoDadosReaisVO.getIdCliente());
			} else if (simulacaoDadosReaisVO.getIdImovel() != null && simulacaoDadosReaisVO.getIdImovel() > 0) {
				pontosConsumo = controladorPontoConsumo
						.listarPontoConsumoPorChaveImovelSimulacaoCalculo(simulacaoDadosReaisVO.getIdImovel());
				imovel = (Imovel) controladorImovel.obter(simulacaoDadosReaisVO.getIdImovel());
				model.addAttribute("imovel", imovel);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = "exibirPesquisaSimulacaoCalculoDadosReais";
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, pontosConsumo);
		request.getSession().removeAttribute(LISTA_FORNECIMENTO_GAS);
		request.getSession().removeAttribute(LISTA_SIMULAR_FORNECIMENTO_GAS);

		return view;
	}

	/**
	 * Metodo que seleciona e carrega os item da linha selecionada.
	 *
	 * @param simulacaoDadosReaisVO
	 *            {@link SimulacaoDadosReaisVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("selecionarPontoConsumo")
	public String selecionarPontoConsumo(SimulacaoDadosReaisVO simulacaoDadosReaisVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(SIMULACAO_DADOS_REAIS_VO, simulacaoDadosReaisVO);
		model.addAttribute(MOSTRAR_VALORES, request.getParameter(MOSTRAR_VALORES));

		try {
			if (simulacaoDadosReaisVO.getChavePrimaria() != null && simulacaoDadosReaisVO.getChavePrimaria() > 0) {
				if (simulacaoDadosReaisVO.getIdPontoConsumo() == null) {
					simulacaoDadosReaisVO.setIdPontoConsumo(simulacaoDadosReaisVO.getChavePrimaria());
				}

				if(simulacaoDadosReaisVO.getIdImovel() == null) {
					ClienteImpl cliente = (ClienteImpl) controladorCliente
						.obterClientePorPontoConsumo(simulacaoDadosReaisVO.getChavePrimaria());
					simulacaoDadosReaisVO.setIdCliente(cliente.getChavePrimaria());
				}

				PontoConsumo pontoConsumo = controladorPontoConsumo
						.obterPontoConsumo(simulacaoDadosReaisVO.getChavePrimaria());

				Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
						.consultarHistoricoConsumoParaSimulacao(pontoConsumo.getChavePrimaria());
				ContratoPontoConsumo contratoPC = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("idSegmento", pontoConsumo.getSegmento().getChavePrimaria());
				filtro.put("habilitado", Boolean.TRUE);
				filtro.put("verificarTarifaVigenciaAutorizada", Boolean.TRUE);

				if (contratoPC == null) {
					contratoPC = controladorContrato
							.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
				}

				model.addAttribute(LISTA_TARIFA_DADOS_FICTICIOS, controladorTarifa.consultarTarifas(filtro));
				request.getSession().setAttribute(LISTA_TARIFA_DADOS_FICTICIOS, controladorTarifa.consultarTarifas(filtro));

				if (contratoPC != null) {

					Collection<Tarifa> listaTarifas = controladorCalculoFornecimentoGas
							.listarTarifasPorPontoConsumo(pontoConsumo, null, null);

					if (listaTarifas != null) {
						model.addAttribute(LISTA_TARIFAS_PONTO_CONSUMO, listaTarifas);
						request.getSession().setAttribute(LISTA_TARIFAS_PONTO_CONSUMO, listaTarifas);
					}

					// Obter pontos de consumo do
					// mesmo contrato.
					Long chaveContrato = Long.valueOf(contratoPC.getContrato().getChavePrimaria());
					simulacaoDadosReaisVO.setIdContrato(chaveContrato);

					EntidadeConteudo tipoAgrupamento = controladorEntidadeConteudo
							.obterEntidadeConteudo(CHAVE_AGRUPAMENTO_POR_VOLUME);
					Collection<PontoConsumo> listaPontoConsumoAgrupados = controladorPontoConsumo
							.listarPontoConsumoFaturamentoAgrupado(chaveContrato, tipoAgrupamento);

					if (listaPontoConsumoAgrupados != null && !listaPontoConsumoAgrupados.isEmpty()) {
						model.addAttribute("listaPontoConsumoAgrupados", listaPontoConsumoAgrupados);
					}

				}
				model.addAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);
				request.getSession().setAttribute(LISTA_HISTORICO_CONSUMO, listaHistoricoConsumo);
			}
		} catch (CalculoFornecimentoException e) {
			super.mensagemErro(model, e.getMessage());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPontosConsumoSimulacao(simulacaoDadosReaisVO, result, request, model);
	}

	/**
	 * Calcular dados reais.
	 *
	 * @param simulacaoDadosReaisVO
	 *            {@link SimulacaoDadosReaisVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws GGASException
	 *             {@link GGASException}
	 */
	@RequestMapping("calcularDadosReais")
	public String calcularDadosReais(SimulacaoDadosReaisVO simulacaoDadosReaisVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		String view = EXIBIR_PONTOS_CONSUMO;
		model.addAttribute(SIMULACAO_DADOS_REAIS_VO, simulacaoDadosReaisVO);
		Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<PontoConsumo>();
		model.addAttribute(MOSTRAR_VALORES, 1L);
		recuperarListasTela(request, model);

		try {
			validarToken(request);
			controladorCalculoFornecimentoGas.validaDadosCalcularValorFornecimentoGas(
					String.valueOf(simulacaoDadosReaisVO.getIdPontoConsumo()), simulacaoDadosReaisVO.getIdTarifaAtual(),
					simulacaoDadosReaisVO.getIdTarifaSimulada(), simulacaoDadosReaisVO.getIdMesAnoCiclo(),
					simulacaoDadosReaisVO.getIdVigenciaTarifa());

			Tarifa tarifaAtual = (Tarifa) controladorTarifa.obter(simulacaoDadosReaisVO.getIdTarifaAtual(),
					ITEM_FATURA);
			Tarifa tarifaSimulada = (Tarifa) controladorTarifa.obter(simulacaoDadosReaisVO.getIdTarifaSimulada(),
					ITEM_FATURA);
			TarifaVigencia tarifaVigencia = controladorTarifa
					.obterTarifaVigencia(simulacaoDadosReaisVO.getIdVigenciaTarifa(), "tarifa", "tarifa.tributos");

			PontoConsumo pontoConsumo = controladorPontoConsumo
					.obterPontoConsumo(simulacaoDadosReaisVO.getIdPontoConsumo());

			if ((!pontoConsumo.getRamoAtividade().equals(tarifaAtual.getRamoAtividade()))
					&& tarifaAtual.getRamoAtividade() != null) {
				throw new NegocioException(Constantes.ERRO_RA_TARIFA_DIVERGE_RA_PC, true);
			}
			 
			ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator
					.getInstancia()
					.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
			
			String scala = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO)
					.getValor();

			BigDecimal qtdCasasDecimaisTarifaAtual = new BigDecimal(scala);
			BigDecimal qtdCasasDecimaisTarifaSimulada = new BigDecimal(scala);

			HistoricoConsumo historicoConsumo = controladorHistoricoConsumo.obterHistoricoConsumoParaSimulacao(
					simulacaoDadosReaisVO.getIdMesAnoCiclo(), "historicoAnterior", "historicoAtual");

			Date dataInicial = null;
			Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
					.consultarHistoricoConsumoPorHistoricoConsumoSintetizador(historicoConsumo);
			if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {

				dataInicial = listaHistoricoConsumo.iterator().next().getHistoricoAtual().getDataLeituraInformada();

			} else if (historicoConsumo.getHistoricoAnterior() != null
					&& historicoConsumo.getHistoricoAnterior().getDataLeituraFaturada() != null) {

				dataInicial = historicoConsumo.getHistoricoAnterior().getDataLeituraFaturada();

			} else {

				Date dataLeitura = controladorHistoricoMedicao.setarDataLeituraAnterior(null,
						historicoConsumo.getHistoricoAtual().getPontoConsumo(),
						historicoConsumo.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor());
				dataInicial = dataLeitura;
			}

			Date dataFinal = DataUtil.gerarDataHmsZerados(historicoConsumo.getHistoricoAtual().getDataLeituraFaturada());

			BigDecimal consumoApurado = historicoConsumo.getConsumoApurado();

			// Verifica se possui faturamento agrupado
			// - mais de um ponto de consumo
			EntidadeConteudo tipoAgrupamento = controladorEntidadeConteudo
					.obterEntidadeConteudo(CHAVE_AGRUPAMENTO_POR_VOLUME);
			Collection<PontoConsumo> listaPontoConsumoAgrupados = controladorPontoConsumo
					.listarPontoConsumoFaturamentoAgrupado(simulacaoDadosReaisVO.getIdContrato(), tipoAgrupamento);

			int anoMesFaturamento = historicoConsumo.getAnoMesFaturamento();
			int ciclo = historicoConsumo.getNumeroCiclo();

			consumoApurado = obtemCollecaoPontoConsumo(colecaoPontoConsumo, pontoConsumo, consumoApurado,
					listaPontoConsumoAgrupados, anoMesFaturamento, ciclo);

			// Verificar se cliente está adimplente
			boolean isConsiderarDescontos = !controladorFatura
					.verificarInadimplenciaPontoConsumo(simulacaoDadosReaisVO.getIdPontoConsumo());

			// Chamada do método para calcular os
			// dados reais.
			DadosFornecimentoGasVO dadosFornecimentoGasReal = controladorCalculoFornecimentoGas
					.calcularValorFornecimentoGas(tarifaAtual.getItemFatura(), dataInicial, dataFinal, consumoApurado,
							colecaoPontoConsumo, isConsiderarDescontos, null, false, null, Boolean.FALSE, null, null);

			// Chamada do método para calcular os
			// dados simulados.
			DadosFornecimentoGasVO dadosFornecimentoGasSimulado = controladorCalculoFornecimentoGas
					.calcularValorFornecimentoGas(tarifaSimulada.getItemFatura(), dataInicial, dataFinal,
							consumoApurado, colecaoPontoConsumo, tarifaVigencia, isConsiderarDescontos, false, null,
							Boolean.FALSE);

			// Na simulação os dados serão exibidos
			// agrupados por faixa.
			Collection<DadosFaixaVO> dadosFaixaReal = controladorCalculoFornecimentoGas
					.agruparDadosFaixaParaExibicao(dadosFornecimentoGasReal);
			Collection<DadosFaixaVO> dadosFaixaSimulado = controladorCalculoFornecimentoGas
					.agruparDadosFaixaParaExibicao(dadosFornecimentoGasSimulado);

			// Se dados faixa for null ou vazio quer
			// dizer que não foi possível realizar o
			// agrupamento das faixas e será exibida
			// apenas uma linha totalizadora.
			if (dadosFaixaReal == null || dadosFaixaReal.isEmpty()) {
				dadosFaixaReal = controladorCalculoFornecimentoGas
						.agruparDadosVigenciaParaExibicao(dadosFornecimentoGasReal);
			}

			// Se dados faixa for null ou vazio quer
			// dizer que não foi possível realizar o
			// agrupamento das faixas e será exibida
			// apenas uma linha totalizadora.
			if (dadosFaixaSimulado == null || dadosFaixaSimulado.isEmpty()) {
				dadosFaixaSimulado = controladorCalculoFornecimentoGas
						.agruparDadosVigenciaParaExibicao(dadosFornecimentoGasSimulado);
			}

			// Preenche valor total liquido com e sem
			// imposto.
			controladorCalculoFornecimentoGas.preencherValoresTotalLiquido(dadosFaixaReal);
			controladorCalculoFornecimentoGas.preencherValoresTotalLiquido(dadosFaixaSimulado);

			Collection<DadosTributoVO> listaTributosAliquotaVO = dadosFornecimentoGasSimulado.getColecaoTributos();

			// É preciso adicionar na lista os
			// tributos substitutos.
			if (dadosFornecimentoGasSimulado.getIcmsSubstituto() != null) {
				listaTributosAliquotaVO.add(dadosFornecimentoGasSimulado.getIcmsSubstituto());
			}

			selecionarPontoConsumo(simulacaoDadosReaisVO, result, request, model);

			request.getSession().setAttribute("listaTributacao", listaTributosAliquotaVO);
			request.getSession().setAttribute(LISTA_FORNECIMENTO_GAS, dadosFaixaReal);
			request.getSession().setAttribute(LISTA_SIMULAR_FORNECIMENTO_GAS, dadosFaixaSimulado);
			model.addAttribute("listaTributacao", listaTributosAliquotaVO);
			model.addAttribute(LISTA_FORNECIMENTO_GAS, dadosFaixaReal);
			model.addAttribute(LISTA_SIMULAR_FORNECIMENTO_GAS, dadosFaixaSimulado);

			String mascaraTarifaAtual = Util.montarMascaraPorQtdCasasDecimais(qtdCasasDecimaisTarifaAtual.intValue());
			String mascaraTarifaSimulada = Util
					.montarMascaraPorQtdCasasDecimais(qtdCasasDecimaisTarifaSimulada.intValue());

			request.getSession().setAttribute("maskTarifaAtual", mascaraTarifaAtual);
			request.getSession().setAttribute("maskTarifaSimulada", mascaraTarifaSimulada);
			model.addAttribute("maskTarifaAtual", mascaraTarifaAtual);
			model.addAttribute("maskTarifaSimulada", mascaraTarifaSimulada);

		} catch (CalculoFornecimentoException e) {
			super.mensagemErro(model, e.getMessage());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirPontosConsumoSimulacao(simulacaoDadosReaisVO, result, request, model);
		}

		return view;

	}

	/**
	 * Método responsável pela ordenação da listagem em tela de Simulação de Dados
	 *
	 * @param simulacaoDadosReaisVO {@link SimulacaoDadosReaisVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarSimulacaoCalculo")
	public String pesquisarSimulacaoCalculo(SimulacaoDadosReaisVO simulacaoDadosReaisVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Collection<PontoConsumo> listaPontoConsumo = null;
		Boolean isPorCliente = null;

		if (simulacaoDadosReaisVO.getIndicadorPesquisa() != null) {
			if ("indicadorPesquisaCliente".equals(simulacaoDadosReaisVO.getIndicadorPesquisa())) {
				isPorCliente = Boolean.TRUE;
			} else if ("indicadorPesquisaImovel".equals(simulacaoDadosReaisVO.getIndicadorPesquisa())) {
				isPorCliente = Boolean.FALSE;
			}
		}

		if (isPorCliente != null) {
			if (isPorCliente.booleanValue()) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(simulacaoDadosReaisVO.getIdCliente());
			} else {
				listaPontoConsumo = controladorImovel.listarPontoConsumoPorChaveImovel(simulacaoDadosReaisVO.getIdImovel());
			}
		}

		model.addAttribute(LISTA_PONTO_CONSUMO, listaPontoConsumo);
		return exibirPontosConsumoSimulacao(simulacaoDadosReaisVO, result, request, model);
	}

	private BigDecimal obtemCollecaoPontoConsumo(Collection<PontoConsumo> colecaoPontoConsumo,
			PontoConsumo pontoConsumo, BigDecimal consumoApurado, Collection<PontoConsumo> listaPontoConsumoAgrupados,
			int anoMesFaturamento, int ciclo) throws GGASException {
		if (listaPontoConsumoAgrupados != null && !listaPontoConsumoAgrupados.isEmpty()) {

			colecaoPontoConsumo.addAll(listaPontoConsumoAgrupados);

			// obter consumo apurado dos pontos de
			// consumo para essa referencia.
			consumoApurado = controladorHistoricoConsumo.obterConsumoApuradoPontoConsumoPorReferencia(
					anoMesFaturamento, ciclo, listaPontoConsumoAgrupados);

		} else {
			colecaoPontoConsumo.add(pontoConsumo);
		}
		return consumoApurado;
	}

	/**
	 * Método responsável por recuperar listas disponíveis em tela
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 */
	private void recuperarListasTela(HttpServletRequest request, Model model) {
		if (request.getSession().getAttribute(LISTA_HISTORICO_CONSUMO) != null) {
			model.addAttribute(LISTA_HISTORICO_CONSUMO, request.getSession().getAttribute(LISTA_HISTORICO_CONSUMO));
		}
		if (request.getSession().getAttribute(LISTA_TARIFAS_PONTO_CONSUMO) != null) {
			model.addAttribute(LISTA_TARIFAS_PONTO_CONSUMO,
					request.getSession().getAttribute(LISTA_TARIFAS_PONTO_CONSUMO));
		}
		if (request.getSession().getAttribute(LISTA_TARIFA_DADOS_FICTICIOS) != null) {
			model.addAttribute(LISTA_TARIFA_DADOS_FICTICIOS,
					request.getSession().getAttribute(LISTA_TARIFA_DADOS_FICTICIOS));
		}
	}

}
