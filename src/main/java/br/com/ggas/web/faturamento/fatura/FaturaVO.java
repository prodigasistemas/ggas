/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class FaturaVO extends EntidadeNegocioImpl implements EntidadeNegocio, Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7930617019637937912L;

	private String valor; 
	
	private String vencimento;
	
	private String tipo;
	
	private String descricao;
	
	private String numero;
	
	private Fatura fatura;

	private BigDecimal saldo;

	private BigDecimal saldoCorrigido;

	private DocumentoFiscal documentoFiscal;

	private BigDecimal valorAtualizado;

	private Date dataPagamento;

	private String numeroCodigoBarras;

	private String status;

	private String numeroNotaFiscal;

	private Date dataInicio;

	private Date dataFinal;

	private Date dataEmissao;

	private String consumo;

	private Date dataVencimento;

	private Date dataApresentacao;

	private Date dataInicioMedicao;

	private Date dataFinalMedicao;

	private Long quantidadeRevisao;

	public Fatura getFatura() {

		return fatura;
	}

	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	public BigDecimal getSaldo() {

		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	/**
	 * @return the saldoCorrigido
	 */
	public BigDecimal getSaldoCorrigido() {
	
		return saldoCorrigido;
	}
	
	/**
	 * @param saldoCorrigido the saldoCorrigido to set
	 */
	public void setSaldoCorrigido(BigDecimal saldoCorrigido) {
	
		this.saldoCorrigido = saldoCorrigido;
	}

	public DocumentoFiscal getDocumentoFiscal() {

		return documentoFiscal;
	}

	public void setDocumentoFiscal(DocumentoFiscal documentoFiscal) {

		this.documentoFiscal = documentoFiscal;
	}

	@Override
	public long getChavePrimaria() {

		long chave = 0;
		if(fatura != null) {
			chave = fatura.getChavePrimaria();
		}
		return chave;
	}

	@Override
	public void setChavePrimaria(long chavePrimaria) {

		if(fatura != null) {
			fatura.setChavePrimaria(chavePrimaria);
		}
	}

	@Override
	public boolean isHabilitado() {

		boolean habilitado = false;
		if(fatura != null) {
			habilitado = fatura.isHabilitado();
		}
		return habilitado;
	}

	@Override
	public void setHabilitado(boolean habilitado) {

		if(fatura != null) {
			fatura.setHabilitado(habilitado);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	public BigDecimal getValorAtualizado() {

		return valorAtualizado;
	}

	public void setValorAtualizado(BigDecimal valorAtualizado) {

		this.valorAtualizado = valorAtualizado;
	}

	public Date getDataPagamento() {
		Date data = null;
		if (this.dataPagamento != null) {
			data = (Date) dataPagamento.clone();
		}
		return data;
	}

	public void setDataPagamento(Date dataPagamento) {

		this.dataPagamento = dataPagamento;
	}

	public String getNumeroCodigoBarras() {

		return numeroCodigoBarras;
	}

	public void setNumeroCodigoBarras(String numeroCodigoBarras) {

		this.numeroCodigoBarras = numeroCodigoBarras;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public String getNumeroNotaFiscal() {

		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {

		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	public Date getDataInicio() {

		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		if(dataInicio != null){
			this.dataInicio = (Date) dataInicio.clone();
		} else {
			this.dataInicio = null;
		}
	}

	public Date getDataFinal() {
		Date data = null;
		if (this.dataFinal != null) {
			data = (Date) dataFinal.clone();
		}
		return data;
	}

	public void setDataFinal(Date dataFinal) {

		this.dataFinal = dataFinal;
	}

	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	public String getConsumo() {

		return consumo;
	}

	public void setConsumo(String consumo) {

		this.consumo = consumo;
	}

	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	public Date getDataApresentacao() {
		Date data = null;
		if (this.dataApresentacao != null) {
			data = (Date) dataApresentacao.clone();
		}
		return data;
	}

	public void setDataApresentacao(Date dataApresentacao) {

		this.dataApresentacao = dataApresentacao;
	}

	public Date getDataInicioMedicao() {
		Date data = null;
		if (this.dataInicioMedicao != null) {
			data = (Date) dataInicioMedicao.clone();
		}
		return data;
	}

	public void setDataInicioMedicao(Date dataInicioMedicao) {

		this.dataInicioMedicao = dataInicioMedicao;
	}

	public Date getDataFinalMedicao() {
		Date data = null;
		if (this.dataFinalMedicao != null) {
			data = (Date) dataFinalMedicao.clone();
		}
		return data;
	}

	public void setDataFinalMedicao(Date dataFinalMedicao) {

		this.dataFinalMedicao = dataFinalMedicao;
	}

	public Long getQuantidadeRevisao() {

		return quantidadeRevisao;
	}

	public void setQuantidadeRevisao(Long quantidadeRevisao) {

		this.quantidadeRevisao = quantidadeRevisao;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {

		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {

		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {

		this.tipo = tipo;
	}

	/**
	 * @return the vencimento
	 */
	public String getVencimento() {

		return vencimento;
	}

	/**
	 * @param vencimento the vencimento to set
	 */
	public void setVencimento(String vencimento) {

		this.vencimento = vencimento;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {

		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {

		this.valor = valor;
	}

}
