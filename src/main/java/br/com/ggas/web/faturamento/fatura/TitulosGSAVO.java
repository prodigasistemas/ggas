package br.com.ggas.web.faturamento.fatura;

public class TitulosGSAVO {
	private String numeroTitulo;
	private String dataEmissao;
	private String dataVencimento;
	private String valorDocumento;
	private String situacaoTitulo;
	private String numeroNotaFiscal;
	private String status;
	private String titularTitulo;
	private String dataDoPagamento;
	private String cnpjcpf;
	private String diasAtraso;
	private String dataProtocoloCorte;
	private String sequencialTitulo;
	
	public String getNumeroTitulo() {
		return numeroTitulo;
	}
	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public String getValorDocumento() {
		return valorDocumento;
	}
	public void setValorDocumento(String valorDocumento) {
		this.valorDocumento = valorDocumento;
	}
	public String getSituacaoTitulo() {
		return situacaoTitulo;
	}
	public void setSituacaoTitulo(String situacaoTitulo) {
		this.situacaoTitulo = situacaoTitulo;
	}
	public String getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}
	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTitularTitulo() {
		return titularTitulo;
	}
	public void setTitularTitulo(String titularTitulo) {
		this.titularTitulo = titularTitulo;
	}
	public String getDataDoPagamento() {
		return dataDoPagamento;
	}
	public void setDataDoPagamento(String dataDoPagamento) {
		this.dataDoPagamento = dataDoPagamento;
	}
	public String getCnpjcpf() {
		return cnpjcpf;
	}
	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}
	public String getDiasAtraso() {
		return diasAtraso;
	}
	public void setDiasAtraso(String diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	public String getDataProtocoloCorte() {
		return dataProtocoloCorte;
	}
	public void setDataProtocoloCorte(String dataProtocoloCorte) {
		this.dataProtocoloCorte = dataProtocoloCorte;
	}
	public String getSequencialTitulo() {
		return sequencialTitulo;
	}
	public void setSequencialTitulo(String sequencialTitulo) {
		this.sequencialTitulo = sequencialTitulo;
	}
	
	

}
