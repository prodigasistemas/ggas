/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.tarifa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.faturamento.tarifa.impl.PrecoGasImpl;
import br.com.ggas.faturamento.tarifa.impl.PrecoGasItemImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelo registro do preço do Gás
 * 
 * 
 */
@Controller
public class PrecoGasAction extends GenericAction {

	private static final String PRECO_GAS_ITEM = "precoGasItem";

	private static final String TELA_EXIBIR_INSERCAO_PRECO_GAS = "exibirInsercaoPrecoGas";

	private static final String TELA_EXIBIR_ALTERACAO_PRECO_GAS = "exibirAlteracaoPrecoGas";

	private static final String RETORNO_TELA = "retornoTela";

	private static final String LISTA_CONTRATO_COMPRA = "listaContratoCompra";

	private static final String ID_CONTRATO_COMPRA = "idContratoCompra";

	private static final String PRECO_GAS_ITENS = "precoGasItens";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String INDEX_LISTA = "indexLista";

	private static final String VALOR = "valor";

	private static final String DATA_FORNECIMENTO_FIM = "dataFornecimentoFim";

	private static final String DATA_FORNECIMENTO_INICIO = "dataFornecimentoInicio";

	private static final String DATA_EMISSAO_NOTA_FISCAL = "dataEmissaoNotaFiscal";

	private static final String DATA_DE_VENCIMENTO = "Data de Vencimento";

	private static final String DATA_VENCIMENTO = "dataVencimento";

	private static final String DATA_DE_FIM_DO_FORNECIMENTO = "Data de Fim do Fornecimento";

	private static final String DATA_DE_INICIO_DO_FORNECIMENTO = "Data de Início do Fornecimento";

	private static final String DATA_DE_EMISSAO_DA_NOTA_FISCAL = "Data de Emissão da Nota Fiscal";

	private static final String LISTA_ITEM_FATURA = "listaItemFatura";

	private static final String REQUEST_LISTA_PRECO_GAS_ITEM = "listaPrecoGasItem";

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String NUMERO_NOTA_FISCAL_PRECO_GAS = "numeroNotaFiscal";

	private static final String DATA_INICIO_EMISSAO_PRECO_GAS = "dataInicioEmissao";

	private static final String DATA_FIM_EMISSAO_PRECO_GAS = "dataFimEmissao";

	private static final String LISTA_PRECO_GAS = "listaPrecoGas";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String VOLUME = "volume";

	private static final String PRECO_GAS = "precoGas";

	@Autowired
	private ControladorPrecoGas controladorPrecoGas;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * Método responsável para exibir a tela da transação pesquisar Preço do Gás.
	 * 
	 * @throws NegocioException
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaPrecoGas")
	public String exibirPesquisaPrecoGas(Model model, HttpServletRequest request) throws NegocioException {

		request.getSession().removeAttribute(PRECO_GAS);
		model.addAttribute(LISTA_CONTRATO_COMPRA, controladorEntidadeConteudo.obterListaContratoCompra());

		return "exibirPesquisaPrecoGas";
	}

	/**
	 * Método responsável por pesquisar os Preços do Gas.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("pesquisarPrecoGas")
	public String pesquisarPrecoGas(PrecoGasImpl precoGas, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute("precoGas", precoGas);
		Map<String, Object> filtro = new HashMap<String, Object>();
		Collection<PrecoGas> listaPrecoGas = null;
		try {
			prepararFiltroPrecoGas(filtro, precoGas, request, model);
			listaPrecoGas = controladorPrecoGas.consultarPrecoGas(filtro);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}
		
		model.addAttribute(LISTA_PRECO_GAS, listaPrecoGas);

		return "forward:exibirPesquisaPrecoGas";
	}

	/**
	 * Método responsável para adicionar um item ao Preço do Gás na tela de
	 * inclusão.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param precoGasItem
	 *            - {@link PrecoGasItemImpl}
	 * @param results
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked" })
	@RequestMapping("adicionarPrecoGasItem")
	public String adicionarPrecoGasItem(PrecoGasImpl precoGas, BindingResult result, PrecoGasItemImpl precoGasItem,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		try {
			model.addAttribute(PRECO_GAS, precoGas);
			popularPrecoGasRegistro(precoGas, results, precoGasItem, request, model);
			validarToken(request);
			precoGasItem.setDadosAuditoria(getDadosAuditoria(request));
			precoGasItem.setHabilitado(true);
			precoGasItem.setUltimaAlteracao(Calendar.getInstance().getTime());

			Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
					.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			if (precoGasItens == null) {
				precoGasItens = new ArrayList<PrecoGasItem>();
			}

			controladorPrecoGas.validarPrecoGasItem(precoGas, precoGasItens, precoGasItem);
			precoGasItens.add(precoGasItem);
			model.addAttribute(DATA_VENCIMENTO, null);
			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (GGASException e) {
			model.addAttribute(PRECO_GAS, precoGas);
			model.addAttribute(PRECO_GAS_ITEM, precoGasItem);
			mensagemErroParametrizado(model, request, e);
		}

		model.addAttribute("volumeEstado", String.valueOf('A'));
		this.carregarCampos(model);
		saveToken(request);

		return TELA_EXIBIR_INSERCAO_PRECO_GAS;
	}

	/**
	 * Método responsável para adicionar um item ao Preço do Gás na tela de
	 * alteração.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param precoGasItem
	 *            - {@link PrecoGasItemImpl}
	 * @param results
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return string - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked" })
	@RequestMapping("adicionarPrecoGasItemAlteracao")
	public String adicionarPrecoGasItemAlteracao(PrecoGasImpl precoGas, BindingResult result,
			PrecoGasItemImpl precoGasItem, BindingResult results, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			popularPrecoGasRegistro(precoGas, results, precoGasItem, request, model);
			validarToken(request);
			precoGasItem.setDadosAuditoria(getDadosAuditoria(request));
			precoGasItem.setHabilitado(true);
			precoGasItem.setUltimaAlteracao(Calendar.getInstance().getTime());

			Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
					.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			if (precoGasItens == null) {
				precoGasItens = new ArrayList<PrecoGasItem>();
			}


			precoGasItens.add(precoGasItem);

			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			model.addAttribute(PRECO_GAS, precoGas);
			model.addAttribute(PRECO_GAS_ITEM, precoGasItem);
			mensagemErroParametrizado(model, request, e);
		}

		this.carregarCampos(model);

		model.addAttribute("volumeEstado", String.valueOf('A'));

		saveToken(request);

		return TELA_EXIBIR_ALTERACAO_PRECO_GAS;
	}

	/**
	 * Método responsável para excluir um item ao Preço do Gás na tela de inclusão.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param indexLista
	 *            - {@link Integer}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("excluirPrecoGasItem")
	public String excluirPrecoGasItem(PrecoGasImpl precoGas, BindingResult result,
			@RequestParam(INDEX_LISTA) Integer indexLista, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			this.popularPrecoGas(precoGas, request, model);
			model.addAttribute(PRECO_GAS, precoGas);
			validarToken(request);
			Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
					.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			if (precoGasItens != null && indexLista != null) {
				((List) precoGasItens).remove(indexLista.intValue());
			}

			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}
		this.carregarCampos(model);

		return TELA_EXIBIR_INSERCAO_PRECO_GAS;
	}

	/**
	 * Método responsável para excluir um item ao Preço do Gás na tela de Alteração.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param indexLista
	 *            - {@link Integer}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping("excluirPrecoGasItemAlteracao")
	public String excluirPrecoGasItemAlteracao(PrecoGasImpl precoGas, BindingResult result,
			@RequestParam(INDEX_LISTA) Integer indexLista, HttpServletRequest request, Model model)
			throws GGASException {

		try {
			this.popularPrecoGas(precoGas, request, model);
			model.addAttribute(PRECO_GAS, precoGas);
			validarToken(request);
			Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
					.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			if (precoGasItens != null && indexLista != null) {
				controladorPrecoGas
						.validarRemoverPrecoGasItem((PrecoGasItem) ((List) precoGasItens).get(indexLista.intValue()));
				((List) precoGasItens).remove(indexLista.intValue());
			}

			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			request.setAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		carregarCampos(model);

		saveToken(request);

		return TELA_EXIBIR_ALTERACAO_PRECO_GAS;
	}

	/**
	 * Método responsável para popular o Preço do Gás.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	private void popularPrecoGas(PrecoGasImpl precoGas, HttpServletRequest request, Model model) throws GGASException {

		String dataEmissaoNotaFiscal = (String) request.getParameter(DATA_EMISSAO_NOTA_FISCAL);
		String dataFornecimentoInicio = (String) request.getParameter(DATA_FORNECIMENTO_INICIO);
		String dataFornecimentoFim = (String) request.getParameter(DATA_FORNECIMENTO_FIM);
		model.addAttribute(DATA_FORNECIMENTO_FIM, dataFornecimentoFim);
		model.addAttribute(DATA_FORNECIMENTO_INICIO, dataFornecimentoInicio);
		model.addAttribute(DATA_EMISSAO_NOTA_FISCAL, dataEmissaoNotaFiscal);

		Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
				.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);

		if (dataEmissaoNotaFiscal != null && !dataEmissaoNotaFiscal.isEmpty()) {
			precoGas.setDataEmissaoNotaFiscal(Util.converterCampoStringParaData(DATA_DE_EMISSAO_DA_NOTA_FISCAL,
					dataEmissaoNotaFiscal, Constantes.FORMATO_DATA_BR));
		}

		if (dataFornecimentoInicio != null && !dataFornecimentoInicio.isEmpty()) {
			precoGas.setDataFornecimentoInicio(Util.converterCampoStringParaData(DATA_DE_INICIO_DO_FORNECIMENTO,
					dataFornecimentoInicio, Constantes.FORMATO_DATA_BR));

		}

		if (dataFornecimentoFim != null && !dataFornecimentoFim.isEmpty()) {
			precoGas.setDataFornecimentoFim(Util.converterCampoStringParaData(DATA_DE_FIM_DO_FORNECIMENTO,
					dataFornecimentoFim, Constantes.FORMATO_DATA_BR));
		}

		if (precoGasItens != null) {
			precoGas.getPrecoGasItens().clear();
			precoGas.getPrecoGasItens().addAll(precoGasItens);
		}
	}

	/**
	 * Método responsável para popular o item ao Preço do Gás .
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param precoGasItem
	 *            - {@link PrecoGasItemImpl}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void popularPrecoGasItem(HttpServletRequest request, PrecoGasItemImpl precoGasItem, Model model)
			throws GGASException {

		String valor = (String) request.getParameter(VALOR);
		String volume = (String) request.getParameter(VOLUME);

		String dataVencimento = (String) request.getParameter(DATA_VENCIMENTO);

		model.addAttribute(DATA_VENCIMENTO, dataVencimento);
		precoGasItem.setValor(null);
		precoGasItem.setVolume(null);
		precoGasItem.setDataVencimento(null);

		if (valor != null && !valor.isEmpty()) {
			precoGasItem.setValor(Util.converterCampoStringParaValorBigDecimal("Valor", valor,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (volume != null && !volume.isEmpty()) {
			precoGasItem.setVolume(Util.converterCampoStringParaValorBigDecimal("Volume", volume,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		model.addAttribute(PRECO_GAS_ITEM, precoGasItem);

		if (dataVencimento != null && !dataVencimento.isEmpty()) {
			precoGasItem.setDataVencimento(
					Util.converterCampoStringParaData(DATA_DE_VENCIMENTO, dataVencimento, Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * Método responsável para exibir a tela de inserir o Preço do Gás.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 * @return String - {@link String}
	 */
	@RequestMapping(TELA_EXIBIR_INSERCAO_PRECO_GAS)
	public String exibirInsercaoPrecoGas(HttpServletRequest request, Model model) throws GGASException {

		if (request.getParameter(RETORNO_TELA) == null) {
			request.getSession().removeAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
		}

		carregarCampos(model);
		saveToken(request);

		return TELA_EXIBIR_INSERCAO_PRECO_GAS;

	}

	/**
	 * Método responsável para exibir a tela de lteração do Preço do Gás.
	 * 
	 * @param precoGasTmp
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(TELA_EXIBIR_ALTERACAO_PRECO_GAS)
	public String exibirAlteracaoPrecoGas(PrecoGasImpl precoGasTmp, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		if (!super.isPostBack(request)) {

			if (request.getParameter(RETORNO_TELA) == null) {
				salvarPesquisa(request, precoGasTmp, PRECO_GAS);
				salvarDatasPesquisa(request);
				request.getSession().removeAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			}
			if (!Boolean.valueOf(request.getParameter(RETORNO_TELA))) {

				Collection<PrecoGasItem> precoGasItens = null;
				try {
					PrecoGas precoGas = (PrecoGas) controladorPrecoGas.obter(chavePrimaria, PRECO_GAS_ITENS);
					model.addAttribute(PRECO_GAS, precoGas);
					model.addAttribute(DATA_FORNECIMENTO_FIM, Util.converterDataParaStringSemHora(
							precoGas.getDataFornecimentoFim(), Constantes.FORMATO_DATA_BR));
					model.addAttribute(DATA_FORNECIMENTO_INICIO, Util.converterDataParaStringSemHora(
							precoGas.getDataFornecimentoInicio(), Constantes.FORMATO_DATA_BR));
					model.addAttribute(DATA_EMISSAO_NOTA_FISCAL, Util.converterDataParaStringSemHora(
							precoGas.getDataEmissaoNotaFiscal(), Constantes.FORMATO_DATA_BR));

					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put("idPrecoGas", precoGas.getChavePrimaria());
					precoGasItens = controladorPrecoGas.consultarPrecoGasItem(filtro);
				} catch (GGASException e) {
					mensagemErroParametrizado(model, request, e);
				}

				request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
				model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
			}
		}

		carregarCampos(model);
		saveToken(request);

		return TELA_EXIBIR_ALTERACAO_PRECO_GAS;
	}

	/**
	 * Responsável por salvar as datas utilizadas na pesquisa.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 */
	private void salvarDatasPesquisa(HttpServletRequest request) {

		String periodoEmissaoInicio = request.getParameter(DATA_INICIO_EMISSAO_PRECO_GAS);
		String periodoEmissaoFim = request.getParameter(DATA_FIM_EMISSAO_PRECO_GAS);

		salvarPesquisa(request, periodoEmissaoInicio, DATA_INICIO_EMISSAO_PRECO_GAS);
		salvarPesquisa(request, periodoEmissaoFim, DATA_FIM_EMISSAO_PRECO_GAS);
	}

	/**
	 * Método responsável para Criar o preço do Gás.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param precoGasItem
	 *            - {@link PrecoGasItemImpl}
	 * @param results
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("inserirPrecoGas")
	public String inserirPrecoGas(PrecoGasImpl precoGas, BindingResult result, PrecoGasItemImpl precoGasItem,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		String view = "";
		try {
			this.popularPrecoGas(precoGas, request, model);
			validarToken(request);
			precoGas.setDadosAuditoria(getDadosAuditoria(request));
			precoGas.setHabilitado(true);
			precoGas.setUltimaAlteracao(Calendar.getInstance().getTime());

			controladorPrecoGas.inserirPrecoGas(precoGas);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, PrecoGas.PRECO_GAS);
			view = pesquisarPrecoGas(precoGas, result, request, model);
		} catch (GGASException e) {
			model.addAttribute(PRECO_GAS, precoGas);
			try {
				popularPrecoGasItem(request, precoGasItem, model);
			} catch (GGASException e2) {
				LOG.info(e2);
				model.addAttribute(PRECO_GAS_ITEM, precoGasItem);
				mensagemErroParametrizado(model, request, e);
			}
			mensagemErroParametrizado(model, request, e);
			view = exibirInsercaoPrecoGas(request, model);
		}

		return view;
	}

	/**
	 * Método responsável para Alterar o Preço do Gás.
	 * 
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param precoGasItem
	 *            - {@link PrecoGasItemImpl}
	 * @param results
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarPrecoGas")
	public String alterarPrecoGas(PrecoGasImpl precoGas, BindingResult result, PrecoGasItemImpl precoGasItem,
			BindingResult results, HttpServletRequest request, Model model) throws GGASException {

		String view = "";
		try {
			this.popularPrecoGas(precoGas, request, model);
			validarToken(request);
			model.addAttribute(PRECO_GAS_ITEM, precoGasItem);
			precoGas.setDadosAuditoria(getDadosAuditoria(request));
			precoGas.setUltimaAlteracao(Calendar.getInstance().getTime());

			controladorPrecoGas.atualizarPrecoGas(precoGas);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, PrecoGas.PRECO_GAS);
			view = pesquisarPrecoGas(precoGas, result, request, model);
		} catch (GGASException e) {
			model.addAttribute(PRECO_GAS, precoGas);
			try {
				popularPrecoGasItem(request, precoGasItem, model);
			} catch (GGASException e2) {
				LOG.info(e2);
				model.addAttribute(PRECO_GAS_ITEM, precoGasItem);
				mensagemErroParametrizado(model, request, e);
			}
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoPrecoGas(precoGas, results, precoGas.getChavePrimaria(), request, model);
		}

		return view;
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            - {@link }
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFaturaPrecoGas());
		model.addAttribute(LISTA_CONTRATO_COMPRA, controladorEntidadeConteudo.obterListaContratoCompra());

		model.addAttribute("intervaloAnosData", intervaloAnosData());
	}

	/**
	 * Intervalo anos data.
	 * 
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private String intervaloAnosData() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Método responsável para Criar o filtro do preço do Gás.
	 * 
	 * @throws GGASException
	 * 
	 * @param filtro
	 *            - {@link Map}
	 * @param precoGas
	 *            - {@link PrecoGasImpl}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void prepararFiltroPrecoGas(Map<String, Object> filtro, PrecoGasImpl precoGas, HttpServletRequest request,
			Model model) throws GGASException {

		String dataFimEmissao = request.getParameter(DATA_FIM_EMISSAO_PRECO_GAS);
		String dataInicioEmissao = request.getParameter(DATA_INICIO_EMISSAO_PRECO_GAS);
		model.addAttribute(DATA_INICIO_EMISSAO_PRECO_GAS, dataInicioEmissao);
		model.addAttribute(DATA_FIM_EMISSAO_PRECO_GAS, dataFimEmissao);

		if (precoGas != null) {
			if (precoGas.getNumeroNotaFiscal() != null) {
				filtro.put(NUMERO_NOTA_FISCAL_PRECO_GAS, precoGas.getNumeroNotaFiscal().toString());
			}

			if (precoGas.getContratoCompra() != null && precoGas.getContratoCompra().getChavePrimaria() > 0) {
				filtro.put(ID_CONTRATO_COMPRA, precoGas.getContratoCompra().getChavePrimaria());
			}
		}

		if (dataInicioEmissao != null && !StringUtils.isEmpty(dataInicioEmissao)) {
			filtro.put(DATA_INICIO_EMISSAO_PRECO_GAS, Util.converterCampoStringParaData(DATA_INICIO_EMISSAO_PRECO_GAS,
					dataInicioEmissao, Constantes.FORMATO_DATA_BR));
		}

		if (dataFimEmissao != null && !StringUtils.isEmpty(dataFimEmissao)) {
			filtro.put(DATA_FIM_EMISSAO_PRECO_GAS,
					Util.converterCampoStringParaData(DATA_FIM_EMISSAO_PRECO_GAS, dataFimEmissao, Constantes.FORMATO_DATA_BR));
		}
	}

	/**
	 * Método responsável por excluir o Preco Gas.
	 * 
	 * @throws GGASException
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("excluirPrecoGas")
	public String excluirPrecoGas(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			DadosAuditoria auditoria = getDadosAuditoria(request);
			controladorPrecoGas.removerPrecoGas(chavesPrimarias, auditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, PrecoGas.PRECO_GAS);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarPrecoGas(null, null, request, model);
	}

	/**
	 * Método responsável para exibir o detalhamento do preço do Gás.
	 * 
	 * @param precoGasTmp
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoPrecoGas")
	public String exibirDetalhamentoPrecoGas(PrecoGasImpl precoGasTmp, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		if (!super.isPostBack(request)) {

			if (request.getParameter(RETORNO_TELA) == null) {
				salvarPesquisa(request, precoGasTmp, PRECO_GAS);
				salvarDatasPesquisa(request);
				request.getSession().removeAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);
			}

			PrecoGasImpl precoGas = (PrecoGasImpl) controladorPrecoGas.obter(chavePrimaria, PRECO_GAS_ITENS);
			model.addAttribute(PRECO_GAS, precoGas);

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idPrecoGas", precoGas.getChavePrimaria());
			Collection<PrecoGasItem> precoGasItens = controladorPrecoGas.consultarPrecoGasItem(filtro);

			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		}

		carregarCampos(model);

		saveToken(request);

		return "exibirDetalhamentoPrecoGas";
	}

	/**
	 * 
	 * @param precoGasTmp
	 *            - {@link PrecoGasImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("voltaPrecoGas")
	public String voltarPrecoGas(PrecoGasImpl precoGasTmp, BindingResult result, HttpServletRequest request,
			Model model) throws GGASException {
		PrecoGasImpl precoGas = precoGasTmp;
		if (precoGasTmp != null) {
			precoGas = (PrecoGasImpl) retornaPesquisa(request, PRECO_GAS);
			request.setAttribute(DATA_INICIO_EMISSAO_PRECO_GAS,
					retornaPesquisa(request, DATA_INICIO_EMISSAO_PRECO_GAS));
			request.setAttribute(DATA_FIM_EMISSAO_PRECO_GAS, retornaPesquisa(request, DATA_FIM_EMISSAO_PRECO_GAS));
			request.removeAttribute(PRECO_GAS);
		}

		return pesquisarPrecoGas(precoGas, result, request, model);
	}

	/**
	 * Método responsável por popular das entidades da tela
	 * @param precoGas
	 * @param result
	 * @param precoGasItem
	 * @param request
	 * @param model
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("popularPrecoGasRegistro")
	private void popularPrecoGasRegistro(PrecoGasImpl precoGas, BindingResult result, PrecoGasItemImpl precoGasItem,
			HttpServletRequest request, Model model) throws GGASException {

		String dataEmissaoNotaFiscal = (String) request.getParameter(DATA_EMISSAO_NOTA_FISCAL);
		String dataFornecimentoInicio = (String) request.getParameter(DATA_FORNECIMENTO_INICIO);
		String dataFornecimentoFim = (String) request.getParameter(DATA_FORNECIMENTO_FIM);
		model.addAttribute(DATA_FORNECIMENTO_FIM, dataFornecimentoFim);
		model.addAttribute(DATA_FORNECIMENTO_INICIO, dataFornecimentoInicio);
		model.addAttribute(DATA_EMISSAO_NOTA_FISCAL, dataEmissaoNotaFiscal);

		String valor = (String) request.getParameter(VALOR);
		String volume = (String) request.getParameter(VOLUME);
		String dataVencimento = (String) request.getParameter(DATA_VENCIMENTO);
		model.addAttribute(DATA_VENCIMENTO, dataVencimento);
		Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
				.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);

		if (precoGasItens != null) {
			precoGas.getPrecoGasItens().clear();
			precoGas.getPrecoGasItens().addAll(precoGasItens);
		}

		precoGasItem.setValor(null);
		precoGasItem.setVolume(null);
		precoGasItem.setDataVencimento(null);

		if (valor != null && !valor.isEmpty()) {
			precoGasItem.setValor(Util.converterCampoStringParaValorBigDecimal("Valor", valor,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (volume != null && !volume.isEmpty()) {
			precoGasItem.setVolume(Util.converterCampoStringParaValorBigDecimal("Volume", volume,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}

		if (dataEmissaoNotaFiscal != null && !dataEmissaoNotaFiscal.isEmpty()) {
			precoGas.setDataEmissaoNotaFiscal(Util.converterCampoStringParaData(DATA_DE_EMISSAO_DA_NOTA_FISCAL,
					dataEmissaoNotaFiscal, Constantes.FORMATO_DATA_BR));
		}

		if (dataFornecimentoInicio != null && !dataFornecimentoInicio.isEmpty()) {
			precoGas.setDataFornecimentoInicio(Util.converterCampoStringParaData(DATA_DE_INICIO_DO_FORNECIMENTO,
					dataFornecimentoInicio, Constantes.FORMATO_DATA_BR));

		}

		if (dataFornecimentoFim != null && !dataFornecimentoFim.isEmpty()) {
			precoGas.setDataFornecimentoFim(Util.converterCampoStringParaData(DATA_DE_FIM_DO_FORNECIMENTO,
					dataFornecimentoFim, Constantes.FORMATO_DATA_BR));
		}

		if (dataVencimento != null && !dataVencimento.isEmpty()) {
			precoGasItem.setDataVencimento(
					Util.converterCampoStringParaData(DATA_DE_VENCIMENTO, dataVencimento, Constantes.FORMATO_DATA_BR));
		}
	}
	
	/**
	 * Responsável por limpar lista de itens e redirecionar para tela de inclusão
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("limparPrecoGasItem")
	public String limparPrecoGasItem(HttpServletRequest request, Model model) throws GGASException {

		@SuppressWarnings("unchecked")
		Collection<PrecoGasItem> precoGasItens = (Collection<PrecoGasItem>) request.getSession()
				.getAttribute(REQUEST_LISTA_PRECO_GAS_ITEM);

		if (precoGasItens != null) {
			precoGasItens.clear();
			request.getSession().setAttribute(REQUEST_LISTA_PRECO_GAS_ITEM, precoGasItens);
			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);
		}

		carregarCampos(model);
		saveToken(request);

		return exibirInsercaoPrecoGas(request, model);
	}
	
}
