/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.grupofaturamento;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas ao Grupo Faturamento 
 *
 */
@Controller
public class GrupoFaturamentoAction extends GenericAction {

	private static final int INDICE_FINAL = 2;

	private static final int INDICE_INCIAL = 2;

	private static final String GRUPO_FATURAMENTO = "grupoFaturamento";

	@Autowired
	private ControladorRota controladorRota;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String ID_PERIODICIDADE = "idPeriodicidade";

	private static final String ID_TIPO_LEITURA = "idTipoLeitura";
	
	private static final String DIA_VENCIMENTO = "diaVencimento";

	private static final String IS_CONTROLAR_CICLOS = "isControlarCiclos";
			
	/**
	 * Método responsável pela exibição de tela de pesquisa dos Grupos de Faturamento
	 * 
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaGrupoFaturamento")
	public String exibirPesquisaGrupoFaturamento(Model model) throws NegocioException {

		carregarListas(model);

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaGrupoFaturamento";
	}

	/**
	 * Método responsável pela pesquisa dos Grupos de Faturamento
	 * 
	 * @param grupoFaturamento - {@link GrupoFaturamentoImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link Boolean}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarGrupoFaturamento")
	public String pesquisarGrupoFaturamento(GrupoFaturamentoImpl grupoFaturamento, BindingResult result,
			@RequestParam("habilitado") Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		carregarListas(model);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
		prepararFiltro(grupoFaturamento, habilitado, filtro);

		Collection<TabelaAuxiliar> listaGrupoFaturamento =
				controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, GrupoFaturamentoImpl.class.getName());

		model.addAttribute("listaGrupoFaturamento", listaGrupoFaturamento);

		return "exibirPesquisaGrupoFaturamento";
	}

	/**
	 * Método responsável pela tela de inclusão de Grupos de Faturamento
	 * 
	 * @param model - {@link Model}
	 * @param request - {@link HttpServletRequest}
	 * @return String - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("exibirInclusaoGrupoFaturamento")
	public String exibirInclusaoGrupoFaturamento(Model model, HttpServletRequest request) throws NegocioException {

		carregarListas(model);
		anoMesReferencia(model);
		saveToken(request);

		return "exibirInclusaoGrupoFaturamento";
	}

	/**
	 * Método responsável pela inclusão de Grupos de Faturamento
	 * 
	 * @param grupoFaturamento - {@link GrupoFaturamentoImpl}
	 * @param result - {@link BindingResult}
	 * @param anoMes - {@link String}
	 * @param dia dia da composicao da data
	 * @param indContCascTarif - the indContCascTarif
	 * @param indVencIgualFat - the indVencIgualFat
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("incluirGrupoFaturamento")
	public String incluirGrupoFaturamento(GrupoFaturamentoImpl grupoFaturamento, BindingResult result,
			@RequestParam("referenciaAnoMes") String anoMes, @RequestParam(DIA_VENCIMENTO) Long dia,
			@RequestParam(value="indicadorContiCascataTarifa", required = false) String indContCascTarif,
			@RequestParam(value="indicadorVencimIguaisFatura", required = false) String indVencIgualFat,
			HttpServletRequest request, Model model) throws GGASException {

 		String view = "forward:exibirInclusaoGrupoFaturamento";
		model.addAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
		if(dia != null) {
			model.addAttribute(DIA_VENCIMENTO, grupoFaturamento.getDiaVencimento());
		}
		try {
			grupoFaturamento.setAnoMesReferencia(Integer.valueOf(Util.converterMesAnoEmAnoMes(anoMes)));
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(grupoFaturamento, GrupoFaturamentoImpl.GRUPO_DE_FATURAMENTO);
			if("".equals(grupoFaturamento.getDescricaoAbreviada())) {
				grupoFaturamento.setDescricaoAbreviada(null);
			}

			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(grupoFaturamento);
			
			grupoFaturamento.setIndicadorContinuidadeCascataTarifa(Boolean.FALSE);
			grupoFaturamento.setIndicadorVencimentoIgualFatura(Boolean.FALSE);
			if(indContCascTarif != null && !indContCascTarif.isEmpty()){
				grupoFaturamento.setIndicadorContinuidadeCascataTarifa(Util.converterStringEmBoolean(indContCascTarif));
			}
			
			
			if( indVencIgualFat != null && !indVencIgualFat.isEmpty()){
				if(grupoFaturamento.getIndicadorContinuidadeCascataTarifa()){
					grupoFaturamento.setIndicadorVencimentoIgualFatura(Util.converterStringEmBoolean(indVencIgualFat));
				}
			}
			
			controladorTabelaAuxiliar.inserir(grupoFaturamento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, GrupoFaturamentoImpl.GRUPO_DE_FATURAMENTO);
			view = pesquisarGrupoFaturamento(grupoFaturamento, result, grupoFaturamento.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável pela exibição da tela de alteração dos Grupos de Faturamento
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoGrupoFaturamento")
	public String exbibirAlterarGrupoFaturamento(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		String view = "exibirAlteracaoGrupoFaturamento";
		carregarListas(model);

		anoMesReferencia(model);
		if (!super.isPostBack(request)) {

			try {
				GrupoFaturamentoImpl grupoFaturamento = (GrupoFaturamentoImpl) controladorTabelaAuxiliar.obter(chavePrimaria,
						GrupoFaturamentoImpl.class, "periodicidade", "tipoLeitura");
				model.addAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
				model.addAttribute(DIA_VENCIMENTO, grupoFaturamento.getDiaVencimento());
				model.addAttribute(IS_CONTROLAR_CICLOS, isUtilizadorMultiplosCiclos());
			} catch (GGASException e) {
				mensagemErroParametrizado(model, e);
				view = exibirPesquisaGrupoFaturamento(model);
			}
		}
		saveToken(request);

		return view;
	}

	/**
	 * @param model - {@link Model}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void anoMesReferencia(Model model) throws NegocioException {
		ParametroSistema parametroSistema =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
		String mesAnoSemBarra = Util.converterAnoMesEmMesAno(parametroSistema.getValor());
		String mes = mesAnoSemBarra.substring(0, INDICE_FINAL);
		String ano = mesAnoSemBarra.substring(INDICE_INCIAL, mesAnoSemBarra.length());
		model.addAttribute(ANO_MES_REFERENCIA, mes + "/" + ano);
	}

	/**
	 * Método responsável pela alteração dos Grupos de Faturamento
	 * 
	 * @param grupoFaturamento - {@link GrupoFaturamentoImpl}
	 * @param result - {@link BindingResult}
	 * @param anoMes - {@link String}
	 * @param dia dia da composicao da data
	 * @param indContCascTarif - the indContCascTarif
	 * @param indVencIgualFat - the indVencIgualFat
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarGrupoFaturamento")
	public String alterarGrupoFaturamento(GrupoFaturamentoImpl grupoFaturamento, BindingResult result,
			@RequestParam("referenciaAnoMes") String anoMes, @RequestParam(DIA_VENCIMENTO) Long dia,
			@RequestParam(value="indicadorContiCascataTarifa", required = false) String indContCascTarif,
			@RequestParam(value="indicadorVencimIguaisFatura", required = false) String indVencIgualFat,
			HttpServletRequest request, Model model) throws GGASException {
		
		String view = "forward:exibirAlteracaoGrupoFaturamento";
		model.addAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
		if(dia != null) {
			model.addAttribute(DIA_VENCIMENTO, grupoFaturamento.getDiaVencimento());
		}
		try {
			grupoFaturamento.setAnoMesReferencia(Integer.valueOf(Util.converterMesAnoEmAnoMes(anoMes)));
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(grupoFaturamento, GrupoFaturamentoImpl.GRUPO_DE_FATURAMENTO);
			if("".equals(grupoFaturamento.getDescricaoAbreviada())) {
				grupoFaturamento.setDescricaoAbreviada(null);
			}
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(grupoFaturamento);
			grupoFaturamento.setIndicadorContinuidadeCascataTarifa(Boolean.FALSE);
			grupoFaturamento.setIndicadorVencimentoIgualFatura(Boolean.FALSE);
			
			if(indContCascTarif != null && !indContCascTarif.isEmpty()){
				grupoFaturamento.setIndicadorContinuidadeCascataTarifa(Util.converterStringEmBoolean(indContCascTarif));
			}
			if(indVencIgualFat != null && !indVencIgualFat.isEmpty()){
				if(grupoFaturamento.getIndicadorContinuidadeCascataTarifa()){
					grupoFaturamento.setIndicadorVencimentoIgualFatura(Util.converterStringEmBoolean(indVencIgualFat));
				} 
			}
			controladorTabelaAuxiliar.atualizar(grupoFaturamento);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, GrupoFaturamentoImpl.GRUPO_DE_FATURAMENTO);
			view = pesquisarGrupoFaturamento(grupoFaturamento, result, grupoFaturamento.isHabilitado(), model);
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		}
		return view;
	}

	/**
	 * Método responsável pela exibição da tela de Detalhamento dos Grupos de Faturamento
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("exibirDetalharGrupoFaturamento")
	public String exibirDetalharGrupoFaturamento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws GGASException {

		String view = "exibirDetalharGrupoFaturamento";
		try {
			anoMesReferencia(model);

			GrupoFaturamentoImpl grupoFaturamento = (GrupoFaturamentoImpl) controladorTabelaAuxiliar.obter(chavePrimaria,
					GrupoFaturamentoImpl.class, "periodicidade", "tipoLeitura");
			model.addAttribute(GRUPO_FATURAMENTO, grupoFaturamento);
			model.addAttribute(DIA_VENCIMENTO, grupoFaturamento.getDiaVencimento());
			model.addAttribute(IS_CONTROLAR_CICLOS, isUtilizadorMultiplosCiclos());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
			view = exibirPesquisaGrupoFaturamento(model);
		}

		return view;
	}

	/**
	 * Método responsável pela remoção de Grupos de Faturamento
	 * 
	 * @param chavesPrimarias - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("removerGrupoFaturamento")
	public String removerGrupoFaturamento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) {
		String view = "forward:pesquisarGrupoFaturamento";
		try {
			getDadosAuditoria(request);
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, GrupoFaturamentoImpl.class, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, request, GrupoFaturamentoImpl.GRUPO_DE_FATURAMENTO);
			view = pesquisarGrupoFaturamento(null, null, true, model);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * @param grupoFaturamento - {@link GrupoFaturamentoImpl}
	 * @param habilitado - {@link Boolean}
	 * @param filtro - {@link Map}
	 */
	private void prepararFiltro(GrupoFaturamentoImpl grupoFaturamento, Boolean habilitado, Map<String, Object> filtro) {

		if (grupoFaturamento != null) {
			if (!StringUtils.isEmpty(grupoFaturamento.getDescricao())) {
				filtro.put(DESCRICAO, grupoFaturamento.getDescricao());
			}
			if (!StringUtils.isEmpty(grupoFaturamento.getDescricaoAbreviada())) {
				filtro.put("descricaoAbreviada", grupoFaturamento.getDescricaoAbreviada());
			}
			if (grupoFaturamento.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, grupoFaturamento.getChavePrimaria());
			}

			if (grupoFaturamento.getPeriodicidade() != null) {
				filtro.put(ID_PERIODICIDADE, grupoFaturamento.getPeriodicidade().getChavePrimaria());
			}

			if (grupoFaturamento.getTipoLeitura() != null) {
				filtro.put(ID_TIPO_LEITURA, grupoFaturamento.getTipoLeitura().getChavePrimaria());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
	}

	/**
	 * @param model - {@link Model}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private void carregarListas(Model model) throws NegocioException {
		model.addAttribute("listaPeriodicidades", controladorRota.listarPeriodicidades());
		model.addAttribute("listaTiposLeitura", controladorRota.listarTiposLeitura());
		model.addAttribute(IS_CONTROLAR_CICLOS, isUtilizadorMultiplosCiclos());
	}
	
	private boolean isUtilizadorMultiplosCiclos() throws NegocioException{
		ParametroSistema parametroSistema = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_UTILIZAR_MULTIPLOS_CICLOS);
		return (Integer.parseInt(parametroSistema.getValor()) == 1);
	}

}
