package br.com.ggas.web.faturamento.leitura.dto;

public class AnaliseLeituraDTO {
	
	private Long chavePontoConsumo;
	private Long chaveHistoricoMedicao;
	private String dataLeitura;
	private String leituraAnterior;
	private String leituraAtual;
	private String dataLeituraAnterior;
	private Long chaveAnormalidadeLeitura;
	private String isConsistir;
	
	public Long getChavePontoConsumo() {
		return chavePontoConsumo;
	}
	public void setChavePontoConsumo(Long chavePontoConsumo) {
		this.chavePontoConsumo = chavePontoConsumo;
	}
	public Long getChaveHistoricoMedicao() {
		return chaveHistoricoMedicao;
	}
	public void setChaveHistoricoMedicao(Long chaveHistoricoMedicao) {
		this.chaveHistoricoMedicao = chaveHistoricoMedicao;
	}
	public String getDataLeitura() {
		return dataLeitura;
	}
	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
	public String getLeituraAnterior() {
		return leituraAnterior;
	}
	public void setLeituraAnterior(String leituraAnterior) {
		this.leituraAnterior = leituraAnterior;
	}
	public String getLeituraAtual() {
		return leituraAtual;
	}
	public void setLeituraAtual(String leituraAtual) {
		this.leituraAtual = leituraAtual;
	}
	public String getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}
	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}
	public Long getChaveAnormalidadeLeitura() {
		return chaveAnormalidadeLeitura;
	}
	public void setChaveAnormalidadeLeitura(Long chaveAnormalidadeLeitura) {
		this.chaveAnormalidadeLeitura = chaveAnormalidadeLeitura;
	}
	public String getIsConsistir() {
		return isConsistir;
	}
	public void setIsConsistir(String isConsistir) {
		this.isConsistir = isConsistir;
	}

	
	
}
