/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.geral.apresentacao.deserializadorjson.LocalDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDate;

/**
 * DTO que representa a lista de dados de medição
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientCommentDensity"})
public class DadosMedicaoDTO {

	private Long chaveLeitura;
	private Double novaLeitura;
	private Long chaveAnormalidade;
	private Double pressao;
	private Double temperatura;
	private String observacaoLeitura;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dataLeitura;

	public Long getChaveLeitura() {
		return chaveLeitura;
	}

	public void setChaveLeitura(Long chaveLeitura) {
		this.chaveLeitura = chaveLeitura;
	}

	public Double getNovaLeitura() {
		return novaLeitura;
	}

	public void setNovaLeitura(Double novaLeitura) {
		this.novaLeitura = novaLeitura;
	}

	public Long getChaveAnormalidade() {
		return chaveAnormalidade;
	}

	public void setChaveAnormalidade(Long chaveAnormalidade) {
		this.chaveAnormalidade = chaveAnormalidade;
	}

	public Double getPressao() {
		return pressao;
	}

	public void setPressao(Double pressao) {
		this.pressao = pressao;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public String getObservacaoLeitura() {
		return observacaoLeitura;
	}

	public void setObservacaoLeitura(String observacaoLeitura) {
		this.observacaoLeitura = observacaoLeitura;
	}

	public LocalDate getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(LocalDate dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
}
