package br.com.ggas.web.faturamento.fatura;

import java.io.Serializable;
/**
 * Classe responsável por tranferir dados estre as telas do Controle de Impressão
 *
 */
public class FaturaImpressaoVO implements Serializable {

	private String dataGeracaoInicial;
	private String dataGeracaoFinal;
	private String dataEmissaoInicial;
	private String dataEmissaoFinal;
	private Boolean indicadorEmissao;
	private Long idCliente;
	private Long idRota;
	private Long idGrupoFaturamento;
	private Long[] chavesPrimarias;

	private static final long serialVersionUID = -587678591167821966L;

	public String getDataGeracaoInicial() {
		return dataGeracaoInicial;
	}

	public void setDataGeracaoInicial(String dataGeracaoInicial) {
		this.dataGeracaoInicial = dataGeracaoInicial;
	}

	public String getDataGeracaoFinal() {
		return dataGeracaoFinal;
	}

	public void setDataGeracaoFinal(String dataGeracaoFinal) {
		this.dataGeracaoFinal = dataGeracaoFinal;
	}

	public String getDataEmissaoInicial() {
		return dataEmissaoInicial;
	}

	public void setDataEmissaoInicial(String dataEmissaoInicial) {
		this.dataEmissaoInicial = dataEmissaoInicial;
	}

	public String getDataEmissaoFinal() {
		return dataEmissaoFinal;
	}

	public void setDataEmissaoFinal(String dataEmissaoFinal) {
		this.dataEmissaoFinal = dataEmissaoFinal;
	}

	public Boolean getIndicadorEmissao() {
		return indicadorEmissao;
	}

	public void setIndicadorEmissao(Boolean indicadorEmissao) {
		this.indicadorEmissao = indicadorEmissao;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	public Long[] getChavesPrimarias() {
		Long[] chaves = null;
		if(this.chavesPrimarias != null) {
			chaves = this.chavesPrimarias.clone(); 
		}
		return chaves;
	}

	public void setChavesPrimarias(Long[] chaves) {
		if(chaves != null) {
			this.chavesPrimarias = chaves.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}
}
