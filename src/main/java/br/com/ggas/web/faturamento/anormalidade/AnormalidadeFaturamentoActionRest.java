/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.anormalidade;

import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.impl.FaturamentoAnormalidadeImpl;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Action responsável por gerenciar as anormalidades de faturamento
 *
 * @author italo.alan@logiquesistemas.com.br
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
@Controller
public class AnormalidadeFaturamentoActionRest extends GenericActionRest {

	private static final Logger LOG = Logger.getLogger(AnormalidadeFaturamentoActionRest.class);

	@Autowired
	private ControladorFaturamentoAnormalidade controladorFaturamentoAnormalidade;

	/**
	 * Exibe a tela de listagem de anormalidades
	 *
	 * @return retorna o {@link ModelAndView} referente a página de retorno
	 * @throws GGASException lançada caso ocorra falha na busca
	 */
	@RequestMapping("pesquisarFaturamentoAnormalidade")
	public ModelAndView pesquisarFaturamentoAnormalidade(@RequestParam(required = false) String descricao,
			@RequestParam(required = false) Boolean indicadorUso,
			@RequestParam(required = false) Boolean indicadorImpedeFaturamento) throws GGASException {
		Fachada fachada = Fachada.getInstancia();
		Collection<FaturamentoAnormalidade> faturamentoAnormalidades =
				fachada.listarAnormalidadesFaturamento(descricao, indicadorUso, indicadorImpedeFaturamento);

		faturamentoAnormalidades = faturamentoAnormalidades.stream()
				.sorted(Comparator.comparing(FaturamentoAnormalidade::getDescricaoLower)).collect(Collectors.toList());

		return new ModelAndView("pesquisarFaturamentoAnormalidade").
				addObject("descricaoAnormalidade", descricao).
				addObject("anormalidadeHabilitado", indicadorUso).
				addObject("anormalidadeIndicadorImpedeFaturamento", indicadorImpedeFaturamento).
				addObject("anormalidades", faturamentoAnormalidades);
	}

	/**
	 * Salvar anormalidade de faturamento
	 * @param faturamentoAnormalidade o objeto rest recebido
	 *
	 * 	@return retorna o {@link ModelAndView} referente a página de retorno
	 */
	@RequestMapping(value = "faturamentoAnormalidade/salvar", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public ResponseEntity<String> salvarAnormalidade(@RequestBody FaturamentoAnormalidadeImpl faturamentoAnormalidade) {

		ResponseEntity<String> resposta;
		try {
			FaturamentoAnormalidade faturamentoBanco =
					(FaturamentoAnormalidade) controladorFaturamentoAnormalidade.obter(faturamentoAnormalidade.getChavePrimaria());
			faturamentoBanco.setIndicadorImpedeFaturamento(faturamentoAnormalidade.getIndicadorImpedeFaturamento());
			faturamentoBanco.setHabilitado(faturamentoAnormalidade.isHabilitado());
			controladorFaturamentoAnormalidade.atualizar(faturamentoBanco);
			resposta = new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Falha ao salvar ", e);
			resposta = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return resposta;
	}
}
