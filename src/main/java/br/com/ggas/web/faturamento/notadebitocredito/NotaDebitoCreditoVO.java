/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.notadebitocredito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class NotaDebitoCreditoVO extends EntidadeNegocioImpl implements EntidadeNegocio, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4546706690107286502L;

	private String cicloReferencia;

	private String valor;

	private Date dataEmissao;

	private Date dataVencimento;

	private TipoDocumento tipoDocumento;

	private EntidadeConteudo situacaoPagamento;

	private long idPontoConsumo;

	private String saldo;

	private boolean indicadorPontoConsumo;

	private BigDecimal valorTotal;

	private CreditoDebitoSituacao creditoDebitoSituacao;

	private PontoConsumo pontoConsumo;

	private long numeroDocumento;

	private String nomeCliente;

	/**
	 * @return the cicloReferencia
	 */
	public String getCicloReferencia() {

		return cicloReferencia;
	}

	/**
	 * @param cicloReferencia
	 *            the cicloReferencia to set
	 */
	public void setCicloReferencia(String cicloReferencia) {

		this.cicloReferencia = cicloReferencia;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor) {

		this.valor = valor;
	}

	/**
	 * @return the dataEmissao
	 */
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	/**
	 * @param dataEmissao
	 *            the dataEmissao to set
	 */
	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the dataVencimento
	 */
	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the tipoDocumento
	 */
	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the situacaoPagamento
	 */
	public EntidadeConteudo getSituacaoPagamento() {

		return situacaoPagamento;
	}

	/**
	 * @param situacaoPagamento
	 *            the situacaoPagamento to set
	 */
	public void setSituacaoPagamento(EntidadeConteudo situacaoPagamento) {

		this.situacaoPagamento = situacaoPagamento;
	}

	/**
	 * @return the idPontoConsumo
	 */
	public long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	public void setIdPontoConsumo(long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {

		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {

		this.saldo = saldo;
	}

	/**
	 * @param indicadorPontoConsumo
	 *            the indicadorPontoConsumo to set
	 */
	public void setIndicadorPontoConsumo(boolean indicadorPontoConsumo) {

		this.indicadorPontoConsumo = indicadorPontoConsumo;
	}

	/**
	 * @return indicadorPontoConsumo
	 */
	public boolean getIndicadorPontoConsumo() {

		return indicadorPontoConsumo;
	}

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/**
	 * @return valorTotal
	 */
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/**
	 * @return creditoDebitoSituacao
	 */
	public CreditoDebitoSituacao getCreditoDebitoSituacao() {

		return creditoDebitoSituacao;
	}

	/**
	 * @param creditoDebitoSituacao
	 *            the creditoDebitoSituacao to set
	 */
	public void setCreditoDebitoSituacao(CreditoDebitoSituacao creditoDebitoSituacao) {

		this.creditoDebitoSituacao = creditoDebitoSituacao;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	public long getNumeroDocumento() {

		numeroDocumento = getChavePrimaria();
		return numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

}
