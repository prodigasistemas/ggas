package br.com.ggas.web.faturamento.tarifa;

/**
 * @author Everton
 * Classe criada com o propósito de realizar a interpolação de dados do spring com
 * a camada de controller.
 *
 */
public class AssocTempTarifaPontoConsumoVO {

	private String dataInicioVigencia;

	private String dataFimVigencia;

	private Long idPontoConsumo;

	private Long idCliente;

	private Long idTarifa;

	private Long idItemFatura;

	private Integer indexLista;

	/**
	 * @return String
	 */
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	/**
	 * @param dataInicioVigencia 
	 */
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	/**
	 * @return String
	 */
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}

	/**
	 * @param dataFimVigencia
	 */
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	/**
	 * @return Long
	 */
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo 
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	/**
	 * @return Long
	 */
	public Long getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return long
	 */
	public Long getIdTarifa() {
		return idTarifa;
	}

	/**
	 * @param idTarifa
	 */
	public void setIdTarifa(Long idTarifa) {
		this.idTarifa = idTarifa;
	}

	/**
	 * @return Long
	 */
	public Long getIdItemFatura() {
		return idItemFatura;
	}

	/**
	 * @param idItemFatura
	 */
	public void setIdItemFatura(Long idItemFatura) {
		this.idItemFatura = idItemFatura;
	}

	/**
	 * @return Integer
	 */
	public Integer getIndexLista() {
		return indexLista;
	}

	/**
	 * @param indexLista
	 */
	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}

}
