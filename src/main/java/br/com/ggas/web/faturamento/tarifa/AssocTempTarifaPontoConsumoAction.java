package br.com.ggas.web.faturamento.tarifa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por realizar ações das respectivas funcionalidades de associação temporária tarifa com ponto de consumo
 */
@Controller
public class AssocTempTarifaPontoConsumoAction extends GenericAction {

	private String retorno;

	private static final String SUCESSO_MANUTENCAO_LISTA = "sucessoManutencaoLista";

	private static final String LISTA_ITEM_FATURA = "listaItemFatura";

	private static final String DATA_FINAL = "Data Final";

	private static final String LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO = "listaAssocTempTarifaPontoConsumo";

	private static final String DATA_INICIAL = "Data Inicial";

	private static final String INDEX_LISTA = "indexLista";

	private static final String PONTO_PONSUMO = "pontoConsumo";

	private static final String CLIENTE = "cliente";

	private static final String INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1 = "InclusaoAssocTempTarifaPontoConsumoPasso1";

	private Date dataInicial;

	private Date dataFinal;

	private EntidadeConteudo entidadeConteudo;

	@Autowired
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorQuadra controladorQuadra;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorTarifa controladorTarifa;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Método responsável por exibir a tela de Associação de tarifa ao um ponto de consumo
	 *
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @return exibir a página de exibir associação de tarica ao um ponto de consumo {@link - String}
	 * @throws GGASException caso ocorrá algum problema {@link - GGASExpection}
	 */
	@RequestMapping("exibirAssocTempTarifaPontoConsumo")
	public String exibirAssocTempTarifaPontoConsumo(Model model, HttpServletRequest request) throws GGASException {

		model.addAttribute("limparDadosClienteImovel", Boolean.TRUE);
		limparAssocTempTarifaPontoConsumoItens(request);

		return "exibirAssocTempTarifaPontoConsumo";
	}

	/**
	 * Método responsável por pesquisar os clientes ou imoveis com pontos de consumo.
	 * 
	 * @param chavePrimariaCliente {@link - Long}
	 * @param chavePrimariaImovel {@link - Long}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @param indicadorPesquisa {@link - String}
	 * @return retornará os pontos de consumos encontrados {@link String}
	 * @throws GGASException caso ocorrá algum problema {@link - GGASException}
	 */
	@RequestMapping("pesquisarPontosConsumo")
	public String pesquisarPontosConsumo(@RequestParam(value = "chavePrimariaCliente", required = false) Long chavePrimariaCliente,
			@RequestParam(value = "chavePrimariaImovel", required = false) Long chavePrimariaImovel, Model model,
			HttpServletRequest request, @RequestParam(value = "indicadorPesquisa", required = false) String indicadorPesquisa)
			throws GGASException {

		Collection<PontoConsumo> listaPontoConsumo = null;

		model.addAttribute("indicadorPesquisa", indicadorPesquisa);

		if (chavePrimariaImovel != null && chavePrimariaImovel > 0) {
			listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(chavePrimariaImovel, Boolean.TRUE);
			model.addAttribute("imovel", controladorImovel.obter(chavePrimariaImovel));
		}

		if (chavePrimariaCliente != null && chavePrimariaCliente > 0) {
			listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(chavePrimariaCliente);
			model.addAttribute(CLIENTE, controladorCliente.obter(chavePrimariaCliente));
		}

		model.addAttribute("listaPontoConsumo", listaPontoConsumo);

		return exibirAssocTempTarifaPontoConsumo(model, request);
	}

	/**
	 * Exibe a tela de inclusão associação de tarifa temporária com ponto de consuno
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param chavePrimariaPontoConsumo {@link - Long}
	 * @param model {@link - Model}
	 * @param chavePrimariaCliente {@link - Long}
	 * @return retornará a tela de inclusão de tarifa temporária com ponto de consumo {@link - String}
	 * @throws GGASException caso ocorrá algum erro {@link - GGASException}
	 */
	@RequestMapping("exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1")
	public String exibirIncluisaoAssocTemporariaTarifaPontoConsumoPasso1(HttpServletRequest request,
			@RequestParam(value = "chavePrimariaPontoConsumo", required = true) Long chavePrimariaPontoConsumo, Model model,
			@RequestParam(value = "chavePrimariaCliente", required = false) Long chavePrimariaCliente) throws GGASException {

		retorno = pesquisarPontosConsumo(null, null, model, request, null);

		saveToken(request);

		PontoConsumoImpl pontoConsumo = (PontoConsumoImpl) controladorPontoConsumo.obter(chavePrimariaPontoConsumo);
		model.addAttribute(PONTO_PONSUMO, pontoConsumo);

		if (chavePrimariaCliente != null && chavePrimariaCliente > 0) {
			model.addAttribute(CLIENTE, controladorCliente.obter(chavePrimariaCliente));
		}

		try {

			Collection<EntidadeConteudo> itensFatura = this.listaItensFatura(pontoConsumo);

			Collection<TarifaPontoConsumo> listaAssocTempTarifaPontoConsumo = new ArrayList<TarifaPontoConsumo>();
			carregarCamposTarifaPontoConsumo(model);

			if (!super.isPostBack(request)) {
				listaAssocTempTarifaPontoConsumo.addAll(controladorTarifa.listarTarifaPontoConsumo(chavePrimariaPontoConsumo));
			} else {
				listaAssocTempTarifaPontoConsumo =
						(Collection<TarifaPontoConsumo>) request.getSession().getAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO);
			}

			model.addAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);
			model.addAttribute(LISTA_ITEM_FATURA, itensFatura);
			request.getSession().setAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);

			retorno = INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1;

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}
		return retorno;
	}

	/**
	 * Método responsável por adicionar ou alterar as tarifas com seu respectivo ponto de consumo.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param request {@link - HttpServletRequest}
	 * @param model {@link - Model}
	 * @return retornará para mesma tela com a tarifa adicionada ou alterada. {@link - String}
	 * @throws GGASException caso ocorrá algum erro. {@link - GGASException}
	 */
	@RequestMapping("adicionarAssocTempTarifaPontoConsumo")
	public String adicionarAssocTempTarifaPontoConsumo(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO,
			HttpServletRequest request, Model model) throws GGASException {

		PontoConsumoImpl pontoConsumo = null;
		Integer indexLista = null;
				
		if(assocTempTarifaPontoConsumoVO != null) {
			pontoConsumo = (PontoConsumoImpl) controladorPontoConsumo.obter(assocTempTarifaPontoConsumoVO.getIdPontoConsumo());
			indexLista = assocTempTarifaPontoConsumoVO.getIndexLista();
		}

		popularClienteModel(assocTempTarifaPontoConsumoVO, model);

		Collection<EntidadeConteudo> listaItemFatura = this.listaItensFatura(pontoConsumo);

		List<TarifaPontoConsumo> listaAssocTempTarifaPontoConsumo =
				(List<TarifaPontoConsumo>) request.getSession().getAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO);

		if (listaAssocTempTarifaPontoConsumo == null) {
			listaAssocTempTarifaPontoConsumo = new ArrayList<TarifaPontoConsumo>();
		}
		try {

			validarToken(request);

			if (indexLista == null || indexLista < 0) {

				TarifaPontoConsumo itemTarifaPontoConsumo = (TarifaPontoConsumo) controladorTarifa.criarTarifaPontoConsumo();
				this.popularTarifaPontoConsumo(assocTempTarifaPontoConsumoVO, pontoConsumo, request, itemTarifaPontoConsumo);

				controladorTarifa.validarItemFaturaRepetidoTarifaPontoConsumo(listaAssocTempTarifaPontoConsumo, itemTarifaPontoConsumo,
						indexLista);
				listaAssocTempTarifaPontoConsumo.add(itemTarifaPontoConsumo);

			} else {
				this.alterarTarifaPontoConsumoItens(assocTempTarifaPontoConsumoVO, listaAssocTempTarifaPontoConsumo, request, pontoConsumo,
						model);
				model.addAttribute("paramAlteracao", Boolean.TRUE);
			}

			model.addAttribute(SUCESSO_MANUTENCAO_LISTA, true);

		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		Collections.sort(listaAssocTempTarifaPontoConsumo, new Comparator<TarifaPontoConsumo>() {

			@Override
			public int compare(TarifaPontoConsumo d1, TarifaPontoConsumo d2) {

				return d1.getDataInicioVigencia().compareTo(d2.getDataInicioVigencia());
			}
		});

		Collections.reverse(listaAssocTempTarifaPontoConsumo);

		popularModelCamposAssocTarifa(assocTempTarifaPontoConsumoVO, model);
		popularModelAdicionarAssocTarifa(model, pontoConsumo, listaItemFatura, listaAssocTempTarifaPontoConsumo);

		saveToken(request);

		return INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1;

	}

	/**
	 * Método responsável por popular o cliente para ser exibido na tela caso exclua, adicione ou altere as tarifas associadas.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param model {@link - Model}
	 * @throws NegocioException caso ocorrá algum erro {@link - NegocioException}
	 */
	private void popularClienteModel(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO, Model model) throws NegocioException {
		if (assocTempTarifaPontoConsumoVO != null && assocTempTarifaPontoConsumoVO.getIdCliente() != null
				&& assocTempTarifaPontoConsumoVO.getIdCliente() > 0) {
			model.addAttribute(CLIENTE, controladorCliente.obter(assocTempTarifaPontoConsumoVO.getIdCliente()));
		}
	}

	/**
	 * Método responsável por montar receber o model e popular objetos para ser exibidos na tela.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param model {@link - Model}
	 */
	private void popularModelCamposAssocTarifa(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO, Model model) {

		if (assocTempTarifaPontoConsumoVO != null) {
			model.addAttribute("dataInicioVigencia", assocTempTarifaPontoConsumoVO.getDataInicioVigencia());
			model.addAttribute("dataFimVigencia", assocTempTarifaPontoConsumoVO.getDataFimVigencia());
			model.addAttribute("idItemFatura", assocTempTarifaPontoConsumoVO.getIdItemFatura());
			model.addAttribute("idTarifa", assocTempTarifaPontoConsumoVO.getIdTarifa());
			model.addAttribute(INDEX_LISTA, assocTempTarifaPontoConsumoVO.getIndexLista());
		}
	}

	/**
	 * * Método responsável por montar receber o model e popular objetos para ser exibidos na tela.
	 * 
	 * @param model {@link - Model}
	 * @param pontoConsumo {@link - PontoConsumoImpl}
	 * @param listaItemFatura {@link - EntidadeConteudo}
	 * @param listaAssocTempTarifaPontoConsumo {@link - TarifaPontoConsumo}
	 */
	private void popularModelAdicionarAssocTarifa(Model model, PontoConsumoImpl pontoConsumo, Collection<EntidadeConteudo> listaItemFatura,
			List<TarifaPontoConsumo> listaAssocTempTarifaPontoConsumo) {

		model.addAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);
		model.addAttribute(LISTA_ITEM_FATURA, listaItemFatura);
		model.addAttribute(PONTO_PONSUMO, pontoConsumo);

	}

	/**
	 * Método responsável por alterar as tarifas associadas temporariamente ao um ponto de consumo.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param listaTarifaPontoConsumo {@link - TarifaPontoConsumo}
	 * @param request {@link - HttpServletRequest}
	 * @param pontoConsumo {@link - PontoConsumo}
	 * @param model {@link - Model}
	 * @throws GGASException caso ocorrá algum erro. {@link GGASException}
	 */
	public void alterarTarifaPontoConsumoItens(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO,
			Collection<TarifaPontoConsumo> listaTarifaPontoConsumo, HttpServletRequest request, PontoConsumoImpl pontoConsumo, Model model)
			throws GGASException {

		if (listaTarifaPontoConsumo != null && assocTempTarifaPontoConsumoVO.getIndexLista() != null) {
			TarifaPontoConsumo itemTarifaPontoConsumo = (TarifaPontoConsumo) controladorTarifa.criarTarifaPontoConsumo();

			itemTarifaPontoConsumo.setChavePrimaria(((List<TarifaPontoConsumo>) listaTarifaPontoConsumo)
					.get(assocTempTarifaPontoConsumoVO.getIndexLista()).getChavePrimaria());

			this.popularTarifaPontoConsumo(assocTempTarifaPontoConsumoVO, pontoConsumo, request, itemTarifaPontoConsumo);

			try {
				controladorTarifa.validarItemFaturaRepetidoTarifaPontoConsumo(listaTarifaPontoConsumo, itemTarifaPontoConsumo,
						assocTempTarifaPontoConsumoVO.getIndexLista());
			} catch (GGASException e) {
				mensagemErro(model, new NegocioException(e));
			}

			((List) listaTarifaPontoConsumo).remove(assocTempTarifaPontoConsumoVO.getIndexLista().intValue());
			((List) listaTarifaPontoConsumo).add(itemTarifaPontoConsumo);
		}

	}

	/**
	 * Método responsável limpar o atributo da sessão.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @throws GGASException caso ocorrá algum erro. {@link GGASException}
	 */
	private void limparAssocTempTarifaPontoConsumoItens(HttpServletRequest request) throws GGASException {

		request.getSession().removeAttribute("listaPontoConsumo");
	}

	/**
	 * Lista itens fatura.
	 * 
	 * @param pontoConsumo {@link - PontoConsumo}
	 * @return retornará uma collection de EntidadeConteúdo {@link EntidadeConteudo}
	 * @throws GGASException {@link - GGASException
	 */
	private Collection<EntidadeConteudo> listaItensFatura(PontoConsumo pontoConsumo) throws GGASException {

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
		}

		return controladorTarifa.listarItemFaturaPontoConsumo(contratoPontoConsumo);
	}

	/**
	 * Carregar campos tarifa ponto consumo.
	 * 
	 * @param model {@link - Model}
	 * @throws GGASException {@link - GGASException}
	 */
	private void carregarCamposTarifaPontoConsumo(Model model) throws GGASException {

		model.addAttribute("listaTipoDevolucao", controladorEntidadeConteudo.listarTipoDevolucao());
	}

	/**
	 * Método responsável por verificar consistencia de dados da entidade idItemFatura, para ser populado no método.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @return Boolean.
	 */
	private boolean verificarItemFatura(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO) {
		return assocTempTarifaPontoConsumoVO != null && assocTempTarifaPontoConsumoVO.getIdItemFatura() != null
				&& assocTempTarifaPontoConsumoVO.getIdItemFatura() > 0;
	}

	/**
	 * Método responsável por verificar consistencia de dados da entidade idItemFatura, para ser populado no método.
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @return Boolean.
	 */
	private boolean verificarItemTarifa(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO) {
		return assocTempTarifaPontoConsumoVO != null && assocTempTarifaPontoConsumoVO.getIdTarifa() != null
				&& assocTempTarifaPontoConsumoVO.getIdTarifa() > 0;
	}

	/**
	 * Método responsável por preparar os itens para adicionar associação ou alterar as tarifas temporariamente associadas ao ponto de
	 * consumo
	 * 
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param pontoConsumo {@link - PontoConsumo}
	 * @param request {@link - HttpServletRequest}
	 * @param itemTarifaPontoConsumo {@link - TarifaPontoConsumo}
	 * @throws GGASException caso ocorrá algum erro {@link - GGASException}
	 */
	private void popularTarifaPontoConsumo(AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO, PontoConsumoImpl pontoConsumo,
			HttpServletRequest request, TarifaPontoConsumo itemTarifaPontoConsumo) throws GGASException {

		dataInicial = null;
		dataFinal = null;

		if (verificarItemFatura(assocTempTarifaPontoConsumoVO)) {
			entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(assocTempTarifaPontoConsumoVO.getIdItemFatura());
		}

		if (pontoConsumo != null && pontoConsumo.getChavePrimaria() > 0) {
			itemTarifaPontoConsumo.setPontoConsumo(pontoConsumo);
		}

		if (verificarItemTarifa(assocTempTarifaPontoConsumoVO)) {
			Tarifa tarifa = controladorTarifa.obterTarifa(assocTempTarifaPontoConsumoVO.getIdTarifa());
			tarifa.setItemFatura(entidadeConteudo);
			itemTarifaPontoConsumo.setTarifa(tarifa);
		}

		if (assocTempTarifaPontoConsumoVO != null && !StringUtils.isEmpty(assocTempTarifaPontoConsumoVO.getDataInicioVigencia())) {
			itemTarifaPontoConsumo.setDataInicioVigencia(Util.converterCampoStringParaData(DATA_INICIAL,
					assocTempTarifaPontoConsumoVO.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR));

			dataInicial =
					Util.converterCampoStringParaData(DATA_INICIAL, assocTempTarifaPontoConsumoVO.getDataInicioVigencia(),
							Constantes.FORMATO_DATA_BR);
		}

		if (assocTempTarifaPontoConsumoVO != null && !StringUtils.isEmpty(assocTempTarifaPontoConsumoVO.getDataFimVigencia())) {
			itemTarifaPontoConsumo.setDataFimVigencia(Util.converterCampoStringParaData(DATA_FINAL,
					assocTempTarifaPontoConsumoVO.getDataFimVigencia(), Constantes.FORMATO_DATA_BR));

			dataFinal =
					Util.converterCampoStringParaData(DATA_FINAL, assocTempTarifaPontoConsumoVO.getDataFimVigencia(),
							Constantes.FORMATO_DATA_BR);
		}

		if(assocTempTarifaPontoConsumoVO != null) {
			controladorTarifa.validarDadosAdicionarAssocTempTarifaPontoConsumo(dataInicial, dataFinal,
					assocTempTarifaPontoConsumoVO.getIdItemFatura(), assocTempTarifaPontoConsumoVO.getIdTarifa());
		}

		itemTarifaPontoConsumo.setHabilitado(Boolean.TRUE);
		itemTarifaPontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
		itemTarifaPontoConsumo.setDadosAuditoria(getDadosAuditoria(request));

	}

	/**
	 * Método responsável por excluir uma TarifaPontoConsumo.
	 * 
	 * @param request {@link - HttpServletRequest}
	 * @param assocTempTarifaPontoConsumoVO {@link - AssocTempTarifaPontoConsumoVO}
	 * @param model {@link - Model }
	 * @param response {@link - HttpServletResponse}
	 * @return retorna para mesma página apagando a tarifa associada ao ponto de consumo
	 * @throws GGASException caso ocorrá algum erro.
	 */
	@RequestMapping("excluirAssocTemporariaTarifaPontoConsumoItem")
	public String excluirAssocTemporariaTarifaPontoConsumoItem(HttpServletRequest request,
			AssocTempTarifaPontoConsumoVO assocTempTarifaPontoConsumoVO, Model model, HttpServletResponse response) throws GGASException {

		popularClienteModel(assocTempTarifaPontoConsumoVO, model);

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(assocTempTarifaPontoConsumoVO.getIdPontoConsumo());

		Collection<EntidadeConteudo> itensFatura = this.listaItensFatura(pontoConsumo);

		List<TarifaPontoConsumo> listaAssocTempTarifaPontoConsumo =
				(List<TarifaPontoConsumo>) request.getSession().getAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO);
		try {

			validarToken(request);
			if (listaAssocTempTarifaPontoConsumo != null && assocTempTarifaPontoConsumoVO.getIndexLista() != null) {
				TarifaPontoConsumo tarifaPontoConsumo =
						listaAssocTempTarifaPontoConsumo.get(assocTempTarifaPontoConsumoVO.getIndexLista().intValue());
				if (tarifaPontoConsumo != null && tarifaPontoConsumo.getChavePrimaria() > 0) {
					controladorTarifa.validarTarifaFaturamento(tarifaPontoConsumo);
				}

				listaAssocTempTarifaPontoConsumo.remove(assocTempTarifaPontoConsumoVO.getIndexLista().intValue());
			}

			if (listaAssocTempTarifaPontoConsumo != null) {
				Collections.sort(listaAssocTempTarifaPontoConsumo, new Comparator<TarifaPontoConsumo>() {

					@Override
					public int compare(TarifaPontoConsumo d1, TarifaPontoConsumo d2) {

						return d1.getDataInicioVigencia().compareTo(d2.getDataInicioVigencia());
					}
				});

				Collections.reverse(listaAssocTempTarifaPontoConsumo);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addAttribute(PONTO_PONSUMO, pontoConsumo);
		model.addAttribute(LISTA_ITEM_FATURA, itensFatura);
		model.addAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);
		request.getSession().setAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);
		model.addAttribute(INDEX_LISTA, -1);
		saveToken(request);

		return INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1;

	}

	/**
	 * Método responsável por salvar o ponto de consumo com suas tarifas de ponto de consumo.
	 * 
	 * @param idPontoConsumo {@link - Long}
	 * @param idCliente {@link - Long}
	 * @param model {@link - Model}
	 * @param request {@link - HttpServletRequest}
	 * @param response {@link - HttpServletResponse}
	 * @return retorna para mesma tela com a mensagem de sucesso {@link - String}
	 * @throws GGASException caso ocorrá algum erro {@link - GGASException}
	 */
	@RequestMapping(INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1)
	public String incluirAssocTemporariaTarifaPontoConsumoPasso1(@RequestParam("idPontoConsumo") Long idPontoConsumo,
			@RequestParam("idCliente") Long idCliente, Model model, HttpServletRequest request, HttpServletResponse response)
			throws GGASException {

		List<TarifaPontoConsumo> lista =
				(List<TarifaPontoConsumo>) request.getSession().getAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO);

		if (idCliente != null && idCliente > 0) {
			model.addAttribute("cliente", controladorCliente.obter(idCliente));
		}

		controladorTarifa.atualizarTarifasPontoConsumo(idPontoConsumo, lista, getDadosAuditoria(request));

		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(idPontoConsumo);

		Collection<EntidadeConteudo> itensFatura = this.listaItensFatura(pontoConsumo);

		List<TarifaPontoConsumo> listaAssocTempTarifaPontoConsumo =
				(List<TarifaPontoConsumo>) request.getSession().getAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO);

		Collections.sort(listaAssocTempTarifaPontoConsumo, new Comparator<TarifaPontoConsumo>() {

			@Override
			public int compare(TarifaPontoConsumo d1, TarifaPontoConsumo d2) {

				return d1.getDataInicioVigencia().compareTo(d2.getDataInicioVigencia());
			}
		});
		Collections.reverse(listaAssocTempTarifaPontoConsumo);

		request.setAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);
		request.setAttribute(LISTA_ITEM_FATURA, itensFatura);
		request.getSession().setAttribute(LISTA_ASSOC_TEMP_TARIFA_PONTO_CONSUMO, listaAssocTempTarifaPontoConsumo);

		this.popularModelAdicionarAssocTarifa(model, (PontoConsumoImpl) pontoConsumo, itensFatura, listaAssocTempTarifaPontoConsumo);

		mensagemSucesso(model, Constantes.OPERACAO_CONCLUIDA_COM_SUCESSO);

		return INCLUSAO_ASSOC_TEMP_TARIFA_PASSO1;
	}

}
