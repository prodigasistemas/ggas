/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;

/**
 * Possui a implementação do cálculo de volume de leitura,
 * incluindo a caso de virada de medidor
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Component
public class CalculadoraVolumeLeitura {

	@Autowired
	private ControladorMedidor controladorMedidor;

	/**
	 * Cálcula o volume consumido por um cliente para uma determinada leitura
	 * O cálculo envolve a subtração da leitura atual e a leitura anterior.
	 * No caso em que ocorre virada de medidor é realizada a seguinte fórmula:
	 *
	 * MAIOR_VALOR_MEDIDOR - leitura anterior + leitura atual + 1,
	 * Onde MAIOR_VALOR_MEDIDOR é o maior valor possível que pode ser representado pelos dígitos do visor do medidor
	 *
	 * @param leitura leitura movimento
	 * @return retorna o volume consumido. Caso não exista leitura, nulo é retornado
	 */
	public Double calcularVolume(LeituraMovimentoDTO leitura) {
		Double retorno = null;
		if (leitura.getLeituraAnterior() != null && leitura.getLeituraAtual() != null) {
			retorno = realizarCalculoVolume(leitura.getLeituraAnterior(), leitura.getLeituraAtual(), leitura.getChaveLeituraMovimento());
		}
		return retorno;
	}

	private Double realizarCalculoVolume(Double leituraAnterior, Double leituraAtual, Long chaveLeitura) {
		Double volume = leituraAtual - leituraAnterior;

		if (leituraAtual < leituraAnterior) {
			volume = realizarCalculoViradaMedidor(leituraAnterior, leituraAtual, chaveLeitura);
		}

		return volume;

	}

	private Double realizarCalculoViradaMedidor(Double leituraAnterior, Double leituraAtual, Long chaveLeitura) {
		final Integer qtdDigitos = controladorMedidor.obterDigitoMedidorPorLeituraMovimento(chaveLeitura);
		final Double maiorValorVisorMedidor = obterMaiorInteiroNDigitos(qtdDigitos);

		return maiorValorVisorMedidor - leituraAnterior + leituraAtual + 1;
	}

	/**
	 * Obtém o maior inteiro possível para N dígitos
	 * Exemplo: digitos 6, retorno será 999999
	 * 
	 * @param digitos número de dígitos
	 * @return retorna o maior inteiro para n dígitos
	 */
	@SuppressWarnings("squid:S109")
	private Double obterMaiorInteiroNDigitos(Integer digitos) {
		return Math.pow(10, digitos) - 1;
	}

}
